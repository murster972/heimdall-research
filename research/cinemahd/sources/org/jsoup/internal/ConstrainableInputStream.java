package org.jsoup.internal;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;
import org.jsoup.helper.Validate;

public final class ConstrainableInputStream extends BufferedInputStream {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f6960a;
    private final int b;
    private long c;
    private long d = -1;
    private int e;
    private boolean f;

    private ConstrainableInputStream(InputStream inputStream, int i, int i2) {
        super(inputStream, i);
        boolean z = true;
        Validate.b(i2 >= 0);
        this.b = i2;
        this.e = i2;
        this.f6960a = i2 == 0 ? false : z;
        this.c = System.nanoTime();
    }

    public static ConstrainableInputStream a(InputStream inputStream, int i, int i2) {
        if (inputStream instanceof ConstrainableInputStream) {
            return (ConstrainableInputStream) inputStream;
        }
        return new ConstrainableInputStream(inputStream, i, i2);
    }

    private boolean s() {
        if (this.d != -1 && System.nanoTime() - this.c > this.d) {
            return true;
        }
        return false;
    }

    public ByteBuffer b(int i) throws IOException {
        boolean z = true;
        Validate.b(i >= 0, "maxSize must be 0 (unlimited) or larger");
        if (i <= 0) {
            z = false;
        }
        int i2 = 32768;
        if (z && i < 32768) {
            i2 = i;
        }
        byte[] bArr = new byte[i2];
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(i2);
        while (true) {
            int read = read(bArr);
            if (read == -1) {
                break;
            }
            if (z) {
                if (read >= i) {
                    byteArrayOutputStream.write(bArr, 0, i);
                    break;
                }
                i -= read;
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
        return ByteBuffer.wrap(byteArrayOutputStream.toByteArray());
    }

    public int read(byte[] bArr, int i, int i2) throws IOException {
        int i3;
        if (this.f || (this.f6960a && this.e <= 0)) {
            return -1;
        }
        if (Thread.interrupted()) {
            this.f = true;
            return -1;
        } else if (!s()) {
            if (this.f6960a && i2 > (i3 = this.e)) {
                i2 = i3;
            }
            try {
                int read = super.read(bArr, i, i2);
                this.e -= read;
                return read;
            } catch (SocketTimeoutException unused) {
                return 0;
            }
        } else {
            throw new SocketTimeoutException("Read timeout");
        }
    }

    public void reset() throws IOException {
        super.reset();
        this.e = this.b - this.markpos;
    }

    public ConstrainableInputStream a(long j, long j2) {
        this.c = j;
        this.d = j2 * 1000000;
        return this;
    }
}
