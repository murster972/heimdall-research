package org.jsoup;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.net.URL;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

public interface Connection {

    public interface Base<T extends Base> {
        URL a();

        T a(String str);

        T a(String str, String str2);

        T a(URL url);

        T a(Method method);

        Map<String, String> b();

        T b(String str, String str2);

        boolean b(String str);

        Map<String, List<String>> c();

        Method method();
    }

    public interface KeyVal {
        String a();

        InputStream b();

        boolean c();

        String key();

        String value();
    }

    public enum Method {
        GET(false),
        POST(true),
        PUT(true),
        DELETE(false),
        PATCH(true),
        HEAD(false),
        OPTIONS(false),
        TRACE(false);
        
        private final boolean hasBody;

        private Method(boolean z) {
            this.hasBody = z;
        }

        public final boolean a() {
            return this.hasBody;
        }
    }

    public interface Request extends Base<Request> {
        Request a(int i);

        Request a(Parser parser);

        Request a(boolean z);

        Request c(String str);

        boolean d();

        String e();

        boolean f();

        Proxy g();

        Collection<KeyVal> h();

        boolean i();

        boolean j();

        String k();

        int l();

        Parser m();

        int timeout();
    }

    public interface Response extends Base<Response> {
        String body();

        Document parse() throws IOException;
    }

    Connection a(int i);

    Connection a(String str);

    Connection a(String str, String str2);

    Connection a(boolean z);

    Connection b(String str);

    Response execute() throws IOException;

    Document get() throws IOException;
}
