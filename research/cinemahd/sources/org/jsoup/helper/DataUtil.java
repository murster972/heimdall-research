package org.jsoup.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.IllegalCharsetNameException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.internal.ConstrainableInputStream;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.XmlDeclaration;
import org.jsoup.parser.Parser;

public final class DataUtil {

    /* renamed from: a  reason: collision with root package name */
    private static final Pattern f6955a = Pattern.compile("(?i)\\bcharset=\\s*(?:[\"'])?([^\\s,;\"']*)");
    private static final char[] b = "-_1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();

    private static class BomCharset {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final String f6956a;
        /* access modifiers changed from: private */
        public final int b;

        public BomCharset(String str, int i) {
            this.f6956a = str;
            this.b = i;
        }
    }

    private DataUtil() {
    }

    static void a(InputStream inputStream, OutputStream outputStream) throws IOException {
        byte[] bArr = new byte[32768];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                return;
            }
        }
    }

    private static String b(String str) {
        if (!(str == null || str.length() == 0)) {
            String replaceAll = str.trim().replaceAll("[\"']", "");
            try {
                if (Charset.isSupported(replaceAll)) {
                    return replaceAll;
                }
                String upperCase = replaceAll.toUpperCase(Locale.ENGLISH);
                if (Charset.isSupported(upperCase)) {
                    return upperCase;
                }
            } catch (IllegalCharsetNameException unused) {
            }
        }
        return null;
    }

    static Document a(InputStream inputStream, String str, String str2, Parser parser) throws IOException {
        if (inputStream == null) {
            return new Document(str2);
        }
        ConstrainableInputStream a2 = ConstrainableInputStream.a(inputStream, 32768, 0);
        a2.mark(5120);
        ByteBuffer a3 = a((InputStream) a2, 5119);
        boolean z = a2.read() == -1;
        a2.reset();
        BomCharset a4 = a(a3);
        if (a4 != null) {
            str = a4.f6956a;
            a2.skip((long) a4.b);
        }
        Document document = null;
        if (str == null) {
            Document a5 = parser.a(Charset.forName("UTF-8").decode(a3).toString(), str2);
            Iterator it2 = a5.g("meta[http-equiv=content-type], meta[charset]").iterator();
            String str3 = null;
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                if (element.d("http-equiv")) {
                    str3 = a(element.b("content"));
                }
                if (str3 == null && element.d("charset")) {
                    str3 = element.b("charset");
                    continue;
                }
                if (str3 != null) {
                    break;
                }
            }
            if (str3 == null && a5.c() > 0 && (a5.a(0) instanceof XmlDeclaration)) {
                XmlDeclaration xmlDeclaration = (XmlDeclaration) a5.a(0);
                if (xmlDeclaration.u().equals("xml")) {
                    str3 = xmlDeclaration.b("encoding");
                }
            }
            String b2 = b(str3);
            if (b2 != null && !b2.equalsIgnoreCase("UTF-8")) {
                str = b2.trim().replaceAll("[\"']", "");
            } else if (z) {
                document = a5;
            }
        } else {
            Validate.a(str, "Must set charset arg to character set of file to parse. Set to null to attempt to detect from HTML");
        }
        if (document == null) {
            if (str == null) {
                str = "UTF-8";
            }
            document = parser.a((Reader) new BufferedReader(new InputStreamReader(a2, str), 32768), str2);
            document.I().a(str);
        }
        a2.close();
        return document;
    }

    static String b() {
        StringBuilder sb = new StringBuilder(32);
        Random random = new Random();
        for (int i = 0; i < 32; i++) {
            char[] cArr = b;
            sb.append(cArr[random.nextInt(cArr.length)]);
        }
        return sb.toString();
    }

    public static ByteBuffer a(InputStream inputStream, int i) throws IOException {
        Validate.b(i >= 0, "maxSize must be 0 (unlimited) or larger");
        return ConstrainableInputStream.a(inputStream, 32768, i).b(i);
    }

    static ByteBuffer a() {
        return ByteBuffer.allocate(0);
    }

    static String a(String str) {
        if (str == null) {
            return null;
        }
        Matcher matcher = f6955a.matcher(str);
        if (matcher.find()) {
            return b(matcher.group(1).trim().replace("charset=", ""));
        }
        return null;
    }

    private static BomCharset a(ByteBuffer byteBuffer) {
        byteBuffer.mark();
        byte[] bArr = new byte[4];
        if (byteBuffer.remaining() >= bArr.length) {
            byteBuffer.get(bArr);
            byteBuffer.rewind();
        }
        if ((bArr[0] == 0 && bArr[1] == 0 && bArr[2] == -2 && bArr[3] == -1) || (bArr[0] == -1 && bArr[1] == -2 && bArr[2] == 0 && bArr[3] == 0)) {
            return new BomCharset("UTF-32", 0);
        }
        if ((bArr[0] == -2 && bArr[1] == -1) || (bArr[0] == -1 && bArr[1] == -2)) {
            return new BomCharset("UTF-16", 0);
        }
        if (bArr[0] == -17 && bArr[1] == -69 && bArr[2] == -65) {
            return new BomCharset("UTF-8", 3);
        }
        return null;
    }
}
