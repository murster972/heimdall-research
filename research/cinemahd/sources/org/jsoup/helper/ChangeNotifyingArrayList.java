package org.jsoup.helper;

import java.util.ArrayList;
import java.util.Collection;

public abstract class ChangeNotifyingArrayList<E> extends ArrayList<E> {
    public ChangeNotifyingArrayList(int i) {
        super(i);
    }

    public abstract void a();

    public boolean add(E e) {
        a();
        return super.add(e);
    }

    public boolean addAll(Collection<? extends E> collection) {
        a();
        return super.addAll(collection);
    }

    public void clear() {
        a();
        super.clear();
    }

    public E remove(int i) {
        a();
        return super.remove(i);
    }

    public boolean removeAll(Collection<?> collection) {
        a();
        return super.removeAll(collection);
    }

    /* access modifiers changed from: protected */
    public void removeRange(int i, int i2) {
        a();
        super.removeRange(i, i2);
    }

    public boolean retainAll(Collection<?> collection) {
        a();
        return super.retainAll(collection);
    }

    public E set(int i, E e) {
        a();
        return super.set(i, e);
    }

    public void add(int i, E e) {
        a();
        super.add(i, e);
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        a();
        return super.addAll(i, collection);
    }

    public boolean remove(Object obj) {
        a();
        return super.remove(obj);
    }
}
