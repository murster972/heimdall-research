package org.jsoup.helper;

import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import org.jsoup.Connection;
import org.jsoup.UncheckedIOException;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;
import org.jsoup.parser.TokenQueue;

public class HttpConnection implements Connection {

    /* renamed from: a  reason: collision with root package name */
    private Connection.Request f6957a = new Request();
    private Connection.Response b = new Response();

    private static abstract class Base<T extends Connection.Base> implements Connection.Base<T> {

        /* renamed from: a  reason: collision with root package name */
        URL f6958a;
        Connection.Method b;
        Map<String, List<String>> c;
        Map<String, String> d;

        private static String g(String str) {
            try {
                byte[] bytes = str.getBytes("ISO-8859-1");
                if (!a(bytes)) {
                    return str;
                }
                return new String(bytes, "UTF-8");
            } catch (UnsupportedEncodingException unused) {
                return str;
            }
        }

        private List<String> h(String str) {
            Validate.a((Object) str);
            for (Map.Entry next : this.c.entrySet()) {
                if (str.equalsIgnoreCase((String) next.getKey())) {
                    return (List) next.getValue();
                }
            }
            return Collections.emptyList();
        }

        private Map.Entry<String, List<String>> i(String str) {
            String a2 = Normalizer.a(str);
            for (Map.Entry<String, List<String>> next : this.c.entrySet()) {
                if (Normalizer.a(next.getKey()).equals(a2)) {
                    return next;
                }
            }
            return null;
        }

        public URL a() {
            return this.f6958a;
        }

        public boolean b(String str) {
            Validate.a(str, "Header name must not be empty");
            return h(str).size() != 0;
        }

        public T c(String str, String str2) {
            Validate.b(str);
            if (str2 == null) {
                str2 = "";
            }
            List f = f(str);
            if (f.isEmpty()) {
                f = new ArrayList();
                this.c.put(str, f);
            }
            f.add(g(str2));
            return this;
        }

        public boolean d(String str, String str2) {
            Validate.b(str);
            Validate.b(str2);
            for (String equalsIgnoreCase : f(str)) {
                if (str2.equalsIgnoreCase(equalsIgnoreCase)) {
                    return true;
                }
            }
            return false;
        }

        public String e(String str) {
            Validate.a((Object) str, "Header name must not be null");
            List<String> h = h(str);
            if (h.size() > 0) {
                return StringUtil.a((Collection) h, ", ");
            }
            return null;
        }

        public List<String> f(String str) {
            Validate.b(str);
            return h(str);
        }

        public Connection.Method method() {
            return this.b;
        }

        private Base() {
            this.c = new LinkedHashMap();
            this.d = new LinkedHashMap();
        }

        public T a(URL url) {
            Validate.a((Object) url, "URL must not be null");
            this.f6958a = url;
            return this;
        }

        public T b(String str, String str2) {
            Validate.a(str, "Cookie name must not be empty");
            Validate.a((Object) str2, "Cookie value must not be null");
            this.d.put(str, str2);
            return this;
        }

        public T a(Connection.Method method) {
            Validate.a((Object) method, "Method must not be null");
            this.b = method;
            return this;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:13:0x0026, code lost:
            if ((((r8[1] & 255) == 187) & ((r8[2] & 255) == 191)) != false) goto L_0x002a;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static boolean a(byte[] r8) {
            /*
                int r0 = r8.length
                r1 = 3
                r2 = 0
                r3 = 1
                if (r0 < r1) goto L_0x0029
                byte r0 = r8[r2]
                r0 = r0 & 255(0xff, float:3.57E-43)
                r4 = 239(0xef, float:3.35E-43)
                if (r0 != r4) goto L_0x0029
                byte r0 = r8[r3]
                r0 = r0 & 255(0xff, float:3.57E-43)
                r4 = 187(0xbb, float:2.62E-43)
                if (r0 != r4) goto L_0x0018
                r0 = 1
                goto L_0x0019
            L_0x0018:
                r0 = 0
            L_0x0019:
                r4 = 2
                byte r4 = r8[r4]
                r4 = r4 & 255(0xff, float:3.57E-43)
                r5 = 191(0xbf, float:2.68E-43)
                if (r4 != r5) goto L_0x0024
                r4 = 1
                goto L_0x0025
            L_0x0024:
                r4 = 0
            L_0x0025:
                r0 = r0 & r4
                if (r0 == 0) goto L_0x0029
                goto L_0x002a
            L_0x0029:
                r1 = 0
            L_0x002a:
                int r0 = r8.length
            L_0x002b:
                if (r1 >= r0) goto L_0x005d
                byte r4 = r8[r1]
                r5 = r4 & 128(0x80, float:1.794E-43)
                if (r5 != 0) goto L_0x0034
                goto L_0x005a
            L_0x0034:
                r5 = r4 & 224(0xe0, float:3.14E-43)
                r6 = 192(0xc0, float:2.69E-43)
                if (r5 != r6) goto L_0x003d
                int r4 = r1 + 1
                goto L_0x004e
            L_0x003d:
                r5 = r4 & 240(0xf0, float:3.36E-43)
                r7 = 224(0xe0, float:3.14E-43)
                if (r5 != r7) goto L_0x0046
                int r4 = r1 + 2
                goto L_0x004e
            L_0x0046:
                r4 = r4 & 248(0xf8, float:3.48E-43)
                r5 = 240(0xf0, float:3.36E-43)
                if (r4 != r5) goto L_0x005c
                int r4 = r1 + 3
            L_0x004e:
                if (r1 >= r4) goto L_0x005a
                int r1 = r1 + 1
                byte r5 = r8[r1]
                r5 = r5 & r6
                r7 = 128(0x80, float:1.794E-43)
                if (r5 == r7) goto L_0x004e
                return r2
            L_0x005a:
                int r1 = r1 + r3
                goto L_0x002b
            L_0x005c:
                return r2
            L_0x005d:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.helper.HttpConnection.Base.a(byte[]):boolean");
        }

        public Map<String, String> b() {
            return this.d;
        }

        public boolean d(String str) {
            Validate.a(str, "Cookie name must not be empty");
            return this.d.containsKey(str);
        }

        public Map<String, List<String>> c() {
            return this.c;
        }

        public T a(String str, String str2) {
            Validate.a(str, "Header name must not be empty");
            a(str);
            c(str, str2);
            return this;
        }

        public T a(String str) {
            Validate.a(str, "Header name must not be empty");
            Map.Entry<String, List<String>> i = i(str);
            if (i != null) {
                this.c.remove(i.getKey());
            }
            return this;
        }
    }

    public static class Request extends Base<Connection.Request> implements Connection.Request {
        private Proxy e;
        private int f = HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT;
        private int g = 1048576;
        private boolean h = true;
        private Collection<Connection.KeyVal> i = new ArrayList();
        private String j = null;
        private boolean k = false;
        private boolean l = false;
        private Parser m;
        /* access modifiers changed from: private */
        public boolean n = false;
        private boolean o = true;
        private String p = "UTF-8";

        Request() {
            super();
            this.b = Connection.Method.GET;
            c("Accept-Encoding", "gzip");
            c("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36");
            this.m = Parser.b();
        }

        public boolean d() {
            return this.k;
        }

        public String e() {
            return this.p;
        }

        public Proxy g() {
            return this.e;
        }

        public Collection<Connection.KeyVal> h() {
            return this.i;
        }

        public boolean i() {
            return this.h;
        }

        public boolean j() {
            return this.l;
        }

        public String k() {
            return this.j;
        }

        public int l() {
            return this.g;
        }

        public Parser m() {
            return this.m;
        }

        public int timeout() {
            return this.f;
        }

        public Connection.Request c(String str) {
            this.j = str;
            return this;
        }

        public boolean f() {
            return this.o;
        }

        public Request a(int i2) {
            Validate.b(i2 >= 0, "Timeout milliseconds must be 0 (infinite) or greater");
            this.f = i2;
            return this;
        }

        public Connection.Request a(boolean z) {
            this.l = z;
            return this;
        }

        public Request a(Parser parser) {
            this.m = parser;
            this.n = true;
            return this;
        }
    }

    public static class Response extends Base<Connection.Response> implements Connection.Response {
        private static SSLSocketFactory m;
        private static final Pattern n = Pattern.compile("(application|text)/\\w*\\+?xml.*");
        private ByteBuffer e;
        private InputStream f;
        private String g;
        private String h;
        private boolean i = false;
        private boolean j = false;
        private int k = 0;
        private Connection.Request l;

        Response() {
            super();
        }

        private static String c(Connection.Request request) {
            StringBuilder a2 = StringUtil.a();
            boolean z = true;
            for (Map.Entry next : request.b().entrySet()) {
                if (!z) {
                    a2.append("; ");
                } else {
                    z = false;
                }
                a2.append((String) next.getKey());
                a2.append('=');
                a2.append((String) next.getValue());
            }
            return a2.toString();
        }

        private static HostnameVerifier o() {
            return new HostnameVerifier() {
                public boolean verify(String str, SSLSession sSLSession) {
                    return true;
                }
            };
        }

        /* JADX WARNING: Can't wrap try/catch for region: R(7:5|6|7|8|9|10|11) */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0028 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static synchronized void p() throws java.io.IOException {
            /*
                java.lang.Class<org.jsoup.helper.HttpConnection$Response> r0 = org.jsoup.helper.HttpConnection.Response.class
                monitor-enter(r0)
                javax.net.ssl.SSLSocketFactory r1 = m     // Catch:{ all -> 0x0032 }
                if (r1 != 0) goto L_0x0030
                r1 = 1
                javax.net.ssl.TrustManager[] r1 = new javax.net.ssl.TrustManager[r1]     // Catch:{ all -> 0x0032 }
                r2 = 0
                org.jsoup.helper.HttpConnection$Response$2 r3 = new org.jsoup.helper.HttpConnection$Response$2     // Catch:{ all -> 0x0032 }
                r3.<init>()     // Catch:{ all -> 0x0032 }
                r1[r2] = r3     // Catch:{ all -> 0x0032 }
                java.lang.String r2 = "SSL"
                javax.net.ssl.SSLContext r2 = javax.net.ssl.SSLContext.getInstance(r2)     // Catch:{ KeyManagementException | NoSuchAlgorithmException -> 0x0028 }
                r3 = 0
                java.security.SecureRandom r4 = new java.security.SecureRandom     // Catch:{ KeyManagementException | NoSuchAlgorithmException -> 0x0028 }
                r4.<init>()     // Catch:{ KeyManagementException | NoSuchAlgorithmException -> 0x0028 }
                r2.init(r3, r1, r4)     // Catch:{ KeyManagementException | NoSuchAlgorithmException -> 0x0028 }
                javax.net.ssl.SSLSocketFactory r1 = r2.getSocketFactory()     // Catch:{ KeyManagementException | NoSuchAlgorithmException -> 0x0028 }
                m = r1     // Catch:{ KeyManagementException | NoSuchAlgorithmException -> 0x0028 }
                goto L_0x0030
            L_0x0028:
                java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x0032 }
                java.lang.String r2 = "Can't create unsecure trust manager"
                r1.<init>(r2)     // Catch:{ all -> 0x0032 }
                throw r1     // Catch:{ all -> 0x0032 }
            L_0x0030:
                monitor-exit(r0)
                return
            L_0x0032:
                r1 = move-exception
                monitor-exit(r0)
                throw r1
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.helper.HttpConnection.Response.p():void");
        }

        private void q() {
            Validate.b(this.i, "Request must be executed (with .execute(), .get(), or .post() before getting response body");
            if (this.e == null) {
                Validate.a(this.j, "Request has already been read (with .parse())");
                try {
                    this.e = DataUtil.a(this.f, this.l.l());
                    this.j = true;
                    r();
                } catch (IOException e2) {
                    throw new UncheckedIOException(e2);
                } catch (Throwable th) {
                    this.j = true;
                    r();
                    throw th;
                }
            }
        }

        private void r() {
            InputStream inputStream = this.f;
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused) {
                } catch (Throwable th) {
                    this.f = null;
                    throw th;
                }
                this.f = null;
            }
        }

        public String body() {
            String str;
            q();
            String str2 = this.g;
            if (str2 == null) {
                str = Charset.forName("UTF-8").decode(this.e).toString();
            } else {
                str = Charset.forName(str2).decode(this.e).toString();
            }
            this.e.rewind();
            return str;
        }

        public String n() {
            return this.h;
        }

        public Document parse() throws IOException {
            Validate.b(this.i, "Request must be executed (with .execute(), .get(), or .post() before parsing response");
            ByteBuffer byteBuffer = this.e;
            if (byteBuffer != null) {
                this.f = new ByteArrayInputStream(byteBuffer.array());
                this.j = false;
            }
            Validate.a(this.j, "Input stream already read and parsed, cannot re-read.");
            Document a2 = DataUtil.a(this.f, this.g, this.f6958a.toExternalForm(), this.l.m());
            this.g = a2.I().a().name();
            this.j = true;
            r();
            return a2;
        }

        /* JADX WARNING: Removed duplicated region for block: B:24:0x0080 A[Catch:{ IOException -> 0x01dc }] */
        /* JADX WARNING: Removed duplicated region for block: B:29:0x00a1 A[Catch:{ IOException -> 0x01dc }] */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x010e A[Catch:{ IOException -> 0x01dc }] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        static org.jsoup.helper.HttpConnection.Response a(org.jsoup.Connection.Request r8, org.jsoup.helper.HttpConnection.Response r9) throws java.io.IOException {
            /*
                java.lang.String r0 = "Location"
                java.lang.String r1 = "Request must not be null"
                org.jsoup.helper.Validate.a((java.lang.Object) r8, (java.lang.String) r1)
                java.net.URL r1 = r8.a()
                java.lang.String r1 = r1.getProtocol()
                java.lang.String r2 = "http"
                boolean r2 = r1.equals(r2)
                if (r2 != 0) goto L_0x0028
                java.lang.String r2 = "https"
                boolean r1 = r1.equals(r2)
                if (r1 == 0) goto L_0x0020
                goto L_0x0028
            L_0x0020:
                java.net.MalformedURLException r8 = new java.net.MalformedURLException
                java.lang.String r9 = "Only http & https protocols supported"
                r8.<init>(r9)
                throw r8
            L_0x0028:
                org.jsoup.Connection$Method r1 = r8.method()
                boolean r1 = r1.a()
                java.lang.String r2 = r8.k()
                r3 = 1
                if (r2 == 0) goto L_0x0039
                r2 = 1
                goto L_0x003a
            L_0x0039:
                r2 = 0
            L_0x003a:
                if (r1 != 0) goto L_0x0054
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                r4.<init>()
                java.lang.String r5 = "Cannot set a request body for HTTP method "
                r4.append(r5)
                org.jsoup.Connection$Method r5 = r8.method()
                r4.append(r5)
                java.lang.String r4 = r4.toString()
                org.jsoup.helper.Validate.a((boolean) r2, (java.lang.String) r4)
            L_0x0054:
                java.util.Collection r4 = r8.h()
                int r4 = r4.size()
                r5 = 0
                if (r4 <= 0) goto L_0x0067
                if (r1 == 0) goto L_0x0063
                if (r2 == 0) goto L_0x0067
            L_0x0063:
                d(r8)
                goto L_0x006e
            L_0x0067:
                if (r1 == 0) goto L_0x006e
                java.lang.String r1 = e(r8)
                goto L_0x006f
            L_0x006e:
                r1 = r5
            L_0x006f:
                long r6 = java.lang.System.nanoTime()
                java.net.HttpURLConnection r2 = a((org.jsoup.Connection.Request) r8)
                r2.connect()     // Catch:{ IOException -> 0x01dc }
                boolean r4 = r2.getDoOutput()     // Catch:{ IOException -> 0x01dc }
                if (r4 == 0) goto L_0x0087
                java.io.OutputStream r4 = r2.getOutputStream()     // Catch:{ IOException -> 0x01dc }
                a(r8, r4, r1)     // Catch:{ IOException -> 0x01dc }
            L_0x0087:
                int r1 = r2.getResponseCode()     // Catch:{ IOException -> 0x01dc }
                org.jsoup.helper.HttpConnection$Response r4 = new org.jsoup.helper.HttpConnection$Response     // Catch:{ IOException -> 0x01dc }
                r4.<init>(r9)     // Catch:{ IOException -> 0x01dc }
                r4.a((java.net.HttpURLConnection) r2, (org.jsoup.Connection.Response) r9)     // Catch:{ IOException -> 0x01dc }
                r4.l = r8     // Catch:{ IOException -> 0x01dc }
                boolean r9 = r4.b(r0)     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x010e
                boolean r9 = r8.i()     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x010e
                r9 = 307(0x133, float:4.3E-43)
                if (r1 == r9) goto L_0x00b9
                org.jsoup.Connection$Method r9 = org.jsoup.Connection.Method.GET     // Catch:{ IOException -> 0x01dc }
                r8.a((org.jsoup.Connection.Method) r9)     // Catch:{ IOException -> 0x01dc }
                java.util.Collection r9 = r8.h()     // Catch:{ IOException -> 0x01dc }
                r9.clear()     // Catch:{ IOException -> 0x01dc }
                r8.c(r5)     // Catch:{ IOException -> 0x01dc }
                java.lang.String r9 = "Content-Type"
                r8.a((java.lang.String) r9)     // Catch:{ IOException -> 0x01dc }
            L_0x00b9:
                java.lang.String r9 = r4.e(r0)     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x00d4
                java.lang.String r0 = "http:/"
                boolean r0 = r9.startsWith(r0)     // Catch:{ IOException -> 0x01dc }
                if (r0 == 0) goto L_0x00d4
                r0 = 6
                char r1 = r9.charAt(r0)     // Catch:{ IOException -> 0x01dc }
                r3 = 47
                if (r1 == r3) goto L_0x00d4
                java.lang.String r9 = r9.substring(r0)     // Catch:{ IOException -> 0x01dc }
            L_0x00d4:
                java.net.URL r0 = r8.a()     // Catch:{ IOException -> 0x01dc }
                java.net.URL r9 = org.jsoup.helper.StringUtil.a((java.net.URL) r0, (java.lang.String) r9)     // Catch:{ IOException -> 0x01dc }
                java.net.URL r9 = org.jsoup.helper.HttpConnection.a((java.net.URL) r9)     // Catch:{ IOException -> 0x01dc }
                r8.a((java.net.URL) r9)     // Catch:{ IOException -> 0x01dc }
                java.util.Map<java.lang.String, java.lang.String> r9 = r4.d     // Catch:{ IOException -> 0x01dc }
                java.util.Set r9 = r9.entrySet()     // Catch:{ IOException -> 0x01dc }
                java.util.Iterator r9 = r9.iterator()     // Catch:{ IOException -> 0x01dc }
            L_0x00ed:
                boolean r0 = r9.hasNext()     // Catch:{ IOException -> 0x01dc }
                if (r0 == 0) goto L_0x0109
                java.lang.Object r0 = r9.next()     // Catch:{ IOException -> 0x01dc }
                java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ IOException -> 0x01dc }
                java.lang.Object r1 = r0.getKey()     // Catch:{ IOException -> 0x01dc }
                java.lang.String r1 = (java.lang.String) r1     // Catch:{ IOException -> 0x01dc }
                java.lang.Object r0 = r0.getValue()     // Catch:{ IOException -> 0x01dc }
                java.lang.String r0 = (java.lang.String) r0     // Catch:{ IOException -> 0x01dc }
                r8.b(r1, r0)     // Catch:{ IOException -> 0x01dc }
                goto L_0x00ed
            L_0x0109:
                org.jsoup.helper.HttpConnection$Response r8 = a((org.jsoup.Connection.Request) r8, (org.jsoup.helper.HttpConnection.Response) r4)     // Catch:{ IOException -> 0x01dc }
                return r8
            L_0x010e:
                r9 = 200(0xc8, float:2.8E-43)
                if (r1 < r9) goto L_0x0116
                r9 = 400(0x190, float:5.6E-43)
                if (r1 < r9) goto L_0x011c
            L_0x0116:
                boolean r9 = r8.d()     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x01cc
            L_0x011c:
                java.lang.String r9 = r4.n()     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x014d
                boolean r0 = r8.j()     // Catch:{ IOException -> 0x01dc }
                if (r0 != 0) goto L_0x014d
                java.lang.String r0 = "text/"
                boolean r0 = r9.startsWith(r0)     // Catch:{ IOException -> 0x01dc }
                if (r0 != 0) goto L_0x014d
                java.util.regex.Pattern r0 = n     // Catch:{ IOException -> 0x01dc }
                java.util.regex.Matcher r0 = r0.matcher(r9)     // Catch:{ IOException -> 0x01dc }
                boolean r0 = r0.matches()     // Catch:{ IOException -> 0x01dc }
                if (r0 == 0) goto L_0x013d
                goto L_0x014d
            L_0x013d:
                org.jsoup.UnsupportedMimeTypeException r0 = new org.jsoup.UnsupportedMimeTypeException     // Catch:{ IOException -> 0x01dc }
                java.lang.String r1 = "Unhandled content type. Must be text/*, application/xml, or application/xhtml+xml"
                java.net.URL r8 = r8.a()     // Catch:{ IOException -> 0x01dc }
                java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x01dc }
                r0.<init>(r1, r9, r8)     // Catch:{ IOException -> 0x01dc }
                throw r0     // Catch:{ IOException -> 0x01dc }
            L_0x014d:
                if (r9 == 0) goto L_0x016f
                java.util.regex.Pattern r0 = n     // Catch:{ IOException -> 0x01dc }
                java.util.regex.Matcher r9 = r0.matcher(r9)     // Catch:{ IOException -> 0x01dc }
                boolean r9 = r9.matches()     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x016f
                boolean r9 = r8 instanceof org.jsoup.helper.HttpConnection.Request     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x016f
                r9 = r8
                org.jsoup.helper.HttpConnection$Request r9 = (org.jsoup.helper.HttpConnection.Request) r9     // Catch:{ IOException -> 0x01dc }
                boolean r9 = r9.n     // Catch:{ IOException -> 0x01dc }
                if (r9 != 0) goto L_0x016f
                org.jsoup.parser.Parser r9 = org.jsoup.parser.Parser.c()     // Catch:{ IOException -> 0x01dc }
                r8.a((org.jsoup.parser.Parser) r9)     // Catch:{ IOException -> 0x01dc }
            L_0x016f:
                java.lang.String r9 = r4.h     // Catch:{ IOException -> 0x01dc }
                java.lang.String r9 = org.jsoup.helper.DataUtil.a((java.lang.String) r9)     // Catch:{ IOException -> 0x01dc }
                r4.g = r9     // Catch:{ IOException -> 0x01dc }
                int r9 = r2.getContentLength()     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x01c3
                org.jsoup.Connection$Method r9 = r8.method()     // Catch:{ IOException -> 0x01dc }
                org.jsoup.Connection$Method r0 = org.jsoup.Connection.Method.HEAD     // Catch:{ IOException -> 0x01dc }
                if (r9 == r0) goto L_0x01c3
                r4.f = r5     // Catch:{ IOException -> 0x01dc }
                java.io.InputStream r9 = r2.getErrorStream()     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x0192
                java.io.InputStream r9 = r2.getErrorStream()     // Catch:{ IOException -> 0x01dc }
                goto L_0x0196
            L_0x0192:
                java.io.InputStream r9 = r2.getInputStream()     // Catch:{ IOException -> 0x01dc }
            L_0x0196:
                r4.f = r9     // Catch:{ IOException -> 0x01dc }
                java.lang.String r9 = "Content-Encoding"
                java.lang.String r0 = "gzip"
                boolean r9 = r4.d(r9, r0)     // Catch:{ IOException -> 0x01dc }
                if (r9 == 0) goto L_0x01ab
                java.util.zip.GZIPInputStream r9 = new java.util.zip.GZIPInputStream     // Catch:{ IOException -> 0x01dc }
                java.io.InputStream r0 = r4.f     // Catch:{ IOException -> 0x01dc }
                r9.<init>(r0)     // Catch:{ IOException -> 0x01dc }
                r4.f = r9     // Catch:{ IOException -> 0x01dc }
            L_0x01ab:
                java.io.InputStream r9 = r4.f     // Catch:{ IOException -> 0x01dc }
                r0 = 32768(0x8000, float:4.5918E-41)
                int r1 = r8.l()     // Catch:{ IOException -> 0x01dc }
                org.jsoup.internal.ConstrainableInputStream r9 = org.jsoup.internal.ConstrainableInputStream.a(r9, r0, r1)     // Catch:{ IOException -> 0x01dc }
                int r8 = r8.timeout()     // Catch:{ IOException -> 0x01dc }
                long r0 = (long) r8     // Catch:{ IOException -> 0x01dc }
                r9.a(r6, r0)     // Catch:{ IOException -> 0x01dc }
                r4.f = r9     // Catch:{ IOException -> 0x01dc }
                goto L_0x01c9
            L_0x01c3:
                java.nio.ByteBuffer r8 = org.jsoup.helper.DataUtil.a()     // Catch:{ IOException -> 0x01dc }
                r4.e = r8     // Catch:{ IOException -> 0x01dc }
            L_0x01c9:
                r4.i = r3
                return r4
            L_0x01cc:
                org.jsoup.HttpStatusException r9 = new org.jsoup.HttpStatusException     // Catch:{ IOException -> 0x01dc }
                java.lang.String r0 = "HTTP error fetching URL"
                java.net.URL r8 = r8.a()     // Catch:{ IOException -> 0x01dc }
                java.lang.String r8 = r8.toString()     // Catch:{ IOException -> 0x01dc }
                r9.<init>(r0, r1, r8)     // Catch:{ IOException -> 0x01dc }
                throw r9     // Catch:{ IOException -> 0x01dc }
            L_0x01dc:
                r8 = move-exception
                r2.disconnect()
                throw r8
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.helper.HttpConnection.Response.a(org.jsoup.Connection$Request, org.jsoup.helper.HttpConnection$Response):org.jsoup.helper.HttpConnection$Response");
        }

        private static String e(Connection.Request request) {
            if (!request.b(TraktV2.HEADER_CONTENT_TYPE)) {
                if (HttpConnection.b(request)) {
                    String b = DataUtil.b();
                    request.a(TraktV2.HEADER_CONTENT_TYPE, "multipart/form-data; boundary=" + b);
                    return b;
                }
                request.a(TraktV2.HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded; charset=" + request.e());
            }
            return null;
        }

        static Response b(Connection.Request request) throws IOException {
            return a(request, (Response) null);
        }

        private static void d(Connection.Request request) throws IOException {
            boolean z;
            URL a2 = request.a();
            StringBuilder a3 = StringUtil.a();
            a3.append(a2.getProtocol());
            a3.append("://");
            a3.append(a2.getAuthority());
            a3.append(a2.getPath());
            a3.append("?");
            if (a2.getQuery() != null) {
                a3.append(a2.getQuery());
                z = false;
            } else {
                z = true;
            }
            for (Connection.KeyVal next : request.h()) {
                Validate.a(next.c(), "InputStream data not supported in URL query string.");
                if (!z) {
                    a3.append('&');
                } else {
                    z = false;
                }
                a3.append(URLEncoder.encode(next.key(), "UTF-8"));
                a3.append('=');
                a3.append(URLEncoder.encode(next.value(), "UTF-8"));
            }
            request.a(new URL(a3.toString()));
            request.h().clear();
        }

        private Response(Response response) throws IOException {
            super();
            if (response != null) {
                this.k = response.k + 1;
                if (this.k >= 20) {
                    throw new IOException(String.format("Too many redirects occurred trying to load URL %s", new Object[]{response.a()}));
                }
            }
        }

        private static HttpURLConnection a(Connection.Request request) throws IOException {
            URLConnection uRLConnection;
            if (request.g() == null) {
                uRLConnection = request.a().openConnection();
            } else {
                uRLConnection = request.a().openConnection(request.g());
            }
            HttpURLConnection httpURLConnection = (HttpURLConnection) uRLConnection;
            httpURLConnection.setRequestMethod(request.method().name());
            httpURLConnection.setInstanceFollowRedirects(false);
            httpURLConnection.setConnectTimeout(request.timeout());
            httpURLConnection.setReadTimeout(request.timeout() / 2);
            if ((httpURLConnection instanceof HttpsURLConnection) && !request.f()) {
                p();
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) httpURLConnection;
                httpsURLConnection.setSSLSocketFactory(m);
                httpsURLConnection.setHostnameVerifier(o());
            }
            if (request.method().a()) {
                httpURLConnection.setDoOutput(true);
            }
            if (request.b().size() > 0) {
                httpURLConnection.addRequestProperty("Cookie", c(request));
            }
            for (Map.Entry next : request.c().entrySet()) {
                for (String addRequestProperty : (List) next.getValue()) {
                    httpURLConnection.addRequestProperty((String) next.getKey(), addRequestProperty);
                }
            }
            return httpURLConnection;
        }

        private void a(HttpURLConnection httpURLConnection, Connection.Response response) throws IOException {
            this.b = Connection.Method.valueOf(httpURLConnection.getRequestMethod());
            this.f6958a = httpURLConnection.getURL();
            httpURLConnection.getResponseCode();
            httpURLConnection.getResponseMessage();
            this.h = httpURLConnection.getContentType();
            a((Map<String, List<String>>) a(httpURLConnection));
            if (response != null) {
                for (Map.Entry next : response.b().entrySet()) {
                    if (!d((String) next.getKey())) {
                        b((String) next.getKey(), (String) next.getValue());
                    }
                }
            }
        }

        private static LinkedHashMap<String, List<String>> a(HttpURLConnection httpURLConnection) {
            LinkedHashMap<String, List<String>> linkedHashMap = new LinkedHashMap<>();
            int i2 = 0;
            while (true) {
                String headerFieldKey = httpURLConnection.getHeaderFieldKey(i2);
                String headerField = httpURLConnection.getHeaderField(i2);
                if (headerFieldKey == null && headerField == null) {
                    return linkedHashMap;
                }
                i2++;
                if (!(headerFieldKey == null || headerField == null)) {
                    if (linkedHashMap.containsKey(headerFieldKey)) {
                        linkedHashMap.get(headerFieldKey).add(headerField);
                    } else {
                        ArrayList arrayList = new ArrayList();
                        arrayList.add(headerField);
                        linkedHashMap.put(headerFieldKey, arrayList);
                    }
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void a(Map<String, List<String>> map) {
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                if (str != null) {
                    List<String> list = (List) next.getValue();
                    if (str.equalsIgnoreCase("Set-Cookie")) {
                        for (String str2 : list) {
                            if (str2 != null) {
                                TokenQueue tokenQueue = new TokenQueue(str2);
                                String trim = tokenQueue.a("=").trim();
                                String trim2 = tokenQueue.c(";").trim();
                                if (trim.length() > 0) {
                                    b(trim, trim2);
                                }
                            }
                        }
                    }
                    for (String c : list) {
                        c(str, c);
                    }
                }
            }
        }

        private static void a(Connection.Request request, OutputStream outputStream, String str) throws IOException {
            Collection<Connection.KeyVal> h2 = request.h();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, request.e()));
            if (str != null) {
                for (Connection.KeyVal next : h2) {
                    bufferedWriter.write("--");
                    bufferedWriter.write(str);
                    bufferedWriter.write("\r\n");
                    bufferedWriter.write("Content-Disposition: form-data; name=\"");
                    bufferedWriter.write(HttpConnection.e(next.key()));
                    bufferedWriter.write("\"");
                    if (next.c()) {
                        bufferedWriter.write("; filename=\"");
                        bufferedWriter.write(HttpConnection.e(next.value()));
                        bufferedWriter.write("\"\r\nContent-Type: ");
                        bufferedWriter.write(next.a() != null ? next.a() : "application/octet-stream");
                        bufferedWriter.write("\r\n\r\n");
                        bufferedWriter.flush();
                        DataUtil.a(next.b(), outputStream);
                        outputStream.flush();
                    } else {
                        bufferedWriter.write("\r\n\r\n");
                        bufferedWriter.write(next.value());
                    }
                    bufferedWriter.write("\r\n");
                }
                bufferedWriter.write("--");
                bufferedWriter.write(str);
                bufferedWriter.write("--");
            } else if (request.k() != null) {
                bufferedWriter.write(request.k());
            } else {
                boolean z = true;
                for (Connection.KeyVal next2 : h2) {
                    if (!z) {
                        bufferedWriter.append('&');
                    } else {
                        z = false;
                    }
                    bufferedWriter.write(URLEncoder.encode(next2.key(), request.e()));
                    bufferedWriter.write(61);
                    bufferedWriter.write(URLEncoder.encode(next2.value(), request.e()));
                }
            }
            bufferedWriter.close();
        }
    }

    private HttpConnection() {
    }

    public static Connection d(String str) {
        HttpConnection httpConnection = new HttpConnection();
        httpConnection.b(str);
        return httpConnection;
    }

    /* access modifiers changed from: private */
    public static String e(String str) {
        if (str == null) {
            return null;
        }
        return str.replaceAll("\"", "%22");
    }

    private static String f(String str) {
        try {
            return a(new URL(str)).toExternalForm();
        } catch (Exception unused) {
            return str;
        }
    }

    public Connection b(String str) {
        Validate.a(str, "Must supply a valid URL");
        try {
            this.f6957a.a(new URL(f(str)));
            return this;
        } catch (MalformedURLException e) {
            throw new IllegalArgumentException("Malformed URL: " + str, e);
        }
    }

    public Connection.Response execute() throws IOException {
        this.b = Response.b(this.f6957a);
        return this.b;
    }

    public Document get() throws IOException {
        this.f6957a.a(Connection.Method.GET);
        execute();
        return this.b.parse();
    }

    static URL a(URL url) {
        try {
            return new URL(new URI(url.toExternalForm().replaceAll(" ", "%20")).toASCIIString());
        } catch (Exception unused) {
            return url;
        }
    }

    /* access modifiers changed from: private */
    public static boolean b(Connection.Request request) {
        for (Connection.KeyVal c : request.h()) {
            if (c.c()) {
                return true;
            }
        }
        return false;
    }

    public Connection a(String str) {
        Validate.a((Object) str, "User agent must not be null");
        this.f6957a.a("User-Agent", str);
        return this;
    }

    public Connection a(int i) {
        this.f6957a.a(i);
        return this;
    }

    public Connection a(boolean z) {
        this.f6957a.a(z);
        return this;
    }

    public Connection a(String str, String str2) {
        this.f6957a.a(str, str2);
        return this;
    }
}
