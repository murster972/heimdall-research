package org.jsoup.select;

import java.util.Iterator;
import org.jsoup.nodes.Element;

abstract class StructuralEvaluator extends Evaluator {

    /* renamed from: a  reason: collision with root package name */
    Evaluator f7010a;

    static class Has extends StructuralEvaluator {
        public Has(Evaluator evaluator) {
            this.f7010a = evaluator;
        }

        public boolean a(Element element, Element element2) {
            Iterator it2 = element2.w().iterator();
            while (it2.hasNext()) {
                Element element3 = (Element) it2.next();
                if (element3 != element2 && this.f7010a.a(element, element3)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format(":has(%s)", new Object[]{this.f7010a});
        }
    }

    static class ImmediateParent extends StructuralEvaluator {
        public ImmediateParent(Evaluator evaluator) {
            this.f7010a = evaluator;
        }

        public boolean a(Element element, Element element2) {
            Element n;
            if (element == element2 || (n = element2.n()) == null || !this.f7010a.a(element, n)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return String.format(":ImmediateParent%s", new Object[]{this.f7010a});
        }
    }

    static class ImmediatePreviousSibling extends StructuralEvaluator {
        public ImmediatePreviousSibling(Evaluator evaluator) {
            this.f7010a = evaluator;
        }

        public boolean a(Element element, Element element2) {
            Element C;
            if (element == element2 || (C = element2.C()) == null || !this.f7010a.a(element, C)) {
                return false;
            }
            return true;
        }

        public String toString() {
            return String.format(":prev%s", new Object[]{this.f7010a});
        }
    }

    static class Not extends StructuralEvaluator {
        public Not(Evaluator evaluator) {
            this.f7010a = evaluator;
        }

        public boolean a(Element element, Element element2) {
            return !this.f7010a.a(element, element2);
        }

        public String toString() {
            return String.format(":not%s", new Object[]{this.f7010a});
        }
    }

    static class Parent extends StructuralEvaluator {
        public Parent(Evaluator evaluator) {
            this.f7010a = evaluator;
        }

        public boolean a(Element element, Element element2) {
            if (element == element2) {
                return false;
            }
            for (Element n = element2.n(); !this.f7010a.a(element, n); n = n.n()) {
                if (n == element) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return String.format(":parent%s", new Object[]{this.f7010a});
        }
    }

    static class PreviousSibling extends StructuralEvaluator {
        public PreviousSibling(Evaluator evaluator) {
            this.f7010a = evaluator;
        }

        public boolean a(Element element, Element element2) {
            if (element == element2) {
                return false;
            }
            for (Element C = element2.C(); C != null; C = C.C()) {
                if (this.f7010a.a(element, C)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format(":prev*%s", new Object[]{this.f7010a});
        }
    }

    static class Root extends Evaluator {
        Root() {
        }

        public boolean a(Element element, Element element2) {
            return element == element2;
        }
    }

    StructuralEvaluator() {
    }
}
