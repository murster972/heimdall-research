package org.jsoup.select;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.jsoup.nodes.Element;

public class Elements extends ArrayList<Element> {
    public Elements() {
    }

    public String a(String str) {
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (element.d(str)) {
                return element.b(str);
            }
        }
        return "";
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
            sb.append(element.l());
        }
        return sb.toString();
    }

    public String c() {
        StringBuilder sb = new StringBuilder();
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (sb.length() != 0) {
                sb.append(" ");
            }
            sb.append(element.G());
        }
        return sb.toString();
    }

    public String toString() {
        return b();
    }

    public Elements(int i) {
        super(i);
    }

    public Elements clone() {
        Elements elements = new Elements(size());
        Iterator it2 = iterator();
        while (it2.hasNext()) {
            elements.add(((Element) it2.next()).clone());
        }
        return elements;
    }

    public Elements(List<Element> list) {
        super(list);
    }

    public Element a() {
        if (isEmpty()) {
            return null;
        }
        return (Element) get(size() - 1);
    }

    public Elements b(String str) {
        return Selector.a(str, (Iterable<Element>) this);
    }
}
