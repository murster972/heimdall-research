package org.jsoup.select;

import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.NodeFilter;

public class Collector {

    private static class Accumulator implements NodeVisitor {

        /* renamed from: a  reason: collision with root package name */
        private final Element f6990a;
        private final Elements b;
        private final Evaluator c;

        Accumulator(Element element, Elements elements, Evaluator evaluator) {
            this.f6990a = element;
            this.b = elements;
            this.c = evaluator;
        }

        public void a(Node node, int i) {
        }

        public void b(Node node, int i) {
            if (node instanceof Element) {
                Element element = (Element) node;
                if (this.c.a(this.f6990a, element)) {
                    this.b.add(element);
                }
            }
        }
    }

    private static class FirstFinder implements NodeFilter {

        /* renamed from: a  reason: collision with root package name */
        private final Element f6991a;
        /* access modifiers changed from: private */
        public Element b = null;
        private final Evaluator c;

        FirstFinder(Element element, Evaluator evaluator) {
            this.f6991a = element;
            this.c = evaluator;
        }

        public NodeFilter.FilterResult b(Node node, int i) {
            if (node instanceof Element) {
                Element element = (Element) node;
                if (this.c.a(this.f6991a, element)) {
                    this.b = element;
                    return NodeFilter.FilterResult.STOP;
                }
            }
            return NodeFilter.FilterResult.CONTINUE;
        }

        public NodeFilter.FilterResult a(Node node, int i) {
            return NodeFilter.FilterResult.CONTINUE;
        }
    }

    private Collector() {
    }

    public static Elements a(Evaluator evaluator, Element element) {
        Elements elements = new Elements();
        NodeTraversor.a((NodeVisitor) new Accumulator(element, elements, evaluator), (Node) element);
        return elements;
    }

    public static Element b(Evaluator evaluator, Element element) {
        FirstFinder firstFinder = new FirstFinder(element, evaluator);
        NodeTraversor.a((NodeFilter) firstFinder, (Node) element);
        return firstFinder.b;
    }
}
