package org.jsoup.select;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.parser.TokenQueue;
import org.jsoup.select.CombiningEvaluator;
import org.jsoup.select.Evaluator;
import org.jsoup.select.Selector;
import org.jsoup.select.StructuralEvaluator;

public class QueryParser {
    private static final String[] d = {",", ">", "+", "~", " "};
    private static final String[] e = {"=", "!=", "^=", "$=", "*=", "~="};
    private static final Pattern f = Pattern.compile("(([+-])?(\\d+)?)n(\\s*([+-])?\\s*\\d+)?", 2);
    private static final Pattern g = Pattern.compile("([+-])?(\\d+)");

    /* renamed from: a  reason: collision with root package name */
    private TokenQueue f7009a;
    private String b;
    private List<Evaluator> c = new ArrayList();

    private QueryParser(String str) {
        this.b = str;
        this.f7009a = new TokenQueue(str);
    }

    public static Evaluator a(String str) {
        try {
            return new QueryParser(str).a();
        } catch (IllegalArgumentException e2) {
            throw new Selector.SelectorParseException(e2.getMessage(), new Object[0]);
        }
    }

    private void b() {
        this.c.add(new Evaluator.AllElements());
    }

    private void c() {
        TokenQueue tokenQueue = new TokenQueue(this.f7009a.a('[', ']'));
        String a2 = tokenQueue.a(e);
        Validate.b(a2);
        tokenQueue.d();
        if (tokenQueue.e()) {
            if (a2.startsWith("^")) {
                this.c.add(new Evaluator.AttributeStarting(a2.substring(1)));
            } else {
                this.c.add(new Evaluator.Attribute(a2));
            }
        } else if (tokenQueue.d("=")) {
            this.c.add(new Evaluator.AttributeWithValue(a2, tokenQueue.h()));
        } else if (tokenQueue.d("!=")) {
            this.c.add(new Evaluator.AttributeWithValueNot(a2, tokenQueue.h()));
        } else if (tokenQueue.d("^=")) {
            this.c.add(new Evaluator.AttributeWithValueStarting(a2, tokenQueue.h()));
        } else if (tokenQueue.d("$=")) {
            this.c.add(new Evaluator.AttributeWithValueEnding(a2, tokenQueue.h()));
        } else if (tokenQueue.d("*=")) {
            this.c.add(new Evaluator.AttributeWithValueContaining(a2, tokenQueue.h()));
        } else if (tokenQueue.d("~=")) {
            this.c.add(new Evaluator.AttributeWithValueMatching(a2, Pattern.compile(tokenQueue.h())));
        } else {
            throw new Selector.SelectorParseException("Could not parse attribute query '%s': unexpected token at '%s'", this.b, tokenQueue.h());
        }
    }

    private void d() {
        String b2 = this.f7009a.b();
        Validate.b(b2);
        this.c.add(new Evaluator.Class(b2.trim()));
    }

    private void e() {
        String b2 = this.f7009a.b();
        Validate.b(b2);
        this.c.add(new Evaluator.Id(b2));
    }

    private void f() {
        String c2 = this.f7009a.c();
        Validate.b(c2);
        if (c2.startsWith("*|")) {
            this.c.add(new CombiningEvaluator.Or(new Evaluator.Tag(Normalizer.b(c2)), new Evaluator.TagEndsWith(Normalizer.b(c2.replace("*|", ":")))));
            return;
        }
        if (c2.contains("|")) {
            c2 = c2.replace("|", ":");
        }
        this.c.add(new Evaluator.Tag(c2.trim()));
    }

    private int g() {
        String trim = this.f7009a.a(")").trim();
        Validate.b(StringUtil.b(trim), "Index must be numeric");
        return Integer.parseInt(trim);
    }

    private String h() {
        StringBuilder sb = new StringBuilder();
        while (!this.f7009a.e()) {
            if (this.f7009a.e("(")) {
                sb.append("(");
                sb.append(this.f7009a.a('(', ')'));
                sb.append(")");
            } else if (this.f7009a.e("[")) {
                sb.append("[");
                sb.append(this.f7009a.a('[', ']'));
                sb.append("]");
            } else if (this.f7009a.b(d)) {
                break;
            } else {
                sb.append(this.f7009a.a());
            }
        }
        return sb.toString();
    }

    private void i() {
        this.f7009a.b(":containsData");
        String f2 = TokenQueue.f(this.f7009a.a('(', ')'));
        Validate.a(f2, ":containsData(text) query must not be empty");
        this.c.add(new Evaluator.ContainsData(f2));
    }

    private void j() {
        if (this.f7009a.d("#")) {
            e();
        } else if (this.f7009a.d(".")) {
            d();
        } else if (this.f7009a.g() || this.f7009a.e("*|")) {
            f();
        } else if (this.f7009a.e("[")) {
            c();
        } else if (this.f7009a.d("*")) {
            b();
        } else if (this.f7009a.d(":lt(")) {
            n();
        } else if (this.f7009a.d(":gt(")) {
            m();
        } else if (this.f7009a.d(":eq(")) {
            l();
        } else if (this.f7009a.e(":has(")) {
            k();
        } else if (this.f7009a.e(":contains(")) {
            a(false);
        } else if (this.f7009a.e(":containsOwn(")) {
            a(true);
        } else if (this.f7009a.e(":containsData(")) {
            i();
        } else if (this.f7009a.e(":matches(")) {
            b(false);
        } else if (this.f7009a.e(":matchesOwn(")) {
            b(true);
        } else if (this.f7009a.e(":not(")) {
            o();
        } else if (this.f7009a.d(":nth-child(")) {
            a(false, false);
        } else if (this.f7009a.d(":nth-last-child(")) {
            a(true, false);
        } else if (this.f7009a.d(":nth-of-type(")) {
            a(false, true);
        } else if (this.f7009a.d(":nth-last-of-type(")) {
            a(true, true);
        } else if (this.f7009a.d(":first-child")) {
            this.c.add(new Evaluator.IsFirstChild());
        } else if (this.f7009a.d(":last-child")) {
            this.c.add(new Evaluator.IsLastChild());
        } else if (this.f7009a.d(":first-of-type")) {
            this.c.add(new Evaluator.IsFirstOfType());
        } else if (this.f7009a.d(":last-of-type")) {
            this.c.add(new Evaluator.IsLastOfType());
        } else if (this.f7009a.d(":only-child")) {
            this.c.add(new Evaluator.IsOnlyChild());
        } else if (this.f7009a.d(":only-of-type")) {
            this.c.add(new Evaluator.IsOnlyOfType());
        } else if (this.f7009a.d(":empty")) {
            this.c.add(new Evaluator.IsEmpty());
        } else if (this.f7009a.d(":root")) {
            this.c.add(new Evaluator.IsRoot());
        } else {
            throw new Selector.SelectorParseException("Could not parse query '%s': unexpected token at '%s'", this.b, this.f7009a.h());
        }
    }

    private void k() {
        this.f7009a.b(":has");
        String a2 = this.f7009a.a('(', ')');
        Validate.a(a2, ":has(el) subselect must not be empty");
        this.c.add(new StructuralEvaluator.Has(a(a2)));
    }

    private void l() {
        this.c.add(new Evaluator.IndexEquals(g()));
    }

    private void m() {
        this.c.add(new Evaluator.IndexGreaterThan(g()));
    }

    private void n() {
        this.c.add(new Evaluator.IndexLessThan(g()));
    }

    private void o() {
        this.f7009a.b(":not");
        String a2 = this.f7009a.a('(', ')');
        Validate.a(a2, ":not(selector) subselect must not be empty");
        this.c.add(new StructuralEvaluator.Not(a(a2)));
    }

    private void b(boolean z) {
        this.f7009a.b(z ? ":matchesOwn" : ":matches");
        String a2 = this.f7009a.a('(', ')');
        Validate.a(a2, ":matches(regex) query must not be empty");
        if (z) {
            this.c.add(new Evaluator.MatchesOwn(Pattern.compile(a2)));
        } else {
            this.c.add(new Evaluator.Matches(Pattern.compile(a2)));
        }
    }

    /* access modifiers changed from: package-private */
    public Evaluator a() {
        this.f7009a.d();
        if (this.f7009a.b(d)) {
            this.c.add(new StructuralEvaluator.Root());
            a(this.f7009a.a());
        } else {
            j();
        }
        while (!this.f7009a.e()) {
            boolean d2 = this.f7009a.d();
            if (this.f7009a.b(d)) {
                a(this.f7009a.a());
            } else if (d2) {
                a(' ');
            } else {
                j();
            }
        }
        if (this.c.size() == 1) {
            return this.c.get(0);
        }
        return new CombiningEvaluator.And((Collection<Evaluator>) this.c);
    }

    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0046  */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0057  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00b0  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(char r11) {
        /*
            r10 = this;
            org.jsoup.parser.TokenQueue r0 = r10.f7009a
            r0.d()
            java.lang.String r0 = r10.h()
            org.jsoup.select.Evaluator r0 = a((java.lang.String) r0)
            java.util.List<org.jsoup.select.Evaluator> r1 = r10.c
            int r1 = r1.size()
            r2 = 44
            r3 = 1
            r4 = 0
            if (r1 != r3) goto L_0x0033
            java.util.List<org.jsoup.select.Evaluator> r1 = r10.c
            java.lang.Object r1 = r1.get(r4)
            org.jsoup.select.Evaluator r1 = (org.jsoup.select.Evaluator) r1
            boolean r5 = r1 instanceof org.jsoup.select.CombiningEvaluator.Or
            if (r5 == 0) goto L_0x003a
            if (r11 == r2) goto L_0x003a
            r5 = r1
            org.jsoup.select.CombiningEvaluator$Or r5 = (org.jsoup.select.CombiningEvaluator.Or) r5
            org.jsoup.select.Evaluator r5 = r5.a()
            r6 = 1
            r9 = r5
            r5 = r1
            r1 = r9
            goto L_0x003c
        L_0x0033:
            org.jsoup.select.CombiningEvaluator$And r1 = new org.jsoup.select.CombiningEvaluator$And
            java.util.List<org.jsoup.select.Evaluator> r5 = r10.c
            r1.<init>((java.util.Collection<org.jsoup.select.Evaluator>) r5)
        L_0x003a:
            r5 = r1
            r6 = 0
        L_0x003c:
            java.util.List<org.jsoup.select.Evaluator> r7 = r10.c
            r7.clear()
            r7 = 62
            r8 = 2
            if (r11 != r7) goto L_0x0057
            org.jsoup.select.CombiningEvaluator$And r11 = new org.jsoup.select.CombiningEvaluator$And
            org.jsoup.select.Evaluator[] r2 = new org.jsoup.select.Evaluator[r8]
            r2[r4] = r0
            org.jsoup.select.StructuralEvaluator$ImmediateParent r0 = new org.jsoup.select.StructuralEvaluator$ImmediateParent
            r0.<init>(r1)
            r2[r3] = r0
            r11.<init>((org.jsoup.select.Evaluator[]) r2)
            goto L_0x00ae
        L_0x0057:
            r7 = 32
            if (r11 != r7) goto L_0x006c
            org.jsoup.select.CombiningEvaluator$And r11 = new org.jsoup.select.CombiningEvaluator$And
            org.jsoup.select.Evaluator[] r2 = new org.jsoup.select.Evaluator[r8]
            r2[r4] = r0
            org.jsoup.select.StructuralEvaluator$Parent r0 = new org.jsoup.select.StructuralEvaluator$Parent
            r0.<init>(r1)
            r2[r3] = r0
            r11.<init>((org.jsoup.select.Evaluator[]) r2)
            goto L_0x00ae
        L_0x006c:
            r7 = 43
            if (r11 != r7) goto L_0x0081
            org.jsoup.select.CombiningEvaluator$And r11 = new org.jsoup.select.CombiningEvaluator$And
            org.jsoup.select.Evaluator[] r2 = new org.jsoup.select.Evaluator[r8]
            r2[r4] = r0
            org.jsoup.select.StructuralEvaluator$ImmediatePreviousSibling r0 = new org.jsoup.select.StructuralEvaluator$ImmediatePreviousSibling
            r0.<init>(r1)
            r2[r3] = r0
            r11.<init>((org.jsoup.select.Evaluator[]) r2)
            goto L_0x00ae
        L_0x0081:
            r7 = 126(0x7e, float:1.77E-43)
            if (r11 != r7) goto L_0x0096
            org.jsoup.select.CombiningEvaluator$And r11 = new org.jsoup.select.CombiningEvaluator$And
            org.jsoup.select.Evaluator[] r2 = new org.jsoup.select.Evaluator[r8]
            r2[r4] = r0
            org.jsoup.select.StructuralEvaluator$PreviousSibling r0 = new org.jsoup.select.StructuralEvaluator$PreviousSibling
            r0.<init>(r1)
            r2[r3] = r0
            r11.<init>((org.jsoup.select.Evaluator[]) r2)
            goto L_0x00ae
        L_0x0096:
            if (r11 != r2) goto L_0x00bd
            boolean r11 = r1 instanceof org.jsoup.select.CombiningEvaluator.Or
            if (r11 == 0) goto L_0x00a3
            org.jsoup.select.CombiningEvaluator$Or r1 = (org.jsoup.select.CombiningEvaluator.Or) r1
            r1.b(r0)
            r11 = r1
            goto L_0x00ae
        L_0x00a3:
            org.jsoup.select.CombiningEvaluator$Or r11 = new org.jsoup.select.CombiningEvaluator$Or
            r11.<init>()
            r11.b(r1)
            r11.b(r0)
        L_0x00ae:
            if (r6 == 0) goto L_0x00b7
            r0 = r5
            org.jsoup.select.CombiningEvaluator$Or r0 = (org.jsoup.select.CombiningEvaluator.Or) r0
            r0.a(r11)
            r11 = r5
        L_0x00b7:
            java.util.List<org.jsoup.select.Evaluator> r0 = r10.c
            r0.add(r11)
            return
        L_0x00bd:
            org.jsoup.select.Selector$SelectorParseException r0 = new org.jsoup.select.Selector$SelectorParseException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Unknown combinator: "
            r1.append(r2)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            java.lang.Object[] r1 = new java.lang.Object[r4]
            r0.<init>(r11, r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.select.QueryParser.a(char):void");
    }

    private void a(boolean z, boolean z2) {
        String b2 = Normalizer.b(this.f7009a.a(")"));
        Matcher matcher = f.matcher(b2);
        Matcher matcher2 = g.matcher(b2);
        int i = 2;
        int i2 = 0;
        if ("odd".equals(b2)) {
            i2 = 1;
        } else if (!"even".equals(b2)) {
            if (matcher.matches()) {
                int parseInt = matcher.group(3) != null ? Integer.parseInt(matcher.group(1).replaceFirst("^\\+", "")) : 1;
                if (matcher.group(4) != null) {
                    i2 = Integer.parseInt(matcher.group(4).replaceFirst("^\\+", ""));
                }
                i = parseInt;
            } else if (matcher2.matches()) {
                i2 = Integer.parseInt(matcher2.group().replaceFirst("^\\+", ""));
                i = 0;
            } else {
                throw new Selector.SelectorParseException("Could not parse nth-index '%s': unexpected format", b2);
            }
        }
        if (z2) {
            if (z) {
                this.c.add(new Evaluator.IsNthLastOfType(i, i2));
            } else {
                this.c.add(new Evaluator.IsNthOfType(i, i2));
            }
        } else if (z) {
            this.c.add(new Evaluator.IsNthLastChild(i, i2));
        } else {
            this.c.add(new Evaluator.IsNthChild(i, i2));
        }
    }

    private void a(boolean z) {
        this.f7009a.b(z ? ":containsOwn" : ":contains");
        String f2 = TokenQueue.f(this.f7009a.a('(', ')'));
        Validate.a(f2, ":contains(text) query must not be empty");
        if (z) {
            this.c.add(new Evaluator.ContainsOwnText(f2));
        } else {
            this.c.add(new Evaluator.ContainsText(f2));
        }
    }
}
