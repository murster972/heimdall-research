package org.jsoup.select;

import java.util.Iterator;
import java.util.regex.Pattern;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.XmlDeclaration;

public abstract class Evaluator {

    public static final class AllElements extends Evaluator {
        public boolean a(Element element, Element element2) {
            return true;
        }

        public String toString() {
            return "*";
        }
    }

    public static final class Attribute extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f6993a;

        public Attribute(String str) {
            this.f6993a = str;
        }

        public boolean a(Element element, Element element2) {
            return element2.d(this.f6993a);
        }

        public String toString() {
            return String.format("[%s]", new Object[]{this.f6993a});
        }
    }

    public static abstract class AttributeKeyPair extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        String f6994a;
        String b;

        public AttributeKeyPair(String str, String str2) {
            Validate.b(str);
            Validate.b(str2);
            this.f6994a = Normalizer.b(str);
            if ((str2.startsWith("\"") && str2.endsWith("\"")) || (str2.startsWith("'") && str2.endsWith("'"))) {
                str2 = str2.substring(1, str2.length() - 1);
            }
            this.b = Normalizer.b(str2);
        }
    }

    public static final class AttributeStarting extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f6995a;

        public AttributeStarting(String str) {
            Validate.b(str);
            this.f6995a = Normalizer.a(str);
        }

        public boolean a(Element element, Element element2) {
            for (org.jsoup.nodes.Attribute key : element2.a().a()) {
                if (Normalizer.a(key.getKey()).startsWith(this.f6995a)) {
                    return true;
                }
            }
            return false;
        }

        public String toString() {
            return String.format("[^%s]", new Object[]{this.f6995a});
        }
    }

    public static final class AttributeWithValue extends AttributeKeyPair {
        public AttributeWithValue(String str, String str2) {
            super(str, str2);
        }

        public boolean a(Element element, Element element2) {
            return element2.d(this.f6994a) && this.b.equalsIgnoreCase(element2.b(this.f6994a).trim());
        }

        public String toString() {
            return String.format("[%s=%s]", new Object[]{this.f6994a, this.b});
        }
    }

    public static final class AttributeWithValueContaining extends AttributeKeyPair {
        public AttributeWithValueContaining(String str, String str2) {
            super(str, str2);
        }

        public boolean a(Element element, Element element2) {
            return element2.d(this.f6994a) && Normalizer.a(element2.b(this.f6994a)).contains(this.b);
        }

        public String toString() {
            return String.format("[%s*=%s]", new Object[]{this.f6994a, this.b});
        }
    }

    public static final class AttributeWithValueEnding extends AttributeKeyPair {
        public AttributeWithValueEnding(String str, String str2) {
            super(str, str2);
        }

        public boolean a(Element element, Element element2) {
            return element2.d(this.f6994a) && Normalizer.a(element2.b(this.f6994a)).endsWith(this.b);
        }

        public String toString() {
            return String.format("[%s$=%s]", new Object[]{this.f6994a, this.b});
        }
    }

    public static final class AttributeWithValueMatching extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        String f6996a;
        Pattern b;

        public AttributeWithValueMatching(String str, Pattern pattern) {
            this.f6996a = Normalizer.b(str);
            this.b = pattern;
        }

        public boolean a(Element element, Element element2) {
            return element2.d(this.f6996a) && this.b.matcher(element2.b(this.f6996a)).find();
        }

        public String toString() {
            return String.format("[%s~=%s]", new Object[]{this.f6996a, this.b.toString()});
        }
    }

    public static final class AttributeWithValueNot extends AttributeKeyPair {
        public AttributeWithValueNot(String str, String str2) {
            super(str, str2);
        }

        public boolean a(Element element, Element element2) {
            return !this.b.equalsIgnoreCase(element2.b(this.f6994a));
        }

        public String toString() {
            return String.format("[%s!=%s]", new Object[]{this.f6994a, this.b});
        }
    }

    public static final class AttributeWithValueStarting extends AttributeKeyPair {
        public AttributeWithValueStarting(String str, String str2) {
            super(str, str2);
        }

        public boolean a(Element element, Element element2) {
            return element2.d(this.f6994a) && Normalizer.a(element2.b(this.f6994a)).startsWith(this.b);
        }

        public String toString() {
            return String.format("[%s^=%s]", new Object[]{this.f6994a, this.b});
        }
    }

    public static final class Class extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f6997a;

        public Class(String str) {
            this.f6997a = str;
        }

        public boolean a(Element element, Element element2) {
            return element2.f(this.f6997a);
        }

        public String toString() {
            return String.format(".%s", new Object[]{this.f6997a});
        }
    }

    public static final class ContainsData extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f6998a;

        public ContainsData(String str) {
            this.f6998a = Normalizer.a(str);
        }

        public boolean a(Element element, Element element2) {
            return Normalizer.a(element2.u()).contains(this.f6998a);
        }

        public String toString() {
            return String.format(":containsData(%s)", new Object[]{this.f6998a});
        }
    }

    public static final class ContainsOwnText extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f6999a;

        public ContainsOwnText(String str) {
            this.f6999a = Normalizer.a(str);
        }

        public boolean a(Element element, Element element2) {
            return Normalizer.a(element2.B()).contains(this.f6999a);
        }

        public String toString() {
            return String.format(":containsOwn(%s)", new Object[]{this.f6999a});
        }
    }

    public static final class ContainsText extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f7000a;

        public ContainsText(String str) {
            this.f7000a = Normalizer.a(str);
        }

        public boolean a(Element element, Element element2) {
            return Normalizer.a(element2.G()).contains(this.f7000a);
        }

        public String toString() {
            return String.format(":contains(%s)", new Object[]{this.f7000a});
        }
    }

    public static abstract class CssNthEvaluator extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        protected final int f7001a;
        protected final int b;

        public CssNthEvaluator(int i, int i2) {
            this.f7001a = i;
            this.b = i2;
        }

        /* access modifiers changed from: protected */
        public abstract String a();

        public boolean a(Element element, Element element2) {
            Element n = element2.n();
            if (n == null || (n instanceof Document)) {
                return false;
            }
            int b2 = b(element, element2);
            int i = this.f7001a;
            if (i == 0) {
                return b2 == this.b;
            }
            int i2 = this.b;
            if ((b2 - i2) * i < 0 || (b2 - i2) % i != 0) {
                return false;
            }
            return true;
        }

        /* access modifiers changed from: protected */
        public abstract int b(Element element, Element element2);

        public String toString() {
            if (this.f7001a == 0) {
                return String.format(":%s(%d)", new Object[]{a(), Integer.valueOf(this.b)});
            } else if (this.b == 0) {
                return String.format(":%s(%dn)", new Object[]{a(), Integer.valueOf(this.f7001a)});
            } else {
                return String.format(":%s(%dn%+d)", new Object[]{a(), Integer.valueOf(this.f7001a), Integer.valueOf(this.b)});
            }
        }
    }

    public static final class Id extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f7002a;

        public Id(String str) {
            this.f7002a = str;
        }

        public boolean a(Element element, Element element2) {
            return this.f7002a.equals(element2.z());
        }

        public String toString() {
            return String.format("#%s", new Object[]{this.f7002a});
        }
    }

    public static final class IndexEquals extends IndexEvaluator {
        public IndexEquals(int i) {
            super(i);
        }

        public boolean a(Element element, Element element2) {
            return element2.v() == this.f7003a;
        }

        public String toString() {
            return String.format(":eq(%d)", new Object[]{Integer.valueOf(this.f7003a)});
        }
    }

    public static abstract class IndexEvaluator extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        int f7003a;

        public IndexEvaluator(int i) {
            this.f7003a = i;
        }
    }

    public static final class IndexGreaterThan extends IndexEvaluator {
        public IndexGreaterThan(int i) {
            super(i);
        }

        public boolean a(Element element, Element element2) {
            return element2.v() > this.f7003a;
        }

        public String toString() {
            return String.format(":gt(%d)", new Object[]{Integer.valueOf(this.f7003a)});
        }
    }

    public static final class IndexLessThan extends IndexEvaluator {
        public IndexLessThan(int i) {
            super(i);
        }

        public boolean a(Element element, Element element2) {
            return element != element2 && element2.v() < this.f7003a;
        }

        public String toString() {
            return String.format(":lt(%d)", new Object[]{Integer.valueOf(this.f7003a)});
        }
    }

    public static final class IsEmpty extends Evaluator {
        public boolean a(Element element, Element element2) {
            for (Node next : element2.d()) {
                if (!(next instanceof Comment) && !(next instanceof XmlDeclaration) && !(next instanceof DocumentType)) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return ":empty";
        }
    }

    public static final class IsFirstChild extends Evaluator {
        public boolean a(Element element, Element element2) {
            Element n = element2.n();
            return n != null && !(n instanceof Document) && element2.v() == 0;
        }

        public String toString() {
            return ":first-child";
        }
    }

    public static final class IsFirstOfType extends IsNthOfType {
        public IsFirstOfType() {
            super(0, 1);
        }

        public String toString() {
            return ":first-of-type";
        }
    }

    public static final class IsLastChild extends Evaluator {
        public boolean a(Element element, Element element2) {
            Element n = element2.n();
            if (n == null || (n instanceof Document) || element2.v() != n.t().size() - 1) {
                return false;
            }
            return true;
        }

        public String toString() {
            return ":last-child";
        }
    }

    public static final class IsLastOfType extends IsNthLastOfType {
        public IsLastOfType() {
            super(0, 1);
        }

        public String toString() {
            return ":last-of-type";
        }
    }

    public static final class IsNthChild extends CssNthEvaluator {
        public IsNthChild(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        public String a() {
            return "nth-child";
        }

        /* access modifiers changed from: protected */
        public int b(Element element, Element element2) {
            return element2.v() + 1;
        }
    }

    public static final class IsNthLastChild extends CssNthEvaluator {
        public IsNthLastChild(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        public String a() {
            return "nth-last-child";
        }

        /* access modifiers changed from: protected */
        public int b(Element element, Element element2) {
            return element2.n().t().size() - element2.v();
        }
    }

    public static class IsNthLastOfType extends CssNthEvaluator {
        public IsNthLastOfType(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        public String a() {
            return "nth-last-of-type";
        }

        /* access modifiers changed from: protected */
        public int b(Element element, Element element2) {
            Elements t = element2.n().t();
            int i = 0;
            for (int v = element2.v(); v < t.size(); v++) {
                if (((Element) t.get(v)).E().equals(element2.E())) {
                    i++;
                }
            }
            return i;
        }
    }

    public static class IsNthOfType extends CssNthEvaluator {
        public IsNthOfType(int i, int i2) {
            super(i, i2);
        }

        /* access modifiers changed from: protected */
        public String a() {
            return "nth-of-type";
        }

        /* access modifiers changed from: protected */
        public int b(Element element, Element element2) {
            Iterator it2 = element2.n().t().iterator();
            int i = 0;
            while (it2.hasNext()) {
                Element element3 = (Element) it2.next();
                if (element3.E().equals(element2.E())) {
                    i++;
                    continue;
                }
                if (element3 == element2) {
                    break;
                }
            }
            return i;
        }
    }

    public static final class IsOnlyChild extends Evaluator {
        public boolean a(Element element, Element element2) {
            Element n = element2.n();
            return n != null && !(n instanceof Document) && element2.D().size() == 0;
        }

        public String toString() {
            return ":only-child";
        }
    }

    public static final class IsOnlyOfType extends Evaluator {
        public boolean a(Element element, Element element2) {
            Element n = element2.n();
            if (n == null || (n instanceof Document)) {
                return false;
            }
            Iterator it2 = n.t().iterator();
            int i = 0;
            while (it2.hasNext()) {
                if (((Element) it2.next()).E().equals(element2.E())) {
                    i++;
                }
            }
            return i == 1;
        }

        public String toString() {
            return ":only-of-type";
        }
    }

    public static final class IsRoot extends Evaluator {
        public boolean a(Element element, Element element2) {
            if (element instanceof Document) {
                element = element.c(0);
            }
            return element2 == element;
        }

        public String toString() {
            return ":root";
        }
    }

    public static final class Matches extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private Pattern f7004a;

        public Matches(Pattern pattern) {
            this.f7004a = pattern;
        }

        public boolean a(Element element, Element element2) {
            return this.f7004a.matcher(element2.G()).find();
        }

        public String toString() {
            return String.format(":matches(%s)", new Object[]{this.f7004a});
        }
    }

    public static final class MatchesOwn extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private Pattern f7005a;

        public MatchesOwn(Pattern pattern) {
            this.f7005a = pattern;
        }

        public boolean a(Element element, Element element2) {
            return this.f7005a.matcher(element2.B()).find();
        }

        public String toString() {
            return String.format(":matchesOwn(%s)", new Object[]{this.f7005a});
        }
    }

    public static final class Tag extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f7006a;

        public Tag(String str) {
            this.f7006a = str;
        }

        public boolean a(Element element, Element element2) {
            return element2.F().equalsIgnoreCase(this.f7006a);
        }

        public String toString() {
            return String.format("%s", new Object[]{this.f7006a});
        }
    }

    public static final class TagEndsWith extends Evaluator {

        /* renamed from: a  reason: collision with root package name */
        private String f7007a;

        public TagEndsWith(String str) {
            this.f7007a = str;
        }

        public boolean a(Element element, Element element2) {
            return element2.F().endsWith(this.f7007a);
        }

        public String toString() {
            return String.format("%s", new Object[]{this.f7007a});
        }
    }

    protected Evaluator() {
    }

    public abstract boolean a(Element element, Element element2);
}
