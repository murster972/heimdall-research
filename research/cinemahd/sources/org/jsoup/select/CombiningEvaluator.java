package org.jsoup.select;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Element;

abstract class CombiningEvaluator extends Evaluator {

    /* renamed from: a  reason: collision with root package name */
    final ArrayList<Evaluator> f6992a;
    int b;

    static final class And extends CombiningEvaluator {
        And(Collection<Evaluator> collection) {
            super(collection);
        }

        public boolean a(Element element, Element element2) {
            for (int i = 0; i < this.b; i++) {
                if (!this.f6992a.get(i).a(element, element2)) {
                    return false;
                }
            }
            return true;
        }

        public String toString() {
            return StringUtil.a((Collection) this.f6992a, " ");
        }

        And(Evaluator... evaluatorArr) {
            this((Collection<Evaluator>) Arrays.asList(evaluatorArr));
        }
    }

    CombiningEvaluator() {
        this.b = 0;
        this.f6992a = new ArrayList<>();
    }

    /* access modifiers changed from: package-private */
    public Evaluator a() {
        int i = this.b;
        if (i > 0) {
            return this.f6992a.get(i - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.b = this.f6992a.size();
    }

    /* access modifiers changed from: package-private */
    public void a(Evaluator evaluator) {
        this.f6992a.set(this.b - 1, evaluator);
    }

    CombiningEvaluator(Collection<Evaluator> collection) {
        this();
        this.f6992a.addAll(collection);
        b();
    }

    static final class Or extends CombiningEvaluator {
        Or(Collection<Evaluator> collection) {
            if (this.b > 1) {
                this.f6992a.add(new And(collection));
            } else {
                this.f6992a.addAll(collection);
            }
            b();
        }

        public boolean a(Element element, Element element2) {
            for (int i = 0; i < this.b; i++) {
                if (this.f6992a.get(i).a(element, element2)) {
                    return true;
                }
            }
            return false;
        }

        public void b(Evaluator evaluator) {
            this.f6992a.add(evaluator);
            b();
        }

        public String toString() {
            return StringUtil.a((Collection) this.f6992a, ", ");
        }

        Or(Evaluator... evaluatorArr) {
            this((Collection<Evaluator>) Arrays.asList(evaluatorArr));
        }

        Or() {
        }
    }
}
