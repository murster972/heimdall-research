package org.jsoup.select;

import java.util.ArrayList;
import java.util.IdentityHashMap;
import java.util.Iterator;
import java.util.List;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Element;

public class Selector {

    public static class SelectorParseException extends IllegalStateException {
        public SelectorParseException(String str, Object... objArr) {
            super(String.format(str, objArr));
        }
    }

    private Selector() {
    }

    public static Elements a(String str, Element element) {
        Validate.b(str);
        return a(QueryParser.a(str), element);
    }

    public static Element b(String str, Element element) {
        Validate.b(str);
        return Collector.b(QueryParser.a(str), element);
    }

    public static Elements a(Evaluator evaluator, Element element) {
        Validate.a((Object) evaluator);
        Validate.a((Object) element);
        return Collector.a(evaluator, element);
    }

    public static Elements a(String str, Iterable<Element> iterable) {
        Validate.b(str);
        Validate.a((Object) iterable);
        Evaluator a2 = QueryParser.a(str);
        ArrayList arrayList = new ArrayList();
        IdentityHashMap identityHashMap = new IdentityHashMap();
        for (Element a3 : iterable) {
            Iterator it2 = a(a2, a3).iterator();
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                if (!identityHashMap.containsKey(element)) {
                    arrayList.add(element);
                    identityHashMap.put(element, Boolean.TRUE);
                }
            }
        }
        return new Elements((List<Element>) arrayList);
    }
}
