package org.jsoup.select;

import org.jsoup.nodes.Node;
import org.jsoup.select.NodeFilter;

public class NodeTraversor {
    public static void a(NodeVisitor nodeVisitor, Node node) {
        Node node2 = node;
        int i = 0;
        while (node2 != null) {
            nodeVisitor.b(node2, i);
            if (node2.c() > 0) {
                node2 = node2.a(0);
                i++;
            } else {
                while (node2.i() == null && i > 0) {
                    nodeVisitor.a(node2, i);
                    node2 = node2.o();
                    i--;
                }
                nodeVisitor.a(node2, i);
                if (node2 != node) {
                    node2 = node2.i();
                } else {
                    return;
                }
            }
        }
    }

    public static NodeFilter.FilterResult a(NodeFilter nodeFilter, Node node) {
        Node node2 = node;
        int i = 0;
        while (node2 != null) {
            NodeFilter.FilterResult b = nodeFilter.b(node2, i);
            if (b == NodeFilter.FilterResult.STOP) {
                return b;
            }
            if (b != NodeFilter.FilterResult.CONTINUE || node2.c() <= 0) {
                while (node2.i() == null && i > 0) {
                    if ((b == NodeFilter.FilterResult.CONTINUE || b == NodeFilter.FilterResult.SKIP_CHILDREN) && (b = nodeFilter.a(node2, i)) == NodeFilter.FilterResult.STOP) {
                        return b;
                    }
                    Node o = node2.o();
                    i--;
                    if (b == NodeFilter.FilterResult.d) {
                        node2.p();
                    }
                    b = NodeFilter.FilterResult.CONTINUE;
                    node2 = o;
                }
                if ((b == NodeFilter.FilterResult.CONTINUE || b == NodeFilter.FilterResult.SKIP_CHILDREN) && (b = nodeFilter.a(node2, i)) == NodeFilter.FilterResult.STOP) {
                    return b;
                }
                if (node2 == node) {
                    return b;
                }
                Node i2 = node2.i();
                if (b == NodeFilter.FilterResult.d) {
                    node2.p();
                }
                node2 = i2;
            } else {
                node2 = node2.a(0);
                i++;
            }
        }
        return NodeFilter.FilterResult.CONTINUE;
    }
}
