package org.jsoup;

import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Parser;

public class Jsoup {
    private Jsoup() {
    }

    public static Document a(String str, String str2, Parser parser) {
        return parser.a(str, str2);
    }

    public static Document b(String str) {
        return Parser.b(str, "");
    }

    public static Connection a(String str) {
        return HttpConnection.d(str);
    }
}
