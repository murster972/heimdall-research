package org.jsoup.parser;

import java.io.Reader;
import java.io.StringReader;
import org.jsoup.nodes.Document;

public class Parser {

    /* renamed from: a  reason: collision with root package name */
    private TreeBuilder f6981a;
    private int b = 0;
    private ParseErrorList c;
    private ParseSettings d;

    public Parser(TreeBuilder treeBuilder) {
        this.f6981a = treeBuilder;
        this.d = treeBuilder.b();
    }

    public static Document b(String str, String str2) {
        HtmlTreeBuilder htmlTreeBuilder = new HtmlTreeBuilder();
        return htmlTreeBuilder.b(new StringReader(str), str2, ParseErrorList.b(), htmlTreeBuilder.b());
    }

    public static Parser c() {
        return new Parser(new XmlTreeBuilder());
    }

    public Document a(String str, String str2) {
        this.c = a() ? ParseErrorList.a(this.b) : ParseErrorList.b();
        return this.f6981a.b(new StringReader(str), str2, this.c, this.d);
    }

    public static Parser b() {
        return new Parser(new HtmlTreeBuilder());
    }

    public Document a(Reader reader, String str) {
        this.c = a() ? ParseErrorList.a(this.b) : ParseErrorList.b();
        return this.f6981a.b(reader, str, this.c, this.d);
    }

    public boolean a() {
        return this.b > 0;
    }
}
