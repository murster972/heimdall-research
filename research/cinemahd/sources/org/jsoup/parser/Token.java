package org.jsoup.parser;

import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Attributes;

abstract class Token {

    /* renamed from: a  reason: collision with root package name */
    TokenType f6983a;

    static final class Character extends Token {
        private String b;

        Character() {
            super();
            this.f6983a = TokenType.Character;
        }

        /* access modifiers changed from: package-private */
        public Character a(String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public Token l() {
            this.b = null;
            return this;
        }

        /* access modifiers changed from: package-private */
        public String n() {
            return this.b;
        }

        public String toString() {
            return n();
        }
    }

    static final class Comment extends Token {
        final StringBuilder b = new StringBuilder();
        boolean c = false;

        Comment() {
            super();
            this.f6983a = TokenType.Comment;
        }

        /* access modifiers changed from: package-private */
        public Token l() {
            Token.a(this.b);
            this.c = false;
            return this;
        }

        /* access modifiers changed from: package-private */
        public String n() {
            return this.b.toString();
        }

        public String toString() {
            return "<!--" + n() + "-->";
        }
    }

    static final class Doctype extends Token {
        final StringBuilder b = new StringBuilder();
        String c = null;
        final StringBuilder d = new StringBuilder();
        final StringBuilder e = new StringBuilder();
        boolean f = false;

        Doctype() {
            super();
            this.f6983a = TokenType.Doctype;
        }

        /* access modifiers changed from: package-private */
        public Token l() {
            Token.a(this.b);
            this.c = null;
            Token.a(this.d);
            Token.a(this.e);
            this.f = false;
            return this;
        }

        /* access modifiers changed from: package-private */
        public String n() {
            return this.b.toString();
        }

        /* access modifiers changed from: package-private */
        public String o() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public String p() {
            return this.d.toString();
        }

        public String q() {
            return this.e.toString();
        }

        public boolean r() {
            return this.f;
        }
    }

    static final class EOF extends Token {
        EOF() {
            super();
            this.f6983a = TokenType.EOF;
        }

        /* access modifiers changed from: package-private */
        public Token l() {
            return this;
        }
    }

    static final class EndTag extends Tag {
        EndTag() {
            this.f6983a = TokenType.EndTag;
        }

        public String toString() {
            return "</" + q() + ">";
        }
    }

    static final class StartTag extends Tag {
        StartTag() {
            this.j = new Attributes();
            this.f6983a = TokenType.StartTag;
        }

        /* access modifiers changed from: package-private */
        public StartTag a(String str, Attributes attributes) {
            this.b = str;
            this.j = attributes;
            this.c = Normalizer.a(this.b);
            return this;
        }

        public String toString() {
            Attributes attributes = this.j;
            if (attributes == null || attributes.size() <= 0) {
                return "<" + q() + ">";
            }
            return "<" + q() + " " + this.j.toString() + ">";
        }

        /* access modifiers changed from: package-private */
        public Tag l() {
            super.l();
            this.j = new Attributes();
            return this;
        }
    }

    static abstract class Tag extends Token {
        protected String b;
        protected String c;
        private String d;
        private StringBuilder e = new StringBuilder();
        private String f;
        private boolean g = false;
        private boolean h = false;
        boolean i = false;
        Attributes j;

        Tag() {
            super();
        }

        private void u() {
            this.h = true;
            String str = this.f;
            if (str != null) {
                this.e.append(str);
                this.f = null;
            }
        }

        /* access modifiers changed from: package-private */
        public final void a(String str) {
            String str2 = this.d;
            if (str2 != null) {
                str = str2.concat(str);
            }
            this.d = str;
        }

        /* access modifiers changed from: package-private */
        public final void b(String str) {
            u();
            if (this.e.length() == 0) {
                this.f = str;
            } else {
                this.e.append(str);
            }
        }

        /* access modifiers changed from: package-private */
        public final void c(String str) {
            String str2 = this.b;
            if (str2 != null) {
                str = str2.concat(str);
            }
            this.b = str;
            this.c = Normalizer.a(this.b);
        }

        /* access modifiers changed from: package-private */
        public final Tag d(String str) {
            this.b = str;
            this.c = Normalizer.a(str);
            return this;
        }

        /* access modifiers changed from: package-private */
        public final void n() {
            if (this.d != null) {
                r();
            }
        }

        /* access modifiers changed from: package-private */
        public final Attributes o() {
            return this.j;
        }

        /* access modifiers changed from: package-private */
        public final boolean p() {
            return this.i;
        }

        /* access modifiers changed from: package-private */
        public final String q() {
            String str = this.b;
            Validate.a(str == null || str.length() == 0);
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public final void r() {
            String str;
            if (this.j == null) {
                this.j = new Attributes();
            }
            String str2 = this.d;
            if (str2 != null) {
                this.d = str2.trim();
                if (this.d.length() > 0) {
                    if (this.h) {
                        str = this.e.length() > 0 ? this.e.toString() : this.f;
                    } else {
                        str = this.g ? "" : null;
                    }
                    this.j.a(this.d, str);
                }
            }
            this.d = null;
            this.g = false;
            this.h = false;
            Token.a(this.e);
            this.f = null;
        }

        /* access modifiers changed from: package-private */
        public final String s() {
            return this.c;
        }

        /* access modifiers changed from: package-private */
        public final void t() {
            this.g = true;
        }

        /* access modifiers changed from: package-private */
        public final void a(char c2) {
            a(String.valueOf(c2));
        }

        /* access modifiers changed from: package-private */
        public Tag l() {
            this.b = null;
            this.c = null;
            this.d = null;
            Token.a(this.e);
            this.f = null;
            this.g = false;
            this.h = false;
            this.i = false;
            this.j = null;
            return this;
        }

        /* access modifiers changed from: package-private */
        public final void a(int[] iArr) {
            u();
            for (int appendCodePoint : iArr) {
                this.e.appendCodePoint(appendCodePoint);
            }
        }

        /* access modifiers changed from: package-private */
        public final void c(char c2) {
            c(String.valueOf(c2));
        }

        /* access modifiers changed from: package-private */
        public final void b(char c2) {
            u();
            this.e.append(c2);
        }
    }

    enum TokenType {
        Doctype,
        StartTag,
        EndTag,
        Comment,
        Character,
        EOF
    }

    static void a(StringBuilder sb) {
        if (sb != null) {
            sb.delete(0, sb.length());
        }
    }

    /* access modifiers changed from: package-private */
    public final Comment b() {
        return (Comment) this;
    }

    /* access modifiers changed from: package-private */
    public final Doctype c() {
        return (Doctype) this;
    }

    /* access modifiers changed from: package-private */
    public final EndTag d() {
        return (EndTag) this;
    }

    /* access modifiers changed from: package-private */
    public final StartTag e() {
        return (StartTag) this;
    }

    /* access modifiers changed from: package-private */
    public final boolean f() {
        return this.f6983a == TokenType.Character;
    }

    /* access modifiers changed from: package-private */
    public final boolean g() {
        return this.f6983a == TokenType.Comment;
    }

    /* access modifiers changed from: package-private */
    public final boolean h() {
        return this.f6983a == TokenType.Doctype;
    }

    /* access modifiers changed from: package-private */
    public final boolean i() {
        return this.f6983a == TokenType.EOF;
    }

    /* access modifiers changed from: package-private */
    public final boolean j() {
        return this.f6983a == TokenType.EndTag;
    }

    /* access modifiers changed from: package-private */
    public final boolean k() {
        return this.f6983a == TokenType.StartTag;
    }

    /* access modifiers changed from: package-private */
    public abstract Token l();

    /* access modifiers changed from: package-private */
    public String m() {
        return getClass().getSimpleName();
    }

    private Token() {
    }

    /* access modifiers changed from: package-private */
    public final Character a() {
        return (Character) this;
    }
}
