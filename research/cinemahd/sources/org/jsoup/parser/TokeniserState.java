package org.jsoup.parser;

import java.util.Arrays;
import org.jsoup.parser.Token;

enum TokeniserState {
    Data {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char j = characterReader.j();
            if (j == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(characterReader.b());
            } else if (j == '&') {
                tokeniser.a(TokeniserState.CharacterReferenceInData);
            } else if (j == '<') {
                tokeniser.a(TokeniserState.TagOpen);
            } else if (j != 65535) {
                tokeniser.a(characterReader.c());
            } else {
                tokeniser.a((Token) new Token.EOF());
            }
        }
    },
    CharacterReferenceInData {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.b(tokeniser, TokeniserState.Data);
        }
    },
    Rcdata {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char j = characterReader.j();
            if (j == 0) {
                tokeniser.c((TokeniserState) this);
                characterReader.a();
                tokeniser.a(65533);
            } else if (j == '&') {
                tokeniser.a(TokeniserState.CharacterReferenceInRcdata);
            } else if (j == '<') {
                tokeniser.a(TokeniserState.RcdataLessthanSign);
            } else if (j != 65535) {
                tokeniser.a(characterReader.a('&', '<', 0));
            } else {
                tokeniser.a((Token) new Token.EOF());
            }
        }
    },
    CharacterReferenceInRcdata {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.b(tokeniser, TokeniserState.Rcdata);
        }
    },
    Rawtext {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.e(tokeniser, characterReader, this, TokeniserState.RawtextLessthanSign);
        }
    },
    ScriptData {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.e(tokeniser, characterReader, this, TokeniserState.ScriptDataLessthanSign);
        }
    },
    PLAINTEXT {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char j = characterReader.j();
            if (j == 0) {
                tokeniser.c((TokeniserState) this);
                characterReader.a();
                tokeniser.a(65533);
            } else if (j != 65535) {
                tokeniser.a(characterReader.a(0));
            } else {
                tokeniser.a((Token) new Token.EOF());
            }
        }
    },
    TagOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char j = characterReader.j();
            if (j == '!') {
                tokeniser.a(TokeniserState.MarkupDeclarationOpen);
            } else if (j == '/') {
                tokeniser.a(TokeniserState.EndTagOpen);
            } else if (j == '?') {
                tokeniser.a(TokeniserState.BogusComment);
            } else if (characterReader.n()) {
                tokeniser.a(true);
                tokeniser.d(TokeniserState.TagName);
            } else {
                tokeniser.c((TokeniserState) this);
                tokeniser.a('<');
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    EndTagOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.k()) {
                tokeniser.b((TokeniserState) this);
                tokeniser.a("</");
                tokeniser.d(TokeniserState.Data);
            } else if (characterReader.n()) {
                tokeniser.a(false);
                tokeniser.d(TokeniserState.TagName);
            } else if (characterReader.b('>')) {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(TokeniserState.Data);
            } else {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(TokeniserState.BogusComment);
            }
        }
    },
    TagName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.i.c(characterReader.h());
            char b = characterReader.b();
            if (b != 0) {
                if (b != ' ') {
                    if (b == '/') {
                        tokeniser.d(TokeniserState.SelfClosingStartTag);
                        return;
                    } else if (b == '>') {
                        tokeniser.g();
                        tokeniser.d(TokeniserState.Data);
                        return;
                    } else if (b == 65535) {
                        tokeniser.b((TokeniserState) this);
                        tokeniser.d(TokeniserState.Data);
                        return;
                    } else if (!(b == 9 || b == 10 || b == 12 || b == 13)) {
                        return;
                    }
                }
                tokeniser.d(TokeniserState.BeforeAttributeName);
                return;
            }
            tokeniser.i.c(TokeniserState.w0);
        }
    },
    RcdataLessthanSign {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.b('/')) {
                tokeniser.d();
                tokeniser.a(TokeniserState.RCDATAEndTagOpen);
                return;
            }
            if (characterReader.n() && tokeniser.a() != null) {
                if (!characterReader.b("</" + tokeniser.a())) {
                    tokeniser.i = tokeniser.a(false).d(tokeniser.a());
                    tokeniser.g();
                    characterReader.q();
                    tokeniser.d(TokeniserState.Data);
                    return;
                }
            }
            tokeniser.a("<");
            tokeniser.d(TokeniserState.Rcdata);
        }
    },
    RCDATAEndTagOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.n()) {
                tokeniser.a(false);
                tokeniser.i.c(characterReader.j());
                tokeniser.h.append(characterReader.j());
                tokeniser.a(TokeniserState.RCDATAEndTagName);
                return;
            }
            tokeniser.a("</");
            tokeniser.d(TokeniserState.Rcdata);
        }
    },
    RCDATAEndTagName {
        private void b(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.a("</" + tokeniser.h.toString());
            characterReader.q();
            tokeniser.d(TokeniserState.Rcdata);
        }

        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.n()) {
                String f = characterReader.f();
                tokeniser.i.c(f);
                tokeniser.h.append(f);
                return;
            }
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                if (tokeniser.h()) {
                    tokeniser.d(TokeniserState.BeforeAttributeName);
                } else {
                    b(tokeniser, characterReader);
                }
            } else if (b != '/') {
                if (b != '>') {
                    b(tokeniser, characterReader);
                } else if (tokeniser.h()) {
                    tokeniser.g();
                    tokeniser.d(TokeniserState.Data);
                } else {
                    b(tokeniser, characterReader);
                }
            } else if (tokeniser.h()) {
                tokeniser.d(TokeniserState.SelfClosingStartTag);
            } else {
                b(tokeniser, characterReader);
            }
        }
    },
    RawtextLessthanSign {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.b('/')) {
                tokeniser.d();
                tokeniser.a(TokeniserState.RawtextEndTagOpen);
                return;
            }
            tokeniser.a('<');
            tokeniser.d(TokeniserState.Rawtext);
        }
    },
    RawtextEndTagOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.f(tokeniser, characterReader, TokeniserState.RawtextEndTagName, TokeniserState.Rawtext);
        }
    },
    RawtextEndTagName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.b(tokeniser, characterReader, TokeniserState.Rawtext);
        }
    },
    ScriptDataLessthanSign {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == '!') {
                tokeniser.a("<!");
                tokeniser.d(TokeniserState.ScriptDataEscapeStart);
            } else if (b != '/') {
                tokeniser.a("<");
                characterReader.q();
                tokeniser.d(TokeniserState.ScriptData);
            } else {
                tokeniser.d();
                tokeniser.d(TokeniserState.ScriptDataEndTagOpen);
            }
        }
    },
    ScriptDataEndTagOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.f(tokeniser, characterReader, TokeniserState.ScriptDataEndTagName, TokeniserState.ScriptData);
        }
    },
    ScriptDataEndTagName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.b(tokeniser, characterReader, TokeniserState.ScriptData);
        }
    },
    ScriptDataEscapeStart {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.b('-')) {
                tokeniser.a('-');
                tokeniser.a(TokeniserState.ScriptDataEscapeStartDash);
                return;
            }
            tokeniser.d(TokeniserState.ScriptData);
        }
    },
    ScriptDataEscapeStartDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.b('-')) {
                tokeniser.a('-');
                tokeniser.a(TokeniserState.ScriptDataEscapedDashDash);
                return;
            }
            tokeniser.d(TokeniserState.ScriptData);
        }
    },
    ScriptDataEscaped {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.k()) {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
                return;
            }
            char j = characterReader.j();
            if (j == 0) {
                tokeniser.c((TokeniserState) this);
                characterReader.a();
                tokeniser.a(65533);
            } else if (j == '-') {
                tokeniser.a('-');
                tokeniser.a(TokeniserState.ScriptDataEscapedDash);
            } else if (j != '<') {
                tokeniser.a(characterReader.a('-', '<', 0));
            } else {
                tokeniser.a(TokeniserState.ScriptDataEscapedLessthanSign);
            }
        }
    },
    ScriptDataEscapedDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.k()) {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
                return;
            }
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(65533);
                tokeniser.d(TokeniserState.ScriptDataEscaped);
            } else if (b == '-') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataEscapedDashDash);
            } else if (b != '<') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataEscaped);
            } else {
                tokeniser.d(TokeniserState.ScriptDataEscapedLessthanSign);
            }
        }
    },
    ScriptDataEscapedDashDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.k()) {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
                return;
            }
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(65533);
                tokeniser.d(TokeniserState.ScriptDataEscaped);
            } else if (b == '-') {
                tokeniser.a(b);
            } else if (b == '<') {
                tokeniser.d(TokeniserState.ScriptDataEscapedLessthanSign);
            } else if (b != '>') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataEscaped);
            } else {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptData);
            }
        }
    },
    ScriptDataEscapedLessthanSign {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.n()) {
                tokeniser.d();
                tokeniser.h.append(characterReader.j());
                tokeniser.a("<" + characterReader.j());
                tokeniser.a(TokeniserState.ScriptDataDoubleEscapeStart);
            } else if (characterReader.b('/')) {
                tokeniser.d();
                tokeniser.a(TokeniserState.ScriptDataEscapedEndTagOpen);
            } else {
                tokeniser.a('<');
                tokeniser.d(TokeniserState.ScriptDataEscaped);
            }
        }
    },
    ScriptDataEscapedEndTagOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.n()) {
                tokeniser.a(false);
                tokeniser.i.c(characterReader.j());
                tokeniser.h.append(characterReader.j());
                tokeniser.a(TokeniserState.ScriptDataEscapedEndTagName);
                return;
            }
            tokeniser.a("</");
            tokeniser.d(TokeniserState.ScriptDataEscaped);
        }
    },
    ScriptDataEscapedEndTagName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.b(tokeniser, characterReader, TokeniserState.ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscapeStart {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.d(tokeniser, characterReader, TokeniserState.ScriptDataDoubleEscaped, TokeniserState.ScriptDataEscaped);
        }
    },
    ScriptDataDoubleEscaped {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char j = characterReader.j();
            if (j == 0) {
                tokeniser.c((TokeniserState) this);
                characterReader.a();
                tokeniser.a(65533);
            } else if (j == '-') {
                tokeniser.a(j);
                tokeniser.a(TokeniserState.ScriptDataDoubleEscapedDash);
            } else if (j == '<') {
                tokeniser.a(j);
                tokeniser.a(TokeniserState.ScriptDataDoubleEscapedLessthanSign);
            } else if (j != 65535) {
                tokeniser.a(characterReader.a('-', '<', 0));
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    ScriptDataDoubleEscapedDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(65533);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscaped);
            } else if (b == '-') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscapedDashDash);
            } else if (b == '<') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscapedLessthanSign);
            } else if (b != 65535) {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscaped);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    ScriptDataDoubleEscapedDashDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(65533);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscaped);
            } else if (b == '-') {
                tokeniser.a(b);
            } else if (b == '<') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscapedLessthanSign);
            } else if (b == '>') {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptData);
            } else if (b != 65535) {
                tokeniser.a(b);
                tokeniser.d(TokeniserState.ScriptDataDoubleEscaped);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    ScriptDataDoubleEscapedLessthanSign {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.b('/')) {
                tokeniser.a('/');
                tokeniser.d();
                tokeniser.a(TokeniserState.ScriptDataDoubleEscapeEnd);
                return;
            }
            tokeniser.d(TokeniserState.ScriptDataDoubleEscaped);
        }
    },
    ScriptDataDoubleEscapeEnd {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            TokeniserState.d(tokeniser, characterReader, TokeniserState.ScriptDataEscaped, TokeniserState.ScriptDataDoubleEscaped);
        }
    },
    BeforeAttributeName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.i.r();
                characterReader.q();
                tokeniser.d(TokeniserState.AttributeName);
            } else if (b != ' ') {
                if (!(b == '\"' || b == '\'')) {
                    if (b == '/') {
                        tokeniser.d(TokeniserState.SelfClosingStartTag);
                        return;
                    } else if (b == 65535) {
                        tokeniser.b((TokeniserState) this);
                        tokeniser.d(TokeniserState.Data);
                        return;
                    } else if (b != 9 && b != 10 && b != 12 && b != 13) {
                        switch (b) {
                            case '<':
                            case '=':
                                break;
                            case '>':
                                tokeniser.g();
                                tokeniser.d(TokeniserState.Data);
                                return;
                            default:
                                tokeniser.i.r();
                                characterReader.q();
                                tokeniser.d(TokeniserState.AttributeName);
                                return;
                        }
                    } else {
                        return;
                    }
                }
                tokeniser.c((TokeniserState) this);
                tokeniser.i.r();
                tokeniser.i.a(b);
                tokeniser.d(TokeniserState.AttributeName);
            }
        }
    },
    AttributeName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.i.a(characterReader.b(TokeniserState.u0));
            char b = characterReader.b();
            if (b != 0) {
                if (b != ' ') {
                    if (!(b == '\"' || b == '\'')) {
                        if (b == '/') {
                            tokeniser.d(TokeniserState.SelfClosingStartTag);
                            return;
                        } else if (b == 65535) {
                            tokeniser.b((TokeniserState) this);
                            tokeniser.d(TokeniserState.Data);
                            return;
                        } else if (!(b == 9 || b == 10 || b == 12 || b == 13)) {
                            switch (b) {
                                case '<':
                                    break;
                                case '=':
                                    tokeniser.d(TokeniserState.BeforeAttributeValue);
                                    return;
                                case '>':
                                    tokeniser.g();
                                    tokeniser.d(TokeniserState.Data);
                                    return;
                                default:
                                    return;
                            }
                        }
                    }
                    tokeniser.c((TokeniserState) this);
                    tokeniser.i.a(b);
                    return;
                }
                tokeniser.d(TokeniserState.AfterAttributeName);
                return;
            }
            tokeniser.c((TokeniserState) this);
            tokeniser.i.a(65533);
        }
    },
    AfterAttributeName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.i.a(65533);
                tokeniser.d(TokeniserState.AttributeName);
            } else if (b != ' ') {
                if (!(b == '\"' || b == '\'')) {
                    if (b == '/') {
                        tokeniser.d(TokeniserState.SelfClosingStartTag);
                        return;
                    } else if (b == 65535) {
                        tokeniser.b((TokeniserState) this);
                        tokeniser.d(TokeniserState.Data);
                        return;
                    } else if (b != 9 && b != 10 && b != 12 && b != 13) {
                        switch (b) {
                            case '<':
                                break;
                            case '=':
                                tokeniser.d(TokeniserState.BeforeAttributeValue);
                                return;
                            case '>':
                                tokeniser.g();
                                tokeniser.d(TokeniserState.Data);
                                return;
                            default:
                                tokeniser.i.r();
                                characterReader.q();
                                tokeniser.d(TokeniserState.AttributeName);
                                return;
                        }
                    } else {
                        return;
                    }
                }
                tokeniser.c((TokeniserState) this);
                tokeniser.i.r();
                tokeniser.i.a(b);
                tokeniser.d(TokeniserState.AttributeName);
            }
        }
    },
    BeforeAttributeValue {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.i.b(65533);
                tokeniser.d(TokeniserState.AttributeValue_unquoted);
            } else if (b == ' ') {
            } else {
                if (b != '\"') {
                    if (b != '`') {
                        if (b == 65535) {
                            tokeniser.b((TokeniserState) this);
                            tokeniser.g();
                            tokeniser.d(TokeniserState.Data);
                            return;
                        } else if (b != 9 && b != 10 && b != 12 && b != 13) {
                            if (b == '&') {
                                characterReader.q();
                                tokeniser.d(TokeniserState.AttributeValue_unquoted);
                                return;
                            } else if (b != '\'') {
                                switch (b) {
                                    case '<':
                                    case '=':
                                        break;
                                    case '>':
                                        tokeniser.c((TokeniserState) this);
                                        tokeniser.g();
                                        tokeniser.d(TokeniserState.Data);
                                        return;
                                    default:
                                        characterReader.q();
                                        tokeniser.d(TokeniserState.AttributeValue_unquoted);
                                        return;
                                }
                            } else {
                                tokeniser.d(TokeniserState.AttributeValue_singleQuoted);
                                return;
                            }
                        } else {
                            return;
                        }
                    }
                    tokeniser.c((TokeniserState) this);
                    tokeniser.i.b(b);
                    tokeniser.d(TokeniserState.AttributeValue_unquoted);
                    return;
                }
                tokeniser.d(TokeniserState.AttributeValue_doubleQuoted);
            }
        }
    },
    AttributeValue_doubleQuoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            String a2 = characterReader.a(TokeniserState.t0);
            if (a2.length() > 0) {
                tokeniser.i.b(a2);
            } else {
                tokeniser.i.t();
            }
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.i.b(65533);
            } else if (b == '\"') {
                tokeniser.d(TokeniserState.AfterAttributeValue_quoted);
            } else if (b == '&') {
                int[] a3 = tokeniser.a('\"', true);
                if (a3 != null) {
                    tokeniser.i.a(a3);
                } else {
                    tokeniser.i.b('&');
                }
            } else if (b == 65535) {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    AttributeValue_singleQuoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            String a2 = characterReader.a(TokeniserState.s0);
            if (a2.length() > 0) {
                tokeniser.i.b(a2);
            } else {
                tokeniser.i.t();
            }
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.i.b(65533);
            } else if (b == 65535) {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            } else if (b == '&') {
                int[] a3 = tokeniser.a('\'', true);
                if (a3 != null) {
                    tokeniser.i.a(a3);
                } else {
                    tokeniser.i.b('&');
                }
            } else if (b == '\'') {
                tokeniser.d(TokeniserState.AfterAttributeValue_quoted);
            }
        }
    },
    AttributeValue_unquoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            String b = characterReader.b(TokeniserState.v0);
            if (b.length() > 0) {
                tokeniser.i.b(b);
            }
            char b2 = characterReader.b();
            if (b2 != 0) {
                if (b2 != ' ') {
                    if (!(b2 == '\"' || b2 == '`')) {
                        if (b2 == 65535) {
                            tokeniser.b((TokeniserState) this);
                            tokeniser.d(TokeniserState.Data);
                            return;
                        } else if (!(b2 == 9 || b2 == 10 || b2 == 12 || b2 == 13)) {
                            if (b2 == '&') {
                                int[] a2 = tokeniser.a('>', true);
                                if (a2 != null) {
                                    tokeniser.i.a(a2);
                                    return;
                                } else {
                                    tokeniser.i.b('&');
                                    return;
                                }
                            } else if (b2 != '\'') {
                                switch (b2) {
                                    case '<':
                                    case '=':
                                        break;
                                    case '>':
                                        tokeniser.g();
                                        tokeniser.d(TokeniserState.Data);
                                        return;
                                    default:
                                        return;
                                }
                            }
                        }
                    }
                    tokeniser.c((TokeniserState) this);
                    tokeniser.i.b(b2);
                    return;
                }
                tokeniser.d(TokeniserState.BeforeAttributeName);
                return;
            }
            tokeniser.c((TokeniserState) this);
            tokeniser.i.b(65533);
        }
    },
    AfterAttributeValue_quoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                tokeniser.d(TokeniserState.BeforeAttributeName);
            } else if (b == '/') {
                tokeniser.d(TokeniserState.SelfClosingStartTag);
            } else if (b == '>') {
                tokeniser.g();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.c((TokeniserState) this);
                characterReader.q();
                tokeniser.d(TokeniserState.BeforeAttributeName);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    SelfClosingStartTag {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == '>') {
                tokeniser.i.i = true;
                tokeniser.g();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.c((TokeniserState) this);
                characterReader.q();
                tokeniser.d(TokeniserState.BeforeAttributeName);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    BogusComment {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            characterReader.q();
            Token.Comment comment = new Token.Comment();
            comment.c = true;
            comment.b.append(characterReader.a('>'));
            tokeniser.a((Token) comment);
            tokeniser.a(TokeniserState.Data);
        }
    },
    MarkupDeclarationOpen {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.c("--")) {
                tokeniser.b();
                tokeniser.d(TokeniserState.CommentStart);
            } else if (characterReader.d("DOCTYPE")) {
                tokeniser.d(TokeniserState.Doctype);
            } else if (characterReader.c("[CDATA[")) {
                tokeniser.d(TokeniserState.CdataSection);
            } else {
                tokeniser.c((TokeniserState) this);
                tokeniser.a(TokeniserState.BogusComment);
            }
        }
    },
    CommentStart {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.n.b.append(65533);
                tokeniser.d(TokeniserState.Comment);
            } else if (b == '-') {
                tokeniser.d(TokeniserState.CommentStartDash);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.n.b.append(b);
                tokeniser.d(TokeniserState.Comment);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    CommentStartDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.n.b.append(65533);
                tokeniser.d(TokeniserState.Comment);
            } else if (b == '-') {
                tokeniser.d(TokeniserState.CommentStartDash);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.n.b.append(b);
                tokeniser.d(TokeniserState.Comment);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    Comment {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char j = characterReader.j();
            if (j == 0) {
                tokeniser.c((TokeniserState) this);
                characterReader.a();
                tokeniser.n.b.append(65533);
            } else if (j == '-') {
                tokeniser.a(TokeniserState.CommentEndDash);
            } else if (j != 65535) {
                tokeniser.n.b.append(characterReader.a('-', 0));
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    CommentEndDash {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                StringBuilder sb = tokeniser.n.b;
                sb.append('-');
                sb.append(65533);
                tokeniser.d(TokeniserState.Comment);
            } else if (b == '-') {
                tokeniser.d(TokeniserState.CommentEnd);
            } else if (b != 65535) {
                StringBuilder sb2 = tokeniser.n.b;
                sb2.append('-');
                sb2.append(b);
                tokeniser.d(TokeniserState.Comment);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    CommentEnd {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                StringBuilder sb = tokeniser.n.b;
                sb.append("--");
                sb.append(65533);
                tokeniser.d(TokeniserState.Comment);
            } else if (b == '!') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.CommentEndBang);
            } else if (b == '-') {
                tokeniser.c((TokeniserState) this);
                tokeniser.n.b.append('-');
            } else if (b == '>') {
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.c((TokeniserState) this);
                StringBuilder sb2 = tokeniser.n.b;
                sb2.append("--");
                sb2.append(b);
                tokeniser.d(TokeniserState.Comment);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    CommentEndBang {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                StringBuilder sb = tokeniser.n.b;
                sb.append("--!");
                sb.append(65533);
                tokeniser.d(TokeniserState.Comment);
            } else if (b == '-') {
                tokeniser.n.b.append("--!");
                tokeniser.d(TokeniserState.CommentEndDash);
            } else if (b == '>') {
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                StringBuilder sb2 = tokeniser.n.b;
                sb2.append("--!");
                sb2.append(b);
                tokeniser.d(TokeniserState.Comment);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.e();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    Doctype {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                tokeniser.d(TokeniserState.BeforeDoctypeName);
                return;
            }
            if (b != '>') {
                if (b != 65535) {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.d(TokeniserState.BeforeDoctypeName);
                    return;
                }
                tokeniser.b((TokeniserState) this);
            }
            tokeniser.c((TokeniserState) this);
            tokeniser.c();
            tokeniser.m.f = true;
            tokeniser.f();
            tokeniser.d(TokeniserState.Data);
        }
    },
    BeforeDoctypeName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.n()) {
                tokeniser.c();
                tokeniser.d(TokeniserState.DoctypeName);
                return;
            }
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.c();
                tokeniser.m.b.append(65533);
                tokeniser.d(TokeniserState.DoctypeName);
            } else if (b == ' ') {
            } else {
                if (b == 65535) {
                    tokeniser.b((TokeniserState) this);
                    tokeniser.c();
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                } else if (b != 9 && b != 10 && b != 12 && b != 13) {
                    tokeniser.c();
                    tokeniser.m.b.append(b);
                    tokeniser.d(TokeniserState.DoctypeName);
                }
            }
        }
    },
    DoctypeName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.n()) {
                tokeniser.m.b.append(characterReader.f());
                return;
            }
            char b = characterReader.b();
            if (b != 0) {
                if (b != ' ') {
                    if (b == '>') {
                        tokeniser.f();
                        tokeniser.d(TokeniserState.Data);
                        return;
                    } else if (b == 65535) {
                        tokeniser.b((TokeniserState) this);
                        tokeniser.m.f = true;
                        tokeniser.f();
                        tokeniser.d(TokeniserState.Data);
                        return;
                    } else if (!(b == 9 || b == 10 || b == 12 || b == 13)) {
                        tokeniser.m.b.append(b);
                        return;
                    }
                }
                tokeniser.d(TokeniserState.AfterDoctypeName);
                return;
            }
            tokeniser.c((TokeniserState) this);
            tokeniser.m.b.append(65533);
        }
    },
    AfterDoctypeName {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            if (characterReader.k()) {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (characterReader.c(9, 10, 13, 12, ' ')) {
                characterReader.a();
            } else if (characterReader.b('>')) {
                tokeniser.f();
                tokeniser.a(TokeniserState.Data);
            } else if (characterReader.d("PUBLIC")) {
                tokeniser.m.c = "PUBLIC";
                tokeniser.d(TokeniserState.AfterDoctypePublicKeyword);
            } else if (characterReader.d("SYSTEM")) {
                tokeniser.m.c = "SYSTEM";
                tokeniser.d(TokeniserState.AfterDoctypeSystemKeyword);
            } else {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.a(TokeniserState.BogusDoctype);
            }
        }
    },
    AfterDoctypePublicKeyword {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                tokeniser.d(TokeniserState.BeforeDoctypePublicIdentifier);
            } else if (b == '\"') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.DoctypePublicIdentifier_doubleQuoted);
            } else if (b == '\'') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.DoctypePublicIdentifier_singleQuoted);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.d(TokeniserState.BogusDoctype);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    BeforeDoctypePublicIdentifier {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b != 9 && b != 10 && b != 12 && b != 13 && b != ' ') {
                if (b == '\"') {
                    tokeniser.d(TokeniserState.DoctypePublicIdentifier_doubleQuoted);
                } else if (b == '\'') {
                    tokeniser.d(TokeniserState.DoctypePublicIdentifier_singleQuoted);
                } else if (b == '>') {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                } else if (b != 65535) {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.d(TokeniserState.BogusDoctype);
                } else {
                    tokeniser.b((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                }
            }
        }
    },
    DoctypePublicIdentifier_doubleQuoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.d.append(65533);
            } else if (b == '\"') {
                tokeniser.d(TokeniserState.AfterDoctypePublicIdentifier);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.m.d.append(b);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    DoctypePublicIdentifier_singleQuoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.d.append(65533);
            } else if (b == '\'') {
                tokeniser.d(TokeniserState.AfterDoctypePublicIdentifier);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.m.d.append(b);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    AfterDoctypePublicIdentifier {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                tokeniser.d(TokeniserState.BetweenDoctypePublicAndSystemIdentifiers);
            } else if (b == '\"') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.DoctypeSystemIdentifier_doubleQuoted);
            } else if (b == '\'') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.DoctypeSystemIdentifier_singleQuoted);
            } else if (b == '>') {
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.d(TokeniserState.BogusDoctype);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    BetweenDoctypePublicAndSystemIdentifiers {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b != 9 && b != 10 && b != 12 && b != 13 && b != ' ') {
                if (b == '\"') {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.d(TokeniserState.DoctypeSystemIdentifier_doubleQuoted);
                } else if (b == '\'') {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.d(TokeniserState.DoctypeSystemIdentifier_singleQuoted);
                } else if (b == '>') {
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                } else if (b != 65535) {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.d(TokeniserState.BogusDoctype);
                } else {
                    tokeniser.b((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                }
            }
        }
    },
    AfterDoctypeSystemKeyword {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                tokeniser.d(TokeniserState.BeforeDoctypeSystemIdentifier);
            } else if (b == '\"') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.DoctypeSystemIdentifier_doubleQuoted);
            } else if (b == '\'') {
                tokeniser.c((TokeniserState) this);
                tokeniser.d(TokeniserState.DoctypeSystemIdentifier_singleQuoted);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    BeforeDoctypeSystemIdentifier {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b != 9 && b != 10 && b != 12 && b != 13 && b != ' ') {
                if (b == '\"') {
                    tokeniser.d(TokeniserState.DoctypeSystemIdentifier_doubleQuoted);
                } else if (b == '\'') {
                    tokeniser.d(TokeniserState.DoctypeSystemIdentifier_singleQuoted);
                } else if (b == '>') {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                } else if (b != 65535) {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.d(TokeniserState.BogusDoctype);
                } else {
                    tokeniser.b((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                }
            }
        }
    },
    DoctypeSystemIdentifier_doubleQuoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.e.append(65533);
            } else if (b == '\"') {
                tokeniser.d(TokeniserState.AfterDoctypeSystemIdentifier);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.m.e.append(b);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    DoctypeSystemIdentifier_singleQuoted {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == 0) {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.e.append(65533);
            } else if (b == '\'') {
                tokeniser.d(TokeniserState.AfterDoctypeSystemIdentifier);
            } else if (b == '>') {
                tokeniser.c((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b != 65535) {
                tokeniser.m.e.append(b);
            } else {
                tokeniser.b((TokeniserState) this);
                tokeniser.m.f = true;
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    AfterDoctypeSystemIdentifier {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b != 9 && b != 10 && b != 12 && b != 13 && b != ' ') {
                if (b == '>') {
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                } else if (b != 65535) {
                    tokeniser.c((TokeniserState) this);
                    tokeniser.d(TokeniserState.BogusDoctype);
                } else {
                    tokeniser.b((TokeniserState) this);
                    tokeniser.m.f = true;
                    tokeniser.f();
                    tokeniser.d(TokeniserState.Data);
                }
            }
        }
    },
    BogusDoctype {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            char b = characterReader.b();
            if (b == '>') {
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            } else if (b == 65535) {
                tokeniser.f();
                tokeniser.d(TokeniserState.Data);
            }
        }
    },
    CdataSection {
        /* access modifiers changed from: package-private */
        public void a(Tokeniser tokeniser, CharacterReader characterReader) {
            tokeniser.a(characterReader.a("]]>"));
            characterReader.c("]]>");
            tokeniser.d(TokeniserState.Data);
        }
    };
    
    /* access modifiers changed from: private */
    public static final char[] s0 = null;
    /* access modifiers changed from: private */
    public static final char[] t0 = null;
    /* access modifiers changed from: private */
    public static final char[] u0 = null;
    /* access modifiers changed from: private */
    public static final char[] v0 = null;
    /* access modifiers changed from: private */
    public static final String w0 = null;

    static {
        s0 = new char[]{'\'', '&', 0};
        t0 = new char[]{'\"', '&', 0};
        u0 = new char[]{9, 10, 13, 12, ' ', '/', '=', '>', 0, '\"', '\'', '<'};
        v0 = new char[]{9, 10, 13, 12, ' ', '&', '>', 0, '\"', '\'', '<', '=', '`'};
        w0 = String.valueOf(65533);
        Arrays.sort(s0);
        Arrays.sort(t0);
        Arrays.sort(u0);
        Arrays.sort(v0);
    }

    /* access modifiers changed from: private */
    public static void f(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState, TokeniserState tokeniserState2) {
        if (characterReader.n()) {
            tokeniser.a(false);
            tokeniser.d(tokeniserState);
            return;
        }
        tokeniser.a("</");
        tokeniser.d(tokeniserState2);
    }

    /* access modifiers changed from: package-private */
    public abstract void a(Tokeniser tokeniser, CharacterReader characterReader);

    /* access modifiers changed from: private */
    public static void d(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState, TokeniserState tokeniserState2) {
        if (characterReader.n()) {
            String f = characterReader.f();
            tokeniser.h.append(f);
            tokeniser.a(f);
            return;
        }
        char b = characterReader.b();
        if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ' || b == '/' || b == '>') {
            if (tokeniser.h.toString().equals("script")) {
                tokeniser.d(tokeniserState);
            } else {
                tokeniser.d(tokeniserState2);
            }
            tokeniser.a(b);
            return;
        }
        characterReader.q();
        tokeniser.d(tokeniserState2);
    }

    /* access modifiers changed from: private */
    public static void e(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState, TokeniserState tokeniserState2) {
        char j = characterReader.j();
        if (j == 0) {
            tokeniser.c(tokeniserState);
            characterReader.a();
            tokeniser.a(65533);
        } else if (j == '<') {
            tokeniser.a(tokeniserState2);
        } else if (j != 65535) {
            tokeniser.a(characterReader.a('<', 0));
        } else {
            tokeniser.a((Token) new Token.EOF());
        }
    }

    /* access modifiers changed from: private */
    public static void b(Tokeniser tokeniser, CharacterReader characterReader, TokeniserState tokeniserState) {
        if (characterReader.n()) {
            String f = characterReader.f();
            tokeniser.i.c(f);
            tokeniser.h.append(f);
            return;
        }
        boolean z = true;
        if (tokeniser.h() && !characterReader.k()) {
            char b = characterReader.b();
            if (b == 9 || b == 10 || b == 12 || b == 13 || b == ' ') {
                tokeniser.d(BeforeAttributeName);
            } else if (b == '/') {
                tokeniser.d(SelfClosingStartTag);
            } else if (b != '>') {
                tokeniser.h.append(b);
            } else {
                tokeniser.g();
                tokeniser.d(Data);
            }
            z = false;
        }
        if (z) {
            tokeniser.a("</" + tokeniser.h.toString());
            tokeniser.d(tokeniserState);
        }
    }

    /* access modifiers changed from: private */
    public static void b(Tokeniser tokeniser, TokeniserState tokeniserState) {
        int[] a2 = tokeniser.a((Character) null, false);
        if (a2 == null) {
            tokeniser.a('&');
        } else {
            tokeniser.a(a2);
        }
        tokeniser.d(tokeniserState);
    }
}
