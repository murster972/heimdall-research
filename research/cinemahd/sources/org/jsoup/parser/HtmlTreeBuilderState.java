package org.jsoup.parser;

import com.facebook.react.uimanager.ViewProps;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.ui.contract.AdContract;
import java.util.ArrayList;
import java.util.Iterator;
import org.jsoup.helper.StringUtil;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.parser.Token;

enum HtmlTreeBuilderState {
    Initial {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.b(token)) {
                return true;
            }
            if (token.g()) {
                htmlTreeBuilder.a(token.b());
            } else if (token.h()) {
                Token.Doctype c = token.c();
                DocumentType documentType = new DocumentType(htmlTreeBuilder.h.a(c.n()), c.p(), c.q());
                documentType.f(c.o());
                htmlTreeBuilder.k().f((Node) documentType);
                if (c.r()) {
                    htmlTreeBuilder.k().a(Document.QuirksMode.quirks);
                }
                htmlTreeBuilder.b(HtmlTreeBuilderState.BeforeHtml);
            } else {
                htmlTreeBuilder.b(HtmlTreeBuilderState.BeforeHtml);
                return htmlTreeBuilder.a(token);
            }
            return true;
        }
    },
    BeforeHtml {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.k("html");
            htmlTreeBuilder.b(HtmlTreeBuilderState.BeforeHead);
            return htmlTreeBuilder.a(token);
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.h()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            }
            if (token.g()) {
                htmlTreeBuilder.a(token.b());
            } else if (HtmlTreeBuilderState.b(token)) {
                return true;
            } else {
                if (token.k() && token.e().s().equals("html")) {
                    htmlTreeBuilder.a(token.e());
                    htmlTreeBuilder.b(HtmlTreeBuilderState.BeforeHead);
                } else if (token.j() && StringUtil.a(token.d().s(), "head", "body", "html", "br")) {
                    return b(token, htmlTreeBuilder);
                } else {
                    if (!token.j()) {
                        return b(token, htmlTreeBuilder);
                    }
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            }
            return true;
        }
    },
    BeforeHead {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.b(token)) {
                return true;
            }
            if (token.g()) {
                htmlTreeBuilder.a(token.b());
            } else if (token.h()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else if (token.k() && token.e().s().equals("html")) {
                return HtmlTreeBuilderState.InBody.a(token, htmlTreeBuilder);
            } else {
                if (token.k() && token.e().s().equals("head")) {
                    htmlTreeBuilder.k(htmlTreeBuilder.a(token.e()));
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InHead);
                } else if (token.j() && StringUtil.a(token.d().s(), "head", "body", "html", "br")) {
                    htmlTreeBuilder.b("head");
                    return htmlTreeBuilder.a(token);
                } else if (token.j()) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    htmlTreeBuilder.b("head");
                    return htmlTreeBuilder.a(token);
                }
            }
            return true;
        }
    },
    InHead {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.b(token)) {
                htmlTreeBuilder.a(token.a());
                return true;
            }
            int i = AnonymousClass24.f6977a[token.f6983a.ordinal()];
            if (i == 1) {
                htmlTreeBuilder.a(token.b());
            } else if (i == 2) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else if (i == 3) {
                Token.StartTag e = token.e();
                String s = e.s();
                if (s.equals("html")) {
                    return HtmlTreeBuilderState.InBody.a(token, htmlTreeBuilder);
                }
                if (StringUtil.a(s, "base", "basefont", "bgsound", AdContract.AdvertisementBus.COMMAND, "link")) {
                    Element b = htmlTreeBuilder.b(e);
                    if (s.equals("base") && b.d("href")) {
                        htmlTreeBuilder.e(b);
                    }
                } else if (s.equals("meta")) {
                    htmlTreeBuilder.b(e);
                } else if (s.equals("title")) {
                    HtmlTreeBuilderState.d(e, htmlTreeBuilder);
                } else if (StringUtil.a(s, "noframes", "style")) {
                    HtmlTreeBuilderState.c(e, htmlTreeBuilder);
                } else if (s.equals("noscript")) {
                    htmlTreeBuilder.a(e);
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InHeadNoscript);
                } else if (s.equals("script")) {
                    htmlTreeBuilder.b.d(TokeniserState.ScriptData);
                    htmlTreeBuilder.t();
                    htmlTreeBuilder.b(HtmlTreeBuilderState.Text);
                    htmlTreeBuilder.a(e);
                } else if (!s.equals("head")) {
                    return a(token, (TreeBuilder) htmlTreeBuilder);
                } else {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            } else if (i != 4) {
                return a(token, (TreeBuilder) htmlTreeBuilder);
            } else {
                String s2 = token.d().s();
                if (s2.equals("head")) {
                    htmlTreeBuilder.w();
                    htmlTreeBuilder.b(HtmlTreeBuilderState.AfterHead);
                } else if (StringUtil.a(s2, "body", "html", "br")) {
                    return a(token, (TreeBuilder) htmlTreeBuilder);
                } else {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            }
            return true;
        }

        private boolean a(Token token, TreeBuilder treeBuilder) {
            treeBuilder.a("head");
            return treeBuilder.a(token);
        }
    },
    InHeadNoscript {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
            htmlTreeBuilder.a(new Token.Character().a(token.toString()));
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.h()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return true;
            } else if (token.k() && token.e().s().equals("html")) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            } else {
                if (token.j() && token.d().s().equals("noscript")) {
                    htmlTreeBuilder.w();
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InHead);
                    return true;
                } else if (HtmlTreeBuilderState.b(token) || token.g() || (token.k() && StringUtil.a(token.e().s(), "basefont", "bgsound", "link", "meta", "noframes", "style"))) {
                    return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InHead);
                } else {
                    if (token.j() && token.d().s().equals("br")) {
                        return b(token, htmlTreeBuilder);
                    }
                    if ((!token.k() || !StringUtil.a(token.e().s(), "head", "noscript")) && !token.j()) {
                        return b(token, htmlTreeBuilder);
                    }
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            }
        }
    },
    AfterHead {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.b("body");
            htmlTreeBuilder.a(true);
            return htmlTreeBuilder.a(token);
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            Token token2 = token;
            HtmlTreeBuilder htmlTreeBuilder2 = htmlTreeBuilder;
            if (HtmlTreeBuilderState.b(token)) {
                htmlTreeBuilder2.a(token.a());
                return true;
            } else if (token.g()) {
                htmlTreeBuilder2.a(token.b());
                return true;
            } else if (token.h()) {
                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                return true;
            } else if (token.k()) {
                Token.StartTag e = token.e();
                String s = e.s();
                if (s.equals("html")) {
                    return htmlTreeBuilder2.a(token2, HtmlTreeBuilderState.InBody);
                }
                if (s.equals("body")) {
                    htmlTreeBuilder2.a(e);
                    htmlTreeBuilder2.a(false);
                    htmlTreeBuilder2.b(HtmlTreeBuilderState.InBody);
                    return true;
                } else if (s.equals("frameset")) {
                    htmlTreeBuilder2.a(e);
                    htmlTreeBuilder2.b(HtmlTreeBuilderState.InFrameset);
                    return true;
                } else if (StringUtil.a(s, "base", "basefont", "bgsound", "link", "meta", "noframes", "script", "style", "title")) {
                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                    Element m = htmlTreeBuilder.m();
                    htmlTreeBuilder2.g(m);
                    htmlTreeBuilder2.a(token2, HtmlTreeBuilderState.InHead);
                    htmlTreeBuilder2.j(m);
                    return true;
                } else if (s.equals("head")) {
                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    b(token, htmlTreeBuilder);
                    return true;
                }
            } else if (!token.j()) {
                b(token, htmlTreeBuilder);
                return true;
            } else if (StringUtil.a(token.d().s(), "body", "html")) {
                b(token, htmlTreeBuilder);
                return true;
            } else {
                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                return false;
            }
        }
    },
    InBody {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            Element element;
            Token token2 = token;
            HtmlTreeBuilder htmlTreeBuilder2 = htmlTreeBuilder;
            int i = AnonymousClass24.f6977a[token2.f6983a.ordinal()];
            boolean z = true;
            if (i == 1) {
                htmlTreeBuilder2.a(token.b());
            } else if (i != 2) {
                int i2 = 3;
                if (i == 3) {
                    Token.StartTag e = token.e();
                    String s = e.s();
                    if (s.equals("a")) {
                        if (htmlTreeBuilder2.d("a") != null) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            htmlTreeBuilder2.a("a");
                            Element e2 = htmlTreeBuilder2.e("a");
                            if (e2 != null) {
                                htmlTreeBuilder2.i(e2);
                                htmlTreeBuilder2.j(e2);
                            }
                        }
                        htmlTreeBuilder.x();
                        htmlTreeBuilder2.h(htmlTreeBuilder2.a(e));
                    } else if (StringUtil.b(s, Constants.i)) {
                        htmlTreeBuilder.x();
                        htmlTreeBuilder2.b(e);
                        htmlTreeBuilder2.a(false);
                    } else if (StringUtil.b(s, Constants.b)) {
                        if (htmlTreeBuilder2.f("p")) {
                            htmlTreeBuilder2.a("p");
                        }
                        htmlTreeBuilder2.a(e);
                    } else if (s.equals("span")) {
                        htmlTreeBuilder.x();
                        htmlTreeBuilder2.a(e);
                    } else if (s.equals("li")) {
                        htmlTreeBuilder2.a(false);
                        ArrayList<Element> o = htmlTreeBuilder.o();
                        int size = o.size() - 1;
                        while (true) {
                            if (size <= 0) {
                                break;
                            }
                            Element element2 = o.get(size);
                            if (!element2.j().equals("li")) {
                                if (htmlTreeBuilder2.d(element2) && !StringUtil.b(element2.j(), Constants.e)) {
                                    break;
                                }
                                size--;
                            } else {
                                htmlTreeBuilder2.a("li");
                                break;
                            }
                        }
                        if (htmlTreeBuilder2.f("p")) {
                            htmlTreeBuilder2.a("p");
                        }
                        htmlTreeBuilder2.a(e);
                    } else if (s.equals("html")) {
                        htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                        Element element3 = htmlTreeBuilder.o().get(0);
                        Iterator<Attribute> it2 = e.o().iterator();
                        while (it2.hasNext()) {
                            Attribute next = it2.next();
                            if (!element3.d(next.getKey())) {
                                element3.a().a(next);
                            }
                        }
                    } else if (StringUtil.b(s, Constants.f6978a)) {
                        return htmlTreeBuilder2.a(token2, HtmlTreeBuilderState.InHead);
                    } else {
                        if (s.equals("body")) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            ArrayList<Element> o2 = htmlTreeBuilder.o();
                            if (o2.size() == 1 || (o2.size() > 2 && !o2.get(1).j().equals("body"))) {
                                return false;
                            }
                            htmlTreeBuilder2.a(false);
                            Element element4 = o2.get(1);
                            Iterator<Attribute> it3 = e.o().iterator();
                            while (it3.hasNext()) {
                                Attribute next2 = it3.next();
                                if (!element4.d(next2.getKey())) {
                                    element4.a().a(next2);
                                }
                            }
                        } else if (s.equals("frameset")) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            ArrayList<Element> o3 = htmlTreeBuilder.o();
                            if (o3.size() == 1 || ((o3.size() > 2 && !o3.get(1).j().equals("body")) || !htmlTreeBuilder.h())) {
                                return false;
                            }
                            Element element5 = o3.get(1);
                            if (element5.n() != null) {
                                element5.p();
                            }
                            for (int i3 = 1; o3.size() > i3; i3 = 1) {
                                o3.remove(o3.size() - i3);
                            }
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder2.b(HtmlTreeBuilderState.InFrameset);
                        } else if (StringUtil.b(s, Constants.c)) {
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            if (StringUtil.b(htmlTreeBuilder.a().j(), Constants.c)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                htmlTreeBuilder.w();
                            }
                            htmlTreeBuilder2.a(e);
                        } else if (StringUtil.b(s, Constants.d)) {
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder2.a(false);
                        } else if (s.equals("form")) {
                            if (htmlTreeBuilder.l() != null) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder2.a(e, true);
                            return true;
                        } else if (StringUtil.b(s, Constants.f)) {
                            htmlTreeBuilder2.a(false);
                            ArrayList<Element> o4 = htmlTreeBuilder.o();
                            int size2 = o4.size() - 1;
                            while (true) {
                                if (size2 <= 0) {
                                    break;
                                }
                                Element element6 = o4.get(size2);
                                if (!StringUtil.b(element6.j(), Constants.f)) {
                                    if (htmlTreeBuilder2.d(element6) && !StringUtil.b(element6.j(), Constants.e)) {
                                        break;
                                    }
                                    size2--;
                                } else {
                                    htmlTreeBuilder2.a(element6.j());
                                    break;
                                }
                            }
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder2.a(e);
                        } else if (s.equals("plaintext")) {
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder2.b.d(TokeniserState.PLAINTEXT);
                        } else if (s.equals("button")) {
                            if (htmlTreeBuilder2.f("button")) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                htmlTreeBuilder2.a("button");
                                htmlTreeBuilder2.a((Token) e);
                            } else {
                                htmlTreeBuilder.x();
                                htmlTreeBuilder2.a(e);
                                htmlTreeBuilder2.a(false);
                            }
                        } else if (StringUtil.b(s, Constants.g)) {
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.h(htmlTreeBuilder2.a(e));
                        } else if (s.equals("nobr")) {
                            htmlTreeBuilder.x();
                            if (htmlTreeBuilder2.h("nobr")) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                htmlTreeBuilder2.a("nobr");
                                htmlTreeBuilder.x();
                            }
                            htmlTreeBuilder2.h(htmlTreeBuilder2.a(e));
                        } else if (StringUtil.b(s, Constants.h)) {
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder.p();
                            htmlTreeBuilder2.a(false);
                        } else if (s.equals("table")) {
                            if (htmlTreeBuilder.k().J() != Document.QuirksMode.quirks && htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder2.a(false);
                            htmlTreeBuilder2.b(HtmlTreeBuilderState.InTable);
                        } else if (s.equals("input")) {
                            htmlTreeBuilder.x();
                            if (!htmlTreeBuilder2.b(e).b("type").equalsIgnoreCase(ViewProps.HIDDEN)) {
                                htmlTreeBuilder2.a(false);
                            }
                        } else if (StringUtil.b(s, Constants.j)) {
                            htmlTreeBuilder2.b(e);
                        } else if (s.equals("hr")) {
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder2.b(e);
                            htmlTreeBuilder2.a(false);
                        } else if (s.equals("image")) {
                            if (htmlTreeBuilder2.e("svg") == null) {
                                return htmlTreeBuilder2.a((Token) e.d("img"));
                            }
                            htmlTreeBuilder2.a(e);
                        } else if (s.equals("isindex")) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            if (htmlTreeBuilder.l() != null) {
                                return false;
                            }
                            htmlTreeBuilder2.b("form");
                            if (e.j.c("action")) {
                                htmlTreeBuilder.l().a("action", e.j.a("action"));
                            }
                            htmlTreeBuilder2.b("hr");
                            htmlTreeBuilder2.b("label");
                            htmlTreeBuilder2.a((Token) new Token.Character().a(e.j.c("prompt") ? e.j.a("prompt") : "This is a searchable index. Enter search keywords: "));
                            Attributes attributes = new Attributes();
                            Iterator<Attribute> it4 = e.j.iterator();
                            while (it4.hasNext()) {
                                Attribute next3 = it4.next();
                                if (!StringUtil.b(next3.getKey(), Constants.k)) {
                                    attributes.a(next3);
                                }
                            }
                            attributes.a(MediationMetaData.KEY_NAME, "isindex");
                            htmlTreeBuilder2.a("input", attributes);
                            htmlTreeBuilder2.a("label");
                            htmlTreeBuilder2.b("hr");
                            htmlTreeBuilder2.a("form");
                        } else if (s.equals("textarea")) {
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder2.b.d(TokeniserState.Rcdata);
                            htmlTreeBuilder.t();
                            htmlTreeBuilder2.a(false);
                            htmlTreeBuilder2.b(HtmlTreeBuilderState.Text);
                        } else if (s.equals("xmp")) {
                            if (htmlTreeBuilder2.f("p")) {
                                htmlTreeBuilder2.a("p");
                            }
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(false);
                            HtmlTreeBuilderState.c(e, htmlTreeBuilder2);
                        } else if (s.equals("iframe")) {
                            htmlTreeBuilder2.a(false);
                            HtmlTreeBuilderState.c(e, htmlTreeBuilder2);
                        } else if (s.equals("noembed")) {
                            HtmlTreeBuilderState.c(e, htmlTreeBuilder2);
                        } else if (s.equals("select")) {
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(e);
                            htmlTreeBuilder2.a(false);
                            HtmlTreeBuilderState A = htmlTreeBuilder.A();
                            if (A.equals(HtmlTreeBuilderState.InTable) || A.equals(HtmlTreeBuilderState.InCaption) || A.equals(HtmlTreeBuilderState.InTableBody) || A.equals(HtmlTreeBuilderState.InRow) || A.equals(HtmlTreeBuilderState.InCell)) {
                                htmlTreeBuilder2.b(HtmlTreeBuilderState.InSelectInTable);
                            } else {
                                htmlTreeBuilder2.b(HtmlTreeBuilderState.InSelect);
                            }
                        } else if (StringUtil.b(s, Constants.l)) {
                            if (htmlTreeBuilder.a().j().equals("option")) {
                                htmlTreeBuilder2.a("option");
                            }
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(e);
                        } else if (StringUtil.b(s, Constants.m)) {
                            if (htmlTreeBuilder2.h("ruby")) {
                                htmlTreeBuilder.i();
                                if (!htmlTreeBuilder.a().j().equals("ruby")) {
                                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                    htmlTreeBuilder2.l("ruby");
                                }
                                htmlTreeBuilder2.a(e);
                            }
                        } else if (s.equals("math")) {
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(e);
                        } else if (s.equals("svg")) {
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(e);
                        } else if (StringUtil.b(s, Constants.n)) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            return false;
                        } else {
                            htmlTreeBuilder.x();
                            htmlTreeBuilder2.a(e);
                        }
                    }
                } else if (i == 4) {
                    Token.EndTag d = token.d();
                    String s2 = d.s();
                    if (StringUtil.b(s2, Constants.p)) {
                        int i4 = 0;
                        while (i4 < 8) {
                            Element d2 = htmlTreeBuilder2.d(s2);
                            if (d2 == null) {
                                return b(token, htmlTreeBuilder);
                            }
                            if (!htmlTreeBuilder2.f(d2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                htmlTreeBuilder2.i(d2);
                                return z;
                            } else if (!htmlTreeBuilder2.h(d2.j())) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            } else {
                                if (htmlTreeBuilder.a() != d2) {
                                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                }
                                ArrayList<Element> o5 = htmlTreeBuilder.o();
                                int size3 = o5.size();
                                Element element7 = null;
                                int i5 = 0;
                                boolean z2 = false;
                                while (true) {
                                    if (i5 >= size3 || i5 >= 64) {
                                        element = null;
                                    } else {
                                        element = o5.get(i5);
                                        if (element != d2) {
                                            if (z2 && htmlTreeBuilder2.d(element)) {
                                                break;
                                            }
                                        } else {
                                            element7 = o5.get(i5 - 1);
                                            z2 = true;
                                        }
                                        i5++;
                                    }
                                }
                                element = null;
                                if (element == null) {
                                    htmlTreeBuilder2.m(d2.j());
                                    htmlTreeBuilder2.i(d2);
                                    return z;
                                }
                                Element element8 = element;
                                Element element9 = element8;
                                int i6 = 0;
                                while (i6 < i2) {
                                    if (htmlTreeBuilder2.f(element8)) {
                                        element8 = htmlTreeBuilder2.a(element8);
                                    }
                                    if (!htmlTreeBuilder2.c(element8)) {
                                        htmlTreeBuilder2.j(element8);
                                    } else if (element8 == d2) {
                                        break;
                                    } else {
                                        Element element10 = new Element(Tag.a(element8.j(), ParseSettings.d), htmlTreeBuilder.j());
                                        htmlTreeBuilder2.b(element8, element10);
                                        htmlTreeBuilder2.c(element8, element10);
                                        if (element9.n() != null) {
                                            element9.p();
                                        }
                                        element10.f((Node) element9);
                                        element8 = element10;
                                        element9 = element8;
                                    }
                                    i6++;
                                    i2 = 3;
                                }
                                if (StringUtil.b(element7.j(), Constants.q)) {
                                    if (element9.n() != null) {
                                        element9.p();
                                    }
                                    htmlTreeBuilder2.a((Node) element9);
                                } else {
                                    if (element9.n() != null) {
                                        element9.p();
                                    }
                                    element7.f((Node) element9);
                                }
                                Element element11 = new Element(d2.E(), htmlTreeBuilder.j());
                                element11.a().a(d2.a());
                                for (Node f : (Node[]) element.d().toArray(new Node[element.c()])) {
                                    element11.f(f);
                                }
                                element.f((Node) element11);
                                htmlTreeBuilder2.i(d2);
                                htmlTreeBuilder2.j(d2);
                                htmlTreeBuilder2.a(element, element11);
                                i4++;
                                z = true;
                                i2 = 3;
                            }
                        }
                    } else if (StringUtil.b(s2, Constants.o)) {
                        if (!htmlTreeBuilder2.h(s2)) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            return false;
                        }
                        htmlTreeBuilder.i();
                        if (!htmlTreeBuilder.a().j().equals(s2)) {
                            htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                        }
                        htmlTreeBuilder2.m(s2);
                    } else if (s2.equals("span")) {
                        return b(token, htmlTreeBuilder);
                    } else {
                        if (s2.equals("li")) {
                            if (!htmlTreeBuilder2.g(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                            htmlTreeBuilder2.c(s2);
                            if (!htmlTreeBuilder.a().j().equals(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            }
                            htmlTreeBuilder2.m(s2);
                        } else if (s2.equals("body")) {
                            if (!htmlTreeBuilder2.h("body")) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                            htmlTreeBuilder2.b(HtmlTreeBuilderState.AfterBody);
                        } else if (s2.equals("html")) {
                            if (htmlTreeBuilder2.a("body")) {
                                return htmlTreeBuilder2.a((Token) d);
                            }
                        } else if (s2.equals("form")) {
                            FormElement l = htmlTreeBuilder.l();
                            htmlTreeBuilder2.a((FormElement) null);
                            if (l == null || !htmlTreeBuilder2.h(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                            htmlTreeBuilder.i();
                            if (!htmlTreeBuilder.a().j().equals(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            }
                            htmlTreeBuilder2.j((Element) l);
                        } else if (s2.equals("p")) {
                            if (!htmlTreeBuilder2.f(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                htmlTreeBuilder2.b(s2);
                                return htmlTreeBuilder2.a((Token) d);
                            }
                            htmlTreeBuilder2.c(s2);
                            if (!htmlTreeBuilder.a().j().equals(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            }
                            htmlTreeBuilder2.m(s2);
                        } else if (StringUtil.b(s2, Constants.f)) {
                            if (!htmlTreeBuilder2.h(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                            htmlTreeBuilder2.c(s2);
                            if (!htmlTreeBuilder.a().j().equals(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            }
                            htmlTreeBuilder2.m(s2);
                        } else if (StringUtil.b(s2, Constants.c)) {
                            if (!htmlTreeBuilder2.a(Constants.c)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                            htmlTreeBuilder2.c(s2);
                            if (!htmlTreeBuilder.a().j().equals(s2)) {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                            }
                            htmlTreeBuilder2.b(Constants.c);
                        } else if (s2.equals("sarcasm")) {
                            return b(token, htmlTreeBuilder);
                        } else {
                            if (StringUtil.b(s2, Constants.h)) {
                                if (!htmlTreeBuilder2.h(MediationMetaData.KEY_NAME)) {
                                    if (!htmlTreeBuilder2.h(s2)) {
                                        htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                        return false;
                                    }
                                    htmlTreeBuilder.i();
                                    if (!htmlTreeBuilder.a().j().equals(s2)) {
                                        htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                    }
                                    htmlTreeBuilder2.m(s2);
                                    htmlTreeBuilder.d();
                                }
                            } else if (!s2.equals("br")) {
                                return b(token, htmlTreeBuilder);
                            } else {
                                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                                htmlTreeBuilder2.b("br");
                                return false;
                            }
                        }
                    }
                } else if (i == 5) {
                    Token.Character a2 = token.a();
                    if (a2.n().equals(HtmlTreeBuilderState.x)) {
                        htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                        return false;
                    } else if (!htmlTreeBuilder.h() || !HtmlTreeBuilderState.b((Token) a2)) {
                        htmlTreeBuilder.x();
                        htmlTreeBuilder2.a(a2);
                        htmlTreeBuilder2.a(false);
                    } else {
                        htmlTreeBuilder.x();
                        htmlTreeBuilder2.a(a2);
                    }
                }
            } else {
                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                return false;
            }
            return true;
        }

        /* access modifiers changed from: package-private */
        public boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            String a2 = htmlTreeBuilder.h.a(token.d().q());
            ArrayList<Element> o = htmlTreeBuilder.o();
            int size = o.size() - 1;
            while (true) {
                if (size < 0) {
                    break;
                }
                Element element = o.get(size);
                if (element.j().equals(a2)) {
                    htmlTreeBuilder.c(a2);
                    if (!a2.equals(htmlTreeBuilder.a().j())) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    }
                    htmlTreeBuilder.m(a2);
                } else if (htmlTreeBuilder.d(element)) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    size--;
                }
            }
            return true;
        }
    },
    Text {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.f()) {
                htmlTreeBuilder.a(token.a());
                return true;
            } else if (token.i()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                htmlTreeBuilder.w();
                htmlTreeBuilder.b(htmlTreeBuilder.v());
                return htmlTreeBuilder.a(token);
            } else if (!token.j()) {
                return true;
            } else {
                htmlTreeBuilder.w();
                htmlTreeBuilder.b(htmlTreeBuilder.v());
                return true;
            }
        }
    },
    InTable {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            Token token2 = token;
            HtmlTreeBuilder htmlTreeBuilder2 = htmlTreeBuilder;
            if (token.f()) {
                htmlTreeBuilder.u();
                htmlTreeBuilder.t();
                htmlTreeBuilder2.b(HtmlTreeBuilderState.InTableText);
                return htmlTreeBuilder2.a(token2);
            } else if (token.g()) {
                htmlTreeBuilder2.a(token.b());
                return true;
            } else if (token.h()) {
                htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                return false;
            } else if (token.k()) {
                Token.StartTag e = token.e();
                String s = e.s();
                if (s.equals("caption")) {
                    htmlTreeBuilder.f();
                    htmlTreeBuilder.p();
                    htmlTreeBuilder2.a(e);
                    htmlTreeBuilder2.b(HtmlTreeBuilderState.InCaption);
                } else if (s.equals("colgroup")) {
                    htmlTreeBuilder.f();
                    htmlTreeBuilder2.a(e);
                    htmlTreeBuilder2.b(HtmlTreeBuilderState.InColumnGroup);
                } else if (s.equals("col")) {
                    htmlTreeBuilder2.b("colgroup");
                    return htmlTreeBuilder2.a(token2);
                } else if (StringUtil.a(s, "tbody", "tfoot", "thead")) {
                    htmlTreeBuilder.f();
                    htmlTreeBuilder2.a(e);
                    htmlTreeBuilder2.b(HtmlTreeBuilderState.InTableBody);
                } else if (StringUtil.a(s, "td", "th", "tr")) {
                    htmlTreeBuilder2.b("tbody");
                    return htmlTreeBuilder2.a(token2);
                } else if (s.equals("table")) {
                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                    if (htmlTreeBuilder2.a("table")) {
                        return htmlTreeBuilder2.a(token2);
                    }
                } else if (StringUtil.a(s, "style", "script")) {
                    return htmlTreeBuilder2.a(token2, HtmlTreeBuilderState.InHead);
                } else {
                    if (s.equals("input")) {
                        if (!e.j.a("type").equalsIgnoreCase(ViewProps.HIDDEN)) {
                            return b(token, htmlTreeBuilder);
                        }
                        htmlTreeBuilder2.b(e);
                    } else if (!s.equals("form")) {
                        return b(token, htmlTreeBuilder);
                    } else {
                        htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                        if (htmlTreeBuilder.l() != null) {
                            return false;
                        }
                        htmlTreeBuilder2.a(e, false);
                    }
                }
                return true;
            } else if (token.j()) {
                String s2 = token.d().s();
                if (s2.equals("table")) {
                    if (!htmlTreeBuilder2.j(s2)) {
                        htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                        return false;
                    }
                    htmlTreeBuilder2.m("table");
                    htmlTreeBuilder.z();
                    return true;
                } else if (!StringUtil.a(s2, "body", "caption", "col", "colgroup", "html", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                    return b(token, htmlTreeBuilder);
                } else {
                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                    return false;
                }
            } else if (!token.i()) {
                return b(token, htmlTreeBuilder);
            } else {
                if (htmlTreeBuilder.a().j().equals("html")) {
                    htmlTreeBuilder2.a((HtmlTreeBuilderState) this);
                }
                return true;
            }
        }

        /* access modifiers changed from: package-private */
        public boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
            if (!StringUtil.a(htmlTreeBuilder.a().j(), "table", "tbody", "tfoot", "thead", "tr")) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            }
            htmlTreeBuilder.b(true);
            boolean a2 = htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            htmlTreeBuilder.b(false);
            return a2;
        }
    },
    InTableText {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (AnonymousClass24.f6977a[token.f6983a.ordinal()] != 5) {
                if (htmlTreeBuilder.n().size() > 0) {
                    for (String next : htmlTreeBuilder.n()) {
                        if (!HtmlTreeBuilderState.b(next)) {
                            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                            if (StringUtil.a(htmlTreeBuilder.a().j(), "table", "tbody", "tfoot", "thead", "tr")) {
                                htmlTreeBuilder.b(true);
                                htmlTreeBuilder.a((Token) new Token.Character().a(next), HtmlTreeBuilderState.InBody);
                                htmlTreeBuilder.b(false);
                            } else {
                                htmlTreeBuilder.a((Token) new Token.Character().a(next), HtmlTreeBuilderState.InBody);
                            }
                        } else {
                            htmlTreeBuilder.a(new Token.Character().a(next));
                        }
                    }
                    htmlTreeBuilder.u();
                }
                htmlTreeBuilder.b(htmlTreeBuilder.v());
                return htmlTreeBuilder.a(token);
            }
            Token.Character a2 = token.a();
            if (a2.n().equals(HtmlTreeBuilderState.x)) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            }
            htmlTreeBuilder.n().add(a2.n());
            return true;
        }
    },
    InCaption {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (!token.j() || !token.d().s().equals("caption")) {
                if ((token.k() && StringUtil.a(token.e().s(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) || (token.j() && token.d().s().equals("table"))) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    if (htmlTreeBuilder.a("caption")) {
                        return htmlTreeBuilder.a(token);
                    }
                    return true;
                } else if (!token.j() || !StringUtil.a(token.d().s(), "body", "col", "colgroup", "html", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                    return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
                } else {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            } else if (!htmlTreeBuilder.j(token.d().s())) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else {
                htmlTreeBuilder.i();
                if (!htmlTreeBuilder.a().j().equals("caption")) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                }
                htmlTreeBuilder.m("caption");
                htmlTreeBuilder.d();
                htmlTreeBuilder.b(HtmlTreeBuilderState.InTable);
                return true;
            }
        }
    },
    InColumnGroup {
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x008d, code lost:
            if (r2.equals("html") == false) goto L_0x009a;
         */
        /* JADX WARNING: Removed duplicated region for block: B:41:0x009d  */
        /* JADX WARNING: Removed duplicated region for block: B:45:0x00a8  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public boolean a(org.jsoup.parser.Token r9, org.jsoup.parser.HtmlTreeBuilder r10) {
            /*
                r8 = this;
                boolean r0 = org.jsoup.parser.HtmlTreeBuilderState.b((org.jsoup.parser.Token) r9)
                r1 = 1
                if (r0 == 0) goto L_0x000f
                org.jsoup.parser.Token$Character r9 = r9.a()
                r10.a((org.jsoup.parser.Token.Character) r9)
                return r1
            L_0x000f:
                int[] r0 = org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.f6977a
                org.jsoup.parser.Token$TokenType r2 = r9.f6983a
                int r2 = r2.ordinal()
                r0 = r0[r2]
                if (r0 == r1) goto L_0x00b3
                r2 = 2
                if (r0 == r2) goto L_0x00af
                r2 = 3
                r3 = 0
                java.lang.String r4 = "html"
                if (r0 == r2) goto L_0x0071
                r2 = 4
                if (r0 == r2) goto L_0x0043
                r2 = 6
                if (r0 == r2) goto L_0x002f
                boolean r9 = r8.a((org.jsoup.parser.Token) r9, (org.jsoup.parser.TreeBuilder) r10)
                return r9
            L_0x002f:
                org.jsoup.nodes.Element r0 = r10.a()
                java.lang.String r0 = r0.j()
                boolean r0 = r0.equals(r4)
                if (r0 == 0) goto L_0x003e
                return r1
            L_0x003e:
                boolean r9 = r8.a((org.jsoup.parser.Token) r9, (org.jsoup.parser.TreeBuilder) r10)
                return r9
            L_0x0043:
                org.jsoup.parser.Token$EndTag r0 = r9.d()
                java.lang.String r0 = r0.c
                java.lang.String r2 = "colgroup"
                boolean r0 = r0.equals(r2)
                if (r0 == 0) goto L_0x006c
                org.jsoup.nodes.Element r9 = r10.a()
                java.lang.String r9 = r9.j()
                boolean r9 = r9.equals(r4)
                if (r9 == 0) goto L_0x0063
                r10.a((org.jsoup.parser.HtmlTreeBuilderState) r8)
                return r3
            L_0x0063:
                r10.w()
                org.jsoup.parser.HtmlTreeBuilderState r9 = org.jsoup.parser.HtmlTreeBuilderState.InTable
                r10.b((org.jsoup.parser.HtmlTreeBuilderState) r9)
                goto L_0x00ba
            L_0x006c:
                boolean r9 = r8.a((org.jsoup.parser.Token) r9, (org.jsoup.parser.TreeBuilder) r10)
                return r9
            L_0x0071:
                org.jsoup.parser.Token$StartTag r0 = r9.e()
                java.lang.String r2 = r0.s()
                r5 = -1
                int r6 = r2.hashCode()
                r7 = 98688(0x18180, float:1.38291E-40)
                if (r6 == r7) goto L_0x0090
                r7 = 3213227(0x3107ab, float:4.50269E-39)
                if (r6 == r7) goto L_0x0089
                goto L_0x009a
            L_0x0089:
                boolean r2 = r2.equals(r4)
                if (r2 == 0) goto L_0x009a
                goto L_0x009b
            L_0x0090:
                java.lang.String r3 = "col"
                boolean r2 = r2.equals(r3)
                if (r2 == 0) goto L_0x009a
                r3 = 1
                goto L_0x009b
            L_0x009a:
                r3 = -1
            L_0x009b:
                if (r3 == 0) goto L_0x00a8
                if (r3 == r1) goto L_0x00a4
                boolean r9 = r8.a((org.jsoup.parser.Token) r9, (org.jsoup.parser.TreeBuilder) r10)
                return r9
            L_0x00a4:
                r10.b((org.jsoup.parser.Token.StartTag) r0)
                goto L_0x00ba
            L_0x00a8:
                org.jsoup.parser.HtmlTreeBuilderState r0 = org.jsoup.parser.HtmlTreeBuilderState.InBody
                boolean r9 = r10.a((org.jsoup.parser.Token) r9, (org.jsoup.parser.HtmlTreeBuilderState) r0)
                return r9
            L_0x00af:
                r10.a((org.jsoup.parser.HtmlTreeBuilderState) r8)
                goto L_0x00ba
            L_0x00b3:
                org.jsoup.parser.Token$Comment r9 = r9.b()
                r10.a((org.jsoup.parser.Token.Comment) r9)
            L_0x00ba:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass12.a(org.jsoup.parser.Token, org.jsoup.parser.HtmlTreeBuilder):boolean");
        }

        private boolean a(Token token, TreeBuilder treeBuilder) {
            if (treeBuilder.a("colgroup")) {
                return treeBuilder.a(token);
            }
            return true;
        }
    },
    InTableBody {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InTable);
        }

        private boolean c(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (htmlTreeBuilder.j("tbody") || htmlTreeBuilder.j("thead") || htmlTreeBuilder.h("tfoot")) {
                htmlTreeBuilder.e();
                htmlTreeBuilder.a(htmlTreeBuilder.a().j());
                return htmlTreeBuilder.a(token);
            }
            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            int i = AnonymousClass24.f6977a[token.f6983a.ordinal()];
            if (i == 3) {
                Token.StartTag e = token.e();
                String s = e.s();
                if (s.equals(Advertisement.KEY_TEMPLATE)) {
                    htmlTreeBuilder.a(e);
                    return true;
                } else if (s.equals("tr")) {
                    htmlTreeBuilder.e();
                    htmlTreeBuilder.a(e);
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InRow);
                    return true;
                } else if (StringUtil.a(s, "th", "td")) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    htmlTreeBuilder.b("tr");
                    return htmlTreeBuilder.a((Token) e);
                } else if (StringUtil.a(s, "caption", "col", "colgroup", "tbody", "tfoot", "thead")) {
                    return c(token, htmlTreeBuilder);
                } else {
                    return b(token, htmlTreeBuilder);
                }
            } else if (i != 4) {
                return b(token, htmlTreeBuilder);
            } else {
                String s2 = token.d().s();
                if (StringUtil.a(s2, "tbody", "tfoot", "thead")) {
                    if (!htmlTreeBuilder.j(s2)) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        return false;
                    }
                    htmlTreeBuilder.e();
                    htmlTreeBuilder.w();
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InTable);
                    return true;
                } else if (s2.equals("table")) {
                    return c(token, htmlTreeBuilder);
                } else {
                    if (!StringUtil.a(s2, "body", "caption", "col", "colgroup", "html", "td", "th", "tr")) {
                        return b(token, htmlTreeBuilder);
                    }
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            }
        }
    },
    InRow {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InTable);
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.k()) {
                Token.StartTag e = token.e();
                String s = e.s();
                if (s.equals(Advertisement.KEY_TEMPLATE)) {
                    htmlTreeBuilder.a(e);
                    return true;
                } else if (StringUtil.a(s, "th", "td")) {
                    htmlTreeBuilder.g();
                    htmlTreeBuilder.a(e);
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InCell);
                    htmlTreeBuilder.p();
                    return true;
                } else if (StringUtil.a(s, "caption", "col", "colgroup", "tbody", "tfoot", "thead", "tr")) {
                    return a(token, (TreeBuilder) htmlTreeBuilder);
                } else {
                    return b(token, htmlTreeBuilder);
                }
            } else if (!token.j()) {
                return b(token, htmlTreeBuilder);
            } else {
                String s2 = token.d().s();
                if (s2.equals("tr")) {
                    if (!htmlTreeBuilder.j(s2)) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        return false;
                    }
                    htmlTreeBuilder.g();
                    htmlTreeBuilder.w();
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InTableBody);
                    return true;
                } else if (s2.equals("table")) {
                    return a(token, (TreeBuilder) htmlTreeBuilder);
                } else {
                    if (StringUtil.a(s2, "tbody", "tfoot", "thead")) {
                        if (!htmlTreeBuilder.j(s2)) {
                            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                            return false;
                        }
                        htmlTreeBuilder.a("tr");
                        return htmlTreeBuilder.a(token);
                    } else if (!StringUtil.a(s2, "body", "caption", "col", "colgroup", "html", "td", "th")) {
                        return b(token, htmlTreeBuilder);
                    } else {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        return false;
                    }
                }
            }
        }

        private boolean a(Token token, TreeBuilder treeBuilder) {
            if (treeBuilder.a("tr")) {
                return treeBuilder.a(token);
            }
            return false;
        }
    },
    InCell {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.j()) {
                String s = token.d().s();
                if (StringUtil.a(s, "td", "th")) {
                    if (!htmlTreeBuilder.j(s)) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        htmlTreeBuilder.b(HtmlTreeBuilderState.InRow);
                        return false;
                    }
                    htmlTreeBuilder.i();
                    if (!htmlTreeBuilder.a().j().equals(s)) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    }
                    htmlTreeBuilder.m(s);
                    htmlTreeBuilder.d();
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InRow);
                    return true;
                } else if (StringUtil.a(s, "body", "caption", "col", "colgroup", "html")) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                } else if (!StringUtil.a(s, "table", "tbody", "tfoot", "thead", "tr")) {
                    return b(token, htmlTreeBuilder);
                } else {
                    if (!htmlTreeBuilder.j(s)) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        return false;
                    }
                    a(htmlTreeBuilder);
                    return htmlTreeBuilder.a(token);
                }
            } else if (!token.k() || !StringUtil.a(token.e().s(), "caption", "col", "colgroup", "tbody", "td", "tfoot", "th", "thead", "tr")) {
                return b(token, htmlTreeBuilder);
            } else {
                if (htmlTreeBuilder.j("td") || htmlTreeBuilder.j("th")) {
                    a(htmlTreeBuilder);
                    return htmlTreeBuilder.a(token);
                }
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            }
        }

        private void a(HtmlTreeBuilder htmlTreeBuilder) {
            if (htmlTreeBuilder.j("td")) {
                htmlTreeBuilder.a("td");
            } else {
                htmlTreeBuilder.a("th");
            }
        }
    },
    InSelect {
        private boolean b(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
            return false;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            switch (AnonymousClass24.f6977a[token.f6983a.ordinal()]) {
                case 1:
                    htmlTreeBuilder.a(token.b());
                    break;
                case 2:
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                case 3:
                    Token.StartTag e = token.e();
                    String s = e.s();
                    if (s.equals("html")) {
                        return htmlTreeBuilder.a((Token) e, HtmlTreeBuilderState.InBody);
                    }
                    if (s.equals("option")) {
                        if (htmlTreeBuilder.a().j().equals("option")) {
                            htmlTreeBuilder.a("option");
                        }
                        htmlTreeBuilder.a(e);
                        break;
                    } else if (s.equals("optgroup")) {
                        if (htmlTreeBuilder.a().j().equals("option")) {
                            htmlTreeBuilder.a("option");
                        } else if (htmlTreeBuilder.a().j().equals("optgroup")) {
                            htmlTreeBuilder.a("optgroup");
                        }
                        htmlTreeBuilder.a(e);
                        break;
                    } else if (s.equals("select")) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        return htmlTreeBuilder.a("select");
                    } else if (StringUtil.a(s, "input", "keygen", "textarea")) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        if (!htmlTreeBuilder.i("select")) {
                            return false;
                        }
                        htmlTreeBuilder.a("select");
                        return htmlTreeBuilder.a((Token) e);
                    } else if (s.equals("script")) {
                        return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InHead);
                    } else {
                        return b(token, htmlTreeBuilder);
                    }
                case 4:
                    String s2 = token.d().s();
                    char c = 65535;
                    int hashCode = s2.hashCode();
                    if (hashCode != -1010136971) {
                        if (hashCode != -906021636) {
                            if (hashCode == -80773204 && s2.equals("optgroup")) {
                                c = 0;
                            }
                        } else if (s2.equals("select")) {
                            c = 2;
                        }
                    } else if (s2.equals("option")) {
                        c = 1;
                    }
                    if (c != 0) {
                        if (c == 1) {
                            if (!htmlTreeBuilder.a().j().equals("option")) {
                                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                                break;
                            } else {
                                htmlTreeBuilder.w();
                                break;
                            }
                        } else if (c == 2) {
                            if (htmlTreeBuilder.i(s2)) {
                                htmlTreeBuilder.m(s2);
                                htmlTreeBuilder.z();
                                break;
                            } else {
                                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                                return false;
                            }
                        } else {
                            return b(token, htmlTreeBuilder);
                        }
                    } else {
                        if (htmlTreeBuilder.a().j().equals("option") && htmlTreeBuilder.a(htmlTreeBuilder.a()) != null && htmlTreeBuilder.a(htmlTreeBuilder.a()).j().equals("optgroup")) {
                            htmlTreeBuilder.a("option");
                        }
                        if (!htmlTreeBuilder.a().j().equals("optgroup")) {
                            htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                            break;
                        } else {
                            htmlTreeBuilder.w();
                            break;
                        }
                    }
                case 5:
                    Token.Character a2 = token.a();
                    if (!a2.n().equals(HtmlTreeBuilderState.x)) {
                        htmlTreeBuilder.a(a2);
                        break;
                    } else {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        return false;
                    }
                case 6:
                    if (!htmlTreeBuilder.a().j().equals("html")) {
                        htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                        break;
                    }
                    break;
                default:
                    return b(token, htmlTreeBuilder);
            }
            return true;
        }
    },
    InSelectInTable {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.k() && StringUtil.a(token.e().s(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                htmlTreeBuilder.a("select");
                return htmlTreeBuilder.a(token);
            } else if (!token.j() || !StringUtil.a(token.d().s(), "caption", "table", "tbody", "tfoot", "thead", "tr", "td", "th")) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InSelect);
            } else {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                if (!htmlTreeBuilder.j(token.d().s())) {
                    return false;
                }
                htmlTreeBuilder.a("select");
                return htmlTreeBuilder.a(token);
            }
        }
    },
    AfterBody {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.b(token)) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            }
            if (token.g()) {
                htmlTreeBuilder.a(token.b());
                return true;
            } else if (token.h()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else if (token.k() && token.e().s().equals("html")) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            } else {
                if (!token.j() || !token.d().s().equals("html")) {
                    if (token.i()) {
                        return true;
                    }
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    htmlTreeBuilder.b(HtmlTreeBuilderState.InBody);
                    return htmlTreeBuilder.a(token);
                } else if (htmlTreeBuilder.r()) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                } else {
                    htmlTreeBuilder.b(HtmlTreeBuilderState.AfterAfterBody);
                    return true;
                }
            }
        }
    },
    InFrameset {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.b(token)) {
                htmlTreeBuilder.a(token.a());
            } else if (token.g()) {
                htmlTreeBuilder.a(token.b());
            } else if (token.h()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else if (token.k()) {
                Token.StartTag e = token.e();
                String s = e.s();
                char c = 65535;
                switch (s.hashCode()) {
                    case -1644953643:
                        if (s.equals("frameset")) {
                            c = 1;
                            break;
                        }
                        break;
                    case 3213227:
                        if (s.equals("html")) {
                            c = 0;
                            break;
                        }
                        break;
                    case 97692013:
                        if (s.equals("frame")) {
                            c = 2;
                            break;
                        }
                        break;
                    case 1192721831:
                        if (s.equals("noframes")) {
                            c = 3;
                            break;
                        }
                        break;
                }
                if (c == 0) {
                    return htmlTreeBuilder.a((Token) e, HtmlTreeBuilderState.InBody);
                }
                if (c == 1) {
                    htmlTreeBuilder.a(e);
                } else if (c == 2) {
                    htmlTreeBuilder.b(e);
                } else if (c == 3) {
                    return htmlTreeBuilder.a((Token) e, HtmlTreeBuilderState.InHead);
                } else {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            } else if (!token.j() || !token.d().s().equals("frameset")) {
                if (!token.i()) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                } else if (!htmlTreeBuilder.a().j().equals("html")) {
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                }
            } else if (htmlTreeBuilder.a().j().equals("html")) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else {
                htmlTreeBuilder.w();
                if (!htmlTreeBuilder.r() && !htmlTreeBuilder.a().j().equals("frameset")) {
                    htmlTreeBuilder.b(HtmlTreeBuilderState.AfterFrameset);
                }
            }
            return true;
        }
    },
    AfterFrameset {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (HtmlTreeBuilderState.b(token)) {
                htmlTreeBuilder.a(token.a());
                return true;
            } else if (token.g()) {
                htmlTreeBuilder.a(token.b());
                return true;
            } else if (token.h()) {
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            } else if (token.k() && token.e().s().equals("html")) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            } else {
                if (token.j() && token.d().s().equals("html")) {
                    htmlTreeBuilder.b(HtmlTreeBuilderState.AfterAfterFrameset);
                    return true;
                } else if (token.k() && token.e().s().equals("noframes")) {
                    return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InHead);
                } else {
                    if (token.i()) {
                        return true;
                    }
                    htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                    return false;
                }
            }
        }
    },
    AfterAfterBody {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.g()) {
                htmlTreeBuilder.a(token.b());
                return true;
            } else if (token.h() || HtmlTreeBuilderState.b(token) || (token.k() && token.e().s().equals("html"))) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            } else {
                if (token.i()) {
                    return true;
                }
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                htmlTreeBuilder.b(HtmlTreeBuilderState.InBody);
                return htmlTreeBuilder.a(token);
            }
        }
    },
    AfterAfterFrameset {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            if (token.g()) {
                htmlTreeBuilder.a(token.b());
                return true;
            } else if (token.h() || HtmlTreeBuilderState.b(token) || (token.k() && token.e().s().equals("html"))) {
                return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InBody);
            } else {
                if (token.i()) {
                    return true;
                }
                if (token.k() && token.e().s().equals("noframes")) {
                    return htmlTreeBuilder.a(token, HtmlTreeBuilderState.InHead);
                }
                htmlTreeBuilder.a((HtmlTreeBuilderState) this);
                return false;
            }
        }
    },
    ForeignContent {
        /* access modifiers changed from: package-private */
        public boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder) {
            return true;
        }
    };
    
    /* access modifiers changed from: private */
    public static String x;

    /* renamed from: org.jsoup.parser.HtmlTreeBuilderState$24  reason: invalid class name */
    static /* synthetic */ class AnonymousClass24 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6977a = null;

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                org.jsoup.parser.Token$TokenType[] r0 = org.jsoup.parser.Token.TokenType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6977a = r0
                int[] r0 = f6977a     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.Comment     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6977a     // Catch:{ NoSuchFieldError -> 0x001f }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.Doctype     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6977a     // Catch:{ NoSuchFieldError -> 0x002a }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.StartTag     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6977a     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.EndTag     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6977a     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.Character     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f6977a     // Catch:{ NoSuchFieldError -> 0x004b }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.EOF     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilderState.AnonymousClass24.<clinit>():void");
        }
    }

    static final class Constants {

        /* renamed from: a  reason: collision with root package name */
        static final String[] f6978a = null;
        static final String[] b = null;
        static final String[] c = null;
        static final String[] d = null;
        static final String[] e = null;
        static final String[] f = null;
        static final String[] g = null;
        static final String[] h = null;
        static final String[] i = null;
        static final String[] j = null;
        static final String[] k = null;
        static final String[] l = null;
        static final String[] m = null;
        static final String[] n = null;
        static final String[] o = null;
        static final String[] p = null;
        static final String[] q = null;

        static {
            f6978a = new String[]{"base", "basefont", "bgsound", AdContract.AdvertisementBus.COMMAND, "link", "meta", "noframes", "script", "style", "title"};
            b = new String[]{"address", "article", "aside", "blockquote", "center", "details", "dir", "div", "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "menu", "nav", "ol", "p", "section", "summary", "ul"};
            c = new String[]{"h1", "h2", "h3", "h4", "h5", "h6"};
            d = new String[]{"listing", "pre"};
            e = new String[]{"address", "div", "p"};
            f = new String[]{"dd", "dt"};
            g = new String[]{"b", "big", "code", "em", "font", "i", "s", "small", "strike", "strong", "tt", "u"};
            h = new String[]{"applet", "marquee", "object"};
            i = new String[]{"area", "br", "embed", "img", "keygen", "wbr"};
            j = new String[]{"param", "source", "track"};
            k = new String[]{"action", MediationMetaData.KEY_NAME, "prompt"};
            l = new String[]{"optgroup", "option"};
            m = new String[]{"rp", "rt"};
            n = new String[]{"caption", "col", "colgroup", "frame", "head", "tbody", "td", "tfoot", "th", "thead", "tr"};
            o = new String[]{"address", "article", "aside", "blockquote", "button", "center", "details", "dir", "div", "dl", "fieldset", "figcaption", "figure", "footer", "header", "hgroup", "listing", "menu", "nav", "ol", "pre", "section", "summary", "ul"};
            p = new String[]{"a", "b", "big", "code", "em", "font", "i", "nobr", "s", "small", "strike", "strong", "tt", "u"};
            q = new String[]{"table", "tbody", "tfoot", "thead", "tr"};
        }

        Constants() {
        }
    }

    static {
        x = String.valueOf(0);
    }

    /* access modifiers changed from: private */
    public static void c(Token.StartTag startTag, HtmlTreeBuilder htmlTreeBuilder) {
        htmlTreeBuilder.b.d(TokeniserState.Rawtext);
        htmlTreeBuilder.t();
        htmlTreeBuilder.b(Text);
        htmlTreeBuilder.a(startTag);
    }

    /* access modifiers changed from: private */
    public static void d(Token.StartTag startTag, HtmlTreeBuilder htmlTreeBuilder) {
        htmlTreeBuilder.b.d(TokeniserState.Rcdata);
        htmlTreeBuilder.t();
        htmlTreeBuilder.b(Text);
        htmlTreeBuilder.a(startTag);
    }

    /* access modifiers changed from: package-private */
    public abstract boolean a(Token token, HtmlTreeBuilder htmlTreeBuilder);

    /* access modifiers changed from: private */
    public static boolean b(Token token) {
        if (token.f()) {
            return b(token.a().n());
        }
        return false;
    }

    /* access modifiers changed from: private */
    public static boolean b(String str) {
        for (int i = 0; i < str.length(); i++) {
            if (!StringUtil.b((int) str.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
