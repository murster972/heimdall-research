package org.jsoup.parser;

import java.util.ArrayList;

public class ParseErrorList extends ArrayList<ParseError> {
    private final int maxSize;

    ParseErrorList(int i, int i2) {
        super(i);
        this.maxSize = i2;
    }

    public static ParseErrorList b() {
        return new ParseErrorList(0, 0);
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return size() < this.maxSize;
    }

    public static ParseErrorList a(int i) {
        return new ParseErrorList(16, i);
    }
}
