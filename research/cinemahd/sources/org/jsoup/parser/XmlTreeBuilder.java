package org.jsoup.parser;

import java.io.Reader;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Token;

public class XmlTreeBuilder extends TreeBuilder {

    /* renamed from: org.jsoup.parser.XmlTreeBuilder$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6989a = new int[Token.TokenType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                org.jsoup.parser.Token$TokenType[] r0 = org.jsoup.parser.Token.TokenType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6989a = r0
                int[] r0 = f6989a     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.StartTag     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6989a     // Catch:{ NoSuchFieldError -> 0x001f }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.EndTag     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6989a     // Catch:{ NoSuchFieldError -> 0x002a }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.Comment     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6989a     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.Character     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6989a     // Catch:{ NoSuchFieldError -> 0x0040 }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.Doctype     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f6989a     // Catch:{ NoSuchFieldError -> 0x004b }
                org.jsoup.parser.Token$TokenType r1 = org.jsoup.parser.Token.TokenType.EOF     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.XmlTreeBuilder.AnonymousClass1.<clinit>():void");
        }
    }

    /* access modifiers changed from: protected */
    public void a(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        super.a(reader, str, parseErrorList, parseSettings);
        this.d.add(this.c);
        this.c.I().a(Document.OutputSettings.Syntax.xml);
    }

    /* access modifiers changed from: package-private */
    public ParseSettings b() {
        return ParseSettings.d;
    }

    /* access modifiers changed from: protected */
    public boolean a(Token token) {
        switch (AnonymousClass1.f6989a[token.f6983a.ordinal()]) {
            case 1:
                a(token.e());
                return true;
            case 2:
                a(token.d());
                return true;
            case 3:
                a(token.b());
                return true;
            case 4:
                a(token.a());
                return true;
            case 5:
                a(token.c());
                return true;
            case 6:
                return true;
            default:
                Validate.a("Unexpected token type: " + token.f6983a);
                throw null;
        }
    }

    private void a(Node node) {
        a().f(node);
    }

    /* access modifiers changed from: package-private */
    public Element a(Token.StartTag startTag) {
        Tag a2 = Tag.a(startTag.q(), this.h);
        String str = this.e;
        ParseSettings parseSettings = this.h;
        Attributes attributes = startTag.j;
        parseSettings.a(attributes);
        Element element = new Element(a2, str, attributes);
        a((Node) element);
        if (!startTag.p()) {
            this.d.add(element);
        } else if (!a2.f()) {
            a2.i();
        }
        return element;
    }

    /* JADX WARNING: type inference failed for: r2v5, types: [org.jsoup.nodes.Node, org.jsoup.nodes.XmlDeclaration] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(org.jsoup.parser.Token.Comment r6) {
        /*
            r5 = this;
            org.jsoup.nodes.Comment r0 = new org.jsoup.nodes.Comment
            java.lang.String r1 = r6.n()
            r0.<init>(r1)
            boolean r6 = r6.c
            if (r6 == 0) goto L_0x0075
            java.lang.String r6 = r0.u()
            int r1 = r6.length()
            r2 = 1
            if (r1 <= r2) goto L_0x0075
            java.lang.String r1 = "!"
            boolean r3 = r6.startsWith(r1)
            if (r3 != 0) goto L_0x0028
            java.lang.String r3 = "?"
            boolean r3 = r6.startsWith(r3)
            if (r3 == 0) goto L_0x0075
        L_0x0028:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "<"
            r0.append(r3)
            int r3 = r6.length()
            int r3 = r3 - r2
            java.lang.String r2 = r6.substring(r2, r3)
            r0.append(r2)
            java.lang.String r2 = ">"
            r0.append(r2)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = r5.e
            org.jsoup.parser.Parser r3 = org.jsoup.parser.Parser.c()
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.a(r0, r2, r3)
            r2 = 0
            org.jsoup.nodes.Element r0 = r0.c((int) r2)
            org.jsoup.nodes.XmlDeclaration r2 = new org.jsoup.nodes.XmlDeclaration
            org.jsoup.parser.ParseSettings r3 = r5.h
            java.lang.String r4 = r0.F()
            java.lang.String r3 = r3.a((java.lang.String) r4)
            boolean r6 = r6.startsWith(r1)
            r2.<init>(r3, r6)
            org.jsoup.nodes.Attributes r6 = r2.a()
            org.jsoup.nodes.Attributes r0 = r0.a()
            r6.a((org.jsoup.nodes.Attributes) r0)
            r0 = r2
        L_0x0075:
            r5.a((org.jsoup.nodes.Node) r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.XmlTreeBuilder.a(org.jsoup.parser.Token$Comment):void");
    }

    /* access modifiers changed from: package-private */
    public void a(Token.Character character) {
        a((Node) new TextNode(character.n()));
    }

    /* access modifiers changed from: package-private */
    public void a(Token.Doctype doctype) {
        DocumentType documentType = new DocumentType(this.h.a(doctype.n()), doctype.p(), doctype.q());
        documentType.f(doctype.o());
        a((Node) documentType);
    }

    private void a(Token.EndTag endTag) {
        Element element;
        String q = endTag.q();
        int size = this.d.size() - 1;
        while (true) {
            if (size < 0) {
                element = null;
                break;
            }
            element = this.d.get(size);
            if (element.j().equals(q)) {
                break;
            }
            size--;
        }
        if (element != null) {
            int size2 = this.d.size() - 1;
            while (size2 >= 0) {
                Element element2 = this.d.get(size2);
                this.d.remove(size2);
                if (element2 != element) {
                    size2--;
                } else {
                    return;
                }
            }
        }
    }
}
