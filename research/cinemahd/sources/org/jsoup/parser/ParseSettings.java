package org.jsoup.parser;

import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Attributes;

public class ParseSettings {
    public static final ParseSettings c = new ParseSettings(false, false);
    public static final ParseSettings d = new ParseSettings(true, true);

    /* renamed from: a  reason: collision with root package name */
    private final boolean f6980a;
    private final boolean b;

    public ParseSettings(boolean z, boolean z2) {
        this.f6980a = z;
        this.b = z2;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        String trim = str.trim();
        return !this.f6980a ? Normalizer.a(trim) : trim;
    }

    /* access modifiers changed from: package-private */
    public Attributes a(Attributes attributes) {
        if (!this.b) {
            attributes.c();
        }
        return attributes;
    }
}
