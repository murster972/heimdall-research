package org.jsoup.parser;

import com.vungle.warren.model.Advertisement;
import com.vungle.warren.ui.contract.AdContract;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Comment;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.FormElement;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.parser.Token;

public class HtmlTreeBuilder extends TreeBuilder {
    static final String[] A = {"html", "table"};
    static final String[] B = {"optgroup", "option"};
    static final String[] C = {"dd", "dt", "li", "optgroup", "option", "p", "rp", "rt"};
    static final String[] D = {"address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", "body", "br", "button", "caption", "center", "col", "colgroup", AdContract.AdvertisementBus.COMMAND, "dd", "details", "dir", "div", "dl", "dt", "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3", "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "iframe", "img", "input", "isindex", "li", "link", "listing", "marquee", "menu", "meta", "nav", "noembed", "noframes", "noscript", "object", "ol", "p", "param", "plaintext", "pre", "script", "section", "select", "style", "summary", "table", "tbody", "td", "textarea", "tfoot", "th", "thead", "title", "tr", "ul", "wbr", "xmp"};
    static final String[] x = {"applet", "caption", "html", "marquee", "object", "table", "td", "th"};
    static final String[] y = {"ol", "ul"};
    static final String[] z = {"button"};
    private HtmlTreeBuilderState k;
    private HtmlTreeBuilderState l;
    private boolean m;
    private Element n;
    private FormElement o;
    private Element p;
    private ArrayList<Element> q;
    private List<String> r;
    private Token.EndTag s;
    private boolean t;
    private boolean u;
    private boolean v;
    private String[] w = {null};

    HtmlTreeBuilder() {
    }

    private void c(String... strArr) {
        int size = this.d.size() - 1;
        while (size >= 0) {
            Element element = this.d.get(size);
            if (!StringUtil.a(element.j(), strArr) && !element.j().equals("html")) {
                this.d.remove(size);
                size--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public HtmlTreeBuilderState A() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public ParseSettings b() {
        return ParseSettings.c;
    }

    /* access modifiers changed from: package-private */
    public boolean d(Element element) {
        return StringUtil.b(element.j(), D);
    }

    /* access modifiers changed from: package-private */
    public void e(Element element) {
        if (!this.m) {
            String a2 = element.a("href");
            if (a2.length() != 0) {
                this.e = a2;
                this.m = true;
                this.c.e(a2);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f(Element element) {
        return a(this.d, element);
    }

    /* access modifiers changed from: package-private */
    public void g(Element element) {
        this.d.add(element);
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public boolean i(String str) {
        for (int size = this.d.size() - 1; size >= 0; size--) {
            String j = this.d.get(size).j();
            if (j.equals(str)) {
                return true;
            }
            if (!StringUtil.b(j, B)) {
                return false;
            }
        }
        Validate.a("Should not be reachable");
        throw null;
    }

    /* access modifiers changed from: package-private */
    public String j() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Document k() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void l(String str) {
        int size = this.d.size() - 1;
        while (size >= 0 && !this.d.get(size).j().equals(str)) {
            this.d.remove(size);
            size--;
        }
    }

    /* access modifiers changed from: package-private */
    public void m(String str) {
        int size = this.d.size() - 1;
        while (size >= 0) {
            this.d.remove(size);
            if (!this.d.get(size).j().equals(str)) {
                size--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public List<String> n() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<Element> o() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void p() {
        this.q.add((Object) null);
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public boolean r() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public Element s() {
        if (this.q.size() <= 0) {
            return null;
        }
        ArrayList<Element> arrayList = this.q;
        return arrayList.get(arrayList.size() - 1);
    }

    /* access modifiers changed from: package-private */
    public void t() {
        this.l = this.k;
    }

    public String toString() {
        return "TreeBuilder{currentToken=" + this.f + ", state=" + this.k + ", currentElement=" + a() + '}';
    }

    /* access modifiers changed from: package-private */
    public void u() {
        this.r = new ArrayList();
    }

    /* access modifiers changed from: package-private */
    public HtmlTreeBuilderState v() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public Element w() {
        return this.d.remove(this.d.size() - 1);
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    /* JADX WARNING: Removed duplicated region for block: B:13:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    void x() {
        /*
            r7 = this;
            org.jsoup.nodes.Element r0 = r7.s()
            if (r0 == 0) goto L_0x0058
            boolean r1 = r7.f((org.jsoup.nodes.Element) r0)
            if (r1 == 0) goto L_0x000d
            goto L_0x0058
        L_0x000d:
            java.util.ArrayList<org.jsoup.nodes.Element> r1 = r7.q
            int r1 = r1.size()
            r2 = 1
            int r1 = r1 - r2
            r3 = r0
            r0 = r1
        L_0x0017:
            r4 = 0
            if (r0 != 0) goto L_0x001b
            goto L_0x002e
        L_0x001b:
            java.util.ArrayList<org.jsoup.nodes.Element> r3 = r7.q
            int r0 = r0 + -1
            java.lang.Object r3 = r3.get(r0)
            org.jsoup.nodes.Element r3 = (org.jsoup.nodes.Element) r3
            if (r3 == 0) goto L_0x002d
            boolean r5 = r7.f((org.jsoup.nodes.Element) r3)
            if (r5 == 0) goto L_0x0017
        L_0x002d:
            r2 = 0
        L_0x002e:
            if (r2 != 0) goto L_0x003b
            java.util.ArrayList<org.jsoup.nodes.Element> r2 = r7.q
            int r0 = r0 + 1
            java.lang.Object r2 = r2.get(r0)
            org.jsoup.nodes.Element r2 = (org.jsoup.nodes.Element) r2
            r3 = r2
        L_0x003b:
            org.jsoup.helper.Validate.a((java.lang.Object) r3)
            java.lang.String r2 = r3.j()
            org.jsoup.nodes.Element r2 = r7.k((java.lang.String) r2)
            org.jsoup.nodes.Attributes r5 = r2.a()
            org.jsoup.nodes.Attributes r6 = r3.a()
            r5.a((org.jsoup.nodes.Attributes) r6)
            java.util.ArrayList<org.jsoup.nodes.Element> r5 = r7.q
            r5.set(r0, r2)
            if (r0 != r1) goto L_0x002d
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilder.x():void");
    }

    /* access modifiers changed from: package-private */
    public Element y() {
        int size = this.q.size();
        if (size > 0) {
            return this.q.remove(size - 1);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void z() {
        int size = this.d.size() - 1;
        boolean z2 = false;
        while (size >= 0) {
            Element element = this.d.get(size);
            if (size == 0) {
                element = this.p;
                z2 = true;
            }
            String j = element.j();
            if ("select".equals(j)) {
                b(HtmlTreeBuilderState.InSelect);
                return;
            } else if ("td".equals(j) || ("th".equals(j) && !z2)) {
                b(HtmlTreeBuilderState.InCell);
                return;
            } else if ("tr".equals(j)) {
                b(HtmlTreeBuilderState.InRow);
                return;
            } else if ("tbody".equals(j) || "thead".equals(j) || "tfoot".equals(j)) {
                b(HtmlTreeBuilderState.InTableBody);
                return;
            } else if ("caption".equals(j)) {
                b(HtmlTreeBuilderState.InCaption);
                return;
            } else if ("colgroup".equals(j)) {
                b(HtmlTreeBuilderState.InColumnGroup);
                return;
            } else if ("table".equals(j)) {
                b(HtmlTreeBuilderState.InTable);
                return;
            } else if ("head".equals(j)) {
                b(HtmlTreeBuilderState.InBody);
                return;
            } else if ("body".equals(j)) {
                b(HtmlTreeBuilderState.InBody);
                return;
            } else if ("frameset".equals(j)) {
                b(HtmlTreeBuilderState.InFrameset);
                return;
            } else if ("html".equals(j)) {
                b(HtmlTreeBuilderState.BeforeHead);
                return;
            } else if (z2) {
                b(HtmlTreeBuilderState.InBody);
                return;
            } else {
                size--;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        super.a(reader, str, parseErrorList, parseSettings);
        this.k = HtmlTreeBuilderState.Initial;
        this.l = null;
        this.m = false;
        this.n = null;
        this.o = null;
        this.p = null;
        this.q = new ArrayList<>();
        this.r = new ArrayList();
        this.s = new Token.EndTag();
        this.t = true;
        this.u = false;
        this.v = false;
    }

    /* access modifiers changed from: package-private */
    public void b(HtmlTreeBuilderState htmlTreeBuilderState) {
        this.k = htmlTreeBuilderState;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        c("table");
    }

    /* access modifiers changed from: package-private */
    public void g() {
        c("tr", Advertisement.KEY_TEMPLATE);
    }

    /* access modifiers changed from: package-private */
    public boolean h(String str) {
        return a(str, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    public boolean j(Element element) {
        for (int size = this.d.size() - 1; size >= 0; size--) {
            if (this.d.get(size) == element) {
                this.d.remove(size);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Element k(String str) {
        Element element = new Element(Tag.a(str, this.h), this.e);
        b(element);
        return element;
    }

    private boolean d(Element element, Element element2) {
        return element.j().equals(element2.j()) && element.a().equals(element2.a());
    }

    /* access modifiers changed from: package-private */
    public void b(Element element) {
        b((Node) element);
        this.d.add(element);
    }

    /* access modifiers changed from: package-private */
    public boolean f(String str) {
        return a(str, z);
    }

    /* access modifiers changed from: package-private */
    public boolean g(String str) {
        return a(str, y);
    }

    /* access modifiers changed from: package-private */
    public void h(Element element) {
        int size = this.q.size() - 1;
        int i = 0;
        while (true) {
            if (size >= 0) {
                Element element2 = this.q.get(size);
                if (element2 == null) {
                    break;
                }
                if (d(element, element2)) {
                    i++;
                }
                if (i == 3) {
                    this.q.remove(size);
                    break;
                }
                size--;
            } else {
                break;
            }
        }
        this.q.add(element);
    }

    /* access modifiers changed from: package-private */
    public void k(Element element) {
        this.n = element;
    }

    /* access modifiers changed from: package-private */
    public Element b(Token.StartTag startTag) {
        Tag a2 = Tag.a(startTag.q(), this.h);
        Element element = new Element(a2, this.e, startTag.j);
        b((Node) element);
        if (startTag.p()) {
            if (!a2.f()) {
                a2.i();
            } else if (!a2.d()) {
                this.b.b("Tag cannot be self closing; not a void tag");
            }
        }
        return element;
    }

    /* access modifiers changed from: package-private */
    public void c(Element element, Element element2) {
        a(this.d, element, element2);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP:0: B:0:0x0000->B:3:0x000c, LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void d() {
        /*
            r1 = this;
        L_0x0000:
            java.util.ArrayList<org.jsoup.nodes.Element> r0 = r1.q
            boolean r0 = r0.isEmpty()
            if (r0 != 0) goto L_0x000e
            org.jsoup.nodes.Element r0 = r1.y()
            if (r0 != 0) goto L_0x0000
        L_0x000e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.HtmlTreeBuilder.d():void");
    }

    /* access modifiers changed from: package-private */
    public boolean j(String str) {
        return a(str, A, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    public FormElement l() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public Element m() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
        while (str != null && !a().j().equals(str) && StringUtil.b(a().j(), C)) {
            w();
        }
    }

    /* access modifiers changed from: package-private */
    public Element d(String str) {
        for (int size = this.q.size() - 1; size >= 0; size--) {
            Element element = this.q.get(size);
            if (element == null) {
                return null;
            }
            if (element.j().equals(str)) {
                return element;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Element e(String str) {
        for (int size = this.d.size() - 1; size >= 0; size--) {
            Element element = this.d.get(size);
            if (element.j().equals(str)) {
                return element;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        c((String) null);
    }

    /* access modifiers changed from: package-private */
    public void i(Element element) {
        for (int size = this.q.size() - 1; size >= 0; size--) {
            if (this.q.get(size) == element) {
                this.q.remove(size);
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean c(Element element) {
        return a(this.q, element);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        c("tbody", "tfoot", "thead", Advertisement.KEY_TEMPLATE);
    }

    private void b(Node node) {
        FormElement formElement;
        if (this.d.size() == 0) {
            this.c.f(node);
        } else if (q()) {
            a(node);
        } else {
            a().f(node);
        }
        if (node instanceof Element) {
            Element element = (Element) node;
            if (element.E().e() && (formElement = this.o) != null) {
                formElement.b(element);
            }
        }
    }

    /* access modifiers changed from: protected */
    public boolean a(Token token) {
        this.f = token;
        return this.k.a(token, this);
    }

    /* access modifiers changed from: package-private */
    public boolean a(Token token, HtmlTreeBuilderState htmlTreeBuilderState) {
        this.f = token;
        return htmlTreeBuilderState.a(token, this);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.t = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(HtmlTreeBuilderState htmlTreeBuilderState) {
        if (this.g.a()) {
            this.g.add(new ParseError(this.f6988a.o(), "Unexpected token [%s] when in state [%s]", this.f.m(), htmlTreeBuilderState));
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String... strArr) {
        int size = this.d.size() - 1;
        while (size >= 0) {
            this.d.remove(size);
            if (!StringUtil.b(this.d.get(size).j(), strArr)) {
                size--;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Element a(Token.StartTag startTag) {
        if (startTag.p()) {
            Element b = b(startTag);
            this.d.add(b);
            this.b.d(TokeniserState.Data);
            this.b.a((Token) this.s.l().d(b.F()));
            return b;
        }
        Tag a2 = Tag.a(startTag.q(), this.h);
        String str = this.e;
        ParseSettings parseSettings = this.h;
        Attributes attributes = startTag.j;
        parseSettings.a(attributes);
        Element element = new Element(a2, str, attributes);
        b(element);
        return element;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.u = z2;
    }

    /* access modifiers changed from: package-private */
    public void b(Element element, Element element2) {
        a(this.q, element, element2);
    }

    /* access modifiers changed from: package-private */
    public FormElement a(Token.StartTag startTag, boolean z2) {
        FormElement formElement = new FormElement(Tag.a(startTag.q(), this.h), this.e, startTag.j);
        a(formElement);
        b((Node) formElement);
        if (z2) {
            this.d.add(formElement);
        }
        return formElement;
    }

    /* access modifiers changed from: package-private */
    public void a(Token.Comment comment) {
        b((Node) new Comment(comment.n()));
    }

    /* access modifiers changed from: package-private */
    public void a(Token.Character character) {
        Node node;
        String F = a().F();
        if (F.equals("script") || F.equals("style")) {
            node = new DataNode(character.n());
        } else {
            node = new TextNode(character.n());
        }
        a().f(node);
    }

    private boolean a(ArrayList<Element> arrayList, Element element) {
        for (int size = arrayList.size() - 1; size >= 0; size--) {
            if (arrayList.get(size) == element) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public Element a(Element element) {
        for (int size = this.d.size() - 1; size >= 0; size--) {
            if (this.d.get(size) == element) {
                return this.d.get(size - 1);
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(Element element, Element element2) {
        int lastIndexOf = this.d.lastIndexOf(element);
        Validate.b(lastIndexOf != -1);
        this.d.add(lastIndexOf + 1, element2);
    }

    private void a(ArrayList<Element> arrayList, Element element, Element element2) {
        int lastIndexOf = arrayList.lastIndexOf(element);
        Validate.b(lastIndexOf != -1);
        arrayList.set(lastIndexOf, element2);
    }

    private boolean a(String str, String[] strArr, String[] strArr2) {
        String[] strArr3 = this.w;
        strArr3[0] = str;
        return a(strArr3, strArr, strArr2);
    }

    private boolean a(String[] strArr, String[] strArr2, String[] strArr3) {
        int size = this.d.size() - 1;
        if (size > 100) {
            size = 100;
        }
        while (size >= 0) {
            String j = this.d.get(size).j();
            if (StringUtil.b(j, strArr)) {
                return true;
            }
            if (StringUtil.b(j, strArr2)) {
                return false;
            }
            if (strArr3 != null && StringUtil.b(j, strArr3)) {
                return false;
            }
            size--;
        }
        Validate.a("Should not be reachable");
        throw null;
    }

    /* access modifiers changed from: package-private */
    public boolean a(String[] strArr) {
        return a(strArr, x, (String[]) null);
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, String[] strArr) {
        return a(str, x, strArr);
    }

    /* access modifiers changed from: package-private */
    public void a(FormElement formElement) {
        this.o = formElement;
    }

    /* access modifiers changed from: package-private */
    public void a(Node node) {
        Element element;
        Element e = e("table");
        boolean z2 = false;
        if (e == null) {
            element = this.d.get(0);
        } else if (e.n() != null) {
            element = e.n();
            z2 = true;
        } else {
            element = a(e);
        }
        if (z2) {
            Validate.a((Object) e);
            e.a(node);
            return;
        }
        element.f(node);
    }
}
