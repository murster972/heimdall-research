package org.jsoup.parser;

import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;

public class TokenQueue {

    /* renamed from: a  reason: collision with root package name */
    private String f6985a;
    private int b = 0;

    public TokenQueue(String str) {
        Validate.a((Object) str);
        this.f6985a = str;
    }

    private int i() {
        return this.f6985a.length() - this.b;
    }

    public boolean a(char... cArr) {
        if (e()) {
            return false;
        }
        for (char c : cArr) {
            if (this.f6985a.charAt(this.b) == c) {
                return true;
            }
        }
        return false;
    }

    public boolean b(String... strArr) {
        for (String e : strArr) {
            if (e(e)) {
                return true;
            }
        }
        return false;
    }

    public String c(String str) {
        int indexOf = this.f6985a.indexOf(str, this.b);
        if (indexOf == -1) {
            return h();
        }
        String substring = this.f6985a.substring(this.b, indexOf);
        this.b += substring.length();
        return substring;
    }

    public boolean d(String str) {
        if (!e(str)) {
            return false;
        }
        this.b += str.length();
        return true;
    }

    public boolean e() {
        return i() == 0;
    }

    public boolean f() {
        return !e() && StringUtil.b((int) this.f6985a.charAt(this.b));
    }

    public boolean g() {
        return !e() && Character.isLetterOrDigit(this.f6985a.charAt(this.b));
    }

    public String h() {
        String str = this.f6985a;
        String substring = str.substring(this.b, str.length());
        this.b = this.f6985a.length();
        return substring;
    }

    public String toString() {
        return this.f6985a.substring(this.b);
    }

    public static String f(String str) {
        StringBuilder a2 = StringUtil.a();
        char[] charArray = str.toCharArray();
        int length = charArray.length;
        int i = 0;
        char c = 0;
        while (i < length) {
            char c2 = charArray[i];
            if (c2 != '\\') {
                a2.append(c2);
            } else if (c != 0 && c == '\\') {
                a2.append(c2);
            }
            i++;
            c = c2;
        }
        return a2.toString();
    }

    public boolean e(String str) {
        return this.f6985a.regionMatches(true, this.b, str, 0, str.length());
    }

    public void b(String str) {
        if (e(str)) {
            int length = str.length();
            if (length <= i()) {
                this.b += length;
                return;
            }
            throw new IllegalStateException("Queue not long enough to consume sequence");
        }
        throw new IllegalStateException("Queue did not match expected sequence");
    }

    public boolean d() {
        boolean z = false;
        while (f()) {
            this.b++;
            z = true;
        }
        return z;
    }

    public char a() {
        String str = this.f6985a;
        int i = this.b;
        this.b = i + 1;
        return str.charAt(i);
    }

    public String a(String... strArr) {
        int i = this.b;
        while (!e() && !b(strArr)) {
            this.b++;
        }
        return this.f6985a.substring(i, this.b);
    }

    public String c() {
        int i = this.b;
        while (!e() && (g() || b("*|", "|", "_", "-"))) {
            this.b++;
        }
        return this.f6985a.substring(i, this.b);
    }

    public String a(String str) {
        String c = c(str);
        d(str);
        return c;
    }

    public String b() {
        int i = this.b;
        while (!e() && (g() || a('-', '_'))) {
            this.b++;
        }
        return this.f6985a.substring(i, this.b);
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0067 A[EDGE_INSN: B:36:0x0067->B:28:0x0067 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String a(char r9, char r10) {
        /*
            r8 = this;
            r0 = 0
            r1 = -1
            r2 = 0
            r3 = -1
            r4 = 0
            r5 = -1
        L_0x0006:
            boolean r6 = r8.e()
            if (r6 == 0) goto L_0x000d
            goto L_0x0067
        L_0x000d:
            char r6 = r8.a()
            java.lang.Character r6 = java.lang.Character.valueOf(r6)
            if (r0 == 0) goto L_0x001b
            r7 = 92
            if (r0 == r7) goto L_0x005b
        L_0x001b:
            r7 = 39
            java.lang.Character r7 = java.lang.Character.valueOf(r7)
            boolean r7 = r6.equals(r7)
            if (r7 != 0) goto L_0x0033
            r7 = 34
            java.lang.Character r7 = java.lang.Character.valueOf(r7)
            boolean r7 = r6.equals(r7)
            if (r7 == 0) goto L_0x003b
        L_0x0033:
            char r7 = r6.charValue()
            if (r7 == r9) goto L_0x003b
            r2 = r2 ^ 1
        L_0x003b:
            if (r2 == 0) goto L_0x003e
            goto L_0x0065
        L_0x003e:
            java.lang.Character r7 = java.lang.Character.valueOf(r9)
            boolean r7 = r6.equals(r7)
            if (r7 == 0) goto L_0x004f
            int r4 = r4 + 1
            if (r3 != r1) goto L_0x005b
            int r3 = r8.b
            goto L_0x005b
        L_0x004f:
            java.lang.Character r7 = java.lang.Character.valueOf(r10)
            boolean r7 = r6.equals(r7)
            if (r7 == 0) goto L_0x005b
            int r4 = r4 + -1
        L_0x005b:
            if (r4 <= 0) goto L_0x0061
            if (r0 == 0) goto L_0x0061
            int r5 = r8.b
        L_0x0061:
            char r0 = r6.charValue()
        L_0x0065:
            if (r4 > 0) goto L_0x0006
        L_0x0067:
            if (r5 < 0) goto L_0x0070
            java.lang.String r9 = r8.f6985a
            java.lang.String r9 = r9.substring(r3, r5)
            goto L_0x0072
        L_0x0070:
            java.lang.String r9 = ""
        L_0x0072:
            if (r4 > 0) goto L_0x0075
            return r9
        L_0x0075:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r0 = "Did not find balanced marker at '"
            r10.append(r0)
            r10.append(r9)
            java.lang.String r9 = "'"
            r10.append(r9)
            java.lang.String r9 = r10.toString()
            org.jsoup.helper.Validate.a((java.lang.String) r9)
            r9 = 0
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: org.jsoup.parser.TokenQueue.a(char, char):java.lang.String");
    }
}
