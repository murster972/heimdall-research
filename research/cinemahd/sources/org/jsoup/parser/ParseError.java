package org.jsoup.parser;

public class ParseError {

    /* renamed from: a  reason: collision with root package name */
    private int f6979a;
    private String b;

    ParseError(int i, String str) {
        this.f6979a = i;
        this.b = str;
    }

    public String toString() {
        return this.f6979a + ": " + this.b;
    }

    ParseError(int i, String str, Object... objArr) {
        this.b = String.format(str, objArr);
        this.f6979a = i;
    }
}
