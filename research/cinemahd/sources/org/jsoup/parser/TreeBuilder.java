package org.jsoup.parser;

import java.io.Reader;
import java.util.ArrayList;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Token;

abstract class TreeBuilder {

    /* renamed from: a  reason: collision with root package name */
    CharacterReader f6988a;
    Tokeniser b;
    protected Document c;
    protected ArrayList<Element> d;
    protected String e;
    protected Token f;
    protected ParseErrorList g;
    protected ParseSettings h;
    private Token.StartTag i = new Token.StartTag();
    private Token.EndTag j = new Token.EndTag();

    TreeBuilder() {
    }

    /* access modifiers changed from: protected */
    public void a(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        Validate.a((Object) reader, "String input must not be null");
        Validate.a((Object) str, "BaseURI must not be null");
        this.c = new Document(str);
        this.h = parseSettings;
        this.f6988a = new CharacterReader(reader);
        this.g = parseErrorList;
        this.f = null;
        this.b = new Tokeniser(this.f6988a, parseErrorList);
        this.d = new ArrayList<>(32);
        this.e = str;
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(Token token);

    /* access modifiers changed from: package-private */
    public Document b(Reader reader, String str, ParseErrorList parseErrorList, ParseSettings parseSettings) {
        a(reader, str, parseErrorList, parseSettings);
        c();
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public abstract ParseSettings b();

    /* access modifiers changed from: protected */
    public void c() {
        Token i2;
        do {
            i2 = this.b.i();
            a(i2);
            i2.l();
        } while (i2.f6983a != Token.TokenType.EOF);
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        Token token = this.f;
        Token.StartTag startTag = this.i;
        if (token == startTag) {
            return a((Token) new Token.StartTag().d(str));
        }
        return a((Token) startTag.l().d(str));
    }

    public boolean a(String str, Attributes attributes) {
        Token token = this.f;
        Token.StartTag startTag = this.i;
        if (token == startTag) {
            return a((Token) new Token.StartTag().a(str, attributes));
        }
        startTag.l();
        this.i.a(str, attributes);
        return a((Token) this.i);
    }

    /* access modifiers changed from: protected */
    public boolean a(String str) {
        Token token = this.f;
        Token.EndTag endTag = this.j;
        if (token == endTag) {
            return a((Token) new Token.EndTag().d(str));
        }
        return a((Token) endTag.l().d(str));
    }

    /* access modifiers changed from: protected */
    public Element a() {
        int size = this.d.size();
        if (size > 0) {
            return this.d.get(size - 1);
        }
        return null;
    }
}
