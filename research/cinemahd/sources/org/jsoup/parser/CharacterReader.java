package org.jsoup.parser;

import com.google.android.gms.ads.AdRequest;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Arrays;
import java.util.Locale;
import org.jsoup.UncheckedIOException;
import org.jsoup.helper.Validate;

public final class CharacterReader {

    /* renamed from: a  reason: collision with root package name */
    private final char[] f6975a;
    private final Reader b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private final String[] h;

    public CharacterReader(Reader reader, int i) {
        this.h = new String[AdRequest.MAX_CONTENT_URL_LENGTH];
        Validate.a((Object) reader);
        Validate.b(reader.markSupported());
        this.b = reader;
        this.f6975a = new char[(i <= 32768 ? i : 32768)];
        r();
    }

    private void r() {
        int i = this.e;
        if (i >= this.d) {
            try {
                this.f += i;
                this.b.skip((long) i);
                this.b.mark(32768);
                this.c = this.b.read(this.f6975a);
                this.b.reset();
                this.e = 0;
                this.g = 0;
                int i2 = 24576;
                if (this.c <= 24576) {
                    i2 = this.c;
                }
                this.d = i2;
            } catch (IOException e2) {
                throw new UncheckedIOException(e2);
            }
        }
    }

    public void a() {
        this.e++;
    }

    /* access modifiers changed from: package-private */
    public char b() {
        r();
        char c2 = k() ? 65535 : this.f6975a[this.e];
        this.e++;
        return c2;
    }

    /* access modifiers changed from: package-private */
    public int c(char c2) {
        r();
        for (int i = this.e; i < this.c; i++) {
            if (c2 == this.f6975a[i]) {
                return i - this.e;
            }
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        char c2;
        r();
        int i = this.e;
        while (true) {
            int i2 = this.e;
            if (i2 < this.c && (c2 = this.f6975a[i2]) >= '0' && c2 <= '9') {
                this.e = i2 + 1;
            }
        }
        return a(this.f6975a, this.h, i, this.e - i);
    }

    /* access modifiers changed from: package-private */
    public String e() {
        char c2;
        r();
        int i = this.e;
        while (true) {
            int i2 = this.e;
            if (i2 < this.c && (((c2 = this.f6975a[i2]) >= '0' && c2 <= '9') || ((c2 >= 'A' && c2 <= 'F') || (c2 >= 'a' && c2 <= 'f')))) {
                this.e++;
            }
        }
        return a(this.f6975a, this.h, i, this.e - i);
    }

    /* access modifiers changed from: package-private */
    public String f() {
        char c2;
        r();
        int i = this.e;
        while (true) {
            int i2 = this.e;
            if (i2 < this.c && (((c2 = this.f6975a[i2]) >= 'A' && c2 <= 'Z') || ((c2 >= 'a' && c2 <= 'z') || Character.isLetter(c2)))) {
                this.e++;
            }
        }
        return a(this.f6975a, this.h, i, this.e - i);
    }

    /* access modifiers changed from: package-private */
    public String g() {
        char c2;
        r();
        int i = this.e;
        while (true) {
            int i2 = this.e;
            if (i2 < this.c && (((c2 = this.f6975a[i2]) >= 'A' && c2 <= 'Z') || ((c2 >= 'a' && c2 <= 'z') || Character.isLetter(c2)))) {
                this.e++;
            }
        }
        while (!k() && (r1 = this.f6975a[r2]) >= '0' && r1 <= '9') {
            this.e = (r2 = this.e) + 1;
        }
        return a(this.f6975a, this.h, i, this.e - i);
    }

    /* access modifiers changed from: package-private */
    public String h() {
        char c2;
        r();
        int i = this.e;
        int i2 = this.c;
        char[] cArr = this.f6975a;
        while (true) {
            int i3 = this.e;
            if (i3 >= i2 || (c2 = cArr[i3]) == 9 || c2 == 10 || c2 == 13 || c2 == 12 || c2 == ' ' || c2 == '/' || c2 == '>' || c2 == 0) {
                int i4 = this.e;
            } else {
                this.e = i3 + 1;
            }
        }
        int i42 = this.e;
        return i42 > i ? a(this.f6975a, this.h, i, i42 - i) : "";
    }

    /* access modifiers changed from: package-private */
    public String i() {
        r();
        char[] cArr = this.f6975a;
        String[] strArr = this.h;
        int i = this.e;
        String a2 = a(cArr, strArr, i, this.c - i);
        this.e = this.c;
        return a2;
    }

    public char j() {
        r();
        if (k()) {
            return 65535;
        }
        return this.f6975a[this.e];
    }

    public boolean k() {
        return this.e >= this.c;
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.g = this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean m() {
        char c2;
        if (!k() && (c2 = this.f6975a[this.e]) >= '0' && c2 <= '9') {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        if (k()) {
            return false;
        }
        char c2 = this.f6975a[this.e];
        if ((c2 < 'A' || c2 > 'Z') && ((c2 < 'a' || c2 > 'z') && !Character.isLetter(c2))) {
            return false;
        }
        return true;
    }

    public int o() {
        return this.f + this.e;
    }

    /* access modifiers changed from: package-private */
    public void p() {
        this.e = this.g;
    }

    /* access modifiers changed from: package-private */
    public void q() {
        this.e--;
    }

    public String toString() {
        char[] cArr = this.f6975a;
        int i = this.e;
        return new String(cArr, i, this.c - i);
    }

    /* access modifiers changed from: package-private */
    public int a(CharSequence charSequence) {
        r();
        char charAt = charSequence.charAt(0);
        int i = this.e;
        while (i < this.c) {
            if (charAt != this.f6975a[i]) {
                do {
                    i++;
                    if (i >= this.c) {
                        break;
                    }
                } while (charAt == this.f6975a[i]);
            }
            int i2 = i + 1;
            int length = (charSequence.length() + i2) - 1;
            int i3 = this.c;
            if (i < i3 && length <= i3) {
                int i4 = i2;
                int i5 = 1;
                while (i4 < length && charSequence.charAt(i5) == this.f6975a[i4]) {
                    i4++;
                    i5++;
                }
                if (i4 == length) {
                    return i - this.e;
                }
            }
            i = i2;
        }
        return -1;
    }

    /* access modifiers changed from: package-private */
    public String b(char... cArr) {
        r();
        int i = this.e;
        int i2 = this.c;
        char[] cArr2 = this.f6975a;
        while (true) {
            int i3 = this.e;
            if (i3 >= i2 || Arrays.binarySearch(cArr, cArr2[i3]) >= 0) {
                int i4 = this.e;
            } else {
                this.e++;
            }
        }
        int i42 = this.e;
        return i42 > i ? a(this.f6975a, this.h, i, i42 - i) : "";
    }

    /* access modifiers changed from: package-private */
    public String c() {
        char c2;
        r();
        int i = this.e;
        int i2 = this.c;
        char[] cArr = this.f6975a;
        while (true) {
            int i3 = this.e;
            if (i3 >= i2 || (c2 = cArr[i3]) == '&' || c2 == '<' || c2 == 0) {
                int i4 = this.e;
            } else {
                this.e = i3 + 1;
            }
        }
        int i42 = this.e;
        return i42 > i ? a(this.f6975a, this.h, i, i42 - i) : "";
    }

    /* access modifiers changed from: package-private */
    public boolean d(char[] cArr) {
        r();
        return !k() && Arrays.binarySearch(cArr, this.f6975a[this.e]) >= 0;
    }

    /* access modifiers changed from: package-private */
    public boolean e(String str) {
        r();
        int length = str.length();
        if (length > this.c - this.e) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (str.charAt(i) != this.f6975a[this.e + i]) {
                return false;
            }
        }
        return true;
    }

    public CharacterReader(Reader reader) {
        this(reader, 32768);
    }

    /* access modifiers changed from: package-private */
    public boolean f(String str) {
        r();
        int length = str.length();
        if (length > this.c - this.e) {
            return false;
        }
        for (int i = 0; i < length; i++) {
            if (Character.toUpperCase(str.charAt(i)) != Character.toUpperCase(this.f6975a[this.e + i])) {
                return false;
            }
        }
        return true;
    }

    public CharacterReader(String str) {
        this(new StringReader(str), str.length());
    }

    /* access modifiers changed from: package-private */
    public boolean d(String str) {
        if (!f(str)) {
            return false;
        }
        this.e += str.length();
        return true;
    }

    public String a(char c2) {
        int c3 = c(c2);
        if (c3 == -1) {
            return i();
        }
        String a2 = a(this.f6975a, this.h, this.e, c3);
        this.e += c3;
        return a2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(char c2) {
        return !k() && this.f6975a[this.e] == c2;
    }

    /* access modifiers changed from: package-private */
    public boolean b(String str) {
        return a((CharSequence) str.toLowerCase(Locale.ENGLISH)) > -1 || a((CharSequence) str.toUpperCase(Locale.ENGLISH)) > -1;
    }

    /* access modifiers changed from: package-private */
    public boolean c(char... cArr) {
        if (k()) {
            return false;
        }
        r();
        char c2 = this.f6975a[this.e];
        for (char c3 : cArr) {
            if (c3 == c2) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public String a(String str) {
        int a2 = a((CharSequence) str);
        if (a2 == -1) {
            return i();
        }
        String a3 = a(this.f6975a, this.h, this.e, a2);
        this.e += a2;
        return a3;
    }

    /* access modifiers changed from: package-private */
    public boolean c(String str) {
        r();
        if (!e(str)) {
            return false;
        }
        this.e += str.length();
        return true;
    }

    public String a(char... cArr) {
        r();
        int i = this.e;
        int i2 = this.c;
        char[] cArr2 = this.f6975a;
        loop0:
        while (this.e < i2) {
            for (char c2 : cArr) {
                if (cArr2[this.e] == c2) {
                    break loop0;
                }
            }
            this.e++;
        }
        int i3 = this.e;
        return i3 > i ? a(this.f6975a, this.h, i, i3 - i) : "";
    }

    private static String a(char[] cArr, String[] strArr, int i, int i2) {
        if (i2 > 12) {
            return new String(cArr, i, i2);
        }
        int i3 = 0;
        int i4 = i;
        int i5 = 0;
        while (i3 < i2) {
            i5 = (i5 * 31) + cArr[i4];
            i3++;
            i4++;
        }
        int length = (strArr.length - 1) & i5;
        String str = strArr[length];
        if (str == null) {
            String str2 = new String(cArr, i, i2);
            strArr[length] = str2;
            return str2;
        } else if (a(cArr, i, i2, str)) {
            return str;
        } else {
            String str3 = new String(cArr, i, i2);
            strArr[length] = str3;
            return str3;
        }
    }

    static boolean a(char[] cArr, int i, int i2, String str) {
        if (i2 != str.length()) {
            return false;
        }
        int i3 = 0;
        while (true) {
            int i4 = i2 - 1;
            if (i2 == 0) {
                return true;
            }
            int i5 = i + 1;
            int i6 = i3 + 1;
            if (cArr[i] != str.charAt(i3)) {
                return false;
            }
            i = i5;
            i2 = i4;
            i3 = i6;
        }
    }
}
