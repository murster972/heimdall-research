package org.jsoup.parser;

import com.facebook.common.util.ByteConstants;
import java.util.Arrays;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.Token;

final class Tokeniser {
    private static final char[] r = {9, 10, 13, 12, ' ', '<', '&'};

    /* renamed from: a  reason: collision with root package name */
    private final CharacterReader f6986a;
    private final ParseErrorList b;
    private TokeniserState c = TokeniserState.Data;
    private Token d;
    private boolean e = false;
    private String f = null;
    private StringBuilder g = new StringBuilder(ByteConstants.KB);
    StringBuilder h = new StringBuilder(ByteConstants.KB);
    Token.Tag i;
    Token.StartTag j = new Token.StartTag();
    Token.EndTag k = new Token.EndTag();
    Token.Character l = new Token.Character();
    Token.Doctype m = new Token.Doctype();
    Token.Comment n = new Token.Comment();
    private String o;
    private final int[] p = new int[1];
    private final int[] q = new int[2];

    static {
        Arrays.sort(r);
    }

    Tokeniser(CharacterReader characterReader, ParseErrorList parseErrorList) {
        this.f6986a = characterReader;
        this.b = parseErrorList;
    }

    /* access modifiers changed from: package-private */
    public void a(Token token) {
        Validate.a(this.e, "There is an unread token pending!");
        this.d = token;
        this.e = true;
        Token.TokenType tokenType = token.f6983a;
        if (tokenType == Token.TokenType.StartTag) {
            this.o = ((Token.StartTag) token).b;
        } else if (tokenType == Token.TokenType.EndTag && ((Token.EndTag) token).j != null) {
            b("Attributes incorrectly present on end tag");
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.n.l();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.m.l();
    }

    /* access modifiers changed from: package-private */
    public void d(TokeniserState tokeniserState) {
        this.c = tokeniserState;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        a((Token) this.n);
    }

    /* access modifiers changed from: package-private */
    public void f() {
        a((Token) this.m);
    }

    /* access modifiers changed from: package-private */
    public void g() {
        this.i.n();
        a((Token) this.i);
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.o != null && this.i.q().equalsIgnoreCase(this.o);
    }

    /* access modifiers changed from: package-private */
    public Token i() {
        while (!this.e) {
            this.c.a(this, this.f6986a);
        }
        if (this.g.length() > 0) {
            String sb = this.g.toString();
            StringBuilder sb2 = this.g;
            sb2.delete(0, sb2.length());
            this.f = null;
            return this.l.a(sb);
        }
        String str = this.f;
        if (str != null) {
            Token.Character a2 = this.l.a(str);
            this.f = null;
            return a2;
        }
        this.e = false;
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void b(TokeniserState tokeniserState) {
        if (this.b.a()) {
            this.b.add(new ParseError(this.f6986a.o(), "Unexpectedly reached end of file (EOF) in input state [%s]", tokeniserState));
        }
    }

    /* access modifiers changed from: package-private */
    public void c(TokeniserState tokeniserState) {
        if (this.b.a()) {
            this.b.add(new ParseError(this.f6986a.o(), "Unexpected character '%s' in input state [%s]", Character.valueOf(this.f6986a.j()), tokeniserState));
        }
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Token.a(this.h);
    }

    private void c(String str) {
        if (this.b.a()) {
            this.b.add(new ParseError(this.f6986a.o(), "Invalid character reference: %s", str));
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        if (this.b.a()) {
            this.b.add(new ParseError(this.f6986a.o(), str));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.f == null) {
            this.f = str;
            return;
        }
        if (this.g.length() == 0) {
            this.g.append(this.f);
        }
        this.g.append(str);
    }

    /* access modifiers changed from: package-private */
    public void a(int[] iArr) {
        a(new String(iArr, 0, iArr.length));
    }

    /* access modifiers changed from: package-private */
    public void a(char c2) {
        a(String.valueOf(c2));
    }

    /* access modifiers changed from: package-private */
    public void a(TokeniserState tokeniserState) {
        this.f6986a.a();
        this.c = tokeniserState;
    }

    /* access modifiers changed from: package-private */
    public int[] a(Character ch, boolean z) {
        int i2;
        if (this.f6986a.k()) {
            return null;
        }
        if ((ch != null && ch.charValue() == this.f6986a.j()) || this.f6986a.d(r)) {
            return null;
        }
        int[] iArr = this.p;
        this.f6986a.l();
        if (this.f6986a.c("#")) {
            boolean d2 = this.f6986a.d("X");
            CharacterReader characterReader = this.f6986a;
            String e2 = d2 ? characterReader.e() : characterReader.d();
            if (e2.length() == 0) {
                c("numeric reference with no numerals");
                this.f6986a.p();
                return null;
            }
            if (!this.f6986a.c(";")) {
                c("missing semicolon");
            }
            try {
                i2 = Integer.valueOf(e2, d2 ? 16 : 10).intValue();
            } catch (NumberFormatException unused) {
                i2 = -1;
            }
            if (i2 == -1 || ((i2 >= 55296 && i2 <= 57343) || i2 > 1114111)) {
                c("character outside of valid range");
                iArr[0] = 65533;
                return iArr;
            }
            iArr[0] = i2;
            return iArr;
        }
        String g2 = this.f6986a.g();
        boolean b2 = this.f6986a.b(';');
        if (!(Entities.a(g2) || (Entities.b(g2) && b2))) {
            this.f6986a.p();
            if (b2) {
                c(String.format("invalid named referenece '%s'", new Object[]{g2}));
            }
            return null;
        } else if (!z || (!this.f6986a.n() && !this.f6986a.m() && !this.f6986a.c('=', '-', '_'))) {
            if (!this.f6986a.c(";")) {
                c("missing semicolon");
            }
            int a2 = Entities.a(g2, this.q);
            if (a2 == 1) {
                iArr[0] = this.q[0];
                return iArr;
            } else if (a2 == 2) {
                return this.q;
            } else {
                Validate.a("Unexpected characters returned for " + g2);
                throw null;
            }
        } else {
            this.f6986a.p();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Token.Tag a(boolean z) {
        this.i = z ? this.j.l() : this.k.l();
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.o;
    }
}
