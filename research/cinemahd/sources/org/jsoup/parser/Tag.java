package org.jsoup.parser;

import com.facebook.common.util.UriUtil;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.ui.contract.AdContract;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.helper.Validate;

public class Tag {
    private static final Map<String, Tag> j = new HashMap();
    private static final String[] k = {"html", "head", "body", "frameset", "script", "noscript", "style", "meta", "link", "title", "frame", "noframes", "section", "nav", "aside", "hgroup", "header", "footer", "p", "h1", "h2", "h3", "h4", "h5", "h6", "ul", "ol", "pre", "div", "blockquote", "hr", "address", "figure", "figcaption", "form", "fieldset", "ins", "del", "dl", "dt", "dd", "li", "table", "caption", "thead", "tfoot", "tbody", "colgroup", "col", "tr", "th", "td", Advertisement.KEY_VIDEO, "audio", "canvas", "details", "menu", "plaintext", Advertisement.KEY_TEMPLATE, "article", "main", "svg", "math"};
    private static final String[] l = {"object", "base", "font", "tt", "i", "b", "u", "big", "small", "em", "strong", "dfn", "code", "samp", "kbd", "var", "cite", "abbr", "time", "acronym", "mark", "ruby", "rt", "rp", "a", "img", "br", "wbr", "map", "q", "sub", "sup", "bdo", "iframe", "embed", "span", "input", "select", "textarea", "label", "button", "optgroup", "option", "legend", "datalist", "keygen", "output", "progress", "meter", "area", "param", "source", "track", "summary", AdContract.AdvertisementBus.COMMAND, "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track", UriUtil.DATA_SCHEME, "bdi", "s"};
    private static final String[] m = {"meta", "link", "base", "frame", "img", "br", "wbr", "embed", "hr", "input", "keygen", "col", AdContract.AdvertisementBus.COMMAND, "device", "area", "basefont", "bgsound", "menuitem", "param", "source", "track"};
    private static final String[] n = {"title", "a", "p", "h1", "h2", "h3", "h4", "h5", "h6", "pre", "address", "li", "th", "td", "script", "style", "ins", "del", "s"};
    private static final String[] o = {"pre", "plaintext", "title", "textarea"};
    private static final String[] p = {"button", "fieldset", "input", "keygen", "object", "output", "select", "textarea"};
    private static final String[] q = {"input", "keygen", "object", "select", "textarea"};

    /* renamed from: a  reason: collision with root package name */
    private String f6982a;
    private boolean b = true;
    private boolean c = true;
    private boolean d = true;
    private boolean e = false;
    private boolean f = false;
    private boolean g = false;
    private boolean h = false;
    private boolean i = false;

    static {
        for (String tag : k) {
            a(new Tag(tag));
        }
        for (String tag2 : l) {
            Tag tag3 = new Tag(tag2);
            tag3.b = false;
            tag3.c = false;
            a(tag3);
        }
        for (String str : m) {
            Tag tag4 = j.get(str);
            Validate.a((Object) tag4);
            tag4.d = false;
            tag4.e = true;
        }
        for (String str2 : n) {
            Tag tag5 = j.get(str2);
            Validate.a((Object) tag5);
            tag5.c = false;
        }
        for (String str3 : o) {
            Tag tag6 = j.get(str3);
            Validate.a((Object) tag6);
            tag6.g = true;
        }
        for (String str4 : p) {
            Tag tag7 = j.get(str4);
            Validate.a((Object) tag7);
            tag7.h = true;
        }
        for (String str5 : q) {
            Tag tag8 = j.get(str5);
            Validate.a((Object) tag8);
            tag8.i = true;
        }
    }

    private Tag(String str) {
        this.f6982a = str;
    }

    public static Tag a(String str, ParseSettings parseSettings) {
        Validate.a((Object) str);
        Tag tag = j.get(str);
        if (tag != null) {
            return tag;
        }
        String a2 = parseSettings.a(str);
        Validate.b(a2);
        Tag tag2 = j.get(a2);
        if (tag2 != null) {
            return tag2;
        }
        Tag tag3 = new Tag(a2);
        tag3.b = false;
        return tag3;
    }

    public String b() {
        return this.f6982a;
    }

    public boolean c() {
        return this.b;
    }

    public boolean d() {
        return this.e;
    }

    public boolean e() {
        return this.h;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tag)) {
            return false;
        }
        Tag tag = (Tag) obj;
        if (this.f6982a.equals(tag.f6982a) && this.d == tag.d && this.e == tag.e && this.c == tag.c && this.b == tag.b && this.g == tag.g && this.f == tag.f && this.h == tag.h && this.i == tag.i) {
            return true;
        }
        return false;
    }

    public boolean f() {
        return j.containsKey(this.f6982a);
    }

    public boolean g() {
        return this.e || this.f;
    }

    public boolean h() {
        return this.g;
    }

    public int hashCode() {
        return (((((((((((((((this.f6982a.hashCode() * 31) + (this.b ? 1 : 0)) * 31) + (this.c ? 1 : 0)) * 31) + (this.d ? 1 : 0)) * 31) + (this.e ? 1 : 0)) * 31) + (this.f ? 1 : 0)) * 31) + (this.g ? 1 : 0)) * 31) + (this.h ? 1 : 0)) * 31) + (this.i ? 1 : 0);
    }

    /* access modifiers changed from: package-private */
    public Tag i() {
        this.f = true;
        return this;
    }

    public String toString() {
        return this.f6982a;
    }

    public boolean a() {
        return this.c;
    }

    private static void a(Tag tag) {
        j.put(tag.f6982a, tag);
    }
}
