package org.jsoup.nodes;

import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import org.jsoup.nodes.Entities;
import org.jsoup.parser.ParseSettings;
import org.jsoup.parser.Tag;

public class Document extends Element {
    private OutputSettings i = new OutputSettings();
    private QuirksMode j = QuirksMode.noQuirks;

    public static class OutputSettings implements Cloneable {

        /* renamed from: a  reason: collision with root package name */
        private Entities.EscapeMode f6964a = Entities.EscapeMode.base;
        private Charset b;
        CharsetEncoder c;
        Entities.CoreCharset d;
        private boolean e = true;
        private boolean f = false;
        private int g = 1;
        private Syntax h = Syntax.html;

        public enum Syntax {
            html,
            xml
        }

        public OutputSettings() {
            a(Charset.forName("UTF8"));
        }

        public Charset a() {
            return this.b;
        }

        public Entities.EscapeMode b() {
            return this.f6964a;
        }

        public int c() {
            return this.g;
        }

        public boolean d() {
            return this.f;
        }

        /* access modifiers changed from: package-private */
        public CharsetEncoder e() {
            this.c = this.b.newEncoder();
            this.d = Entities.CoreCharset.a(this.c.charset().name());
            return this.c;
        }

        public boolean f() {
            return this.e;
        }

        public Syntax g() {
            return this.h;
        }

        public OutputSettings a(Charset charset) {
            this.b = charset;
            return this;
        }

        public OutputSettings clone() {
            try {
                OutputSettings outputSettings = (OutputSettings) super.clone();
                outputSettings.a(this.b.name());
                outputSettings.f6964a = Entities.EscapeMode.valueOf(this.f6964a.name());
                return outputSettings;
            } catch (CloneNotSupportedException e2) {
                throw new RuntimeException(e2);
            }
        }

        public OutputSettings a(String str) {
            a(Charset.forName(str));
            return this;
        }

        public OutputSettings a(Syntax syntax) {
            this.h = syntax;
            return this;
        }
    }

    public enum QuirksMode {
        noQuirks,
        quirks,
        limitedQuirks
    }

    public Document(String str) {
        super(Tag.a("#root", ParseSettings.c), str);
    }

    private Element a(String str, Node node) {
        if (node.j().equals(str)) {
            return (Element) node;
        }
        int c = node.c();
        for (int i2 = 0; i2 < c; i2++) {
            Element a2 = a(str, node.a(i2));
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    public Element H() {
        return a("body", this);
    }

    public OutputSettings I() {
        return this.i;
    }

    public QuirksMode J() {
        return this.j;
    }

    public String j() {
        return "#document";
    }

    public String l() {
        return super.y();
    }

    public Document clone() {
        Document document = (Document) super.clone();
        document.i = this.i.clone();
        return document;
    }

    public Document a(QuirksMode quirksMode) {
        this.j = quirksMode;
        return this;
    }
}
