package org.jsoup.nodes;

import com.unity3d.ads.metadata.MediationMetaData;
import java.io.IOException;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class DocumentType extends LeafNode {
    public DocumentType(String str, String str2, String str3) {
        Validate.a((Object) str);
        Validate.a((Object) str2);
        Validate.a((Object) str3);
        a(MediationMetaData.KEY_NAME, str);
        a("publicId", str2);
        if (g("publicId")) {
            a("pubSysKey", "PUBLIC");
        }
        a("systemId", str3);
    }

    private boolean g(String str) {
        return !StringUtil.a(b(str));
    }

    /* access modifiers changed from: package-private */
    public void c(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    public void f(String str) {
        if (str != null) {
            a("pubSysKey", str);
        }
    }

    public String j() {
        return "#doctype";
    }

    /* access modifiers changed from: package-private */
    public void b(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.g() != Document.OutputSettings.Syntax.html || g("publicId") || g("systemId")) {
            appendable.append("<!DOCTYPE");
        } else {
            appendable.append("<!doctype");
        }
        if (g(MediationMetaData.KEY_NAME)) {
            appendable.append(" ").append(b(MediationMetaData.KEY_NAME));
        }
        if (g("pubSysKey")) {
            appendable.append(" ").append(b("pubSysKey"));
        }
        if (g("publicId")) {
            appendable.append(" \"").append(b("publicId")).append('\"');
        }
        if (g("systemId")) {
            appendable.append(" \"").append(b("systemId")).append('\"');
        }
        appendable.append('>');
    }
}
