package org.jsoup.nodes;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;
import org.jsoup.helper.ChangeNotifyingArrayList;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.parser.Tag;
import org.jsoup.select.Collector;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;
import org.jsoup.select.Selector;

public class Element extends Node {
    private static final List<Node> h = Collections.emptyList();
    /* access modifiers changed from: private */
    public Tag c;
    private WeakReference<List<Element>> d;
    List<Node> e;
    private Attributes f;
    private String g;

    private static final class NodeList extends ChangeNotifyingArrayList<Node> {
        private final Element owner;

        NodeList(Element element, int i) {
            super(i);
            this.owner = element;
        }

        public void a() {
            this.owner.k();
        }
    }

    static {
        Pattern.compile("\\s+");
    }

    public Element(Tag tag, String str, Attributes attributes) {
        Validate.a((Object) tag);
        Validate.a((Object) str);
        this.e = h;
        this.g = str;
        this.f = attributes;
        this.c = tag;
    }

    private List<Element> H() {
        List<Element> list;
        WeakReference<List<Element>> weakReference = this.d;
        if (weakReference != null && (list = (List) weakReference.get()) != null) {
            return list;
        }
        int size = this.e.size();
        ArrayList arrayList = new ArrayList(size);
        for (int i = 0; i < size; i++) {
            Node node = this.e.get(i);
            if (node instanceof Element) {
                arrayList.add((Element) node);
            }
        }
        this.d = new WeakReference<>(arrayList);
        return arrayList;
    }

    public boolean A() {
        return this.c.c();
    }

    public String B() {
        StringBuilder sb = new StringBuilder();
        b(sb);
        return sb.toString().trim();
    }

    public Element C() {
        if (this.f6972a == null) {
            return null;
        }
        List<Element> H = n().H();
        Integer valueOf = Integer.valueOf(a(this, H));
        Validate.a((Object) valueOf);
        if (valueOf.intValue() > 0) {
            return H.get(valueOf.intValue() - 1);
        }
        return null;
    }

    public Elements D() {
        if (this.f6972a == null) {
            return new Elements(0);
        }
        List<Element> H = n().H();
        Elements elements = new Elements(H.size() - 1);
        for (Element next : H) {
            if (next != this) {
                elements.add(next);
            }
        }
        return elements;
    }

    public Tag E() {
        return this.c;
    }

    public String F() {
        return this.c.b();
    }

    public String G() {
        final StringBuilder sb = new StringBuilder();
        NodeTraversor.a((NodeVisitor) new NodeVisitor(this) {
            public void a(Node node, int i) {
            }

            public void b(Node node, int i) {
                if (node instanceof TextNode) {
                    Element.b(sb, (TextNode) node);
                } else if (node instanceof Element) {
                    Element element = (Element) node;
                    if (sb.length() <= 0) {
                        return;
                    }
                    if ((element.A() || element.c.b().equals("br")) && !TextNode.a(sb)) {
                        sb.append(' ');
                    }
                }
            }
        }, (Node) this);
        return sb.toString().trim();
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.g = str;
    }

    /* access modifiers changed from: protected */
    public List<Node> e() {
        if (this.e == h) {
            this.e = new NodeList(this, 4);
        }
        return this.e;
    }

    public Element f(Node node) {
        Validate.a((Object) node);
        d(node);
        e();
        this.e.add(node);
        node.b(this.e.size() - 1);
        return this;
    }

    /* access modifiers changed from: protected */
    public boolean g() {
        return this.f != null;
    }

    public Element h(String str) {
        return Selector.b(str, this);
    }

    public String j() {
        return this.c.b();
    }

    /* access modifiers changed from: package-private */
    public void k() {
        super.k();
        this.d = null;
    }

    public final Element n() {
        return (Element) this.f6972a;
    }

    public Elements t() {
        return new Elements(H());
    }

    public String toString() {
        return l();
    }

    public String u() {
        StringBuilder sb = new StringBuilder();
        for (Node next : this.e) {
            if (next instanceof DataNode) {
                sb.append(((DataNode) next).u());
            } else if (next instanceof Comment) {
                sb.append(((Comment) next).u());
            } else if (next instanceof Element) {
                sb.append(((Element) next).u());
            }
        }
        return sb.toString();
    }

    public int v() {
        if (n() == null) {
            return 0;
        }
        return a(this, n().H());
    }

    public Elements w() {
        return Collector.a(new Evaluator.AllElements(), this);
    }

    public boolean x() {
        for (Node next : this.e) {
            if (next instanceof TextNode) {
                if (!((TextNode) next).v()) {
                    return true;
                }
            } else if ((next instanceof Element) && ((Element) next).x()) {
                return true;
            }
        }
        return false;
    }

    public String y() {
        StringBuilder a2 = StringUtil.a();
        a(a2);
        boolean f2 = f().f();
        String sb = a2.toString();
        return f2 ? sb.trim() : sb;
    }

    public String z() {
        return a().b("id");
    }

    public String b() {
        return this.g;
    }

    public int c() {
        return this.e.size();
    }

    public Elements g(String str) {
        return Selector.a(str, this);
    }

    private void b(StringBuilder sb) {
        for (Node next : this.e) {
            if (next instanceof TextNode) {
                b(sb, (TextNode) next);
            } else if (next instanceof Element) {
                a((Element) next, sb);
            }
        }
    }

    static boolean g(Node node) {
        if (node == null || !(node instanceof Element)) {
            return false;
        }
        Element element = (Element) node;
        if (element.c.h() || (element.n() != null && element.n().c.h())) {
            return true;
        }
        return false;
    }

    public Attributes a() {
        if (!g()) {
            this.f = new Attributes();
        }
        return this.f;
    }

    public Element c(int i) {
        return H().get(i);
    }

    public Element clone() {
        return (Element) super.clone();
    }

    /* access modifiers changed from: package-private */
    public void c(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (!this.e.isEmpty() || !this.c.g()) {
            if (outputSettings.f() && !this.e.isEmpty() && (this.c.a() || (outputSettings.d() && (this.e.size() > 1 || (this.e.size() == 1 && !(this.e.get(0) instanceof TextNode)))))) {
                a(appendable, i, outputSettings);
            }
            appendable.append("</").append(F()).append('>');
        }
    }

    public Element a(String str, String str2) {
        super.a(str, str2);
        return this;
    }

    public boolean f(String str) {
        String b = a().b("class");
        int length = b.length();
        int length2 = str.length();
        if (length != 0 && length >= length2) {
            if (length == length2) {
                return str.equalsIgnoreCase(b);
            }
            boolean z = false;
            int i = 0;
            for (int i2 = 0; i2 < length; i2++) {
                if (Character.isWhitespace(b.charAt(i2))) {
                    if (!z) {
                        continue;
                    } else if (i2 - i == length2 && b.regionMatches(true, i, str, 0, length2)) {
                        return true;
                    } else {
                        z = false;
                    }
                } else if (!z) {
                    i = i2;
                    z = true;
                }
            }
            if (z && length - i == length2) {
                return b.regionMatches(true, i, str, 0, length2);
            }
        }
        return false;
    }

    public Element a(Node node) {
        super.a(node);
        return this;
    }

    public Element(Tag tag, String str) {
        this(tag, str, (Attributes) null);
    }

    private static <E extends Element> int a(Element element, List<E> list) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == element) {
                return i;
            }
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public static void b(StringBuilder sb, TextNode textNode) {
        String u = textNode.u();
        if (g(textNode.f6972a)) {
            sb.append(u);
        } else {
            StringUtil.a(sb, u, TextNode.a(sb));
        }
    }

    private static void a(Element element, StringBuilder sb) {
        if (element.c.b().equals("br") && !TextNode.a(sb)) {
            sb.append(" ");
        }
    }

    private void a(StringBuilder sb) {
        for (Node a2 : this.e) {
            a2.a((Appendable) sb);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.f() && (this.c.a() || ((n() != null && n().E().a()) || outputSettings.d()))) {
            if (!(appendable instanceof StringBuilder)) {
                a(appendable, i, outputSettings);
            } else if (((StringBuilder) appendable).length() > 0) {
                a(appendable, i, outputSettings);
            }
        }
        appendable.append('<').append(F());
        Attributes attributes = this.f;
        if (attributes != null) {
            attributes.a(appendable, outputSettings);
        }
        if (!this.e.isEmpty() || !this.c.g()) {
            appendable.append('>');
        } else if (outputSettings.g() != Document.OutputSettings.Syntax.html || !this.c.d()) {
            appendable.append(" />");
        } else {
            appendable.append('>');
        }
    }

    /* access modifiers changed from: protected */
    public Element b(Node node) {
        Element element = (Element) super.b(node);
        Attributes attributes = this.f;
        element.f = attributes != null ? attributes.clone() : null;
        element.g = this.g;
        element.e = new NodeList(element, this.e.size());
        element.e.addAll(this.e);
        return element;
    }
}
