package org.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.SerializationException;
import org.jsoup.helper.StringUtil;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.select.NodeTraversor;
import org.jsoup.select.NodeVisitor;

public abstract class Node implements Cloneable {

    /* renamed from: a  reason: collision with root package name */
    Node f6972a;
    int b;

    private static class OuterHtmlVisitor implements NodeVisitor {

        /* renamed from: a  reason: collision with root package name */
        private Appendable f6974a;
        private Document.OutputSettings b;

        OuterHtmlVisitor(Appendable appendable, Document.OutputSettings outputSettings) {
            this.f6974a = appendable;
            this.b = outputSettings;
            outputSettings.e();
        }

        public void a(Node node, int i) {
            if (!node.j().equals("#text")) {
                try {
                    node.c(this.f6974a, i, this.b);
                } catch (IOException e) {
                    throw new SerializationException(e);
                }
            }
        }

        public void b(Node node, int i) {
            try {
                node.b(this.f6974a, i, this.b);
            } catch (IOException e) {
                throw new SerializationException(e);
            }
        }
    }

    protected Node() {
    }

    public abstract Attributes a();

    public Node a(String str, String str2) {
        a().b(str, str2);
        return this;
    }

    public abstract String b();

    public String b(String str) {
        Validate.a((Object) str);
        if (!g()) {
            return "";
        }
        String b2 = a().b(str);
        if (b2.length() > 0) {
            return b2;
        }
        if (str.startsWith("abs:")) {
            return a(str.substring(4));
        }
        return "";
    }

    /* access modifiers changed from: package-private */
    public abstract void b(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException;

    public abstract int c();

    /* access modifiers changed from: package-private */
    public abstract void c(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException;

    /* access modifiers changed from: protected */
    public abstract void c(String str);

    /* access modifiers changed from: protected */
    public void c(Node node) {
        Validate.b(node.f6972a == this);
        int i = node.b;
        e().remove(i);
        c(i);
        node.f6972a = null;
    }

    public boolean d(String str) {
        Validate.a((Object) str);
        if (str.startsWith("abs:")) {
            String substring = str.substring(4);
            if (a().d(substring) && !a(substring).equals("")) {
                return true;
            }
        }
        return a().d(str);
    }

    /* access modifiers changed from: protected */
    public abstract List<Node> e();

    public void e(final String str) {
        Validate.a((Object) str);
        a((NodeVisitor) new NodeVisitor(this) {
            public void a(Node node, int i) {
            }

            public void b(Node node, int i) {
                node.c(str);
            }
        });
    }

    public boolean equals(Object obj) {
        return this == obj;
    }

    /* access modifiers changed from: package-private */
    public Document.OutputSettings f() {
        Document m = m();
        if (m == null) {
            m = new Document("");
        }
        return m.I();
    }

    /* access modifiers changed from: protected */
    public abstract boolean g();

    public boolean h() {
        return this.f6972a != null;
    }

    public Node i() {
        Node node = this.f6972a;
        if (node == null) {
            return null;
        }
        List<Node> e = node.e();
        int i = this.b + 1;
        if (e.size() > i) {
            return e.get(i);
        }
        return null;
    }

    public abstract String j();

    /* access modifiers changed from: package-private */
    public void k() {
    }

    public String l() {
        StringBuilder sb = new StringBuilder(128);
        a((Appendable) sb);
        return sb.toString();
    }

    public Document m() {
        Node q = q();
        if (q instanceof Document) {
            return (Document) q;
        }
        return null;
    }

    public Node n() {
        return this.f6972a;
    }

    public final Node o() {
        return this.f6972a;
    }

    public void p() {
        Validate.a((Object) this.f6972a);
        this.f6972a.c(this);
    }

    public Node q() {
        Node node = this;
        while (true) {
            Node node2 = node.f6972a;
            if (node2 == null) {
                return node;
            }
            node = node2;
        }
    }

    public int r() {
        return this.b;
    }

    public List<Node> s() {
        Node node = this.f6972a;
        if (node == null) {
            return Collections.emptyList();
        }
        List<Node> e = node.e();
        ArrayList arrayList = new ArrayList(e.size() - 1);
        for (Node next : e) {
            if (next != this) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public String toString() {
        return l();
    }

    public String a(String str) {
        Validate.b(str);
        if (!d(str)) {
            return "";
        }
        return StringUtil.a(b(), b(str));
    }

    public Node clone() {
        Node b2 = b((Node) null);
        LinkedList linkedList = new LinkedList();
        linkedList.add(b2);
        while (!linkedList.isEmpty()) {
            Node node = (Node) linkedList.remove();
            int c = node.c();
            for (int i = 0; i < c; i++) {
                List<Node> e = node.e();
                Node b3 = e.get(i).b(node);
                e.set(i, b3);
                linkedList.add(b3);
            }
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public void e(Node node) {
        Validate.a((Object) node);
        Node node2 = this.f6972a;
        if (node2 != null) {
            node2.c(this);
        }
        this.f6972a = node;
    }

    public Node a(int i) {
        return e().get(i);
    }

    private void c(int i) {
        List<Node> e = e();
        while (i < e.size()) {
            e.get(i).b(i);
            i++;
        }
    }

    public Node a(Node node) {
        Validate.a((Object) node);
        Validate.a((Object) this.f6972a);
        this.f6972a.a(this.b, node);
        return this;
    }

    public List<Node> d() {
        return Collections.unmodifiableList(e());
    }

    /* access modifiers changed from: protected */
    public void b(int i) {
        this.b = i;
    }

    /* access modifiers changed from: protected */
    public void d(Node node) {
        node.e(this);
    }

    /* access modifiers changed from: protected */
    public Node b(Node node) {
        int i;
        try {
            Node node2 = (Node) super.clone();
            node2.f6972a = node;
            if (node == null) {
                i = 0;
            } else {
                i = this.b;
            }
            node2.b = i;
            return node2;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i, Node... nodeArr) {
        Validate.a((Object[]) nodeArr);
        List<Node> e = e();
        for (Node d : nodeArr) {
            d(d);
        }
        e.addAll(i, Arrays.asList(nodeArr));
        c(i);
    }

    public Node a(NodeVisitor nodeVisitor) {
        Validate.a((Object) nodeVisitor);
        NodeTraversor.a(nodeVisitor, this);
        return this;
    }

    /* access modifiers changed from: protected */
    public void a(Appendable appendable) {
        NodeTraversor.a((NodeVisitor) new OuterHtmlVisitor(appendable, f()), this);
    }

    /* access modifiers changed from: protected */
    public void a(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        appendable.append(10).append(StringUtil.c(i * outputSettings.c()));
    }
}
