package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.nodes.Document;

public class DataNode extends LeafNode {
    public DataNode(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public void c(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    public String j() {
        return "#data";
    }

    public String toString() {
        return l();
    }

    public String u() {
        return t();
    }

    /* access modifiers changed from: package-private */
    public void b(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        appendable.append(u());
    }
}
