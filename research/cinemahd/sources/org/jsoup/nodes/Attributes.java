package org.jsoup.nodes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;
import org.jsoup.internal.Normalizer;
import org.jsoup.nodes.Document;

public class Attributes implements Iterable<Attribute>, Cloneable {
    private static final String[] d = new String[0];
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f6962a = 0;
    String[] b;
    String[] c;

    public Attributes() {
        String[] strArr = d;
        this.b = strArr;
        this.c = strArr;
    }

    private void c(String str, String str2) {
        a(this.f6962a + 1);
        String[] strArr = this.b;
        int i = this.f6962a;
        strArr[i] = str;
        this.c[i] = str2;
        this.f6962a = i + 1;
    }

    static String f(String str) {
        return str == null ? "" : str;
    }

    private int g(String str) {
        Validate.a((Object) str);
        for (int i = 0; i < this.f6962a; i++) {
            if (str.equalsIgnoreCase(this.b[i])) {
                return i;
            }
        }
        return -1;
    }

    /* access modifiers changed from: private */
    public void remove(int i) {
        Validate.a(i >= this.f6962a);
        int i2 = (this.f6962a - i) - 1;
        if (i2 > 0) {
            String[] strArr = this.b;
            int i3 = i + 1;
            System.arraycopy(strArr, i3, strArr, i, i2);
            String[] strArr2 = this.c;
            System.arraycopy(strArr2, i3, strArr2, i, i2);
        }
        this.f6962a--;
        String[] strArr3 = this.b;
        int i4 = this.f6962a;
        strArr3[i4] = null;
        this.c[i4] = null;
    }

    public boolean d(String str) {
        return g(str) != -1;
    }

    /* access modifiers changed from: package-private */
    public int e(String str) {
        Validate.a((Object) str);
        for (int i = 0; i < this.f6962a; i++) {
            if (str.equals(this.b[i])) {
                return i;
            }
        }
        return -1;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Attributes.class != obj.getClass()) {
            return false;
        }
        Attributes attributes = (Attributes) obj;
        if (this.f6962a == attributes.f6962a && Arrays.equals(this.b, attributes.b)) {
            return Arrays.equals(this.c, attributes.c);
        }
        return false;
    }

    public int hashCode() {
        return (((this.f6962a * 31) + Arrays.hashCode(this.b)) * 31) + Arrays.hashCode(this.c);
    }

    public Iterator<Attribute> iterator() {
        return new Iterator<Attribute>() {

            /* renamed from: a  reason: collision with root package name */
            int f6963a = 0;

            public boolean hasNext() {
                return this.f6963a < Attributes.this.f6962a;
            }

            public void remove() {
                Attributes attributes = Attributes.this;
                int i = this.f6963a - 1;
                this.f6963a = i;
                attributes.remove(i);
            }

            public Attribute next() {
                Attributes attributes = Attributes.this;
                String[] strArr = attributes.b;
                int i = this.f6963a;
                Attribute attribute = new Attribute(strArr[i], attributes.c[i], attributes);
                this.f6963a++;
                return attribute;
            }
        };
    }

    public int size() {
        return this.f6962a;
    }

    public String toString() {
        return b();
    }

    private void a(int i) {
        Validate.b(i >= this.f6962a);
        int length = this.b.length;
        if (length < i) {
            int i2 = length >= 4 ? this.f6962a * 2 : 4;
            if (i <= i2) {
                i = i2;
            }
            this.b = a(this.b, i);
            this.c = a(this.c, i);
        }
    }

    public String b(String str) {
        int g = g(str);
        if (g == -1) {
            return "";
        }
        return f(this.c[g]);
    }

    public Attributes clone() {
        try {
            Attributes attributes = (Attributes) super.clone();
            attributes.f6962a = this.f6962a;
            this.b = a(this.b, this.f6962a);
            this.c = a(this.c, this.f6962a);
            return attributes;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str, String str2) {
        int g = g(str);
        if (g != -1) {
            this.c[g] = str2;
            if (!this.b[g].equals(str)) {
                this.b[g] = str;
                return;
            }
            return;
        }
        c(str, str2);
    }

    public boolean c(String str) {
        return e(str) != -1;
    }

    public void c() {
        for (int i = 0; i < this.f6962a; i++) {
            String[] strArr = this.b;
            strArr[i] = Normalizer.a(strArr[i]);
        }
    }

    private static String[] a(String[] strArr, int i) {
        String[] strArr2 = new String[i];
        System.arraycopy(strArr, 0, strArr2, 0, Math.min(strArr.length, i));
        return strArr2;
    }

    public String b() {
        StringBuilder sb = new StringBuilder();
        try {
            a((Appendable) sb, new Document("").I());
            return sb.toString();
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    public String a(String str) {
        int e = e(str);
        if (e == -1) {
            return "";
        }
        return f(this.c[e]);
    }

    public Attributes a(String str, String str2) {
        int e = e(str);
        if (e != -1) {
            this.c[e] = str2;
        } else {
            c(str, str2);
        }
        return this;
    }

    public Attributes a(Attribute attribute) {
        Validate.a((Object) attribute);
        a(attribute.getKey(), attribute.getValue());
        attribute.c = this;
        return this;
    }

    public void a(Attributes attributes) {
        if (attributes.size() != 0) {
            a(this.f6962a + attributes.f6962a);
            Iterator<Attribute> it2 = attributes.iterator();
            while (it2.hasNext()) {
                a(it2.next());
            }
        }
    }

    public List<Attribute> a() {
        Attribute attribute;
        ArrayList arrayList = new ArrayList(this.f6962a);
        for (int i = 0; i < this.f6962a; i++) {
            String[] strArr = this.c;
            if (strArr[i] == null) {
                attribute = new BooleanAttribute(this.b[i]);
            } else {
                attribute = new Attribute(this.b[i], strArr[i], this);
            }
            arrayList.add(attribute);
        }
        return Collections.unmodifiableList(arrayList);
    }

    /* access modifiers changed from: package-private */
    public final void a(Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        int i = this.f6962a;
        for (int i2 = 0; i2 < i; i2++) {
            String str = this.b[i2];
            String str2 = this.c[i2];
            appendable.append(' ').append(str);
            if (outputSettings.g() != Document.OutputSettings.Syntax.html || (str2 != null && (!str2.equals(str) || !Attribute.b(str)))) {
                appendable.append("=\"");
                if (str2 == null) {
                    str2 = "";
                }
                Entities.a(appendable, str2, outputSettings, true, false, false);
                appendable.append('\"');
            }
        }
    }
}
