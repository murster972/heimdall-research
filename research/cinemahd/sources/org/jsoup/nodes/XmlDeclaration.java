package org.jsoup.nodes;

import java.io.IOException;
import java.util.Iterator;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class XmlDeclaration extends LeafNode {
    private final boolean d;

    public XmlDeclaration(String str, boolean z) {
        Validate.a((Object) str);
        this.c = str;
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public void c(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    public String j() {
        return "#declaration";
    }

    public String toString() {
        return l();
    }

    public String u() {
        return t();
    }

    private void a(Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        Iterator<Attribute> it2 = a().iterator();
        while (it2.hasNext()) {
            Attribute next = it2.next();
            if (!next.getKey().equals(j())) {
                appendable.append(' ');
                next.a(appendable, outputSettings);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        String str = "!";
        appendable.append("<").append(this.d ? str : "?").append(t());
        a(appendable, outputSettings);
        if (!this.d) {
            str = "?";
        }
        appendable.append(str).append(">");
    }
}
