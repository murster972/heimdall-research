package org.jsoup.nodes;

import com.facebook.react.bridge.BaseJavaModule;
import com.facebook.react.uimanager.ViewProps;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import org.jsoup.SerializationException;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;

public class Attribute implements Map.Entry<String, String>, Cloneable {
    private static final String[] d = {"allowfullscreen", BaseJavaModule.METHOD_TYPE_ASYNC, "autofocus", "checked", "compact", "declare", "default", "defer", "disabled", "formnovalidate", ViewProps.HIDDEN, "inert", "ismap", "itemscope", "multiple", "muted", "nohref", "noresize", "noshade", "novalidate", "nowrap", "open", "readonly", "required", "reversed", "seamless", "selected", "sortable", "truespeed", "typemustmatch"};

    /* renamed from: a  reason: collision with root package name */
    private String f6961a;
    private String b;
    Attributes c;

    public Attribute(String str, String str2) {
        this(str, str2, (Attributes) null);
    }

    protected static boolean b(String str) {
        return Arrays.binarySearch(d, str) >= 0;
    }

    /* renamed from: a */
    public String setValue(String str) {
        int e;
        String a2 = this.c.a(this.f6961a);
        Attributes attributes = this.c;
        if (!(attributes == null || (e = attributes.e(this.f6961a)) == -1)) {
            this.c.c[e] = str;
        }
        this.b = str;
        return a2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Attribute attribute = (Attribute) obj;
        String str = this.f6961a;
        if (str == null ? attribute.f6961a != null : !str.equals(attribute.f6961a)) {
            return false;
        }
        String str2 = this.b;
        String str3 = attribute.b;
        if (str2 != null) {
            return str2.equals(str3);
        }
        if (str3 == null) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        String str = this.f6961a;
        int i = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.b;
        if (str2 != null) {
            i = str2.hashCode();
        }
        return hashCode + i;
    }

    public String toString() {
        return a();
    }

    public Attribute(String str, String str2, Attributes attributes) {
        Validate.a((Object) str);
        this.f6961a = str.trim();
        Validate.b(str);
        this.b = str2;
        this.c = attributes;
    }

    public Attribute clone() {
        try {
            return (Attribute) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException(e);
        }
    }

    public String getKey() {
        return this.f6961a;
    }

    public String getValue() {
        return this.b;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        try {
            a(sb, new Document("").I());
            return sb.toString();
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    protected static void a(String str, String str2, Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        appendable.append(str);
        if (!a(str, str2, outputSettings)) {
            appendable.append("=\"");
            Entities.a(appendable, Attributes.f(str2), outputSettings, true, false, false);
            appendable.append('\"');
        }
    }

    /* access modifiers changed from: protected */
    public void a(Appendable appendable, Document.OutputSettings outputSettings) throws IOException {
        a(this.f6961a, this.b, appendable, outputSettings);
    }

    protected static boolean a(String str, String str2, Document.OutputSettings outputSettings) {
        return (str2 == null || "".equals(str2) || str2.equalsIgnoreCase(str)) && outputSettings.g() == Document.OutputSettings.Syntax.html && b(str);
    }
}
