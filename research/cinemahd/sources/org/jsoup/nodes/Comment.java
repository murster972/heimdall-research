package org.jsoup.nodes;

import java.io.IOException;
import org.jsoup.nodes.Document;

public class Comment extends LeafNode {
    public Comment(String str) {
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public void c(Appendable appendable, int i, Document.OutputSettings outputSettings) {
    }

    public String j() {
        return "#comment";
    }

    public String toString() {
        return l();
    }

    public String u() {
        return t();
    }

    /* access modifiers changed from: package-private */
    public void b(Appendable appendable, int i, Document.OutputSettings outputSettings) throws IOException {
        if (outputSettings.f()) {
            a(appendable, i, outputSettings);
        }
        appendable.append("<!--").append(u()).append("-->");
    }
}
