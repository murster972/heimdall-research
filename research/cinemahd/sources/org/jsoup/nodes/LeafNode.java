package org.jsoup.nodes;

import java.util.List;
import org.jsoup.helper.Validate;

abstract class LeafNode extends Node {
    Object c;

    LeafNode() {
    }

    private void u() {
        if (!g()) {
            Object obj = this.c;
            Attributes attributes = new Attributes();
            this.c = attributes;
            if (obj != null) {
                attributes.a(j(), (String) obj);
            }
        }
    }

    public final Attributes a() {
        u();
        return (Attributes) this.c;
    }

    public String b(String str) {
        Validate.a((Object) str);
        if (!g()) {
            return str.equals(j()) ? (String) this.c : "";
        }
        return super.b(str);
    }

    public int c() {
        return 0;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
    }

    public boolean d(String str) {
        u();
        return super.d(str);
    }

    /* access modifiers changed from: protected */
    public List<Node> e() {
        throw new UnsupportedOperationException("Leaf Nodes do not have child nodes.");
    }

    /* access modifiers changed from: protected */
    public final boolean g() {
        return this.c instanceof Attributes;
    }

    /* access modifiers changed from: package-private */
    public String t() {
        return b(j());
    }

    public Node a(String str, String str2) {
        if (g() || !str.equals(j())) {
            u();
            super.a(str, str2);
        } else {
            this.c = str2;
        }
        return this;
    }

    public String b() {
        return h() ? n().b() : "";
    }

    public String a(String str) {
        u();
        return super.a(str);
    }
}
