package org.jcodec;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ToJSON {

    /* renamed from: a  reason: collision with root package name */
    static Set<Class> f4140a = new HashSet();

    static {
        f4140a.add(String.class);
        f4140a.add(Byte.class);
        f4140a.add(Short.class);
        f4140a.add(Integer.class);
        f4140a.add(Long.class);
        f4140a.add(Float.class);
        f4140a.add(Double.class);
        f4140a.add(Character.class);
    }

    public static void a(Object obj, StringBuilder sb, String... strArr) {
        sb.append("{\n");
        HashSet hashSet = new HashSet(Arrays.asList(strArr));
        for (Method method : obj.getClass().getMethods()) {
            if (a(method)) {
                try {
                    String b = b(method);
                    if (strArr.length <= 0 || hashSet.contains(b)) {
                        Object invoke = method.invoke(obj, new Object[0]);
                        if (invoke.getClass().isPrimitive() || f4140a.contains(invoke.getClass()) || (invoke instanceof Iterable)) {
                            sb.append(b + ": ");
                            a(sb, invoke);
                            sb.append(",\n");
                        }
                    }
                } catch (Exception unused) {
                }
            }
        }
        sb.append("}");
    }

    private static String b(Method method) {
        String substring = method.getName().substring(3);
        return substring.substring(0, 1).toLowerCase() + substring.substring(1);
    }

    private static void a(StringBuilder sb, Object obj) {
        if (obj == null) {
            sb.append("null");
        } else if (obj == String.class) {
            sb.append("'");
            sb.append((String) obj);
            sb.append("'");
        } else if (obj instanceof Iterable) {
            Iterator it2 = ((Iterable) obj).iterator();
            sb.append("[");
            while (it2.hasNext()) {
                a(it2.next(), sb, new String[0]);
                if (it2.hasNext()) {
                    sb.append(",");
                }
            }
            sb.append("]");
        } else {
            sb.append(String.valueOf(obj));
        }
    }

    public static boolean a(Method method) {
        if (method.getName().startsWith("get") && method.getParameterTypes().length == 0 && !Void.TYPE.equals(method.getReturnType())) {
            return true;
        }
        return false;
    }
}
