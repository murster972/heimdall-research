package org.jcodec;

import java.nio.ByteBuffer;

public class PixelAspectExt extends Box {
    private int b;
    private int c;

    public PixelAspectExt() {
        super(new Header(b()));
    }

    public static String b() {
        return "pasp";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putInt(this.b);
        byteBuffer.putInt(this.c);
    }

    public Rational a() {
        return new Rational(this.b, this.c);
    }
}
