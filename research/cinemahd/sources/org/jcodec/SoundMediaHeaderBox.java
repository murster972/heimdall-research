package org.jcodec;

import java.nio.ByteBuffer;

public class SoundMediaHeaderBox extends FullBox {
    private short d;

    public SoundMediaHeaderBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "smhd";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putShort(this.d);
        byteBuffer.putShort(0);
    }
}
