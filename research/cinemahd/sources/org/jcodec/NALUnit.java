package org.jcodec;

import java.nio.ByteBuffer;

public class NALUnit {

    /* renamed from: a  reason: collision with root package name */
    public NALUnitType f4120a;
    public int b;

    public NALUnit(NALUnitType nALUnitType, int i) {
        this.f4120a = nALUnitType;
        this.b = i;
    }

    public static NALUnit a(ByteBuffer byteBuffer) {
        byte b2 = byteBuffer.get() & 255;
        return new NALUnit(NALUnitType.a(b2 & 31), (b2 >> 5) & 3);
    }
}
