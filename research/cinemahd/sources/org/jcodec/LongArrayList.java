package org.jcodec;

public class LongArrayList {

    /* renamed from: a  reason: collision with root package name */
    private long[] f4116a;
    private int b;
    private int c;

    public LongArrayList() {
        this(128);
    }

    public long[] a() {
        int i = this.b;
        long[] jArr = new long[i];
        System.arraycopy(this.f4116a, 0, jArr, 0, i);
        return jArr;
    }

    public LongArrayList(int i) {
        this.c = i;
        this.f4116a = new long[i];
    }

    public void a(long j) {
        int i = this.b;
        long[] jArr = this.f4116a;
        if (i >= jArr.length) {
            long[] jArr2 = new long[(jArr.length + this.c)];
            System.arraycopy(jArr, 0, jArr2, 0, jArr.length);
            this.f4116a = jArr2;
        }
        long[] jArr3 = this.f4116a;
        int i2 = this.b;
        this.b = i2 + 1;
        jArr3[i2] = j;
    }
}
