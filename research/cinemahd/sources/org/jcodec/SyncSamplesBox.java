package org.jcodec;

import java.nio.ByteBuffer;

public class SyncSamplesBox extends FullBox {
    private int[] d;

    public SyncSamplesBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "stss";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.length);
        int i = 0;
        while (true) {
            int[] iArr = this.d;
            if (i < iArr.length) {
                byteBuffer.putInt(iArr[i]);
                i++;
            } else {
                return;
            }
        }
    }

    public SyncSamplesBox(int[] iArr) {
        this();
        this.d = iArr;
    }
}
