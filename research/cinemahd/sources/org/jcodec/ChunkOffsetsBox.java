package org.jcodec;

import java.nio.ByteBuffer;

public class ChunkOffsetsBox extends FullBox {
    private long[] d;

    public ChunkOffsetsBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "stco";
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.length);
        for (long j : this.d) {
            byteBuffer.putInt((int) j);
        }
    }
}
