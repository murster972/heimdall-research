package org.jcodec;

import java.nio.ByteBuffer;

public class SampleToChunkBox extends FullBox {
    private SampleToChunkEntry[] d;

    public static class SampleToChunkEntry {

        /* renamed from: a  reason: collision with root package name */
        private long f4130a;
        private int b;
        private int c;

        public SampleToChunkEntry(long j, int i, int i2) {
            this.f4130a = j;
            this.b = i;
            this.c = i2;
        }

        public int a() {
            return this.b;
        }

        public int b() {
            return this.c;
        }

        public long c() {
            return this.f4130a;
        }
    }

    public SampleToChunkBox(SampleToChunkEntry[] sampleToChunkEntryArr) {
        super(new Header(a()));
        this.d = sampleToChunkEntryArr;
    }

    public static String a() {
        return "stsc";
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.length);
        for (SampleToChunkEntry sampleToChunkEntry : this.d) {
            byteBuffer.putInt((int) sampleToChunkEntry.c());
            byteBuffer.putInt(sampleToChunkEntry.a());
            byteBuffer.putInt(sampleToChunkEntry.b());
        }
    }

    public SampleToChunkBox() {
        super(new Header(a()));
    }
}
