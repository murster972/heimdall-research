package org.jcodec;

import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;

public class FileTypeBox extends Box {
    private String b;
    private int c;
    private Collection<String> d = new LinkedList();

    public FileTypeBox(String str, int i, Collection<String> collection) {
        super(new Header(a()));
        this.b = str;
        this.c = i;
        this.d = collection;
    }

    public static String a() {
        return "ftyp";
    }

    public void a(ByteBuffer byteBuffer) {
        byteBuffer.put(JCodecUtil.a(this.b));
        byteBuffer.putInt(this.c);
        for (String a2 : this.d) {
            byteBuffer.put(JCodecUtil.a(a2));
        }
    }
}
