package org.jcodec;

public class IntArrayList {

    /* renamed from: a  reason: collision with root package name */
    private int[] f4114a;
    private int b;
    private int c;

    public IntArrayList() {
        this(128);
    }

    public void a(int i) {
        int i2 = this.b;
        int[] iArr = this.f4114a;
        if (i2 >= iArr.length) {
            int[] iArr2 = new int[(iArr.length + this.c)];
            System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
            this.f4114a = iArr2;
        }
        int[] iArr3 = this.f4114a;
        int i3 = this.b;
        this.b = i3 + 1;
        iArr3[i3] = i;
    }

    public int[] b() {
        int i = this.b;
        int[] iArr = new int[i];
        System.arraycopy(this.f4114a, 0, iArr, 0, i);
        return iArr;
    }

    public IntArrayList(int i) {
        this.c = i;
        this.f4114a = new int[i];
    }

    public int a() {
        return this.b;
    }
}
