package org.jcodec;

import java.nio.ByteBuffer;

public class VideoMediaHeaderBox extends FullBox {
    int d;
    int e;
    int f;
    int g;

    public VideoMediaHeaderBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "vmhd";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putShort((short) this.d);
        byteBuffer.putShort((short) this.e);
        byteBuffer.putShort((short) this.f);
        byteBuffer.putShort((short) this.g);
    }

    public VideoMediaHeaderBox(int i, int i2, int i3, int i4) {
        super(new Header(a()));
        this.d = i;
        this.e = i2;
        this.f = i3;
        this.g = i4;
    }
}
