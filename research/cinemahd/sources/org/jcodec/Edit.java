package org.jcodec;

public class Edit {

    /* renamed from: a  reason: collision with root package name */
    private long f4110a;
    private long b;
    private float c;

    public Edit(long j, long j2, float f) {
        this.f4110a = j;
        this.b = j2;
        this.c = f;
    }

    public long a() {
        return this.f4110a;
    }

    public long b() {
        return this.b;
    }

    public float c() {
        return this.c;
    }

    public void a(long j) {
        this.b = j;
    }
}
