package org.jcodec;

import java.nio.ByteBuffer;

public class HandlerBox extends FullBox {
    private String d;
    private String e;
    private String f;
    private int g;
    private int h;
    private String i;

    public HandlerBox(String str, String str2, String str3, int i2, int i3) {
        super(new Header("hdlr"));
        this.d = str;
        this.e = str2;
        this.f = str3;
        this.g = i2;
        this.h = i3;
        this.i = "";
    }

    public static String a() {
        return "hdlr";
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.put(JCodecUtil.a(this.d));
        byteBuffer.put(JCodecUtil.a(this.e));
        byteBuffer.put(JCodecUtil.a(this.f));
        byteBuffer.putInt(this.g);
        byteBuffer.putInt(this.h);
        String str = this.i;
        if (str != null) {
            byteBuffer.put(JCodecUtil.a(str));
        }
    }

    public HandlerBox() {
        super(new Header(a()));
    }
}
