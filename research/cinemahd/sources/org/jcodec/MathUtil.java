package org.jcodec;

public class MathUtil {
    public static final int a(int i) {
        int i2 = i >> 31;
        return (i ^ i2) - i2;
    }

    public static final int b(int i) {
        if (i == 0) {
            return 0;
        }
        return (a(i) << 1) - ((~i) >>> 31);
    }
}
