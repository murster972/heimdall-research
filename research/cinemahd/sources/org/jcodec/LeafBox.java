package org.jcodec;

import java.nio.ByteBuffer;

public class LeafBox extends Box {
    private ByteBuffer b;

    public LeafBox(Header header, ByteBuffer byteBuffer) {
        super(header);
        this.b = byteBuffer;
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        NIOUtils.a(byteBuffer, this.b);
    }
}
