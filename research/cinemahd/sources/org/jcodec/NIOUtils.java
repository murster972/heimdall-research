package org.jcodec;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;

public class NIOUtils {
    public static void a(ByteBuffer byteBuffer, ByteBuffer byteBuffer2) {
        if (byteBuffer2.hasArray()) {
            byteBuffer.put(byteBuffer2.array(), byteBuffer2.arrayOffset() + byteBuffer2.position(), Math.min(byteBuffer.remaining(), byteBuffer2.remaining()));
        } else {
            byteBuffer.put(b(byteBuffer2, byteBuffer.remaining()));
        }
    }

    public static byte[] b(ByteBuffer byteBuffer, int i) {
        byte[] bArr = new byte[Math.min(byteBuffer.remaining(), i)];
        byteBuffer.duplicate().get(bArr);
        return bArr;
    }

    public static int a(ByteBuffer byteBuffer, int i) {
        int min = Math.min(byteBuffer.remaining(), i);
        byteBuffer.position(byteBuffer.position() + min);
        return min;
    }

    public static void a(ByteBuffer byteBuffer, String str, int i) {
        byteBuffer.put((byte) str.length());
        byteBuffer.put(JCodecUtil.a(str));
        a(byteBuffer, i - str.length());
    }

    public static void a(ByteBuffer byteBuffer, String str) {
        byteBuffer.put((byte) str.length());
        byteBuffer.put(JCodecUtil.a(str));
    }

    public static void a(WritableByteChannel writableByteChannel, long j) throws IOException {
        writableByteChannel.write((ByteBuffer) ByteBuffer.allocate(8).putLong(j).flip());
    }

    public static FileChannelWrapper a(String str, long j) throws FileNotFoundException {
        return new FileChannelWrapper(new FileOutputStream(str).getChannel(), str, j);
    }
}
