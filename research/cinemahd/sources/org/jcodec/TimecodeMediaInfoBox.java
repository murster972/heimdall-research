package org.jcodec;

import java.nio.ByteBuffer;

public class TimecodeMediaInfoBox extends FullBox {
    private short d;
    private short e;
    private short f;
    private short[] g;
    private short[] h;
    private String i;

    public TimecodeMediaInfoBox(short s, short s2, short s3, short[] sArr, short[] sArr2, String str) {
        this(new Header(a()));
        this.d = s;
        this.e = s2;
        this.f = s3;
        this.g = sArr;
        this.h = sArr2;
        this.i = str;
    }

    public static String a() {
        return "tcmi";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putShort(this.d);
        byteBuffer.putShort(this.e);
        byteBuffer.putShort(this.f);
        byteBuffer.putShort(0);
        byteBuffer.putShort(this.g[0]);
        byteBuffer.putShort(this.g[1]);
        byteBuffer.putShort(this.g[2]);
        byteBuffer.putShort(this.h[0]);
        byteBuffer.putShort(this.h[1]);
        byteBuffer.putShort(this.h[2]);
        NIOUtils.a(byteBuffer, this.i);
    }

    public TimecodeMediaInfoBox(Header header) {
        super(header);
        this.g = new short[3];
        this.h = new short[3];
    }
}
