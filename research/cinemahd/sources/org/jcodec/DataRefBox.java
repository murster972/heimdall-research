package org.jcodec;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class DataRefBox extends NodeBox {
    private static final MyFactory c = new MyFactory();

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> c = new HashMap();

        public MyFactory() {
            Class<AliasBox> cls = AliasBox.class;
            this.c.put(UrlBox.a(), UrlBox.class);
            this.c.put(AliasBox.d(), cls);
            this.c.put("cios", cls);
        }
    }

    public DataRefBox() {
        this(new Header(b()));
    }

    public static String b() {
        return "dref";
    }

    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putInt(0);
        byteBuffer.putInt(this.b.size());
        super.a(byteBuffer);
    }

    private DataRefBox(Header header) {
        super(header);
    }
}
