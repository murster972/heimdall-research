package org.jcodec;

import com.facebook.common.util.ByteConstants;
import com.google.ar.core.ImageMetadata;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.WritableByteChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MP4Muxer {

    /* renamed from: a  reason: collision with root package name */
    private List<AbstractMP4MuxerTrack> f4117a;
    private long b;
    private int c;
    SeekableByteChannel d;

    public MP4Muxer(SeekableByteChannel seekableByteChannel, Brand brand) throws IOException {
        this(seekableByteChannel, brand.a());
    }

    public static VideoSampleEntry a(String str, Size size, String str2) {
        return new VideoSampleEntry(new Header(str), 0, 0, "jcod", 0, 768, (short) size.b(), (short) size.a(), 72, 72, 1, str2 != null ? str2 : "jcodec", 24, 1, -1);
    }

    public AbstractMP4MuxerTrack b() {
        for (AbstractMP4MuxerTrack next : this.f4117a) {
            if (next.d()) {
                return next;
            }
        }
        return null;
    }

    public void c() throws IOException {
        a(a());
    }

    public MP4Muxer(SeekableByteChannel seekableByteChannel, FileTypeBox fileTypeBox) throws IOException {
        this.f4117a = new ArrayList();
        this.c = 1;
        this.d = seekableByteChannel;
        ByteBuffer allocate = ByteBuffer.allocate(ByteConstants.KB);
        fileTypeBox.b(allocate);
        new Header("wide", 8).a(allocate);
        new Header("mdat", 1).a(allocate);
        this.b = (long) allocate.position();
        allocate.putLong(0);
        allocate.flip();
        seekableByteChannel.write(allocate);
    }

    public FramesMP4MuxerTrack a(TrackType trackType, int i) {
        SeekableByteChannel seekableByteChannel = this.d;
        int i2 = this.c;
        this.c = i2 + 1;
        FramesMP4MuxerTrack framesMP4MuxerTrack = new FramesMP4MuxerTrack(seekableByteChannel, i2, trackType, i);
        this.f4117a.add(framesMP4MuxerTrack);
        return framesMP4MuxerTrack;
    }

    public void a(MovieBox movieBox) throws IOException {
        MP4Util.a(this.d, movieBox);
        this.d.g(this.b);
        NIOUtils.a((WritableByteChannel) this.d, (this.d.position() - this.b) + 8);
    }

    public MovieBox a() throws IOException {
        MovieBox movieBox = new MovieBox();
        MovieHeaderBox a2 = a((NodeBox) movieBox);
        movieBox.a(a2);
        for (AbstractMP4MuxerTrack a3 : this.f4117a) {
            Box a4 = a3.a(a2);
            if (a4 != null) {
                movieBox.a(a4);
            }
        }
        return movieBox;
    }

    private MovieHeaderBox a(NodeBox nodeBox) {
        int b2 = this.f4117a.get(0).b();
        long c2 = this.f4117a.get(0).c();
        AbstractMP4MuxerTrack b3 = b();
        if (b3 != null) {
            b2 = b3.b();
            c2 = b3.c();
        }
        return new MovieHeaderBox(b2, c2, 1.0f, 1.0f, new Date().getTime(), new Date().getTime(), new int[]{ImageMetadata.CONTROL_AE_ANTIBANDING_MODE, 0, 0, 0, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE, 0, 0, 0, 1073741824}, this.c);
    }
}
