package org.jcodec;

import android.annotation.TargetApi;
import com.facebook.ads.AudienceNetworkActivity;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class UrlBox extends FullBox {
    private String d;

    public static String a() {
        return "url ";
    }

    /* access modifiers changed from: protected */
    @TargetApi(9)
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        Charset forName = Charset.forName(AudienceNetworkActivity.WEBVIEW_ENCODING);
        String str = this.d;
        if (str != null) {
            NIOUtils.a(byteBuffer, ByteBuffer.wrap(str.getBytes(forName)));
            byteBuffer.put((byte) 0);
        }
    }
}
