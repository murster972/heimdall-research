package org.jcodec;

import java.nio.ByteBuffer;

public class CompositionOffsetsBox extends FullBox {
    private Entry[] d;

    public static class Entry {

        /* renamed from: a  reason: collision with root package name */
        public int f4109a;
        public int b;

        public Entry(int i, int i2) {
            this.f4109a = i;
            this.b = i2;
        }

        public int a() {
            return this.b;
        }
    }

    public CompositionOffsetsBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "ctts";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.length);
        int i = 0;
        while (true) {
            Entry[] entryArr = this.d;
            if (i < entryArr.length) {
                byteBuffer.putInt(entryArr[i].f4109a);
                byteBuffer.putInt(this.d[i].b);
                i++;
            } else {
                return;
            }
        }
    }

    public CompositionOffsetsBox(Entry[] entryArr) {
        super(new Header(a()));
        this.d = entryArr;
    }
}
