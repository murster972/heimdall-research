package org.jcodec;

public enum ColorSpace {
    YUV420(3, new int[]{0, 1, 2}, new int[]{0, 1, 1}, new int[]{0, 1, 1}),
    YUV422(3, new int[]{0, 1, 2}, new int[]{0, 1, 1}, new int[]{0, 0, 0}),
    YUV444(3, new int[]{0, 1, 2}, new int[]{0, 0, 0}, new int[]{0, 0, 0}),
    MONO(1, new int[]{0, 0, 0}, new int[]{0, 0, 0}, new int[]{0, 0, 0});
    
    public int[] compHeight;
    public int[] compPlane;
    public int[] compWidth;
    public int nComp;

    private ColorSpace(int i, int[] iArr, int[] iArr2, int[] iArr3) {
        this.nComp = i;
        this.compPlane = iArr;
        this.compWidth = iArr2;
        this.compHeight = iArr3;
    }
}
