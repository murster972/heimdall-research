package org.jcodec;

import com.unity3d.ads.metadata.MediationMetaData;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class VideoSampleEntry extends SampleEntry {
    private static final MyFactory q = new MyFactory();
    private short d;
    private short e;
    private String f;
    private int g;
    private int h;
    private short i;
    private short j;
    private float k;
    private float l;
    private short m;
    private String n;
    private short o;
    private short p;

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> c = new HashMap();

        public MyFactory() {
            this.c.put(PixelAspectExt.b(), PixelAspectExt.class);
            this.c.put(ColorExtension.a(), ColorExtension.class);
            this.c.put(GamaExtension.a(), GamaExtension.class);
            this.c.put(CleanApertureExtension.a(), CleanApertureExtension.class);
            this.c.put(FielExtension.a(), FielExtension.class);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public VideoSampleEntry(Header header, short s, short s2, String str, int i2, int i3, short s3, short s4, long j2, long j3, short s5, String str2, short s6, short s7, short s8) {
        super(header, s7);
        Header header2 = header;
        this.d = s;
        this.e = s2;
        this.f = str;
        this.g = i2;
        this.h = i3;
        this.i = s3;
        this.j = s4;
        this.k = (float) j2;
        this.l = (float) j3;
        this.m = s5;
        this.n = str2;
        this.o = s6;
        this.p = s8;
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putShort(this.d);
        byteBuffer.putShort(this.e);
        byteBuffer.put(JCodecUtil.a(this.f), 0, 4);
        byteBuffer.putInt(this.g);
        byteBuffer.putInt(this.h);
        byteBuffer.putShort(this.i);
        byteBuffer.putShort(this.j);
        byteBuffer.putInt((int) (this.k * 65536.0f));
        byteBuffer.putInt((int) (this.l * 65536.0f));
        byteBuffer.putInt(0);
        byteBuffer.putShort(this.m);
        NIOUtils.a(byteBuffer, this.n, 31);
        byteBuffer.putShort(this.o);
        byteBuffer.putShort(this.p);
        c(byteBuffer);
    }

    public int b() {
        return this.j;
    }

    public int c() {
        return this.i;
    }

    public void a(StringBuilder sb) {
        sb.append(this.f4104a.a() + ": {\n");
        sb.append("entry: ");
        ToJSON.a(this, sb, MediationMetaData.KEY_VERSION, "revision", "vendor", "temporalQual", "spacialQual", "width", "height", "hRes", "vRes", "frameCount", "compressorName", "depth", "clrTbl");
        sb.append(",\nexts: [\n");
        b(sb);
        sb.append("\n]\n");
        sb.append("}\n");
    }
}
