package org.jcodec;

import java.nio.ByteBuffer;
import org.jcodec.RefPicMarking;

public class MappedH264ES {

    /* renamed from: a  reason: collision with root package name */
    private ByteBuffer f4119a;
    private SliceHeaderReader b;
    private IntObjectMap<PictureParameterSet> c = new IntObjectMap<>();
    private IntObjectMap<SeqParameterSet> d = new IntObjectMap<>();
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;

    public MappedH264ES(ByteBuffer byteBuffer) {
        this.f4119a = byteBuffer;
        this.b = new SliceHeaderReader();
        this.i = 0;
    }

    private SliceHeader a(ByteBuffer byteBuffer, NALUnit nALUnit) {
        BitReader bitReader = new BitReader(byteBuffer);
        SliceHeader a2 = this.b.a(bitReader);
        PictureParameterSet a3 = this.c.a(a2.i);
        this.b.a(a2, nALUnit, this.d.a(a3.e), a3, bitReader);
        return a2;
    }

    private void b(SliceHeader sliceHeader, int i2) {
        this.f = (this.f + 1) % i2;
    }

    public Packet c() {
        ByteBuffer duplicate = this.f4119a.duplicate();
        NALUnit nALUnit = null;
        SliceHeader sliceHeader = null;
        while (true) {
            this.f4119a.mark();
            ByteBuffer c2 = H264Utils.c(this.f4119a);
            if (c2 == null) {
                break;
            }
            NALUnit a2 = NALUnit.a(c2);
            NALUnitType nALUnitType = a2.f4120a;
            if (nALUnitType == NALUnitType.IDR_SLICE || nALUnitType == NALUnitType.NON_IDR_SLICE) {
                SliceHeader a3 = a(c2, a2);
                if (nALUnit != null && sliceHeader != null && !a(nALUnit, a2, sliceHeader, a3)) {
                    this.f4119a.reset();
                    break;
                }
                sliceHeader = a3;
                nALUnit = a2;
            } else if (nALUnitType == NALUnitType.PPS) {
                PictureParameterSet b2 = PictureParameterSet.b(c2);
                this.c.a(b2.d, b2);
            } else if (nALUnitType == NALUnitType.SPS) {
                SeqParameterSet b3 = SeqParameterSet.b(c2);
                this.d.a(b3.t, b3);
            }
        }
        duplicate.limit(this.f4119a.position());
        if (sliceHeader == null) {
            return null;
        }
        return a(duplicate, nALUnit, sliceHeader);
    }

    private int b(int i2, NALUnit nALUnit, SliceHeader sliceHeader) {
        return nALUnit.b == 0 ? (i2 * 2) - 1 : i2 * 2;
    }

    public SeqParameterSet[] b() {
        return (SeqParameterSet[]) this.d.a((T[]) new SeqParameterSet[0]);
    }

    private boolean a(NALUnit nALUnit, NALUnit nALUnit2, SliceHeader sliceHeader, SliceHeader sliceHeader2) {
        if (sliceHeader.i != sliceHeader2.i || sliceHeader.j != sliceHeader2.j) {
            return false;
        }
        SeqParameterSet seqParameterSet = sliceHeader.f4136a;
        if (seqParameterSet.f4133a == 0 && sliceHeader.m != sliceHeader2.m) {
            return false;
        }
        if (seqParameterSet.f4133a == 1) {
            int[] iArr = sliceHeader.o;
            int i2 = iArr[0];
            int[] iArr2 = sliceHeader2.o;
            if (!(i2 == iArr2[0] && iArr[1] == iArr2[1])) {
                return false;
            }
        }
        if ((nALUnit.b == 0 || nALUnit2.b == 0) && nALUnit.b != nALUnit2.b) {
            return false;
        }
        if ((nALUnit.f4120a == NALUnitType.IDR_SLICE) == (nALUnit2.f4120a == NALUnitType.IDR_SLICE) && sliceHeader.l == sliceHeader2.l) {
            return true;
        }
        return false;
    }

    private Packet a(ByteBuffer byteBuffer, NALUnit nALUnit, SliceHeader sliceHeader) {
        NALUnit nALUnit2 = nALUnit;
        SliceHeader sliceHeader2 = sliceHeader;
        int i2 = 1 << (sliceHeader2.f4136a.g + 4);
        if (a(sliceHeader2, i2)) {
            b(sliceHeader2, i2);
        }
        int a2 = a(sliceHeader2.j, i2, a(sliceHeader2.c));
        int c2 = nALUnit2.f4120a == NALUnitType.NON_IDR_SLICE ? c(a2, nALUnit2, sliceHeader2) : 0;
        long j = (long) a2;
        int i3 = this.i;
        this.i = i3 + 1;
        return new Packet(byteBuffer, j, 1, 1, (long) i3, nALUnit2.f4120a == NALUnitType.IDR_SLICE, (TapeTimecode) null, c2);
    }

    private int c(int i2, NALUnit nALUnit, SliceHeader sliceHeader) {
        int i3 = sliceHeader.f4136a.f4133a;
        if (i3 == 0) {
            return a(nALUnit, sliceHeader);
        }
        if (i3 == 1) {
            return a(i2, nALUnit, sliceHeader);
        }
        return b(i2, nALUnit, sliceHeader);
    }

    private int a(int i2, int i3, boolean z) {
        int i4;
        if (this.f > i2) {
            i4 = this.e + i3;
        } else {
            i4 = this.e;
        }
        int i5 = i4 + i2;
        if (z) {
            i2 = 0;
        }
        this.f = i2;
        this.e = i4;
        return i5;
    }

    private boolean a(SliceHeader sliceHeader, int i2) {
        int i3 = sliceHeader.j;
        int i4 = this.f;
        return (i3 == i4 || i3 == (i4 + 1) % i2) ? false : true;
    }

    private int a(int i2, NALUnit nALUnit, SliceHeader sliceHeader) {
        int i3;
        int i4;
        if (sliceHeader.f4136a.I == 0) {
            i2 = 0;
        }
        if (nALUnit.b == 0 && i2 > 0) {
            i2--;
        }
        int i5 = 0;
        int i6 = 0;
        while (true) {
            SeqParameterSet seqParameterSet = sliceHeader.f4136a;
            i3 = seqParameterSet.I;
            if (i5 >= i3) {
                break;
            }
            i6 += seqParameterSet.F[i5];
            i5++;
        }
        if (i2 > 0) {
            int i7 = i2 - 1;
            int i8 = i7 / i3;
            int i9 = i7 % i3;
            i4 = i8 * i6;
            for (int i10 = 0; i10 <= i9; i10++) {
                i4 += sliceHeader.f4136a.F[i10];
            }
        } else {
            i4 = 0;
        }
        if (nALUnit.b == 0) {
            i4 += sliceHeader.f4136a.v;
        }
        return i4 + sliceHeader.o[0];
    }

    private int a(NALUnit nALUnit, SliceHeader sliceHeader) {
        int i2;
        int i3 = sliceHeader.m;
        int i4 = 1 << (sliceHeader.f4136a.h + 4);
        int i5 = this.h;
        if (i3 >= i5 || i5 - i3 < i4 / 2) {
            int i6 = this.h;
            if (i3 <= i6 || i3 - i6 <= i4 / 2) {
                i2 = this.g;
            } else {
                i2 = this.g - i4;
            }
        } else {
            i2 = this.g + i4;
        }
        if (nALUnit.b != 0) {
            this.g = i2;
            this.h = i3;
        }
        return i2 + i3;
    }

    private boolean a(RefPicMarking refPicMarking) {
        if (refPicMarking == null) {
            return false;
        }
        for (RefPicMarking.Instruction a2 : refPicMarking.a()) {
            if (a2.a() == RefPicMarking.InstrType.CLEAR) {
                return true;
            }
        }
        return false;
    }

    public PictureParameterSet[] a() {
        return (PictureParameterSet[]) this.c.a((T[]) new PictureParameterSet[0]);
    }
}
