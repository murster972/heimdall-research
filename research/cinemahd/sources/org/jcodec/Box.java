package org.jcodec;

import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import junit.framework.Assert;

public abstract class Box {

    /* renamed from: a  reason: collision with root package name */
    protected Header f4104a;

    public Box(Header header) {
        this.f4104a = header;
    }

    public static <T> T a(NodeBox nodeBox, Class<T> cls, String... strArr) {
        T[] a2 = a((Box) nodeBox, cls, strArr);
        if (a2.length > 0) {
            return a2[0];
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public abstract void a(ByteBuffer byteBuffer);

    public void b(ByteBuffer byteBuffer) {
        ByteBuffer duplicate = byteBuffer.duplicate();
        NIOUtils.a(byteBuffer, 8);
        a(byteBuffer);
        this.f4104a.a((byteBuffer.position() - duplicate.position()) - 8);
        Assert.assertEquals(this.f4104a.b(), 8);
        this.f4104a.a(duplicate);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        a(sb);
        return sb.toString();
    }

    private static void a(Box box, List<String> list, Collection<Box> collection) {
        if (list.size() > 0) {
            String remove = list.remove(0);
            if (box instanceof NodeBox) {
                for (Box next : ((NodeBox) box).a()) {
                    if (remove == null || remove.equals(next.f4104a.a())) {
                        a(next, list, collection);
                    }
                }
            }
            list.add(0, remove);
            return;
        }
        collection.add(box);
    }

    public static <T> T[] a(Box box, Class<T> cls, String... strArr) {
        LinkedList linkedList = new LinkedList();
        LinkedList linkedList2 = new LinkedList();
        for (String add : strArr) {
            linkedList2.add(add);
        }
        a(box, (List<String>) linkedList2, (Collection<Box>) linkedList);
        return linkedList.toArray((Object[]) Array.newInstance(cls, 0));
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        sb.append("'" + this.f4104a.a() + "'");
    }
}
