package org.jcodec;

import java.nio.ByteBuffer;

public class Packet {

    /* renamed from: a  reason: collision with root package name */
    private ByteBuffer f4122a;
    private long b;
    private long c;
    private boolean d;

    public Packet(ByteBuffer byteBuffer, long j, long j2, long j3, long j4, boolean z, TapeTimecode tapeTimecode) {
        this(byteBuffer, j, j2, j3, j4, z, tapeTimecode, 0);
    }

    public ByteBuffer a() {
        return this.f4122a;
    }

    public long b() {
        return this.c;
    }

    public long c() {
        return this.b;
    }

    public boolean d() {
        return this.d;
    }

    public Packet(ByteBuffer byteBuffer, long j, long j2, long j3, long j4, boolean z, TapeTimecode tapeTimecode, int i) {
        this.f4122a = byteBuffer;
        this.b = j;
        this.c = j3;
        this.d = z;
    }
}
