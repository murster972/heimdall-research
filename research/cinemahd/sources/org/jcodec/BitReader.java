package org.jcodec;

import java.nio.ByteBuffer;

public class BitReader {

    /* renamed from: a  reason: collision with root package name */
    protected int f4102a = 0;
    protected int b = b();
    private ByteBuffer c;

    public BitReader(ByteBuffer byteBuffer) {
        this.c = byteBuffer;
    }

    private int d() {
        if (this.c.hasRemaining()) {
            return this.c.get() & 255;
        }
        return 0;
    }

    private int e() {
        this.f4102a -= this.c.remaining() << 3;
        byte b2 = 0;
        if (this.c.hasRemaining()) {
            b2 = 0 | (this.c.get() & 255);
        }
        int i = b2 << 8;
        if (this.c.hasRemaining()) {
            i |= this.c.get() & 255;
        }
        int i2 = i << 8;
        if (this.c.hasRemaining()) {
            i2 |= this.c.get() & 255;
        }
        int i3 = i2 << 8;
        return this.c.hasRemaining() ? i3 | (this.c.get() & 255) : i3;
    }

    public int a() {
        int i = this.b;
        int i2 = i >>> 31;
        this.b = i << 1;
        this.f4102a++;
        if (this.f4102a == 32) {
            this.b = b();
        }
        return i2;
    }

    public final int b() {
        if (this.c.remaining() < 4) {
            return e();
        }
        this.f4102a -= 32;
        return ((this.c.get() & 255) << 24) | ((this.c.get() & 255) << 16) | ((this.c.get() & 255) << 8) | (this.c.get() & 255);
    }

    public int c() {
        return ((this.c.remaining() << 3) + 32) - this.f4102a;
    }

    public int a(int i) {
        if (i <= 24) {
            while (true) {
                int i2 = this.f4102a;
                if (i2 + i <= 32) {
                    return this.b >>> (32 - i);
                }
                this.f4102a = i2 - 8;
                this.b |= d() << this.f4102a;
            }
        } else {
            throw new IllegalArgumentException("Can not check more then 24 bit");
        }
    }

    public int b(int i) {
        if (i <= 32) {
            int i2 = 0;
            int i3 = this.f4102a;
            if (i + i3 > 31) {
                i -= 32 - i3;
                i2 = (0 | (this.b >>> i3)) << i;
                this.f4102a = 32;
                this.b = b();
            }
            if (i == 0) {
                return i2;
            }
            int i4 = this.b;
            int i5 = i2 | (i4 >>> (32 - i));
            this.b = i4 << i;
            this.f4102a += i;
            return i5;
        }
        throw new IllegalArgumentException("Can not read more then 32 bit");
    }
}
