package org.jcodec;

import com.facebook.imageutils.JfifUtil;

public class AspectRatio {
    public static final AspectRatio b = new AspectRatio(JfifUtil.MARKER_FIRST_BYTE);

    /* renamed from: a  reason: collision with root package name */
    private int f4101a;

    private AspectRatio(int i) {
        this.f4101a = i;
    }

    public static AspectRatio a(int i) {
        AspectRatio aspectRatio = b;
        if (i == aspectRatio.f4101a) {
            return aspectRatio;
        }
        return new AspectRatio(i);
    }

    public int a() {
        return this.f4101a;
    }
}
