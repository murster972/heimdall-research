package org.jcodec;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelWrapper implements SeekableByteChannel {

    /* renamed from: a  reason: collision with root package name */
    private FileChannel f4111a;
    private String b;
    private long c;

    public FileChannelWrapper(FileChannel fileChannel, String str, long j) throws FileNotFoundException {
        this.f4111a = fileChannel;
        this.b = str;
        this.c = j;
    }

    public void a(FileChannel fileChannel) {
        this.f4111a = fileChannel;
    }

    public void close() throws IOException {
        this.f4111a.close();
    }

    public SeekableByteChannel g(long j) throws IOException {
        this.f4111a.position(j);
        return this;
    }

    public String getFileName() {
        return this.b;
    }

    public boolean isOpen() {
        return this.f4111a.isOpen();
    }

    public long n() {
        return this.c;
    }

    public long position() throws IOException {
        return this.f4111a.position();
    }

    public int read(ByteBuffer byteBuffer) throws IOException {
        return this.f4111a.read(byteBuffer);
    }

    public int write(ByteBuffer byteBuffer) throws IOException {
        return this.f4111a.write(byteBuffer);
    }
}
