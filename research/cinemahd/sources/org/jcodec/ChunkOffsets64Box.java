package org.jcodec;

import java.nio.ByteBuffer;

public class ChunkOffsets64Box extends FullBox {
    private long[] d;

    public ChunkOffsets64Box() {
        super(new Header(a(), 0));
    }

    public static String a() {
        return "co64";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.length);
        for (long putLong : this.d) {
            byteBuffer.putLong(putLong);
        }
    }

    public ChunkOffsets64Box(long[] jArr) {
        this();
        this.d = jArr;
    }
}
