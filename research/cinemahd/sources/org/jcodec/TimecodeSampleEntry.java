package org.jcodec;

import java.nio.ByteBuffer;
import java.util.HashMap;

public class TimecodeSampleEntry extends SampleEntry {
    private static final MyFactory h = new MyFactory();
    private int d;
    private int e;
    private int f;
    private byte g;

    public static class MyFactory extends BoxFactory {
        public MyFactory() {
            new HashMap();
        }
    }

    public TimecodeSampleEntry() {
        super(new Header("tmcd"));
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(0);
        byteBuffer.putInt(this.d);
        byteBuffer.putInt(this.e);
        byteBuffer.putInt(this.f);
        byteBuffer.put(this.g);
        byteBuffer.put((byte) -49);
    }

    public void a(StringBuilder sb) {
        sb.append(this.f4104a.a() + ": {\n");
        sb.append("entry: ");
        ToJSON.a(this, sb, "flags", "timescale", "frameDuration", "numFrames");
        sb.append(",\nexts: [\n");
        b(sb);
        sb.append("\n]\n");
        sb.append("}\n");
    }
}
