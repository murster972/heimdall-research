package org.jcodec;

import java.nio.ByteBuffer;

public class TrackHeaderBox extends FullBox {
    private int d;
    private long e;
    private float f;
    private float g;
    private long h;
    private long i;
    private float j;
    private short k;
    private long l;
    private int[] m;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TrackHeaderBox(int i2, long j2, float f2, float f3, long j3, long j4, float f4, short s, long j5, int[] iArr) {
        super(new Header(a()));
        this.d = i2;
        this.e = j2;
        this.f = f2;
        this.g = f3;
        this.h = j3;
        this.i = j4;
        this.j = f4;
        this.k = s;
        this.l = j5;
        this.m = iArr;
    }

    public static String a() {
        return "tkhd";
    }

    private void c(ByteBuffer byteBuffer) {
        for (int i2 = 0; i2 < 9; i2++) {
            byteBuffer.putInt(this.m[i2]);
        }
    }

    private void d(ByteBuffer byteBuffer) {
        byteBuffer.putShort((short) ((int) (((double) this.j) * 256.0d)));
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(TimeUtil.a(this.h));
        byteBuffer.putInt(TimeUtil.a(this.i));
        byteBuffer.putInt(this.d);
        byteBuffer.putInt(0);
        byteBuffer.putInt((int) this.e);
        byteBuffer.putInt(0);
        byteBuffer.putInt(0);
        byteBuffer.putShort(this.k);
        byteBuffer.putShort((short) ((int) this.l));
        d(byteBuffer);
        byteBuffer.putShort(0);
        c(byteBuffer);
        byteBuffer.putInt((int) (this.f * 65536.0f));
        byteBuffer.putInt((int) (this.g * 65536.0f));
    }

    public TrackHeaderBox() {
        super(new Header(a()));
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        super.a(sb);
        sb.append(": ");
        ToJSON.a(this, sb, "trackId", "duration", "width", "height", "created", "modified", "volume", "layer", "altGroup");
    }
}
