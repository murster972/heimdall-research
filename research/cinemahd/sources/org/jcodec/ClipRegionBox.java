package org.jcodec;

import java.nio.ByteBuffer;

public class ClipRegionBox extends Box {
    private short b;
    private short c;
    private short d;
    private short e;
    private short f;

    public ClipRegionBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "crgn";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putShort(this.b);
        byteBuffer.putShort(this.c);
        byteBuffer.putShort(this.d);
        byteBuffer.putShort(this.e);
        byteBuffer.putShort(this.f);
    }
}
