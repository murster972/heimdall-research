package org.jcodec;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class AvcCBox extends Box {
    private int b;
    private int c;
    private int d;
    private List<ByteBuffer> e;
    private List<ByteBuffer> f;

    public AvcCBox() {
        super(new Header(a()));
        this.e = new ArrayList();
        this.f = new ArrayList();
    }

    public static String a() {
        return "avcC";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.put((byte) 1);
        byteBuffer.put((byte) this.b);
        byteBuffer.put((byte) this.c);
        byteBuffer.put((byte) this.d);
        byteBuffer.put((byte) -1);
        byteBuffer.put((byte) (this.e.size() | 224));
        for (ByteBuffer next : this.e) {
            byteBuffer.putShort((short) (next.remaining() + 1));
            byteBuffer.put((byte) 103);
            NIOUtils.a(byteBuffer, next);
        }
        byteBuffer.put((byte) this.f.size());
        for (ByteBuffer next2 : this.f) {
            byteBuffer.putShort((short) ((byte) (next2.remaining() + 1)));
            byteBuffer.put((byte) 104);
            NIOUtils.a(byteBuffer, next2);
        }
    }

    public AvcCBox(int i, int i2, int i3, List<ByteBuffer> list, List<ByteBuffer> list2) {
        this();
        this.b = i;
        this.c = i2;
        this.d = i3;
        this.e = list;
        this.f = list2;
    }
}
