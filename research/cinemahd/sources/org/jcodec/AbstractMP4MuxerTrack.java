package org.jcodec;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import org.jcodec.SampleToChunkBox;

public abstract class AbstractMP4MuxerTrack {

    /* renamed from: a  reason: collision with root package name */
    protected int f4098a;
    protected TrackType b;
    protected int c;
    protected Rational d;
    protected Unit e;
    protected long f;
    protected List<ByteBuffer> g = new ArrayList();
    protected List<SampleToChunkBox.SampleToChunkEntry> h = new ArrayList();
    protected int i = -1;
    protected int j = 0;
    protected boolean k;
    protected List<SampleEntry> l = new ArrayList();
    protected List<Edit> m;
    private String n;

    /* renamed from: org.jcodec.AbstractMP4MuxerTrack$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4099a = new int[TrackType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                org.jcodec.TrackType[] r0 = org.jcodec.TrackType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f4099a = r0
                int[] r0 = f4099a     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.jcodec.TrackType r1 = org.jcodec.TrackType.VIDEO     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f4099a     // Catch:{ NoSuchFieldError -> 0x001f }
                org.jcodec.TrackType r1 = org.jcodec.TrackType.SOUND     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f4099a     // Catch:{ NoSuchFieldError -> 0x002a }
                org.jcodec.TrackType r1 = org.jcodec.TrackType.TIMECODE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jcodec.AbstractMP4MuxerTrack.AnonymousClass1.<clinit>():void");
        }
    }

    public AbstractMP4MuxerTrack(int i2, TrackType trackType, int i3) {
        this.f4098a = i2;
        this.b = trackType;
        this.c = i3;
    }

    /* access modifiers changed from: protected */
    public abstract Box a(MovieHeaderBox movieHeaderBox) throws IOException;

    public void a(Rational rational, Unit unit) {
        this.d = rational;
        this.e = unit;
    }

    public int b() {
        return this.c;
    }

    public abstract long c();

    public void c(TrakBox trakBox) {
        Size a2 = a();
        if (this.b == TrackType.VIDEO) {
            NodeBox nodeBox = new NodeBox(new Header("tapt"));
            nodeBox.a((Box) new ClearApertureBox(a2.b(), a2.a()));
            nodeBox.a((Box) new ProductionApertureBox(a2.b(), a2.a()));
            nodeBox.a((Box) new EncodedPixelBox(a2.b(), a2.a()));
            trakBox.a((Box) nodeBox);
        }
    }

    public boolean d() {
        return this.b == TrackType.VIDEO;
    }

    /* access modifiers changed from: protected */
    public void b(TrakBox trakBox) {
        if (this.n != null) {
            NodeBox nodeBox = new NodeBox(new Header("udta"));
            nodeBox.a((Box) new NameBox(this.n));
            trakBox.a((Box) nodeBox);
        }
    }

    public Size a() {
        int i2;
        int i3 = 0;
        if (this.l.get(0) instanceof VideoSampleEntry) {
            VideoSampleEntry videoSampleEntry = (VideoSampleEntry) this.l.get(0);
            PixelAspectExt pixelAspectExt = (PixelAspectExt) Box.a((NodeBox) videoSampleEntry, PixelAspectExt.class, PixelAspectExt.b());
            Rational a2 = pixelAspectExt != null ? pixelAspectExt.a() : new Rational(1, 1);
            i3 = (a2.b() * videoSampleEntry.c()) / a2.a();
            i2 = videoSampleEntry.b();
        } else {
            i2 = 0;
        }
        return new Size(i3, i2);
    }

    public void a(SampleEntry sampleEntry) {
        if (!this.k) {
            this.l.add(sampleEntry);
            return;
        }
        throw new IllegalStateException("The muxer track has finished muxing");
    }

    /* access modifiers changed from: protected */
    public void a(TrakBox trakBox) {
        if (this.m != null) {
            NodeBox nodeBox = new NodeBox(new Header("edts"));
            nodeBox.a((Box) new EditListBox(this.m));
            trakBox.a((Box) nodeBox);
        }
    }

    /* access modifiers changed from: protected */
    public void a(MediaInfoBox mediaInfoBox, TrackType trackType) {
        int i2 = AnonymousClass1.f4099a[trackType.ordinal()];
        if (i2 == 1) {
            VideoMediaHeaderBox videoMediaHeaderBox = new VideoMediaHeaderBox(0, 0, 0, 0);
            videoMediaHeaderBox.a(1);
            mediaInfoBox.a((Box) videoMediaHeaderBox);
        } else if (i2 == 2) {
            SoundMediaHeaderBox soundMediaHeaderBox = new SoundMediaHeaderBox();
            soundMediaHeaderBox.a(1);
            mediaInfoBox.a((Box) soundMediaHeaderBox);
        } else if (i2 == 3) {
            NodeBox nodeBox = new NodeBox(new Header("gmhd"));
            nodeBox.a((Box) new GenericMediaInfoBox());
            NodeBox nodeBox2 = new NodeBox(new Header("tmcd"));
            nodeBox.a((Box) nodeBox2);
            nodeBox2.a((Box) new TimecodeMediaInfoBox(0, 0, 12, new short[]{0, 0, 0}, new short[]{255, 255, 255}, "Lucida Grande"));
            mediaInfoBox.a((Box) nodeBox);
        } else {
            throw new IllegalStateException("Handler " + trackType.a() + " not supported");
        }
    }

    /* access modifiers changed from: protected */
    public void a(NodeBox nodeBox) {
        DataInfoBox dataInfoBox = new DataInfoBox();
        nodeBox.a((Box) dataInfoBox);
        DataRefBox dataRefBox = new DataRefBox();
        dataInfoBox.a((Box) dataRefBox);
        dataRefBox.a((Box) new LeafBox(new Header("alis", 0), ByteBuffer.wrap(new byte[]{0, 0, 0, 1})));
    }
}
