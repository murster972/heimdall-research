package org.jcodec;

public class ScalingList {

    /* renamed from: a  reason: collision with root package name */
    public int[] f4131a;
    public boolean b;

    public void a(BitWriter bitWriter) {
        int i = 0;
        if (this.b) {
            CAVLCWriter.a(bitWriter, 0, "SPS: ");
            return;
        }
        int i2 = 8;
        while (true) {
            int[] iArr = this.f4131a;
            if (i < iArr.length) {
                CAVLCWriter.a(bitWriter, (iArr[i] - i2) - 256, "SPS: ");
                i2 = this.f4131a[i];
                i++;
            } else {
                return;
            }
        }
    }

    public static ScalingList a(BitReader bitReader, int i) {
        ScalingList scalingList = new ScalingList();
        scalingList.f4131a = new int[i];
        int i2 = 0;
        int i3 = 8;
        int i4 = 8;
        while (i2 < i) {
            if (i3 != 0) {
                i3 = ((CAVLCReader.b(bitReader, "deltaScale") + i4) + 256) % 256;
                scalingList.b = i2 == 0 && i3 == 0;
            }
            int[] iArr = scalingList.f4131a;
            if (i3 != 0) {
                i4 = i3;
            }
            iArr[i2] = i4;
            i4 = scalingList.f4131a[i2];
            i2++;
        }
        return scalingList;
    }
}
