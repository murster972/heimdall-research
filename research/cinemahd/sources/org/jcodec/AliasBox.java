package org.jcodec;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AliasBox extends FullBox {
    /* access modifiers changed from: private */
    public static Set<Integer> w = new HashSet();
    private String d;
    private short e;
    private short f;
    private short g;
    private String h;
    private int i;
    private short j;
    private short k;
    private int l;
    private String m;
    private int n;
    private int o;
    private String p;
    private String q;
    private short r;
    private short s;
    private int t;
    private short u;
    private List<ExtraField> v;

    public static class ExtraField {

        /* renamed from: a  reason: collision with root package name */
        short f4100a;
        int b;
        byte[] c;

        public String toString() {
            try {
                return new String(this.c, 0, this.b, AliasBox.w.contains(Short.valueOf(this.f4100a)) ? "UTF-16" : "UTF-8");
            } catch (UnsupportedEncodingException unused) {
                return null;
            }
        }
    }

    static {
        w.add(14);
        w.add(15);
    }

    public AliasBox() {
        super(new Header(d(), 0));
    }

    public static String d() {
        return "alis";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        if ((this.c & 1) == 0) {
            byteBuffer.put(JCodecUtil.a(this.d), 0, 4);
            byteBuffer.putShort(this.e);
            byteBuffer.putShort(this.f);
            byteBuffer.putShort(this.g);
            NIOUtils.a(byteBuffer, this.h, 27);
            byteBuffer.putInt(this.i);
            byteBuffer.putShort(this.j);
            byteBuffer.putShort(this.k);
            byteBuffer.putInt(this.l);
            NIOUtils.a(byteBuffer, this.m, 63);
            byteBuffer.putInt(this.n);
            byteBuffer.putInt(this.o);
            byteBuffer.put(JCodecUtil.a(this.p), 0, 4);
            byteBuffer.put(JCodecUtil.a(this.q), 0, 4);
            byteBuffer.putShort(this.r);
            byteBuffer.putShort(this.s);
            byteBuffer.putInt(this.t);
            byteBuffer.putShort(this.u);
            byteBuffer.put(new byte[10]);
            for (ExtraField next : this.v) {
                byteBuffer.putShort(next.f4100a);
                byteBuffer.putShort((short) next.b);
                byteBuffer.put(next.c);
            }
            byteBuffer.putShort(-1);
            byteBuffer.putShort(0);
        }
    }

    public ExtraField b(int i2) {
        for (ExtraField next : this.v) {
            if (next.f4100a == i2) {
                return next;
            }
        }
        return null;
    }

    public boolean b() {
        return (this.c & 1) != 0;
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        super.a(sb);
        sb.append(": ");
        if (b()) {
            sb.append("'self'");
            return;
        }
        sb.append("'" + a() + "'");
    }

    public String a() {
        ExtraField b = b(18);
        if (b == null) {
            return null;
        }
        return "/" + b.toString();
    }
}
