package org.jcodec;

public class CAVLCWriter {
    private CAVLCWriter() {
    }

    public static void a(BitWriter bitWriter, int i) {
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if (i2 >= 15) {
                i2 = 0;
                break;
            }
            int i4 = (1 << i2) + i3;
            if (i < i4) {
                break;
            }
            i2++;
            i3 = i4;
        }
        bitWriter.a(0, i2);
        bitWriter.a(1);
        bitWriter.a(i - i3, i2);
    }

    public static void b(BitWriter bitWriter, int i, String str) {
        a(bitWriter, i);
        Debug.a(str, Integer.valueOf(i));
    }

    public static void a(BitWriter bitWriter, int i, String str) {
        a(bitWriter, MathUtil.b(i));
        Debug.a(str, Integer.valueOf(i));
    }

    public static void a(BitWriter bitWriter, boolean z, String str) {
        bitWriter.a(z ? 1 : 0);
        Debug.a(str, Integer.valueOf(z));
    }

    public static void a(BitWriter bitWriter, int i, int i2) {
        bitWriter.a(i, i2);
    }

    public static void a(BitWriter bitWriter, long j, int i, String str) {
        for (int i2 = 0; i2 < i; i2++) {
            bitWriter.a(1 & ((int) (j >> ((i - i2) - 1))));
        }
        Debug.a(str, Long.valueOf(j));
    }

    public static void a(BitWriter bitWriter) {
        bitWriter.a(1);
        bitWriter.a();
    }
}
