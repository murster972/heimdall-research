package org.jcodec;

import java.nio.ByteBuffer;

public class SampleEntry extends NodeBox {
    private short c;

    public SampleEntry(Header header) {
        super(header);
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.put(new byte[]{0, 0, 0, 0, 0, 0});
        byteBuffer.putShort(this.c);
    }

    /* access modifiers changed from: protected */
    public void c(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
    }

    public SampleEntry(Header header, short s) {
        super(header);
        this.c = s;
    }
}
