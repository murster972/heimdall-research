package org.jcodec;

public class SliceHeader {
    public int A;

    /* renamed from: a  reason: collision with root package name */
    public SeqParameterSet f4136a;
    public PictureParameterSet b;
    public RefPicMarking c;
    public RefPicMarkingIDR d;
    public int[][][] e;
    public PredictionWeightTable f;
    public boolean g;
    public SliceType h;
    public int i;
    public int j;
    public boolean k;
    public int l;
    public int m;
    public int n;
    public int[] o;
    public int p;
    public boolean q;
    public boolean r;
    public int[] s = new int[2];
    public int t;
    public int u;
    public boolean v;
    public int w;
    public int x;
    public int y;
    public int z;
}
