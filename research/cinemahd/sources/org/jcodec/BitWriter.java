package org.jcodec;

import java.nio.ByteBuffer;

public class BitWriter {

    /* renamed from: a  reason: collision with root package name */
    private final ByteBuffer f4103a;
    private int b;
    private int c;

    public BitWriter(ByteBuffer byteBuffer) {
        this.f4103a = byteBuffer;
        byteBuffer.position();
    }

    private final void b(int i) {
        this.f4103a.put((byte) (i >>> 24));
        this.f4103a.put((byte) (i >> 16));
        this.f4103a.put((byte) (i >> 8));
        this.f4103a.put((byte) i);
    }

    public void a() {
        int i = (this.c + 7) >> 3;
        for (int i2 = 0; i2 < i; i2++) {
            this.f4103a.put((byte) (this.b >>> 24));
            this.b <<= 8;
        }
    }

    public final void a(int i, int i2) {
        if (i2 > 32) {
            throw new IllegalArgumentException("Max 32 bit to write");
        } else if (i2 != 0) {
            int i3 = i & (-1 >>> (32 - i2));
            int i4 = this.c;
            if (32 - i4 >= i2) {
                this.b = (i3 << ((32 - i4) - i2)) | this.b;
                this.c = i4 + i2;
                if (this.c == 32) {
                    b(this.b);
                    this.c = 0;
                    this.b = 0;
                    return;
                }
                return;
            }
            int i5 = i2 - (32 - i4);
            this.b |= i3 >>> i5;
            b(this.b);
            this.b = i3 << (32 - i5);
            this.c = i5;
        }
    }

    public void a(int i) {
        int i2 = this.b;
        int i3 = this.c;
        this.b = (i << ((32 - i3) - 1)) | i2;
        this.c = i3 + 1;
        if (this.c == 32) {
            b(this.b);
            this.c = 0;
            this.b = 0;
        }
    }
}
