package org.jcodec;

import java.nio.ByteBuffer;

public class MP4Packet extends Packet {
    private int e;

    public MP4Packet(ByteBuffer byteBuffer, long j, long j2, long j3, long j4, boolean z, TapeTimecode tapeTimecode, long j5, int i) {
        super(byteBuffer, j, j2, j3, j4, z, tapeTimecode);
        this.e = i;
    }

    public int e() {
        return this.e;
    }
}
