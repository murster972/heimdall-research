package org.jcodec;

import java.nio.ByteBuffer;

public class TimeToSampleBox extends FullBox {
    private TimeToSampleEntry[] d;

    public static class TimeToSampleEntry {

        /* renamed from: a  reason: collision with root package name */
        int f4138a;
        int b;

        public TimeToSampleEntry(int i, int i2) {
            this.f4138a = i;
            this.b = i2;
        }

        public int a() {
            return this.f4138a;
        }

        public int b() {
            return this.b;
        }
    }

    public TimeToSampleBox(TimeToSampleEntry[] timeToSampleEntryArr) {
        super(new Header(a()));
        this.d = timeToSampleEntryArr;
    }

    public static String a() {
        return "stts";
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.length);
        for (TimeToSampleEntry timeToSampleEntry : this.d) {
            byteBuffer.putInt(timeToSampleEntry.a());
            byteBuffer.putInt(timeToSampleEntry.b());
        }
    }

    public TimeToSampleBox() {
        super(new Header(a()));
    }
}
