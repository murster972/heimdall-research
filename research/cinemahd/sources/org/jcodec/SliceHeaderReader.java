package org.jcodec;

import java.lang.reflect.Array;
import java.util.ArrayList;
import org.jcodec.RefPicMarking;

public class SliceHeaderReader {
    private static int a(int i) {
        int i2 = i - 1;
        int i3 = 0;
        while (i2 != 0) {
            i2 >>= 1;
            i3++;
        }
        return i3;
    }

    private static int[][] b(BitReader bitReader) {
        IntArrayList intArrayList = new IntArrayList();
        IntArrayList intArrayList2 = new IntArrayList();
        while (true) {
            int c = CAVLCReader.c(bitReader, "SH: reordering_of_pic_nums_idc");
            if (c == 3) {
                return new int[][]{intArrayList.b(), intArrayList2.b()};
            }
            intArrayList.a(c);
            intArrayList2.a(CAVLCReader.c(bitReader, "SH: abs_diff_pic_num_minus1"));
        }
    }

    public SliceHeader a(BitReader bitReader) {
        SliceHeader sliceHeader = new SliceHeader();
        CAVLCReader.c(bitReader, "SH: first_mb_in_slice");
        int c = CAVLCReader.c(bitReader, "SH: slice_type");
        sliceHeader.h = SliceType.a(c % 5);
        int i = c / 5;
        sliceHeader.i = CAVLCReader.c(bitReader, "SH: pic_parameter_set_id");
        return sliceHeader;
    }

    public SliceHeader a(SliceHeader sliceHeader, NALUnit nALUnit, SeqParameterSet seqParameterSet, PictureParameterSet pictureParameterSet, BitReader bitReader) {
        int i;
        SliceType sliceType;
        sliceHeader.b = pictureParameterSet;
        sliceHeader.f4136a = seqParameterSet;
        sliceHeader.j = CAVLCReader.b(bitReader, seqParameterSet.g + 4, "SH: frame_num");
        if (!seqParameterSet.z) {
            sliceHeader.g = CAVLCReader.a(bitReader, "SH: field_pic_flag");
            if (sliceHeader.g) {
                sliceHeader.k = CAVLCReader.a(bitReader, "SH: bottom_field_flag");
            }
        }
        if (nALUnit.f4120a == NALUnitType.IDR_SLICE) {
            sliceHeader.l = CAVLCReader.c(bitReader, "SH: idr_pic_id");
        }
        if (seqParameterSet.f4133a == 0) {
            sliceHeader.m = CAVLCReader.b(bitReader, seqParameterSet.h + 4, "SH: pic_order_cnt_lsb");
            if (pictureParameterSet.f && !seqParameterSet.b) {
                sliceHeader.n = CAVLCReader.b(bitReader, "SH: delta_pic_order_cnt_bottom");
            }
        }
        sliceHeader.o = new int[2];
        if (seqParameterSet.f4133a == 1 && !seqParameterSet.c) {
            sliceHeader.o[0] = CAVLCReader.b(bitReader, "SH: delta_pic_order_cnt[0]");
            if (pictureParameterSet.f && !seqParameterSet.b) {
                sliceHeader.o[1] = CAVLCReader.b(bitReader, "SH: delta_pic_order_cnt[1]");
            }
        }
        if (pictureParameterSet.p) {
            sliceHeader.p = CAVLCReader.c(bitReader, "SH: redundant_pic_cnt");
        }
        if (sliceHeader.h == SliceType.B) {
            sliceHeader.q = CAVLCReader.a(bitReader, "SH: direct_spatial_mv_pred_flag");
        }
        SliceType sliceType2 = sliceHeader.h;
        if (sliceType2 == SliceType.P || sliceType2 == SliceType.SP || sliceType2 == SliceType.B) {
            sliceHeader.r = CAVLCReader.a(bitReader, "SH: num_ref_idx_active_override_flag");
            if (sliceHeader.r) {
                sliceHeader.s[0] = CAVLCReader.c(bitReader, "SH: num_ref_idx_l0_active_minus1");
                if (sliceHeader.h == SliceType.B) {
                    sliceHeader.s[1] = CAVLCReader.c(bitReader, "SH: num_ref_idx_l1_active_minus1");
                }
            }
        }
        a(sliceHeader, bitReader);
        if ((pictureParameterSet.i && ((sliceType = sliceHeader.h) == SliceType.P || sliceType == SliceType.SP)) || (pictureParameterSet.j == 1 && sliceHeader.h == SliceType.B)) {
            a(seqParameterSet, pictureParameterSet, sliceHeader, bitReader);
        }
        if (nALUnit.b != 0) {
            a(nALUnit, sliceHeader, bitReader);
        }
        if (pictureParameterSet.f4123a && sliceHeader.h.a()) {
            sliceHeader.t = CAVLCReader.c(bitReader, "SH: cabac_init_idc");
        }
        sliceHeader.u = CAVLCReader.b(bitReader, "SH: slice_qp_delta");
        SliceType sliceType3 = sliceHeader.h;
        if (sliceType3 == SliceType.SP || sliceType3 == SliceType.SI) {
            if (sliceHeader.h == SliceType.SP) {
                sliceHeader.v = CAVLCReader.a(bitReader, "SH: sp_for_switch_flag");
            }
            sliceHeader.w = CAVLCReader.b(bitReader, "SH: slice_qs_delta");
        }
        if (pictureParameterSet.n) {
            sliceHeader.x = CAVLCReader.c(bitReader, "SH: disable_deblocking_filter_idc");
            if (sliceHeader.x != 1) {
                sliceHeader.y = CAVLCReader.b(bitReader, "SH: slice_alpha_c0_offset_div2");
                sliceHeader.z = CAVLCReader.b(bitReader, "SH: slice_beta_offset_div2");
            }
        }
        if (pictureParameterSet.g > 0 && (i = pictureParameterSet.h) >= 3 && i <= 5) {
            int a2 = (H264Utils.a(seqParameterSet) * (seqParameterSet.j + 1)) / (pictureParameterSet.c + 1);
            if ((H264Utils.a(seqParameterSet) * (seqParameterSet.j + 1)) % (pictureParameterSet.c + 1) > 0) {
                a2++;
            }
            sliceHeader.A = CAVLCReader.b(bitReader, a(a2 + 1), "SH: slice_group_change_cycle");
        }
        return sliceHeader;
    }

    private static void a(NALUnit nALUnit, SliceHeader sliceHeader, BitReader bitReader) {
        int c;
        if (nALUnit.f4120a == NALUnitType.IDR_SLICE) {
            sliceHeader.d = new RefPicMarkingIDR(CAVLCReader.a(bitReader, "SH: no_output_of_prior_pics_flag"), CAVLCReader.a(bitReader, "SH: long_term_reference_flag"));
        } else if (CAVLCReader.a(bitReader, "SH: adaptive_ref_pic_marking_mode_flag")) {
            ArrayList arrayList = new ArrayList();
            do {
                c = CAVLCReader.c(bitReader, "SH: memory_management_control_operation");
                RefPicMarking.Instruction instruction = null;
                switch (c) {
                    case 1:
                        instruction = new RefPicMarking.Instruction(RefPicMarking.InstrType.REMOVE_SHORT, CAVLCReader.c(bitReader, "SH: difference_of_pic_nums_minus1") + 1, 0);
                        break;
                    case 2:
                        instruction = new RefPicMarking.Instruction(RefPicMarking.InstrType.REMOVE_LONG, CAVLCReader.c(bitReader, "SH: long_term_pic_num"), 0);
                        break;
                    case 3:
                        instruction = new RefPicMarking.Instruction(RefPicMarking.InstrType.CONVERT_INTO_LONG, CAVLCReader.c(bitReader, "SH: difference_of_pic_nums_minus1") + 1, CAVLCReader.c(bitReader, "SH: long_term_frame_idx"));
                        break;
                    case 4:
                        instruction = new RefPicMarking.Instruction(RefPicMarking.InstrType.TRUNK_LONG, CAVLCReader.c(bitReader, "SH: max_long_term_frame_idx_plus1") - 1, 0);
                        break;
                    case 5:
                        instruction = new RefPicMarking.Instruction(RefPicMarking.InstrType.CLEAR, 0, 0);
                        break;
                    case 6:
                        instruction = new RefPicMarking.Instruction(RefPicMarking.InstrType.MARK_LONG, CAVLCReader.c(bitReader, "SH: long_term_frame_idx"), 0);
                        break;
                }
                if (instruction != null) {
                    arrayList.add(instruction);
                    continue;
                }
            } while (c != 0);
            sliceHeader.c = new RefPicMarking((RefPicMarking.Instruction[]) arrayList.toArray(new RefPicMarking.Instruction[0]));
        }
    }

    private static void a(SeqParameterSet seqParameterSet, PictureParameterSet pictureParameterSet, SliceHeader sliceHeader, BitReader bitReader) {
        Class<int> cls = int.class;
        sliceHeader.f = new PredictionWeightTable();
        int[] iArr = sliceHeader.r ? sliceHeader.s : pictureParameterSet.b;
        int[] iArr2 = {iArr[0] + 1, iArr[1] + 1};
        sliceHeader.f.f4125a = CAVLCReader.c(bitReader, "SH: luma_log2_weight_denom");
        if (seqParameterSet.f != ColorSpace.MONO) {
            sliceHeader.f.b = CAVLCReader.c(bitReader, "SH: chroma_log2_weight_denom");
        }
        PredictionWeightTable predictionWeightTable = sliceHeader.f;
        int i = 1 << predictionWeightTable.f4125a;
        int i2 = 1 << predictionWeightTable.b;
        for (int i3 = 0; i3 < 2; i3++) {
            PredictionWeightTable predictionWeightTable2 = sliceHeader.f;
            predictionWeightTable2.c[i3] = new int[iArr2[i3]];
            predictionWeightTable2.e[i3] = new int[iArr2[i3]];
            predictionWeightTable2.d[i3] = (int[][]) Array.newInstance(cls, new int[]{2, iArr2[i3]});
            sliceHeader.f.f[i3] = (int[][]) Array.newInstance(cls, new int[]{2, iArr2[i3]});
            for (int i4 = 0; i4 < iArr2[i3]; i4++) {
                PredictionWeightTable predictionWeightTable3 = sliceHeader.f;
                predictionWeightTable3.c[i3][i4] = i;
                predictionWeightTable3.e[i3][i4] = 0;
                int[][][] iArr3 = predictionWeightTable3.d;
                iArr3[i3][0][i4] = i2;
                int[][][] iArr4 = predictionWeightTable3.f;
                iArr4[i3][0][i4] = 0;
                iArr3[i3][1][i4] = i2;
                iArr4[i3][1][i4] = 0;
            }
        }
        a(seqParameterSet, pictureParameterSet, sliceHeader, bitReader, iArr2, 0);
        if (sliceHeader.h == SliceType.B) {
            a(seqParameterSet, pictureParameterSet, sliceHeader, bitReader, iArr2, 1);
        }
    }

    private static void a(SeqParameterSet seqParameterSet, PictureParameterSet pictureParameterSet, SliceHeader sliceHeader, BitReader bitReader, int[] iArr, int i) {
        for (int i2 = 0; i2 < iArr[i]; i2++) {
            if (CAVLCReader.a(bitReader, "SH: luma_weight_l0_flag")) {
                sliceHeader.f.c[i][i2] = CAVLCReader.b(bitReader, "SH: weight");
                sliceHeader.f.e[i][i2] = CAVLCReader.b(bitReader, "SH: offset");
            }
            if (seqParameterSet.f != ColorSpace.MONO && CAVLCReader.a(bitReader, "SH: chroma_weight_l0_flag")) {
                sliceHeader.f.d[i][0][i2] = CAVLCReader.b(bitReader, "SH: weight");
                sliceHeader.f.f[i][0][i2] = CAVLCReader.b(bitReader, "SH: offset");
                sliceHeader.f.d[i][1][i2] = CAVLCReader.b(bitReader, "SH: weight");
                sliceHeader.f.f[i][1][i2] = CAVLCReader.b(bitReader, "SH: offset");
            }
        }
    }

    private static void a(SliceHeader sliceHeader, BitReader bitReader) {
        sliceHeader.e = new int[2][][];
        if (sliceHeader.h.a() && CAVLCReader.a(bitReader, "SH: ref_pic_list_reordering_flag_l0")) {
            sliceHeader.e[0] = b(bitReader);
        }
        if (sliceHeader.h == SliceType.B && CAVLCReader.a(bitReader, "SH: ref_pic_list_reordering_flag_l1")) {
            sliceHeader.e[1] = b(bitReader);
        }
    }
}
