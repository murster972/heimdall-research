package org.jcodec;

public enum SliceType {
    P,
    B,
    I,
    SP,
    SI;

    public boolean a() {
        return (this == I || this == SI) ? false : true;
    }

    public static SliceType a(int i) {
        for (SliceType sliceType : values()) {
            if (sliceType.ordinal() == i) {
                return sliceType;
            }
        }
        return null;
    }
}
