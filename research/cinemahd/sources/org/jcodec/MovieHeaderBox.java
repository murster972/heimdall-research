package org.jcodec;

import java.nio.ByteBuffer;

public class MovieHeaderBox extends FullBox {
    private int d;
    private long e;
    private float f;
    private float g;
    private long h;
    private long i;
    private int[] j;
    private int k;

    public MovieHeaderBox(int i2, long j2, float f2, float f3, long j3, long j4, int[] iArr, int i3) {
        super(new Header(b()));
        this.d = i2;
        this.e = j2;
        this.f = f2;
        this.g = f3;
        this.h = j3;
        this.i = j4;
        this.j = iArr;
        this.k = i3;
    }

    public static String b() {
        return "mvhd";
    }

    private void b(ByteBuffer byteBuffer, float f2) {
        byteBuffer.putShort((short) ((int) (((double) f2) * 256.0d)));
    }

    private void c(ByteBuffer byteBuffer) {
        for (int i2 = 0; i2 < Math.min(9, this.j.length); i2++) {
            byteBuffer.putInt(this.j[i2]);
        }
        for (int min = Math.min(9, this.j.length); min < 9; min++) {
            byteBuffer.putInt(0);
        }
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(TimeUtil.a(this.h));
        byteBuffer.putInt(TimeUtil.a(this.i));
        byteBuffer.putInt(this.d);
        byteBuffer.putInt((int) this.e);
        a(byteBuffer, this.f);
        b(byteBuffer, this.g);
        byteBuffer.put(new byte[10]);
        c(byteBuffer);
        byteBuffer.put(new byte[24]);
        byteBuffer.putInt(this.k);
    }

    private void a(ByteBuffer byteBuffer, float f2) {
        byteBuffer.putInt((int) (((double) f2) * 65536.0d));
    }

    public int a() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        super.a(sb);
        sb.append(": ");
        ToJSON.a(this, sb, "timescale", "duration", "rate", "volume", "created", "modified", "nextTrackId");
    }
}
