package org.jcodec;

import com.google.android.gms.ads.AdRequest;
import java.util.Arrays;

public enum Brand {
    MP4("isom", AdRequest.MAX_CONTENT_URL_LENGTH, new String[]{"isom", "iso2", "avc1", "mp41"});
    
    private FileTypeBox ftyp;

    private Brand(String str, int i, String[] strArr) {
        this.ftyp = new FileTypeBox(str, i, Arrays.asList(strArr));
    }

    public FileTypeBox a() {
        return this.ftyp;
    }
}
