package org.jcodec;

public class Rational {

    /* renamed from: a  reason: collision with root package name */
    private final int f4126a;
    private final int b;

    public Rational(int i, int i2) {
        this.f4126a = i;
        this.b = i2;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.f4126a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Rational.class != obj.getClass()) {
            return false;
        }
        Rational rational = (Rational) obj;
        return this.b == rational.b && this.f4126a == rational.f4126a;
    }

    public int hashCode() {
        return ((this.b + 31) * 31) + this.f4126a;
    }
}
