package org.jcodec;

import java.nio.ByteBuffer;

public class FullBox extends Box {
    protected byte b;
    protected int c;

    public FullBox(Header header) {
        super(header);
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putInt((this.b << 24) | (this.c & 16777215));
    }

    public void a(int i) {
        this.c = i;
    }
}
