package org.jcodec;

import java.nio.ByteBuffer;
import org.jcodec.VUIParameters;

public class SeqParameterSet {
    public boolean A;
    public int B;
    public int C;
    public int D;
    public int E;
    public int[] F;
    public VUIParameters G;
    public ScalingMatrix H;
    public int I;

    /* renamed from: a  reason: collision with root package name */
    public int f4133a;
    public boolean b;
    public boolean c;
    public boolean d;
    public boolean e;
    public ColorSpace f;
    public int g;
    public int h;
    public int i;
    public int j;
    public int k;
    public int l;
    public boolean m;
    public int n;
    public boolean o;
    public boolean p;
    public boolean q;
    public boolean r;
    public int s;
    public int t;
    public boolean u;
    public int v;
    public int w;
    public int x;
    public boolean y;
    public boolean z;

    /* renamed from: org.jcodec.SeqParameterSet$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4134a = new int[ColorSpace.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                org.jcodec.ColorSpace[] r0 = org.jcodec.ColorSpace.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f4134a = r0
                int[] r0 = f4134a     // Catch:{ NoSuchFieldError -> 0x0014 }
                org.jcodec.ColorSpace r1 = org.jcodec.ColorSpace.MONO     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f4134a     // Catch:{ NoSuchFieldError -> 0x001f }
                org.jcodec.ColorSpace r1 = org.jcodec.ColorSpace.YUV420     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f4134a     // Catch:{ NoSuchFieldError -> 0x002a }
                org.jcodec.ColorSpace r1 = org.jcodec.ColorSpace.YUV422     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f4134a     // Catch:{ NoSuchFieldError -> 0x0035 }
                org.jcodec.ColorSpace r1 = org.jcodec.ColorSpace.YUV444     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: org.jcodec.SeqParameterSet.AnonymousClass1.<clinit>():void");
        }
    }

    public static ColorSpace a(int i2) {
        if (i2 == 0) {
            return ColorSpace.MONO;
        }
        if (i2 == 1) {
            return ColorSpace.YUV420;
        }
        if (i2 == 2) {
            return ColorSpace.YUV422;
        }
        if (i2 == 3) {
            return ColorSpace.YUV444;
        }
        throw new RuntimeException("Colorspace not supported");
    }

    public static SeqParameterSet b(ByteBuffer byteBuffer) {
        BitReader bitReader = new BitReader(byteBuffer);
        SeqParameterSet seqParameterSet = new SeqParameterSet();
        seqParameterSet.n = CAVLCReader.a(bitReader, 8, "SPS: profile_idc");
        seqParameterSet.o = CAVLCReader.a(bitReader, "SPS: constraint_set_0_flag");
        seqParameterSet.p = CAVLCReader.a(bitReader, "SPS: constraint_set_1_flag");
        seqParameterSet.q = CAVLCReader.a(bitReader, "SPS: constraint_set_2_flag");
        seqParameterSet.r = CAVLCReader.a(bitReader, "SPS: constraint_set_3_flag");
        CAVLCReader.a(bitReader, 4, "SPS: reserved_zero_4bits");
        seqParameterSet.s = CAVLCReader.a(bitReader, 8, "SPS: level_idc");
        seqParameterSet.t = CAVLCReader.c(bitReader, "SPS: seq_parameter_set_id");
        int i2 = seqParameterSet.n;
        if (i2 == 100 || i2 == 110 || i2 == 122 || i2 == 144) {
            seqParameterSet.f = a(CAVLCReader.c(bitReader, "SPS: chroma_format_idc"));
            if (seqParameterSet.f == ColorSpace.YUV444) {
                seqParameterSet.u = CAVLCReader.a(bitReader, "SPS: residual_color_transform_flag");
            }
            seqParameterSet.k = CAVLCReader.c(bitReader, "SPS: bit_depth_luma_minus8");
            seqParameterSet.l = CAVLCReader.c(bitReader, "SPS: bit_depth_chroma_minus8");
            seqParameterSet.m = CAVLCReader.a(bitReader, "SPS: qpprime_y_zero_transform_bypass_flag");
            if (CAVLCReader.a(bitReader, "SPS: seq_scaling_matrix_present_lag")) {
                a(bitReader, seqParameterSet);
            }
        } else {
            seqParameterSet.f = ColorSpace.YUV420;
        }
        seqParameterSet.g = CAVLCReader.c(bitReader, "SPS: log2_max_frame_num_minus4");
        seqParameterSet.f4133a = CAVLCReader.c(bitReader, "SPS: pic_order_cnt_type");
        int i3 = seqParameterSet.f4133a;
        if (i3 == 0) {
            seqParameterSet.h = CAVLCReader.c(bitReader, "SPS: log2_max_pic_order_cnt_lsb_minus4");
        } else if (i3 == 1) {
            seqParameterSet.c = CAVLCReader.a(bitReader, "SPS: delta_pic_order_always_zero_flag");
            seqParameterSet.v = CAVLCReader.b(bitReader, "SPS: offset_for_non_ref_pic");
            seqParameterSet.w = CAVLCReader.b(bitReader, "SPS: offset_for_top_to_bottom_field");
            seqParameterSet.I = CAVLCReader.c(bitReader, "SPS: num_ref_frames_in_pic_order_cnt_cycle");
            seqParameterSet.F = new int[seqParameterSet.I];
            for (int i4 = 0; i4 < seqParameterSet.I; i4++) {
                int[] iArr = seqParameterSet.F;
                iArr[i4] = CAVLCReader.b(bitReader, "SPS: offsetForRefFrame [" + i4 + "]");
            }
        }
        seqParameterSet.x = CAVLCReader.c(bitReader, "SPS: num_ref_frames");
        seqParameterSet.y = CAVLCReader.a(bitReader, "SPS: gaps_in_frame_num_value_allowed_flag");
        seqParameterSet.j = CAVLCReader.c(bitReader, "SPS: pic_width_in_mbs_minus1");
        seqParameterSet.i = CAVLCReader.c(bitReader, "SPS: pic_height_in_map_units_minus1");
        seqParameterSet.z = CAVLCReader.a(bitReader, "SPS: frame_mbs_only_flag");
        if (!seqParameterSet.z) {
            seqParameterSet.d = CAVLCReader.a(bitReader, "SPS: mb_adaptive_frame_field_flag");
        }
        seqParameterSet.e = CAVLCReader.a(bitReader, "SPS: direct_8x8_inference_flag");
        seqParameterSet.A = CAVLCReader.a(bitReader, "SPS: frame_cropping_flag");
        if (seqParameterSet.A) {
            seqParameterSet.B = CAVLCReader.c(bitReader, "SPS: frame_crop_left_offset");
            seqParameterSet.C = CAVLCReader.c(bitReader, "SPS: frame_crop_right_offset");
            seqParameterSet.D = CAVLCReader.c(bitReader, "SPS: frame_crop_top_offset");
            seqParameterSet.E = CAVLCReader.c(bitReader, "SPS: frame_crop_bottom_offset");
        }
        if (CAVLCReader.a(bitReader, "SPS: vui_parameters_present_flag")) {
            seqParameterSet.G = b(bitReader);
        }
        return seqParameterSet;
    }

    public static int a(ColorSpace colorSpace) {
        int i2 = AnonymousClass1.f4134a[colorSpace.ordinal()];
        if (i2 == 1) {
            return 0;
        }
        if (i2 == 2) {
            return 1;
        }
        if (i2 == 3) {
            return 2;
        }
        if (i2 == 4) {
            return 3;
        }
        throw new RuntimeException("Colorspace not supported");
    }

    private static void a(BitReader bitReader, SeqParameterSet seqParameterSet) {
        seqParameterSet.H = new ScalingMatrix();
        for (int i2 = 0; i2 < 8; i2++) {
            if (CAVLCReader.a(bitReader, "SPS: seqScalingListPresentFlag")) {
                ScalingMatrix scalingMatrix = seqParameterSet.H;
                scalingMatrix.f4132a = new ScalingList[8];
                scalingMatrix.b = new ScalingList[8];
                if (i2 < 6) {
                    scalingMatrix.f4132a[i2] = ScalingList.a(bitReader, 16);
                } else {
                    scalingMatrix.b[i2 - 6] = ScalingList.a(bitReader, 64);
                }
            }
        }
    }

    private static HRDParameters a(BitReader bitReader) {
        HRDParameters hRDParameters = new HRDParameters();
        hRDParameters.f4112a = CAVLCReader.c(bitReader, "SPS: cpb_cnt_minus1");
        hRDParameters.b = CAVLCReader.a(bitReader, 4, "HRD: bit_rate_scale");
        hRDParameters.c = CAVLCReader.a(bitReader, 4, "HRD: cpb_size_scale");
        int i2 = hRDParameters.f4112a;
        hRDParameters.d = new int[(i2 + 1)];
        hRDParameters.e = new int[(i2 + 1)];
        hRDParameters.f = new boolean[(i2 + 1)];
        for (int i3 = 0; i3 <= hRDParameters.f4112a; i3++) {
            hRDParameters.d[i3] = CAVLCReader.c(bitReader, "HRD: bit_rate_value_minus1");
            hRDParameters.e[i3] = CAVLCReader.c(bitReader, "HRD: cpb_size_value_minus1");
            hRDParameters.f[i3] = CAVLCReader.a(bitReader, "HRD: cbr_flag");
        }
        hRDParameters.g = CAVLCReader.a(bitReader, 5, "HRD: initial_cpb_removal_delay_length_minus1");
        hRDParameters.h = CAVLCReader.a(bitReader, 5, "HRD: cpb_removal_delay_length_minus1");
        hRDParameters.i = CAVLCReader.a(bitReader, 5, "HRD: dpb_output_delay_length_minus1");
        hRDParameters.j = CAVLCReader.a(bitReader, 5, "HRD: time_offset_length");
        return hRDParameters;
    }

    public void a(ByteBuffer byteBuffer) {
        BitWriter bitWriter = new BitWriter(byteBuffer);
        CAVLCWriter.a(bitWriter, (long) this.n, 8, "SPS: profile_idc");
        CAVLCWriter.a(bitWriter, this.o, "SPS: constraint_set_0_flag");
        CAVLCWriter.a(bitWriter, this.p, "SPS: constraint_set_1_flag");
        CAVLCWriter.a(bitWriter, this.q, "SPS: constraint_set_2_flag");
        CAVLCWriter.a(bitWriter, this.r, "SPS: constraint_set_3_flag");
        CAVLCWriter.a(bitWriter, 0, 4, "SPS: reserved");
        CAVLCWriter.a(bitWriter, (long) this.s, 8, "SPS: level_idc");
        CAVLCWriter.b(bitWriter, this.t, "SPS: seq_parameter_set_id");
        int i2 = this.n;
        boolean z2 = false;
        if (i2 == 100 || i2 == 110 || i2 == 122 || i2 == 144) {
            CAVLCWriter.b(bitWriter, a(this.f), "SPS: chroma_format_idc");
            if (this.f == ColorSpace.YUV444) {
                CAVLCWriter.a(bitWriter, this.u, "SPS: residual_color_transform_flag");
            }
            CAVLCWriter.b(bitWriter, this.k, "SPS: ");
            CAVLCWriter.b(bitWriter, this.l, "SPS: ");
            CAVLCWriter.a(bitWriter, this.m, "SPS: qpprime_y_zero_transform_bypass_flag");
            CAVLCWriter.a(bitWriter, this.H != null, "SPS: ");
            if (this.H != null) {
                for (int i3 = 0; i3 < 8; i3++) {
                    if (i3 < 6) {
                        CAVLCWriter.a(bitWriter, this.H.f4132a[i3] != null, "SPS: ");
                        ScalingList[] scalingListArr = this.H.f4132a;
                        if (scalingListArr[i3] != null) {
                            scalingListArr[i3].a(bitWriter);
                        }
                    } else {
                        int i4 = i3 - 6;
                        CAVLCWriter.a(bitWriter, this.H.b[i4] != null, "SPS: ");
                        ScalingList[] scalingListArr2 = this.H.b;
                        if (scalingListArr2[i4] != null) {
                            scalingListArr2[i4].a(bitWriter);
                        }
                    }
                }
            }
        }
        CAVLCWriter.b(bitWriter, this.g, "SPS: log2_max_frame_num_minus4");
        CAVLCWriter.b(bitWriter, this.f4133a, "SPS: pic_order_cnt_type");
        int i5 = this.f4133a;
        if (i5 == 0) {
            CAVLCWriter.b(bitWriter, this.h, "SPS: log2_max_pic_order_cnt_lsb_minus4");
        } else if (i5 == 1) {
            CAVLCWriter.a(bitWriter, this.c, "SPS: delta_pic_order_always_zero_flag");
            CAVLCWriter.a(bitWriter, this.v, "SPS: offset_for_non_ref_pic");
            CAVLCWriter.a(bitWriter, this.w, "SPS: offset_for_top_to_bottom_field");
            CAVLCWriter.b(bitWriter, this.F.length, "SPS: ");
            int i6 = 0;
            while (true) {
                int[] iArr = this.F;
                if (i6 >= iArr.length) {
                    break;
                }
                CAVLCWriter.a(bitWriter, iArr[i6], "SPS: ");
                i6++;
            }
        }
        CAVLCWriter.b(bitWriter, this.x, "SPS: num_ref_frames");
        CAVLCWriter.a(bitWriter, this.y, "SPS: gaps_in_frame_num_value_allowed_flag");
        CAVLCWriter.b(bitWriter, this.j, "SPS: pic_width_in_mbs_minus1");
        CAVLCWriter.b(bitWriter, this.i, "SPS: pic_height_in_map_units_minus1");
        CAVLCWriter.a(bitWriter, this.z, "SPS: frame_mbs_only_flag");
        if (!this.z) {
            CAVLCWriter.a(bitWriter, this.d, "SPS: mb_adaptive_frame_field_flag");
        }
        CAVLCWriter.a(bitWriter, this.e, "SPS: direct_8x8_inference_flag");
        CAVLCWriter.a(bitWriter, this.A, "SPS: frame_cropping_flag");
        if (this.A) {
            CAVLCWriter.b(bitWriter, this.B, "SPS: frame_crop_left_offset");
            CAVLCWriter.b(bitWriter, this.C, "SPS: frame_crop_right_offset");
            CAVLCWriter.b(bitWriter, this.D, "SPS: frame_crop_top_offset");
            CAVLCWriter.b(bitWriter, this.E, "SPS: frame_crop_bottom_offset");
        }
        if (this.G != null) {
            z2 = true;
        }
        CAVLCWriter.a(bitWriter, z2, "SPS: ");
        VUIParameters vUIParameters = this.G;
        if (vUIParameters != null) {
            a(vUIParameters, bitWriter);
        }
        CAVLCWriter.a(bitWriter);
    }

    private static VUIParameters b(BitReader bitReader) {
        VUIParameters vUIParameters = new VUIParameters();
        vUIParameters.f4143a = CAVLCReader.a(bitReader, "VUI: aspect_ratio_info_present_flag");
        if (vUIParameters.f4143a) {
            vUIParameters.y = AspectRatio.a(CAVLCReader.a(bitReader, 8, "VUI: aspect_ratio"));
            if (vUIParameters.y == AspectRatio.b) {
                vUIParameters.b = CAVLCReader.a(bitReader, 16, "VUI: sar_width");
                vUIParameters.c = CAVLCReader.a(bitReader, 16, "VUI: sar_height");
            }
        }
        vUIParameters.d = CAVLCReader.a(bitReader, "VUI: overscan_info_present_flag");
        if (vUIParameters.d) {
            vUIParameters.e = CAVLCReader.a(bitReader, "VUI: overscan_appropriate_flag");
        }
        vUIParameters.f = CAVLCReader.a(bitReader, "VUI: video_signal_type_present_flag");
        if (vUIParameters.f) {
            vUIParameters.g = CAVLCReader.a(bitReader, 3, "VUI: video_format");
            vUIParameters.h = CAVLCReader.a(bitReader, "VUI: video_full_range_flag");
            vUIParameters.i = CAVLCReader.a(bitReader, "VUI: colour_description_present_flag");
            if (vUIParameters.i) {
                vUIParameters.j = CAVLCReader.a(bitReader, 8, "VUI: colour_primaries");
                vUIParameters.k = CAVLCReader.a(bitReader, 8, "VUI: transfer_characteristics");
                vUIParameters.l = CAVLCReader.a(bitReader, 8, "VUI: matrix_coefficients");
            }
        }
        vUIParameters.m = CAVLCReader.a(bitReader, "VUI: chroma_loc_info_present_flag");
        if (vUIParameters.m) {
            vUIParameters.n = CAVLCReader.c(bitReader, "VUI chroma_sample_loc_type_top_field");
            vUIParameters.o = CAVLCReader.c(bitReader, "VUI chroma_sample_loc_type_bottom_field");
        }
        vUIParameters.p = CAVLCReader.a(bitReader, "VUI: timing_info_present_flag");
        if (vUIParameters.p) {
            vUIParameters.q = CAVLCReader.a(bitReader, 32, "VUI: num_units_in_tick");
            vUIParameters.r = CAVLCReader.a(bitReader, 32, "VUI: time_scale");
            vUIParameters.s = CAVLCReader.a(bitReader, "VUI: fixed_frame_rate_flag");
        }
        boolean a2 = CAVLCReader.a(bitReader, "VUI: nal_hrd_parameters_present_flag");
        if (a2) {
            vUIParameters.v = a(bitReader);
        }
        boolean a3 = CAVLCReader.a(bitReader, "VUI: vcl_hrd_parameters_present_flag");
        if (a3) {
            vUIParameters.w = a(bitReader);
        }
        if (a2 || a3) {
            vUIParameters.t = CAVLCReader.a(bitReader, "VUI: low_delay_hrd_flag");
        }
        vUIParameters.u = CAVLCReader.a(bitReader, "VUI: pic_struct_present_flag");
        if (CAVLCReader.a(bitReader, "VUI: bitstream_restriction_flag")) {
            vUIParameters.x = new VUIParameters.BitstreamRestriction();
            vUIParameters.x.f4144a = CAVLCReader.a(bitReader, "VUI: motion_vectors_over_pic_boundaries_flag");
            vUIParameters.x.b = CAVLCReader.c(bitReader, "VUI max_bytes_per_pic_denom");
            vUIParameters.x.c = CAVLCReader.c(bitReader, "VUI max_bits_per_mb_denom");
            vUIParameters.x.d = CAVLCReader.c(bitReader, "VUI log2_max_mv_length_horizontal");
            vUIParameters.x.e = CAVLCReader.c(bitReader, "VUI log2_max_mv_length_vertical");
            vUIParameters.x.f = CAVLCReader.c(bitReader, "VUI num_reorder_frames");
            vUIParameters.x.g = CAVLCReader.c(bitReader, "VUI max_dec_frame_buffering");
        }
        return vUIParameters;
    }

    private void a(VUIParameters vUIParameters, BitWriter bitWriter) {
        CAVLCWriter.a(bitWriter, vUIParameters.f4143a, "VUI: aspect_ratio_info_present_flag");
        if (vUIParameters.f4143a) {
            CAVLCWriter.a(bitWriter, (long) vUIParameters.y.a(), 8, "VUI: aspect_ratio");
            if (vUIParameters.y == AspectRatio.b) {
                CAVLCWriter.a(bitWriter, (long) vUIParameters.b, 16, "VUI: sar_width");
                CAVLCWriter.a(bitWriter, (long) vUIParameters.c, 16, "VUI: sar_height");
            }
        }
        CAVLCWriter.a(bitWriter, vUIParameters.d, "VUI: overscan_info_present_flag");
        if (vUIParameters.d) {
            CAVLCWriter.a(bitWriter, vUIParameters.e, "VUI: overscan_appropriate_flag");
        }
        CAVLCWriter.a(bitWriter, vUIParameters.f, "VUI: video_signal_type_present_flag");
        if (vUIParameters.f) {
            CAVLCWriter.a(bitWriter, (long) vUIParameters.g, 3, "VUI: video_format");
            CAVLCWriter.a(bitWriter, vUIParameters.h, "VUI: video_full_range_flag");
            CAVLCWriter.a(bitWriter, vUIParameters.i, "VUI: colour_description_present_flag");
            if (vUIParameters.i) {
                CAVLCWriter.a(bitWriter, (long) vUIParameters.j, 8, "VUI: colour_primaries");
                CAVLCWriter.a(bitWriter, (long) vUIParameters.k, 8, "VUI: transfer_characteristics");
                CAVLCWriter.a(bitWriter, (long) vUIParameters.l, 8, "VUI: matrix_coefficients");
            }
        }
        CAVLCWriter.a(bitWriter, vUIParameters.m, "VUI: chroma_loc_info_present_flag");
        if (vUIParameters.m) {
            CAVLCWriter.b(bitWriter, vUIParameters.n, "VUI: chroma_sample_loc_type_top_field");
            CAVLCWriter.b(bitWriter, vUIParameters.o, "VUI: chroma_sample_loc_type_bottom_field");
        }
        CAVLCWriter.a(bitWriter, vUIParameters.p, "VUI: timing_info_present_flag");
        if (vUIParameters.p) {
            CAVLCWriter.a(bitWriter, (long) vUIParameters.q, 32, "VUI: num_units_in_tick");
            CAVLCWriter.a(bitWriter, (long) vUIParameters.r, 32, "VUI: time_scale");
            CAVLCWriter.a(bitWriter, vUIParameters.s, "VUI: fixed_frame_rate_flag");
        }
        boolean z2 = true;
        CAVLCWriter.a(bitWriter, vUIParameters.v != null, "VUI: ");
        HRDParameters hRDParameters = vUIParameters.v;
        if (hRDParameters != null) {
            a(hRDParameters, bitWriter);
        }
        CAVLCWriter.a(bitWriter, vUIParameters.w != null, "VUI: ");
        HRDParameters hRDParameters2 = vUIParameters.w;
        if (hRDParameters2 != null) {
            a(hRDParameters2, bitWriter);
        }
        if (!(vUIParameters.v == null && vUIParameters.w == null)) {
            CAVLCWriter.a(bitWriter, vUIParameters.t, "VUI: low_delay_hrd_flag");
        }
        CAVLCWriter.a(bitWriter, vUIParameters.u, "VUI: pic_struct_present_flag");
        if (vUIParameters.x == null) {
            z2 = false;
        }
        CAVLCWriter.a(bitWriter, z2, "VUI: ");
        VUIParameters.BitstreamRestriction bitstreamRestriction = vUIParameters.x;
        if (bitstreamRestriction != null) {
            CAVLCWriter.a(bitWriter, bitstreamRestriction.f4144a, "VUI: motion_vectors_over_pic_boundaries_flag");
            CAVLCWriter.b(bitWriter, vUIParameters.x.b, "VUI: max_bytes_per_pic_denom");
            CAVLCWriter.b(bitWriter, vUIParameters.x.c, "VUI: max_bits_per_mb_denom");
            CAVLCWriter.b(bitWriter, vUIParameters.x.d, "VUI: log2_max_mv_length_horizontal");
            CAVLCWriter.b(bitWriter, vUIParameters.x.e, "VUI: log2_max_mv_length_vertical");
            CAVLCWriter.b(bitWriter, vUIParameters.x.f, "VUI: num_reorder_frames");
            CAVLCWriter.b(bitWriter, vUIParameters.x.g, "VUI: max_dec_frame_buffering");
        }
    }

    private void a(HRDParameters hRDParameters, BitWriter bitWriter) {
        CAVLCWriter.b(bitWriter, hRDParameters.f4112a, "HRD: cpb_cnt_minus1");
        CAVLCWriter.a(bitWriter, (long) hRDParameters.b, 4, "HRD: bit_rate_scale");
        CAVLCWriter.a(bitWriter, (long) hRDParameters.c, 4, "HRD: cpb_size_scale");
        for (int i2 = 0; i2 <= hRDParameters.f4112a; i2++) {
            CAVLCWriter.b(bitWriter, hRDParameters.d[i2], "HRD: ");
            CAVLCWriter.b(bitWriter, hRDParameters.e[i2], "HRD: ");
            CAVLCWriter.a(bitWriter, hRDParameters.f[i2], "HRD: ");
        }
        CAVLCWriter.a(bitWriter, (long) hRDParameters.g, 5, "HRD: initial_cpb_removal_delay_length_minus1");
        CAVLCWriter.a(bitWriter, (long) hRDParameters.h, 5, "HRD: cpb_removal_delay_length_minus1");
        CAVLCWriter.a(bitWriter, (long) hRDParameters.i, 5, "HRD: dpb_output_delay_length_minus1");
        CAVLCWriter.a(bitWriter, (long) hRDParameters.j, 5, "HRD: time_offset_length");
    }
}
