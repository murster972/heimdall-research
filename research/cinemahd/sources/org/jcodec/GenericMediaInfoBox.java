package org.jcodec;

import java.nio.ByteBuffer;

public class GenericMediaInfoBox extends FullBox {
    private short d;
    private short e;
    private short f;
    private short g;
    private short h;

    public GenericMediaInfoBox(Header header) {
        super(header);
    }

    public static String a() {
        return "gmin";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putShort(this.d);
        byteBuffer.putShort(this.e);
        byteBuffer.putShort(this.f);
        byteBuffer.putShort(this.g);
        byteBuffer.putShort(this.h);
        byteBuffer.putShort(0);
    }

    public GenericMediaInfoBox() {
        this(new Header(a()));
    }
}
