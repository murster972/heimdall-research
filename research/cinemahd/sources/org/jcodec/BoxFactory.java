package org.jcodec;

import java.util.HashMap;
import java.util.Map;

public class BoxFactory {
    private static BoxFactory b = new BoxFactory();

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Class<? extends Box>> f4105a = new HashMap();

    public BoxFactory() {
        Class<LeafBox> cls = LeafBox.class;
        Class<NodeBox> cls2 = NodeBox.class;
        this.f4105a.put(VideoMediaHeaderBox.a(), VideoMediaHeaderBox.class);
        this.f4105a.put(FileTypeBox.a(), FileTypeBox.class);
        this.f4105a.put(MovieBox.b(), MovieBox.class);
        this.f4105a.put(MovieHeaderBox.b(), MovieHeaderBox.class);
        this.f4105a.put(TrakBox.b(), TrakBox.class);
        this.f4105a.put(TrackHeaderBox.a(), TrackHeaderBox.class);
        this.f4105a.put("edts", cls2);
        this.f4105a.put(EditListBox.a(), EditListBox.class);
        this.f4105a.put(MediaBox.b(), MediaBox.class);
        this.f4105a.put(MediaHeaderBox.a(), MediaHeaderBox.class);
        this.f4105a.put(MediaInfoBox.b(), MediaInfoBox.class);
        this.f4105a.put(HandlerBox.a(), HandlerBox.class);
        this.f4105a.put(DataInfoBox.b(), DataInfoBox.class);
        this.f4105a.put("stbl", cls2);
        this.f4105a.put(SampleDescriptionBox.b(), SampleDescriptionBox.class);
        this.f4105a.put(TimeToSampleBox.a(), TimeToSampleBox.class);
        this.f4105a.put(SyncSamplesBox.a(), SyncSamplesBox.class);
        this.f4105a.put(SampleToChunkBox.a(), SampleToChunkBox.class);
        this.f4105a.put(SampleSizesBox.a(), SampleSizesBox.class);
        this.f4105a.put(ChunkOffsetsBox.a(), ChunkOffsetsBox.class);
        this.f4105a.put("mvex", cls2);
        this.f4105a.put("moof", cls2);
        this.f4105a.put("traf", cls2);
        this.f4105a.put("mfra", cls2);
        this.f4105a.put("skip", cls2);
        this.f4105a.put("meta", cls);
        this.f4105a.put(DataRefBox.b(), DataRefBox.class);
        this.f4105a.put("ipro", cls2);
        this.f4105a.put("sinf", cls2);
        this.f4105a.put(ChunkOffsets64Box.a(), ChunkOffsets64Box.class);
        this.f4105a.put(SoundMediaHeaderBox.a(), SoundMediaHeaderBox.class);
        this.f4105a.put("clip", cls2);
        this.f4105a.put(ClipRegionBox.a(), ClipRegionBox.class);
        this.f4105a.put(LoadSettingsBox.a(), LoadSettingsBox.class);
        this.f4105a.put("tapt", cls2);
        this.f4105a.put("gmhd", cls2);
        this.f4105a.put("tmcd", cls);
        this.f4105a.put("tref", cls2);
        this.f4105a.put(ClearApertureBox.a(), ClearApertureBox.class);
        this.f4105a.put(ProductionApertureBox.a(), ProductionApertureBox.class);
        this.f4105a.put(EncodedPixelBox.a(), EncodedPixelBox.class);
        this.f4105a.put(GenericMediaInfoBox.a(), GenericMediaInfoBox.class);
        this.f4105a.put(TimecodeMediaInfoBox.a(), TimecodeMediaInfoBox.class);
        this.f4105a.put("udta", cls2);
        this.f4105a.put(CompositionOffsetsBox.a(), CompositionOffsetsBox.class);
        this.f4105a.put(NameBox.a(), NameBox.class);
    }

    public static BoxFactory a() {
        return b;
    }
}
