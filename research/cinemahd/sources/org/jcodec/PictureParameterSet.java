package org.jcodec;

import java.nio.ByteBuffer;
import java.util.Arrays;

public class PictureParameterSet {

    /* renamed from: a  reason: collision with root package name */
    public boolean f4123a;
    public int[] b = new int[2];
    public int c;
    public int d;
    public int e;
    public boolean f;
    public int g;
    public int h;
    public boolean i;
    public int j;
    public int k;
    public int l;
    public int m;
    public boolean n;
    public boolean o;
    public boolean p;
    public int[] q;
    public int[] r;
    public int[] s;
    public boolean t;
    public int[] u;
    public PPSExt v;

    public static class PPSExt {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4124a;
        public ScalingMatrix b;
        public int c;
    }

    public static PictureParameterSet b(ByteBuffer byteBuffer) {
        BitReader bitReader = new BitReader(byteBuffer);
        PictureParameterSet pictureParameterSet = new PictureParameterSet();
        pictureParameterSet.d = CAVLCReader.c(bitReader, "PPS: pic_parameter_set_id");
        pictureParameterSet.e = CAVLCReader.c(bitReader, "PPS: seq_parameter_set_id");
        pictureParameterSet.f4123a = CAVLCReader.a(bitReader, "PPS: entropy_coding_mode_flag");
        pictureParameterSet.f = CAVLCReader.a(bitReader, "PPS: pic_order_present_flag");
        pictureParameterSet.g = CAVLCReader.c(bitReader, "PPS: num_slice_groups_minus1");
        if (pictureParameterSet.g > 0) {
            pictureParameterSet.h = CAVLCReader.c(bitReader, "PPS: slice_group_map_type");
            int i2 = pictureParameterSet.g;
            pictureParameterSet.q = new int[(i2 + 1)];
            pictureParameterSet.r = new int[(i2 + 1)];
            pictureParameterSet.s = new int[(i2 + 1)];
            int i3 = pictureParameterSet.h;
            if (i3 == 0) {
                for (int i4 = 0; i4 <= pictureParameterSet.g; i4++) {
                    pictureParameterSet.s[i4] = CAVLCReader.c(bitReader, "PPS: run_length_minus1");
                }
            } else if (i3 == 2) {
                for (int i5 = 0; i5 < pictureParameterSet.g; i5++) {
                    pictureParameterSet.q[i5] = CAVLCReader.c(bitReader, "PPS: top_left");
                    pictureParameterSet.r[i5] = CAVLCReader.c(bitReader, "PPS: bottom_right");
                }
            } else {
                int i6 = 3;
                if (i3 == 3 || i3 == 4 || i3 == 5) {
                    pictureParameterSet.t = CAVLCReader.a(bitReader, "PPS: slice_group_change_direction_flag");
                    pictureParameterSet.c = CAVLCReader.c(bitReader, "PPS: slice_group_change_rate_minus1");
                } else if (i3 == 6) {
                    if (i2 + 1 <= 4) {
                        i6 = i2 + 1 > 2 ? 2 : 1;
                    }
                    int c2 = CAVLCReader.c(bitReader, "PPS: pic_size_in_map_units_minus1");
                    pictureParameterSet.u = new int[(c2 + 1)];
                    for (int i7 = 0; i7 <= c2; i7++) {
                        int[] iArr = pictureParameterSet.u;
                        iArr[i7] = CAVLCReader.b(bitReader, i6, "PPS: slice_group_id [" + i7 + "]f");
                    }
                }
            }
        }
        pictureParameterSet.b = new int[]{CAVLCReader.c(bitReader, "PPS: num_ref_idx_l0_active_minus1"), CAVLCReader.c(bitReader, "PPS: num_ref_idx_l1_active_minus1")};
        pictureParameterSet.i = CAVLCReader.a(bitReader, "PPS: weighted_pred_flag");
        pictureParameterSet.j = CAVLCReader.a(bitReader, 2, "PPS: weighted_bipred_idc");
        pictureParameterSet.k = CAVLCReader.b(bitReader, "PPS: pic_init_qp_minus26");
        pictureParameterSet.l = CAVLCReader.b(bitReader, "PPS: pic_init_qs_minus26");
        pictureParameterSet.m = CAVLCReader.b(bitReader, "PPS: chroma_qp_index_offset");
        pictureParameterSet.n = CAVLCReader.a(bitReader, "PPS: deblocking_filter_control_present_flag");
        pictureParameterSet.o = CAVLCReader.a(bitReader, "PPS: constrained_intra_pred_flag");
        pictureParameterSet.p = CAVLCReader.a(bitReader, "PPS: redundant_pic_cnt_present_flag");
        if (CAVLCReader.a(bitReader)) {
            pictureParameterSet.v = new PPSExt();
            pictureParameterSet.v.f4124a = CAVLCReader.a(bitReader, "PPS: transform_8x8_mode_flag");
            if (CAVLCReader.a(bitReader, "PPS: pic_scaling_matrix_present_flag")) {
                for (int i8 = 0; i8 < ((pictureParameterSet.v.f4124a ? 1 : 0) * true) + 6; i8++) {
                    if (CAVLCReader.a(bitReader, "PPS: pic_scaling_list_present_flag")) {
                        ScalingMatrix scalingMatrix = pictureParameterSet.v.b;
                        scalingMatrix.f4132a = new ScalingList[8];
                        scalingMatrix.b = new ScalingList[8];
                        if (i8 < 6) {
                            scalingMatrix.f4132a[i8] = ScalingList.a(bitReader, 16);
                        } else {
                            scalingMatrix.b[i8 - 6] = ScalingList.a(bitReader, 64);
                        }
                    }
                }
            }
            pictureParameterSet.v.c = CAVLCReader.b(bitReader, "PPS: second_chroma_qp_index_offset");
        }
        return pictureParameterSet;
    }

    public void a(ByteBuffer byteBuffer) {
        BitWriter bitWriter = new BitWriter(byteBuffer);
        CAVLCWriter.b(bitWriter, this.d, "PPS: pic_parameter_set_id");
        CAVLCWriter.b(bitWriter, this.e, "PPS: seq_parameter_set_id");
        CAVLCWriter.a(bitWriter, this.f4123a, "PPS: entropy_coding_mode_flag");
        CAVLCWriter.a(bitWriter, this.f, "PPS: pic_order_present_flag");
        CAVLCWriter.b(bitWriter, this.g, "PPS: num_slice_groups_minus1");
        if (this.g > 0) {
            CAVLCWriter.b(bitWriter, this.h, "PPS: slice_group_map_type");
            int[] iArr = new int[1];
            int[] iArr2 = new int[1];
            int[] iArr3 = new int[1];
            int i2 = this.h;
            if (i2 == 0) {
                for (int i3 = 0; i3 <= this.g; i3++) {
                    CAVLCWriter.b(bitWriter, iArr3[i3], "PPS: ");
                }
            } else if (i2 == 2) {
                for (int i4 = 0; i4 < this.g; i4++) {
                    CAVLCWriter.b(bitWriter, iArr[i4], "PPS: ");
                    CAVLCWriter.b(bitWriter, iArr2[i4], "PPS: ");
                }
            } else {
                int i5 = 3;
                if (i2 == 3 || i2 == 4 || i2 == 5) {
                    CAVLCWriter.a(bitWriter, this.t, "PPS: slice_group_change_direction_flag");
                    CAVLCWriter.b(bitWriter, this.c, "PPS: slice_group_change_rate_minus1");
                } else if (i2 == 6) {
                    int i6 = this.g;
                    if (i6 + 1 <= 4) {
                        i5 = i6 + 1 > 2 ? 2 : 1;
                    }
                    CAVLCWriter.b(bitWriter, this.u.length, "PPS: ");
                    int i7 = 0;
                    while (true) {
                        int[] iArr4 = this.u;
                        if (i7 > iArr4.length) {
                            break;
                        }
                        CAVLCWriter.a(bitWriter, iArr4[i7], i5);
                        i7++;
                    }
                }
            }
        }
        CAVLCWriter.b(bitWriter, this.b[0], "PPS: num_ref_idx_l0_active_minus1");
        CAVLCWriter.b(bitWriter, this.b[1], "PPS: num_ref_idx_l1_active_minus1");
        CAVLCWriter.a(bitWriter, this.i, "PPS: weighted_pred_flag");
        CAVLCWriter.a(bitWriter, (long) this.j, 2, "PPS: weighted_bipred_idc");
        CAVLCWriter.a(bitWriter, this.k, "PPS: pic_init_qp_minus26");
        CAVLCWriter.a(bitWriter, this.l, "PPS: pic_init_qs_minus26");
        CAVLCWriter.a(bitWriter, this.m, "PPS: chroma_qp_index_offset");
        CAVLCWriter.a(bitWriter, this.n, "PPS: deblocking_filter_control_present_flag");
        CAVLCWriter.a(bitWriter, this.o, "PPS: constrained_intra_pred_flag");
        CAVLCWriter.a(bitWriter, this.p, "PPS: redundant_pic_cnt_present_flag");
        PPSExt pPSExt = this.v;
        if (pPSExt != null) {
            CAVLCWriter.a(bitWriter, pPSExt.f4124a, "PPS: transform_8x8_mode_flag");
            CAVLCWriter.a(bitWriter, this.v.b != null, "PPS: scalindMatrix");
            if (this.v.b != null) {
                int i8 = 0;
                while (true) {
                    PPSExt pPSExt2 = this.v;
                    if (i8 >= ((pPSExt2.f4124a ? 1 : 0) * true) + 6) {
                        break;
                    }
                    if (i8 < 6) {
                        CAVLCWriter.a(bitWriter, pPSExt2.b.f4132a[i8] != null, "PPS: ");
                        ScalingList[] scalingListArr = this.v.b.f4132a;
                        if (scalingListArr[i8] != null) {
                            scalingListArr[i8].a(bitWriter);
                        }
                    } else {
                        int i9 = i8 - 6;
                        CAVLCWriter.a(bitWriter, pPSExt2.b.b[i9] != null, "PPS: ");
                        ScalingList[] scalingListArr2 = this.v.b.b;
                        if (scalingListArr2[i9] != null) {
                            scalingListArr2[i9].a(bitWriter);
                        }
                    }
                    i8++;
                }
            }
            CAVLCWriter.a(bitWriter, this.v.c, "PPS: ");
        }
        CAVLCWriter.a(bitWriter);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || PictureParameterSet.class != obj.getClass()) {
            return false;
        }
        PictureParameterSet pictureParameterSet = (PictureParameterSet) obj;
        if (!Arrays.equals(this.r, pictureParameterSet.r) || this.m != pictureParameterSet.m || this.o != pictureParameterSet.o || this.n != pictureParameterSet.n || this.f4123a != pictureParameterSet.f4123a) {
            return false;
        }
        PPSExt pPSExt = this.v;
        if (pPSExt == null) {
            if (pictureParameterSet.v != null) {
                return false;
            }
        } else if (!pPSExt.equals(pictureParameterSet.v)) {
            return false;
        }
        int[] iArr = this.b;
        int i2 = iArr[0];
        int[] iArr2 = pictureParameterSet.b;
        return i2 == iArr2[0] && iArr[1] == iArr2[1] && this.g == pictureParameterSet.g && this.k == pictureParameterSet.k && this.l == pictureParameterSet.l && this.f == pictureParameterSet.f && this.d == pictureParameterSet.d && this.p == pictureParameterSet.p && Arrays.equals(this.s, pictureParameterSet.s) && this.e == pictureParameterSet.e && this.t == pictureParameterSet.t && this.c == pictureParameterSet.c && Arrays.equals(this.u, pictureParameterSet.u) && this.h == pictureParameterSet.h && Arrays.equals(this.q, pictureParameterSet.q) && this.j == pictureParameterSet.j && this.i == pictureParameterSet.i;
    }

    public int hashCode() {
        int i2 = 1231;
        int hashCode = (((((((((Arrays.hashCode(this.r) + 31) * 31) + this.m) * 31) + (this.o ? 1231 : 1237)) * 31) + (this.n ? 1231 : 1237)) * 31) + (this.f4123a ? 1231 : 1237)) * 31;
        PPSExt pPSExt = this.v;
        int hashCode2 = pPSExt == null ? 0 : pPSExt.hashCode();
        int[] iArr = this.b;
        int hashCode3 = (((((((((((((((((((((((((((((((((hashCode + hashCode2) * 31) + iArr[0]) * 31) + iArr[1]) * 31) + this.g) * 31) + this.k) * 31) + this.l) * 31) + (this.f ? 1231 : 1237)) * 31) + this.d) * 31) + (this.p ? 1231 : 1237)) * 31) + Arrays.hashCode(this.s)) * 31) + this.e) * 31) + (this.t ? 1231 : 1237)) * 31) + this.c) * 31) + Arrays.hashCode(this.u)) * 31) + this.h) * 31) + Arrays.hashCode(this.q)) * 31) + this.j) * 31;
        if (!this.i) {
            i2 = 1237;
        }
        return hashCode3 + i2;
    }
}
