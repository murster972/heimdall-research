package org.jcodec;

import com.google.ar.core.ImageMetadata;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import junit.framework.Assert;
import org.jcodec.CompositionOffsetsBox;
import org.jcodec.SampleToChunkBox;
import org.jcodec.TimeToSampleBox;

public class FramesMP4MuxerTrack extends AbstractMP4MuxerTrack {
    private int A;
    private boolean B = true;
    private TimecodeMP4MuxerTrack C;
    private SeekableByteChannel D;
    private List<TimeToSampleBox.TimeToSampleEntry> o = new ArrayList();
    private long p = 0;
    private long q = -1;
    private LongArrayList r = new LongArrayList();
    private IntArrayList s = new IntArrayList();
    private IntArrayList t = new IntArrayList();
    private List<CompositionOffsetsBox.Entry> u = new ArrayList();
    private int v = 0;
    private int w = 0;
    private long x = 0;
    private int y = -1;
    private long z;

    public FramesMP4MuxerTrack(SeekableByteChannel seekableByteChannel, int i, TrackType trackType, int i2) {
        super(i, trackType, i2);
        this.D = seekableByteChannel;
        a(new Rational(1, 1), Unit.FRAME);
    }

    private void b(MP4Packet mP4Packet) throws IOException {
        TimecodeMP4MuxerTrack timecodeMP4MuxerTrack = this.C;
        if (timecodeMP4MuxerTrack != null) {
            timecodeMP4MuxerTrack.a(mP4Packet);
            throw null;
        }
    }

    public void a(MP4Packet mP4Packet) throws IOException {
        if (!this.k) {
            int e = mP4Packet.e() + 1;
            int c = (int) (mP4Packet.c() - this.x);
            int i = this.v;
            if (c != i) {
                int i2 = this.w;
                if (i2 > 0) {
                    this.u.add(new CompositionOffsetsBox.Entry(i2, i));
                }
                this.v = c;
                this.w = 0;
            }
            this.w++;
            this.x += mP4Packet.b();
            int i3 = this.y;
            if (!(i3 == -1 || i3 == e)) {
                a(i3);
                this.i = -1;
            }
            this.g.add(mP4Packet.a());
            if (mP4Packet.d()) {
                this.t.a(this.A + 1);
            } else {
                this.B = false;
            }
            this.A++;
            this.f += mP4Packet.b();
            if (this.q != -1) {
                long b = mP4Packet.b();
                long j = this.q;
                if (b != j) {
                    this.o.add(new TimeToSampleBox.TimeToSampleEntry((int) this.p, (int) j));
                    this.p = 0;
                }
            }
            this.q = mP4Packet.b();
            this.p++;
            this.z += mP4Packet.b();
            b(e);
            b(mP4Packet);
            this.y = e;
            return;
        }
        throw new IllegalStateException("The muxer track has finished muxing");
    }

    public long c() {
        return this.z;
    }

    private void b(int i) throws IOException {
        Unit unit = this.e;
        Assert.assertTrue(unit == Unit.FRAME || unit == Unit.SEC);
        if (this.e == Unit.FRAME && this.g.size() * this.d.a() == this.d.b()) {
            a(i);
        } else if (this.e == Unit.SEC) {
            long j = this.f;
            if (j > 0 && j * ((long) this.d.a()) >= ((long) (this.d.b() * this.c))) {
                a(i);
            }
        }
    }

    private void b(NodeBox nodeBox) {
        if (this.u.size() > 0) {
            this.u.add(new CompositionOffsetsBox.Entry(this.w, this.v));
            int a2 = a(this.u);
            if (a2 > 0) {
                for (CompositionOffsetsBox.Entry entry : this.u) {
                    entry.b -= a2;
                }
            }
            CompositionOffsetsBox.Entry entry2 = this.u.get(0);
            if (entry2.a() > 0) {
                List<Edit> list = this.m;
                if (list == null) {
                    this.m = new ArrayList();
                    this.m.add(new Edit(this.z, (long) entry2.a(), 1.0f));
                } else {
                    for (Edit next : list) {
                        next.a(next.b() + ((long) entry2.a()));
                    }
                }
            }
            nodeBox.a((Box) new CompositionOffsetsBox((CompositionOffsetsBox.Entry[]) this.u.toArray(new CompositionOffsetsBox.Entry[0])));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) throws IOException {
        if (this.g.size() != 0) {
            this.r.a(this.D.position());
            for (ByteBuffer next : this.g) {
                this.s.a(next.remaining());
                this.D.write(next);
            }
            int i2 = this.i;
            if (i2 == -1 || i2 != this.g.size()) {
                this.h.add(new SampleToChunkBox.SampleToChunkEntry((long) (this.j + 1), this.g.size(), i));
            }
            this.i = this.g.size();
            this.j++;
            this.f = 0;
            this.g.clear();
        }
    }

    /* access modifiers changed from: protected */
    public Box a(MovieHeaderBox movieHeaderBox) throws IOException {
        if (!this.k) {
            a(this.y);
            long j = this.p;
            if (j > 0) {
                this.o.add(new TimeToSampleBox.TimeToSampleEntry((int) j, (int) this.q));
            }
            this.k = true;
            TrakBox trakBox = new TrakBox();
            Size a2 = a();
            TrackHeaderBox trackHeaderBox = r3;
            TrackHeaderBox trackHeaderBox2 = new TrackHeaderBox(this.f4098a, (((long) movieHeaderBox.a()) * this.z) / ((long) this.c), (float) a2.b(), (float) a2.a(), new Date().getTime(), new Date().getTime(), 1.0f, 0, 0, new int[]{ImageMetadata.CONTROL_AE_ANTIBANDING_MODE, 0, 0, 0, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE, 0, 0, 0, 1073741824});
            TrackHeaderBox trackHeaderBox3 = trackHeaderBox;
            trackHeaderBox3.a(15);
            trakBox.a((Box) trackHeaderBox3);
            c(trakBox);
            MediaBox mediaBox = new MediaBox();
            trakBox.a((Box) mediaBox);
            mediaBox.a((Box) new MediaHeaderBox(this.c, this.z, 0, new Date().getTime(), new Date().getTime(), 0));
            mediaBox.a((Box) new HandlerBox("mhlr", this.b.a(), "appl", 0, 0));
            MediaInfoBox mediaInfoBox = new MediaInfoBox();
            mediaBox.a((Box) mediaInfoBox);
            a(mediaInfoBox, this.b);
            mediaInfoBox.a((Box) new HandlerBox("dhlr", "url ", "appl", 0, 0));
            a((NodeBox) mediaInfoBox);
            NodeBox nodeBox = new NodeBox(new Header("stbl"));
            mediaInfoBox.a((Box) nodeBox);
            b(nodeBox);
            a(trakBox);
            b(trakBox);
            nodeBox.a((Box) new SampleDescriptionBox((SampleEntry[]) this.l.toArray(new SampleEntry[0])));
            nodeBox.a((Box) new SampleToChunkBox((SampleToChunkBox.SampleToChunkEntry[]) this.h.toArray(new SampleToChunkBox.SampleToChunkEntry[0])));
            nodeBox.a((Box) new SampleSizesBox(this.s.b()));
            nodeBox.a((Box) new TimeToSampleBox((TimeToSampleBox.TimeToSampleEntry[]) this.o.toArray(new TimeToSampleBox.TimeToSampleEntry[0])));
            nodeBox.a((Box) new ChunkOffsets64Box(this.r.a()));
            if (!this.B && this.t.a() > 0) {
                nodeBox.a((Box) new SyncSamplesBox(this.t.b()));
            }
            return trakBox;
        }
        throw new IllegalStateException("The muxer track has finished muxing");
    }

    public static int a(List<CompositionOffsetsBox.Entry> list) {
        int i = Integer.MAX_VALUE;
        for (CompositionOffsetsBox.Entry next : list) {
            if (next.a() < i) {
                i = next.a();
            }
        }
        return i;
    }
}
