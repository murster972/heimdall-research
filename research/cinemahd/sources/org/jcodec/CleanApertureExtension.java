package org.jcodec;

import java.nio.ByteBuffer;

public class CleanApertureExtension extends Box {
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;

    public CleanApertureExtension() {
        super(new Header(a()));
    }

    public static String a() {
        return "clap";
    }

    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putInt(this.i);
        byteBuffer.putInt(this.h);
        byteBuffer.putInt(this.g);
        byteBuffer.putInt(this.f);
        byteBuffer.putInt(this.e);
        byteBuffer.putInt(this.d);
        byteBuffer.putInt(this.c);
        byteBuffer.putInt(this.b);
    }
}
