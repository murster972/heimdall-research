package org.jcodec;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.HashMap;
import java.util.Map;

public class MP4Util {

    /* renamed from: a  reason: collision with root package name */
    private static Map<Codec, String> f4118a = new HashMap();

    static {
        f4118a.put(Codec.MPEG2, "m2v1");
        f4118a.put(Codec.H264, "avc1");
        f4118a.put(Codec.J2K, "mjp2");
    }

    public static void a(SeekableByteChannel seekableByteChannel, MovieBox movieBox) throws IOException {
        long position = seekableByteChannel.position();
        seekableByteChannel.close();
        RandomAccessFile randomAccessFile = new RandomAccessFile(seekableByteChannel.getFileName(), "rws");
        randomAccessFile.setLength(Math.max(seekableByteChannel.n() * 2, 256000));
        MappedByteBuffer map = randomAccessFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, randomAccessFile.length());
        map.position((int) position);
        movieBox.b(map);
        int position2 = map.position();
        FileChannel channel = randomAccessFile.getChannel();
        channel.truncate((long) position2);
        seekableByteChannel.a(channel);
    }
}
