package org.jcodec;

import java.nio.ByteBuffer;

public class ClearApertureBox extends FullBox {
    private float d;
    private float e;

    public ClearApertureBox(Header header) {
        super(header);
    }

    public static String a() {
        return "clef";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt((int) (this.d * 65536.0f));
        byteBuffer.putInt((int) (this.e * 65536.0f));
    }

    public ClearApertureBox() {
        super(new Header(a()));
    }

    public ClearApertureBox(int i, int i2) {
        this();
        this.d = (float) i;
        this.e = (float) i2;
    }

    public ClearApertureBox(Header header, int i, int i2) {
        super(header);
        this.d = (float) i;
        this.e = (float) i2;
    }
}
