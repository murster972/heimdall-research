package org.jcodec;

public class RefPicMarking {

    /* renamed from: a  reason: collision with root package name */
    private Instruction[] f4127a;

    public enum InstrType {
        REMOVE_SHORT,
        REMOVE_LONG,
        CONVERT_INTO_LONG,
        TRUNK_LONG,
        CLEAR,
        MARK_LONG
    }

    public static class Instruction {

        /* renamed from: a  reason: collision with root package name */
        private InstrType f4129a;

        public Instruction(InstrType instrType, int i, int i2) {
            this.f4129a = instrType;
        }

        public InstrType a() {
            return this.f4129a;
        }
    }

    public RefPicMarking(Instruction[] instructionArr) {
        this.f4127a = instructionArr;
    }

    public Instruction[] a() {
        return this.f4127a;
    }
}
