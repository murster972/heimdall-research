package org.jcodec;

import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class SampleDescriptionBox extends NodeBox {
    public static final MyFactory c = new MyFactory();

    public static class MyFactory extends BoxFactory {
        private Map<String, Class<? extends Box>> c = new HashMap();

        public MyFactory() {
            Class<TimecodeSampleEntry> cls = TimecodeSampleEntry.class;
            Class<SampleEntry> cls2 = SampleEntry.class;
            Class<VideoSampleEntry> cls3 = VideoSampleEntry.class;
            this.c.put("ap4h", cls3);
            this.c.put("apch", cls3);
            this.c.put("apcn", cls3);
            this.c.put("apcs", cls3);
            this.c.put("apco", cls3);
            this.c.put("avc1", cls3);
            this.c.put("cvid", cls3);
            this.c.put("jpeg", cls3);
            this.c.put("smc ", cls3);
            this.c.put("rle ", cls3);
            this.c.put("rpza", cls3);
            this.c.put("kpcd", cls3);
            this.c.put("png ", cls3);
            this.c.put("mjpa", cls3);
            this.c.put("mjpb", cls3);
            this.c.put("SVQ1", cls3);
            this.c.put("SVQ3", cls3);
            this.c.put("mp4v", cls3);
            this.c.put("dvc ", cls3);
            this.c.put("dvcp", cls3);
            this.c.put("gif ", cls3);
            this.c.put("h263", cls3);
            this.c.put("tiff", cls3);
            this.c.put("raw ", cls3);
            this.c.put("2vuY", cls3);
            this.c.put("yuv2", cls3);
            this.c.put("v308", cls3);
            this.c.put("v408", cls3);
            this.c.put("v216", cls3);
            this.c.put("v410", cls3);
            this.c.put("v210", cls3);
            this.c.put("m2v1", cls3);
            this.c.put("m1v1", cls3);
            this.c.put("xd5b", cls3);
            this.c.put("dv5n", cls3);
            this.c.put("jp2h", cls3);
            this.c.put("mjp2", cls3);
            this.c.put("tmcd", cls);
            this.c.put("time", cls);
            this.c.put("c608", cls2);
            this.c.put("c708", cls2);
            this.c.put("text", cls2);
        }
    }

    public SampleDescriptionBox(Header header) {
        super(header);
    }

    public static String b() {
        return "stsd";
    }

    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putInt(0);
        byteBuffer.putInt(this.b.size());
        super.a(byteBuffer);
    }

    public SampleDescriptionBox() {
        this(new Header(b()));
    }

    public SampleDescriptionBox(SampleEntry... sampleEntryArr) {
        this();
        for (SampleEntry add : sampleEntryArr) {
            this.b.add(add);
        }
    }
}
