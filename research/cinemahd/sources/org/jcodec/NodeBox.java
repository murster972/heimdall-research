package org.jcodec;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class NodeBox extends Box {
    protected List<Box> b = new LinkedList();

    public NodeBox(Header header) {
        super(header);
        BoxFactory.a();
    }

    public List<Box> a() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void b(StringBuilder sb) {
        StringBuilder sb2 = new StringBuilder();
        Iterator<Box> it2 = this.b.iterator();
        while (it2.hasNext()) {
            it2.next().a(sb2);
            if (it2.hasNext()) {
                sb2.append(",\n");
            }
        }
        sb.append(sb2.toString().replaceAll("([^\n]*)\n", "  $1\n"));
    }

    public void a(Box box) {
        this.b.add(box);
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        for (Box b2 : this.b) {
            b2.b(byteBuffer);
        }
    }

    public void a(MovieHeaderBox movieHeaderBox) {
        this.b.add(0, movieHeaderBox);
    }

    public void a(StringBuilder sb) {
        super.a(sb);
        sb.append(": {\n");
        b(sb);
        sb.append("\n}");
    }
}
