package org.jcodec;

import java.nio.ByteBuffer;

public class ColorExtension extends Box {
    private short b;
    private short c;
    private short d;

    public ColorExtension() {
        super(new Header(a()));
    }

    public static String a() {
        return "colr";
    }

    public void a(ByteBuffer byteBuffer) {
        byteBuffer.put(JCodecUtil.a("nclc"));
        byteBuffer.putShort(this.b);
        byteBuffer.putShort(this.c);
        byteBuffer.putShort(this.d);
    }
}
