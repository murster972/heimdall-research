package org.jcodec;

import java.util.Calendar;
import java.util.TimeZone;

public class TimeUtil {

    /* renamed from: a  reason: collision with root package name */
    public static final long f4139a;

    static {
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        instance.set(1904, 0, 1, 0, 0, 0);
        instance.set(14, 0);
        f4139a = instance.getTimeInMillis();
    }

    public static int a(long j) {
        return (int) ((j - f4139a) / 1000);
    }
}
