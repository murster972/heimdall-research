package org.jcodec;

public class CAVLCReader {
    private CAVLCReader() {
    }

    public static int a(BitReader bitReader, int i, String str) {
        int b = bitReader.b(i);
        Debug.a(str, Integer.valueOf(b));
        return b;
    }

    public static int b(BitReader bitReader) {
        int i = 0;
        while (bitReader.a() == 0 && i < 31) {
            i++;
        }
        if (i <= 0) {
            return 0;
        }
        return (int) (((long) ((1 << i) - 1)) + ((long) bitReader.b(i)));
    }

    public static int c(BitReader bitReader, String str) {
        int b = b(bitReader);
        Debug.a(str, Integer.valueOf(b));
        return b;
    }

    public static boolean a(BitReader bitReader, String str) {
        boolean z = bitReader.a() != 0;
        Debug.a(str, Integer.valueOf(z ? 1 : 0));
        return z;
    }

    public static int b(BitReader bitReader, String str) {
        int a2 = H264Utils.a(b(bitReader));
        Debug.a(str, Integer.valueOf(a2));
        return a2;
    }

    public static boolean a(BitReader bitReader) {
        return (bitReader.c() < 32 && bitReader.a(1) == 1 && (bitReader.a(24) << 9) == 0) ? false : true;
    }

    public static int b(BitReader bitReader, int i, String str) {
        return a(bitReader, i, str);
    }
}
