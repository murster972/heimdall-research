package org.jcodec;

import com.facebook.common.util.ByteConstants;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class AVCMP4Mux {

    /* renamed from: a  reason: collision with root package name */
    private static AvcCBox f4097a;

    public void a(File file, String str, List<Long> list) throws Exception {
        FileChannelWrapper a2 = NIOUtils.a(str, file.length());
        MP4Muxer mP4Muxer = new MP4Muxer((SeekableByteChannel) a2, Brand.MP4);
        a(mP4Muxer.a(TrackType.VIDEO, 1000000), file, list);
        mP4Muxer.c();
        a2.close();
    }

    private static final MappedByteBuffer a(File file) throws IOException {
        RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");
        MappedByteBuffer map = randomAccessFile.getChannel().map(FileChannel.MapMode.READ_WRITE, 0, file.length());
        randomAccessFile.close();
        return map;
    }

    private static void a(FramesMP4MuxerTrack framesMP4MuxerTrack, File file, List<Long> list) throws IOException {
        FramesMP4MuxerTrack framesMP4MuxerTrack2 = framesMP4MuxerTrack;
        List<Long> list2 = list;
        MappedH264ES mappedH264ES = new MappedH264ES(a(file));
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        int i = 0;
        while (true) {
            Packet c = mappedH264ES.c();
            if (c != null) {
                ByteBuffer a2 = c.a();
                H264Utils.a(a2, arrayList, arrayList2);
                H264Utils.a(a2);
                long longValue = list2.get(i).longValue();
                framesMP4MuxerTrack2.a(new MP4Packet(a2, longValue, 1000000, list.size() + -1 > i ? list2.get(i + 1).longValue() - longValue : 250000, (long) i, true, (TapeTimecode) null, longValue, 0));
                i++;
            } else {
                a(framesMP4MuxerTrack2, mappedH264ES.b(), mappedH264ES.a());
                return;
            }
        }
    }

    private static void a(FramesMP4MuxerTrack framesMP4MuxerTrack, SeqParameterSet[] seqParameterSetArr, PictureParameterSet[] pictureParameterSetArr) {
        SeqParameterSet seqParameterSet = seqParameterSetArr[0];
        VideoSampleEntry a2 = MP4Muxer.a("avc1", new Size((seqParameterSet.j + 1) << 4, H264Utils.a(seqParameterSet) << 4), "JCodec");
        f4097a = new AvcCBox(seqParameterSet.n, 0, seqParameterSet.s, a(seqParameterSetArr), a(pictureParameterSetArr));
        a2.a((Box) f4097a);
        framesMP4MuxerTrack.a((SampleEntry) a2);
    }

    private static List<ByteBuffer> a(PictureParameterSet[] pictureParameterSetArr) {
        ArrayList arrayList = new ArrayList();
        for (PictureParameterSet a2 : pictureParameterSetArr) {
            ByteBuffer allocate = ByteBuffer.allocate(ByteConstants.KB);
            a2.a(allocate);
            allocate.flip();
            arrayList.add(allocate);
        }
        return arrayList;
    }

    private static List<ByteBuffer> a(SeqParameterSet[] seqParameterSetArr) {
        ArrayList arrayList = new ArrayList();
        for (SeqParameterSet a2 : seqParameterSetArr) {
            ByteBuffer allocate = ByteBuffer.allocate(ByteConstants.KB);
            a2.a(allocate);
            allocate.flip();
            arrayList.add(allocate);
        }
        return arrayList;
    }
}
