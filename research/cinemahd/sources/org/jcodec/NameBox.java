package org.jcodec;

import com.unity3d.ads.metadata.MediationMetaData;
import java.nio.ByteBuffer;

public class NameBox extends Box {
    private String b;

    public NameBox(String str) {
        this();
        this.b = str;
    }

    public static String a() {
        return MediationMetaData.KEY_NAME;
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.put(JCodecUtil.a(this.b));
        byteBuffer.putInt(0);
    }

    public NameBox() {
        super(new Header(a()));
    }
}
