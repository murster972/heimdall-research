package org.jcodec;

import java.nio.ByteBuffer;
import java.util.List;

public class EditListBox extends FullBox {
    private List<Edit> d;

    public EditListBox(Header header) {
        super(header);
    }

    public static String a() {
        return "elst";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d.size());
        for (Edit next : this.d) {
            byteBuffer.putInt((int) next.a());
            byteBuffer.putInt((int) next.b());
            byteBuffer.putInt((int) (next.c() * 65536.0f));
        }
    }

    public EditListBox() {
        this(new Header(a()));
    }

    public EditListBox(List<Edit> list) {
        this();
        this.d = list;
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        super.a(sb);
        sb.append(": ");
        ToJSON.a(this, sb, "edits");
    }
}
