package org.jcodec;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

public class H264Utils {
    public static int a(int i) {
        int i2 = i & 1;
        return ((i >> 1) + i2) * ((i2 << 1) - 1);
    }

    public static void a(ByteBuffer byteBuffer) {
        ByteBuffer duplicate = byteBuffer.duplicate();
        ByteBuffer duplicate2 = byteBuffer.duplicate();
        int position = duplicate2.position();
        while (true) {
            ByteBuffer c = c(duplicate);
            if (c != null) {
                duplicate2.position(position);
                duplicate2.putInt(c.remaining());
                position += c.remaining() + 4;
            } else {
                return;
            }
        }
    }

    public static final ByteBuffer b(ByteBuffer byteBuffer) {
        if (!byteBuffer.hasRemaining()) {
            return null;
        }
        int position = byteBuffer.position();
        ByteBuffer slice = byteBuffer.slice();
        slice.order(ByteOrder.BIG_ENDIAN);
        byte b = -1;
        while (true) {
            if (!byteBuffer.hasRemaining()) {
                break;
            }
            b = (b << 8) | (byteBuffer.get() & 255);
            if ((16777215 & b) == 1) {
                byteBuffer.position(byteBuffer.position() - (b == 1 ? 4 : 3));
                slice.limit(byteBuffer.position() - position);
            }
        }
        return slice;
    }

    public static ByteBuffer c(ByteBuffer byteBuffer) {
        d(byteBuffer);
        return b(byteBuffer);
    }

    public static final void d(ByteBuffer byteBuffer) {
        if (byteBuffer.hasRemaining()) {
            byte b = -1;
            while (byteBuffer.hasRemaining()) {
                b = (b << 8) | (byteBuffer.get() & 255);
                if ((16777215 & b) == 1) {
                    byteBuffer.position(byteBuffer.position());
                    return;
                }
            }
        }
    }

    public static void a(ByteBuffer byteBuffer, List<ByteBuffer> list, List<ByteBuffer> list2) {
        ByteBuffer c;
        ByteBuffer duplicate = byteBuffer.duplicate();
        while (duplicate.hasRemaining() && (c = c(duplicate)) != null) {
            NALUnitType nALUnitType = NALUnit.a(c).f4120a;
            if (nALUnitType == NALUnitType.PPS) {
                if (list2 != null) {
                    list2.add(c);
                }
                byteBuffer.position(duplicate.position());
            } else if (nALUnitType == NALUnitType.SPS) {
                if (list != null) {
                    list.add(c);
                }
                byteBuffer.position(duplicate.position());
            } else {
                return;
            }
        }
    }

    public static int a(SeqParameterSet seqParameterSet) {
        return (seqParameterSet.i + 1) << (seqParameterSet.z ^ true ? 1 : 0);
    }
}
