package org.jcodec;

import java.io.Closeable;
import java.io.IOException;
import java.nio.channels.ByteChannel;
import java.nio.channels.Channel;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.channels.WritableByteChannel;

public interface SeekableByteChannel extends ByteChannel, Channel, Closeable, ReadableByteChannel, WritableByteChannel {
    void a(FileChannel fileChannel);

    SeekableByteChannel g(long j) throws IOException;

    String getFileName();

    long n();

    long position() throws IOException;
}
