package org.jcodec;

import java.nio.ByteBuffer;

public class LoadSettingsBox extends Box {
    private int b;
    private int c;
    private int d;
    private int e;

    public LoadSettingsBox() {
        super(new Header(a()));
    }

    public static String a() {
        return "load";
    }

    /* access modifiers changed from: protected */
    public void a(ByteBuffer byteBuffer) {
        byteBuffer.putInt(this.b);
        byteBuffer.putInt(this.c);
        byteBuffer.putInt(this.d);
        byteBuffer.putInt(this.e);
    }
}
