package org.jcodec;

import java.nio.ByteBuffer;

public class SampleSizesBox extends FullBox {
    private int d;
    private int e;
    private int[] f;

    public SampleSizesBox(int[] iArr) {
        this();
        this.f = iArr;
    }

    public static String a() {
        return "stsz";
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(this.d);
        if (this.d == 0) {
            byteBuffer.putInt(this.f.length);
            for (int i : this.f) {
                byteBuffer.putInt((int) ((long) i));
            }
            return;
        }
        byteBuffer.putInt(this.e);
    }

    public SampleSizesBox() {
        super(new Header(a()));
    }
}
