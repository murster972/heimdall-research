package org.jcodec;

public class VUIParameters {

    /* renamed from: a  reason: collision with root package name */
    public boolean f4143a;
    public int b;
    public int c;
    public boolean d;
    public boolean e;
    public boolean f;
    public int g;
    public boolean h;
    public boolean i;
    public int j;
    public int k;
    public int l;
    public boolean m;
    public int n;
    public int o;
    public boolean p;
    public int q;
    public int r;
    public boolean s;
    public boolean t;
    public boolean u;
    public HRDParameters v;
    public HRDParameters w;
    public BitstreamRestriction x;
    public AspectRatio y;

    public static class BitstreamRestriction {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4144a;
        public int b;
        public int c;
        public int d;
        public int e;
        public int f;
        public int g;
    }
}
