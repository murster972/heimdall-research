package org.jcodec;

import java.nio.ByteBuffer;

public class Header {

    /* renamed from: a  reason: collision with root package name */
    private String f4113a;
    private long b;
    private boolean c;

    public Header(String str) {
        this.f4113a = str;
    }

    public String a() {
        return this.f4113a;
    }

    public long b() {
        return (this.c || this.b > 4294967296L) ? 16 : 8;
    }

    public void a(int i) {
        this.b = ((long) i) + b();
    }

    public Header(String str, long j) {
        this.b = j;
        this.f4113a = str;
    }

    public void a(ByteBuffer byteBuffer) {
        long j = this.b;
        if (j > 4294967296L) {
            byteBuffer.putInt(1);
        } else {
            byteBuffer.putInt((int) j);
        }
        byteBuffer.put(JCodecUtil.a(this.f4113a));
        long j2 = this.b;
        if (j2 > 4294967296L) {
            byteBuffer.putLong(j2);
        }
    }
}
