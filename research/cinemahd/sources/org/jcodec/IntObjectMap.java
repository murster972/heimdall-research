package org.jcodec;

import java.lang.reflect.Array;

public class IntObjectMap<T> {

    /* renamed from: a  reason: collision with root package name */
    private Object[] f4115a = new Object[128];
    private int b;

    public void a(int i, T t) {
        Object[] objArr = this.f4115a;
        if (objArr.length <= i) {
            Object[] objArr2 = new Object[(objArr.length + 128)];
            System.arraycopy(objArr, 0, objArr2, 0, objArr.length);
            this.f4115a = objArr2;
        }
        if (this.f4115a[i] == null) {
            this.b++;
        }
        this.f4115a[i] = t;
    }

    public T a(int i) {
        T[] tArr = this.f4115a;
        if (i >= tArr.length) {
            return null;
        }
        return tArr[i];
    }

    public T[] a(T[] tArr) {
        T[] tArr2 = (Object[]) Array.newInstance(tArr.getClass().getComponentType(), this.b);
        int i = 0;
        int i2 = 0;
        while (true) {
            T[] tArr3 = this.f4115a;
            if (i >= tArr3.length) {
                return tArr2;
            }
            if (tArr3[i] != null) {
                tArr2[i2] = tArr3[i];
                i2++;
            }
            i++;
        }
    }
}
