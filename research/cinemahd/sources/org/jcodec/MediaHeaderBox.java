package org.jcodec;

import java.nio.ByteBuffer;

public class MediaHeaderBox extends FullBox {
    private long d;
    private long e;
    private int f;
    private long g;
    private int h;
    private int i;

    public MediaHeaderBox(int i2, long j, int i3, long j2, long j3, int i4) {
        super(new Header(a()));
        this.f = i2;
        this.g = j;
        this.h = i3;
        this.d = j2;
        this.e = j3;
        this.i = i4;
    }

    public static String a() {
        return "mdhd";
    }

    public void a(ByteBuffer byteBuffer) {
        super.a(byteBuffer);
        byteBuffer.putInt(TimeUtil.a(this.d));
        byteBuffer.putInt(TimeUtil.a(this.e));
        byteBuffer.putInt(this.f);
        byteBuffer.putInt((int) this.g);
        byteBuffer.putShort((short) this.h);
        byteBuffer.putShort((short) this.i);
    }

    public MediaHeaderBox() {
        super(new Header(a()));
    }

    /* access modifiers changed from: protected */
    public void a(StringBuilder sb) {
        super.a(sb);
        sb.append(": ");
        ToJSON.a(this, sb, "created", "modified", "timescale", "duration", "language", "quality");
    }
}
