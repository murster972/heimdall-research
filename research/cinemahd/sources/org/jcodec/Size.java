package org.jcodec;

public class Size {

    /* renamed from: a  reason: collision with root package name */
    private int f4135a;
    private int b;

    public Size(int i, int i2) {
        this.f4135a = i;
        this.b = i2;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.f4135a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Size.class != obj.getClass()) {
            return false;
        }
        Size size = (Size) obj;
        return this.b == size.b && this.f4135a == size.f4135a;
    }

    public int hashCode() {
        return ((this.b + 31) * 31) + this.f4135a;
    }
}
