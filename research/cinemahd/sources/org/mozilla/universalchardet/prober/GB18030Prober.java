package org.mozilla.universalchardet.prober;

import java.util.Arrays;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.distributionanalysis.GB2312DistributionAnalysis;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;
import org.mozilla.universalchardet.prober.statemachine.GB18030SMModel;
import org.mozilla.universalchardet.prober.statemachine.SMModel;

public class GB18030Prober extends CharsetProber {
    private static final SMModel e = new GB18030SMModel();

    /* renamed from: a  reason: collision with root package name */
    private CodingStateMachine f7020a = new CodingStateMachine(e);
    private CharsetProber.ProbingState b;
    private GB2312DistributionAnalysis c = new GB2312DistributionAnalysis();
    private byte[] d = new byte[2];

    public GB18030Prober() {
        d();
    }

    public String a() {
        return Constants.h;
    }

    public float b() {
        return this.c.a();
    }

    public CharsetProber.ProbingState c() {
        return this.b;
    }

    public void d() {
        this.f7020a.c();
        this.b = CharsetProber.ProbingState.DETECTING;
        this.c.c();
        Arrays.fill(this.d, (byte) 0);
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        int i3 = i2 + i;
        int i4 = i;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int a2 = this.f7020a.a(bArr[i4]);
            if (a2 == 1) {
                this.b = CharsetProber.ProbingState.NOT_ME;
                break;
            } else if (a2 == 2) {
                this.b = CharsetProber.ProbingState.FOUND_IT;
                break;
            } else {
                if (a2 == 0) {
                    int b2 = this.f7020a.b();
                    if (i4 == i) {
                        byte[] bArr2 = this.d;
                        bArr2[1] = bArr[i];
                        this.c.a(bArr2, 0, b2);
                    } else {
                        this.c.a(bArr, i4 - 1, b2);
                    }
                }
                i4++;
            }
        }
        this.d[0] = bArr[i3 - 1];
        if (this.b == CharsetProber.ProbingState.DETECTING && this.c.b() && b() > 0.95f) {
            this.b = CharsetProber.ProbingState.FOUND_IT;
        }
        return this.b;
    }
}
