package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.sequence.SequenceModel;

public class SingleByteCharsetProber extends CharsetProber {

    /* renamed from: a  reason: collision with root package name */
    private CharsetProber.ProbingState f7026a;
    private SequenceModel b;
    private boolean c;
    private short d;
    private int e;
    private int[] f;
    private int g;
    private int h;
    private CharsetProber i;

    public SingleByteCharsetProber(SequenceModel sequenceModel) {
        this.b = sequenceModel;
        this.c = false;
        this.i = null;
        this.f = new int[4];
        d();
    }

    public String a() {
        CharsetProber charsetProber = this.i;
        if (charsetProber == null) {
            return this.b.a();
        }
        return charsetProber.a();
    }

    public float b() {
        int i2 = this.e;
        if (i2 <= 0) {
            return 0.01f;
        }
        float b2 = ((((((float) this.f[3]) * 1.0f) / ((float) i2)) / this.b.b()) * ((float) this.h)) / ((float) this.g);
        if (b2 >= 1.0f) {
            return 0.99f;
        }
        return b2;
    }

    public CharsetProber.ProbingState c() {
        return this.f7026a;
    }

    public void d() {
        this.f7026a = CharsetProber.ProbingState.DETECTING;
        this.d = 255;
        for (int i2 = 0; i2 < 4; i2++) {
            this.f[i2] = 0;
        }
        this.e = 0;
        this.g = 0;
        this.h = 0;
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i2, int i3) {
        int i4 = i3 + i2;
        while (i2 < i4) {
            short a2 = this.b.a(bArr[i2]);
            if (a2 < 250) {
                this.g++;
            }
            if (a2 < 64) {
                this.h++;
                short s = this.d;
                if (s < 64) {
                    this.e++;
                    if (!this.c) {
                        int[] iArr = this.f;
                        byte a3 = this.b.a((s * 64) + a2);
                        iArr[a3] = iArr[a3] + 1;
                    } else {
                        int[] iArr2 = this.f;
                        byte a4 = this.b.a((a2 * 64) + s);
                        iArr2[a4] = iArr2[a4] + 1;
                    }
                }
            }
            this.d = a2;
            i2++;
        }
        if (this.f7026a == CharsetProber.ProbingState.DETECTING && this.e > 1024) {
            float b2 = b();
            if (b2 > 0.95f) {
                this.f7026a = CharsetProber.ProbingState.FOUND_IT;
            } else if (b2 < 0.05f) {
                this.f7026a = CharsetProber.ProbingState.NOT_ME;
            }
        }
        return this.f7026a;
    }

    public SingleByteCharsetProber(SequenceModel sequenceModel, boolean z, CharsetProber charsetProber) {
        this.b = sequenceModel;
        this.c = z;
        this.i = charsetProber;
        this.f = new int[4];
        d();
    }
}
