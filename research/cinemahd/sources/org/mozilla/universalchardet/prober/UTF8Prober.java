package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;
import org.mozilla.universalchardet.prober.statemachine.SMModel;
import org.mozilla.universalchardet.prober.statemachine.UTF8SMModel;

public class UTF8Prober extends CharsetProber {
    private static final SMModel d = new UTF8SMModel();

    /* renamed from: a  reason: collision with root package name */
    private CodingStateMachine f7027a = new CodingStateMachine(d);
    private CharsetProber.ProbingState b;
    private int c = 0;

    public UTF8Prober() {
        d();
    }

    public String a() {
        return Constants.u;
    }

    public float b() {
        float f = 0.99f;
        if (this.c >= 6) {
            return 0.99f;
        }
        for (int i = 0; i < this.c; i++) {
            f *= 0.5f;
        }
        return 1.0f - f;
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        int i3 = i2 + i;
        while (true) {
            if (i >= i3) {
                break;
            }
            int a2 = this.f7027a.a(bArr[i]);
            if (a2 == 1) {
                this.b = CharsetProber.ProbingState.NOT_ME;
                break;
            } else if (a2 == 2) {
                this.b = CharsetProber.ProbingState.FOUND_IT;
                break;
            } else {
                if (a2 == 0 && this.f7027a.b() >= 2) {
                    this.c++;
                }
                i++;
            }
        }
        if (this.b == CharsetProber.ProbingState.DETECTING && b() > 0.95f) {
            this.b = CharsetProber.ProbingState.FOUND_IT;
        }
        return this.b;
    }

    public void d() {
        this.f7027a.c();
        this.c = 0;
        this.b = CharsetProber.ProbingState.DETECTING;
    }

    public CharsetProber.ProbingState c() {
        return this.b;
    }
}
