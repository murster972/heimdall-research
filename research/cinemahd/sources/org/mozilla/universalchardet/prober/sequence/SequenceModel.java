package org.mozilla.universalchardet.prober.sequence;

public abstract class SequenceModel {

    /* renamed from: a  reason: collision with root package name */
    protected short[] f7030a;
    protected byte[] b;
    protected float c;
    protected String d;

    public SequenceModel(short[] sArr, byte[] bArr, float f, boolean z, String str) {
        this.f7030a = sArr;
        this.b = bArr;
        this.c = f;
        this.d = str;
    }

    public short a(byte b2) {
        return this.f7030a[b2 & 255];
    }

    public float b() {
        return this.c;
    }

    public byte a(int i) {
        return this.b[i];
    }

    public String a() {
        return this.d;
    }
}
