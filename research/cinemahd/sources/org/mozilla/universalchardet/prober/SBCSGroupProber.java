package org.mozilla.universalchardet.prober;

import java.nio.ByteBuffer;
import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.sequence.HebrewModel;
import org.mozilla.universalchardet.prober.sequence.Ibm855Model;
import org.mozilla.universalchardet.prober.sequence.Ibm866Model;
import org.mozilla.universalchardet.prober.sequence.Koi8rModel;
import org.mozilla.universalchardet.prober.sequence.Latin5BulgarianModel;
import org.mozilla.universalchardet.prober.sequence.Latin5Model;
import org.mozilla.universalchardet.prober.sequence.Latin7Model;
import org.mozilla.universalchardet.prober.sequence.MacCyrillicModel;
import org.mozilla.universalchardet.prober.sequence.SequenceModel;
import org.mozilla.universalchardet.prober.sequence.Win1251BulgarianModel;
import org.mozilla.universalchardet.prober.sequence.Win1251Model;
import org.mozilla.universalchardet.prober.sequence.Win1253Model;

public class SBCSGroupProber extends CharsetProber {
    private static final SequenceModel f = new Win1251Model();
    private static final SequenceModel g = new Koi8rModel();
    private static final SequenceModel h = new Latin5Model();
    private static final SequenceModel i = new MacCyrillicModel();
    private static final SequenceModel j = new Ibm866Model();
    private static final SequenceModel k = new Ibm855Model();
    private static final SequenceModel l = new Latin7Model();
    private static final SequenceModel m = new Win1253Model();
    private static final SequenceModel n = new Latin5BulgarianModel();
    private static final SequenceModel o = new Win1251BulgarianModel();
    private static final SequenceModel p = new HebrewModel();

    /* renamed from: a  reason: collision with root package name */
    private CharsetProber.ProbingState f7024a;
    private CharsetProber[] b = new CharsetProber[13];
    private boolean[] c = new boolean[13];
    private int d;
    private int e;

    public SBCSGroupProber() {
        this.b[0] = new SingleByteCharsetProber(f);
        this.b[1] = new SingleByteCharsetProber(g);
        this.b[2] = new SingleByteCharsetProber(h);
        this.b[3] = new SingleByteCharsetProber(i);
        this.b[4] = new SingleByteCharsetProber(j);
        this.b[5] = new SingleByteCharsetProber(k);
        this.b[6] = new SingleByteCharsetProber(l);
        this.b[7] = new SingleByteCharsetProber(m);
        this.b[8] = new SingleByteCharsetProber(n);
        this.b[9] = new SingleByteCharsetProber(o);
        HebrewProber hebrewProber = new HebrewProber();
        CharsetProber[] charsetProberArr = this.b;
        charsetProberArr[10] = hebrewProber;
        charsetProberArr[11] = new SingleByteCharsetProber(p, false, hebrewProber);
        this.b[12] = new SingleByteCharsetProber(p, true, hebrewProber);
        CharsetProber[] charsetProberArr2 = this.b;
        hebrewProber.a(charsetProberArr2[11], charsetProberArr2[12]);
        d();
    }

    public String a() {
        if (this.d == -1) {
            b();
            if (this.d == -1) {
                this.d = 0;
            }
        }
        return this.b[this.d].a();
    }

    public float b() {
        CharsetProber.ProbingState probingState = this.f7024a;
        if (probingState == CharsetProber.ProbingState.FOUND_IT) {
            return 0.99f;
        }
        if (probingState == CharsetProber.ProbingState.NOT_ME) {
            return 0.01f;
        }
        int i2 = 0;
        float f2 = 0.0f;
        while (true) {
            CharsetProber[] charsetProberArr = this.b;
            if (i2 >= charsetProberArr.length) {
                return f2;
            }
            if (this.c[i2]) {
                float b2 = charsetProberArr[i2].b();
                if (f2 < b2) {
                    this.d = i2;
                    f2 = b2;
                }
            }
            i2++;
        }
    }

    public CharsetProber.ProbingState c() {
        return this.f7024a;
    }

    public void d() {
        int i2 = 0;
        this.e = 0;
        while (true) {
            CharsetProber[] charsetProberArr = this.b;
            if (i2 < charsetProberArr.length) {
                charsetProberArr[i2].d();
                this.c[i2] = true;
                this.e++;
                i2++;
            } else {
                this.d = -1;
                this.f7024a = CharsetProber.ProbingState.DETECTING;
                return;
            }
        }
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i2, int i3) {
        ByteBuffer b2 = b(bArr, i2, i3);
        if (b2.position() != 0) {
            int i4 = 0;
            while (true) {
                CharsetProber[] charsetProberArr = this.b;
                if (i4 >= charsetProberArr.length) {
                    break;
                }
                if (this.c[i4]) {
                    CharsetProber.ProbingState c2 = charsetProberArr[i4].c(b2.array(), 0, b2.position());
                    CharsetProber.ProbingState probingState = CharsetProber.ProbingState.FOUND_IT;
                    if (c2 == probingState) {
                        this.d = i4;
                        this.f7024a = probingState;
                        break;
                    }
                    CharsetProber.ProbingState probingState2 = CharsetProber.ProbingState.NOT_ME;
                    if (c2 == probingState2) {
                        this.c[i4] = false;
                        this.e--;
                        if (this.e <= 0) {
                            this.f7024a = probingState2;
                            break;
                        }
                    } else {
                        continue;
                    }
                }
                i4++;
            }
        }
        return this.f7024a;
    }
}
