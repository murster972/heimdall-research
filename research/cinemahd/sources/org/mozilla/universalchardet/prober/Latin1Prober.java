package org.mozilla.universalchardet.prober;

import java.nio.ByteBuffer;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.CharsetProber;

public class Latin1Prober extends CharsetProber {
    private static final byte[] d = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 1, 1, 1, 1, 0, 1, 7, 1, 1, 1, 1, 1, 1, 5, 1, 5, 0, 5, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 7, 1, 7, 0, 7, 5, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 4, 4, 4, 4, 4, 4, 5, 5, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 4, 4, 4, 4, 4, 1, 4, 4, 4, 4, 4, 5, 5, 5, 6, 6, 6, 6, 6, 6, 7, 7, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 6, 6, 6, 6, 6, 1, 6, 6, 6, 6, 6, 7, 7, 7};
    private static final byte[] e = {0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 3, 3, 3, 3, 0, 3, 3, 3, 1, 1, 3, 3, 0, 3, 3, 3, 1, 2, 1, 2, 0, 3, 3, 3, 3, 3, 3, 3, 0, 3, 1, 3, 1, 1, 1, 3, 0, 3, 1, 3, 1, 1, 3, 3};

    /* renamed from: a  reason: collision with root package name */
    private CharsetProber.ProbingState f7022a;
    private byte b;
    private int[] c = new int[4];

    public Latin1Prober() {
        d();
    }

    public String a() {
        return Constants.r;
    }

    public float b() {
        int[] iArr;
        float f;
        if (this.f7022a == CharsetProber.ProbingState.NOT_ME) {
            return 0.01f;
        }
        int i = 0;
        int i2 = 0;
        while (true) {
            iArr = this.c;
            if (i >= iArr.length) {
                break;
            }
            i2 += iArr[i];
            i++;
        }
        float f2 = 0.0f;
        if (i2 <= 0) {
            f = 0.0f;
        } else {
            float f3 = (float) i2;
            f = ((((float) iArr[3]) * 1.0f) / f3) - ((((float) iArr[1]) * 20.0f) / f3);
        }
        if (f >= 0.0f) {
            f2 = f;
        }
        return f2 * 0.5f;
    }

    public CharsetProber.ProbingState c() {
        return this.f7022a;
    }

    public void d() {
        this.f7022a = CharsetProber.ProbingState.DETECTING;
        this.b = 1;
        int i = 0;
        while (true) {
            int[] iArr = this.c;
            if (i < iArr.length) {
                iArr[i] = 0;
                i++;
            } else {
                return;
            }
        }
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        ByteBuffer a2 = a(bArr, i, i2);
        byte[] array = a2.array();
        int position = a2.position();
        int i3 = 0;
        while (true) {
            if (i3 >= position) {
                break;
            }
            byte b2 = d[array[i3] & 255];
            byte b3 = e[(this.b * 8) + b2];
            if (b3 == 0) {
                this.f7022a = CharsetProber.ProbingState.NOT_ME;
                break;
            }
            int[] iArr = this.c;
            iArr[b3] = iArr[b3] + 1;
            this.b = b2;
            i3++;
        }
        return this.f7022a;
    }
}
