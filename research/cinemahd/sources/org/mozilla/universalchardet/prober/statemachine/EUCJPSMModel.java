package org.mozilla.universalchardet.prober.statemachine;

import org.mozilla.universalchardet.Constants;

public class EUCJPSMModel extends SMModel {
    private static int[] f = {PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 5, 5), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 5, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(5, 5, 5, 5, 5, 5, 1, 3), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(5, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 5)};
    private static int[] g = {PkgInt.a(3, 4, 3, 5, 0, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 2, 2, 2, 2), PkgInt.a(2, 2, 0, 1, 0, 1, 1, 1), PkgInt.a(1, 1, 0, 1, 1, 1, 3, 1), PkgInt.a(3, 1, 1, 1, 0, 0, 0, 0)};
    private static int[] h = {2, 2, 2, 3, 1, 0};

    public EUCJPSMModel() {
        super(new PkgInt(3, 7, 2, 15, f), 6, new PkgInt(3, 7, 2, 15, g), h, Constants.i);
    }
}
