package org.mozilla.universalchardet.prober.statemachine;

public abstract class SMModel {

    /* renamed from: a  reason: collision with root package name */
    protected PkgInt f7033a;
    protected int b;
    protected PkgInt c;
    protected int[] d;
    protected String e;

    public SMModel(PkgInt pkgInt, int i, PkgInt pkgInt2, int[] iArr, String str) {
        this.f7033a = pkgInt;
        this.b = i;
        this.c = pkgInt2;
        this.d = iArr;
        this.e = str;
    }

    public int a(byte b2) {
        return this.f7033a.a(b2 & 255);
    }

    public int a(int i, int i2) {
        return this.c.a((i2 * this.b) + i);
    }

    public int a(int i) {
        return this.d[i];
    }

    public String a() {
        return this.e;
    }
}
