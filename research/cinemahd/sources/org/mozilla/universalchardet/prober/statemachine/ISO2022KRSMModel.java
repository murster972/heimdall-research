package org.mozilla.universalchardet.prober.statemachine;

import org.mozilla.universalchardet.Constants;

public class ISO2022KRSMModel extends SMModel {
    private static int[] f = {PkgInt.a(2, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 1, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 3, 0, 0, 0), PkgInt.a(0, 4, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 5, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2)};
    private static int[] g = {PkgInt.a(0, 3, 1, 0, 0, 0, 1, 1), PkgInt.a(1, 1, 1, 1, 2, 2, 2, 2), PkgInt.a(2, 2, 1, 1, 1, 4, 1, 1), PkgInt.a(1, 1, 1, 1, 5, 1, 1, 1), PkgInt.a(1, 1, 1, 2, 0, 0, 0, 0)};
    private static int[] h = {0, 0, 0, 0, 0, 0};

    public ISO2022KRSMModel() {
        super(new PkgInt(3, 7, 2, 15, f), 6, new PkgInt(3, 7, 2, 15, g), h, Constants.c);
    }
}
