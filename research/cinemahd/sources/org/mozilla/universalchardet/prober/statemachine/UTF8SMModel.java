package org.mozilla.universalchardet.prober.statemachine;

import org.mozilla.universalchardet.Constants;

public class UTF8SMModel extends SMModel {
    private static int[] f = {PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 0, 0), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 0, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(2, 2, 2, 2, 3, 3, 3, 3), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(5, 5, 5, 5, 5, 5, 5, 5), PkgInt.a(0, 0, 6, 6, 6, 6, 6, 6), PkgInt.a(6, 6, 6, 6, 6, 6, 6, 6), PkgInt.a(6, 6, 6, 6, 6, 6, 6, 6), PkgInt.a(6, 6, 6, 6, 6, 6, 6, 6), PkgInt.a(7, 8, 8, 8, 8, 8, 8, 8), PkgInt.a(8, 8, 8, 8, 8, 9, 8, 8), PkgInt.a(10, 11, 11, 11, 11, 11, 11, 11), PkgInt.a(12, 13, 13, 13, 14, 15, 0, 0)};
    private static int[] g = {PkgInt.a(1, 0, 1, 1, 1, 1, 12, 10), PkgInt.a(9, 11, 8, 7, 6, 5, 4, 3), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(1, 1, 5, 5, 5, 5, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 5, 5, 5, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 7, 7, 7, 7, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 7, 7, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 9, 9, 9, 9, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 9, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 12, 12, 12, 12, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 12, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 12, 12, 12, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 0, 0, 0, 0, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1)};
    private static int[] h = {0, 1, 0, 0, 0, 0, 2, 3, 3, 3, 4, 4, 5, 5, 6, 6};

    public UTF8SMModel() {
        super(new PkgInt(3, 7, 2, 15, f), 16, new PkgInt(3, 7, 2, 15, g), h, Constants.u);
    }
}
