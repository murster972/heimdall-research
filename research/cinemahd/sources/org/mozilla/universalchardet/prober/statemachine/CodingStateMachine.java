package org.mozilla.universalchardet.prober.statemachine;

public class CodingStateMachine {

    /* renamed from: a  reason: collision with root package name */
    protected SMModel f7031a;
    protected int b = 0;
    protected int c;
    protected int d;

    public CodingStateMachine(SMModel sMModel) {
        this.f7031a = sMModel;
    }

    public int a(byte b2) {
        int a2 = this.f7031a.a(b2);
        if (this.b == 0) {
            this.d = 0;
            this.c = this.f7031a.a(a2);
        }
        this.b = this.f7031a.a(a2, this.b);
        this.d++;
        return this.b;
    }

    public int b() {
        return this.c;
    }

    public void c() {
        this.b = 0;
    }

    public String a() {
        return this.f7031a.a();
    }
}
