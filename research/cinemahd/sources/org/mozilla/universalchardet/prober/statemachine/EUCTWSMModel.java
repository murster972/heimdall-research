package org.mozilla.universalchardet.prober.statemachine;

import org.mozilla.universalchardet.Constants;

public class EUCTWSMModel extends SMModel {
    private static int[] f = {PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 0, 0), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 0, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 6, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 0, 0, 0, 0, 0, 0, 0), PkgInt.a(0, 3, 4, 4, 4, 4, 4, 4), PkgInt.a(5, 5, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 3, 1, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 0)};
    private static int[] g = {PkgInt.a(1, 1, 0, 3, 3, 3, 4, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 1, 0, 1), PkgInt.a(0, 0, 0, 1, 1, 1, 1, 1), PkgInt.a(5, 1, 1, 1, 0, 1, 0, 0), PkgInt.a(0, 1, 0, 0, 0, 0, 0, 0)};
    private static int[] h = {0, 0, 1, 2, 2, 2, 3};

    public EUCTWSMModel() {
        super(new PkgInt(3, 7, 2, 15, f), 7, new PkgInt(3, 7, 2, 15, g), h, Constants.k);
    }
}
