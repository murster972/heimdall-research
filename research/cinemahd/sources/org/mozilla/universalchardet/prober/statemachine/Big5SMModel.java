package org.mozilla.universalchardet.prober.statemachine;

import org.mozilla.universalchardet.Constants;

public class Big5SMModel extends SMModel {
    private static int[] f = {PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 0, 0), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 0, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(1, 1, 1, 1, 1, 1, 1, 1), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 2), PkgInt.a(2, 2, 2, 2, 2, 2, 2, 1), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 4, 4, 4, 4, 4, 4, 4), PkgInt.a(4, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 3), PkgInt.a(3, 3, 3, 3, 3, 3, 3, 0)};
    private static int[] g = {PkgInt.a(1, 0, 0, 3, 1, 1, 1, 1), PkgInt.a(1, 1, 2, 2, 2, 2, 2, 1), PkgInt.a(1, 0, 0, 0, 0, 0, 0, 0)};
    private static int[] h = {0, 1, 1, 2, 0};

    public Big5SMModel() {
        super(new PkgInt(3, 7, 2, 15, f), 5, new PkgInt(3, 7, 2, 15, g), h, Constants.g);
    }
}
