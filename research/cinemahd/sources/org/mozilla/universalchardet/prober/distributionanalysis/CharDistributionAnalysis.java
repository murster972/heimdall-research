package org.mozilla.universalchardet.prober.distributionanalysis;

public abstract class CharDistributionAnalysis {

    /* renamed from: a  reason: collision with root package name */
    private int f7029a;
    private int b;
    protected int[] c;
    protected float d;

    public CharDistributionAnalysis() {
        c();
    }

    /* access modifiers changed from: protected */
    public abstract int a(byte[] bArr, int i);

    public void a(byte[] bArr, int i, int i2) {
        int a2 = i2 == 2 ? a(bArr, i) : -1;
        if (a2 >= 0) {
            this.b++;
            int[] iArr = this.c;
            if (a2 < iArr.length && 512 > iArr[a2]) {
                this.f7029a++;
            }
        }
    }

    public boolean b() {
        return this.b > 1024;
    }

    public void c() {
        this.b = 0;
        this.f7029a = 0;
    }

    public float a() {
        int i;
        int i2 = this.b;
        if (i2 <= 0 || (i = this.f7029a) <= 4) {
            return 0.01f;
        }
        if (i2 != i) {
            float f = ((float) (i / (i2 - i))) * this.d;
            if (f < 0.99f) {
                return f;
            }
        }
        return 0.99f;
    }
}
