package org.mozilla.universalchardet.prober.distributionanalysis;

public class EUCJPDistributionAnalysis extends JISDistributionAnalysis {
    /* access modifiers changed from: protected */
    public int a(byte[] bArr, int i) {
        byte b = bArr[i] & 255;
        if (b < 161) {
            return -1;
        }
        return (((b - 161) * 94) + (bArr[i + 1] & 255)) - 161;
    }
}
