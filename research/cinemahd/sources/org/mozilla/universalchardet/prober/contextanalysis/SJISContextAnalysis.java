package org.mozilla.universalchardet.prober.contextanalysis;

public class SJISContextAnalysis extends JapaneseContextAnalysis {
    /* access modifiers changed from: protected */
    public int a(byte[] bArr, int i) {
        byte b;
        if ((bArr[i] & 255) != 130 || (b = bArr[i + 1] & 255) < 159 || b > 241) {
            return -1;
        }
        return b - 159;
    }
}
