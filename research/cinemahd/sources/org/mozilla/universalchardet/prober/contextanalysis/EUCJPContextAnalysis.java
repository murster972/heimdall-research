package org.mozilla.universalchardet.prober.contextanalysis;

public class EUCJPContextAnalysis extends JapaneseContextAnalysis {
    /* access modifiers changed from: protected */
    public int a(byte[] bArr, int i) {
        byte b;
        if ((bArr[i] & 255) != 164 || (b = bArr[i + 1] & 255) < 161 || b > 243) {
            return -1;
        }
        return b - 161;
    }
}
