package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;
import org.mozilla.universalchardet.prober.statemachine.HZSMModel;
import org.mozilla.universalchardet.prober.statemachine.ISO2022CNSMModel;
import org.mozilla.universalchardet.prober.statemachine.ISO2022JPSMModel;
import org.mozilla.universalchardet.prober.statemachine.ISO2022KRSMModel;

public class EscCharsetProber extends CharsetProber {
    private static final HZSMModel e = new HZSMModel();
    private static final ISO2022CNSMModel f = new ISO2022CNSMModel();
    private static final ISO2022JPSMModel g = new ISO2022JPSMModel();
    private static final ISO2022KRSMModel h = new ISO2022KRSMModel();

    /* renamed from: a  reason: collision with root package name */
    private CodingStateMachine[] f7019a = new CodingStateMachine[4];
    private int b;
    private CharsetProber.ProbingState c;
    private String d;

    public EscCharsetProber() {
        this.f7019a[0] = new CodingStateMachine(e);
        this.f7019a[1] = new CodingStateMachine(f);
        this.f7019a[2] = new CodingStateMachine(g);
        this.f7019a[3] = new CodingStateMachine(h);
        d();
    }

    public String a() {
        return this.d;
    }

    public float b() {
        return 0.99f;
    }

    public CharsetProber.ProbingState c() {
        return this.c;
    }

    public void d() {
        this.c = CharsetProber.ProbingState.DETECTING;
        int i = 0;
        while (true) {
            CodingStateMachine[] codingStateMachineArr = this.f7019a;
            if (i < codingStateMachineArr.length) {
                codingStateMachineArr[i].c();
                i++;
            } else {
                this.b = codingStateMachineArr.length;
                this.d = null;
                return;
            }
        }
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        int i3 = i2 + i;
        while (i < i3 && this.c == CharsetProber.ProbingState.DETECTING) {
            for (int i4 = this.b - 1; i4 >= 0; i4--) {
                int a2 = this.f7019a[i4].a(bArr[i]);
                if (a2 == 1) {
                    this.b--;
                    int i5 = this.b;
                    if (i5 <= 0) {
                        this.c = CharsetProber.ProbingState.NOT_ME;
                        return this.c;
                    } else if (i4 != i5) {
                        CodingStateMachine[] codingStateMachineArr = this.f7019a;
                        CodingStateMachine codingStateMachine = codingStateMachineArr[i5];
                        codingStateMachineArr[i5] = codingStateMachineArr[i4];
                        codingStateMachineArr[i4] = codingStateMachine;
                    }
                } else if (a2 == 2) {
                    this.c = CharsetProber.ProbingState.FOUND_IT;
                    this.d = this.f7019a[i4].a();
                    return this.c;
                }
            }
            i++;
        }
        return this.c;
    }
}
