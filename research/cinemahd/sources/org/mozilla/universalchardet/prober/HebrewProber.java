package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.CharsetProber;

public class HebrewProber extends CharsetProber {

    /* renamed from: a  reason: collision with root package name */
    private int f7021a;
    private int b;
    private byte c;
    private byte d;
    private CharsetProber e = null;
    private CharsetProber f = null;

    public HebrewProber() {
        d();
    }

    protected static boolean a(byte b2) {
        byte b3 = b2 & 255;
        return b3 == 234 || b3 == 237 || b3 == 239 || b3 == 243 || b3 == 245;
    }

    protected static boolean b(byte b2) {
        byte b3 = b2 & 255;
        return b3 == 235 || b3 == 238 || b3 == 240 || b3 == 244;
    }

    public void a(CharsetProber charsetProber, CharsetProber charsetProber2) {
        this.e = charsetProber;
        this.f = charsetProber2;
    }

    public float b() {
        return 0.0f;
    }

    public CharsetProber.ProbingState c() {
        CharsetProber.ProbingState probingState;
        if (this.e.c() == CharsetProber.ProbingState.NOT_ME && this.f.c() == (probingState = CharsetProber.ProbingState.NOT_ME)) {
            return probingState;
        }
        return CharsetProber.ProbingState.DETECTING;
    }

    public void d() {
        this.f7021a = 0;
        this.b = 0;
        this.c = 32;
        this.d = 32;
    }

    public String a() {
        int i = this.f7021a - this.b;
        if (i >= 5) {
            return Constants.t;
        }
        if (i <= -5) {
            return Constants.f;
        }
        float b2 = this.e.b() - this.f.b();
        if (b2 > 0.01f) {
            return Constants.t;
        }
        if (b2 < -0.01f) {
            return Constants.f;
        }
        if (i < 0) {
            return Constants.f;
        }
        return Constants.t;
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        CharsetProber.ProbingState c2 = c();
        CharsetProber.ProbingState probingState = CharsetProber.ProbingState.NOT_ME;
        if (c2 == probingState) {
            return probingState;
        }
        int i3 = i2 + i;
        while (i < i3) {
            byte b2 = bArr[i];
            if (b2 == 32) {
                if (this.d != 32) {
                    if (a(this.c)) {
                        this.f7021a++;
                    } else if (b(this.c)) {
                        this.b++;
                    }
                }
            } else if (this.d == 32 && a(this.c) && b2 != 32) {
                this.b++;
            }
            this.d = this.c;
            this.c = b2;
            i++;
        }
        return CharsetProber.ProbingState.DETECTING;
    }
}
