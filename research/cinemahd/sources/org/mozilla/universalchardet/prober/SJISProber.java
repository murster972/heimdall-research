package org.mozilla.universalchardet.prober;

import java.util.Arrays;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.contextanalysis.SJISContextAnalysis;
import org.mozilla.universalchardet.prober.distributionanalysis.SJISDistributionAnalysis;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;
import org.mozilla.universalchardet.prober.statemachine.SJISSMModel;
import org.mozilla.universalchardet.prober.statemachine.SMModel;

public class SJISProber extends CharsetProber {
    private static final SMModel f = new SJISSMModel();

    /* renamed from: a  reason: collision with root package name */
    private CodingStateMachine f7025a = new CodingStateMachine(f);
    private CharsetProber.ProbingState b;
    private SJISContextAnalysis c = new SJISContextAnalysis();
    private SJISDistributionAnalysis d = new SJISDistributionAnalysis();
    private byte[] e = new byte[2];

    public SJISProber() {
        d();
    }

    public String a() {
        return Constants.l;
    }

    public float b() {
        return Math.max(this.c.a(), this.d.a());
    }

    public CharsetProber.ProbingState c() {
        return this.b;
    }

    public void d() {
        this.f7025a.c();
        this.b = CharsetProber.ProbingState.DETECTING;
        this.c.c();
        this.d.c();
        Arrays.fill(this.e, (byte) 0);
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        int i3 = i2 + i;
        int i4 = i;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int a2 = this.f7025a.a(bArr[i4]);
            if (a2 == 1) {
                this.b = CharsetProber.ProbingState.NOT_ME;
                break;
            } else if (a2 == 2) {
                this.b = CharsetProber.ProbingState.FOUND_IT;
                break;
            } else {
                if (a2 == 0) {
                    int b2 = this.f7025a.b();
                    if (i4 == i) {
                        byte[] bArr2 = this.e;
                        bArr2[1] = bArr[i];
                        this.c.a(bArr2, 2 - b2, b2);
                        this.d.a(this.e, 0, b2);
                    } else {
                        this.c.a(bArr, (i4 + 1) - b2, b2);
                        this.d.a(bArr, i4 - 1, b2);
                    }
                }
                i4++;
            }
        }
        this.e[0] = bArr[i3 - 1];
        if (this.b == CharsetProber.ProbingState.DETECTING && this.c.b() && b() > 0.95f) {
            this.b = CharsetProber.ProbingState.FOUND_IT;
        }
        return this.b;
    }
}
