package org.mozilla.universalchardet.prober;

import org.mozilla.universalchardet.prober.CharsetProber;

public class MBCSGroupProber extends CharsetProber {

    /* renamed from: a  reason: collision with root package name */
    private CharsetProber.ProbingState f7023a;
    private CharsetProber[] b = new CharsetProber[7];
    private boolean[] c = new boolean[7];
    private int d;
    private int e;

    public MBCSGroupProber() {
        this.b[0] = new UTF8Prober();
        this.b[1] = new SJISProber();
        this.b[2] = new EUCJPProber();
        this.b[3] = new GB18030Prober();
        this.b[4] = new EUCKRProber();
        this.b[5] = new Big5Prober();
        this.b[6] = new EUCTWProber();
        d();
    }

    public String a() {
        if (this.d == -1) {
            b();
            if (this.d == -1) {
                this.d = 0;
            }
        }
        return this.b[this.d].a();
    }

    public float b() {
        CharsetProber.ProbingState probingState = this.f7023a;
        if (probingState == CharsetProber.ProbingState.FOUND_IT) {
            return 0.99f;
        }
        if (probingState == CharsetProber.ProbingState.NOT_ME) {
            return 0.01f;
        }
        int i = 0;
        float f = 0.0f;
        while (true) {
            CharsetProber[] charsetProberArr = this.b;
            if (i >= charsetProberArr.length) {
                return f;
            }
            if (this.c[i]) {
                float b2 = charsetProberArr[i].b();
                if (f < b2) {
                    this.d = i;
                    f = b2;
                }
            }
            i++;
        }
    }

    public CharsetProber.ProbingState c() {
        return this.f7023a;
    }

    public void d() {
        int i = 0;
        this.e = 0;
        while (true) {
            CharsetProber[] charsetProberArr = this.b;
            if (i < charsetProberArr.length) {
                charsetProberArr[i].d();
                this.c[i] = true;
                this.e++;
                i++;
            } else {
                this.d = -1;
                this.f7023a = CharsetProber.ProbingState.DETECTING;
                return;
            }
        }
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        byte[] bArr2 = new byte[i2];
        int i3 = i2 + i;
        boolean z = true;
        int i4 = 0;
        while (i < i3) {
            if ((bArr[i] & 128) != 0) {
                bArr2[i4] = bArr[i];
                i4++;
                z = true;
            } else if (z) {
                bArr2[i4] = bArr[i];
                i4++;
                z = false;
            }
            i++;
        }
        int i5 = 0;
        while (true) {
            CharsetProber[] charsetProberArr = this.b;
            if (i5 >= charsetProberArr.length) {
                break;
            }
            if (this.c[i5]) {
                CharsetProber.ProbingState c2 = charsetProberArr[i5].c(bArr2, 0, i4);
                CharsetProber.ProbingState probingState = CharsetProber.ProbingState.FOUND_IT;
                if (c2 == probingState) {
                    this.d = i5;
                    this.f7023a = probingState;
                    break;
                }
                CharsetProber.ProbingState probingState2 = CharsetProber.ProbingState.NOT_ME;
                if (c2 == probingState2) {
                    this.c[i5] = false;
                    this.e--;
                    if (this.e <= 0) {
                        this.f7023a = probingState2;
                        break;
                    }
                } else {
                    continue;
                }
            }
            i5++;
        }
        return this.f7023a;
    }
}
