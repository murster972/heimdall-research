package org.mozilla.universalchardet.prober;

import java.util.Arrays;
import org.mozilla.universalchardet.Constants;
import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.contextanalysis.EUCJPContextAnalysis;
import org.mozilla.universalchardet.prober.distributionanalysis.EUCJPDistributionAnalysis;
import org.mozilla.universalchardet.prober.statemachine.CodingStateMachine;
import org.mozilla.universalchardet.prober.statemachine.EUCJPSMModel;
import org.mozilla.universalchardet.prober.statemachine.SMModel;

public class EUCJPProber extends CharsetProber {
    private static final SMModel f = new EUCJPSMModel();

    /* renamed from: a  reason: collision with root package name */
    private CodingStateMachine f7016a = new CodingStateMachine(f);
    private CharsetProber.ProbingState b;
    private EUCJPContextAnalysis c = new EUCJPContextAnalysis();
    private EUCJPDistributionAnalysis d = new EUCJPDistributionAnalysis();
    private byte[] e = new byte[2];

    public EUCJPProber() {
        d();
    }

    public String a() {
        return Constants.i;
    }

    public float b() {
        return Math.max(this.c.a(), this.d.a());
    }

    public CharsetProber.ProbingState c() {
        return this.b;
    }

    public void d() {
        this.f7016a.c();
        this.b = CharsetProber.ProbingState.DETECTING;
        this.c.c();
        this.d.c();
        Arrays.fill(this.e, (byte) 0);
    }

    public CharsetProber.ProbingState c(byte[] bArr, int i, int i2) {
        int i3 = i2 + i;
        int i4 = i;
        while (true) {
            if (i4 >= i3) {
                break;
            }
            int a2 = this.f7016a.a(bArr[i4]);
            if (a2 == 1) {
                this.b = CharsetProber.ProbingState.NOT_ME;
                break;
            } else if (a2 == 2) {
                this.b = CharsetProber.ProbingState.FOUND_IT;
                break;
            } else {
                if (a2 == 0) {
                    int b2 = this.f7016a.b();
                    if (i4 == i) {
                        byte[] bArr2 = this.e;
                        bArr2[1] = bArr[i];
                        this.c.a(bArr2, 0, b2);
                        this.d.a(this.e, 0, b2);
                    } else {
                        int i5 = i4 - 1;
                        this.c.a(bArr, i5, b2);
                        this.d.a(bArr, i5, b2);
                    }
                }
                i4++;
            }
        }
        this.e[0] = bArr[i3 - 1];
        if (this.b == CharsetProber.ProbingState.DETECTING && this.c.b() && b() > 0.95f) {
            this.b = CharsetProber.ProbingState.FOUND_IT;
        }
        return this.b;
    }
}
