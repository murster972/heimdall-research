package org.mozilla.universalchardet;

import org.mozilla.universalchardet.prober.CharsetProber;
import org.mozilla.universalchardet.prober.EscCharsetProber;
import org.mozilla.universalchardet.prober.Latin1Prober;
import org.mozilla.universalchardet.prober.MBCSGroupProber;
import org.mozilla.universalchardet.prober.SBCSGroupProber;

public class UniversalDetector {

    /* renamed from: a  reason: collision with root package name */
    private InputState f7012a;
    private boolean b;
    private boolean c;
    private boolean d;
    private byte e;
    private String f;
    private CharsetProber[] g = new CharsetProber[3];
    private CharsetProber h = null;
    private CharsetListener i;

    public enum InputState {
        PURE_ASCII,
        ESC_ASCII,
        HIGHBYTE
    }

    public UniversalDetector(CharsetListener charsetListener) {
        this.i = charsetListener;
        int i2 = 0;
        while (true) {
            CharsetProber[] charsetProberArr = this.g;
            if (i2 < charsetProberArr.length) {
                charsetProberArr[i2] = null;
                i2++;
            } else {
                d();
                return;
            }
        }
    }

    public void a(byte[] bArr, int i2, int i3) {
        if (!this.b) {
            if (i3 > 0) {
                this.d = true;
            }
            int i4 = 0;
            if (this.c) {
                this.c = false;
                if (i3 > 3) {
                    byte b2 = bArr[i2] & 255;
                    byte b3 = bArr[i2 + 1] & 255;
                    byte b4 = bArr[i2 + 2] & 255;
                    byte b5 = bArr[i2 + 3] & 255;
                    if (b2 != 0) {
                        if (b2 != 239) {
                            if (b2 != 254) {
                                if (b2 == 255) {
                                    if (b3 == 254 && b4 == 0 && b5 == 0) {
                                        this.f = Constants.y;
                                    } else if (b3 == 254) {
                                        this.f = Constants.w;
                                    }
                                }
                            } else if (b3 == 255 && b4 == 0 && b5 == 0) {
                                this.f = Constants.A;
                            } else if (b3 == 255) {
                                this.f = Constants.v;
                            }
                        } else if (b3 == 187 && b4 == 191) {
                            this.f = Constants.u;
                        }
                    } else if (b3 == 0 && b4 == 254 && b5 == 255) {
                        this.f = Constants.x;
                    } else if (b3 == 0 && b4 == 255 && b5 == 254) {
                        this.f = Constants.B;
                    }
                    if (this.f != null) {
                        this.b = true;
                        return;
                    }
                }
            }
            int i5 = i2 + i3;
            for (int i6 = i2; i6 < i5; i6++) {
                byte b6 = bArr[i6] & 255;
                if ((b6 & 128) == 0 || b6 == 160) {
                    if (this.f7012a == InputState.PURE_ASCII && (b6 == 27 || (b6 == 123 && this.e == 126))) {
                        this.f7012a = InputState.ESC_ASCII;
                    }
                    this.e = bArr[i6];
                } else {
                    InputState inputState = this.f7012a;
                    InputState inputState2 = InputState.HIGHBYTE;
                    if (inputState != inputState2) {
                        this.f7012a = inputState2;
                        if (this.h != null) {
                            this.h = null;
                        }
                        CharsetProber[] charsetProberArr = this.g;
                        if (charsetProberArr[0] == null) {
                            charsetProberArr[0] = new MBCSGroupProber();
                        }
                        CharsetProber[] charsetProberArr2 = this.g;
                        if (charsetProberArr2[1] == null) {
                            charsetProberArr2[1] = new SBCSGroupProber();
                        }
                        CharsetProber[] charsetProberArr3 = this.g;
                        if (charsetProberArr3[2] == null) {
                            charsetProberArr3[2] = new Latin1Prober();
                        }
                    }
                }
            }
            InputState inputState3 = this.f7012a;
            if (inputState3 == InputState.ESC_ASCII) {
                if (this.h == null) {
                    this.h = new EscCharsetProber();
                }
                if (this.h.c(bArr, i2, i3) == CharsetProber.ProbingState.FOUND_IT) {
                    this.b = true;
                    this.f = this.h.a();
                }
            } else if (inputState3 == InputState.HIGHBYTE) {
                while (true) {
                    CharsetProber[] charsetProberArr4 = this.g;
                    if (i4 >= charsetProberArr4.length) {
                        return;
                    }
                    if (charsetProberArr4[i4].c(bArr, i2, i3) == CharsetProber.ProbingState.FOUND_IT) {
                        this.b = true;
                        this.f = this.g[i4].a();
                        return;
                    }
                    i4++;
                }
            }
        }
    }

    public String b() {
        return this.f;
    }

    public boolean c() {
        return this.b;
    }

    public void d() {
        int i2 = 0;
        this.b = false;
        this.c = true;
        this.f = null;
        this.d = false;
        this.f7012a = InputState.PURE_ASCII;
        this.e = 0;
        CharsetProber charsetProber = this.h;
        if (charsetProber != null) {
            charsetProber.d();
        }
        while (true) {
            CharsetProber[] charsetProberArr = this.g;
            if (i2 < charsetProberArr.length) {
                if (charsetProberArr[i2] != null) {
                    charsetProberArr[i2].d();
                }
                i2++;
            } else {
                return;
            }
        }
    }

    public void a() {
        CharsetProber[] charsetProberArr;
        if (this.d) {
            String str = this.f;
            if (str != null) {
                this.b = true;
                CharsetListener charsetListener = this.i;
                if (charsetListener != null) {
                    charsetListener.a(str);
                }
            } else if (this.f7012a == InputState.HIGHBYTE) {
                float f2 = 0.0f;
                int i2 = 0;
                int i3 = 0;
                while (true) {
                    charsetProberArr = this.g;
                    if (i2 >= charsetProberArr.length) {
                        break;
                    }
                    float b2 = charsetProberArr[i2].b();
                    if (b2 > f2) {
                        i3 = i2;
                        f2 = b2;
                    }
                    i2++;
                }
                if (f2 > 0.2f) {
                    this.f = charsetProberArr[i3].a();
                    CharsetListener charsetListener2 = this.i;
                    if (charsetListener2 != null) {
                        charsetListener2.a(this.f);
                    }
                }
            } else {
                InputState inputState = InputState.ESC_ASCII;
            }
        }
    }
}
