package org.apache.commons.lang3;

import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.text.translate.AggregateTranslator;
import org.apache.commons.lang3.text.translate.CharSequenceTranslator;
import org.apache.commons.lang3.text.translate.EntityArrays;
import org.apache.commons.lang3.text.translate.JavaUnicodeEscaper;
import org.apache.commons.lang3.text.translate.LookupTranslator;
import org.apache.commons.lang3.text.translate.NumericEntityEscaper;
import org.apache.commons.lang3.text.translate.NumericEntityUnescaper;
import org.apache.commons.lang3.text.translate.OctalUnescaper;
import org.apache.commons.lang3.text.translate.UnicodeUnescaper;
import org.apache.commons.lang3.text.translate.UnicodeUnpairedSurrogateRemover;

@Deprecated
public class StringEscapeUtils {

    /* renamed from: a  reason: collision with root package name */
    public static final CharSequenceTranslator f4088a = new AggregateTranslator(new OctalUnescaper(), new UnicodeUnescaper(), new LookupTranslator(EntityArrays.j()), new LookupTranslator(new String[]{"\\\\", "\\"}, new String[]{"\\\"", "\""}, new String[]{"\\'", "'"}, new String[]{"\\", ""}));

    static class CsvEscaper extends CharSequenceTranslator {
        private static final String b = String.valueOf('\"');
        private static final char[] c = {',', '\"', 13, 10};

        CsvEscaper() {
        }

        public int a(CharSequence charSequence, int i, Writer writer) throws IOException {
            if (i == 0) {
                if (StringUtils.b(charSequence.toString(), c)) {
                    writer.write(charSequence.toString());
                } else {
                    writer.write(34);
                    String charSequence2 = charSequence.toString();
                    String str = b;
                    writer.write(StringUtils.a(charSequence2, str, b + b));
                    writer.write(34);
                }
                return Character.codePointCount(charSequence, 0, charSequence.length());
            }
            throw new IllegalStateException("CsvEscaper should never reach the [1] index");
        }
    }

    static class CsvUnescaper extends CharSequenceTranslator {
        private static final String b = String.valueOf('\"');
        private static final char[] c = {',', '\"', 13, 10};

        CsvUnescaper() {
        }

        public int a(CharSequence charSequence, int i, Writer writer) throws IOException {
            if (i != 0) {
                throw new IllegalStateException("CsvUnescaper should never reach the [1] index");
            } else if (charSequence.charAt(0) == '\"' && charSequence.charAt(charSequence.length() - 1) == '\"') {
                String charSequence2 = charSequence.subSequence(1, charSequence.length() - 1).toString();
                if (StringUtils.a(charSequence2, c)) {
                    writer.write(StringUtils.a(charSequence2, b + b, b));
                } else {
                    writer.write(charSequence.toString());
                }
                return Character.codePointCount(charSequence, 0, charSequence.length());
            } else {
                writer.write(charSequence.toString());
                return Character.codePointCount(charSequence, 0, charSequence.length());
            }
        }
    }

    static {
        new LookupTranslator(new String[]{"\"", "\\\""}, new String[]{"\\", "\\\\"}).a(new LookupTranslator(EntityArrays.i())).a(JavaUnicodeEscaper.a(32, 127));
        new AggregateTranslator(new LookupTranslator(new String[]{"'", "\\'"}, new String[]{"\"", "\\\""}, new String[]{"\\", "\\\\"}, new String[]{"/", "\\/"}), new LookupTranslator(EntityArrays.i()), JavaUnicodeEscaper.a(32, 127));
        new AggregateTranslator(new LookupTranslator(new String[]{"\"", "\\\""}, new String[]{"\\", "\\\\"}, new String[]{"/", "\\/"}), new LookupTranslator(EntityArrays.i()), JavaUnicodeEscaper.a(32, 127));
        new AggregateTranslator(new LookupTranslator(EntityArrays.c()), new LookupTranslator(EntityArrays.a()));
        new AggregateTranslator(new LookupTranslator(EntityArrays.c()), new LookupTranslator(EntityArrays.a()), new LookupTranslator(new String[]{"\u0000", ""}, new String[]{"\u0001", ""}, new String[]{"\u0002", ""}, new String[]{"\u0003", ""}, new String[]{"\u0004", ""}, new String[]{"\u0005", ""}, new String[]{"\u0006", ""}, new String[]{"\u0007", ""}, new String[]{"\b", ""}, new String[]{"\u000b", ""}, new String[]{"\f", ""}, new String[]{"\u000e", ""}, new String[]{"\u000f", ""}, new String[]{"\u0010", ""}, new String[]{"\u0011", ""}, new String[]{"\u0012", ""}, new String[]{"\u0013", ""}, new String[]{"\u0014", ""}, new String[]{"\u0015", ""}, new String[]{"\u0016", ""}, new String[]{"\u0017", ""}, new String[]{"\u0018", ""}, new String[]{"\u0019", ""}, new String[]{"\u001a", ""}, new String[]{"\u001b", ""}, new String[]{"\u001c", ""}, new String[]{"\u001d", ""}, new String[]{"\u001e", ""}, new String[]{"\u001f", ""}, new String[]{"￾", ""}, new String[]{"￿", ""}), NumericEntityEscaper.a(127, 132), NumericEntityEscaper.a(134, 159), new UnicodeUnpairedSurrogateRemover());
        new AggregateTranslator(new LookupTranslator(EntityArrays.c()), new LookupTranslator(EntityArrays.a()), new LookupTranslator(new String[]{"\u0000", ""}, new String[]{"\u000b", "&#11;"}, new String[]{"\f", "&#12;"}, new String[]{"￾", ""}, new String[]{"￿", ""}), NumericEntityEscaper.a(1, 8), NumericEntityEscaper.a(14, 31), NumericEntityEscaper.a(127, 132), NumericEntityEscaper.a(134, 159), new UnicodeUnpairedSurrogateRemover());
        new AggregateTranslator(new LookupTranslator(EntityArrays.c()), new LookupTranslator(EntityArrays.g()));
        new AggregateTranslator(new LookupTranslator(EntityArrays.c()), new LookupTranslator(EntityArrays.g()), new LookupTranslator(EntityArrays.e()));
        new CsvEscaper();
        new AggregateTranslator(new LookupTranslator(EntityArrays.d()), new LookupTranslator(EntityArrays.h()), new NumericEntityUnescaper(new NumericEntityUnescaper.OPTION[0]));
        new AggregateTranslator(new LookupTranslator(EntityArrays.d()), new LookupTranslator(EntityArrays.h()), new LookupTranslator(EntityArrays.f()), new NumericEntityUnescaper(new NumericEntityUnescaper.OPTION[0]));
        new AggregateTranslator(new LookupTranslator(EntityArrays.d()), new LookupTranslator(EntityArrays.b()), new NumericEntityUnescaper(new NumericEntityUnescaper.OPTION[0]));
        new CsvUnescaper();
    }

    public static final String a(String str) {
        return f4088a.a((CharSequence) str);
    }
}
