package org.apache.commons.lang3.tuple;

import java.io.Serializable;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.builder.CompareToBuilder;

public abstract class Pair<L, R> implements Map.Entry<L, R>, Comparable<Pair<L, R>>, Serializable {
    private static final long serialVersionUID = 4954918890077093841L;

    /* renamed from: a */
    public int compareTo(Pair<L, R> pair) {
        return new CompareToBuilder().a(a(), (Object) pair.a()).a(b(), (Object) pair.b()).a();
    }

    public abstract L a();

    public abstract R b();

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Map.Entry)) {
            return false;
        }
        Map.Entry entry = (Map.Entry) obj;
        if (!Objects.equals(getKey(), entry.getKey()) || !Objects.equals(getValue(), entry.getValue())) {
            return false;
        }
        return true;
    }

    public final L getKey() {
        return a();
    }

    public R getValue() {
        return b();
    }

    public int hashCode() {
        int i = 0;
        int hashCode = getKey() == null ? 0 : getKey().hashCode();
        if (getValue() != null) {
            i = getValue().hashCode();
        }
        return hashCode ^ i;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append('(');
        sb.append(a());
        sb.append(',');
        sb.append(b());
        sb.append(')');
        return sb.toString();
    }
}
