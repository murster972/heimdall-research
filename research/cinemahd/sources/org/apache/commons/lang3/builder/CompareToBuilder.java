package org.apache.commons.lang3.builder;

import java.util.Comparator;

public class CompareToBuilder implements Builder<Integer> {

    /* renamed from: a  reason: collision with root package name */
    private int f4089a = 0;

    private void b(Object obj, Object obj2, Comparator<?> comparator) {
        if (obj instanceof long[]) {
            a((long[]) obj, (long[]) obj2);
        } else if (obj instanceof int[]) {
            a((int[]) obj, (int[]) obj2);
        } else if (obj instanceof short[]) {
            a((short[]) obj, (short[]) obj2);
        } else if (obj instanceof char[]) {
            a((char[]) obj, (char[]) obj2);
        } else if (obj instanceof byte[]) {
            a((byte[]) obj, (byte[]) obj2);
        } else if (obj instanceof double[]) {
            a((double[]) obj, (double[]) obj2);
        } else if (obj instanceof float[]) {
            a((float[]) obj, (float[]) obj2);
        } else if (obj instanceof boolean[]) {
            a((boolean[]) obj, (boolean[]) obj2);
        } else {
            a((Object[]) obj, (Object[]) obj2, comparator);
        }
    }

    public CompareToBuilder a(Object obj, Object obj2) {
        return a(obj, obj2, (Comparator<?>) null);
    }

    public CompareToBuilder a(Object obj, Object obj2, Comparator<?> comparator) {
        if (this.f4089a != 0 || obj == obj2) {
            return this;
        }
        if (obj == null) {
            this.f4089a = -1;
            return this;
        } else if (obj2 == null) {
            this.f4089a = 1;
            return this;
        } else {
            if (obj.getClass().isArray()) {
                b(obj, obj2, comparator);
            } else if (comparator == null) {
                this.f4089a = ((Comparable) obj).compareTo(obj2);
            } else {
                this.f4089a = comparator.compare(obj, obj2);
            }
            return this;
        }
    }

    public CompareToBuilder a(long j, long j2) {
        if (this.f4089a != 0) {
            return this;
        }
        int i = (j > j2 ? 1 : (j == j2 ? 0 : -1));
        this.f4089a = i < 0 ? -1 : i > 0 ? 1 : 0;
        return this;
    }

    public CompareToBuilder a(int i, int i2) {
        if (this.f4089a != 0) {
            return this;
        }
        this.f4089a = i < i2 ? -1 : i > i2 ? 1 : 0;
        return this;
    }

    public CompareToBuilder a(short s, short s2) {
        if (this.f4089a != 0) {
            return this;
        }
        this.f4089a = s < s2 ? -1 : s > s2 ? 1 : 0;
        return this;
    }

    public CompareToBuilder a(char c, char c2) {
        if (this.f4089a != 0) {
            return this;
        }
        this.f4089a = c < c2 ? -1 : c > c2 ? 1 : 0;
        return this;
    }

    public CompareToBuilder a(byte b, byte b2) {
        if (this.f4089a != 0) {
            return this;
        }
        this.f4089a = b < b2 ? -1 : b > b2 ? 1 : 0;
        return this;
    }

    public CompareToBuilder a(double d, double d2) {
        if (this.f4089a != 0) {
            return this;
        }
        this.f4089a = Double.compare(d, d2);
        return this;
    }

    public CompareToBuilder a(float f, float f2) {
        if (this.f4089a != 0) {
            return this;
        }
        this.f4089a = Float.compare(f, f2);
        return this;
    }

    public CompareToBuilder a(boolean z, boolean z2) {
        if (this.f4089a != 0 || z == z2) {
            return this;
        }
        if (!z) {
            this.f4089a = -1;
        } else {
            this.f4089a = 1;
        }
        return this;
    }

    public CompareToBuilder a(Object[] objArr, Object[] objArr2, Comparator<?> comparator) {
        if (this.f4089a != 0 || objArr == objArr2) {
            return this;
        }
        int i = -1;
        if (objArr == null) {
            this.f4089a = -1;
            return this;
        } else if (objArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (objArr.length != objArr2.length) {
            if (objArr.length >= objArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < objArr.length && this.f4089a == 0; i2++) {
                a(objArr[i2], objArr2[i2], comparator);
            }
            return this;
        }
    }

    public CompareToBuilder a(long[] jArr, long[] jArr2) {
        if (this.f4089a != 0 || jArr == jArr2) {
            return this;
        }
        int i = -1;
        if (jArr == null) {
            this.f4089a = -1;
            return this;
        } else if (jArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (jArr.length != jArr2.length) {
            if (jArr.length >= jArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < jArr.length && this.f4089a == 0; i2++) {
                a(jArr[i2], jArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(int[] iArr, int[] iArr2) {
        if (this.f4089a != 0 || iArr == iArr2) {
            return this;
        }
        int i = -1;
        if (iArr == null) {
            this.f4089a = -1;
            return this;
        } else if (iArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (iArr.length != iArr2.length) {
            if (iArr.length >= iArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < iArr.length && this.f4089a == 0; i2++) {
                a(iArr[i2], iArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(short[] sArr, short[] sArr2) {
        if (this.f4089a != 0 || sArr == sArr2) {
            return this;
        }
        int i = -1;
        if (sArr == null) {
            this.f4089a = -1;
            return this;
        } else if (sArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (sArr.length != sArr2.length) {
            if (sArr.length >= sArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < sArr.length && this.f4089a == 0; i2++) {
                a(sArr[i2], sArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(char[] cArr, char[] cArr2) {
        if (this.f4089a != 0 || cArr == cArr2) {
            return this;
        }
        int i = -1;
        if (cArr == null) {
            this.f4089a = -1;
            return this;
        } else if (cArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (cArr.length != cArr2.length) {
            if (cArr.length >= cArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < cArr.length && this.f4089a == 0; i2++) {
                a(cArr[i2], cArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(byte[] bArr, byte[] bArr2) {
        if (this.f4089a != 0 || bArr == bArr2) {
            return this;
        }
        int i = -1;
        if (bArr == null) {
            this.f4089a = -1;
            return this;
        } else if (bArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (bArr.length != bArr2.length) {
            if (bArr.length >= bArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < bArr.length && this.f4089a == 0; i2++) {
                a(bArr[i2], bArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(double[] dArr, double[] dArr2) {
        if (this.f4089a != 0 || dArr == dArr2) {
            return this;
        }
        int i = -1;
        if (dArr == null) {
            this.f4089a = -1;
            return this;
        } else if (dArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (dArr.length != dArr2.length) {
            if (dArr.length >= dArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < dArr.length && this.f4089a == 0; i2++) {
                a(dArr[i2], dArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(float[] fArr, float[] fArr2) {
        if (this.f4089a != 0 || fArr == fArr2) {
            return this;
        }
        int i = -1;
        if (fArr == null) {
            this.f4089a = -1;
            return this;
        } else if (fArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (fArr.length != fArr2.length) {
            if (fArr.length >= fArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < fArr.length && this.f4089a == 0; i2++) {
                a(fArr[i2], fArr2[i2]);
            }
            return this;
        }
    }

    public CompareToBuilder a(boolean[] zArr, boolean[] zArr2) {
        if (this.f4089a != 0 || zArr == zArr2) {
            return this;
        }
        int i = -1;
        if (zArr == null) {
            this.f4089a = -1;
            return this;
        } else if (zArr2 == null) {
            this.f4089a = 1;
            return this;
        } else if (zArr.length != zArr2.length) {
            if (zArr.length >= zArr2.length) {
                i = 1;
            }
            this.f4089a = i;
            return this;
        } else {
            for (int i2 = 0; i2 < zArr.length && this.f4089a == 0; i2++) {
                a(zArr[i2], zArr2[i2]);
            }
            return this;
        }
    }

    public int a() {
        return this.f4089a;
    }
}
