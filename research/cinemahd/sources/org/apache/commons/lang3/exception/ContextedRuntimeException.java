package org.apache.commons.lang3.exception;

public class ContextedRuntimeException extends RuntimeException implements ExceptionContext {
    private static final long serialVersionUID = 20110706;
    private final ExceptionContext exceptionContext = new DefaultExceptionContext();

    public String a(String str) {
        return this.exceptionContext.a(str);
    }

    public String getMessage() {
        return a(super.getMessage());
    }
}
