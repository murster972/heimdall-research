package org.apache.commons.lang3.text.translate;

import java.io.IOException;
import java.io.Writer;
import org.apache.commons.lang3.ArrayUtils;

@Deprecated
public class AggregateTranslator extends CharSequenceTranslator {
    private final CharSequenceTranslator[] b;

    public AggregateTranslator(CharSequenceTranslator... charSequenceTranslatorArr) {
        this.b = (CharSequenceTranslator[]) ArrayUtils.a((T[]) charSequenceTranslatorArr);
    }

    public int a(CharSequence charSequence, int i, Writer writer) throws IOException {
        for (CharSequenceTranslator a2 : this.b) {
            int a3 = a2.a(charSequence, i, writer);
            if (a3 != 0) {
                return a3;
            }
        }
        return 0;
    }
}
