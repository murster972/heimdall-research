package org.apache.commons.lang3.text.translate;

import java.io.IOException;
import java.io.Writer;

@Deprecated
public class UnicodeEscaper extends CodePointTranslator {
    private final int b;
    private final int c;
    private final boolean d;

    public UnicodeEscaper() {
        this(0, Integer.MAX_VALUE, true);
    }

    public boolean a(int i, Writer writer) throws IOException {
        if (this.d) {
            if (i < this.b || i > this.c) {
                return false;
            }
        } else if (i >= this.b && i <= this.c) {
            return false;
        }
        if (i > 65535) {
            writer.write(b(i));
            return true;
        }
        writer.write("\\u");
        writer.write(CharSequenceTranslator.f4090a[(i >> 12) & 15]);
        writer.write(CharSequenceTranslator.f4090a[(i >> 8) & 15]);
        writer.write(CharSequenceTranslator.f4090a[(i >> 4) & 15]);
        writer.write(CharSequenceTranslator.f4090a[i & 15]);
        return true;
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        return "\\u" + CharSequenceTranslator.a(i);
    }

    protected UnicodeEscaper(int i, int i2, boolean z) {
        this.b = i;
        this.c = i2;
        this.d = z;
    }
}
