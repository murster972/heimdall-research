package org.apache.commons.lang3.text.translate;

@Deprecated
public class JavaUnicodeEscaper extends UnicodeEscaper {
    public JavaUnicodeEscaper(int i, int i2, boolean z) {
        super(i, i2, z);
    }

    public static JavaUnicodeEscaper a(int i, int i2) {
        return new JavaUnicodeEscaper(i, i2, false);
    }

    /* access modifiers changed from: protected */
    public String b(int i) {
        char[] chars = Character.toChars(i);
        return "\\u" + CharSequenceTranslator.a((int) chars[0]) + "\\u" + CharSequenceTranslator.a((int) chars[1]);
    }
}
