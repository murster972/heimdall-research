package org.apache.commons.lang3;

import java.lang.reflect.Array;

public class ArrayUtils {
    public static <T> T[] a(T[] tArr) {
        if (tArr == null) {
            return null;
        }
        return (Object[]) tArr.clone();
    }

    public static int a(Object obj) {
        if (obj == null) {
            return 0;
        }
        return Array.getLength(obj);
    }

    public static boolean a(char[] cArr) {
        return a((Object) cArr) == 0;
    }
}
