package org.apache.commons.vfs2.util;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public final class Messages {

    /* renamed from: a  reason: collision with root package name */
    private static ConcurrentMap<String, MessageFormat> f4096a = new ConcurrentHashMap();
    private static final ResourceBundle b = new CombinedResources("org.apache.commons.vfs2.Resources");

    private Messages() {
    }

    public static String a(String str, Object... objArr) {
        if (str == null) {
            return null;
        }
        try {
            return a(str).format(objArr);
        } catch (MissingResourceException unused) {
            return "Unknown message with code \"" + str + "\".";
        }
    }

    private static MessageFormat a(String str) throws MissingResourceException {
        MessageFormat messageFormat = (MessageFormat) f4096a.get(str);
        if (messageFormat != null) {
            return messageFormat;
        }
        f4096a.putIfAbsent(str, new MessageFormat(b.getString(str)));
        return (MessageFormat) f4096a.get(str);
    }
}
