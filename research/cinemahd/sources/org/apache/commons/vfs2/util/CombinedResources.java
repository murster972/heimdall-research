package org.apache.commons.vfs2.util;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class CombinedResources extends ResourceBundle {

    /* renamed from: a  reason: collision with root package name */
    private final String f4094a;
    private boolean b;
    /* access modifiers changed from: private */
    public final Properties c = new Properties();

    public CombinedResources(String str) {
        this.f4094a = str;
    }

    /* access modifiers changed from: protected */
    public void b() {
        if (!this.b) {
            a(a());
            a(Locale.getDefault());
            a(getLocale());
            this.b = true;
        }
    }

    public Enumeration<String> getKeys() {
        if (!this.b) {
            b();
        }
        return new Enumeration<String>() {
            public boolean hasMoreElements() {
                return CombinedResources.this.c.keys().hasMoreElements();
            }

            public String nextElement() {
                return (String) CombinedResources.this.c.keys().nextElement();
            }
        };
    }

    /* access modifiers changed from: protected */
    public Object handleGetObject(String str) {
        if (!this.b) {
            b();
        }
        return this.c.get(str);
    }

    /* access modifiers changed from: protected */
    public void a(Locale locale) {
        if (locale != null) {
            String[] strArr = {locale.getLanguage(), locale.getCountry(), locale.getVariant()};
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 3; i++) {
                sb.append(a());
                for (int i2 = 0; i2 < i; i2++) {
                    sb.append('_');
                    sb.append(strArr[i2]);
                }
                if (strArr[i].length() != 0) {
                    sb.append('_');
                    sb.append(strArr[i]);
                    a(sb.toString());
                }
                sb.setLength(0);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        ClassLoader classLoader = CombinedResources.class.getClassLoader();
        if (classLoader == null) {
            classLoader = ClassLoader.getSystemClassLoader();
        }
        try {
            Enumeration<URL> resources = classLoader.getResources(str.replace('.', '/') + ".properties");
            while (resources.hasMoreElements()) {
                try {
                    this.c.load(resources.nextElement().openConnection().getInputStream());
                } catch (IOException unused) {
                }
            }
        } catch (IOException unused2) {
        }
    }

    public String a() {
        return this.f4094a;
    }
}
