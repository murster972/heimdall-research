package org.apache.commons.vfs2;

import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.commons.vfs2.util.Messages;

public class FileSystemException extends IOException {
    private static final long serialVersionUID = 20101208;

    /* renamed from: info  reason: collision with root package name */
    private final String[] f4093info;

    static {
        Pattern.compile("[a-z]+://.*");
        Pattern.compile(":(?:[^/]+)@");
    }

    public String[] a() {
        return this.f4093info;
    }

    public String getMessage() {
        return Messages.a(super.getMessage(), a());
    }
}
