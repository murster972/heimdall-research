package org.apache.commons.vfs2;

public class FileTypeHasNoContentException extends FileSystemException {
    private static final long serialVersionUID = 20101208;
}
