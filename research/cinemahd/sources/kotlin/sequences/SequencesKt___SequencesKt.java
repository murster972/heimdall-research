package kotlin.sequences;

import com.facebook.react.uimanager.ViewProps;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

class SequencesKt___SequencesKt extends SequencesKt___SequencesJvmKt {
    public static <T> Sequence<T> a(Sequence<? extends T> sequence, int i) {
        Intrinsics.b(sequence, "$this$drop");
        if (!(i >= 0)) {
            throw new IllegalArgumentException(("Requested element count " + i + " is less than zero.").toString());
        } else if (i == 0) {
            return sequence;
        } else {
            if (sequence instanceof DropTakeSequence) {
                return ((DropTakeSequence) sequence).a(i);
            }
            return new DropSequence(sequence, i);
        }
    }

    public static <T> List<T> b(Sequence<? extends T> sequence) {
        Intrinsics.b(sequence, "$this$toList");
        return CollectionsKt__CollectionsKt.b(c(sequence));
    }

    public static final <T> List<T> c(Sequence<? extends T> sequence) {
        Intrinsics.b(sequence, "$this$toMutableList");
        ArrayList arrayList = new ArrayList();
        a(sequence, arrayList);
        return arrayList;
    }

    public static final <T, C extends Collection<? super T>> C a(Sequence<? extends T> sequence, C c) {
        Intrinsics.b(sequence, "$this$toCollection");
        Intrinsics.b(c, "destination");
        for (Object add : sequence) {
            c.add(add);
        }
        return c;
    }

    public static <T, R> Sequence<R> a(Sequence<? extends T> sequence, Function1<? super T, ? extends R> function1) {
        Intrinsics.b(sequence, "$this$map");
        Intrinsics.b(function1, ViewProps.TRANSFORM);
        return new TransformingSequence(sequence, function1);
    }

    public static final <T, A extends Appendable> A a(Sequence<? extends T> sequence, A a2, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Function1<? super T, ? extends CharSequence> function1) {
        Intrinsics.b(sequence, "$this$joinTo");
        Intrinsics.b(a2, "buffer");
        Intrinsics.b(charSequence, "separator");
        Intrinsics.b(charSequence2, "prefix");
        Intrinsics.b(charSequence3, "postfix");
        Intrinsics.b(charSequence4, "truncated");
        a2.append(charSequence2);
        int i2 = 0;
        for (Object next : sequence) {
            i2++;
            if (i2 > 1) {
                a2.append(charSequence);
            }
            if (i >= 0 && i2 > i) {
                break;
            }
            StringsKt__AppendableKt.a(a2, next, function1);
        }
        if (i >= 0 && i2 > i) {
            a2.append(charSequence4);
        }
        a2.append(charSequence3);
        return a2;
    }

    public static /* synthetic */ String a(Sequence sequence, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Function1 function1, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            charSequence = ", ";
        }
        CharSequence charSequence5 = "";
        CharSequence charSequence6 = (i2 & 2) != 0 ? charSequence5 : charSequence2;
        if ((i2 & 4) == 0) {
            charSequence5 = charSequence3;
        }
        int i3 = (i2 & 8) != 0 ? -1 : i;
        if ((i2 & 16) != 0) {
            charSequence4 = "...";
        }
        CharSequence charSequence7 = charSequence4;
        if ((i2 & 32) != 0) {
            function1 = null;
        }
        return a(sequence, charSequence, charSequence6, charSequence5, i3, charSequence7, function1);
    }

    public static final <T> String a(Sequence<? extends T> sequence, CharSequence charSequence, CharSequence charSequence2, CharSequence charSequence3, int i, CharSequence charSequence4, Function1<? super T, ? extends CharSequence> function1) {
        Intrinsics.b(sequence, "$this$joinToString");
        Intrinsics.b(charSequence, "separator");
        Intrinsics.b(charSequence2, "prefix");
        Intrinsics.b(charSequence3, "postfix");
        Intrinsics.b(charSequence4, "truncated");
        StringBuilder sb = new StringBuilder();
        a(sequence, sb, charSequence, charSequence2, charSequence3, i, charSequence4, function1);
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "joinTo(StringBuilder(), …ed, transform).toString()");
        return sb2;
    }

    public static <T> Iterable<T> a(Sequence<? extends T> sequence) {
        Intrinsics.b(sequence, "$this$asIterable");
        return new SequencesKt___SequencesKt$asIterable$$inlined$Iterable$1(sequence);
    }
}
