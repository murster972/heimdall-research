package kotlin.sequences;

import java.util.Iterator;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

public final class TransformingSequence<T, R> implements Sequence<R> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Sequence<T> f6948a;
    /* access modifiers changed from: private */
    public final Function1<T, R> b;

    public TransformingSequence(Sequence<? extends T> sequence, Function1<? super T, ? extends R> function1) {
        Intrinsics.b(sequence, "sequence");
        Intrinsics.b(function1, "transformer");
        this.f6948a = sequence;
        this.b = function1;
    }

    public Iterator<R> iterator() {
        return new TransformingSequence$iterator$1(this);
    }
}
