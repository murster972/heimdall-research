package kotlin.sequences;

import java.util.Iterator;
import kotlin.jvm.internal.markers.KMappedMarker;

public final class DropSequence$iterator$1 implements Iterator<T>, KMappedMarker {

    /* renamed from: a  reason: collision with root package name */
    private final Iterator<T> f6946a;
    private int b;

    DropSequence$iterator$1(DropSequence dropSequence) {
        this.f6946a = dropSequence.f6945a.iterator();
        this.b = dropSequence.b;
    }

    private final void a() {
        while (this.b > 0 && this.f6946a.hasNext()) {
            this.f6946a.next();
            this.b--;
        }
    }

    public boolean hasNext() {
        a();
        return this.f6946a.hasNext();
    }

    public T next() {
        a();
        return this.f6946a.next();
    }

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
