package kotlin.sequences;

import java.util.Iterator;
import kotlin.jvm.internal.markers.KMappedMarker;

public final class TransformingSequence$iterator$1 implements Iterator<R>, KMappedMarker {

    /* renamed from: a  reason: collision with root package name */
    private final Iterator<T> f6949a;
    final /* synthetic */ TransformingSequence b;

    TransformingSequence$iterator$1(TransformingSequence transformingSequence) {
        this.b = transformingSequence;
        this.f6949a = transformingSequence.f6948a.iterator();
    }

    public boolean hasNext() {
        return this.f6949a.hasNext();
    }

    public R next() {
        return this.b.b.invoke(this.f6949a.next());
    }

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
