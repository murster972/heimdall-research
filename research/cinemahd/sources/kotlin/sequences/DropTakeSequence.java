package kotlin.sequences;

public interface DropTakeSequence<T> extends Sequence<T> {
    Sequence<T> a(int i);
}
