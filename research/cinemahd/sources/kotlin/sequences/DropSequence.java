package kotlin.sequences;

import java.util.Iterator;
import kotlin.jvm.internal.Intrinsics;

public final class DropSequence<T> implements Sequence<T>, DropTakeSequence<T> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Sequence<T> f6945a;
    /* access modifiers changed from: private */
    public final int b;

    public DropSequence(Sequence<? extends T> sequence, int i) {
        Intrinsics.b(sequence, "sequence");
        this.f6945a = sequence;
        this.b = i;
        if (!(this.b >= 0)) {
            throw new IllegalArgumentException(("count must be non-negative, but was " + this.b + '.').toString());
        }
    }

    public Iterator<T> iterator() {
        return new DropSequence$iterator$1(this);
    }

    public Sequence<T> a(int i) {
        int i2 = this.b + i;
        return i2 < 0 ? new DropSequence(this, i) : new DropSequence(this.f6945a, i2);
    }
}
