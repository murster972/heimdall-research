package kotlin.collections;

import java.util.Set;
import kotlin.jvm.internal.Intrinsics;

class SetsKt__SetsKt extends SetsKt__SetsJVMKt {
    public static <T> Set<T> a() {
        return EmptySet.f6925a;
    }

    public static final <T> Set<T> a(Set<? extends T> set) {
        Intrinsics.b(set, "$this$optimizeReadOnlySet");
        int size = set.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return set;
        }
        return SetsKt__SetsJVMKt.a(set.iterator().next());
    }
}
