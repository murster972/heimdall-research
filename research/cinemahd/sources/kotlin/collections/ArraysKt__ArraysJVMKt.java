package kotlin.collections;

class ArraysKt__ArraysJVMKt {
    public static final void a(int i, int i2) {
        if (i > i2) {
            throw new IndexOutOfBoundsException("toIndex (" + i + ") is greater than size (" + i2 + ").");
        }
    }
}
