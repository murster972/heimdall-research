package kotlin.collections;

import java.util.Collection;
import java.util.Iterator;
import kotlin.jvm.internal.ArrayIteratorKt;
import kotlin.jvm.internal.CollectionToArray;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;

final class ArrayAsCollection<T> implements Collection<T>, KMappedMarker {

    /* renamed from: a  reason: collision with root package name */
    private final T[] f6920a;
    private final boolean b;

    public ArrayAsCollection(T[] tArr, boolean z) {
        Intrinsics.b(tArr, "values");
        this.f6920a = tArr;
        this.b = z;
    }

    public int a() {
        return this.f6920a.length;
    }

    public boolean add(T t) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(Collection<? extends T> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public void clear() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean contains(Object obj) {
        return ArraysKt___ArraysKt.a(this.f6920a, obj);
    }

    public boolean containsAll(Collection<? extends Object> collection) {
        Intrinsics.b(collection, "elements");
        if (collection.isEmpty()) {
            return true;
        }
        Iterator<T> it2 = collection.iterator();
        while (it2.hasNext()) {
            if (!contains(it2.next())) {
                return false;
            }
        }
        return true;
    }

    public boolean isEmpty() {
        return this.f6920a.length == 0;
    }

    public Iterator<T> iterator() {
        return ArrayIteratorKt.a(this.f6920a);
    }

    public boolean remove(Object obj) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean removeAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean retainAll(Collection<? extends Object> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public final /* bridge */ int size() {
        return a();
    }

    public final Object[] toArray() {
        return CollectionsKt__CollectionsJVMKt.a(this.f6920a, this.b);
    }

    public <T> T[] toArray(T[] tArr) {
        return CollectionToArray.a(this, tArr);
    }
}
