package kotlin.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntRange;

class ArraysKt___ArraysKt extends ArraysKt___ArraysJvmKt {
    public static final <T> boolean a(T[] tArr, T t) {
        Intrinsics.b(tArr, "$this$contains");
        return b(tArr, t) >= 0;
    }

    public static final <T> int b(T[] tArr, T t) {
        Intrinsics.b(tArr, "$this$indexOf");
        int i = 0;
        if (t == null) {
            int length = tArr.length;
            while (i < length) {
                if (tArr[i] == null) {
                    return i;
                }
                i++;
            }
            return -1;
        }
        int length2 = tArr.length;
        while (i < length2) {
            if (Intrinsics.a((Object) t, (Object) tArr[i])) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public static <T> IntRange c(T[] tArr) {
        Intrinsics.b(tArr, "$this$indices");
        return new IntRange(0, d(tArr));
    }

    public static <T> int d(T[] tArr) {
        Intrinsics.b(tArr, "$this$lastIndex");
        return tArr.length - 1;
    }

    public static <T> T e(T[] tArr) {
        Intrinsics.b(tArr, "$this$singleOrNull");
        if (tArr.length == 1) {
            return tArr[0];
        }
        return null;
    }

    public static <T> List<T> f(T[] tArr) {
        Intrinsics.b(tArr, "$this$toList");
        int length = tArr.length;
        if (length == 0) {
            return CollectionsKt__CollectionsKt.a();
        }
        if (length != 1) {
            return g(tArr);
        }
        return CollectionsKt__CollectionsJVMKt.a(tArr[0]);
    }

    public static <T> List<T> g(T[] tArr) {
        Intrinsics.b(tArr, "$this$toMutableList");
        return new ArrayList(CollectionsKt__CollectionsKt.a(tArr));
    }

    public static char a(char[] cArr) {
        Intrinsics.b(cArr, "$this$single");
        int length = cArr.length;
        if (length == 0) {
            throw new NoSuchElementException("Array is empty.");
        } else if (length == 1) {
            return cArr[0];
        } else {
            throw new IllegalArgumentException("Array has more than one element.");
        }
    }

    public static final <T> List<T> b(T[] tArr) {
        Intrinsics.b(tArr, "$this$filterNotNull");
        ArrayList arrayList = new ArrayList();
        a(tArr, arrayList);
        return arrayList;
    }

    public static final <C extends Collection<? super T>, T> C a(T[] tArr, C c) {
        Intrinsics.b(tArr, "$this$filterNotNullTo");
        Intrinsics.b(c, "destination");
        for (T t : tArr) {
            if (t != null) {
                c.add(t);
            }
        }
        return c;
    }
}
