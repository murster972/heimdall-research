package kotlin.collections;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.RandomAccess;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.markers.KMappedMarker;

public abstract class AbstractList<E> extends AbstractCollection<E> implements List<E>, KMappedMarker {

    /* renamed from: a  reason: collision with root package name */
    public static final Companion f6918a = new Companion((DefaultConstructorMarker) null);

    public static final class Companion {
        private Companion() {
        }

        public final void a(int i, int i2) {
            if (i < 0 || i >= i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        public final void b(int i, int i2) {
            if (i < 0 || i > i2) {
                throw new IndexOutOfBoundsException("index: " + i + ", size: " + i2);
            }
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }

        public final void a(int i, int i2, int i3) {
            if (i < 0 || i2 > i3) {
                throw new IndexOutOfBoundsException("fromIndex: " + i + ", toIndex: " + i2 + ", size: " + i3);
            } else if (i > i2) {
                throw new IllegalArgumentException("fromIndex: " + i + " > toIndex: " + i2);
            }
        }

        public final int a(Collection<?> collection) {
            Intrinsics.b(collection, "c");
            Iterator<?> it2 = collection.iterator();
            int i = 1;
            while (it2.hasNext()) {
                Object next = it2.next();
                i = (i * 31) + (next != null ? next.hashCode() : 0);
            }
            return i;
        }

        public final boolean a(Collection<?> collection, Collection<?> collection2) {
            Intrinsics.b(collection, "c");
            Intrinsics.b(collection2, "other");
            if (collection.size() != collection2.size()) {
                return false;
            }
            Iterator<?> it2 = collection2.iterator();
            for (Object a2 : collection) {
                if (!Intrinsics.a((Object) a2, (Object) it2.next())) {
                    return false;
                }
            }
            return true;
        }
    }

    private class IteratorImpl implements Iterator<E>, KMappedMarker {

        /* renamed from: a  reason: collision with root package name */
        private int f6919a;

        public IteratorImpl() {
        }

        /* access modifiers changed from: protected */
        public final int a() {
            return this.f6919a;
        }

        public boolean hasNext() {
            return this.f6919a < AbstractList.this.size();
        }

        public E next() {
            if (hasNext()) {
                AbstractList abstractList = AbstractList.this;
                int i = this.f6919a;
                this.f6919a = i + 1;
                return abstractList.get(i);
            }
            throw new NoSuchElementException();
        }

        public void remove() {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        /* access modifiers changed from: protected */
        public final void a(int i) {
            this.f6919a = i;
        }
    }

    private class ListIteratorImpl extends AbstractList<E>.IteratorImpl implements ListIterator<E>, KMappedMarker {
        public ListIteratorImpl(int i) {
            super();
            AbstractList.f6918a.b(i, AbstractList.this.size());
            a(i);
        }

        public void add(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }

        public boolean hasPrevious() {
            return a() > 0;
        }

        public int nextIndex() {
            return a();
        }

        public E previous() {
            if (hasPrevious()) {
                AbstractList abstractList = AbstractList.this;
                a(a() - 1);
                return abstractList.get(a());
            }
            throw new NoSuchElementException();
        }

        public int previousIndex() {
            return a() - 1;
        }

        public void set(E e) {
            throw new UnsupportedOperationException("Operation is not supported for read-only collection");
        }
    }

    private static final class SubList<E> extends AbstractList<E> implements RandomAccess {
        private int b;
        private final AbstractList<E> c;
        private final int d;

        public SubList(AbstractList<? extends E> abstractList, int i, int i2) {
            Intrinsics.b(abstractList, "list");
            this.c = abstractList;
            this.d = i;
            AbstractList.f6918a.a(this.d, i2, this.c.size());
            this.b = i2 - this.d;
        }

        public int a() {
            return this.b;
        }

        public E get(int i) {
            AbstractList.f6918a.a(i, this.b);
            return this.c.get(this.d + i);
        }
    }

    protected AbstractList() {
    }

    public void add(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean addAll(int i, Collection<? extends E> collection) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof List)) {
            return false;
        }
        return f6918a.a((Collection<?>) this, (Collection<?>) (Collection) obj);
    }

    public abstract E get(int i);

    public int hashCode() {
        return f6918a.a(this);
    }

    public int indexOf(Object obj) {
        int i = 0;
        for (Object a2 : this) {
            if (Intrinsics.a(a2, obj)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    public Iterator<E> iterator() {
        return new IteratorImpl();
    }

    public int lastIndexOf(Object obj) {
        ListIterator listIterator = listIterator(size());
        while (listIterator.hasPrevious()) {
            if (Intrinsics.a(listIterator.previous(), obj)) {
                return listIterator.nextIndex();
            }
        }
        return -1;
    }

    public ListIterator<E> listIterator() {
        return new ListIteratorImpl(0);
    }

    public E remove(int i) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public E set(int i, E e) {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public List<E> subList(int i, int i2) {
        return new SubList(this, i, i2);
    }

    public ListIterator<E> listIterator(int i) {
        return new ListIteratorImpl(i);
    }
}
