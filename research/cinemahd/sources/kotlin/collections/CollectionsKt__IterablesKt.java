package kotlin.collections;

import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;

class CollectionsKt__IterablesKt extends CollectionsKt__CollectionsKt {
    public static <T> int a(Iterable<? extends T> iterable, int i) {
        Intrinsics.b(iterable, "$this$collectionSizeOrDefault");
        return iterable instanceof Collection ? ((Collection) iterable).size() : i;
    }
}
