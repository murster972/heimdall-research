package kotlin.collections;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import kotlin.Pair;
import kotlin.TypeCastException;
import kotlin.jvm.internal.Intrinsics;

class MapsKt__MapsKt extends MapsKt__MapsJVMKt {
    public static <K, V> Map<K, V> a() {
        EmptyMap emptyMap = EmptyMap.f6924a;
        if (emptyMap != null) {
            return emptyMap;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.collections.Map<K, V>");
    }

    public static final <K, V> Map<K, V> b(Map<K, ? extends V> map) {
        Intrinsics.b(map, "$this$optimizeReadOnlyMap");
        int size = map.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return map;
        }
        return MapsKt__MapsJVMKt.a(map);
    }

    public static <K, V> Map<K, V> c(Map<? extends K, ? extends V> map) {
        Intrinsics.b(map, "$this$toMap");
        int size = map.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return d(map);
        }
        return MapsKt__MapsJVMKt.a(map);
    }

    public static <K, V> Map<K, V> d(Map<? extends K, ? extends V> map) {
        Intrinsics.b(map, "$this$toMutableMap");
        return new LinkedHashMap(map);
    }

    public static final <K, V> void a(Map<? super K, ? super V> map, Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        Intrinsics.b(map, "$this$putAll");
        Intrinsics.b(iterable, "pairs");
        for (Pair pair : iterable) {
            map.put(pair.a(), pair.b());
        }
    }

    public static <K, V> Map<K, V> a(Iterable<? extends Pair<? extends K, ? extends V>> iterable) {
        Intrinsics.b(iterable, "$this$toMap");
        if (iterable instanceof Collection) {
            Collection collection = (Collection) iterable;
            int size = collection.size();
            if (size == 0) {
                return a();
            }
            if (size != 1) {
                LinkedHashMap linkedHashMap = new LinkedHashMap(MapsKt__MapsJVMKt.a(collection.size()));
                a(iterable, linkedHashMap);
                return linkedHashMap;
            }
            return MapsKt__MapsJVMKt.a((Pair) (iterable instanceof List ? ((List) iterable).get(0) : iterable.iterator().next()));
        }
        LinkedHashMap linkedHashMap2 = new LinkedHashMap();
        a(iterable, linkedHashMap2);
        return b(linkedHashMap2);
    }

    public static final <K, V, M extends Map<? super K, ? super V>> M a(Iterable<? extends Pair<? extends K, ? extends V>> iterable, M m) {
        Intrinsics.b(iterable, "$this$toMap");
        Intrinsics.b(m, "destination");
        a(m, iterable);
        return m;
    }
}
