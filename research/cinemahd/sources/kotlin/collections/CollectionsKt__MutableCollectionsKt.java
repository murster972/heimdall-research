package kotlin.collections;

import java.util.Collection;
import kotlin.jvm.internal.Intrinsics;

class CollectionsKt__MutableCollectionsKt extends CollectionsKt__MutableCollectionsJVMKt {
    public static <T> boolean a(Collection<? super T> collection, Iterable<? extends T> iterable) {
        Intrinsics.b(collection, "$this$addAll");
        Intrinsics.b(iterable, "elements");
        if (iterable instanceof Collection) {
            return collection.addAll((Collection) iterable);
        }
        boolean z = false;
        for (Object add : iterable) {
            if (collection.add(add)) {
                z = true;
            }
        }
        return z;
    }

    public static <T> boolean a(Collection<? super T> collection, T[] tArr) {
        Intrinsics.b(collection, "$this$addAll");
        Intrinsics.b(tArr, "elements");
        return collection.addAll(ArraysKt___ArraysJvmKt.a(tArr));
    }
}
