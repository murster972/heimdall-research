package kotlin.collections;

import java.util.Arrays;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;

class ArraysKt___ArraysJvmKt extends ArraysKt__ArraysKt {
    public static <T> List<T> a(T[] tArr) {
        Intrinsics.b(tArr, "$this$asList");
        List<T> a2 = ArraysUtilJVM.a(tArr);
        Intrinsics.a((Object) a2, "ArraysUtilJVM.asList(this)");
        return a2;
    }

    public static /* synthetic */ byte[] a(byte[] bArr, byte[] bArr2, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i = 0;
        }
        if ((i4 & 4) != 0) {
            i2 = 0;
        }
        if ((i4 & 8) != 0) {
            i3 = bArr.length;
        }
        byte[] unused = a(bArr, bArr2, i, i2, i3);
        return bArr2;
    }

    public static byte[] a(byte[] bArr, byte[] bArr2, int i, int i2, int i3) {
        Intrinsics.b(bArr, "$this$copyInto");
        Intrinsics.b(bArr2, "destination");
        System.arraycopy(bArr, i2, bArr2, i, i3 - i2);
        return bArr2;
    }

    public static byte[] a(byte[] bArr, int i, int i2) {
        Intrinsics.b(bArr, "$this$copyOfRangeImpl");
        ArraysKt__ArraysJVMKt.a(i2, bArr.length);
        byte[] copyOfRange = Arrays.copyOfRange(bArr, i, i2);
        Intrinsics.a((Object) copyOfRange, "java.util.Arrays.copyOfR…this, fromIndex, toIndex)");
        return copyOfRange;
    }

    public static /* synthetic */ void a(Object[] objArr, Object obj, int i, int i2, int i3, Object obj2) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = objArr.length;
        }
        a((T[]) objArr, obj, i, i2);
    }

    public static <T> void a(T[] tArr, T t, int i, int i2) {
        Intrinsics.b(tArr, "$this$fill");
        Arrays.fill(tArr, i, i2, t);
    }

    public static /* synthetic */ void a(int[] iArr, int i, int i2, int i3, int i4, Object obj) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = iArr.length;
        }
        a(iArr, i, i2, i3);
    }

    public static final void a(int[] iArr, int i, int i2, int i3) {
        Intrinsics.b(iArr, "$this$fill");
        Arrays.fill(iArr, i2, i3, i);
    }
}
