package kotlin.collections;

import java.util.Collections;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;

class CollectionsKt__MutableCollectionsJVMKt extends CollectionsKt__IteratorsKt {
    public static <T extends Comparable<? super T>> void c(List<T> list) {
        Intrinsics.b(list, "$this$sort");
        if (list.size() > 1) {
            Collections.sort(list);
        }
    }
}
