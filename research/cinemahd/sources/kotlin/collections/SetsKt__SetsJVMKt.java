package kotlin.collections;

import java.util.Collections;
import java.util.Set;
import kotlin.jvm.internal.Intrinsics;

class SetsKt__SetsJVMKt {
    public static final <T> Set<T> a(T t) {
        Set<T> singleton = Collections.singleton(t);
        Intrinsics.a((Object) singleton, "java.util.Collections.singleton(element)");
        return singleton;
    }
}
