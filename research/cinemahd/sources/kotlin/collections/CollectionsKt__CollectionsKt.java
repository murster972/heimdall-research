package kotlin.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import kotlin.jvm.internal.Intrinsics;

class CollectionsKt__CollectionsKt extends CollectionsKt__CollectionsJVMKt {
    public static final <T> Collection<T> a(T[] tArr) {
        Intrinsics.b(tArr, "$this$asCollection");
        return new ArrayAsCollection(tArr, false);
    }

    public static <T> List<T> b(T... tArr) {
        Intrinsics.b(tArr, "elements");
        return tArr.length > 0 ? ArraysKt___ArraysJvmKt.a(tArr) : a();
    }

    public static <T> List<T> c(T... tArr) {
        Intrinsics.b(tArr, "elements");
        return ArraysKt___ArraysKt.b(tArr);
    }

    public static <T> List<T> d(T... tArr) {
        Intrinsics.b(tArr, "elements");
        return tArr.length == 0 ? new ArrayList() : new ArrayList(new ArrayAsCollection(tArr, true));
    }

    public static <T> List<T> a() {
        return EmptyList.f6923a;
    }

    public static <T> List<T> b(List<? extends T> list) {
        Intrinsics.b(list, "$this$optimizeReadOnlyList");
        int size = list.size();
        if (size == 0) {
            return a();
        }
        if (size != 1) {
            return list;
        }
        return CollectionsKt__CollectionsJVMKt.a(list.get(0));
    }

    public static void c() {
        throw new ArithmeticException("Index overflow has happened.");
    }

    public static <T> int a(List<? extends T> list) {
        Intrinsics.b(list, "$this$lastIndex");
        return list.size() - 1;
    }

    public static /* synthetic */ int a(List list, Comparable comparable, int i, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            i2 = list.size();
        }
        return a(list, comparable, i, i2);
    }

    public static final <T extends Comparable<? super T>> int a(List<? extends T> list, T t, int i, int i2) {
        Intrinsics.b(list, "$this$binarySearch");
        a(list.size(), i, i2);
        int i3 = i2 - 1;
        while (i <= i3) {
            int i4 = (i + i3) >>> 1;
            int a2 = ComparisonsKt__ComparisonsKt.a((Comparable) list.get(i4), t);
            if (a2 < 0) {
                i = i4 + 1;
            } else if (a2 <= 0) {
                return i4;
            } else {
                i3 = i4 - 1;
            }
        }
        return -(i + 1);
    }

    public static void b() {
        throw new ArithmeticException("Count overflow has happened.");
    }

    private static final void a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("fromIndex (" + i2 + ") is greater than toIndex (" + i3 + ").");
        } else if (i2 < 0) {
            throw new IndexOutOfBoundsException("fromIndex (" + i2 + ") is less than zero.");
        } else if (i3 > i) {
            throw new IndexOutOfBoundsException("toIndex (" + i3 + ") is greater than size (" + i + ").");
        }
    }
}
