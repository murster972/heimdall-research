package kotlin.jvm.internal;

import java.io.Serializable;

public abstract class Lambda<R> implements FunctionBase<R>, Serializable {
    private final int arity;

    public Lambda(int i) {
        this.arity = i;
    }

    public int getArity() {
        return this.arity;
    }

    public String toString() {
        String a2 = Reflection.a(this);
        Intrinsics.a((Object) a2, "Reflection.renderLambdaToString(this)");
        return a2;
    }
}
