package kotlin.jvm.internal;

import java.util.List;
import kotlin.jvm.internal.markers.KMappedMarker;

public class TypeIntrinsics {
    private static <T extends Throwable> T a(T t) {
        Intrinsics.a(t, TypeIntrinsics.class.getName());
        return t;
    }

    public static List b(Object obj) {
        try {
            return (List) obj;
        } catch (ClassCastException e) {
            a(e);
            throw null;
        }
    }

    public static void a(Object obj, String str) {
        String name = obj == null ? "null" : obj.getClass().getName();
        a(name + " cannot be cast to " + str);
        throw null;
    }

    public static void a(String str) {
        a(new ClassCastException(str));
        throw null;
    }

    public static ClassCastException a(ClassCastException classCastException) {
        a(classCastException);
        throw classCastException;
    }

    public static List a(Object obj) {
        if (!(obj instanceof KMappedMarker)) {
            return b(obj);
        }
        a(obj, "kotlin.collections.MutableList");
        throw null;
    }
}
