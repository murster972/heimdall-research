package kotlin.jvm.internal;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.jvm.internal.markers.KMappedMarker;

final class ArrayIterator<T> implements Iterator<T>, KMappedMarker {

    /* renamed from: a  reason: collision with root package name */
    private int f6936a;
    private final T[] b;

    public ArrayIterator(T[] tArr) {
        Intrinsics.b(tArr, "array");
        this.b = tArr;
    }

    public boolean hasNext() {
        return this.f6936a < this.b.length;
    }

    public T next() {
        try {
            T[] tArr = this.b;
            int i = this.f6936a;
            this.f6936a = i + 1;
            return tArr[i];
        } catch (ArrayIndexOutOfBoundsException e) {
            this.f6936a--;
            throw new NoSuchElementException(e.getMessage());
        }
    }

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
