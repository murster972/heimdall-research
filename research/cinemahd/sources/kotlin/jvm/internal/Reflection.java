package kotlin.jvm.internal;

import kotlin.reflect.KClass;
import kotlin.reflect.KMutableProperty0;

public class Reflection {

    /* renamed from: a  reason: collision with root package name */
    private static final ReflectionFactory f6940a;

    static {
        ReflectionFactory reflectionFactory = null;
        try {
            reflectionFactory = (ReflectionFactory) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (reflectionFactory == null) {
            reflectionFactory = new ReflectionFactory();
        }
        f6940a = reflectionFactory;
    }

    public static KClass a(Class cls) {
        return f6940a.a(cls);
    }

    public static String a(Lambda lambda) {
        return f6940a.a(lambda);
    }

    public static KMutableProperty0 a(MutablePropertyReference0 mutablePropertyReference0) {
        return f6940a.a(mutablePropertyReference0);
    }
}
