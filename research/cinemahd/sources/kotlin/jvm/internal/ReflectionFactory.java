package kotlin.jvm.internal;

import kotlin.reflect.KClass;
import kotlin.reflect.KMutableProperty0;

public class ReflectionFactory {
    public KClass a(Class cls) {
        return new ClassReference(cls);
    }

    public KMutableProperty0 a(MutablePropertyReference0 mutablePropertyReference0) {
        return mutablePropertyReference0;
    }

    public String a(Lambda lambda) {
        return a((FunctionBase) lambda);
    }

    public String a(FunctionBase functionBase) {
        String obj = functionBase.getClass().getGenericInterfaces()[0].toString();
        return obj.startsWith("kotlin.jvm.functions.") ? obj.substring(21) : obj;
    }
}
