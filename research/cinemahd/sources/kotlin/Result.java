package kotlin;

import java.io.Serializable;
import kotlin.jvm.internal.Intrinsics;

public final class Result<T> implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    public static final Companion f6915a = new Companion((DefaultConstructorMarker) null);
    private final Object value;

    public static final class Companion {
        private Companion() {
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public static final class Failure implements Serializable {
        public final Throwable exception;

        public Failure(Throwable th) {
            Intrinsics.b(th, "exception");
            this.exception = th;
        }

        public boolean equals(Object obj) {
            return (obj instanceof Failure) && Intrinsics.a((Object) this.exception, (Object) ((Failure) obj).exception);
        }

        public int hashCode() {
            return this.exception.hashCode();
        }

        public String toString() {
            return "Failure(" + this.exception + ')';
        }
    }

    public static Object a(Object obj) {
        return obj;
    }

    public static boolean a(Object obj, Object obj2) {
        return (obj2 instanceof Result) && Intrinsics.a(obj, ((Result) obj2).a());
    }

    public static int b(Object obj) {
        if (obj != null) {
            return obj.hashCode();
        }
        return 0;
    }

    public static String c(Object obj) {
        if (obj instanceof Failure) {
            return obj.toString();
        }
        return "Success(" + obj + ')';
    }

    public final /* synthetic */ Object a() {
        return this.value;
    }

    public boolean equals(Object obj) {
        return a(this.value, obj);
    }

    public int hashCode() {
        return b(this.value);
    }

    public String toString() {
        return c(this.value);
    }
}
