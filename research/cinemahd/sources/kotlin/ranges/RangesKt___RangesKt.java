package kotlin.ranges;

import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntProgression;

class RangesKt___RangesKt extends RangesKt__RangesKt {
    public static int a(int i, int i2) {
        return i < i2 ? i2 : i;
    }

    public static long a(long j, long j2) {
        return j > j2 ? j2 : j;
    }

    public static IntProgression a(IntProgression intProgression, int i) {
        Intrinsics.b(intProgression, "$this$step");
        RangesKt__RangesKt.a(i > 0, Integer.valueOf(i));
        IntProgression.Companion companion = IntProgression.d;
        int first = intProgression.getFirst();
        int last = intProgression.getLast();
        if (intProgression.a() <= 0) {
            i = -i;
        }
        return companion.a(first, last, i);
    }

    public static int b(int i, int i2) {
        return i > i2 ? i2 : i;
    }

    public static IntProgression c(int i, int i2) {
        return IntProgression.d.a(i, i2, -1);
    }

    public static IntRange d(int i, int i2) {
        if (i2 <= Integer.MIN_VALUE) {
            return IntRange.f.a();
        }
        return new IntRange(i, i2 - 1);
    }

    public static int a(int i, int i2, int i3) {
        if (i2 > i3) {
            throw new IllegalArgumentException("Cannot coerce value to an empty range: maximum " + i3 + " is less than minimum " + i2 + '.');
        } else if (i < i2) {
            return i2;
        } else {
            return i > i3 ? i3 : i;
        }
    }
}
