package kotlin.ranges;

import kotlin.jvm.internal.Intrinsics;

class RangesKt__RangesKt {
    public static final void a(boolean z, Number number) {
        Intrinsics.b(number, "step");
        if (!z) {
            throw new IllegalArgumentException("Step must be positive, was: " + number + '.');
        }
    }
}
