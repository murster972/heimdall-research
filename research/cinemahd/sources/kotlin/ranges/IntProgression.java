package kotlin.ranges;

import kotlin.collections.IntIterator;
import kotlin.internal.ProgressionUtilKt;
import kotlin.jvm.internal.markers.KMappedMarker;

public class IntProgression implements Iterable<Integer>, KMappedMarker {
    public static final Companion d = new Companion((DefaultConstructorMarker) null);

    /* renamed from: a  reason: collision with root package name */
    private final int f6942a;
    private final int b;
    private final int c;

    public static final class Companion {
        private Companion() {
        }

        public final IntProgression a(int i, int i2, int i3) {
            return new IntProgression(i, i2, i3);
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public IntProgression(int i, int i2, int i3) {
        if (i3 == 0) {
            throw new IllegalArgumentException("Step must be non-zero.");
        } else if (i3 != Integer.MIN_VALUE) {
            this.f6942a = i;
            this.b = ProgressionUtilKt.b(i, i2, i3);
            this.c = i3;
        } else {
            throw new IllegalArgumentException("Step must be greater than Int.MIN_VALUE to avoid overflow on negation.");
        }
    }

    public final int a() {
        return this.c;
    }

    public boolean equals(Object obj) {
        if (obj instanceof IntProgression) {
            if (!isEmpty() || !((IntProgression) obj).isEmpty()) {
                IntProgression intProgression = (IntProgression) obj;
                if (!(this.f6942a == intProgression.f6942a && this.b == intProgression.b && this.c == intProgression.c)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public final int getFirst() {
        return this.f6942a;
    }

    public final int getLast() {
        return this.b;
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (((this.f6942a * 31) + this.b) * 31) + this.c;
    }

    public boolean isEmpty() {
        if (this.c > 0) {
            if (this.f6942a > this.b) {
                return true;
            }
        } else if (this.f6942a < this.b) {
            return true;
        }
        return false;
    }

    public String toString() {
        int i;
        StringBuilder sb;
        if (this.c > 0) {
            sb = new StringBuilder();
            sb.append(this.f6942a);
            sb.append("..");
            sb.append(this.b);
            sb.append(" step ");
            i = this.c;
        } else {
            sb = new StringBuilder();
            sb.append(this.f6942a);
            sb.append(" downTo ");
            sb.append(this.b);
            sb.append(" step ");
            i = -this.c;
        }
        sb.append(i);
        return sb.toString();
    }

    public IntIterator iterator() {
        return new IntProgressionIterator(this.f6942a, this.b, this.c);
    }
}
