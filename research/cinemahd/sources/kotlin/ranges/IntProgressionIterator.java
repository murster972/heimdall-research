package kotlin.ranges;

import java.util.NoSuchElementException;
import kotlin.collections.IntIterator;

public final class IntProgressionIterator extends IntIterator {

    /* renamed from: a  reason: collision with root package name */
    private final int f6943a;
    private boolean b;
    private int c;
    private final int d;

    public IntProgressionIterator(int i, int i2, int i3) {
        this.d = i3;
        this.f6943a = i2;
        boolean z = true;
        if (this.d <= 0 ? i < i2 : i > i2) {
            z = false;
        }
        this.b = z;
        this.c = !this.b ? this.f6943a : i;
    }

    public int a() {
        int i = this.c;
        if (i != this.f6943a) {
            this.c = this.d + i;
        } else if (this.b) {
            this.b = false;
        } else {
            throw new NoSuchElementException();
        }
        return i;
    }

    public boolean hasNext() {
        return this.b;
    }
}
