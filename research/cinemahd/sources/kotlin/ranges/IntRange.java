package kotlin.ranges;

public final class IntRange extends IntProgression implements ClosedRange<Integer> {
    /* access modifiers changed from: private */
    public static final IntRange e = new IntRange(1, 0);
    public static final Companion f = new Companion((DefaultConstructorMarker) null);

    public static final class Companion {
        private Companion() {
        }

        public final IntRange a() {
            return IntRange.e;
        }

        public /* synthetic */ Companion(DefaultConstructorMarker defaultConstructorMarker) {
            this();
        }
    }

    public IntRange(int i, int i2) {
        super(i, i2, 1);
    }

    public Integer b() {
        return Integer.valueOf(getLast());
    }

    public Integer c() {
        return Integer.valueOf(getFirst());
    }

    public boolean equals(Object obj) {
        if (obj instanceof IntRange) {
            if (!isEmpty() || !((IntRange) obj).isEmpty()) {
                IntRange intRange = (IntRange) obj;
                if (!(getFirst() == intRange.getFirst() && getLast() == intRange.getLast())) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (getFirst() * 31) + getLast();
    }

    public boolean isEmpty() {
        return getFirst() > getLast();
    }

    public String toString() {
        return getFirst() + ".." + getLast();
    }
}
