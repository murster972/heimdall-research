package kotlin;

import java.io.Serializable;
import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

final class SynchronizedLazyImpl<T> implements Lazy<T>, Serializable {
    private volatile Object _value;
    private Function0<? extends T> initializer;
    private final Object lock;

    public SynchronizedLazyImpl(Function0<? extends T> function0, Object obj) {
        Intrinsics.b(function0, "initializer");
        this.initializer = function0;
        this._value = UNINITIALIZED_VALUE.f6916a;
        this.lock = obj == null ? this : obj;
    }

    private final Object writeReplace() {
        return new InitializedLazyImpl(getValue());
    }

    public boolean a() {
        return this._value != UNINITIALIZED_VALUE.f6916a;
    }

    public T getValue() {
        T t;
        T t2 = this._value;
        if (t2 != UNINITIALIZED_VALUE.f6916a) {
            return t2;
        }
        synchronized (this.lock) {
            t = this._value;
            if (t == UNINITIALIZED_VALUE.f6916a) {
                Function0 function0 = this.initializer;
                if (function0 != null) {
                    t = function0.invoke();
                    this._value = t;
                    this.initializer = null;
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        }
        return t;
    }

    public String toString() {
        return a() ? String.valueOf(getValue()) : "Lazy value not initialized yet.";
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ SynchronizedLazyImpl(Function0 function0, Object obj, int i, DefaultConstructorMarker defaultConstructorMarker) {
        this(function0, (i & 2) != 0 ? null : obj);
    }
}
