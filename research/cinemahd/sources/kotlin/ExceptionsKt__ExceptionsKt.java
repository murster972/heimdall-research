package kotlin;

import kotlin.internal.PlatformImplementationsKt;
import kotlin.jvm.internal.Intrinsics;

class ExceptionsKt__ExceptionsKt {
    public static void a(Throwable th, Throwable th2) {
        Intrinsics.b(th, "$this$addSuppressed");
        Intrinsics.b(th2, "exception");
        PlatformImplementationsKt.f6935a.a(th, th2);
    }
}
