package kotlin.text;

import java.util.Collection;
import java.util.List;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

final class StringsKt__StringsKt$rangesDelimitedBy$4 extends Lambda implements Function2<CharSequence, Integer, Pair<? extends Integer, ? extends Integer>> {
    final /* synthetic */ List $delimitersList;
    final /* synthetic */ boolean $ignoreCase;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StringsKt__StringsKt$rangesDelimitedBy$4(List list, boolean z) {
        super(2);
        this.$delimitersList = list;
        this.$ignoreCase = z;
    }

    public /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) {
        return a((CharSequence) obj, ((Number) obj2).intValue());
    }

    public final Pair<Integer, Integer> a(CharSequence charSequence, int i) {
        Intrinsics.b(charSequence, "$receiver");
        Pair a2 = StringsKt__StringsKt.b(charSequence, (Collection<String>) this.$delimitersList, i, this.$ignoreCase, false);
        if (a2 != null) {
            return TuplesKt.a(a2.c(), Integer.valueOf(((String) a2.d()).length()));
        }
        return null;
    }
}
