package kotlin.text;

import kotlin.ranges.IntRange;

class CharsKt__CharJVMKt {
    public static final boolean a(char c) {
        return Character.isWhitespace(c) || Character.isSpaceChar(c);
    }

    public static final int a(char c, int i) {
        return Character.digit(c, i);
    }

    public static int a(int i) {
        if (2 <= i && 36 >= i) {
            return i;
        }
        throw new IllegalArgumentException("radix " + i + " was not in valid range " + new IntRange(2, 36));
    }
}
