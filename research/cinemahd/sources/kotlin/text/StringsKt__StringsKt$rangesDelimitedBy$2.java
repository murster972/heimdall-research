package kotlin.text;

import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.functions.Function2;
import kotlin.jvm.internal.Intrinsics;
import kotlin.jvm.internal.Lambda;

final class StringsKt__StringsKt$rangesDelimitedBy$2 extends Lambda implements Function2<CharSequence, Integer, Pair<? extends Integer, ? extends Integer>> {
    final /* synthetic */ char[] $delimiters;
    final /* synthetic */ boolean $ignoreCase;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    StringsKt__StringsKt$rangesDelimitedBy$2(char[] cArr, boolean z) {
        super(2);
        this.$delimiters = cArr;
        this.$ignoreCase = z;
    }

    public /* bridge */ /* synthetic */ Object a(Object obj, Object obj2) {
        return a((CharSequence) obj, ((Number) obj2).intValue());
    }

    public final Pair<Integer, Integer> a(CharSequence charSequence, int i) {
        Intrinsics.b(charSequence, "$receiver");
        int a2 = StringsKt__StringsKt.a(charSequence, this.$delimiters, i, this.$ignoreCase);
        if (a2 < 0) {
            return null;
        }
        return TuplesKt.a(Integer.valueOf(a2), 1);
    }
}
