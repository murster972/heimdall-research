package kotlin.text;

import java.nio.charset.Charset;
import kotlin.jvm.internal.Intrinsics;

public final class Charsets {

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f6950a;
    private static Charset b;
    private static Charset c;
    public static final Charsets d = new Charsets();

    static {
        Charset forName = Charset.forName("UTF-8");
        Intrinsics.a((Object) forName, "Charset.forName(\"UTF-8\")");
        f6950a = forName;
        Intrinsics.a((Object) Charset.forName("UTF-16"), "Charset.forName(\"UTF-16\")");
        Intrinsics.a((Object) Charset.forName("UTF-16BE"), "Charset.forName(\"UTF-16BE\")");
        Intrinsics.a((Object) Charset.forName("UTF-16LE"), "Charset.forName(\"UTF-16LE\")");
        Intrinsics.a((Object) Charset.forName("US-ASCII"), "Charset.forName(\"US-ASCII\")");
        Intrinsics.a((Object) Charset.forName("ISO-8859-1"), "Charset.forName(\"ISO-8859-1\")");
    }

    private Charsets() {
    }

    public final Charset a() {
        Charset charset = c;
        if (charset != null) {
            return charset;
        }
        Charset forName = Charset.forName("UTF-32BE");
        Intrinsics.a((Object) forName, "Charset.forName(\"UTF-32BE\")");
        c = forName;
        return forName;
    }

    public final Charset b() {
        Charset charset = b;
        if (charset != null) {
            return charset;
        }
        Charset forName = Charset.forName("UTF-32LE");
        Intrinsics.a((Object) forName, "Charset.forName(\"UTF-32LE\")");
        b = forName;
        return forName;
    }
}
