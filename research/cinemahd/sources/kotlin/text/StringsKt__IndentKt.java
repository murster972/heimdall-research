package kotlin.text;

import java.util.ArrayList;
import java.util.List;
import kotlin.TypeCastException;
import kotlin.collections.CollectionsKt;
import kotlin.jvm.functions.Function1;
import kotlin.jvm.internal.Intrinsics;

class StringsKt__IndentKt extends StringsKt__AppendableKt {
    public static /* synthetic */ String a(String str, String str2, int i, Object obj) {
        if ((i & 1) != 0) {
            str2 = "|";
        }
        return a(str, str2);
    }

    public static final String a(String str, String str2) {
        Intrinsics.b(str, "$this$trimMargin");
        Intrinsics.b(str2, "marginPrefix");
        return a(str, "", str2);
    }

    public static final String a(String str, String str2, String str3) {
        int i;
        Intrinsics.b(str, "$this$replaceIndentByMargin");
        Intrinsics.b(str2, "newIndent");
        Intrinsics.b(str3, "marginPrefix");
        if (!StringsKt__StringsJVMKt.a((CharSequence) str3)) {
            List<String> e = StringsKt__StringsKt.e(str);
            int length = str.length() + (str2.length() * e.size());
            Function1<String, String> a2 = a(str2);
            int a3 = CollectionsKt__CollectionsKt.a(e);
            ArrayList arrayList = new ArrayList();
            int i2 = 0;
            for (T next : e) {
                int i3 = i2 + 1;
                String str4 = null;
                if (i2 >= 0) {
                    String str5 = (String) next;
                    if (!(i2 == 0 || i2 == a3) || !StringsKt__StringsJVMKt.a((CharSequence) str5)) {
                        int length2 = str5.length();
                        int i4 = 0;
                        while (true) {
                            if (i4 >= length2) {
                                i = -1;
                                break;
                            } else if (!CharsKt__CharJVMKt.a(str5.charAt(i4))) {
                                i = i4;
                                break;
                            } else {
                                i4++;
                            }
                        }
                        if (i != -1) {
                            int i5 = i;
                            if (StringsKt__StringsJVMKt.a(str5, str3, i, false, 4, (Object) null)) {
                                int length3 = i5 + str3.length();
                                if (str5 != null) {
                                    str4 = str5.substring(length3);
                                    Intrinsics.a((Object) str4, "(this as java.lang.String).substring(startIndex)");
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            }
                        }
                        if (str4 == null || (str4 = a2.invoke(str4)) == null) {
                            str4 = str5;
                        }
                    }
                    if (str4 != null) {
                        arrayList.add(str4);
                    }
                    i2 = i3;
                } else {
                    CollectionsKt.c();
                    throw null;
                }
            }
            StringBuilder sb = new StringBuilder(length);
            Appendable unused = CollectionsKt___CollectionsKt.a(arrayList, sb, ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Function1) null, 124, (Object) null);
            String sb2 = sb.toString();
            Intrinsics.a((Object) sb2, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
            return sb2;
        }
        throw new IllegalArgumentException("marginPrefix must be non-blank string.".toString());
    }

    private static final Function1<String, String> a(String str) {
        if (str.length() == 0) {
            return StringsKt__IndentKt$getIndentFunction$1.f6953a;
        }
        return new StringsKt__IndentKt$getIndentFunction$2(str);
    }
}
