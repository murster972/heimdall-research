package kotlin.text;

import java.util.Iterator;
import java.util.NoSuchElementException;
import kotlin.TypeCastException;
import kotlin.jvm.internal.markers.KMappedMarker;
import kotlin.ranges.IntRange;

public final class DelimitedRangesSequence$iterator$1 implements Iterator<IntRange>, KMappedMarker {

    /* renamed from: a  reason: collision with root package name */
    private int f6952a = -1;
    private int b;
    private int c;
    private IntRange d;
    private int e;
    final /* synthetic */ DelimitedRangesSequence f;

    DelimitedRangesSequence$iterator$1(DelimitedRangesSequence delimitedRangesSequence) {
        this.f = delimitedRangesSequence;
        this.b = RangesKt___RangesKt.a(delimitedRangesSequence.b, 0, delimitedRangesSequence.f6951a.length());
        this.c = this.b;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0023, code lost:
        if (r6.e < r6.f.c) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private final void a() {
        /*
            r6 = this;
            int r0 = r6.c
            r1 = 0
            if (r0 >= 0) goto L_0x000c
            r6.f6952a = r1
            r0 = 0
            r6.d = r0
            goto L_0x00a2
        L_0x000c:
            kotlin.text.DelimitedRangesSequence r0 = r6.f
            int r0 = r0.c
            r2 = -1
            r3 = 1
            if (r0 <= 0) goto L_0x0025
            int r0 = r6.e
            int r0 = r0 + r3
            r6.e = r0
            int r0 = r6.e
            kotlin.text.DelimitedRangesSequence r4 = r6.f
            int r4 = r4.c
            if (r0 >= r4) goto L_0x0033
        L_0x0025:
            int r0 = r6.c
            kotlin.text.DelimitedRangesSequence r4 = r6.f
            java.lang.CharSequence r4 = r4.f6951a
            int r4 = r4.length()
            if (r0 <= r4) goto L_0x0049
        L_0x0033:
            int r0 = r6.b
            kotlin.ranges.IntRange r1 = new kotlin.ranges.IntRange
            kotlin.text.DelimitedRangesSequence r4 = r6.f
            java.lang.CharSequence r4 = r4.f6951a
            int r4 = kotlin.text.StringsKt__StringsKt.c(r4)
            r1.<init>(r0, r4)
            r6.d = r1
            r6.c = r2
            goto L_0x00a0
        L_0x0049:
            kotlin.text.DelimitedRangesSequence r0 = r6.f
            kotlin.jvm.functions.Function2 r0 = r0.d
            kotlin.text.DelimitedRangesSequence r4 = r6.f
            java.lang.CharSequence r4 = r4.f6951a
            int r5 = r6.c
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            java.lang.Object r0 = r0.a(r4, r5)
            kotlin.Pair r0 = (kotlin.Pair) r0
            if (r0 != 0) goto L_0x0079
            int r0 = r6.b
            kotlin.ranges.IntRange r1 = new kotlin.ranges.IntRange
            kotlin.text.DelimitedRangesSequence r4 = r6.f
            java.lang.CharSequence r4 = r4.f6951a
            int r4 = kotlin.text.StringsKt__StringsKt.c(r4)
            r1.<init>(r0, r4)
            r6.d = r1
            r6.c = r2
            goto L_0x00a0
        L_0x0079:
            java.lang.Object r2 = r0.a()
            java.lang.Number r2 = (java.lang.Number) r2
            int r2 = r2.intValue()
            java.lang.Object r0 = r0.b()
            java.lang.Number r0 = (java.lang.Number) r0
            int r0 = r0.intValue()
            int r4 = r6.b
            kotlin.ranges.IntRange r4 = kotlin.ranges.RangesKt___RangesKt.d(r4, r2)
            r6.d = r4
            int r2 = r2 + r0
            r6.b = r2
            int r2 = r6.b
            if (r0 != 0) goto L_0x009d
            r1 = 1
        L_0x009d:
            int r2 = r2 + r1
            r6.c = r2
        L_0x00a0:
            r6.f6952a = r3
        L_0x00a2:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: kotlin.text.DelimitedRangesSequence$iterator$1.a():void");
    }

    public boolean hasNext() {
        if (this.f6952a == -1) {
            a();
        }
        return this.f6952a == 1;
    }

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }

    public IntRange next() {
        if (this.f6952a == -1) {
            a();
        }
        if (this.f6952a != 0) {
            IntRange intRange = this.d;
            if (intRange != null) {
                this.d = null;
                this.f6952a = -1;
                return intRange;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.ranges.IntRange");
        }
        throw new NoSuchElementException();
    }
}
