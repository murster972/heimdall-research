package kotlin.text;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import kotlin.Pair;
import kotlin.TuplesKt;
import kotlin.jvm.internal.Intrinsics;
import kotlin.ranges.IntProgression;
import kotlin.ranges.IntRange;
import kotlin.sequences.Sequence;

class StringsKt__StringsKt extends StringsKt__StringsJVMKt {
    public static final IntRange b(CharSequence charSequence) {
        Intrinsics.b(charSequence, "$this$indices");
        return new IntRange(0, charSequence.length() - 1);
    }

    public static final int c(CharSequence charSequence) {
        Intrinsics.b(charSequence, "$this$lastIndex");
        return charSequence.length() - 1;
    }

    public static final Sequence<String> d(CharSequence charSequence) {
        Intrinsics.b(charSequence, "$this$lineSequence");
        return a(charSequence, new String[]{"\r\n", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "\r"}, false, 0, 6, (Object) null);
    }

    public static final List<String> e(CharSequence charSequence) {
        Intrinsics.b(charSequence, "$this$lines");
        return SequencesKt___SequencesKt.b(d(charSequence));
    }

    public static CharSequence f(CharSequence charSequence) {
        Intrinsics.b(charSequence, "$this$trim");
        int length = charSequence.length() - 1;
        int i = 0;
        boolean z = false;
        while (i <= length) {
            boolean a2 = CharsKt__CharJVMKt.a(charSequence.charAt(!z ? i : length));
            if (!z) {
                if (!a2) {
                    z = true;
                } else {
                    i++;
                }
            } else if (!a2) {
                break;
            } else {
                length--;
            }
        }
        return charSequence.subSequence(i, length + 1);
    }

    public static final String a(CharSequence charSequence, IntRange intRange) {
        Intrinsics.b(charSequence, "$this$substring");
        Intrinsics.b(intRange, "range");
        return charSequence.subSequence(intRange.c().intValue(), intRange.b().intValue() + 1).toString();
    }

    public static String b(String str, CharSequence charSequence) {
        Intrinsics.b(str, "$this$removeSuffix");
        Intrinsics.b(charSequence, "suffix");
        if (!b((CharSequence) str, charSequence, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(0, str.length() - charSequence.length());
        Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static String c(String str, CharSequence charSequence) {
        Intrinsics.b(str, "$this$removeSurrounding");
        Intrinsics.b(charSequence, "delimiter");
        return a(str, charSequence, charSequence);
    }

    public static /* synthetic */ String a(String str, char c, String str2, int i, Object obj) {
        if ((i & 2) != 0) {
            str2 = str;
        }
        return a(str, c, str2);
    }

    public static /* synthetic */ boolean c(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return c(charSequence, charSequence2, z);
    }

    public static final String a(String str, char c, String str2) {
        Intrinsics.b(str, "$this$substringAfterLast");
        Intrinsics.b(str2, "missingDelimiterValue");
        int b = b((CharSequence) str, c, 0, false, 6, (Object) null);
        if (b == -1) {
            return str2;
        }
        String substring = str.substring(b + 1, str.length());
        Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static /* synthetic */ boolean b(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return b(charSequence, charSequence2, z);
    }

    public static final boolean c(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Intrinsics.b(charSequence, "$this$startsWith");
        Intrinsics.b(charSequence2, "prefix");
        if (!z && (charSequence instanceof String) && (charSequence2 instanceof String)) {
            return StringsKt__StringsJVMKt.b((String) charSequence, (String) charSequence2, false, 2, (Object) null);
        }
        return a(charSequence, 0, charSequence2, 0, charSequence2.length(), z);
    }

    public static final boolean b(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Intrinsics.b(charSequence, "$this$endsWith");
        Intrinsics.b(charSequence2, "suffix");
        if (!z && (charSequence instanceof String) && (charSequence2 instanceof String)) {
            return StringsKt__StringsJVMKt.a((String) charSequence, (String) charSequence2, false, 2, (Object) null);
        }
        return a(charSequence, charSequence.length() - charSequence2.length(), charSequence2, 0, charSequence2.length(), z);
    }

    public static String a(String str, CharSequence charSequence) {
        Intrinsics.b(str, "$this$removePrefix");
        Intrinsics.b(charSequence, "prefix");
        if (!c(str, charSequence, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length());
        Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
        return substring;
    }

    public static final String a(String str, CharSequence charSequence, CharSequence charSequence2) {
        Intrinsics.b(str, "$this$removeSurrounding");
        Intrinsics.b(charSequence, "prefix");
        Intrinsics.b(charSequence2, "suffix");
        if (str.length() < charSequence.length() + charSequence2.length() || !c(str, charSequence, false, 2, (Object) null) || !b((CharSequence) str, charSequence2, false, 2, (Object) null)) {
            return str;
        }
        String substring = str.substring(charSequence.length(), str.length() - charSequence2.length());
        Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
        return substring;
    }

    public static final int b(CharSequence charSequence, char[] cArr, int i, boolean z) {
        Intrinsics.b(charSequence, "$this$lastIndexOfAny");
        Intrinsics.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            for (int b = RangesKt___RangesKt.b(i, c(charSequence)); b >= 0; b--) {
                char charAt = charSequence.charAt(b);
                int length = cArr.length;
                boolean z2 = false;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (CharsKt__CharKt.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return b;
                }
            }
            return -1;
        }
        return ((String) charSequence).lastIndexOf(ArraysKt___ArraysKt.a(cArr), i);
    }

    public static final boolean a(CharSequence charSequence, int i, CharSequence charSequence2, int i2, int i3, boolean z) {
        Intrinsics.b(charSequence, "$this$regionMatchesImpl");
        Intrinsics.b(charSequence2, "other");
        if (i2 < 0 || i < 0 || i > charSequence.length() - i3 || i2 > charSequence2.length() - i3) {
            return false;
        }
        for (int i4 = 0; i4 < i3; i4++) {
            if (!CharsKt__CharKt.a(charSequence.charAt(i + i4), charSequence2.charAt(i2 + i4), z)) {
                return false;
            }
        }
        return true;
    }

    public static final int a(CharSequence charSequence, char[] cArr, int i, boolean z) {
        boolean z2;
        Intrinsics.b(charSequence, "$this$indexOfAny");
        Intrinsics.b(cArr, "chars");
        if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            int a2 = RangesKt___RangesKt.a(i, 0);
            int c = c(charSequence);
            if (a2 > c) {
                return -1;
            }
            while (true) {
                char charAt = charSequence.charAt(a2);
                int length = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        z2 = false;
                        break;
                    } else if (CharsKt__CharKt.a(cArr[i2], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                if (z2) {
                    return a2;
                }
                if (a2 == c) {
                    return -1;
                }
                a2++;
            }
        } else {
            return ((String) charSequence).indexOf(ArraysKt___ArraysKt.a(cArr), i);
        }
    }

    /* access modifiers changed from: private */
    public static final Pair<Integer, String> b(CharSequence charSequence, Collection<String> collection, int i, boolean z, boolean z2) {
        T t;
        T t2;
        if (z || collection.size() != 1) {
            IntProgression intRange = !z2 ? new IntRange(RangesKt___RangesKt.a(i, 0), charSequence.length()) : RangesKt___RangesKt.c(RangesKt___RangesKt.b(i, c(charSequence)), 0);
            if (charSequence instanceof String) {
                int first = intRange.getFirst();
                int last = intRange.getLast();
                int a2 = intRange.a();
                if (a2 < 0 ? first >= last : first <= last) {
                    while (true) {
                        Iterator<T> it2 = collection.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                t2 = null;
                                break;
                            }
                            t2 = it2.next();
                            String str = (String) t2;
                            if (StringsKt__StringsJVMKt.a(str, 0, (String) charSequence, first, str.length(), z)) {
                                break;
                            }
                        }
                        String str2 = (String) t2;
                        if (str2 == null) {
                            if (first == last) {
                                break;
                            }
                            first += a2;
                        } else {
                            return TuplesKt.a(Integer.valueOf(first), str2);
                        }
                    }
                }
            } else {
                int first2 = intRange.getFirst();
                int last2 = intRange.getLast();
                int a3 = intRange.a();
                if (a3 < 0 ? first2 >= last2 : first2 <= last2) {
                    while (true) {
                        Iterator<T> it3 = collection.iterator();
                        while (true) {
                            if (!it3.hasNext()) {
                                t = null;
                                break;
                            }
                            t = it3.next();
                            String str3 = (String) t;
                            if (a((CharSequence) str3, 0, charSequence, first2, str3.length(), z)) {
                                break;
                            }
                        }
                        String str4 = (String) t;
                        if (str4 == null) {
                            if (first2 == last2) {
                                break;
                            }
                            first2 += a3;
                        } else {
                            return TuplesKt.a(Integer.valueOf(first2), str4);
                        }
                    }
                }
            }
            return null;
        }
        String str5 = (String) CollectionsKt___CollectionsKt.b(collection);
        CharSequence charSequence2 = charSequence;
        String str6 = str5;
        int i2 = i;
        int a4 = !z2 ? a(charSequence2, str6, i2, false, 4, (Object) null) : b(charSequence2, str6, i2, false, 4, (Object) null);
        if (a4 < 0) {
            return null;
        }
        return TuplesKt.a(Integer.valueOf(a4), str5);
    }

    static /* synthetic */ int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2, int i3, Object obj) {
        return a(charSequence, charSequence2, i, i2, z, (i3 & 16) != 0 ? false : z2);
    }

    private static final int a(CharSequence charSequence, CharSequence charSequence2, int i, int i2, boolean z, boolean z2) {
        IntProgression intProgression;
        if (!z2) {
            intProgression = new IntRange(RangesKt___RangesKt.a(i, 0), RangesKt___RangesKt.b(i2, charSequence.length()));
        } else {
            intProgression = RangesKt___RangesKt.c(RangesKt___RangesKt.b(i, c(charSequence)), RangesKt___RangesKt.a(i2, 0));
        }
        if (!(charSequence instanceof String) || !(charSequence2 instanceof String)) {
            int first = intProgression.getFirst();
            int last = intProgression.getLast();
            int a2 = intProgression.a();
            if (a2 >= 0) {
                if (first > last) {
                    return -1;
                }
            } else if (first < last) {
                return -1;
            }
            while (true) {
                if (a(charSequence2, 0, charSequence, first, charSequence2.length(), z)) {
                    return first;
                }
                if (first == last) {
                    return -1;
                }
                first += a2;
            }
        } else {
            int first2 = intProgression.getFirst();
            int last2 = intProgression.getLast();
            int a3 = intProgression.a();
            if (a3 >= 0) {
                if (first2 > last2) {
                    return -1;
                }
            } else if (first2 < last2) {
                return -1;
            }
            while (true) {
                if (StringsKt__StringsJVMKt.a((String) charSequence2, 0, (String) charSequence, first2, charSequence2.length(), z)) {
                    return first2;
                }
                if (first2 == last2) {
                    return -1;
                }
                first2 += a3;
            }
        }
    }

    public static /* synthetic */ int a(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return a(charSequence, c, i, z);
    }

    public static final int a(CharSequence charSequence, char c, int i, boolean z) {
        Intrinsics.b(charSequence, "$this$indexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c, i);
        }
        return a(charSequence, new char[]{c}, i, z);
    }

    public static /* synthetic */ int b(CharSequence charSequence, char c, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = c(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b(charSequence, c, i, z);
    }

    public static final int b(CharSequence charSequence, char c, int i, boolean z) {
        Intrinsics.b(charSequence, "$this$lastIndexOf");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c, i);
        }
        return b(charSequence, new char[]{c}, i, z);
    }

    public static /* synthetic */ int a(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = 0;
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return a(charSequence, str, i, z);
    }

    public static final int a(CharSequence charSequence, String str, int i, boolean z) {
        Intrinsics.b(charSequence, "$this$indexOf");
        Intrinsics.b(str, "string");
        if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(str, i);
        }
        return a(charSequence, str, i, charSequence.length(), z, false, 16, (Object) null);
    }

    public static /* synthetic */ int b(CharSequence charSequence, String str, int i, boolean z, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            i = c(charSequence);
        }
        if ((i2 & 4) != 0) {
            z = false;
        }
        return b(charSequence, str, i, z);
    }

    public static final int b(CharSequence charSequence, String str, int i, boolean z) {
        Intrinsics.b(charSequence, "$this$lastIndexOf");
        Intrinsics.b(str, "string");
        if (z || !(charSequence instanceof String)) {
            return a(charSequence, (CharSequence) str, i, 0, z, true);
        }
        return ((String) charSequence).lastIndexOf(str, i);
    }

    public static /* synthetic */ boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(charSequence, charSequence2, z);
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        Intrinsics.b(charSequence, "$this$contains");
        Intrinsics.b(charSequence2, "other");
        if (charSequence2 instanceof String) {
            if (a(charSequence, (String) charSequence2, 0, z, 2, (Object) null) >= 0) {
                return true;
            }
        } else {
            if (a(charSequence, charSequence2, 0, charSequence.length(), z, false, 16, (Object) null) >= 0) {
                return true;
            }
        }
        return false;
    }

    public static /* synthetic */ Sequence b(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return b(charSequence, cArr, z, i);
    }

    public static final Sequence<String> b(CharSequence charSequence, char[] cArr, boolean z, int i) {
        Intrinsics.b(charSequence, "$this$splitToSequence");
        Intrinsics.b(cArr, "delimiters");
        return SequencesKt___SequencesKt.a(a(charSequence, cArr, 0, z, i, 2, (Object) null), new StringsKt__StringsKt$splitToSequence$2(charSequence));
    }

    public static /* synthetic */ boolean a(CharSequence charSequence, char c, boolean z, int i, Object obj) {
        if ((i & 2) != 0) {
            z = false;
        }
        return a(charSequence, c, z);
    }

    public static final boolean a(CharSequence charSequence, char c, boolean z) {
        Intrinsics.b(charSequence, "$this$contains");
        return a(charSequence, c, 0, z, 2, (Object) null) >= 0;
    }

    static /* synthetic */ Sequence a(CharSequence charSequence, char[] cArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return a(charSequence, cArr, i, z, i2);
    }

    private static final Sequence<IntRange> a(CharSequence charSequence, char[] cArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new DelimitedRangesSequence(charSequence, i, i2, new StringsKt__StringsKt$rangesDelimitedBy$2(cArr, z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    static /* synthetic */ Sequence a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2, int i3, Object obj) {
        if ((i3 & 2) != 0) {
            i = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if ((i3 & 8) != 0) {
            i2 = 0;
        }
        return a(charSequence, strArr, i, z, i2);
    }

    private static final Sequence<IntRange> a(CharSequence charSequence, String[] strArr, int i, boolean z, int i2) {
        if (i2 >= 0) {
            return new DelimitedRangesSequence(charSequence, i, i2, new StringsKt__StringsKt$rangesDelimitedBy$4(ArraysKt___ArraysJvmKt.a(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    public static /* synthetic */ Sequence a(CharSequence charSequence, String[] strArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a(charSequence, strArr, z, i);
    }

    public static final Sequence<String> a(CharSequence charSequence, String[] strArr, boolean z, int i) {
        Intrinsics.b(charSequence, "$this$splitToSequence");
        Intrinsics.b(strArr, "delimiters");
        return SequencesKt___SequencesKt.a(a(charSequence, strArr, 0, z, i, 2, (Object) null), new StringsKt__StringsKt$splitToSequence$1(charSequence));
    }

    public static /* synthetic */ List a(CharSequence charSequence, char[] cArr, boolean z, int i, int i2, Object obj) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if ((i2 & 4) != 0) {
            i = 0;
        }
        return a(charSequence, cArr, z, i);
    }

    public static final List<String> a(CharSequence charSequence, char[] cArr, boolean z, int i) {
        Intrinsics.b(charSequence, "$this$split");
        Intrinsics.b(cArr, "delimiters");
        if (cArr.length == 1) {
            return a(charSequence, String.valueOf(cArr[0]), z, i);
        }
        Iterable<IntRange> a2 = SequencesKt___SequencesKt.a(a(charSequence, cArr, 0, z, i, 2, (Object) null));
        ArrayList arrayList = new ArrayList(CollectionsKt__IterablesKt.a(a2, 10));
        for (IntRange a3 : a2) {
            arrayList.add(a(charSequence, a3));
        }
        return arrayList;
    }

    private static final List<String> a(CharSequence charSequence, String str, boolean z, int i) {
        int i2 = 0;
        if (i >= 0) {
            int a2 = a(charSequence, str, 0, z);
            if (a2 == -1 || i == 1) {
                return CollectionsKt__CollectionsJVMKt.a(charSequence.toString());
            }
            boolean z2 = i > 0;
            int i3 = 10;
            if (z2) {
                i3 = RangesKt___RangesKt.b(i, 10);
            }
            ArrayList arrayList = new ArrayList(i3);
            do {
                arrayList.add(charSequence.subSequence(i2, a2).toString());
                i2 = str.length() + a2;
                if ((z2 && arrayList.size() == i - 1) || (a2 = a(charSequence, str, i2, z)) == -1) {
                    arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
                }
                arrayList.add(charSequence.subSequence(i2, a2).toString());
                i2 = str.length() + a2;
                break;
            } while ((a2 = a(charSequence, str, i2, z)) == -1);
            arrayList.add(charSequence.subSequence(i2, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i + '.').toString());
    }
}
