package kotlin.text;

import kotlin.jvm.internal.Intrinsics;

class StringsKt__StringNumberConversionsKt extends StringsKt__StringNumberConversionsJVMKt {
    public static Integer a(String str) {
        Intrinsics.b(str, "$this$toIntOrNull");
        return a(str, 10);
    }

    public static final Integer a(String str, int i) {
        boolean z;
        int i2;
        Intrinsics.b(str, "$this$toIntOrNull");
        int unused = CharsKt__CharJVMKt.a(i);
        int length = str.length();
        if (length == 0) {
            return null;
        }
        int i3 = 0;
        char charAt = str.charAt(0);
        int i4 = -2147483647;
        int i5 = 1;
        if (charAt >= '0') {
            z = false;
            i5 = 0;
        } else if (length == 1) {
            return null;
        } else {
            if (charAt == '-') {
                i4 = Integer.MIN_VALUE;
                z = true;
            } else if (charAt != '+') {
                return null;
            } else {
                z = false;
            }
        }
        int i6 = -59652323;
        while (i5 < length) {
            int a2 = CharsKt__CharJVMKt.a(str.charAt(i5), i);
            if (a2 < 0) {
                return null;
            }
            if ((i3 < i6 && (i6 != -59652323 || i3 < (i6 = i4 / i))) || (i2 = i3 * i) < i4 + a2) {
                return null;
            }
            i3 = i2 - a2;
            i5++;
        }
        return z ? Integer.valueOf(i3) : Integer.valueOf(-i3);
    }
}
