package kotlin.reflect;

public interface KProperty<R> extends KCallable<R> {

    public interface Accessor<R> {
    }

    public interface Getter<R> extends Accessor<R>, KFunction<R> {
    }

    boolean isConst();

    boolean isLateinit();
}
