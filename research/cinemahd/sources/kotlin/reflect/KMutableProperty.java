package kotlin.reflect;

import kotlin.Unit;
import kotlin.reflect.KProperty;

public interface KMutableProperty<R> extends KProperty<R> {

    public interface Setter<R> extends KProperty.Accessor<R>, KFunction<Unit> {
    }
}
