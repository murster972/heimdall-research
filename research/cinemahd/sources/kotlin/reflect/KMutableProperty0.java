package kotlin.reflect;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import kotlin.reflect.KMutableProperty;

public interface KMutableProperty0<R> extends KProperty0<R>, KMutableProperty<R> {

    public interface Setter<R> extends KMutableProperty.Setter<R>, Function1<R, Unit> {
    }

    Setter<R> getSetter();
}
