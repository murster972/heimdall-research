package kotlin.reflect;

public interface KClass<T> extends KDeclarationContainer, KAnnotatedElement, KClassifier {
}
