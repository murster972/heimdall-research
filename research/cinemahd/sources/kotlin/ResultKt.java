package kotlin;

import kotlin.Result;
import kotlin.jvm.internal.Intrinsics;

public final class ResultKt {
    public static final Object a(Throwable th) {
        Intrinsics.b(th, "exception");
        return new Result.Failure(th);
    }
}
