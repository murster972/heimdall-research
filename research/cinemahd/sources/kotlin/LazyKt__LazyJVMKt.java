package kotlin;

import kotlin.jvm.functions.Function0;
import kotlin.jvm.internal.Intrinsics;

class LazyKt__LazyJVMKt {
    public static <T> Lazy<T> a(Function0<? extends T> function0) {
        Intrinsics.b(function0, "initializer");
        return new SynchronizedLazyImpl(function0, (Object) null, 2, (DefaultConstructorMarker) null);
    }
}
