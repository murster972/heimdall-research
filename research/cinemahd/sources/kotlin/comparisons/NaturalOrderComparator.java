package kotlin.comparisons;

import java.util.Comparator;
import kotlin.jvm.internal.Intrinsics;

final class NaturalOrderComparator implements Comparator<Comparable<? super Object>> {

    /* renamed from: a  reason: collision with root package name */
    public static final NaturalOrderComparator f6926a = new NaturalOrderComparator();

    private NaturalOrderComparator() {
    }

    /* renamed from: a */
    public int compare(Comparable<Object> comparable, Comparable<Object> comparable2) {
        Intrinsics.b(comparable, "a");
        Intrinsics.b(comparable2, "b");
        return comparable.compareTo(comparable2);
    }

    public final Comparator<Comparable<Object>> reversed() {
        return ReverseOrderComparator.f6927a;
    }
}
