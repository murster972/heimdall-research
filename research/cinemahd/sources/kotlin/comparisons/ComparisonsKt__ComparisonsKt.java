package kotlin.comparisons;

import java.util.Comparator;
import kotlin.TypeCastException;

class ComparisonsKt__ComparisonsKt {
    public static <T extends Comparable<?>> int a(T t, T t2) {
        if (t == t2) {
            return 0;
        }
        if (t == null) {
            return -1;
        }
        if (t2 == null) {
            return 1;
        }
        return t.compareTo(t2);
    }

    public static <T extends Comparable<? super T>> Comparator<T> a() {
        NaturalOrderComparator naturalOrderComparator = NaturalOrderComparator.f6926a;
        if (naturalOrderComparator != null) {
            return naturalOrderComparator;
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.Comparator<T> /* = java.util.Comparator<T> */");
    }
}
