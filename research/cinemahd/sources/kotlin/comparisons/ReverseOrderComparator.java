package kotlin.comparisons;

import java.util.Comparator;
import kotlin.jvm.internal.Intrinsics;

final class ReverseOrderComparator implements Comparator<Comparable<? super Object>> {

    /* renamed from: a  reason: collision with root package name */
    public static final ReverseOrderComparator f6927a = new ReverseOrderComparator();

    private ReverseOrderComparator() {
    }

    /* renamed from: a */
    public int compare(Comparable<Object> comparable, Comparable<Object> comparable2) {
        Intrinsics.b(comparable, "a");
        Intrinsics.b(comparable2, "b");
        return comparable2.compareTo(comparable);
    }

    public final Comparator<Comparable<Object>> reversed() {
        return NaturalOrderComparator.f6926a;
    }
}
