package kotlin.coroutines;

public interface CoroutineContext {

    public interface Element extends CoroutineContext {
    }

    public interface Key<E extends Element> {
    }

    <E extends Element> E a(Key<E> key);
}
