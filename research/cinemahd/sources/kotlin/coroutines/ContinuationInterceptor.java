package kotlin.coroutines;

import kotlin.coroutines.CoroutineContext;

public interface ContinuationInterceptor extends CoroutineContext.Element {

    /* renamed from: a  reason: collision with root package name */
    public static final Key f6928a = Key.f6929a;

    public static final class Key implements CoroutineContext.Key<ContinuationInterceptor> {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ Key f6929a = new Key();

        private Key() {
        }
    }

    void a(Continuation<?> continuation);

    <T> Continuation<T> b(Continuation<? super T> continuation);
}
