package kotlin.coroutines.jvm.internal;

import com.unity3d.ads.metadata.MediationMetaData;
import java.lang.reflect.Method;
import kotlin.jvm.internal.Intrinsics;

final class ModuleNameRetriever {

    /* renamed from: a  reason: collision with root package name */
    private static final Cache f6932a = new Cache((Method) null, (Method) null, (Method) null);
    public static Cache b;
    public static final ModuleNameRetriever c = new ModuleNameRetriever();

    private static final class Cache {

        /* renamed from: a  reason: collision with root package name */
        public final Method f6933a;
        public final Method b;
        public final Method c;

        public Cache(Method method, Method method2, Method method3) {
            this.f6933a = method;
            this.b = method2;
            this.c = method3;
        }
    }

    private ModuleNameRetriever() {
    }

    private final Cache b(BaseContinuationImpl baseContinuationImpl) {
        try {
            Cache cache = new Cache(Class.class.getDeclaredMethod("getModule", new Class[0]), baseContinuationImpl.getClass().getClassLoader().loadClass("java.lang.Module").getDeclaredMethod("getDescriptor", new Class[0]), baseContinuationImpl.getClass().getClassLoader().loadClass("java.lang.module.ModuleDescriptor").getDeclaredMethod(MediationMetaData.KEY_NAME, new Class[0]));
            b = cache;
            return cache;
        } catch (Exception unused) {
            Cache cache2 = f6932a;
            b = cache2;
            return cache2;
        }
    }

    public final String a(BaseContinuationImpl baseContinuationImpl) {
        Method method;
        Object invoke;
        Method method2;
        Object invoke2;
        Intrinsics.b(baseContinuationImpl, "continuation");
        Cache cache = b;
        if (cache == null) {
            cache = b(baseContinuationImpl);
        }
        if (cache == f6932a || (method = cache.f6933a) == null || (invoke = method.invoke(baseContinuationImpl.getClass(), new Object[0])) == null || (method2 = cache.b) == null || (invoke2 = method2.invoke(invoke, new Object[0])) == null) {
            return null;
        }
        Method method3 = cache.c;
        Object invoke3 = method3 != null ? method3.invoke(invoke2, new Object[0]) : null;
        if (!(invoke3 instanceof String)) {
            invoke3 = null;
        }
        return (String) invoke3;
    }
}
