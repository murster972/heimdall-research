package kotlin.coroutines.jvm.internal;

import kotlin.coroutines.Continuation;
import kotlin.jvm.internal.Intrinsics;

public final class DebugProbesKt {
    public static final void a(Continuation<?> continuation) {
        Intrinsics.b(continuation, "frame");
    }

    public static final void b(Continuation<?> continuation) {
        Intrinsics.b(continuation, "frame");
    }
}
