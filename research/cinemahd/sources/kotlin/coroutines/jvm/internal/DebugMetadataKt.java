package kotlin.coroutines.jvm.internal;

import java.lang.reflect.Field;
import kotlin.jvm.internal.Intrinsics;

public final class DebugMetadataKt {
    private static final DebugMetadata a(BaseContinuationImpl baseContinuationImpl) {
        return (DebugMetadata) baseContinuationImpl.getClass().getAnnotation(DebugMetadata.class);
    }

    private static final int b(BaseContinuationImpl baseContinuationImpl) {
        try {
            Field declaredField = baseContinuationImpl.getClass().getDeclaredField("label");
            Intrinsics.a((Object) declaredField, "field");
            declaredField.setAccessible(true);
            Object obj = declaredField.get(baseContinuationImpl);
            if (!(obj instanceof Integer)) {
                obj = null;
            }
            Integer num = (Integer) obj;
            return (num != null ? num.intValue() : 0) - 1;
        } catch (Exception unused) {
            return -1;
        }
    }

    public static final StackTraceElement c(BaseContinuationImpl baseContinuationImpl) {
        int i;
        String str;
        Intrinsics.b(baseContinuationImpl, "$this$getStackTraceElementImpl");
        DebugMetadata a2 = a(baseContinuationImpl);
        if (a2 == null) {
            return null;
        }
        a(1, a2.v());
        int b = b(baseContinuationImpl);
        if (b < 0) {
            i = -1;
        } else {
            i = a2.l()[b];
        }
        String a3 = ModuleNameRetriever.c.a(baseContinuationImpl);
        if (a3 == null) {
            str = a2.c();
        } else {
            str = a3 + '/' + a2.c();
        }
        return new StackTraceElement(str, a2.m(), a2.f(), i);
    }

    private static final void a(int i, int i2) {
        if (i2 > i) {
            throw new IllegalStateException(("Debug metadata version mismatch. Expected: " + i + ", got " + i2 + ". Please update the Kotlin standard library.").toString());
        }
    }
}
