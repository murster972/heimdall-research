package retrofit2;

import java.lang.reflect.Method;
import kotlin.KotlinNullPointerException;
import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CancellableContinuation;

public final class KotlinExtensions$await$2$2 implements Callback<T> {
    final /* synthetic */ CancellableContinuation $continuation;

    KotlinExtensions$await$2$2(CancellableContinuation cancellableContinuation) {
        this.$continuation = cancellableContinuation;
    }

    public void onFailure(Call<T> call, Throwable th) {
        Intrinsics.b(call, "call");
        Intrinsics.b(th, "t");
        Result.Companion companion = Result.f6915a;
        Object a2 = ResultKt.a(th);
        Result.a(a2);
        this.$continuation.resumeWith(a2);
    }

    public void onResponse(Call<T> call, Response<T> response) {
        Intrinsics.b(call, "call");
        Intrinsics.b(response, "response");
        if (response.isSuccessful()) {
            T body = response.body();
            if (body == null) {
                Object tag = call.request().tag(Invocation.class);
                if (tag != null) {
                    Intrinsics.a(tag, "call.request().tag(Invocation::class.java)!!");
                    Method method = ((Invocation) tag).method();
                    StringBuilder sb = new StringBuilder();
                    sb.append("Response from ");
                    Intrinsics.a((Object) method, "method");
                    Class<?> declaringClass = method.getDeclaringClass();
                    Intrinsics.a((Object) declaringClass, "method.declaringClass");
                    sb.append(declaringClass.getName());
                    sb.append('.');
                    sb.append(method.getName());
                    sb.append(" was null but response body type was declared as non-null");
                    KotlinNullPointerException kotlinNullPointerException = new KotlinNullPointerException(sb.toString());
                    Result.Companion companion = Result.f6915a;
                    Object a2 = ResultKt.a(kotlinNullPointerException);
                    Result.a(a2);
                    this.$continuation.resumeWith(a2);
                    return;
                }
                Intrinsics.a();
                throw null;
            }
            Result.Companion companion2 = Result.f6915a;
            Result.a(body);
            this.$continuation.resumeWith(body);
            return;
        }
        HttpException httpException = new HttpException(response);
        Result.Companion companion3 = Result.f6915a;
        Object a3 = ResultKt.a(httpException);
        Result.a(a3);
        this.$continuation.resumeWith(a3);
    }
}
