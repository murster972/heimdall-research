package retrofit2;

import kotlin.Result;
import kotlin.ResultKt;
import kotlin.jvm.internal.Intrinsics;
import kotlinx.coroutines.CancellableContinuation;

public final class KotlinExtensions$await$4$2 implements Callback<T> {
    final /* synthetic */ CancellableContinuation $continuation;

    KotlinExtensions$await$4$2(CancellableContinuation cancellableContinuation) {
        this.$continuation = cancellableContinuation;
    }

    public void onFailure(Call<T> call, Throwable th) {
        Intrinsics.b(call, "call");
        Intrinsics.b(th, "t");
        Result.Companion companion = Result.f6915a;
        Object a2 = ResultKt.a(th);
        Result.a(a2);
        this.$continuation.resumeWith(a2);
    }

    public void onResponse(Call<T> call, Response<T> response) {
        Intrinsics.b(call, "call");
        Intrinsics.b(response, "response");
        if (response.isSuccessful()) {
            T body = response.body();
            Result.Companion companion = Result.f6915a;
            Result.a(body);
            this.$continuation.resumeWith(body);
            return;
        }
        HttpException httpException = new HttpException(response);
        Result.Companion companion2 = Result.f6915a;
        Object a2 = ResultKt.a(httpException);
        Result.a(a2);
        this.$continuation.resumeWith(a2);
    }
}
