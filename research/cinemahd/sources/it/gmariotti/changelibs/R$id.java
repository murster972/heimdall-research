package it.gmariotti.changelibs;

public final class R$id {
    public static final int chg_headerDate = 2131296496;
    public static final int chg_headerVersion = 2131296497;
    public static final int chg_row = 2131296498;
    public static final int chg_rowheader = 2131296499;
    public static final int chg_text = 2131296500;
    public static final int chg_textbullet = 2131296501;
    public static final int item_touch_helper_previous_elevation = 2131296721;

    private R$id() {
    }
}
