package it.gmariotti.changelibs.library.parser;

import android.content.Context;
import android.util.Log;
import android.util.Xml;
import it.gmariotti.changelibs.library.Constants;
import it.gmariotti.changelibs.library.Util;
import it.gmariotti.changelibs.library.internal.ChangeLog;
import it.gmariotti.changelibs.library.internal.ChangeLogException;
import it.gmariotti.changelibs.library.internal.ChangeLogRow;
import it.gmariotti.changelibs.library.internal.ChangeLogRowHeader;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class XmlParser extends BaseParser {
    private static String e = "XmlParser";
    private static List<String> f = new ArrayList<String>() {
        {
            add("changelogbug");
            add("changelogimprovement");
            add("changelogtext");
        }
    };
    private int c = Constants.f6901a;
    private String d = null;

    public XmlParser(Context context, int i) {
        super(context);
        this.c = i;
    }

    public ChangeLog a() throws Exception {
        InputStream inputStream;
        try {
            if (this.d != null) {
                inputStream = Util.a(this.f6910a) ? new URL(this.d).openStream() : null;
            } else {
                inputStream = this.f6910a.getResources().openRawResource(this.c);
            }
            if (inputStream != null) {
                XmlPullParser newPullParser = Xml.newPullParser();
                newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
                newPullParser.setInput(inputStream, (String) null);
                newPullParser.nextTag();
                ChangeLog changeLog = new ChangeLog();
                a(newPullParser, changeLog);
                inputStream.close();
                return changeLog;
            }
            Log.d(e, "Changelog.xml not found");
            throw new ChangeLogException("Changelog.xml not found");
        } catch (XmlPullParserException e2) {
            Log.d(e, "XmlPullParseException while parsing changelog file", e2);
            throw e2;
        } catch (IOException e3) {
            Log.d(e, "Error i/o with changelog.xml", e3);
            throw e3;
        }
    }

    /* access modifiers changed from: protected */
    public void b(XmlPullParser xmlPullParser, ChangeLog changeLog) throws Exception {
        if (xmlPullParser != null) {
            xmlPullParser.require(2, (String) null, "changelogversion");
            String attributeValue = xmlPullParser.getAttributeValue((String) null, "versionName");
            String attributeValue2 = xmlPullParser.getAttributeValue((String) null, "versionCode");
            int i = 0;
            if (attributeValue2 != null) {
                try {
                    i = Integer.parseInt(attributeValue2);
                } catch (NumberFormatException unused) {
                    Log.w(e, "Error while parsing versionCode.It must be a numeric value. Check you file.");
                }
            }
            String attributeValue3 = xmlPullParser.getAttributeValue((String) null, "changeDate");
            if (attributeValue != null) {
                ChangeLogRowHeader changeLogRowHeader = new ChangeLogRowHeader();
                changeLogRowHeader.e(attributeValue);
                changeLogRowHeader.b(attributeValue3);
                changeLog.a((ChangeLogRow) changeLogRowHeader);
                while (xmlPullParser.next() != 3) {
                    if (xmlPullParser.getEventType() == 2) {
                        if (f.contains(xmlPullParser.getName())) {
                            a(xmlPullParser, changeLog, attributeValue, i);
                        }
                    }
                }
                return;
            }
            throw new ChangeLogException("VersionName required in changeLogVersion node");
        }
    }

    public XmlParser(Context context, String str) {
        super(context);
        this.d = str;
    }

    /* access modifiers changed from: protected */
    public void a(XmlPullParser xmlPullParser, ChangeLog changeLog) throws Exception {
        if (xmlPullParser != null && changeLog != null) {
            xmlPullParser.require(2, (String) null, "changelog");
            String attributeValue = xmlPullParser.getAttributeValue((String) null, "bulletedList");
            if (attributeValue == null || attributeValue.equals("true")) {
                changeLog.a(true);
                this.b = true;
            } else {
                changeLog.a(false);
                this.b = false;
            }
            while (xmlPullParser.next() != 3) {
                if (xmlPullParser.getEventType() == 2 && xmlPullParser.getName().equals("changelogversion")) {
                    b(xmlPullParser, changeLog);
                }
            }
        }
    }

    private void a(XmlPullParser xmlPullParser, ChangeLog changeLog, String str, int i) throws Exception {
        if (xmlPullParser != null) {
            String name = xmlPullParser.getName();
            ChangeLogRow changeLogRow = new ChangeLogRow();
            changeLogRow.e(str);
            changeLogRow.b(i);
            String attributeValue = xmlPullParser.getAttributeValue((String) null, "changeTextTitle");
            if (attributeValue != null) {
                changeLogRow.d(attributeValue);
            }
            String attributeValue2 = xmlPullParser.getAttributeValue((String) null, "bulletedList");
            int i2 = 1;
            if (attributeValue2 == null) {
                changeLogRow.a(this.b);
            } else if (attributeValue2.equals("true")) {
                changeLogRow.a(true);
            } else {
                changeLogRow.a(false);
            }
            if (xmlPullParser.next() == 4) {
                String text = xmlPullParser.getText();
                if (text != null) {
                    changeLogRow.a(text);
                    if (!name.equalsIgnoreCase("changelogbug")) {
                        i2 = name.equalsIgnoreCase("changelogimprovement") ? 2 : 0;
                    }
                    changeLogRow.a(i2);
                    xmlPullParser.nextTag();
                } else {
                    throw new ChangeLogException("ChangeLogText required in changeLogText node");
                }
            }
            changeLog.a(changeLogRow);
        }
    }
}
