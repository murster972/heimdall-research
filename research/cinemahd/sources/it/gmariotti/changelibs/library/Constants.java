package it.gmariotti.changelibs.library;

import it.gmariotti.changelibs.R$layout;
import it.gmariotti.changelibs.R$raw;
import it.gmariotti.changelibs.R$string;

public class Constants {

    /* renamed from: a  reason: collision with root package name */
    public static final int f6901a = R$raw.changelog;
    public static final int b = R$layout.changelogrow_layout;
    public static final int c = R$layout.changelogrowheader_layout;
    public static final int d = R$string.changelog_header_version;
}
