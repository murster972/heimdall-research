package it.gmariotti.changelibs.library.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import it.gmariotti.changelibs.R$string;
import it.gmariotti.changelibs.R$styleable;
import it.gmariotti.changelibs.library.Constants;
import it.gmariotti.changelibs.library.Util;
import it.gmariotti.changelibs.library.internal.ChangeLog;
import it.gmariotti.changelibs.library.internal.ChangeLogAdapter;
import it.gmariotti.changelibs.library.internal.ChangeLogRow;
import it.gmariotti.changelibs.library.parser.XmlParser;
import java.util.Iterator;

public class ChangeLogListView extends ListView implements AdapterView.OnItemClickListener {
    protected static String f = "ChangeLogListView";

    /* renamed from: a  reason: collision with root package name */
    protected int f6911a = Constants.b;
    protected int b = Constants.c;
    protected int c = Constants.f6901a;
    protected String d = null;
    protected ChangeLogAdapter e;

    public ChangeLogListView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        if (Build.VERSION.SDK_INT >= 21) {
            setNestedScrollingEnabled(true);
        }
        a(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void a(AttributeSet attributeSet, int i) {
        b(attributeSet, i);
        a();
        setDividerHeight(0);
    }

    /* access modifiers changed from: protected */
    public void b(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R$styleable.ChangeLogListView, i, i);
        try {
            this.f6911a = obtainStyledAttributes.getResourceId(R$styleable.ChangeLogListView_rowLayoutId, this.f6911a);
            this.b = obtainStyledAttributes.getResourceId(R$styleable.ChangeLogListView_rowHeaderLayoutId, this.b);
            this.c = obtainStyledAttributes.getResourceId(R$styleable.ChangeLogListView_changeLogFileResourceId, this.c);
            this.d = obtainStyledAttributes.getString(R$styleable.ChangeLogListView_changeLogFileResourceUrl);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
    }

    public void setAdapter(ChangeLogAdapter changeLogAdapter) {
        super.setAdapter(changeLogAdapter);
    }

    protected class ParseAsyncTask extends AsyncTask<Void, Void, ChangeLog> {

        /* renamed from: a  reason: collision with root package name */
        private ChangeLogAdapter f6912a;
        private XmlParser b;

        public ParseAsyncTask(ChangeLogAdapter changeLogAdapter, XmlParser xmlParser) {
            this.f6912a = changeLogAdapter;
            this.b = xmlParser;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ChangeLog doInBackground(Void... voidArr) {
            try {
                if (this.b != null) {
                    return this.b.a();
                }
                return null;
            } catch (Exception e) {
                Log.e(ChangeLogListView.f, ChangeLogListView.this.getResources().getString(R$string.changelog_internal_error_parsing), e);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ChangeLog changeLog) {
            if (changeLog != null) {
                if (Build.VERSION.SDK_INT >= 11) {
                    this.f6912a.addAll(changeLog.a());
                } else if (changeLog.a() != null) {
                    Iterator it2 = changeLog.a().iterator();
                    while (it2.hasNext()) {
                        this.f6912a.add((ChangeLogRow) it2.next());
                    }
                }
                this.f6912a.notifyDataSetChanged();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        XmlParser xmlParser;
        try {
            if (this.d != null) {
                xmlParser = new XmlParser(getContext(), this.d);
            } else {
                xmlParser = new XmlParser(getContext(), this.c);
            }
            this.e = new ChangeLogAdapter(getContext(), new ChangeLog().a());
            this.e.b(this.f6911a);
            this.e.a(this.b);
            if (this.d != null) {
                if (this.d == null || !Util.a(getContext())) {
                    Toast.makeText(getContext(), R$string.changelog_internal_error_internet_connection, 1).show();
                    setAdapter(this.e);
                }
            }
            new ParseAsyncTask(this.e, xmlParser).execute(new Void[0]);
            setAdapter(this.e);
        } catch (Exception e2) {
            Log.e(f, getResources().getString(R$string.changelog_internal_error_parsing), e2);
        }
    }
}
