package it.gmariotti.changelibs.library.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Toast;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import it.gmariotti.changelibs.R$string;
import it.gmariotti.changelibs.R$styleable;
import it.gmariotti.changelibs.library.Constants;
import it.gmariotti.changelibs.library.Util;
import it.gmariotti.changelibs.library.internal.ChangeLog;
import it.gmariotti.changelibs.library.internal.ChangeLogRecyclerViewAdapter;
import it.gmariotti.changelibs.library.parser.XmlParser;

public class ChangeLogRecyclerView extends RecyclerView {
    protected static String f = "ChangeLogRecyclerView";

    /* renamed from: a  reason: collision with root package name */
    protected int f6913a;
    protected int b;
    protected int c;
    protected String d;
    protected ChangeLogRecyclerViewAdapter e;

    public ChangeLogRecyclerView(Context context) {
        this(context, (AttributeSet) null);
    }

    /* access modifiers changed from: protected */
    @TargetApi(21)
    public void a(AttributeSet attributeSet, int i) {
        b(attributeSet, i);
        b();
        c();
    }

    /* access modifiers changed from: protected */
    public void b(AttributeSet attributeSet, int i) {
        TypedArray obtainStyledAttributes = getContext().getTheme().obtainStyledAttributes(attributeSet, R$styleable.ChangeLogListView, i, i);
        try {
            this.f6913a = obtainStyledAttributes.getResourceId(R$styleable.ChangeLogListView_rowLayoutId, this.f6913a);
            this.b = obtainStyledAttributes.getResourceId(R$styleable.ChangeLogListView_rowHeaderLayoutId, this.b);
            this.c = obtainStyledAttributes.getResourceId(R$styleable.ChangeLogListView_changeLogFileResourceId, this.c);
            this.d = obtainStyledAttributes.getString(R$styleable.ChangeLogListView_changeLogFileResourceUrl);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    /* access modifiers changed from: protected */
    public void c() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(1);
        setLayoutManager(linearLayoutManager);
    }

    public ChangeLogRecyclerView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    protected class ParseAsyncTask extends AsyncTask<Void, Void, ChangeLog> {

        /* renamed from: a  reason: collision with root package name */
        private ChangeLogRecyclerViewAdapter f6914a;
        private XmlParser b;

        public ParseAsyncTask(ChangeLogRecyclerViewAdapter changeLogRecyclerViewAdapter, XmlParser xmlParser) {
            this.f6914a = changeLogRecyclerViewAdapter;
            this.b = xmlParser;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public ChangeLog doInBackground(Void... voidArr) {
            try {
                if (this.b != null) {
                    return this.b.a();
                }
                return null;
            } catch (Exception e) {
                Log.e(ChangeLogRecyclerView.f, ChangeLogRecyclerView.this.getResources().getString(R$string.changelog_internal_error_parsing), e);
                return null;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(ChangeLog changeLog) {
            if (changeLog != null) {
                this.f6914a.a(changeLog.a());
            }
        }
    }

    public ChangeLogRecyclerView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f6913a = Constants.b;
        this.b = Constants.c;
        this.c = Constants.f6901a;
        this.d = null;
        a(attributeSet, i);
    }

    /* access modifiers changed from: protected */
    public void b() {
        XmlParser xmlParser;
        try {
            if (this.d != null) {
                xmlParser = new XmlParser(getContext(), this.d);
            } else {
                xmlParser = new XmlParser(getContext(), this.c);
            }
            this.e = new ChangeLogRecyclerViewAdapter(getContext(), new ChangeLog().a());
            this.e.b(this.f6913a);
            this.e.a(this.b);
            if (this.d != null) {
                if (this.d == null || !Util.a(getContext())) {
                    Toast.makeText(getContext(), R$string.changelog_internal_error_internet_connection, 1).show();
                    setAdapter(this.e);
                }
            }
            new ParseAsyncTask(this.e, xmlParser).execute(new Void[0]);
            setAdapter(this.e);
        } catch (Exception e2) {
            Log.e(f, getResources().getString(R$string.changelog_internal_error_parsing), e2);
        }
    }
}
