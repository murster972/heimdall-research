package it.gmariotti.changelibs.library.internal;

import java.util.Iterator;
import java.util.LinkedList;

public class ChangeLog {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<ChangeLogRow> f6902a = new LinkedList<>();
    private boolean b;

    public void a(ChangeLogRow changeLogRow) {
        if (changeLogRow != null) {
            if (this.f6902a == null) {
                this.f6902a = new LinkedList<>();
            }
            this.f6902a.add(changeLogRow);
        }
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("bulletedList=" + this.b);
        sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        LinkedList<ChangeLogRow> linkedList = this.f6902a;
        if (linkedList != null) {
            Iterator it2 = linkedList.iterator();
            while (it2.hasNext()) {
                sb.append("row=[");
                sb.append(((ChangeLogRow) it2.next()).toString());
                sb.append("]\n");
            }
        } else {
            sb.append("rows:none");
        }
        return sb.toString();
    }

    public void a(boolean z) {
        this.b = z;
    }

    public LinkedList<ChangeLogRow> a() {
        return this.f6902a;
    }
}
