package it.gmariotti.changelibs.library.internal;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import it.gmariotti.changelibs.library.Constants;
import java.util.List;

public class ChangeLogAdapter extends ArrayAdapter<ChangeLogRow> {

    /* renamed from: a  reason: collision with root package name */
    private int f6903a = Constants.b;
    private int b = Constants.c;
    private int c = Constants.d;
    private final Context d;

    static class ViewHolderHeader {

        /* renamed from: a  reason: collision with root package name */
        TextView f6904a;
        TextView b;

        public ViewHolderHeader(TextView textView, TextView textView2) {
            this.f6904a = textView;
            this.b = textView2;
        }
    }

    static class ViewHolderRow {

        /* renamed from: a  reason: collision with root package name */
        TextView f6905a;
        TextView b;

        public ViewHolderRow(TextView textView, TextView textView2) {
            this.f6905a = textView;
            this.b = textView2;
        }
    }

    public ChangeLogAdapter(Context context, List<ChangeLogRow> list) {
        super(context, 0, list);
        this.d = context;
    }

    public void a(int i) {
        this.b = i;
    }

    public void b(int i) {
        this.f6903a = i;
    }

    public int getItemViewType(int i) {
        return ((ChangeLogRow) getItem(i)).c() ? 1 : 0;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v12, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v6, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v13, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v14, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v15, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v16, resolved type: it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader} */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0030, code lost:
        r8 = r1.inflate(r6.b, r9, false);
        r4 = new it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderHeader((android.widget.TextView) r8.findViewById(it.gmariotti.changelibs.R$id.chg_headerVersion), (android.widget.TextView) r8.findViewById(it.gmariotti.changelibs.R$id.chg_headerDate));
        r8.setTag(r4);
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x004e, code lost:
        if (r0 == null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0050, code lost:
        if (r4 == null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0054, code lost:
        if (r4.f6904a == null) goto L_0x0078;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0056, code lost:
        r7 = new java.lang.StringBuilder();
        r9 = getContext().getString(r6.c);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0065, code lost:
        if (r9 == null) goto L_0x006a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0067, code lost:
        r7.append(r9);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x006a, code lost:
        r7.append(r0.b);
        r4.f6904a.setText(r7.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0078, code lost:
        r7 = r4.b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x007a, code lost:
        if (r7 == null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x007c, code lost:
        r9 = r0.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x007e, code lost:
        if (r9 == null) goto L_0x0089;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0080, code lost:
        r7.setText(r9);
        r4.b.setVisibility(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0089, code lost:
        r7.setText("");
        r4.b.setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00a3, code lost:
        if (r4 == null) goto L_0x00a5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00a5, code lost:
        r8 = r1.inflate(r6.f6903a, r9, false);
        r4 = new it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderRow((android.widget.TextView) r8.findViewById(it.gmariotti.changelibs.R$id.chg_text), (android.widget.TextView) r8.findViewById(it.gmariotti.changelibs.R$id.chg_textbullet));
        r8.setTag(r4);
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c3, code lost:
        if (r0 == null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00c5, code lost:
        if (r4 == null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c7, code lost:
        r7 = r4.f6905a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00c9, code lost:
        if (r7 == null) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00cb, code lost:
        r7.setText(android.text.Html.fromHtml(r0.a(r6.d)));
        r4.f6905a.setMovementMethod(android.text.method.LinkMovementMethod.getInstance());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e3, code lost:
        if (r4.b == null) goto L_0x00f6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00e9, code lost:
        if (r0.b() == false) goto L_0x00f1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00eb, code lost:
        r4.b.setVisibility(0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f1, code lost:
        r4.b.setVisibility(8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x002e, code lost:
        if (r4 == null) goto L_0x0030;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r7, android.view.View r8, android.view.ViewGroup r9) {
        /*
            r6 = this;
            java.lang.Object r0 = r6.getItem(r7)
            it.gmariotti.changelibs.library.internal.ChangeLogRow r0 = (it.gmariotti.changelibs.library.internal.ChangeLogRow) r0
            int r7 = r6.getItemViewType(r7)
            android.content.Context r1 = r6.d
            java.lang.String r2 = "layout_inflater"
            java.lang.Object r1 = r1.getSystemService(r2)
            android.view.LayoutInflater r1 = (android.view.LayoutInflater) r1
            r2 = 8
            r3 = 0
            r4 = 0
            if (r7 == 0) goto L_0x0094
            r5 = 1
            if (r7 == r5) goto L_0x001f
            goto L_0x00f6
        L_0x001f:
            if (r8 == 0) goto L_0x002c
            java.lang.Object r7 = r8.getTag()
            boolean r5 = r7 instanceof it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderHeader
            if (r5 == 0) goto L_0x002c
            r4 = r7
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader r4 = (it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderHeader) r4
        L_0x002c:
            if (r8 == 0) goto L_0x0030
            if (r4 != 0) goto L_0x004e
        L_0x0030:
            int r7 = r6.b
            android.view.View r8 = r1.inflate(r7, r9, r3)
            int r7 = it.gmariotti.changelibs.R$id.chg_headerVersion
            android.view.View r7 = r8.findViewById(r7)
            android.widget.TextView r7 = (android.widget.TextView) r7
            int r9 = it.gmariotti.changelibs.R$id.chg_headerDate
            android.view.View r9 = r8.findViewById(r9)
            android.widget.TextView r9 = (android.widget.TextView) r9
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader r4 = new it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderHeader
            r4.<init>(r7, r9)
            r8.setTag(r4)
        L_0x004e:
            if (r0 == 0) goto L_0x00f6
            if (r4 == 0) goto L_0x00f6
            android.widget.TextView r7 = r4.f6904a
            if (r7 == 0) goto L_0x0078
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            android.content.Context r9 = r6.getContext()
            int r1 = r6.c
            java.lang.String r9 = r9.getString(r1)
            if (r9 == 0) goto L_0x006a
            r7.append(r9)
        L_0x006a:
            java.lang.String r9 = r0.b
            r7.append(r9)
            android.widget.TextView r9 = r4.f6904a
            java.lang.String r7 = r7.toString()
            r9.setText(r7)
        L_0x0078:
            android.widget.TextView r7 = r4.b
            if (r7 == 0) goto L_0x00f6
            java.lang.String r9 = r0.d
            if (r9 == 0) goto L_0x0089
            r7.setText(r9)
            android.widget.TextView r7 = r4.b
            r7.setVisibility(r3)
            goto L_0x00f6
        L_0x0089:
            java.lang.String r9 = ""
            r7.setText(r9)
            android.widget.TextView r7 = r4.b
            r7.setVisibility(r2)
            goto L_0x00f6
        L_0x0094:
            if (r8 == 0) goto L_0x00a1
            java.lang.Object r7 = r8.getTag()
            boolean r5 = r7 instanceof it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderRow
            if (r5 == 0) goto L_0x00a1
            r4 = r7
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow r4 = (it.gmariotti.changelibs.library.internal.ChangeLogAdapter.ViewHolderRow) r4
        L_0x00a1:
            if (r8 == 0) goto L_0x00a5
            if (r4 != 0) goto L_0x00c3
        L_0x00a5:
            int r7 = r6.f6903a
            android.view.View r8 = r1.inflate(r7, r9, r3)
            int r7 = it.gmariotti.changelibs.R$id.chg_text
            android.view.View r7 = r8.findViewById(r7)
            android.widget.TextView r7 = (android.widget.TextView) r7
            int r9 = it.gmariotti.changelibs.R$id.chg_textbullet
            android.view.View r9 = r8.findViewById(r9)
            android.widget.TextView r9 = (android.widget.TextView) r9
            it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow r4 = new it.gmariotti.changelibs.library.internal.ChangeLogAdapter$ViewHolderRow
            r4.<init>(r7, r9)
            r8.setTag(r4)
        L_0x00c3:
            if (r0 == 0) goto L_0x00f6
            if (r4 == 0) goto L_0x00f6
            android.widget.TextView r7 = r4.f6905a
            if (r7 == 0) goto L_0x00e1
            android.content.Context r9 = r6.d
            java.lang.String r9 = r0.a((android.content.Context) r9)
            android.text.Spanned r9 = android.text.Html.fromHtml(r9)
            r7.setText(r9)
            android.widget.TextView r7 = r4.f6905a
            android.text.method.MovementMethod r9 = android.text.method.LinkMovementMethod.getInstance()
            r7.setMovementMethod(r9)
        L_0x00e1:
            android.widget.TextView r7 = r4.b
            if (r7 == 0) goto L_0x00f6
            boolean r7 = r0.b()
            if (r7 == 0) goto L_0x00f1
            android.widget.TextView r7 = r4.b
            r7.setVisibility(r3)
            goto L_0x00f6
        L_0x00f1:
            android.widget.TextView r7 = r4.b
            r7.setVisibility(r2)
        L_0x00f6:
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: it.gmariotti.changelibs.library.internal.ChangeLogAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public int getViewTypeCount() {
        return 2;
    }

    public boolean isEnabled(int i) {
        return false;
    }
}
