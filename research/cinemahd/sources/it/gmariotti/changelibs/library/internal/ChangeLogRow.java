package it.gmariotti.changelibs.library.internal;

import android.content.Context;
import it.gmariotti.changelibs.R$string;

public class ChangeLogRow {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f6909a;
    protected String b;
    protected int c;
    protected String d;
    private boolean e;
    private String f;
    private int g;

    public void a(String str) {
        if (str != null) {
            str = str.replaceAll("\\[", "<").replaceAll("\\]", ">");
        }
        c(str);
    }

    public void b(boolean z) {
        this.f6909a = z;
    }

    public boolean c() {
        return this.f6909a;
    }

    public void d(String str) {
    }

    public void e(String str) {
        this.b = str;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("header=" + this.f6909a);
        sb.append(",");
        sb.append("versionName=" + this.b);
        sb.append(",");
        sb.append("versionCode=" + this.c);
        sb.append(",");
        sb.append("bulletedList=" + this.e);
        sb.append(",");
        sb.append("changeText=" + this.f);
        return sb.toString();
    }

    public boolean b() {
        return this.e;
    }

    public void c(String str) {
        this.f = str;
    }

    public void a(boolean z) {
        this.e = z;
    }

    public void b(int i) {
        this.c = i;
    }

    public String a() {
        return this.f;
    }

    public void b(String str) {
        this.d = str;
    }

    public String a(Context context) {
        String str;
        if (context == null) {
            return a();
        }
        int i = this.g;
        if (i == 1) {
            str = context.getResources().getString(R$string.changelog_row_prefix_bug).replaceAll("\\[", "<").replaceAll("\\]", ">");
        } else if (i != 2) {
            str = "";
        } else {
            str = context.getResources().getString(R$string.changelog_row_prefix_improvement).replaceAll("\\[", "<").replaceAll("\\]", ">");
        }
        return str + " " + this.f;
    }

    public void a(int i) {
        this.g = i;
    }
}
