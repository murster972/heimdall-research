package it.gmariotti.changelibs.library.internal;

import android.content.Context;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import it.gmariotti.changelibs.R$id;
import it.gmariotti.changelibs.library.Constants;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ChangeLogRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6906a;
    private int b = Constants.b;
    private int c = Constants.c;
    private int d = Constants.d;
    private List<ChangeLogRow> e;

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        public TextView f6907a;
        public TextView b;

        public ViewHolderHeader(View view) {
            super(view);
            this.f6907a = (TextView) view.findViewById(R$id.chg_headerVersion);
            this.b = (TextView) view.findViewById(R$id.chg_headerDate);
        }
    }

    public static class ViewHolderRow extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        public TextView f6908a;
        public TextView b;

        public ViewHolderRow(View view) {
            super(view);
            this.f6908a = (TextView) view.findViewById(R$id.chg_text);
            this.b = (TextView) view.findViewById(R$id.chg_textbullet);
        }
    }

    public ChangeLogRecyclerViewAdapter(Context context, List<ChangeLogRow> list) {
        this.f6906a = context;
        this.e = list == null ? new ArrayList<>() : list;
    }

    private boolean c(int i) {
        return getItem(i).c();
    }

    private ChangeLogRow getItem(int i) {
        return this.e.get(i);
    }

    public void a(LinkedList<ChangeLogRow> linkedList) {
        int size = this.e.size();
        this.e.addAll(linkedList);
        notifyItemRangeInserted(size, linkedList.size() + size);
    }

    public void b(int i) {
        this.b = i;
    }

    public int getItemCount() {
        return this.e.size();
    }

    public int getItemViewType(int i) {
        return c(i) ? 1 : 0;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (c(i)) {
            a((ViewHolderHeader) viewHolder, i);
        } else {
            a((ViewHolderRow) viewHolder, i);
        }
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 1) {
            return new ViewHolderHeader(LayoutInflater.from(viewGroup.getContext()).inflate(this.c, viewGroup, false));
        }
        return new ViewHolderRow(LayoutInflater.from(viewGroup.getContext()).inflate(this.b, viewGroup, false));
    }

    private void a(ViewHolderRow viewHolderRow, int i) {
        ChangeLogRow item = getItem(i);
        if (item != null) {
            TextView textView = viewHolderRow.f6908a;
            if (textView != null) {
                textView.setText(Html.fromHtml(item.a(this.f6906a)));
                viewHolderRow.f6908a.setMovementMethod(LinkMovementMethod.getInstance());
            }
            if (viewHolderRow.b == null) {
                return;
            }
            if (item.b()) {
                viewHolderRow.b.setVisibility(0);
            } else {
                viewHolderRow.b.setVisibility(8);
            }
        }
    }

    private void a(ViewHolderHeader viewHolderHeader, int i) {
        ChangeLogRow item = getItem(i);
        if (item != null) {
            if (viewHolderHeader.f6907a != null) {
                StringBuilder sb = new StringBuilder();
                String string = this.f6906a.getString(this.d);
                if (string != null) {
                    sb.append(string);
                }
                sb.append(item.b);
                viewHolderHeader.f6907a.setText(sb.toString());
            }
            TextView textView = viewHolderHeader.b;
            if (textView != null) {
                String str = item.d;
                if (str != null) {
                    textView.setText(str);
                    viewHolderHeader.b.setVisibility(0);
                    return;
                }
                textView.setText("");
                viewHolderHeader.b.setVisibility(8);
            }
        }
    }

    public void a(int i) {
        this.c = i;
    }
}
