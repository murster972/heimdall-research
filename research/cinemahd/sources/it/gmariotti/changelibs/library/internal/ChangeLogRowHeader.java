package it.gmariotti.changelibs.library.internal;

public class ChangeLogRowHeader extends ChangeLogRow {
    public ChangeLogRowHeader() {
        b(true);
        a(false);
        d((String) null);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("header=" + this.f6909a);
        sb.append(",");
        sb.append("versionName=" + this.b);
        sb.append(",");
        sb.append("changeDate=" + this.d);
        return sb.toString();
    }
}
