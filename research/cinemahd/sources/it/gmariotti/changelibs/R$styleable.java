package it.gmariotti.changelibs;

import com.yoku.marumovie.R;

public final class R$styleable {
    public static final int[] ChangeLogListView = {R.attr.changeLogFileResourceId, R.attr.changeLogFileResourceUrl, R.attr.rowHeaderLayoutId, R.attr.rowLayoutId};
    public static final int ChangeLogListView_changeLogFileResourceId = 0;
    public static final int ChangeLogListView_changeLogFileResourceUrl = 1;
    public static final int ChangeLogListView_rowHeaderLayoutId = 2;
    public static final int ChangeLogListView_rowLayoutId = 3;
    public static final int[] RecyclerView = {16842948, 16842993, R.attr.fastScrollEnabled, R.attr.fastScrollHorizontalThumbDrawable, R.attr.fastScrollHorizontalTrackDrawable, R.attr.fastScrollVerticalThumbDrawable, R.attr.fastScrollVerticalTrackDrawable, R.attr.layoutManager, R.attr.reverseLayout, R.attr.spanCount, R.attr.stackFromEnd};
    public static final int RecyclerView_android_descendantFocusability = 1;
    public static final int RecyclerView_android_orientation = 0;
    public static final int RecyclerView_fastScrollEnabled = 2;
    public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
    public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
    public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
    public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
    public static final int RecyclerView_layoutManager = 7;
    public static final int RecyclerView_reverseLayout = 8;
    public static final int RecyclerView_spanCount = 9;
    public static final int RecyclerView_stackFromEnd = 10;

    private R$styleable() {
    }
}
