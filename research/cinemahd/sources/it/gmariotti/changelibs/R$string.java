package it.gmariotti.changelibs;

public final class R$string {
    public static final int changelog_header_version = 2131755173;
    public static final int changelog_internal_error_internet_connection = 2131755174;
    public static final int changelog_internal_error_parsing = 2131755175;
    public static final int changelog_row_bulletpoint = 2131755176;
    public static final int changelog_row_prefix_bug = 2131755177;
    public static final int changelog_row_prefix_improvement = 2131755178;
    public static final int font_fontFamily_material_item = 2131755332;

    private R$string() {
    }
}
