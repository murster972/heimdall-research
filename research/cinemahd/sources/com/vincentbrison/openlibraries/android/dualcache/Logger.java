package com.vincentbrison.openlibraries.android.dualcache;

import android.util.Log;

final class Logger {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f6581a;

    Logger(boolean z) {
        this.f6581a = z;
    }

    private void a(int i, String str, String str2) {
        if (this.f6581a) {
            Log.println(i, str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        a(4, "dualcache", str);
    }

    /* access modifiers changed from: package-private */
    public void a(Throwable th) {
        if (this.f6581a) {
            Log.e("dualcache", "error : ", th);
        }
    }
}
