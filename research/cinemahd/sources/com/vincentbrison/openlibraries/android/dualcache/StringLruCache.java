package com.vincentbrison.openlibraries.android.dualcache;

import java.nio.charset.Charset;

class StringLruCache extends RamLruCache<String, String> {
    public StringLruCache(int i) {
        super(i);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int b(String str, String str2) {
        return str2.getBytes(Charset.defaultCharset()).length;
    }
}
