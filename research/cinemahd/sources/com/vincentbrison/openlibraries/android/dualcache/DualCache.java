package com.vincentbrison.openlibraries.android.dualcache;

import com.jakewharton.disklrucache.DiskLruCache;
import java.io.File;
import java.io.IOException;

public class DualCache<T> {

    /* renamed from: a  reason: collision with root package name */
    private final RamLruCache f6576a;
    private DiskLruCache b;
    private final int c;
    private final int d;
    private final DualCacheRamMode e;
    private final DualCacheDiskMode f;
    private final CacheSerializer<T> g;
    private final CacheSerializer<T> h;
    private final DualCacheLock i = new DualCacheLock();
    private final Logger j;
    private final LoggerHelper k;

    /* renamed from: com.vincentbrison.openlibraries.android.dualcache.DualCache$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6577a = new int[DualCacheRamMode.values().length];
        static final /* synthetic */ int[] b = new int[DualCacheDiskMode.values().length];

        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0027 */
        static {
            /*
                com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode[] r0 = com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode r2 = com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode[] r1 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.values()
                int r1 = r1.length
                int[] r1 = new int[r1]
                f6577a = r1
                int[] r1 = f6577a     // Catch:{ NoSuchFieldError -> 0x0027 }
                com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r2 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER     // Catch:{ NoSuchFieldError -> 0x0027 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0027 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0027 }
            L_0x0027:
                int[] r0 = f6577a     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_REFERENCE     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.vincentbrison.openlibraries.android.dualcache.DualCache.AnonymousClass1.<clinit>():void");
        }
    }

    DualCache(int i2, Logger logger, DualCacheRamMode dualCacheRamMode, CacheSerializer<T> cacheSerializer, int i3, SizeOf<T> sizeOf, DualCacheDiskMode dualCacheDiskMode, CacheSerializer<T> cacheSerializer2, int i4, File file) {
        this.d = i2;
        this.e = dualCacheRamMode;
        this.h = cacheSerializer;
        this.f = dualCacheDiskMode;
        this.g = cacheSerializer2;
        this.j = logger;
        this.k = new LoggerHelper(logger);
        int i5 = AnonymousClass1.f6577a[dualCacheRamMode.ordinal()];
        if (i5 == 1) {
            this.f6576a = new StringLruCache(i3);
        } else if (i5 != 2) {
            this.f6576a = null;
        } else {
            this.f6576a = new ReferenceLruCache(i3, sizeOf);
        }
        if (AnonymousClass1.b[dualCacheDiskMode.ordinal()] != 1) {
            this.c = 0;
            return;
        }
        this.c = i4;
        try {
            a(file);
        } catch (IOException e2) {
            logger.a((Throwable) e2);
        }
    }

    private void a(File file) throws IOException {
        this.b = DiskLruCache.a(file, this.d, 1, (long) this.c);
    }

    public DualCacheRamMode b() {
        return this.e;
    }

    /* JADX INFO: finally extract failed */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public T c(java.lang.String r4) {
        /*
            r3 = this;
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r0 = r3.e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER
            boolean r0 = r0.equals(r1)
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = r3.e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r2 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_REFERENCE
            boolean r1 = r1.equals(r2)
            r2 = 0
            if (r0 != 0) goto L_0x0018
            if (r1 == 0) goto L_0x0016
            goto L_0x0018
        L_0x0016:
            r0 = r2
            goto L_0x001e
        L_0x0018:
            com.vincentbrison.openlibraries.android.dualcache.RamLruCache r0 = r3.f6576a
            java.lang.Object r0 = r0.b(r4)
        L_0x001e:
            if (r0 != 0) goto L_0x00b0
            com.vincentbrison.openlibraries.android.dualcache.LoggerHelper r0 = r3.k
            r0.b(r4)
            com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode r0 = r3.f
            com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode r1 = com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x006e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheLock r0 = r3.i     // Catch:{ IOException -> 0x0042 }
            r0.a(r4)     // Catch:{ IOException -> 0x0042 }
            com.jakewharton.disklrucache.DiskLruCache r0 = r3.b     // Catch:{ IOException -> 0x0042 }
            com.jakewharton.disklrucache.DiskLruCache$Snapshot r0 = r0.f((java.lang.String) r4)     // Catch:{ IOException -> 0x0042 }
            com.vincentbrison.openlibraries.android.dualcache.DualCacheLock r1 = r3.i
            r1.b(r4)
            goto L_0x004e
        L_0x0040:
            r0 = move-exception
            goto L_0x0068
        L_0x0042:
            r0 = move-exception
            com.vincentbrison.openlibraries.android.dualcache.Logger r1 = r3.j     // Catch:{ all -> 0x0040 }
            r1.a((java.lang.Throwable) r0)     // Catch:{ all -> 0x0040 }
            com.vincentbrison.openlibraries.android.dualcache.DualCacheLock r0 = r3.i
            r0.b(r4)
            r0 = r2
        L_0x004e:
            if (r0 == 0) goto L_0x0062
            com.vincentbrison.openlibraries.android.dualcache.LoggerHelper r1 = r3.k
            r1.d(r4)
            r1 = 0
            java.lang.String r0 = r0.getString(r1)     // Catch:{ IOException -> 0x005b }
            goto L_0x006f
        L_0x005b:
            r0 = move-exception
            com.vincentbrison.openlibraries.android.dualcache.Logger r1 = r3.j
            r1.a((java.lang.Throwable) r0)
            goto L_0x006e
        L_0x0062:
            com.vincentbrison.openlibraries.android.dualcache.LoggerHelper r0 = r3.k
            r0.c(r4)
            goto L_0x006e
        L_0x0068:
            com.vincentbrison.openlibraries.android.dualcache.DualCacheLock r1 = r3.i
            r1.b(r4)
            throw r0
        L_0x006e:
            r0 = r2
        L_0x006f:
            if (r0 == 0) goto L_0x00d2
            com.vincentbrison.openlibraries.android.dualcache.CacheSerializer<T> r1 = r3.g
            r1.a((java.lang.String) r0)
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = r3.e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r2 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_REFERENCE
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x0090
            com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode r1 = r3.f
            com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode r2 = com.vincentbrison.openlibraries.android.dualcache.DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00af
            com.vincentbrison.openlibraries.android.dualcache.RamLruCache r1 = r3.f6576a
            r1.a(r4, r0)
            goto L_0x00af
        L_0x0090:
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = r3.e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r2 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER
            boolean r1 = r1.equals(r2)
            if (r1 == 0) goto L_0x00af
            com.vincentbrison.openlibraries.android.dualcache.CacheSerializer<T> r1 = r3.g
            com.vincentbrison.openlibraries.android.dualcache.CacheSerializer<T> r2 = r3.h
            if (r1 != r2) goto L_0x00a6
            com.vincentbrison.openlibraries.android.dualcache.RamLruCache r1 = r3.f6576a
            r1.a(r4, r0)
            goto L_0x00af
        L_0x00a6:
            com.vincentbrison.openlibraries.android.dualcache.RamLruCache r1 = r3.f6576a
            java.lang.String r2 = r2.a(r0)
            r1.a(r4, r2)
        L_0x00af:
            return r0
        L_0x00b0:
            com.vincentbrison.openlibraries.android.dualcache.LoggerHelper r1 = r3.k
            r1.a(r4)
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r4 = r3.e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_REFERENCE
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00c0
            return r0
        L_0x00c0:
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r4 = r3.e
            com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode r1 = com.vincentbrison.openlibraries.android.dualcache.DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00d2
            com.vincentbrison.openlibraries.android.dualcache.CacheSerializer<T> r4 = r3.h
            java.lang.String r0 = (java.lang.String) r0
            r4.a((java.lang.String) r0)
            return r0
        L_0x00d2:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.vincentbrison.openlibraries.android.dualcache.DualCache.c(java.lang.String):java.lang.Object");
    }

    public DualCacheDiskMode a() {
        return this.f;
    }

    public void b(String str) {
        if (!this.e.equals(DualCacheRamMode.DISABLE)) {
            this.f6576a.c(str);
        }
        if (!this.f.equals(DualCacheDiskMode.DISABLE)) {
            try {
                this.i.a(str);
                this.b.g(str);
            } catch (IOException e2) {
                this.j.a((Throwable) e2);
            } catch (Throwable th) {
                this.i.b(str);
                throw th;
            }
            this.i.b(str);
        }
    }

    public void a(String str, T t) {
        if (this.e.equals(DualCacheRamMode.ENABLE_WITH_REFERENCE)) {
            this.f6576a.a(str, t);
        }
        String str2 = null;
        if (this.e.equals(DualCacheRamMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
            str2 = this.h.a(t);
            this.f6576a.a(str, str2);
        }
        if (this.f.equals(DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER)) {
            try {
                this.i.a(str);
                DiskLruCache.Editor e2 = this.b.e(str);
                if (this.h == this.g) {
                    e2.a(0, str2);
                } else {
                    e2.a(0, this.g.a(t));
                }
                e2.b();
            } catch (IOException e3) {
                this.j.a((Throwable) e3);
            } catch (Throwable th) {
                this.i.b(str);
                throw th;
            }
            this.i.b(str);
        }
    }

    public boolean a(String str) {
        if (!this.e.equals(DualCacheRamMode.DISABLE) && this.f6576a.a().containsKey(str)) {
            return true;
        }
        try {
            this.i.a(str);
            if (!this.f.equals(DualCacheDiskMode.DISABLE) && this.b.f(str) != null) {
                this.i.b(str);
                return true;
            }
        } catch (IOException e2) {
            this.j.a((Throwable) e2);
        } catch (Throwable th) {
            this.i.b(str);
            throw th;
        }
        this.i.b(str);
        return false;
    }
}
