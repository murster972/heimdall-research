package com.vincentbrison.openlibraries.android.dualcache;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

class DualCacheLock {

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentMap<String, Lock> f6579a = new ConcurrentHashMap();
    private final ReadWriteLock b = new ReentrantReadWriteLock();

    DualCacheLock() {
    }

    private Lock c(String str) {
        if (!this.f6579a.containsKey(str)) {
            this.f6579a.putIfAbsent(str, new ReentrantLock());
        }
        return (Lock) this.f6579a.get(str);
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.b.readLock().lock();
        c(str).lock();
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        c(str).unlock();
        this.b.readLock().unlock();
    }
}
