package com.vincentbrison.openlibraries.android.dualcache;

public class ReferenceLruCache<T> extends RamLruCache<String, T> {
    private SizeOf<T> i;

    public ReferenceLruCache(int i2, SizeOf<T> sizeOf) {
        super(i2);
        this.i = sizeOf;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public int b(String str, T t) {
        return this.i.a(t);
    }
}
