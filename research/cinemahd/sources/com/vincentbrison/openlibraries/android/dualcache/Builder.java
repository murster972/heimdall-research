package com.vincentbrison.openlibraries.android.dualcache;

import android.content.Context;
import java.io.File;

public class Builder<T> {

    /* renamed from: a  reason: collision with root package name */
    private String f6575a;
    private int b;
    private boolean c = false;
    private int d;
    private DualCacheRamMode e = null;
    private CacheSerializer<T> f;
    private SizeOf<T> g;
    private int h;
    private DualCacheDiskMode i = null;
    private CacheSerializer<T> j;
    private File k;

    public Builder(String str, int i2) {
        this.f6575a = str;
        this.b = i2;
    }

    public DualCache<T> a() {
        if (this.e == null) {
            throw new IllegalStateException("No ram mode set");
        } else if (this.i != null) {
            DualCache dualCache = new DualCache(this.b, new Logger(this.c), this.e, this.f, this.d, this.g, this.i, this.j, this.h, this.k);
            boolean equals = dualCache.b().equals(DualCacheRamMode.DISABLE);
            boolean equals2 = dualCache.a().equals(DualCacheDiskMode.DISABLE);
            if (!equals || !equals2) {
                return dualCache;
            }
            throw new IllegalStateException("The ram cache layer and the disk cache layer are disable. You have to use at least one of those layers.");
        } else {
            throw new IllegalStateException("No disk mode set");
        }
    }

    public Builder<T> b() {
        this.c = true;
        return this;
    }

    public Builder<T> c() {
        this.e = DualCacheRamMode.DISABLE;
        return this;
    }

    public Builder<T> a(int i2, boolean z, CacheSerializer<T> cacheSerializer, Context context) {
        a(i2, a(z, context), cacheSerializer);
        return this;
    }

    public Builder<T> a(int i2, File file, CacheSerializer<T> cacheSerializer) {
        this.k = file;
        this.i = DualCacheDiskMode.ENABLE_WITH_SPECIFIC_SERIALIZER;
        this.h = i2;
        this.j = cacheSerializer;
        return this;
    }

    private File a(boolean z, Context context) {
        if (z) {
            return context.getDir("dualcache" + this.f6575a, 0);
        }
        return new File(context.getCacheDir().getPath() + "/" + "dualcache" + "/" + this.f6575a);
    }
}
