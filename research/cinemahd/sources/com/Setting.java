package com;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;
import com.yoku.marumovie.R;
import java.io.File;

public class Setting {
    public static File a(Context context) {
        return a(context, R.string.download_path_key, Environment.DIRECTORY_MOVIES);
    }

    public static String b(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(context.getString(R.string.download_path_key), Environment.DIRECTORY_MOVIES);
    }

    private static File a(Context context, int i, String str) {
        SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        String string = context.getString(i);
        String string2 = defaultSharedPreferences.getString(string, (String) null);
        if (string2 != null && !string2.isEmpty()) {
            return new File(string2.trim());
        }
        File a2 = a(str);
        SharedPreferences.Editor edit = defaultSharedPreferences.edit();
        edit.putString(string, new File(a2, "NewPipe").getAbsolutePath());
        edit.apply();
        return a2;
    }

    private static File a(String str) {
        return new File(Environment.getExternalStorageDirectory(), str);
    }
}
