package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import com.facebook.imageutils.JfifUtil;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

class cc {
    private static cc g;

    /* renamed from: a  reason: collision with root package name */
    private final HashSet<Integer> f1982a = new HashSet<>();
    private final HashSet<Integer> b = new HashSet<>();
    private List<WeakReference<View>> c = new ArrayList();
    private List<WeakReference<View>> d = new ArrayList();
    private final HashMap<View, HashMap<WeakReference<View>, Rect>> e = new HashMap<>();
    private xc f = new xc(this);

    private /* synthetic */ cc() {
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ '6');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ '.');
        }
        return new String(cArr);
    }

    private /* synthetic */ boolean a(int i, int i2) {
        return (i & i2) == i2;
    }

    private /* synthetic */ void b(List<WeakReference<View>> list, View view) {
        synchronized (list) {
            for (WeakReference next : list) {
                if (next.get() != null && ((View) next.get()).equals(view)) {
                    return;
                }
            }
            list.add(new WeakReference(view));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        a(this.d, view);
        b(this.c, view);
    }

    private /* synthetic */ void a(List<WeakReference<View>> list, View view, Integer num) {
        synchronized (list) {
            for (int size = list.size() - 1; size >= 0; size--) {
                WeakReference weakReference = list.get(size);
                if (weakReference.get() != null) {
                    if (num != null) {
                        if (((View) weakReference.get()).getId() == num.intValue()) {
                            list.remove(size);
                        }
                    }
                }
                if (!((View) weakReference.get()).equals(view)) {
                }
                list.remove(size);
            }
        }
    }

    private /* synthetic */ boolean b(View view) {
        if (this.f.f2084a.contains(Integer.valueOf(view.getId()))) {
            if (gd.a(0)) {
                gd.a(0, qb.a(" \u0010\u0010\u0011U\u0007\u0010\u0005\u001c\r\u0010\u0007U\n\u001b\u0010\u0010\r\u0006\n\u0001\n\u0003\u0006U\u0015\u001c\u0006\u0002C\u001c\u0007OCP\u0010"), rb.c(view));
            }
            return false;
        }
        int i = 0;
        while (i < this.f.b.size()) {
            WeakReference weakReference = this.f.b.get(i);
            if (weakReference.get() == null || !((View) weakReference.get()).equals(view)) {
                i++;
            } else {
                if (gd.a(0)) {
                    gd.a(0, wd.e("\u0002223w%2'>/2%w(922/$(#(!$w7>$ {wd$"), rb.c(view));
                }
                return false;
            }
        }
        if (pg.h().T() && EditText.class.isInstance(view)) {
            EditText editText = (EditText) view;
            if (a(editText.getInputType(), 129) || a(editText.getInputType(), 145) || a(editText.getInputType(), 18) || a(editText.getInputType(), (int) JfifUtil.MARKER_APP1)) {
                if (gd.a(0)) {
                    gd.a(0, qb.a("+\u001c\u0007\u001c\r\u0012C\u0003\n\u0010\u0014U\u0007\u0000\u0006U\u0017\u001aC\u0005\u0002\u0006\u0010\u0002\f\u0007\u0007U\u0005\u001c\u0006\u0019\u0007OCP\u0010"), rb.c(view));
                }
                return true;
            }
        }
        if (!pg.h().m() || (!EditText.class.isInstance(view) && !AutoCompleteTextView.class.isInstance(view))) {
            List<Object> b2 = pg.h().b("class");
            if (b2 != null && !b2.isEmpty()) {
                for (int i2 = 0; i2 < b2.size(); i2++) {
                    Object obj = b2.get(i2);
                    if (obj != null && (obj instanceof Class)) {
                        Class cls = (Class) obj;
                        if (cls.isInstance(view)) {
                            if (gd.a(0)) {
                                gd.a(0, qb.a("+\u001c\u0007\u001c\r\u0012C\u0003\n\u0010\u0014U\u0007\u0000\u0006U\u0017\u001aC\u0006\u0006\u0007\u0015\u0010\u0011U\u000b\u001c\u0007\u0010C\u0016\f\u0018\u0013\u001a\r\u0010\r\u0001CRF\u0006DOCP\u0010"), cls.getName(), rb.c(view));
                            }
                            return true;
                        }
                    }
                }
            }
            if (this.f.d.contains(Integer.valueOf(view.getId()))) {
                a(view);
            }
            int i3 = 0;
            while (i3 < this.f.e.size()) {
                WeakReference weakReference2 = this.f.e.get(i3);
                if (weakReference2.get() == null || !((View) weakReference2.get()).equals(view)) {
                    i3++;
                } else {
                    if (gd.a(0)) {
                        gd.a(0, wd.e("\t>%>/0a!(26w%\"$w58a\"223w(92# 9\"2{wd$"), rb.c(view));
                    }
                    return true;
                }
            }
            return false;
        }
        if (gd.a(0)) {
            gd.a(0, wd.e("\t>%>/0a!(26w%\"$w58a>/'4#a1(2-3{wd$"), rb.c(view));
        }
        return true;
    }

    private /* synthetic */ void a(View view, View view2, Rect rect) {
        synchronized (this.e) {
            if (!this.e.containsKey(view)) {
                this.e.put(view, new HashMap());
            }
            this.e.get(view).put(view2 != null ? new WeakReference(view2) : null, rect);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(List<View> list) throws Exception {
        synchronized (this.e) {
            this.e.clear();
        }
        if (xd.b().d() && list != null && !list.isEmpty()) {
            this.f.a();
            for (int i = 0; i < list.size(); i++) {
                View view = list.get(i);
                if (!a(view) && view.isShown()) {
                    a(view, list, view);
                }
            }
        }
    }

    @TargetApi(11)
    private /* synthetic */ void a(View view, List<View> list, View view2) {
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int i = 0;
            while (i < viewGroup.getChildCount()) {
                View childAt = viewGroup.getChildAt(i);
                i++;
                a(childAt, list, view2);
            }
        }
        if (b(view) && view.isShown()) {
            if (Build.VERSION.SDK_INT >= 11 && view.getAlpha() <= 0.0f) {
                return;
            }
            if (pg.h().k() || (rb.g(view) && !a(view, list))) {
                a(view2, view, rb.e(view));
            }
        }
    }

    public static synchronized cc a() {
        cc ccVar;
        synchronized (cc.class) {
            if (g == null) {
                g = new cc();
            }
            ccVar = g;
        }
        return ccVar;
    }

    private /* synthetic */ void a(List<WeakReference<View>> list, View view) {
        a(list, view, (Integer) null);
    }

    private /* synthetic */ boolean a(View view, List<View> list) {
        if (pg.h().g()) {
            return false;
        }
        int size = list.size() - 1;
        while (size >= 0) {
            View view2 = list.get(size);
            if (view.getRootView() == view2) {
                break;
            } else if (rb.e(view2).contains(rb.e(view))) {
                return true;
            } else {
                size--;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public HashMap<View, HashMap<WeakReference<View>, Rect>> m6a() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m8a() {
        List<Object> b2 = pg.h().b("screen");
        if (b2 != null && !b2.isEmpty()) {
            mc a2 = sc.a().a();
            for (Object obj : b2) {
                String obj2 = obj.toString();
                if (a2 != null && obj2.startsWith("!") && obj2.endsWith(a2.a()) && obj2.length() == a2.a().length() + 1) {
                    gd.a(0, wd.e("\u0002223w%2'>/2%w(922/$(#(!$w2432$9{wd$"), a2.a());
                    return false;
                } else if (obj2.compareTo("*") == 0) {
                    gd.b(0, qb.a(" \u0010\u0010\u0011U\u0007\u0010\u0005\u001c\r\u0010\u0007U\u0006\u0003\u0006\u0007\u001aU\u0010\u0016\u0011\u0010\u0006\u001bC\u001c\u0010U\u0010\u0010\r\u0006\n\u0001\n\u0003\u0006"));
                    return true;
                } else if (a2 != null && a2.a().equalsIgnoreCase(obj2)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m7a() {
        synchronized (this.e) {
            this.e.clear();
        }
    }
}
