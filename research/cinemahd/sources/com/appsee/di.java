package com.appsee;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Point;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.ViewConfiguration;
import java.util.Locale;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class di {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1988a;
    private static boolean b;
    private static boolean c;
    private static final DisplayMetrics d = new DisplayMetrics();
    private static final Point e = new Point();
    private static String f;
    private static final li g = new li();
    private static int h;
    private static int i;
    private static String j;
    private static float k;
    private static String l;
    private static boolean m;
    private static String n;
    private static String o;

    di() {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean a(zn znVar) {
        return znVar == zn.e || znVar == zn.f2095a;
    }

    /* renamed from: b  reason: collision with other method in class */
    public static String m17b() throws PackageManager.NameNotFoundException {
        b();
        return n;
    }

    public static String c() {
        if (o == null) {
            o = ho.a().getPackageName();
            if (yb.a(o)) {
                o = "";
            }
        }
        return o;
    }

    public static String d() {
        return Build.MODEL;
    }

    /* renamed from: e  reason: collision with other method in class */
    public static String m21e() {
        if (j == null) {
            Locale locale = ho.a().getResources().getConfiguration().locale;
            j = String.format(rc.a("$d,2r"), new Object[]{locale.getLanguage(), locale.getCountry()});
        }
        return j;
    }

    public static String f() {
        if (f == null) {
            f = Build.MANUFACTURER;
        }
        return f;
    }

    /* renamed from: g  reason: collision with other method in class */
    static synchronized boolean m24g() {
        boolean z;
        synchronized (di.class) {
            if (!c) {
                boolean z2 = true;
                c = true;
                Dimension a2 = b().a(true);
                if (a((float) a2.c()) < 600.0f || a((float) a2.a()) < 600.0f) {
                    z2 = false;
                }
                b = z2;
            }
            z = b;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(Dimension[] dimensionArr, Display display, zn znVar) {
        c(dimensionArr, display, znVar);
        int i2 = 3;
        while (true) {
            int i3 = i2 - 1;
            if (i2 <= 0) {
                return;
            }
            if (dimensionArr[0].b() > 1.0d || dimensionArr[1].b() > 1.0d) {
                gd.b(1, rc.a("Sruex~op!pdcu~op!dbedro7r~{rr7hy!gneue`~u7nehroc`chxo"));
                try {
                    Thread.sleep(100);
                } catch (InterruptedException unused) {
                }
                c(dimensionArr, display, znVar);
                i2 = i3;
            } else {
                return;
            }
        }
    }

    /* renamed from: b  reason: collision with other method in class */
    static long m14b() {
        return SystemClock.elapsedRealtime();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0014, code lost:
        if (r4.getWidth() > r4.getHeight()) goto L_0x0016;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0028, code lost:
        if (r0.c() > r0.a()) goto L_0x0016;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ com.appsee.zn b(android.view.Display r4) {
        /*
            com.appsee.pg r0 = com.appsee.pg.h()
            boolean r0 = r0.z()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0018
            int r0 = r4.getWidth()
            int r3 = r4.getHeight()
            if (r0 <= r3) goto L_0x002b
        L_0x0016:
            r2 = 1
            goto L_0x002b
        L_0x0018:
            com.appsee.Dimension r0 = new com.appsee.Dimension
            r0.<init>()
            a((com.appsee.Dimension) r0, (android.view.Display) r4, (boolean) r2)
            int r3 = r0.c()
            int r0 = r0.a()
            if (r3 <= r0) goto L_0x002b
            goto L_0x0016
        L_0x002b:
            int r4 = r4.getRotation()
            if (r4 == 0) goto L_0x0051
            if (r4 == r1) goto L_0x004a
            r0 = 2
            if (r4 == r0) goto L_0x0043
            r0 = 3
            if (r4 == r0) goto L_0x003c
            com.appsee.zn r4 = com.appsee.zn.b
            goto L_0x0057
        L_0x003c:
            com.appsee.zn r4 = com.appsee.zn.f2095a
            if (r2 != 0) goto L_0x0057
            com.appsee.zn r4 = com.appsee.zn.h
            return r4
        L_0x0043:
            com.appsee.zn r4 = com.appsee.zn.f
            if (r2 == 0) goto L_0x0057
            com.appsee.zn r4 = com.appsee.zn.f2095a
            return r4
        L_0x004a:
            com.appsee.zn r4 = com.appsee.zn.e
            if (r2 != 0) goto L_0x0057
            com.appsee.zn r4 = com.appsee.zn.f
            return r4
        L_0x0051:
            com.appsee.zn r4 = com.appsee.zn.h
            if (r2 == 0) goto L_0x0057
            com.appsee.zn r4 = com.appsee.zn.e
        L_0x0057:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.di.b(android.view.Display):com.appsee.zn");
    }

    /* renamed from: g  reason: collision with other method in class */
    public static String m23g() {
        if (l == null) {
            l = Build.VERSION.RELEASE;
            gd.a(1, lf.a("UBM&Pctuoih<#b"), Integer.valueOf(Build.VERSION.SDK_INT));
        }
        return l;
    }

    /* access modifiers changed from: private */
    @TargetApi(17)
    public static /* synthetic */ void a(Dimension dimension, Display display, boolean z) {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 17) {
            display.getRealSize(e);
            Point point = e;
            dimension.a(point.x, point.y);
        } else if (i2 >= 13) {
            try {
                dimension.a(((Integer) Display.class.getMethod(rc.a("fruE``V~eci"), new Class[0]).invoke(display, new Object[0])).intValue(), ((Integer) Display.class.getMethod(lf.a("acrTgqNcoanr"), new Class[0]).invoke(display, new Object[0])).intValue());
            } catch (Exception unused) {
                dimension.a(0, 0);
            }
        } else if (i2 >= 12) {
            try {
                dimension.a(((Integer) Display.class.getMethod(rc.a("pdcSr`{V~eci"), new Class[0]).invoke(display, new Object[0])).intValue(), ((Integer) Display.class.getMethod(lf.a("acrTcgjNcoanr"), new Class[0]).invoke(display, new Object[0])).intValue());
            } catch (Exception unused2) {
                dimension.a(0, 0);
            }
        } else {
            dimension.a(display.getWidth(), display.getHeight());
        }
        if (z) {
            dimension.a(dimension.a(), dimension.c());
        }
    }

    private static /* synthetic */ void c(Dimension[] dimensionArr, Display display, zn znVar) {
        boolean a2 = a(znVar);
        b(dimensionArr[0], display, a2);
        a(dimensionArr[1], display, a2);
    }

    static int g() {
        e();
        return h;
    }

    /* renamed from: e  reason: collision with other method in class */
    public static int m20e() {
        return Build.VERSION.SDK_INT;
    }

    @TargetApi(14)
    /* renamed from: e  reason: collision with other method in class */
    static boolean m22e() {
        if (Build.VERSION.SDK_INT >= 14) {
            return ViewConfiguration.get(ho.a()).hasPermanentMenuKey();
        }
        return true;
    }

    static float e() {
        try {
            Runtime runtime = Runtime.getRuntime();
            return (1.0f - (((float) runtime.freeMemory()) / ((float) runtime.totalMemory()))) * 100.0f;
        } catch (Exception unused) {
            return -1.0f;
        }
    }

    public static float b() {
        return ho.a().getResources().getConfiguration().fontScale;
    }

    public static String a() {
        return Build.VERSION.INCREMENTAL;
    }

    static float a(float f2) {
        e();
        return f2 / k;
    }

    @TargetApi(23)
    /* renamed from: b  reason: collision with other method in class */
    static JSONObject m18b() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(rc.a("tnsdy`zd"), Build.VERSION.CODENAME);
            if (Build.VERSION.SDK_INT >= 23) {
                jSONObject.put(lf.a("ucestorYvgren"), Build.VERSION.SECURITY_PATCH);
                jSONObject.put(rc.a("cvrr^xr"), Build.VERSION.BASE_OS);
                jSONObject.put(lf.a("vtcpocqYubmYohr"), Build.VERSION.PREVIEW_SDK_INT);
            }
            jSONObject.put(rc.a("cx`ee"), Build.BOARD);
            jSONObject.put(lf.a("diirjigbct"), Build.BOOTLOADER);
            jSONObject.put(rc.a("ce`ye"), Build.BRAND);
            jSONObject.put(lf.a("evsYgdo"), Build.CPU_ABI);
            jSONObject.put(rc.a("tqb^vc~3"), Build.CPU_ABI2);
            jSONObject.put(lf.a("bcpoec"), Build.DEVICE);
            jSONObject.put(rc.a("e~rgmvx"), Build.DISPLAY);
            jSONObject.put(lf.a("`ohactvtohr"), Build.FINGERPRINT);
            jSONObject.put(rc.a("ndu"), Build.HOST);
            jSONObject.put(lf.a("ngtbqgtc"), Build.HARDWARE);
            jSONObject.put(rc.a("~e"), Build.ID);
            jSONObject.put(lf.a("vtibser"), Build.PRODUCT);
            jSONObject.put(rc.a("sve~n"), Build.VERSION.SDK_INT >= 14 ? Build.getRadioVersion() : Build.RADIO);
            jSONObject.put(lf.a("rgau"), Build.TAGS);
            jSONObject.put(rc.a("chzd"), Build.TIME);
            jSONObject.put(lf.a("rvc"), Build.TYPE);
            jSONObject.put(rc.a("ut~ms^brrs"), Build.USER);
            jSONObject.put(lf.a("shmhiqh"), rc.a("tyjyn`o"));
            if (Build.VERSION.SDK_INT >= 9) {
                jSONObject.put(lf.a("uctogj"), Build.SERIAL);
            }
            if (Build.VERSION.SDK_INT >= 21) {
                JSONArray jSONArray = new JSONArray();
                String[] strArr = Build.SUPPORTED_ABIS;
                int length = strArr.length;
                int i2 = 0;
                while (i2 < length) {
                    String str = strArr[i2];
                    i2++;
                    jSONArray.put(str);
                }
                jSONObject.put(rc.a("dtgqxscds^vc~r"), jSONArray);
            }
        } catch (Exception unused) {
        }
        return jSONObject;
    }

    /* renamed from: b  reason: collision with other method in class */
    public static int m13b() throws PackageManager.NameNotFoundException {
        b();
        return i;
    }

    private static /* synthetic */ void b(Dimension dimension, Display display, boolean z) {
        display.getMetrics(d);
        DisplayMetrics displayMetrics = d;
        dimension.a(displayMetrics.widthPixels, displayMetrics.heightPixels);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 == 11 || i2 == 12) {
            Resources resources = ho.a().getResources();
            int identifier = resources.getIdentifier(rc.a("ovw~fvu~ny^u`e^d~fu"), lf.a("bokch"), rc.a("`yeen~e"));
            if (identifier <= 0 || resources.getDimensionPixelSize(identifier) <= 0) {
                int identifier2 = resources.getIdentifier(lf.a("urgrsuYdgtYncoanr"), rc.a("e~lro"), lf.a("ghbtiob"));
                if (identifier2 > 0 && resources.getDimensionPixelSize(identifier2) > 0) {
                    dimension.a(dimension.a() - resources.getDimensionPixelSize(identifier2));
                }
            } else {
                dimension.a(dimension.a() - resources.getDimensionPixelSize(identifier));
            }
        }
        if (z) {
            dimension.a(dimension.a(), dimension.c());
        }
    }

    /* renamed from: b  reason: collision with other method in class */
    public static mg m16b() {
        Application a2 = ho.a();
        if (a2.checkCallingOrSelfPermission(rc.a("`yeen~e9qrszhdr~ny/VBTDDRHORU@NEJHRC@CD")) != 0) {
            return mg.f2030a;
        }
        mg mgVar = mg.f2030a;
        NetworkInfo activeNetworkInfo = ((ConnectivityManager) a2.getSystemService(lf.a("eihhceropor"))).getActiveNetworkInfo();
        if (activeNetworkInfo == null) {
            mgVar = mg.d;
        } else if (activeNetworkInfo.getType() == 1 || activeNetworkInfo.getType() == 6 || activeNetworkInfo.getType() == 7 || (Build.VERSION.SDK_INT > 13 && activeNetworkInfo.getType() == 9)) {
            mgVar = mg.b;
        } else if (activeNetworkInfo.getType() == 0 || activeNetworkInfo.getType() == 4 || activeNetworkInfo.getType() == 5) {
            mgVar = mg.c;
        }
        return (activeNetworkInfo == null || activeNetworkInfo.isConnected()) ? mgVar : mg.d;
    }

    /* renamed from: b  reason: collision with other method in class */
    static li m15b() {
        return g;
    }

    /* renamed from: b  reason: collision with other method in class */
    public static synchronized boolean m19b() {
        boolean z;
        synchronized (di.class) {
            if (!f1988a) {
                boolean z2 = true;
                f1988a = true;
                Dimension[] a2 = b().a(true);
                if (a2[0].equals(a2[1])) {
                    z2 = false;
                }
                m = z2;
            }
            z = m;
        }
        return z;
    }
}
