package com.appsee;

import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.TouchDelegate;
import android.view.View;
import java.lang.ref.WeakReference;

class n extends TouchDelegate {

    /* renamed from: a  reason: collision with root package name */
    private TouchDelegate f2035a;
    final /* synthetic */ r b;
    /* access modifiers changed from: private */
    public WeakReference<View> c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    n(r rVar, View view) {
        super(new Rect(), view == null ? rVar.a() : view);
        this.b = rVar;
        if (view != null) {
            this.c = new WeakReference<>(view);
            this.f2035a = view.getTouchDelegate();
        }
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        oj.b((z) new a(this, motionEvent));
        TouchDelegate touchDelegate = this.f2035a;
        if (touchDelegate != null) {
            return touchDelegate.onTouchEvent(motionEvent);
        }
        return false;
    }
}
