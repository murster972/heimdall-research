package com.appsee;

import java.io.File;
import java.io.FilenameFilter;

/* renamed from: com.appsee.if  reason: invalid class name */
class Cif implements FilenameFilter {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ String f2009a;

    Cif(String str) {
        this.f2009a = str;
    }

    public boolean accept(File file, String str) {
        return str.startsWith(this.f2009a) && str.length() > this.f2009a.length() && str.charAt(this.f2009a.length()) == '.';
    }
}
