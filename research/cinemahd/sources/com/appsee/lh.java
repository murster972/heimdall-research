package com.appsee;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;

class lh {
    private static final Object e = new Object();
    private static lh f;

    /* renamed from: a  reason: collision with root package name */
    private ScheduledThreadPoolExecutor f2025a = null;
    private long b = 0;
    private long c = 0;
    private HashMap<Long, fm> d = new HashMap<>();

    private /* synthetic */ lh() {
    }

    private /* synthetic */ void d() {
        try {
            if (a() && di.b() - this.c >= ((long) pg.h().e())) {
                this.c = di.b();
                xd.b().c(false);
            }
        } catch (Exception e2) {
            qe.a(e2, em.a("7G\u0000Z\u0000\u0015\u001b[RG\u0007[\u001c\\\u001cRRx6\u0015\u0018Z\u0007G\u001cT\u001e\\\u001cR"));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void e() {
        try {
            if (c() && xd.b().b() > ((long) pg.h().a())) {
                synchronized (this.d) {
                    if (this.b > 0) {
                        long b2 = xd.b().b() - this.b;
                        (this.d.containsKey(Long.valueOf(this.b)) ? this.d.get(Long.valueOf(this.b)) : new fm(this.b, b2)).a(b2);
                        if (!ub.a().a()) {
                            this.b = 0;
                            gd.a(1, ui.a("2\u0010\u0002\u0010\u0015\u0001\u0013\u0011V48'V\u0013\u001f\u001b\u001f\u0006\u001e\u0010\u0012U\u0017\u0013\u0002\u0010\u0004US\u0011"), Long.valueOf(b2));
                        }
                    } else if (ub.a().a()) {
                        this.b = xd.b().b() - ((long) pg.h().a());
                        this.d.put(Long.valueOf(this.b), new fm(this.b, (long) pg.h().a()));
                        gd.b(1, em.a("4Z\u0007[\u0016\u0015\u0002Z\u0001F\u001bW\u001ePRt<g"));
                    }
                }
            }
        } catch (Exception e2) {
            qe.a(e2, ui.a("3\u0007\u0004\u001a\u0004U\u0015\u001d\u0013\u0016\u001d\u001c\u0018\u0012V\u0013\u0019\u0007V48'"));
        }
    }

    private /* synthetic */ boolean f() {
        return a() || c();
    }

    public void b() {
        synchronized (e) {
            if (f()) {
                int L = pg.h().L();
                gd.a(1, ui.a("&\u0002\u0014\u0004\u0001\u001f\u001b\u0011U7\u0005\u0006\u0006\u0013\u0010V\"\u0017\u0001\u0015\u001d2\u001a\u0011U\u0013\u0003\u0013\u0007\u000fUS\u0011V\u0018\u001f\u0019\u001f\u0006\u0013\u0016\u0019\u001b\u0012\u0006"), Integer.valueOf(L));
                this.f2025a = new ScheduledThreadPoolExecutor(1);
                long j = (long) L;
                this.f2025a.scheduleWithFixedDelay(new ni(this), j, j, TimeUnit.MILLISECONDS);
            }
        }
    }

    public void c() {
        synchronized (this.d) {
            this.d.clear();
            this.b = 0;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m52a() {
        synchronized (e) {
            if (this.f2025a != null) {
                try {
                    gd.b(1, em.a("f\u0006Z\u0002E\u001b[\u0015\u0015\u0005T\u0006V\u001aQ\u001dR"));
                    this.f2025a.shutdownNow();
                    if (!this.f2025a.awaitTermination(5, TimeUnit.SECONDS)) {
                        gd.a(3, ui.a("4\u0006\u0005\u0005\u0010\u0013U!\u0014\u0002\u0016\u001e\u0011\u0019\u0012V\u001b\u0019\u0001V\u0001\u0013\u0007\u001b\u001c\u0018\u0014\u0002\u0010\u0012U\u0017\u0006V\u0010\u000e\u0005\u0013\u0016\u0002\u0010\u0012T"));
                    }
                    this.f2025a = null;
                } catch (Exception e2) {
                    qe.a(e2, em.a("7G\u0000Z\u0000\u0015\u0006G\u000b\\\u001cRRA\u001d\u0015\u0001A\u001dERT\u0002E\u0001P\u0017\u0015\u0005T\u0006V\u001aQ\u001dR"));
                }
            }
        }
    }

    public static synchronized lh a() {
        lh lhVar;
        synchronized (lh.class) {
            if (f == null) {
                f = new lh();
            }
            lhVar = f;
        }
        return lhVar;
    }

    public HashMap<Long, fm> a(boolean z) {
        HashMap<Long, fm> hashMap = this.d;
        if (!z) {
            return hashMap;
        }
        synchronized (hashMap) {
            this.d = new HashMap<>();
        }
        return hashMap;
    }

    public static JSONArray a(HashMap<Long, fm> hashMap) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        synchronized (hashMap) {
            for (Map.Entry<Long, fm> value : hashMap.entrySet()) {
                jSONArray.put(((fm) value.getValue()).a());
            }
        }
        return jSONArray;
    }
}
