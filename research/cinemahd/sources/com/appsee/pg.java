package com.appsee;

import com.facebook.ads.AdError;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import org.joda.time.DateTimeConstants;
import org.json.JSONException;
import org.json.JSONObject;

class pg {
    private static pg K0;
    private boolean A;
    private boolean A0;
    private boolean B;
    private boolean B0;
    private boolean C;
    private double C0;
    private List<i> D;
    private boolean D0;
    private int E;
    private boolean E0;
    private long F;
    private final HashMap<String, HashMap<String, List<Object>>> F0 = new HashMap<>();
    private List<md> G;
    private final ArrayList<String> G0 = new ArrayList<>();
    private boolean H;
    private sh H0;
    private boolean I;
    private boolean I0;
    private boolean J;
    private boolean J0;
    private int K;
    private String L;
    private boolean M;
    private boolean N;
    private boolean O;
    private boolean P;
    private boolean Q;
    private boolean R;
    private boolean S;
    private int T;
    private boolean U;
    private final jm V = oc.a().a(oi.a("y(T!S O5[3S(Tgi$H\"_)y/[)]\"^"));
    private boolean W;
    private boolean X;
    private int Y;
    private String Z;

    /* renamed from: a  reason: collision with root package name */
    private int f2052a;
    private boolean a0;
    private int b;
    private volatile HashMap<String, List<Object>> b0;
    private boolean c;
    private int c0;
    private double d;
    private int d0;
    private boolean e;
    private final Object e0 = new Object();
    private int f;
    private volatile JSONObject f0;
    private boolean g;
    private boolean g0;
    private boolean h;
    private boolean h0;
    private boolean i;
    private boolean i0;
    private String j;
    private boolean j0;
    private int k;
    private boolean k0;
    private boolean l;
    private boolean l0;
    private boolean m;
    private volatile JSONObject m0;
    private int n;
    private boolean n0;
    private boolean o;
    private boolean o0;
    private boolean p;
    private int p0;
    private boolean q;
    private boolean q0;
    private boolean r;
    private boolean r0;
    private EnumSet<tg> s;
    private boolean s0;
    private boolean t;
    private int t0;
    private boolean u;
    private int u0;
    private int v;
    private boolean v0;
    private boolean w;
    private boolean w0;
    private int x;
    private boolean x0;
    private int y;
    private double y0;
    private boolean z;
    private boolean z0;

    private /* synthetic */ pg() {
        try {
            b(new JSONObject());
        } catch (JSONException unused) {
            qe.a((Throwable) null, cc.a("s\\DAD\u000eW^FBOGXI\u0016JSHW[ZZ\u0016MY@PGQ[DOBGY@E"));
        }
    }

    private /* synthetic */ void A(boolean z2) {
        this.C = z2;
    }

    private /* synthetic */ void L(boolean z2) {
        this.p = z2;
    }

    private /* synthetic */ void N(boolean z2) {
        this.j0 = z2;
    }

    private /* synthetic */ void P(boolean z2) {
        this.w0 = z2;
    }

    private /* synthetic */ void Q(boolean z2) {
        this.I = z2;
    }

    private /* synthetic */ void S(boolean z2) {
        this.X = z2;
    }

    private /* synthetic */ void T(boolean z2) {
        this.w = z2;
    }

    private /* synthetic */ void X(boolean z2) {
        this.s0 = z2;
    }

    private /* synthetic */ void Y(boolean z2) {
        this.k0 = z2;
    }

    private /* synthetic */ void Z(boolean z2) {
        this.R = z2;
    }

    private /* synthetic */ List<Object> a(String str, String str2) {
        String str3 = str.contains(cc.a("v")) ? str.split(oi.a("z"))[1] : "DEFAULT_HIDE_COMPONENTS";
        if (!this.F0.containsKey(str3)) {
            this.F0.put(str3, new HashMap());
        }
        if (!this.F0.get(str3).containsKey(str2)) {
            this.F0.get(str3).put(str2, new ArrayList());
        }
        return (List) this.F0.get(str3).get(str2);
    }

    private /* synthetic */ void a0(boolean z2) {
        this.z0 = z2;
    }

    private /* synthetic */ void d(boolean z2) {
        this.z = z2;
    }

    private /* synthetic */ void f(boolean z2) {
        this.x0 = z2;
    }

    private /* synthetic */ void k(boolean z2) {
        this.D0 = z2;
    }

    private /* synthetic */ void m(boolean z2) {
        this.J = z2;
    }

    private /* synthetic */ void n(boolean z2) {
        this.o0 = z2;
    }

    private /* synthetic */ void p(int i2) {
        this.d0 = i2;
    }

    private /* synthetic */ void q(int i2) {
        this.y = i2;
    }

    private /* synthetic */ void s(boolean z2) {
        this.A0 = z2;
    }

    private /* synthetic */ void t(boolean z2) {
        this.a0 = z2;
    }

    private /* synthetic */ void v(boolean z2) {
        this.I0 = z2;
    }

    private /* synthetic */ void z(boolean z2) {
        this.e = z2;
    }

    /* access modifiers changed from: package-private */
    public int B() {
        return this.E;
    }

    /* access modifiers changed from: package-private */
    public boolean C() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public boolean D() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public int E() {
        return this.y;
    }

    /* access modifiers changed from: package-private */
    public boolean F() {
        return this.N;
    }

    /* access modifiers changed from: package-private */
    public boolean G() {
        return this.D0;
    }

    /* access modifiers changed from: package-private */
    public boolean H() {
        return this.B;
    }

    /* access modifiers changed from: package-private */
    public boolean I() {
        return this.B0;
    }

    /* access modifiers changed from: package-private */
    public boolean J() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public int K() {
        return this.K;
    }

    /* access modifiers changed from: package-private */
    public double M() {
        return this.y0;
    }

    /* access modifiers changed from: package-private */
    public boolean O() {
        return this.a0;
    }

    /* access modifiers changed from: package-private */
    public boolean R() {
        return this.o0;
    }

    /* access modifiers changed from: package-private */
    public boolean U() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean V() {
        return this.v0;
    }

    /* access modifiers changed from: package-private */
    public boolean W() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b  reason: collision with other method in class */
    public boolean m86b() {
        return this.R;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) throws JSONException {
        this.Z = str;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e  reason: collision with other method in class */
    public String m89e() {
        return this.Z;
    }

    /* access modifiers changed from: package-private */
    public int g() {
        return this.T;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public Boolean m96h() {
        return Boolean.valueOf(this.n0);
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.s0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: j  reason: collision with other method in class */
    public boolean m101j() {
        return this.r0;
    }

    /* access modifiers changed from: package-private */
    public int l() {
        int i2 = this.f;
        return i2 == 0 ? DateTimeConstants.MILLIS_PER_MINUTE : i2;
    }

    /* access modifiers changed from: package-private */
    public boolean o() {
        return this.X;
    }

    /* access modifiers changed from: package-private */
    public boolean r() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public boolean u() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public boolean w() {
        return this.I;
    }

    /* access modifiers changed from: package-private */
    public boolean x() {
        return this.A0;
    }

    /* access modifiers changed from: package-private */
    public boolean y() {
        return this.H;
    }

    private /* synthetic */ void B(boolean z2) {
        this.u = z2;
    }

    private /* synthetic */ void C(boolean z2) {
        this.g0 = z2;
    }

    private /* synthetic */ void D(boolean z2) {
        this.c = z2;
    }

    private /* synthetic */ void F(boolean z2) {
        this.n0 = z2;
    }

    private /* synthetic */ void G(boolean z2) {
        this.q0 = z2;
    }

    private /* synthetic */ void H(boolean z2) {
        this.S = z2;
    }

    private /* synthetic */ void I(boolean z2) {
        this.H = z2;
    }

    private /* synthetic */ void J(boolean z2) {
        this.i0 = z2;
    }

    private /* synthetic */ void K(boolean z2) {
        this.B0 = z2;
    }

    private /* synthetic */ void O(boolean z2) {
        this.B = z2;
    }

    private /* synthetic */ void R(boolean z2) {
        this.U = z2;
    }

    private /* synthetic */ void U(boolean z2) {
        this.N = z2;
    }

    private /* synthetic */ void V(boolean z2) {
        this.W = z2;
    }

    private /* synthetic */ void W(boolean z2) {
        this.l0 = z2;
    }

    private /* synthetic */ void e(String str) {
        this.L = str;
    }

    private /* synthetic */ void g(boolean z2) {
        this.m = z2;
    }

    public static synchronized pg h() {
        pg pgVar;
        synchronized (pg.class) {
            if (K0 == null) {
                K0 = new pg();
            }
            pgVar = K0;
        }
        return pgVar;
    }

    private /* synthetic */ void i(boolean z2) {
        this.r0 = z2;
    }

    private /* synthetic */ void j(boolean z2) {
        this.h = z2;
    }

    private /* synthetic */ void l(int i2) {
        this.n = i2;
    }

    private /* synthetic */ void o(int i2) {
        this.f = i2;
    }

    private /* synthetic */ void q(boolean z2) {
        this.q = z2;
    }

    private /* synthetic */ void r(boolean z2) {
        this.P = z2;
    }

    private /* synthetic */ void u(boolean z2) {
        this.l = z2;
    }

    private /* synthetic */ void w(boolean z2) {
        this.M = z2;
    }

    private /* synthetic */ void x(boolean z2) {
        this.g = z2;
    }

    private /* synthetic */ void y(boolean z2) {
        this.h0 = z2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: A  reason: collision with other method in class */
    public boolean m77A() {
        return this.W;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: E  reason: collision with other method in class */
    public boolean m79E() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: L  reason: collision with other method in class */
    public boolean m81L() {
        return this.g0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: M  reason: collision with other method in class */
    public int m82M() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public boolean N() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public boolean P() {
        return this.l0;
    }

    /* access modifiers changed from: package-private */
    public boolean Q() {
        return this.j0;
    }

    /* access modifiers changed from: package-private */
    public boolean S() {
        return this.S;
    }

    /* access modifiers changed from: package-private */
    public boolean T() {
        return this.w0;
    }

    /* access modifiers changed from: package-private */
    public boolean X() {
        return this.O;
    }

    /* access modifiers changed from: package-private */
    public boolean Y() {
        return this.i0;
    }

    /* access modifiers changed from: package-private */
    public boolean Z() {
        return this.J0;
    }

    /* access modifiers changed from: package-private */
    public List<Object> b(String str) {
        if (this.b0 != null) {
            return this.b0.get(str);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: c  reason: collision with other method in class */
    public boolean m87c() {
        return this.x0;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.f2052a;
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.J;
    }

    /* access modifiers changed from: package-private */
    public int k() {
        return this.d0;
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public boolean p() {
        return this.k0;
    }

    /* access modifiers changed from: package-private */
    public boolean s() {
        return this.P;
    }

    /* access modifiers changed from: package-private */
    public boolean t() {
        return this.z0;
    }

    /* access modifiers changed from: package-private */
    public boolean v() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean z() {
        return this.m;
    }

    private /* synthetic */ void E(boolean z2) {
        this.o = z2;
    }

    private /* synthetic */ void b(long j2) {
        this.F = j2;
    }

    private /* synthetic */ void c(boolean z2) {
        this.E0 = z2;
    }

    private /* synthetic */ void d(int i2) {
        this.T = i2;
    }

    private /* synthetic */ void e(boolean z2) {
        this.O = z2;
    }

    private /* synthetic */ void f(int i2) {
        this.v = i2;
    }

    private /* synthetic */ void g(int i2) {
        this.c0 = i2;
    }

    private /* synthetic */ void i(int i2) {
        this.x = i2;
    }

    private /* synthetic */ void j(int i2) {
        this.p0 = i2;
    }

    private /* synthetic */ void l(boolean z2) {
        this.r = z2;
    }

    private /* synthetic */ void n(int i2) {
        this.k = i2;
    }

    private /* synthetic */ void o(boolean z2) {
        this.Q = z2;
    }

    private /* synthetic */ void p(boolean z2) {
        this.v0 = z2;
    }

    /* access modifiers changed from: package-private */
    public int A() {
        return this.c0 | -16777216;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: B  reason: collision with other method in class */
    public boolean m78B() {
        return this.E0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: K  reason: collision with other method in class */
    public boolean m80K() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public int L() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: M  reason: collision with other method in class */
    public String m83M() {
        String replace = UUID.randomUUID().toString().replace(cc.a("\u001b"), "");
        gd.a(2, oi.a("\u0004U)\\.]\u0015_6O\"I3s#\u001az\u001abI"), replace);
        return replace;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: k  reason: collision with other method in class */
    public boolean m102k() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.i;
    }

    private /* synthetic */ void b(double d2) {
        this.y0 = d2;
    }

    private /* synthetic */ void c(double d2) {
        this.C0 = d2;
    }

    private /* synthetic */ void d(String str) {
        if (yb.a(str) || !this.F0.containsKey(str)) {
            str = "DEFAULT_HIDE_COMPONENTS";
        }
        this.b0 = this.F0.get(str);
        gd.a(1, cc.a("}SZBGXI\u0016f_JSmYCFAXKXZE\u000ePAD\u000eEMDKS@\u0016\t\u0013]\u0011"), str);
    }

    private /* synthetic */ void e(int i2) {
        this.E = i2;
    }

    private /* synthetic */ void k(int i2) {
        this.K = i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: g  reason: collision with other method in class */
    public boolean m91g() {
        return this.M;
    }

    /* access modifiers changed from: package-private */
    public int j() {
        return this.Y;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: l  reason: collision with other method in class */
    public boolean m103l() {
        return this.U;
    }

    private /* synthetic */ void h(int i2) {
        this.b = i2;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) throws JSONException {
        this.J0 = z2;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return this.x;
    }

    private /* synthetic */ void b(JSONObject jSONObject) throws JSONException {
        c(jSONObject.optInt(oi.a("\u0006t\u0015n/H\"I/U+^"), 5000));
        a(jSONObject.optInt(cc.a("w^FhDO[KrKBKUZ_AXgXZS\\@OZ"), AdError.SERVER_ERROR_CODE));
        O(jSONObject.optBoolean(oi.a("{2N(~\"N\"Y3i$H\"_)I"), false));
        b(jSONObject.optLong(cc.a("uFC@]}_TS"), 102400));
        a(sh.values()[jSONObject.optInt(oi.a("\u0004V._)N\u000bU I\u0012J+U&^\u0017U+S$C"), 1)]);
        a(jSONObject.optLong(cc.a("jSLCIpBWIE"), 0));
        M(jSONObject.optBoolean(oi.a("~\"V\"N\"o)\\.T.I/_#l.^\"U\u0001S+_"), true));
        s(jSONObject.optBoolean(cc.a("rKBKUZwMBGY@E"), false));
        U(jSONObject.optBoolean(oi.a("~\"N\"Y3y5[4R\"I"), false));
        e(jSONObject.optBoolean(cc.a("jSZSMBiS]B[DKE"), false));
        Z(jSONObject.optBoolean(oi.a("\u0003_3_$N\u0017U7O7I"), true));
        n(jSONObject.optBoolean(cc.a("rKBKUZ{KX[E"), true));
        c(jSONObject.optBoolean(oi.a("~.I&X+_\u0006J7|5[*_4~\"N\"Y3S(T"), false));
        v(jSONObject.optBoolean(cc.a("rGEOTBSoEWXMs@UARKD"), false));
        W(jSONObject.optBoolean(oi.a("\u0003S4[%V\"{4C)Y\u0002T$U#_5h\"I\"N"), false));
        l(jSONObject.optBoolean(cc.a("j_]WLZKtAC@RGXItAN}S@EGBG@GBWs@^OXMSCS@B"), false));
        K(jSONObject.optBoolean(oi.a("~.I&X+_\f_>X([5^\u000fU(Q\u0015_$U1_5C"), false));
        J(jSONObject.optBoolean(cc.a("j_]WLZKd[X@_@Q}BOBK@PKDKXMS"), false));
        G(jSONObject.optBoolean(oi.a("\u0002T&X+_\u0014_)I.N.L\"?N5[\u0017[4I"), false));
        B(jSONObject.optBoolean(cc.a("~GRK@F[B"), false));
        P(jSONObject.optBoolean(oi.a("\u000fS#_\u0014_)I.N.L\"l._0I"), false));
        x(jSONObject.optBoolean(cc.a("IXADKpBWIeKU[DKaGXJYYE"), false));
        k(jSONObject.optBoolean(oi.a("\u000e])U5_\u0012T5_4J(T4S1_\u0012T$V.Y,[%V\"I"), false));
        Q(jSONObject.optBoolean(cc.a("gX]BOZB}KOLYODJ~AYEE"), true));
        X(jSONObject.optBoolean(oi.a("s)I3[+V\n_)O\u0013U2Y/v.I3_)_5I"), true));
        Y(jSONObject.optBoolean(cc.a("gX]BOZBaGXJYYuOZBTOUEE"), true));
        h(jSONObject.optInt(oi.a("\n[?~.H\"Y3U5C\u0014S=_"), 52428800));
        k(jSONObject.optInt(cc.a("cWVsXS@B~DAFKDZ_KE"), 0));
        e(jSONObject.optInt(oi.a("\n[?l.^\"U\u000b_)]3R"), 0));
        i(jSONObject.optInt(cc.a("crdY[D@WB_@QgXZS\\@OZ"), 1000));
        a(jSONObject.optDouble(oi.a("\nS)q\"C%U&H#r\"S R3n/H\"I/U+^"), 0.25d));
        q(jSONObject.optBoolean(cc.a("aZJ~GBzS]B"), false));
        j(jSONObject.optInt(oi.a("j&O4_\u0011[+S#[3S(T\u0003O5[3S(T"), AdError.SERVER_ERROR_CODE));
        o(jSONObject.optInt(cc.a("~ZB^bG[KY[BgXc_BZGEKUAXJE"), DateTimeConstants.MILLIS_PER_MINUTE));
        b(jSONObject.optBoolean(oi.a("\u0015_$U5^\n_3[#[3["), false));
        j(jSONObject.optBoolean(cc.a("dKUADJbKEZ`GRKY"), false));
        a(jSONObject.optBoolean(oi.a("h\"Y(H#l.^\"U"), false));
        w(jSONObject.optBoolean(cc.a("eKX]_Z_XScW]]GXIfKDy_@RAA"), false));
        e(jSONObject.optString(oi.a("i\"I4S(T\u000e^"), ""));
        z(jSONObject.optBoolean(cc.a("eE_^eKX]_Z_XSx_]_L_B_ZOzS]B"), false));
        R(jSONObject.optBoolean(oi.a("\u0013[,_\u0014Y5_\"T4R(N4m/_){)S*[3S)]"), false));
        i(jSONObject.optBoolean(cc.a("zWES}U\\SKX]^AB]aFS@dKUWUBS\\`GSYeMDAZB_@Q"), true));
        t(jSONObject.optBoolean(oi.a("n&Q\"i$H\"_)I/U3I\u0010R\"T\u0015S7J+S)]"), false));
        A(jSONObject.optBoolean(cc.a("zWES}U\\SKX]^AB]aFS@bACM^oUZ_XS"), false));
        L(jSONObject.optBoolean(oi.a("\u0013[,_\u0014Y5_\"T4R(N4m/_)l._0i$H(V+S)]"), true));
        u(jSONObject.optBoolean(cc.a("c^ZAWJw^FgUAX"), false));
        V(jSONObject.optBoolean(oi.a("\u0012J+U&^\u0005_)Y/W&H,I"), false));
        m(jSONObject.optBoolean(cc.a("{FBYORh_BS]y@u\\W]^"), true));
        I(jSONObject.optBoolean(oi.a("\u0012J+U&^\u000bU |.H4N"), false));
        f(jSONObject.optBoolean(cc.a("c^ZAWJ{KBOROBOy\\RKDKR"), true));
        l(jSONObject.optInt(oi.a("o7V([#h\"N5C\u0006N3_*J3I"), 3));
        m(jSONObject.optInt(cc.a("{FBYOR|SZDWeBSKFz_CS"), 0));
        d(jSONObject.optBoolean(oi.a("o7V([#n(O$R\u0002L\"T3I"), false));
        T(jSONObject.optBoolean(cc.a("{FBYORx_JSAy@u\\W]^"), false));
        F(jSONObject.optBoolean(oi.a("\u0012I\"y(W7H\"I4S(T"), true));
        C(jSONObject.optBoolean(cc.a("c]SbYMWBaGXJYY{OXOQKD"), false));
        D(jSONObject.optBoolean(oi.a("\u0012I\"l.^\"U\u000f[5^0[5_\u0004[)L&I"), false));
        r(jSONObject.optBoolean(cc.a("{EKaKTx_KA}XOF]^ABoZYWWE"), false));
        h(jSONObject.optBoolean(oi.a("\u0012I\"m\"X\u0011S\"M\u0014T&J4R(N\u0001O+V4Y5_\"T"), true));
        N(jSONObject.optBoolean(cc.a("{EKoxc"), false));
        p(jSONObject.optBoolean(oi.a("\u0011[+S#[3_\n_3[#[3[\rI(T"), false));
        d(jSONObject.optInt(cc.a("x_JSAtGB\\WZS"), 0));
        b((double) jSONObject.optInt(oi.a("\u0011S#_(|\u0017i"), 0));
        q(jSONObject.optInt(cc.a("`GRKYfSGQFB"), 0));
        p(jSONObject.optInt(oi.a("\u0011S#_(m.^3R"), 0));
        f(jSONObject.optInt(cc.a("yWZUFRAQgXZS\\@OZ"), 1000));
        c(jSONObject.optDouble(oi.a("\u0010_%l._0|2V+i$H\"_)n/H\"I/U+^"), 0.7d));
        a0(jSONObject.optBoolean(cc.a("eADZg[S[SlO}_TS"), false));
        o(jSONObject.optBoolean(oi.a("i(H3k2_2_\u0003_4Y\"T#S)]"), true));
        H(jSONObject.optBoolean(cc.a("{EKbK[^`GRKYh_BS`WCS"), true));
        E(jSONObject.optBoolean(oi.a("m.T#U0y&V+X&Y,r(U,I\u0015_!V\"Y3S(T"), false));
        n(jSONObject.optInt(cc.a("y_@RAAmWBZLWM]fYA]]dKPBSMBGY@rKFZ^"), 5));
        b(jSONObject.optInt(oi.a("\u0004H&I/o7V([#n.W\"U2N\nS+V.I"), 0));
        g(jSONObject.optInt(cc.a("eKX]_Z_XScW]]GXIuAZAD"), 1644825));
        g(jSONObject.optBoolean(oi.a("\u0012I\"~\"J5_$[3_#u5S\"T3[3S(T"), true));
        y(jSONObject.optBoolean(cc.a("f_JSlOzDOX]FODKXMO"), false));
        S(jSONObject.optBoolean(oi.a("\u0014_)^\n_3[#[3[\u0012J+U&^\u0002H5U5I"), false));
    }

    private /* synthetic */ void c(int i2) {
        this.u0 = i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: e  reason: collision with other method in class */
    public boolean m90e() {
        return this.h0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public String m97h() {
        return this.j;
    }

    private /* synthetic */ void a(long j2) {
        this.s = tg.a(j2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: d  reason: collision with other method in class */
    public boolean m88d() {
        return this.q0;
    }

    /* JADX WARNING: type inference failed for: r3v5, types: [org.json.JSONObject] */
    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) throws Exception {
        if (jSONObject.has(oi.a("\u0004V._)N\u000e^"))) {
            c(jSONObject.getString(oi.a("\u0004V._)N\u000e^")));
            pc.b();
            synchronized (this.e0) {
                this.f0 = jSONObject;
                this.m0 = em.a(jSONObject);
                this.G0.clear();
                h();
                M();
                m();
                b(this.f0);
                mc a2 = sc.a().a();
                if (a2 != null) {
                    a(a2.a());
                }
            }
            el.a().a(this.f0.optJSONArray(cc.a("jSZSMBkNZS\\XOZ}reE")));
            this.V.c();
            this.V.a();
            return;
        }
        qe.a(cc.a("mZGS@BgR\u000e_]\u0016C_]EGXI\u0016HDA[\u000eUAXH_I\u0016\\S_CKEZ\u0017"));
        throw null;
    }

    private /* synthetic */ void m(int i2) {
        this.f2052a = i2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: M  reason: collision with other method in class */
    public boolean m84M() {
        return this.I0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m  reason: collision with other method in class */
    public boolean m107m() {
        return this.u;
    }

    private /* synthetic */ void M(boolean z2) {
        this.t = z2;
    }

    /* renamed from: m  reason: collision with other method in class */
    public String m105m() {
        return this.L;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m  reason: collision with other method in class */
    public List<md> m106m() {
        return this.G;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: m  reason: collision with other method in class */
    public int m104m() {
        return this.p0;
    }

    /* access modifiers changed from: package-private */
    public double m() {
        return this.C0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public long m94h() {
        return this.F;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0016, code lost:
        if (r6.equals("hide") != false) goto L_0x0018;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(java.lang.String r5, java.lang.String r6, java.lang.String r7) {
        /*
            r4 = this;
            java.lang.String r0 = "skip"
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0018
            java.lang.String r0 = "class"
            boolean r0 = r6.equals(r0)
            if (r0 != 0) goto L_0x0018
            java.lang.String r0 = "hide"
            boolean r0 = r6.equals(r0)
            if (r0 == 0) goto L_0x0032
        L_0x0018:
            java.lang.Class r7 = java.lang.Class.forName(r7)     // Catch:{ ClassNotFoundException -> 0x001d }
            goto L_0x0032
        L_0x001d:
            java.lang.String r0 = "mW@XAB\u000ePGXJ\u0016MZOE]\u0016AP\u000eBWFK\u0016\t\u0013]\u0011\u000ePAD\u000e^GRK\u0016MYCFAXKXZ\u0016AP\u000eBWFK\u0016\t\u0013]\u0011\u0000\u0016ESW\u0016\u0013\u0016\t\u0013]\u0011"
            java.lang.String r0 = com.appsee.cc.a((java.lang.String) r0)
            r1 = 3
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r2 = 0
            r1[r2] = r7
            r2 = 1
            r1[r2] = r6
            r3 = 2
            r1[r3] = r5
            com.appsee.gd.a(r2, r0, r1)
        L_0x0032:
            java.util.List r5 = r4.a(r5, r6)
            r5.add(r7)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.pg.a(java.lang.String, java.lang.String, java.lang.String):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public EnumSet<tg> m98h() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public List<i> m99h() {
        List<i> list = this.D;
        return list == null ? new ArrayList() : list;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public sh m95h() {
        return this.H0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public boolean m100h() {
        return this.q;
    }

    private /* synthetic */ void h(boolean z2) {
        this.i = z2;
    }

    private /* synthetic */ void a(sh shVar) {
        this.H0 = shVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public int m93h() {
        return this.t0;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) throws JSONException {
        synchronized (this.e0) {
            if (this.f0 != null) {
                gd.a(1, oi.a("h\"\\5_4R.T \u001a$O4N(WgI$H\"_)\u001a$U)\\.]2H&N.U)Ig\\(HgI$H\"_)\u001a`\u001f4\u001d"), str);
                this.V.b();
                d(str);
                boolean z2 = !this.G0.isEmpty();
                Iterator<String> it2 = this.G0.iterator();
                while (it2.hasNext()) {
                    String next = it2.next();
                    if (this.f0.has(next)) {
                        this.m0.put(next, this.f0.get(next));
                    } else {
                        this.m0.remove(next);
                    }
                }
                this.G0.clear();
                String format = String.format(cc.a("v\u000bE"), new Object[]{str});
                Iterator<String> keys = this.f0.keys();
                while (keys.hasNext()) {
                    String next2 = keys.next();
                    if (next2.endsWith(format)) {
                        String str2 = next2.split(oi.a("z"))[0];
                        gd.a(1, cc.a("oF^ZW_@Q\u000eE^SM_H_M\u0016ESW\u0016\t\u0013]\u0011\u000ePAD\u000eEMDKS@\u0016\t\u0013]\u0011"), str2, str);
                        this.G0.add(str2);
                        this.m0.put(str2, this.f0.get(next2));
                    }
                }
                if (z2 || !this.G0.isEmpty()) {
                    b(this.m0);
                }
                this.V.d();
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: h  reason: collision with other method in class */
    public double m92h() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) throws JSONException {
        this.A = z2;
    }

    private /* synthetic */ void a(int i2) {
        this.t0 = i2;
    }

    private /* synthetic */ void a(double d2) {
        this.d = d2;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.u0;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m85a() {
        return this.Q;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.n;
    }

    private /* synthetic */ void b(int i2) {
        this.Y = i2;
    }
}
