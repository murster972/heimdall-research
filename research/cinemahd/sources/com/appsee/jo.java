package com.appsee;

import android.graphics.Rect;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class jo implements k {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2014a;
    private po b;
    private long c;
    private long d;
    private List<Short> e;
    private Rect f;
    private int g;

    public jo(po poVar, boolean z, short[] sArr, long j, long j2, Rect rect, int i) {
        this.b = poVar;
        this.f2014a = z;
        this.d = j;
        this.c = j2;
        a(rect);
        this.g = i;
        if (sArr != null && sArr.length > 0) {
            this.e = new ArrayList(sArr.length);
            int length = sArr.length;
            int i2 = 0;
            while (i2 < length) {
                short s = sArr[i2];
                i2++;
                this.e.add(Short.valueOf(s));
            }
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m43a() {
        this.e = null;
    }

    public boolean c() {
        po poVar = this.b;
        return poVar == po.f2053a || poVar == po.h || poVar == po.f || poVar == po.g;
    }

    /* renamed from: a  reason: collision with other method in class */
    public long m39a() {
        return this.d;
    }

    public void a(Rect rect) {
        this.f = rect != null ? new Rect(rect) : null;
    }

    /* renamed from: a  reason: collision with other method in class */
    public List<Short> m41a() {
        return this.e;
    }

    /* renamed from: a  reason: collision with other method in class */
    public po m40a() {
        return this.b;
    }

    public void a(long j) {
        this.c = j;
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m42a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(md.a("\r"), this.b.ordinal());
        jSONObject.put(em.a("G"), this.f2014a ? 1 : 0);
        jSONObject.put(md.a(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE), this.d);
        jSONObject.put(em.a("P"), this.c);
        List<Short> list = this.e;
        if (list != null && !list.isEmpty()) {
            jSONObject.put(md.a("\u001e"), new JSONArray(this.e));
        }
        jSONObject.put(em.a("W"), b());
        return jSONObject;
    }

    public void b(long j) {
        this.d = j;
    }

    /* renamed from: b  reason: collision with other method in class */
    public boolean m44b() {
        po poVar = this.b;
        return poVar == po.e || poVar == po.d;
    }

    public long b() {
        return this.c;
    }

    public int a() {
        return this.g;
    }

    public void a(boolean z) {
        this.f2014a = z;
    }
}
