package com.appsee;

public interface AppseeListener {
    void a(AppseeScreenDetectedInfo appseeScreenDetectedInfo);

    void a(AppseeSessionEndedInfo appseeSessionEndedInfo);

    void a(AppseeSessionEndingInfo appseeSessionEndingInfo);

    void a(AppseeSessionStartedInfo appseeSessionStartedInfo);

    void a(AppseeSessionStartingInfo appseeSessionStartingInfo);
}
