package com.appsee;

import android.annotation.TargetApi;
import android.os.Build;
import android.text.TextUtils;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

class yb {

    /* renamed from: a  reason: collision with root package name */
    private static final Charset f2088a = Charset.forName(qb.a("6!%X["));

    yb() {
    }

    /* renamed from: a  reason: collision with other method in class */
    public static boolean m165a(String str) {
        return str == null || str.length() == 0 || str.trim().length() == 0;
    }

    public static boolean b(String str, String str2) {
        return a(str, false).equals(str2);
    }

    public static String a(Iterable iterable, String str) {
        if (iterable == null || !iterable.iterator().hasNext()) {
            return null;
        }
        return TextUtils.join(str, iterable);
    }

    public static String b(String str) {
        int indexOf;
        if (!a(str) && (indexOf = str.indexOf(sd.a("W"))) >= 1) {
            return str.substring(0, indexOf);
        }
        return null;
    }

    public static boolean a(String str, String str2) {
        if (str == null && str2 == null) {
            return true;
        }
        if (str == null || str2 == null) {
            return false;
        }
        return str.trim().toLowerCase().equals(str2.trim().toLowerCase());
    }

    public static String a(String str) throws Exception {
        return a(a(str));
    }

    @TargetApi(9)
    /* renamed from: a  reason: collision with other method in class */
    public static byte[] m166a(String str) {
        return Build.VERSION.SDK_INT >= 9 ? str.getBytes(f2088a) : str.getBytes();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x003b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0040  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.io.InputStream r5) throws java.io.IOException {
        /*
            r0 = 0
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ all -> 0x0035 }
            java.nio.charset.Charset r2 = f2088a     // Catch:{ all -> 0x0035 }
            r1.<init>(r5, r2)     // Catch:{ all -> 0x0035 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x0030 }
            r2.<init>(r1)     // Catch:{ all -> 0x0030 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x002e }
            r3 = 100
            r0.<init>(r3)     // Catch:{ all -> 0x002e }
        L_0x0014:
            java.lang.String r3 = r2.readLine()     // Catch:{ all -> 0x002e }
            if (r3 == 0) goto L_0x001e
            r0.append(r3)     // Catch:{ all -> 0x002e }
            goto L_0x0014
        L_0x001e:
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x002e }
            r2.close()
            r1.close()
            if (r5 == 0) goto L_0x002d
            r5.close()
        L_0x002d:
            return r0
        L_0x002e:
            r0 = move-exception
            goto L_0x0039
        L_0x0030:
            r2 = move-exception
            r4 = r2
            r2 = r0
            r0 = r4
            goto L_0x0039
        L_0x0035:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x0039:
            if (r2 == 0) goto L_0x003e
            r2.close()
        L_0x003e:
            if (r1 == 0) goto L_0x0043
            r1.close()
        L_0x0043:
            if (r5 == 0) goto L_0x0048
            r5.close()
        L_0x0048:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.yb.a(java.io.InputStream):java.lang.String");
    }

    public static String a(String str, boolean z) {
        if (a(str)) {
            return "";
        }
        int indexOf = z ? str.indexOf(qb.a("M")) : str.lastIndexOf(sd.a("W"));
        if (indexOf < 0) {
            return "";
        }
        return str.substring(indexOf + 1).toLowerCase();
    }

    public static String a(Object[] objArr, String str) {
        if (objArr == null || objArr.length == 0) {
            return null;
        }
        return TextUtils.join(str, objArr);
    }

    public static boolean a(CharSequence charSequence) {
        return charSequence == null || a(charSequence.toString());
    }

    public static String a(Date date) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(qb.a("\u0007\u0011L8.Z\u001a\f\u001a\fC=+O\u000e\u0018Y\u0006\u0010"), Locale.ENGLISH);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(sd.a(",\u0007:")));
        return simpleDateFormat.format(date);
    }

    public static String a(byte[] bArr) throws Exception {
        MessageDigest instance = MessageDigest.getInstance(qb.a(".1V"));
        instance.update(bArr);
        byte[] digest = instance.digest();
        StringBuilder sb = new StringBuilder(digest.length * 4);
        int i = 0;
        while (i < digest.length) {
            String hexString = Integer.toHexString(digest[i] & 255);
            while (hexString.length() < 2) {
                StringBuilder insert = new StringBuilder().insert(0, sd.a("I"));
                insert.append(hexString);
                hexString = insert.toString();
            }
            i++;
            sb.append(hexString);
        }
        return sb.toString();
    }

    public static String a(String str, int i) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        byte[] digest = MessageDigest.getInstance(sd.a("*\u001b8~KfO")).digest(a(str));
        StringBuilder sb = new StringBuilder(digest.length * 4);
        int min = Math.min(i / 2, digest.length);
        int i2 = 0;
        while (i2 < min) {
            String hexString = Integer.toHexString(digest[i2] & 255);
            if (hexString.length() == 1) {
                sb.append('0');
            }
            i2++;
            sb.append(hexString);
        }
        return sb.toString();
    }
}
