package com.appsee;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TabWidget;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

class rb {

    /* renamed from: a  reason: collision with root package name */
    private static final ArrayList<WeakReference<View>> f2058a = new ArrayList<>();
    private static final List<WeakReference<View>> b = Collections.unmodifiableList(f2058a);
    private static final Rect c = new Rect();
    private static h d = new gb();
    private static HashMap<sb, List<Object>> e = null;
    private static int f = 10000;
    private static final int[] g = new int[2];
    private static h h = new vb();
    private static List<Object> i;
    private static h j = new fb();
    private static final int[] k = new int[2];
    private static List<Object> l;
    private static List<Object> m;
    private static final ArrayList<WeakReference<View>> n = new ArrayList<>();
    private static List<Object> o;
    private static h p = new mb();

    static {
        try {
            i = new ArrayList();
            l = new ArrayList();
            m = new ArrayList();
            e = new HashMap<>();
            o = new ArrayList();
            l.add(AbsListView.class);
            l.add(ScrollView.class);
            i.add(AbsListView.class);
            i.add(SeekBar.class);
            i.add(Button.class);
            i.add(ImageButton.class);
            i.add(EditText.class);
            i.add(TabWidget.class);
            a();
            i.addAll(e.get(sb.h));
            i.addAll(e.get(sb.d));
            i.addAll(e.get(sb.i));
            i.addAll(e.get(sb.j));
            i.addAll(e.get(sb.g));
            i.addAll(e.get(sb.e));
            i.addAll(e.get(sb.f2062a));
            i.addAll(e.get(sb.f));
            m.add(Button.class);
            m.add(ImageButton.class);
            m.addAll(e.get(sb.h));
            m.addAll(e.get(sb.g));
            m.addAll(e.get(sb.e));
            m.addAll(e.get(sb.j));
            m.addAll(e.get(sb.f2062a));
            o.addAll(e.get(sb.h));
            o.addAll(e.get(sb.d));
        } catch (Exception e2) {
            qe.a(e2, hn.a("\u000bf<{<4\"{/p'z)4\u001b]\u000fz/x7n+f"));
        }
    }

    rb() {
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean b(View view) {
        if (view != null && Build.VERSION.SDK_INT >= 14) {
            try {
                Object a2 = tc.a((Object) view, (Class<?>) View.class, em.a("X3A\u0006T\u0011];[\u0014Z"));
                if (a2 != null) {
                    return Boolean.TRUE.equals(tc.a(a2, hn.a("#B'q9G-f!x\"W&u s+p")));
                }
            } catch (Exception e2) {
                qe.a(e2, em.a("p\u0000G\u001dGRQ\u0017A\u0017G\u001f\\\u001cPRF\u0011G\u001dY\u001e\\\u001cRRF\u0006T\u0006P"));
            }
        }
        return false;
    }

    static <T extends View> List<T> c(View view, List<Object> list) {
        ArrayList arrayList = new ArrayList();
        a(view, list, arrayList);
        return arrayList;
    }

    /* renamed from: e  reason: collision with other method in class */
    static MenuItem m124e(View view) throws Exception {
        if (view == null) {
            return null;
        }
        return (MenuItem) tc.a((Object) view, hn.a("s+`\u0007`+y\nu:u"), (Class<?>[]) null, new Object[0]);
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x003e A[Catch:{ Exception -> 0x00be }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static /* synthetic */ boolean f(android.view.View r9) {
        /*
            java.lang.String r0 = "\u001b]\u000fz/x7n+ft4\r{;x*zi`ns+`n3#Q6}:} s\u001c}>d\"q=3n} 4<q(x+w:}!z"
            java.lang.String r1 = "A\u0007U u\"m4q<.nW!a\"p 3:4)q:4iy\u000bl'`'z)F'd>x+g\r{;z:3n} 4<q(x+w:}!z"
            r2 = 0
            if (r9 != 0) goto L_0x0008
            return r2
        L_0x0008:
            int r3 = android.os.Build.VERSION.SDK_INT
            r4 = 21
            if (r3 >= r4) goto L_0x000f
            return r2
        L_0x000f:
            android.graphics.drawable.Drawable r9 = r9.getBackground()
            if (r9 == 0) goto L_0x00be
            boolean r3 = r9 instanceof android.graphics.drawable.RippleDrawable
            if (r3 == 0) goto L_0x00be
            java.lang.String r3 = "X \\\u0002E\u001eP"
            java.lang.String r3 = com.appsee.em.a((java.lang.String) r3)     // Catch:{ Exception -> 0x00be }
            java.lang.Object r3 = com.appsee.tc.a((java.lang.Object) r9, (java.lang.String) r3)     // Catch:{ Exception -> 0x00be }
            r4 = 1
            if (r3 != 0) goto L_0x003b
            java.lang.Boolean r3 = java.lang.Boolean.TRUE     // Catch:{ Exception -> 0x00be }
            java.lang.String r5 = "y\fu-)f!a p\u000fw:}8q"
            java.lang.String r5 = com.appsee.hn.a((java.lang.String) r5)     // Catch:{ Exception -> 0x00be }
            java.lang.Object r5 = com.appsee.tc.a((java.lang.Object) r9, (java.lang.String) r5)     // Catch:{ Exception -> 0x00be }
            boolean r3 = r3.equals(r5)     // Catch:{ Exception -> 0x00be }
            if (r3 == 0) goto L_0x0039
            goto L_0x003b
        L_0x0039:
            r3 = 0
            goto L_0x003c
        L_0x003b:
            r3 = 1
        L_0x003c:
            if (r3 != 0) goto L_0x00bd
            java.lang.String r5 = "\u001fp\n\\\u0006\\\u001cR \\\u0002E\u001eP\u0001v\u001d@\u001cA"
            java.lang.String r5 = com.appsee.em.a((java.lang.String) r5)     // Catch:{ Exception -> 0x00be }
            java.lang.Object r5 = com.appsee.tc.a((java.lang.Object) r9, (java.lang.String) r5)     // Catch:{ Exception -> 0x00be }
            java.lang.Integer r5 = (java.lang.Integer) r5     // Catch:{ Exception -> 0x00be }
            r6 = 2
            if (r5 == 0) goto L_0x00a7
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00be }
            r7 = 23
            if (r1 >= r7) goto L_0x005b
            int r9 = r5.intValue()     // Catch:{ Exception -> 0x00be }
            if (r9 <= 0) goto L_0x005a
            r2 = 1
        L_0x005a:
            return r2
        L_0x005b:
            int r1 = r5.intValue()     // Catch:{ Exception -> 0x00be }
            if (r1 <= 0) goto L_0x00bd
            java.lang.String r1 = "X7M\u001bA\u001b[\u0015g\u001bE\u0002Y\u0017F"
            java.lang.String r1 = com.appsee.em.a((java.lang.String) r1)     // Catch:{ Exception -> 0x00be }
            java.lang.Object r9 = com.appsee.tc.a((java.lang.Object) r9, (java.lang.String) r1)     // Catch:{ Exception -> 0x00be }
            java.lang.Object[] r9 = (java.lang.Object[]) r9     // Catch:{ Exception -> 0x00be }
            if (r9 == 0) goto L_0x0091
            r0 = 0
        L_0x0070:
            int r1 = r5.intValue()     // Catch:{ Exception -> 0x00be }
            if (r0 >= r1) goto L_0x00bd
            r1 = r9[r0]     // Catch:{ Exception -> 0x00be }
            java.lang.String r6 = "]\u0013F4\\\u001c\\\u0001]\u0017Q7M\u001bA"
            java.lang.String r6 = com.appsee.em.a((java.lang.String) r6)     // Catch:{ Exception -> 0x00be }
            r7 = 0
            java.lang.Object[] r8 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x00be }
            java.lang.Object r1 = com.appsee.tc.a((java.lang.Object) r1, (java.lang.String) r6, (java.lang.Class<?>[]) r7, (java.lang.Object[]) r8)     // Catch:{ Exception -> 0x00be }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ Exception -> 0x00be }
            boolean r1 = r1.booleanValue()     // Catch:{ Exception -> 0x00be }
            if (r1 != 0) goto L_0x008e
            return r4
        L_0x008e:
            int r0 = r0 + 1
            goto L_0x0070
        L_0x0091:
            com.appsee.hn.a((java.lang.String) r0)     // Catch:{ Exception -> 0x00be }
            java.lang.String r9 = "'|3[\u0013Y\u000bO\u0017GH\u00151Z\u0007Y\u0016[UARR\u0017AR\u0012\u001fp\n\\\u0006\\\u001cR \\\u0002E\u001eP\u0001\u0012R\\\u001c\u0015\u0000P\u0014Y\u0017V\u0006\\\u001d["
            java.lang.String r9 = com.appsee.em.a((java.lang.String) r9)     // Catch:{ Exception -> 0x00be }
            com.appsee.gd.b(r6, r9)     // Catch:{ Exception -> 0x00be }
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x00be }
            java.lang.String r0 = com.appsee.hn.a((java.lang.String) r0)     // Catch:{ Exception -> 0x00be }
            r9.<init>(r0)     // Catch:{ Exception -> 0x00be }
            throw r9     // Catch:{ Exception -> 0x00be }
        L_0x00a7:
            com.appsee.hn.a((java.lang.String) r1)     // Catch:{ Exception -> 0x00be }
            java.lang.String r9 = "`;t\u001cT\u001eL\bP\u0000\u000fRv\u001d@\u001eQ\u001c\u0012\u0006\u0015\u0015P\u0006\u0015UX7M\u001bA\u001b[\u0015g\u001bE\u0002Y\u0017F1Z\u0007[\u0006\u0012R\\\u001c\u0015\u0000P\u0014Y\u0017V\u0006\\\u001d["
            java.lang.String r9 = com.appsee.em.a((java.lang.String) r9)     // Catch:{ Exception -> 0x00be }
            com.appsee.gd.b(r6, r9)     // Catch:{ Exception -> 0x00be }
            java.lang.IllegalStateException r9 = new java.lang.IllegalStateException     // Catch:{ Exception -> 0x00be }
            java.lang.String r0 = com.appsee.hn.a((java.lang.String) r1)     // Catch:{ Exception -> 0x00be }
            r9.<init>(r0)     // Catch:{ Exception -> 0x00be }
            throw r9     // Catch:{ Exception -> 0x00be }
        L_0x00bd:
            return r3
        L_0x00be:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.rb.f(android.view.View):boolean");
    }

    static boolean g(View view) {
        Rect e2 = e(view);
        if (view.getWidth() < 2 || view.getHeight() < 2) {
            return false;
        }
        if (b(view, e2.left + 1, e2.top + 1) || b(view, e2.right - 1, e2.top + 1) || b(view, e2.left + 1, e2.bottom - 1) || b(view, e2.right - 1, e2.bottom - 1)) {
            return true;
        }
        return false;
    }

    private static /* synthetic */ Rect h(View view) {
        Rect rect = new Rect();
        if (view == null) {
            return rect;
        }
        view.getGlobalVisibleRect(rect);
        view.getLocationInWindow(k);
        int[] iArr = k;
        rect.set(iArr[0], iArr[1], iArr[0] + rect.width(), k[1] + rect.height());
        return rect;
    }

    /* renamed from: i  reason: collision with other method in class */
    static boolean m131i(View view) {
        return a(o, view);
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ boolean k(View view) {
        if (view != null && a(sb.f, view)) {
            try {
                if (((Integer) tc.a((Object) view, hn.a(")q:G-f!x\"G:u:q"), 0, new Object[0])).intValue() != 0) {
                    return true;
                }
            } catch (Exception unused) {
            }
        }
        return false;
    }

    static <T extends View> T d(List<T> list) {
        if (list == null || list.isEmpty()) {
            return null;
        }
        for (T t : list) {
            if (t.isShown() && t.getWidth() > 0 && t.getHeight() > 0) {
                return t;
            }
        }
        return null;
    }

    /* renamed from: e  reason: collision with other method in class */
    static View m125e(View view) {
        if (view == null) {
            return null;
        }
        if (view.isShown() && view.isEnabled() && view.isClickable()) {
            return view;
        }
        ViewParent parent = view.getParent();
        if (parent == null || !(parent instanceof View)) {
            return null;
        }
        return e((View) parent);
    }

    static View i(View view) {
        return a(view, l, (View) null);
    }

    static View l(View view) {
        if (view == null) {
            return null;
        }
        return a(view, i);
    }

    static boolean c(AbsListView absListView) {
        return !b(absListView) || !a(absListView);
    }

    /* renamed from: i  reason: collision with other method in class */
    static String m130i(View view) {
        TextView textView;
        if (view == null || !(view instanceof LinearLayout) || (textView = (TextView) view.findViewById(16908299)) == null || yb.a(textView.getText())) {
            return null;
        }
        return textView.getText().toString();
    }

    /* renamed from: l  reason: collision with other method in class */
    static String m132l(View view) {
        List<TextView> a2 = a(view, (Class<?>) TextView.class);
        if (a2.isEmpty()) {
            return null;
        }
        for (TextView textView : a2) {
            if (!yb.a(textView.getText()) && textView.isShown() && textView.getWidth() > 0 && textView.getHeight() > 0) {
                return textView.getText().toString();
            }
        }
        return null;
    }

    static View b(List<View> list) {
        return a(list, p);
    }

    static boolean c(View view, Class<?> cls) {
        if (view == null) {
            return false;
        }
        if (cls.isInstance(view)) {
            return true;
        }
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                if (c(viewGroup.getChildAt(i2), cls)) {
                    return true;
                }
            }
        }
        return false;
    }

    private static /* synthetic */ boolean b(AbsListView absListView) {
        if (absListView == null || absListView.getCount() == 0) {
            return true;
        }
        return absListView.getFirstVisiblePosition() == 0 && absListView.getChildAt(0).getTop() >= absListView.getTop();
    }

    static boolean b(View view, Class<?> cls) {
        return b(view, a((Class<?>[]) new Class[]{cls}));
    }

    static Rect e(View view) {
        Rect rect = new Rect();
        if (view == null) {
            return rect;
        }
        view.getGlobalVisibleRect(rect);
        view.getRootView().getLocationOnScreen(g);
        int[] iArr = g;
        rect.offset(iArr[0], iArr[1]);
        return rect;
    }

    static boolean b(View view, List<Object> list) {
        if (view == null) {
            return false;
        }
        for (T t : c(view.getRootView(), list)) {
            if (!view.equals(t) && t != null && view.getId() == t.getId()) {
                return false;
            }
        }
        return true;
    }

    static View c(List<View> list) {
        return a(list, h);
    }

    static String c(View view) {
        String str;
        String str2 = "";
        if (view == null) {
            return str2;
        }
        try {
            if (view instanceof TextView) {
                str = String.format(hn.a("l1=6"), new Object[]{((TextView) view).getText().toString()}).replace(em.a("?"), hn.a("\u0012z")).replace(em.a("8"), hn.a("\u0012f"));
            } else {
                str = null;
            }
            Locale locale = Locale.US;
            String a2 = em.a("\u0010\u0001\u0015Z\u0010\u0016\u0019WQ[\u0015WFH\u0010\u0010\u000fWWH\u0010\u0010\u000fWWWF");
            Object[] objArr = new Object[9];
            objArr[0] = view.getClass().getName();
            objArr[1] = Integer.valueOf(view.getId());
            objArr[2] = Integer.valueOf(view.hashCode());
            objArr[3] = a(e(view));
            objArr[4] = Boolean.valueOf(view.isShown());
            objArr[5] = Boolean.valueOf(view.isClickable());
            objArr[6] = Boolean.valueOf(view.isEnabled());
            objArr[7] = Boolean.valueOf(e(view));
            if (str != null) {
                StringBuilder insert = new StringBuilder().insert(0, hn.a("."));
                insert.append(str);
                str2 = insert.toString();
            }
            objArr[8] = str2;
            return String.format(locale, a2, objArr);
        } catch (Exception unused) {
            return null;
        }
    }

    /* renamed from: e  reason: collision with other method in class */
    static List<View> m129e(View view) {
        return c(view, o);
    }

    @TargetApi(11)
    private static /* synthetic */ boolean b(View view, int i2, int i3) {
        View a2 = a(view.getRootView(), i2, i3);
        return (Build.VERSION.SDK_INT >= 11 && a2 != null && a2.getAlpha() < 1.0f) || a2 == view;
    }

    /* renamed from: e  reason: collision with other method in class */
    static Object m127e(View view) throws Exception {
        if (Build.VERSION.SDK_INT < 24 || !view.getClass().getName().equals("com.android.internal.policy.DecorView")) {
            return tc.a((Object) view, hn.a(":|'gj$"));
        }
        return tc.a((Object) view, em.a("X%\\\u001cQ\u001dB"));
    }

    static List<WeakReference<View>> b(Object[] objArr) {
        if (objArr == null || objArr.length == 0) {
            return null;
        }
        f2058a.clear();
        n.clear();
        HashMap<Integer, List<View>> a2 = a(objArr);
        Integer[] numArr = (Integer[]) a2.keySet().toArray(new Integer[0]);
        Arrays.sort(numArr);
        for (Integer num : numArr) {
            for (View view : a2.get(num)) {
                if (view.hasWindowFocus() || num.equals(Integer.valueOf(f))) {
                    n.add(new WeakReference(view));
                } else {
                    f2058a.add(new WeakReference(view));
                }
            }
        }
        f2058a.addAll(n);
        return b;
    }

    static View e(List<View> list) {
        return a(list, d);
    }

    /* renamed from: e  reason: collision with other method in class */
    static ImageView m126e(View view) throws Exception {
        List<ImageView> a2 = a(view, (Class<?>) ImageView.class);
        if (a2.isEmpty()) {
            return null;
        }
        for (ImageView imageView : a2) {
            if (imageView.isShown() && imageView.getWidth() > 0 && imageView.getHeight() > 0) {
                return imageView;
            }
        }
        return null;
    }

    static boolean a(sb sbVar, View view) {
        return a(e.get(sbVar), view);
    }

    /* renamed from: c  reason: collision with other method in class */
    static boolean m123c(View view) {
        return a(m, view);
    }

    private static /* synthetic */ View a(List<View> list, h hVar) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            View a2 = a(list.get(i2), hVar);
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    /* renamed from: e  reason: collision with other method in class */
    static String m128e(View view) {
        return a(view, true, m);
    }

    /* renamed from: a  reason: collision with other method in class */
    static View m117a(View view, Class<?> cls) {
        ArrayList arrayList = new ArrayList(1);
        arrayList.add(cls);
        return a(view, (List<Object>) arrayList);
    }

    private static /* synthetic */ Class<?> a(String str) {
        try {
            return Class.forName(str);
        } catch (ClassNotFoundException unused) {
            gd.b(1, em.a("1Y\u0013F\u0001\u0015\u0011T\u001c[\u001dARW\u0017\u0015\u001eZ\u0013Q\u0017QRB\u001bA\u001a\\\u001c\u0015'|3[\u0013Y\u000bO\u0017GH\u0015").concat(str));
            return db.class;
        }
    }

    private static /* synthetic */ boolean a(List<Object> list, View view) {
        int size = list.size();
        for (int i2 = 0; i2 < size; i2++) {
            Object obj = list.get(i2);
            if (obj instanceof Class) {
                if (((Class) obj).isInstance(view)) {
                    return true;
                }
            } else if ((obj instanceof String) && obj.toString().equalsIgnoreCase(view.getClass().getName())) {
                return true;
            }
        }
        return false;
    }

    private static /* synthetic */ View a(View view, h hVar) {
        if (view != null && view.isShown()) {
            if (hVar.a(view)) {
                return view;
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                    View a2 = a(viewGroup.getChildAt(i2), hVar);
                    if (a2 != null) {
                        return a2;
                    }
                }
            }
        }
        return null;
    }

    private static /* synthetic */ boolean a(View view, View view2) {
        while (view != null) {
            if (!view.equals(view2)) {
                ViewParent parent = view.getParent();
                if (parent == null || !(parent instanceof View)) {
                    break;
                }
                view = (View) parent;
            } else {
                return true;
            }
        }
        return false;
    }

    /* renamed from: a  reason: collision with other method in class */
    static <T extends View> List<T> m120a(View view, Class<?> cls) {
        ArrayList arrayList = new ArrayList(1);
        ArrayList arrayList2 = new ArrayList();
        arrayList.add(cls);
        a(view, (List<Object>) arrayList, arrayList2);
        return arrayList2;
    }

    static Rect a(Rect rect) {
        if (rect == null || rect.width() < 1 || rect.height() < 1) {
            return null;
        }
        Dimension a2 = di.b().a(true);
        zn a3 = di.b().a();
        if (a3 == zn.h) {
            return rect;
        }
        if (a3 == zn.f) {
            return new Rect(a2.c() - rect.right, a2.a() - rect.bottom, a2.c() - rect.left, a2.a() - rect.top);
        }
        if (a3 == zn.e) {
            return new Rect(a2.c() - rect.bottom, rect.left, a2.c() - rect.top, rect.right);
        }
        return a3 == zn.f2095a ? new Rect(rect.top, a2.a() - rect.right, rect.bottom, a2.a() - rect.left) : rect;
    }

    static View a(MotionEvent motionEvent, View view) {
        Rect e2 = e(view);
        return a(view, (int) (short) (((short) ((int) motionEvent.getX(motionEvent.getActionIndex()))) + e2.left), (int) (short) (((short) ((int) motionEvent.getY(motionEvent.getActionIndex()))) + e2.top));
    }

    private static /* synthetic */ <T> void a(View view, List<Object> list, List<T> list2) {
        if (view != null) {
            if (!list2.contains(view) && a(list, view)) {
                list2.add(view);
            }
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                int i2 = 0;
                while (i2 < viewGroup.getChildCount()) {
                    View childAt = viewGroup.getChildAt(i2);
                    i2++;
                    a(childAt, list, list2);
                }
            }
        }
    }

    private static /* synthetic */ boolean a(AbsListView absListView) {
        if (absListView == null || absListView.getCount() == 0 || (absListView.getLastVisiblePosition() == absListView.getCount() - 1 && absListView.getChildAt(absListView.getChildCount() - 1).getBottom() <= absListView.getBottom())) {
            return true;
        }
        return false;
    }

    static int a(View view, Class<?> cls) {
        List<View> a2 = a(view, cls);
        int i2 = 0;
        if (a2 != null && !a2.isEmpty()) {
            for (View isShown : a2) {
                if (isShown.isShown()) {
                    i2++;
                }
            }
        }
        return i2;
    }

    static View a(List<View> list) {
        return a(list, j);
    }

    static Point a(Point point) {
        Dimension a2 = di.b().a(true);
        zn a3 = di.b().a();
        if (a3 == zn.h) {
            return point;
        }
        if (a3 == zn.f) {
            return new Point(a2.c() - point.x, a2.a() - point.y);
        }
        if (a3 == zn.e) {
            return new Point(a2.c() - point.y, point.x);
        }
        return a3 == zn.f2095a ? new Point(point.y, a2.a() - point.x) : point;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r2 = r3.top;
        r3 = r3.bottom;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ boolean a(android.graphics.Rect r3, int r4, int r5) {
        /*
            int r0 = r3.left
            int r1 = r3.right
            if (r0 >= r1) goto L_0x0016
            int r2 = r3.top
            int r3 = r3.bottom
            if (r2 >= r3) goto L_0x0016
            if (r0 > r4) goto L_0x0016
            if (r4 > r1) goto L_0x0016
            if (r2 > r5) goto L_0x0016
            if (r5 > r3) goto L_0x0016
            r3 = 1
            return r3
        L_0x0016:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.rb.a(android.graphics.Rect, int, int):boolean");
    }

    private static /* synthetic */ String a(View view, String str, boolean z) {
        if (view == null) {
            return "";
        }
        StringBuilder insert = new StringBuilder().insert(0, str);
        insert.append(c(view));
        StringBuilder sb = new StringBuilder(insert.toString());
        if (z) {
            Log.d("appsee", sb.toString());
        }
        sb.append(em.a("?"));
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int i2 = 0;
            while (i2 < viewGroup.getChildCount()) {
                View childAt = viewGroup.getChildAt(i2);
                StringBuilder insert2 = new StringBuilder().insert(0, str);
                insert2.append(hn.a("\u001d"));
                i2++;
                sb.append(a(childAt, insert2.toString(), z));
            }
        }
        return sb.toString();
    }

    static void a(View view, boolean z, boolean z2) {
        String a2 = a(view, "", z2);
        if (z) {
            lg.c(a2);
        }
    }

    private static /* synthetic */ View a(View view, List<Object> list, View view2) {
        if (view == null) {
            return view2;
        }
        if (a(list, view)) {
            view2 = view;
        }
        ViewParent parent = view.getParent();
        return (parent == null || !(parent instanceof View)) ? view2 : a((View) parent, list, view2);
    }

    private static /* synthetic */ View a(View view, List<Object> list) {
        if (view == null) {
            return null;
        }
        if (a(list, view)) {
            return view;
        }
        ViewParent parent = view.getParent();
        if (parent == null || !(parent instanceof View)) {
            return null;
        }
        return a((View) parent, list);
    }

    private static /* synthetic */ View a(View view, int i2, int i3) {
        View a2;
        if (pg.h().h() || (a2 = a(view, i2, i3, false)) == null) {
            return a(view, i2, i3, true);
        }
        return a(a2, i2, i3, true);
    }

    static boolean a(Window.Callback callback) {
        if (callback != null) {
            return (callback instanceof AlertDialog) || callback.getClass().getName().equalsIgnoreCase(hn.a("/z*f!}*:=a>d!f::8#`u>d`U\"q<`\n}/x!s"));
        }
        return false;
    }

    private static /* synthetic */ View a(View view, int i2, int i3, boolean z) {
        if (view != null && view.isShown() && a(e(view), i2, i3)) {
            if (view instanceof ViewGroup) {
                ViewGroup viewGroup = (ViewGroup) view;
                for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                    View a2 = a(viewGroup.getChildAt(childCount), i2, i3, z);
                    if (a2 != null) {
                        return a2;
                    }
                }
            }
            if (z || (view.isEnabled() && view.isClickable())) {
                return view;
            }
        }
        return null;
    }

    static int a(AbsListView absListView, int i2) {
        try {
            return ((Integer) tc.a((Object) absListView, em.a("S\u001b[\u0016x\u001dA\u001bZ\u001cg\u001dB"), 1, Integer.valueOf(i2 - h(absListView).top))).intValue();
        } catch (Exception e2) {
            qe.a((Throwable) e2, hn.a("Q<f!fns+`:} snX'g:B'q94:{;w&q*4'`+yb4\u0002}=`\u001am>qt4kg"), absListView.getClass().getName());
            return -1;
        }
    }

    static String a(View view, boolean z, List<Object> list) {
        if (!z) {
            return String.valueOf(a(view, view.getRootView(), list));
        }
        ArrayList<Class> arrayList = new ArrayList<>(2);
        arrayList.addAll(e.get(sb.b));
        arrayList.addAll(e.get(sb.i));
        arrayList.addAll(l);
        ArrayList arrayList2 = new ArrayList(list);
        arrayList2.addAll(arrayList);
        String valueOf = String.valueOf(a(view, view.getRootView(), (List<Object>) arrayList2));
        AbsListView absListView = (AbsListView) a(view, (Class<?>) AbsListView.class);
        if (absListView != null) {
            gd.b(1, hn.a("@/'z)4\u0002}=`\u0018}+cn} z+fn} p+l"));
            View a2 = a((ViewGroup) absListView, view);
            if (a2 == null) {
                return valueOf;
            }
            int a3 = a(view, a2, list);
            return String.format(Locale.US, em.a("\u0010\u0001\u001bWQ"), new Object[]{valueOf, Integer.valueOf(a3)});
        }
        for (Class cls : arrayList) {
            View a4 = a(view, (Class<?>) cls);
            if (a4 != null) {
                gd.a(1, hn.a("\u001au%} sn} z+fn} p+lnr<{#4kg"), cls.getName());
                int a5 = a(view, a4, list);
                valueOf = String.format(Locale.US, em.a("\u0010\u0001\u001bWQ"), new Object[]{valueOf, Integer.valueOf(a5)});
            }
        }
        return valueOf;
    }

    static Bitmap a(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        if (!(drawable instanceof BitmapDrawable)) {
            drawable = drawable.getCurrent();
        }
        if (drawable == null) {
            return null;
        }
        Rect bounds = drawable.getBounds();
        boolean z = drawable instanceof BitmapDrawable;
        if (!z && bounds.width() > 0 && bounds.height() > 0) {
            int width = bounds.width();
            int height = bounds.height();
            Bitmap createBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(createBitmap);
            if (!(width == bounds.width() && height == bounds.height())) {
                canvas.scale(((float) width) / ((float) bounds.width()), ((float) height) / ((float) bounds.height()));
            }
            drawable.draw(canvas);
            return createBitmap;
        } else if (z) {
            return ((BitmapDrawable) drawable).getBitmap();
        } else {
            return null;
        }
    }

    private static /* synthetic */ int a(View view, View view2, List<Object> list) {
        List<T> c2 = c(view2, list);
        Collections.sort(c2, new jb());
        for (int i2 = 0; i2 < c2.size(); i2++) {
            View view3 = (View) c2.get(i2);
            if (view == view3 || a(view, view3)) {
                return i2 + 1;
            }
        }
        return -1;
    }

    /* renamed from: a  reason: collision with other method in class */
    static boolean m121a(Drawable drawable) {
        return drawable != null && drawable.isVisible() && drawable.getBounds().width() > 0 && drawable.getBounds().height() > 0;
    }

    static int a(ViewGroup viewGroup, View view) {
        if (!(viewGroup == null || view == null)) {
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                if (viewGroup.getChildAt(i2) == view) {
                    return i2;
                }
            }
        }
        return -1;
    }

    static List<Object> a(Class<?>... clsArr) {
        ArrayList arrayList = new ArrayList(clsArr.length);
        for (Class<?> cls : clsArr) {
            if (!arrayList.contains(cls)) {
                arrayList.add(cls);
            }
        }
        return arrayList;
    }

    /* renamed from: a  reason: collision with other method in class */
    static List<Object> m119a() {
        return Collections.unmodifiableList(m);
    }

    /* renamed from: a  reason: collision with other method in class */
    static View m118a(ViewGroup viewGroup, View view) {
        if (!(viewGroup == null || view == null)) {
            for (int i2 = 0; i2 < viewGroup.getChildCount(); i2++) {
                View childAt = viewGroup.getChildAt(i2);
                if (a(view, childAt)) {
                    return childAt;
                }
            }
        }
        return null;
    }

    static Rect a() {
        return a(di.b().a(true));
    }

    /* renamed from: a  reason: collision with other method in class */
    static boolean m122a(View view, Class<?> cls) {
        List<View> a2 = a(view, cls);
        if (a2 != null && !a2.isEmpty()) {
            for (View view2 : a2) {
                if (view2.isShown() && view2.getWidth() > 0 && view2.getHeight() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    private static /* synthetic */ HashMap<Integer, List<View>> a(Object[] objArr) {
        HashMap<Integer, List<View>> hashMap = new HashMap<>();
        for (View view : objArr) {
            if (view.isShown()) {
                r a2 = ub.a().a(view);
                int i2 = f;
                if (!(a2 == null || a2.a() == null)) {
                    i2 = a2.a().getAttributes().type;
                }
                if (!hashMap.containsKey(Integer.valueOf(i2))) {
                    hashMap.put(Integer.valueOf(i2), new ArrayList());
                }
                hashMap.get(Integer.valueOf(i2)).add(view);
            }
        }
        return hashMap;
    }

    private static /* synthetic */ Rect a(Dimension[] dimensionArr) {
        Dimension dimension = dimensionArr[0];
        Dimension dimension2 = dimensionArr[1];
        int i2 = xb.f2083a[di.b().a().ordinal()];
        if (i2 == 1) {
            return new Rect(0, 0, dimension.c(), dimension.a());
        }
        if (i2 == 2) {
            return new Rect(dimension2.c() - dimension.c(), dimension2.a() - dimension.a(), dimension2.c(), dimension2.a());
        }
        if (i2 != 3) {
            if (i2 != 4) {
                return new Rect(0, 0, dimension.c(), dimension.a());
            }
            if (di.g()) {
                return new Rect(0, 0, dimension.c(), dimension.a());
            }
            return new Rect(0, dimension2.a() - dimension.a(), dimension2.c(), dimension2.a());
        } else if (di.g()) {
            return new Rect(dimension2.c() - dimension.c(), dimension2.a() - dimension.a(), dimension2.c(), dimension2.a());
        } else {
            return new Rect(0, 0, dimension.c(), dimension.a());
        }
    }

    static boolean a(WebView webView) {
        if (webView != null && webView.isShown()) {
            webView.getRootView().getWindowVisibleDisplayFrame(c);
            if (((double) (webView.getWidth() * webView.getHeight())) >= ((double) (c.width() * c.height())) * pg.h().m()) {
                return true;
            }
        }
        return false;
    }

    @TargetApi(11)
    static Rect a(r rVar) throws Exception {
        Rect rect;
        Window a2 = rVar.a();
        View a3 = rVar.a();
        boolean z = true;
        boolean z2 = (a2 == null || a2.getAttributes() == null || (a2.getAttributes().flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0) ? false : true;
        if (!ub.a().c()) {
            if (Build.VERSION.SDK_INT < 16 || a3 == null || (a3.getSystemUiVisibility() & 2) == 0) {
                z = false;
            }
            if (!z2 && !z) {
                return a(di.b().a());
            }
            Dimension a4 = di.b().a();
            return new Rect(0, 0, a4.c(), a4.a());
        }
        if (!z2) {
            a3.getWindowVisibleDisplayFrame(c);
            rect = c;
        } else {
            rect = e(a3);
        }
        return a(rect);
    }
}
