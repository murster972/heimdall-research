package com.appsee;

import android.app.Application;
import android.os.Build;

class ho {

    /* renamed from: a  reason: collision with root package name */
    private static Application f2005a;

    ho() {
    }

    public static Application a() {
        Application application = f2005a;
        if (application != null) {
            return application;
        }
        throw new NullPointerException("APPSEE_NO_CONTEXT");
    }

    /* renamed from: a  reason: collision with other method in class */
    public static synchronized boolean m33a() {
        synchronized (ho.class) {
            if (f2005a == null) {
                try {
                    Class<?> cls = Class.forName(rc.a("vossxhs/vqg/VbchahcxCiedve"));
                    if (Build.VERSION.SDK_INT >= 9) {
                        f2005a = (Application) cls.getMethod(rc.a("ttesroc@gq{ht`chxo"), new Class[0]).invoke((Object) null, new Object[0]);
                    } else {
                        f2005a = (Application) cls.getMethod(rc.a("pdc@gq{ht`chxo"), new Class[0]).invoke(cls.getMethod(rc.a("bbsedyuVbchahcxCiedve"), new Class[0]).invoke((Object) null, new Object[0]), new Object[0]);
                    }
                } catch (Exception unused) {
                    return false;
                }
            }
            if (f2005a != null) {
                return true;
            }
            return false;
        }
    }
}
