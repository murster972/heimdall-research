package com.appsee;

import java.util.HashMap;

class ab {
    private static ab b;

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, ob> f1971a = new HashMap<>();

    private /* synthetic */ ab() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m0a() {
        synchronized (this.f1971a) {
            this.f1971a.putAll(pc.a("Appsee_3p"));
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        synchronized (this.f1971a) {
            this.f1971a.clear();
            a();
        }
    }

    public static synchronized ab a() {
        ab abVar;
        synchronized (ab.class) {
            if (b == null) {
                b = new ab();
            }
            abVar = b;
        }
        return abVar;
    }

    static ob a(String str) {
        ob obVar = new ob();
        String substring = str.substring(10);
        int lastIndexOf = substring.lastIndexOf(hn.a("K"));
        boolean z = false;
        obVar.a(substring.substring(0, lastIndexOf));
        String substring2 = substring.substring(lastIndexOf + 1);
        obVar.a(substring2.charAt(1) == '1');
        if (substring2.charAt(3) == '1') {
            z = true;
        }
        obVar.b(z);
        return obVar;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, ob> a(boolean z) {
        HashMap<String, ob> hashMap = this.f1971a;
        if (!z) {
            return hashMap;
        }
        synchronized (hashMap) {
            this.f1971a = new HashMap<>();
            a();
        }
        return hashMap;
    }
}
