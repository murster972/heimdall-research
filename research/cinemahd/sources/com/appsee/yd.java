package com.appsee;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.jcodec.AVCMP4Mux;

class yd implements j {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2090a;
    private int b;
    private int c;
    private jm d = oc.a().a(zo.a("u\bO\u0000M\f~\u0007X\u0006_\f"));
    private int e;
    private boolean f;
    private boolean g;
    private String h;
    private List<Long> i = new ArrayList();
    private int j;
    private int k = 0;
    private String l;

    private /* synthetic */ void b() throws Exception {
        File a2 = lg.a(this.l);
        new AVCMP4Mux().a(a2, this.h, this.i);
        lg.b(a2);
    }

    public void a(int i2, int i3, int i4, int i5, String str, boolean z) throws Exception {
        this.e = i2;
        this.b = i3;
        this.c = i4;
        this.j = i5;
        this.h = str;
        this.l = String.format(wd.e("r2yd$"), new Object[]{pg.h().m(), "h264"});
        this.f = z;
        this.i.clear();
        this.k = 0;
        gd.b(1, zo.a(" U\u0000O\u0000Z\u0005R\u0013^IU\bO\u0000M\f\u001b\u001fR\r^\u0006\u001b\fU\nT\r^\u001b"));
        List h2 = pg.h().h();
        String[] strArr = new String[h2.size()];
        int[] iArr = new int[h2.size()];
        for (int i6 = 0; i6 < h2.size(); i6++) {
            strArr[i6] = ((i) h2.get(i6)).b;
            iArr[i6] = ((i) h2.get(i6)).f2006a.intValue();
        }
        this.f2090a = AppseeNativeExtensions.a(lg.a(this.l).getAbsolutePath(), this.e, this.b, this.j, this.c, strArr, iArr, pg.h().Q(), gd.a() <= 1, this.f);
        if (!this.f2090a) {
            throw new Exception(wd.e("\u00026/9.#a432 #$w/65>72a!(3$8a2/4.3$%"));
        }
    }

    public void a(ch chVar, long j2) throws Exception {
        if (this.f2090a) {
            this.d.b();
            if (this.f) {
                gd.b(1, wd.e("\u0004$9%>/0a136,2a1.%a9 #(!$w$9\"8%>/0"));
            }
            this.k++;
            this.i.add(Long.valueOf(j2));
            AppseeNativeExtensions.a(this.e, this.b, chVar.b(), j2, this.f);
            this.d.d();
            return;
        }
        throw new Exception(zo.a("'Z\u001dR\u001f^I^\u0007X\u0006_\fIIR\u001a\u001b\u0007T\u001d\u001b\u0000U\u0000O\u0000Z\u001d^\r"));
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m167a() {
        return this.g;
    }

    /* JADX INFO: finally extract failed */
    public void a() throws Exception {
        this.d.c();
        if (this.f2090a) {
            gd.a(1, wd.e("\u0007>/>2?a2/4.3(9&{a2/4.3$3a136,2a4.\"/#{wd3"), Integer.valueOf(this.k));
            boolean a2 = AppseeNativeExtensions.a();
            if (!a2) {
                gd.b(1, zo.a("u\bO\u0000M\f\u001b\fU\nT\rR\u0007\\I]\u0000U\u0000H\u0001^\r\u001b\u001eR\u001dSI^\u001bI\u0006I\u001a"));
            }
            gd.b(1, wd.e("\f\"9>/0a!(3$8"));
            try {
                this.g = true;
                jm a3 = oc.a().a(zo.a("v\u001cC\u0000U\u000e"));
                a3.b();
                b();
                a3.d();
                a3.c();
                this.g = false;
                if (!a2) {
                    throw new Exception(wd.e("\u0004%383w'>/>2?(9&w$9\"8%>/0a!(3$8"));
                }
            } catch (Throwable th) {
                this.g = false;
                throw th;
            }
        } else {
            throw new Exception(zo.a("~\u0007X\u0006_\fIIR\u001a\u001b\u0007T\u001d\u001b\u0000U\u0000O\u0000Z\u001d^\r"));
        }
    }
}
