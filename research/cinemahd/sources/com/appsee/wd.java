package com.appsee;

import android.content.pm.PackageManager;
import android.os.Build;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class wd {
    private static wd e;

    /* renamed from: a  reason: collision with root package name */
    private final List<File> f2078a = new ArrayList();
    private boolean b = false;
    private boolean c = false;
    private final vc d;

    private /* synthetic */ wd() {
        this.d = Build.VERSION.SDK_INT >= 23 ? new ic() : new vc();
    }

    private static /* synthetic */ void a(JSONObject jSONObject, Exception exc) {
        if (exc != null) {
            qe.a((Throwable) exc, String.format(oi.a("5H(Hg]\"N3S)]gY(T!S O5[3S(T}\u001abI"), new Object[]{exc.getMessage()}), false);
        }
        rd.a().d();
        try {
            pc.b(new JSONObject(jSONObject, new String[]{"Connectivity", "StartTime", "RequestId", "AppUserId"}));
        } catch (JSONException unused) {
            gd.b(2, qb.a(" \u001a\u0016\u0019\u0007\u001bD\u0001C\u0006\u0002\u0003\u0006U\u0017\u001d\u0006U\u0002\u0005\u0013U\u000f\u0014\u0016\u001b\u0000\u001dC\u0011\u0002\u0001\u0002YC\u0006\u0006\u0006\u0010\u001c\f\u001bC\u0002\n\u0019\u000fU\u000b\u0014\u0015\u0010C\u001b\fU\u0017\u0007\u0002\u0016\u0006"));
        }
    }

    private /* synthetic */ void b(boolean z) {
        while (true) {
            synchronized (this.f2078a) {
                if (this.f2078a.isEmpty()) {
                    break;
                }
                File remove = this.f2078a.remove(0);
                if (!remove.exists()) {
                    gd.a(1, qb.a("0\u001e\n\u0005\u0013\u001c\r\u0012C\u0011\u0006\u0019\u0006\u0001\u0006\u0011C\u0013\n\u0019\u0006UF\u0006OU\u0013\u0007\f\u0017\u0002\u0017\u000f\fC\u0007\u0006\u0006\u0016\u0019\u0017\u001c\r\u0012C\u0013\u0011\u001a\u000eU5\u001c\u0007\u0010\f \u0013\u0019\f\u0014\u0007H'\u001a-\u001a\u0017 \u0013\u0019\f\u0014\u0007"), remove.getName());
                } else if (!d(yb.a(remove.getName(), false))) {
                    gd.a(1, oi.a("\u0015_*U1S)]gT(Tj[7J4_\"\u001a!S+_}\u001abI"), remove.getName());
                    lg.b(remove);
                } else if (!yb.b(remove.getName(), "md") || !pg.h().V() || a(remove)) {
                    String c2 = c(remove.getName());
                    if (a(remove, c2, z)) {
                        ec a2 = a(remove, c2);
                        if (b(remove)) {
                            if (pg.h().c() && a2.b == qc.b) {
                                gd.b(2, oi.a("|&S+_#\u001a3UgO7V([#\u001a*^g\\.V\"\u001aa\u001a\u0012J+U&^\n_3[#[3[\bH#_5_#\u001a!V&]gS4\u001a3H2_k\u001a&X(H3S)]gO7V([#\u0014"));
                                break;
                            }
                            try {
                                a(a2.f1990a, c2);
                            } catch (Exception e2) {
                                qe.a((Throwable) e2, qb.a("0\u0011\u0007\f\u0007C\u0000\u0013\u0019\f\u0014\u0007\u001c\r\u0012C\u0013\n\u0019\u0006OCP\u0010"), remove.getName());
                            }
                        } else {
                            continue;
                        }
                    } else {
                        continue;
                    }
                } else {
                    gd.a(1, qb.a("'\u0006\u0018\f\u0003\n\u001b\u0004U\u0000\u001a\u0011\u0007\u0016\u0005\u0017\u0010\u0007U\u000e\u0010\u0017\u0014\u0007\u0014\u0017\u0014C\u0013\n\u0019\u0006OCP\u0010"), remove.getName());
                    lg.b(remove);
                }
            }
        }
        a(false);
        gd.b(2, oi.a("\u0003U)_gO7V([#S)]"));
    }

    private /* synthetic */ void c(List<File> list) {
        if (pg.h().e() > 0) {
            ArrayList arrayList = new ArrayList();
            for (File next : list) {
                if (next.exists() && yb.b(next.getName(), "md")) {
                    arrayList.add(c(next.getName()));
                }
            }
            for (int size = list.size() - 1; size >= 0; size--) {
                File file = list.get(size);
                if (file.exists() && yb.b(file.getName(), "mdj")) {
                    String c2 = c(file.getName());
                    if (c2 != null && arrayList.contains(c2)) {
                        list.remove(size);
                        lg.b(file);
                    } else if (c2 != null) {
                        File a2 = lg.a(String.format(oi.a("\u001f4\u0014bI"), new Object[]{c2, "md"}));
                        if (!file.renameTo(a2)) {
                            gd.a(2, qb.a("6\f\u0000\u000f\u0011\rR\u0017U\u0011\u0010\r\u0014\u000e\u0010C\u001f\f\u0000\u0011\u001b\u0002\u0019\n\u001b\u0004U\u0005\u001c\u000f\u0010CP\u0010"), file.getName());
                        } else {
                            list.remove(size);
                            list.add(size, a2);
                            if (pg.h().E()) {
                                lg.b(lg.a(String.format(oi.a("\u001f4\u0014bI"), new Object[]{c2, "mp4"})));
                                lg.b(lg.a(String.format(qb.a("F\u0006MP\u0010"), new Object[]{c2, "mp4_tmp"})));
                            }
                            gd.a(2, oi.a("o4S)]gP(O5T&Vg\\.V\"\u001a!U5\u001a4_4I.U)s#\u001az\u001abI"), c2);
                        }
                    }
                }
            }
        }
    }

    private /* synthetic */ boolean d(String str) {
        return b(str) > 0;
    }

    public static String e(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 'W');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'A');
        }
        return new String(cArr);
    }

    private /* synthetic */ boolean f(String str) {
        return str.equals("log") || str.equals("md");
    }

    private /* synthetic */ boolean a(File file) {
        try {
            if (new JSONObject(lg.b(file)).length() != 0) {
                return true;
            }
            throw new JSONException(oi.a("\n_3[#[3[g\\.V\"\u001a.Igp\u0014u\t\u001a\"W7N>\u001b"));
        } catch (JSONException e2) {
            qe.a((Throwable) e2, qb.a("%\u0014\n\u0019\u0006\u0011C\u0003\u0002\u0019\n\u0011\u0002\u0001\n\u001b\u0004U\u000e\u0010\u0017\u0014\u0007\u0014\u0017\u0014C\u0013\n\u0019\u0006TC3\n\u0019\u0006OF\u0006C&\u0006\u0006\u0010\u001c\f\u001b*\u0011YP\u0010"), file.getAbsoluteFile().toString(), pg.h().m());
            return false;
        }
    }

    private static /* synthetic */ void a(JSONObject jSONObject, String str) throws JSONException, PackageManager.NameNotFoundException {
        Dimension[] a2 = di.b().a(true);
        zn a3 = di.b().a();
        Dimension dimension = a2[0];
        Dimension dimension2 = a2[1];
        jSONObject.put("Connectivity", di.b().ordinal());
        jSONObject.put("StartTime", yb.a(new Date()));
        jSONObject.put("RequestId", str);
        jSONObject.put(qb.a("4\u0013\u00055\u0010\u0011\u0006\n\u001a\r"), di.b());
        jSONObject.put(oi.a("\u0006J7l\"H4S(T\u0004U#_"), di.b());
        jSONObject.put(qb.a("7\u0016\u001b\u0007\u0019\u0006<\u0007"), di.c());
        jSONObject.put(oi.a("\u0003_1S$_\u0004U#_"), di.d());
        jSONObject.put(qb.a(",&5\u0010\u0011\u0006\n\u001a\r"), di.g());
        jSONObject.put(oi.a("{)^5U.^\u0014~\fl\"H4S(T"), di.e());
        jSONObject.put(qb.a("&\u001a\u0006\u0017\u0010\u000e9\f\u0016\u0002\u0019\u0006"), di.e());
        jSONObject.put(oi.a("i$H\"_)m.^3R"), dimension2.c());
        jSONObject.put(qb.a("&\u0000\u0007\u0006\u0010\r=\u0006\u001c\u0004\u001d\u0017"), dimension2.a());
        jSONObject.put(oi.a("\u0004V._)N\u0013S*_"), jSONObject.getString("StartTime"));
        Object e2 = pc.e();
        if (e2 != null) {
            jSONObject.put(qb.a(",\u0013\u0005\u0019\n\u001b\u0006&\u0006\u0006\u0010\u001c\f\u001b\u0010"), e2);
        }
        jSONObject.put(oi.a("\u0003_1S$_\u0004U#_\u000eT3_5T&V"), di.a());
        jSONObject.put(qb.a("8\u0002\u001b\u0016\u0013\u0002\u0016\u0017\u0000\u0011\u0010\u0011"), di.f());
        jSONObject.put(oi.a("t\"N\u0014Y5_\"T\u000f_.]/N"), dimension.a());
        jSONObject.put(qb.a(";\u0006\u00010\u0016\u0011\u0010\u0006\u001b4\u001c\u0007\u0001\u000b"), dimension.c());
        jSONObject.put(oi.a("i$H\"_)~7S"), di.g());
        jSONObject.put(qb.a("%\u000b\f\u0010\u001c\u0000\u0014\u000f8\u0006\u001b\u00167\u0016\u0001\u0017\u001a\r"), di.e());
        jSONObject.put(oi.a("|(T3i$[+_"), (double) di.b());
        jSONObject.put(qb.a("<\r\u001c\u0017\u001c\u0002\u0019,\u0007\n\u0010\r\u0001\u0002\u0001\n\u001a\r"), a3.ordinal());
        JSONArray e3 = pc.e();
        if (e3 != null) {
            jSONObject.put(oi.a("\n_3[#[3[\u0002H5U5I"), e3);
        }
        List<String> a4 = qb.a();
        if (a4 != null) {
            jSONObject.put(qb.a("=\u0002\u0007\u0007\u0002\u0002\u0007\u00060\r\u0016\f\u0011\u0006\u0007\u0010"), new JSONArray(a4));
        }
        jSONObject.put(oi.a("\u0003_1S$_\u0014J\"Y"), di.b());
        if (!yb.a(xd.b().b())) {
            jSONObject.put("AppUserId", xd.b().b());
        }
        String a5 = uc.a();
        if (a5 != null) {
            jSONObject.put("OfflineConfigurationHash", a5);
        }
        gd.a(1, qb.a(" \u001a\r\u0013\n\u0012C%\u0002\u0007\u0002\u0018\u0010OCP\u0010"), jSONObject.toString());
    }

    private /* synthetic */ String c(String str) {
        String b2 = yb.b(str);
        if (yb.b(str, "log")) {
            b2 = pg.h().m();
        }
        if (pb.a(str)) {
            b2 = str.replace("TEST-", "").replace(String.format(oi.a("\u0014bI"), new Object[]{"mp4"}), "");
        }
        return yb.b(str, "png") ? str.split(qb.a("<"))[0] : b2;
    }

    private /* synthetic */ boolean b(File file) {
        if (!yb.b(file.getName(), "md")) {
            if (file.getName().toLowerCase().endsWith(String.format(qb.a("[F\u0006MP\u0010"), new Object[]{"md", "zip"}))) {
                return true;
            }
            return false;
        }
        return true;
    }

    private /* synthetic */ void b(List<File> list) {
        for (int size = list.size() - 1; size >= 0; size--) {
            File file = list.get(size);
            if (!yb.a(file.getName()) && file.getName().startsWith("o_") && d(yb.a(file.getName(), false))) {
                list.remove(size);
            }
        }
    }

    private /* synthetic */ void c() {
        List<File> a2 = lg.a((FilenameFilter) null);
        a(a2);
        b(a2);
        c(a2);
        Collections.sort(a2, new ed(this));
        StringBuilder sb = new StringBuilder(a2.size() * 50);
        for (File name : a2) {
            sb.append(name.getName());
            sb.append(qb.a("YC"));
        }
        gd.a(1, oi.a("\u0012J+U&^gK2_2_}\u001abI"), sb.toString());
        synchronized (this.f2078a) {
            this.f2078a.clear();
            this.f2078a.addAll(a2);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00ce A[Catch:{ JSONException -> 0x0150, all -> 0x015c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ com.appsee.ec b(java.io.File r21, java.lang.String r22) throws java.io.IOException {
        /*
            r20 = this;
            r1 = r20
            r2 = r21
            long r12 = r21.length()
            com.appsee.ec r14 = new com.appsee.ec
            com.appsee.qc r0 = com.appsee.qc.b
            r3 = 0
            r14.<init>(r0, r3)
            byte[] r0 = com.appsee.lg.c()
            r4 = 0
            int r0 = com.appsee.lg.a((java.io.File) r2, (long) r4, (byte[]) r0)
            r15 = 0
            r16 = r4
            r18 = 0
        L_0x001f:
            if (r0 <= 0) goto L_0x015f
            java.lang.String r3 = r21.getName()
            boolean r3 = r1.e((java.lang.String) r3)
            r4 = 2
            if (r3 == 0) goto L_0x0044
            com.appsee.mg r3 = com.appsee.di.b()
            com.appsee.mg r5 = com.appsee.mg.b
            if (r3 == r5) goto L_0x0044
            boolean r3 = com.appsee.pc.a((java.lang.String) r22)
            if (r3 == 0) goto L_0x0044
            java.lang.String r0 = "s)\u001a\u0012J+U&^\bT\u0010S\u0001S\bT+CgW(^\"\u001a&T#\u001a\tu\u0013\u001a(Tgm.|.\u0016gI,S7J.T \u001a3R.Ig\\.V\"\u001d4\u001a2J+U&^"
            java.lang.String r0 = com.appsee.oi.a((java.lang.String) r0)
            com.appsee.gd.b(r4, r0)
            return r14
        L_0x0044:
            r10 = 1
            int r3 = (r16 > r12 ? 1 : (r16 == r12 ? 0 : -1))
            if (r3 < 0) goto L_0x0053
            java.lang.String r0 = "%\u001c\u000f\u0010C\u0016\u000b\u0014\r\u0012\u0006\u0011C\u0011\u0016\u0007\n\u001b\u0004U\u0016\u0005\u000f\u001a\u0002\u0011\n\u001b\u0004YC\u0006\b\u001c\u0013\u0005\n\u001b\u0004"
            java.lang.String r0 = com.appsee.qb.a(r0)
            com.appsee.gd.b(r10, r0)
            return r14
        L_0x0053:
            java.lang.String r3 = "\u0004[+V.T \u001a2J+U&^g\\(Hg\u001f4\u001a0S3RgU!\\4_3\u0007b^k\u001a4S=_z\u001f#\u0016gY/O)QgI.@\"\u0007b^"
            java.lang.String r3 = com.appsee.oi.a((java.lang.String) r3)
            r5 = 4
            java.lang.Object[] r5 = new java.lang.Object[r5]
            java.lang.String r6 = r21.getName()
            r5[r15] = r6
            java.lang.Long r6 = java.lang.Long.valueOf(r16)
            r5[r10] = r6
            java.lang.Long r6 = java.lang.Long.valueOf(r12)
            r5[r4] = r6
            r4 = 3
            com.appsee.pg r6 = com.appsee.pg.h()
            long r6 = r6.h()
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            r5[r4] = r6
            com.appsee.gd.a(r10, r3, r5)
            com.appsee.vc r11 = r1.d
            monitor-enter(r11)
            com.appsee.vc r3 = r1.d     // Catch:{ Exception -> 0x00c0, all -> 0x00bb }
            byte[] r4 = com.appsee.lg.c()     // Catch:{ Exception -> 0x00c0, all -> 0x00bb }
            java.lang.String r7 = r21.getName()     // Catch:{ Exception -> 0x00c0, all -> 0x00bb }
            r5 = r0
            r6 = r22
            r8 = r16
            r19 = r11
            r10 = r12
            org.json.JSONObject r3 = r3.a((byte[]) r4, (int) r5, (java.lang.String) r6, (java.lang.String) r7, (long) r8, (long) r10)     // Catch:{ Exception -> 0x00b9 }
            if (r3 == 0) goto L_0x00a9
            java.lang.String r4 = "0\u001e\n\u0005,\u001b&\u0007\u0011\u001a\u0011"
            java.lang.String r4 = com.appsee.qb.a(r4)     // Catch:{ Exception -> 0x00b9 }
            boolean r4 = r3.optBoolean(r4, r15)     // Catch:{ Exception -> 0x00b9 }
            r4 = r18 | r4
            r18 = r4
        L_0x00a9:
            monitor-exit(r19)     // Catch:{ all -> 0x015c }
            long r4 = (long) r0
            long r4 = r16 + r4
            byte[] r0 = com.appsee.lg.c()
            int r0 = com.appsee.lg.a((java.io.File) r2, (long) r4, (byte[]) r0)
            r16 = r4
            goto L_0x001f
        L_0x00b9:
            r0 = move-exception
            goto L_0x00c3
        L_0x00bb:
            r0 = move-exception
            r19 = r11
            goto L_0x015d
        L_0x00c0:
            r0 = move-exception
            r19 = r11
        L_0x00c3:
            java.lang.String r3 = "5H(HgO7V([#S)]gY/O)Qk\u001a4Q.J7S)]gN/S4\u001a!S+_"
            java.lang.String r3 = com.appsee.oi.a((java.lang.String) r3)     // Catch:{ all -> 0x015c }
            com.appsee.qe.a((java.lang.Throwable) r0, (java.lang.String) r3, (boolean) r15)     // Catch:{ all -> 0x015c }
            if (r18 == 0) goto L_0x00d2
            com.appsee.qc r3 = com.appsee.qc.f2055a     // Catch:{ all -> 0x015c }
            r14.b = r3     // Catch:{ all -> 0x015c }
        L_0x00d2:
            com.appsee.pg r3 = com.appsee.pg.h()     // Catch:{ all -> 0x015c }
            boolean r3 = r3.o()     // Catch:{ all -> 0x015c }
            if (r3 == 0) goto L_0x015a
            boolean r2 = r20.b((java.io.File) r21)     // Catch:{ all -> 0x015c }
            if (r2 == 0) goto L_0x015a
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ all -> 0x015c }
            r2.<init>()     // Catch:{ all -> 0x015c }
            java.lang.String r3 = "0\u0010\u0010\u0006\n\u001a\r<\u0007"
            java.lang.String r3 = com.appsee.qb.a(r3)     // Catch:{ all -> 0x015c }
            r4 = r22
            r2.put(r3, r4)     // Catch:{ all -> 0x015c }
            java.lang.String r3 = "5H(H"
            java.lang.String r3 = com.appsee.oi.a((java.lang.String) r3)     // Catch:{ all -> 0x015c }
            java.lang.String r0 = r0.getMessage()     // Catch:{ all -> 0x015c }
            r2.put(r3, r0)     // Catch:{ all -> 0x015c }
            java.lang.String r0 = "3\n\u0019\u0006&\n\u000f\u0006"
            java.lang.String r0 = com.appsee.qb.a(r0)     // Catch:{ all -> 0x015c }
            java.lang.Long r3 = java.lang.Long.valueOf(r12)     // Catch:{ all -> 0x015c }
            r2.put(r0, r3)     // Catch:{ all -> 0x015c }
            java.lang.String r0 = "\u0004O5H\"T3s)^\"B"
            java.lang.String r0 = com.appsee.oi.a((java.lang.String) r0)     // Catch:{ all -> 0x015c }
            java.lang.Long r3 = java.lang.Long.valueOf(r16)     // Catch:{ all -> 0x015c }
            r2.put(r0, r3)     // Catch:{ all -> 0x015c }
            java.lang.String r0 = " \u001d\u0016\u001b\b&\n\u000f\u0006"
            java.lang.String r0 = com.appsee.qb.a(r0)     // Catch:{ all -> 0x015c }
            com.appsee.pg r3 = com.appsee.pg.h()     // Catch:{ all -> 0x015c }
            long r3 = r3.h()     // Catch:{ all -> 0x015c }
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x015c }
            r2.put(r0, r3)     // Catch:{ all -> 0x015c }
            java.lang.String r0 = "\u0014C4N\"W\u0012J3S*_"
            java.lang.String r0 = com.appsee.oi.a((java.lang.String) r0)     // Catch:{ all -> 0x015c }
            long r3 = android.os.SystemClock.uptimeMillis()     // Catch:{ all -> 0x015c }
            r5 = 1000(0x3e8, double:4.94E-321)
            long r3 = r3 / r5
            java.lang.Long r3 = java.lang.Long.valueOf(r3)     // Catch:{ all -> 0x015c }
            r2.put(r0, r3)     // Catch:{ all -> 0x015c }
            com.appsee.pc.a((java.util.HashMap<java.lang.String, java.lang.Object>) r2)     // Catch:{ JSONException -> 0x0150 }
            java.lang.String r0 = "\u000e\u0011C\u0010\u0011\u0007\f\u0007C\u0014\u0007\u0011\u0006\u0011C\u0001\fU0\u001d\u0002\u0007\u0006\u00113\u0007\u0006\u0013\u0006\u0007\u0006\u001b\u0000\u0010\u0010"
            java.lang.String r0 = com.appsee.qb.a(r0)     // Catch:{ JSONException -> 0x0150 }
            r2 = 1
            com.appsee.gd.b(r2, r0)     // Catch:{ JSONException -> 0x0150 }
            goto L_0x015a
        L_0x0150:
            r0 = move-exception
            java.lang.String r2 = "\u0002H5U5\u001a&J7_)^.T \u001a\n~g_5H(H4\u001a!H(WgI/[5_#\u001a7H\"\\\"H\"T$_4\u0014"
            java.lang.String r2 = com.appsee.oi.a((java.lang.String) r2)     // Catch:{ all -> 0x015c }
            com.appsee.qe.a(r0, r2)     // Catch:{ all -> 0x015c }
        L_0x015a:
            monitor-exit(r19)     // Catch:{ all -> 0x015c }
            return r14
        L_0x015c:
            r0 = move-exception
        L_0x015d:
            monitor-exit(r19)     // Catch:{ all -> 0x015c }
            throw r0
        L_0x015f:
            com.appsee.ec r0 = new com.appsee.ec
            com.appsee.qc r2 = com.appsee.qc.c
            com.appsee.yc r3 = r1.a((org.json.JSONObject) r3)
            r0.<init>(r2, r3)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.wd.b(java.io.File, java.lang.String):com.appsee.ec");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m154a() {
        gd.b(1, qb.a("0\u0001\f\u0005\u0013\u001c\r\u0012C\u0000\u0013\u0019\f\u0014\u0007"));
        if (!a()) {
            gd.b(1, oi.a("t(\u001a)_\"^gN(\u001a4N(J"));
            return;
        }
        synchronized (this.f2078a) {
            this.f2078a.clear();
        }
        this.c = true;
        this.d.a();
    }

    private /* synthetic */ void a(List<File> list) {
        String c2;
        if (xd.b().d()) {
            for (int size = list.size() - 1; size >= 0; size--) {
                File file = list.get(size);
                String a2 = yb.a(file.getName(), false);
                if (file.exists() && ((d(a2) || a2.equals("h264") || a2.equals("mp4_tmp")) && (c2 = c(file.getName())) != null && c2.equalsIgnoreCase(pg.h().m()))) {
                    list.remove(size);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0083 A[Catch:{ Exception -> 0x002c }] */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0095 A[Catch:{ Exception -> 0x002c }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a9 A[Catch:{ Exception -> 0x002c }] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00ae A[Catch:{ Exception -> 0x002c }, DONT_GENERATE] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b() {
        /*
            r10 = this;
            boolean r0 = r10.a()
            if (r0 == 0) goto L_0x0010
            java.lang.String r0 = "6\u0002\u001bD\u0001C\u0016\u0002\u0019\u000fU\u0004\u0010\u0017U\u0000\u001a\r\u0013\n\u0012\u0016\u0007\u0002\u0001\n\u001a\rYC\u0017\u0016\u0006\u001aU\u0016\u0005\u000f\u001a\u0002\u0011\n\u001b\u0004"
            java.lang.String r0 = com.appsee.qb.a(r0)
            com.appsee.qe.b(r0)
            return
        L_0x0010:
            r0 = 60000(0xea60, float:8.4078E-41)
            com.appsee.pg r1 = com.appsee.pg.h()
            java.lang.String r1 = r1.M()
            org.json.JSONObject r2 = new org.json.JSONObject
            r2.<init>()
            com.appsee.vc r3 = r10.d     // Catch:{ Exception -> 0x00cb }
            monitor-enter(r3)     // Catch:{ Exception -> 0x00cb }
            a((org.json.JSONObject) r2, (java.lang.String) r1)     // Catch:{ all -> 0x00c8 }
            r4 = 0
            com.appsee.hc r1 = com.appsee.uc.a((java.lang.String) r1, (org.json.JSONObject) r2)     // Catch:{ Exception -> 0x002c }
            goto L_0x0037
        L_0x002c:
            r1 = move-exception
            java.lang.String r5 = "\u0002H5U5\u001a0R.V\"\u001a5_3H._1S)]gN/_gU!\\+S)_gY(T!S O5[3S(T"
            java.lang.String r5 = com.appsee.oi.a((java.lang.String) r5)     // Catch:{ all -> 0x00c8 }
            com.appsee.qe.a(r1, r5)     // Catch:{ all -> 0x00c8 }
            r1 = r4
        L_0x0037:
            if (r1 == 0) goto L_0x0053
            org.json.JSONObject r5 = r1.b     // Catch:{ all -> 0x00c8 }
            java.lang.String r6 = " \u001a\r\u0013\n\u0012+\u0001\u0017\u00057\u001c\u000e\u0010\f\u0000\u0017<\r8\n\u0019\u000f\u001c\u0010\u0010\u0000\u001a\r\u0011\u0010"
            java.lang.String r6 = com.appsee.qb.a(r6)     // Catch:{ all -> 0x00c8 }
            boolean r5 = r5.has(r6)     // Catch:{ all -> 0x00c8 }
            if (r5 == 0) goto L_0x0053
            org.json.JSONObject r0 = r1.b     // Catch:{ all -> 0x00c8 }
            java.lang.String r5 = "y(T!S r3N7n.W\"U2N\u000eT\nS+V.I\"Y(T#I"
            java.lang.String r5 = com.appsee.oi.a((java.lang.String) r5)     // Catch:{ all -> 0x00c8 }
            int r0 = r0.getInt(r5)     // Catch:{ all -> 0x00c8 }
        L_0x0053:
            r5 = 1
            r6 = 0
            com.appsee.vc r7 = r10.d     // Catch:{ IOException -> 0x0098, IllegalArgumentException -> 0x0088, JSONException -> 0x0076 }
            org.json.JSONObject r0 = r7.a((org.json.JSONObject) r2, (int) r0)     // Catch:{ IOException -> 0x0098, IllegalArgumentException -> 0x0088, JSONException -> 0x0076 }
            com.appsee.pc.i()     // Catch:{ IOException -> 0x0074, IllegalArgumentException -> 0x0072, JSONException -> 0x0070 }
            java.lang.String r7 = "-\u0010\u0017\u0002\f\u0007\b\u0010\u0011U\u0004\u001a\u0017U\u0000\u001a\r\u0013\n\u0012\u0016\u0007\u0002\u0001\n\u001a\rOiP\u0010"
            java.lang.String r7 = com.appsee.qb.a(r7)     // Catch:{ IOException -> 0x0074, IllegalArgumentException -> 0x0072, JSONException -> 0x0070 }
            java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ IOException -> 0x0074, IllegalArgumentException -> 0x0072, JSONException -> 0x0070 }
            java.lang.String r9 = r0.toString()     // Catch:{ IOException -> 0x0074, IllegalArgumentException -> 0x0072, JSONException -> 0x0070 }
            r8[r6] = r9     // Catch:{ IOException -> 0x0074, IllegalArgumentException -> 0x0072, JSONException -> 0x0070 }
            com.appsee.gd.a(r5, r7, r8)     // Catch:{ IOException -> 0x0074, IllegalArgumentException -> 0x0072, JSONException -> 0x0070 }
            goto L_0x00a7
        L_0x0070:
            r0 = move-exception
            goto L_0x0078
        L_0x0072:
            r0 = move-exception
            goto L_0x008a
        L_0x0074:
            r0 = move-exception
            goto L_0x009a
        L_0x0076:
            r0 = move-exception
            r5 = 0
        L_0x0078:
            java.lang.String r6 = "p\u0014u\t\u001a\"B$_7N.U)\u001a0R.V\"\u001a!_3Y/S)]gY(T!S O5[3S(T"
            java.lang.String r6 = com.appsee.oi.a((java.lang.String) r6)     // Catch:{ all -> 0x00c8 }
            com.appsee.qe.a(r0, r6)     // Catch:{ all -> 0x00c8 }
            if (r1 == 0) goto L_0x0086
            org.json.JSONObject r0 = r1.b     // Catch:{ all -> 0x00c8 }
            goto L_0x00a7
        L_0x0086:
            r0 = r4
            goto L_0x00a7
        L_0x0088:
            r0 = move-exception
            r5 = 0
        L_0x008a:
            java.lang.String r6 = "5\u0014\u000f\u001c\u0007\u0014\u0017\u001c\f\u001bC\u0010\u001b\u0016\u0006\u0005\u0017\u001c\f\u001bC\u0002\u000b\u001c\u000f\u0010C\u0013\u0006\u0001\u0000\u001d\n\u001b\u0004U\u0000\u001a\r\u0013\n\u0012\u0016\u0007\u0002\u0001\n\u001a\r"
            java.lang.String r6 = com.appsee.qb.a(r6)     // Catch:{ all -> 0x00c8 }
            com.appsee.qe.a(r0, r6)     // Catch:{ all -> 0x00c8 }
            if (r1 == 0) goto L_0x0086
            org.json.JSONObject r0 = r1.b     // Catch:{ all -> 0x00c8 }
            goto L_0x00a7
        L_0x0098:
            r0 = move-exception
            r5 = 0
        L_0x009a:
            java.lang.String r6 = "\t_3M(H,\u001a\"B$_7N.U)\u001a0R.V\"\u001a!_3Y/S)]gY(T!S O5[3S(T"
            java.lang.String r6 = com.appsee.oi.a((java.lang.String) r6)     // Catch:{ all -> 0x00c8 }
            com.appsee.qe.a(r0, r6)     // Catch:{ all -> 0x00c8 }
            if (r1 == 0) goto L_0x0086
            org.json.JSONObject r0 = r1.b     // Catch:{ all -> 0x00c8 }
        L_0x00a7:
            if (r0 != 0) goto L_0x00ae
            a((org.json.JSONObject) r2, (java.lang.Exception) r4)     // Catch:{ all -> 0x00c8 }
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            return
        L_0x00ae:
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            if (r5 != 0) goto L_0x00b6
            com.appsee.kc r1 = r1.f2003a     // Catch:{ Exception -> 0x00cb }
            com.appsee.uc.a((org.json.JSONObject) r2, (com.appsee.kc) r1)     // Catch:{ Exception -> 0x00cb }
        L_0x00b6:
            com.appsee.pg r1 = com.appsee.pg.h()     // Catch:{ Exception -> 0x00cb }
            r1.a((org.json.JSONObject) r0)     // Catch:{ Exception -> 0x00cb }
            com.appsee.uc.a((boolean) r5, (org.json.JSONObject) r0)     // Catch:{ Exception -> 0x00cb }
            com.appsee.rd r0 = com.appsee.rd.a()     // Catch:{ Exception -> 0x00cb }
            r0.e()     // Catch:{ Exception -> 0x00cb }
            return
        L_0x00c8:
            r0 = move-exception
            monitor-exit(r3)     // Catch:{ all -> 0x00c8 }
            throw r0     // Catch:{ Exception -> 0x00cb }
        L_0x00cb:
            r0 = move-exception
            a((org.json.JSONObject) r2, (java.lang.Exception) r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.wd.b():void");
    }

    private /* synthetic */ void a(yc ycVar, String str) {
        if (ycVar == null) {
            return;
        }
        if (ycVar == yc.c) {
            gd.a(1, qb.a("2\f\u0001C#\n\u0011\u0006\u001a6\u0005\u000f\u001a\u0002\u0011^1\f;\f\u00016\u0005\u000f\u001a\u0002\u0011XU\u0007\u0010\u000f\u0010\u0017\u001c\r\u0012C\u0003\n\u0011\u0006\u001aC\u0013\f\u0007CP\u0010"), str);
            lg.a(str);
            return;
        }
        if (lg.b(String.format(oi.a("\u001f4\u0014bI"), new Object[]{str, "mp4"})) != null) {
            try {
                pc.a(str, ycVar);
            } catch (JSONException e2) {
                qe.a((Throwable) e2, qb.a("&\u0007\u0011\u001a\u0011U\u0014\u001d\n\u0019\u0006U\u0002\u0005\u0013\u0019\u001a\u001c\r\u0012C\u0014C#\n\u0011\u0006\u001a6\u0005\u000f\u001a\u0002\u00113\u001a\u000f\u001c\u0000\fC\u0013\f\u0007C\u0006\u0006\u0006\u0010\u001c\f\u001bCP\u0010"), str);
            }
        }
    }

    private /* synthetic */ boolean a(String str) {
        return yb.a(str.replace(String.format(qb.a("MP\u0010"), new Object[]{"zip"}), ""), false).equals("mp4");
    }

    private /* synthetic */ boolean a(File file, String str, boolean z) {
        if (str == null) {
            gd.b(1, qb.a("-\u001aC&\u0006\u0006\u0010\u001c\f\u001b*\u0011OU\u0010\u001e\n\u0005\u0013\u001c\r\u0012C\u0000\u0013\u0019\f\u0014\u0007"));
            return false;
        } else if (!z || !e(file.getName()) || pg.h().W()) {
            return true;
        } else {
            gd.b(2, oi.a("\u0003_!_5H.T \u001a$H&I/\u001a1S#_(\u001a2J+U&^"));
            return false;
        }
    }

    private /* synthetic */ yc a(JSONObject jSONObject) {
        if (jSONObject == null || !jSONObject.has("VideoUploadPolicy")) {
            return null;
        }
        try {
            return yc.values()[jSONObject.getInt("VideoUploadPolicy")];
        } catch (JSONException e2) {
            qe.a((Throwable) e2, qb.a("0\u0011\u0007\f\u0007C\u0002\u000b\u001c\u000f\u0010C\u0010\u001b\u0001\u0011\u0014\u0000\u0001\n\u001b\u0004U\u0002U5\u001c\u0007\u0010\f \u0013\u0019\f\u0014\u0007%\f\u0019\n\u0016\u001aU\u0005\u0007\f\u0018C?0:-OCP\u0010"), jSONObject.toString());
            return null;
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ int a(File file, File file2) {
        return (!yb.b(file.getName(), "mp4") || !yb.b(file2.getName(), "mp4")) ? file.lastModified() < file2.lastModified() ? -1 : 1 : pg.h().t() ? pg.h().a() ? file.length() > file2.length() ? -1 : 1 : file.length() < file2.length() ? -1 : 1 : pg.h().a() ? file.lastModified() > file2.lastModified() ? -1 : 1 : file.lastModified() < file2.lastModified() ? -1 : 1;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ int b(String str) {
        if (str.equals("md") || str.equals("mdj")) {
            return 2;
        }
        if (str.equals("log")) {
            return pg.h().y() ? 1 : 3;
        }
        if (str.equals("mp4")) {
            return 5;
        }
        if (str.equals("png")) {
            return 6;
        }
        if (str.equals("ico")) {
            return 7;
        }
        if (!str.equals("h264") && !str.equals("mp4_tmp")) {
            gd.a(1, oi.a("o)Q)U0Tg_?N\"T4S(T}\u001abI"), str);
        }
        return 0;
    }

    public void a(boolean z) {
        if (!a(true)) {
            gd.b(1, oi.a("{+H\"[#CgO7V([#S)]"));
            return;
        }
        this.c = false;
        c();
        b(z);
    }

    public static synchronized wd a() {
        wd wdVar;
        synchronized (wd.class) {
            if (e == null) {
                e = new wd();
            }
            wdVar = e;
        }
        return wdVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x00db A[Catch:{ JSONException -> 0x00df }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ com.appsee.ec a(java.io.File r9, java.lang.String r10) throws java.io.IOException {
        /*
            r8 = this;
            java.lang.String r0 = "\u0012J+U&^.T \u001a$U)N\"T3\u0000g\u001f4\u0014i\u0014"
            java.lang.String r0 = com.appsee.oi.a((java.lang.String) r0)
            r1 = 1
            java.lang.Object[] r2 = new java.lang.Object[r1]
            java.lang.String r3 = r9.getName()
            r4 = 0
            r2[r4] = r3
            r3 = 2
            com.appsee.gd.a(r3, r0, r2)
            com.appsee.pg r0 = com.appsee.pg.h()
            java.lang.Boolean r0 = r0.h()
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = r9.getName()
            java.lang.String r0 = com.appsee.yb.a((java.lang.String) r0, (boolean) r4)
            boolean r0 = r8.f(r0)
            if (r0 == 0) goto L_0x005e
            java.lang.String r0 = "6\f\u0018\u0013\u0007\u0006\u0006\u0010\u001c\r\u0012C\u0013\n\u0019\u0006"
            java.lang.String r0 = com.appsee.qb.a(r0)
            com.appsee.gd.b(r1, r0)
            java.io.File r0 = com.appsee.lg.b((java.io.File) r9)
            if (r0 == 0) goto L_0x005f
            boolean r2 = r0.exists()
            if (r2 == 0) goto L_0x005f
            java.lang.String r2 = r0.getName()
            java.lang.String r3 = "zip"
            boolean r2 = r2.endsWith(r3)
            if (r2 == 0) goto L_0x005f
            long r2 = r0.length()
            r5 = 8
            int r7 = (r2 > r5 ? 1 : (r2 == r5 ? 0 : -1))
            if (r7 > 0) goto L_0x005f
            com.appsee.lg.b((java.io.File) r0)     // Catch:{ Exception -> 0x005e }
        L_0x005e:
            r0 = r9
        L_0x005f:
            com.appsee.ec r2 = r8.b(r0, r10)
            r3 = 1
        L_0x0064:
            com.appsee.qc r5 = r2.b
            com.appsee.qc r6 = com.appsee.qc.b
            if (r5 != r6) goto L_0x00c4
            com.appsee.pg r5 = com.appsee.pg.h()
            int r5 = r5.b()
            if (r3 > r5) goto L_0x00c4
            boolean r5 = r8.c
            if (r5 != 0) goto L_0x00c4
            java.lang.String r2 = "\u0001[.V\"^gN(\u001a2J+U&^g\\.V\"\u001a3H>S)]g[ [.T"
            java.lang.String r2 = com.appsee.oi.a((java.lang.String) r2)
            com.appsee.gd.b(r1, r2)
            com.appsee.pg r2 = com.appsee.pg.h()
            int r2 = r2.d()
            if (r2 <= 0) goto L_0x00bd
            java.lang.String r2 = "0\u0019\u0006\u0010\u0013\u001c\r\u0012C\u0013\f\u0007CP\u0007U\u0010\u0010\u0000\u001a\r\u0011\u0010"
            java.lang.String r2 = com.appsee.qb.a(r2)
            java.lang.Object[] r5 = new java.lang.Object[r1]
            com.appsee.pg r6 = com.appsee.pg.h()
            int r6 = r6.d()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            r5[r4] = r6
            com.appsee.gd.a(r1, r2, r5)
            com.appsee.pg r2 = com.appsee.pg.h()     // Catch:{ InterruptedException -> 0x00b3 }
            int r2 = r2.d()     // Catch:{ InterruptedException -> 0x00b3 }
            int r2 = r2 * 1000
            long r5 = (long) r2     // Catch:{ InterruptedException -> 0x00b3 }
            java.lang.Thread.sleep(r5)     // Catch:{ InterruptedException -> 0x00b3 }
            goto L_0x00bd
        L_0x00b3:
            r2 = move-exception
            java.lang.String r5 = "\u0002H5U5\u001a4V\"_7S)]k\u001a4_)^.T \u001a!S+_gT(M"
            java.lang.String r5 = com.appsee.oi.a((java.lang.String) r5)
            com.appsee.qe.a(r2, r5)
        L_0x00bd:
            int r3 = r3 + 1
            com.appsee.ec r2 = r8.b(r0, r10)
            goto L_0x0064
        L_0x00c4:
            com.appsee.qc r3 = r2.b
            com.appsee.qc r5 = com.appsee.qc.f2055a
            if (r3 == r5) goto L_0x00ce
            com.appsee.qc r5 = com.appsee.qc.c
            if (r3 != r5) goto L_0x00ed
        L_0x00ce:
            com.appsee.lg.b((java.io.File) r9)
            java.lang.String r3 = r9.getName()     // Catch:{ JSONException -> 0x00df }
            boolean r3 = r8.a((java.lang.String) r3)     // Catch:{ JSONException -> 0x00df }
            if (r3 == 0) goto L_0x00ed
            com.appsee.pc.a((java.lang.String) r10)     // Catch:{ JSONException -> 0x00df }
            goto L_0x00ed
        L_0x00df:
            r3 = move-exception
            java.lang.String r5 = "0\u0011\u0007\f\u0007C\u0002\u000b\u001c\u000f\u0010C\u0007\u0006\u0018\f\u0003\n\u001b\u0004U5\u001c\u0007\u0010\f \u0013\u0019\f\u0014\u0007%\f\u0019\n\u0016\u001aU\u0002\u0013\u0017\u0010\u0011U\u0010\u0010\u0010\u0006\n\u001a\rUF\u0006C\u0002\u0002\u0006C\u0000\u0013\u0019\f\u0014\u0007\u0010\u0007"
            java.lang.String r5 = com.appsee.qb.a(r5)
            java.lang.Object[] r1 = new java.lang.Object[r1]
            r1[r4] = r10
            com.appsee.qe.a((java.lang.Throwable) r3, (java.lang.String) r5, (java.lang.Object[]) r1)
        L_0x00ed:
            if (r0 == 0) goto L_0x0100
            java.lang.String r9 = r9.getName()
            java.lang.String r10 = r0.getName()
            boolean r9 = r9.equals(r10)
            if (r9 != 0) goto L_0x0100
            com.appsee.lg.b((java.io.File) r0)
        L_0x0100:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.wd.a(java.io.File, java.lang.String):com.appsee.ec");
    }
}
