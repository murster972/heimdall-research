package com.appsee;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class oc {
    private static oc b;

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, jm> f2043a = new HashMap<>();

    private /* synthetic */ oc() {
    }

    public static JSONObject a(HashMap<String, jm> hashMap) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : hashMap.entrySet()) {
            int c = ((jm) next.getValue()).c();
            if (c >= 0) {
                jSONObject.put((String) next.getKey(), c);
            }
        }
        return jSONObject;
    }

    public static synchronized oc a() {
        oc ocVar;
        synchronized (oc.class) {
            if (b == null) {
                b = new oc();
            }
            ocVar = b;
        }
        return ocVar;
    }

    /* renamed from: a  reason: collision with other method in class */
    public synchronized void m67a() {
        synchronized (this.f2043a) {
            this.f2043a.clear();
        }
    }

    public HashMap<String, jm> a(boolean z) {
        HashMap<String, jm> hashMap = this.f2043a;
        if (!z) {
            return hashMap;
        }
        synchronized (hashMap) {
            this.f2043a = new HashMap<>();
        }
        return hashMap;
    }

    public jm a(String str) {
        jm jmVar;
        synchronized (this.f2043a) {
            jmVar = this.f2043a.get(str);
            if (jmVar == null) {
                jmVar = new jm(str);
                this.f2043a.put(str, jmVar);
            }
        }
        return jmVar;
    }
}
