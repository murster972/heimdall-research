package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Build;
import android.view.SurfaceView;
import android.view.TextureView;
import android.view.View;
import com.facebook.common.util.ByteConstants;
import com.facebook.imagepipeline.common.RotationOptions;
import com.vungle.warren.AdLoader;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

class s {
    private static s L;
    private ch A;
    private final Object B = new Object();
    private oi C;
    private long D;
    private Paint E = new Paint();
    private final List<gc> F = new ArrayList();
    private final HashMap<View, Rect> G = new HashMap<>();
    private jm H = oc.a().a(ui.a(" \u001c\u0012\u0010\u0019'\u0013\u0016\u0019\u0007\u0012\u0010\u0004U&\u0007\u001f\u0003\u0017\u0016\u000f6\u001e\u0010\u0015\u001e"));
    private String I;
    /* access modifiers changed from: private */
    public qb J;
    private jm K = oc.a().a(ui.a("8\u001a\u00182:8\u001f\r"));

    /* renamed from: a  reason: collision with root package name */
    private jm f2061a = oc.a().a(bb.a("TQuWb\\tZhF"));
    private boolean b;
    private jm c = oc.a().a(bb.a("Q[cWh`bQh@cWu\u0012TKtFb_BDb\\s~nAsWiWums[dY"));
    private jm d = oc.a().a(ui.a("#\u001f\u0011\u0013\u001a$\u0010\u0015\u001a\u0004\u0011\u0013\u0007V'\u0019\u001a\u0002#\u001f\u0010\u0001\u00060\u0010\u0002\u0016\u001e\u001c\u0018\u0012"));
    private int e;
    private long f = 0;
    /* access modifiers changed from: private */
    public Thread g;
    private jm h = oc.a().a(ui.a(" \u001c\u0012\u0010\u0019'\u0013\u0016\u0019\u0007\u0012\u0010\u0004U9\u0007\u001f\u0010\u0018\u0001\u0017\u0001\u001f\u001a\u00181\u0013\u0001\u0013\u0016\u0002\u001c\u0019\u001b"));
    private int i;
    private long j;
    private boolean k;
    private final List<View> l = new ArrayList();
    private boolean m;
    private Dimension n;
    private jm o = oc.a().a(bb.a("Q[cWh`bQh@cWu\u0012S[jWums[dY"));
    private bb p;
    private final Rect q = new Rect();
    private jm r = oc.a().a(bb.a("dnVb]FBwWiV"));
    private jm s = oc.a().a(ui.a("=\u001f\u0011\u0013#\u001f\u0010\u0001\u0006"));
    private jm t = oc.a().a(bb.a("Q[cWh`bQh@cWu\u0012TYnBA@f_bqoWdY"));
    private boolean u;
    private long v;
    private volatile boolean w = false;
    private int x;
    /* access modifiers changed from: private */
    public Exception y;
    private Dimension z;

    private /* synthetic */ s() {
        this.E.setColor(Color.rgb(25, 25, 25));
    }

    private /* synthetic */ boolean b(List<View> list) {
        List<T> c2;
        View a2;
        View e2;
        View c3;
        View b2;
        String a3 = bb.a("TYnBw[iU'@b\\cWu[iU'DnWp\u0012eWdSrAb\u0012\"A");
        if (!pg.h().l() && (b2 = rb.b(list)) != null) {
            gd.a(1, a3, String.format(ui.a("\u0007\u0003\u001b\u0018\u001c\u0018\u0012V\u0014\u0018\u001c\u001b\u0014\u0002\u001c\u0019\u001bV\u0011\u0013\u0001\u0013\u0016\u0002\u0010\u0012U^P\u0005\\"), new Object[]{rb.c(b2)}));
            return true;
        } else if (!pg.h().O() && (c3 = rb.c(list)) != null) {
            gd.a(1, a3, String.format(bb.a("@r\\i[iU'Si[jSs[h\\'VbFbQsWc\u0012/\u0017t\u001b"), new Object[]{String.format(ui.a("\u0004\u0000\u0018\u001b\u001f\u001b\u0011U$\u001c\u0006\u0005\u001a\u0010V\u0011\u0013\u0001\u0013\u0016\u0002\u0010\u0012U^P\u0005\\"), new Object[]{rb.c(c3)})}));
            return true;
        } else if (!pg.h().j() && (e2 = rb.e(list)) != null) {
            gd.a(1, a3, String.format(bb.a("@r\\i[iU'Si[jSs[h\\'VbFbQsWc\u0012/\u0017t\u001b"), new Object[]{String.format(ui.a("\u0005\u0016\u0004\u001a\u001a\u0019\u001f\u001b\u0011U$\u0010\u0015\f\u0015\u0019\u0013\u0007 \u001c\u0013\u0002V\u0011\u0013\u0001\u0013\u0016\u0002\u0010\u0012U^P\u0005\\"), new Object[]{rb.c(e2)})}));
            return true;
        } else if (pg.h().D() || (a2 = rb.a(list)) == null) {
            List<Object> b3 = pg.h().b("skip");
            if (b3 != null && !b3.isEmpty()) {
                for (View next : list) {
                    View view = null;
                    if (!b3.isEmpty() && (c2 = rb.c(next, b3)) != null && !c2.isEmpty()) {
                        view = (View) c2.get(0);
                        continue;
                    }
                    if (view != null) {
                        gd.a(1, a3, String.format(bb.a("@r\\i[iU'Si[jSs[h\\'VbFbQsWc\u0012/\u0017t\u001b"), new Object[]{String.format(ui.a("'\u0013\u001b\u0012\u0010\u0004&\u001d\u001c\u0006U\u0000\u001c\u0013\u0002V\u0011\u0013\u0001\u0013\u0016\u0002\u0010\u0012U^P\u0005\\"), new Object[]{view.getClass().getName()})}));
                        return true;
                    }
                }
            }
            return false;
        } else {
            gd.a(1, a3, String.format(bb.a("@r\\i[iU'Si[jSs[h\\'VbFbQsWc\u0012/\u0017t\u001b"), new Object[]{String.format(ui.a("\u0005\u0016\u0004\u001a\u001a\u0019\u001f\u001b\u0011U\u0000\u001c\u0013\u0002V\u0011\u0013\u0001\u0013\u0016\u0002\u0010\u0012U^P\u0005\\"), new Object[]{rb.c(a2)})}));
            return true;
        }
    }

    private /* synthetic */ void f() throws Exception {
        a(a(), (List<View>) null);
    }

    private /* synthetic */ void g() {
        this.J = null;
        this.y = null;
        gd.b(1, bb.a("Q[cWh\u0012tSqWc\u001c"));
        d();
        this.b = true;
    }

    private /* synthetic */ void h() {
        gc remove;
        while (true) {
            synchronized (this.F) {
                if (!this.F.isEmpty()) {
                    remove = this.F.remove(0);
                } else {
                    return;
                }
            }
            a(remove);
        }
        while (true) {
        }
    }

    private /* synthetic */ void i() throws Exception {
        if (xd.b().d()) {
            if (pg.h().u() || !wb.a().a()) {
                this.c.b();
                ub.a().b();
                this.c.d();
                this.h.b();
                di.b().b();
                this.h.d();
                try {
                    this.d.b();
                    Object[] a2 = ub.a().a();
                    if (a(a2)) {
                        gd.b(1, bb.a("TYnBw[iU'DnVb]'TuSjW"));
                        return;
                    }
                    List<WeakReference<View>> b2 = rb.b(a2);
                    this.d.d();
                    this.l.clear();
                    if (b2 != null && !b2.isEmpty()) {
                        for (WeakReference<View> weakReference : b2) {
                            View view = (View) weakReference.get();
                            if (view != null) {
                                this.l.add(view);
                            }
                        }
                    }
                    try {
                        this.t.b();
                        if (b(this.l)) {
                            this.t.d();
                            this.l.clear();
                            cc.a().a();
                            return;
                        }
                        this.t.d();
                        boolean a3 = cc.a().a();
                        if (!this.k && !a3) {
                            if (!this.l.isEmpty()) {
                                a(this.l);
                                this.l.clear();
                                cc.a().a();
                                return;
                            }
                        }
                        String a4 = ui.a("\u0003\u001f\u0011\u0013\u001aV\u001c\u0005U\u0006\u0014\u0003\u0006\u0013\u0011");
                        if (a3) {
                            a4 = bb.a("fQs[qW'Ad@bWi\u0012nA'Ab\\t[s[qW");
                        }
                        if (this.l.isEmpty()) {
                            a4 = ui.a("\u0004\u001a\u0019\u0001V\u0003\u001f\u0010\u0001\u0006V\u0014\u0004\u0010V\u0010\u001b\u0005\u0002\f");
                        }
                        gd.a(1, bb.a("FVc[iU'WjBsK'Ad@bWiAo]s\u0012eWdSrAb\u0012\"A"), a4);
                        f();
                        this.l.clear();
                        cc.a().a();
                    } catch (Throwable th) {
                        this.l.clear();
                        cc.a().a();
                        throw th;
                    }
                } finally {
                    this.d.d();
                }
            } else {
                gd.b(1, ui.a("%\u001e\u001f\u0005\u0006\u001c\u0018\u0012V\u0003\u001f\u0011\u0013\u001aV\u0013\u0004\u0014\u001b\u0010V\u0017\u0013\u0016\u0017\u0000\u0005\u0010V\u0001\u0019\u0000\u0015\u001dV\u001c\u0005U\u0017\u0016\u0002\u001c\u0000\u0010"));
            }
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void j() {
        while (this.w) {
            try {
                long b2 = di.b();
                k();
                h();
                long b3 = ((long) (1000 / this.x)) - (di.b() - b2);
                if (b3 > 0) {
                    Thread.sleep(b3);
                }
            } catch (Exception e2) {
                qe.a(e2, ui.a("0\u0014\u0002\u0014\u001aU\u0013\u0007\u0004\u001a\u0004U\u001f\u001bV\u0003\u001f\u0011\u0013\u001aV\u0001\u001e\u0007\u0013\u0014\u0012["));
                return;
            }
        }
        h();
        gd.b(1, bb.a("thGiV'@bQh@c[iU'As]w\u0012t[`\\f^"));
        l();
    }

    private /* synthetic */ void k() {
        try {
            long b2 = di.b();
            if (b2 - this.v > AdLoader.RETRY_DELAY) {
                if (gd.a(0)) {
                    gd.a(0, ui.a(";\u0010\u001b\u001a\u0004\fV \u0005\u0014\u0011\u0010LUS[D\u0013SP"), Float.valueOf(di.e()));
                }
                this.v = b2;
            }
        } catch (Exception unused) {
        }
    }

    private /* synthetic */ void l() throws Exception {
        gc a2 = a();
        long j2 = (this.D / 1000) + this.j + ((long) (1000 / this.x));
        if (a2 != null) {
            a2.a(j2);
            a(a2);
        }
        while (this.i <= this.x * 2) {
            if (!Thread.currentThread().isInterrupted()) {
                gc a3 = a();
                if (a3 != null) {
                    j2 += (long) (1000 / this.x);
                    a3.a(j2);
                    gd.a(1, ui.a("4\u0012\u0011\u001f\u001b\u0011U\u0010\u001a\u0004\u0016\u0013\u0011V\u0010\u001b\u0005\u0002\fV\u0013\u0004\u0014\u001b\u0010V\u0014\u0002OS\u0011"), Long.valueOf(a3.a()));
                    a(a3);
                } else {
                    gd.b(2, bb.a("scVn\\`\u0012b_b@`WiQ~\u0012eGaTb@'Th@'WiVn\\`\u0012q[cWh\u0012`@fQbTr^kK"));
                    this.C.b();
                }
            } else {
                return;
            }
        }
        if (!Thread.currentThread().isInterrupted()) {
            try {
                this.J.b();
            } catch (Exception e2) {
                qe.a(e2, ui.a("0\u0004\u0007\u0019\u0007V\u0013\u001f\u001b\u001f\u0006\u001e\u001c\u0018\u0012V\u0003\u001f\u0011\u0013\u001aV\u0007\u0013\u0016\u0019\u0007\u0012\u001c\u0018\u0012"));
                e();
            }
            if (!Thread.currentThread().isInterrupted()) {
                g();
            }
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m137a() {
        return this.u;
    }

    /* renamed from: c  reason: collision with other method in class */
    public boolean m138c() {
        return this.k;
    }

    public void d() {
        this.w = false;
        this.D = 0;
        this.i = 0;
        this.m = true;
        bb bbVar = this.p;
        if (bbVar != null) {
            bbVar.a();
        }
        this.f2061a.a();
        this.s.a();
        this.r.a();
        this.K.a();
        this.o.a();
        this.H.a();
        this.c.a();
        this.h.a();
        this.t.a();
        this.d.a();
        oi oiVar = this.C;
        if (oiVar != null) {
            oiVar.a();
            c();
        }
    }

    public boolean e() {
        return this.w;
    }

    private /* synthetic */ void a(gc gcVar) {
        this.s.b();
        b(gcVar);
        this.s.d();
        this.r.b();
        if (this.j == 0) {
            this.j = gcVar.a();
        }
        long a2 = (gcVar.a() - this.j) * 1000;
        ch a3 = gcVar.a();
        long j2 = this.D;
        if (a2 > j2 || j2 == 0) {
            try {
                this.J.a(a3, a2);
                this.i++;
                this.D = a2;
            } catch (Exception e2) {
                qe.a(e2, ui.a(" \u0018\u0014\u0014\u0019\u0013U\u0002\u001aV\u0002\u0004\u001c\u0002\u0010V\u0017\u0003\u0013\u0010\u0010\u0004U\u0002\u001aV\u0003\u001f\u0011\u0013\u001a"));
            }
        }
        gcVar.a((ch) null);
        this.C.a(a3);
        this.r.d();
    }

    public void c() {
        this.u = false;
    }

    /* renamed from: a  reason: collision with other method in class */
    public long m135a() {
        return this.j;
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m136a() {
        this.j = 0;
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    public void a(boolean r17) throws java.lang.Exception {
        /*
            r16 = this;
            r1 = r16
            java.lang.String r2 = "\"\u001c\u001b\u0010\u0019\u0000\u0002U\u0001\u0014\u001f\u0001\u001f\u001b\u0011U\u0010\u001a\u0004U\u0000\u001c\u0012\u0010\u0019U\u0002\u001aV\u0017\u0013U\u0004\u0010\u0017\u0011\u000f["
            java.lang.String r3 = "swBtWbdnVb]W@hQbAt]u\u0012c[bV'SaFb@'DnVb]'Fn_b]rF"
            java.lang.String r4 = "mp4"
            java.lang.String r5 = "mp4_tmp"
            java.lang.String r6 = "\"\u0017\u001c\u0002\u001c\u0018\u0012V\u0013\u0019\u0007V\u0003\u001f\u0011\u0013\u001aV\u0001\u0019U\u0014\u0010V\u0007\u0013\u0014\u0012\f"
            java.lang.String r7 = "f@lWc\u0012uWd]uVn\\`\u0012:\u0012I}"
            r8 = 300(0x12c, float:4.2E-43)
            r9 = 100
            r11 = 100
            r12 = 0
            r13 = 0
            r14 = 1
            boolean r0 = r1.w     // Catch:{ all -> 0x0114 }
            if (r0 != 0) goto L_0x0088
            java.lang.String r0 = "5\u0014\u001a\u0019\u0013\u0011V\u0013\u001f\u001b\u001f\u0006\u001e'\u0013\u0016\u0019\u0007\u0012\u001c\u0018\u0012V\u0002\u001e\u0010\u0018U\u0018\u001a\u0002U\u0004\u0010\u0015\u001a\u0004\u0011\u001f\u001b\u0011["
            java.lang.String r0 = com.appsee.ui.a((java.lang.String) r0)     // Catch:{ all -> 0x0114 }
            com.appsee.qe.b(r0)     // Catch:{ all -> 0x0114 }
            com.appsee.bb r0 = r1.p
            if (r0 == 0) goto L_0x002d
            r0.a()
            r1.p = r13
        L_0x002d:
            r1.u = r14
            r1.w = r12
            java.lang.String r0 = com.appsee.bb.a((java.lang.String) r7)
            com.appsee.gd.b(r14, r0)
        L_0x0038:
            boolean r0 = r1.b
            if (r0 != 0) goto L_0x0057
            if (r12 >= r11) goto L_0x0057
            java.lang.String r0 = com.appsee.ui.a((java.lang.String) r6)
            int r12 = r12 + 1
            com.appsee.gd.b(r14, r0)
            java.lang.Thread.sleep(r9)
            com.appsee.qb r0 = r1.J
            if (r0 == 0) goto L_0x0038
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x0038
            r11 = 300(0x12c, float:4.2E-43)
            goto L_0x0038
        L_0x0057:
            r16.c()
            r16.d()
            if (r17 == 0) goto L_0x0062
            r16.a()
        L_0x0062:
            if (r12 < r11) goto L_0x007e
            java.lang.Thread r0 = r1.g
            if (r0 == 0) goto L_0x006c
            r0.interrupt()
            goto L_0x0073
        L_0x006c:
            java.lang.String r0 = com.appsee.bb.a((java.lang.String) r3)
            com.appsee.qe.a(r13, r0)
        L_0x0073:
            r16.e()
            java.lang.String r0 = com.appsee.ui.a((java.lang.String) r2)
            com.appsee.qe.a((java.lang.String) r0)
            throw r13
        L_0x007e:
            java.lang.String r0 = r1.I
            java.lang.String r2 = r0.replace(r5, r4)
            com.appsee.lg.a((java.lang.String) r0, (java.lang.String) r2)
            return
        L_0x0088:
            com.appsee.jm r0 = r1.f2061a     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.s     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.r     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.o     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.H     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.c     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.h     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.t     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.d     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.jm r0 = r1.K     // Catch:{ all -> 0x0114 }
            r0.c()     // Catch:{ all -> 0x0114 }
            com.appsee.bb r0 = r1.p
            if (r0 == 0) goto L_0x00c3
            r0.a()
            r1.p = r13
        L_0x00c3:
            r1.u = r14
            r1.w = r12
            java.lang.String r0 = com.appsee.bb.a((java.lang.String) r7)
            com.appsee.gd.b(r14, r0)
        L_0x00ce:
            boolean r0 = r1.b
            if (r0 != 0) goto L_0x00ed
            if (r12 >= r11) goto L_0x00ed
            java.lang.String r0 = com.appsee.ui.a((java.lang.String) r6)
            int r12 = r12 + 1
            com.appsee.gd.b(r14, r0)
            java.lang.Thread.sleep(r9)
            com.appsee.qb r0 = r1.J
            if (r0 == 0) goto L_0x00ce
            boolean r0 = r0.a()
            if (r0 == 0) goto L_0x00ce
            r11 = 300(0x12c, float:4.2E-43)
            goto L_0x00ce
        L_0x00ed:
            r16.c()
            r16.d()
            if (r17 == 0) goto L_0x00f8
            r16.a()
        L_0x00f8:
            if (r12 < r11) goto L_0x007e
            java.lang.Thread r0 = r1.g
            if (r0 == 0) goto L_0x0102
            r0.interrupt()
            goto L_0x0109
        L_0x0102:
            java.lang.String r0 = com.appsee.bb.a((java.lang.String) r3)
            com.appsee.qe.a(r13, r0)
        L_0x0109:
            r16.e()
            java.lang.String r0 = com.appsee.ui.a((java.lang.String) r2)
            com.appsee.qe.a((java.lang.String) r0)
            throw r13
        L_0x0114:
            r0 = move-exception
            com.appsee.bb r15 = r1.p
            if (r15 == 0) goto L_0x011e
            r15.a()
            r1.p = r13
        L_0x011e:
            r1.u = r14
            r1.w = r12
            java.lang.String r7 = com.appsee.bb.a((java.lang.String) r7)
            com.appsee.gd.b(r14, r7)
        L_0x0129:
            boolean r7 = r1.b
            if (r7 != 0) goto L_0x0148
            if (r12 >= r11) goto L_0x0148
            java.lang.String r7 = com.appsee.ui.a((java.lang.String) r6)
            int r12 = r12 + 1
            com.appsee.gd.b(r14, r7)
            java.lang.Thread.sleep(r9)
            com.appsee.qb r7 = r1.J
            if (r7 == 0) goto L_0x0129
            boolean r7 = r7.a()
            if (r7 == 0) goto L_0x0129
            r11 = 300(0x12c, float:4.2E-43)
            goto L_0x0129
        L_0x0148:
            r16.c()
            r16.d()
            if (r17 == 0) goto L_0x0153
            r16.a()
        L_0x0153:
            if (r12 < r11) goto L_0x016f
            java.lang.Thread r0 = r1.g
            if (r0 == 0) goto L_0x015d
            r0.interrupt()
            goto L_0x0164
        L_0x015d:
            java.lang.String r0 = com.appsee.bb.a((java.lang.String) r3)
            com.appsee.qe.a(r13, r0)
        L_0x0164:
            r16.e()
            java.lang.String r0 = com.appsee.ui.a((java.lang.String) r2)
            com.appsee.qe.a((java.lang.String) r0)
            throw r13
        L_0x016f:
            java.lang.String r2 = r1.I
            java.lang.String r3 = r2.replace(r5, r4)
            com.appsee.lg.a((java.lang.String) r2, (java.lang.String) r3)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.s.a(boolean):void");
    }

    /* access modifiers changed from: package-private */
    public void b() throws Exception {
        if (this.g != null) {
            throw new Exception(bb.a("FBwAbWQ[cWhbu]dWtAh@'Fo@bSc\u0012f^uWfV~\u0012bJnAsA'Eo[kW'FuKn\\`\u0012s]'BuWwSuW'Th@'DnVb]'@bQh@c[iU"));
        } else if (this.w) {
            qe.b(ui.a("5\u0014\u001a\u0019\u0013\u0011V\u0005\u0004\u0010\u0006\u0014\u0004\u00107\u001b\u0012&\u0002\u0014\u0004\u0001$\u0010\u0015\u001a\u0004\u0011\u001f\u001b\u0011U\u0001\u001d\u0013\u001bV\u0014\u001a\u0007\u0013\u0014\u0012\fV\u0007\u0013\u0016\u0019\u0007\u0012\u001c\u0018\u0012"));
        } else {
            if (this.n == null) {
                this.n = di.b().a(true);
            }
            this.z = new Dimension(pg.h().k(), pg.h().E());
            this.e = pg.h().g() * ByteConstants.KB;
            this.x = (int) pg.h().M();
            this.E.setColor(pg.h().A());
            d();
            a();
            oi oiVar = this.C;
            if (oiVar == null) {
                this.C = new oi(2, this.z.c(), this.z.a());
            } else {
                oiVar.a(2, this.z.c(), this.z.a());
            }
            synchronized (this.F) {
                this.F.clear();
            }
            String a2 = bb.a("\u0017t\u001c\"A");
            Object[] objArr = new Object[2];
            objArr[0] = pg.h().m();
            objArr[1] = pg.h().S() ? "mp4_tmp" : "mp4";
            this.I = lg.a(String.format(a2, objArr)).getAbsolutePath();
            gd.a(1, ui.a("#\u001f\u0011\u0013\u001aV\u0005\u0017\u0001\u001eUKUS\u0006"), this.I);
            CountDownLatch countDownLatch = new CountDownLatch(1);
            this.g = new Thread(new p(this, countDownLatch), bb.a("FBwAbWQ[cWhbu]dWtAh@"));
            this.w = true;
            this.b = false;
            this.g.start();
            countDownLatch.await();
            Exception exc = this.y;
            if (exc == null) {
                this.p = new bb(new f(this), 1000 / this.x);
                this.p.b();
                f();
                return;
            }
            throw exc;
        }
    }

    private /* synthetic */ void b(gc gcVar) {
        a(gcVar, true);
    }

    @TargetApi(11)
    private /* synthetic */ void a(View view, Map<View, Float> map) {
        List<Object> b2 = pg.h().b("hide");
        if (b2 != null && !b2.isEmpty()) {
            for (T t2 : rb.c(view, b2)) {
                if (t2.isShown()) {
                    Float f2 = null;
                    if (pg.h().e()) {
                        f2 = Float.valueOf(t2.getAlpha());
                        t2.setAlpha(0.0f);
                    } else {
                        t2.setVisibility(4);
                    }
                    map.put(t2, f2);
                }
            }
        }
    }

    @TargetApi(14)
    private /* synthetic */ boolean a(Object[] objArr) throws Exception {
        boolean z2;
        if (!(objArr == null || objArr.length == 0)) {
            int length = objArr.length;
            int i2 = 0;
            while (true) {
                if (i2 >= length) {
                    z2 = false;
                    break;
                }
                View view = objArr[i2];
                if (rb.a(view, (Class<?>) SurfaceView.class) || (Build.VERSION.SDK_INT >= 14 && rb.a(view, (Class<?>) TextureView.class))) {
                    z2 = true;
                } else {
                    i2++;
                }
            }
            if (z2) {
                if (this.f == 0) {
                    this.f = di.b();
                }
                if (di.b() - this.f < 750) {
                    return true;
                }
            } else {
                this.f = 0;
            }
        }
        return false;
    }

    /*  JADX ERROR: StackOverflow in pass: MarkFinallyVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:47)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:81)
        */
    @android.annotation.TargetApi(11)
    private /* synthetic */ void a(java.util.List<android.view.View> r10) throws java.lang.Exception {
        /*
            r9 = this;
            if (r10 == 0) goto L_0x01b0
            boolean r0 = r10.isEmpty()
            if (r0 == 0) goto L_0x000a
            goto L_0x01b0
        L_0x000a:
            com.appsee.jm r0 = r9.f2061a
            r0.b()
            com.appsee.jm r0 = r9.H
            r0.b()
            com.appsee.cc r0 = com.appsee.cc.a()
            java.util.List<android.view.View> r1 = r9.l
            r0.a((java.util.List<android.view.View>) r1)
            com.appsee.jm r0 = r9.H
            r0.d()
            com.appsee.gc r0 = new com.appsee.gc
            r0.<init>()
            long r1 = com.appsee.di.b()
            r0.a((long) r1)
            com.appsee.li r1 = com.appsee.di.b()
            com.appsee.zn r1 = r1.a()
            r0.a((com.appsee.zn) r1)
            com.appsee.ch r1 = r9.c()
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0046
            r0.a((com.appsee.ch) r1)
            r1 = 0
            goto L_0x0050
        L_0x0046:
            com.appsee.oi r1 = r9.C
            com.appsee.ch r1 = r1.a()
            r0.a((com.appsee.ch) r1)
            r1 = 1
        L_0x0050:
            com.appsee.ch r4 = r0.a()
            if (r4 != 0) goto L_0x0057
            return
        L_0x0057:
            com.appsee.ch r4 = r0.a()
            boolean r4 = r4.b()
            if (r4 != 0) goto L_0x006e
            java.lang.String r1 = "qf\\i]s\u0012k]dY'PrTaWu\u0012a]u\u0012tQuWb\\tZhF'@b\\cWu[iU"
            java.lang.String r1 = com.appsee.bb.a((java.lang.String) r1)
            com.appsee.gd.b(r2, r1)
            r9.a((com.appsee.gc) r0, (java.util.List<android.view.View>) r10)
            return
        L_0x006e:
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            com.appsee.ch r5 = r0.a()     // Catch:{ Exception -> 0x00f2 }
            android.graphics.Canvas r5 = r5.b()     // Catch:{ Exception -> 0x00f2 }
            java.util.Iterator r6 = r10.iterator()     // Catch:{ Exception -> 0x00f2 }
        L_0x007f:
            boolean r7 = r6.hasNext()     // Catch:{ Exception -> 0x00f2 }
            if (r7 == 0) goto L_0x00a4
            java.lang.Object r7 = r6.next()     // Catch:{ Exception -> 0x00f2 }
            android.view.View r7 = (android.view.View) r7     // Catch:{ Exception -> 0x00f2 }
            r9.a((android.view.View) r7, (java.util.Map<android.view.View, java.lang.Float>) r4)     // Catch:{ Exception -> 0x00f2 }
            if (r1 == 0) goto L_0x007f
            r9.a((com.appsee.gc) r0, (android.graphics.Canvas) r5, (android.view.View) r7)     // Catch:{ Exception -> 0x00f2 }
            com.appsee.pg r8 = com.appsee.pg.h()     // Catch:{ Exception -> 0x00f2 }
            boolean r8 = r8.g()     // Catch:{ Exception -> 0x00f2 }
            if (r8 == 0) goto L_0x007f
            r9.a((java.util.List<android.view.View>) r10, (android.view.View) r7, (com.appsee.gc) r0)     // Catch:{ Exception -> 0x00f2 }
            r9.a((com.appsee.gc) r0, (boolean) r3)     // Catch:{ Exception -> 0x00f2 }
            goto L_0x007f
        L_0x00a4:
            com.appsee.ch r1 = r0.a()
            r1.d()
            java.util.Set r1 = r4.entrySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x00b3:
            boolean r4 = r1.hasNext()
            if (r4 == 0) goto L_0x0158
            java.lang.Object r4 = r1.next()
            java.util.Map$Entry r4 = (java.util.Map.Entry) r4
            com.appsee.pg r5 = com.appsee.pg.h()
            boolean r5 = r5.e()
            if (r5 == 0) goto L_0x00dd
            java.lang.Object r5 = r4.getKey()
            android.view.View r5 = (android.view.View) r5
            java.lang.Object r6 = r4.getValue()
            java.lang.Float r6 = (java.lang.Float) r6
            float r6 = r6.floatValue()
            r5.setAlpha(r6)
            goto L_0x00e6
        L_0x00dd:
            java.lang.Object r5 = r4.getKey()
            android.view.View r5 = (android.view.View) r5
            r5.setVisibility(r3)
        L_0x00e6:
            java.lang.Object r4 = r4.getKey()
            android.view.View r4 = (android.view.View) r4
            r4.requestLayout()
            goto L_0x00b3
        L_0x00f0:
            r10 = move-exception
            goto L_0x0163
        L_0x00f2:
            r1 = move-exception
            com.appsee.ch r2 = r0.a()     // Catch:{ all -> 0x00f0 }
            if (r2 == 0) goto L_0x0102
            com.appsee.oi r2 = r9.C     // Catch:{ all -> 0x00f0 }
            com.appsee.ch r5 = r0.a()     // Catch:{ all -> 0x00f0 }
            r2.a((com.appsee.ch) r5)     // Catch:{ all -> 0x00f0 }
        L_0x0102:
            java.lang.String r2 = "0\u0004\u0007\u0019\u0007V\u0011\u0004\u0014\u0001\u001c\u0018\u0012V\u0003\u001f\u0011\u0013\u001aV\u0013\u0004\u0014\u001b\u0010"
            java.lang.String r2 = com.appsee.ui.a((java.lang.String) r2)     // Catch:{ all -> 0x00f0 }
            com.appsee.qe.a(r1, r2)     // Catch:{ all -> 0x00f0 }
            com.appsee.ch r1 = r0.a()
            r1.d()
            java.util.Set r1 = r4.entrySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x011a:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x0157
            java.lang.Object r2 = r1.next()
            java.util.Map$Entry r2 = (java.util.Map.Entry) r2
            com.appsee.pg r4 = com.appsee.pg.h()
            boolean r4 = r4.e()
            if (r4 == 0) goto L_0x0144
            java.lang.Object r4 = r2.getKey()
            android.view.View r4 = (android.view.View) r4
            java.lang.Object r5 = r2.getValue()
            java.lang.Float r5 = (java.lang.Float) r5
            float r5 = r5.floatValue()
            r4.setAlpha(r5)
            goto L_0x014d
        L_0x0144:
            java.lang.Object r4 = r2.getKey()
            android.view.View r4 = (android.view.View) r4
            r4.setVisibility(r3)
        L_0x014d:
            java.lang.Object r2 = r2.getKey()
            android.view.View r2 = (android.view.View) r2
            r2.requestLayout()
            goto L_0x011a
        L_0x0157:
            r2 = 0
        L_0x0158:
            if (r2 == 0) goto L_0x015d
            r9.a((com.appsee.gc) r0, (java.util.List<android.view.View>) r10)
        L_0x015d:
            com.appsee.jm r10 = r9.f2061a
            r10.d()
            return
        L_0x0163:
            com.appsee.ch r0 = r0.a()
            r0.d()
            java.util.Set r0 = r4.entrySet()
            java.util.Iterator r0 = r0.iterator()
        L_0x0172:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x01af
            java.lang.Object r1 = r0.next()
            java.util.Map$Entry r1 = (java.util.Map.Entry) r1
            com.appsee.pg r2 = com.appsee.pg.h()
            boolean r2 = r2.e()
            if (r2 == 0) goto L_0x019c
            java.lang.Object r2 = r1.getKey()
            android.view.View r2 = (android.view.View) r2
            java.lang.Object r4 = r1.getValue()
            java.lang.Float r4 = (java.lang.Float) r4
            float r4 = r4.floatValue()
            r2.setAlpha(r4)
            goto L_0x01a5
        L_0x019c:
            java.lang.Object r2 = r1.getKey()
            android.view.View r2 = (android.view.View) r2
            r2.setVisibility(r3)
        L_0x01a5:
            java.lang.Object r1 = r1.getKey()
            android.view.View r1 = (android.view.View) r1
            r1.requestLayout()
            goto L_0x0172
        L_0x01af:
            throw r10
        L_0x01b0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.s.a(java.util.List):void");
    }

    public static synchronized s a() {
        s sVar;
        synchronized (s.class) {
            if (L == null) {
                L = new s();
            }
            sVar = L;
        }
        return sVar;
    }

    private /* synthetic */ void a(List<View> list, View view, gc gcVar) throws Exception {
        this.G.clear();
        try {
            for (Map.Entry entry : cc.a().a().entrySet()) {
                if (view == null || view.equals(entry.getKey())) {
                    for (Map.Entry entry2 : ((HashMap) entry.getValue()).entrySet()) {
                        View view2 = entry2.getKey() != null ? (View) ((WeakReference) entry2.getKey()).get() : null;
                        Rect rect = (Rect) entry2.getValue();
                        Rect e2 = rb.e(view2);
                        if (view2 != null && !e2.equals(rect)) {
                            if (!pg.h().N()) {
                                this.G.put(view2, new Rect(Math.min(rect.left, e2.left), Math.min(rect.top, e2.top), Math.max(rect.right, e2.right), Math.max(rect.bottom, e2.bottom)));
                            }
                        }
                        this.G.put(view2, entry2.getValue());
                    }
                }
            }
            if (pg.h().d()) {
                cc.a().a(list);
                HashSet hashSet = new HashSet();
                for (Map.Entry entry3 : cc.a().a().entrySet()) {
                    if (view == null || view.equals(entry3.getKey())) {
                        Iterator it2 = ((HashMap) entry3.getValue()).entrySet().iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            Map.Entry entry4 = (Map.Entry) it2.next();
                            View view3 = entry4.getKey() != null ? (View) ((WeakReference) entry4.getKey()).get() : null;
                            if (view3 != null) {
                                hashSet.add(view3);
                                if (!this.G.containsKey(view3)) {
                                    this.G.put((Object) null, new Rect(0, 0, this.z.c(), this.z.a()));
                                    break;
                                }
                            }
                        }
                    }
                }
                Iterator<Map.Entry<View, Rect>> it3 = this.G.entrySet().iterator();
                while (true) {
                    if (it3.hasNext()) {
                        if (!hashSet.contains(it3.next().getKey())) {
                            this.G.put((Object) null, new Rect(0, 0, this.z.c(), this.z.a()));
                            break;
                        }
                    } else {
                        break;
                    }
                }
            }
            gcVar.a(this.G);
        } finally {
            this.G.clear();
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: android.view.View} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: android.webkit.WebView} */
    /* JADX WARNING: Multi-variable type inference failed */
    @android.annotation.TargetApi(21)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(com.appsee.gc r9, android.graphics.Canvas r10, android.view.View r11) {
        /*
            r8 = this;
            int r0 = r10.save()
            int r1 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x009e }
            r2 = 21
            r3 = 0
            if (r1 < r2) goto L_0x0026
            com.appsee.pg r1 = com.appsee.pg.h()     // Catch:{ all -> 0x009e }
            boolean r1 = r1.q()     // Catch:{ all -> 0x009e }
            if (r1 != 0) goto L_0x001f
            com.appsee.pg r1 = com.appsee.pg.h()     // Catch:{ all -> 0x009e }
            boolean r1 = r1.s()     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x0026
        L_0x001f:
            java.lang.Class<android.webkit.WebView> r1 = android.webkit.WebView.class
            java.util.List r1 = com.appsee.rb.a((android.view.View) r11, (java.lang.Class<?>) r1)     // Catch:{ all -> 0x009e }
            goto L_0x0027
        L_0x0026:
            r1 = r3
        L_0x0027:
            if (r1 == 0) goto L_0x0086
            boolean r2 = r1.isEmpty()     // Catch:{ all -> 0x009e }
            if (r2 != 0) goto L_0x0086
            r2 = -1
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x009e }
            r2 = r3
            r4 = -1
        L_0x0036:
            boolean r5 = r1.hasNext()     // Catch:{ all -> 0x009e }
            if (r5 == 0) goto L_0x0065
            java.lang.Object r5 = r1.next()     // Catch:{ all -> 0x009e }
            android.view.View r5 = (android.view.View) r5     // Catch:{ all -> 0x009e }
            boolean r6 = r5.isShown()     // Catch:{ all -> 0x009e }
            if (r6 != 0) goto L_0x0049
            goto L_0x0036
        L_0x0049:
            if (r2 == 0) goto L_0x0057
            int r6 = r5.getWidth()     // Catch:{ all -> 0x009e }
            int r7 = r5.getHeight()     // Catch:{ all -> 0x009e }
            int r6 = r6 * r7
            if (r6 <= r4) goto L_0x0036
        L_0x0057:
            r2 = r5
            android.webkit.WebView r2 = (android.webkit.WebView) r2     // Catch:{ all -> 0x009e }
            int r4 = r5.getHeight()     // Catch:{ all -> 0x009e }
            int r5 = r5.getWidth()     // Catch:{ all -> 0x009e }
            int r4 = r4 * r5
            goto L_0x0036
        L_0x0065:
            if (r2 == 0) goto L_0x0086
            com.appsee.pg r1 = com.appsee.pg.h()     // Catch:{ all -> 0x009e }
            boolean r1 = r1.s()     // Catch:{ all -> 0x009e }
            if (r1 != 0) goto L_0x0081
            com.appsee.pg r1 = com.appsee.pg.h()     // Catch:{ all -> 0x009e }
            boolean r1 = r1.q()     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x0086
            boolean r1 = com.appsee.rb.a((android.webkit.WebView) r2)     // Catch:{ all -> 0x009e }
            if (r1 == 0) goto L_0x0086
        L_0x0081:
            android.graphics.Picture r3 = r2.capturePicture()     // Catch:{ all -> 0x009e }
            r11 = r2
        L_0x0086:
            android.graphics.Rect r1 = com.appsee.rb.e((android.view.View) r11)     // Catch:{ all -> 0x009e }
            com.appsee.zn r9 = r9.a()     // Catch:{ all -> 0x009e }
            r8.a((android.graphics.Rect) r1, (com.appsee.zn) r9, (android.graphics.Canvas) r10)     // Catch:{ all -> 0x009e }
            if (r3 == 0) goto L_0x0097
            r3.draw(r10)     // Catch:{ all -> 0x009e }
            goto L_0x009a
        L_0x0097:
            r11.draw(r10)     // Catch:{ all -> 0x009e }
        L_0x009a:
            r10.restoreToCount(r0)
            return
        L_0x009e:
            r9 = move-exception
            r10.restoreToCount(r0)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.s.a(com.appsee.gc, android.graphics.Canvas, android.view.View):void");
    }

    private /* synthetic */ void a(gc gcVar, boolean z2) {
        ch a2;
        Canvas b2;
        int save;
        if (gcVar.a() != null && !gcVar.a().isEmpty() && (a2 = gcVar.a()) != null) {
            if (z2) {
                try {
                    a2.c();
                } catch (Exception e2) {
                    qe.a(e2, bb.a("qf\\i]s\u0012c@fE'Ab\\t[s[qW'@bQsA"));
                    return;
                }
            }
            try {
                b2 = a2.b();
                for (Rect rect : gcVar.a()) {
                    save = b2.save();
                    a(rect, gcVar.a(), b2);
                    this.q.set(0, 0, rect.width(), rect.height());
                    b2.drawRect(this.q, this.E);
                    b2.restoreToCount(save);
                }
                gcVar.a().clear();
                if (z2) {
                    a2.d();
                }
            } catch (Throwable th) {
                if (z2) {
                    a2.d();
                }
                throw th;
            }
        }
    }

    private /* synthetic */ void a(Rect rect, zn znVar, Canvas canvas) {
        int i2 = y.f2087a[znVar.ordinal()];
        int i3 = i2 != 1 ? i2 != 2 ? i2 != 3 ? 0 : RotationOptions.ROTATE_180 : RotationOptions.ROTATE_270 : 90;
        canvas.scale(((float) this.z.c()) / ((float) this.n.c()), ((float) this.z.a()) / ((float) this.n.a()));
        if (i3 > 0) {
            canvas.rotate((float) i3, ((float) rect.width()) / 2.0f, ((float) rect.height()) / 2.0f);
            if (i3 == 90) {
                float width = ((float) (rect.width() - rect.height())) / 2.0f;
                canvas.translate(width, width);
                canvas.translate((float) rect.left, (float) (rect.bottom - this.n.c()));
            }
            if (i3 == 270) {
                float width2 = ((float) (rect.width() - rect.height())) / -2.0f;
                canvas.translate(width2, width2);
                canvas.translate((float) (rect.right - this.n.a()), (float) rect.top);
            }
            if (i3 == 180) {
                canvas.translate((float) (rect.right - this.n.c()), (float) (rect.bottom - this.n.a()));
                return;
            }
            return;
        }
        canvas.translate((float) rect.left, (float) rect.top);
    }

    private /* synthetic */ void a(gc gcVar, List<View> list) throws Exception {
        if (gcVar != null) {
            if (!pg.h().g()) {
                a(list, (View) null, gcVar);
            }
            synchronized (this.F) {
                if (this.F.size() > 10) {
                    qe.b(bb.a("N_fUb\u0012vGbGb\u0012s]h\u0012e[`\u001e'Al[wBn\\`\u001c"));
                } else {
                    this.F.add(gcVar);
                }
            }
        }
    }
}
