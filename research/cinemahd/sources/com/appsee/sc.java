package com.appsee;

import java.util.ArrayList;
import java.util.List;

class sc {
    private static sc c;

    /* renamed from: a  reason: collision with root package name */
    private List<mc> f2063a = new ArrayList();
    private List<ac> b = new ArrayList();

    private /* synthetic */ sc() {
    }

    public Object[] a(boolean z) {
        List<mc> list = this.f2063a;
        Object[] objArr = {list, this.b};
        if (!z) {
            return objArr;
        }
        synchronized (list) {
            this.f2063a = new ArrayList();
        }
        synchronized (this.b) {
            this.b = new ArrayList();
        }
        return objArr;
    }

    private /* synthetic */ boolean a(wc wcVar) {
        return wcVar == wc.e || wcVar == wc.b;
    }

    public void a(String str, wc wcVar, boolean z) throws Exception {
        if (str == null) {
            str = "";
        }
        long b2 = xd.b().b();
        long d = xd.b().d();
        if (z && !a(wcVar)) {
            AppseeScreenDetectedInfo appseeScreenDetectedInfo = new AppseeScreenDetectedInfo(str);
            oj.a((c) new fc(this, appseeScreenDetectedInfo), false);
            if (yb.a(appseeScreenDetectedInfo.a())) {
                gd.b(1, zo.a("h\u0002R\u0019K\u0000U\u000e\u001b\b_\rR\u0007\\IH\nI\f^\u0007\u001b\u000b^\nZ\u001cH\f\u001b\u001aX\u001b^\fUIU\bV\f\u001b\u0000HI^\u0004K\u001dBH"));
                return;
            } else if (!str.equals(appseeScreenDetectedInfo.a())) {
                str = appseeScreenDetectedInfo.a();
                z = false;
            }
        }
        mc a2 = a();
        if (a2 == null || !yb.a(a2.a(), str)) {
            mc mcVar = new mc(str, b2, z, wcVar);
            if (mcVar.a() < 0) {
                mcVar.a(0);
                if (!z) {
                    mcVar.b(d);
                }
            }
            if (a2 != null && a2.a() == mcVar.a() && a2.a() != 0 && !a(a2.a())) {
                gd.b(1, zo.a("[\u001b\u001aX\u001b^\fU\u001a\u0017IH\bV\f\u001b\u001dR\u0004^E\u001b\u0005Z\u001aOIR\u001a\u001b\u001eR\u0007U\u0000U\u000e"));
                synchronized (this.f2063a) {
                    this.f2063a.remove(a2);
                }
            }
            gd.a(1, ui.a("&\u0002\u0014\u0004\u0001\u001f\u001b\u0011U\u0005\u0016\u0004\u0010\u0013\u001bV\u0010\u0000\u0010\u0018\u0001LUS\u0006V\u0001\u000f\u0005\u0013UKUS\u0006V\u0014\u0002US\u0011"), str, wcVar.toString(), Long.valueOf(mcVar.a()));
            synchronized (this.f2063a) {
                this.f2063a.add(mcVar);
            }
            pg.h().a(str);
            return;
        }
        gd.a(1, ui.a("<\u0011\u001b\u0019\u0007\u001f\u001b\u0011U\u0005\u0014\u001b\u0010V\u0006\u0015\u0007\u0013\u0010\u0018U\u0002\u0002\u001f\u0016\u0013U[US\u0006"), str);
    }

    public void a(ac acVar, String str) {
        synchronized (this.b) {
            this.b.add(acVar);
        }
        gd.a(1, ui.a("7\u0011\u0012\u0010\u0012U\u0018\u0010\u0001U\u0006\u001a\u0006\u0000\u0006U\u0012\u0014\u0002\u0014LUS\u0006"), acVar.a());
    }

    public static synchronized sc a() {
        sc scVar;
        synchronized (sc.class) {
            if (c == null) {
                c = new sc();
            }
            scVar = c;
        }
        return scVar;
    }

    /* renamed from: a  reason: collision with other method in class */
    public mc m139a() {
        synchronized (this.f2063a) {
            if (this.f2063a.isEmpty()) {
                return null;
            }
            mc mcVar = this.f2063a.get(this.f2063a.size() - 1);
            return mcVar;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m140a() {
        synchronized (this.f2063a) {
            this.f2063a.clear();
        }
        synchronized (this.b) {
            this.b.clear();
        }
    }
}
