package com.appsee;

import android.view.MotionEvent;
import java.util.HashMap;

class sd {

    /* renamed from: a  reason: collision with root package name */
    private g f2064a;
    int b = 30;
    private HashMap<Integer, bd> c = new HashMap<>();
    short d = 1500;

    public sd(g gVar) {
        this.f2064a = gVar;
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 'y');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'S');
        }
        return new String(cArr);
    }

    private /* synthetic */ void a(int i, float f, float f2, long j, MotionEvent motionEvent) {
        bd bdVar = this.c.get(Integer.valueOf(i));
        if (this.f2064a != null && di.a(Math.abs(f - bdVar.f1978a)) < ((float) this.b) && di.a(Math.abs(f2 - bdVar.b)) < ((float) this.b) && j - bdVar.c >= ((long) this.d)) {
            this.f2064a.a(motionEvent);
        }
    }

    private /* synthetic */ void a(int i, float f, float f2, long j) {
        this.c.put(Integer.valueOf(i), new bd(this, f, f2, j));
    }

    public boolean a(MotionEvent motionEvent) {
        int actionIndex = motionEvent.getActionIndex();
        int pointerId = motionEvent.getPointerId(actionIndex);
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            a(pointerId, motionEvent.getX(actionIndex), motionEvent.getY(actionIndex), motionEvent.getEventTime());
            return false;
        } else if (actionMasked == 1) {
            a(pointerId, motionEvent.getX(actionIndex), motionEvent.getY(actionIndex), motionEvent.getEventTime(), motionEvent);
            return false;
        } else if (actionMasked == 2) {
            return false;
        } else {
            if (actionMasked == 5) {
                a(pointerId, motionEvent.getX(actionIndex), motionEvent.getY(actionIndex), motionEvent.getEventTime());
                return false;
            } else if (actionMasked != 6) {
                return false;
            } else {
                a(pointerId, motionEvent.getX(actionIndex), motionEvent.getY(actionIndex), motionEvent.getEventTime(), motionEvent);
                return false;
            }
        }
    }
}
