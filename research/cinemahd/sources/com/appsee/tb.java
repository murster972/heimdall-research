package com.appsee;

import org.json.JSONException;
import org.json.JSONObject;

class tb extends JSONObject {
    tb(short s, short s2, short s3, long j, eb ebVar, boolean z) throws JSONException {
        put(md.a("\u001e"), s);
        put(qb.a("\u0006"), ebVar.ordinal());
        put(md.a("\u0001"), s2);
        put(qb.a("\u001a"), s3);
        put(md.a("\r"), j);
        if (z) {
            put(qb.a("\b"), 1);
        }
    }

    public long a() {
        try {
            return getLong(md.a("\r"));
        } catch (JSONException unused) {
            return -1;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public eb m141a() {
        try {
            return eb.values()[getInt(qb.a("\u0006"))];
        } catch (JSONException unused) {
            return eb.d;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public short m142a() {
        try {
            return (short) getInt(qb.a("\u0004"));
        } catch (JSONException unused) {
            return -1;
        }
    }
}
