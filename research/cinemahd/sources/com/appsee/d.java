package com.appsee;

import android.view.MotionEvent;
import android.view.View;

class d implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f1985a;

    private /* synthetic */ d(r rVar) {
        this.f1985a = rVar;
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        oj.b((z) new m(this, motionEvent));
        if (r.a(this.f1985a) != null) {
            return r.a(this.f1985a).onTouch(view, motionEvent);
        }
        return false;
    }

    /* synthetic */ d(r rVar, e eVar) {
        this(rVar);
    }
}
