package com.appsee;

import java.util.concurrent.CountDownLatch;

class p implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ s f2048a;
    final /* synthetic */ CountDownLatch b;

    p(s sVar, CountDownLatch countDownLatch) {
        this.f2048a = sVar;
        this.b = countDownLatch;
    }

    public void run() {
        try {
            qb unused = this.f2048a.J = new qb();
            s.a(this.f2048a).a(s.a(this.f2048a).c(), s.a(this.f2048a).a(), s.a(this.f2048a), s.b(this.f2048a), s.a(this.f2048a), pg.h().h().contains(tg.d));
            this.b.countDown();
            this.f2048a.j();
            Thread unused2 = this.f2048a.g = null;
        } catch (Exception e) {
            s.a(this.f2048a);
            Exception unused3 = this.f2048a.y = e;
            Thread unused4 = this.f2048a.g = null;
            this.b.countDown();
        } catch (Throwable th) {
            this.b.countDown();
            throw th;
        }
    }
}
