package com.appsee;

import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class el {
    private static el b;

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Boolean> f1992a;

    private /* synthetic */ el() {
    }

    public static JSONObject a(Map<String, Boolean> map) {
        if (map == null || map.isEmpty()) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        try {
            for (String next : map.keySet()) {
                jSONObject.put(next, map.get(next));
            }
        } catch (JSONException e) {
            qe.a(e, bb.a("wu@h@'Eo[kW'Ab@nSk[}[iU'WFb@iSk\u0012tVlA"));
        }
        if (jSONObject.length() == 0) {
            return null;
        }
        return jSONObject;
    }

    public static synchronized el a() {
        el elVar;
        synchronized (el.class) {
            if (b == null) {
                b = new el();
            }
            elVar = b;
        }
        return elVar;
    }

    public void a(JSONArray jSONArray) {
        if (jSONArray != null) {
            this.f1992a = new HashMap();
            for (int i = 0; i < jSONArray.length(); i++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    if (jSONObject != null) {
                        this.f1992a.put((String) jSONObject.get(bb.a("acYISjW")), Boolean.valueOf(a(jSONObject.getJSONArray(lf.a("Ejguucu")))));
                    }
                } catch (Exception e) {
                    qe.a(e, lf.a("Cttit&bcrceroha&c~rcthgj&ubm"));
                }
            }
        }
    }

    public Map<String, Boolean> a(boolean z) {
        Map<String, Boolean> map = this.f1992a;
        if (z) {
            this.f1992a = null;
        }
        return map;
    }
}
