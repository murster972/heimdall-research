package com.appsee;

import android.content.SharedPreferences;
import java.io.IOException;
import java.util.Set;
import org.json.JSONObject;

class wg implements SharedPreferences.Editor {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gl f2079a;

    private /* synthetic */ wg(gl glVar) {
        this.f2079a = glVar;
    }

    private /* synthetic */ boolean a() {
        try {
            synchronized (this.f2079a.b) {
                lg.a(gl.c, yb.a(new JSONObject(this.f2079a.b).toString()));
            }
            return true;
        } catch (IOException e) {
            qe.a(e, wd.e("\u00076(;$3a#.w2672a\u00161'22$w2? %$3a'32'232/4$$"));
            return false;
        }
    }

    public void apply() {
        a();
    }

    public SharedPreferences.Editor clear() {
        this.f2079a.b.clear();
        return this;
    }

    public boolean commit() {
        return a();
    }

    public SharedPreferences.Editor putBoolean(String str, boolean z) {
        this.f2079a.b.put(str, Boolean.valueOf(z));
        return this;
    }

    public SharedPreferences.Editor putFloat(String str, float f) {
        this.f2079a.b.put(str, Float.valueOf(f));
        return this;
    }

    public SharedPreferences.Editor putInt(String str, int i) {
        this.f2079a.b.put(str, Integer.valueOf(i));
        return this;
    }

    public SharedPreferences.Editor putLong(String str, long j) {
        this.f2079a.b.put(str, Long.valueOf(j));
        return this;
    }

    public SharedPreferences.Editor putString(String str, String str2) {
        if (str2 == null) {
            remove(str);
            return this;
        }
        this.f2079a.b.put(str, str2);
        return this;
    }

    public SharedPreferences.Editor putStringSet(String str, Set<String> set) {
        if (set == null) {
            remove(str);
            return this;
        }
        this.f2079a.b.put(str, set);
        return this;
    }

    public SharedPreferences.Editor remove(String str) {
        this.f2079a.b.remove(str);
        return this;
    }
}
