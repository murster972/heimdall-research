package com.appsee;

import android.graphics.Rect;
import android.view.View;
import java.util.Comparator;

class jb implements Comparator<View> {
    jb() {
    }

    /* renamed from: a */
    public int compare(View view, View view2) {
        Rect e = rb.e(view);
        Rect e2 = rb.e(view2);
        int i = e.left;
        int i2 = e2.left;
        return i == i2 ? e.top > e2.top ? 1 : -1 : i > i2 ? 1 : -1;
    }
}
