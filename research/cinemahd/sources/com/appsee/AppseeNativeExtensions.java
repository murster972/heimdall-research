package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.os.Build;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

class AppseeNativeExtensions {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1965a = false;
    private static boolean b = false;
    private static final List<String> c = new ArrayList();

    AppseeNativeExtensions() {
    }

    static void a(int i, int i2, Bitmap bitmap, long j, boolean z) {
        if (b) {
            convertAndEncode(i, i2, bitmap, j, z);
        }
    }

    private static /* synthetic */ String b() {
        int i = Build.VERSION.SDK_INT;
        if (i >= 9 && i < 11) {
            return cc.a("oF^EKSx_JSAs@UARKD\u0003\u000f");
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 < 14 || i2 >= 16) {
            return null;
        }
        return wd.e("\u00161'22$\u0001(3$8\u00049\"8%23zpc");
    }

    @TargetApi(21)
    private static /* synthetic */ List<String> c() {
        if (c.isEmpty()) {
            if (Build.VERSION.SDK_INT < 21) {
                if (!yb.a(Build.CPU_ABI)) {
                    c.add(Build.CPU_ABI);
                }
                if (!yb.a(Build.CPU_ABI2)) {
                    c.add(Build.CPU_ABI2);
                }
            } else {
                String[] strArr = Build.SUPPORTED_ABIS;
                int length = strArr.length;
                int i = 0;
                while (i < length) {
                    String str = strArr[i];
                    i++;
                    c.add(str);
                }
            }
        }
        return c;
    }

    private static native /* synthetic */ String changeEGLSwapBehavior(boolean z);

    private static native /* synthetic */ void convertAndEncode(int i, int i2, Bitmap bitmap, long j, boolean z);

    private static native /* synthetic */ void convertToYuv(int i, int i2, Bitmap bitmap, ByteBuffer byteBuffer, boolean z, boolean z2, boolean z3);

    private static native /* synthetic */ String copyEglImageBufferToBitmap(Bitmap bitmap, boolean z, boolean z2);

    public static native void crashNative();

    private static native /* synthetic */ String createAndAttachEglImage(int i, int i2, int i3);

    static void d() {
        if (f1965a) {
            oj.a((z) new kn());
        }
    }

    private static native /* synthetic */ String disposeEglImage();

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r0 = android.os.Build.SUPPORTED_64_BIT_ABIS;
     */
    @android.annotation.TargetApi(21)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ boolean e() {
        /*
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 21
            if (r0 < r1) goto L_0x000f
            java.lang.String[] r0 = android.os.Build.SUPPORTED_64_BIT_ABIS
            if (r0 == 0) goto L_0x000f
            int r0 = r0.length
            if (r0 <= 0) goto L_0x000f
            r0 = 1
            return r0
        L_0x000f:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.AppseeNativeExtensions.e():boolean");
    }

    static void f() {
        if (f1965a) {
            oj.a((z) new eh());
        }
    }

    private static native /* synthetic */ boolean finishEncoding();

    static boolean h() {
        try {
            if (!f1965a) {
                gd.a(1, cc.a("mC\\DKXZ\u0016mf{\u0016otg\u0016oDM^GBKUZC\\S\u0014\u0016\u000bE"), yb.a((Iterable) c(), wd.e("{")));
                if (b(cc.a("W\\[")) || b(wd.e(",>1$")) || b(cc.a("\u0016\u0000"))) {
                    a(cc.a("oF^EKS`WZ_XSkNZS@EGY@E"));
                    f1965a = true;
                    d();
                } else {
                    gd.a(1, wd.e("\u0012<('1>/0a;.6%>/0a61'22$w/65>72a29#$92>.9a;(5a342a#.w(976->%w\u0002\u0007\u0014w\u0000\u0015\bw %\"?(#$45\"32"));
                    return false;
                }
            }
            return true;
        } catch (Throwable th) {
            qe.a(new Exception(th), wd.e("\u00123%.%a;.6%>/0a61'22$p2w/65>72a29#$92#(8/$"));
            return false;
        }
    }

    private static native /* synthetic */ boolean initEncoder(String str, int i, int i2, int i3, int i4, String[] strArr, int[] iArr, boolean z, boolean z2, boolean z3);

    /* access modifiers changed from: private */
    public static native /* synthetic */ void installSignalHandlers(String str, String str2);

    private static native /* synthetic */ void saveMainThreadId();

    static boolean a(lb lbVar) {
        int i = mi.f2031a[lbVar.ordinal()];
        if (i == 1) {
            return h();
        }
        if (i != 2) {
            return true;
        }
        return f();
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ boolean b(java.lang.String r3) {
        /*
            java.util.List r0 = c()
            java.util.Iterator r0 = r0.iterator()
        L_0x0008:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0022
            java.lang.Object r1 = r0.next()
            java.lang.String r1 = (java.lang.String) r1
            boolean r2 = r1.contains(r3)
            if (r2 != 0) goto L_0x0020
            boolean r1 = r1.equalsIgnoreCase(r3)
            if (r1 == 0) goto L_0x0008
        L_0x0020:
            r3 = 1
            return r3
        L_0x0022:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.AppseeNativeExtensions.b(java.lang.String):boolean");
    }

    static boolean a() {
        return b && finishEncoding();
    }

    static boolean a(String str, int i, int i2, int i3, int i4, String[] strArr, int[] iArr, boolean z, boolean z2, boolean z3) {
        if (!b) {
            return false;
        }
        return initEncoder(str, i, i2, i3, i4, strArr, iArr, z, z2, z3);
    }

    private static /* synthetic */ void a(String str) throws Exception {
        gd.a(1, cc.a("zAWJ_@Q\u000e\u0013]\u0016\u0006XOBG@K\u001f\u0002\u0016GEj_\\SMB\u0014\u0016\u000bT"), str, false);
        List<String> c2 = c();
        lg.b(c2.get(0));
        File a2 = lg.a(lg.e, String.format(wd.e("->#r2y28"), new Object[]{str}));
        try {
            System.load(a2.getAbsolutePath());
        } catch (UnsatisfiedLinkError e) {
            if (e() || (c2.size() > 1 && c2.get(0).contains(cc.a("N\u0016\u0000")))) {
                qe.a(e, wd.e("\u00123%.%a;.6%>/0a61'22$w/65>72a;(5363."));
                gd.b(2, cc.a("pO_BSJ\u0016ZY\u000eZAWJ\u0016OF^EKS\u000eXOBG@K\u0016B_LDODW\u001a\u000eB\\OGXI\u0016ZY\u000eZAWJ\u0016GB\u000e_@\u0016\u001d\u0004\u0003TGB\u000e@KD]_AX"));
                lg.b(a2);
                lg.b(c2.get(1));
                System.load(a2.getAbsolutePath());
                return;
            }
            throw e;
        }
    }

    static void a(int i, int i2, Bitmap bitmap, ByteBuffer byteBuffer, boolean z, boolean z2, boolean z3) {
        if (f1965a) {
            convertToYuv(i, i2, bitmap, byteBuffer, z, z2, z3);
        }
    }
}
