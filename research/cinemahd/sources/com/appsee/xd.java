package com.appsee;

import android.graphics.Rect;
import com.vungle.warren.AdLoader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class xd {
    private static xd o;

    /* renamed from: a  reason: collision with root package name */
    private final List<ui> f2085a = new ArrayList();
    private cd b;
    private long c;
    private boolean d = false;
    private final List<nc> e = new ArrayList();
    private zn f = zn.b;
    private wl g = null;
    private long h;
    private String i;
    private String j;
    private long k;
    private volatile boolean l = false;
    private final List<ag> m = new ArrayList();
    private Rect n;

    private /* synthetic */ xd() {
    }

    public void a(boolean z) {
        this.d = z;
    }

    /* renamed from: b  reason: collision with other method in class */
    public Rect m158b() {
        return this.n;
    }

    public void c() throws JSONException, IOException {
        try {
            lh.a().a();
            c(true);
        } finally {
            b(true);
        }
    }

    /* renamed from: d  reason: collision with other method in class */
    public void m162d() {
        synchronized (this.m) {
            this.m.clear();
        }
    }

    public void e() {
        sc.a().a();
        ab.a().b();
        d();
        this.h = 0;
        this.c = 0;
    }

    public static synchronized xd b() {
        xd xdVar;
        synchronized (xd.class) {
            if (o == null) {
                o = new xd();
            }
            xdVar = o;
        }
        return xdVar;
    }

    public void a() {
        this.k = di.b();
        this.c = this.k - this.h;
        this.l = true;
        this.d = false;
        this.g = null;
        if (ym.a().a()) {
            bo.a().a(ak.k, ym.a().b() ? md.a("\u0017\u000b\u0016\u001c") : bb.a("Tf^tW"), (String) null, (jo) null);
        }
        this.f = di.b().a(true);
        lh.a().b();
    }

    public void c(boolean z) throws JSONException, IOException {
        long b2 = b();
        if (b2 < 0) {
            b2 = 0;
        }
        JSONArray jSONArray = new JSONArray();
        HashSet hashSet = new HashSet();
        synchronized (this.e) {
            for (nc next : this.e) {
                if (!z) {
                    jSONArray.put(next.a());
                } else if (b2 - next.a() > AdLoader.RETRY_DELAY) {
                    jSONArray.put(next.a());
                } else {
                    hashSet.add(Long.valueOf(next.a()));
                    gd.a(1, bb.a("{`\\h@bV']u[b\\sSs[h\\'WqWiF=\u0012\"A"), next.a().toString());
                }
            }
        }
        JSONArray jSONArray2 = new JSONArray();
        synchronized (this.f2085a) {
            for (ui next2 : this.f2085a) {
                if (!hashSet.contains(Long.valueOf(next2.a()))) {
                    jSONArray2.put(next2.a());
                }
            }
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(md.a("*\u0017\n\r\n\u0018\u000f6\u0011\u0010\u0006\u0017\u0017\u0018\u0017\u0010\f\u0017"), this.f.ordinal());
        jSONObject.put(bb.a("{i[s[f^FBwtuSjW"), a(z));
        jSONObject.put(md.a(":\u0011\u0018\u0010\u0011\u0006\u001d"), this.d);
        jSONObject.put(bb.a("CGuSs[h\\"), b2);
        jSONObject.put(md.a("3\u000b\u0006*\u0006\n\u0010\u0010\f\u0017'\f\u0011\u0018\u0017\u0010\f\u0017"), this.c);
        jSONObject.put(bb.a("dnVb]UWd]uVbV"), pg.h().K());
        jSONObject.put(md.a(" \f\u0010\r\f\u0014&\u000f\u0006\u0017\u0017\n"), em.b(this.m));
        Object[] a2 = sc.a().a(z);
        jSONObject.put(bb.a("ad@bWiA"), em.b((List) a2[0]));
        jSONObject.put(md.a("3\u0016\u0013\f\u0013\n"), em.b((List) a2[1]));
        jSONObject.put(bb.a("@WtFr@bA"), em.b(ij.a().a(z)));
        jSONObject.put(md.a("8\u0000\r\n\u0016\r\n"), em.b(bo.a().a(z)));
        jSONObject.put(bb.a("}u[b\\sSs[h\\BDb\\sA"), jSONArray);
        jSONObject.put(md.a("\"\t\u0013?\u0011\u0018\u000e\u001c&\u000f\u0006\u0017\u0017\n"), jSONArray2);
        jSONObject.put(bb.a("fo[uVWSuF~{cA"), em.b(ab.a().a(z).values()));
        jSONObject.put(md.a("\"71\n"), lh.a(lh.a().a(z)));
        if (!z) {
            jSONObject.put(bb.a("bf@s[f^"), true);
        }
        if (!yb.a(this.j)) {
            jSONObject.put("AppUserId", this.j);
        }
        if (this.b == null) {
            if (!yb.a(this.i)) {
                jSONObject.put(bb.a("~hQfFn]ivbAd@nBs[h\\"), this.i);
            }
            if (pg.h().n()) {
                jSONObject.put(md.a("-\f\f\u0000\u0011&\u000f\u0006\u0017\u0017\n"), em.a(wb.a().a(z)));
            }
            if (this.g != null) {
                jSONObject.put(bb.a("D@fAo~hU"), this.g.a());
            }
            if (pg.h().A()) {
                jSONObject.put(md.a("!\u001c\r\u001a\u000b\u0014\u0002\u000b\b\n"), oc.a(oc.a().a(z)));
            }
            JSONObject a3 = el.a(el.a().a(z));
            if (a3 != null) {
                jSONObject.put(bb.a("BJsWu\\f^TvLA"), a3);
            }
            String jSONObject2 = jSONObject.toString();
            gd.a(z ? 1 : 0, md.a("\u0013\u0010\u0016\rY^YF\n"), jSONObject2);
            lg.a(jSONObject2, pg.h().m(), !z);
            return;
        }
        md.a("/\u0016\u0000\u0018\u0017\u0010\f\u0017");
        this.b.a();
        throw null;
    }

    /* renamed from: d  reason: collision with other method in class */
    public boolean m163d() {
        return this.l;
    }

    /* renamed from: b  reason: collision with other method in class */
    public long m157b() {
        long b2;
        long j2;
        if (pg.h().K()) {
            if (s.a().a() == 0) {
                return -1;
            }
            b2 = di.b();
            j2 = s.a().a();
        } else if (this.k == 0) {
            return -1;
        } else {
            b2 = di.b();
            j2 = this.k;
        }
        return b2 - j2;
    }

    public long d() {
        return di.b() - this.h;
    }

    /* renamed from: b  reason: collision with other method in class */
    public String m159b() {
        return this.j;
    }

    /* renamed from: b  reason: collision with other method in class */
    public void m160b() {
        this.h = di.b();
    }

    /* renamed from: b  reason: collision with other method in class */
    public boolean m161b() {
        return this.d;
    }

    private /* synthetic */ boolean a(Object obj) {
        if (obj == null) {
            return true;
        }
        if (obj.getClass().equals(Double.class)) {
            double doubleValue = ((Double) obj).doubleValue();
            return !Double.isNaN(doubleValue) && !Double.isInfinite(doubleValue);
        } else if (!obj.getClass().equals(Float.class)) {
            return obj.getClass().equals(Integer.class) || obj.getClass().equals(String.class) || obj.getClass().equals(Date.class) || obj.getClass().equals(URL.class) || obj.getClass().equals(Boolean.class);
        } else {
            float floatValue = ((Float) obj).floatValue();
            return !Float.isNaN(floatValue) && !Float.isInfinite(floatValue);
        }
    }

    public void b(boolean z) {
        this.k = 0;
        this.l = false;
        lh.a().a();
        synchronized (this.e) {
            this.e.clear();
        }
        synchronized (this.f2085a) {
            this.f2085a.clear();
        }
        lh.a().c();
        wb.a().a();
        ij.a().a();
        bo.a().b();
        oc.a().a();
        if (z) {
            e();
        }
    }

    public void a(Rect rect) {
        this.n = new Rect(rect);
    }

    public void a(zn znVar) throws Exception {
        zn znVar2;
        r a2;
        if (this.l) {
            long b2 = b();
            if (!pg.h().B() && (a2 = ub.a().a()) != null) {
                a(rb.a(a2), b2);
            }
            synchronized (this.e) {
                if (this.e.isEmpty()) {
                    znVar2 = this.f;
                } else {
                    znVar2 = this.e.get(this.e.size() - 1).a();
                }
                if (znVar != znVar2) {
                    gd.a(1, bb.a("FVc[iU']u[b\\sSs[h\\'WqWiF=\u0012\"A"), znVar.toString());
                    List<nc> list = this.e;
                    if (b2 < 0) {
                        b2 = 0;
                    }
                    list.add(new nc(b2, znVar));
                }
            }
        }
    }

    public void a(Rect rect, long j2) {
        if (d() && this.n != null) {
            synchronized (this.f2085a) {
                Rect rect2 = this.n;
                if (!this.f2085a.isEmpty()) {
                    rect2 = this.f2085a.get(this.f2085a.size() - 1).a();
                }
                if (!rect.equals(rect2)) {
                    gd.a(1, md.a("\"\u001d\u0007\u0010\r\u001eC\u0018\u0013\tC\u001f\u0011\u0018\u000e\u001cC\u001c\u0015\u001c\r\rYYF\n"), rect.toString());
                    ui uiVar = new ui(rect, j2);
                    if (uiVar.a() < 0) {
                        uiVar.a(0);
                    }
                    this.f2085a.add(uiVar);
                }
            }
        }
    }

    public void a(wl wlVar) {
        this.g = wlVar;
    }

    public void a(String str, Map<String, Object> map) {
        if (yb.a(str)) {
            gd.b(3, md.a("\u0018\u0007\u001d&\u000f\u0006\u0017\u0017C4\u0010\u0017\u00113\u000b\f\t\u0006\u000b\u0017\u0010\u0006\nYY*\u0017\u0015\u0018\u000f\u0010\u0007Y\u0006\u000f\u0006\u0017\u0017Y\r\u0018\u000e\u001cYYD^"));
            return;
        }
        gd.a(1, md.a("\"\u001d\u0007\u0010\r\u001eC\u001a\u0016\n\u0017\u0016\u000eY\u0006\u000f\u0006\u0017\u0017CC\\\u0010YF\n"), str, (map == null || map.isEmpty()) ? "" : yb.a((Iterable) map.values(), bb.a("+\u0012")));
        HashMap hashMap = new HashMap();
        if (map != null && !map.isEmpty()) {
            for (Map.Entry next : map.entrySet()) {
                Object value = next.getValue();
                if (yb.a((String) next.getKey())) {
                    gd.a(3, bb.a("N\\qSk[c\u0012w@hBb@sK'\\f_b\b'\u0015 \u0012a]u\u0012bDb\\s\u0012 \u0017t\u0015"), str);
                } else if (!a(value)) {
                    gd.a(3, md.a("0\r\u000f\u0002\u0015\n\u001dC\u000f\u0002\u0015\u0016\u001cC\u001f\f\u000bC\t\u0011\u0016\u0013\u001c\u0011\r\u001aYD\\\u0010^C\u0016\u0005Y\u0006\u000f\u0006\u0017\u0017YD\\\u0010^"), next.getKey(), str);
                } else {
                    if (value == null) {
                        value = "";
                    }
                    if (value.getClass().equals(URL.class)) {
                        value = ((URL) value).toString();
                    }
                    if (value.getClass().equals(Date.class)) {
                        value = yb.a((Date) value);
                    }
                    if (value.getClass().equals(Boolean.class)) {
                        value = value.toString().toLowerCase();
                    }
                    hashMap.put(next.getKey(), value);
                }
            }
        }
        if (hashMap.isEmpty()) {
            hashMap = null;
        }
        long b2 = b();
        ag agVar = new ag(str, hashMap);
        if (b2 < 0) {
            agVar.b(d());
            b2 = 0;
        }
        agVar.a(b2);
        int size = agVar.a() != null ? agVar.a().size() : 1;
        if (pg.h().K() <= 0 || b() + size <= pg.h().K()) {
            synchronized (this.m) {
                this.m.add(agVar);
            }
            return;
        }
        gd.a(3, bb.a("{`\\h@n\\`\u0012bDb\\s\u0012 \u0017t\u0015'An\\dW'KhG'WQbWcWc\u0012sZb\u0012f_hGiF']a\u0012bDb\\sA'SqSn^fPkW'[i\u0012~]r@'BkSi\u001c"), agVar.a());
    }

    public void a(String str) {
        this.j = str;
    }
}
