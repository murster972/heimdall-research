package com.appsee;

import android.annotation.TargetApi;
import android.os.Build;
import android.text.TextUtils;
import java.io.OutputStream;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

@TargetApi(9)
class ic extends vc {
    private boolean e = false;
    /* access modifiers changed from: private */
    public HttpURLConnection f = null;
    private boolean g = false;
    private CookieManager h = new CookieManager((CookieStore) null, CookiePolicy.ACCEPT_ALL);

    ic() {
        if (Build.VERSION.SDK_INT < 23) {
            throw new UnsupportedOperationException(lf.a("Rnou&uctpct&vti~&ou&gpgojgdjc&ihj&`tik&Ghbtiob&kgtunkgjjiq&ghb&sv"));
        }
    }

    private /* synthetic */ JSONObject a(String str, HashMap<String, String> hashMap, byte[] bArr, int i) throws Exception {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        d(httpURLConnection);
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setConnectTimeout(i);
        httpURLConnection.setDoInput(true);
        httpURLConnection.setDoOutput(true);
        httpURLConnection.setRequestMethod(cc.a("~y}b"));
        httpURLConnection.setRequestProperty(lf.a("Geecvr"), cc.a("OF^ZGUOBGY@\u0019DEAX"));
        for (Map.Entry next : hashMap.entrySet()) {
            httpURLConnection.setRequestProperty((String) next.getKey(), (String) next.getValue());
        }
        a(httpURLConnection);
        List<HttpCookie> cookies = this.h.getCookieStore().getCookies();
        if (cookies.size() > 0) {
            ArrayList arrayList = new ArrayList(cookies.size());
            for (HttpCookie httpCookie : cookies) {
                arrayList.add(httpCookie.toString());
            }
            httpURLConnection.setRequestProperty(lf.a("Eiimoc"), TextUtils.join(cc.a("\r"), arrayList));
        }
        OutputStream outputStream = httpURLConnection.getOutputStream();
        b(httpURLConnection);
        outputStream.write(bArr);
        outputStream.flush();
        outputStream.close();
        int responseCode = httpURLConnection.getResponseCode();
        String a2 = yb.a(responseCode >= 200 && responseCode < 400 ? httpURLConnection.getInputStream() : httpURLConnection.getErrorStream());
        List<String> list = (List) httpURLConnection.getHeaderFields().get(lf.a("Ucr+Eiimoc"));
        if (list != null) {
            for (String str2 : list) {
                try {
                    this.h.getCookieStore().add(new URI(str), HttpCookie.parse(str2).get(0));
                } catch (Exception e2) {
                    qe.a((Throwable) e2, cc.a("kD\\Y\\\u0016]WX_@Q\u000eXKBYY\\]\u000eUOZB\u0016MYA]GS\u0014\u0016\u000bE"), str2);
                }
            }
        }
        c(httpURLConnection);
        if (responseCode != 200) {
            gd.a(1, lf.a("Air&cttitEibc&;&#b&`tik&uctpct&egjj"), Integer.valueOf(responseCode));
        }
        return a(a2);
    }

    private /* synthetic */ void b(HttpURLConnection httpURLConnection) throws Exception {
        synchronized (vc.d) {
            boolean z = this.e;
            this.e = false;
            if (!z) {
                this.f = httpURLConnection;
                if (httpURLConnection == null) {
                    this.g = false;
                }
            } else {
                throw new Exception(cc.a("XKBYY\\]\u000eDKG[S]B\u000eUOXMSBSJ\u0017"));
            }
        }
    }

    private /* synthetic */ void c(HttpURLConnection httpURLConnection) throws Exception {
        httpURLConnection.disconnect();
        b((HttpURLConnection) null);
    }

    private /* synthetic */ void d(HttpURLConnection httpURLConnection) {
        try {
            boolean startsWith = httpURLConnection.getURL().getProtocol().startsWith(cc.a("^ZB^E"));
            Object obj = httpURLConnection;
            if (startsWith) {
                obj = tc.a((Object) httpURLConnection, lf.a("bcjcagrc"));
            }
            if (obj != null) {
                Object a2 = tc.a(obj, lf.a("ejochr"));
                if (a2 != null) {
                    tc.a(a2, lf.a("ucrTcrtIhEihhceroih@gojstc"), 1, false);
                    return;
                }
                throw new NullPointerException(cc.a("^ZB^\u0016MZGS@B\u000e_]\u0016@CBZ"));
            }
            throw new NullPointerException(cc.a("C\\ZmY@XKUZ_AX\u000e_]\u0016@CBZ"));
        } catch (Exception e2) {
            qe.a(e2, cc.a("MW@XAB\u000eRGEOTBS\u000eUAX@SMBGY@\u0016\\SZDW\u0016AX\u000ePO_BC\\S"));
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public JSONObject a(String str, JSONObject jSONObject, int i) throws Exception {
        try {
            a(true);
            HashMap hashMap = new HashMap();
            hashMap.put(cc.a("mY@BKXZ\u001bZO^S"), lf.a("gvvjoegroih)luih"));
            JSONObject a2 = a(str, hashMap, yb.a(jSONObject.toString()), i);
            a(false);
            return a2;
        } catch (Throwable th) {
            a(false);
            throw th;
        }
    }

    public void a() {
        synchronized (vc.d) {
            if (this.f != null) {
                Thread thread = new Thread(new lc(this), cc.a("w^F]SKxKBYY\\]}BAF^_@Qz^\\SOR"));
                try {
                    thread.start();
                    thread.join();
                } catch (InterruptedException e2) {
                    qe.a(e2, lf.a("Cttit&Urivvoha&hcrqitm&egjj"));
                }
            } else if (this.g) {
                this.e = true;
            }
        }
    }

    /* JADX INFO: finally extract failed */
    /* access modifiers changed from: protected */
    public JSONObject a(String str, byte[] bArr, int i, String str2, Map<String, String> map, int i2) throws Exception {
        try {
            a(true);
            String format = String.format(cc.a("\u0003\u001b\u0003\u001b\u0003\u001b\u0003\u001b\u0003\u001b\u000bE"), new Object[]{UUID.randomUUID().toString().replace(lf.a("+"), "")});
            byte[] a2 = a(bArr, i, str2, map, format);
            HashMap hashMap = new HashMap();
            hashMap.put(cc.a("mY@BKXZ\u001bZO^S"), String.format(lf.a("ksjrovgtr)`itk+bgrg=&dishbgt;#u"), new Object[]{format}));
            String str3 = str;
            JSONObject a3 = a(str, hashMap, a2, i2);
            a(false);
            return a3;
        } catch (Throwable th) {
            a(false);
            throw th;
        }
    }

    private /* synthetic */ void a(HttpURLConnection httpURLConnection) {
        httpURLConnection.addRequestProperty(cc.a("mZGS@BgR"), pg.h().e());
        httpURLConnection.addRequestProperty(lf.a("GVOMc"), pg.h().h());
        httpURLConnection.addRequestProperty(cc.a("`KD]_AX"), Appsee.b);
        httpURLConnection.addRequestProperty(lf.a("Vjgr`itk"), cc.a("\u0007"));
        httpURLConnection.addRequestProperty(lf.a("GVOGsrn"), vc.c);
    }

    private /* synthetic */ void a(boolean z) {
        synchronized (vc.d) {
            this.g = z;
        }
    }
}
