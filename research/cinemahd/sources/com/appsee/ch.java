package com.appsee;

import android.graphics.Bitmap;
import android.graphics.Canvas;

class ch {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1984a = false;
    private boolean b = false;
    private Canvas c;
    private Bitmap d;

    public ch(int i, int i2) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, Bitmap.Config.ARGB_8888);
        this.d = createBitmap;
        this.c = new Canvas(createBitmap);
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ '&');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 1);
        }
        return new String(cArr);
    }

    public synchronized void a() {
        this.b = true;
        if (!this.f1984a) {
            b();
        }
    }

    /* renamed from: b  reason: collision with other method in class */
    public synchronized boolean m12b() {
        if (!this.f1984a) {
            if (!this.b) {
                this.f1984a = true;
                return true;
            }
        }
        return false;
    }

    public synchronized void c() {
        if (this.f1984a) {
            throw new RuntimeException(md.a(";\u0016\u001f\u0005\u001c\u0011Y\n\nC\u0018\u000f\u000b\u0006\u0018\u0007\u0000C\u0015\f\u001a\b\u001c\u0007X"));
        } else if (!this.b) {
            this.f1984a = true;
        } else {
            throw new RuntimeException(oi.a("\u0005O!\\\"HgY&T)U3\u001a%_gV(Y,_#\u001a%_$[2I\"\u001a.N4\u001a&V5_&^>\u001a%_\"TgY+_&T\"^"));
        }
    }

    public synchronized void d() {
        this.f1984a = false;
        if (this.b) {
            b();
        }
    }

    /* renamed from: b  reason: collision with other method in class */
    public synchronized Canvas m11b() {
        return this.c;
    }

    public synchronized Bitmap b() {
        return this.d;
    }
}
