package com.appsee;

import java.io.File;
import java.util.Comparator;

class ed implements Comparator<File> {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ wd f1991a;

    ed(wd wdVar) {
        this.f1991a = wdVar;
    }

    /* renamed from: a */
    public int compare(File file, File file2) {
        int a2 = this.f1991a.b(yb.a(file.getName(), false));
        int a3 = this.f1991a.b(yb.a(file2.getName(), false));
        if (a2 < a3) {
            return -1;
        }
        if (a2 > a3) {
            return 1;
        }
        return this.f1991a.a(file, file2);
    }
}
