package com.appsee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.PowerManager;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class zo {
    private static zo l;

    /* renamed from: a  reason: collision with root package name */
    private hd f2096a;
    private final HashMap<WeakReference<Activity>, Boolean> b;
    private Class<?> c;
    private final Object d;
    private boolean e;
    private boolean f;
    private boolean g;
    private q h;
    private final Object i;
    private PowerManager j;
    private final Object k;

    private /* synthetic */ zo() {
        this.f2096a = null;
        this.g = false;
        this.j = null;
        this.f = true;
        this.h = null;
        this.e = false;
        this.c = null;
        this.b = new HashMap<>();
        this.i = new Object();
        this.d = new Object();
        this.k = new Object();
        this.j = (PowerManager) ho.a().getSystemService(bb.a("BhEb@"));
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = length - 1;
        while (i2 >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'i');
            if (i3 < 0) {
                break;
            }
            i2 = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ ';');
        }
        return new String(cArr);
    }

    private /* synthetic */ void a(String str, boolean z) {
        if (z) {
            b(true);
            synchronized (this.d) {
                this.d.notify();
            }
        }
        if (!b()) {
            synchronized (this.k) {
                if (this.g) {
                    this.g = false;
                    this.e = false;
                    gd.a(1, bb.a("T[`\\f^n\\`\u0012fBw\u0012uWsGu\\n\\`\u0012a@h_'PfQlUu]r\\c\u001e'@bSt]i\u0012*\u0012\"A"), str);
                    if (this.h != null) {
                        this.h.m();
                    }
                }
            }
        }
    }

    static boolean f() {
        return Build.VERSION.SDK_INT >= 14;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b  reason: collision with other method in class */
    public kd m172b() {
        return new kd(this);
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (b()) {
            a(ch.a("ubTdCo\u0006hU!Ig@"));
        }
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        return this.g;
    }

    private /* synthetic */ void b(boolean z) {
        synchronized (this.i) {
            this.f = z;
        }
    }

    /* access modifiers changed from: package-private */
    public void c() throws Exception {
        if (this.g) {
            gd.b(1, bb.a("al[wBn\\`\u0012wSrAb\u0012qSk[cSs[h\\'SwB'Sk@bScK'[i\u0012eSdY`@hGiV"));
            return;
        }
        synchronized (this.d) {
            this.d.wait((long) pg.h().m());
        }
        if (hn.a().a()) {
            gd.b(1, ch.a("`nSoB!GbRhPhRx\u0006bNnIrCs\n!TdRs_hHf\u0006qGtUd\u0006wGmOeGuOnH"));
            oj.a(kp.f2020a);
            return;
        }
        e();
    }

    public static synchronized zo b() {
        zo zoVar;
        synchronized (zo.class) {
            if (l == null) {
                l = new zo();
            }
            zoVar = l;
        }
        return zoVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m171a() {
        return this.e;
    }

    /* access modifiers changed from: private */
    @TargetApi(24)
    public /* synthetic */ void a(Activity activity, boolean z, boolean z2) {
        gd.a(1, bb.a("TWsFn\\`\u0012fQs[q[sK'AsSsW'Th@'\u0015\"A \u0012nAUGi\\n\\`\u0012:\u0012\"P"), activity.getClass().getName(), Boolean.valueOf(z));
        synchronized (this.b) {
            Iterator<Map.Entry<WeakReference<Activity>, Boolean>> it2 = this.b.entrySet().iterator();
            boolean z3 = false;
            while (it2.hasNext()) {
                WeakReference weakReference = (WeakReference) it2.next().getKey();
                Activity activity2 = (Activity) weakReference.get();
                if (activity2 == null) {
                    it2.remove();
                } else if (activity2.equals(activity)) {
                    this.b.put(weakReference, Boolean.valueOf(z));
                    z3 = true;
                } else if (z && (Build.VERSION.SDK_INT < 24 || (Build.VERSION.SDK_INT >= 24 && !activity.isInMultiWindowMode()))) {
                    this.b.put(weakReference, false);
                }
            }
            if (!z3) {
                this.b.put(new WeakReference(activity), Boolean.valueOf(z));
            }
        }
        if (z) {
            a(ch.a("`EuOwOu_!TdUtKdB"), true);
        } else {
            a(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity, boolean z) {
        a(activity, z, false);
    }

    private /* synthetic */ void a(boolean z) {
        if (!this.g && !z && !c()) {
            a(ch.a("hn\u0006rR`TuCe\u0006`EuOwOu_"));
        }
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Activity activity) {
        synchronized (this.b) {
            Iterator<Map.Entry<WeakReference<Activity>, Boolean>> it2 = this.b.entrySet().iterator();
            while (true) {
                if (it2.hasNext()) {
                    if (activity.equals((Activity) ((WeakReference) it2.next().getKey()).get())) {
                        it2.remove();
                        break;
                    }
                } else {
                    break;
                }
            }
        }
        for (r a2 : ub.a().a()) {
            a2.a(activity);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        synchronized (this.d) {
            this.d.notify();
        }
    }
}
