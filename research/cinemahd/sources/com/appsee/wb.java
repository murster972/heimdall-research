package com.appsee;

import android.graphics.Point;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class wb {
    private static wb e;

    /* renamed from: a  reason: collision with root package name */
    private final Map<Integer, short[]> f2076a = new HashMap();
    private List<tb> b = new ArrayList();
    private short[] c = new short[10];
    private short d = 0;

    private /* synthetic */ wb() {
    }

    /* access modifiers changed from: package-private */
    public short b(int i) {
        short[] sArr = this.c;
        if (i >= sArr.length) {
            return -1;
        }
        return sArr[i];
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        return false;
     */
    /* renamed from: a  reason: collision with other method in class */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean m153a() {
        /*
            r9 = this;
            java.util.List<com.appsee.tb> r0 = r9.b
            monitor-enter(r0)
            java.util.List<com.appsee.tb> r1 = r9.b     // Catch:{ all -> 0x003f }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x003f }
            r2 = 0
            if (r1 == 0) goto L_0x000e
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return r2
        L_0x000e:
            java.util.List<com.appsee.tb> r1 = r9.b     // Catch:{ all -> 0x003f }
            java.util.List<com.appsee.tb> r3 = r9.b     // Catch:{ all -> 0x003f }
            int r3 = r3.size()     // Catch:{ all -> 0x003f }
            r4 = 1
            int r3 = r3 - r4
            java.lang.Object r1 = r1.get(r3)     // Catch:{ all -> 0x003f }
            com.appsee.tb r1 = (com.appsee.tb) r1     // Catch:{ all -> 0x003f }
            if (r1 == 0) goto L_0x003d
            com.appsee.eb r3 = r1.a()     // Catch:{ all -> 0x003f }
            com.appsee.eb r5 = com.appsee.eb.b     // Catch:{ all -> 0x003f }
            if (r3 != r5) goto L_0x003d
            com.appsee.xd r3 = com.appsee.xd.b()     // Catch:{ all -> 0x003f }
            long r5 = r3.b()     // Catch:{ all -> 0x003f }
            long r7 = r1.a()     // Catch:{ all -> 0x003f }
            long r5 = r5 - r7
            r7 = 500(0x1f4, double:2.47E-321)
            int r1 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r1 > 0) goto L_0x003d
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return r4
        L_0x003d:
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            return r2
        L_0x003f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.wb.m153a():boolean");
    }

    private /* synthetic */ void a(short s, short s2, short s3, eb ebVar, View view) throws Exception {
        if (xd.b().d() && xd.b().b() != -1) {
            if (ebVar == eb.d || ebVar == eb.c) {
                ub.a().b();
            }
            Rect e2 = rb.e(view);
            short s4 = (short) (s2 + e2.left);
            short s5 = (short) (s3 + e2.top);
            Point a2 = rb.a(new Point(s4, s5));
            if (!s.a().e() || !s.a().c()) {
                tb tbVar = new tb(s, (short) a2.x, (short) a2.y, xd.b().b(), ebVar, ym.a().a());
                synchronized (this.b) {
                    if (ebVar == eb.b) {
                        tb tbVar2 = this.b.isEmpty() ? null : this.b.get(this.b.size() - 1);
                        if (tbVar2 != null && tbVar2.a() == eb.b && tbVar.a() - tbVar2.a() < 120) {
                            return;
                        }
                    }
                    gd.a(1, jm.a("|(O)^gN(O$Rg\u001f#\u001aj\u001ao\u001f#\u0016b^n\u001a,\u0007![+I\""), Short.valueOf(s), Short.valueOf(s4), Short.valueOf(s5));
                    this.b.add(tbVar);
                    return;
                }
            }
            gd.b(1, bb.a("{`\\h@n\\`\u0012wSrAbV'DnVb]'FhGdZ"));
        }
    }

    private /* synthetic */ void a(MotionEvent motionEvent, short[] sArr, View view) throws Exception {
        synchronized (this.f2076a) {
            this.f2076a.put(Integer.valueOf(motionEvent.hashCode()), sArr);
        }
        ij.a().a(motionEvent, view);
    }

    /* access modifiers changed from: package-private */
    public void a(MotionEvent motionEvent, View view) throws Exception {
        eb ebVar;
        short a2;
        eb ebVar2;
        short s;
        short s2;
        MotionEvent motionEvent2 = motionEvent;
        View view2 = view;
        eb ebVar3 = eb.b;
        short x = (short) ((int) motionEvent2.getX(motionEvent.getActionIndex()));
        short y = (short) ((int) motionEvent2.getY(motionEvent.getActionIndex()));
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked != 0) {
            if (actionMasked == 1) {
                ebVar = eb.c;
                a2 = b(motionEvent2.getPointerId(motionEvent.getActionIndex()));
            } else if (actionMasked != 2) {
                if (actionMasked != 3) {
                    if (actionMasked == 5) {
                        ebVar = eb.d;
                        a2 = a(motionEvent2.getPointerId(motionEvent.getActionIndex()));
                    } else if (actionMasked == 6) {
                        ebVar = eb.c;
                        a2 = b(motionEvent2.getPointerId(motionEvent.getActionIndex()));
                    }
                }
                ebVar2 = ebVar3;
                s2 = y;
                s = 0;
            } else {
                eb ebVar4 = eb.b;
                short[] sArr = new short[motionEvent.getPointerCount()];
                short s3 = 0;
                int i = 0;
                while (i < motionEvent.getPointerCount()) {
                    short b2 = b(motionEvent2.getPointerId(i));
                    sArr[i] = b2;
                    if (b2 > 0) {
                        short x2 = (short) ((int) motionEvent2.getX(i));
                        short y2 = (short) ((int) motionEvent2.getY(i));
                        a(b2, x2, y2, ebVar4, view);
                        x = x2;
                        y = y2;
                    }
                    i++;
                    s3 = b2;
                }
                a(motionEvent2, sArr, view2);
                s2 = y;
                ebVar2 = ebVar4;
                s = s3;
            }
            ebVar2 = ebVar;
            s = a2;
            s2 = y;
        } else {
            ebVar = eb.d;
            a2 = a(motionEvent2.getPointerId(motionEvent.getActionIndex()));
            ebVar2 = ebVar;
            s = a2;
            s2 = y;
        }
        short s4 = x;
        if (ebVar2 != eb.b && s > 0) {
            a(s, s4, s2, ebVar2, view);
            a(motionEvent2, new short[]{s}, view2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m152a() {
        synchronized (this.b) {
            this.b.clear();
        }
        synchronized (this.f2076a) {
            this.f2076a.clear();
        }
    }

    public static synchronized wb a() {
        wb wbVar;
        synchronized (wb.class) {
            if (e == null) {
                e = new wb();
            }
            wbVar = e;
        }
        return wbVar;
    }

    /* access modifiers changed from: package-private */
    public short[] a(int i) {
        if (this.f2076a.containsKey(Integer.valueOf(i))) {
            return this.f2076a.get(Integer.valueOf(i));
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public List<tb> a(boolean z) {
        List<tb> list = this.b;
        if (!z) {
            return list;
        }
        synchronized (list) {
            this.b = new ArrayList();
        }
        synchronized (this.f2076a) {
            this.f2076a.clear();
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public List<tb> m151a() {
        return this.b;
    }
}
