package com.appsee;

import android.app.Application;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Locale;

class ym {
    private static Class<?>[] j;
    private static ym k;
    private static Class<?>[] l;

    /* renamed from: a  reason: collision with root package name */
    private boolean f2091a;
    /* access modifiers changed from: private */
    public boolean b = false;
    private long c = 0;
    private final Object d = new Object();
    private WindowManager e = ((WindowManager) ho.a().getSystemService(cc.a("Y_@RAA")));
    private InputMethodManager f = ((InputMethodManager) ho.a().getSystemService(ch.a("hHqSuylCuNnB")));
    private Rect g = new Rect();
    private DisplayMetrics h = new DisplayMetrics();
    private w i = null;

    private /* synthetic */ ym() {
        try {
            Class<?>[] clsArr = new Class[1];
            clsArr[0] = Class.forName("com.android.internal.view.IInputMethodManager");
            j = clsArr;
            Class<?>[] clsArr2 = new Class[1];
            clsArr2[0] = Class.forName("com.android.internal.view.IInputMethodSession");
            l = clsArr2;
        } catch (ClassNotFoundException e2) {
            qe.a(e2, ch.a("BGoHnR!OoOuO`Rd\u0006jCxDnGsB!BdRdEuOnH!EmGrUdU"));
        }
    }

    private /* synthetic */ void a(boolean z, boolean z2) {
        if (!z || !a()) {
            this.b = false;
            return;
        }
        this.b = true;
        new Handler(Looper.getMainLooper()).postDelayed(new vo(this, z2), 1000);
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        boolean z;
        synchronized (this.d) {
            z = this.b;
        }
        return z;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(String str, Object[] objArr) throws Exception {
        if (str.equals(ch.a("UiIvun@uooVtR")) || str.equals(cc.a("EFYY{WeAPZ@F[B"))) {
            a(true, ch.a("HHqSuk`H`AdT/UiIvun@uooVtR)\u000f"), false);
        } else if (str.equals(cc.a("^GRKeAPZ@F[B")) || str.equals(ch.a("NhBdkxun@uooVtR"))) {
            a(false, cc.a("gX^CZ{OXOQKD\u0000^GRKeAPZ@F[B\u0006\u001f"), false);
        } else if (str.equals(ch.a("eOrV`RbNJCxcwCoR"))) {
            if (objArr != null && objArr.length >= 2 && (objArr[1] instanceof KeyEvent)) {
                KeyEvent keyEvent = objArr[1];
                if (keyEvent.getKeyCode() == 4 && keyEvent.getAction() == 1 && a()) {
                    a(false, cc.a("tOUE\u0016LCZBAX\u000eF\\S]EKR"), false);
                }
            }
        } else if (str.equals(ch.a("SqB`RdudJdEuOnH"))) {
            bo.a().a();
        }
    }

    private /* synthetic */ void a(boolean z, String str, boolean z2) throws Exception {
        this.c = di.b();
        if (!z || !ub.a().c()) {
            synchronized (this.d) {
                boolean z3 = true;
                gd.a(1, cc.a("eKBZ_@Q\u000e]KOLYODJ\u0016]BOBK\u0016GEaFKX\u000e\u000b\u000e\u0013L\u001a\u000edKW]Y@\u0016\u0013\u0016\u000bE"), Boolean.valueOf(z), str);
                boolean z4 = this.f2091a;
                this.f2091a = z;
                if (z) {
                    li b2 = di.b();
                    if (z2) {
                        z3 = false;
                    }
                    a(b2.a(z3), z2);
                } else {
                    this.b = false;
                }
                if (!(this.i == null || z == z4)) {
                    this.i.a(z, this.b);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(zn znVar) {
        a(znVar == zn.e || znVar == zn.f2095a, true);
    }

    public static synchronized ym a() {
        ym ymVar;
        synchronized (ym.class) {
            if (k == null) {
                k = new ym();
            }
            ymVar = k;
        }
        return ymVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m168a() throws Exception {
        List a2;
        View a3;
        if (xd.b().d() && pg.h().x() && pg.h().w()) {
            a(j, "com.android.internal.view.IInputMethodManager$Stub$Proxy", cc.a("CeKDX_MS"));
            a(l, "com.android.internal.view.IInputMethodSession$Stub$Proxy", ch.a("letTLCuNnB"));
            if (!this.b && !ub.a().c() && (a2 = ub.a().a()) != null && !a2.isEmpty()) {
                long j2 = 0;
                int i2 = 0;
                boolean z = false;
                while (true) {
                    boolean z2 = true;
                    if (i2 < a2.size()) {
                        r rVar = (r) a2.get(i2);
                        if (rVar.a() != null && (a3 = rVar.a()) != null) {
                            this.e.getDefaultDisplay().getMetrics(this.h);
                            a3.getWindowVisibleDisplayFrame(this.g);
                            int width = this.g.width();
                            DisplayMetrics displayMetrics = this.h;
                            if (width == displayMetrics.widthPixels) {
                                if (((double) Math.abs(displayMetrics.heightPixels - this.g.bottom)) <= ((double) this.h.heightPixels) * pg.h().h()) {
                                    z2 = false;
                                }
                                z |= z2;
                                int i3 = this.g.bottom;
                                int i4 = this.h.heightPixels;
                                if (i3 != i4) {
                                    j2 = (long) Math.abs(i4 - i3);
                                }
                            }
                            i2++;
                        } else {
                            return;
                        }
                    } else {
                        long b2 = di.b() - this.c;
                        if (a() != z && b2 > 500) {
                            a(z, String.format(Locale.US, cc.a("mY@BKXZ\u0016FSGQFB\u000eRGPH\u0016\u000bR"), new Object[]{Long.valueOf(j2)}), true);
                            return;
                        }
                        return;
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m169a() {
        boolean z;
        synchronized (this.d) {
            z = this.f2091a;
        }
        return z;
    }

    private /* synthetic */ void a(Class<?>[] clsArr, String str, String str2) throws Exception {
        Application a2 = ho.a();
        Object a3 = tc.a((Object) this.f, str2);
        if (a3 != null && a3.getClass().equals(Class.forName(str))) {
            tc.a((Object) this.f, str2, Proxy.newProxyInstance(a2.getClassLoader(), clsArr, new go(this, a3)));
        }
    }
}
