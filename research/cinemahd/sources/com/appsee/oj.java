package com.appsee;

import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

class oj {

    /* renamed from: a  reason: collision with root package name */
    private static List<AppseeListener> f2046a;
    private static HashMap<String, ExecutorService> b = new HashMap<>();
    private static final Object c = new Object();

    oj() {
    }

    public static void a(kp kpVar, Bundle bundle) {
        Application a2 = ho.a();
        Intent intent = new Intent(a2, AppseeBackgroundUploader.class);
        intent.putExtra("com.appsee.Action.Mode", kpVar.ordinal());
        if (bundle != null) {
            intent.putExtras(bundle);
        }
        if (kpVar == kp.f || kpVar == kp.e) {
            zo.b().a();
        }
        a2.startService(intent);
        gd.a(1, cc.a("gXZS@B\u000eeKDX_MS\u000eg[S[SJ\u001a\u000e[ARK\u0016\u0013\u0016\u000bE"), kpVar.toString());
    }

    public static void b(z zVar) {
        try {
            zVar.a();
        } catch (Exception e) {
            qe.a(e, cc.a("hWZWB\u0016kNMS^BGY@\u0016Z^\\YYX\u000e_@\u0016ZW]]\u0014\u0016"));
        }
    }

    public static void a(z zVar) {
        if (a()) {
            b(zVar);
            return;
        }
        try {
            Handler handler = new Handler(Looper.getMainLooper());
            Object obj = new Object();
            synchronized (obj) {
                handler.post(new on(zVar, obj));
                obj.wait();
            }
        } catch (Exception e) {
            qe.a(e, qb.a("&\u0007\u0011\u001a\u0011U\u0011\u0000\r\u001b\n\u001b\u0004U\u0010\f\r\u0016\u000b\u0007\f\u001b\n\u000f\u0006\u0011C\u001a\rU\u000e\u0014\n\u001bC\u0001\u000b\u0007\u0006\u0014\u0007"));
        }
    }

    public static void a(c cVar, boolean z) {
        synchronized (c) {
            if (f2046a != null && !f2046a.isEmpty()) {
                if (z) {
                    a(qb.a("\"\u0005\u0013\u0006\u0006\u0010N&\u0006\u0006\u0010\u001c\f\u001bN9\n\u0006\u0017\u0010\r\u0010\u0011"), (z) new bp(cVar));
                } else {
                    a(cVar);
                }
            }
        }
    }

    public static boolean a() {
        return Thread.currentThread().equals(Looper.getMainLooper().getThread());
    }

    public static synchronized void a(String str, z zVar) {
        synchronized (oj.class) {
            if (!b.containsKey(str)) {
                b.put(str, Executors.newSingleThreadExecutor());
            }
            b.get(str).submit(new qn(zVar, str));
        }
    }

    /* access modifiers changed from: private */
    public static /* synthetic */ void a(c cVar) {
        List<AppseeListener> list = f2046a;
        if (list != null && !list.isEmpty()) {
            for (AppseeListener a2 : new ArrayList(f2046a)) {
                try {
                    cVar.a(a2);
                } catch (Exception e) {
                    qe.a(e, cc.a("s\\DAD\u000eD[X@_@Q\u000ew^F]SKzGEZS@S\\\u0016MWBZLWM]"));
                }
            }
        }
    }

    public static void a(kp kpVar) {
        a(kpVar, (Bundle) null);
    }
}
