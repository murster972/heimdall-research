package com.appsee;

import android.view.MotionEvent;

class rc {

    /* renamed from: a  reason: collision with root package name */
    private float f2059a;
    private float b;
    private float c;
    private float d;
    private float e;
    private l f;
    private float g;
    private int h = -1;
    private int i = -1;
    private boolean j;
    private float k;
    private short[] l;
    private float m;

    rc(l lVar) {
        this.f = lVar;
    }

    private /* synthetic */ int a(float f2, float f3, float f4, float f5) {
        float f6 = f2 - f4;
        float f7 = f3 - f5;
        return (int) Math.sqrt((double) ((f6 * f6) + (f7 * f7)));
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i2 = length - 1;
        while (i2 >= 0) {
            int i3 = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 1);
            if (i3 < 0) {
                break;
            }
            i2 = i3 - 1;
            cArr[i3] = (char) (str.charAt(i3) ^ 23);
        }
        return new String(cArr);
    }

    private /* synthetic */ float a(float f2, float f3, float f4, float f5, float f6, float f7, float f8, float f9) {
        float degrees = ((float) Math.toDegrees((double) (((float) Math.atan2((double) (f3 - f5), (double) (f2 - f4))) - ((float) Math.atan2((double) (f7 - f9), (double) (f6 - f8)))))) % 360.0f;
        if (degrees < -180.0f) {
            degrees += 360.0f;
        }
        return degrees > 180.0f ? degrees - 360.0f : degrees;
    }

    /* access modifiers changed from: package-private */
    public void a(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.j = false;
            this.h = motionEvent.getPointerId(motionEvent.getActionIndex());
        } else if (actionMasked == 1) {
            if (this.j) {
                float a2 = a(this.m, this.b, this.e, this.f2059a, this.d, this.c, this.k, this.g);
                int a3 = (int) di.a((float) (a(this.k, this.g, this.d, this.c) - a(this.e, this.f2059a, this.m, this.b)));
                if (this.f != null) {
                    if (Math.abs(a2) > 30.0f) {
                        this.f.a(a2, this.l, motionEvent);
                    }
                    if (Math.abs(a3) > 40) {
                        this.f.a(a3, this.l, motionEvent);
                    }
                }
            }
            this.h = -1;
        } else if (actionMasked == 2) {
        } else {
            if (actionMasked != 5) {
                if (actionMasked == 6) {
                    int pointerId = motionEvent.getPointerId(motionEvent.getActionIndex());
                    if (pointerId == this.h || pointerId == this.i) {
                        this.l = new short[]{wb.a().b(this.h), wb.a().b(this.i)};
                        this.j = true;
                        this.k = motionEvent.getX(motionEvent.findPointerIndex(this.h));
                        this.g = motionEvent.getY(motionEvent.findPointerIndex(this.h));
                        this.d = motionEvent.getX(motionEvent.findPointerIndex(this.i));
                        this.c = motionEvent.getY(motionEvent.findPointerIndex(this.i));
                        this.i = -1;
                        this.h = -1;
                    }
                }
            } else if (this.i == -1 && this.h != -1) {
                this.i = motionEvent.getPointerId(motionEvent.getActionIndex());
                this.e = motionEvent.getX(motionEvent.findPointerIndex(this.h));
                this.f2059a = motionEvent.getY(motionEvent.findPointerIndex(this.h));
                this.m = motionEvent.getX(motionEvent.findPointerIndex(this.i));
                this.b = motionEvent.getY(motionEvent.findPointerIndex(this.i));
            }
        }
    }
}
