package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Rect;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;
import android.view.Surface;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@TargetApi(21)
class zi implements j {

    /* renamed from: a  reason: collision with root package name */
    private int f2094a;
    private ih b;
    private int c;
    private int d;
    private jm e = oc.a().a(hn.a("Q w!p+]#u)q"));
    private boolean f;
    private jm g = oc.a().a(bb.a("DSiDfAD]iDb@t[h\\"));
    private int h;
    private long i;
    private MediaCodec j;
    private long k;
    private boolean l = false;
    private long m;
    private Surface n;
    private Rect o;
    private HashMap<Long, Long> p = new HashMap<>();
    private gh q;
    private jm r = oc.a().a(hn.a("G;f(u-q\u0004u8u\u000bz-{*} s"));
    private String s;
    private MediaMuxer t;
    private int u;
    private CountDownLatch v;

    zi() {
    }

    private /* synthetic */ void b() throws Exception {
        try {
            MediaFormat createVideoFormat = MediaFormat.createVideoFormat("video/avc", this.h, this.f2094a);
            createVideoFormat.setInteger(hn.a("-{\"{<9({<y/`"), 2130708361);
            createVideoFormat.setInteger(bb.a("PnFuSsW"), this.u);
            createVideoFormat.setInteger(hn.a("(f/y+9<u:q"), this.d);
            createVideoFormat.setInteger(bb.a("n\u001fa@f_b\u001fn\\sWuDf^"), 1);
            createVideoFormat.setInteger(hn.a("f+d+u:9>f+b'{;gcr<u#qcu(`+f"), (1000000 / this.d) * 2);
            this.j = MediaCodec.createEncoderByType("video/avc");
            if (this.j != null) {
                if (!pg.h().P()) {
                    this.j.reset();
                }
                MediaCodec mediaCodec = this.j;
                ih ihVar = new ih(this);
                this.b = ihVar;
                mediaCodec.setCallback(ihVar);
                this.j.configure(createVideoFormat, (Surface) null, (MediaCrypto) null, 1);
                this.n = this.j.createInputSurface();
                this.j.start();
                this.v = new CountDownLatch(1);
                return;
            }
            throw new Exception(bb.a("ar@aSdW'WiQhVb@'[t\u0012iGk^"));
        } catch (LinkageError unused) {
            qe.a(hn.a("\b{;z*4\"} /s+4+f<{<4fd<{,u,x74,u*4\u001c[\u00034({<4\u000fg7z-Y+p'u\r{*q-=u4/v!f:} snb'p+{"));
            throw null;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(13:0|1|2|(3:4|(1:6)|7)|8|9|(1:11)|12|13|(1:15)|(3:16|17|(1:19))|20|(3:22|23|25)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(17:0|1|2|(3:4|(1:6)|7)|8|9|(1:11)|12|13|(1:15)|16|17|(1:19)|20|22|23|25) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x0031 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:16:0x003a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:8:0x0023 */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0027 A[Catch:{ Exception -> 0x0031 }] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0035 A[Catch:{ Exception -> 0x003a }] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x003e A[Catch:{ Exception -> 0x0045 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void c() {
        /*
            r2 = this;
            r0 = 1
            r2.l = r0
            r0 = 0
            android.media.MediaCodec r1 = r2.j     // Catch:{ Exception -> 0x0023 }
            if (r1 == 0) goto L_0x0023
            com.appsee.pg r1 = com.appsee.pg.h()     // Catch:{ Exception -> 0x0023 }
            boolean r1 = r1.P()     // Catch:{ Exception -> 0x0023 }
            if (r1 != 0) goto L_0x0017
            android.media.MediaCodec r1 = r2.j     // Catch:{ Exception -> 0x0023 }
            r1.reset()     // Catch:{ Exception -> 0x0023 }
        L_0x0017:
            android.media.MediaCodec r1 = r2.j     // Catch:{ Exception -> 0x0023 }
            r1.stop()     // Catch:{ Exception -> 0x0023 }
            android.media.MediaCodec r1 = r2.j     // Catch:{ Exception -> 0x0023 }
            r1.release()     // Catch:{ Exception -> 0x0023 }
            r2.b = r0     // Catch:{ Exception -> 0x0023 }
        L_0x0023:
            android.media.MediaMuxer r1 = r2.t     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x0031
            android.media.MediaMuxer r1 = r2.t     // Catch:{ Exception -> 0x0031 }
            r1.stop()     // Catch:{ Exception -> 0x0031 }
            android.media.MediaMuxer r1 = r2.t     // Catch:{ Exception -> 0x0031 }
            r1.release()     // Catch:{ Exception -> 0x0031 }
        L_0x0031:
            android.view.Surface r1 = r2.n     // Catch:{ Exception -> 0x003a }
            if (r1 == 0) goto L_0x003a
            android.view.Surface r1 = r2.n     // Catch:{ Exception -> 0x003a }
            r1.release()     // Catch:{ Exception -> 0x003a }
        L_0x003a:
            com.appsee.gh r1 = r2.q     // Catch:{ Exception -> 0x0045 }
            if (r1 == 0) goto L_0x0045
            com.appsee.gh r1 = r2.q     // Catch:{ Exception -> 0x0045 }
            r1.b()     // Catch:{ Exception -> 0x0045 }
            r2.q = r0     // Catch:{ Exception -> 0x0045 }
        L_0x0045:
            r2.j = r0
            r2.n = r0
            r2.t = r0
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0055 }
            java.lang.String r1 = r2.s     // Catch:{ Exception -> 0x0055 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x0055 }
            com.appsee.lg.b((java.io.File) r0)     // Catch:{ Exception -> 0x0055 }
        L_0x0055:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.zi.c():void");
    }

    public void a() throws Exception {
        if (!this.l) {
            this.g.c();
            this.r.c();
            this.e.c();
            if (this.j != null) {
                gd.b(1, bb.a("eu[s[iU'wHa"));
                this.j.signalEndOfInputStream();
                if (!this.v.await(10, TimeUnit.SECONDS)) {
                    gd.a(2, hn.a("@'y+pn{;`nc/}:} snr!fnb'p+{nQ\u0001Gn`!4,qnc<}:`+z"));
                }
                gd.a(1, bb.a("as]wBn\\`\u0012b\\d]cWu\u0012/\u0017c\u001b"), Integer.valueOf(this.p.size()));
                if (!pg.h().P()) {
                    this.b.a();
                    this.j.reset();
                }
                this.j.stop();
                this.j.release();
                this.b = null;
            }
            MediaMuxer mediaMuxer = this.t;
            if (mediaMuxer != null) {
                mediaMuxer.stop();
                this.t.release();
            }
            Surface surface = this.n;
            if (surface != null) {
                surface.release();
            }
            gh ghVar = this.q;
            if (ghVar != null) {
                ghVar.b();
                this.q = null;
            }
            this.j = null;
            this.n = null;
            this.t = null;
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m170a() {
        return false;
    }

    /* JADX WARNING: Removed duplicated region for block: B:42:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0103  */
    @android.annotation.TargetApi(23)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.appsee.ch r16, long r17) throws java.lang.Exception {
        /*
            r15 = this;
            r1 = r15
            boolean r0 = r1.l
            if (r0 == 0) goto L_0x0006
            return
        L_0x0006:
            com.appsee.jm r0 = r1.e
            r0.b()
            boolean r0 = r1.f
            r2 = 1
            if (r0 == 0) goto L_0x0019
            java.lang.String r0 = "\tq:`'z)4+z-{*q<4-u b/g"
            java.lang.String r0 = com.appsee.hn.a((java.lang.String) r0)
            com.appsee.gd.b(r2, r0)
        L_0x0019:
            android.view.Surface r0 = r1.n
            boolean r0 = r0.isValid()
            if (r0 != 0) goto L_0x0022
            return
        L_0x0022:
            r3 = 0
            r7 = 2
            r8 = 0
            r9 = 0
            r10 = 1
            int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x00ca }
            r12 = 23
            if (r0 < r12) goto L_0x003b
            com.appsee.pg r0 = com.appsee.pg.h()     // Catch:{ all -> 0x00ca }
            boolean r0 = r0.v()     // Catch:{ all -> 0x00ca }
            if (r0 == 0) goto L_0x003b
            r0 = 1
            goto L_0x003c
        L_0x003b:
            r0 = 0
        L_0x003c:
            if (r0 == 0) goto L_0x0045
            android.view.Surface r12 = r1.n     // Catch:{ all -> 0x00ca }
            android.graphics.Canvas r12 = r12.lockHardwareCanvas()     // Catch:{ all -> 0x00ca }
            goto L_0x004d
        L_0x0045:
            android.view.Surface r12 = r1.n     // Catch:{ all -> 0x00ca }
            android.graphics.Rect r13 = r1.o     // Catch:{ all -> 0x00ca }
            android.graphics.Canvas r12 = r12.lockCanvas(r13)     // Catch:{ all -> 0x00ca }
        L_0x004d:
            boolean r13 = r1.f     // Catch:{ all -> 0x00c8 }
            if (r13 == 0) goto L_0x006d
            java.lang.String r13 = "C@fEn\\`\u0012h\\'WiQhVb@'Qf\\qSt\u0012\"A"
            java.lang.String r13 = com.appsee.bb.a((java.lang.String) r13)     // Catch:{ all -> 0x00c8 }
            java.lang.Object[] r14 = new java.lang.Object[r2]     // Catch:{ all -> 0x00c8 }
            if (r0 == 0) goto L_0x0062
            java.lang.String r0 = "\u0006u<p9u<q"
            java.lang.String r0 = com.appsee.hn.a((java.lang.String) r0)     // Catch:{ all -> 0x00c8 }
            goto L_0x0068
        L_0x0062:
            java.lang.String r0 = "T]aFpSuW"
            java.lang.String r0 = com.appsee.bb.a((java.lang.String) r0)     // Catch:{ all -> 0x00c8 }
        L_0x0068:
            r14[r9] = r0     // Catch:{ all -> 0x00c8 }
            com.appsee.gd.a(r2, r13, r14)     // Catch:{ all -> 0x00c8 }
        L_0x006d:
            com.appsee.jm r0 = r1.g     // Catch:{ all -> 0x00c8 }
            r0.b()     // Catch:{ all -> 0x00c8 }
            r0 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r12.drawColor(r0)     // Catch:{ all -> 0x00c8 }
            android.graphics.Bitmap r0 = r16.b()     // Catch:{ all -> 0x00c8 }
            r13 = 0
            r12.drawBitmap(r0, r13, r13, r8)     // Catch:{ all -> 0x00c8 }
            com.appsee.jm r0 = r1.g     // Catch:{ all -> 0x00c8 }
            r0.d()     // Catch:{ all -> 0x00c8 }
            java.util.HashMap<java.lang.Long, java.lang.Long> r0 = r1.p
            long r13 = r1.m
            long r5 = r13 + r10
            r1.m = r5
            java.lang.Long r5 = java.lang.Long.valueOf(r13)
            java.lang.Long r6 = java.lang.Long.valueOf(r17)
            r0.put(r5, r6)
            if (r12 == 0) goto L_0x00a1
            android.view.Surface r0 = r1.n
            r0.unlockCanvasAndPost(r12)
            r1.i = r3
            goto L_0x00c2
        L_0x00a1:
            java.lang.String r0 = "\u0000a\"xnw/z8u=49|+zn`<m'z)4:{na x!w%4'`u4-{ g+w;`'b+4,u*4(f/y+gt4kp"
            java.lang.String r0 = com.appsee.hn.a((java.lang.String) r0)
            java.lang.Object[] r2 = new java.lang.Object[r2]
            long r3 = r1.i
            java.lang.Long r3 = java.lang.Long.valueOf(r3)
            r2[r9] = r3
            com.appsee.gd.a(r7, r0, r2)
            long r2 = r1.i
            long r10 = r10 + r2
            r1.i = r10
            r4 = 5
            int r0 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r0 < 0) goto L_0x00c2
            r15.c()
        L_0x00c2:
            com.appsee.jm r0 = r1.e
            r0.d()
            return
        L_0x00c8:
            r0 = move-exception
            goto L_0x00cc
        L_0x00ca:
            r0 = move-exception
            r12 = r8
        L_0x00cc:
            java.util.HashMap<java.lang.Long, java.lang.Long> r5 = r1.p
            long r13 = r1.m
            long r3 = r13 + r10
            r1.m = r3
            java.lang.Long r3 = java.lang.Long.valueOf(r13)
            java.lang.Long r4 = java.lang.Long.valueOf(r17)
            r5.put(r3, r4)
            if (r12 != 0) goto L_0x0103
            java.lang.String r3 = "IGk^'Qf\\qSt\u0012pZb\\'FuKn\\`\u0012s]'Gi^hQl\u0012nF<\u0012d]iAbQrFnDb\u0012eSc\u0012a@f_bA=\u0012\"V"
            java.lang.String r3 = com.appsee.bb.a((java.lang.String) r3)
            java.lang.Object[] r2 = new java.lang.Object[r2]
            long r4 = r1.i
            java.lang.Long r4 = java.lang.Long.valueOf(r4)
            r2[r9] = r4
            com.appsee.gd.a(r7, r3, r2)
            long r2 = r1.i
            long r10 = r10 + r2
            r1.i = r10
            r4 = 5
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 < 0) goto L_0x010c
            r15.c()
            goto L_0x010c
        L_0x0103:
            android.view.Surface r2 = r1.n
            r2.unlockCanvasAndPost(r12)
            r2 = 0
            r1.i = r2
        L_0x010c:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.zi.a(com.appsee.ch, long):void");
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(MediaFormat mediaFormat) throws IOException {
        if (this.t != null) {
            gd.b(2, hn.a("\n{;v\"qny;l+fn} }:}/`'{ 4*q:q-`+p"));
            this.t.stop();
            this.t.release();
            this.t = null;
        }
        gd.a(1, bb.a("N\\nFnSk[}W'WiQhVb@'_rJb@']i\u0012wSsZ:\u0017t\u001e'Th@jSs\u000f\"A"), this.s, mediaFormat.toString());
        this.t = new MediaMuxer(this.s, 0);
        this.c = this.t.addTrack(mediaFormat);
        this.t.start();
    }

    public void a(int i2, int i3, int i4, int i5, String str, boolean z) throws Exception {
        gd.b(1, hn.a("] }:}/x'n+4\u0002{\"x'd!dnu=m wng;f(u-qnb'p+{nq w!p+f"));
        if (Build.VERSION.SDK_INT >= 21) {
            this.h = i2;
            this.f2094a = i3;
            this.o = new Rect(0, 0, this.h, this.f2094a);
            this.u = i4;
            this.d = i5;
            this.s = str;
            this.f = z;
            this.p.clear();
            this.i = 0;
            this.k = 0;
            this.m = 0;
            this.q = new gh(this);
            this.q.start();
            this.q.c();
            return;
        }
        throw new InvalidObjectException(bb.a("K]k^nBhBQ[cWhwiQhVb@'Qf\\i]s\u0012eW'[iAsSiFnSsW']i\u0012k]pWu\u0012F\\c@h[c\u0012qWuAn]iA"));
    }
}
