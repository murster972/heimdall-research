package com.appsee;

import android.view.Display;

public class li {

    /* renamed from: a  reason: collision with root package name */
    private long f2026a;
    private long b;
    private long c;
    private zn d;
    private Dimension[] e = new Dimension[2];

    li() {
        this.e[0] = new Dimension();
        this.e[1] = new Dimension();
    }

    private /* synthetic */ void c() {
        Display b2 = di.b();
        this.d = di.b(b2);
        di.a(this.e[1], b2, di.a(this.d));
        long b3 = di.b();
        this.f2026a = b3;
        this.b = b3;
    }

    /* access modifiers changed from: package-private */
    public Dimension a(boolean z) {
        if (z || di.b() - this.b > 500) {
            c();
        }
        return this.e[1];
    }

    /* access modifiers changed from: package-private */
    public void b() {
        this.d = di.b(di.b());
        this.f2026a = di.b();
    }

    /* renamed from: a  reason: collision with other method in class */
    public zn m53a() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m55a() {
        Display b2 = di.b();
        this.d = di.b(b2);
        di.a(this.e, b2, this.d);
        long b3 = di.b();
        this.f2026a = b3;
        this.b = b3;
        this.c = b3;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public Dimension[] m58a(boolean z) {
        if (z || di.b() - this.c > 500) {
            a();
        }
        return this.e;
    }

    /* renamed from: a  reason: collision with other method in class */
    public zn m54a(boolean z) {
        if (z || di.b() - this.f2026a > 500) {
            b();
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m56a(boolean z) {
        return di.a(a(z));
    }

    /* access modifiers changed from: package-private */
    public Dimension a() {
        return a(false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public Dimension[] m57a() {
        return a(false);
    }
}
