package com.appsee;

import android.os.Handler;
import android.os.Looper;

class bb {

    /* renamed from: a  reason: collision with root package name */
    private Handler f1976a = new Handler(Looper.getMainLooper());
    private final Object b = new Object();
    private Runnable c = null;
    private boolean d = false;

    bb(z zVar, int i) {
        this.c = new kb(this, zVar, i);
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ '2');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 7);
        }
        return new String(cArr);
    }

    public synchronized void b() {
        synchronized (this.b) {
            if (!this.d) {
                this.d = true;
                this.f1976a.post(this.c);
            }
        }
    }

    public synchronized void a() {
        synchronized (this.b) {
            if (this.d) {
                this.d = false;
                this.f1976a.removeCallbacks(this.c);
            }
        }
    }
}
