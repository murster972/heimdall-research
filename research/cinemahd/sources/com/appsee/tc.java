package com.appsee;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

class tc {
    tc() {
    }

    static void a(Object obj, String str, Object obj2) throws Exception {
        Field a2 = a(obj.getClass(), str);
        if (a2 != null) {
            a2.setAccessible(true);
            a2.set(obj, obj2);
        }
    }

    public static <T> T a(Object obj, Class<?> cls) throws IllegalAccessException, IllegalArgumentException {
        Field a2;
        if (obj == null || (a2 = a(obj.getClass(), cls)) == null) {
            return null;
        }
        a2.setAccessible(true);
        return a2.get(obj);
    }

    static StackTraceElement a() {
        return a((Class<?>) Thread.class, qb.a("\u0004\u0010\u0017&\u0017\u0014\u0000\u001e7\u0007\u0002\u0016\u0006"), 3);
    }

    static boolean a(String str) {
        if (yb.a(str)) {
            return false;
        }
        for (StackTraceElement methodName : Thread.currentThread().getStackTrace()) {
            if (methodName.getMethodName().equalsIgnoreCase(str)) {
                return true;
            }
        }
        return false;
    }

    private static /* synthetic */ Field a(Class<?> cls, Class<?> cls2) {
        Class<? super Object> cls3;
        while (cls3 != null) {
            for (Field field : cls3.getDeclaredFields()) {
                if (cls2.isAssignableFrom(field.getType())) {
                    return field;
                }
            }
            Class<? super Object> superclass = cls3.getSuperclass();
            cls3 = cls;
            cls3 = superclass;
        }
        return null;
    }

    private static /* synthetic */ Class<?> a(Class<?> cls) {
        if (cls.isAssignableFrom(Integer.class)) {
            return Integer.TYPE;
        }
        if (cls.isAssignableFrom(Float.class)) {
            return Float.TYPE;
        }
        if (cls.isAssignableFrom(Double.class)) {
            return Double.TYPE;
        }
        return cls.isAssignableFrom(Boolean.class) ? Boolean.TYPE : cls;
    }

    static <T> T a(Object obj, Class<?> cls, String str) throws Exception {
        Field a2 = a(cls, str);
        if (a2 == null) {
            return null;
        }
        a2.setAccessible(true);
        return a2.get(obj);
    }

    static <T> T a(Object obj, String str) throws NoSuchFieldException, IllegalAccessException {
        Field a2 = a(obj.getClass(), str);
        if (a2 == null) {
            return null;
        }
        a2.setAccessible(true);
        return a2.get(obj);
    }

    private static /* synthetic */ Object a(Class<?> cls, Object obj, String str, Class<?>[] clsArr, Object... objArr) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        try {
            Method declaredMethod = cls.getDeclaredMethod(str, clsArr);
            declaredMethod.setAccessible(true);
            return declaredMethod.invoke(obj, objArr);
        } catch (NoSuchMethodException e) {
            Class<? super Object> superclass = cls.getSuperclass();
            if (superclass != null) {
                return a(superclass, obj, str, clsArr, objArr);
            }
            throw e;
        }
    }

    private static /* synthetic */ StackTraceElement a(Class<?> cls, String str, int i) {
        Class<?> cls2;
        int i2;
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        if (stackTrace == null) {
            return null;
        }
        for (int i3 = 0; i3 < stackTrace.length; i3++) {
            StackTraceElement stackTraceElement = stackTrace[i3];
            try {
                cls2 = Class.forName(stackTraceElement.getClassName());
            } catch (ClassNotFoundException unused) {
                gd.a(1, md.a(":\u0002\u0017\r\u0016\u0017Y\u0005\u0010\r\u001dC\u001a\u000f\u0018\u0010\nYYF\n"), stackTraceElement.getClassName());
                cls2 = null;
            }
            if (cls2 != null && cls.isAssignableFrom(cls2) && stackTraceElement.getMethodName().equalsIgnoreCase(str) && (i2 = i3 + i) < stackTrace.length) {
                return stackTrace[i2];
            }
        }
        return null;
    }

    private static /* synthetic */ Field a(Class<?> cls, String str) throws NoSuchFieldException {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException e) {
            Class<? super Object> superclass = cls.getSuperclass();
            if (superclass != null) {
                return a((Class<?>) superclass, str);
            }
            throw e;
        }
    }

    static Object a(Object obj, String str, int i, Object... objArr) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        Class[] clsArr = new Class[i];
        for (int i2 = 0; i2 < i; i2++) {
            clsArr[i2] = objArr[i2] == null ? Object.class : a((Class<?>) objArr[i2].getClass());
        }
        return a(obj.getClass(), obj, str, clsArr, objArr);
    }

    static Object a(Object obj, String str, Class<?>[] clsArr, Object... objArr) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException {
        return a(obj.getClass(), obj, str, clsArr, objArr);
    }
}
