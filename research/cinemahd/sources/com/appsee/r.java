package com.appsee;

import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.PopupWindow;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class r {
    private static HashMap<Class<?>, String> i = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    private Window.Callback f2057a;
    private View.OnTouchListener b;
    private WeakReference<List<View>> c;
    private WeakReference<Object> d;
    private WeakReference<Menu> e;
    private v f;
    private WeakReference<View> g;
    private Window.Callback h;

    static {
        i.clear();
        a(jm.a("&T#H(S#\u00144O7J(H3\u00141\ri[7Ji{$N.U)x&H\u0006Y3S1S3C\u0003_+_ [3_\u000ey\u0014\u001e\u0010S)^(M\u0004[+V%[$Q\u0010H&J7_5"), qb.a("\u00184\u0007\u0002\u0005\u0013\u0010\u0007"));
        a(jm.a("[)^5U.^iL._0\u0014\u0010S)^(M\u0004[+V%[$Q\u0010H&J7_5"), qb.a("\u00184\u0007\u0002\u0005\u0013\u0010\u0007"));
        a(jm.a("[)^5U.^iI2J7U5NiLp\u0014.T3_5T&ViL._0\u0014\u0010S)^(M\u0004[+V%[$Q\u0010H&J7_5"), qb.a("\u00184\u0007\u0002\u0005\u0013\u0010\u0007"));
        a(jm.a("&T#H(S#\u00144O7J(H3\u00141\riL._0\u0014\u0010S)^(M\u0004[+V%[$Q\u0010H&J7_5"), qb.a("\u00184\u0007\u0002\u0005\u0013\u0010\u0007"));
        a(jm.a("U5]iY/H(W.O*\u0014%[4_i{7J+S$[3S(T\u0014N&N2I\n[)[ _5\u001eu"), qb.a("\u000e6\u0002\u0019\u000f\u0017\u0002\u0016\b"));
    }

    public r(Object obj, View view, v vVar) throws Exception {
        this.d = new WeakReference<>(obj);
        this.g = new WeakReference<>(view);
        this.f = vVar;
        if (obj == null) {
            return;
        }
        if (obj instanceof Window) {
            this.f2057a = ((Window) obj).getCallback();
            if (this.f2057a == null) {
                qe.a((Throwable) null, qb.a(" \u0014\r\u001b\f\u0001C\u0013\n\u001b\u0007U4\u001c\r\u0011\f\u0002C\u0001\f\u0000\u0000\u001dC\u0001\u0002\u0007\u0004\u0010\u0017"));
            }
        } else if (obj instanceof PopupWindow) {
            this.b = (View.OnTouchListener) tc.a(obj, jm.a("*n(O$R\u000eT3_5Y\"J3U5"));
        }
    }

    private static /* synthetic */ void a(String str, String str2) {
        try {
            Class<?> cls = Class.forName(str);
            if (cls != null) {
                i.put(cls, str2);
            }
        } catch (ClassNotFoundException unused) {
        }
    }

    /* access modifiers changed from: package-private */
    public Window.Callback b() throws Exception {
        Window.Callback a2 = a();
        if (a2 == null) {
            return null;
        }
        if (this.h == null) {
            Window.Callback a3 = a(a2);
            int i2 = 1;
            while (a3 != null && i2 < pg.h().c()) {
                i2++;
                this.h = a3;
                a3 = a(a3);
            }
        }
        if (this.h == null) {
            this.h = this.f2057a;
        }
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public void c() throws Exception {
        List a2;
        if (this.f != null && xd.b().d() && pg.h().H() && pg.h().i() && (a2 = a()) != null && !a2.isEmpty()) {
            b(a2);
            if (a() != null) {
                this.f.a(this, a2);
            }
        }
    }

    public String toString() {
        String str;
        try {
            Object a2 = a();
            View a3 = a();
            boolean z = a3 != null && a3.isShown();
            if (a2 == null) {
                str = jm.a("\u001ct\b\u001a\u0010s\t~\bm\u001a");
            } else {
                StringBuilder insert = new StringBuilder().insert(0, a2.getClass().getName());
                insert.append(qb.a("X\u000b\u0014\u0010\u001dY"));
                insert.append(a2.hashCode());
                str = insert.toString();
            }
            Window.Callback callback = null;
            try {
                callback = b();
            } catch (Exception e2) {
                StringBuilder insert2 = new StringBuilder().insert(0, jm.a("5H(Hg]\"N3S)]g^\"_7_4NgY&V+X&Y,\u001a!U5\u0000g"));
                insert2.append(str);
                qe.a(e2, insert2.toString());
            }
            String a4 = qb.a("F\u0006YP\u0010OF\u0017YP\u0010");
            Object[] objArr = new Object[4];
            objArr[0] = str;
            objArr[1] = a3 != null ? a3.getClass().getName() : jm.a("a\tU\u0015U(N\u0011S\"M\u001a");
            objArr[2] = Boolean.valueOf(z);
            objArr[3] = callback != null ? callback.getClass().getName() : qb.a(";\f6\u0002\u0019\u000f\u0017\u0002\u0016\b");
            return String.format(a4, objArr);
        } catch (Exception e3) {
            qe.a(e3, jm.a("\u0002H5U5\u001a.Tgm.T#U0m5[7J\"HiN(i3H.T \u0012n"));
            return super.toString();
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b  reason: collision with other method in class */
    public void m116b() throws Exception {
        if (pg.h().p() && xd.b().d()) {
            Object obj = this.d.get();
            if (obj instanceof Window) {
                if (!(a() instanceof t)) {
                    ((Window) obj).setCallback(new t(this));
                    c();
                }
            } else if ((obj instanceof PopupWindow) && !(this.b instanceof d)) {
                ((PopupWindow) obj).setTouchInterceptor(new d(this, (e) null));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Menu a() {
        WeakReference<Menu> weakReference = this.e;
        if (weakReference != null) {
            return (Menu) weakReference.get();
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(List<View> list) {
        for (View next : list) {
            if (!(next.getTouchDelegate() instanceof n)) {
                next.setTouchDelegate(new n(this, next));
            } else {
                return;
            }
        }
    }

    private /* synthetic */ List<View> a(View view) throws Exception {
        if (view == null || !view.isShown()) {
            return null;
        }
        List<View> e2 = rb.e(view);
        for (int size = e2.size() - 1; size >= 0; size--) {
            if (!e2.get(size).isShown()) {
                e2.remove(size);
            }
        }
        return e2;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: b  reason: collision with other method in class */
    public List<View> m115b() {
        WeakReference<List<View>> weakReference = this.c;
        if (weakReference != null) {
            return (List) weakReference.get();
        }
        return null;
    }

    private /* synthetic */ void b(List<View> list) throws Exception {
        if (this.d.get() instanceof PopupWindow) {
            List a2 = ub.a().a();
            int indexOf = a2.indexOf(this);
            for (int i2 = 0; i2 < indexOf; i2++) {
                r rVar = (r) a2.get(i2);
                if (rVar.a() != null) {
                    rVar.b(list);
                }
            }
            return;
        }
        Menu a3 = a();
        if (a3 != null) {
            this.c = new WeakReference<>(list);
            for (int size = list.size() - 1; size >= 0; size--) {
                MenuItem e2 = rb.e(list.get(size));
                int i3 = 0;
                while (i3 < a3.size() && !a3.getItem(i3).equals(e2)) {
                    i3++;
                }
                if (i3 >= a3.size()) {
                    list.remove(size);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m113a() throws Exception {
        Window.Callback b2 = b();
        if (!zo.f() && (b2 instanceof Activity)) {
            Object a2 = tc.a((Object) b2, qb.a("\u000e8\u0002\u001c\r!\u000b\u0007\u0006\u0014\u0007"));
            kd b3 = zo.b().b();
            tc.a((Object) b2, jm.a("W\u000eT4N5O*_)N&N.U)"), (Object) b3);
            tc.a(a2, qb.a("\u0018*\u001b\u0010\u0001\u0011\u0000\u000e\u0010\r\u0001\u0002\u0001\n\u001a\r"), (Object) b3);
            zo.b().a((Activity) b2, true);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public View m110a() {
        return (View) this.g.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public Window m111a() {
        Object obj = this.d.get();
        if (obj instanceof Window) {
            return (Window) obj;
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void a(Activity activity) {
        if (activity == null) {
            return;
        }
        if (activity.equals(this.f2057a) || activity.equals(this.h)) {
            this.f2057a = null;
            this.h = null;
        }
    }

    private /* synthetic */ Window.Callback a(Window.Callback callback) throws Exception {
        for (Map.Entry next : i.entrySet()) {
            if (((Class) next.getKey()).isInstance(callback)) {
                return (Window.Callback) tc.a((Object) callback, (String) next.getValue());
            }
        }
        if (pg.h().J()) {
            return (Window.Callback) tc.a((Object) callback, (Class<?>) Window.Callback.class);
        }
        return null;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(MotionEvent motionEvent) throws Exception {
        a(motionEvent, a());
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(MotionEvent motionEvent, View view) throws Exception {
        wb.a().a(motionEvent, view);
    }

    /* access modifiers changed from: private */
    public /* synthetic */ void a(Menu menu) {
        if (menu == null) {
            this.e = null;
            this.c = null;
            return;
        }
        this.e = new WeakReference<>(menu);
    }

    /* renamed from: a  reason: collision with other method in class */
    public Object m112a() {
        return this.d.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m114a() throws Exception {
        return (this.d.get() instanceof Window) && rb.a(b());
    }
}
