package com.appsee;

import java.util.Locale;

public class Dimension {

    /* renamed from: a  reason: collision with root package name */
    private int f1969a;
    private int b;

    public Dimension() {
    }

    public int a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public double b() {
        return ((double) this.f1969a) / ((double) this.b);
    }

    public int c() {
        return this.f1969a;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || Dimension.class != obj.getClass()) {
            return false;
        }
        Dimension dimension = (Dimension) obj;
        if (this.f1969a == dimension.f1969a && this.b == dimension.b) {
            return true;
        }
        return false;
    }

    public String toString() {
        return String.format(Locale.US, "%dx%d", new Object[]{Integer.valueOf(this.f1969a), Integer.valueOf(this.b)});
    }

    public Dimension(int i, int i2) {
        this.f1969a = i;
        this.b = i2;
    }

    public void a(int i) {
        this.b = i;
    }

    /* access modifiers changed from: package-private */
    public void a(int i, int i2) {
        this.f1969a = i;
        this.b = i2;
    }
}
