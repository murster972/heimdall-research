package com.appsee;

import android.os.Looper;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import ob.appsee.ObTester;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class wl implements k {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2080a;
    private List<fl> b;
    private String c;
    private String d;
    private Throwable e;

    public wl(Throwable th) {
        a(th.getClass().getName(), th.getMessage(), th, true, oj.a());
    }

    private /* synthetic */ void a(String str, String str2, Throwable th, boolean z, boolean z2) {
        this.d = str;
        this.c = str2;
        this.e = th;
        this.b = new ArrayList();
        this.f2080a = z2;
        Map<Thread, StackTraceElement[]> allStackTraces = Thread.getAllStackTraces();
        try {
            Thread thread = Looper.getMainLooper().getThread();
            if (!allStackTraces.containsKey(thread)) {
                allStackTraces.put(thread, thread.getStackTrace());
            }
        } catch (Throwable th2) {
            qe.a(th2, ch.a("e`HoIu\u0006`Be\u0006lGhH!RiTdGe\u0006rR`Ej\u0006uT`Ed\u0006uI!EsGrN!JnA"));
        }
        for (Thread next : allStackTraces.keySet()) {
            boolean equals = next.equals(Thread.currentThread());
            StackTraceElement[] stackTraceElementArr = allStackTraces.get(next);
            if (equals) {
                stackTraceElementArr = a(stackTraceElementArr);
            }
            this.b.add(new fl(this, next.getId(), next.getName(), z && equals, stackTraceElementArr));
        }
    }

    public wl(int i, boolean z) {
        StringBuilder insert = new StringBuilder().insert(0, ch.a("RoF\u001c"));
        insert.append(i);
        a(insert.toString(), (String) null, (Throwable) null, false, z);
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m155a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(ch.a("U_qC"), this.d);
        jSONObject.put(ch.a("kdUrGfC"), this.c);
        String a2 = ch.a("l`P`cyEdVuOnH");
        Throwable th = this.e;
        jSONObject.put(a2, th == null ? null : a(th));
        jSONObject.put(ch.a("NDUCrROGlC"), ObTester.a());
        jSONObject.put(ch.a("GTnKLGhHUNsC`B"), this.f2080a);
        JSONArray jSONArray = new JSONArray();
        for (fl a3 : this.b) {
            jSONArray.put(a3.a());
        }
        jSONObject.put(ch.a("riTdGeU"), jSONArray);
        return jSONObject;
    }

    public Throwable a() {
        return this.e;
    }

    private /* synthetic */ JSONObject a(Throwable th) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        StackTraceElement[] stackTrace = th.getStackTrace();
        int length = stackTrace.length;
        int i = 0;
        while (i < length) {
            StackTraceElement stackTraceElement = stackTrace[i];
            i++;
            jSONArray.put(a(stackTraceElement));
        }
        jSONObject.put(ch.a("kR"), jSONArray);
        jSONObject.put(ch.a("K"), th.getMessage());
        jSONObject.put(ch.a("H"), th.getClass().getName());
        Throwable cause = th.getCause();
        if (cause != null) {
            jSONObject.put(ch.a("E"), a(cause));
        }
        return jSONObject;
    }

    /* access modifiers changed from: private */
    public /* synthetic */ JSONObject a(StackTraceElement stackTraceElement) throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(ch.a("E"), stackTraceElement.getClassName());
        jSONObject.put(ch.a("K"), stackTraceElement.getMethodName());
        if (stackTraceElement.isNativeMethod()) {
            jSONObject.put(ch.a("H"), true);
            return jSONObject;
        }
        if (!yb.a(stackTraceElement.getFileName())) {
            jSONObject.put(ch.a("@"), stackTraceElement.getFileName());
            if (stackTraceElement.getLineNumber() >= 0) {
                jSONObject.put(ch.a("J"), stackTraceElement.getLineNumber());
            }
        }
        return jSONObject;
    }

    private /* synthetic */ StackTraceElement[] a(StackTraceElement[] stackTraceElementArr) {
        if (stackTraceElementArr == null) {
            return stackTraceElementArr;
        }
        ArrayList arrayList = new ArrayList();
        int length = stackTraceElementArr.length - 1;
        while (length >= 0 && !stackTraceElementArr[length].getClassName().equals(xh.class.getName()) && !stackTraceElementArr[length].getClassName().equals(mp.class.getName())) {
            arrayList.add(stackTraceElementArr[length]);
            length--;
        }
        Collections.reverse(arrayList);
        return (StackTraceElement[]) arrayList.toArray(new StackTraceElement[arrayList.size()]);
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m156a() {
        return this.f2080a;
    }
}
