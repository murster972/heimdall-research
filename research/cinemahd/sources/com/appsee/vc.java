package com.appsee;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

class vc {
    protected static String c = "c03e86fd74e090eadb4dc2f5e57b1842";
    protected static final Object d = new Object();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public HttpRequestBase f2073a = null;
    private BasicCookieStore b = new BasicCookieStore();

    vc() {
    }

    private /* synthetic */ void b(HttpRequestBase httpRequestBase) {
        synchronized (d) {
            this.f2073a = httpRequestBase;
        }
    }

    public void a() {
        synchronized (d) {
            if (this.f2073a != null && !this.f2073a.isAborted()) {
                Thread thread = new Thread(new jc(this), sd.a("8#\t \u001c676\r$\u0016!\u0012\u0000\r<\t#\u0010=\u001e\u0007\u0011!\u001c2\u001d"));
                try {
                    thread.start();
                    thread.join();
                } catch (InterruptedException e) {
                    qe.a(e, sd.a("<!\u000b<\u000bs*'\u0016#\t:\u00174Y=\u001c'\u000e<\u000b8Y0\u0018?\u0015"));
                }
            }
        }
    }

    private /* synthetic */ JSONObject a(String str, HashMap<String, String> hashMap, HttpEntity httpEntity, int i) throws Exception {
        DefaultHttpClient defaultHttpClient = new DefaultHttpClient();
        HttpParams params = defaultHttpClient.getParams();
        HttpConnectionParams.setConnectionTimeout(params, i);
        HttpConnectionParams.setSoTimeout(params, i);
        HttpPost httpPost = new HttpPost(str);
        httpPost.setHeader(sd.a("\u0012\u001a0\u001c#\r"), sd.a("2\t#\u0015:\u001a2\r:\u0016=V9\n<\u0017"));
        for (Map.Entry next : hashMap.entrySet()) {
            httpPost.setHeader((String) next.getKey(), (String) next.getValue());
        }
        a((HttpRequestBase) httpPost);
        httpPost.setEntity(httpEntity);
        BasicResponseHandler basicResponseHandler = new BasicResponseHandler();
        defaultHttpClient.setCookieStore(this.b);
        b(httpPost);
        try {
            String str2 = (String) defaultHttpClient.execute(httpPost, basicResponseHandler);
            a((HttpRequestBase) null, defaultHttpClient);
            return a(str2);
        } catch (Exception e) {
            throw e;
        } catch (Throwable th) {
            a((HttpRequestBase) null, defaultHttpClient);
            throw th;
        }
    }

    /* access modifiers changed from: protected */
    public byte[] a(byte[] bArr, int i, String str, Map<String, String> map, String str2) {
        StringBuilder sb = new StringBuilder(550);
        for (Map.Entry next : map.entrySet()) {
            sb.append(sd.a("~T"));
            sb.append(str2);
            sb.append(sd.a("^s"));
            sb.append(sd.a("\u0010\u0016=\r6\u0017'T\u0017\u0010 \t<\n:\r:\u0016=Cs\u001f<\u000b>T7\u0018'\u0018hY=\u0018>\u001cn["));
            sb.append((String) next.getKey());
            sb.append(sd.a("[^s^s"));
            sb.append((String) next.getValue());
            sb.append(sd.a("^s"));
        }
        sb.append(sd.a("~T"));
        sb.append(str2);
        sb.append(sd.a("^s"));
        sb.append(sd.a(":<\u0017'\u001c=\r~=:\n#\u0016 \u0010'\u0010<\u0017iY5\u0016!\u0014~\u001d2\r2Bs\u00172\u00146Dq\u001f:\u00156[hY5\u0010?\u001c=\u0018>\u001cn["));
        sb.append(str);
        sb.append(sd.a("qB^s"));
        sb.append(sd.a("\u0010\u0016=\r6\u0017'T\u0007\u0000#\u001ciY2\t#\u0015:\u001a2\r:\u0016=V<\u001a'\u001c'T \r!\u001c2\u0014^s^s"));
        String format = String.format(sd.a("^s~Tv\n~T^s"), new Object[]{str2});
        byte[] a2 = yb.a(sb.toString());
        byte[] a3 = yb.a(format);
        byte[] bArr2 = new byte[(a2.length + i + a3.length)];
        System.arraycopy(a2, 0, bArr2, 0, a2.length);
        System.arraycopy(bArr, 0, bArr2, a2.length, i);
        System.arraycopy(a3, 0, bArr2, a2.length + i, a3.length);
        return bArr2;
    }

    /* access modifiers changed from: protected */
    public JSONObject a(String str, JSONObject jSONObject, int i) throws Exception {
        HashMap hashMap = new HashMap();
        hashMap.put(sd.a("\u0010\u0016=\r6\u0017'T'\u0000#\u001c"), sd.a("2\t#\u0015:\u001a2\r:\u0016=V9\n<\u0017"));
        return a(str, hashMap, new StringEntity(jSONObject.toString(), sd.a(",\u0007?~A")), i);
    }

    /* access modifiers changed from: protected */
    public JSONObject a(String str) throws JSONException, IllegalArgumentException {
        JSONObject jSONObject = new JSONObject(str);
        String optString = jSONObject.optString(sd.a("<!\u000b<\u000b"), (String) null);
        if (optString == null) {
            return jSONObject;
        }
        throw new IllegalArgumentException(optString);
    }

    public JSONObject a(JSONObject jSONObject, int i) throws Exception {
        return a(String.format(sd.a("\\ V0\u0016=\u001f:\u001e"), new Object[]{Appsee.f1964a}), jSONObject, i);
    }

    public JSONObject a(byte[] bArr, int i, String str, String str2, long j, long j2) throws Exception {
        String format = String.format(sd.a("\\ V&\t?\u00162\u001d"), new Object[]{Appsee.f1964a});
        HashMap hashMap = new HashMap();
        String str3 = str;
        hashMap.put(sd.a("*6\n \u0010<\u0017\u001a\u001d"), str);
        hashMap.put(sd.a("\u0010\f!\u000b6\u0017'0=\u001d6\u0001"), Long.toString(j));
        hashMap.put(sd.a("-<\r2\u0015\u0000\u0010)\u001c"), Long.toString(j2));
        return a(format, bArr, i, str2, (Map<String, String>) hashMap, pg.h().l());
    }

    private /* synthetic */ void a(HttpRequestBase httpRequestBase, DefaultHttpClient defaultHttpClient) {
        defaultHttpClient.getConnectionManager().shutdown();
        b(httpRequestBase);
    }

    /* access modifiers changed from: protected */
    public JSONObject a(String str, byte[] bArr, int i, String str2, Map<String, String> map, int i2) throws Exception {
        String format = String.format(sd.a("~T~T~T~T~Tv\n"), new Object[]{UUID.randomUUID().toString().replace(sd.a("T"), "")});
        byte[] a2 = a(bArr, i, str2, map, format);
        HashMap hashMap = new HashMap();
        hashMap.put(sd.a("\u0010\u0016=\r6\u0017'T'\u0000#\u001c"), String.format(sd.a(">\f?\r:\t2\u000b'V5\u0016!\u0014~\u001d2\r2Bs\u001b<\f=\u001d2\u000b*Dv\n"), new Object[]{format}));
        ByteArrayEntity byteArrayEntity = new ByteArrayEntity(a2);
        String str3 = str;
        return a(str, hashMap, byteArrayEntity, i2);
    }

    private /* synthetic */ void a(HttpRequestBase httpRequestBase) {
        httpRequestBase.addHeader(sd.a("\u0010\u0015:\u001c=\r\u001a\u001d"), pg.h().e());
        httpRequestBase.addHeader(sd.a("\u0012)\u001a26\u0000"), pg.h().h());
        httpRequestBase.addHeader(sd.a("/6\u000b \u0010<\u0017"), Appsee.b);
        httpRequestBase.addHeader(sd.a("\u0003\u00152\r5\u0016!\u0014"), sd.a("H"));
        httpRequestBase.addHeader(sd.a("8\u00030\u0012\f'\u0011"), c);
    }
}
