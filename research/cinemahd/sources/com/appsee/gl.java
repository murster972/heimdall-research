package com.appsee;

import android.content.SharedPreferences;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class gl implements SharedPreferences {
    static final String c = String.format(sd.a("\\ Wv\n"), new Object[]{"Appsee", "json"});
    private static gl d;

    /* renamed from: a  reason: collision with root package name */
    private final wg f2000a = new wg(this);
    /* access modifiers changed from: private */
    public final Map<String, Object> b = new ConcurrentHashMap();

    private /* synthetic */ gl() {
        a();
    }

    public boolean contains(String str) {
        return this.b.containsKey(str);
    }

    public SharedPreferences.Editor edit() {
        return this.f2000a;
    }

    public Map<String, ?> getAll() {
        return this.b;
    }

    public boolean getBoolean(String str, boolean z) {
        return this.b.containsKey(str) ? ((Boolean) this.b.get(str)).booleanValue() : z;
    }

    public float getFloat(String str, float f) {
        return this.b.containsKey(str) ? ((Float) this.b.get(str)).floatValue() : f;
    }

    public int getInt(String str, int i) {
        return this.b.containsKey(str) ? ((Integer) this.b.get(str)).intValue() : i;
    }

    public long getLong(String str, long j) {
        return this.b.containsKey(str) ? ((Long) this.b.get(str)).longValue() : j;
    }

    public String getString(String str, String str2) {
        return this.b.containsKey(str) ? (String) this.b.get(str) : str2;
    }

    public Set<String> getStringSet(String str, Set<String> set) {
        return this.b.containsKey(str) ? (Set) this.b.get(str) : set;
    }

    public void registerOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    public void unregisterOnSharedPreferenceChangeListener(SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
    }

    public static synchronized gl a() {
        gl glVar;
        synchronized (gl.class) {
            if (d == null) {
                d = new gl();
            }
            glVar = d;
        }
        return glVar;
    }
}
