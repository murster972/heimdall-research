package com.appsee;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaCodecInfo;
import android.media.MediaCodecList;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.view.Surface;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;
import org.jcodec.AVCMP4Mux;

@TargetApi(16)
class cf implements j {

    /* renamed from: a  reason: collision with root package name */
    private ByteBuffer f1983a;
    private MediaCodec b;
    private String c;
    private boolean d;
    private int e;
    private ByteBuffer[] f;
    private ByteBuffer[] g;
    private FileChannel h;
    private String i;
    private int j;
    private boolean k;
    private int l;
    private jm m = oc.a().a(cc.a("dWXWkXMYJ_@Q"));
    private int n;
    private List<Long> o = new ArrayList();
    private MediaCodec.BufferInfo p;
    private jm q = oc.a().a(cc.a("s@UARKCWIS"));
    private jm r = oc.a().a(md.a(" 6/ \u0016\r\u000f\u0006\u000b\u0010\u0010\f\u0017"));
    private boolean s = false;
    private long t;

    cf() {
    }

    static List<String> a() {
        int codecCount = MediaCodecList.getCodecCount();
        ArrayList arrayList = new ArrayList(codecCount);
        for (int i2 = 0; i2 < codecCount; i2++) {
            MediaCodecInfo codecInfoAt = MediaCodecList.getCodecInfoAt(i2);
            if (codecInfoAt.isEncoder() && !a(codecInfoAt.getName())) {
                String[] supportedTypes = codecInfoAt.getSupportedTypes();
                int length = supportedTypes.length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (supportedTypes[i3].equalsIgnoreCase("video/avc")) {
                        arrayList.add(codecInfoAt.getName());
                        break;
                    } else {
                        i3++;
                    }
                }
            }
        }
        return arrayList;
    }

    private /* synthetic */ void b() throws Exception {
        File a2 = lg.a(this.c);
        new AVCMP4Mux().a(a2, this.i, this.o);
        lg.b(a2);
    }

    private /* synthetic */ void c() throws Exception {
        int dequeueInputBuffer = this.b.dequeueInputBuffer(1000000);
        if (dequeueInputBuffer >= 0) {
            this.f[dequeueInputBuffer].clear();
            this.b.queueInputBuffer(dequeueInputBuffer, 0, 0, this.t, 4);
            if (this.k) {
                gd.b(1, cc.a("jDO_@_@Q\u000eS@UARKD\u000ePAD\u000eBFS\u000eZOEZ\u0016Z_CS"));
            }
            a(true);
            return;
        }
        throw new Exception(md.a(":\u0002\u0017\r\u0016\u0017Y\u0004\u001c\u0017Y\n\u0017\u0013\f\u0017Y\u0001\f\u0005\u001f\u0006\u000bC\u0018\u0005\r\u0006\u000bCHC\n\u0006\u001aC\u000e\u0002\u0010\u0017Y\u0005\u0016\u0011Y&60Y\u0006\u0017\u0000\u0016\u0007\u0010\r\u001e"));
    }

    private /* synthetic */ void d() throws Exception {
        List h2 = pg.h().h();
        if (h2 == null || h2.isEmpty()) {
            throw new Exception(md.a("-\u0016C\n\u0016\t\u0013\u0016\u0011\r\u0006\u001dC\u001c\r\u001a\f\u001d\u0006\u000b\u0010Y\u0011\u001c\u0017\f\u0011\u0017\u0006\u001dC\u001f\u0011\u0016\u000eY\u0010\u001c\u0011\u000f\u0006\u000b"));
        }
        this.b = MediaCodec.createByCodecName(((i) h2.get(0)).b);
        int intValue = ((i) h2.get(0)).f2006a.intValue();
        this.s = intValue == 19 || intValue == 20;
        MediaFormat createVideoFormat = MediaFormat.createVideoFormat("video/avc", this.l, this.n);
        createVideoFormat.setInteger(cc.a("MYBY\\\u001bHY\\[OB"), intValue);
        createVideoFormat.setInteger(md.a("\u001b\n\r\u0011\u0018\u0017\u001c"), this.j);
        createVideoFormat.setInteger(cc.a("HDO[K\u001b\\WZS"), this.e);
        createVideoFormat.setInteger(md.a("\nT\u0005\u000b\u0002\u0014\u0006T\n\u0017\u0017\u001c\u0011\u000f\u0002\u0015"), 1);
        this.b.configure(createVideoFormat, (Surface) null, (MediaCrypto) null, 1);
        this.b.start();
        this.f = this.b.getInputBuffers();
        this.g = this.b.getOutputBuffers();
        File a2 = lg.a(this.c);
        gd.a(1, cc.a("u\\SOBGXI\u0016F\u0004\u0018\u0002\u000ePGZK\u0016OB\u0014\u0016\u000bE"), a2.getAbsolutePath());
        this.h = new FileOutputStream(a2).getChannel();
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m10a() {
        return this.d;
    }

    public void a(int i2, int i3, int i4, int i5, String str, boolean z) throws Exception {
        gd.b(1, md.a("0\r\u0010\u0017\u0010\u0002\u0015\n\u0003\u0006Y\t\u0018\u0015\u0018C\u000f\n\u001d\u0006\u0016C\u001c\r\u001a\f\u001d\u0006\u000b"));
        this.l = i2;
        this.n = i3;
        this.j = i4;
        this.e = i5;
        this.i = str;
        this.c = String.format(cc.a("\u0013]\u0018\u000bE"), new Object[]{pg.h().m(), "h264"});
        this.k = z;
        this.p = new MediaCodec.BufferInfo();
        this.o.clear();
        d();
    }

    private static /* synthetic */ boolean a(String str) {
        if (!str.toLowerCase().startsWith(cc.a("YCN\u0000QAYIZK\u0018")) && str.toLowerCase().startsWith(md.a("\f\u0014\u001bW"))) {
            return false;
        }
        return true;
    }

    public void a(ch chVar, long j2) throws Exception {
        this.q.b();
        if (this.k) {
            gd.b(1, cc.a("kXMYJ_@Q\u000eP\\WCS"));
        }
        a(chVar);
        this.m.b();
        a(j2);
        if (this.k) {
            gd.b(1, md.a("'\u000b\u0002\u0010\r\u0010\r\u001eC\u001c\r\u001a\f\u001d\u0006\u000b"));
        }
        a(false);
        this.m.d();
        this.q.d();
    }

    private /* synthetic */ void a(boolean z) throws Exception {
        int dequeueOutputBuffer = this.b.dequeueOutputBuffer(this.p, 0);
        if (this.k) {
            gd.a(1, cc.a("jS_CKCKR\u000eS@UARKD\u000eP\\WCS\u0014\u0016\u000bR"), Integer.valueOf(dequeueOutputBuffer));
        }
        while (true) {
            if (dequeueOutputBuffer == -1) {
                if (!z) {
                    return;
                }
            } else if (dequeueOutputBuffer != -2) {
                if (dequeueOutputBuffer == -3) {
                    this.g = this.b.getOutputBuffers();
                } else if (dequeueOutputBuffer >= 0) {
                    ByteBuffer duplicate = this.g[dequeueOutputBuffer].duplicate();
                    if (duplicate.position() != this.p.offset) {
                        gd.a(1, md.a("?\f\f\r\u001dC\u001b\u0002\u001dC\u001c\r\u001a\f\u001d\u0006\u000bC\u001b\u0016\u001f\u0005\u001c\u0011Y\u0013\u0016\u0010\u0010\u0017\u0010\f\u0017YYF\u001d"), Integer.valueOf(duplicate.position()));
                    }
                    duplicate.position(this.p.offset);
                    MediaCodec.BufferInfo bufferInfo = this.p;
                    duplicate.limit(bufferInfo.offset + bufferInfo.size);
                    this.h.write(duplicate.slice());
                    MediaCodec.BufferInfo bufferInfo2 = this.p;
                    if ((bufferInfo2.flags & 2) != 0) {
                        if (this.k) {
                            gd.a(1, cc.a("hY[XJ\u0016MY@PGQ\u000eP\\WCS\u000e_@\u0016]_TS\u0014\u0016\u000bR"), Integer.valueOf(this.p.size));
                        }
                        this.p.size = 0;
                    } else if (bufferInfo2.size > 0) {
                        this.o.add(Long.valueOf(bufferInfo2.presentationTimeUs));
                    }
                    this.b.releaseOutputBuffer(dequeueOutputBuffer, false);
                    if ((this.p.flags & 4) != 0) {
                        if (this.k) {
                            gd.b(1, md.a("%\u0016\u0016\u0017\u0007Y&60Y\u0005\u0015\u0002\u001e"));
                            return;
                        }
                        return;
                    }
                }
            }
            dequeueOutputBuffer = this.b.dequeueOutputBuffer(this.p, 0);
            if (this.k) {
                gd.a(1, cc.a("jS_CKCKR\u000eS@UARKD\u000eP\\WCS\u0014\u0016\u000bR"), Integer.valueOf(dequeueOutputBuffer));
            }
        }
    }

    private /* synthetic */ void a(long j2) throws Exception {
        int dequeueInputBuffer = this.b.dequeueInputBuffer(1000000);
        if (dequeueInputBuffer >= 0) {
            ByteBuffer byteBuffer = this.f[dequeueInputBuffer];
            byteBuffer.clear();
            byteBuffer.put(this.f1983a);
            MediaCodec mediaCodec = this.b;
            int capacity = this.f1983a.capacity();
            this.t = j2;
            mediaCodec.queueInputBuffer(dequeueInputBuffer, 0, capacity, j2, 0);
            return;
        }
        throw new Exception(md.a(" \u0018\r^\u0017Y\u0004\u001c\u0017Y\u0002\u0017C\u0018\u0015\u0018\n\u0015\u0002\u001b\u000f\u001cC\u001c\r\u001a\f\u001d\u0006\u000bD\nC\u0010\r\t\u0016\rC\u001b\u0016\u001f\u0005\u001c\u0011Y\u0002\u001f\u0017\u001c\u0011YRY\u0010\u001c\u0000Y\u0014\u0018\n\r"));
    }

    /* JADX INFO: finally extract failed */
    /* renamed from: a  reason: collision with other method in class */
    public void m9a() throws Exception {
        if (this.b != null) {
            c();
            this.r.c();
            this.m.c();
            this.q.c();
            gd.b(1, md.a("0\r\f\t\u0013\u0010\r\u001eC\u001c\r\u001a\f\u001d\u0006\u000b"));
            this.b.stop();
            this.b.release();
            if (this.h != null) {
                gd.b(1, cc.a("hZ[EF_@Q\u000e^\u001c\u0000\u001a\u0016H_BS"));
                this.h.close();
            }
            try {
                this.d = true;
                jm a2 = oc.a().a(md.a(".\f\u001b\u0010\r\u001e"));
                a2.b();
                b();
                a2.d();
                a2.c();
                this.d = false;
            } catch (Throwable th) {
                this.d = false;
                throw th;
            }
        } else {
            throw new Exception(cc.a("kXMYJS\\\u0016GE\u000eX[ZB\u0017"));
        }
    }

    private /* synthetic */ void a(ch chVar) {
        this.r.b();
        if (this.f1983a == null) {
            gd.b(1, md.a("0\r\u0010\u0017\u0010\u0002\u0015\n\u0003\u0006Y\u001a\f\u0015Y\u0001\f\u0005\u001f\u0006\u000b"));
            this.f1983a = ByteBuffer.allocateDirect(((this.l * this.n) * 3) / 2);
        }
        this.f1983a.clear();
        AppseeNativeExtensions.a(this.l, this.n, chVar.b(), this.f1983a, this.s, pg.h().Q(), this.k);
        this.r.d();
    }
}
