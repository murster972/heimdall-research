package com.appsee;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

class mj {

    /* renamed from: a  reason: collision with root package name */
    private static int f2032a = 128;
    private static int b = 1000;
    private static int c = 16;

    mj() {
    }

    static byte[] a(byte[] bArr, String str) throws Exception {
        Cipher instance = Cipher.getInstance(zo.a("z,hFx+xFk\"x:\u000e9Z\r_\u0000U\u000e"));
        byte[] bArr2 = new byte[c];
        System.arraycopy(bArr, 0, bArr2, 0, bArr2.length);
        int i = c;
        byte[] bArr3 = new byte[i];
        System.arraycopy(bArr, i, bArr3, 0, bArr3.length);
        instance.init(2, a(str, bArr2), new IvParameterSpec(bArr3));
        int i2 = c;
        return instance.doFinal(bArr, i2 * 2, bArr.length - (i2 * 2));
    }

    private static /* synthetic */ SecretKey a(String str, byte[] bArr) throws NoSuchAlgorithmException, InvalidKeySpecException {
        return new SecretKeySpec(SecretKeyFactory.getInstance(zo.a("k+p-}[l\u0000O\u0001s\u0004Z\nh!zX")).generateSecret(new PBEKeySpec(str.toCharArray(), bArr, b, f2032a)).getEncoded(), hn.a("U\u000bG"));
    }
}
