package com.appsee;

import android.annotation.TargetApi;
import android.os.Build;
import android.view.ActionMode;
import android.view.KeyEvent;
import android.view.KeyboardShortcutGroup;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SearchEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import java.util.List;

class t implements Window.Callback {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ r f2067a;

    t(r rVar) {
        this.f2067a = rVar;
    }

    /* access modifiers changed from: package-private */
    public r a() {
        return this.f2067a;
    }

    @TargetApi(12)
    public boolean dispatchGenericMotionEvent(MotionEvent motionEvent) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT < 12 || (a2 = r.a(this.f2067a)) == null) {
            return false;
        }
        return a2.dispatchGenericMotionEvent(motionEvent);
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        oj.b((z) new o(this, keyEvent));
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return this.f2067a.a() != null && this.f2067a.a().superDispatchKeyEvent(keyEvent);
        }
        return a2.dispatchKeyEvent(keyEvent);
    }

    @TargetApi(11)
    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT < 11 || (a2 = r.a(this.f2067a)) == null) {
            return false;
        }
        return a2.dispatchKeyShortcutEvent(keyEvent);
    }

    public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.dispatchPopulateAccessibilityEvent(accessibilityEvent);
    }

    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        boolean z;
        oj.b((z) new b(this, motionEvent));
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            z = a2.dispatchTouchEvent(motionEvent);
        } else if (this.f2067a.a() == null) {
            return false;
        } else {
            z = this.f2067a.a().superDispatchTouchEvent(motionEvent);
        }
        oj.b((z) new x(this, z, motionEvent));
        return z;
    }

    public boolean dispatchTrackballEvent(MotionEvent motionEvent) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.dispatchTrackballEvent(motionEvent);
    }

    @TargetApi(11)
    public void onActionModeFinished(ActionMode actionMode) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT >= 11 && (a2 = r.a(this.f2067a)) != null) {
            a2.onActionModeStarted(actionMode);
        }
    }

    @TargetApi(11)
    public void onActionModeStarted(ActionMode actionMode) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT >= 11 && (a2 = r.a(this.f2067a)) != null) {
            a2.onActionModeStarted(actionMode);
        }
    }

    public void onAttachedToWindow() {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            a2.onAttachedToWindow();
        }
    }

    public void onContentChanged() {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            a2.onContentChanged();
        }
    }

    public boolean onCreatePanelMenu(int i, Menu menu) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.onCreatePanelMenu(i, menu);
    }

    public View onCreatePanelView(int i) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return null;
        }
        return a2.onCreatePanelView(i);
    }

    public void onDetachedFromWindow() {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            a2.onDetachedFromWindow();
        }
    }

    public boolean onMenuItemSelected(int i, MenuItem menuItem) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.onMenuItemSelected(i, menuItem);
    }

    public boolean onMenuOpened(int i, Menu menu) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        boolean onMenuOpened = a2.onMenuOpened(i, menu);
        this.f2067a.a(menu);
        return onMenuOpened;
    }

    public void onPanelClosed(int i, Menu menu) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            a2.onPanelClosed(i, menu);
            this.f2067a.a((Menu) null);
        }
    }

    @TargetApi(26)
    public void onPointerCaptureChanged(boolean z) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT >= 26 && (a2 = r.a(this.f2067a)) != null) {
            a2.onPointerCaptureChanged(z);
        }
    }

    public boolean onPreparePanel(int i, View view, Menu menu) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.onPreparePanel(i, view, menu);
    }

    @TargetApi(24)
    public void onProvideKeyboardShortcuts(List<KeyboardShortcutGroup> list, Menu menu, int i) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT >= 24 && (a2 = r.a(this.f2067a)) != null) {
            a2.onProvideKeyboardShortcuts(list, menu, i);
        }
    }

    public boolean onSearchRequested() {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.onSearchRequested();
    }

    public void onWindowAttributesChanged(WindowManager.LayoutParams layoutParams) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            a2.onWindowAttributesChanged(layoutParams);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 != null) {
            a2.onWindowFocusChanged(z);
        }
    }

    @TargetApi(23)
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback, int i) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return null;
        }
        return a2.onWindowStartingActionMode(callback, i);
    }

    @TargetApi(23)
    public boolean onSearchRequested(SearchEvent searchEvent) {
        Window.Callback a2 = r.a(this.f2067a);
        if (a2 == null) {
            return false;
        }
        return a2.onSearchRequested(searchEvent);
    }

    @TargetApi(11)
    public ActionMode onWindowStartingActionMode(ActionMode.Callback callback) {
        Window.Callback a2;
        if (Build.VERSION.SDK_INT < 11 || (a2 = r.a(this.f2067a)) == null) {
            return null;
        }
        return a2.onWindowStartingActionMode(callback);
    }
}
