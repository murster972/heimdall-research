package com.appsee;

import android.util.Log;

class qe {
    qe() {
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ '4');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'p');
        }
        return new String(cArr);
    }

    /* renamed from: a  reason: collision with other method in class */
    static void m109a(String str) throws Exception {
        a((Throwable) null, str);
        throw new Exception(String.format(ui.a("7\u0005\u0006\u0006\u0013\u0010V\u0010\u000e\u0016\u0013\u0005\u0002\u001c\u0019\u001bLUS\u0006"), new Object[]{str}));
    }

    static void b(String str) {
        gd.b(2, str);
        if (pg.h().h() != sh.d) {
            lg.c(String.format(oi.a("m\u0006h\ts\t}}\u001abI"), new Object[]{str}));
        }
    }

    public static void a(Throwable th, String str, Object... objArr) {
        a(th, String.format(str, objArr), true);
    }

    static void a(Throwable th, String str, boolean z) {
        if (str == null) {
            str = th.getMessage() == null ? "" : th.getMessage();
        }
        if (th != null) {
            str = String.format(oi.a("bIg\u0017g?Y\"J3S(T}\u001abIi0"), new Object[]{str, Log.getStackTraceString(th)});
        }
        gd.a(2, str);
        if (z && pg.h().h() != sh.d) {
            lg.c(String.format(ui.a("0$'9'LUS\u0006"), new Object[]{str}));
        }
    }

    public static void a(Throwable th, String str) {
        a(th, str, true);
    }
}
