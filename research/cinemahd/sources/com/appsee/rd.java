package com.appsee;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.util.List;
import java.util.Map;

class rd implements u, q, w {
    private static rd e;

    /* renamed from: a  reason: collision with root package name */
    private Handler f2060a;
    private Runnable b = new zd(this);
    private boolean c;
    /* access modifiers changed from: private */
    public boolean d = false;

    private /* synthetic */ rd() {
    }

    private /* synthetic */ void h() {
        try {
            lg.a(String.format(md.a("F\nM\u0010\u0000\u0016"), new Object[]{pg.h().m()}), lf.a());
        } catch (Exception e2) {
            qe.a(e2, md.a("<\u0011\u000b\f\u000bC\n\u0002\u000f\n\u0017\u0004Y\u0002\t\u0013Y\n\u001a\f\u0017"));
        }
    }

    private /* synthetic */ void i() {
        gd.b(1, md.a("0\r\n\u0017\u0018\u000f\u0015\n\u0017\u0004Y\u0014\u0010\r\u001d\f\u000eC\u001a\u0002\u0015\u000f\u001b\u0002\u001a\b\n"));
        oj.a((z) new vd(this));
        gd.b(1, md.a("%\u0010\r\u0010\u0010\u0011\u0006\u001dC\u0010\r\n\u0017\u0018\u000f\u0015\n\u0017\u0004Y\u0014\u0010\r\u001d\f\u000eC\u001a\u0002\u0015\u000f\u001b\u0002\u001a\b\n"));
    }

    public void a(zn znVar) throws Exception {
        xd.b().a(znVar);
    }

    public void b() {
        oj.b((z) new id(this));
    }

    /* access modifiers changed from: package-private */
    public void c() throws Exception {
        if (s.a().e()) {
            s.a().a(false);
        }
    }

    public void d() {
        gd.b(1, md.a("8\u0001\u0016\u0011\r\n\u0017\u0004Y\u0010\u001c\u0010\n\n\u0016\rWMW"));
        try {
            ub.a().a();
        } catch (Exception e2) {
            qe.a(e2, md.a("&\u000b\u0011\u0016\u0011Y\u0010\r\f\t\u0013\u0010\r\u001eC\n\u0000\u000b\u0006\u001c\rY\u0007\u001c\u0017\u001c\u0000\r\n\u0016\rW"));
        }
        if (xd.b().d()) {
            xd.b().b(true);
        }
        if (s.a().e()) {
            try {
                s.a().a(true);
                lg.a(pg.h().m());
            } catch (Exception e3) {
                qe.a(e3, md.a("&\u000b\u0011\u0016\u0011Y\u0005\u0010\r\u0010\u0010\u0011\n\u0017\u0004Y\u0015\u0010\u0007\u001c\fY\u0011\u001c\u0000\u0016\u0011\u001d\n\u0017\u0004W"));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void e() throws Exception {
        if (pg.h().h() == sh.b) {
            gd.a(pg.h().h().contains(tg.g) ^ true ? 1 : 0);
            gd.a(true);
        }
        gd.a(2, md.a(">\f\rC\n\u0006\n\u0010\u0010\f\u0017C\u0010\u0007CC\\\u0010UC\u001a\f\u0017\u0005\u0010\u0004Y\u0011\u001c\u0012\f\u0006\n\u0017Y\u0017\u0010\u000e\u001cYYF\n"), pg.h().m(), Long.valueOf(xd.b().d()));
        if (!pg.h().Z()) {
            d();
        } else if (lg.a(pg.h().M())) {
            gd.a(2, md.a("=\n\u000b\u0006\u001a\u0017\u0016\u0011\u0000C\r\f\u0016C\u0015\u0002\u000b\u0004\u001cOY\u0002\u001b\f\u000b\u0017\u0010\r\u001eC\n\u0006\n\u0010\u0010\f\u0017"));
            d();
        } else {
            if (pg.h().F()) {
                if (!this.c) {
                    gd.b(1, md.a("1\u001c\u0004\u0010\u0010\r\u0006\u000b\u0006\u001dC\u001a\u0011\u0018\u0010\u0011\u0006\n"));
                    xh.a();
                    AppseeNativeExtensions.h();
                    this.c = true;
                }
                AppseeNativeExtensions.f();
            }
            hn.a().b();
            xd.b().a();
            if (pg.h().r()) {
                h();
            }
            if (pg.h().C()) {
                pb.a().a();
            }
            if (pg.h().K()) {
                try {
                    s.a().b();
                    if (pg.h().B() > 0) {
                        int B = pg.h().B() * 1000;
                        gd.a(1, md.a(">\f\rC\u000f\n\u001d\u0006\u0016C\u0014\u0002\u0001C\u0015\u0006\u0017\u0004\r\u000bUC\u0018\u0007\u001d\n\u0017\u0004Y\u0000\u0018\u000f\u0015C\r\fY\u0010\r\f\tC\u0010\rYF\u001d"), Integer.valueOf(B));
                        if (this.f2060a == null) {
                            this.f2060a = new Handler(Looper.getMainLooper());
                        }
                        this.f2060a.postDelayed(this.b, (long) B);
                    }
                } catch (Exception e2) {
                    qe.a(e2, md.a("?\u0002\u0010\u000f\u001c\u0007Y\u0017\u0016C\n\u0017\u0018\u0011\rC\u000f\n\u001d\u0006\u0016C\u000b\u0006\u001a\f\u000b\u0007\u0010\r\u001e"));
                    pg.h().a(false);
                }
            }
            i();
            oj.a((c) new ad(this), true);
        }
    }

    public void f() throws Exception {
        if (xd.b().d()) {
            gd.b(2, md.a(" \u0018\r\u0017\f\rC\n\u0017\u0018\u0011\rC\u0017\u0006\u000eC\n\u0006\n\u0010\u0010\f\u0017C\u000e\u000b\u0010\u000f\u001cC\n\u0006\n\u0010\u0010\f\u0017C\u0010\u0010Y\u0011\f\r\u0017\n\u0017\u0004UC\u0010\u0004\u0017\f\u000b\n\u0017\u0004WMW"));
        } else if (m()) {
            gd.b(1, md.a("8\u0013\t\u0010\u001c\u0006Y\u0014\u0016\r^\u0017Y\u0010\r\u0002\u000b\u0017Y\f\u0017C\u0018\rY\f\t\u0017\u001c\u0007T\f\f\u0017Y\u0007\u001c\u0015\u0010\u0000\u001c"));
        } else {
            AppseeSessionStartingInfo appseeSessionStartingInfo = new AppseeSessionStartingInfo(true);
            oj.a((c) new ud(this, appseeSessionStartingInfo), false);
            if (!appseeSessionStartingInfo.a()) {
                gd.b(1, md.a("8\u0013\t\u0010\u001c\u0006Y\u0014\u0016\r^\u0017Y\u0010\r\u0002\u000b\u0017Y\u0001\u001c\u0000\u0018\u0016\n\u0006Y\u0010\u001c\u0010\n\n\u0016\rY\u0014\u0018\u0010Y\u0000\u0018\r\u001a\u0006\u0015\u0006\u001dC\u001b\u001aY\u0017\u0011\u0006Y\u0016\n\u0006\u000b"));
                return;
            }
            gd.b(2, md.a("SISIY0\r\u0002\u000b\u0017\u0010\r\u001eC\u0017\u0006\u000eC\n\u0006\n\u0010\u0010\f\u0017CSISIS"));
            xd.b().b();
            lg.d();
            pc.c();
            if (!yb.a(xd.b().b())) {
                pc.f();
            } else {
                pc.h();
            }
            pg.h().a(false);
            pg.h().b(false);
            xd.b().b(false);
            s.a().d();
            wd.a().b();
        }
    }

    public void g() {
        gd.b(1, md.a("ISIY\u0016\n\u0006\u000bC\u001a\u0002\u0015\u000f\u001c\u0007Y\u0010\r\f\t"));
        xd.b().a(md.a("\"\t\u0013\n\u0006\u001c0\r\f\t"), (Map<String, Object>) null);
        oj.a(kp.c);
    }

    /* renamed from: m  reason: collision with other method in class */
    public boolean m134m() {
        return pc.e();
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m133a() throws Exception {
        if (xd.b().d()) {
            hn.a().a();
        }
    }

    public void b(boolean z) {
        this.d = false;
        Bundle bundle = new Bundle();
        bundle.putBoolean("com.appsee.Action.UploadMode", z);
        wd.a().a();
        oj.a(kp.f, bundle);
    }

    public void m() {
        oj.b((z) new td(this));
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        boolean d2 = xd.b().d();
        String m = pg.h().m();
        if (pg.h().B() > 0) {
            this.f2060a.removeCallbacks(this.b);
        }
        try {
            if (xd.b().d()) {
                xd.b().c();
            }
        } catch (Exception e2) {
            qe.a(e2, md.a("<\u0011\u000b\f\u000bC\u001f\n\u0017\n\n\u000b\u0010\r\u001eC\u0014\u0006\r\u0002\u001d\u0002\r\u0002Y\u0011\u001c\u0000\u0016\u0011\u001d\n\u0017\u0004W"));
        } catch (Throwable th) {
            s.a().a();
            if (pg.h().h() == sh.b) {
                gd.a(false);
            }
            oj.a((c) new pd(this, m), true);
            if (z && !this.d) {
                if (d2) {
                    gd.a();
                }
                wd.a().a(xd.b().b());
            }
            throw th;
        }
        if (s.a().e()) {
            try {
                s.a().a(true);
            } catch (Exception e3) {
                qe.a(e3, md.a("&\u000b\u0011\u0016\u0011Y\u0005\u0010\r\u0010\u0010\u0011\n\u0017\u0004Y\u0015\u0010\u0007\u001c\fY\u0011\u001c\u0000\u0016\u0011\u001d\n\u0017\u0004W"));
            }
        }
        s.a().a();
        if (pg.h().h() == sh.b) {
            gd.a(false);
        }
        oj.a((c) new pd(this, m), true);
        if (z && !this.d) {
            if (d2) {
                gd.a();
            }
            wd.a().a(xd.b().b());
        }
    }

    public void a(boolean z) {
        nl a2 = bo.a().a(ak.k);
        if (a2 != null && xd.b().b() - a2.a() <= 1250) {
            gd.a(1, md.a("0\u001c\u0017\r\n\u0017\u0004Y\u0005\f\u000f\u0015\u0010\u001a\u0011\u001c\u0006\u0017C\u0012\u0006\u0000\u0001\u0016\u0002\u000b\u0007Y\u0002\u001a\u0017\u0010\f\u0017C\u001c\u0015\u001c\r\rYYF\u001b"), Boolean.valueOf(z));
            a2.b(md.a(z ? "\u0017\u000b\u0016\u001c" : "\u001f\u0002\u0015\u0010\u001c"));
        }
    }

    public static synchronized rd a() {
        rd rdVar;
        synchronized (rd.class) {
            if (e == null) {
                e = new rd();
            }
            rdVar = e;
        }
        return rdVar;
    }

    public void a(String str) {
        bo.a().a(str);
    }

    public void a(r rVar) throws Exception {
        rVar.a();
        if (xd.b().d()) {
            rVar.b();
        }
    }

    public void a(boolean z, boolean z2) {
        if (z) {
            bo.a().a(ak.k, md.a(z2 ? "\u0017\u000b\u0016\u001c" : "\u001f\u0002\u0015\u0010\u001c"), (String) null, (jo) null);
        } else {
            bo.a().a(ak.e, (String) null, (String) null, (jo) null);
        }
    }

    public void a(wl wlVar) {
        if (xd.b().d()) {
            s.a().c();
            gd.b(2, md.a("0\u001c\u0010\n\n\u0016\rY\u0000\u000b\u0002\n\u000b\u001c\u0007UC\f\u0013\u0015\f\u0018\u0007\u0010\r\u001e"));
            xd.b().a(true);
            xd.b().a(wlVar);
            this.d = false;
            wd.a().a();
            c(pg.h().f());
        } else if (s.a().a()) {
            this.d = false;
            wd.a().a();
            c(pg.h().f());
        }
    }

    public void a(r rVar, List<View> list) {
        rVar.a(list);
    }
}
