package com.appsee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class fl {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ wl f1995a;
    private boolean b;
    private StackTraceElement[] c;
    private long d;
    private String e;

    public fl(wl wlVar, long j, String str, boolean z, StackTraceElement[] stackTraceElementArr) {
        this.f1995a = wlVar;
        this.d = j;
        this.e = str;
        this.b = z;
        this.c = stackTraceElementArr;
    }

    public JSONObject a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(hn.a("}"), this.d);
        jSONObject.put(rc.a("o"), this.e);
        jSONObject.put(hn.a("w"), this.b);
        JSONArray jSONArray = new JSONArray();
        int i = 0;
        while (true) {
            StackTraceElement[] stackTraceElementArr = this.c;
            if (i < stackTraceElementArr.length) {
                jSONArray.put(this.f1995a.a(stackTraceElementArr[i]));
                i++;
            } else {
                jSONObject.put(rc.a("}u"), jSONArray);
                return jSONObject;
            }
        }
    }
}
