package com.appsee;

import android.graphics.Rect;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TabWidget;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

class ij implements l, GestureDetector.OnGestureListener, g {
    private static ij f;

    /* renamed from: a  reason: collision with root package name */
    private rc f2011a = new rc(this);
    private List<jo> b = new ArrayList();
    private GestureDetector c;
    private WeakReference<View> d;
    private sd e = new sd(this);

    private /* synthetic */ ij() {
    }

    public void a(int i, short[] sArr, MotionEvent motionEvent) {
        a(i >= 0 ? po.e : po.d, motionEvent, sArr);
    }

    /* access modifiers changed from: package-private */
    public void b(MotionEvent motionEvent) {
        synchronized (this.b) {
            jo joVar = this.b.isEmpty() ? null : this.b.get(this.b.size() - 1);
            if (joVar != null && joVar.a() == po.c && joVar.a() == motionEvent.hashCode()) {
                joVar.a(false);
            }
        }
    }

    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        float y = motionEvent2.getY() - motionEvent.getY();
        float x = motionEvent2.getX() - motionEvent.getX();
        if (Math.abs(x) > Math.abs(y)) {
            if (x > 0.0f) {
                a(po.f, motionEvent2);
                return false;
            }
            a(po.h, motionEvent2);
            return false;
        } else if (y > 0.0f) {
            a(po.f2053a, motionEvent2);
            return false;
        } else {
            a(po.g, motionEvent2);
            return false;
        }
    }

    public void onLongPress(MotionEvent motionEvent) {
    }

    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent2, float f2, float f3) {
        return false;
    }

    public void onShowPress(MotionEvent motionEvent) {
    }

    public boolean onSingleTapUp(MotionEvent motionEvent) {
        View a2 = rb.a(motionEvent, (View) this.d.get());
        this.d = null;
        a(po.c, motionEvent, (short[]) null, a2);
        return false;
    }

    public void a(float f2, short[] sArr, MotionEvent motionEvent) {
        a(po.i, motionEvent, sArr);
    }

    /* access modifiers changed from: package-private */
    public void a(po poVar, MotionEvent motionEvent, short[] sArr, View view) {
        MotionEvent motionEvent2 = motionEvent;
        short[] sArr2 = sArr;
        View view2 = view;
        if (xd.b().d()) {
            if (sArr2 == null || sArr2.length == 0) {
                sArr2 = wb.a().a(motionEvent.hashCode());
            }
            short[] sArr3 = sArr2;
            if (sArr3 != null && sArr3.length != 0) {
                jo joVar = new jo(poVar, true, sArr3, -1, -1, (Rect) null, motionEvent2 != null ? motionEvent.hashCode() : 0);
                synchronized (wb.a().a()) {
                    List a2 = wb.a().a();
                    List a3 = joVar.a();
                    if (a3 != null) {
                        int size = a2.size();
                        for (int i = 0; i < size; i++) {
                            tb tbVar = (tb) a2.get(i);
                            if (a3.contains(Short.valueOf(tbVar.a()))) {
                                if (joVar.a() == -1 || tbVar.a() < joVar.a()) {
                                    joVar.b(tbVar.a());
                                }
                                if (joVar.b() == -1 || tbVar.a() > joVar.b()) {
                                    joVar.a(tbVar.a());
                                }
                            }
                        }
                    }
                }
                if (joVar.a() != -1 && joVar.b() != -1) {
                    if (!pg.h().n()) {
                        joVar.a();
                    }
                    if (poVar == po.c) {
                        try {
                            EnumSet h = pg.h().h();
                            View l = rb.l(view);
                            View e2 = rb.e(view);
                            a(joVar, view2, l, e2);
                            if (h.contains(tg.f2069a) || h.contains(tg.f)) {
                                gd.a(1, oi.a("y+S$Q\"^gl._0\u0000bI"), rb.c(view));
                                gd.a(1, ui.a("&\u0003\u0005\u0006\u001a\u0004\u0001\u0013\u0011&\u0014\u0004\u0010\u0018\u0001V#\u001f\u0010\u0001OS\u0006"), rb.c(l));
                                rb.a(view.getRootView(), h.contains(tg.f2069a), h.contains(tg.f));
                            }
                            bo.a().a(l, view2, joVar, motionEvent2);
                            if (!pg.h().G() && e2 == null) {
                                joVar.a(false);
                            }
                        } catch (Exception e3) {
                            qe.a(e3, oi.a("5H(Hg[#^.T \u001a3[7\u001a&Y3S(T"));
                        }
                    }
                    synchronized (this.b) {
                        jo joVar2 = this.b.isEmpty() ? null : this.b.get(this.b.size() - 1);
                        if (joVar2 == null || !joVar2.b() || !joVar.c() || joVar2.a() != joVar.a() || joVar2.b() != joVar.b()) {
                            this.b.add(joVar);
                            gd.a(1, ui.a("0\u001a\u0003\u001b\u0012U\u0011\u0010\u0005\u0001\u0003\u0007\u0013US\u0006V\u0006\u0002\u0014\u0004\u0001V\u0001\u001f\u0018\u0013UKUS\u0011V\u0010\u0018\u0011V\u0001\u001f\u0018\u0013UKUS\u0011"), poVar.toString(), Long.valueOf(joVar.a()), Long.valueOf(joVar.b()));
                        }
                    }
                }
            }
        }
    }

    private /* synthetic */ void a(po poVar, MotionEvent motionEvent) {
        a(poVar, motionEvent, (short[]) null);
    }

    public static synchronized ij a() {
        ij ijVar;
        synchronized (ij.class) {
            if (f == null) {
                f = new ij();
            }
            ijVar = f;
        }
        return ijVar;
    }

    private /* synthetic */ void a(po poVar, MotionEvent motionEvent, short[] sArr) {
        a(poVar, motionEvent, sArr, (View) null);
    }

    private /* synthetic */ void a(jo joVar, View view, View view2, View view3) {
        if (pg.h().n()) {
            try {
                boolean a2 = a(view);
                if (!rb.i(view2)) {
                    if (!a2) {
                        if (view2 instanceof TabWidget) {
                            view2 = rb.a((ViewGroup) view2, view);
                        } else if (!rb.a(sb.j, view2)) {
                            view2 = rb.i(view);
                            if (view2 == null) {
                                view2 = view3 != null ? view3 : view;
                            }
                        }
                        joVar.a(rb.a(rb.e(view2)));
                        gd.a(1, ui.a("\"\u001a\u0003\u0016\u001e'\u0013\u0016\u0002UKUS\u0006"), rb.c(view2));
                    }
                }
                if (a2 && (view2 instanceof AbsListView)) {
                    view2 = rb.a((ViewGroup) view2, view);
                }
                joVar.a(rb.a(rb.e(view2)));
                gd.a(1, ui.a("\"\u001a\u0003\u0016\u001e'\u0013\u0016\u0002UKUS\u0006"), rb.c(view2));
            } catch (Exception unused) {
                joVar.a((Rect) null);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(MotionEvent motionEvent, View view) throws Exception {
        if (pg.h().X()) {
            this.d = new WeakReference<>(view);
            if (this.c == null) {
                this.c = new GestureDetector(ho.a(), this);
            }
            this.f2011a.a(motionEvent);
            this.c.onTouchEvent(motionEvent);
            this.e.a(motionEvent);
            if (motionEvent.getActionMasked() == 1) {
                bo.a().a(rb.l(rb.a(motionEvent, view)));
            }
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public void m35a() {
        synchronized (this.b) {
            this.b.clear();
        }
    }

    public void a(MotionEvent motionEvent) {
        a(po.j, motionEvent);
    }

    /* access modifiers changed from: package-private */
    public List<jo> a(boolean z) {
        List<jo> list = this.b;
        if (!z) {
            return list;
        }
        synchronized (list) {
            this.b = new ArrayList();
        }
        return list;
    }

    private /* synthetic */ boolean a(View view) throws Exception {
        AbsListView absListView;
        View rootView = view.getRootView();
        r a2 = ub.a().a(rootView);
        if (a2 == null || !a2.a() || (absListView = (AbsListView) rb.d(rb.a(rootView, (Class<?>) AbsListView.class))) == null || rb.c(absListView)) {
            return false;
        }
        return true;
    }
}
