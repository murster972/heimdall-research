package com.appsee;

import android.annotation.TargetApi;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.media.MediaCodec;
import android.media.MediaCrypto;
import android.media.MediaFormat;
import android.media.MediaMuxer;
import android.os.Build;
import android.view.Surface;
import java.nio.ByteBuffer;
import java.util.HashMap;

@TargetApi(18)
class ib implements j {

    /* renamed from: a  reason: collision with root package name */
    private jm f2007a = oc.a().a(qe.a("q\u001eW\u001fP\u0015}\u001dU\u0017Q"));
    private ByteBuffer[] b;
    private int c;
    private int d;
    private MediaCodec.BufferInfo e;
    private long f;
    private int g;
    private boolean h;
    private MediaFormat i;
    private int j;
    private jm k = oc.a().a(qe.a("g\u0005F\u0016U\u0013Q:U\u0006U5Z\u0013[\u0014]\u001eS"));
    private long l;
    private MediaCodec m;
    private int n;
    private MediaMuxer o;
    private Rect p;
    private String q;
    private HashMap<Long, Long> r = new HashMap<>();
    private Surface s;
    private jm t = oc.a().a(jm.a("y&T1[4y(T1_5I.U)"));

    ib() {
    }

    @TargetApi(21)
    private /* synthetic */ void b() throws Exception {
        this.i = MediaFormat.createVideoFormat("video/avc", this.n, this.g);
        this.i.setInteger(qe.a("\u0013[\u001c[\u0002\u0019\u0016[\u0002Y\u0011@"), 2130708361);
        this.i.setInteger(jm.a("%S3H&N\""), this.d);
        this.i.setInteger(qe.a("\u0016F\u0011Y\u0015\u0019\u0002U\u0004Q"), this.c);
        this.i.setInteger(jm.a("Sj\\5[*_jS)N\"H1[+"), 1);
        if (Build.VERSION.SDK_INT >= 19) {
            this.i.setInteger(qe.a("F\u0015D\u0015U\u0004\u0019\u0000F\u0015B\u0019[\u0005G]R\u0002U\u001dQ]U\u0016@\u0015F"), (1000000 / this.c) * 2);
        }
        this.m = MediaCodec.createEncoderByType("video/avc");
        if (this.m != null) {
            if (Build.VERSION.SDK_INT >= 21 && !pg.h().P()) {
                this.m.reset();
            }
            this.m.configure(this.i, (Surface) null, (MediaCrypto) null, 1);
            this.s = this.m.createInputSurface();
            this.m.start();
            this.b = this.m.getOutputBuffers();
            return;
        }
        throw new Exception(jm.a("\u0014O5\\&Y\"\u001a\"T$U#_5\u001a.IgT2V+"));
    }

    private /* synthetic */ void c() throws Exception {
        if (this.o != null) {
            gd.b(2, qe.a("4[\u0005V\u001cQPY\u0005L\u0015FP]\u001e]\u0004]\u0011@\u0019[\u001e\u0014\u0014Q\u0004Q\u0013@\u0015P"));
            this.o.stop();
            this.o.release();
            this.o = null;
        }
        gd.a(1, jm.a("s)S3S&V.@\"\u001a\"T$U#_5\u001a*O?_5\u001a(TgJ&N/\u0007bIk\u001a!U5W&Nz\u001f4"), this.q, this.m.getOutputFormat().toString());
        this.o = new MediaMuxer(this.q, 0);
        this.j = this.o.addTrack(this.m.getOutputFormat());
        this.o.start();
    }

    public void a(int i2, int i3, int i4, int i5, String str, boolean z) throws Exception {
        gd.b(1, jm.a("s)S3S&V.@\"\u001a4O5\\&Y\"\u001a1S#_(\u001a\"T$U#_5"));
        this.n = i2;
        this.g = i3;
        this.p = new Rect(0, 0, this.n, this.g);
        this.d = i4;
        this.c = i5;
        this.q = str;
        this.h = z;
        this.e = new MediaCodec.BufferInfo();
        this.r.clear();
        this.l = 0;
        this.f = 0;
        b();
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m34a() {
        return false;
    }

    private /* synthetic */ void a(boolean z) throws Exception {
        int dequeueOutputBuffer = this.m.dequeueOutputBuffer(this.e, 0);
        if (this.h) {
            gd.a(1, jm.a("~\"K2_2_#\u001a\"T$U#_5\u001a!H&W\"\u0000g\u001f#"), Integer.valueOf(dequeueOutputBuffer));
        }
        while (true) {
            if (dequeueOutputBuffer == -1) {
                if (!z) {
                    return;
                }
            } else if (dequeueOutputBuffer == -2) {
                c();
            } else if (dequeueOutputBuffer == -3) {
                this.b = this.m.getOutputBuffers();
            } else if (dequeueOutputBuffer >= 0) {
                ByteBuffer duplicate = this.b[dequeueOutputBuffer].duplicate();
                if (duplicate.position() != this.e.offset) {
                    gd.b(1, qe.a("r\u001fA\u001ePPV\u0011PPQ\u001eW\u001fP\u0015FPV\u0005R\u0016Q\u0002\u0014\u0000[\u0003]\u0004]\u001fZ"));
                    duplicate.position(this.e.offset);
                    MediaCodec.BufferInfo bufferInfo = this.e;
                    duplicate.limit(bufferInfo.offset + bufferInfo.size);
                }
                if ((this.e.flags & 2) != 0) {
                    if (this.h) {
                        gd.b(1, jm.a("|(O)^gY(T!S \u001a!H&W\""));
                    }
                    this.e.size = 0;
                }
                if ((this.e.flags & 4) != 0) {
                    if (this.h) {
                        gd.b(1, qe.a("6[\u0005Z\u0014\u00145{#\u0014\u0016X\u0011S"));
                    }
                    this.e.size = 0;
                }
                if (this.e.size != 0) {
                    if (this.o == null) {
                        gd.b(1, jm.a("w2B\"HgS4\u001a.T.N.[+S=_#\u001a(T+CgU)\u001a!S5I3\u001a#[3[g\\5[*_g\u00122I2[+V>\u001a.NgU)\u001a\u000et\u0001u\u0018u\u0012n\u0017o\u0013e\u0001u\u0015w\u0006n\u0018y\u000f{\t}\u0002~n"));
                        c();
                    }
                    long j2 = this.l;
                    this.l = j2 + 1;
                    if (this.r.containsKey(Long.valueOf(j2))) {
                        long longValue = this.r.get(Long.valueOf(j2)).longValue();
                        if (this.h) {
                            gd.a(1, qe.a("'F\u0019@\u0019Z\u0017\u0014\u0016F\u0011Y\u0015\u0014\u0016[\u0002\u0014\u0004]\u001dQJ\u0014UP"), Long.valueOf(longValue));
                        }
                        MediaCodec.BufferInfo bufferInfo2 = this.e;
                        bufferInfo2.presentationTimeUs = longValue;
                        this.o.writeSampleData(this.j, duplicate, bufferInfo2);
                        this.r.remove(Long.valueOf(j2));
                    } else {
                        gd.a(1, jm.a("|(O)^g_)Y(^\"^g\\5[*_gM.N/\u001a)UgJ5_4_)N&N.U)\u001a3S*_k\u001a4Q.J7S)]g\\5[*_g\u001f#"), Long.valueOf(this.l - 1));
                    }
                }
                this.m.releaseOutputBuffer(dequeueOutputBuffer, false);
                if ((this.e.flags & 4) != 0) {
                    return;
                }
            }
            dequeueOutputBuffer = this.m.dequeueOutputBuffer(this.e, 0);
            if (this.h) {
                gd.a(1, qe.a("4Q\u0001A\u0015A\u0015PPQ\u001eW\u001fP\u0015FPR\u0002U\u001dQJ\u0014UP"), Integer.valueOf(dequeueOutputBuffer));
            }
        }
    }

    @TargetApi(21)
    public void a() throws Exception {
        this.t.c();
        this.k.c();
        this.f2007a.c();
        if (this.m != null) {
            gd.b(1, qe.a("c\u0002]\u0004]\u001eSPq?g"));
            this.m.signalEndOfInputStream();
            a(true);
            gd.a(1, jm.a("\u0014N(J7S)]g_)Y(^\"Hg\u0012b^n"), Integer.valueOf(this.r.size()));
            if (Build.VERSION.SDK_INT >= 21 && !pg.h().P()) {
                this.m.reset();
            }
            this.m.stop();
            this.m.release();
        }
        MediaMuxer mediaMuxer = this.o;
        if (mediaMuxer != null) {
            mediaMuxer.stop();
            this.o.release();
        }
        Surface surface = this.s;
        if (surface != null) {
            surface.release();
        }
        this.b = null;
        this.m = null;
        this.i = null;
        this.s = null;
        this.o = null;
        this.e = null;
    }

    public void a(ch chVar, long j2) throws Exception {
        Canvas canvas;
        this.f2007a.b();
        if (this.h) {
            gd.b(1, qe.a("7Q\u0004@\u0019Z\u0017\u0014\u0015Z\u0013[\u0014Q\u0002\u0014\u0013U\u001eB\u0011G"));
        }
        if (this.s.isValid()) {
            try {
                canvas = this.s.lockCanvas(this.p);
                try {
                    if (this.h) {
                        gd.b(1, jm.a("\u0003H&M.T \u001a(Tg_)Y(^\"HgY&T1[4"));
                    }
                    this.t.b();
                    canvas.drawColor(-16777216);
                    canvas.drawBitmap(chVar.b(), 0.0f, 0.0f, (Paint) null);
                    this.t.d();
                    HashMap<Long, Long> hashMap = this.r;
                    long j3 = this.f;
                    this.f = 1 + j3;
                    hashMap.put(Long.valueOf(j3), Long.valueOf(j2));
                    this.s.unlockCanvasAndPost(canvas);
                    if (this.h) {
                        gd.b(1, qe.a("4F\u0011]\u001e]\u001eSPQ\u001eW\u001fP\u0015F"));
                    }
                    this.k.b();
                    a(false);
                    this.k.d();
                    this.f2007a.d();
                } catch (Throwable th) {
                    th = th;
                    HashMap<Long, Long> hashMap2 = this.r;
                    long j4 = this.f;
                    this.f = 1 + j4;
                    hashMap2.put(Long.valueOf(j4), Long.valueOf(j2));
                    this.s.unlockCanvasAndPost(canvas);
                    throw th;
                }
            } catch (Throwable th2) {
                th = th2;
                canvas = null;
                HashMap<Long, Long> hashMap22 = this.r;
                long j42 = this.f;
                this.f = 1 + j42;
                hashMap22.put(Long.valueOf(j42), Long.valueOf(j2));
                this.s.unlockCanvasAndPost(canvas);
                throw th;
            }
        }
    }
}
