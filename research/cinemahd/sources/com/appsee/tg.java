package com.appsee;

import java.util.EnumSet;

enum tg {
    f(1),
    f2069a(2),
    d(4),
    e(8),
    g(16),
    b(32);
    
    private final long A;

    public static EnumSet<tg> a(long j) {
        EnumSet<tg> noneOf = EnumSet.noneOf(tg.class);
        for (tg tgVar : values()) {
            long j2 = tgVar.A;
            if ((j & j2) == j2) {
                noneOf.add(tgVar);
            }
        }
        return noneOf;
    }
}
