package com.appsee;

import android.annotation.TargetApi;
import android.content.res.AssetManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import com.facebook.common.util.ByteConstants;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.json.JSONException;

class lg {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f2024a;
    private static byte[] b;
    private static File c;
    private static final String d = String.format(sd.a("6\u000b!\u0016!Wv\n"), new Object[]{"log"});
    static final String e = String.format(ch.a("@VqUdCOGuOwC$U"), new Object[]{Appsee.b});

    lg() {
    }

    private static /* synthetic */ long a(File file) {
        long j;
        long j2 = 0;
        if (file.exists()) {
            File[] listFiles = file.listFiles();
            for (int i = 0; i < listFiles.length; i++) {
                if (listFiles[i].isDirectory()) {
                    j = a(listFiles[i]);
                } else {
                    j = listFiles[i].length();
                }
                j2 += j;
            }
        }
        return j2;
    }

    static byte[] b() throws IOException {
        InputStream open = ho.a().getAssets().open("AppseeOffline.config", 3);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(open.available());
        a((OutputStream) byteArrayOutputStream, open);
        return byteArrayOutputStream.toByteArray();
    }

    static void c(String str) {
        try {
            File a2 = a(d);
            if (!a2.exists() || a2.length() < PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
                a(a2, yb.a(String.format(Locale.US, sd.a("v\u001dZTZ\\ s"), new Object[]{Long.valueOf(xd.b().b()), str})), true);
            }
        } catch (Exception e2) {
            Log.e("appsee", ch.a("DTsIs\u0006vThRhHf\u0006uI!JnA"), e2);
        }
    }

    static void d() throws IOException {
        File a2 = a();
        boolean mkdirs = a2.mkdirs();
        if (mkdirs) {
            gd.a(1, ch.a("BTdGuCe\u0006cGrC!BhTdEuIs_;\u0006$U"), "appsee");
        }
        if (mkdirs) {
            return;
        }
        if (!a2.exists() || !a2.isDirectory()) {
            throw new IOException(sd.a(":2\u0017=\u0016'Y0\u000b6\u0018'\u001cs\u0018#\t \u001c6Y5\u0016?\u001d6\u000b"));
        }
    }

    static File b(String str) {
        File a2 = a(str);
        if (a2.exists()) {
            return a2;
        }
        return null;
    }

    /* renamed from: c  reason: collision with other method in class */
    static synchronized byte[] m51c() {
        byte[] bArr;
        synchronized (lg.class) {
            int h = (int) pg.h().h();
            if (f2024a == null || f2024a.length != h) {
                f2024a = new byte[h];
            }
            bArr = f2024a;
        }
        return bArr;
    }

    static void a(String str, boolean z) {
        b(a(yb.a((Object[]) new String[]{str, "mp4"}, sd.a("W"))));
        b(a(yb.a((Object[]) new String[]{str, "h264"}, sd.a("W"))));
        b(a(yb.a((Object[]) new String[]{str, "mp4_tmp"}, sd.a("W"))));
        if (z) {
            b(a(yb.a((Object[]) new String[]{str, "md"}, sd.a("W"))));
            b(a(yb.a((Object[]) new String[]{str, "mdj"}, sd.a("W"))));
            b(a(yb.a((Object[]) new String[]{str, "png"}, sd.a("W"))));
            b(a(yb.a((Object[]) new String[]{str, "ico"}, sd.a("W"))));
            b(a(yb.a((Object[]) new String[]{str, "md", "zip"}, sd.a("W"))));
            b(a(yb.a((Object[]) new String[]{str, "mdj", "zip"}, sd.a("W"))));
        }
    }

    /* renamed from: b  reason: collision with other method in class */
    static void m49b(File file) {
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                String[] list = file.list();
                int length = list.length;
                int i = 0;
                while (i < length) {
                    String str = list[i];
                    i++;
                    b(new File(file, str));
                }
            }
            file.delete();
        }
    }

    static void c() {
        a((FilenameFilter) new yj());
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0020 A[SYNTHETIC, Splitter:B:14:0x0020] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0027 A[SYNTHETIC, Splitter:B:21:0x0027] */
    /* renamed from: b  reason: collision with other method in class */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.lang.String m48b(java.io.File r3) {
        /*
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ IOException -> 0x0024, all -> 0x001c }
            r1.<init>(r3)     // Catch:{ IOException -> 0x0024, all -> 0x001c }
            int r3 = r1.available()     // Catch:{ IOException -> 0x001a, all -> 0x0018 }
            byte[] r3 = new byte[r3]     // Catch:{ IOException -> 0x001a, all -> 0x0018 }
            r1.read(r3)     // Catch:{ IOException -> 0x001a, all -> 0x0018 }
            java.lang.String r2 = new java.lang.String     // Catch:{ IOException -> 0x001a, all -> 0x0018 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x001a, all -> 0x0018 }
            r1.close()     // Catch:{ IOException -> 0x0017 }
        L_0x0017:
            return r2
        L_0x0018:
            r3 = move-exception
            goto L_0x001e
        L_0x001a:
            goto L_0x0025
        L_0x001c:
            r3 = move-exception
            r1 = r0
        L_0x001e:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ IOException -> 0x0023 }
        L_0x0023:
            throw r3
        L_0x0024:
            r1 = r0
        L_0x0025:
            if (r1 == 0) goto L_0x002a
            r1.close()     // Catch:{ IOException -> 0x002a }
        L_0x002a:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.lg.m48b(java.io.File):java.lang.String");
    }

    /* renamed from: a  reason: collision with other method in class */
    static void m45a(FilenameFilter filenameFilter) {
        List<File> a2 = a(filenameFilter);
        if (a2 != null) {
            for (File next : a2) {
                try {
                    b(next);
                } catch (Throwable th) {
                    gd.a(1, ch.a("enSmBo\u0001u\u0006eCmCuC!@hJd\u0006&\u0003r\u0001;\u0006$U"), next.getAbsolutePath(), th.getMessage());
                }
            }
        }
    }

    static void b(String str, String str2) {
        List<File> a2 = a((FilenameFilter) new Cif(str));
        if (a2 != null) {
            for (File next : a2) {
                next.renameTo(new File(next.getParentFile(), String.format(ch.a("\u0003r\b$U"), new Object[]{str2, yb.a(next.getName(), true)})));
            }
        }
    }

    static File a(String str) {
        return new File(a(), str);
    }

    static List<File> a(FilenameFilter filenameFilter) {
        ArrayList arrayList = new ArrayList();
        File a2 = a();
        if (a2.exists()) {
            a(a2, (List<File>) arrayList, filenameFilter);
        }
        return arrayList;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00d7  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x00dc  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00e1  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00ec  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f1  */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00fb A[SYNTHETIC, Splitter:B:62:0x00fb] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.io.File b(java.io.File r10) throws java.io.IOException {
        /*
            java.lang.String r0 = r10.getName()
            java.lang.String r1 = "zip"
            boolean r0 = r0.endsWith(r1)
            if (r0 == 0) goto L_0x0016
            java.lang.String r0 = "NThAhH`J!@hJd\u0006hU!GmTdGe_!EnKqTdUrCe\b"
            java.lang.String r0 = com.appsee.ch.a(r0)
            com.appsee.qe.b(r0)
            return r10
        L_0x0016:
            boolean r0 = r10.exists()
            r2 = 0
            if (r0 != 0) goto L_0x0027
            java.lang.String r10 = "6!\u00104\u0010=\u0018?Y5\u0010?\u001cs\u001d<\u001c \u0017t\rs\u001c+\u0010 \rY=\u0016'Y0\u0016>\t!\u001c \n:\u00174W"
            java.lang.String r10 = com.appsee.sd.a((java.lang.String) r10)
            com.appsee.qe.b(r10)
            return r2
        L_0x0027:
            com.appsee.oc r0 = com.appsee.oc.a()
            java.lang.String r3 = "`hJdenKqTdUrOnH"
            java.lang.String r3 = com.appsee.ch.a(r3)
            com.appsee.jm r0 = r0.a((java.lang.String) r3)
            r0.b()
            java.lang.String r3 = "\\ Wv\n"
            java.lang.String r3 = com.appsee.sd.a((java.lang.String) r3)
            r4 = 2
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.String r5 = r10.getAbsolutePath()
            r6 = 0
            r4[r6] = r5
            r5 = 1
            r4[r5] = r1
            java.lang.String r1 = java.lang.String.format(r3, r4)
            java.lang.String r3 = "BIlVsCrUhHf\u0006gOmC!Rn\u0006$U"
            java.lang.String r3 = com.appsee.ch.a(r3)
            java.lang.Object[] r4 = new java.lang.Object[r5]
            r4[r6] = r1
            com.appsee.gd.a(r5, r3, r4)
            java.io.File r3 = new java.io.File
            r3.<init>(r1)
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x00c1, all -> 0x00bd }
            r4.<init>(r3, r5)     // Catch:{ Exception -> 0x00c1, all -> 0x00bd }
            java.io.FileInputStream r7 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00c1, all -> 0x00bd }
            r7.<init>(r10)     // Catch:{ Exception -> 0x00c1, all -> 0x00bd }
            java.util.zip.Deflater r8 = new java.util.zip.Deflater     // Catch:{ Exception -> 0x00ba, all -> 0x00b7 }
            r8.<init>()     // Catch:{ Exception -> 0x00ba, all -> 0x00b7 }
            java.util.zip.DeflaterOutputStream r9 = new java.util.zip.DeflaterOutputStream     // Catch:{ Exception -> 0x00b5, all -> 0x00b3 }
            r9.<init>(r4, r8)     // Catch:{ Exception -> 0x00b5, all -> 0x00b3 }
            byte[] r2 = b     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            if (r2 != 0) goto L_0x007f
            r2 = 8192(0x2000, float:1.14794E-41)
            byte[] r2 = new byte[r2]     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            b = r2     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
        L_0x007f:
            byte[] r2 = b     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            int r2 = r7.read(r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            if (r2 > 0) goto L_0x00a7
            r9.finish()     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            r9.close()
            r7.close()
            r8.end()
            java.lang.String r10 = "bnHd\u0006bIlVsCrUhHf\u0006uI;\u0006$U"
            java.lang.String r10 = com.appsee.ch.a(r10)
            java.lang.Object[] r2 = new java.lang.Object[r5]
            r2[r6] = r1
            com.appsee.gd.a(r5, r10, r2)
            r0.d()
            r0.c()
            return r3
        L_0x00a7:
            byte[] r4 = b     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            r9.write(r4, r6, r2)     // Catch:{ Exception -> 0x00b0, all -> 0x00ad }
            goto L_0x007f
        L_0x00ad:
            r10 = move-exception
            r2 = r9
            goto L_0x00ea
        L_0x00b0:
            r0 = move-exception
            r2 = r9
            goto L_0x00c4
        L_0x00b3:
            r10 = move-exception
            goto L_0x00ea
        L_0x00b5:
            r0 = move-exception
            goto L_0x00c4
        L_0x00b7:
            r10 = move-exception
            r8 = r2
            goto L_0x00ea
        L_0x00ba:
            r0 = move-exception
            r8 = r2
            goto L_0x00c4
        L_0x00bd:
            r10 = move-exception
            r7 = r2
            r8 = r7
            goto L_0x00ea
        L_0x00c1:
            r0 = move-exception
            r7 = r2
            r8 = r7
        L_0x00c4:
            java.lang.String r1 = "\u0016\u000b!\u0016!Y)\u0010#\t:\u00174Y5\u0010?\u001ciYv\n"
            java.lang.String r1 = com.appsee.sd.a((java.lang.String) r1)     // Catch:{ all -> 0x00e8 }
            java.lang.Object[] r4 = new java.lang.Object[r5]     // Catch:{ all -> 0x00e8 }
            java.lang.String r9 = r10.getName()     // Catch:{ all -> 0x00e8 }
            r4[r6] = r9     // Catch:{ all -> 0x00e8 }
            com.appsee.qe.a((java.lang.Throwable) r0, (java.lang.String) r1, (java.lang.Object[]) r4)     // Catch:{ all -> 0x00e8 }
            if (r2 == 0) goto L_0x00da
            r2.close()
        L_0x00da:
            if (r7 == 0) goto L_0x00df
            r7.close()
        L_0x00df:
            if (r8 == 0) goto L_0x00e4
            r8.end()
        L_0x00e4:
            b((java.io.File) r3)     // Catch:{ Exception -> 0x00e7 }
        L_0x00e7:
            return r10
        L_0x00e8:
            r10 = move-exception
            r6 = 1
        L_0x00ea:
            if (r2 == 0) goto L_0x00ef
            r2.close()
        L_0x00ef:
            if (r7 == 0) goto L_0x00f4
            r7.close()
        L_0x00f4:
            if (r8 == 0) goto L_0x00f9
            r8.end()
        L_0x00f9:
            if (r6 == 0) goto L_0x00fe
            b((java.io.File) r3)     // Catch:{ Exception -> 0x00fe }
        L_0x00fe:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.lg.b(java.io.File):java.io.File");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0017 A[SYNTHETIC, Splitter:B:13:0x0017] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static int a(java.io.File r2, long r3, byte[] r5) throws java.io.IOException {
        /*
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ all -> 0x0014 }
            r1.<init>(r2)     // Catch:{ all -> 0x0014 }
            r1.skip(r3)     // Catch:{ all -> 0x0011 }
            int r2 = r1.read(r5)     // Catch:{ all -> 0x0011 }
            r1.close()     // Catch:{ Exception -> 0x0010 }
        L_0x0010:
            return r2
        L_0x0011:
            r2 = move-exception
            r0 = r1
            goto L_0x0015
        L_0x0014:
            r2 = move-exception
        L_0x0015:
            if (r0 == 0) goto L_0x001a
            r0.close()     // Catch:{ Exception -> 0x001a }
        L_0x001a:
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.lg.a(java.io.File, long, byte[]):int");
    }

    static void a(String str, byte[] bArr) throws IOException {
        a(a(str), bArr, false);
    }

    private static /* synthetic */ void a(File file, List<File> list, FilenameFilter filenameFilter) {
        for (File file2 : file.listFiles(filenameFilter)) {
            if (file2.isDirectory() && !file2.getName().equals(e)) {
                a(file2, list, filenameFilter);
            } else if (!file2.isDirectory() && !file2.getName().equals(gl.c)) {
                list.add(file2.getAbsoluteFile());
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ void a(java.io.File r2, byte[] r3, boolean r4) throws java.io.IOException {
        /*
            if (r3 == 0) goto L_0x002a
            int r0 = r3.length
            if (r0 != 0) goto L_0x0006
            goto L_0x002a
        L_0x0006:
            java.io.File r0 = a()
            boolean r0 = r0.exists()
            if (r0 != 0) goto L_0x0013
            d()
        L_0x0013:
            r0 = 0
            java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ all -> 0x0023 }
            r1.<init>(r2, r4)     // Catch:{ all -> 0x0023 }
            r1.write(r3)     // Catch:{ all -> 0x0020 }
            r1.close()
            return
        L_0x0020:
            r2 = move-exception
            r0 = r1
            goto L_0x0024
        L_0x0023:
            r2 = move-exception
        L_0x0024:
            if (r0 == 0) goto L_0x0029
            r0.close()
        L_0x0029:
            throw r2
        L_0x002a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.lg.a(java.io.File, byte[], boolean):void");
    }

    /* renamed from: a  reason: collision with other method in class */
    static void m46a(String str) {
        a(str, false);
        try {
            pc.a(str);
        } catch (JSONException e2) {
            qe.a((Throwable) e2, ch.a("csTnT!QiOmC!TdKnPhHf\u0006`H!phBdITVmI`BQImOb_!QiCo\u0006bJdGoOoA!Sq\u0006rCrUhIo\u0006$U"), str);
        }
    }

    static File a(String str, String str2) {
        String format = String.format(sd.a("v\nv\nv\n"), new Object[]{a(), File.separator, str});
        File file = new File(format);
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(format, str2);
    }

    private static /* synthetic */ void a(OutputStream outputStream, InputStream inputStream) throws IOException {
        byte[] bArr = new byte[ByteConstants.KB];
        while (true) {
            int read = inputStream.read(bArr);
            if (read != -1) {
                outputStream.write(bArr, 0, read);
            } else {
                inputStream.close();
                outputStream.flush();
                outputStream.close();
                return;
            }
        }
    }

    static void a(String str, Drawable drawable) throws IOException {
        if (drawable != null) {
            gd.a(1, ch.a("u`PhHf\u0006hK`Ad\u0006eGuG!Rn\u0006$U"), str);
            a(str, a(drawable));
        }
    }

    /* renamed from: b  reason: collision with other method in class */
    static void m50b(String str) throws IOException {
        if (!yb.a(str)) {
            String format = String.format(ch.a("$U$U$U"), new Object[]{"appseeAssets", File.separator, str});
            AssetManager assets = ho.a().getAssets();
            for (String str2 : assets.list(format)) {
                File a2 = a(e, String.format(sd.a("\\ W \u0016"), new Object[]{str2}));
                if (!a2.exists()) {
                    String format2 = String.format(ch.a("$U$U$U"), new Object[]{format, File.separator, str2});
                    gd.a(1, sd.a(":<\t*\u0010=\u001es\\ Y'\u0016s\u0015<\u001a2\u0015s\u0018#\t \u001c6Y5\u0016?\u001d6\u000b"), format2);
                    a((OutputStream) new FileOutputStream(a2, false), assets.open(format2));
                }
            }
            return;
        }
        throw new IllegalArgumentException(sd.a(":2\u0017=\u0016'Y5\u0010=\u001ds\n&\t#\u0016!\r6\u001ds:\u0003,s\u0018!\u001a;\u0010'\u001c0\r&\u000b6Y5\u0016!Y\u0012\t#\n6\u001cs\u00172\r:\u000f6Y6\u0001'\u001c=\n:\u0016=\n"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x001f A[SYNTHETIC, Splitter:B:13:0x001f] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ byte[] a(android.graphics.drawable.Drawable r3) {
        /*
            android.graphics.Bitmap r3 = com.appsee.rb.a((android.graphics.drawable.Drawable) r3)
            r0 = 0
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x001b }
            r1.<init>()     // Catch:{ all -> 0x001b }
            android.graphics.Bitmap$CompressFormat r0 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ all -> 0x0019 }
            r2 = 100
            r3.compress(r0, r2, r1)     // Catch:{ all -> 0x0019 }
            byte[] r3 = r1.toByteArray()     // Catch:{ all -> 0x0019 }
            r1.close()     // Catch:{ IOException -> 0x0018 }
        L_0x0018:
            return r3
        L_0x0019:
            r3 = move-exception
            goto L_0x001d
        L_0x001b:
            r3 = move-exception
            r1 = r0
        L_0x001d:
            if (r1 == 0) goto L_0x0022
            r1.close()     // Catch:{ IOException -> 0x0022 }
        L_0x0022:
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.lg.a(android.graphics.drawable.Drawable):byte[]");
    }

    static boolean a(int i) {
        return a(a()) >= ((long) i);
    }

    static void a(String str, String str2, boolean z) throws IOException, JSONException {
        String a2 = sd.a("\\ Wv\n");
        Object[] objArr = new Object[2];
        objArr[0] = str2;
        objArr[1] = z ? "mdj" : "md";
        String format = String.format(a2, objArr);
        File a3 = a(format);
        File a4 = a(String.format(ch.a("$U^RlV"), new Object[]{format}));
        if (z || !a3.exists()) {
            gd.a(z ^ true ? 1 : 0, ch.a("RGwOoA!kE\u0006eGuG!Rn\u0006$U"), a4.getAbsolutePath());
            a(a4, yb.a(str), false);
            a4.renameTo(a3);
            return;
        }
        qe.b(sd.a("\u00146\r2\u001d2\r2Y5\u0010?\u001cs\u0018?\u000b6\u00187\u0000s\u001c+\u0010 \r Us\u0017<\rs\n2\u000f:\u00174W"));
    }

    /* renamed from: a  reason: collision with other method in class */
    static void m47a(String str, String str2) {
        File file = new File(str);
        if (file.exists()) {
            file.renameTo(new File(str2));
        }
    }

    @TargetApi(21)
    private static /* synthetic */ File a() {
        if (c == null) {
            c = new File(String.format(sd.a("v\nv\nv\n"), new Object[]{Build.VERSION.SDK_INT >= 21 ? ho.a().getNoBackupFilesDir() : ho.a().getFilesDir(), File.separator, "appsee"}));
        }
        return c;
    }
}
