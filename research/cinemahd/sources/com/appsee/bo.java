package com.appsee;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TabWidget;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class bo {
    private static bo b;

    /* renamed from: a  reason: collision with root package name */
    private List<nl> f1979a = new ArrayList();

    private /* synthetic */ bo() {
    }

    private static /* synthetic */ byte[] b(Drawable drawable) throws Exception {
        if (drawable == null) {
            return null;
        }
        Bitmap a2 = rb.a(drawable);
        if (a2 == null || a2.getWidth() == 0 || a2.getHeight() == 0) {
            throw new Exception(zo.a("x\bUNOIX\bW\nN\u0005Z\u001d^IS\bH\u0001\u001b\u000fT\u001b\u001bY\u001b\u001aR\u0013^\r\u001b\u0000V\b\\\f"));
        }
        double width = ((double) a2.getWidth()) / ((double) a2.getHeight());
        Dimension dimension = new Dimension(8, 8);
        Dimension dimension2 = new Dimension(16, 4);
        if (Math.abs(width - dimension.b()) >= Math.abs(width - dimension2.b())) {
            dimension = dimension2;
        }
        Bitmap a3 = a(a2, dimension, true);
        byte[] bArr = new byte[72];
        for (int i = 0; i < a3.getHeight(); i++) {
            int i2 = 0;
            while (i2 < a3.getWidth()) {
                int pixel = a3.getPixel(i2, i);
                i2++;
                bArr[(a3.getWidth() * i) + i2] = (byte) Color.red(pixel);
            }
        }
        Bitmap a4 = a(a2, dimension, false);
        for (int i3 = 0; i3 < a4.getHeight(); i3++) {
            for (int i4 = 0; i4 < a4.getWidth(); i4++) {
                if (Color.alpha(a4.getPixel(i4, i3)) == 0) {
                    int width2 = (a4.getWidth() * i3) + i4;
                    int i5 = (width2 / 8) + 64;
                    bArr[i5] = (byte) ((1 << (7 - (width2 % 8))) | bArr[i5]);
                }
            }
        }
        return bArr;
    }

    /* access modifiers changed from: package-private */
    public List<nl> a(boolean z) {
        List<nl> list = this.f1979a;
        if (!z) {
            return list;
        }
        synchronized (list) {
            this.f1979a = new ArrayList();
        }
        return list;
    }

    /* access modifiers changed from: package-private */
    public void a(View view, View view2, jo joVar, MotionEvent motionEvent) throws Exception {
        if (view == null || !xd.b().d() || !pg.h().x()) {
            return;
        }
        if (view instanceof AbsListView) {
            a((AbsListView) view, motionEvent, joVar);
        } else if (view instanceof TabWidget) {
            a((TabWidget) view, view2, joVar);
        } else if (rb.a(sb.i, view)) {
            a((ViewGroup) view, view2, joVar);
        } else if (rb.c(view)) {
            a(view, joVar);
        } else if (!(view instanceof SeekBar) && (view instanceof EditText)) {
            a(ak.j, (String) null, rb.a(view, false, rb.a((Class<?>[]) new Class[]{EditText.class})), joVar, view.getId(), rb.b(view2, (Class<?>) EditText.class), rb.e(view));
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        synchronized (this.f1979a) {
            this.f1979a.clear();
        }
    }

    private /* synthetic */ String b(View view) throws Exception {
        String l = rb.l(view);
        return yb.a(l) ? a(rb.e(view)) : l;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (xd.b().d() && pg.h().x()) {
            if (!this.f1979a.isEmpty()) {
                List<nl> list = this.f1979a;
                nl nlVar = list.get(list.size() - 1);
                if (nlVar != null && nlVar.a() == ak.f && xd.b().b() - nlVar.a() <= 4000) {
                    return;
                }
            }
            a(ak.f, str, (String) null, (jo) null);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m5a() {
        if (xd.b().d() && pg.h().x()) {
            if (!this.f1979a.isEmpty()) {
                List<nl> list = this.f1979a;
                nl nlVar = list.get(list.size() - 1);
                if (nlVar != null && nlVar.a() == ak.l) {
                    return;
                }
            }
            a(ak.l, (String) null, zo.a("X"), (jo) null);
        }
    }

    private /* synthetic */ String a(ImageView imageView) throws Exception {
        if (imageView == null) {
            return null;
        }
        return a(imageView.getDrawable());
    }

    public static synchronized bo a() {
        bo boVar;
        synchronized (bo.class) {
            if (b == null) {
                b = new bo();
            }
            boVar = b;
        }
        return boVar;
    }

    /* access modifiers changed from: package-private */
    public nl a(ak akVar) {
        synchronized (this.f1979a) {
            for (int size = this.f1979a.size() - 1; size >= 0; size--) {
                nl nlVar = this.f1979a.get(size);
                if (nlVar != null && nlVar.a() == akVar) {
                    return nlVar;
                }
            }
            return null;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:54:0x0176  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x017c  */
    @android.annotation.TargetApi(14)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(android.view.View r12, com.appsee.jo r13) throws java.lang.Exception {
        /*
            r11 = this;
            java.lang.String r0 = com.appsee.rb.e((android.view.View) r12)
            java.util.List r1 = com.appsee.rb.a()
            boolean r8 = com.appsee.rb.b((android.view.View) r12, (java.util.List<java.lang.Object>) r1)
            android.graphics.Rect r9 = com.appsee.rb.e((android.view.View) r12)
            java.lang.Class<android.widget.AbsListView> r1 = android.widget.AbsListView.class
            android.view.View r1 = com.appsee.rb.a((android.view.View) r12, (java.lang.Class<?>) r1)
            android.widget.AbsListView r1 = (android.widget.AbsListView) r1
            r2 = 1
            if (r1 == 0) goto L_0x0035
            android.graphics.Rect r1 = com.appsee.rb.e((android.view.View) r1)
            java.lang.String r3 = "#\u0006\u001f\u001b\u0011U\u0002\u001d\u0013U:\u001c\u0005\u0001 \u001c\u0013\u0002Q\u0006V\u0016\u0013\u0019\u001aU\u0014\u001a\u0003\u001b\u0012\u0006"
            java.lang.String r3 = com.appsee.ui.a((java.lang.String) r3)
            com.appsee.gd.b(r2, r3)
            int r3 = r9.left
            int r4 = r1.left
            int r3 = r3 - r4
            int r4 = r9.top
            int r1 = r1.top
            int r4 = r4 - r1
            r9.offsetTo(r3, r4)
        L_0x0035:
            boolean r1 = r12 instanceof android.widget.CheckBox
            r3 = 0
            if (r1 == 0) goto L_0x005b
            com.appsee.ak r0 = com.appsee.ak.f1974a
            r1 = r12
            android.widget.CheckBox r1 = (android.widget.CheckBox) r1
            boolean r1 = r1.isChecked()
            r1 = r1 ^ r2
            java.lang.String r1 = java.lang.Boolean.toString(r1)
            java.lang.Class[] r2 = new java.lang.Class[r2]
            java.lang.Class<android.widget.CheckBox> r4 = android.widget.CheckBox.class
            r2[r3] = r4
            java.util.List r2 = com.appsee.rb.a((java.lang.Class<?>[]) r2)
            java.lang.String r2 = com.appsee.rb.a((android.view.View) r12, (boolean) r3, (java.util.List<java.lang.Object>) r2)
        L_0x0056:
            r3 = r0
            r5 = r2
        L_0x0058:
            r2 = r1
            goto L_0x0170
        L_0x005b:
            boolean r1 = r12 instanceof android.widget.RadioButton
            if (r1 == 0) goto L_0x0070
            com.appsee.ak r1 = com.appsee.ak.c
            r2 = r12
            android.widget.RadioButton r2 = (android.widget.RadioButton) r2
            java.lang.CharSequence r2 = r2.getText()
            java.lang.String r2 = r2.toString()
        L_0x006c:
            r5 = r0
            r3 = r1
            goto L_0x0170
        L_0x0070:
            boolean r1 = r12 instanceof android.widget.ToggleButton
            if (r1 == 0) goto L_0x0082
            com.appsee.ak r1 = com.appsee.ak.c
            r2 = r12
            android.widget.Button r2 = (android.widget.Button) r2
            java.lang.CharSequence r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            goto L_0x006c
        L_0x0082:
            int r1 = android.os.Build.VERSION.SDK_INT
            r4 = 14
            if (r1 <= r4) goto L_0x00a9
            boolean r1 = r12 instanceof android.widget.Switch
            if (r1 == 0) goto L_0x00a9
            com.appsee.ak r0 = com.appsee.ak.f1974a
            r1 = r12
            android.widget.Switch r1 = (android.widget.Switch) r1
            boolean r1 = r1.isChecked()
            r1 = r1 ^ r2
            java.lang.String r1 = java.lang.Boolean.toString(r1)
            java.lang.Class[] r2 = new java.lang.Class[r2]
            java.lang.Class<android.widget.Switch> r4 = android.widget.Switch.class
            r2[r3] = r4
            java.util.List r2 = com.appsee.rb.a((java.lang.Class<?>[]) r2)
            java.lang.String r2 = com.appsee.rb.a((android.view.View) r12, (boolean) r3, (java.util.List<java.lang.Object>) r2)
            goto L_0x0056
        L_0x00a9:
            boolean r1 = r12 instanceof android.widget.ImageButton
            if (r1 == 0) goto L_0x00bb
            com.appsee.ak r1 = com.appsee.ak.c
            r2 = r12
            android.widget.ImageButton r2 = (android.widget.ImageButton) r2
            android.graphics.drawable.Drawable r2 = r2.getDrawable()
            java.lang.String r2 = r11.a((android.graphics.drawable.Drawable) r2)
            goto L_0x006c
        L_0x00bb:
            com.appsee.sb r1 = com.appsee.sb.f2062a
            boolean r1 = com.appsee.rb.a((com.appsee.sb) r1, (android.view.View) r12)
            if (r1 == 0) goto L_0x00d1
            com.appsee.ak r1 = com.appsee.ak.c
            r2 = r12
            android.widget.ImageView r2 = (android.widget.ImageView) r2
            android.graphics.drawable.Drawable r2 = r2.getDrawable()
            java.lang.String r2 = r11.a((android.graphics.drawable.Drawable) r2)
            goto L_0x006c
        L_0x00d1:
            com.appsee.sb r1 = com.appsee.sb.h
            boolean r1 = com.appsee.rb.a((com.appsee.sb) r1, (android.view.View) r12)
            java.lang.String r3 = ""
            if (r1 == 0) goto L_0x0101
            com.appsee.ak r1 = com.appsee.ak.c
            android.view.MenuItem r2 = com.appsee.rb.e((android.view.View) r12)
            if (r2 == 0) goto L_0x00f6
            java.lang.CharSequence r4 = r2.getTitle()
            boolean r4 = com.appsee.yb.a((java.lang.CharSequence) r4)
            if (r4 != 0) goto L_0x00f6
            java.lang.CharSequence r2 = r2.getTitle()
            java.lang.String r2 = r2.toString()
            goto L_0x014a
        L_0x00f6:
            if (r2 == 0) goto L_0x0127
            android.graphics.drawable.Drawable r2 = r2.getIcon()
            java.lang.String r2 = r11.a((android.graphics.drawable.Drawable) r2)
            goto L_0x014a
        L_0x0101:
            com.appsee.sb r1 = com.appsee.sb.g
            boolean r1 = com.appsee.rb.a((com.appsee.sb) r1, (android.view.View) r12)
            if (r1 == 0) goto L_0x012a
            com.appsee.ak r1 = com.appsee.ak.c
            java.lang.Class<android.widget.ImageView> r4 = android.widget.ImageView.class
            java.util.List r4 = com.appsee.rb.a((android.view.View) r12, (java.lang.Class<?>) r4)
            int r5 = r4.size()
            r6 = 2
            if (r5 != r6) goto L_0x0127
            java.lang.Object r2 = r4.get(r2)
            android.widget.ImageView r2 = (android.widget.ImageView) r2
            android.graphics.drawable.Drawable r2 = r2.getDrawable()
            java.lang.String r2 = r11.a((android.graphics.drawable.Drawable) r2)
            goto L_0x014a
        L_0x0127:
            r2 = r1
            r1 = r3
            goto L_0x016c
        L_0x012a:
            com.appsee.sb r1 = com.appsee.sb.e
            boolean r1 = com.appsee.rb.a((com.appsee.sb) r1, (android.view.View) r12)
            if (r1 == 0) goto L_0x014e
            com.appsee.ak r1 = com.appsee.ak.c
            android.widget.ImageView r2 = com.appsee.rb.e((android.view.View) r12)
            java.lang.String r2 = r11.a((android.widget.ImageView) r2)
            boolean r3 = com.appsee.yb.a((java.lang.String) r2)
            if (r3 == 0) goto L_0x014a
            java.lang.String r2 = "z\nO\u0000M\u0000O\u0010\u001b*S\u0006T\u001a^\u001b"
            java.lang.String r2 = com.appsee.zo.a((java.lang.String) r2)
            goto L_0x006c
        L_0x014a:
            r10 = r2
            r2 = r1
            r1 = r10
            goto L_0x016c
        L_0x014e:
            com.appsee.sb r1 = com.appsee.sb.j
            boolean r1 = com.appsee.rb.a((com.appsee.sb) r1, (android.view.View) r12)
            if (r1 == 0) goto L_0x015e
            com.appsee.ak r1 = com.appsee.ak.c
            java.lang.String r2 = r11.b((android.view.View) r12)
            goto L_0x006c
        L_0x015e:
            com.appsee.ak r1 = com.appsee.ak.c
            r2 = r12
            android.widget.Button r2 = (android.widget.Button) r2
            java.lang.CharSequence r2 = r2.getText()
            java.lang.String r2 = r2.toString()
            goto L_0x014a
        L_0x016c:
            r5 = r0
            r3 = r2
            goto L_0x0058
        L_0x0170:
            boolean r0 = com.appsee.yb.a((java.lang.String) r2)
            if (r0 == 0) goto L_0x017c
            java.lang.String r0 = r11.a((android.view.View) r12)
            r4 = r0
            goto L_0x017d
        L_0x017c:
            r4 = r2
        L_0x017d:
            int r7 = r12.getId()
            r2 = r11
            r6 = r13
            r2.a(r3, r4, r5, r6, r7, r8, r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.bo.a(android.view.View, com.appsee.jo):void");
    }

    private static /* synthetic */ Bitmap a(Bitmap bitmap, Dimension dimension, boolean z) {
        Bitmap createBitmap = Bitmap.createBitmap(dimension.c(), dimension.a(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        canvas.setDensity(0);
        Paint paint = new Paint(7);
        if (z) {
            ColorMatrix colorMatrix = new ColorMatrix();
            colorMatrix.setSaturation(0.0f);
            paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
            canvas.drawColor(-1);
        }
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawBitmap(bitmap, new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight()), new Rect(0, 0, dimension.c(), dimension.a()), paint);
        return createBitmap;
    }

    /* access modifiers changed from: package-private */
    public void a(ak akVar, String str, String str2, jo joVar, int i, boolean z, Rect rect) {
        ak akVar2 = akVar;
        if (xd.b().d() && xd.b().b() != -1) {
            if ((akVar2 == ak.k || akVar2 == ak.e) && !this.f1979a.isEmpty()) {
                List<nl> list = this.f1979a;
                if (list.get(list.size() - 1).a() == akVar2) {
                    return;
                }
            }
            nl nlVar = new nl(akVar, str, str2, xd.b().b(), rb.a(rect));
            if (i > 0) {
                nlVar.a(String.valueOf(i));
                nlVar.a(Boolean.valueOf(z));
            }
            if (joVar != null) {
                nlVar.a(joVar.b());
            }
            gd.a(1, ui.a("\\_\\U7\u0011\u0012\u0010\u0012U\u0017\u0016\u0002\u001c\u0019\u001bV\u0001\u000f\u0005\u0013UKUS\u0006ZU\u0006\u0014\u0004\u0014\u001b\u0010\u0002\u0010\u0004\u0006VHVP\u0005U\u001f\u001b\u0012\u0010\u000eUKUS\u0006V\u0014\u0002US\u0011"), akVar.toString(), str, str2, Long.valueOf(nlVar.a()));
            synchronized (this.f1979a) {
                this.f1979a.add(nlVar);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(ak akVar, String str, String str2, jo joVar) {
        a(akVar, str, str2, joVar, -1, false, (Rect) null);
    }

    private /* synthetic */ String a(Drawable drawable) throws Exception {
        byte[] b2;
        if (drawable == null || (b2 = b(drawable)) == null) {
            return null;
        }
        String encodeToString = Base64.encodeToString(b2, 0);
        gd.a(1, zo.a(" v.\u001b!z:sI\u0006I\u001e\u001a"), encodeToString);
        mc a2 = sc.a().a();
        if (a2 != null) {
            for (md mdVar : pg.h().m()) {
                if (a2.a().equalsIgnoreCase(mdVar.a()) && mdVar.a().contains(encodeToString)) {
                    lg.a(String.format(Locale.US, ui.a("S\u0006)P\u0005[S\u0006"), new Object[]{pg.h().m(), mdVar.b(), "png"}), drawable);
                }
            }
        }
        return String.format(zo.a("R\u0004Z\u000e^S\u001e\u001a"), new Object[]{encodeToString});
    }

    /* access modifiers changed from: package-private */
    public void a(AbsListView absListView, MotionEvent motionEvent, jo joVar) {
        int a2 = rb.a(absListView, (int) (short) ((int) motionEvent.getY(motionEvent.getActionIndex())));
        if (a2 < 0) {
            joVar.a(false);
            return;
        }
        a(ak.b, String.format(Locale.US, ui.a("F[S\u0011"), new Object[]{Integer.valueOf(a2)}), rb.a((View) absListView, false, rb.a((Class<?>[]) new Class[]{AbsListView.class})), joVar);
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        if (view instanceof SeekBar) {
            a(ak.g, Integer.toString(((SeekBar) view).getProgress()), rb.a(view, false, rb.a((Class<?>[]) new Class[]{SeekBar.class})), (jo) null, view.getId(), rb.b(view, (Class<?>) SeekBar.class), rb.e(view));
        }
    }

    /* access modifiers changed from: package-private */
    public void a(TabWidget tabWidget, View view, jo joVar) throws Exception {
        String str;
        View a2 = rb.a((ViewGroup) tabWidget, view);
        String b2 = b(a2);
        String a3 = rb.a((View) tabWidget, false, rb.a((Class<?>[]) new Class[]{TabWidget.class}));
        int a4 = rb.a((ViewGroup) tabWidget, a2);
        if (a4 != -1) {
            str = String.format(Locale.US, ui.a("P\u0005[S\u0011"), new Object[]{a3, Integer.valueOf(a4)});
        } else {
            str = a3;
        }
        a(ak.c, b2, str, joVar, a2.getId(), rb.b(a2, a2.getClass()), rb.e(a2));
    }

    /* access modifiers changed from: package-private */
    public void a(ViewGroup viewGroup, View view, jo joVar) throws Exception {
        String str;
        if (viewGroup != null && view != null) {
            if ((view instanceof EditText) || (view instanceof ImageView)) {
                ArrayList arrayList = new ArrayList(rb.a());
                arrayList.add(ImageView.class);
                String a2 = rb.a(view, true, (List<Object>) arrayList);
                String b2 = b(view);
                if (yb.a(b2)) {
                    List a3 = rb.a((View) viewGroup, (Class<?>) ImageView.class);
                    if (a3.size() > 2) {
                        str = a(((ImageView) a3.get(1)).getDrawable());
                        a(ak.c, str, a2, joVar, view.getId(), rb.b(view, view.getClass()), rb.e(view));
                    }
                }
                str = b2;
                a(ak.c, str, a2, joVar, view.getId(), rb.b(view, view.getClass()), rb.e(view));
            }
        }
    }
}
