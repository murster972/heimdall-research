package com.appsee;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.TextView;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

class hn {
    private static hn d;

    /* renamed from: a  reason: collision with root package name */
    private boolean f2004a = false;
    private String b;
    private WeakReference<Object> c;

    private /* synthetic */ hn() {
    }

    public static synchronized hn a() {
        hn hnVar;
        synchronized (hn.class) {
            if (d == null) {
                d = new hn();
            }
            hnVar = d;
        }
        return hnVar;
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 20);
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'N');
        }
        return new String(cArr);
    }

    private /* synthetic */ void b(Object obj) throws Exception {
        Window.Callback b2;
        if (obj == null || !(obj instanceof r) || (b2 = ((r) obj).b()) == null || !b2.getClass().getName().equalsIgnoreCase(jm.a("Y(Wi[)^5U.^iS)N\"H)[+\u0014&J7\u0014\u0004R(U4_5{$N.L.N>"))) {
            this.f2004a = false;
        } else {
            this.f2004a = true;
        }
    }

    private /* synthetic */ void c(r rVar) throws Exception {
        if (xd.b().d() && pg.h().R()) {
            List<View> a2 = rb.a(rVar.a(), (Class<?>) AbsListView.class);
            if (((AbsListView) rb.d(a2)) == null) {
                gd.b(0, qb.a("<\u0004\u001b\f\u0007\n\u001b\u0004U\u000e\u0010\r\u0000C\u0011\u0006\u0001\u0006\u0016\u0017\u001c\f\u001bC\u0017\u0006\u0016\u0002\u0000\u0010\u0010C\u0016\u0002\u001bD\u0001C\u0013\n\u001b\u0007U\u0002\u001b\u001aU\u0015\u001c\u0010\u001c\u0001\u0019\u0006U\u000f\u001c\u0010\u0001C\u0003\n\u0010\u0014"));
                return;
            }
            ArrayList<String> arrayList = new ArrayList<>();
            for (View view : a2) {
                if (view.isShown() && view.getWidth() > 0 && view.getHeight() > 0) {
                    AbsListView absListView = (AbsListView) view;
                    for (int i = 0; i < absListView.getChildCount(); i++) {
                        for (TextView text : rb.a(absListView.getChildAt(i), (Class<?>) TextView.class)) {
                            arrayList.add(text.getText().toString());
                        }
                    }
                }
            }
            StringBuilder sb = new StringBuilder(arrayList.size() * 50);
            sb.append(arrayList.size());
            for (String append : arrayList) {
                sb.append(append);
            }
            String format = String.format(jm.a("w\"T2\u001f4"), new Object[]{yb.a(sb.toString())});
            if (a(rVar, format, true)) {
                sc.a().a(format, wc.b, true);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m32a() {
        return this.f2004a;
    }

    private /* synthetic */ void a(r rVar, Menu menu) throws Exception {
        if (menu != null && xd.b().d() && pg.h().R()) {
            List<View> b2 = rVar.b();
            if (b2 == null || b2.isEmpty()) {
                gd.b(0, jm.a("\u000e])U5S)]gW\"T2\u001a#_3_$N.U)\u001a%_$[2I\"\u001a0H&J7_5\u001d4\u001a*_)O4\u001a&H\"\u001a\"W7N>"));
                return;
            }
            StringBuilder sb = new StringBuilder(b2.size() * 50);
            sb.append(b2.size());
            for (View e : b2) {
                MenuItem e2 = rb.e(e);
                if (!(e2 == null || e2.getTitle() == null)) {
                    sb.append(e2.getTitle().toString());
                }
            }
            String format = String.format(qb.a("8\u0006\u001b\u0016P\u0010"), new Object[]{yb.a(sb.toString())});
            if (a((Object) menu, format)) {
                sc.a().a(format, wc.b, true);
            }
        }
    }

    public void b() throws Exception {
        a((Object) null, (String) null);
    }

    private /* synthetic */ void b(r rVar) throws Exception {
        if (xd.b().d() && pg.h().b()) {
            Window.Callback b2 = rVar.b();
            if (!rVar.a() || !rb.a(b2)) {
                gd.b(0, qb.a("*\u0012\r\u001a\u0011\u001c\r\u0012C\u0005\f\u0005\u0016\u0005C\u0011\u0006\u0001\u0006\u0016\u0017\u001c\f\u001bC\u0017\u0006\u0016\u0002\u0000\u0010\u0010C\u001b\f\u0001C\u0014C\u0003\u0002\u0019\n\u0011C\u0011\n\u0014\u000f\u001a\u0004"));
                return;
            }
            List<View> a2 = rb.a(rVar.a(), (Class<?>) Button.class);
            if (rb.d(a2) == null) {
                gd.b(0, jm.a("\u000e])U5S)]gJ(J2Jg^\"N\"Y3S(TgX\"Y&O4_gT(\u001a1S4S%V\"\u001a%O3N(T4"));
                return;
            }
            Object a3 = tc.a(tc.a((Object) b2, qb.a("\u0018\"\u0019\u0006\u0007\u0017")), jm.a("W\u0013S3V\""));
            String str = "";
            String obj = (a3 == null || !(a3 instanceof CharSequence)) ? str : a3.toString();
            TextView textView = (TextView) rVar.a().findViewById(16908299);
            if (textView != null) {
                str = textView.getText().toString();
            }
            ArrayList arrayList = new ArrayList();
            for (View view : a2) {
                if (view != null && (view instanceof Button) && view.isShown() && view.getWidth() > 0 && view.getHeight() > 0) {
                    arrayList.add(((Button) view).getText().toString());
                }
            }
            ac acVar = new ac(obj, str, arrayList);
            String b3 = acVar.b();
            if (a((Object) rVar, b3)) {
                sc.a().a(b3, wc.e, true);
                sc.a().a(acVar, b3);
                gd.a(1, qb.a("%\u001a\u0016\u001b\u0007U\r\u0010\u0014U\u0002\u0000\u0017\u001aC\u0006\u0000\u0007\u0006\u0010\rOCP\u0010"), b3);
            }
        }
    }

    private /* synthetic */ boolean a(Object obj, String str) throws Exception {
        return a(obj, str, false);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m31a() throws Exception {
        if (xd.b().d()) {
            if (pg.h().H() || pg.h().b() || pg.h().R()) {
                r a2 = ub.a().a();
                if (a2 != null) {
                    Menu a3 = a2.a();
                    if (a3 == null) {
                        a3 = a2;
                    }
                    wc a4 = a((Object) a3);
                    Object a5 = a();
                    if (gd.a(0)) {
                        String a6 = qb.a("-\u0010\u0014U\u0010\u0016\u0011\u0010\u0006\u001bC\u0001\u001a\u0005\u0006OCP\u0010YC\u0016\u0016\u0007\u0011\u0010\r\u0001C\u0014\u0000\u0001\n\u0003\u0006U\u0010\u0016\u0011\u0010\u0006\u001bYUF\u0006OU\u0000\u0000\u0011\u0007\u0006\u001b\u0017U\u0002\u0016\u0017\u001c\u0015\u0010C\u0003\n\u0010\u0014OCP\u0010");
                        Object[] objArr = new Object[3];
                        objArr[0] = a4;
                        String str = this.b;
                        if (str == null) {
                            str = jm.a("T2V+");
                        }
                        objArr[1] = str;
                        objArr[2] = a5 != null ? a5.toString() : qb.a("\u001b\u0016\u0019\u000f");
                        gd.a(0, a6, objArr);
                    }
                    int i = ko.f2019a[a4.ordinal()];
                    if (i != 1) {
                        if (i != 2) {
                            if (pg.h().H()) {
                                String a7 = a((r) a3);
                                if (a((Object) a3, a7)) {
                                    sc.a().a(a7, a4, true);
                                    gd.a(1, jm.a("\u0001U2T#\u001a)_0\u001a&O3UgI$H\"_)\u0000g\u001f4"), a7);
                                }
                            }
                        } else if (pg.h().b()) {
                            b((r) a3);
                        }
                    } else if (pg.h().R()) {
                        if (a3 instanceof Menu) {
                            a(a2, a3);
                        } else {
                            c((r) a3);
                        }
                    }
                } else if (!zo.b().a() && !zo.b().e()) {
                    throw new Exception(jm.a("\u0004[)T(Ng^\"N\"Y3\u001a3U7\u001a*U4NgM.T#U0"));
                }
            }
        }
    }

    private /* synthetic */ wc a(Object obj) throws Exception {
        if (obj == null) {
            return wc.f2077a;
        }
        if (obj instanceof Menu) {
            return wc.b;
        }
        r rVar = (r) obj;
        Object a2 = rVar.a();
        if (a2 instanceof Window) {
            Window.Callback b2 = rVar.b();
            if (b2 instanceof Activity) {
                int a3 = rb.a(rVar.a(), (Class<?>) EditText.class);
                int a4 = rb.a(rVar.a(), (Class<?>) Button.class);
                if (a3 < 2 || a4 <= 0) {
                    return wc.f2077a;
                }
                return wc.d;
            } else if (rb.a(b2)) {
                if ((b2 instanceof TimePickerDialog) || (b2 instanceof DatePickerDialog)) {
                    return wc.f2077a;
                }
                if (rb.a(rVar.a(), (Class<?>) AbsListView.class) > 0) {
                    return wc.b;
                }
                return wc.e;
            }
        } else if (a2 instanceof PopupWindow) {
            return wc.b;
        }
        return wc.f2077a;
    }

    private /* synthetic */ boolean a(Object obj, String str, boolean z) throws Exception {
        String str2;
        if (z && obj == a()) {
            return false;
        }
        if ((this.b != null || str == null) && ((str2 = this.b) == null || str2.equals(str))) {
            return false;
        }
        this.c = new WeakReference<>(obj);
        this.b = str;
        b(obj);
        return true;
    }

    private /* synthetic */ String a(r rVar) throws Exception {
        Object a2 = rVar.a();
        if (a2 == null) {
            return "";
        }
        Window.Callback b2 = rVar.b();
        if (a2 instanceof Window) {
            if (b2 != null) {
                String name = b2.getClass().getName();
                if (yb.a(name)) {
                    return qb.a("8 ->-:4;C& '&0-U-4.0>");
                }
                return name.lastIndexOf(jm.a("i")) > 0 ? name.substring(name.lastIndexOf(qb.a("M")) + 1) : name;
            } else if (rVar.a() != null && rb.c(rVar.a(), (Class<?>) MediaController.class)) {
                return MediaController.class.getSimpleName();
            }
        }
        return a2.getClass().getName();
    }
}
