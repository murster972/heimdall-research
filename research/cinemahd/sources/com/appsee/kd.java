package com.appsee;

import android.app.Activity;
import android.app.Instrumentation;

class kd extends Instrumentation {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ zo f2017a;

    kd(zo zoVar) {
        this.f2017a = zoVar;
    }

    private /* synthetic */ void a(Activity activity) {
        oj.b((z) new od(this, activity));
    }

    public void callActivityOnDestroy(Activity activity) {
        a(activity);
        this.f2017a.a(activity);
        super.callActivityOnDestroy(activity);
    }

    public void callActivityOnPause(Activity activity) {
        oj.b((z) new ld(this));
        super.callActivityOnPause(activity);
    }

    public void callActivityOnResume(Activity activity) {
        oj.b((z) new dd(this, activity));
        super.callActivityOnResume(activity);
    }

    public void callActivityOnStop(Activity activity) {
        a(activity);
        super.callActivityOnStop(activity);
    }
}
