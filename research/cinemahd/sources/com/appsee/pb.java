package com.appsee;

import android.graphics.Canvas;
import android.graphics.Paint;
import com.facebook.common.util.ByteConstants;
import java.io.File;

class pb {
    private static pb d;

    /* renamed from: a  reason: collision with root package name */
    private boolean f2049a;
    private final int[] b = {-256, -16776961, -16711936, -65536};
    private final int[] c = {-65536, -16711936, -16776961, -256};

    private /* synthetic */ pb() {
    }

    static boolean a(String str) {
        return str.startsWith("TEST-");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public void m69a() throws Exception {
        if (!this.f2049a) {
            this.f2049a = true;
            File a2 = lg.a(String.format(sd.a("\\ \\ Wv\n"), new Object[]{"TEST-", pg.h().m(), "mp4"}));
            try {
                Dimension dimension = new Dimension(pg.h().k(), pg.h().E());
                int g = pg.h().g() * ByteConstants.KB;
                int M = (int) pg.h().M();
                long b2 = di.b();
                gd.a(1, hn.a("W<q/`'z)4:q=`nb'p+{t4=}4qn)n1*lkpnr<u#q<u:qn)n1*4nd/`&4s4kg"), Integer.valueOf(dimension.c()), Integer.valueOf(dimension.a()), Integer.valueOf(M), a2.getAbsolutePath());
                qb qbVar = new qb();
                qbVar.a(dimension.c(), dimension.a(), g, M, a2.getAbsolutePath(), pg.h().h().contains(tg.d));
                ch chVar = new ch(dimension.c(), dimension.a());
                Paint paint = new Paint();
                paint.setStrokeWidth(0.0f);
                a(chVar, dimension, paint, this.b);
                long j = 0;
                int i = 0;
                while (i <= M * 4) {
                    if (i == M || i == M * 3) {
                        a(chVar, dimension, paint, this.c);
                    }
                    if (i == M * 2) {
                        a(chVar, dimension, paint, this.b);
                    }
                    gd.a(1, sd.a("\u0004\u000b:\r:\u00174Y'\u001c \rs\u001f!\u0018>\u001ciYv\u001dY\u0007\u0010>\u001ciYv\u001d"), Integer.valueOf(i), Long.valueOf(j));
                    qbVar.a(chVar, j);
                    i++;
                    j += (long) (1000000 / M);
                }
                gd.b(1, hn.a("R'z'g&} sn`+g:48}*q!:`:"));
                qbVar.b();
                gd.a(1, sd.a("?:\u0017:\n;\u001c7Y0\u000b6\u0018'\u0010=\u001es\r6\n'Y%\u00107\u001c<Y:\u0017s\\7Y>\u0010?\u0015:\n6\u001a<\u00177\n"), Long.valueOf(di.b() - b2));
                chVar.a();
            } catch (Exception e) {
                qe.a(e, hn.a("\u000bf<{<4'znw<q/`'z)4:q=`nb'p+{"));
                lg.b(a2);
            } finally {
                this.f2049a = false;
            }
        }
    }

    private /* synthetic */ void a(ch chVar, Dimension dimension, Paint paint, int[] iArr) {
        int c2 = (int) (((float) dimension.c()) * 0.5f);
        int c3 = (int) (((float) dimension.c()) * 0.75f);
        chVar.b().drawColor(-16777216);
        paint.setColor(iArr[0]);
        Canvas b2 = chVar.b();
        float c4 = (float) ((int) (((float) dimension.c()) * 0.25f));
        Paint paint2 = paint;
        b2.drawRect(0.0f, 0.0f, c4, (float) dimension.a(), paint2);
        paint.setColor(iArr[1]);
        float f = (float) c2;
        chVar.b().drawRect(c4, 0.0f, f, (float) dimension.a(), paint2);
        paint.setColor(iArr[2]);
        float f2 = (float) c3;
        chVar.b().drawRect(f, 0.0f, f2, (float) dimension.a(), paint2);
        paint.setColor(iArr[3]);
        chVar.b().drawRect(f2, 0.0f, (float) dimension.c(), (float) dimension.a(), paint2);
    }

    public static synchronized pb a() {
        pb pbVar;
        synchronized (pb.class) {
            if (d == null) {
                d = new pb();
            }
            pbVar = d;
        }
        return pbVar;
    }
}
