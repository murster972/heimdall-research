package com.appsee;

import android.os.Handler;
import android.os.HandlerThread;

class gh extends HandlerThread {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1999a = false;
    final /* synthetic */ zi b;
    private Handler c;
    /* access modifiers changed from: private */
    public final Object d = new Object();
    /* access modifiers changed from: private */
    public Exception e = null;
    private final Object f = new Object();

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    gh(zi ziVar) {
        super(ch.a("gqVrCdgr_oEWOeCncoEnBdTUNsC`B"), 0);
        this.b = ziVar;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:2:0x0003 */
    /* JADX WARNING: Removed duplicated region for block: B:2:0x0003 A[LOOP:0: B:2:0x0003->B:13:0x0003, LOOP_START, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r2 = this;
            java.lang.Object r0 = r2.f
            monitor-enter(r0)
        L_0x0003:
            boolean r1 = r2.f1999a     // Catch:{ all -> 0x000f }
            if (r1 != 0) goto L_0x000d
            java.lang.Object r1 = r2.f     // Catch:{ InterruptedException -> 0x0003 }
            r1.wait()     // Catch:{ InterruptedException -> 0x0003 }
            goto L_0x0003
        L_0x000d:
            monitor-exit(r0)     // Catch:{ all -> 0x000f }
            return
        L_0x000f:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x000f }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.gh.a():void");
    }

    public void b() {
        quit();
    }

    /* access modifiers changed from: package-private */
    public void c() throws Exception {
        a();
        synchronized (this.d) {
            this.c.sendEmptyMessage(1);
            this.d.wait();
            if (this.e != null) {
                throw this.e;
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onLooperPrepared() {
        super.onLooperPrepared();
        this.c = new se(this, getLooper());
        synchronized (this.f) {
            this.f1999a = true;
            this.f.notify();
        }
    }
}
