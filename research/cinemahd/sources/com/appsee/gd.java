package com.appsee;

import android.support.v4.media.session.PlaybackStateCompat;
import android.util.Log;
import java.io.FileOutputStream;

class gd {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f1998a;
    private static long b;

    gd() {
    }

    /* renamed from: a  reason: collision with other method in class */
    public static boolean m30a(int i) {
        try {
            if (i < Appsee.c) {
                return false;
            }
            return i != 0 || !pg.h().h().contains(tg.b) || ub.a().a() % 5 == 0;
        } catch (Exception e) {
            Log.e("appsee", String.format(zo.a("~\u001bI\u0006II_\fO\fI\u0004R\u0007R\u0007\\IW\u0006\\IW\fM\fWG\u001b,I\u001bT\u001b\u0001I\u001e\u001a"), new Object[]{Log.getStackTraceString(e)}));
            return true;
        }
    }

    public static void b(int i, String str) {
        a(str, (Exception) null, i, false);
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ void b() throws java.lang.Exception {
        /*
            java.lang.Runtime r0 = java.lang.Runtime.getRuntime()
            java.lang.String r1 = "\u0005T\u000eX\bOI\u0016\r\u001bDMIW\u0006U\u000e\u001bC\u0001C"
            java.lang.String r1 = com.appsee.zo.a((java.lang.String) r1)
            java.lang.Process r0 = r0.exec(r1)
            java.io.BufferedReader r1 = new java.io.BufferedReader
            java.io.InputStreamReader r2 = new java.io.InputStreamReader
            java.io.InputStream r3 = r0.getInputStream()
            r2.<init>(r3)
            r1.<init>(r2)
            java.lang.String r2 = "\u000fd>g+qng/b'z)4\"{)w/`nu:4kg"
            java.lang.String r2 = com.appsee.hn.a((java.lang.String) r2)
            r3 = 1
            java.lang.Object[] r4 = new java.lang.Object[r3]
            java.util.Date r5 = new java.util.Date
            r5.<init>()
            java.lang.String r5 = r5.toString()
            r6 = 0
            r4[r6] = r5
            java.lang.String r2 = java.lang.String.format(r2, r4)
            java.lang.String r4 = "appsee"
            android.util.Log.d(r4, r2)
            long r4 = com.appsee.di.b()
            r6 = 0
            b = r6
            r6 = 0
        L_0x0043:
            long r7 = com.appsee.di.b()     // Catch:{ all -> 0x009e }
            long r7 = r7 - r4
            r9 = 1000(0x3e8, double:4.94E-321)
            int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r11 > 0) goto L_0x0092
            java.lang.String r7 = r1.readLine()     // Catch:{ all -> 0x009e }
            if (r7 != 0) goto L_0x0055
            goto L_0x0092
        L_0x0055:
            boolean r8 = r7.contains(r2)     // Catch:{ all -> 0x009e }
            if (r8 == 0) goto L_0x005c
            goto L_0x0092
        L_0x005c:
            if (r6 != 0) goto L_0x008c
            java.lang.String r8 = "W\u0006\\\nZ\u001d\u0015\u0005T\u000e"
            java.lang.String r8 = com.appsee.zo.a((java.lang.String) r8)     // Catch:{ all -> 0x009e }
            java.io.File r8 = com.appsee.lg.a((java.lang.String) r8)     // Catch:{ all -> 0x009e }
            boolean r9 = r8.exists()     // Catch:{ all -> 0x009e }
            if (r9 == 0) goto L_0x0074
            long r9 = r8.length()     // Catch:{ all -> 0x009e }
            b = r9     // Catch:{ all -> 0x009e }
        L_0x0074:
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ all -> 0x009e }
            r9.<init>(r8, r3)     // Catch:{ all -> 0x009e }
            java.lang.String r6 = "9c9c9c9c9c9c9c9c9c9c9c9nZ+cnP;y>4c9c9c9c9c9c9c9c9c9c9c9c9c9c9"
            java.lang.String r6 = com.appsee.hn.a((java.lang.String) r6)     // Catch:{ all -> 0x0089 }
            boolean r6 = a((java.io.FileOutputStream) r9, (java.lang.String) r6)     // Catch:{ all -> 0x0089 }
            if (r6 != 0) goto L_0x0087
            r6 = r9
            goto L_0x0092
        L_0x0087:
            r6 = r9
            goto L_0x008c
        L_0x0089:
            r2 = move-exception
            r6 = r9
            goto L_0x009f
        L_0x008c:
            boolean r7 = a((java.io.FileOutputStream) r6, (java.lang.String) r7)     // Catch:{ all -> 0x009e }
            if (r7 != 0) goto L_0x0043
        L_0x0092:
            if (r6 == 0) goto L_0x0097
            r6.close()
        L_0x0097:
            r1.close()
            r0.destroy()
            return
        L_0x009e:
            r2 = move-exception
        L_0x009f:
            if (r6 == 0) goto L_0x00a4
            r6.close()
        L_0x00a4:
            r1.close()
            r0.destroy()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.gd.b():void");
    }

    public static void a(int i) {
        Appsee.c = i;
    }

    public static void a(boolean z) {
        f1998a = z;
    }

    static void a(int i, String str) {
        a(str, (Exception) null, i, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0055 A[Catch:{ Exception -> 0x0059 }] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static /* synthetic */ void a(java.lang.String r8, java.lang.Exception r9, int r10, boolean r11) {
        /*
            java.lang.String r0 = "appsee"
            r1 = 1
            r2 = 0
            r3 = 2
            boolean r4 = a((int) r10)     // Catch:{ Exception -> 0x0059 }
            if (r4 != 0) goto L_0x000c
            return
        L_0x000c:
            if (r8 != 0) goto L_0x0010
            java.lang.String r8 = ""
        L_0x0010:
            if (r9 == 0) goto L_0x0026
            java.lang.String r4 = "\u001e\u001a\u001bD\u001b,C\n^\u0019O\u0000T\u0007\u0001I\u001e\u001a\u0015c"
            java.lang.String r4 = com.appsee.zo.a((java.lang.String) r4)     // Catch:{ Exception -> 0x0059 }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0059 }
            r5[r2] = r8     // Catch:{ Exception -> 0x0059 }
            java.lang.String r9 = android.util.Log.getStackTraceString(r9)     // Catch:{ Exception -> 0x0059 }
            r5[r1] = r9     // Catch:{ Exception -> 0x0059 }
            java.lang.String r8 = java.lang.String.format(r4, r5)     // Catch:{ Exception -> 0x0059 }
        L_0x0026:
            java.util.Locale r9 = java.util.Locale.US     // Catch:{ Exception -> 0x0059 }
            java.lang.String r4 = "@t1*.kg"
            java.lang.String r4 = com.appsee.hn.a((java.lang.String) r4)     // Catch:{ Exception -> 0x0059 }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x0059 }
            java.lang.Thread r6 = java.lang.Thread.currentThread()     // Catch:{ Exception -> 0x0059 }
            long r6 = r6.getId()     // Catch:{ Exception -> 0x0059 }
            java.lang.Long r6 = java.lang.Long.valueOf(r6)     // Catch:{ Exception -> 0x0059 }
            r5[r2] = r6     // Catch:{ Exception -> 0x0059 }
            r5[r1] = r8     // Catch:{ Exception -> 0x0059 }
            java.lang.String r8 = java.lang.String.format(r9, r4, r5)     // Catch:{ Exception -> 0x0059 }
            if (r11 != 0) goto L_0x004e
            r9 = 3
            if (r10 < r9) goto L_0x004a
            goto L_0x004e
        L_0x004a:
            android.util.Log.d(r0, r8)     // Catch:{ Exception -> 0x0059 }
            goto L_0x0051
        L_0x004e:
            android.util.Log.e(r0, r8)     // Catch:{ Exception -> 0x0059 }
        L_0x0051:
            boolean r9 = f1998a     // Catch:{ Exception -> 0x0059 }
            if (r9 == 0) goto L_0x0078
            com.appsee.lg.c(r8)     // Catch:{ Exception -> 0x0059 }
            return
        L_0x0059:
            r9 = move-exception
            java.lang.String r9 = android.util.Log.getStackTraceString(r9)     // Catch:{ Exception -> 0x005f }
            goto L_0x0065
        L_0x005f:
            java.lang.String r9 = "<U\u0002U\u0006L\u0007"
            java.lang.String r9 = com.appsee.zo.a((java.lang.String) r9)
        L_0x0065:
            java.lang.String r10 = "\bu:u\"4+f<{<49f'`'z)4:{nx!s`4\u000bf<{<.n1=\u001e\u0001f's'z/xny+g=u)qt4kg"
            java.lang.String r10 = com.appsee.hn.a((java.lang.String) r10)
            java.lang.Object[] r11 = new java.lang.Object[r3]
            r11[r2] = r9
            r11[r1] = r8
            java.lang.String r8 = java.lang.String.format(r10, r11)
            android.util.Log.e(r0, r8)
        L_0x0078:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.gd.a(java.lang.String, java.lang.Exception, int, boolean):void");
    }

    private static /* synthetic */ boolean a(FileOutputStream fileOutputStream, String str) throws Exception {
        byte[] a2 = yb.a(String.format(hn.a("1=\u001e"), new Object[]{str}));
        b += (long) a2.length;
        if (b >= PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED) {
            return false;
        }
        fileOutputStream.write(a2);
        return true;
    }

    /* renamed from: a  reason: collision with other method in class */
    static void m29a() {
        if (pg.h().h().contains(tg.e)) {
            try {
                b();
            } catch (Exception e) {
                qe.a(e, zo.a("*Z\u0007U\u0006OI\\\fOIW\u0006\\\nZ\u001d\u001b\u000fI\u0006VI_\fM\u0000X\f"));
            }
        }
    }

    public static int a() {
        return Appsee.c;
    }

    public static void a(int i, String str, Object... objArr) {
        if (a(i)) {
            a(String.format(str, objArr), (Exception) null, i, false);
        }
    }
}
