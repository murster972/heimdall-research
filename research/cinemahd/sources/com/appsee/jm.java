package com.appsee;

class jm {

    /* renamed from: a  reason: collision with root package name */
    private long f2013a;
    private double b;
    private int c;
    private String d;

    public jm(String str) {
        this.d = str;
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 'G');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ ':');
        }
        return new String(cArr);
    }

    public void a() {
        this.c = 0;
        this.b = (double) 0;
    }

    /* renamed from: b  reason: collision with other method in class */
    public void m37b() {
        this.f2013a = System.nanoTime();
    }

    /* renamed from: c  reason: collision with other method in class */
    public void m38c() {
        gd.a(1, sd.a("GmGs\\ Y{\\7PsDs\\7Y>\n"), this.d, Integer.valueOf(this.c), Integer.valueOf(c()));
    }

    public void d() {
        if (this.f2013a != 0) {
            this.f2013a = 0;
            this.b += (double) ((System.nanoTime() - this.f2013a) / 1000000);
            this.c++;
        }
    }

    public int b() {
        return this.c;
    }

    public int c() {
        int i = this.c;
        if (i == 0) {
            return -1;
        }
        return (int) (this.b / ((double) i));
    }
}
