package com.appsee;

import org.json.JSONException;
import org.json.JSONObject;

class mc implements k {

    /* renamed from: a  reason: collision with root package name */
    private long f2028a;
    private boolean b;
    private long c = -1;
    private wc d;
    private String e;

    public mc(String str, long j, boolean z, wc wcVar) {
        this.e = str;
        this.f2028a = j;
        this.b = z;
        this.d = wcVar;
    }

    /* renamed from: a  reason: collision with other method in class */
    public wc m59a() {
        return this.d;
    }

    public void b(long j) {
        this.c = j;
    }

    public void a(long j) {
        this.f2028a = j;
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m61a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(jm.a(")"), this.e);
        jSONObject.put(lf.a("g"), this.b);
        jSONObject.put(jm.a("7"), this.d.ordinal());
        jSONObject.put(lf.a("u"), this.f2028a);
        if (this.c != -1) {
            jSONObject.put(jm.a("J3"), this.c);
        }
        return jSONObject;
    }

    public long a() {
        return this.f2028a;
    }

    /* renamed from: a  reason: collision with other method in class */
    public String m60a() {
        return this.e;
    }
}
