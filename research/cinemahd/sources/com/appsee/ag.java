package com.appsee;

import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

class ag implements k {

    /* renamed from: a  reason: collision with root package name */
    private String f1973a;
    private long b;
    private Map<String, Object> c;
    private long d = -1;

    public ag(String str, Map<String, Object> map) {
        this.f1973a = str;
        this.c = map;
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m4a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(zo.a("\u0007"), a());
        jSONObject.put(lf.a("r"), a());
        if (this.d != -1) {
            jSONObject.put(zo.a("K\u001d"), this.d);
        }
        Map<String, Object> map = this.c;
        if (map != null && !map.isEmpty()) {
            JSONObject jSONObject2 = new JSONObject();
            for (Map.Entry next : this.c.entrySet()) {
                jSONObject2.put((String) next.getKey(), next.getValue());
            }
            jSONObject.put(lf.a("v"), jSONObject2);
        }
        return jSONObject;
    }

    public void b(long j) {
        this.d = j;
    }

    public void a(long j) {
        this.b = j;
    }

    /* renamed from: a  reason: collision with other method in class */
    public String m2a() {
        return this.f1973a;
    }

    public long a() {
        return this.b;
    }

    /* renamed from: a  reason: collision with other method in class */
    public Map<String, Object> m3a() {
        return this.c;
    }
}
