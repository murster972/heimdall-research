package com.appsee;

import android.os.Build;
import java.security.InvalidParameterException;
import java.util.List;

class qb {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2054a = false;
    private j b;
    private lb c = lb.b;

    qb() {
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 'c');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'u');
        }
        return new String(cArr);
    }

    static List<String> a() {
        if (Build.VERSION.SDK_INT >= 16) {
            return cf.a();
        }
        return null;
    }

    public void b() throws Exception {
        j jVar = this.b;
        if (jVar != null) {
            jVar.a();
            this.b = null;
            return;
        }
        throw new Exception(bb.a("wiQhVb@'\\hF'[i[s[f^nHbV'Th@'WiQhVn\\`\u0012f\\'[jS`W"));
    }

    /* renamed from: a  reason: collision with other method in class */
    public boolean m108a() throws Exception {
        j jVar = this.b;
        if (jVar != null) {
            return jVar.a();
        }
        if (this.f2054a) {
            return false;
        }
        throw new Exception(bb.a("wiQhVb@'\\hF'[i[s[f^nHbV'Th@'WiQhVn\\`\u0012f\\'[jS`W"));
    }

    /* access modifiers changed from: package-private */
    public void a(ch chVar, long j) throws Exception {
        if (this.b == null) {
            throw new Exception(ch.a("coEnBdT!HnR!OoOuO`Jh\\dB!@nT!CoEnBhHf\u0006`H!OlGfC"));
        } else if (chVar == null) {
            throw new InvalidParameterException(bb.a("JGtF'ArBw^~\u0012n_fUb\u0012eGaTb@"));
        } else if (j >= 0) {
            chVar.c();
            try {
                this.b.a(chVar, j);
            } finally {
                chVar.d();
            }
        } else {
            throw new InvalidParameterException(ch.a("ktUu\u0006qTdUdHuGuOnHUOlCTU"));
        }
    }

    public void a(int i, int i2, int i3, int i4, String str, boolean z) throws Exception {
        a();
        boolean a2 = AppseeNativeExtensions.a(this.c);
        j jVar = this.b;
        if (jVar == null || !a2) {
            throw new Exception(ch.a("e`HoIu\u0006mI`B!PhBdI!CoEnBdT"));
        }
        this.f2054a = true;
        jVar.a(i, i2, i3, i4, str, z);
        gd.a();
    }
}
