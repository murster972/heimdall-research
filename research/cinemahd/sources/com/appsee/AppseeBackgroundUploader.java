package com.appsee;

import android.app.IntentService;
import android.content.Intent;
import java.io.FilenameFilter;

public class AppseeBackgroundUploader extends IntentService {
    public AppseeBackgroundUploader() {
        super(jm.a("{7J4_\"x&Y,]5U2T#o7V([#_5"));
    }

    private /* synthetic */ void a(Exception exc, kp kpVar) {
        qe.a((Throwable) exc, md.a("%\u0018\u0017\u0018\u000fY&\u0001\u0000\u001c\u0013\r\n\u0016\rY\u0017\u0011\u0011\u0016\u0014\u0017C\u0010\rY\u0001\u0018\u0000\u0012\u0004\u000b\f\f\r\u001dC\u0010\r\r\u0006\u0017\u0017Y\u0010\u001c\u0011\u000f\n\u001a\u0006YK\\\u0010PYY"), kpVar.toString());
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        kp kpVar = kp.d;
        if (intent != null) {
            try {
                if (intent.getExtras() != null) {
                    kpVar = kp.values()[intent.getExtras().getInt("com.appsee.Action.Mode")];
                    gd.a(1, jm.a("\u000eT3_)Ngi\"H1S$_gi3[5N\"^k\u001a*U#_g\u0007g\u001f4"), kpVar.toString());
                    switch (kk.f2018a[kpVar.ordinal()]) {
                        case 1:
                            rd.a().c();
                            break;
                        case 2:
                            rd.a().c(intent.getExtras().getBoolean("com.appsee.Action.UploadMode"));
                            break;
                        case 3:
                            rd.a().f();
                            break;
                        case 4:
                            zo.b().c();
                            break;
                        case 5:
                            if (xd.b().d()) {
                                gd.b(3, md.a(":\u0002\u0017D\rC\u001f\f\u000b\u0000\u001cC\u0018C\u0017\u0006\u000eC\n\u0006\n\u0010\u0010\f\u0017C\u000e\u000b\u0010\u000f\u001cC\u0018\rY\u0006\u0001\n\n\u0017\u0010\r\u001eC\n\u0006\n\u0010\u0010\f\u0017C\u0010\u0010Y\u0002\u001a\u0017\u0010\u0015\u001c"));
                                break;
                            } else {
                                rd.a().m();
                                break;
                            }
                        case 6:
                            wd.a().a(false);
                            break;
                        case 7:
                            lg.a((FilenameFilter) null);
                            break;
                        case 8:
                            break;
                    }
                    gd.a(1, md.a("*\u0017\u0017\u001c\r\rC*\u0006\u000b\u0015\u0010\u0000\u001cC?\n\u0017\n\n\u000b\u001c\u0007UC\u0014\f\u001d\u0006Y^YF\n"), kpVar.toString());
                    return;
                }
            } catch (NullPointerException e) {
                if (e.getMessage().equals("APPSEE_NO_CONTEXT")) {
                    gd.b(1, jm.a("{7J4_\"x&Y,]5U2T#o7V([#_5\u001a$[)T(Ng\\.T#\u001a&J7V.Y&N.U)\u001a$U)N\"B3"));
                    return;
                }
                a(e, kpVar);
            } catch (Exception e2) {
                a(e2, kpVar);
            }
        }
        gd.b(1, md.a("8\u0013\t\u0010\u001c\u0006;\u0002\u001a\b\u001e\u0011\u0016\u0016\u0017\u0007,\u0013\u0015\f\u0018\u0007\u001c\u0011Y\u0000\u0018\r\u0017\f\rC\u001f\n\u0017\u0007Y!\f\r\u001d\u000f\u001cC\u0016\rY\n\u0017\u0017\u001c\r\rC\n\u0006\u000b\u0015\u0010\u0000\u001c"));
    }
}
