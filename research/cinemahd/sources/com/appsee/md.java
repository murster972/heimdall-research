package com.appsee;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

class md {

    /* renamed from: a  reason: collision with root package name */
    private String f2029a;
    private List<String> b = new ArrayList();
    private String c;

    public md(String str, String str2, JSONArray jSONArray) {
        this.f2029a = str;
        this.c = str2;
        if (jSONArray != null && jSONArray.length() > 0) {
            for (int i = 0; i < jSONArray.length(); i++) {
                this.b.add(jSONArray.optString(i));
            }
        }
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 'y');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'c');
        }
        return new String(cArr);
    }

    /* renamed from: a  reason: collision with other method in class */
    public List<String> m62a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public String a() {
        return this.f2029a;
    }
}
