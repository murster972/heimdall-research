package com.appsee;

import android.graphics.Rect;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ui {

    /* renamed from: a  reason: collision with root package name */
    private Rect f2072a;
    private long b;

    public ui(Rect rect, long j) {
        this.b = j;
        a(rect);
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ 'u');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'v');
        }
        return new String(cArr);
    }

    public void a(Rect rect) {
        this.f2072a = rect != null ? new Rect(rect) : null;
    }

    public long a() {
        return this.b;
    }

    public void a(long j) {
        this.b = j;
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m150a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(bb.a("F"), this.b);
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(this.f2072a.left);
        jSONArray.put(this.f2072a.top);
        jSONArray.put(this.f2072a.width());
        jSONArray.put(this.f2072a.height());
        jSONObject.put(bb.a("T"), jSONArray);
        return jSONObject;
    }

    /* renamed from: a  reason: collision with other method in class */
    public Rect m149a() {
        return this.f2072a;
    }
}
