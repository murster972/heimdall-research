package com.appsee;

import android.annotation.TargetApi;
import android.media.MediaCodec;
import android.media.MediaFormat;
import java.nio.ByteBuffer;

@TargetApi(21)
class ih extends MediaCodec.Callback {

    /* renamed from: a  reason: collision with root package name */
    private boolean f2010a;
    final /* synthetic */ zi b;

    private /* synthetic */ ih(zi ziVar) {
        this.b = ziVar;
        this.f2010a = false;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a() {
        this.f2010a = true;
    }

    public void onError(MediaCodec mediaCodec, MediaCodec.CodecException codecException) {
        qe.a(codecException, md.a("<\u0011\u000b\f\u000bC\u0010\rY\u0015\u0010\u0007\u001c\fY\u0006\u0017\u0000\u0016\u0007\u0010\r\u001e"));
    }

    public void onInputBufferAvailable(MediaCodec mediaCodec, int i) {
    }

    public synchronized void onOutputBufferAvailable(MediaCodec mediaCodec, int i, MediaCodec.BufferInfo bufferInfo) {
        if (!zi.a(this.b)) {
            try {
                a(mediaCodec, i, bufferInfo);
            } catch (Exception e) {
                qe.a((Throwable) e, bb.a("\u0017t\u0012B@u]u\u0012h\\']i}rFwGsprTaWusqSn^fPkW/\u001b"), "APPSEE_ENCODER_ERROR:");
                zi.b(this.b);
            }
        }
    }

    public synchronized void onOutputFormatChanged(MediaCodec mediaCodec, MediaFormat mediaFormat) {
        if (!zi.a(this.b)) {
            try {
                if (this.f2010a) {
                    qe.a((Throwable) null, bb.a("]i}rFwGsth@jSsqoSiUbV/\u001b'EfA'Qf^kWc\u0012fTsWu\u0012dSk^eSdY'EfA'VnAw]tWc\u0013"));
                    return;
                }
                if (zi.b(this.b)) {
                    gd.b(1, md.a("5\u0010\u0007\u001c\fY\u0006\u0017\u0000\u0016\u0007\u001c\u0011Y\f\f\u0017Y\u0005\u0016\u0011\u0014\u0002\rC\u001a\u000b\u0018\r\u001e\u0006\u001d"));
                }
                this.b.a(mediaFormat);
            } catch (Exception e) {
                qe.a((Throwable) e, bb.a("\u0017t\u0012B@u]u\u0012h\\']i}rFwGsth@jSsqoSiUbV/\u001b"), "APPSEE_ENCODER_ERROR:");
                zi.b(this.b);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(MediaCodec mediaCodec, int i, MediaCodec.BufferInfo bufferInfo) {
        try {
            if (this.f2010a) {
                qe.a((Throwable) null, md.a("\u0016\r6\u0016\r\u0013\f\u0017;\u0016\u001f\u0005\u001c\u00118\u0015\u0018\n\u0015\u0002\u001b\u000f\u001cKPC\u000e\u0002\nC\u001a\u0002\u0015\u000f\u001c\u0007Y\u0002\u001f\u0017\u001c\u0011Y\u0000\u0018\u000f\u0015\u0001\u0018\u0000\u0012C\u000e\u0002\nC\u001d\n\n\u0013\u0016\u0010\u001c\u0007X"));
                return;
            }
            if (zi.b(this.b)) {
                gd.a(1, bb.a("vuSn\\n\\`\u0012b\\d]cWu\u001e'[iVbJ=\u0012\"V"), Integer.valueOf(i));
            }
            zi.a(this.b).b();
            ByteBuffer duplicate = mediaCodec.getOutputBuffer(i).duplicate();
            if ((bufferInfo.flags & 2) != 0) {
                if (zi.b(this.b)) {
                    gd.b(1, md.a("%\u0016\u0016\u0017\u0007Y\u0000\u0016\r\u001f\n\u001eC\u001f\u0011\u0018\u000e\u001c"));
                }
                bufferInfo.size = 0;
            }
            if ((bufferInfo.flags & 4) != 0) {
                if (zi.b(this.b)) {
                    gd.b(1, bb.a("A]r\\c\u0012B}T\u0012a^fU"));
                }
                bufferInfo.size = 0;
            }
            if (bufferInfo.size != 0) {
                if (zi.a(this.b) == null) {
                    gd.b(1, md.a(".\f\u001b\u001c\u0011Y\n\nC\u0010\r\u0010\u0017\u0010\u0002\u0015\n\u0003\u0006\u001dC\u0016\r\u0015\u001aY\f\u0017C\u001f\n\u000b\u0010\rC\u001d\u0002\r\u0002Y\u0005\u000b\u0002\u0014\u0006YK\f\u0010\f\u0002\u0015\u000f\u0000C\u0010\u0017Y\f\u0017C0-?,&,,7)6-<?,+.87& 1\"7$<'P"));
                    this.b.a(mediaCodec.getOutputFormat(i));
                }
                long b2 = zi.b(this.b);
                if (zi.a(this.b).containsKey(Long.valueOf(b2))) {
                    long longValue = ((Long) zi.a(this.b).get(Long.valueOf(b2))).longValue();
                    if (zi.b(this.b)) {
                        gd.a(1, bb.a("P@nFn\\`\u0012a@f_b\u0012a]u\u0012s[jW=\u0012\"V"), Long.valueOf(longValue));
                    }
                    bufferInfo.presentationTimeUs = longValue;
                    zi.a(this.b).writeSampleData(zi.a(this.b), duplicate, bufferInfo);
                    zi.a(this.b).remove(Long.valueOf(b2));
                } else {
                    gd.a(1, md.a("%\u0016\u0016\u0017\u0007Y\u0006\u0017\u0000\u0016\u0007\u001c\u0007Y\u0005\u000b\u0002\u0014\u0006Y\u0014\u0010\u0017\u0011C\u0017\fY\u0013\u000b\u0006\n\u0006\u0017\u0017\u0018\u0017\u0010\f\u0017C\r\n\u0014\u0006UC\n\b\u0010\u0013\t\n\u0017\u0004Y\u0005\u000b\u0002\u0014\u0006YF\u001d"), Long.valueOf(zi.a(this.b) - 1));
                }
            }
            zi.a(this.b).releaseOutputBuffer(i, false);
            if ((bufferInfo.flags & 4) != 0) {
                zi.a(this.b).countDown();
            }
            zi.a(this.b).d();
        } catch (Exception e) {
            qe.a(e, bb.a("wu@h@'[i\u0012FA~\\dar@aSdWQ[cWhwiQhVb@'Zf\\c^bsqSn^fPkWEGaTb@/\u001b"));
        } catch (Throwable th) {
            zi.a(this.b).releaseOutputBuffer(i, false);
            throw th;
        }
    }
}
