package com.appsee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;

class od implements z {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ Activity f2044a;
    final /* synthetic */ kd b;

    od(kd kdVar, Activity activity) {
        this.b = kdVar;
        this.f2044a = activity;
    }

    @TargetApi(11)
    public void a() throws Exception {
        boolean z;
        if (Build.VERSION.SDK_INT >= 11) {
            z = this.f2044a.isChangingConfigurations();
        } else {
            z = tc.a(em.a("\u001aT\u001cQ\u001eP P\u001eT\u0007[\u0011]3V\u0006\\\u0004\\\u0006L"));
        }
        this.b.f2017a.a(this.f2044a, false, z);
    }
}
