package com.appsee;

import android.view.View;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class xc {

    /* renamed from: a  reason: collision with root package name */
    HashSet<Integer> f2084a = new HashSet<>();
    List<WeakReference<View>> b = new ArrayList();
    final /* synthetic */ cc c;
    HashSet<Integer> d = new HashSet<>();
    List<WeakReference<View>> e = new ArrayList();

    xc(cc ccVar) {
        this.c = ccVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a(cc.b(this.c), this.e);
        a(cc.a(this.c), this.b);
        this.d.clear();
        this.d.addAll(cc.a(this.c));
        this.f2084a.clear();
        this.f2084a.addAll(cc.b(this.c));
    }

    private /* synthetic */ void a(List<WeakReference<View>> list, List<WeakReference<View>> list2) {
        list2.clear();
        for (int i = 0; i < list.size(); i++) {
            list2.add(list.get(i));
        }
    }
}
