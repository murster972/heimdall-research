package com.appsee;

import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class ac implements k {

    /* renamed from: a  reason: collision with root package name */
    private String f1972a;
    private List<String> b;
    private String c;
    private String d;

    public ac(String str, String str2, List<String> list) throws Exception {
        this.f1972a = str;
        this.c = str2;
        this.b = list;
        if (this.f1972a == null) {
            this.f1972a = "";
        }
        if (this.c == null) {
            this.c = "";
        }
        a();
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m1a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        List<String> list = this.b;
        if (list != null) {
            for (String put : list) {
                jSONArray.put(put);
            }
        }
        jSONObject.put(md.a(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE), this.d);
        jSONObject.put(em.a("A"), this.f1972a);
        jSONObject.put(md.a("\u0001"), this.c);
        jSONObject.put(em.a("Z"), jSONArray);
        return jSONObject;
    }

    public String b() {
        return this.d;
    }

    public String a() {
        return this.c;
    }
}
