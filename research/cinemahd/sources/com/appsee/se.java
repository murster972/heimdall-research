package com.appsee;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class se extends Handler {

    /* renamed from: a  reason: collision with root package name */
    final /* synthetic */ gh f2065a;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    se(gh ghVar, Looper looper) {
        super(looper);
        this.f2065a = ghVar;
    }

    public void handleMessage(Message message) {
        if (message.what == 1) {
            try {
                zi.a(this.f2065a.b);
                synchronized (this.f2065a.d) {
                    this.f2065a.d.notify();
                }
            } catch (Exception e) {
                try {
                    Exception unused = this.f2065a.e = e;
                    synchronized (this.f2065a.d) {
                        this.f2065a.d.notify();
                    }
                } catch (Throwable th) {
                    synchronized (this.f2065a.d) {
                        this.f2065a.d.notify();
                        throw th;
                    }
                }
            }
        }
    }
}
