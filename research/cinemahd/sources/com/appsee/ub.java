package com.appsee;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import com.original.tase.model.socket.UserResponces;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class ub implements v {
    private static ub A;

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<Object> f2070a;
    private int b;
    private WeakReference<r> c;
    private ArrayList<Class<?>> d;
    private WeakReference<LinearLayout> e;
    private final Object f;
    private boolean g;
    private int h;
    private jm i;
    private int j;
    private volatile boolean k;
    private boolean l;
    private bb m;
    private jm n;
    private volatile long o;
    private final ArrayList<r> p;
    private final z q;
    private long r;
    private boolean s;
    private ArrayList<Class<?>> t;
    private zn u;
    private u v;
    private jm w;
    private final Rect x;
    private Object y;
    private volatile jm z;

    private /* synthetic */ ub() {
        this.g = false;
        this.v = null;
        this.d = null;
        this.t = null;
        this.e = null;
        this.j = 0;
        this.x = new Rect();
        this.l = false;
        this.s = false;
        this.c = null;
        this.k = false;
        this.o = 0;
        this.p = new ArrayList<>();
        this.u = zn.h;
        this.f = new Object();
        this.z = oc.a().a(bb.a("TKtFb_BDb\\sAK[tFb\\b@"));
        this.i = oc.a().a(md.a("*\u001a\n\u0017\u001c\u000e<\u0015\u001c\r\r/\u0010\u0010\r\u0006\u0017\u0006\u000bN6\u0011\u0010\u0006\u0017\u0017\u0018\u0017\u0010\f\u0017"));
        this.w = oc.a().a(bb.a("a~AsWjwqWiFK[tFb\\b@*swBA@f_bA"));
        this.n = oc.a().a(md.a("*\u001a\n\u0017\u001c\u000e<\u0015\u001c\r\r/\u0010\u0010\r\u0006\u0017\u0006\u000bN.\n\u0017\u0007\u0016\u0014\n"));
        this.f2070a = new ArrayList<>();
        this.d = new ArrayList<>();
        this.t = new ArrayList<>();
        this.q = new cb(this);
        try {
            if (Build.VERSION.SDK_INT >= 24) {
                this.d.add(Class.forName("com.android.internal.policy.DecorView"));
                this.d.add(Class.forName(bb.a("SiVu]nV)EnV`Ws\u001cW]wGwen\\c]p\u0016W]wGwvbQh@Q[bE")));
            } else if (Build.VERSION.SDK_INT == 23) {
                this.d.add(Class.forName(md.a("\u001a\f\u0014M\u0018\r\u001d\u0011\u0016\n\u001dM\u0010\r\r\u0006\u000b\r\u0018\u000fW\u0013\u0016\u000f\u0010\u0000\u0000M)\u000b\u0016\r\u001c4\u0010\r\u001d\f\u000eG=\u0006\u001a\f\u000b5\u0010\u0006\u000e")));
                this.d.add(Class.forName(bb.a("SiVu]nV)EnV`Ws\u001cW]wGwen\\c]p\u0016W]wGwvbQh@Q[bE")));
            } else if (Build.VERSION.SDK_INT < 23) {
                this.d.add(Class.forName(md.a("\u0000\u0016\u000eW\u0002\u0017\u0007\u000b\f\u0010\u0007W\n\u0017\u0017\u001c\u0011\u0017\u0002\u0015M\t\f\u0015\n\u001a\u001aW\n\u0014\u0013\u0015M)\u000b\u0016\r\u001c4\u0010\r\u001d\f\u000eG=\u0006\u001a\f\u000b5\u0010\u0006\u000e")));
                this.d.add(Class.forName(bb.a("SiVu]nV)EnV`Ws\u001cW]wGwen\\c]p\u0016W]wGwdnWpqh\\sSn\\b@")));
            }
            this.t.add(LinearLayout.class);
            if (Build.VERSION.SDK_INT >= 16) {
                this.t.add(Class.forName(md.a("\u0018\r\u001d\u0011\u0016\n\u001dM\u000e\n\u001d\u0004\u001c\u0017W&\u001d\n\r\f\u000bG0\r\n\u0006\u000b\u0017\u0010\f\u0017+\u0018\r\u001d\u000f\u001c5\u0010\u0006\u000e")));
            } else if (Build.VERSION.SDK_INT >= 14) {
                this.t.add(Class.forName(bb.a("SiVu]nV)EnV`Ws\u001cSWFQ[bE#{iAb@s[h\\OSiVkWQ[bE")));
            } else if (Build.VERSION.SDK_INT >= 9) {
                this.t.add(Class.forName(md.a("\u0002\u0017\u0007\u000b\f\u0010\u0007W\u0014\u0010\u0007\u001e\u0006\rM-\u0006\u0001\u0017/\n\u001c\u0014]+\u0018\r\u001d\u000f\u001c5\u0010\u0006\u000e")));
            }
        } catch (ClassNotFoundException e2) {
            qe.a(e2, bb.a("DSi\\hF'Tn\\c\u0012cWd]u\u0012q[bE'QkStA"));
        }
    }

    private /* synthetic */ void e() throws Exception {
        try {
            this.o = di.b();
            this.z.b();
            if (!this.g) {
                b();
            }
            this.i.b();
            di.b().a();
            this.i.d();
            this.g = false;
            this.w.b();
            f();
            this.w.d();
            c();
            zo.b().d();
            ym.a().a();
        } finally {
            this.z.d();
        }
    }

    private /* synthetic */ void f() throws Exception {
        r a2;
        if (!pg.h().B() && xd.b().d() && (a2 = a()) != null) {
            if (xd.b().b() == null) {
                gd.b(1, md.a("*\u0006\r\u0017\u0010\r\u001eC\u0010\r\u0010\u0017\u0010\u0002\u0015C\u0018\u0013\tC\u001f\u0011\u0018\u000e\u001c"));
                xd.b().a(rb.a(a2));
            }
            if (c()) {
                b(a2);
                this.s = true;
                return;
            }
            a(a2);
            this.s = false;
        }
    }

    private /* synthetic */ void g() {
        oj.b((z) new hb(this));
    }

    private /* synthetic */ void h() throws Exception {
        Object[] a2 = a();
        a(a2);
        b(a2);
        int size = this.f2070a.size();
        for (int i2 = 0; i2 < Math.max(size, this.p.size()); i2++) {
            if (i2 >= size) {
                a(i2);
            } else {
                View view = (View) this.f2070a.get(i2);
                Object e2 = rb.e(view);
                if (e2 == null) {
                    gd.a(2, md.a(":\u0002\u0017\r\u0016\u0017Y\u0004\u001c\u0017Y\u0014\u0010\r\u001d\f\u000eC\u001f\f\u000bC\u000b\f\u0016\u0017Y\u0015\u0010\u0006\u000eYYF\n"), view.getClass().getName());
                } else if (i2 >= this.p.size()) {
                    a(i2, e2, view);
                } else if (e2.equals(this.p.get(i2).a())) {
                    this.p.get(i2).c();
                } else {
                    gd.a(1, bb.a("ap[sQo[iU'En\\c]pA']i\u0012n\\cW\u0012\"V"), Integer.valueOf(i2));
                    a(i2);
                    a(i2, e2, view);
                    gd.b(1, md.a("%\u0010\r\u0010\u0010\u0011\u0006\u001dC\n\u0014\u0010\u0017\u001a\u000b\u0010\r\u001eC\u000e\n\u0017\u0007\u0016\u0014\n"));
                }
            }
        }
        if (this.v != null) {
            if (size == 0 && !zo.b().a() && !zo.b().e()) {
                gd.b(2, bb.a("`h]sdnWpA'QhGiF'[t\u00127\u0013"));
            }
            i();
            this.v.a();
        }
        this.g = true;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: com.appsee.r} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void i() {
        /*
            r12 = this;
            java.util.List r0 = r12.a()
            int r1 = r0.size()
            r2 = 0
            r3 = 0
            r4 = -1
            r5 = r2
            r4 = 0
            r6 = 0
            r7 = -1
        L_0x000f:
            if (r4 >= r1) goto L_0x0060
            java.lang.Object r8 = r0.get(r4)
            com.appsee.r r8 = (com.appsee.r) r8
            android.view.Window r9 = r8.a()
            android.view.View r10 = r8.a()
            if (r9 == 0) goto L_0x005d
            if (r10 == 0) goto L_0x005d
            boolean r11 = r10.isShown()
            if (r11 != 0) goto L_0x002a
            goto L_0x005d
        L_0x002a:
            boolean r11 = r12.a((com.appsee.r) r8)
            if (r11 != 0) goto L_0x0031
            goto L_0x005d
        L_0x0031:
            boolean r11 = r10.hasWindowFocus()
            if (r6 != r11) goto L_0x004a
            android.view.WindowManager$LayoutParams r11 = r9.getAttributes()
            int r11 = r11.type
            if (r11 < r7) goto L_0x005d
            android.view.WindowManager$LayoutParams r5 = r9.getAttributes()
            int r5 = r5.type
            boolean r7 = r10.hasWindowFocus()
            goto L_0x005a
        L_0x004a:
            boolean r11 = r10.hasWindowFocus()
            if (r11 == 0) goto L_0x005d
            android.view.WindowManager$LayoutParams r5 = r9.getAttributes()
            int r5 = r5.type
            boolean r7 = r10.hasWindowFocus()
        L_0x005a:
            r6 = r6 | r7
            r7 = r5
            r5 = r8
        L_0x005d:
            int r4 = r4 + 1
            goto L_0x000f
        L_0x0060:
            r0 = 2
            r1 = 1
            if (r5 != 0) goto L_0x0089
            com.appsee.zo r4 = com.appsee.zo.b()
            boolean r4 = r4.a()
            if (r4 != 0) goto L_0x0089
            com.appsee.zo r4 = com.appsee.zo.b()
            boolean r4 = r4.e()
            if (r4 != 0) goto L_0x0089
            java.lang.String r4 = "DSi\\hF'VbFbQs\u0012dGu@b\\s\u0012tQuWb\\)\u0012u]hF'DnWpA=\u0012\"A"
            java.lang.String r4 = com.appsee.bb.a((java.lang.String) r4)
            java.lang.Object[] r6 = new java.lang.Object[r1]
            java.lang.String r7 = r12.a()
            r6[r3] = r7
            com.appsee.gd.a(r0, r4, r6)
        L_0x0089:
            boolean r4 = com.appsee.gd.a((int) r3)
            if (r4 == 0) goto L_0x00af
            java.lang.String r4 = "=&;6>]G]Y7\u0016\u0013.\u0011\u0018\u0013\t\u0006\u000bYYF\nOY\"\u0015\u000fY4\u000b\u0002\t\u0013\u001c\u0011\nYYF\n"
            java.lang.String r4 = com.appsee.md.a(r4)
            java.lang.Object[] r0 = new java.lang.Object[r0]
            if (r5 == 0) goto L_0x009e
            java.lang.String r6 = r5.toString()
            goto L_0x00a4
        L_0x009e:
            java.lang.String r6 = "iGk^"
            java.lang.String r6 = com.appsee.bb.a((java.lang.String) r6)
        L_0x00a4:
            r0[r3] = r6
            java.lang.String r6 = r12.a()
            r0[r1] = r6
            com.appsee.gd.a(r3, r4, r0)
        L_0x00af:
            java.lang.ref.WeakReference<com.appsee.r> r0 = r12.c
            if (r0 == 0) goto L_0x00ba
            java.lang.Object r0 = r0.get()
            r2 = r0
            com.appsee.r r2 = (com.appsee.r) r2
        L_0x00ba:
            if (r2 == 0) goto L_0x00be
            if (r2 == r5) goto L_0x00c5
        L_0x00be:
            java.lang.ref.WeakReference r0 = new java.lang.ref.WeakReference
            r0.<init>(r5)
            r12.c = r0
        L_0x00c5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.ub.i():void");
    }

    private /* synthetic */ void j() throws Exception {
        Object obj;
        Object systemService = ho.a().getSystemService(md.a("\u0014\u0010\r\u001d\f\u000e"));
        Class<?> cls = Class.forName(bb.a("f\\c@h[c\u001cq[bE)en\\c]pf\\fUb@N_w^"));
        if (cls != null) {
            while (systemService != null && !cls.isInstance(systemService)) {
                if (systemService.getClass().getName().equals(md.a("\u0002\u0017\u0007\u000b\f\u0010\u0007W\u0015\u0010\u0006\u000eM.\n\u0017\u0007\u0016\u00144\u0002\u0017\u0002\u001e\u0006\u000b*\u0014\u0013\u0015G:\f\u0014\u0013\u0018\u00174\f\u001d\u0006.\u0011\u0018\u0013\t\u0006\u000b"))) {
                    systemService = tc.a(systemService, bb.a("jen\\c]pf\\fUb@"));
                } else {
                    gd.a(3, md.a("%\u0016\u0016\u0017\u0007Y\u0016\u0017\b\u0017\f\u000e\rY\u0014\u0010\r\u001d\f\u000eC\u0014\u0002\u0017\u0002\u001e\u0006\u000bYYF\n"), systemService.getClass().getName());
                    systemService = null;
                }
            }
            this.y = systemService;
            if (Build.VERSION.SDK_INT >= 17 && (obj = this.y) != null) {
                this.y = tc.a(obj, bb.a("_@^hPf^"));
            }
            if (this.y == null) {
                throw new Exception(md.a(":\u0002\u0017\r\u0016\u0017Y\u0004\u001c\u0017Y\u0017\u0016\u0013T\u000f\u001c\u0015\u001c\u000fY\u0014\u0010\r\u001d\f\u000eC\u0014\u0002\u0017\u0002\u001e\u0006\u000b"));
            }
            return;
        }
        gd.b(3, md.a(" \u0018\r\u0017\f\rC\u001f\n\u0017\u0007Y\u0014\u0010\r\u001d\f\u000eC\u0014\u0002\u0017\u0002\u001e\u0006\u000bC\u001a\u000f\u0018\u0010\n"));
        throw new Exception(bb.a("DSi\\hF'Tn\\c\u0012p[iVhE'_f\\fUb@'QkStA"));
    }

    /* access modifiers changed from: package-private */
    public r a(View view) {
        for (r rVar : a()) {
            if (rVar.a() == view) {
                return rVar;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        oj.a(this.q);
    }

    /* access modifiers changed from: package-private */
    @TargetApi(24)
    public boolean c() throws Exception {
        if (Build.VERSION.SDK_INT <= 23) {
            return false;
        }
        for (r rVar : a()) {
            View a2 = rVar.a();
            Window.Callback b2 = rVar.b();
            if (a2 != null && a2.isShown() && b2 != null && (b2 instanceof Activity) && ((Activity) b2).isInMultiWindowMode()) {
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public synchronized void d() throws Exception {
        if (!this.k) {
            try {
                if (this.y == null) {
                    j();
                }
                this.g = false;
                this.j = 0;
                this.u = di.b().a(true);
                this.e = null;
                this.c = null;
                this.x.setEmpty();
                this.r = 0;
                this.l = false;
                this.o = di.b();
                this.m = new bb(new zb(this), UserResponces.USER_RESPONCE_SUCCSES);
                this.m.b();
                this.k = true;
            } catch (Exception e2) {
                qe.a(e2, bb.a("B@u]u\u0012`WsFn\\`\u0012s]w\u0012kWqWk\u0012p[iVhE'_f\\fUb@+\u0012fPh@s[iU'Ad@bWi\u0012cWsWdFn]i\u001c)\u001c"));
                throw e2;
            }
        }
    }

    public void a(r rVar, List<View> list) {
        u uVar = this.v;
        if (uVar != null) {
            uVar.a(rVar, list);
        }
    }

    private /* synthetic */ void b(r rVar) throws Exception {
        Rect a2 = rb.a(rVar);
        long b2 = xd.b().b();
        if (!this.x.equals(a2) || !this.s) {
            this.x.set(a2);
            this.r = b2;
            this.l = false;
        } else if (this.r + ((long) pg.h().h()) > b2 && !this.l) {
            this.l = true;
            xd.b().a(this.x, this.r);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public synchronized void m146a() {
        this.z.c();
        this.w.c();
        this.n.c();
        this.i.c();
        this.z.a();
        this.w.a();
        this.n.a();
        this.i.a();
        if (this.m != null) {
            this.m.a();
        }
        g();
        this.k = false;
    }

    private /* synthetic */ void b(Object[] objArr) {
        this.f2070a.clear();
        if (objArr != null && objArr.length > 0) {
            for (Object obj : objArr) {
                if (obj != null) {
                    Iterator<Class<?>> it2 = this.d.iterator();
                    boolean z2 = false;
                    while (it2.hasNext()) {
                        if (it2.next().isInstance(obj)) {
                            this.f2070a.add(obj);
                            z2 = true;
                        }
                    }
                    if (!z2) {
                        Iterator<Class<?>> it3 = this.t.iterator();
                        while (it3.hasNext()) {
                            if (it3.next().equals(obj.getClass())) {
                                z2 = true;
                            }
                        }
                        if (!z2) {
                            gd.a(1, md.a(",\r\u0012\r\u0016\u0014\u0017C\u000b\f\u0016\u0017Y\u0017\u0000\u0013\u001cYYF\n"), obj.getClass().getName());
                            this.t.add(obj.getClass());
                        }
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public r m144a() {
        WeakReference<r> weakReference = this.c;
        if (weakReference != null) {
            return (r) weakReference.get();
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0021, code lost:
        if (r5.equals(r0.a()) != false) goto L_0x0025;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(int r3, java.lang.Object r4, android.view.View r5) throws java.lang.Exception {
        /*
            r2 = this;
            boolean r0 = r4 instanceof android.view.Window
            if (r0 == 0) goto L_0x0024
            r0 = r4
            android.view.Window r0 = (android.view.Window) r0
            android.view.Window$Callback r1 = r0.getCallback()
            boolean r1 = r1 instanceof com.appsee.t
            if (r1 == 0) goto L_0x0024
            android.view.Window$Callback r0 = r0.getCallback()
            com.appsee.t r0 = (com.appsee.t) r0
            com.appsee.r r0 = r0.a()
            android.view.View r1 = r0.a()
            boolean r1 = r5.equals(r1)
            if (r1 == 0) goto L_0x0024
            goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            if (r0 != 0) goto L_0x002c
            com.appsee.r r0 = new com.appsee.r
            r0.<init>(r4, r5, r2)
        L_0x002c:
            java.util.ArrayList<com.appsee.r> r4 = r2.p
            r4.add(r3, r0)
            com.appsee.u r3 = r2.v
            if (r3 == 0) goto L_0x0038
            r3.a((com.appsee.r) r0)
        L_0x0038:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.ub.a(int, java.lang.Object, android.view.View):void");
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public Object[] m148a() throws Exception {
        Object a2 = tc.a(this.y, md.a("\u000e/\n\u001c\u0014\n"));
        if (a2 instanceof Object[]) {
            return (Object[]) a2;
        }
        if (a2 instanceof List) {
            return ((List) a2).toArray();
        }
        this.j++;
        if (this.j <= 4) {
            return null;
        }
        throw new Exception(bb.a("R\\fPkW'Fh\u0012`Ws\u0012u]hF'DnWpA'Su@fK'Tu]j\u0012p[iVhE'_f\\fUb@"));
    }

    public static synchronized ub a() {
        ub ubVar;
        synchronized (ub.class) {
            if (A == null) {
                A = new ub();
            }
            ubVar = A;
        }
        return ubVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public int m143a() {
        return this.z.b();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public boolean m147a() {
        return di.b() - this.o > ((long) pg.h().a());
    }

    private /* synthetic */ void a(int i2) throws Exception {
        this.p.remove(i2);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: a  reason: collision with other method in class */
    public List<r> m145a() {
        return Collections.unmodifiableList(this.p);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        r4 = r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private /* synthetic */ void a(java.lang.Object[] r7) {
        /*
            r6 = this;
            if (r7 == 0) goto L_0x003d
            int r0 = r7.length
            if (r0 <= 0) goto L_0x003d
            int r0 = r7.length
            r1 = 0
            r2 = 0
        L_0x0008:
            if (r1 >= r0) goto L_0x0038
            r3 = r7[r1]
            boolean r4 = r3 instanceof android.widget.LinearLayout
            if (r4 == 0) goto L_0x0035
            r4 = r3
            android.widget.LinearLayout r4 = (android.widget.LinearLayout) r4
            java.lang.String r5 = com.appsee.rb.i((android.view.View) r4)
            if (r5 != 0) goto L_0x001a
            goto L_0x0035
        L_0x001a:
            android.widget.LinearLayout r2 = r6.a()
            boolean r2 = r3.equals(r2)
            if (r2 != 0) goto L_0x0034
            com.appsee.u r2 = r6.v
            if (r2 == 0) goto L_0x0034
            java.lang.ref.WeakReference r2 = new java.lang.ref.WeakReference
            r2.<init>(r4)
            r6.e = r2
            com.appsee.u r2 = r6.v
            r2.a((java.lang.String) r5)
        L_0x0034:
            r2 = 1
        L_0x0035:
            int r1 = r1 + 1
            goto L_0x0008
        L_0x0038:
            if (r2 != 0) goto L_0x003d
            r7 = 0
            r6.e = r7
        L_0x003d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.ub.a(java.lang.Object[]):void");
    }
}
