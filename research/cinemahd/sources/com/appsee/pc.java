package com.appsee;

import android.content.SharedPreferences;
import android.os.Build;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class pc {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f2050a = new Object();
    private static boolean b = false;

    pc() {
    }

    static void a(bc bcVar) {
        synchronized (f2050a) {
            d().edit().putInt("Appsee_OfflineConfigPolicy", bcVar.ordinal()).commit();
        }
    }

    static void b(String str) {
        synchronized (f2050a) {
            d().edit().putString("Appsee_ValidFallbackHash", str).commit();
        }
    }

    static void c() throws JSONException {
        if (pg.h().e() == null) {
            synchronized (f2050a) {
                pg.h().c(d().getString("Appsee_ClientId", cc.a("\u0006")));
            }
        }
    }

    private static /* synthetic */ SharedPreferences d() {
        if (Build.VERSION.SDK_INT >= 21) {
            a();
            if (b) {
                return gl.a();
            }
        }
        return ho.a().getSharedPreferences("Appsee", 0);
    }

    /* renamed from: e  reason: collision with other method in class */
    static JSONArray m74e() throws JSONException {
        synchronized (f2050a) {
            SharedPreferences d = d();
            if (!d.contains("Appsee_MetadataErrors")) {
                return null;
            }
            JSONArray jSONArray = new JSONArray(d.getString("Appsee_MetadataErrors", cc.a("UK")));
            return jSONArray;
        }
    }

    static void f() {
        synchronized (f2050a) {
            d().edit().putString("Appsee_UserId", xd.b().b()).commit();
        }
    }

    static void g() {
        synchronized (f2050a) {
            SharedPreferences d = d();
            if (d.contains("Appsee_OfflineSessions")) {
                d.edit().remove("Appsee_OfflineSessions").commit();
                gd.b(1, cc.a("dK[A@KR\u000eYHPB_@S\u000eEKE]_AX]\u0016MY@PGQ[DOBGY@E"));
            }
        }
    }

    static void h() {
        synchronized (f2050a) {
            String string = d().getString("Appsee_UserId", (String) null);
            if (!yb.a(string)) {
                xd.b().a(string);
            }
        }
    }

    static void i() {
        SharedPreferences d = d();
        gd.b(1, cc.a("mZKW\\_@Q\u000e[J\u0016KD\\Y\\E\u000eP\\YC\u0016}^ODKR~DKPKDKXMS]\u0018\u0000\u0018"));
        d.edit().remove("Appsee_MetadataErrors").commit();
    }

    static void j() {
        synchronized (f2050a) {
            d().edit().remove("Appsee_OfflineConfig").commit();
        }
    }

    private static synchronized /* synthetic */ void a() {
        String str;
        String str2;
        synchronized (pc.class) {
            if (Build.VERSION.SDK_INT >= 21) {
                if (!b) {
                    gl a2 = gl.a();
                    if (!a2.getAll().isEmpty()) {
                        b = true;
                        return;
                    }
                    gd.b(2, cc.a("{GQ\\WZ_@Q\u000eYBR\u000eEFW\\SJ\u0016^DKPKDKXMS]\u0018\u0000\u0018"));
                    SharedPreferences sharedPreferences = ho.a().getSharedPreferences("Appsee", 0);
                    SharedPreferences.Editor edit = a2.edit();
                    String str3 = null;
                    try {
                        str2 = null;
                        for (Map.Entry next : sharedPreferences.getAll().entrySet()) {
                            try {
                                if (next.getValue() != null) {
                                    str = (String) next.getKey();
                                    try {
                                        str2 = next.getValue().getClass().getName();
                                        if (next.getValue() instanceof String) {
                                            edit.putString((String) next.getKey(), next.getValue().toString());
                                        } else if (next.getValue() instanceof Boolean) {
                                            edit.putBoolean((String) next.getKey(), ((Boolean) next.getValue()).booleanValue());
                                        } else if (next.getValue() instanceof Integer) {
                                            edit.putInt((String) next.getKey(), ((Integer) next.getValue()).intValue());
                                        } else {
                                            throw new UnsupportedOperationException(cc.a("gXXWB_J\u0016ZO^S"));
                                        }
                                        str3 = str;
                                    } catch (Exception e) {
                                        e = e;
                                        qe.a((Throwable) e, cc.a("s\\DAD\u000e[GQ\\WZ_@Q\u000eEFW\\SJ\u0016^DKPKDKXMS]\u0016ESW\u0016\t\u0013]\u0011\u000e@OZ[SzO^S\u0014\u0016\u000bE"), str, str2);
                                        sharedPreferences.edit().clear().commit();
                                        gd.b(2, cc.a("c_IDOBGXI\u0016AZJ\u0016]^ODKR\u000eF\\SHS\\S@UKE\u0000\u0018\u0000\u0016jY@S"));
                                    }
                                }
                            } catch (Exception e2) {
                                e = e2;
                                str = str3;
                            }
                        }
                        edit.commit();
                        b = true;
                    } catch (Exception e3) {
                        e = e3;
                        str2 = null;
                        str = null;
                        qe.a((Throwable) e, cc.a("s\\DAD\u000e[GQ\\WZ_@Q\u000eEFW\\SJ\u0016^DKPKDKXMS]\u0016ESW\u0016\t\u0013]\u0011\u000e@OZ[SzO^S\u0014\u0016\u000bE"), str, str2);
                        sharedPreferences.edit().clear().commit();
                        gd.b(2, cc.a("c_IDOBGXI\u0016AZJ\u0016]^ODKR\u000eF\\SHS\\S@UKE\u0000\u0018\u0000\u0016jY@S"));
                    }
                    try {
                        sharedPreferences.edit().clear().commit();
                    } catch (Exception e4) {
                        qe.a(e4, cc.a("uOX@YZ\u0016MZKW\\\u0016AZJ\u0016]^ODKR\u000eF\\SHS\\S@UKE\u000eWHBKD\u000e[GQ\\WZ_AX"));
                    }
                    gd.b(2, cc.a("c_IDOBGXI\u0016AZJ\u0016]^ODKR\u000eF\\SHS\\S@UKE\u0000\u0018\u0000\u0016jY@S"));
                }
            }
        }
    }

    static void b() {
        synchronized (f2050a) {
            d().edit().putString("Appsee_ClientId", pg.h().e()).commit();
        }
    }

    /* renamed from: e  reason: collision with other method in class */
    static String m73e() {
        String string;
        synchronized (f2050a) {
            string = d().getString("Appsee_ValidFallbackHash", (String) null);
        }
        return string;
    }

    /* renamed from: e  reason: collision with other method in class */
    static Object m72e() throws JSONException {
        synchronized (f2050a) {
            String string = d().getString("Appsee_OfflineSessions", (String) null);
            if (string == null) {
                return null;
            }
            JSONArray jSONArray = new JSONArray(string);
            return jSONArray;
        }
    }

    static void b(JSONObject jSONObject) throws JSONException {
        JSONArray jSONArray;
        synchronized (f2050a) {
            SharedPreferences d = d();
            String string = d.getString("Appsee_OfflineSessions", (String) null);
            if (string != null) {
                jSONArray = new JSONArray(string);
            }
            jSONArray.put(jSONObject);
            d.edit().putString("Appsee_OfflineSessions", jSONArray.toString()).commit();
            gd.a(1, cc.a("eO@KR\u000eYHPB_@S\u000eROBO\u0016\u0013\u0016\u000bE"), jSONArray.toString());
        }
    }

    /* renamed from: e  reason: collision with other method in class */
    static JSONObject m75e() {
        synchronized (f2050a) {
            String string = d().getString("Appsee_OfflineConfig", (String) null);
            if (string == null) {
                return null;
            }
            try {
                JSONObject jSONObject = new JSONObject(string);
                return jSONObject;
            } catch (JSONException e) {
                qe.a(e, cc.a("s\\DAD\u000eRKEKDGWB_T_@Q\u000eyHPB_@S\u000euAXH_IC\\WZ_AX"));
                return null;
            }
        }
    }

    /* renamed from: e  reason: collision with other method in class */
    static boolean m76e() {
        boolean z;
        e();
        synchronized (f2050a) {
            z = d().getBoolean("Appsee_IsOptOut", false);
        }
        return z;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002d, code lost:
        return r2;
     */
    /* renamed from: a  reason: collision with other method in class */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static boolean m71a(java.lang.String r5) {
        /*
            java.lang.Object r0 = f2050a
            monitor-enter(r0)
            android.content.SharedPreferences r1 = d()     // Catch:{ all -> 0x003e }
            java.lang.String r2 = "Appsee_UploadPolicies"
            r3 = 0
            java.lang.String r1 = r1.getString(r2, r3)     // Catch:{ all -> 0x003e }
            r2 = 0
            if (r1 != 0) goto L_0x0013
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            return r2
        L_0x0013:
            r3 = 1
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x002e }
            r4.<init>(r1)     // Catch:{ JSONException -> 0x002e }
            com.appsee.yc r1 = com.appsee.yc.b     // Catch:{ all -> 0x003e }
            int r1 = r1.ordinal()     // Catch:{ all -> 0x003e }
            int r5 = r4.optInt(r5, r1)     // Catch:{ all -> 0x003e }
            com.appsee.yc r1 = com.appsee.yc.f2089a     // Catch:{ all -> 0x003e }
            int r1 = r1.ordinal()     // Catch:{ all -> 0x003e }
            if (r5 != r1) goto L_0x002c
            r2 = 1
        L_0x002c:
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            return r2
        L_0x002e:
            r1 = move-exception
            java.lang.String r4 = "kD\\Y\\\u0016JS]S\\_OZGLGXI\u0016}S]EGY@\u0016{FBYOR\u000efAZGUW\u0016HY\\\u0016]S]EGY@\u0016\u000bE"
            java.lang.String r4 = com.appsee.cc.a((java.lang.String) r4)     // Catch:{ all -> 0x003e }
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x003e }
            r3[r2] = r5     // Catch:{ all -> 0x003e }
            com.appsee.qe.a((java.lang.Throwable) r1, (java.lang.String) r4, (java.lang.Object[]) r3)     // Catch:{ all -> 0x003e }
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            return r2
        L_0x003e:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x003e }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.appsee.pc.m71a(java.lang.String):boolean");
    }

    static bc e() {
        synchronized (f2050a) {
            SharedPreferences d = d();
            if (!d.contains("Appsee_OfflineConfigPolicy")) {
                return null;
            }
            bc bcVar = bc.values()[d.getInt("Appsee_OfflineConfigPolicy", -1)];
            return bcVar;
        }
    }

    static void a(JSONObject jSONObject) {
        if (jSONObject != null) {
            synchronized (f2050a) {
                d().edit().putString("Appsee_OfflineConfig", jSONObject.toString()).commit();
            }
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    static void m70a(String str) throws JSONException {
        synchronized (f2050a) {
            SharedPreferences d = d();
            String string = d.getString("Appsee_UploadPolicies", (String) null);
            if (string != null) {
                JSONObject jSONObject = new JSONObject(string);
                jSONObject.remove(str);
                d.edit().putString("Appsee_UploadPolicies", jSONObject.toString()).commit();
                gd.a(1, cc.a("|SCYXSJ\u0016]S]EGY@\u0016[FBYOR\u000eFAZGUW\u0016HY\\\u0016]S]EGY@\u0016\u000bE"), str);
            }
        }
    }

    static HashMap<String, ob> a(String str) {
        HashMap<String, ob> hashMap;
        e();
        synchronized (f2050a) {
            Map<String, ?> all = d().getAll();
            hashMap = new HashMap<>();
            for (String next : all.keySet()) {
                if (next.startsWith(str)) {
                    ob a2 = ab.a(next);
                    a2.b(all.get(next).toString());
                    hashMap.put(next, a2);
                }
            }
        }
        return hashMap;
    }

    static void a(String str, yc ycVar) throws JSONException {
        JSONObject jSONObject;
        synchronized (f2050a) {
            SharedPreferences d = d();
            String string = d.getString("Appsee_UploadPolicies", (String) null);
            if (string != null) {
                jSONObject = new JSONObject(string);
            }
            JSONObject put = jSONObject.put(str, ycVar.ordinal());
            d.edit().putString("Appsee_UploadPolicies", put.toString()).commit();
            gd.a(1, cc.a("}WXSJ\u0016]S]EGY@\u0016[FBYOR\u000eFAZGUGS]\u0016\u0013\u0016\u000bE"), put.toString());
        }
    }

    static void a(HashMap<String, Object> hashMap) throws JSONException {
        ArrayList e = e();
        if (e == null) {
            e = new ArrayList();
        }
        e.add(hashMap);
        synchronized (f2050a) {
            SharedPreferences d = d();
            gd.b(1, cc.a("c^ROBGXI\u0016CR\u000eS\\DAD]\u0018\u0000\u0018"));
            d.edit().remove("Appsee_MetadataErrors").putString("Appsee_MetadataErrors", new JSONArray(e).toString()).commit();
        }
    }
}
