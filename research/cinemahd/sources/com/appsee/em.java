package com.appsee;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class em {
    em() {
    }

    public static String a(String str) {
        int length = str.length();
        char[] cArr = new char[length];
        int i = length - 1;
        while (i >= 0) {
            int i2 = i - 1;
            cArr[i] = (char) (str.charAt(i) ^ '5');
            if (i2 < 0) {
                break;
            }
            i = i2 - 1;
            cArr[i2] = (char) (str.charAt(i2) ^ 'r');
        }
        return new String(cArr);
    }

    /* renamed from: a  reason: collision with other method in class */
    static JSONObject m25a(JSONObject jSONObject) throws JSONException {
        return a(jSONObject, (String[]) null);
    }

    static <T extends k> JSONArray b(Iterable<T> iterable) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        synchronized (iterable) {
            for (T a2 : iterable) {
                jSONArray.put(a2.a());
            }
        }
        return jSONArray;
    }

    static JSONObject a(JSONObject jSONObject, String... strArr) throws JSONException {
        if (jSONObject == null) {
            return null;
        }
        HashSet hashSet = new HashSet();
        if (strArr != null) {
            Collections.addAll(hashSet, strArr);
        }
        JSONObject jSONObject2 = new JSONObject();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (!hashSet.contains(next)) {
                jSONObject2.put(next, jSONObject.get(next));
            }
        }
        return jSONObject2;
    }

    public static <T extends JSONObject> JSONArray a(Iterable<T> iterable) throws JSONException {
        JSONArray jSONArray = new JSONArray();
        synchronized (iterable) {
            for (T put : iterable) {
                jSONArray.put(put);
            }
        }
        return jSONArray;
    }

    static JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) throws JSONException {
        if (!(jSONObject == null || jSONObject2 == null)) {
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                jSONObject.put(next, jSONObject2.get(next));
            }
        }
        return jSONObject;
    }

    static <T> List<T> a(JSONArray jSONArray) throws JSONException {
        if (jSONArray == null) {
            return new ArrayList();
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        int i = 0;
        while (i < jSONArray.length()) {
            Object obj = jSONArray.get(i);
            if (obj instanceof JSONArray) {
                obj = a((JSONArray) obj);
            } else if (obj instanceof JSONObject) {
                obj = a((JSONObject) obj);
            }
            i++;
            arrayList.add(obj);
        }
        return arrayList;
    }

    static Map<String, Object> a(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            Object obj = jSONObject.get(next);
            if (obj instanceof JSONArray) {
                obj = a((JSONArray) obj);
            } else if (obj instanceof JSONObject) {
                obj = a((JSONObject) obj);
            }
            hashMap.put(next, obj);
        }
        return hashMap;
    }
}
