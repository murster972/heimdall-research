package com.appsee;

import android.graphics.Rect;
import org.json.JSONException;
import org.json.JSONObject;

class nl implements k {

    /* renamed from: a  reason: collision with root package name */
    private String f2040a;
    private String b;
    private Rect c;
    private String d;
    private ak e;
    private boolean f;
    private long g;

    public nl(ak akVar, String str, String str2, long j, Rect rect) {
        this.e = akVar;
        this.b = str;
        this.f2040a = str2;
        this.g = j;
        this.c = rect != null ? new Rect(rect) : null;
    }

    private /* synthetic */ JSONObject b() throws JSONException {
        Rect rect = this.c;
        if (rect == null || rect.width() == 0 || this.c.height() == 0) {
            return null;
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(md.a("\u0001"), this.c.left);
        jSONObject.put(oi.a("C"), this.c.top);
        String a2 = md.a("\u000e");
        Rect rect2 = this.c;
        jSONObject.put(a2, rect2.right - rect2.left);
        String a3 = oi.a("R");
        Rect rect3 = this.c;
        jSONObject.put(a3, rect3.bottom - rect3.top);
        return jSONObject;
    }

    public long a() {
        return this.g;
    }

    public void a(long j) {
        this.g = j;
    }

    public void a(String str) {
        this.d = str;
    }

    public void a(Boolean bool) {
        this.f = bool.booleanValue();
    }

    /* renamed from: a  reason: collision with other method in class */
    public JSONObject m66a() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(md.a("\r"), this.e.ordinal());
        jSONObject.put(oi.a("I"), this.g);
        jSONObject.put(md.a("\u0010"), this.f2040a);
        jSONObject.put(oi.a("J"), this.b);
        if (this.e == ak.c) {
            if (!yb.a(this.d)) {
                jSONObject.put(md.a("\n\u001d"), this.d);
                jSONObject.put(oi.a(".O"), this.f ? 1 : 0);
            }
            jSONObject.put(md.a("\u001b"), b());
        }
        return jSONObject;
    }

    public void b(String str) {
        this.b = str;
    }

    /* renamed from: a  reason: collision with other method in class */
    public ak m65a() {
        return this.e;
    }
}
