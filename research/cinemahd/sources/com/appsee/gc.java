package com.appsee;

import android.graphics.Rect;
import android.view.View;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class gc {

    /* renamed from: a  reason: collision with root package name */
    private List<Rect> f1997a;
    private ch b;
    private long c;
    private zn d;

    public long a() {
        return this.c;
    }

    public void a(long j) {
        this.c = j;
    }

    public void a(HashMap<View, Rect> hashMap) {
        if (hashMap != null && !hashMap.isEmpty()) {
            this.f1997a = new ArrayList(hashMap.values());
        }
    }

    /* renamed from: a  reason: collision with other method in class */
    public List<Rect> m28a() {
        return this.f1997a;
    }

    /* renamed from: a  reason: collision with other method in class */
    public zn m27a() {
        return this.d;
    }

    public void a(zn znVar) {
        this.d = znVar;
    }

    /* renamed from: a  reason: collision with other method in class */
    public ch m26a() {
        return this.b;
    }

    public void a(ch chVar) {
        this.b = chVar;
    }
}
