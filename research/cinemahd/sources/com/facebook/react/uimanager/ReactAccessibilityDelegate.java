package com.facebook.react.uimanager;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.style.URLSpan;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;
import com.applovin.sdk.AppLovinMediationProvider;
import com.facebook.react.R;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Dynamic;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactNoCrashSoftException;
import com.facebook.react.bridge.ReactSoftException;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableMapKeySetIterator;
import com.facebook.react.bridge.ReadableType;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.uimanager.events.RCTEventEmitter;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.HashMap;

public class ReactAccessibilityDelegate extends AccessibilityDelegateCompat {
    private static final int SEND_EVENT = 1;
    private static final String STATE_CHECKED = "checked";
    private static final String STATE_DISABLED = "disabled";
    private static final String STATE_SELECTED = "selected";
    private static final String TAG = "ReactAccessibilityDelegate";
    private static final int TIMEOUT_SEND_ACCESSIBILITY_EVENT = 200;
    public static final HashMap<String, Integer> sActionIdMap = new HashMap<>();
    private static int sCounter = 1056964608;
    private final HashMap<Integer, String> mAccessibilityActionsMap = new HashMap<>();
    private Handler mHandler = new Handler() {
        public void handleMessage(Message message) {
            ((View) message.obj).sendAccessibilityEvent(4);
        }
    };

    /* renamed from: com.facebook.react.uimanager.ReactAccessibilityDelegate$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {
        static final /* synthetic */ int[] $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole = new int[AccessibilityRole.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(54:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|(3:53|54|56)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(56:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|56) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:17:0x0062 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x006e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x007a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0086 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0092 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x009e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x00aa */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00b6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x00c2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x00ce */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00da */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00e6 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00f2 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00fe */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x010a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x0116 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x0122 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x012e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x013a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole[] r0 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole = r0
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.BUTTON     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.SEARCH     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.IMAGE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.IMAGEBUTTON     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.KEYBOARDKEY     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x004b }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.TEXT     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.ADJUSTABLE     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.CHECKBOX     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x006e }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.RADIO     // Catch:{ NoSuchFieldError -> 0x006e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x006e }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x006e }
            L_0x006e:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x007a }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.SPINBUTTON     // Catch:{ NoSuchFieldError -> 0x007a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007a }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007a }
            L_0x007a:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0086 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.SWITCH     // Catch:{ NoSuchFieldError -> 0x0086 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0086 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0086 }
            L_0x0086:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0092 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.NONE     // Catch:{ NoSuchFieldError -> 0x0092 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0092 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0092 }
            L_0x0092:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x009e }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.LINK     // Catch:{ NoSuchFieldError -> 0x009e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009e }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009e }
            L_0x009e:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00aa }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.SUMMARY     // Catch:{ NoSuchFieldError -> 0x00aa }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00aa }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00aa }
            L_0x00aa:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00b6 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.HEADER     // Catch:{ NoSuchFieldError -> 0x00b6 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b6 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b6 }
            L_0x00b6:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00c2 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.ALERT     // Catch:{ NoSuchFieldError -> 0x00c2 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c2 }
                r2 = 16
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00c2 }
            L_0x00c2:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00ce }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.COMBOBOX     // Catch:{ NoSuchFieldError -> 0x00ce }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ce }
                r2 = 17
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ce }
            L_0x00ce:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00da }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.MENU     // Catch:{ NoSuchFieldError -> 0x00da }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00da }
                r2 = 18
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00da }
            L_0x00da:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00e6 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.MENUBAR     // Catch:{ NoSuchFieldError -> 0x00e6 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e6 }
                r2 = 19
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00e6 }
            L_0x00e6:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00f2 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.MENUITEM     // Catch:{ NoSuchFieldError -> 0x00f2 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00f2 }
                r2 = 20
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00f2 }
            L_0x00f2:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x00fe }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.PROGRESSBAR     // Catch:{ NoSuchFieldError -> 0x00fe }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00fe }
                r2 = 21
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00fe }
            L_0x00fe:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x010a }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.RADIOGROUP     // Catch:{ NoSuchFieldError -> 0x010a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x010a }
                r2 = 22
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x010a }
            L_0x010a:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0116 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.SCROLLBAR     // Catch:{ NoSuchFieldError -> 0x0116 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0116 }
                r2 = 23
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0116 }
            L_0x0116:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0122 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.TAB     // Catch:{ NoSuchFieldError -> 0x0122 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0122 }
                r2 = 24
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0122 }
            L_0x0122:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x012e }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.TABLIST     // Catch:{ NoSuchFieldError -> 0x012e }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x012e }
                r2 = 25
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x012e }
            L_0x012e:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x013a }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.TIMER     // Catch:{ NoSuchFieldError -> 0x013a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x013a }
                r2 = 26
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x013a }
            L_0x013a:
                int[] r0 = $SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole     // Catch:{ NoSuchFieldError -> 0x0146 }
                com.facebook.react.uimanager.ReactAccessibilityDelegate$AccessibilityRole r1 = com.facebook.react.uimanager.ReactAccessibilityDelegate.AccessibilityRole.TOOLBAR     // Catch:{ NoSuchFieldError -> 0x0146 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0146 }
                r2 = 27
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0146 }
            L_0x0146:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.uimanager.ReactAccessibilityDelegate.AnonymousClass2.<clinit>():void");
        }
    }

    public enum AccessibilityRole {
        NONE,
        BUTTON,
        LINK,
        SEARCH,
        IMAGE,
        IMAGEBUTTON,
        KEYBOARDKEY,
        TEXT,
        ADJUSTABLE,
        SUMMARY,
        HEADER,
        ALERT,
        CHECKBOX,
        COMBOBOX,
        MENU,
        MENUBAR,
        MENUITEM,
        PROGRESSBAR,
        RADIO,
        RADIOGROUP,
        SCROLLBAR,
        SPINBUTTON,
        SWITCH,
        TAB,
        TABLIST,
        TIMER,
        TOOLBAR;

        public static AccessibilityRole fromValue(String str) {
            for (AccessibilityRole accessibilityRole : values()) {
                if (accessibilityRole.name().equalsIgnoreCase(str)) {
                    return accessibilityRole;
                }
            }
            throw new IllegalArgumentException("Invalid accessibility role value: " + str);
        }

        public static String getValue(AccessibilityRole accessibilityRole) {
            switch (AnonymousClass2.$SwitchMap$com$facebook$react$uimanager$ReactAccessibilityDelegate$AccessibilityRole[accessibilityRole.ordinal()]) {
                case 1:
                    return "android.widget.Button";
                case 2:
                    return "android.widget.EditText";
                case 3:
                    return "android.widget.ImageView";
                case 4:
                    return "android.widget.ImageButon";
                case 5:
                    return "android.inputmethodservice.Keyboard$Key";
                case 6:
                    return "android.widget.TextView";
                case 7:
                    return "android.widget.SeekBar";
                case 8:
                    return "android.widget.CheckBox";
                case 9:
                    return "android.widget.RadioButton";
                case 10:
                    return "android.widget.SpinButton";
                case 11:
                    return "android.widget.Switch";
                case 12:
                case 13:
                case 14:
                case 15:
                case 16:
                case 17:
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                    return "android.view.View";
                default:
                    throw new IllegalArgumentException("Invalid accessibility role value: " + accessibilityRole);
            }
        }
    }

    static {
        sActionIdMap.put("activate", Integer.valueOf(AccessibilityNodeInfoCompat.AccessibilityActionCompat.f.a()));
        sActionIdMap.put("longpress", Integer.valueOf(AccessibilityNodeInfoCompat.AccessibilityActionCompat.g.a()));
        sActionIdMap.put("increment", Integer.valueOf(AccessibilityNodeInfoCompat.AccessibilityActionCompat.h.a()));
        sActionIdMap.put("decrement", Integer.valueOf(AccessibilityNodeInfoCompat.AccessibilityActionCompat.i.a()));
    }

    private void scheduleAccessibilityEventSender(View view) {
        if (this.mHandler.hasMessages(1, view)) {
            this.mHandler.removeMessages(1, view);
        }
        this.mHandler.sendMessageDelayed(this.mHandler.obtainMessage(1, view), 200);
    }

    public static void setDelegate(View view) {
        if (ViewCompat.y(view)) {
            return;
        }
        if (view.getTag(R.id.accessibility_role) != null || view.getTag(R.id.accessibility_state) != null || view.getTag(R.id.accessibility_actions) != null) {
            ViewCompat.a(view, (AccessibilityDelegateCompat) new ReactAccessibilityDelegate());
        }
    }

    public static void setRole(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, AccessibilityRole accessibilityRole, Context context) {
        if (accessibilityRole == null) {
            accessibilityRole = AccessibilityRole.NONE;
        }
        accessibilityNodeInfoCompat.a((CharSequence) AccessibilityRole.getValue(accessibilityRole));
        if (accessibilityRole.equals(AccessibilityRole.LINK)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.link_description));
            if (accessibilityNodeInfoCompat.e() != null) {
                SpannableString spannableString = new SpannableString(accessibilityNodeInfoCompat.e());
                spannableString.setSpan(new URLSpan(""), 0, spannableString.length(), 0);
                accessibilityNodeInfoCompat.b((CharSequence) spannableString);
            }
            if (accessibilityNodeInfoCompat.i() != null) {
                SpannableString spannableString2 = new SpannableString(accessibilityNodeInfoCompat.i());
                spannableString2.setSpan(new URLSpan(""), 0, spannableString2.length(), 0);
                accessibilityNodeInfoCompat.h((CharSequence) spannableString2);
            }
        } else if (accessibilityRole.equals(AccessibilityRole.SEARCH)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.search_description));
        } else if (accessibilityRole.equals(AccessibilityRole.IMAGE)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.image_description));
        } else if (accessibilityRole.equals(AccessibilityRole.IMAGEBUTTON)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.imagebutton_description));
            accessibilityNodeInfoCompat.e(true);
        } else if (accessibilityRole.equals(AccessibilityRole.BUTTON)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.button_description));
            accessibilityNodeInfoCompat.e(true);
        } else if (accessibilityRole.equals(AccessibilityRole.SUMMARY)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.summary_description));
        } else if (accessibilityRole.equals(AccessibilityRole.HEADER)) {
            accessibilityNodeInfoCompat.b((Object) AccessibilityNodeInfoCompat.CollectionItemInfoCompat.a(0, 1, 0, 1, true));
        } else if (accessibilityRole.equals(AccessibilityRole.ALERT)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.alert_description));
        } else if (accessibilityRole.equals(AccessibilityRole.COMBOBOX)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.combobox_description));
        } else if (accessibilityRole.equals(AccessibilityRole.MENU)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.menu_description));
        } else if (accessibilityRole.equals(AccessibilityRole.MENUBAR)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.menubar_description));
        } else if (accessibilityRole.equals(AccessibilityRole.MENUITEM)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.menuitem_description));
        } else if (accessibilityRole.equals(AccessibilityRole.PROGRESSBAR)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.progressbar_description));
        } else if (accessibilityRole.equals(AccessibilityRole.RADIOGROUP)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.radiogroup_description));
        } else if (accessibilityRole.equals(AccessibilityRole.SCROLLBAR)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.scrollbar_description));
        } else if (accessibilityRole.equals(AccessibilityRole.SPINBUTTON)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.spinbutton_description));
        } else if (accessibilityRole.equals(AccessibilityRole.TAB)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.rn_tab_description));
        } else if (accessibilityRole.equals(AccessibilityRole.TABLIST)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.tablist_description));
        } else if (accessibilityRole.equals(AccessibilityRole.TIMER)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.timer_description));
        } else if (accessibilityRole.equals(AccessibilityRole.TOOLBAR)) {
            accessibilityNodeInfoCompat.g((CharSequence) context.getString(R.string.toolbar_description));
        }
    }

    private static void setState(AccessibilityNodeInfoCompat accessibilityNodeInfoCompat, ReadableMap readableMap, Context context) {
        ReadableMapKeySetIterator keySetIterator = readableMap.keySetIterator();
        while (keySetIterator.hasNextKey()) {
            String nextKey = keySetIterator.nextKey();
            Dynamic dynamic = readableMap.getDynamic(nextKey);
            if (nextKey.equals(STATE_SELECTED) && dynamic.getType() == ReadableType.Boolean) {
                accessibilityNodeInfoCompat.o(dynamic.asBoolean());
            } else if (nextKey.equals(STATE_DISABLED) && dynamic.getType() == ReadableType.Boolean) {
                accessibilityNodeInfoCompat.h(!dynamic.asBoolean());
            } else if (nextKey.equals(STATE_CHECKED) && dynamic.getType() == ReadableType.Boolean) {
                boolean asBoolean = dynamic.asBoolean();
                accessibilityNodeInfoCompat.c(true);
                accessibilityNodeInfoCompat.d(asBoolean);
                if (accessibilityNodeInfoCompat.c().equals(AccessibilityRole.getValue(AccessibilityRole.SWITCH))) {
                    accessibilityNodeInfoCompat.h((CharSequence) context.getString(asBoolean ? R.string.state_on_description : R.string.state_off_description));
                }
            }
        }
    }

    public void onInitializeAccessibilityEvent(View view, AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(view, accessibilityEvent);
        ReadableMap readableMap = (ReadableMap) view.getTag(R.id.accessibility_value);
        if (readableMap != null && readableMap.hasKey("min") && readableMap.hasKey("now") && readableMap.hasKey(AppLovinMediationProvider.MAX)) {
            Dynamic dynamic = readableMap.getDynamic("min");
            Dynamic dynamic2 = readableMap.getDynamic("now");
            Dynamic dynamic3 = readableMap.getDynamic(AppLovinMediationProvider.MAX);
            if (dynamic != null && dynamic.getType() == ReadableType.Number && dynamic2 != null && dynamic2.getType() == ReadableType.Number && dynamic3 != null && dynamic3.getType() == ReadableType.Number) {
                int asInt = dynamic.asInt();
                int asInt2 = dynamic2.asInt();
                int asInt3 = dynamic3.asInt();
                if (asInt3 > asInt && asInt2 >= asInt && asInt3 >= asInt2) {
                    accessibilityEvent.setItemCount(asInt3 - asInt);
                    accessibilityEvent.setCurrentItemIndex(asInt2);
                }
            }
        }
    }

    public void onInitializeAccessibilityNodeInfo(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat);
        AccessibilityRole accessibilityRole = (AccessibilityRole) view.getTag(R.id.accessibility_role);
        if (accessibilityRole != null) {
            setRole(accessibilityNodeInfoCompat, accessibilityRole, view.getContext());
        }
        ReadableMap readableMap = (ReadableMap) view.getTag(R.id.accessibility_state);
        if (readableMap != null) {
            setState(accessibilityNodeInfoCompat, readableMap, view.getContext());
        }
        ReadableArray readableArray = (ReadableArray) view.getTag(R.id.accessibility_actions);
        if (readableArray != null) {
            int i = 0;
            while (i < readableArray.size()) {
                ReadableMap map = readableArray.getMap(i);
                if (map.hasKey(MediationMetaData.KEY_NAME)) {
                    int i2 = sCounter;
                    String string = map.hasKey("label") ? map.getString("label") : null;
                    if (sActionIdMap.containsKey(map.getString(MediationMetaData.KEY_NAME))) {
                        i2 = sActionIdMap.get(map.getString(MediationMetaData.KEY_NAME)).intValue();
                    } else {
                        sCounter++;
                    }
                    this.mAccessibilityActionsMap.put(Integer.valueOf(i2), map.getString(MediationMetaData.KEY_NAME));
                    accessibilityNodeInfoCompat.a(new AccessibilityNodeInfoCompat.AccessibilityActionCompat(i2, string));
                    i++;
                } else {
                    throw new IllegalArgumentException("Unknown accessibility action.");
                }
            }
        }
        ReadableMap readableMap2 = (ReadableMap) view.getTag(R.id.accessibility_value);
        if (readableMap2 != null && readableMap2.hasKey("min") && readableMap2.hasKey("now") && readableMap2.hasKey(AppLovinMediationProvider.MAX)) {
            Dynamic dynamic = readableMap2.getDynamic("min");
            Dynamic dynamic2 = readableMap2.getDynamic("now");
            Dynamic dynamic3 = readableMap2.getDynamic(AppLovinMediationProvider.MAX);
            if (dynamic != null && dynamic.getType() == ReadableType.Number && dynamic2 != null && dynamic2.getType() == ReadableType.Number && dynamic3 != null && dynamic3.getType() == ReadableType.Number) {
                int asInt = dynamic.asInt();
                int asInt2 = dynamic2.asInt();
                int asInt3 = dynamic3.asInt();
                if (asInt3 > asInt && asInt2 >= asInt && asInt3 >= asInt2) {
                    accessibilityNodeInfoCompat.a(AccessibilityNodeInfoCompat.RangeInfoCompat.a(0, (float) asInt, (float) asInt3, (float) asInt2));
                }
            }
        }
    }

    public boolean performAccessibilityAction(View view, int i, Bundle bundle) {
        if (!this.mAccessibilityActionsMap.containsKey(Integer.valueOf(i))) {
            return super.performAccessibilityAction(view, i, bundle);
        }
        WritableMap createMap = Arguments.createMap();
        createMap.putString("actionName", this.mAccessibilityActionsMap.get(Integer.valueOf(i)));
        ReactContext reactContext = (ReactContext) view.getContext();
        if (reactContext.hasActiveCatalystInstance()) {
            ((RCTEventEmitter) reactContext.getJSModule(RCTEventEmitter.class)).receiveEvent(view.getId(), "topAccessibilityAction", createMap);
        } else {
            ReactSoftException.logSoftException(TAG, new ReactNoCrashSoftException("Cannot get RCTEventEmitter, no CatalystInstance"));
        }
        AccessibilityRole accessibilityRole = (AccessibilityRole) view.getTag(R.id.accessibility_role);
        ReadableMap readableMap = (ReadableMap) view.getTag(R.id.accessibility_value);
        if (accessibilityRole != AccessibilityRole.ADJUSTABLE) {
            return true;
        }
        if (i != AccessibilityNodeInfoCompat.AccessibilityActionCompat.h.a() && i != AccessibilityNodeInfoCompat.AccessibilityActionCompat.i.a()) {
            return true;
        }
        if (readableMap != null && !readableMap.hasKey("text")) {
            scheduleAccessibilityEventSender(view);
        }
        return super.performAccessibilityAction(view, i, bundle);
    }
}
