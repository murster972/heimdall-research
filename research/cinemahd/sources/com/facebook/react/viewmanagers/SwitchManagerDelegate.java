package com.facebook.react.viewmanagers;

import android.view.View;
import com.facebook.react.uimanager.BaseViewManagerDelegate;
import com.facebook.react.uimanager.BaseViewManagerInterface;
import com.facebook.react.viewmanagers.SwitchManagerInterface;

public class SwitchManagerDelegate<T extends View, U extends BaseViewManagerInterface<T> & SwitchManagerInterface<T>> extends BaseViewManagerDelegate<T, U> {
    public SwitchManagerDelegate(U u) {
        super(u);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setProperty(T r4, java.lang.String r5, java.lang.Object r6) {
        /*
            r3 = this;
            int r0 = r5.hashCode()
            r1 = 0
            switch(r0) {
                case -1742453971: goto L_0x004f;
                case 111972721: goto L_0x0045;
                case 270940796: goto L_0x003b;
                case 1084662482: goto L_0x0031;
                case 1296813577: goto L_0x0027;
                case 1327599912: goto L_0x001d;
                case 1912319986: goto L_0x0013;
                case 2113632767: goto L_0x0009;
                default: goto L_0x0008;
            }
        L_0x0008:
            goto L_0x0059
        L_0x0009:
            java.lang.String r0 = "trackColorForTrue"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 7
            goto L_0x005a
        L_0x0013:
            java.lang.String r0 = "thumbTintColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 4
            goto L_0x005a
        L_0x001d:
            java.lang.String r0 = "tintColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 2
            goto L_0x005a
        L_0x0027:
            java.lang.String r0 = "onTintColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 3
            goto L_0x005a
        L_0x0031:
            java.lang.String r0 = "trackColorForFalse"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 6
            goto L_0x005a
        L_0x003b:
            java.lang.String r0 = "disabled"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 0
            goto L_0x005a
        L_0x0045:
            java.lang.String r0 = "value"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 1
            goto L_0x005a
        L_0x004f:
            java.lang.String r0 = "thumbColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0059
            r0 = 5
            goto L_0x005a
        L_0x0059:
            r0 = -1
        L_0x005a:
            r2 = 0
            switch(r0) {
                case 0: goto L_0x00f4;
                case 1: goto L_0x00e3;
                case 2: goto L_0x00ce;
                case 3: goto L_0x00b9;
                case 4: goto L_0x00a4;
                case 5: goto L_0x008f;
                case 6: goto L_0x0079;
                case 7: goto L_0x0063;
                default: goto L_0x005e;
            }
        L_0x005e:
            super.setProperty(r4, r5, r6)
            goto L_0x0104
        L_0x0063:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x006a
            goto L_0x0074
        L_0x006a:
            java.lang.Double r6 = (java.lang.Double) r6
            int r6 = r6.intValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
        L_0x0074:
            r5.setTrackColorForTrue(r4, r2)
            goto L_0x0104
        L_0x0079:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x0080
            goto L_0x008a
        L_0x0080:
            java.lang.Double r6 = (java.lang.Double) r6
            int r6 = r6.intValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
        L_0x008a:
            r5.setTrackColorForFalse(r4, r2)
            goto L_0x0104
        L_0x008f:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x0096
            goto L_0x00a0
        L_0x0096:
            java.lang.Double r6 = (java.lang.Double) r6
            int r6 = r6.intValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
        L_0x00a0:
            r5.setThumbColor(r4, r2)
            goto L_0x0104
        L_0x00a4:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00ab
            goto L_0x00b5
        L_0x00ab:
            java.lang.Double r6 = (java.lang.Double) r6
            int r6 = r6.intValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
        L_0x00b5:
            r5.setThumbTintColor(r4, r2)
            goto L_0x0104
        L_0x00b9:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00c0
            goto L_0x00ca
        L_0x00c0:
            java.lang.Double r6 = (java.lang.Double) r6
            int r6 = r6.intValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
        L_0x00ca:
            r5.setOnTintColor(r4, r2)
            goto L_0x0104
        L_0x00ce:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00d5
            goto L_0x00df
        L_0x00d5:
            java.lang.Double r6 = (java.lang.Double) r6
            int r6 = r6.intValue()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r6)
        L_0x00df:
            r5.setTintColor(r4, r2)
            goto L_0x0104
        L_0x00e3:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00ea
            goto L_0x00f0
        L_0x00ea:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r1 = r6.booleanValue()
        L_0x00f0:
            r5.setValue(r4, r1)
            goto L_0x0104
        L_0x00f4:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SwitchManagerInterface r5 = (com.facebook.react.viewmanagers.SwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00fb
            goto L_0x0101
        L_0x00fb:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r1 = r6.booleanValue()
        L_0x0101:
            r5.setDisabled(r4, r1)
        L_0x0104:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.viewmanagers.SwitchManagerDelegate.setProperty(android.view.View, java.lang.String, java.lang.Object):void");
    }
}
