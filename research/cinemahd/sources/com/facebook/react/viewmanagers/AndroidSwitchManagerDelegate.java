package com.facebook.react.viewmanagers;

import android.view.View;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.uimanager.BaseViewManagerDelegate;
import com.facebook.react.uimanager.BaseViewManagerInterface;
import com.facebook.react.viewmanagers.AndroidSwitchManagerInterface;

public class AndroidSwitchManagerDelegate<T extends View, U extends BaseViewManagerInterface<T> & AndroidSwitchManagerInterface<T>> extends BaseViewManagerDelegate<T, U> {
    public AndroidSwitchManagerDelegate(U u) {
        super(u);
    }

    public void receiveCommand(AndroidSwitchManagerInterface<T> androidSwitchManagerInterface, T t, String str, ReadableArray readableArray) {
        if (((str.hashCode() == -669744680 && str.equals("setNativeValue")) ? (char) 0 : 65535) == 0) {
            androidSwitchManagerInterface.setNativeValue(t, readableArray.getBoolean(0));
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setProperty(T r4, java.lang.String r5, java.lang.Object r6) {
        /*
            r3 = this;
            int r0 = r5.hashCode()
            r1 = 1
            r2 = 0
            switch(r0) {
                case -1742453971: goto L_0x005b;
                case -1609594047: goto L_0x0051;
                case -287374307: goto L_0x0046;
                case 3551: goto L_0x003c;
                case 111972721: goto L_0x0032;
                case 270940796: goto L_0x0028;
                case 1084662482: goto L_0x001e;
                case 1912319986: goto L_0x0014;
                case 2113632767: goto L_0x000a;
                default: goto L_0x0009;
            }
        L_0x0009:
            goto L_0x0065
        L_0x000a:
            java.lang.String r0 = "trackColorForTrue"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 4
            goto L_0x0066
        L_0x0014:
            java.lang.String r0 = "thumbTintColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 7
            goto L_0x0066
        L_0x001e:
            java.lang.String r0 = "trackColorForFalse"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 3
            goto L_0x0066
        L_0x0028:
            java.lang.String r0 = "disabled"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 0
            goto L_0x0066
        L_0x0032:
            java.lang.String r0 = "value"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 5
            goto L_0x0066
        L_0x003c:
            java.lang.String r0 = "on"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 6
            goto L_0x0066
        L_0x0046:
            java.lang.String r0 = "trackTintColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 8
            goto L_0x0066
        L_0x0051:
            java.lang.String r0 = "enabled"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 1
            goto L_0x0066
        L_0x005b:
            java.lang.String r0 = "thumbColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0065
            r0 = 2
            goto L_0x0066
        L_0x0065:
            r0 = -1
        L_0x0066:
            switch(r0) {
                case 0: goto L_0x00f3;
                case 1: goto L_0x00e2;
                case 2: goto L_0x00d2;
                case 3: goto L_0x00c2;
                case 4: goto L_0x00b2;
                case 5: goto L_0x00a1;
                case 6: goto L_0x0090;
                case 7: goto L_0x007f;
                case 8: goto L_0x006e;
                default: goto L_0x0069;
            }
        L_0x0069:
            super.setProperty(r4, r5, r6)
            goto L_0x0103
        L_0x006e:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setTrackTintColor(r4, r6)
            goto L_0x0103
        L_0x007f:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setThumbTintColor(r4, r6)
            goto L_0x0103
        L_0x0090:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            if (r6 != 0) goto L_0x0097
            goto L_0x009d
        L_0x0097:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r2 = r6.booleanValue()
        L_0x009d:
            r5.setOn(r4, r2)
            goto L_0x0103
        L_0x00a1:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00a8
            goto L_0x00ae
        L_0x00a8:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r2 = r6.booleanValue()
        L_0x00ae:
            r5.setValue(r4, r2)
            goto L_0x0103
        L_0x00b2:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setTrackColorForTrue(r4, r6)
            goto L_0x0103
        L_0x00c2:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setTrackColorForFalse(r4, r6)
            goto L_0x0103
        L_0x00d2:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setThumbColor(r4, r6)
            goto L_0x0103
        L_0x00e2:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00e9
            goto L_0x00ef
        L_0x00e9:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r1 = r6.booleanValue()
        L_0x00ef:
            r5.setEnabled(r4, r1)
            goto L_0x0103
        L_0x00f3:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.AndroidSwitchManagerInterface r5 = (com.facebook.react.viewmanagers.AndroidSwitchManagerInterface) r5
            if (r6 != 0) goto L_0x00fa
            goto L_0x0100
        L_0x00fa:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r2 = r6.booleanValue()
        L_0x0100:
            r5.setDisabled(r4, r2)
        L_0x0103:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.viewmanagers.AndroidSwitchManagerDelegate.setProperty(android.view.View, java.lang.String, java.lang.Object):void");
    }
}
