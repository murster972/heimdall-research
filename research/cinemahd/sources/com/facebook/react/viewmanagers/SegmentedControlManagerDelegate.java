package com.facebook.react.viewmanagers;

import android.view.View;
import com.facebook.react.uimanager.BaseViewManagerDelegate;
import com.facebook.react.uimanager.BaseViewManagerInterface;
import com.facebook.react.viewmanagers.SegmentedControlManagerInterface;

public class SegmentedControlManagerDelegate<T extends View, U extends BaseViewManagerInterface<T> & SegmentedControlManagerInterface<T>> extends BaseViewManagerDelegate<T, U> {
    public SegmentedControlManagerDelegate(U u) {
        super(u);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v0, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v1, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v5, resolved type: boolean} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v6, resolved type: boolean} */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setProperty(T r4, java.lang.String r5, java.lang.Object r6) {
        /*
            r3 = this;
            int r0 = r5.hashCode()
            r1 = 1
            r2 = 0
            switch(r0) {
                case -1609594047: goto L_0x0046;
                case -1063571914: goto L_0x003c;
                case -823812830: goto L_0x0032;
                case 1287124693: goto L_0x0028;
                case 1327599912: goto L_0x001e;
                case 1436069623: goto L_0x0014;
                case 1684715624: goto L_0x000a;
                default: goto L_0x0009;
            }
        L_0x0009:
            goto L_0x0050
        L_0x000a:
            java.lang.String r0 = "momentary"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 6
            goto L_0x0051
        L_0x0014:
            java.lang.String r0 = "selectedIndex"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 1
            goto L_0x0051
        L_0x001e:
            java.lang.String r0 = "tintColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 3
            goto L_0x0051
        L_0x0028:
            java.lang.String r0 = "backgroundColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 5
            goto L_0x0051
        L_0x0032:
            java.lang.String r0 = "values"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 0
            goto L_0x0051
        L_0x003c:
            java.lang.String r0 = "textColor"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 4
            goto L_0x0051
        L_0x0046:
            java.lang.String r0 = "enabled"
            boolean r0 = r5.equals(r0)
            if (r0 == 0) goto L_0x0050
            r0 = 2
            goto L_0x0051
        L_0x0050:
            r0 = -1
        L_0x0051:
            switch(r0) {
                case 0: goto L_0x00bc;
                case 1: goto L_0x00ab;
                case 2: goto L_0x009a;
                case 3: goto L_0x008a;
                case 4: goto L_0x007a;
                case 5: goto L_0x006a;
                case 6: goto L_0x0059;
                default: goto L_0x0054;
            }
        L_0x0054:
            super.setProperty(r4, r5, r6)
            goto L_0x00c5
        L_0x0059:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            if (r6 != 0) goto L_0x0060
            goto L_0x0066
        L_0x0060:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r2 = r6.booleanValue()
        L_0x0066:
            r5.setMomentary(r4, r2)
            goto L_0x00c5
        L_0x006a:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setBackgroundColor(r4, r6)
            goto L_0x00c5
        L_0x007a:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setTextColor(r4, r6)
            goto L_0x00c5
        L_0x008a:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            android.content.Context r0 = r4.getContext()
            java.lang.Integer r6 = com.facebook.react.bridge.ColorPropConverter.getColor(r6, r0)
            r5.setTintColor(r4, r6)
            goto L_0x00c5
        L_0x009a:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            if (r6 != 0) goto L_0x00a1
            goto L_0x00a7
        L_0x00a1:
            java.lang.Boolean r6 = (java.lang.Boolean) r6
            boolean r1 = r6.booleanValue()
        L_0x00a7:
            r5.setEnabled(r4, r1)
            goto L_0x00c5
        L_0x00ab:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            if (r6 != 0) goto L_0x00b2
            goto L_0x00b8
        L_0x00b2:
            java.lang.Double r6 = (java.lang.Double) r6
            int r2 = r6.intValue()
        L_0x00b8:
            r5.setSelectedIndex(r4, r2)
            goto L_0x00c5
        L_0x00bc:
            U r5 = r3.mViewManager
            com.facebook.react.viewmanagers.SegmentedControlManagerInterface r5 = (com.facebook.react.viewmanagers.SegmentedControlManagerInterface) r5
            com.facebook.react.bridge.ReadableArray r6 = (com.facebook.react.bridge.ReadableArray) r6
            r5.setValues(r4, r6)
        L_0x00c5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.react.viewmanagers.SegmentedControlManagerDelegate.setProperty(android.view.View, java.lang.String, java.lang.Object):void");
    }
}
