package com.facebook.react.bridge;

import android.view.View;
import com.facebook.infer.annotation.ThreadConfined;

public interface UIManager extends JSIModule, PerformanceCounter {
    @ThreadConfined("UI")
    <T extends View> int addRootView(T t, WritableMap writableMap, String str);

    void dispatchCommand(int i, int i2, ReadableArray readableArray);

    void dispatchCommand(int i, String str, ReadableArray readableArray);

    <T> T getEventDispatcher();

    void sendAccessibilityEvent(int i, int i2);

    @ThreadConfined("UI")
    void synchronouslyUpdateViewOnUIThread(int i, ReadableMap readableMap);

    @ThreadConfined("UI")
    void updateRootLayoutSpecs(int i, int i2, int i3);
}
