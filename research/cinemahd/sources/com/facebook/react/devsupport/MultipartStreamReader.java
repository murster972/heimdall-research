package com.facebook.react.devsupport;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okio.Buffer;
import okio.BufferedSource;
import okio.ByteString;
import okio.Sink;

public class MultipartStreamReader {
    private static final String CRLF = "\r\n";
    private final String mBoundary;
    private long mLastProgressEvent;
    private final BufferedSource mSource;

    public interface ChunkListener {
        void onChunkComplete(Map<String, String> map, Buffer buffer, boolean z) throws IOException;

        void onChunkProgress(Map<String, String> map, long j, long j2) throws IOException;
    }

    public MultipartStreamReader(BufferedSource bufferedSource, String str) {
        this.mSource = bufferedSource;
        this.mBoundary = str;
    }

    private void emitChunk(Buffer buffer, boolean z, ChunkListener chunkListener) throws IOException {
        ByteString d = ByteString.d("\r\n\r\n");
        long b = buffer.b(d);
        if (b == -1) {
            chunkListener.onChunkComplete((Map<String, String>) null, buffer, z);
            return;
        }
        Buffer buffer2 = new Buffer();
        Buffer buffer3 = new Buffer();
        buffer.read(buffer2, b);
        buffer.skip((long) d.size());
        buffer.a((Sink) buffer3);
        chunkListener.onChunkComplete(parseHeaders(buffer2), buffer3, z);
    }

    private void emitProgress(Map<String, String> map, long j, boolean z, ChunkListener chunkListener) throws IOException {
        if (map != null && chunkListener != null) {
            long currentTimeMillis = System.currentTimeMillis();
            if (currentTimeMillis - this.mLastProgressEvent > 16 || z) {
                this.mLastProgressEvent = currentTimeMillis;
                chunkListener.onChunkProgress(map, j, map.get("Content-Length") != null ? Long.parseLong(map.get("Content-Length")) : 0);
            }
        }
    }

    private Map<String, String> parseHeaders(Buffer buffer) {
        HashMap hashMap = new HashMap();
        for (String str : buffer.h().split(CRLF)) {
            int indexOf = str.indexOf(":");
            if (indexOf != -1) {
                hashMap.put(str.substring(0, indexOf).trim(), str.substring(indexOf + 1).trim());
            }
        }
        return hashMap;
    }

    public boolean readAllParts(ChunkListener chunkListener) throws IOException {
        boolean z;
        ByteString d = ByteString.d("\r\n--" + this.mBoundary + CRLF);
        ByteString d2 = ByteString.d("\r\n--" + this.mBoundary + "--" + CRLF);
        ByteString d3 = ByteString.d("\r\n\r\n");
        Buffer buffer = new Buffer();
        long j = 0;
        long j2 = 0;
        long j3 = 0;
        Map<String, String> map = null;
        while (true) {
            long max = Math.max(j - ((long) d2.size()), j2);
            long a2 = buffer.a(d, max);
            if (a2 == -1) {
                a2 = buffer.a(d2, max);
                z = true;
            } else {
                z = false;
            }
            if (a2 == -1) {
                long u = buffer.u();
                if (map == null) {
                    long a3 = buffer.a(d3, max);
                    if (a3 >= 0) {
                        this.mSource.read(buffer, a3);
                        Buffer buffer2 = new Buffer();
                        buffer.a(buffer2, max, a3 - max);
                        j3 = buffer2.u() + ((long) d3.size());
                        map = parseHeaders(buffer2);
                    }
                } else {
                    emitProgress(map, buffer.u() - j3, false, chunkListener);
                }
                if (this.mSource.read(buffer, (long) 4096) <= 0) {
                    return false;
                }
                j = u;
            } else {
                long j4 = a2 - j2;
                if (j2 > 0) {
                    Buffer buffer3 = new Buffer();
                    buffer.skip(j2);
                    buffer.read(buffer3, j4);
                    emitProgress(map, buffer3.u() - j3, true, chunkListener);
                    emitChunk(buffer3, z, chunkListener);
                    j3 = 0;
                    map = null;
                } else {
                    ChunkListener chunkListener2 = chunkListener;
                    buffer.skip(a2);
                }
                if (z) {
                    return true;
                }
                j2 = (long) d.size();
                j = j2;
            }
        }
    }
}
