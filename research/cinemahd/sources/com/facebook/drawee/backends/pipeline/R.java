package com.facebook.drawee.backends.pipeline;

public final class R {

    public static final class attr {
        public static final int actualImageResource = 2130968610;
        public static final int actualImageScaleType = 2130968611;
        public static final int actualImageUri = 2130968612;
        public static final int backgroundImage = 2130968642;
        public static final int fadeDuration = 2130968888;
        public static final int failureImage = 2130968889;
        public static final int failureImageScaleType = 2130968890;
        public static final int overlayImage = 2130969092;
        public static final int placeholderImage = 2130969111;
        public static final int placeholderImageScaleType = 2130969112;
        public static final int pressedStateOverlayImage = 2130969130;
        public static final int progressBarAutoRotateInterval = 2130969132;
        public static final int progressBarImage = 2130969133;
        public static final int progressBarImageScaleType = 2130969134;
        public static final int retryImage = 2130969145;
        public static final int retryImageScaleType = 2130969146;
        public static final int roundAsCircle = 2130969150;
        public static final int roundBottomEnd = 2130969151;
        public static final int roundBottomLeft = 2130969152;
        public static final int roundBottomRight = 2130969153;
        public static final int roundBottomStart = 2130969154;
        public static final int roundTopEnd = 2130969155;
        public static final int roundTopLeft = 2130969156;
        public static final int roundTopRight = 2130969157;
        public static final int roundTopStart = 2130969158;
        public static final int roundWithOverlayColor = 2130969159;
        public static final int roundedCornerRadius = 2130969160;
        public static final int roundingBorderColor = 2130969161;
        public static final int roundingBorderPadding = 2130969162;
        public static final int roundingBorderWidth = 2130969163;
        public static final int viewAspectRatio = 2130969340;

        private attr() {
        }
    }

    public static final class id {
        public static final int center = 2131296485;
        public static final int centerCrop = 2131296486;
        public static final int centerInside = 2131296487;
        public static final int fitBottomStart = 2131296637;
        public static final int fitCenter = 2131296638;
        public static final int fitEnd = 2131296639;
        public static final int fitStart = 2131296640;
        public static final int fitXY = 2131296641;
        public static final int focusCrop = 2131296646;
        public static final int none = 2131296922;

        private id() {
        }
    }

    public static final class styleable {
        public static final int[] GenericDraweeHierarchy = {com.yoku.marumovie.R.attr.actualImageScaleType, com.yoku.marumovie.R.attr.backgroundImage, com.yoku.marumovie.R.attr.fadeDuration, com.yoku.marumovie.R.attr.failureImage, com.yoku.marumovie.R.attr.failureImageScaleType, com.yoku.marumovie.R.attr.overlayImage, com.yoku.marumovie.R.attr.placeholderImage, com.yoku.marumovie.R.attr.placeholderImageScaleType, com.yoku.marumovie.R.attr.pressedStateOverlayImage, com.yoku.marumovie.R.attr.progressBarAutoRotateInterval, com.yoku.marumovie.R.attr.progressBarImage, com.yoku.marumovie.R.attr.progressBarImageScaleType, com.yoku.marumovie.R.attr.retryImage, com.yoku.marumovie.R.attr.retryImageScaleType, com.yoku.marumovie.R.attr.roundAsCircle, com.yoku.marumovie.R.attr.roundBottomEnd, com.yoku.marumovie.R.attr.roundBottomLeft, com.yoku.marumovie.R.attr.roundBottomRight, com.yoku.marumovie.R.attr.roundBottomStart, com.yoku.marumovie.R.attr.roundTopEnd, com.yoku.marumovie.R.attr.roundTopLeft, com.yoku.marumovie.R.attr.roundTopRight, com.yoku.marumovie.R.attr.roundTopStart, com.yoku.marumovie.R.attr.roundWithOverlayColor, com.yoku.marumovie.R.attr.roundedCornerRadius, com.yoku.marumovie.R.attr.roundingBorderColor, com.yoku.marumovie.R.attr.roundingBorderPadding, com.yoku.marumovie.R.attr.roundingBorderWidth, com.yoku.marumovie.R.attr.viewAspectRatio};
        public static final int GenericDraweeHierarchy_actualImageScaleType = 0;
        public static final int GenericDraweeHierarchy_backgroundImage = 1;
        public static final int GenericDraweeHierarchy_fadeDuration = 2;
        public static final int GenericDraweeHierarchy_failureImage = 3;
        public static final int GenericDraweeHierarchy_failureImageScaleType = 4;
        public static final int GenericDraweeHierarchy_overlayImage = 5;
        public static final int GenericDraweeHierarchy_placeholderImage = 6;
        public static final int GenericDraweeHierarchy_placeholderImageScaleType = 7;
        public static final int GenericDraweeHierarchy_pressedStateOverlayImage = 8;
        public static final int GenericDraweeHierarchy_progressBarAutoRotateInterval = 9;
        public static final int GenericDraweeHierarchy_progressBarImage = 10;
        public static final int GenericDraweeHierarchy_progressBarImageScaleType = 11;
        public static final int GenericDraweeHierarchy_retryImage = 12;
        public static final int GenericDraweeHierarchy_retryImageScaleType = 13;
        public static final int GenericDraweeHierarchy_roundAsCircle = 14;
        public static final int GenericDraweeHierarchy_roundBottomEnd = 15;
        public static final int GenericDraweeHierarchy_roundBottomLeft = 16;
        public static final int GenericDraweeHierarchy_roundBottomRight = 17;
        public static final int GenericDraweeHierarchy_roundBottomStart = 18;
        public static final int GenericDraweeHierarchy_roundTopEnd = 19;
        public static final int GenericDraweeHierarchy_roundTopLeft = 20;
        public static final int GenericDraweeHierarchy_roundTopRight = 21;
        public static final int GenericDraweeHierarchy_roundTopStart = 22;
        public static final int GenericDraweeHierarchy_roundWithOverlayColor = 23;
        public static final int GenericDraweeHierarchy_roundedCornerRadius = 24;
        public static final int GenericDraweeHierarchy_roundingBorderColor = 25;
        public static final int GenericDraweeHierarchy_roundingBorderPadding = 26;
        public static final int GenericDraweeHierarchy_roundingBorderWidth = 27;
        public static final int GenericDraweeHierarchy_viewAspectRatio = 28;
        public static final int[] SimpleDraweeView = {com.yoku.marumovie.R.attr.actualImageResource, com.yoku.marumovie.R.attr.actualImageScaleType, com.yoku.marumovie.R.attr.actualImageUri, com.yoku.marumovie.R.attr.backgroundImage, com.yoku.marumovie.R.attr.fadeDuration, com.yoku.marumovie.R.attr.failureImage, com.yoku.marumovie.R.attr.failureImageScaleType, com.yoku.marumovie.R.attr.overlayImage, com.yoku.marumovie.R.attr.placeholderImage, com.yoku.marumovie.R.attr.placeholderImageScaleType, com.yoku.marumovie.R.attr.pressedStateOverlayImage, com.yoku.marumovie.R.attr.progressBarAutoRotateInterval, com.yoku.marumovie.R.attr.progressBarImage, com.yoku.marumovie.R.attr.progressBarImageScaleType, com.yoku.marumovie.R.attr.retryImage, com.yoku.marumovie.R.attr.retryImageScaleType, com.yoku.marumovie.R.attr.roundAsCircle, com.yoku.marumovie.R.attr.roundBottomEnd, com.yoku.marumovie.R.attr.roundBottomLeft, com.yoku.marumovie.R.attr.roundBottomRight, com.yoku.marumovie.R.attr.roundBottomStart, com.yoku.marumovie.R.attr.roundTopEnd, com.yoku.marumovie.R.attr.roundTopLeft, com.yoku.marumovie.R.attr.roundTopRight, com.yoku.marumovie.R.attr.roundTopStart, com.yoku.marumovie.R.attr.roundWithOverlayColor, com.yoku.marumovie.R.attr.roundedCornerRadius, com.yoku.marumovie.R.attr.roundingBorderColor, com.yoku.marumovie.R.attr.roundingBorderPadding, com.yoku.marumovie.R.attr.roundingBorderWidth, com.yoku.marumovie.R.attr.viewAspectRatio};
        public static final int SimpleDraweeView_actualImageResource = 0;
        public static final int SimpleDraweeView_actualImageScaleType = 1;
        public static final int SimpleDraweeView_actualImageUri = 2;
        public static final int SimpleDraweeView_backgroundImage = 3;
        public static final int SimpleDraweeView_fadeDuration = 4;
        public static final int SimpleDraweeView_failureImage = 5;
        public static final int SimpleDraweeView_failureImageScaleType = 6;
        public static final int SimpleDraweeView_overlayImage = 7;
        public static final int SimpleDraweeView_placeholderImage = 8;
        public static final int SimpleDraweeView_placeholderImageScaleType = 9;
        public static final int SimpleDraweeView_pressedStateOverlayImage = 10;
        public static final int SimpleDraweeView_progressBarAutoRotateInterval = 11;
        public static final int SimpleDraweeView_progressBarImage = 12;
        public static final int SimpleDraweeView_progressBarImageScaleType = 13;
        public static final int SimpleDraweeView_retryImage = 14;
        public static final int SimpleDraweeView_retryImageScaleType = 15;
        public static final int SimpleDraweeView_roundAsCircle = 16;
        public static final int SimpleDraweeView_roundBottomEnd = 17;
        public static final int SimpleDraweeView_roundBottomLeft = 18;
        public static final int SimpleDraweeView_roundBottomRight = 19;
        public static final int SimpleDraweeView_roundBottomStart = 20;
        public static final int SimpleDraweeView_roundTopEnd = 21;
        public static final int SimpleDraweeView_roundTopLeft = 22;
        public static final int SimpleDraweeView_roundTopRight = 23;
        public static final int SimpleDraweeView_roundTopStart = 24;
        public static final int SimpleDraweeView_roundWithOverlayColor = 25;
        public static final int SimpleDraweeView_roundedCornerRadius = 26;
        public static final int SimpleDraweeView_roundingBorderColor = 27;
        public static final int SimpleDraweeView_roundingBorderPadding = 28;
        public static final int SimpleDraweeView_roundingBorderWidth = 29;
        public static final int SimpleDraweeView_viewAspectRatio = 30;

        private styleable() {
        }
    }

    private R() {
    }
}
