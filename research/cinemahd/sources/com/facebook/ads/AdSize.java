package com.facebook.ads;

import com.facebook.ads.internal.protocol.e;
import java.io.Serializable;

public class AdSize implements Serializable {
    @Deprecated
    public static final AdSize BANNER_320_50 = new AdSize(e.BANNER_320_50);
    public static final AdSize BANNER_HEIGHT_50 = new AdSize(e.BANNER_HEIGHT_50);
    public static final AdSize BANNER_HEIGHT_90 = new AdSize(e.BANNER_HEIGHT_90);
    public static final AdSize INTERSTITIAL = new AdSize(e.INTERSTITIAL);
    public static final AdSize RECTANGLE_HEIGHT_250 = new AdSize(e.RECTANGLE_HEIGHT_250);

    /* renamed from: a  reason: collision with root package name */
    private final int f2535a;
    private final int b;

    public AdSize(int i, int i2) {
        this.f2535a = i;
        this.b = i2;
    }

    private AdSize(e eVar) {
        this.f2535a = eVar.a();
        this.b = eVar.b();
    }

    public static AdSize fromWidthAndHeight(int i, int i2) {
        AdSize adSize = INTERSTITIAL;
        if (adSize.b == i2 && adSize.f2535a == i) {
            return adSize;
        }
        AdSize adSize2 = BANNER_320_50;
        if (adSize2.b == i2 && adSize2.f2535a == i) {
            return adSize2;
        }
        AdSize adSize3 = BANNER_HEIGHT_50;
        if (adSize3.b == i2 && adSize3.f2535a == i) {
            return adSize3;
        }
        AdSize adSize4 = BANNER_HEIGHT_90;
        if (adSize4.b == i2 && adSize4.f2535a == i) {
            return adSize4;
        }
        AdSize adSize5 = RECTANGLE_HEIGHT_250;
        if (adSize5.b == i2 && adSize5.f2535a == i) {
            return adSize5;
        }
        return null;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || AdSize.class != obj.getClass()) {
            return false;
        }
        AdSize adSize = (AdSize) obj;
        return this.f2535a == adSize.f2535a && this.b == adSize.b;
    }

    public int getHeight() {
        return this.b;
    }

    public int getWidth() {
        return this.f2535a;
    }

    public int hashCode() {
        return (this.f2535a * 31) + this.b;
    }

    public e toInternalAdSize() {
        return e.a(this.f2535a, this.b);
    }
}
