package com.facebook.ads;

import com.facebook.ads.internal.view.f.a.a;

public enum VideoStartReason {
    NOT_STARTED(a.NOT_STARTED),
    USER_STARTED(a.USER_STARTED),
    AUTO_STARTED(a.AUTO_STARTED);
    

    /* renamed from: a  reason: collision with root package name */
    private final a f2587a;

    private VideoStartReason(a aVar) {
        this.f2587a = aVar;
    }

    /* access modifiers changed from: package-private */
    public a a() {
        return this.f2587a;
    }
}
