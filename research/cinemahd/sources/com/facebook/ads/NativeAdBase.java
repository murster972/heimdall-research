package com.facebook.ads;

import android.content.Context;
import android.view.View;
import com.facebook.ads.internal.adapters.y;
import com.facebook.ads.internal.h.d;
import com.facebook.ads.internal.n.e;
import com.facebook.ads.internal.n.f;
import com.facebook.ads.internal.n.h;
import com.facebook.ads.internal.n.i;
import com.facebook.ads.internal.protocol.a;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.view.hscroll.b;
import org.json.JSONObject;

public abstract class NativeAdBase implements Ad {

    /* renamed from: a  reason: collision with root package name */
    private final f f2566a;

    public static class Image {

        /* renamed from: a  reason: collision with root package name */
        private final h f2568a;

        Image(h hVar) {
            this.f2568a = hVar;
        }

        public Image(String str, int i, int i2) {
            this.f2568a = new h(str, i, i2);
        }

        public static Image fromJSONObject(JSONObject jSONObject) {
            h a2 = h.a(jSONObject);
            if (a2 == null) {
                return null;
            }
            return new Image(a2);
        }

        public int getHeight() {
            return this.f2568a.c();
        }

        public int getWidth() {
            return this.f2568a.b();
        }
    }

    public enum MediaCacheFlag {
        NONE(e.NONE),
        ALL(e.ALL);
        

        /* renamed from: a  reason: collision with root package name */
        private final e f2569a;

        private MediaCacheFlag(e eVar) {
            this.f2569a = eVar;
        }

        /* access modifiers changed from: package-private */
        public e a() {
            return this.f2569a;
        }

        public long getCacheFlagValue() {
            return this.f2569a.a();
        }
    }

    public enum NativeComponentTag {
        AD_ICON(j.INTERNAL_AD_ICON),
        AD_TITLE(j.INTERNAL_AD_TITLE),
        AD_COVER_IMAGE(j.INTERNAL_AD_COVER_IMAGE),
        AD_SUBTITLE(j.INTERNAL_AD_SUBTITLE),
        AD_BODY(j.INTERNAL_AD_BODY),
        AD_CALL_TO_ACTION(j.INTERNAL_AD_CALL_TO_ACTION),
        AD_SOCIAL_CONTEXT(j.INTERNAL_AD_SOCIAL_CONTEXT),
        AD_CHOICES_ICON(j.INTERNAL_AD_CHOICES_ICON),
        AD_MEDIA(j.INTERNAL_AD_MEDIA);
        

        /* renamed from: a  reason: collision with root package name */
        private final j f2570a;

        private NativeComponentTag(j jVar) {
            this.f2570a = jVar;
        }

        public static void tagView(View view, NativeComponentTag nativeComponentTag) {
            if (view != null && nativeComponentTag != null) {
                j.a(view, nativeComponentTag.f2570a);
            }
        }
    }

    public static class Rating {

        /* renamed from: a  reason: collision with root package name */
        private final com.facebook.ads.internal.n.j f2571a;

        public Rating(double d, double d2) {
            this.f2571a = new com.facebook.ads.internal.n.j(d, d2);
        }

        Rating(com.facebook.ads.internal.n.j jVar) {
            this.f2571a = jVar;
        }

        public static Rating fromJSONObject(JSONObject jSONObject) {
            com.facebook.ads.internal.n.j a2 = com.facebook.ads.internal.n.j.a(jSONObject);
            if (a2 == null) {
                return null;
            }
            return new Rating(a2);
        }

        public double getScale() {
            return this.f2571a.b();
        }

        public double getValue() {
            return this.f2571a.a();
        }
    }

    public NativeAdBase(Context context, y yVar, d dVar) {
        this.f2566a = new f(context, yVar, dVar, getViewTraversalPredicate());
    }

    public NativeAdBase(Context context, String str) {
        this.f2566a = new f(context, str, getViewTraversalPredicate());
    }

    NativeAdBase(NativeAdBase nativeAdBase) {
        this.f2566a = new f(nativeAdBase.f2566a);
    }

    NativeAdBase(f fVar) {
        this.f2566a = fVar;
    }

    public static f.c getViewTraversalPredicate() {
        return new f.c() {
            public boolean a(View view) {
                return (view instanceof MediaViewVideoRenderer) || (view instanceof AdChoicesView) || (view instanceof b);
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void a(AdIconView adIconView) {
        if (adIconView != null) {
            this.f2566a.d(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(MediaView mediaView) {
        if (mediaView != null) {
            this.f2566a.c(true);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(com.facebook.ads.internal.protocol.f fVar) {
        this.f2566a.a(fVar);
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f2566a.a(z);
    }

    public void destroy() {
        this.f2566a.e();
    }

    public void downloadMedia() {
        this.f2566a.d();
    }

    /* access modifiers changed from: package-private */
    public f g() {
        return this.f2566a;
    }

    public String getAdBodyText() {
        return this.f2566a.o();
    }

    public String getAdCallToAction() {
        return this.f2566a.q();
    }

    public Image getAdChoicesIcon() {
        if (this.f2566a.y() == null) {
            return null;
        }
        return new Image(this.f2566a.y());
    }

    public String getAdChoicesImageUrl() {
        if (this.f2566a.y() == null) {
            return null;
        }
        return this.f2566a.y().a();
    }

    public String getAdChoicesLinkUrl() {
        return this.f2566a.z();
    }

    public String getAdChoicesText() {
        return this.f2566a.A();
    }

    public Image getAdCoverImage() {
        if (this.f2566a.k() == null) {
            return null;
        }
        return new Image(this.f2566a.k());
    }

    public String getAdHeadline() {
        return this.f2566a.n();
    }

    public Image getAdIcon() {
        if (this.f2566a.j() == null) {
            return null;
        }
        return new Image(this.f2566a.j());
    }

    public String getAdLinkDescription() {
        return this.f2566a.s();
    }

    public AdNetwork getAdNetwork() {
        return AdNetwork.fromInternalAdNetwork(this.f2566a.b());
    }

    public String getAdSocialContext() {
        return this.f2566a.r();
    }

    @Deprecated
    public Rating getAdStarRating() {
        if (this.f2566a.w() == null) {
            return null;
        }
        return new Rating(this.f2566a.w());
    }

    public String getAdTranslation() {
        return this.f2566a.u();
    }

    public String getAdUntrimmedBodyText() {
        return this.f2566a.p();
    }

    public NativeAdViewAttributes getAdViewAttributes() {
        if (this.f2566a.l() == null) {
            return null;
        }
        return new NativeAdViewAttributes(this.f2566a.l());
    }

    public String getAdvertiserName() {
        return this.f2566a.m();
    }

    public String getId() {
        return this.f2566a.x();
    }

    public String getPlacementId() {
        return this.f2566a.f();
    }

    public String getPromotedTranslation() {
        return this.f2566a.v();
    }

    public String getSponsoredTranslation() {
        return this.f2566a.t();
    }

    /* access modifiers changed from: package-private */
    public y h() {
        return this.f2566a.a();
    }

    public boolean hasCallToAction() {
        return this.f2566a.i();
    }

    /* access modifiers changed from: package-private */
    public String i() {
        return this.f2566a.G();
    }

    public boolean isAdInvalidated() {
        return this.f2566a.c();
    }

    public boolean isAdLoaded() {
        return this.f2566a.g();
    }

    public boolean isNativeConfigEnabled() {
        return this.f2566a.h();
    }

    public void loadAd() {
        loadAd(MediaCacheFlag.ALL);
    }

    public void loadAd(MediaCacheFlag mediaCacheFlag) {
        this.f2566a.a(mediaCacheFlag.a(), (String) null);
    }

    public void loadAdFromBid(String str) {
        loadAdFromBid(str, MediaCacheFlag.ALL);
    }

    public void loadAdFromBid(String str, MediaCacheFlag mediaCacheFlag) {
        this.f2566a.a(mediaCacheFlag.a(), str);
    }

    public void onCtaBroadcast() {
        this.f2566a.H();
    }

    public void setAdListener(final NativeAdListener nativeAdListener) {
        if (nativeAdListener != null) {
            this.f2566a.a((i) new i() {
                public void a() {
                    nativeAdListener.onMediaDownloaded(NativeAdBase.this);
                }

                public void a(a aVar) {
                    nativeAdListener.onError(NativeAdBase.this, AdError.getAdErrorFromWrapper(aVar));
                }

                public void b() {
                    nativeAdListener.onAdLoaded(NativeAdBase.this);
                }

                public void c() {
                    nativeAdListener.onAdClicked(NativeAdBase.this);
                }

                public void d() {
                    nativeAdListener.onLoggingImpression(NativeAdBase.this);
                }
            });
        }
    }

    public void setOnTouchListener(View.OnTouchListener onTouchListener) {
        this.f2566a.a(onTouchListener);
    }

    public void unregisterView() {
        this.f2566a.J();
    }
}
