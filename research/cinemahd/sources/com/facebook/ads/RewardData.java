package com.facebook.ads;

public class RewardData {

    /* renamed from: a  reason: collision with root package name */
    private String f2582a;
    private String b;

    public RewardData(String str, String str2) {
        this.f2582a = str;
        this.b = str2;
    }

    public String getCurrency() {
        return this.b;
    }

    public String getUserID() {
        return this.f2582a;
    }
}
