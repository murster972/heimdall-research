package com.facebook.ads.internal.e;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Looper;
import com.facebook.ads.internal.e.f;
import com.facebook.ads.internal.q.d.b;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2696a = ("SELECT tokens." + h.f2703a.b + ", " + "tokens" + "." + h.b.b + ", " + "events" + "." + c.f2695a.b + ", " + "events" + "." + c.c.b + ", " + "events" + "." + c.d.b + ", " + "events" + "." + c.e.b + ", " + "events" + "." + c.f.b + ", " + "events" + "." + c.g.b + ", " + "events" + "." + c.h.b + ", " + "events" + "." + c.i.b + " FROM " + "events" + " JOIN " + "tokens" + " ON " + "events" + "." + c.b.b + " = " + "tokens" + "." + h.f2703a.b + " ORDER BY " + "events" + "." + c.e.b + " ASC");
    private static final int b = Runtime.getRuntime().availableProcessors();
    private static final int c = Math.max(2, Math.min(b - 1, 4));
    private static final int d = ((b * 2) + 1);
    private static final ThreadFactory e = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f2697a = new AtomicInteger(1);

        public Thread newThread(Runnable runnable) {
            return new Thread(runnable, "DatabaseTask #" + this.f2697a.getAndIncrement());
        }
    };
    private static final BlockingQueue<Runnable> f = new LinkedBlockingQueue(128);
    private static final Executor g;
    private static final ReentrantReadWriteLock h = new ReentrantReadWriteLock();
    private static final Lock i = h.readLock();
    /* access modifiers changed from: private */
    public static final Lock j = h.writeLock();
    /* access modifiers changed from: private */
    public final Context k;
    /* access modifiers changed from: private */
    public final h l = new h(this);
    /* access modifiers changed from: private */
    public final c m = new c(this);
    private SQLiteOpenHelper n;

    private static class a<T> extends AsyncTask<Void, Void, T> {

        /* renamed from: a  reason: collision with root package name */
        private final f<T> f2699a;
        private final a<T> b;
        private final Context c;
        private f.a d;

        a(Context context, f<T> fVar, a<T> aVar) {
            this.f2699a = fVar;
            this.b = aVar;
            this.c = context;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public T doInBackground(Void... voidArr) {
            T t = null;
            try {
                t = this.f2699a.b();
                this.d = this.f2699a.c();
                return t;
            } catch (Exception e) {
                com.facebook.ads.internal.q.d.a.a(this.c, "database", b.l, e);
                this.d = f.a.UNKNOWN;
                return t;
            }
        }

        /* access modifiers changed from: protected */
        public void onPostExecute(T t) {
            f.a aVar = this.d;
            if (aVar == null) {
                this.b.a(t);
            } else {
                this.b.a(aVar.a(), this.d.b());
            }
            this.b.a();
        }
    }

    static {
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(c, d, 30, TimeUnit.SECONDS, f, e);
        threadPoolExecutor.allowCoreThreadTimeOut(true);
        g = threadPoolExecutor;
    }

    public d(Context context) {
        this.k = context;
    }

    private synchronized SQLiteDatabase i() {
        if (this.n == null) {
            this.n = new e(this.k, this);
        }
        return this.n.getWritableDatabase();
    }

    public Cursor a(int i2) {
        i.lock();
        try {
            SQLiteDatabase a2 = a();
            return a2.rawQuery(f2696a + " LIMIT " + String.valueOf(i2), (String[]) null);
        } finally {
            i.unlock();
        }
    }

    public SQLiteDatabase a() {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            return i();
        }
        throw new IllegalStateException("Cannot call getDatabase from the UI thread!");
    }

    public <T> AsyncTask a(f<T> fVar, a<T> aVar) {
        return com.facebook.ads.internal.q.a.d.a(g, new a(this.k.getApplicationContext(), fVar, aVar), new Void[0]);
    }

    public AsyncTask a(String str, int i2, String str2, double d2, double d3, String str3, Map<String, String> map, a<String> aVar) {
        final String str4 = str;
        final int i3 = i2;
        final String str5 = str2;
        final double d4 = d2;
        final double d5 = d3;
        final String str6 = str3;
        final Map<String, String> map2 = map;
        return a(new i<String>() {
            /* JADX WARNING: Removed duplicated region for block: B:32:0x008e A[Catch:{ Exception -> 0x0092 }] */
            /* JADX WARNING: Removed duplicated region for block: B:44:0x00b5 A[Catch:{ Exception -> 0x00b9 }] */
            /* renamed from: a */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public java.lang.String b() {
                /*
                    r15 = this;
                    java.lang.String r0 = "database"
                    java.lang.String r1 = r2
                    boolean r1 = android.text.TextUtils.isEmpty(r1)
                    r2 = 0
                    if (r1 == 0) goto L_0x000c
                    return r2
                L_0x000c:
                    java.util.concurrent.locks.Lock r1 = com.facebook.ads.internal.e.d.j
                    r1.lock()
                    com.facebook.ads.internal.e.d r1 = com.facebook.ads.internal.e.d.this     // Catch:{ Exception -> 0x006e, all -> 0x0069 }
                    android.database.sqlite.SQLiteDatabase r1 = r1.a()     // Catch:{ Exception -> 0x006e, all -> 0x0069 }
                    r1.beginTransaction()     // Catch:{ Exception -> 0x0067 }
                    com.facebook.ads.internal.e.d r3 = com.facebook.ads.internal.e.d.this     // Catch:{ Exception -> 0x0067 }
                    com.facebook.ads.internal.e.c r4 = r3.m     // Catch:{ Exception -> 0x0067 }
                    com.facebook.ads.internal.e.d r3 = com.facebook.ads.internal.e.d.this     // Catch:{ Exception -> 0x0067 }
                    com.facebook.ads.internal.e.h r3 = r3.l     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r5 = r2     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r5 = r3.a(r5)     // Catch:{ Exception -> 0x0067 }
                    int r6 = r3     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r7 = r4     // Catch:{ Exception -> 0x0067 }
                    double r8 = r5     // Catch:{ Exception -> 0x0067 }
                    double r10 = r7     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r12 = r9     // Catch:{ Exception -> 0x0067 }
                    java.util.Map r13 = r10     // Catch:{ Exception -> 0x0067 }
                    java.lang.String r3 = r4.a(r5, r6, r7, r8, r10, r12, r13)     // Catch:{ Exception -> 0x0067 }
                    r1.setTransactionSuccessful()     // Catch:{ Exception -> 0x0067 }
                    if (r1 == 0) goto L_0x005f
                    boolean r2 = r1.isOpen()
                    if (r2 == 0) goto L_0x005f
                    boolean r2 = r1.inTransaction()     // Catch:{ Exception -> 0x0053 }
                    if (r2 == 0) goto L_0x005f
                    r1.endTransaction()     // Catch:{ Exception -> 0x0053 }
                    goto L_0x005f
                L_0x0053:
                    r1 = move-exception
                    com.facebook.ads.internal.e.d r2 = com.facebook.ads.internal.e.d.this
                    android.content.Context r2 = r2.k
                    int r4 = com.facebook.ads.internal.q.d.b.k
                    com.facebook.ads.internal.q.d.a.a((android.content.Context) r2, (java.lang.String) r0, (int) r4, (java.lang.Exception) r1)
                L_0x005f:
                    java.util.concurrent.locks.Lock r0 = com.facebook.ads.internal.e.d.j
                    r0.unlock()
                    return r3
                L_0x0067:
                    r3 = move-exception
                    goto L_0x0070
                L_0x0069:
                    r1 = move-exception
                    r14 = r2
                    r2 = r1
                    r1 = r14
                    goto L_0x00a7
                L_0x006e:
                    r3 = move-exception
                    r1 = r2
                L_0x0070:
                    com.facebook.ads.internal.e.f$a r4 = com.facebook.ads.internal.e.f.a.DATABASE_INSERT     // Catch:{ all -> 0x00a6 }
                    r15.a(r4)     // Catch:{ all -> 0x00a6 }
                    com.facebook.ads.internal.e.d r4 = com.facebook.ads.internal.e.d.this     // Catch:{ all -> 0x00a6 }
                    android.content.Context r4 = r4.k     // Catch:{ all -> 0x00a6 }
                    int r5 = com.facebook.ads.internal.q.d.b.i     // Catch:{ all -> 0x00a6 }
                    com.facebook.ads.internal.q.d.a.a((android.content.Context) r4, (java.lang.String) r0, (int) r5, (java.lang.Exception) r3)     // Catch:{ all -> 0x00a6 }
                    if (r1 == 0) goto L_0x009e
                    boolean r3 = r1.isOpen()
                    if (r3 == 0) goto L_0x009e
                    boolean r3 = r1.inTransaction()     // Catch:{ Exception -> 0x0092 }
                    if (r3 == 0) goto L_0x009e
                    r1.endTransaction()     // Catch:{ Exception -> 0x0092 }
                    goto L_0x009e
                L_0x0092:
                    r1 = move-exception
                    com.facebook.ads.internal.e.d r3 = com.facebook.ads.internal.e.d.this
                    android.content.Context r3 = r3.k
                    int r4 = com.facebook.ads.internal.q.d.b.k
                    com.facebook.ads.internal.q.d.a.a((android.content.Context) r3, (java.lang.String) r0, (int) r4, (java.lang.Exception) r1)
                L_0x009e:
                    java.util.concurrent.locks.Lock r0 = com.facebook.ads.internal.e.d.j
                    r0.unlock()
                    return r2
                L_0x00a6:
                    r2 = move-exception
                L_0x00a7:
                    if (r1 == 0) goto L_0x00c5
                    boolean r3 = r1.isOpen()
                    if (r3 == 0) goto L_0x00c5
                    boolean r3 = r1.inTransaction()     // Catch:{ Exception -> 0x00b9 }
                    if (r3 == 0) goto L_0x00c5
                    r1.endTransaction()     // Catch:{ Exception -> 0x00b9 }
                    goto L_0x00c5
                L_0x00b9:
                    r1 = move-exception
                    com.facebook.ads.internal.e.d r3 = com.facebook.ads.internal.e.d.this
                    android.content.Context r3 = r3.k
                    int r4 = com.facebook.ads.internal.q.d.b.k
                    com.facebook.ads.internal.q.d.a.a((android.content.Context) r3, (java.lang.String) r0, (int) r4, (java.lang.Exception) r1)
                L_0x00c5:
                    java.util.concurrent.locks.Lock r0 = com.facebook.ads.internal.e.d.j
                    r0.unlock()
                    throw r2
                */
                throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.e.d.AnonymousClass2.b():java.lang.String");
            }
        }, aVar);
    }

    public boolean a(String str) {
        j.lock();
        boolean z = true;
        try {
            a().execSQL("UPDATE " + "events" + " SET " + c.i.b + "=" + c.i.b + "+1" + " WHERE " + c.f2695a.b + "=?", new String[]{str});
        } catch (SQLiteException unused) {
            z = false;
        }
        j.unlock();
        return z;
    }

    public synchronized void b() {
        for (g e2 : c()) {
            e2.e();
        }
        if (this.n != null) {
            this.n.close();
            this.n = null;
        }
    }

    public boolean b(String str) {
        j.lock();
        try {
            return this.m.a(str);
        } finally {
            j.unlock();
        }
    }

    public g[] c() {
        return new g[]{this.l, this.m};
    }

    public Cursor d() {
        i.lock();
        try {
            return this.m.c();
        } finally {
            i.unlock();
        }
    }

    public Cursor e() {
        i.lock();
        try {
            return this.m.d();
        } finally {
            i.unlock();
        }
    }

    public Cursor f() {
        i.lock();
        try {
            return this.l.c();
        } finally {
            i.unlock();
        }
    }

    public void g() {
        j.lock();
        try {
            this.l.d();
        } finally {
            j.unlock();
        }
    }
}
