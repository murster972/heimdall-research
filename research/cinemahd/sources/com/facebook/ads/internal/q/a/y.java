package com.facebook.ads.internal.q.a;

import java.lang.ref.WeakReference;

public abstract class y<T> implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final WeakReference<T> f2830a;

    public y(T t) {
        this.f2830a = new WeakReference<>(t);
    }

    public T a() {
        return this.f2830a.get();
    }
}
