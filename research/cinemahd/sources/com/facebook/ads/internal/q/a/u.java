package com.facebook.ads.internal.q.a;

import android.os.Build;
import android.view.InputDevice;
import android.view.MotionEvent;
import android.view.View;
import java.util.HashMap;
import java.util.Map;

public class u {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2828a = "u";
    private boolean b;
    private int c = -1;
    private int d = -1;
    private int e = -1;
    private int f = -1;
    private long g = -1;
    private int h = -1;
    private long i = -1;
    private long j = -1;
    private int k = -1;
    private int l = -1;
    private int m = -1;
    private int n = -1;
    private float o = -1.0f;
    private float p = -1.0f;
    private float q = -1.0f;
    private View r;
    private View s;

    private j f() {
        View view;
        View view2 = this.r;
        if (view2 == null || (view = this.s) == null) {
            return j.INTERNAL_NULL_VIEW;
        }
        if (view2 != view) {
            return j.INTERNAL_NO_CLICK;
        }
        if (Build.VERSION.SDK_INT < 4) {
            return j.INTERNAL_API_TOO_LOW;
        }
        Object tag = view2.getTag(j.o);
        return tag == null ? j.INTERNAL_NO_TAG : !(tag instanceof j) ? j.INTERNAL_WRONG_TAG_CLASS : (j) tag;
    }

    public void a() {
        this.g = System.currentTimeMillis();
    }

    public void a(MotionEvent motionEvent, View view, View view2) {
        if (!this.b) {
            this.b = true;
            InputDevice device = motionEvent.getDevice();
            if (device != null) {
                InputDevice.MotionRange motionRange = device.getMotionRange(0);
                InputDevice.MotionRange motionRange2 = device.getMotionRange(1);
                if (!(motionRange == null || motionRange2 == null)) {
                    this.q = Math.min(motionRange.getRange(), motionRange2.getRange());
                }
            }
            if (this.q <= 0.0f) {
                this.q = (float) Math.min(view.getMeasuredWidth(), view.getMeasuredHeight());
            }
        }
        int[] iArr = new int[2];
        view.getLocationInWindow(iArr);
        int[] iArr2 = new int[2];
        view2.getLocationInWindow(iArr2);
        int action = motionEvent.getAction();
        if (action != 0) {
            if (action != 1) {
                if (action == 2) {
                    float f2 = this.o;
                    this.o = f2 - (f2 / ((float) this.h));
                    float f3 = this.o;
                    float pressure = motionEvent.getPressure();
                    int i2 = this.h;
                    this.o = f3 + (pressure / ((float) i2));
                    float f4 = this.p;
                    this.p = f4 - (f4 / ((float) i2));
                    float f5 = this.p;
                    float size = motionEvent.getSize();
                    int i3 = this.h;
                    this.p = f5 + (size / ((float) i3));
                    this.h = i3 + 1;
                    return;
                } else if (action != 3) {
                    return;
                }
            }
            this.j = System.currentTimeMillis();
            this.m = (int) (((float) ((((int) (motionEvent.getX() + 0.5f)) + iArr2[0]) - iArr[0])) / x.b);
            this.n = (int) (((float) ((((int) (motionEvent.getY() + 0.5f)) + iArr2[1]) - iArr[1])) / x.b);
            this.s = view2;
            return;
        }
        float f6 = x.b;
        this.c = (int) (((float) iArr[0]) / f6);
        this.d = (int) (((float) iArr[1]) / f6);
        this.e = (int) (((float) view.getWidth()) / x.b);
        this.f = (int) (((float) view.getHeight()) / x.b);
        this.h = 1;
        this.i = System.currentTimeMillis();
        this.k = (int) (((float) ((((int) (motionEvent.getX() + 0.5f)) + iArr2[0]) - iArr[0])) / x.b);
        this.l = (int) (((float) ((((int) (motionEvent.getY() + 0.5f)) + iArr2[1]) - iArr[1])) / x.b);
        this.o = motionEvent.getPressure();
        this.p = motionEvent.getSize();
        this.r = view2;
    }

    public boolean b() {
        return this.g != -1;
    }

    public long c() {
        if (b()) {
            return System.currentTimeMillis() - this.g;
        }
        return -1;
    }

    public boolean d() {
        return this.b;
    }

    public Map<String, String> e() {
        long j2;
        if (!this.b) {
            return null;
        }
        String valueOf = String.valueOf((this.p * this.q) / 2.0f);
        long j3 = this.g;
        if (j3 > 0) {
            long j4 = this.j;
            if (j4 > j3) {
                j2 = j4 - j3;
                HashMap hashMap = new HashMap();
                hashMap.put("adPositionX", String.valueOf(this.c));
                hashMap.put("adPositionY", String.valueOf(this.d));
                hashMap.put("width", String.valueOf(this.e));
                hashMap.put("height", String.valueOf(this.f));
                hashMap.put("clickDelayTime", String.valueOf(j2));
                hashMap.put("startTime", String.valueOf(this.i));
                hashMap.put("endTime", String.valueOf(this.j));
                hashMap.put("startX", String.valueOf(this.k));
                hashMap.put("startY", String.valueOf(this.l));
                hashMap.put("clickX", String.valueOf(this.m));
                hashMap.put("clickY", String.valueOf(this.n));
                hashMap.put("endX", String.valueOf(this.m));
                hashMap.put("endY", String.valueOf(this.n));
                hashMap.put("force", String.valueOf(this.o));
                hashMap.put("radiusX", valueOf);
                hashMap.put("radiusY", valueOf);
                hashMap.put("clickedViewTag", String.valueOf(f().a()));
                return hashMap;
            }
        }
        j2 = -1;
        HashMap hashMap2 = new HashMap();
        hashMap2.put("adPositionX", String.valueOf(this.c));
        hashMap2.put("adPositionY", String.valueOf(this.d));
        hashMap2.put("width", String.valueOf(this.e));
        hashMap2.put("height", String.valueOf(this.f));
        hashMap2.put("clickDelayTime", String.valueOf(j2));
        hashMap2.put("startTime", String.valueOf(this.i));
        hashMap2.put("endTime", String.valueOf(this.j));
        hashMap2.put("startX", String.valueOf(this.k));
        hashMap2.put("startY", String.valueOf(this.l));
        hashMap2.put("clickX", String.valueOf(this.m));
        hashMap2.put("clickY", String.valueOf(this.n));
        hashMap2.put("endX", String.valueOf(this.m));
        hashMap2.put("endY", String.valueOf(this.n));
        hashMap2.put("force", String.valueOf(this.o));
        hashMap2.put("radiusX", valueOf);
        hashMap2.put("radiusY", valueOf);
        hashMap2.put("clickedViewTag", String.valueOf(f().a()));
        return hashMap2;
    }
}
