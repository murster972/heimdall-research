package com.facebook.ads.internal.q.a;

import org.json.JSONArray;

public enum c {
    APP_AD(0),
    LINK_AD(1),
    APP_AD_V2(2),
    LINK_AD_V2(3),
    APP_ENGAGEMENT_AD(4),
    AD_CHOICES(5),
    JS_TRIGGER(6),
    JS_TRIGGER_NO_AUTO_IMP_LOGGING(7),
    VIDEO_AD(8),
    INLINE_VIDEO_AD(9),
    BANNER_TO_INTERSTITIAL(10),
    NATIVE_CLOSE_BUTTON(11),
    UNIFIED_LOGGING(16),
    HTTP_LINKS(17);
    
    public static final c[] o = null;
    private static final String q = null;
    private final int p;

    static {
        int i;
        c cVar;
        c cVar2;
        c cVar3;
        c cVar4;
        c cVar5;
        c cVar6;
        c cVar7;
        o = new c[]{cVar, cVar2, cVar3, cVar4, cVar5, cVar6, cVar7};
        JSONArray jSONArray = new JSONArray();
        for (c a2 : o) {
            jSONArray.put(a2.a());
        }
        q = jSONArray.toString();
    }

    private c(int i) {
        this.p = i;
    }

    public static String b() {
        return q;
    }

    public int a() {
        return this.p;
    }

    public String toString() {
        return String.valueOf(this.p);
    }
}
