package com.facebook.ads.internal.q.e;

import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.util.Log;
import android.view.Window;
import com.facebook.ads.internal.q.a.z;
import com.facebook.ads.internal.q.d.a;
import java.util.HashMap;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2842a = "b";

    public static Map<String, String> a(Context context) {
        String str;
        String str2;
        HashMap hashMap = new HashMap();
        if (context == null) {
            Log.v(f2842a, "Null context in window interactive check.");
            return hashMap;
        }
        try {
            hashMap.put("kgr", String.valueOf(c(context)));
            if (context instanceof Activity) {
                Window window = ((Activity) context).getWindow();
                if (window != null) {
                    int i = window.getAttributes().flags;
                    hashMap.put("wt", Integer.toString(window.getAttributes().type));
                    int i2 = 4194304 & i;
                    String str3 = DiskLruCache.VERSION_1;
                    hashMap.put("wfdkg", i2 > 0 ? str3 : "0");
                    if ((524288 & i) <= 0) {
                        str3 = "0";
                    }
                    hashMap.put("wfswl", str3);
                    return hashMap;
                }
                str = f2842a;
                str2 = "Invalid window in window interactive check, assuming interactive.";
            } else {
                str = f2842a;
                str2 = "Invalid Activity context in window interactive check, assuming interactive.";
            }
            Log.v(str, str2);
        } catch (Exception e) {
            Log.e(f2842a, "Exception in window info check", e);
            a.a(context, "risky", com.facebook.ads.internal.q.d.b.r, e);
        }
        return hashMap;
    }

    public static boolean b(Context context) {
        return !z.b(a(context));
    }

    public static boolean c(Context context) {
        KeyguardManager keyguardManager = (KeyguardManager) context.getSystemService("keyguard");
        return keyguardManager != null && keyguardManager.inKeyguardRestrictedInputMode();
    }
}
