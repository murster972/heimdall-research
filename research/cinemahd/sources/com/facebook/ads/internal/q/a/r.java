package com.facebook.ads.internal.q.a;

import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.vungle.warren.AdLoader;

public class r implements View.OnSystemUiVisibilityChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private final View f2823a;
    private int b;
    private Window c;
    private a d = a.DEFAULT;
    private final Runnable e = new Runnable() {
        public void run() {
            r.this.a(false);
        }
    };

    /* renamed from: com.facebook.ads.internal.q.a.r$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2825a = new int[a.values().length];

        static {
            try {
                f2825a[a.FULL_SCREEN.ordinal()] = 1;
            } catch (NoSuchFieldError unused) {
            }
        }
    }

    public enum a {
        DEFAULT,
        FULL_SCREEN
    }

    public r(View view) {
        this.f2823a = view;
        this.f2823a.setOnSystemUiVisibilityChangeListener(this);
    }

    private void a(int i, boolean z) {
        int i2;
        Window window = this.c;
        if (window != null) {
            WindowManager.LayoutParams attributes = window.getAttributes();
            if (z) {
                i2 = i | attributes.flags;
            } else {
                i2 = (~i) & attributes.flags;
            }
            attributes.flags = i2;
            this.c.setAttributes(attributes);
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (!a.DEFAULT.equals(this.d)) {
            int i = 3840;
            if (!z) {
                i = 3847;
            }
            Handler handler = this.f2823a.getHandler();
            if (handler != null && z) {
                handler.removeCallbacks(this.e);
                handler.postDelayed(this.e, AdLoader.RETRY_DELAY);
            }
            this.f2823a.setSystemUiVisibility(i);
        }
    }

    public void a() {
        this.c = null;
    }

    public void a(Window window) {
        this.c = window;
    }

    public void a(a aVar) {
        this.d = aVar;
        if (AnonymousClass2.f2825a[this.d.ordinal()] != 1) {
            a(67108864, false);
            a(134217728, false);
            this.f2823a.setSystemUiVisibility(0);
            return;
        }
        a(67108864, true);
        a(134217728, true);
        a(false);
    }

    public void onSystemUiVisibilityChange(int i) {
        this.b = i;
        if (((this.b ^ i) & 2) != 0 && (i & 2) == 0) {
            a(true);
        }
    }
}
