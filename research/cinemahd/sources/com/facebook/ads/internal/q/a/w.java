package com.facebook.ads.internal.q.a;

import android.content.Context;
import android.media.AudioManager;
import com.facebook.ads.AudienceNetworkActivity;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class w {
    public static float a(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        if (audioManager == null) {
            return 0.0f;
        }
        int streamVolume = audioManager.getStreamVolume(3);
        int streamMaxVolume = audioManager.getStreamMaxVolume(3);
        if (streamMaxVolume > 0) {
            return (((float) streamVolume) * 1.0f) / ((float) streamMaxVolume);
        }
        return 0.0f;
    }

    public static void a(Map<String, String> map, boolean z, boolean z2) {
        String str = DiskLruCache.VERSION_1;
        map.put(AudienceNetworkActivity.AUTOPLAY, z ? str : "0");
        if (!z2) {
            str = "0";
        }
        map.put("inline", str);
    }
}
