package com.facebook.ads.internal.q.a;

import android.text.TextUtils;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f2810a = false;
    private static boolean b = false;

    public static synchronized String a(String str) {
        synchronized (b.class) {
            if (!a()) {
                return null;
            }
            String property = System.getProperty("fb.e2e." + str);
            return property;
        }
    }

    public static synchronized boolean a() {
        boolean z;
        synchronized (b.class) {
            if (!b) {
                f2810a = "true".equals(System.getProperty("fb.running_e2e"));
                b = true;
            }
            z = f2810a;
        }
        return z;
    }

    public static synchronized boolean b(String str) {
        boolean z;
        synchronized (b.class) {
            z = !TextUtils.isEmpty(a(str));
        }
        return z;
    }
}
