package com.facebook.ads.internal.q.d;

import android.content.Context;
import com.facebook.ads.internal.f.e;
import com.facebook.ads.internal.i.c;
import java.util.Map;
import java.util.Set;

public class a {
    public static void a(Context context, String str, int i, Exception exc) {
        if (a(context, str, i, Math.random())) {
            b(context, str, i, exc);
        }
    }

    static boolean a(Context context, String str, int i, double d) {
        double d2;
        double d3;
        Set<String> i2 = com.facebook.ads.internal.l.a.i(context);
        if (i2.contains(str + ":" + i)) {
            d2 = (double) (com.facebook.ads.internal.l.a.k(context) * com.facebook.ads.internal.l.a.j(context));
            d3 = 10000.0d;
        } else {
            d2 = (double) com.facebook.ads.internal.l.a.k(context);
            d3 = 100.0d;
        }
        return d >= 1.0d - (d2 / d3);
    }

    private static void b(Context context, String str, int i, Exception exc) {
        Map<String, String> b = new c(context, false).b();
        b.put("subtype", str);
        b.put("subtype_code", String.valueOf(i));
        e.a(exc, context, b);
    }
}
