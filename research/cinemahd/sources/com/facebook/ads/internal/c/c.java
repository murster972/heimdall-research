package com.facebook.ads.internal.c;

public class c {

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        public String f2685a;
        public String b;
        public boolean c;

        public a(String str, String str2, boolean z) {
            this.f2685a = str;
            this.b = str2;
            this.c = z;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:?, code lost:
        r0 = new com.facebook.ads.internal.c.c.a((java.lang.String) null, (java.lang.String) null, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0062, code lost:
        if (r11 != null) goto L_0x0064;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0064, code lost:
        r11.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0067, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0068, code lost:
        r0 = th;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x005d */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.facebook.ads.internal.c.c.a a(android.content.ContentResolver r11) {
        /*
            java.lang.String r0 = "limit_tracking"
            java.lang.String r1 = "androidid"
            java.lang.String r2 = "aid"
            r3 = 0
            r4 = 0
            java.lang.String[] r7 = new java.lang.String[]{r2, r1, r0}     // Catch:{ Exception -> 0x005c, all -> 0x0059 }
            java.lang.String r5 = "content://com.facebook.katana.provider.AttributionIdProvider"
            android.net.Uri r6 = android.net.Uri.parse(r5)     // Catch:{ Exception -> 0x005c, all -> 0x0059 }
            r8 = 0
            r9 = 0
            r10 = 0
            r5 = r11
            android.database.Cursor r11 = r5.query(r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x005c, all -> 0x0059 }
            if (r11 == 0) goto L_0x004e
            boolean r5 = r11.moveToFirst()     // Catch:{ Exception -> 0x005d }
            if (r5 != 0) goto L_0x0023
            goto L_0x004e
        L_0x0023:
            int r2 = r11.getColumnIndex(r2)     // Catch:{ Exception -> 0x005d }
            java.lang.String r2 = r11.getString(r2)     // Catch:{ Exception -> 0x005d }
            int r1 = r11.getColumnIndex(r1)     // Catch:{ Exception -> 0x005d }
            java.lang.String r1 = r11.getString(r1)     // Catch:{ Exception -> 0x005d }
            int r0 = r11.getColumnIndex(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.String r0 = r11.getString(r0)     // Catch:{ Exception -> 0x005d }
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r0)     // Catch:{ Exception -> 0x005d }
            com.facebook.ads.internal.c.c$a r5 = new com.facebook.ads.internal.c.c$a     // Catch:{ Exception -> 0x005d }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x005d }
            r5.<init>(r2, r1, r0)     // Catch:{ Exception -> 0x005d }
            if (r11 == 0) goto L_0x004d
            r11.close()
        L_0x004d:
            return r5
        L_0x004e:
            com.facebook.ads.internal.c.c$a r0 = new com.facebook.ads.internal.c.c$a     // Catch:{ Exception -> 0x005d }
            r0.<init>(r4, r4, r3)     // Catch:{ Exception -> 0x005d }
            if (r11 == 0) goto L_0x0058
            r11.close()
        L_0x0058:
            return r0
        L_0x0059:
            r0 = move-exception
            r11 = r4
            goto L_0x0069
        L_0x005c:
            r11 = r4
        L_0x005d:
            com.facebook.ads.internal.c.c$a r0 = new com.facebook.ads.internal.c.c$a     // Catch:{ all -> 0x0068 }
            r0.<init>(r4, r4, r3)     // Catch:{ all -> 0x0068 }
            if (r11 == 0) goto L_0x0067
            r11.close()
        L_0x0067:
            return r0
        L_0x0068:
            r0 = move-exception
        L_0x0069:
            if (r11 == 0) goto L_0x006e
            r11.close()
        L_0x006e:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.c.c.a(android.content.ContentResolver):com.facebook.ads.internal.c.c$a");
    }
}
