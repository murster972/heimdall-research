package com.facebook.ads.internal.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.facebook.ads.internal.c.a;
import com.facebook.ads.internal.c.c;

public class b {

    /* renamed from: a  reason: collision with root package name */
    public static String f2684a = "";
    public static String b = "";
    public static boolean c = false;
    public static String d = "";

    public static void a(Context context) {
        c.a aVar;
        try {
            SharedPreferences sharedPreferences = context.getSharedPreferences("SDKIDFA", 0);
            if (sharedPreferences.contains("attributionId")) {
                f2684a = sharedPreferences.getString("attributionId", "");
            }
            if (sharedPreferences.contains("advertisingId")) {
                b = sharedPreferences.getString("advertisingId", "");
                c = sharedPreferences.getBoolean("limitAdTracking", c);
                d = a.c.SHARED_PREFS.name();
            }
            a aVar2 = null;
            try {
                aVar = c.a(context.getContentResolver());
            } catch (Exception e) {
                com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(e, "Error retrieving attribution id from fb4a"));
                aVar = null;
            }
            if (!(aVar == null || aVar.f2685a == null)) {
                f2684a = aVar.f2685a;
            }
            if (com.facebook.ads.internal.q.a.b.a() && com.facebook.ads.internal.q.a.b.b("aid_override")) {
                f2684a = com.facebook.ads.internal.q.a.b.a("aid_override");
            }
            try {
                aVar2 = a.a(context, aVar);
            } catch (Exception e2) {
                com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(e2, "Error retrieving advertising id from Google Play Services"));
            }
            if (aVar2 != null) {
                String a2 = aVar2.a();
                Boolean valueOf = Boolean.valueOf(aVar2.b());
                if (a2 != null) {
                    b = a2;
                    c = valueOf.booleanValue();
                    d = aVar2.c().name();
                }
            }
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("attributionId", f2684a);
            edit.putString("advertisingId", b);
            edit.putBoolean("limitAdTracking", c);
            edit.apply();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }
}
