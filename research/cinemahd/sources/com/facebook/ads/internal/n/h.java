package com.facebook.ads.internal.n;

import com.vungle.warren.model.ReportDBAdapter;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private final String f2754a;
    private final int b;
    private final int c;

    public h(String str, int i, int i2) {
        this.f2754a = str;
        this.b = i;
        this.c = i2;
    }

    public static h a(JSONObject jSONObject) {
        String optString;
        if (jSONObject == null || (optString = jSONObject.optString(ReportDBAdapter.ReportColumns.COLUMN_URL)) == null) {
            return null;
        }
        return new h(optString, jSONObject.optInt("width", 0), jSONObject.optInt("height", 0));
    }

    public String a() {
        return this.f2754a;
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }
}
