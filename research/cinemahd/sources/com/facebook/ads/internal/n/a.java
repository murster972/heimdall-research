package com.facebook.ads.internal.n;

import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.facebook.ads.AdIconView;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.view.component.c;
import com.facebook.ads.internal.view.component.g;
import java.util.ArrayList;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final float f2737a = x.b;
    private final k b;
    private final f c;
    private final RelativeLayout d;
    private ArrayList<View> e = new ArrayList<>();

    /* renamed from: com.facebook.ads.internal.n.a$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2738a = new int[l.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.facebook.ads.internal.n.l[] r0 = com.facebook.ads.internal.n.l.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2738a = r0
                int[] r0 = f2738a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.n.l r1 = com.facebook.ads.internal.n.l.HEIGHT_400     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2738a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.n.l r1 = com.facebook.ads.internal.n.l.HEIGHT_300     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2738a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.n.l r1 = com.facebook.ads.internal.n.l.HEIGHT_100     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2738a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.n.l r1 = com.facebook.ads.internal.n.l.HEIGHT_120     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.n.a.AnonymousClass1.<clinit>():void");
        }
    }

    /* JADX WARNING: type inference failed for: r8v0, types: [com.facebook.ads.AdIconView] */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0082, code lost:
        if (r0 != 2) goto L_0x008b;
     */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public a(android.content.Context r3, com.facebook.ads.internal.n.f r4, android.widget.RelativeLayout r5, android.widget.RelativeLayout r6, com.facebook.ads.MediaView r7, com.facebook.ads.AdIconView r8, com.facebook.ads.internal.n.l r9, com.facebook.ads.internal.n.k r10) {
        /*
            r2 = this;
            r2.<init>()
            r2.d = r5
            android.widget.RelativeLayout r5 = r2.d
            int r0 = r10.b()
            com.facebook.ads.internal.q.a.x.a((android.view.View) r5, (int) r0)
            r2.b = r10
            r2.c = r4
            int r5 = r9.b()
            android.widget.RelativeLayout r10 = r2.d
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            float r5 = (float) r5
            float r1 = f2737a
            float r5 = r5 * r1
            int r5 = java.lang.Math.round(r5)
            r1 = -1
            r0.<init>(r1, r5)
            r10.setLayoutParams(r0)
            com.facebook.ads.internal.view.r r5 = new com.facebook.ads.internal.view.r
            r5.<init>(r3)
            float r10 = f2737a
            r0 = 1133248512(0x438c0000, float:280.0)
            float r10 = r10 * r0
            int r10 = java.lang.Math.round(r10)
            r5.setMinWidth(r10)
            float r10 = f2737a
            r0 = 1136361472(0x43bb8000, float:375.0)
            float r10 = r10 * r0
            int r10 = java.lang.Math.round(r10)
            r5.setMaxWidth(r10)
            android.widget.RelativeLayout$LayoutParams r10 = new android.widget.RelativeLayout$LayoutParams
            r10.<init>(r1, r1)
            r0 = 13
            r10.addRule(r0, r1)
            r5.setLayoutParams(r10)
            android.widget.RelativeLayout r10 = r2.d
            r10.addView(r5)
            android.widget.LinearLayout r10 = new android.widget.LinearLayout
            r10.<init>(r3)
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            r2.e = r3
            r3 = 1
            r10.setOrientation(r3)
            android.widget.RelativeLayout$LayoutParams r0 = new android.widget.RelativeLayout$LayoutParams
            r0.<init>(r1, r1)
            r10.setLayoutParams(r0)
            r5.addView(r10)
            int[] r0 = com.facebook.ads.internal.n.a.AnonymousClass1.f2738a
            int r1 = r9.ordinal()
            r0 = r0[r1]
            if (r0 == r3) goto L_0x0085
            r3 = 2
            if (r0 == r3) goto L_0x0088
            goto L_0x008b
        L_0x0085:
            r2.a((android.view.ViewGroup) r10)
        L_0x0088:
            r2.a(r10, r7)
        L_0x008b:
            r2.a(r10, r9, r8)
            android.widget.RelativeLayout r3 = r2.d
            if (r7 == 0) goto L_0x0093
            goto L_0x0094
        L_0x0093:
            r7 = r8
        L_0x0094:
            java.util.ArrayList<android.view.View> r8 = r2.e
            r4.a((android.view.View) r3, (com.facebook.ads.internal.n.g) r7, (java.util.List<android.view.View>) r8)
            android.view.ViewGroup$LayoutParams r3 = r6.getLayoutParams()
            android.widget.RelativeLayout$LayoutParams r3 = (android.widget.RelativeLayout.LayoutParams) r3
            r4 = 11
            r3.addRule(r4)
            float r4 = f2737a
            r7 = 1082130432(0x40800000, float:4.0)
            float r4 = r4 * r7
            int r4 = java.lang.Math.round(r4)
            float r8 = f2737a
            float r8 = r8 * r7
            int r8 = java.lang.Math.round(r8)
            float r9 = f2737a
            float r9 = r9 * r7
            int r9 = java.lang.Math.round(r9)
            float r10 = f2737a
            float r10 = r10 * r7
            int r7 = java.lang.Math.round(r10)
            r3.setMargins(r4, r8, r9, r7)
            r5.addView(r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.n.a.<init>(android.content.Context, com.facebook.ads.internal.n.f, android.widget.RelativeLayout, android.widget.RelativeLayout, com.facebook.ads.MediaView, com.facebook.ads.AdIconView, com.facebook.ads.internal.n.l, com.facebook.ads.internal.n.k):void");
    }

    private void a(ViewGroup viewGroup) {
        g gVar = new g(this.d.getContext(), this.c, this.b);
        gVar.setLayoutParams(new LinearLayout.LayoutParams(-1, Math.round(f2737a * 110.0f)));
        viewGroup.addView(gVar);
    }

    private void a(ViewGroup viewGroup, RelativeLayout relativeLayout) {
        RelativeLayout relativeLayout2 = new RelativeLayout(this.d.getContext());
        relativeLayout2.setLayoutParams(new LinearLayout.LayoutParams(-1, Math.round(f2737a * 180.0f)));
        x.a((View) relativeLayout2, this.b.b());
        relativeLayout2.addView(relativeLayout);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, (int) (f2737a * 180.0f));
        layoutParams.addRule(13, -1);
        relativeLayout.setLayoutParams(layoutParams);
        viewGroup.addView(relativeLayout2);
        this.e.add(relativeLayout);
    }

    private void a(ViewGroup viewGroup, l lVar, AdIconView adIconView) {
        c cVar = new c(this.d.getContext(), this.c, this.b, adIconView, a(lVar), b(lVar));
        cVar.setLayoutParams(new LinearLayout.LayoutParams(-1, Math.round(((float) b(lVar)) * f2737a)));
        viewGroup.addView(cVar);
        this.e.add(cVar.getIconView());
        this.e.add(cVar.getCallToActionView());
    }

    private boolean a(l lVar) {
        return lVar == l.HEIGHT_300 || lVar == l.HEIGHT_120;
    }

    private int b(l lVar) {
        int i = AnonymousClass1.f2738a[lVar.ordinal()];
        if (i == 1) {
            return (lVar.b() - 180) / 2;
        }
        if (i == 2) {
            return lVar.b() - 180;
        }
        if (i == 3 || i == 4) {
            return lVar.b();
        }
        return 0;
    }

    public void a() {
        this.c.J();
    }
}
