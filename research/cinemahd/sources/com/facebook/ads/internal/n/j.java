package com.facebook.ads.internal.n;

import org.json.JSONObject;

public class j {

    /* renamed from: a  reason: collision with root package name */
    private final double f2755a;
    private final double b;

    public j(double d, double d2) {
        this.f2755a = d;
        this.b = d2;
    }

    public static j a(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        double optDouble = jSONObject.optDouble("value", 0.0d);
        double optDouble2 = jSONObject.optDouble("scale", 0.0d);
        if (optDouble == 0.0d || optDouble2 == 0.0d) {
            return null;
        }
        return new j(optDouble, optDouble2);
    }

    public double a() {
        return this.f2755a;
    }

    public double b() {
        return this.b;
    }
}
