package com.facebook.ads.internal.n;

public enum e {
    NONE(0),
    ALL(1);
    
    private final long c;

    private e(long j) {
        this.c = j;
    }

    public long a() {
        return this.c;
    }
}
