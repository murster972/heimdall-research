package com.facebook.ads.internal.adapters.a;

import java.io.Serializable;

public class j implements Serializable {
    private static final long serialVersionUID = -5352540123250859603L;

    /* renamed from: a  reason: collision with root package name */
    private final String f2623a;
    private final int b;
    private final String c;

    j(String str, int i, String str2) {
        this.f2623a = str;
        this.b = i;
        this.c = str2;
    }

    public String a() {
        return this.f2623a;
    }

    public int b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }
}
