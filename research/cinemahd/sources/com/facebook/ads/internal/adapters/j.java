package com.facebook.ads.internal.adapters;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RelativeLayout;
import com.facebook.ads.AdError;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.p;
import com.facebook.ads.internal.view.f.a;
import com.facebook.ads.internal.view.f.b.b;
import com.facebook.ads.internal.view.f.b.d;
import com.facebook.ads.internal.view.f.b.l;
import com.facebook.ads.internal.view.f.c.d;
import com.facebook.ads.internal.view.f.c.e;
import com.facebook.ads.internal.view.f.c.i;
import com.facebook.ads.internal.view.f.c.k;
import com.facebook.common.util.UriUtil;
import com.original.tase.model.socket.UserResponces;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class j extends s implements p<Bundle> {
    static final /* synthetic */ boolean e = (!j.class.desiredAssertionStatus());

    /* renamed from: a  reason: collision with root package name */
    protected c f2644a;
    protected a b;
    protected JSONObject c;
    protected Context d;
    private final f<b> f = new f<b>() {
        public Class<b> a() {
            return b.class;
        }

        public void a(b bVar) {
            if (j.this.j != null) {
                j.this.j.d(j.this);
            }
        }
    };
    private final f<l> g = new f<l>() {
        public Class<l> a() {
            return l.class;
        }

        public void a(l lVar) {
            boolean unused = j.this.l = true;
            if (j.this.j != null) {
                j.this.j.a(j.this);
            }
        }
    };
    private final f<d> h = new f<d>() {
        public Class<d> a() {
            return d.class;
        }

        public void a(d dVar) {
            if (j.this.j != null) {
                j.this.j.a((s) j.this, AdError.INTERNAL_ERROR);
            }
        }
    };
    private final f<com.facebook.ads.internal.view.f.b.a> i = new f<com.facebook.ads.internal.view.f.b.a>() {
        public Class<com.facebook.ads.internal.view.f.b.a> a() {
            return com.facebook.ads.internal.view.f.b.a.class;
        }

        public void a(com.facebook.ads.internal.view.f.b.a aVar) {
            if (j.this.j != null) {
                j.this.j.b(j.this);
            }
        }
    };
    /* access modifiers changed from: private */
    public com.facebook.ads.a.a j;
    private String k;
    /* access modifiers changed from: private */
    public boolean l = false;
    private com.facebook.ads.internal.view.f.c m;
    private String n;
    private boolean o = false;
    private com.facebook.ads.internal.d.b p;

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00b9, code lost:
        if (r13.isNull(r0) == false) goto L_0x00be;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(android.content.Context r17, com.facebook.ads.a.a r18, org.json.JSONObject r19, com.facebook.ads.internal.m.c r20, android.os.Bundle r21, java.util.EnumSet<com.facebook.ads.CacheFlag> r22, int r23) {
        /*
            r16 = this;
            r9 = r16
            r10 = r17
            r0 = r19
            r11 = r21
            r9.d = r10
            r1 = r18
            r9.j = r1
            r12 = r20
            r9.f2644a = r12
            r9.c = r0
            r1 = 0
            r9.l = r1
            java.lang.String r2 = "video"
            org.json.JSONObject r13 = r0.getJSONObject(r2)
            java.lang.String r2 = "ct"
            java.lang.String r0 = r0.optString(r2)
            r9.n = r0
            com.facebook.ads.internal.view.f.a r0 = new com.facebook.ads.internal.view.f.a
            r0.<init>(r10)
            r9.b = r0
            com.facebook.ads.internal.view.f.a r0 = r9.b
            r2 = r23
            r0.setVideoProgressReportIntervalMs(r2)
            r16.a()
            com.facebook.ads.internal.view.f.a r0 = r9.b
            com.facebook.ads.internal.j.e r0 = r0.getEventBus()
            r2 = 4
            com.facebook.ads.internal.j.f[] r2 = new com.facebook.ads.internal.j.f[r2]
            com.facebook.ads.internal.j.f<com.facebook.ads.internal.view.f.b.b> r3 = r9.f
            r2[r1] = r3
            com.facebook.ads.internal.j.f<com.facebook.ads.internal.view.f.b.l> r1 = r9.g
            r3 = 1
            r2[r3] = r1
            com.facebook.ads.internal.j.f<com.facebook.ads.internal.view.f.b.d> r1 = r9.h
            r3 = 2
            r2[r3] = r1
            com.facebook.ads.internal.j.f<com.facebook.ads.internal.view.f.b.a> r1 = r9.i
            r3 = 3
            r2[r3] = r1
            r0.a((T[]) r2)
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            com.facebook.ads.internal.adapters.j$5 r15 = new com.facebook.ads.internal.adapters.j$5
            r2 = 4502148214488346440(0x3e7ad7f29abcaf48, double:1.0E-7)
            r4 = -4616189618054758400(0xbff0000000000000, double:-1.0)
            r6 = 4562254508917369340(0x3f50624dd2f1a9fc, double:0.001)
            r8 = 0
            r0 = r15
            r1 = r16
            r0.<init>(r2, r4, r6, r8)
            r14.add(r15)
            if (r11 == 0) goto L_0x008d
            com.facebook.ads.internal.view.f.b r8 = new com.facebook.ads.internal.view.f.b
            com.facebook.ads.internal.view.f.a r3 = r9.b
            java.lang.String r5 = r9.n
            java.lang.String r0 = "logger"
            android.os.Bundle r6 = r11.getBundle(r0)
            r7 = 0
            r0 = r8
            r1 = r17
            r2 = r20
            r4 = r14
            r0.<init>(r1, r2, r3, r4, r5, r6, r7)
            r9.m = r8
            goto L_0x009e
        L_0x008d:
            com.facebook.ads.internal.view.f.b r6 = new com.facebook.ads.internal.view.f.b
            com.facebook.ads.internal.view.f.a r3 = r9.b
            java.lang.String r5 = r9.n
            r0 = r6
            r1 = r17
            r2 = r20
            r4 = r14
            r0.<init>((android.content.Context) r1, (com.facebook.ads.internal.m.c) r2, (com.facebook.ads.internal.view.f.a) r3, (java.util.List<com.facebook.ads.internal.b.b>) r4, (java.lang.String) r5)
            r9.m = r6
        L_0x009e:
            com.facebook.ads.a.a r0 = r9.j
            com.facebook.ads.internal.view.f.a r1 = r9.b
            r0.a((com.facebook.ads.internal.adapters.s) r9, (android.view.View) r1)
            com.facebook.ads.internal.q.a.s$a r0 = com.facebook.ads.internal.q.a.s.a(r17)
            com.facebook.ads.internal.q.a.s$a r1 = com.facebook.ads.internal.q.a.s.a.MOBILE_INTERNET
            if (r0 != r1) goto L_0x00bc
            java.lang.String r0 = "videoHDURL"
            boolean r1 = r13.has(r0)
            if (r1 == 0) goto L_0x00bc
            boolean r1 = r13.isNull(r0)
            if (r1 != 0) goto L_0x00bc
            goto L_0x00be
        L_0x00bc:
            java.lang.String r0 = "videoURL"
        L_0x00be:
            java.lang.String r0 = r13.getString(r0)
            r9.k = r0
            com.facebook.ads.CacheFlag r0 = com.facebook.ads.CacheFlag.VIDEO
            r1 = r22
            boolean r0 = r1.contains(r0)
            if (r0 == 0) goto L_0x00e7
            com.facebook.ads.internal.d.b r0 = new com.facebook.ads.internal.d.b
            r0.<init>(r10)
            r9.p = r0
            com.facebook.ads.internal.d.b r0 = r9.p
            java.lang.String r1 = r9.k
            r0.a((java.lang.String) r1)
            com.facebook.ads.internal.d.b r0 = r9.p
            com.facebook.ads.internal.adapters.j$6 r1 = new com.facebook.ads.internal.adapters.j$6
            r1.<init>()
            r0.a((com.facebook.ads.internal.d.a) r1)
            goto L_0x00f0
        L_0x00e7:
            com.facebook.ads.internal.view.f.a r0 = r9.b
            java.lang.String r1 = r16.h()
            r0.setVideoURI((java.lang.String) r1)
        L_0x00f0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.adapters.j.a(android.content.Context, com.facebook.ads.a.a, org.json.JSONObject, com.facebook.ads.internal.m.c, android.os.Bundle, java.util.EnumSet, int):void");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0004, code lost:
        r1 = r2.k;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String h() {
        /*
            r2 = this;
            com.facebook.ads.internal.d.b r0 = r2.p
            if (r0 == 0) goto L_0x000d
            java.lang.String r1 = r2.k
            if (r1 == 0) goto L_0x000d
            java.lang.String r0 = r0.b((java.lang.String) r1)
            goto L_0x000f
        L_0x000d:
            java.lang.String r0 = ""
        L_0x000f:
            boolean r1 = android.text.TextUtils.isEmpty(r0)
            if (r1 == 0) goto L_0x0017
            java.lang.String r0 = r2.k
        L_0x0017:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.adapters.j.h():java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void a() {
        if (!e && this.d == null) {
            throw new AssertionError();
        } else if (e || this.c != null) {
            JSONObject optJSONObject = this.c.optJSONObject("text");
            if (optJSONObject == null) {
                optJSONObject = new JSONObject();
            }
            this.b.a((com.facebook.ads.internal.view.f.a.b) new k(this.d));
            com.facebook.ads.internal.view.f.c.l lVar = new com.facebook.ads.internal.view.f.c.l(this.d);
            this.b.a((com.facebook.ads.internal.view.f.a.b) lVar);
            this.b.a((com.facebook.ads.internal.view.f.a.b) new com.facebook.ads.internal.view.f.c.d(lVar, d.a.INVSIBLE));
            this.b.a((com.facebook.ads.internal.view.f.a.b) new com.facebook.ads.internal.view.f.c.b(this.d));
            String b2 = b();
            if (b2 != null) {
                com.facebook.ads.internal.view.f.c.c cVar = new com.facebook.ads.internal.view.f.c.c(this.d, b2);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams.addRule(12);
                layoutParams.addRule(9);
                cVar.setLayoutParams(layoutParams);
                cVar.setCountdownTextColor(-1);
                this.b.a((com.facebook.ads.internal.view.f.a.b) cVar);
            }
            if (this.c.has("cta") && !this.c.isNull("cta")) {
                JSONObject jSONObject = this.c.getJSONObject("cta");
                e eVar = new e(this.d, jSONObject.getString(ReportDBAdapter.ReportColumns.COLUMN_URL), this.f2644a, this.n, jSONObject.getString("text"));
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams2.addRule(10);
                layoutParams2.addRule(11);
                eVar.setLayoutParams(layoutParams2);
                this.b.a((com.facebook.ads.internal.view.f.a.b) eVar);
            }
            String d2 = d();
            if (!TextUtils.isEmpty(d2)) {
                this.b.a((com.facebook.ads.internal.view.f.a.b) new com.facebook.ads.internal.view.f.c.a(this.d, d2, this.n, new float[]{0.0f, 0.0f, 8.0f, 0.0f}));
            }
            int c2 = c();
            if (c2 > 0) {
                i iVar = new i(this.d, c2, optJSONObject.optString("skipAdIn", "Skip Ad in"), optJSONObject.optString("skipAd", "Skip Ad"));
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(12);
                layoutParams3.addRule(11);
                iVar.setLayoutParams(layoutParams3);
                iVar.setPadding(0, 0, 0, 30);
                this.b.a((com.facebook.ads.internal.view.f.a.b) iVar);
            }
        } else {
            throw new AssertionError();
        }
    }

    public final void a(Context context, com.facebook.ads.a.a aVar, c cVar, Bundle bundle, EnumSet<CacheFlag> enumSet) {
        try {
            JSONObject jSONObject = new JSONObject(bundle.getString("ad_response"));
            a(context, aVar, jSONObject, cVar, bundle, enumSet, jSONObject.optInt("video_time_polling_interval", UserResponces.USER_RESPONCE_SUCCSES));
        } catch (JSONException unused) {
            aVar.a((s) this, AdError.INTERNAL_ERROR);
        }
    }

    public final void a(Context context, com.facebook.ads.a.a aVar, Map<String, Object> map, c cVar, EnumSet<CacheFlag> enumSet) {
        try {
            JSONObject jSONObject = (JSONObject) map.get(UriUtil.DATA_SCHEME);
            com.facebook.ads.internal.h.d dVar = (com.facebook.ads.internal.h.d) map.get("definition");
            a(context, aVar, jSONObject, cVar, (Bundle) null, enumSet, dVar == null ? UserResponces.USER_RESPONCE_SUCCSES : dVar.k());
        } catch (JSONException unused) {
            aVar.a((s) this, AdError.INTERNAL_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public String b() {
        if (e || this.c != null) {
            try {
                JSONObject jSONObject = this.c.getJSONObject("capabilities");
                if (jSONObject.has(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_COUNTDOWN)) {
                    if (!jSONObject.isNull(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_COUNTDOWN)) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_COUNTDOWN);
                        if (jSONObject2.has("format")) {
                            return jSONObject2.optString("format");
                        }
                    }
                }
                return null;
            } catch (Exception e2) {
                Log.w(String.valueOf(j.class), "Invalid JSON", e2);
                return null;
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    public int c() {
        if (e || this.c != null) {
            try {
                JSONObject jSONObject = this.c.getJSONObject("capabilities");
                if (jSONObject.has("skipButton")) {
                    if (!jSONObject.isNull("skipButton")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("skipButton");
                        if (jSONObject2.has("skippableSeconds")) {
                            return jSONObject2.getInt("skippableSeconds");
                        }
                    }
                }
                return -1;
            } catch (Exception e2) {
                Log.w(String.valueOf(j.class), "Invalid JSON", e2);
                return -1;
            }
        } else {
            throw new AssertionError();
        }
    }

    /* access modifiers changed from: protected */
    public String d() {
        if (e || this.c != null) {
            try {
                JSONObject jSONObject = this.c.getJSONObject("capabilities");
                if (jSONObject.has("adChoices")) {
                    if (!jSONObject.isNull("adChoices")) {
                        JSONObject jSONObject2 = jSONObject.getJSONObject("adChoices");
                        if (jSONObject2.has(ReportDBAdapter.ReportColumns.COLUMN_URL)) {
                            return jSONObject2.getString(ReportDBAdapter.ReportColumns.COLUMN_URL);
                        }
                    }
                }
                return null;
            } catch (Exception e2) {
                Log.w(String.valueOf(j.class), "Invalid JSON", e2);
                return null;
            }
        } else {
            throw new AssertionError();
        }
    }

    public boolean e() {
        if (!this.l || this.b == null) {
            return false;
        }
        if (this.m.j() > 0) {
            this.b.a(this.m.j());
        }
        this.b.a(com.facebook.ads.internal.view.f.a.a.AUTO_STARTED);
        return true;
    }

    /* access modifiers changed from: protected */
    public void f() {
        c cVar = this.f2644a;
        if (cVar != null && !this.o) {
            this.o = true;
            cVar.a(this.n, new HashMap());
            com.facebook.ads.a.a aVar = this.j;
            if (aVar != null) {
                aVar.c(this);
            }
        }
    }

    public Bundle g() {
        if (this.m == null || this.c == null) {
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putBundle("logger", this.m.g());
        bundle.putString("ad_response", this.c.toString());
        return bundle;
    }

    public void onDestroy() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.f();
            this.b.k();
        }
        this.j = null;
        this.f2644a = null;
        this.k = null;
        this.l = false;
        this.n = null;
        this.b = null;
        this.m = null;
        this.c = null;
        this.d = null;
        this.o = false;
    }
}
