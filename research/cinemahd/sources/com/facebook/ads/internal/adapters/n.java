package com.facebook.ads.internal.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.a.k;
import com.facebook.ads.internal.d.b;
import com.facebook.ads.internal.h.d;
import com.facebook.ads.internal.l.a;
import com.facebook.common.util.UriUtil;
import java.util.Map;
import java.util.UUID;
import org.json.JSONObject;

public class n extends ab {
    private static final String c = "n";
    private static final int[] d = {-1, -6, -7, -8};
    private final String e = UUID.randomUUID().toString();
    private Context f;
    /* access modifiers changed from: private */
    public ac g;
    /* access modifiers changed from: private */
    public boolean h = false;
    private String i;
    private String j;
    private long k;
    /* access modifiers changed from: private */
    public k l;
    private ad m;

    private void a(Context context, final boolean z) {
        if (a.f(context)) {
            Log.d(c, "Playable Ads pre-caching is disabled.");
            this.h = true;
            this.g.a(this);
            return;
        }
        WebView webView = new WebView(context);
        webView.getSettings().setCacheMode(1);
        webView.setWebViewClient(new WebViewClient() {

            /* renamed from: a  reason: collision with root package name */
            boolean f2661a = false;

            private void a() {
                boolean unused = n.this.h = true;
                n.this.g.a(n.this);
            }

            /* access modifiers changed from: private */
            public void a(WebResourceError webResourceError) {
                if (z || !n.this.a(webResourceError)) {
                    n.this.g.a(n.this, AdError.CACHE_ERROR);
                } else {
                    a();
                }
            }

            public void onPageFinished(WebView webView, String str) {
                this.f2661a = true;
                a();
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                super.onPageStarted(webView, str, bitmap);
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        AnonymousClass1 r0 = AnonymousClass1.this;
                        if (!r0.f2661a) {
                            r0.a((WebResourceError) null);
                        }
                    }
                }, 10000);
            }

            public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
                this.f2661a = true;
                a(webResourceError);
            }
        });
        webView.loadUrl(this.l.e().j().a());
    }

    /* access modifiers changed from: private */
    public boolean a(WebResourceError webResourceError) {
        if (webResourceError == null || Build.VERSION.SDK_INT < 23) {
            return true;
        }
        for (int i2 : d) {
            if (webResourceError.getErrorCode() == i2) {
                return true;
            }
        }
        return false;
    }

    private void b(Context context, final boolean z) {
        final b bVar = new b(context);
        bVar.a(this.l.e().a());
        bVar.a(this.l.e().g(), this.l.e().i(), this.l.e().h());
        bVar.a(this.l.a().b(), -1, -1);
        for (String a2 : this.l.f().d()) {
            bVar.a(a2, -1, -1);
        }
        bVar.a((com.facebook.ads.internal.d.a) new com.facebook.ads.internal.d.a() {
            private void c() {
                boolean unused = n.this.h = true;
                n.this.g.a(n.this);
                n.this.l.b(bVar.b(n.this.l.e().a()));
            }

            public void a() {
                c();
            }

            public void b() {
                if (z) {
                    n.this.g.a(n.this, AdError.CACHE_ERROR);
                } else {
                    c();
                }
            }
        });
    }

    private boolean c() {
        return this.l.e().j() != null;
    }

    private void d() {
        LocalBroadcastManager a2 = LocalBroadcastManager.a(this.f);
        ad adVar = this.m;
        a2.a(adVar, adVar.a());
    }

    private void e() {
        if (this.m != null) {
            try {
                LocalBroadcastManager.a(this.f).a((BroadcastReceiver) this.m);
            } catch (Exception unused) {
            }
        }
    }

    private String f() {
        String str;
        if (this.f2625a == null) {
            return null;
        }
        String urlPrefix = AdSettings.getUrlPrefix();
        if (urlPrefix == null || urlPrefix.isEmpty()) {
            str = "https://www.facebook.com/audience_network/server_side_reward";
        } else {
            str = String.format("https://www.%s.facebook.com/audience_network/server_side_reward", new Object[]{urlPrefix});
        }
        Uri parse = Uri.parse(str);
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(parse.getScheme());
        builder.authority(parse.getAuthority());
        builder.path(parse.getPath());
        builder.query(parse.getQuery());
        builder.fragment(parse.getFragment());
        builder.appendQueryParameter("puid", this.f2625a.getUserID());
        builder.appendQueryParameter("pc", this.f2625a.getCurrency());
        builder.appendQueryParameter("ptid", this.e);
        builder.appendQueryParameter("appid", this.i);
        return builder.build().toString();
    }

    public int a() {
        k kVar = this.l;
        if (kVar == null) {
            return -1;
        }
        return kVar.e().d();
    }

    public void a(Context context, ac acVar, Map<String, Object> map, boolean z) {
        this.f = context;
        this.g = acVar;
        this.h = false;
        this.j = (String) map.get("placementId");
        this.k = ((Long) map.get(AudienceNetworkActivity.REQUEST_TIME)).longValue();
        int k2 = ((d) map.get("definition")).k();
        String str = this.j;
        this.i = str != null ? str.split("_")[0] : "";
        this.l = k.a((JSONObject) map.get(UriUtil.DATA_SCHEME));
        this.l.a(k2);
        if (!TextUtils.isEmpty(this.l.e().a()) || c()) {
            this.m = new ad(this.e, this, acVar);
            d();
            if (c()) {
                a(context, z);
            } else {
                b(context, z);
            }
        } else {
            this.g.a(this, AdError.INTERNAL_ERROR);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x006b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean b() {
        /*
            r6 = this;
            boolean r0 = r6.h
            r1 = 0
            if (r0 != 0) goto L_0x0006
            return r1
        L_0x0006:
            java.lang.String r0 = r6.f()
            com.facebook.ads.internal.adapters.a.k r2 = r6.l
            r2.a((java.lang.String) r0)
            android.content.Intent r2 = new android.content.Intent
            android.content.Context r3 = r6.f
            java.lang.Class<com.facebook.ads.AudienceNetworkActivity> r4 = com.facebook.ads.AudienceNetworkActivity.class
            r2.<init>(r3, r4)
            com.facebook.ads.internal.settings.a$a r3 = com.facebook.ads.internal.settings.a.C0025a.REWARDED_VIDEO
            java.lang.String r4 = "viewType"
            r2.putExtra(r4, r3)
            com.facebook.ads.internal.adapters.a.k r3 = r6.l
            java.lang.String r4 = "rewardedVideoAdDataBundle"
            r2.putExtra(r4, r3)
            java.lang.String r3 = r6.e
            java.lang.String r4 = "uniqueId"
            r2.putExtra(r4, r3)
            java.lang.String r3 = "rewardServerURL"
            r2.putExtra(r3, r0)
            java.lang.String r0 = r6.j
            java.lang.String r3 = "placementId"
            r2.putExtra(r3, r0)
            long r3 = r6.k
            java.lang.String r0 = "requestTime"
            r2.putExtra(r0, r3)
            int r0 = r6.b
            r3 = -1
            java.lang.String r4 = "predefinedOrientationKey"
            r5 = 1
            if (r0 == r3) goto L_0x0059
            android.content.Context r0 = r6.f
            android.content.ContentResolver r0 = r0.getContentResolver()
            java.lang.String r3 = "accelerometer_rotation"
            int r0 = android.provider.Settings.System.getInt(r0, r3, r1)
            if (r0 == r5) goto L_0x0059
            int r0 = r6.b
            goto L_0x0062
        L_0x0059:
            android.content.Context r0 = r6.f
            boolean r0 = com.facebook.ads.internal.l.a.o(r0)
            if (r0 != 0) goto L_0x0065
            r0 = 6
        L_0x0062:
            r2.putExtra(r4, r0)
        L_0x0065:
            android.content.Context r0 = r6.f
            boolean r0 = r0 instanceof android.app.Activity
            if (r0 != 0) goto L_0x0075
            int r0 = r2.getFlags()
            r1 = 268435456(0x10000000, float:2.5243549E-29)
            r0 = r0 | r1
            r2.setFlags(r0)
        L_0x0075:
            android.content.Context r0 = r6.f
            r0.startActivity(r2)
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.adapters.n.b():boolean");
    }

    public void onDestroy() {
        e();
    }
}
