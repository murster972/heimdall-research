package com.facebook.ads.internal.adapters.a;

import java.io.Serializable;

public class e implements Serializable {
    private static final long serialVersionUID = -4041915335826065133L;

    /* renamed from: a  reason: collision with root package name */
    private final String f2617a;
    private final String b;

    e(String str, String str2) {
        this.f2617a = a(str);
        this.b = a(str2);
    }

    private static String a(String str) {
        return "null".equalsIgnoreCase(str) ? "" : str;
    }

    public String a() {
        return this.f2617a;
    }

    public String b() {
        return this.b;
    }
}
