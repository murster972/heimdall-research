package com.facebook.ads.internal.adapters.a;

import java.io.Serializable;

public class i implements Serializable {
    private static final long serialVersionUID = 351643298236575728L;

    /* renamed from: a  reason: collision with root package name */
    private final String f2621a;
    private final String b;
    private final String c;
    private final String d;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f2622a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public String d;

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.f2622a = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public i a() {
            return new i(this);
        }

        /* access modifiers changed from: package-private */
        public a b(String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a c(String str) {
            this.c = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a d(String str) {
            this.d = str;
            return this;
        }
    }

    private i(a aVar) {
        this.f2621a = aVar.f2622a;
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
    }

    public String a() {
        return this.f2621a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public String d() {
        return this.d;
    }
}
