package com.facebook.ads.internal.adapters.a;

import java.io.Serializable;

public class c implements Serializable {
    private static final long serialVersionUID = 5306126965868117466L;

    /* renamed from: a  reason: collision with root package name */
    private final String f2614a;
    private final String b;
    private final String c;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f2615a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public String c;

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.f2615a = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public c a() {
            return new c(this);
        }

        /* access modifiers changed from: package-private */
        public a b(String str) {
            this.b = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a c(String str) {
            this.c = str;
            return this;
        }
    }

    private c(a aVar) {
        this.f2614a = aVar.f2615a;
        this.b = aVar.b;
        this.c = aVar.c;
    }

    public String a() {
        return this.f2614a;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }
}
