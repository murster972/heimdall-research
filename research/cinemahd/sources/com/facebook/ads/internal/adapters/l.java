package com.facebook.ads.internal.adapters;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.a.c;
import com.facebook.ads.internal.adapters.k;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.f.a.b;
import com.facebook.ads.internal.view.f.b.t;
import com.facebook.ads.internal.view.f.c.a;
import com.facebook.ads.internal.view.f.c.d;
import com.facebook.ads.internal.view.f.c.g;
import com.facebook.ads.internal.view.f.c.j;
import com.facebook.ads.internal.view.f.c.n;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.HashMap;
import org.json.JSONObject;

public class l extends j implements View.OnTouchListener, a {
    static final /* synthetic */ boolean i = (!l.class.desiredAssertionStatus());
    private static final String j = l.class.getSimpleName();
    private int A = -12286980;
    private boolean B = false;
    private com.facebook.ads.internal.view.f.a.a C;
    final int f = 64;
    final int g = 64;
    final int h = 16;
    private a.C0026a k;
    /* access modifiers changed from: private */
    public Activity l;
    private AudienceNetworkActivity.BackButtonInterceptor m = new AudienceNetworkActivity.BackButtonInterceptor() {
        public boolean interceptBackButton() {
            com.facebook.ads.internal.view.f.a aVar;
            if (l.this.x == null) {
                return false;
            }
            if (!l.this.x.a()) {
                return true;
            }
            if (!(l.this.x.getSkipSeconds() == 0 || (aVar = l.this.b) == null)) {
                aVar.e();
            }
            com.facebook.ads.internal.view.f.a aVar2 = l.this.b;
            if (aVar2 != null) {
                aVar2.f();
            }
            return false;
        }
    };
    private final View.OnTouchListener n = new View.OnTouchListener() {
        public boolean onTouch(View view, MotionEvent motionEvent) {
            com.facebook.ads.internal.view.f.a aVar;
            if (motionEvent.getAction() != 1) {
                return true;
            }
            if (l.this.x != null) {
                if (!l.this.x.a()) {
                    return true;
                }
                if (!(l.this.x.getSkipSeconds() == 0 || (aVar = l.this.b) == null)) {
                    aVar.e();
                }
                com.facebook.ads.internal.view.f.a aVar2 = l.this.b;
                if (aVar2 != null) {
                    aVar2.f();
                }
            }
            l.this.l.finish();
            return true;
        }
    };
    private k.a o = k.a.UNSPECIFIED;
    private com.facebook.ads.internal.view.c.a p;
    private TextView q;
    private TextView r;
    private ImageView s;
    private a.C0030a t;
    private n u;
    private ViewGroup v;
    private d w;
    /* access modifiers changed from: private */
    public j x;
    private int y = -1;
    private int z = -10525069;

    /* JADX WARNING: Removed duplicated region for block: B:119:0x05a9  */
    /* JADX WARNING: Removed duplicated region for block: B:121:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(int r19) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            float r2 = com.facebook.ads.internal.q.a.x.b
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams
            r4 = 1113587712(0x42600000, float:56.0)
            float r4 = r4 * r2
            int r4 = (int) r4
            r3.<init>(r4, r4)
            r5 = 10
            r3.addRule(r5)
            r6 = 11
            r3.addRule(r6)
            r6 = 1098907648(0x41800000, float:16.0)
            float r6 = r6 * r2
            int r6 = (int) r6
            com.facebook.ads.internal.view.f.c.j r7 = r0.x
            r7.setPadding(r6, r6, r6, r6)
            com.facebook.ads.internal.view.f.c.j r7 = r0.x
            r7.setLayoutParams(r3)
            boolean r3 = r18.h()
            if (r3 == 0) goto L_0x0032
            com.facebook.ads.internal.view.f.c.d$a r3 = com.facebook.ads.internal.view.f.c.d.a.FADE_OUT_ON_PLAY
            goto L_0x0034
        L_0x0032:
            com.facebook.ads.internal.view.f.c.d$a r3 = com.facebook.ads.internal.view.f.c.d.a.VISIBLE
        L_0x0034:
            com.facebook.ads.internal.view.f.a r7 = r0.b
            int r7 = r7.getId()
            r9 = 0
            r10 = 2
            r13 = 16
            r14 = 1
            r15 = -2
            r12 = 12
            r11 = -1
            r8 = 0
            if (r1 != r14) goto L_0x01bd
            boolean r16 = r18.m()
            if (r16 != 0) goto L_0x0052
            boolean r16 = r18.n()
            if (r16 == 0) goto L_0x01bd
        L_0x0052:
            android.graphics.drawable.GradientDrawable r1 = new android.graphics.drawable.GradientDrawable
            android.graphics.drawable.GradientDrawable$Orientation r4 = android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM
            int[] r7 = new int[r10]
            r7 = {0, -15658735} // fill-array
            r1.<init>(r4, r7)
            r1.setCornerRadius(r9)
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams
            r4.<init>(r11, r15)
            r4.addRule(r5)
            com.facebook.ads.internal.view.f.a r5 = r0.b
            r5.setLayoutParams(r4)
            com.facebook.ads.internal.view.f.a r4 = r0.b
            r0.a((android.view.View) r4)
            com.facebook.ads.internal.view.f.c.j r4 = r0.x
            r0.a((android.view.View) r4)
            com.facebook.ads.internal.view.f.c.a$a r4 = r0.t
            if (r4 == 0) goto L_0x007f
            r0.a((android.view.View) r4)
        L_0x007f:
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams
            com.facebook.ads.internal.view.c.a r5 = r0.p
            if (r5 == 0) goto L_0x0088
            r5 = 64
            goto L_0x0089
        L_0x0088:
            r5 = 0
        L_0x0089:
            int r5 = r5 + 60
            int r5 = r5 + r13
            int r5 = r5 + r13
            int r5 = r5 + r13
            float r5 = (float) r5
            float r5 = r5 * r2
            int r5 = (int) r5
            r4.<init>(r11, r5)
            r4.addRule(r12)
            android.widget.RelativeLayout r5 = new android.widget.RelativeLayout
            android.content.Context r7 = r0.d
            r5.<init>(r7)
            com.facebook.ads.internal.q.a.x.a((android.view.View) r5, (android.graphics.drawable.Drawable) r1)
            r5.setLayoutParams(r4)
            com.facebook.ads.internal.view.c.a r1 = r0.p
            if (r1 == 0) goto L_0x00ac
            r1 = 64
            goto L_0x00ad
        L_0x00ac:
            r1 = 0
        L_0x00ad:
            int r1 = r1 + r13
            int r1 = r1 + r13
            float r1 = (float) r1
            float r1 = r1 * r2
            int r1 = (int) r1
            r5.setPadding(r6, r8, r6, r1)
            r0.v = r5
            boolean r1 = r0.B
            if (r1 != 0) goto L_0x00c1
            com.facebook.ads.internal.view.f.c.d r1 = r0.w
            r1.a((android.view.View) r5, (com.facebook.ads.internal.view.f.c.d.a) r3)
        L_0x00c1:
            r0.a((android.view.View) r5)
            com.facebook.ads.internal.view.f.c.n r1 = r0.u
            if (r1 == 0) goto L_0x00e6
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1086324736(0x40c00000, float:6.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.<init>(r11, r3)
            r1.addRule(r12)
            r3 = -1061158912(0xffffffffc0c00000, float:-6.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.topMargin = r3
            com.facebook.ads.internal.view.f.c.n r3 = r0.u
            r3.setLayoutParams(r1)
            com.facebook.ads.internal.view.f.c.n r1 = r0.u
            r0.a((android.view.View) r1)
        L_0x00e6:
            com.facebook.ads.internal.view.c.a r1 = r0.p
            if (r1 == 0) goto L_0x010a
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1115684864(0x42800000, float:64.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.<init>(r11, r3)
            r1.bottomMargin = r6
            r1.leftMargin = r6
            r1.rightMargin = r6
            r1.addRule(r14)
            r1.addRule(r12)
            com.facebook.ads.internal.view.c.a r3 = r0.p
            r3.setLayoutParams(r1)
            com.facebook.ads.internal.view.c.a r1 = r0.p
            r0.a((android.view.View) r1)
        L_0x010a:
            android.widget.ImageView r1 = r0.s
            if (r1 == 0) goto L_0x012a
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1114636288(0x42700000, float:60.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.<init>(r3, r3)
            r1.addRule(r12)
            r3 = 9
            r1.addRule(r3)
            android.widget.ImageView r3 = r0.s
            r3.setLayoutParams(r1)
            android.widget.ImageView r1 = r0.s
            r0.a(r5, r1)
        L_0x012a:
            android.widget.TextView r1 = r0.q
            if (r1 == 0) goto L_0x0176
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r1.<init>(r11, r15)
            r3 = 1108344832(0x42100000, float:36.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.bottomMargin = r3
            r1.addRule(r12)
            r3 = 9
            r1.addRule(r3)
            android.widget.TextView r3 = r0.q
            android.text.TextUtils$TruncateAt r4 = android.text.TextUtils.TruncateAt.END
            r3.setEllipsize(r4)
            android.widget.TextView r3 = r0.q
            r4 = 8388611(0x800003, float:1.1754948E-38)
            r3.setGravity(r4)
            android.widget.TextView r3 = r0.q
            r3.setLayoutParams(r1)
            android.widget.TextView r1 = r0.q
            r1.setMaxLines(r14)
            android.widget.TextView r1 = r0.q
            r3 = 1116733440(0x42900000, float:72.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.setPadding(r3, r8, r8, r8)
            android.widget.TextView r1 = r0.q
            r1.setTextColor(r11)
            android.widget.TextView r1 = r0.q
            r3 = 1099956224(0x41900000, float:18.0)
            r1.setTextSize(r3)
            android.widget.TextView r1 = r0.q
            r0.a(r5, r1)
        L_0x0176:
            android.widget.TextView r1 = r0.r
            if (r1 == 0) goto L_0x0594
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r1.<init>(r11, r15)
            r1.addRule(r12)
            r3 = 9
            r1.addRule(r3)
            r3 = 1082130432(0x40800000, float:4.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.bottomMargin = r3
            android.widget.TextView r3 = r0.r
            android.text.TextUtils$TruncateAt r4 = android.text.TextUtils.TruncateAt.END
            r3.setEllipsize(r4)
            android.widget.TextView r3 = r0.r
            r4 = 8388611(0x800003, float:1.1754948E-38)
            r3.setGravity(r4)
            android.widget.TextView r3 = r0.r
            r3.setLayoutParams(r1)
            android.widget.TextView r1 = r0.r
            r1.setMaxLines(r14)
            android.widget.TextView r1 = r0.r
            r3 = 1116733440(0x42900000, float:72.0)
            float r2 = r2 * r3
            int r2 = (int) r2
            r1.setPadding(r2, r8, r8, r8)
            android.widget.TextView r1 = r0.r
            r1.setTextColor(r11)
            android.widget.TextView r1 = r0.r
            r0.a(r5, r1)
            goto L_0x0594
        L_0x01bd:
            r13 = 1073741824(0x40000000, float:2.0)
            r17 = 1117782016(0x42a00000, float:80.0)
            r9 = 17
            if (r1 != r14) goto L_0x02ee
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r1.<init>(r11, r15)
            r1.addRule(r5)
            com.facebook.ads.internal.view.f.a r3 = r0.b
            r3.setLayoutParams(r1)
            com.facebook.ads.internal.view.f.a r1 = r0.b
            r0.a((android.view.View) r1)
            com.facebook.ads.internal.view.f.c.j r1 = r0.x
            r0.a((android.view.View) r1)
            com.facebook.ads.internal.view.f.c.a$a r1 = r0.t
            if (r1 == 0) goto L_0x01e3
            r0.a((android.view.View) r1)
        L_0x01e3:
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            android.content.Context r3 = r0.d
            r1.<init>(r3)
            r0.v = r1
            r3 = 112(0x70, float:1.57E-43)
            r1.setGravity(r3)
            r1.setOrientation(r14)
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams
            r3.<init>(r11, r11)
            r4 = 1107558400(0x42040000, float:33.0)
            float r4 = r4 * r2
            int r4 = (int) r4
            r3.leftMargin = r4
            r3.rightMargin = r4
            r5 = 1090519040(0x41000000, float:8.0)
            float r5 = r5 * r2
            int r5 = (int) r5
            r3.topMargin = r5
            com.facebook.ads.internal.view.c.a r5 = r0.p
            if (r5 != 0) goto L_0x0210
            r3.bottomMargin = r6
            goto L_0x0215
        L_0x0210:
            float r5 = r2 * r17
            int r5 = (int) r5
            r3.bottomMargin = r5
        L_0x0215:
            r5 = 3
            r3.addRule(r5, r7)
            r1.setLayoutParams(r3)
            r0.a((android.view.View) r1)
            com.facebook.ads.internal.view.c.a r3 = r0.p
            if (r3 == 0) goto L_0x0243
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams
            r5 = 1115684864(0x42800000, float:64.0)
            float r8 = r2 * r5
            int r5 = (int) r8
            r3.<init>(r11, r5)
            r3.bottomMargin = r6
            r3.leftMargin = r4
            r3.rightMargin = r4
            r3.addRule(r14)
            r3.addRule(r12)
            com.facebook.ads.internal.view.c.a r4 = r0.p
            r4.setLayoutParams(r3)
            com.facebook.ads.internal.view.c.a r3 = r0.p
            r0.a((android.view.View) r3)
        L_0x0243:
            android.widget.TextView r3 = r0.q
            if (r3 == 0) goto L_0x027f
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r3.<init>(r15, r15)
            r3.weight = r13
            r3.gravity = r9
            android.widget.TextView r4 = r0.q
            android.text.TextUtils$TruncateAt r5 = android.text.TextUtils.TruncateAt.END
            r4.setEllipsize(r5)
            android.widget.TextView r4 = r0.q
            r4.setGravity(r9)
            android.widget.TextView r4 = r0.q
            r4.setLayoutParams(r3)
            android.widget.TextView r3 = r0.q
            r3.setMaxLines(r10)
            android.widget.TextView r3 = r0.q
            r4 = 0
            r3.setPadding(r4, r4, r4, r4)
            android.widget.TextView r3 = r0.q
            int r4 = r0.z
            r3.setTextColor(r4)
            android.widget.TextView r3 = r0.q
            r4 = 1103101952(0x41c00000, float:24.0)
            r3.setTextSize(r4)
            android.widget.TextView r3 = r0.q
            r0.a(r1, r3)
        L_0x027f:
            android.widget.ImageView r3 = r0.s
            if (r3 == 0) goto L_0x029c
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r4 = 1115684864(0x42800000, float:64.0)
            float r4 = r4 * r2
            int r4 = (int) r4
            r3.<init>(r4, r4)
            r4 = 0
            r3.weight = r4
            r3.gravity = r9
            android.widget.ImageView r4 = r0.s
            r4.setLayoutParams(r3)
            android.widget.ImageView r3 = r0.s
            r0.a(r1, r3)
        L_0x029c:
            android.widget.TextView r3 = r0.r
            if (r3 == 0) goto L_0x02d3
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r3.<init>(r11, r15)
            r3.weight = r13
            r4 = 16
            r3.gravity = r4
            android.widget.TextView r5 = r0.r
            android.text.TextUtils$TruncateAt r6 = android.text.TextUtils.TruncateAt.END
            r5.setEllipsize(r6)
            android.widget.TextView r5 = r0.r
            r5.setGravity(r4)
            android.widget.TextView r4 = r0.r
            r4.setLayoutParams(r3)
            android.widget.TextView r3 = r0.r
            r3.setMaxLines(r10)
            android.widget.TextView r3 = r0.r
            r4 = 0
            r3.setPadding(r4, r4, r4, r4)
            android.widget.TextView r3 = r0.r
            int r4 = r0.z
            r3.setTextColor(r4)
            android.widget.TextView r3 = r0.r
            r0.a(r1, r3)
        L_0x02d3:
            com.facebook.ads.internal.view.f.c.n r1 = r0.u
            if (r1 == 0) goto L_0x042b
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1086324736(0x40c00000, float:6.0)
            float r2 = r2 * r3
            int r2 = (int) r2
            r1.<init>(r11, r2)
            r2 = 3
            r1.addRule(r2, r7)
            com.facebook.ads.internal.view.f.c.n r2 = r0.u
            r2.setLayoutParams(r1)
            com.facebook.ads.internal.view.f.c.n r1 = r0.u
            goto L_0x0428
        L_0x02ee:
            boolean r1 = r18.o()
            if (r1 == 0) goto L_0x0437
            boolean r1 = r18.n()
            if (r1 != 0) goto L_0x0437
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r1.<init>(r15, r11)
            r3 = 9
            r1.addRule(r3)
            com.facebook.ads.internal.view.f.a r3 = r0.b
            r3.setLayoutParams(r1)
            com.facebook.ads.internal.view.f.a r1 = r0.b
            r0.a((android.view.View) r1)
            com.facebook.ads.internal.view.f.c.j r1 = r0.x
            r0.a((android.view.View) r1)
            com.facebook.ads.internal.view.f.c.a$a r1 = r0.t
            if (r1 == 0) goto L_0x031a
            r0.a((android.view.View) r1)
        L_0x031a:
            android.widget.LinearLayout r1 = new android.widget.LinearLayout
            android.content.Context r3 = r0.d
            r1.<init>(r3)
            r0.v = r1
            r3 = 112(0x70, float:1.57E-43)
            r1.setGravity(r3)
            r1.setOrientation(r14)
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams
            r3.<init>(r11, r11)
            r3.leftMargin = r6
            r3.rightMargin = r6
            r4 = 1090519040(0x41000000, float:8.0)
            float r4 = r4 * r2
            int r4 = (int) r4
            r3.topMargin = r4
            float r4 = r2 * r17
            int r4 = (int) r4
            r3.bottomMargin = r4
            r3.addRule(r14, r7)
            r1.setLayoutParams(r3)
            r0.a((android.view.View) r1)
            com.facebook.ads.internal.view.f.c.n r3 = r0.u
            if (r3 == 0) goto L_0x0374
            android.widget.RelativeLayout$LayoutParams r3 = new android.widget.RelativeLayout$LayoutParams
            r4 = 1086324736(0x40c00000, float:6.0)
            float r8 = r2 * r4
            int r4 = (int) r8
            r3.<init>(r11, r4)
            r4 = 5
            r3.addRule(r4, r7)
            r4 = 7
            r3.addRule(r4, r7)
            r4 = 3
            r3.addRule(r4, r7)
            r4 = -1061158912(0xffffffffc0c00000, float:-6.0)
            float r4 = r4 * r2
            int r4 = (int) r4
            r3.topMargin = r4
            com.facebook.ads.internal.view.f.c.n r4 = r0.u
            r4.setLayoutParams(r3)
            com.facebook.ads.internal.view.f.c.n r3 = r0.u
            r0.a((android.view.View) r3)
        L_0x0374:
            android.widget.TextView r3 = r0.q
            if (r3 == 0) goto L_0x03b0
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r3.<init>(r15, r15)
            r3.weight = r13
            r3.gravity = r9
            android.widget.TextView r4 = r0.q
            android.text.TextUtils$TruncateAt r8 = android.text.TextUtils.TruncateAt.END
            r4.setEllipsize(r8)
            android.widget.TextView r4 = r0.q
            r4.setGravity(r9)
            android.widget.TextView r4 = r0.q
            r4.setLayoutParams(r3)
            android.widget.TextView r3 = r0.q
            r3.setMaxLines(r5)
            android.widget.TextView r3 = r0.q
            r4 = 0
            r3.setPadding(r4, r4, r4, r4)
            android.widget.TextView r3 = r0.q
            int r4 = r0.z
            r3.setTextColor(r4)
            android.widget.TextView r3 = r0.q
            r4 = 1103101952(0x41c00000, float:24.0)
            r3.setTextSize(r4)
            android.widget.TextView r3 = r0.q
            r0.a(r1, r3)
        L_0x03b0:
            android.widget.ImageView r3 = r0.s
            if (r3 == 0) goto L_0x03cd
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r4 = 1115684864(0x42800000, float:64.0)
            float r8 = r2 * r4
            int r4 = (int) r8
            r3.<init>(r4, r4)
            r4 = 0
            r3.weight = r4
            r3.gravity = r9
            android.widget.ImageView r4 = r0.s
            r4.setLayoutParams(r3)
            android.widget.ImageView r3 = r0.s
            r0.a(r1, r3)
        L_0x03cd:
            android.widget.TextView r3 = r0.r
            if (r3 == 0) goto L_0x0404
            android.widget.LinearLayout$LayoutParams r3 = new android.widget.LinearLayout$LayoutParams
            r3.<init>(r11, r15)
            r3.weight = r13
            r4 = 16
            r3.gravity = r4
            android.widget.TextView r4 = r0.r
            android.text.TextUtils$TruncateAt r8 = android.text.TextUtils.TruncateAt.END
            r4.setEllipsize(r8)
            android.widget.TextView r4 = r0.r
            r4.setGravity(r9)
            android.widget.TextView r4 = r0.r
            r4.setLayoutParams(r3)
            android.widget.TextView r3 = r0.r
            r3.setMaxLines(r5)
            android.widget.TextView r3 = r0.r
            r4 = 0
            r3.setPadding(r4, r4, r4, r4)
            android.widget.TextView r3 = r0.r
            int r4 = r0.z
            r3.setTextColor(r4)
            android.widget.TextView r3 = r0.r
            r0.a(r1, r3)
        L_0x0404:
            com.facebook.ads.internal.view.c.a r1 = r0.p
            if (r1 == 0) goto L_0x042b
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1115684864(0x42800000, float:64.0)
            float r2 = r2 * r3
            int r2 = (int) r2
            r1.<init>(r11, r2)
            r1.bottomMargin = r6
            r1.leftMargin = r6
            r1.rightMargin = r6
            r1.addRule(r14)
            r1.addRule(r12)
            r1.addRule(r14, r7)
            com.facebook.ads.internal.view.c.a r2 = r0.p
            r2.setLayoutParams(r1)
            com.facebook.ads.internal.view.c.a r1 = r0.p
        L_0x0428:
            r0.a((android.view.View) r1)
        L_0x042b:
            com.facebook.ads.internal.view.f.a r1 = r0.b
            android.view.ViewParent r1 = r1.getParent()
            android.view.View r1 = (android.view.View) r1
            int r2 = r0.y
            goto L_0x059e
        L_0x0437:
            android.graphics.drawable.GradientDrawable r1 = new android.graphics.drawable.GradientDrawable
            android.graphics.drawable.GradientDrawable$Orientation r5 = android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM
            int[] r7 = new int[r10]
            r7 = {0, -15658735} // fill-array
            r1.<init>(r5, r7)
            r5 = 0
            r1.setCornerRadius(r5)
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams
            r5.<init>(r11, r11)
            com.facebook.ads.internal.view.f.a r7 = r0.b
            r7.setLayoutParams(r5)
            com.facebook.ads.internal.view.f.a r5 = r0.b
            r0.a((android.view.View) r5)
            com.facebook.ads.internal.view.f.c.j r5 = r0.x
            r0.a((android.view.View) r5)
            com.facebook.ads.internal.view.f.c.a$a r5 = r0.t
            if (r5 == 0) goto L_0x0462
            r0.a((android.view.View) r5)
        L_0x0462:
            android.widget.RelativeLayout$LayoutParams r5 = new android.widget.RelativeLayout$LayoutParams
            r7 = 1123549184(0x42f80000, float:124.0)
            float r7 = r7 * r2
            int r7 = (int) r7
            r5.<init>(r11, r7)
            r5.addRule(r12)
            android.widget.RelativeLayout r7 = new android.widget.RelativeLayout
            android.content.Context r8 = r0.d
            r7.<init>(r8)
            com.facebook.ads.internal.q.a.x.a((android.view.View) r7, (android.graphics.drawable.Drawable) r1)
            r7.setLayoutParams(r5)
            r1 = 0
            r7.setPadding(r6, r1, r6, r6)
            r0.v = r7
            boolean r1 = r0.B
            if (r1 != 0) goto L_0x048b
            com.facebook.ads.internal.view.f.c.d r1 = r0.w
            r1.a((android.view.View) r7, (com.facebook.ads.internal.view.f.c.d.a) r3)
        L_0x048b:
            r0.a((android.view.View) r7)
            com.facebook.ads.internal.view.c.a r1 = r0.p
            if (r1 == 0) goto L_0x04b2
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1121714176(0x42dc0000, float:110.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.<init>(r3, r4)
            r1.rightMargin = r6
            r1.bottomMargin = r6
            r1.addRule(r12)
            r3 = 11
            r1.addRule(r3)
            com.facebook.ads.internal.view.c.a r3 = r0.p
            r3.setLayoutParams(r1)
            com.facebook.ads.internal.view.c.a r1 = r0.p
            r0.a((android.view.View) r1)
        L_0x04b2:
            android.widget.ImageView r1 = r0.s
            if (r1 == 0) goto L_0x04d9
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1115684864(0x42800000, float:64.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.<init>(r3, r3)
            r1.addRule(r12)
            r3 = 9
            r1.addRule(r3)
            r3 = 1090519040(0x41000000, float:8.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.bottomMargin = r3
            android.widget.ImageView r3 = r0.s
            r3.setLayoutParams(r1)
            android.widget.ImageView r1 = r0.s
            r0.a(r7, r1)
        L_0x04d9:
            android.widget.TextView r1 = r0.q
            if (r1 == 0) goto L_0x0530
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r1.<init>(r11, r15)
            r3 = 1111490560(0x42400000, float:48.0)
            float r3 = r3 * r2
            int r3 = (int) r3
            r1.bottomMargin = r3
            r1.addRule(r12)
            r3 = 9
            r1.addRule(r3)
            android.widget.TextView r3 = r0.q
            android.text.TextUtils$TruncateAt r4 = android.text.TextUtils.TruncateAt.END
            r3.setEllipsize(r4)
            android.widget.TextView r3 = r0.q
            r4 = 8388611(0x800003, float:1.1754948E-38)
            r3.setGravity(r4)
            android.widget.TextView r3 = r0.q
            r3.setLayoutParams(r1)
            android.widget.TextView r1 = r0.q
            r1.setMaxLines(r14)
            android.widget.TextView r1 = r0.q
            float r3 = r2 * r17
            int r3 = (int) r3
            com.facebook.ads.internal.view.c.a r4 = r0.p
            if (r4 == 0) goto L_0x051a
            r4 = 1123811328(0x42fc0000, float:126.0)
            float r4 = r4 * r2
            int r8 = (int) r4
            r4 = 0
            goto L_0x051c
        L_0x051a:
            r4 = 0
            r8 = 0
        L_0x051c:
            r1.setPadding(r3, r4, r8, r4)
            android.widget.TextView r1 = r0.q
            r1.setTextColor(r11)
            android.widget.TextView r1 = r0.q
            r3 = 1103101952(0x41c00000, float:24.0)
            r1.setTextSize(r3)
            android.widget.TextView r1 = r0.q
            r0.a(r7, r1)
        L_0x0530:
            android.widget.TextView r1 = r0.r
            if (r1 == 0) goto L_0x0579
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r1.<init>(r11, r15)
            r1.addRule(r12)
            r3 = 9
            r1.addRule(r3)
            android.widget.TextView r3 = r0.r
            android.text.TextUtils$TruncateAt r4 = android.text.TextUtils.TruncateAt.END
            r3.setEllipsize(r4)
            android.widget.TextView r3 = r0.r
            r4 = 8388611(0x800003, float:1.1754948E-38)
            r3.setGravity(r4)
            android.widget.TextView r3 = r0.r
            r3.setLayoutParams(r1)
            android.widget.TextView r1 = r0.r
            r1.setMaxLines(r10)
            android.widget.TextView r1 = r0.r
            r1.setTextColor(r11)
            android.widget.TextView r1 = r0.r
            float r3 = r2 * r17
            int r3 = (int) r3
            com.facebook.ads.internal.view.c.a r4 = r0.p
            if (r4 == 0) goto L_0x056f
            r4 = 1123811328(0x42fc0000, float:126.0)
            float r4 = r4 * r2
            int r8 = (int) r4
            r4 = 0
            goto L_0x0571
        L_0x056f:
            r4 = 0
            r8 = 0
        L_0x0571:
            r1.setPadding(r3, r4, r8, r4)
            android.widget.TextView r1 = r0.r
            r0.a(r7, r1)
        L_0x0579:
            com.facebook.ads.internal.view.f.c.n r1 = r0.u
            if (r1 == 0) goto L_0x0594
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams
            r3 = 1086324736(0x40c00000, float:6.0)
            float r2 = r2 * r3
            int r2 = (int) r2
            r1.<init>(r11, r2)
            r1.addRule(r12)
            com.facebook.ads.internal.view.f.c.n r2 = r0.u
            r2.setLayoutParams(r1)
            com.facebook.ads.internal.view.f.c.n r1 = r0.u
            r0.a((android.view.View) r1)
        L_0x0594:
            com.facebook.ads.internal.view.f.a r1 = r0.b
            android.view.ViewParent r1 = r1.getParent()
            android.view.View r1 = (android.view.View) r1
            r2 = -16777216(0xffffffffff000000, float:-1.7014118E38)
        L_0x059e:
            com.facebook.ads.internal.q.a.x.a((android.view.View) r1, (int) r2)
            com.facebook.ads.internal.view.f.a r1 = r0.b
            android.view.View r1 = r1.getRootView()
            if (r1 == 0) goto L_0x05ac
            r1.setOnTouchListener(r0)
        L_0x05ac:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.adapters.l.a(int):void");
    }

    private void a(View view) {
        a.C0026a aVar = this.k;
        if (aVar != null) {
            aVar.a(view);
        }
    }

    private void a(ViewGroup viewGroup, View view) {
        if (viewGroup != null) {
            viewGroup.addView(view);
        }
    }

    private void b(View view) {
        ViewGroup viewGroup;
        if (view != null && (viewGroup = (ViewGroup) view.getParent()) != null) {
            viewGroup.removeView(view);
        }
    }

    private boolean m() {
        return ((double) (this.b.getVideoHeight() > 0 ? ((float) this.b.getVideoWidth()) / ((float) this.b.getVideoHeight()) : -1.0f)) <= 0.9d;
    }

    private boolean n() {
        if (this.b.getVideoHeight() <= 0) {
            return false;
        }
        Rect rect = new Rect();
        this.l.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);
        if (rect.width() > rect.height()) {
            return ((float) (rect.width() - ((rect.height() * this.b.getVideoWidth()) / this.b.getVideoHeight()))) - (x.b * 192.0f) < 0.0f;
        }
        int height = rect.height();
        float f2 = x.b;
        return ((((float) (height - ((rect.width() * this.b.getVideoHeight()) / this.b.getVideoWidth()))) - (f2 * 64.0f)) - (64.0f * f2)) - (f2 * 40.0f) < 0.0f;
    }

    private boolean o() {
        double videoWidth = (double) (this.b.getVideoHeight() > 0 ? ((float) this.b.getVideoWidth()) / ((float) this.b.getVideoHeight()) : -1.0f);
        return videoWidth > 0.9d && videoWidth < 1.1d;
    }

    private void p() {
        b((View) this.b);
        b((View) this.p);
        b((View) this.q);
        b((View) this.r);
        b((View) this.s);
        b((View) this.u);
        b((View) this.v);
        b((View) this.x);
        a.C0030a aVar = this.t;
        if (aVar != null) {
            b((View) aVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        JSONObject jSONObject = this.c;
        if (jSONObject == null) {
            Log.e(j, "Unable to add UI without a valid ad response.");
            return;
        }
        String string = jSONObject.getString("ct");
        String optString = this.c.getJSONObject("context").optString(AdUnitActivity.EXTRA_ORIENTATION);
        if (!optString.isEmpty()) {
            this.o = k.a.a(Integer.parseInt(optString));
        }
        if (this.c.has("layout") && !this.c.isNull("layout")) {
            JSONObject jSONObject2 = this.c.getJSONObject("layout");
            this.y = (int) jSONObject2.optLong("bgColor", (long) this.y);
            this.z = (int) jSONObject2.optLong("textColor", (long) this.z);
            this.A = (int) jSONObject2.optLong("accentColor", (long) this.A);
            this.B = jSONObject2.optBoolean("persistentAdDetails", this.B);
        }
        JSONObject jSONObject3 = this.c.getJSONObject("text");
        this.b.setId(Build.VERSION.SDK_INT >= 17 ? View.generateViewId() : x.a());
        int c = c();
        Context context = this.d;
        if (c < 0) {
            c = 0;
        }
        this.x = new j(context, c, this.A);
        this.x.setOnTouchListener(this.n);
        this.b.a((b) this.x);
        if (this.c.has("cta") && !this.c.isNull("cta")) {
            JSONObject jSONObject4 = this.c.getJSONObject("cta");
            this.p = new com.facebook.ads.internal.view.c.a(this.d, jSONObject4.getString(ReportDBAdapter.ReportColumns.COLUMN_URL), jSONObject4.getString("text"), this.A, this.b, this.f2644a, string);
            c.a(this.d, this.f2644a, string, Uri.parse(jSONObject4.getString(ReportDBAdapter.ReportColumns.COLUMN_URL)), new HashMap());
        }
        if (this.c.has("icon") && !this.c.isNull("icon")) {
            JSONObject jSONObject5 = this.c.getJSONObject("icon");
            this.s = new ImageView(this.d);
            com.facebook.ads.internal.view.b.d dVar = new com.facebook.ads.internal.view.b.d(this.s);
            float f2 = x.b;
            dVar.a((int) (f2 * 64.0f), (int) (f2 * 64.0f)).a(jSONObject5.getString(ReportDBAdapter.ReportColumns.COLUMN_URL));
        }
        if (this.c.has("image") && !this.c.isNull("image")) {
            JSONObject jSONObject6 = this.c.getJSONObject("image");
            g gVar = new g(this.d);
            this.b.a((b) gVar);
            gVar.setImage(jSONObject6.getString(ReportDBAdapter.ReportColumns.COLUMN_URL));
        }
        String optString2 = jSONObject3.optString("title");
        if (!optString2.isEmpty()) {
            this.q = new TextView(this.d);
            this.q.setText(optString2);
            this.q.setTypeface(Typeface.defaultFromStyle(1));
        }
        String optString3 = jSONObject3.optString("subtitle");
        if (!optString3.isEmpty()) {
            this.r = new TextView(this.d);
            this.r.setText(optString3);
            this.r.setTextSize(16.0f);
        }
        this.u = new n(this.d);
        this.b.a((b) this.u);
        String d = d();
        if (!TextUtils.isEmpty(d)) {
            this.t = new a.C0030a(this.d, "AdChoices", d, new float[]{0.0f, 0.0f, 8.0f, 0.0f}, string);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(10);
            layoutParams.addRule(9);
            this.t.setLayoutParams(layoutParams);
        }
        this.b.a((b) new com.facebook.ads.internal.view.f.c.k(this.d));
        com.facebook.ads.internal.view.f.c.l lVar = new com.facebook.ads.internal.view.f.c.l(this.d);
        this.b.a((b) lVar);
        d.a aVar = h() ? d.a.FADE_OUT_ON_PLAY : d.a.VISIBLE;
        this.b.a((b) new d(lVar, aVar));
        this.w = new d(new RelativeLayout(this.d), aVar);
        this.b.a((b) this.w);
    }

    @TargetApi(17)
    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        this.l = audienceNetworkActivity;
        if (i || this.k != null) {
            audienceNetworkActivity.addBackButtonInterceptor(this.m);
            p();
            a(this.l.getResources().getConfiguration().orientation);
            if (h()) {
                e();
            } else {
                f();
            }
        } else {
            throw new AssertionError();
        }
    }

    public void a(Configuration configuration) {
        p();
        a(configuration.orientation);
    }

    public void a(Bundle bundle) {
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        if (i || this.c != null) {
            try {
                return this.c.getJSONObject(Advertisement.KEY_VIDEO).getBoolean(AudienceNetworkActivity.AUTOPLAY);
            } catch (Exception e) {
                Log.w(String.valueOf(l.class), "Invalid JSON", e);
                return true;
            }
        } else {
            throw new AssertionError();
        }
    }

    public void i() {
        com.facebook.ads.internal.view.f.a aVar = this.b;
        if (aVar != null && aVar.getState() == com.facebook.ads.internal.view.f.d.d.STARTED) {
            this.C = this.b.getVideoStartReason();
            this.b.a(false);
        }
    }

    public void j() {
        com.facebook.ads.internal.view.f.a.a aVar;
        com.facebook.ads.internal.view.f.a aVar2 = this.b;
        if (aVar2 != null && (aVar = this.C) != null) {
            aVar2.a(aVar);
        }
    }

    public k.a k() {
        return this.o;
    }

    public void l() {
        Activity activity = this.l;
        if (activity != null) {
            activity.finish();
        }
    }

    public void onDestroy() {
        JSONObject jSONObject = this.c;
        if (!(jSONObject == null || this.f2644a == null)) {
            String optString = jSONObject.optString("ct");
            if (!TextUtils.isEmpty(optString)) {
                this.f2644a.i(optString, new HashMap());
            }
        }
        com.facebook.ads.internal.view.f.a aVar = this.b;
        if (aVar != null) {
            aVar.f();
        }
        k.a((com.facebook.ads.internal.view.a) this);
    }

    public boolean onTouch(View view, MotionEvent motionEvent) {
        com.facebook.ads.internal.view.f.a aVar = this.b;
        if (aVar == null) {
            return true;
        }
        aVar.getEventBus().a(new t(view, motionEvent));
        return true;
    }

    public void setListener(a.C0026a aVar) {
        this.k = aVar;
    }
}
