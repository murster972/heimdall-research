package com.facebook.ads.internal.adapters;

import com.facebook.ads.internal.protocol.AdPlacementType;
import com.facebook.ads.internal.q.a.v;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final Set<g> f2634a = new HashSet();
    private static final Map<AdPlacementType, String> b = new ConcurrentHashMap();

    /* renamed from: com.facebook.ads.internal.adapters.e$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2635a = new int[AdPlacementType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.facebook.ads.internal.protocol.AdPlacementType[] r0 = com.facebook.ads.internal.protocol.AdPlacementType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2635a = r0
                int[] r0 = f2635a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.BANNER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2635a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.INTERSTITIAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2635a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.NATIVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2635a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.NATIVE_BANNER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f2635a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.INSTREAM     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f2635a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.adapters.e.AnonymousClass1.<clinit>():void");
        }
    }

    static {
        for (g next : g.a()) {
            Class cls = null;
            switch (AnonymousClass1.f2635a[next.m.ordinal()]) {
                case 1:
                    cls = BannerAdapter.class;
                    break;
                case 2:
                    cls = InterstitialAdapter.class;
                    break;
                case 3:
                case 4:
                    cls = y.class;
                    break;
                case 5:
                    cls = s.class;
                    break;
                case 6:
                    cls = ab.class;
                    break;
            }
            if (cls != null) {
                Class<?> cls2 = next.j;
                if (cls2 == null) {
                    try {
                        cls2 = Class.forName(next.k);
                    } catch (ClassNotFoundException unused) {
                    }
                }
                if (cls2 != null && cls.isAssignableFrom(cls2)) {
                    f2634a.add(next);
                }
            }
        }
    }

    public static AdAdapter a(f fVar, AdPlacementType adPlacementType) {
        AdAdapter adAdapter = null;
        try {
            g b2 = b(fVar, adPlacementType);
            if (b2 != null && f2634a.contains(b2)) {
                Class<?> cls = b2.j;
                if (cls == null) {
                    cls = Class.forName(b2.k);
                }
                AdAdapter adAdapter2 = (AdAdapter) cls.newInstance();
                try {
                    if (!(adAdapter2 instanceof m)) {
                        return adAdapter2;
                    }
                    ((m) adAdapter2).a(adPlacementType);
                    return adAdapter2;
                } catch (Exception e) {
                    adAdapter = adAdapter2;
                    e = e;
                    e.printStackTrace();
                    return adAdapter;
                }
            }
        } catch (Exception e2) {
            e = e2;
            e.printStackTrace();
            return adAdapter;
        }
        return adAdapter;
    }

    public static AdAdapter a(String str, AdPlacementType adPlacementType) {
        return a(f.a(str), adPlacementType);
    }

    public static String a(AdPlacementType adPlacementType) {
        if (b.containsKey(adPlacementType)) {
            return b.get(adPlacementType);
        }
        HashSet hashSet = new HashSet();
        for (g next : f2634a) {
            if (next.m == adPlacementType) {
                hashSet.add(next.l.toString());
            }
        }
        String a2 = v.a(hashSet, ",");
        b.put(adPlacementType, a2);
        return a2;
    }

    private static g b(f fVar, AdPlacementType adPlacementType) {
        for (g next : f2634a) {
            if (next.l == fVar && next.m == adPlacementType) {
                return next;
            }
        }
        return null;
    }
}
