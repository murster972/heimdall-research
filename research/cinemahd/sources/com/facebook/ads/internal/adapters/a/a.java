package com.facebook.ads.internal.adapters.a;

import java.io.Serializable;

public class a implements Serializable {
    private static final long serialVersionUID = 9136244113276723461L;

    /* renamed from: a  reason: collision with root package name */
    private final d f2611a;
    private final d b;

    a(d dVar, d dVar2) {
        this.f2611a = dVar;
        this.b = dVar2;
    }

    public d a() {
        return this.f2611a;
    }

    public d b() {
        return this.b;
    }
}
