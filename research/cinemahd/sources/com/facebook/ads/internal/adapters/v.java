package com.facebook.ads.internal.adapters;

public class v {

    /* renamed from: com.facebook.ads.internal.adapters.v$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2671a = new int[f.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.facebook.ads.internal.adapters.f[] r0 = com.facebook.ads.internal.adapters.f.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2671a = r0
                int[] r0 = f2671a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.adapters.f r1 = com.facebook.ads.internal.adapters.f.ADMOB     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2671a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.adapters.f r1 = com.facebook.ads.internal.adapters.f.YAHOO     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2671a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.adapters.f r1 = com.facebook.ads.internal.adapters.f.INMOBI     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2671a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.adapters.f r1 = com.facebook.ads.internal.adapters.f.AN     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.adapters.v.AnonymousClass1.<clinit>():void");
        }
    }

    public static String a(f fVar) {
        int i = AnonymousClass1.f2671a[fVar.ordinal()];
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? "" : "Audience Network" : "InMobi" : "Flurry" : "AdMob";
    }
}
