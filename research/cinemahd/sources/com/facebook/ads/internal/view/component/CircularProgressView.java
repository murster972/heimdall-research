package com.facebook.ads.internal.view.component;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.view.View;
import android.view.animation.LinearInterpolator;
import androidx.annotation.Keep;

public class CircularProgressView extends View {

    /* renamed from: a  reason: collision with root package name */
    private final float f2891a = (Resources.getSystem().getDisplayMetrics().density * 3.0f);
    private final RectF b = new RectF();
    private final Paint c = new Paint(1);
    private final Paint d;
    private float e = 0.0f;

    public CircularProgressView(Context context) {
        super(context);
        this.c.setStyle(Paint.Style.STROKE);
        this.c.setStrokeWidth(this.f2891a);
        this.d = new Paint(1);
        this.d.setStyle(Paint.Style.STROKE);
        this.d.setStrokeWidth(this.f2891a);
    }

    public void a(int i, int i2) {
        this.c.setColor(i);
        this.d.setColor(i2);
    }

    @Keep
    public float getProgress() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawArc(this.b, 0.0f, 360.0f, false, this.c);
        canvas.drawArc(this.b, -90.0f, (this.e * 360.0f) / 100.0f, false, this.d);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int min = Math.min(View.getDefaultSize(getSuggestedMinimumHeight(), i2), View.getDefaultSize(getSuggestedMinimumWidth(), i));
        setMeasuredDimension(min, min);
        float f = (float) min;
        this.b.set((this.f2891a / 2.0f) + 0.0f + ((float) getPaddingLeft()), (this.f2891a / 2.0f) + 0.0f + ((float) getPaddingTop()), (f - (this.f2891a / 2.0f)) - ((float) getPaddingRight()), (f - (this.f2891a / 2.0f)) - ((float) getPaddingBottom()));
    }

    @Keep
    public void setProgress(float f) {
        this.e = Math.min(f, 100.0f);
        postInvalidate();
    }

    public void setProgressWithAnimation(float f) {
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, "progress", new float[]{f});
        ofFloat.setDuration(400);
        ofFloat.setInterpolator(new LinearInterpolator());
        ofFloat.start();
    }
}
