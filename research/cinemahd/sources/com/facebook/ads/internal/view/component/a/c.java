package com.facebook.ads.internal.view.component.a;

import android.util.DisplayMetrics;
import com.facebook.ads.internal.adapters.a.d;
import com.facebook.ads.internal.adapters.a.h;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.view.component.a;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final int f2901a;
    private static final int b;

    static {
        DisplayMetrics displayMetrics = x.f2829a;
        f2901a = displayMetrics.heightPixels;
        b = displayMetrics.widthPixels;
    }

    private static float a(h hVar) {
        int h = hVar.c().h();
        int i = hVar.c().i();
        if (i > 0) {
            return ((float) h) / ((float) i);
        }
        return -1.0f;
    }

    private static int a(int i) {
        int a2 = x.a(16) + (a.f2892a * 2);
        return (f2901a - i) - (a2 + (b.f2900a * 2));
    }

    public static b a(d dVar) {
        b bVar;
        boolean z = true;
        d a2 = dVar.k() == 1 ? dVar.g().b().a() : dVar.g().b().b();
        h hVar = dVar.g().d().get(0);
        double a3 = (double) a(hVar);
        if (a(dVar.k(), dVar.j(), a3)) {
            if (dVar.k() != 2) {
                z = false;
            }
            bVar = new a(dVar, a2, z);
        } else {
            bVar = new e(dVar, a(a3), a2);
        }
        bVar.a(hVar, dVar.g().c(), a3);
        return bVar;
    }

    private static boolean a(double d) {
        return d < 0.9d;
    }

    private static boolean a(double d, int i) {
        return a(i) < b(d);
    }

    private static boolean a(int i, int i2, double d) {
        return i == 2 || a(d, i2);
    }

    private static int b(double d) {
        return (int) (((double) (b - (b.f2900a * 2))) / d);
    }
}
