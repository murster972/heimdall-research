package com.facebook.ads.internal.view.f;

import android.database.ContentObserver;
import android.os.Handler;

class e extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private final c f3039a;

    e(Handler handler, c cVar) {
        super(handler);
        this.f3039a = cVar;
    }

    public boolean deliverSelfNotifications() {
        return false;
    }

    public void onChange(boolean z) {
        this.f3039a.e();
    }
}
