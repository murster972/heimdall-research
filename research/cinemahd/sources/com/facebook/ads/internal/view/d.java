package com.facebook.ads.internal.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.view.View;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.q.a.x;

public class d extends View {

    /* renamed from: a  reason: collision with root package name */
    private Paint f2911a;
    private Paint b;
    private Paint c;
    private int d;
    private boolean e;

    public d(Context context) {
        this(context, 60, true);
    }

    public d(Context context, int i, boolean z) {
        super(context);
        this.d = i;
        this.e = z;
        if (z) {
            this.f2911a = new Paint();
            this.f2911a.setColor(-3355444);
            this.f2911a.setStyle(Paint.Style.STROKE);
            this.f2911a.setStrokeWidth(3.0f);
            this.f2911a.setAntiAlias(true);
            this.b = new Paint();
            this.b.setColor(-1287371708);
            this.b.setStyle(Paint.Style.FILL);
            this.b.setAntiAlias(true);
            this.c = new Paint();
            this.c.setColor(-1);
            this.c.setStyle(Paint.Style.STROKE);
            this.c.setStrokeWidth(6.0f);
            this.c.setAntiAlias(true);
        }
        a();
    }

    private void a() {
        float f = x.b;
        int i = this.d;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams((int) (((float) i) * f), (int) (((float) i) * f));
        layoutParams.addRule(10);
        layoutParams.addRule(11);
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.e) {
            if (canvas.isHardwareAccelerated() && Build.VERSION.SDK_INT < 17) {
                setLayerType(1, (Paint) null);
            }
            int min = Math.min(canvas.getWidth(), canvas.getHeight());
            int i = min / 2;
            int i2 = (i * 2) / 3;
            float f = (float) i;
            canvas.drawCircle(f, f, (float) i2, this.f2911a);
            canvas.drawCircle(f, f, (float) (i2 - 2), this.b);
            int i3 = min / 3;
            float f2 = (float) i3;
            float f3 = (float) (i3 * 2);
            Canvas canvas2 = canvas;
            float f4 = f2;
            float f5 = f3;
            canvas2.drawLine(f2, f4, f3, f5, this.c);
            canvas2.drawLine(f3, f4, f2, f5, this.c);
        }
        super.onDraw(canvas);
    }
}
