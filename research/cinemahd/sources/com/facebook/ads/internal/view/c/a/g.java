package com.facebook.ads.internal.view.c.a;

import android.text.TextUtils;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.k;
import com.facebook.ads.internal.q.a.u;
import com.facebook.ads.internal.r.a;
import com.facebook.ads.internal.view.component.a.a.b;
import java.util.Map;

class g extends RecyclerView.ViewHolder {

    /* renamed from: a  reason: collision with root package name */
    private final b f2886a;
    private final int b;
    private final int c;
    private final int d;
    private final int e;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public a g;
    private a.C0024a h;
    /* access modifiers changed from: private */
    public a i;

    g(b bVar, a aVar, int i2, int i3, int i4, int i5) {
        super(bVar);
        this.f2886a = bVar;
        this.i = aVar;
        this.b = i2;
        this.c = i3;
        this.d = i4;
        this.e = i5;
    }

    private String a(com.facebook.ads.internal.d.b bVar, String str) {
        String b2 = (bVar == null || str == null) ? "" : bVar.b(str);
        return !TextUtils.isEmpty(b2) ? b2 : str;
    }

    private void a(c cVar, u uVar, String str, final b bVar) {
        if (!this.f) {
            a aVar = this.g;
            if (aVar != null) {
                aVar.c();
                this.g = null;
            }
            final Map<String, String> a2 = bVar.a();
            final String str2 = str;
            final u uVar2 = uVar;
            final c cVar2 = cVar;
            this.h = new a.C0024a() {
                public void a() {
                    if (!g.this.i.b() && !TextUtils.isEmpty(str2)) {
                        if (g.this.g != null) {
                            g.this.g.a((Map<String, String>) a2);
                        }
                        a2.put("touch", k.a(uVar2.e()));
                        cVar2.a(str2, a2);
                    }
                    boolean unused = g.this.f = true;
                }
            };
            this.g = new a(this.f2886a, 10, this.h);
            this.g.a(100);
            this.g.b(100);
            this.f2886a.setOnAssetsLoadedListener(new b.a() {
                public void a() {
                    if (bVar.b() == 0) {
                        g.this.i.a();
                    }
                    g.this.g.a();
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void a(b bVar, c cVar, com.facebook.ads.internal.d.b bVar2, u uVar, String str, boolean z) {
        int b2 = bVar.b();
        this.f2886a.setTag(-1593835536, Integer.valueOf(b2));
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(this.b, -2);
        marginLayoutParams.setMargins(b2 == 0 ? this.c : this.d, 0, b2 >= this.e + -1 ? this.c : this.d, 0);
        String g2 = bVar.c().c().g();
        String a2 = bVar.c().c().a();
        this.f2886a.setIsVideo(!TextUtils.isEmpty(a2));
        if (this.f2886a.f()) {
            this.f2886a.setVideoPlaceholderUrl(g2);
            this.f2886a.setVideoUrl(a(bVar2, a2));
            if (z) {
                this.f2886a.h();
            }
        } else {
            this.f2886a.setImageUrl(g2);
        }
        this.f2886a.setLayoutParams(marginLayoutParams);
        this.f2886a.a(bVar.c().a().a(), bVar.c().a().c());
        this.f2886a.a(bVar.c().b().b(), bVar.c().b().a(), bVar.a());
        this.f2886a.a(bVar.a());
        a(cVar, uVar, str, bVar);
    }
}
