package com.facebook.ads.internal.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.a.k;
import com.facebook.ads.internal.q.a.u;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.r.a;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.e.b;
import com.facebook.ads.internal.view.f;
import com.facebook.ads.internal.view.f.a;
import com.facebook.ads.internal.view.f.b.c;
import com.facebook.ads.internal.view.f.b.e;
import com.facebook.ads.internal.view.f.b.m;
import com.facebook.ads.internal.view.f.b.n;
import com.facebook.ads.internal.view.f.b.z;
import com.facebook.ads.internal.view.f.c.d;
import com.facebook.ads.internal.view.f.c.f;
import com.facebook.ads.internal.view.f.c.j;
import com.facebook.ads.internal.view.f.c.l;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class o extends RelativeLayout implements a {

    /* renamed from: a  reason: collision with root package name */
    static final /* synthetic */ boolean f3069a = (!o.class.desiredAssertionStatus());
    private static final int b;
    private static final int c;
    private static final int d;
    private static final int e;
    private static final int f;
    private static final int g;
    private static final int h;
    private static final int i;
    private static final RelativeLayout.LayoutParams j = new RelativeLayout.LayoutParams(-1, -1);
    private Context A;
    /* access modifiers changed from: private */
    public a B;
    /* access modifiers changed from: private */
    public a.C0026a C;
    private com.facebook.ads.internal.view.e.a D;
    /* access modifiers changed from: private */
    public d E;
    private l F;
    /* access modifiers changed from: private */
    public j G;
    private f H;
    /* access modifiers changed from: private */
    public b I;
    /* access modifiers changed from: private */
    public boolean J = false;
    private final AudienceNetworkActivity.BackButtonInterceptor k = new AudienceNetworkActivity.BackButtonInterceptor() {
        public boolean interceptBackButton() {
            return !o.this.J;
        }
    };
    private final c l = new c() {
        public void a(com.facebook.ads.internal.view.f.b.b bVar) {
            if (o.this.C != null) {
                o.this.I.d();
                o.this.c();
                o.this.C.a(z.REWARDED_VIDEO_COMPLETE.a(), bVar);
            }
        }
    };
    private final e m = new e() {
        public void a(com.facebook.ads.internal.view.f.b.d dVar) {
            if (o.this.C != null) {
                o.this.C.a(z.REWARDED_VIDEO_ERROR.a());
            }
            o.this.a();
        }
    };
    private final m n = new m() {
        public void a(com.facebook.ads.internal.view.f.b.l lVar) {
            if (o.this.B != null) {
                o.this.B.a(com.facebook.ads.internal.view.f.a.a.USER_STARTED);
                o.this.r.a();
                o.this.z.set(o.this.B.j());
                o.this.f();
            }
        }
    };
    private final com.facebook.ads.internal.view.f.b.o o = new com.facebook.ads.internal.view.f.b.o() {
        public void a(n nVar) {
            if (o.this.B != null && o.this.E != null && o.this.B.getDuration() - o.this.B.getCurrentPositionInMillis() <= 3000 && o.this.E.a()) {
                o.this.E.b();
            }
        }
    };
    /* access modifiers changed from: private */
    public final k p;
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.m.c q;
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.r.a r;
    private final a.C0024a s;
    /* access modifiers changed from: private */
    public final u t = new u();
    private final com.facebook.ads.internal.view.f.c.o u;
    private final com.facebook.ads.internal.view.f.b v;
    private final RelativeLayout w;
    private final f x;
    private final com.facebook.ads.internal.adapters.a.d y;
    /* access modifiers changed from: private */
    public final AtomicBoolean z = new AtomicBoolean(false);

    /* renamed from: com.facebook.ads.internal.view.o$9  reason: invalid class name */
    static /* synthetic */ class AnonymousClass9 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f3078a = new int[b.a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.facebook.ads.internal.view.e.b$a[] r0 = com.facebook.ads.internal.view.e.b.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f3078a = r0
                int[] r0 = f3078a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.view.e.b$a r1 = com.facebook.ads.internal.view.e.b.a.MARKUP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f3078a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.view.e.b$a r1 = com.facebook.ads.internal.view.e.b.a.SCREENSHOTS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f3078a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.view.e.b$a r1 = com.facebook.ads.internal.view.e.b.a.INFO     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.view.o.AnonymousClass9.<clinit>():void");
        }
    }

    static {
        float f2 = x.b;
        b = (int) (12.0f * f2);
        c = (int) (18.0f * f2);
        d = (int) (16.0f * f2);
        e = (int) (72.0f * f2);
        f = (int) (f2 * 56.0f);
        g = (int) (56.0f * f2);
        h = (int) (28.0f * f2);
        i = (int) (f2 * 20.0f);
    }

    public o(Context context, com.facebook.ads.internal.m.c cVar, com.facebook.ads.internal.view.f.a aVar, a.C0026a aVar2, k kVar) {
        super(context);
        this.A = context;
        this.C = aVar2;
        this.B = aVar;
        this.q = cVar;
        this.p = kVar;
        this.y = this.p.d().a();
        this.w = new RelativeLayout(context);
        this.u = new com.facebook.ads.internal.view.f.c.o(this.A);
        this.x = new f(this.A);
        new com.facebook.ads.internal.view.b.d(this.w, i).a().a(com.facebook.ads.internal.l.a.e(this.A)).a(this.p.e().g());
        this.s = new a.C0024a() {
            public void a() {
                if (!o.this.t.b()) {
                    o.this.t.a();
                    HashMap hashMap = new HashMap();
                    if (!TextUtils.isEmpty(o.this.p.g())) {
                        o.this.r.a((Map<String, String>) hashMap);
                        hashMap.put("touch", com.facebook.ads.internal.q.a.k.a(o.this.t.e()));
                        o.this.q.a(o.this.p.g(), hashMap);
                    }
                    if (o.this.C != null) {
                        o.this.C.a(z.REWARDED_VIDEO_IMPRESSION.a());
                    }
                }
            }
        };
        this.r = new com.facebook.ads.internal.r.a(this, 1, this.s);
        this.r.a(250);
        this.v = new com.facebook.ads.internal.view.f.b(this.A, this.q, this.B, this.p.g());
        this.I = new b(this.A, this.q, this.p, this.C, this.r, this.t);
        if (f3069a || this.B != null) {
            this.B.setVideoProgressReportIntervalMs(kVar.h());
            x.a((View) this.B, -16777216);
            this.B.getEventBus().a((T[]) new com.facebook.ads.internal.j.f[]{this.l, this.m, this.n, this.o});
            return;
        }
        throw new AssertionError();
    }

    private void b() {
        com.facebook.ads.internal.view.f.a aVar;
        com.facebook.ads.internal.view.f.a.b bVar;
        com.facebook.ads.internal.view.f.a aVar2 = this.B;
        if (aVar2 != null) {
            aVar2.d();
            this.B.a((com.facebook.ads.internal.view.f.a.b) new com.facebook.ads.internal.view.f.c.k(this.A));
            this.B.a((com.facebook.ads.internal.view.f.a.b) this.x);
            this.B.a((com.facebook.ads.internal.view.f.a.b) this.u);
            this.F = new l(this.A, true);
            d dVar = new d(this.F, d.a.FADE_OUT_ON_PLAY, true);
            this.B.a((com.facebook.ads.internal.view.f.a.b) this.F);
            this.B.a((com.facebook.ads.internal.view.f.a.b) dVar);
            this.D = new com.facebook.ads.internal.view.e.a(this.A, e, this.y, this.q, this.C, this.I.b() == b.a.INFO, this.I.b() == b.a.INFO, this.r, this.t);
            this.D.setInfo(this.p);
            this.E = new d(this.D, d.a.FADE_OUT_ON_PLAY, true);
            this.B.a((com.facebook.ads.internal.view.f.a.b) this.E);
            if (this.I.a() && this.p.e().c() > 0) {
                this.G = new j(this.A, this.p.e().c(), -12286980);
                this.G.setButtonMode(j.a.SKIP_BUTTON_MODE);
                this.G.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        if (o.this.G != null && o.this.G.a() && o.this.G.getSkipSeconds() != 0 && o.this.B != null) {
                            o.this.B.e();
                        }
                    }
                });
                aVar = this.B;
                bVar = this.G;
            } else if (!this.I.a()) {
                this.H = new f(this.A);
                this.H.a(this.p.a(), this.p.g(), this.p.e().c());
                if (this.p.e().c() <= 0) {
                    this.H.b();
                }
                if (this.I.b() != b.a.INFO) {
                    this.H.c();
                }
                this.H.setToolbarListener(new f.a() {
                    public void a() {
                        if (!o.this.J && o.this.B != null) {
                            boolean unused = o.this.J = true;
                            o.this.B.e();
                        } else if (o.this.J && o.this.C != null) {
                            o.this.C.a(z.REWARDED_VIDEO_END_ACTIVITY.a());
                        }
                    }
                });
                aVar = this.B;
                bVar = this.H;
            } else {
                return;
            }
            aVar.a(bVar);
        }
    }

    /* access modifiers changed from: private */
    public void c() {
        RelativeLayout.LayoutParams layoutParams;
        this.J = true;
        e();
        d();
        com.facebook.ads.internal.view.f.a aVar = this.B;
        if (aVar != null) {
            aVar.d();
            this.B.setVisibility(4);
        }
        f fVar = this.H;
        if (fVar != null) {
            fVar.a(true);
            this.H.c();
        }
        x.a(this.B, this.G, this.x, this.u);
        Pair<b.a, View> c2 = this.I.c();
        int i2 = AnonymousClass9.f3078a[((b.a) c2.first).ordinal()];
        if (i2 != 1) {
            if (i2 == 2) {
                com.facebook.ads.internal.view.e.a aVar2 = this.D;
                if (aVar2 != null) {
                    aVar2.setVisibility(0);
                    this.D.a();
                }
                layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams.setMargins(0, g, 0, 0);
                layoutParams.addRule(2, this.D.getId());
            } else if (i2 == 3) {
                x.a(this.D);
                layoutParams = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams.addRule(15);
                int i3 = d;
                layoutParams.setMargins(i3, i3, i3, i3);
            } else {
                return;
            }
            this.w.addView((View) c2.second, layoutParams);
            this.t.a();
            return;
        }
        x.a(this.D);
        this.w.addView((View) c2.second, j);
    }

    private void d() {
        if (Build.VERSION.SDK_INT > 19) {
            AutoTransition autoTransition = new AutoTransition();
            autoTransition.setDuration(200);
            autoTransition.setInterpolator(new AccelerateDecelerateInterpolator());
            TransitionManager.beginDelayedTransition(this.w, autoTransition);
        }
    }

    private void e() {
        Context context = this.A;
        if (context != null) {
            FrameLayout frameLayout = new FrameLayout(context);
            frameLayout.setLayoutParams(j);
            x.a((View) frameLayout, -1509949440);
            this.w.addView(frameLayout, 0);
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        this.x.setVisibility(this.z.get() ? 0 : 8);
    }

    private void setUpContentLayoutForVideo(int i2) {
        this.w.removeAllViews();
        this.w.addView(this.B, j);
        com.facebook.ads.internal.view.e.a aVar = this.D;
        if (aVar != null) {
            x.a((View) aVar);
            this.D.a(i2);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams.addRule(12);
            com.facebook.ads.internal.view.e.a aVar2 = this.D;
            int i3 = d;
            aVar2.setPadding(i3, i3, i3, i3);
            this.w.addView(this.D, layoutParams);
        }
        if (this.G != null) {
            int i4 = f;
            RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i4, i4);
            layoutParams2.addRule(10);
            layoutParams2.addRule(11);
            j jVar = this.G;
            int i5 = d;
            jVar.setPadding(i5, i5, i5, i5);
            this.w.addView(this.G, layoutParams2);
        }
        int i6 = h;
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(i6, i6);
        layoutParams3.addRule(10);
        layoutParams3.addRule(11);
        int i7 = b;
        layoutParams3.setMargins(i7, g + i7, i7, c);
        this.w.addView(this.x, layoutParams3);
        f();
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(12);
        this.w.addView(this.u, layoutParams4);
    }

    public void a() {
        com.facebook.ads.internal.view.f.a aVar = this.B;
        if (aVar != null) {
            aVar.f();
            this.B.k();
        }
        com.facebook.ads.internal.r.a aVar2 = this.r;
        if (aVar2 != null) {
            aVar2.c();
        }
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        if (this.B != null && this.C != null) {
            b();
            audienceNetworkActivity.addBackButtonInterceptor(this.k);
            this.B.setVideoURI(!TextUtils.isEmpty(this.p.e().b()) ? this.p.e().b() : this.p.e().a());
            setUpContentLayoutForVideo(audienceNetworkActivity.getResources().getConfiguration().orientation);
            addView(this.w, j);
            f fVar = this.H;
            if (fVar != null) {
                x.a((View) fVar);
                this.H.a(this.y, true);
                addView(this.H, new RelativeLayout.LayoutParams(-1, g));
            }
            setLayoutParams(j);
            this.C.a((View) this);
        }
    }

    public void a(Bundle bundle) {
    }

    public int getCurrentPosition() {
        com.facebook.ads.internal.view.f.a aVar = this.B;
        if (aVar != null) {
            return aVar.getCurrentPositionInMillis();
        }
        return 0;
    }

    public void i() {
        com.facebook.ads.internal.view.f.a aVar = this.B;
        if (aVar != null) {
            aVar.a(false);
        }
    }

    public void j() {
        com.facebook.ads.internal.view.f.a aVar = this.B;
        if (aVar != null && this.C != null && aVar.l() && !this.B.m()) {
            this.B.a(com.facebook.ads.internal.view.f.a.a.USER_STARTED);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        com.facebook.ads.internal.view.e.a aVar = this.D;
        if (aVar != null) {
            aVar.a(configuration.orientation);
        }
    }

    public void onDestroy() {
        a();
        com.facebook.ads.internal.view.f.a aVar = this.B;
        if (aVar != null) {
            aVar.getEventBus().b((T[]) new com.facebook.ads.internal.j.f[]{this.l, this.m, this.n, this.o});
        }
        if (!TextUtils.isEmpty(this.p.g())) {
            HashMap hashMap = new HashMap();
            this.r.a((Map<String, String>) hashMap);
            hashMap.put("touch", com.facebook.ads.internal.q.a.k.a(this.t.e()));
            this.q.i(this.p.g(), hashMap);
        }
        f fVar = this.H;
        if (fVar != null) {
            fVar.setToolbarListener((f.a) null);
        }
        this.v.a();
        this.B = null;
        this.I.e();
        this.G = null;
        this.D = null;
        this.E = null;
        this.C = null;
        this.A = null;
        this.u.a();
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.t.a(motionEvent, this, this);
        return super.onInterceptTouchEvent(motionEvent);
    }

    /* access modifiers changed from: package-private */
    public void setEndCardController(b bVar) {
        this.I = bVar;
    }

    public void setListener(a.C0026a aVar) {
    }
}
