package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.view.View;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.q.a.x;

public class f extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private static final int f2909a = ((int) (x.b * 4.0f));
    private final Path b = new Path();
    private final RectF c = new RectF();

    public f(Context context) {
        super(context);
        x.a((View) this, 0);
        if (Build.VERSION.SDK_INT < 18) {
            setLayerType(1, (Paint) null);
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.c.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
        this.b.reset();
        Path path = this.b;
        RectF rectF = this.c;
        int i = f2909a;
        path.addRoundRect(rectF, (float) i, (float) i, Path.Direction.CW);
        canvas.clipPath(this.b);
        super.onDraw(canvas);
    }
}
