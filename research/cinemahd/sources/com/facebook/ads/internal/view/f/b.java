package com.facebook.ads.internal.view.f;

import android.content.Context;
import android.os.Bundle;
import com.facebook.ads.internal.j.f;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.view.f.b.h;
import com.facebook.ads.internal.view.f.b.j;
import com.facebook.ads.internal.view.f.b.l;
import com.facebook.ads.internal.view.f.b.m;
import com.facebook.ads.internal.view.f.b.n;
import com.facebook.ads.internal.view.f.b.p;
import com.facebook.ads.internal.view.f.b.r;
import com.facebook.ads.internal.view.f.b.s;
import com.facebook.ads.internal.view.f.b.v;
import com.facebook.ads.internal.view.f.b.w;
import com.facebook.ads.internal.view.f.b.x;
import com.facebook.ads.internal.view.f.b.y;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class b extends c {

    /* renamed from: a  reason: collision with root package name */
    public int f2941a;
    /* access modifiers changed from: private */
    public final w b;
    /* access modifiers changed from: private */
    public final f<r> c;
    /* access modifiers changed from: private */
    public final f<h> d;
    /* access modifiers changed from: private */
    public final f<j> e;
    /* access modifiers changed from: private */
    public final f<n> f;
    /* access modifiers changed from: private */
    public final f<com.facebook.ads.internal.view.f.b.b> g;
    /* access modifiers changed from: private */
    public final f<p> h;
    /* access modifiers changed from: private */
    public final f<x> i;
    /* access modifiers changed from: private */
    public final f<y> j;
    /* access modifiers changed from: private */
    public final f<s> k;
    /* access modifiers changed from: private */
    public final m l;
    /* access modifiers changed from: private */
    public final a m;
    /* access modifiers changed from: private */
    public boolean n;

    public b(Context context, c cVar, a aVar, String str) {
        this(context, cVar, aVar, (List<com.facebook.ads.internal.b.b>) new ArrayList(), str);
    }

    public b(Context context, c cVar, a aVar, String str, Bundle bundle) {
        this(context, cVar, aVar, new ArrayList(), str, bundle, (Map<String, String>) null);
    }

    public b(Context context, c cVar, a aVar, String str, Map<String, String> map) {
        this(context, cVar, aVar, new ArrayList(), str, (Bundle) null, map);
    }

    public b(Context context, c cVar, a aVar, List<com.facebook.ads.internal.b.b> list, String str) {
        super(context, cVar, aVar, list, str);
        this.b = new w() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2942a = (!b.class.desiredAssertionStatus());

            public void a(v vVar) {
                if (f2942a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        bVar.e();
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        };
        this.c = new f<r>() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2949a = (!b.class.desiredAssertionStatus());

            public Class<r> a() {
                return r.class;
            }

            public void a(r rVar) {
                if (f2949a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        bVar.f();
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        };
        this.d = new f<h>() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2950a = (!b.class.desiredAssertionStatus());

            public Class<h> a() {
                return h.class;
            }

            public void a(h hVar) {
                if (f2950a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        bVar.h();
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        };
        this.e = new f<j>() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2951a = (!b.class.desiredAssertionStatus());

            public Class<j> a() {
                return j.class;
            }

            public void a(j jVar) {
                if (f2951a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        if (!bVar.n) {
                            boolean unused = b.this.n = true;
                        } else {
                            b.this.i();
                        }
                    }
                } else {
                    throw new AssertionError();
                }
            }
        };
        this.f = new f<n>() {
            public Class<n> a() {
                return n.class;
            }

            public void a(n nVar) {
                int a2 = nVar.a();
                b bVar = b.this;
                if (bVar.f2941a <= 0 || a2 != bVar.m.getDuration() || b.this.m.getDuration() <= b.this.f2941a) {
                    b.this.a(a2);
                }
            }
        };
        this.g = new f<com.facebook.ads.internal.view.f.b.b>() {
            public Class<com.facebook.ads.internal.view.f.b.b> a() {
                return com.facebook.ads.internal.view.f.b.b.class;
            }

            public void a(com.facebook.ads.internal.view.f.b.b bVar) {
                b bVar2;
                int a2 = bVar.a();
                int b = bVar.b();
                int i = b.this.f2941a;
                if (i <= 0 || a2 != b || b <= i) {
                    if (b >= a2 + 500) {
                        bVar2 = b.this;
                    } else if (b == 0) {
                        bVar2 = b.this;
                        a2 = bVar2.f2941a;
                    } else {
                        b.this.b(b);
                        return;
                    }
                    bVar2.b(a2);
                }
            }
        };
        this.h = new f<p>() {
            public Class<p> a() {
                return p.class;
            }

            public void a(p pVar) {
                b.this.a(pVar.a(), pVar.b());
            }
        };
        this.i = new f<x>() {
            public Class<x> a() {
                return x.class;
            }

            public void a(x xVar) {
                b.this.b();
            }
        };
        this.j = new f<y>() {
            public Class<y> a() {
                return y.class;
            }

            public void a(y yVar) {
                b.this.c();
            }
        };
        this.k = new f<s>() {
            public Class<s> a() {
                return s.class;
            }

            public void a(s sVar) {
                b bVar = b.this;
                bVar.a(bVar.j(), b.this.j());
            }
        };
        this.l = new m() {
            public void a(l lVar) {
                b bVar = b.this;
                bVar.f2941a = bVar.m.getDuration();
            }
        };
        this.n = false;
        this.m = aVar;
        this.m.getEventBus().a((T[]) new f[]{this.b, this.f, this.c, this.e, this.d, this.g, this.h, this.i, this.j, this.l, this.k});
    }

    public b(Context context, c cVar, a aVar, List<com.facebook.ads.internal.b.b> list, String str, Bundle bundle, Map<String, String> map) {
        super(context, cVar, aVar, list, str, bundle, map);
        this.b = new w() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2942a = (!b.class.desiredAssertionStatus());

            public void a(v vVar) {
                if (f2942a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        bVar.e();
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        };
        this.c = new f<r>() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2949a = (!b.class.desiredAssertionStatus());

            public Class<r> a() {
                return r.class;
            }

            public void a(r rVar) {
                if (f2949a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        bVar.f();
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        };
        this.d = new f<h>() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2950a = (!b.class.desiredAssertionStatus());

            public Class<h> a() {
                return h.class;
            }

            public void a(h hVar) {
                if (f2950a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        bVar.h();
                        return;
                    }
                    return;
                }
                throw new AssertionError();
            }
        };
        this.e = new f<j>() {

            /* renamed from: a  reason: collision with root package name */
            static final /* synthetic */ boolean f2951a = (!b.class.desiredAssertionStatus());

            public Class<j> a() {
                return j.class;
            }

            public void a(j jVar) {
                if (f2951a || b.this != null) {
                    b bVar = b.this;
                    if (bVar != null) {
                        if (!bVar.n) {
                            boolean unused = b.this.n = true;
                        } else {
                            b.this.i();
                        }
                    }
                } else {
                    throw new AssertionError();
                }
            }
        };
        this.f = new f<n>() {
            public Class<n> a() {
                return n.class;
            }

            public void a(n nVar) {
                int a2 = nVar.a();
                b bVar = b.this;
                if (bVar.f2941a <= 0 || a2 != bVar.m.getDuration() || b.this.m.getDuration() <= b.this.f2941a) {
                    b.this.a(a2);
                }
            }
        };
        this.g = new f<com.facebook.ads.internal.view.f.b.b>() {
            public Class<com.facebook.ads.internal.view.f.b.b> a() {
                return com.facebook.ads.internal.view.f.b.b.class;
            }

            public void a(com.facebook.ads.internal.view.f.b.b bVar) {
                b bVar2;
                int a2 = bVar.a();
                int b = bVar.b();
                int i = b.this.f2941a;
                if (i <= 0 || a2 != b || b <= i) {
                    if (b >= a2 + 500) {
                        bVar2 = b.this;
                    } else if (b == 0) {
                        bVar2 = b.this;
                        a2 = bVar2.f2941a;
                    } else {
                        b.this.b(b);
                        return;
                    }
                    bVar2.b(a2);
                }
            }
        };
        this.h = new f<p>() {
            public Class<p> a() {
                return p.class;
            }

            public void a(p pVar) {
                b.this.a(pVar.a(), pVar.b());
            }
        };
        this.i = new f<x>() {
            public Class<x> a() {
                return x.class;
            }

            public void a(x xVar) {
                b.this.b();
            }
        };
        this.j = new f<y>() {
            public Class<y> a() {
                return y.class;
            }

            public void a(y yVar) {
                b.this.c();
            }
        };
        this.k = new f<s>() {
            public Class<s> a() {
                return s.class;
            }

            public void a(s sVar) {
                b bVar = b.this;
                bVar.a(bVar.j(), b.this.j());
            }
        };
        this.l = new m() {
            public void a(l lVar) {
                b bVar = b.this;
                bVar.f2941a = bVar.m.getDuration();
            }
        };
        this.n = false;
        this.m = aVar;
        this.m.getEventBus().a((T[]) new f[]{this.b, this.f, this.c, this.e, this.d, this.g, this.h, this.i, this.j, this.k});
    }

    public void a() {
        this.m.getStateHandler().post(new Runnable() {
            public void run() {
                b.this.m.getEventBus().b((T[]) new f[]{b.this.b, b.this.f, b.this.c, b.this.e, b.this.d, b.this.g, b.this.h, b.this.i, b.this.j, b.this.l, b.this.k});
            }
        });
    }
}
