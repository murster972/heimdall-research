package com.facebook.ads.internal.view;

import android.content.Context;
import android.widget.RelativeLayout;

public class r extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private int f3081a = 0;
    private int b = 0;

    public r(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        int i3;
        super.onMeasure(i, i2);
        if ((this.b > 0 && getMeasuredWidth() > (i3 = this.b)) || getMeasuredWidth() < (i3 = this.f3081a)) {
            setMeasuredDimension(i3, getMeasuredHeight());
        }
    }

    public void setMaxWidth(int i) {
        this.b = i;
    }

    public void setMinWidth(int i) {
        this.f3081a = i;
    }
}
