package com.facebook.ads.internal.view.a;

import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.ConsoleMessage;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.common.util.UriUtil;
import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Set;

@TargetApi(19)
public class f extends com.facebook.ads.internal.q.c.a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f2856a = f.class.getSimpleName();
    /* access modifiers changed from: private */
    public static final Set<String> b = new HashSet(2);
    private a c;
    private d d;
    private long e = -1;
    private long f = -1;
    private long g = -1;
    private long h = -1;

    public interface a {
        void a(int i);

        void a(String str);

        void b(String str);

        void c(String str);
    }

    static class b extends WebChromeClient {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<a> f2857a;
        private final WeakReference<d> b;

        b(WeakReference<a> weakReference, WeakReference<d> weakReference2) {
            this.f2857a = weakReference;
            this.b = weakReference2;
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            String message = consoleMessage.message();
            if (TextUtils.isEmpty(message) || consoleMessage.messageLevel() != ConsoleMessage.MessageLevel.LOG || this.b.get() == null) {
                return true;
            }
            ((d) this.b.get()).a(message);
            return true;
        }

        public void onProgressChanged(WebView webView, int i) {
            super.onProgressChanged(webView, i);
            if (this.b.get() != null) {
                ((d) this.b.get()).a();
            }
            if (this.f2857a.get() != null) {
                ((a) this.f2857a.get()).a(i);
            }
        }

        public void onReceivedTitle(WebView webView, String str) {
            super.onReceivedTitle(webView, str);
            if (this.f2857a.get() != null) {
                ((a) this.f2857a.get()).b(str);
            }
        }
    }

    static class c extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<a> f2858a;
        private final WeakReference<Context> b;

        c(WeakReference<a> weakReference, WeakReference<Context> weakReference2) {
            this.f2858a = weakReference;
            this.b = weakReference2;
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (this.f2858a.get() != null) {
                ((a) this.f2858a.get()).c(str);
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (this.f2858a.get() != null) {
                ((a) this.f2858a.get()).a(str);
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Uri parse = Uri.parse(str);
            if (f.b.contains(parse.getScheme()) || this.b.get() == null) {
                return false;
            }
            try {
                ((Context) this.b.get()).startActivity(new Intent("android.intent.action.VIEW", parse));
                return true;
            } catch (ActivityNotFoundException e) {
                Log.w(f.f2856a, "Activity not found to handle URI.", e);
                return false;
            } catch (Exception e2) {
                Log.e(f.f2856a, "Unknown exception occurred when trying to handle URI.", e2);
                return false;
            }
        }
    }

    static {
        b.add(UriUtil.HTTP_SCHEME);
        b.add(UriUtil.HTTPS_SCHEME);
    }

    public f(Context context) {
        super(context);
        f();
    }

    private void f() {
        WebSettings settings = getSettings();
        settings.setSupportZoom(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccessFromFileURLs(false);
        settings.setAllowUniversalAccessFromFileURLs(false);
        settings.setAllowFileAccess(false);
        this.d = new d(this);
    }

    private void g() {
        if (this.f > -1 && this.g > -1 && this.h > -1) {
            this.d.a(false);
        }
    }

    /* access modifiers changed from: protected */
    public WebChromeClient a() {
        return new b(new WeakReference(this.c), new WeakReference(this.d));
    }

    public void a(long j) {
        if (this.e < 0) {
            this.e = j;
        }
    }

    public void a(String str) {
        try {
            evaluateJavascript(str, (ValueCallback) null);
        } catch (IllegalStateException unused) {
            loadUrl("javascript:" + str);
        }
    }

    /* access modifiers changed from: protected */
    public WebViewClient b() {
        return new c(new WeakReference(this.c), new WeakReference(getContext()));
    }

    public void b(long j) {
        if (this.f < 0) {
            this.f = j;
        }
        g();
    }

    public void c(long j) {
        if (this.h < 0) {
            this.h = j;
        }
        g();
    }

    public void destroy() {
        this.c = null;
        com.facebook.ads.internal.q.c.b.a(this);
        super.destroy();
    }

    public long getDomContentLoadedMs() {
        return this.f;
    }

    public String getFirstUrl() {
        WebBackForwardList copyBackForwardList = copyBackForwardList();
        return copyBackForwardList.getSize() > 0 ? copyBackForwardList.getItemAtIndex(0).getUrl() : getUrl();
    }

    public long getLoadFinishMs() {
        return this.h;
    }

    public long getResponseEndMs() {
        return this.e;
    }

    public long getScrollReadyMs() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.g < 0 && computeVerticalScrollRange() > getHeight()) {
            this.g = System.currentTimeMillis();
            g();
        }
    }

    public void setListener(a aVar) {
        this.c = aVar;
    }
}
