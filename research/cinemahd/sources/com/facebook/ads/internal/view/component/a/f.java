package com.facebook.ads.internal.view.component.a;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.view.component.h;

final class f extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private final View f2904a;
    private final com.facebook.ads.internal.view.component.f b;

    public f(Context context, View view) {
        super(context);
        this.f2904a = view;
        this.b = new com.facebook.ads.internal.view.component.f(context);
        x.a((View) this.b);
    }

    public void a(int i) {
        this.f2904a.setLayoutParams(new RelativeLayout.LayoutParams(-1, i));
    }

    public void a(View view, View view2, h hVar, boolean z) {
        this.b.addView(this.f2904a, new RelativeLayout.LayoutParams(-1, -2));
        if (view2 != null) {
            int i = b.b;
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(i, i);
            layoutParams.addRule(6, this.f2904a.getId());
            layoutParams.addRule(7, this.f2904a.getId());
            int i2 = b.f2900a;
            layoutParams.setMargins(i2, i2, i2, i2);
            this.b.addView(view2, layoutParams);
        }
        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams2.addRule(8, this.f2904a.getId());
        if (hVar != null) {
            if (z) {
                LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(-1, -2);
                hVar.setAlignment(3);
                int i3 = b.f2900a;
                layoutParams3.setMargins(i3 / 2, i3 / 2, i3 / 2, i3 / 2);
                linearLayout.addView(hVar, layoutParams3);
                GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{-1778384896, 0});
                gradientDrawable.setCornerRadius(0.0f);
                gradientDrawable.setGradientType(0);
                x.a((View) linearLayout, (Drawable) gradientDrawable);
            } else {
                RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
                layoutParams4.addRule(3, this.b.getId());
                layoutParams4.setMargins(0, b.f2900a, 0, 0);
                hVar.setAlignment(17);
                addView(hVar, layoutParams4);
            }
        }
        if (view != null) {
            linearLayout.addView(view, new RelativeLayout.LayoutParams(-1, -2));
        }
        this.b.addView(linearLayout, layoutParams2);
        addView(this.b, new RelativeLayout.LayoutParams(-1, -2));
    }
}
