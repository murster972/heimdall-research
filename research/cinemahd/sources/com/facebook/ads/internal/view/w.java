package com.facebook.ads.internal.view;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

public class w extends View {

    /* renamed from: a  reason: collision with root package name */
    private v f3089a;

    public w(Context context, v vVar) {
        super(context);
        this.f3089a = vVar;
        setLayoutParams(new ViewGroup.LayoutParams(0, 0));
    }

    public void onWindowVisibilityChanged(int i) {
        v vVar = this.f3089a;
        if (vVar != null) {
            vVar.a(i);
        }
    }
}
