package com.facebook.ads.internal.view.f.b;

import android.view.MotionEvent;
import android.view.View;
import com.facebook.ads.internal.j.d;

public class t extends d {

    /* renamed from: a  reason: collision with root package name */
    private final View f2958a;
    private final MotionEvent b;

    public t(View view, MotionEvent motionEvent) {
        this.f2958a = view;
        this.b = motionEvent;
    }

    public MotionEvent a() {
        return this.b;
    }
}
