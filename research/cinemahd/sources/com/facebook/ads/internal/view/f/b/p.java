package com.facebook.ads.internal.view.f.b;

import com.facebook.ads.internal.j.d;

public class p extends d {

    /* renamed from: a  reason: collision with root package name */
    private final int f2957a;
    private final int b;

    public p(int i, int i2) {
        this.f2957a = i;
        this.b = i2;
    }

    public int a() {
        return this.f2957a;
    }

    public int b() {
        return this.b;
    }
}
