package com.facebook.ads.internal.view.f.c;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.View;
import android.widget.ImageView;
import com.facebook.ads.internal.q.b.c;
import com.facebook.ads.internal.view.f.a;
import com.facebook.ads.internal.view.f.a.b;
import com.facebook.ads.internal.view.f.b.v;
import com.facebook.ads.internal.view.f.b.w;

public class f extends ImageView implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final int f2991a = ((int) (Resources.getSystem().getDisplayMetrics().density * 4.0f));
    private final Paint b = new Paint();
    /* access modifiers changed from: private */
    public a c;
    private final w d = new w() {
        public void a(v vVar) {
            f.this.a();
        }
    };

    public f(Context context) {
        super(context);
        this.b.setColor(-1728053248);
        setColorFilter(-1);
        int i = f2991a;
        setPadding(i, i, i, i);
        c();
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                a aVar;
                float f;
                if (f.this.c != null) {
                    if (f.this.b()) {
                        aVar = f.this.c;
                        f = 1.0f;
                    } else {
                        aVar = f.this.c;
                        f = 0.0f;
                    }
                    aVar.setVolume(f);
                    f.this.a();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean b() {
        a aVar = this.c;
        return aVar != null && aVar.getVolume() == 0.0f;
    }

    private void c() {
        setImageBitmap(c.a(com.facebook.ads.internal.q.b.b.SOUND_ON));
    }

    private void d() {
        setImageBitmap(c.a(com.facebook.ads.internal.q.b.b.SOUND_OFF));
    }

    public final void a() {
        if (this.c != null) {
            if (b()) {
                d();
            } else {
                c();
            }
        }
    }

    public void a(a aVar) {
        this.c = aVar;
        a aVar2 = this.c;
        if (aVar2 != null) {
            aVar2.getEventBus().a(this.d);
        }
    }

    public void b(a aVar) {
        a aVar2 = this.c;
        if (aVar2 != null) {
            aVar2.getEventBus().b(this.d);
        }
        this.c = null;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int width = getWidth() / 2;
        int height = getHeight() / 2;
        canvas.drawCircle((float) width, (float) height, (float) Math.min(width, height), this.b);
        super.onDraw(canvas);
    }
}
