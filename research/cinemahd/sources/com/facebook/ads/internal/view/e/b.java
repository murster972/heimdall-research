package com.facebook.ads.internal.view.e;

import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.a.d;
import com.facebook.ads.internal.adapters.a.k;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.u;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.q.c.e;
import com.facebook.ads.internal.q.c.f;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.b.a;
import com.facebook.ads.internal.view.component.e;
import com.facebook.ads.internal.view.component.h;
import com.facebook.ads.internal.view.f.b.z;
import com.vungle.warren.ui.JavascriptBridge;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executor;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f2917a = "b";
    private static final int b;
    private static final int c;
    private static final int d;
    /* access modifiers changed from: private */
    public final Context e;
    /* access modifiers changed from: private */
    public final c f;
    /* access modifiers changed from: private */
    public final k g;
    private final String h;
    private final d i;
    private final com.facebook.ads.internal.r.a j;
    private final u k;
    private Executor l = AsyncTask.THREAD_POOL_EXECUTOR;
    /* access modifiers changed from: private */
    public a.C0026a m;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.view.b.a n;
    private a.b o;

    /* renamed from: com.facebook.ads.internal.view.e.b$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2921a = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.facebook.ads.internal.view.e.b$a[] r0 = com.facebook.ads.internal.view.e.b.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2921a = r0
                int[] r0 = f2921a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.view.e.b$a r1 = com.facebook.ads.internal.view.e.b.a.MARKUP     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2921a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.view.e.b$a r1 = com.facebook.ads.internal.view.e.b.a.SCREENSHOTS     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2921a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.view.e.b$a r1 = com.facebook.ads.internal.view.e.b.a.INFO     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.view.e.b.AnonymousClass3.<clinit>():void");
        }
    }

    public enum a {
        SCREENSHOTS,
        MARKUP,
        INFO
    }

    static {
        float f2 = x.b;
        b = (int) (4.0f * f2);
        c = (int) (72.0f * f2);
        d = (int) (f2 * 8.0f);
    }

    public b(Context context, c cVar, k kVar, a.C0026a aVar, com.facebook.ads.internal.r.a aVar2, u uVar) {
        this.e = context;
        this.f = cVar;
        this.g = kVar;
        this.m = aVar;
        this.h = com.facebook.ads.internal.j.c.a(this.g.f().b());
        this.i = this.g.d().a();
        this.j = aVar2;
        this.k = uVar;
    }

    /* access modifiers changed from: private */
    public void g() {
        a.C0026a aVar = this.m;
        if (aVar != null) {
            aVar.a(z.REWARDED_VIDEO_END_ACTIVITY.a());
        }
    }

    private View h() {
        h hVar = new h(this.e, this.i, true, false, false);
        hVar.a(this.g.b().a(), this.g.b().c(), false, true);
        hVar.setAlignment(17);
        com.facebook.ads.internal.view.component.a aVar = new com.facebook.ads.internal.view.component.a(this.e, true, false, z.REWARDED_VIDEO_AD_CLICK.a(), this.i, this.f, this.m, this.j, this.k);
        aVar.a(this.g.c(), this.g.g(), new HashMap());
        e eVar = new e(this.e);
        x.a((View) eVar, 0);
        eVar.setRadius(50);
        new com.facebook.ads.internal.view.b.d((ImageView) eVar).a().a(this.g.a().b());
        LinearLayout linearLayout = new LinearLayout(this.e);
        linearLayout.setOrientation(1);
        linearLayout.setGravity(17);
        int i2 = c;
        linearLayout.addView(eVar, new LinearLayout.LayoutParams(i2, i2));
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
        int i3 = d;
        layoutParams.setMargins(0, i3, 0, i3);
        linearLayout.addView(hVar, layoutParams);
        linearLayout.addView(aVar, layoutParams);
        return linearLayout;
    }

    private View i() {
        RecyclerView recyclerView = new RecyclerView(this.e);
        recyclerView.setLayoutManager(new LinearLayoutManager(this.e, 0, false));
        recyclerView.setAdapter(new c(this.g.f().d(), b));
        return recyclerView;
    }

    private View j() {
        this.o = new a.c() {
            public void a() {
                if (b.this.n != null && !TextUtils.isEmpty(b.this.g.f().c())) {
                    b.this.n.post(new Runnable() {
                        public void run() {
                            if (b.this.n == null || b.this.n.c()) {
                                Log.w(b.f2917a, "Webview already destroyed, cannot activate");
                                return;
                            }
                            com.facebook.ads.internal.view.b.a f = b.this.n;
                            f.loadUrl("javascript:" + b.this.g.f().c());
                        }
                    });
                }
            }

            public void a(String str, Map<String, String> map) {
                Uri parse = Uri.parse(str);
                if (!"fbad".equals(parse.getScheme()) || !parse.getAuthority().equals(JavascriptBridge.MraidHandler.CLOSE_ACTION)) {
                    if ("fbad".equals(parse.getScheme()) && com.facebook.ads.internal.a.c.a(parse.getAuthority()) && b.this.m != null) {
                        b.this.m.a(z.REWARDED_VIDEO_AD_CLICK.a());
                    }
                    com.facebook.ads.internal.a.b a2 = com.facebook.ads.internal.a.c.a(b.this.e, b.this.f, b.this.g.g(), parse, map);
                    if (a2 != null) {
                        try {
                            a2.b();
                        } catch (Exception e) {
                            Log.e(b.f2917a, "Error executing action", e);
                        }
                    }
                } else {
                    b.this.g();
                }
            }
        };
        this.n = new com.facebook.ads.internal.view.b.a(this.e, new WeakReference(this.o), 1);
        this.n.loadDataWithBaseURL(com.facebook.ads.internal.q.c.b.a(), this.h, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, AudienceNetworkActivity.WEBVIEW_ENCODING, (String) null);
        return this.n;
    }

    public boolean a() {
        return b() == a.MARKUP;
    }

    public a b() {
        return !this.g.f().d().isEmpty() ? a.SCREENSHOTS : !TextUtils.isEmpty(this.h) ? a.MARKUP : a.INFO;
    }

    public Pair<a, View> c() {
        a b2 = b();
        int i2 = AnonymousClass3.f2921a[b2.ordinal()];
        return i2 != 1 ? i2 != 2 ? new Pair<>(b2, h()) : new Pair<>(b2, i()) : new Pair<>(b2, j());
    }

    public void d() {
        String a2 = this.g.f().a();
        if (!TextUtils.isEmpty(a2)) {
            com.facebook.ads.internal.q.c.e eVar = new com.facebook.ads.internal.q.c.e(this.e, new HashMap());
            eVar.a((e.a) new e.a() {
                public void a() {
                    if (b.this.m != null) {
                        b.this.m.a(z.REWARD_SERVER_FAILED.a());
                    }
                }

                public void a(f fVar) {
                    a.C0026a aVar;
                    z zVar;
                    if (b.this.m != null) {
                        if (fVar == null || !fVar.a()) {
                            aVar = b.this.m;
                            zVar = z.REWARD_SERVER_FAILED;
                        } else {
                            aVar = b.this.m;
                            zVar = z.REWARD_SERVER_SUCCESS;
                        }
                        aVar.a(zVar.a());
                    }
                }
            });
            eVar.executeOnExecutor(this.l, new String[]{a2});
        }
    }

    public void e() {
        com.facebook.ads.internal.view.b.a aVar = this.n;
        if (aVar != null) {
            aVar.destroy();
            this.n = null;
            this.o = null;
        }
    }
}
