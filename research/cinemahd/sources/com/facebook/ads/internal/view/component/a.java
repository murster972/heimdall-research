package com.facebook.ads.internal.view.component;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.StateListDrawable;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import androidx.core.graphics.ColorUtils;
import com.facebook.ads.internal.a.b;
import com.facebook.ads.internal.adapters.a.d;
import com.facebook.ads.internal.adapters.a.e;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.k;
import com.facebook.ads.internal.q.a.u;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.view.a;
import java.util.Locale;
import java.util.Map;

public class a extends Button {

    /* renamed from: a  reason: collision with root package name */
    public static final int f2892a;
    private static final int b;
    private final Paint c = new Paint();
    private final RectF d;
    private final boolean e;
    /* access modifiers changed from: private */
    public final String f;
    /* access modifiers changed from: private */
    public final com.facebook.ads.internal.r.a g;
    /* access modifiers changed from: private */
    public final u h;
    /* access modifiers changed from: private */
    public final c i;
    /* access modifiers changed from: private */
    public final a.C0026a j;

    static {
        float f2 = x.b;
        b = (int) (4.0f * f2);
        f2892a = (int) (f2 * 16.0f);
    }

    public a(Context context, boolean z, boolean z2, String str, d dVar, c cVar, a.C0026a aVar, com.facebook.ads.internal.r.a aVar2, u uVar) {
        super(context);
        this.i = cVar;
        this.j = aVar;
        this.e = z;
        this.f = str;
        this.g = aVar2;
        this.h = uVar;
        x.a(this, false, 16);
        setGravity(17);
        int i2 = f2892a;
        setPadding(i2, i2, i2, i2);
        setTextColor(dVar.f(z2));
        int e2 = dVar.e(z2);
        int a2 = ColorUtils.a(e2, -16777216, 0.1f);
        this.c.setStyle(Paint.Style.FILL);
        this.c.setColor(e2);
        this.d = new RectF();
        if (!z) {
            StateListDrawable stateListDrawable = new StateListDrawable();
            stateListDrawable.addState(new int[]{16842919}, new ColorDrawable(a2));
            stateListDrawable.addState(new int[0], new ColorDrawable(e2));
            x.a((View) this, (Drawable) stateListDrawable);
        }
    }

    public void a(e eVar, String str, Map<String, String> map) {
        a(eVar.b(), eVar.a(), str, map);
    }

    public void a(String str, final String str2, final String str3, final Map<String, String> map) {
        if (TextUtils.isEmpty(str) || TextUtils.isEmpty(str2) || this.i == null) {
            setVisibility(8);
            return;
        }
        setText(str.toUpperCase(Locale.US));
        setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String str;
                String str2;
                try {
                    Uri parse = Uri.parse(str2);
                    a.this.g.a((Map<String, String>) map);
                    map.put("touch", k.a(a.this.h.e()));
                    b a2 = com.facebook.ads.internal.a.c.a(a.this.getContext(), a.this.i, str3, parse, map);
                    if (a2 != null) {
                        a2.b();
                    }
                    if (a.this.j != null) {
                        a.this.j.a(a.this.f);
                    }
                } catch (ActivityNotFoundException e) {
                    e = e;
                    str2 = String.valueOf(a.class);
                    str = "Error while opening " + str2;
                    Log.e(str2, str, e);
                } catch (Exception e2) {
                    e = e2;
                    str2 = String.valueOf(a.class);
                    str = "Error executing action";
                    Log.e(str2, str, e);
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        if (this.e) {
            this.d.set(0.0f, 0.0f, (float) getWidth(), (float) getHeight());
            RectF rectF = this.d;
            int i2 = b;
            canvas.drawRoundRect(rectF, (float) i2, (float) i2, this.c);
        }
        super.onDraw(canvas);
    }
}
