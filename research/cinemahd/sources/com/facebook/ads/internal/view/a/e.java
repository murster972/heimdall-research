package com.facebook.ads.internal.view.a;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.facebook.ads.internal.q.b.b;
import com.facebook.ads.internal.q.b.c;
import com.facebook.common.util.UriUtil;

@TargetApi(19)
public class e extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f2855a;
    private TextView b;
    private Drawable c;

    public e(Context context) {
        super(context);
        a();
    }

    private void a() {
        float f = getResources().getDisplayMetrics().density;
        setOrientation(1);
        this.f2855a = new TextView(getContext());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, -2);
        this.f2855a.setTextColor(-16777216);
        this.f2855a.setTextSize(2, 20.0f);
        this.f2855a.setEllipsize(TextUtils.TruncateAt.END);
        this.f2855a.setSingleLine(true);
        this.f2855a.setVisibility(8);
        addView(this.f2855a, layoutParams);
        this.b = new TextView(getContext());
        LinearLayout.LayoutParams layoutParams2 = new LinearLayout.LayoutParams(-1, -2);
        this.b.setAlpha(0.5f);
        this.b.setTextColor(-16777216);
        this.b.setTextSize(2, 15.0f);
        this.b.setCompoundDrawablePadding((int) (f * 5.0f));
        this.b.setEllipsize(TextUtils.TruncateAt.END);
        this.b.setSingleLine(true);
        this.b.setVisibility(8);
        addView(this.b, layoutParams2);
    }

    private Drawable getPadlockDrawable() {
        if (this.c == null) {
            this.c = c.a(getContext(), b.BROWSER_PADLOCK);
        }
        return this.c;
    }

    public void setSubtitle(String str) {
        TextView textView;
        int i;
        if (TextUtils.isEmpty(str)) {
            this.b.setText((CharSequence) null);
            textView = this.b;
            i = 8;
        } else {
            Uri parse = Uri.parse(str);
            this.b.setText(parse.getHost());
            this.b.setCompoundDrawablesRelativeWithIntrinsicBounds(UriUtil.HTTPS_SCHEME.equals(parse.getScheme()) ? getPadlockDrawable() : null, (Drawable) null, (Drawable) null, (Drawable) null);
            textView = this.b;
            i = 0;
        }
        textView.setVisibility(i);
    }

    public void setTitle(String str) {
        TextView textView;
        int i;
        if (TextUtils.isEmpty(str)) {
            this.f2855a.setText((CharSequence) null);
            textView = this.f2855a;
            i = 8;
        } else {
            this.f2855a.setText(str);
            textView = this.f2855a;
            i = 0;
        }
        textView.setVisibility(i);
    }
}
