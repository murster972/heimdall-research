package com.facebook.ads.internal.view.e;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class c extends RecyclerView.Adapter<e> {

    /* renamed from: a  reason: collision with root package name */
    private final List<String> f2923a;
    private final int b;

    c(List<String> list, int i) {
        this.f2923a = list;
        this.b = i;
    }

    /* renamed from: a */
    public e onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new e(new d(viewGroup.getContext()));
    }

    /* renamed from: a */
    public void onBindViewHolder(e eVar, int i) {
        String str = this.f2923a.get(i);
        ViewGroup.MarginLayoutParams marginLayoutParams = new ViewGroup.MarginLayoutParams(-2, -1);
        int i2 = this.b;
        if (i == 0) {
            i2 *= 4;
        }
        marginLayoutParams.setMargins(i2, 0, i >= getItemCount() + -1 ? this.b * 4 : this.b, 0);
        eVar.a().setLayoutParams(marginLayoutParams);
        eVar.a().a(str);
    }

    public int getItemCount() {
        return this.f2923a.size();
    }
}
