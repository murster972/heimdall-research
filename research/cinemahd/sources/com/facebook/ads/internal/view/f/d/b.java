package com.facebook.ads.internal.view.f.d;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.MediaController;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.facebook.ads.internal.view.f.a.a;

@TargetApi(14)
public class b extends TextureView implements MediaPlayer.OnBufferingUpdateListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnSeekCompleteListener, MediaPlayer.OnVideoSizeChangedListener, TextureView.SurfaceTextureListener, c {
    private static final String t = b.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    private Uri f3034a;
    private e b;
    private Surface c;
    /* access modifiers changed from: private */
    public MediaPlayer d;
    /* access modifiers changed from: private */
    public MediaController e;
    private d f;
    private d g;
    private d h;
    private boolean i = false;
    private View j;
    private int k = 0;
    private long l;
    private int m = 0;
    private int n = 0;
    private float o = 1.0f;
    private boolean p = false;
    private int q = 3;
    private boolean r = false;
    private boolean s = false;
    private int u = 0;
    /* access modifiers changed from: private */
    public boolean v = false;
    private a w = a.NOT_STARTED;
    private final MediaController.MediaPlayerControl x = new MediaController.MediaPlayerControl() {
        public boolean canPause() {
            return true;
        }

        public boolean canSeekBackward() {
            return true;
        }

        public boolean canSeekForward() {
            return true;
        }

        public int getAudioSessionId() {
            if (b.this.d != null) {
                return b.this.d.getAudioSessionId();
            }
            return 0;
        }

        public int getBufferPercentage() {
            return 0;
        }

        public int getCurrentPosition() {
            return b.this.getCurrentPosition();
        }

        public int getDuration() {
            return b.this.getDuration();
        }

        public boolean isPlaying() {
            return b.this.d != null && b.this.d.isPlaying();
        }

        public void pause() {
            b.this.a(true);
        }

        public void seekTo(int i) {
            b.this.a(i);
        }

        public void start() {
            b.this.a(a.USER_STARTED);
        }
    };

    public b(Context context) {
        super(context);
        d dVar = d.IDLE;
        this.f = dVar;
        this.g = dVar;
        this.h = dVar;
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d dVar = d.IDLE;
        this.f = dVar;
        this.g = dVar;
        this.h = dVar;
    }

    public b(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        d dVar = d.IDLE;
        this.f = dVar;
        this.g = dVar;
        this.h = dVar;
    }

    @TargetApi(21)
    public b(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        d dVar = d.IDLE;
        this.f = dVar;
        this.g = dVar;
        this.h = dVar;
    }

    private boolean a(Surface surface) {
        MediaPlayer mediaPlayer = this.d;
        if (mediaPlayer == null) {
            return false;
        }
        try {
            mediaPlayer.setSurface(surface);
            return true;
        } catch (IllegalStateException e2) {
            com.facebook.ads.internal.q.d.a.a(getContext(), "player", com.facebook.ads.internal.q.d.b.s, (Exception) e2);
            Log.d(t, "The MediaPlayer failed", e2);
            return false;
        }
    }

    private boolean f() {
        d dVar = this.f;
        return dVar == d.PREPARED || dVar == d.STARTED || dVar == d.PAUSED || dVar == d.PLAYBACK_COMPLETED;
    }

    private boolean g() {
        MediaPlayer mediaPlayer = this.d;
        if (mediaPlayer == null) {
            return false;
        }
        try {
            mediaPlayer.reset();
            return true;
        } catch (IllegalStateException e2) {
            com.facebook.ads.internal.q.d.a.a(getContext(), "player", com.facebook.ads.internal.q.d.b.t, (Exception) e2);
            Log.d(t, "The MediaPlayer failed", e2);
            return false;
        }
    }

    private boolean h() {
        d dVar = this.f;
        return (dVar == d.PREPARING || dVar == d.PREPARED) ? false : true;
    }

    private boolean i() {
        d dVar = this.f;
        return (dVar == d.PREPARING || dVar == d.PREPARED) ? false : true;
    }

    private void setVideoState(d dVar) {
        if (dVar != this.f) {
            this.f = dVar;
            e eVar = this.b;
            if (eVar != null) {
                eVar.a(dVar);
            }
        }
    }

    public void a() {
        if (!this.r) {
            a(false);
        }
    }

    public void a(int i2) {
        if (this.d == null || !f()) {
            this.k = i2;
        } else if (i2 < getDuration() && i2 > 0) {
            this.u = getCurrentPosition();
            this.k = i2;
            this.d.seekTo(i2);
        }
    }

    public void a(a aVar) {
        d dVar = d.STARTED;
        this.g = dVar;
        this.w = aVar;
        d dVar2 = this.f;
        if (dVar2 == dVar || dVar2 == d.PREPARED || dVar2 == d.IDLE || dVar2 == d.PAUSED || dVar2 == d.PLAYBACK_COMPLETED) {
            MediaPlayer mediaPlayer = this.d;
            if (mediaPlayer == null) {
                setup(this.f3034a);
            } else {
                int i2 = this.k;
                if (i2 > 0) {
                    mediaPlayer.seekTo(i2);
                }
                this.d.start();
                if (this.f != d.PREPARED || this.s) {
                    setVideoState(d.STARTED);
                }
            }
        }
        if (isAvailable()) {
            onSurfaceTextureAvailable(getSurfaceTexture(), 0, 0);
        }
    }

    public void a(boolean z) {
        d dVar;
        this.g = d.PAUSED;
        if (this.d == null) {
            dVar = d.IDLE;
        } else if (i()) {
            if (z) {
                this.h = d.PAUSED;
                this.i = true;
            }
            this.d.pause();
            if (this.f != d.PLAYBACK_COMPLETED) {
                dVar = d.PAUSED;
            } else {
                return;
            }
        } else {
            return;
        }
        setVideoState(dVar);
    }

    public void b() {
        setVideoState(d.PLAYBACK_COMPLETED);
        c();
        this.k = 0;
    }

    public void c() {
        this.g = d.IDLE;
        MediaPlayer mediaPlayer = this.d;
        if (mediaPlayer != null) {
            int currentPosition = mediaPlayer.getCurrentPosition();
            if (currentPosition > 0) {
                this.k = currentPosition;
            }
            this.d.stop();
            g();
            this.d.release();
            this.d = null;
            MediaController mediaController = this.e;
            if (mediaController != null) {
                mediaController.hide();
                this.e.setEnabled(false);
            }
        }
        setVideoState(d.IDLE);
    }

    @SuppressLint({"NewApi"})
    public boolean d() {
        MediaPlayer mediaPlayer = this.d;
        if (mediaPlayer == null || Build.VERSION.SDK_INT < 16) {
            return false;
        }
        try {
            for (MediaPlayer.TrackInfo trackType : mediaPlayer.getTrackInfo()) {
                if (trackType.getTrackType() == 2) {
                    return true;
                }
            }
            return false;
        } catch (RuntimeException e2) {
            Log.e(t, "Couldn't retrieve video information", e2);
            return true;
        }
    }

    public void e() {
        if (this.d != null) {
            a((Surface) null);
            this.d.setOnBufferingUpdateListener((MediaPlayer.OnBufferingUpdateListener) null);
            this.d.setOnCompletionListener((MediaPlayer.OnCompletionListener) null);
            this.d.setOnErrorListener((MediaPlayer.OnErrorListener) null);
            this.d.setOnInfoListener((MediaPlayer.OnInfoListener) null);
            this.d.setOnPreparedListener((MediaPlayer.OnPreparedListener) null);
            this.d.setOnVideoSizeChangedListener((MediaPlayer.OnVideoSizeChangedListener) null);
            this.d.setOnSeekCompleteListener((MediaPlayer.OnSeekCompleteListener) null);
            g();
            this.d = null;
            setVideoState(d.IDLE);
        }
    }

    public int getCurrentPosition() {
        if (this.d == null || !f()) {
            return 0;
        }
        return this.d.getCurrentPosition();
    }

    public int getDuration() {
        if (this.d == null || !f()) {
            return 0;
        }
        return this.d.getDuration();
    }

    public long getInitialBufferTime() {
        return this.l;
    }

    public a getStartReason() {
        return this.w;
    }

    public d getState() {
        return this.f;
    }

    public d getTargetState() {
        return this.g;
    }

    public int getVideoHeight() {
        return this.n;
    }

    public int getVideoWidth() {
        return this.m;
    }

    public View getView() {
        return this;
    }

    public float getVolume() {
        return this.o;
    }

    public void onBufferingUpdate(MediaPlayer mediaPlayer, int i2) {
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        MediaPlayer mediaPlayer2 = this.d;
        if (mediaPlayer2 != null) {
            mediaPlayer2.pause();
        }
        setVideoState(d.PLAYBACK_COMPLETED);
        a(0);
        this.k = 0;
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        if (this.q <= 0 || getState() != d.STARTED) {
            setVideoState(d.ERROR);
            c();
        } else {
            this.q--;
            c();
            a(this.w);
        }
        return true;
    }

    public boolean onInfo(MediaPlayer mediaPlayer, int i2, int i3) {
        d dVar;
        if (i2 != 3) {
            if (i2 == 701) {
                dVar = d.BUFFERING;
            } else if (i2 != 702 || !h()) {
                return false;
            } else {
                dVar = d.STARTED;
            }
            setVideoState(dVar);
            return false;
        }
        this.s = true;
        d dVar2 = this.g;
        d dVar3 = d.STARTED;
        if (dVar2 == dVar3) {
            setVideoState(dVar3);
        }
        return true;
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        setVideoState(d.PREPARED);
        if (this.p && !this.v) {
            this.e = new MediaController(getContext());
            MediaController mediaController = this.e;
            View view = this.j;
            if (view == null) {
                view = this;
            }
            mediaController.setAnchorView(view);
            this.e.setMediaPlayer(this.x);
            this.e.setEnabled(true);
        }
        setRequestedVolume(this.o);
        this.m = mediaPlayer.getVideoWidth();
        this.n = mediaPlayer.getVideoHeight();
        int i2 = this.k;
        if (i2 > 0) {
            if (i2 >= this.d.getDuration()) {
                this.k = 0;
            }
            this.d.seekTo(this.k);
            this.k = 0;
        }
        if (this.g == d.STARTED) {
            a(this.w);
        }
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        e eVar = this.b;
        if (eVar != null) {
            eVar.a(this.u, this.k);
            this.k = 0;
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (this.c == null) {
            this.c = new Surface(surfaceTexture);
        }
        if (!a(this.c)) {
            setVideoState(d.ERROR);
            e();
            return;
        }
        this.i = false;
        d dVar = this.f;
        d dVar2 = d.PAUSED;
        if (dVar == dVar2 && this.h != dVar2) {
            a(this.w);
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        a((Surface) null);
        Surface surface = this.c;
        if (surface != null) {
            surface.release();
            this.c = null;
        }
        if (!this.i) {
            this.h = this.p ? d.STARTED : this.f;
            this.i = true;
        }
        if (this.f != d.PAUSED) {
            a(false);
        }
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i2, int i3) {
        this.m = mediaPlayer.getVideoWidth();
        this.n = mediaPlayer.getVideoHeight();
        if (this.m != 0 && this.n != 0) {
            requestLayout();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.d != null) {
            MediaController mediaController = this.e;
            if (mediaController != null && mediaController.isShowing()) {
                return;
            }
            if (!z) {
                if (!this.i) {
                    this.h = this.p ? d.STARTED : this.f;
                    this.i = true;
                }
                if (this.f != d.PAUSED) {
                    a();
                    return;
                }
                return;
            }
            this.i = false;
            d dVar = this.f;
            d dVar2 = d.PAUSED;
            if (dVar == dVar2 && this.h != dVar2) {
                a(this.w);
            }
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 24) {
            super.setBackgroundDrawable(drawable);
        } else if (AdInternalSettings.isDebugBuild()) {
            Log.w(t, "Google always throw an exception with setBackgroundDrawable on Nougat above. so we silently ignore it.");
        }
    }

    public void setBackgroundPlaybackEnabled(boolean z) {
        this.r = z;
    }

    public void setControlsAnchorView(View view) {
        this.j = view;
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (!b.this.v && b.this.e != null && motionEvent.getAction() == 1) {
                    if (b.this.e.isShowing()) {
                        b.this.e.hide();
                    } else {
                        b.this.e.show();
                    }
                }
                return true;
            }
        });
    }

    public void setForeground(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 24) {
            super.setForeground(drawable);
        } else if (AdInternalSettings.isDebugBuild()) {
            Log.w(t, "Google always throw an exception with setForeground on Nougat above. so we silently ignore it.");
        }
    }

    public void setFullScreen(boolean z) {
        this.p = z;
        if (this.p && !this.v) {
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (!b.this.v && b.this.e != null && motionEvent.getAction() == 1) {
                        if (b.this.e.isShowing()) {
                            b.this.e.hide();
                        } else {
                            b.this.e.show();
                        }
                    }
                    return true;
                }
            });
        }
    }

    public void setRequestedVolume(float f2) {
        d dVar;
        this.o = f2;
        MediaPlayer mediaPlayer = this.d;
        if (mediaPlayer != null && (dVar = this.f) != d.PREPARING && dVar != d.IDLE) {
            mediaPlayer.setVolume(f2, f2);
        }
    }

    public void setVideoMPD(String str) {
    }

    public void setVideoStateChangeListener(e eVar) {
        this.b = eVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x0114  */
    /* JADX WARNING: Removed duplicated region for block: B:41:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void setup(android.net.Uri r11) {
        /*
            r10 = this;
            java.lang.String r0 = "Unable to close"
            r1 = 0
            r10.s = r1
            r10.f3034a = r11
            android.media.MediaPlayer r2 = r10.d
            r3 = 0
            if (r2 == 0) goto L_0x001a
            r10.g()
            r10.a((android.view.Surface) r3)
            android.media.MediaPlayer r2 = r10.d
            com.facebook.ads.internal.view.f.d.d r4 = com.facebook.ads.internal.view.f.d.d.IDLE
            r10.setVideoState(r4)
            goto L_0x001f
        L_0x001a:
            android.media.MediaPlayer r2 = new android.media.MediaPlayer
            r2.<init>()
        L_0x001f:
            java.lang.String r4 = r11.getScheme()     // Catch:{ Exception -> 0x00ec }
            java.lang.String r5 = "asset"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x00ec }
            if (r4 == 0) goto L_0x00c2
            android.content.Context r4 = r10.getContext()     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            android.content.res.AssetManager r4 = r4.getAssets()     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            java.lang.String r11 = r11.getPath()     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            r5 = 1
            java.lang.String r11 = r11.substring(r5)     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            android.content.res.AssetFileDescriptor r3 = r4.openFd(r11)     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            long r6 = r3.getStartOffset()     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            long r8 = r3.getLength()     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            java.io.FileDescriptor r5 = r3.getFileDescriptor()     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            r4 = r2
            r4.setDataSource(r5, r6, r8)     // Catch:{ SecurityException -> 0x0071, IOException -> 0x006f }
            if (r3 == 0) goto L_0x00c9
            r3.close()     // Catch:{ IOException -> 0x0057 }
            goto L_0x00c9
        L_0x0057:
            r11 = move-exception
            java.lang.String r3 = t     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            r4.<init>()     // Catch:{ Exception -> 0x00ec }
            r4.append(r0)     // Catch:{ Exception -> 0x00ec }
            r4.append(r11)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r11 = r4.toString()     // Catch:{ Exception -> 0x00ec }
        L_0x0069:
            android.util.Log.w(r3, r11)     // Catch:{ Exception -> 0x00ec }
            goto L_0x00c9
        L_0x006d:
            r11 = move-exception
            goto L_0x00a6
        L_0x006f:
            r11 = move-exception
            goto L_0x0072
        L_0x0071:
            r11 = move-exception
        L_0x0072:
            java.lang.String r4 = t     // Catch:{ all -> 0x006d }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x006d }
            r5.<init>()     // Catch:{ all -> 0x006d }
            java.lang.String r6 = "Failed to open assets "
            r5.append(r6)     // Catch:{ all -> 0x006d }
            r5.append(r11)     // Catch:{ all -> 0x006d }
            java.lang.String r11 = r5.toString()     // Catch:{ all -> 0x006d }
            android.util.Log.w(r4, r11)     // Catch:{ all -> 0x006d }
            com.facebook.ads.internal.view.f.d.d r11 = com.facebook.ads.internal.view.f.d.d.ERROR     // Catch:{ all -> 0x006d }
            r10.setVideoState(r11)     // Catch:{ all -> 0x006d }
            if (r3 == 0) goto L_0x00c9
            r3.close()     // Catch:{ IOException -> 0x0093 }
            goto L_0x00c9
        L_0x0093:
            r11 = move-exception
            java.lang.String r3 = t     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            r4.<init>()     // Catch:{ Exception -> 0x00ec }
            r4.append(r0)     // Catch:{ Exception -> 0x00ec }
            r4.append(r11)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r11 = r4.toString()     // Catch:{ Exception -> 0x00ec }
            goto L_0x0069
        L_0x00a6:
            if (r3 == 0) goto L_0x00c1
            r3.close()     // Catch:{ IOException -> 0x00ac }
            goto L_0x00c1
        L_0x00ac:
            r3 = move-exception
            java.lang.String r4 = t     // Catch:{ Exception -> 0x00ec }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ec }
            r5.<init>()     // Catch:{ Exception -> 0x00ec }
            r5.append(r0)     // Catch:{ Exception -> 0x00ec }
            r5.append(r3)     // Catch:{ Exception -> 0x00ec }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x00ec }
            android.util.Log.w(r4, r0)     // Catch:{ Exception -> 0x00ec }
        L_0x00c1:
            throw r11     // Catch:{ Exception -> 0x00ec }
        L_0x00c2:
            java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x00ec }
            r2.setDataSource(r11)     // Catch:{ Exception -> 0x00ec }
        L_0x00c9:
            r2.setLooping(r1)     // Catch:{ Exception -> 0x00ec }
            r2.setOnBufferingUpdateListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.setOnCompletionListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.setOnErrorListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.setOnInfoListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.setOnPreparedListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.setOnVideoSizeChangedListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.setOnSeekCompleteListener(r10)     // Catch:{ Exception -> 0x00ec }
            r2.prepareAsync()     // Catch:{ Exception -> 0x00ec }
            r10.d = r2     // Catch:{ Exception -> 0x00ec }
            com.facebook.ads.internal.view.f.d.d r11 = com.facebook.ads.internal.view.f.d.d.PREPARING     // Catch:{ Exception -> 0x00ec }
            r10.setVideoState(r11)     // Catch:{ Exception -> 0x00ec }
            goto L_0x010b
        L_0x00ec:
            r11 = move-exception
            com.facebook.ads.internal.view.f.d.d r0 = com.facebook.ads.internal.view.f.d.d.ERROR
            r10.setVideoState(r0)
            r2.release()
            java.lang.String r0 = t
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Cannot prepare media player with SurfaceTexture: "
            r2.append(r3)
            r2.append(r11)
            java.lang.String r11 = r2.toString()
            android.util.Log.e(r0, r11)
        L_0x010b:
            r10.setSurfaceTextureListener(r10)
            boolean r11 = r10.isAvailable()
            if (r11 == 0) goto L_0x011b
            android.graphics.SurfaceTexture r11 = r10.getSurfaceTexture()
            r10.onSurfaceTextureAvailable(r11, r1, r1)
        L_0x011b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.view.f.d.b.setup(android.net.Uri):void");
    }
}
