package com.facebook.ads.internal.view.c.a;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.internal.adapters.a.g;
import com.facebook.ads.internal.adapters.a.h;
import com.facebook.ads.internal.d.b;
import com.facebook.ads.internal.j.a;
import com.facebook.ads.internal.m.c;
import com.facebook.ads.internal.q.a.k;
import com.facebook.ads.internal.q.a.u;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.r.a;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.c.a.d;
import com.facebook.ads.internal.view.component.d;
import com.facebook.ads.internal.view.i;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class f extends i {
    private static final int e;
    private static final int f;
    private static final int g;
    private static final int h;
    private static final int i;
    /* access modifiers changed from: private */
    public final u j = new u();
    private b k;
    private LinearLayout l;
    /* access modifiers changed from: private */
    public String m;
    private long n;
    private String o;
    private List<b> p;
    /* access modifiers changed from: private */
    public d q;
    private c r;
    /* access modifiers changed from: private */
    public a s;
    private a.C0024a t;
    private int u;
    private int v;

    static {
        float f2 = x.b;
        e = (int) (48.0f * f2);
        f = (int) (f2 * 8.0f);
        g = (int) (8.0f * f2);
        h = (int) (56.0f * f2);
        i = (int) (f2 * 12.0f);
    }

    public f(Context context, c cVar, b bVar) {
        super(context, cVar);
        this.k = bVar;
    }

    private void a(g gVar) {
        this.m = gVar.c();
        this.o = gVar.e();
        this.u = gVar.f();
        this.v = gVar.g();
        List<h> d = gVar.d();
        this.p = new ArrayList(d.size());
        for (int i2 = 0; i2 < d.size(); i2++) {
            this.p.add(new b(i2, d.size(), d.get(i2)));
        }
    }

    private void a(a aVar) {
        new PagerSnapHelper().a((RecyclerView) this.r);
        aVar.a((d.a) new d.a() {
            public void a(int i) {
                if (f.this.q != null) {
                    f.this.q.a(i);
                }
            }
        });
        this.q = new com.facebook.ads.internal.view.component.d(getContext(), this.d.a(), this.p.size());
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-1, g);
        layoutParams.setMargins(0, i, 0, 0);
        this.q.setLayoutParams(layoutParams);
    }

    public void a() {
        LinearLayout linearLayout = this.l;
        if (linearLayout != null) {
            linearLayout.removeAllViews();
            this.l = null;
        }
        c cVar = this.r;
        if (cVar != null) {
            cVar.removeAllViews();
            this.r = null;
        }
        com.facebook.ads.internal.view.component.d dVar = this.q;
        if (dVar != null) {
            dVar.removeAllViews();
            this.q = null;
        }
    }

    public void a(Intent intent, Bundle bundle, AudienceNetworkActivity audienceNetworkActivity) {
        g gVar = (g) intent.getSerializableExtra("ad_data_bundle");
        super.a(audienceNetworkActivity, gVar);
        a(gVar);
        setUpLayout(audienceNetworkActivity.getResources().getConfiguration().orientation);
        this.n = System.currentTimeMillis();
    }

    public void a(Bundle bundle) {
    }

    public void i() {
    }

    public void j() {
    }

    public void onConfigurationChanged(Configuration configuration) {
        a();
        setUpLayout(configuration.orientation);
        super.onConfigurationChanged(configuration);
    }

    public void onDestroy() {
        super.onDestroy();
        com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(this.n, a.C0022a.XOUT, this.o));
        if (!TextUtils.isEmpty(this.m)) {
            HashMap hashMap = new HashMap();
            this.s.a((Map<String, String>) hashMap);
            hashMap.put("touch", k.a(this.j.e()));
            this.b.i(this.m, hashMap);
        }
        a();
        this.s.c();
        this.s = null;
        this.t = null;
        this.p = null;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        this.j.a(motionEvent, this, this);
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void setUpLayout(int i2) {
        int i3;
        int i4;
        int i5;
        f fVar;
        int i6 = i2;
        this.l = new LinearLayout(getContext());
        this.l.setGravity(i6 == 1 ? 17 : 48);
        this.l.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.l.setOrientation(1);
        DisplayMetrics displayMetrics = x.f2829a;
        int i7 = displayMetrics.widthPixels;
        int i8 = displayMetrics.heightPixels;
        if (i6 == 1) {
            int min = Math.min(i7 - (f * 4), i8 / 2);
            int i9 = (i7 - min) / 8;
            i5 = min;
            i4 = i9;
            i3 = i9 * 4;
        } else {
            int i10 = h + e;
            int i11 = f;
            i5 = i8 - (i10 + (i11 * 2));
            i3 = i11 * 2;
            i4 = i11;
        }
        this.t = new a.C0024a() {
            public void a() {
                HashMap hashMap = new HashMap();
                if (!f.this.j.b()) {
                    f.this.j.a();
                    if (f.this.getAudienceNetworkListener() != null) {
                        f.this.getAudienceNetworkListener().a("com.facebook.ads.interstitial.impression.logged");
                    }
                    if (!TextUtils.isEmpty(f.this.m)) {
                        f.this.s.a((Map<String, String>) hashMap);
                        hashMap.put("touch", k.a(f.this.j.e()));
                        f.this.b.a(f.this.m, hashMap);
                    }
                }
            }
        };
        this.s = new com.facebook.ads.internal.r.a(this, 1, this.t);
        this.s.a(this.u);
        this.s.b(this.v);
        this.r = new c(getContext());
        this.r.setLayoutParams(new RelativeLayout.LayoutParams(-1, -2));
        a aVar = new a(this.r, i6, this.p, this.s);
        c cVar = this.r;
        List<b> list = this.p;
        c cVar2 = this.b;
        b bVar = this.k;
        com.facebook.ads.internal.r.a aVar2 = this.s;
        u uVar = this.j;
        a.C0026a audienceNetworkListener = getAudienceNetworkListener();
        com.facebook.ads.internal.adapters.a.a aVar3 = this.d;
        d dVar = r1;
        a aVar4 = aVar;
        d dVar2 = new d(list, cVar2, bVar, aVar2, uVar, audienceNetworkListener, i6 == 1 ? aVar3.a() : aVar3.b(), this.m, i5, i4, i3, i2, aVar4);
        cVar.setAdapter(dVar);
        int i12 = i2;
        if (i12 == 1) {
            fVar = this;
            fVar.a(aVar4);
        } else {
            fVar = this;
        }
        fVar.l.addView(fVar.r);
        com.facebook.ads.internal.view.component.d dVar3 = fVar.q;
        if (dVar3 != null) {
            fVar.l.addView(dVar3);
        }
        fVar.a(fVar.l, false, i12);
    }
}
