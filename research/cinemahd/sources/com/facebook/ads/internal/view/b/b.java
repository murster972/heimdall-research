package com.facebook.ads.internal.view.b;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.ads.internal.q.a.j;
import com.facebook.ads.internal.q.a.x;

public class b extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final ImageView f2869a;
    private int b;
    private int c;

    public b(Context context) {
        super(context);
        this.f2869a = new ImageView(context);
        a();
    }

    public b(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.f2869a = new ImageView(context, attributeSet);
        a();
    }

    public b(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        this.f2869a = new ImageView(context, attributeSet, i);
        a();
    }

    @TargetApi(21)
    public b(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
        this.f2869a = new ImageView(context, attributeSet, i, i2);
        a();
    }

    private void a() {
        this.f2869a.setScaleType(ImageView.ScaleType.FIT_XY);
        addView(this.f2869a, new FrameLayout.LayoutParams(-2, -2));
        j.a(this.f2869a, j.INTERNAL_AD_MEDIA);
    }

    public void a(Bitmap bitmap, Bitmap bitmap2) {
        if (bitmap2 != null) {
            x.a((View) this, (Drawable) new BitmapDrawable(getContext().getResources(), bitmap2));
        } else {
            x.a((View) this, 0);
        }
        if (bitmap != null) {
            this.b = bitmap.getWidth();
            this.c = bitmap.getHeight();
            this.f2869a.setImageBitmap(Bitmap.createBitmap(bitmap));
            return;
        }
        this.f2869a.setImageDrawable((Drawable) null);
    }

    public ImageView getBodyImageView() {
        return this.f2869a;
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        int i5;
        int i6 = i3 - i;
        int i7 = i4 - i2;
        int i8 = this.b;
        if (i8 <= 0 || (i5 = this.c) <= 0) {
            super.onLayout(z, i, i2, i3, i4);
            return;
        }
        float min = Math.min(((float) i6) / ((float) i8), ((float) i7) / ((float) i5));
        int i9 = i + (i6 / 2);
        int i10 = i2 + (i7 / 2);
        int i11 = ((int) (((float) this.b) * min)) / 2;
        int i12 = ((int) (min * ((float) this.c))) / 2;
        this.f2869a.layout(i9 - i11, i10 - i12, i9 + i11, i10 + i12);
    }
}
