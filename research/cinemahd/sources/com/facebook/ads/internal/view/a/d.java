package com.facebook.ads.internal.view.a;

import android.text.TextUtils;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final f f2854a;
    private boolean b = true;

    public d(f fVar) {
        this.f2854a = fVar;
    }

    private static long a(String str, String str2) {
        String substring = str.substring(str2.length());
        if (TextUtils.isEmpty(substring)) {
            return -1;
        }
        try {
            Long valueOf = Long.valueOf(Long.parseLong(substring));
            if (valueOf.longValue() < 0) {
                return -1;
            }
            return valueOf.longValue();
        } catch (NumberFormatException unused) {
            return -1;
        }
    }

    public void a() {
        if (this.b) {
            if (this.f2854a.canGoBack() || this.f2854a.canGoForward()) {
                this.b = false;
            } else {
                this.f2854a.a("void((function() {try {  if (!window.performance || !window.performance.timing || !document ||       !document.body || document.body.scrollHeight <= 0 ||       !document.body.children || document.body.children.length < 1) {    return;  }  var nvtiming__an_t = window.performance.timing;  if (nvtiming__an_t.responseEnd > 0) {    console.log('ANNavResponseEnd:'+nvtiming__an_t.responseEnd);  }  if (nvtiming__an_t.domContentLoadedEventStart > 0) {    console.log('ANNavDomContentLoaded:' + nvtiming__an_t.domContentLoadedEventStart);  }  if (nvtiming__an_t.loadEventEnd > 0) {    console.log('ANNavLoadEventEnd:' + nvtiming__an_t.loadEventEnd);  }} catch(err) {  console.log('an_navigation_timing_error:' + err.message);}})());");
            }
        }
    }

    public void a(String str) {
        if (this.b) {
            if (str.startsWith("ANNavResponseEnd:")) {
                this.f2854a.a(a(str, "ANNavResponseEnd:"));
            } else if (str.startsWith("ANNavDomContentLoaded:")) {
                this.f2854a.b(a(str, "ANNavDomContentLoaded:"));
            } else if (str.startsWith("ANNavLoadEventEnd:")) {
                this.f2854a.c(a(str, "ANNavLoadEventEnd:"));
            }
        }
    }

    public void a(boolean z) {
        this.b = z;
    }
}
