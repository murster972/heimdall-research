package com.facebook.ads.internal.view.component.a;

import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import com.facebook.ads.internal.adapters.a.d;
import com.facebook.ads.internal.q.a.x;

public class a extends b {
    public a(d dVar, d dVar2, boolean z) {
        super(dVar, dVar2, true);
        RelativeLayout relativeLayout = new RelativeLayout(dVar.a());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(12);
        GradientDrawable gradientDrawable = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{-1778384896, 0});
        gradientDrawable.setCornerRadius(0.0f);
        gradientDrawable.setGradientType(0);
        x.a((View) relativeLayout, (Drawable) gradientDrawable);
        LinearLayout linearLayout = new LinearLayout(dVar.a());
        linearLayout.setOrientation(z ^ true ? 1 : 0);
        linearLayout.setGravity(80);
        x.a((View) linearLayout);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -2);
        int i = dVar.h() == null ? b.f2900a : b.f2900a / 2;
        int i2 = b.f2900a;
        layoutParams2.setMargins(i2, 0, i2, i);
        LinearLayout.LayoutParams layoutParams3 = new LinearLayout.LayoutParams(z ? -2 : -1, -2);
        layoutParams3.setMargins(z ? b.f2900a : 0, z ? 0 : b.f2900a, 0, 0);
        LinearLayout.LayoutParams layoutParams4 = new LinearLayout.LayoutParams(z ? 0 : -1, -2);
        layoutParams4.setMargins(0, 0, 0, 0);
        layoutParams4.weight = 1.0f;
        linearLayout.addView(getTitleDescContainer(), layoutParams4);
        linearLayout.addView(getCtaButton(), layoutParams3);
        relativeLayout.addView(linearLayout, layoutParams2);
        if (dVar.h() != null) {
            RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-1, -2);
            layoutParams5.setMargins(0, 0, 0, 0);
            layoutParams5.addRule(3, linearLayout.getId());
            relativeLayout.addView(dVar.h(), layoutParams5);
        }
        addView(dVar.d(), new RelativeLayout.LayoutParams(-1, -1));
        addView(relativeLayout, layoutParams);
        if (dVar.i() != null) {
            int i3 = b.b;
            RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(i3, i3);
            layoutParams6.addRule(10);
            layoutParams6.addRule(11);
            int i4 = b.f2900a;
            int i5 = b.f2900a;
            layoutParams6.setMargins(i4, dVar.j() + i4, i5, i5);
            addView(dVar.i(), layoutParams6);
        }
    }

    public boolean a() {
        return true;
    }
}
