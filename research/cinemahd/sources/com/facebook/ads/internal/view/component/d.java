package com.facebook.ads.internal.view.component;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.core.graphics.ColorUtils;
import com.facebook.ads.internal.q.a.x;
import java.util.ArrayList;
import java.util.List;

public class d extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final int f2907a;
    private final int b;
    private final int c;
    private int d = -1;
    private List<GradientDrawable> e;

    public d(Context context, com.facebook.ads.internal.adapters.a.d dVar, int i) {
        super(context);
        setOrientation(0);
        setGravity(17);
        float f = x.b;
        int i2 = (int) (8.0f * f);
        int i3 = (int) (6.0f * f);
        this.c = (int) (f * 1.0f);
        this.f2907a = dVar.a(false);
        this.b = ColorUtils.d(this.f2907a, 128);
        this.e = new ArrayList();
        for (int i4 = 0; i4 < i; i4++) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setShape(1);
            gradientDrawable.setSize(i2, i2);
            gradientDrawable.setStroke(this.c, 0);
            ImageView imageView = new ImageView(context);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(-2, -2);
            layoutParams.setMargins(0, 0, i3, 0);
            layoutParams.gravity = 17;
            imageView.setLayoutParams(layoutParams);
            imageView.setImageDrawable(gradientDrawable);
            this.e.add(gradientDrawable);
            addView(imageView);
        }
        a(0);
    }

    public void a(int i) {
        int i2;
        int i3;
        if (this.d != i) {
            this.d = i;
            for (int i4 = 0; i4 < this.e.size(); i4++) {
                if (i4 == i) {
                    i3 = this.f2907a;
                    i2 = i3;
                } else {
                    i2 = this.b;
                    i3 = 0;
                }
                this.e.get(i4).setStroke(this.c, i3);
                this.e.get(i4).setColor(i2);
                this.e.get(i4).invalidateSelf();
            }
        }
    }
}
