package com.facebook.ads.internal.view.f.d;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.SurfaceTexture;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.MediaController;
import com.facebook.ads.internal.settings.AdInternalSettings;
import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.b;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.util.Util;
import com.google.android.exoplayer2.video.h;

@TargetApi(14)
public class a extends TextureView implements TextureView.SurfaceTextureListener, c, ExoPlayer.EventListener, SimpleExoPlayer.VideoListener {

    /* renamed from: a  reason: collision with root package name */
    private static final String f3030a = a.class.getSimpleName();
    private Uri b;
    private String c;
    private e d;
    private Surface e;
    /* access modifiers changed from: private */
    public SimpleExoPlayer f;
    /* access modifiers changed from: private */
    public MediaController g;
    private d h;
    private d i;
    private d j;
    private boolean k = false;
    private View l;
    private boolean m = false;
    private boolean n = false;
    private long o;
    private long p;
    private long q;
    private int r;
    private int s;
    private float t = 1.0f;
    private int u = -1;
    private boolean v = false;
    private boolean w = false;
    private com.facebook.ads.internal.view.f.a.a x = com.facebook.ads.internal.view.f.a.a.NOT_STARTED;
    private boolean y = false;

    public a(Context context) {
        super(context);
        d dVar = d.IDLE;
        this.h = dVar;
        this.i = dVar;
        this.j = dVar;
    }

    public a(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        d dVar = d.IDLE;
        this.h = dVar;
        this.i = dVar;
        this.j = dVar;
    }

    public a(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        d dVar = d.IDLE;
        this.h = dVar;
        this.i = dVar;
        this.j = dVar;
    }

    @TargetApi(21)
    public a(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        d dVar = d.IDLE;
        this.h = dVar;
        this.i = dVar;
        this.j = dVar;
    }

    private void f() {
        DefaultBandwidthMeter defaultBandwidthMeter = new DefaultBandwidthMeter();
        this.f = ExoPlayerFactory.a(getContext(), new DefaultTrackSelector(new AdaptiveTrackSelection.Factory(defaultBandwidthMeter)), new DefaultLoadControl());
        this.f.a((SimpleExoPlayer.VideoListener) this);
        this.f.addListener(this);
        this.f.a(false);
        if (this.n && !this.v) {
            this.g = new MediaController(getContext());
            MediaController mediaController = this.g;
            View view = this.l;
            if (view == null) {
                view = this;
            }
            mediaController.setAnchorView(view);
            this.g.setMediaPlayer(new MediaController.MediaPlayerControl() {
                public boolean canPause() {
                    return true;
                }

                public boolean canSeekBackward() {
                    return true;
                }

                public boolean canSeekForward() {
                    return true;
                }

                public int getAudioSessionId() {
                    if (a.this.f != null) {
                        return a.this.f.w();
                    }
                    return 0;
                }

                public int getBufferPercentage() {
                    if (a.this.f != null) {
                        return a.this.f.a();
                    }
                    return 0;
                }

                public int getCurrentPosition() {
                    return a.this.getCurrentPosition();
                }

                public int getDuration() {
                    return a.this.getDuration();
                }

                public boolean isPlaying() {
                    return a.this.f != null && a.this.f.m();
                }

                public void pause() {
                    a.this.a(true);
                }

                public void seekTo(int i) {
                    a.this.a(i);
                }

                public void start() {
                    a.this.a(com.facebook.ads.internal.view.f.a.a.USER_STARTED);
                }
            });
            this.g.setEnabled(true);
        }
        String str = this.c;
        if (str == null || str.length() == 0 || this.y) {
            this.f.a((MediaSource) new ExtractorMediaSource(this.b, new DefaultDataSourceFactory(getContext(), Util.a(getContext(), "ads"), (TransferListener) defaultBandwidthMeter), new DefaultExtractorsFactory(), (Handler) null, (ExtractorMediaSource.EventListener) null));
        }
        setVideoState(d.PREPARING);
        if (isAvailable()) {
            onSurfaceTextureAvailable(getSurfaceTexture(), 0, 0);
        }
    }

    private void g() {
        Surface surface = this.e;
        if (surface != null) {
            surface.release();
            this.e = null;
        }
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.y();
            this.f = null;
        }
        this.g = null;
        this.m = false;
        setVideoState(d.IDLE);
    }

    private void setVideoState(d dVar) {
        if (dVar != this.h) {
            this.h = dVar;
            if (this.h == d.STARTED) {
                this.m = true;
            }
            e eVar = this.d;
            if (eVar != null) {
                eVar.a(dVar);
            }
        }
    }

    public void a() {
        if (!this.w) {
            a(false);
        }
    }

    public void a(int i2) {
        if (this.f != null) {
            this.u = getCurrentPosition();
            this.f.a((long) i2);
            return;
        }
        this.q = (long) i2;
    }

    public /* synthetic */ void a(int i2, int i3) {
        h.a(this, i2, i3);
    }

    public void a(com.facebook.ads.internal.view.f.a.a aVar) {
        this.i = d.STARTED;
        this.x = aVar;
        if (this.f == null) {
            setup(this.b);
            return;
        }
        d dVar = this.h;
        if (dVar == d.PREPARED || dVar == d.PAUSED || dVar == d.PLAYBACK_COMPLETED) {
            this.f.a(true);
            setVideoState(d.STARTED);
        }
    }

    public /* synthetic */ void a(Timeline timeline, Object obj, int i2) {
        b.a(this, timeline, obj, i2);
    }

    public void a(boolean z) {
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.a(false);
        } else {
            setVideoState(d.IDLE);
        }
    }

    public void b() {
        setVideoState(d.PLAYBACK_COMPLETED);
        c();
        this.q = 0;
    }

    public /* synthetic */ void b(int i2) {
        b.a((Player.EventListener) this, i2);
    }

    public /* synthetic */ void b(boolean z) {
        b.b((Player.EventListener) this, z);
    }

    public void c() {
        this.i = d.IDLE;
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.u();
            this.f.y();
            this.f = null;
        }
        setVideoState(d.IDLE);
    }

    public boolean d() {
        SimpleExoPlayer simpleExoPlayer = this.f;
        return (simpleExoPlayer == null || simpleExoPlayer.v() == null) ? false : true;
    }

    public void e() {
        g();
    }

    public int getCurrentPosition() {
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer != null) {
            return (int) simpleExoPlayer.getCurrentPosition();
        }
        return 0;
    }

    public int getDuration() {
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer == null) {
            return 0;
        }
        return (int) simpleExoPlayer.getDuration();
    }

    public long getInitialBufferTime() {
        return this.p;
    }

    public com.facebook.ads.internal.view.f.a.a getStartReason() {
        return this.x;
    }

    public d getState() {
        return this.h;
    }

    public d getTargetState() {
        return this.i;
    }

    public int getVideoHeight() {
        return this.s;
    }

    public int getVideoWidth() {
        return this.r;
    }

    public View getView() {
        return this;
    }

    public float getVolume() {
        return this.t;
    }

    public /* synthetic */ void h() {
        b.a(this);
    }

    public void onLoadingChanged(boolean z) {
    }

    public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
    }

    public void onPlayerError(ExoPlaybackException exoPlaybackException) {
        setVideoState(d.ERROR);
        exoPlaybackException.printStackTrace();
        com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(exoPlaybackException, "[ExoPlayer] Error during playback of ExoPlayer"));
    }

    public void onPlayerStateChanged(boolean z, int i2) {
        d dVar;
        if (i2 == 1) {
            dVar = d.IDLE;
        } else if (i2 == 2) {
            int i3 = this.u;
            if (i3 >= 0) {
                this.u = -1;
                this.d.a(i3, getCurrentPosition());
                return;
            }
            return;
        } else if (i2 == 3) {
            if (this.o != 0) {
                this.p = System.currentTimeMillis() - this.o;
            }
            setRequestedVolume(this.t);
            long j2 = this.q;
            if (j2 > 0 && j2 < this.f.getDuration()) {
                this.f.a(this.q);
                this.q = 0;
            }
            if (this.f.getCurrentPosition() != 0 && !z && this.m) {
                dVar = d.PAUSED;
            } else if (!z && this.h != d.PLAYBACK_COMPLETED) {
                setVideoState(d.PREPARED);
                if (this.i == d.STARTED) {
                    a(this.x);
                    this.i = d.IDLE;
                    return;
                }
                return;
            } else {
                return;
            }
        } else if (i2 == 4) {
            if (z) {
                setVideoState(d.PLAYBACK_COMPLETED);
            }
            SimpleExoPlayer simpleExoPlayer = this.f;
            if (simpleExoPlayer != null) {
                simpleExoPlayer.a(false);
                if (!z) {
                    this.f.t();
                }
            }
            this.m = false;
            return;
        } else {
            return;
        }
        setVideoState(dVar);
    }

    public void onPositionDiscontinuity() {
    }

    public void onRenderedFirstFrame() {
    }

    public /* synthetic */ void onRepeatModeChanged(int i2) {
        b.b((Player.EventListener) this, i2);
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        Surface surface = this.e;
        if (surface != null) {
            surface.release();
        }
        this.e = new Surface(surfaceTexture);
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer != null) {
            simpleExoPlayer.a(this.e);
            this.k = false;
            d dVar = this.h;
            d dVar2 = d.PAUSED;
            if (dVar == dVar2 && this.j != dVar2) {
                a(this.x);
            }
        }
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        Surface surface = this.e;
        if (surface != null) {
            surface.release();
            this.e = null;
            SimpleExoPlayer simpleExoPlayer = this.f;
            if (simpleExoPlayer != null) {
                simpleExoPlayer.a((Surface) null);
            }
        }
        if (!this.k) {
            this.j = this.n ? d.STARTED : this.h;
            this.k = true;
        }
        if (this.h != d.PAUSED) {
            a(false);
        }
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
    }

    public void onTimelineChanged(Timeline timeline, Object obj) {
    }

    public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
    }

    public void onVideoSizeChanged(int i2, int i3, int i4, float f2) {
        this.r = i2;
        this.s = i3;
        if (this.r != 0 && this.s != 0) {
            requestLayout();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.f != null) {
            MediaController mediaController = this.g;
            if (mediaController != null && mediaController.isShowing()) {
                return;
            }
            if (!z) {
                if (!this.k) {
                    this.j = this.n ? d.STARTED : this.h;
                    this.k = true;
                }
                if (this.h != d.PAUSED) {
                    a();
                    return;
                }
                return;
            }
            this.k = false;
            d dVar = this.h;
            d dVar2 = d.PAUSED;
            if (dVar == dVar2 && this.j != dVar2) {
                a(this.x);
            }
        }
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 24) {
            super.setBackgroundDrawable(drawable);
        } else if (AdInternalSettings.isDebugBuild()) {
            Log.w(f3030a, "Google always throw an exception with setBackgroundDrawable on Nougat above. so we silently ignore it.");
        }
    }

    public void setBackgroundPlaybackEnabled(boolean z) {
        this.w = z;
    }

    public void setControlsAnchorView(View view) {
        this.l = view;
        view.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (a.this.g != null && motionEvent.getAction() == 1) {
                    if (a.this.g.isShowing()) {
                        a.this.g.hide();
                    } else {
                        a.this.g.show();
                    }
                }
                return true;
            }
        });
    }

    public void setForeground(Drawable drawable) {
        if (Build.VERSION.SDK_INT < 24) {
            super.setForeground(drawable);
        } else if (AdInternalSettings.isDebugBuild()) {
            Log.w(f3030a, "Google always throw an exception with setForeground on Nougat above. so we silently ignore it.");
        }
    }

    public void setFullScreen(boolean z) {
        this.n = z;
        if (z && !this.v) {
            setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (a.this.g != null && motionEvent.getAction() == 1) {
                        if (a.this.g.isShowing()) {
                            a.this.g.hide();
                        } else {
                            a.this.g.show();
                        }
                    }
                    return true;
                }
            });
        }
    }

    public void setRequestedVolume(float f2) {
        d dVar;
        this.t = f2;
        SimpleExoPlayer simpleExoPlayer = this.f;
        if (simpleExoPlayer != null && (dVar = this.h) != d.PREPARING && dVar != d.IDLE) {
            simpleExoPlayer.a(f2);
        }
    }

    public void setTestMode(boolean z) {
        this.y = z;
    }

    public void setVideoMPD(String str) {
        this.c = str;
    }

    public void setVideoStateChangeListener(e eVar) {
        this.d = eVar;
    }

    public void setup(Uri uri) {
        if (this.f != null) {
            g();
        }
        this.b = uri;
        setSurfaceTextureListener(this);
        f();
    }
}
