package com.facebook.ads.internal.h;

import java.util.ArrayList;
import java.util.List;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private List<a> f2712a = new ArrayList();
    private int b = 0;
    private d c;
    private String d;

    public c(d dVar, String str) {
        this.c = dVar;
        this.d = str;
    }

    public d a() {
        return this.c;
    }

    public void a(a aVar) {
        this.f2712a.add(aVar);
    }

    public String b() {
        return this.d;
    }

    public int c() {
        return this.f2712a.size();
    }

    public a d() {
        if (this.b >= this.f2712a.size()) {
            return null;
        }
        this.b++;
        return this.f2712a.get(this.b - 1);
    }

    public boolean e() {
        return this.c == null || System.currentTimeMillis() > this.c.a() + ((long) this.c.l());
    }
}
