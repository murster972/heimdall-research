package com.facebook.ads.internal.f;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import com.facebook.ads.internal.q.a.n;
import com.facebook.ads.internal.q.a.q;
import com.facebook.ads.internal.q.a.t;
import com.facebook.common.util.UriUtil;
import java.io.FileOutputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2707a = "com.facebook.ads.internal.f.e";
    private static final Object b = new Object();
    private static final Set<String> c = Collections.synchronizedSet(new HashSet());
    private static final Map<String, Integer> d = Collections.synchronizedMap(new HashMap());

    public static d a(Exception exc, Context context, Map<String, String> map) {
        try {
            d dVar = new d(n.b(), n.c(), new b(q.a((Throwable) exc), map, true).a());
            try {
                a(dVar, context);
                return dVar;
            } catch (Exception unused) {
                return dVar;
            }
        } catch (Exception unused2) {
            return null;
        }
    }

    public static JSONArray a(Context context) {
        return a(context, -1);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v1, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v2, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v3, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v0, resolved type: java.io.InputStreamReader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v24, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v25, resolved type: java.lang.Throwable} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v17, resolved type: java.io.InputStreamReader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v32, resolved type: java.lang.Throwable} */
    /* JADX WARNING: type inference failed for: r3v14 */
    /* JADX WARNING: type inference failed for: r9v27 */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00be A[SYNTHETIC, Splitter:B:57:0x00be] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x00c6 A[Catch:{ IOException -> 0x00c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00cb A[Catch:{ IOException -> 0x00c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00da A[SYNTHETIC, Splitter:B:72:0x00da] */
    /* JADX WARNING: Removed duplicated region for block: B:79:0x00e4 A[Catch:{ IOException -> 0x00e0 }] */
    /* JADX WARNING: Removed duplicated region for block: B:81:0x00e9 A[Catch:{ IOException -> 0x00e0 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static org.json.JSONArray a(android.content.Context r8, int r9) {
        /*
            org.json.JSONArray r0 = new org.json.JSONArray
            r0.<init>()
            java.lang.Object r1 = b
            monitor-enter(r1)
            r2 = 0
            java.io.File r3 = new java.io.File     // Catch:{ IOException -> 0x00b2, JSONException -> 0x00b0, all -> 0x00ab }
            java.io.File r4 = r8.getFilesDir()     // Catch:{ IOException -> 0x00b2, JSONException -> 0x00b0, all -> 0x00ab }
            java.lang.String r5 = "debuglogs"
            r3.<init>(r4, r5)     // Catch:{ IOException -> 0x00b2, JSONException -> 0x00b0, all -> 0x00ab }
            boolean r3 = r3.exists()     // Catch:{ IOException -> 0x00b2, JSONException -> 0x00b0, all -> 0x00ab }
            if (r3 == 0) goto L_0x008e
            java.lang.String r3 = "debuglogs"
            java.io.FileInputStream r8 = r8.openFileInput(r3)     // Catch:{ IOException -> 0x00b2, JSONException -> 0x00b0, all -> 0x00ab }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x008b, JSONException -> 0x0089, all -> 0x0086 }
            r3.<init>(r8)     // Catch:{ IOException -> 0x008b, JSONException -> 0x0089, all -> 0x0086 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0084, JSONException -> 0x0082 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0084, JSONException -> 0x0082 }
        L_0x002a:
            java.lang.String r2 = r4.readLine()     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            if (r2 == 0) goto L_0x0078
            if (r9 == 0) goto L_0x0078
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            r5.<init>(r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            java.lang.String r2 = "attempt"
            boolean r2 = r5.has(r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            if (r2 != 0) goto L_0x0045
            java.lang.String r2 = "attempt"
            r6 = 0
            r5.put(r2, r6)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
        L_0x0045:
            java.lang.String r2 = "id"
            java.lang.String r2 = r5.getString(r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            java.util.Set<java.lang.String> r6 = c     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            boolean r6 = r6.contains(r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            if (r6 != 0) goto L_0x002a
            java.lang.String r6 = "attempt"
            int r6 = r5.getInt(r6)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            java.util.Map<java.lang.String, java.lang.Integer> r7 = d     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            boolean r7 = r7.containsKey(r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            if (r7 == 0) goto L_0x006d
            java.lang.String r6 = "attempt"
            java.util.Map<java.lang.String, java.lang.Integer> r7 = d     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            java.lang.Object r2 = r7.get(r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            r5.put(r6, r2)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            goto L_0x0070
        L_0x006d:
            a((java.lang.String) r2, (int) r6)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
        L_0x0070:
            r0.put(r5)     // Catch:{ IOException -> 0x007f, JSONException -> 0x007d, all -> 0x007a }
            if (r9 <= 0) goto L_0x002a
            int r9 = r9 + -1
            goto L_0x002a
        L_0x0078:
            r2 = r4
            goto L_0x0090
        L_0x007a:
            r9 = move-exception
            goto L_0x00d8
        L_0x007d:
            r9 = move-exception
            goto L_0x0080
        L_0x007f:
            r9 = move-exception
        L_0x0080:
            r2 = r4
            goto L_0x00b5
        L_0x0082:
            r9 = move-exception
            goto L_0x00b5
        L_0x0084:
            r9 = move-exception
            goto L_0x00b5
        L_0x0086:
            r9 = move-exception
            r3 = r2
            goto L_0x00ae
        L_0x0089:
            r9 = move-exception
            goto L_0x008c
        L_0x008b:
            r9 = move-exception
        L_0x008c:
            r3 = r2
            goto L_0x00b5
        L_0x008e:
            r8 = r2
            r3 = r8
        L_0x0090:
            if (r2 == 0) goto L_0x0098
            r2.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x0098
        L_0x0096:
            r8 = move-exception
            goto L_0x00a3
        L_0x0098:
            if (r3 == 0) goto L_0x009d
            r3.close()     // Catch:{ IOException -> 0x0096 }
        L_0x009d:
            if (r8 == 0) goto L_0x00d4
            r8.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x00d4
        L_0x00a3:
            java.lang.String r9 = f2707a     // Catch:{ all -> 0x00de }
            java.lang.String r2 = "Failed to close buffers"
        L_0x00a7:
            android.util.Log.e(r9, r2, r8)     // Catch:{ all -> 0x00de }
            goto L_0x00d4
        L_0x00ab:
            r9 = move-exception
            r8 = r2
            r3 = r8
        L_0x00ae:
            r4 = r3
            goto L_0x00d8
        L_0x00b0:
            r9 = move-exception
            goto L_0x00b3
        L_0x00b2:
            r9 = move-exception
        L_0x00b3:
            r8 = r2
            r3 = r8
        L_0x00b5:
            java.lang.String r4 = f2707a     // Catch:{ all -> 0x00d6 }
            java.lang.String r5 = "Failed to read crashes"
            android.util.Log.e(r4, r5, r9)     // Catch:{ all -> 0x00d6 }
            if (r2 == 0) goto L_0x00c4
            r2.close()     // Catch:{ IOException -> 0x00c2 }
            goto L_0x00c4
        L_0x00c2:
            r8 = move-exception
            goto L_0x00cf
        L_0x00c4:
            if (r3 == 0) goto L_0x00c9
            r3.close()     // Catch:{ IOException -> 0x00c2 }
        L_0x00c9:
            if (r8 == 0) goto L_0x00d4
            r8.close()     // Catch:{ IOException -> 0x00c2 }
            goto L_0x00d4
        L_0x00cf:
            java.lang.String r9 = f2707a     // Catch:{ all -> 0x00de }
            java.lang.String r2 = "Failed to close buffers"
            goto L_0x00a7
        L_0x00d4:
            monitor-exit(r1)     // Catch:{ all -> 0x00de }
            return r0
        L_0x00d6:
            r9 = move-exception
            r4 = r2
        L_0x00d8:
            if (r4 == 0) goto L_0x00e2
            r4.close()     // Catch:{ IOException -> 0x00e0 }
            goto L_0x00e2
        L_0x00de:
            r8 = move-exception
            goto L_0x00f5
        L_0x00e0:
            r8 = move-exception
            goto L_0x00ed
        L_0x00e2:
            if (r3 == 0) goto L_0x00e7
            r3.close()     // Catch:{ IOException -> 0x00e0 }
        L_0x00e7:
            if (r8 == 0) goto L_0x00f4
            r8.close()     // Catch:{ IOException -> 0x00e0 }
            goto L_0x00f4
        L_0x00ed:
            java.lang.String r0 = f2707a     // Catch:{ all -> 0x00de }
            java.lang.String r2 = "Failed to close buffers"
            android.util.Log.e(r0, r2, r8)     // Catch:{ all -> 0x00de }
        L_0x00f4:
            throw r9     // Catch:{ all -> 0x00de }
        L_0x00f5:
            monitor-exit(r1)     // Catch:{ all -> 0x00de }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.f.e.a(android.content.Context, int):org.json.JSONArray");
    }

    private static JSONObject a(d dVar) {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("id", UUID.randomUUID().toString());
        jSONObject.put("type", dVar.a());
        jSONObject.put("time", t.a(dVar.b()));
        jSONObject.put("session_time", t.a(dVar.c()));
        jSONObject.put("session_id", dVar.d());
        jSONObject.put(UriUtil.DATA_SCHEME, dVar.e() != null ? new JSONObject(dVar.e()) : new JSONObject());
        jSONObject.put("attempt", 0);
        return jSONObject;
    }

    public static void a(d dVar, Context context) {
        if (dVar != null && context != null) {
            synchronized (b) {
                try {
                    JSONObject a2 = a(dVar);
                    FileOutputStream openFileOutput = context.openFileOutput("debuglogs", 32768);
                    openFileOutput.write((a2.toString() + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE).getBytes());
                    openFileOutput.close();
                    d(context);
                } catch (Exception e) {
                    Log.e(f2707a, "Failed to store crash", e);
                }
            }
        }
    }

    public static void a(String str) {
        Integer num = d.get(str);
        if (num == null) {
            num = 0;
        } else {
            d.remove(str);
        }
        d.put(str, Integer.valueOf(num.intValue() + 1));
    }

    private static void a(String str, int i) {
        if (!c.contains(str)) {
            if (d.containsKey(str)) {
                d.remove(str);
            }
            d.put(str, Integer.valueOf(i));
            return;
        }
        throw new RuntimeException("finished event should not be updated to OngoingEvent.");
    }

    public static int b(Context context) {
        return context.getApplicationContext().getSharedPreferences("DEBUG_PREF", 0).getInt("EventCount", 0) - c.size();
    }

    private static void b(Context context, int i) {
        SharedPreferences.Editor edit = context.getApplicationContext().getSharedPreferences("DEBUG_PREF", 0).edit();
        if (i < 0) {
            i = 0;
        }
        edit.putInt("EventCount", i).apply();
    }

    public static void b(String str) {
        if (d.containsKey(str)) {
            d.remove(str);
        }
        c.add(str);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.io.InputStreamReader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v15, resolved type: java.io.InputStreamReader} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v17, resolved type: java.io.InputStreamReader} */
    /* JADX WARNING: type inference failed for: r3v0 */
    /* JADX WARNING: type inference failed for: r3v2, types: [java.io.FileOutputStream] */
    /* JADX WARNING: type inference failed for: r3v3, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r3v4 */
    /* JADX WARNING: type inference failed for: r3v6, types: [java.io.BufferedReader] */
    /* JADX WARNING: type inference failed for: r3v7 */
    /* JADX WARNING: type inference failed for: r3v9 */
    /* JADX WARNING: type inference failed for: r5v12 */
    /* JADX WARNING: type inference failed for: r3v10 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:64:0x00f9 A[SYNTHETIC, Splitter:B:64:0x00f9] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x0101 A[Catch:{ IOException -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x0106 A[Catch:{ IOException -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010b A[Catch:{ IOException -> 0x00fd }] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x0127 A[SYNTHETIC, Splitter:B:82:0x0127] */
    /* JADX WARNING: Removed duplicated region for block: B:89:0x0131 A[Catch:{ IOException -> 0x012d }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x0136 A[Catch:{ IOException -> 0x012d }] */
    /* JADX WARNING: Removed duplicated region for block: B:93:0x013b A[Catch:{ IOException -> 0x012d }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean c(android.content.Context r11) {
        /*
            org.json.JSONArray r0 = new org.json.JSONArray
            r0.<init>()
            java.lang.Object r1 = b
            monitor-enter(r1)
            r2 = 0
            r3 = 0
            java.io.File r4 = new java.io.File     // Catch:{ IOException -> 0x00ec, JSONException -> 0x00ea, all -> 0x00e5 }
            java.io.File r5 = r11.getFilesDir()     // Catch:{ IOException -> 0x00ec, JSONException -> 0x00ea, all -> 0x00e5 }
            java.lang.String r6 = "debuglogs"
            r4.<init>(r5, r6)     // Catch:{ IOException -> 0x00ec, JSONException -> 0x00ea, all -> 0x00e5 }
            boolean r4 = r4.exists()     // Catch:{ IOException -> 0x00ec, JSONException -> 0x00ea, all -> 0x00e5 }
            if (r4 == 0) goto L_0x00ab
            java.lang.String r4 = "debuglogs"
            java.io.FileInputStream r4 = r11.openFileInput(r4)     // Catch:{ IOException -> 0x00ec, JSONException -> 0x00ea, all -> 0x00e5 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x00a7, JSONException -> 0x00a5, all -> 0x00a2 }
            r5.<init>(r4)     // Catch:{ IOException -> 0x00a7, JSONException -> 0x00a5, all -> 0x00a2 }
            java.io.BufferedReader r6 = new java.io.BufferedReader     // Catch:{ IOException -> 0x009e, JSONException -> 0x009c, all -> 0x0098 }
            r6.<init>(r5)     // Catch:{ IOException -> 0x009e, JSONException -> 0x009c, all -> 0x0098 }
        L_0x002b:
            java.lang.String r7 = r6.readLine()     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            if (r7 == 0) goto L_0x005b
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r8.<init>(r7)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            java.lang.String r7 = "id"
            java.lang.String r7 = r8.getString(r7)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            java.util.Set<java.lang.String> r9 = c     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            boolean r9 = r9.contains(r7)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            if (r9 != 0) goto L_0x002b
            java.util.Map<java.lang.String, java.lang.Integer> r9 = d     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            boolean r9 = r9.containsKey(r7)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            if (r9 == 0) goto L_0x0057
            java.lang.String r9 = "attempt"
            java.util.Map<java.lang.String, java.lang.Integer> r10 = d     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            java.lang.Object r7 = r10.get(r7)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r8.put(r9, r7)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
        L_0x0057:
            r0.put(r8)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            goto L_0x002b
        L_0x005b:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r7.<init>()     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            int r8 = r0.length()     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r9 = 0
        L_0x0065:
            if (r9 >= r8) goto L_0x007a
            org.json.JSONObject r10 = r0.getJSONObject(r9)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            java.lang.String r10 = r10.toString()     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r7.append(r10)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r10 = 10
            r7.append(r10)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            int r9 = r9 + 1
            goto L_0x0065
        L_0x007a:
            java.lang.String r0 = "debuglogs"
            java.io.FileOutputStream r3 = r11.openFileOutput(r0, r2)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            java.lang.String r0 = r7.toString()     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            byte[] r0 = r0.getBytes()     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r3.write(r0)     // Catch:{ IOException -> 0x0093, JSONException -> 0x0091, all -> 0x008e }
            r0 = r3
            r3 = r6
            goto L_0x00ae
        L_0x008e:
            r11 = move-exception
            goto L_0x0125
        L_0x0091:
            r11 = move-exception
            goto L_0x0094
        L_0x0093:
            r11 = move-exception
        L_0x0094:
            r0 = r3
            r3 = r6
            goto L_0x00f0
        L_0x0098:
            r11 = move-exception
            r6 = r3
            goto L_0x0125
        L_0x009c:
            r11 = move-exception
            goto L_0x009f
        L_0x009e:
            r11 = move-exception
        L_0x009f:
            r0 = r3
            goto L_0x00f0
        L_0x00a2:
            r11 = move-exception
            r5 = r3
            goto L_0x00e8
        L_0x00a5:
            r11 = move-exception
            goto L_0x00a8
        L_0x00a7:
            r11 = move-exception
        L_0x00a8:
            r0 = r3
            r5 = r0
            goto L_0x00f0
        L_0x00ab:
            r0 = r3
            r4 = r0
            r5 = r4
        L_0x00ae:
            int r6 = b((android.content.Context) r11)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00e1 }
            b(r11, r6)     // Catch:{ IOException -> 0x00e3, JSONException -> 0x00e1 }
            if (r3 == 0) goto L_0x00bd
            r3.close()     // Catch:{ IOException -> 0x00bb }
            goto L_0x00bd
        L_0x00bb:
            r11 = move-exception
            goto L_0x00cd
        L_0x00bd:
            if (r5 == 0) goto L_0x00c2
            r5.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00c2:
            if (r4 == 0) goto L_0x00c7
            r4.close()     // Catch:{ IOException -> 0x00bb }
        L_0x00c7:
            if (r0 == 0) goto L_0x00d4
            r0.close()     // Catch:{ IOException -> 0x00bb }
            goto L_0x00d4
        L_0x00cd:
            java.lang.String r0 = f2707a     // Catch:{ all -> 0x012b }
            java.lang.String r2 = "Failed to close buffers"
            android.util.Log.e(r0, r2, r11)     // Catch:{ all -> 0x012b }
        L_0x00d4:
            java.util.Set<java.lang.String> r11 = c     // Catch:{ all -> 0x012b }
            r11.clear()     // Catch:{ all -> 0x012b }
            java.util.Map<java.lang.String, java.lang.Integer> r11 = d     // Catch:{ all -> 0x012b }
            r11.clear()     // Catch:{ all -> 0x012b }
            monitor-exit(r1)     // Catch:{ all -> 0x012b }
            r11 = 1
            return r11
        L_0x00e1:
            r11 = move-exception
            goto L_0x00f0
        L_0x00e3:
            r11 = move-exception
            goto L_0x00f0
        L_0x00e5:
            r11 = move-exception
            r4 = r3
            r5 = r4
        L_0x00e8:
            r6 = r5
            goto L_0x0125
        L_0x00ea:
            r11 = move-exception
            goto L_0x00ed
        L_0x00ec:
            r11 = move-exception
        L_0x00ed:
            r0 = r3
            r4 = r0
            r5 = r4
        L_0x00f0:
            java.lang.String r6 = f2707a     // Catch:{ all -> 0x0122 }
            java.lang.String r7 = "Failed to rewrite File."
            android.util.Log.e(r6, r7, r11)     // Catch:{ all -> 0x0122 }
            if (r3 == 0) goto L_0x00ff
            r3.close()     // Catch:{ IOException -> 0x00fd }
            goto L_0x00ff
        L_0x00fd:
            r11 = move-exception
            goto L_0x010f
        L_0x00ff:
            if (r5 == 0) goto L_0x0104
            r5.close()     // Catch:{ IOException -> 0x00fd }
        L_0x0104:
            if (r4 == 0) goto L_0x0109
            r4.close()     // Catch:{ IOException -> 0x00fd }
        L_0x0109:
            if (r0 == 0) goto L_0x0116
            r0.close()     // Catch:{ IOException -> 0x00fd }
            goto L_0x0116
        L_0x010f:
            java.lang.String r0 = f2707a     // Catch:{ all -> 0x012b }
            java.lang.String r3 = "Failed to close buffers"
            android.util.Log.e(r0, r3, r11)     // Catch:{ all -> 0x012b }
        L_0x0116:
            java.util.Set<java.lang.String> r11 = c     // Catch:{ all -> 0x012b }
            r11.clear()     // Catch:{ all -> 0x012b }
            java.util.Map<java.lang.String, java.lang.Integer> r11 = d     // Catch:{ all -> 0x012b }
            r11.clear()     // Catch:{ all -> 0x012b }
            monitor-exit(r1)     // Catch:{ all -> 0x012b }
            return r2
        L_0x0122:
            r11 = move-exception
            r6 = r3
            r3 = r0
        L_0x0125:
            if (r6 == 0) goto L_0x012f
            r6.close()     // Catch:{ IOException -> 0x012d }
            goto L_0x012f
        L_0x012b:
            r11 = move-exception
            goto L_0x0151
        L_0x012d:
            r0 = move-exception
            goto L_0x013f
        L_0x012f:
            if (r5 == 0) goto L_0x0134
            r5.close()     // Catch:{ IOException -> 0x012d }
        L_0x0134:
            if (r4 == 0) goto L_0x0139
            r4.close()     // Catch:{ IOException -> 0x012d }
        L_0x0139:
            if (r3 == 0) goto L_0x0146
            r3.close()     // Catch:{ IOException -> 0x012d }
            goto L_0x0146
        L_0x013f:
            java.lang.String r2 = f2707a     // Catch:{ all -> 0x012b }
            java.lang.String r3 = "Failed to close buffers"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x012b }
        L_0x0146:
            java.util.Set<java.lang.String> r0 = c     // Catch:{ all -> 0x012b }
            r0.clear()     // Catch:{ all -> 0x012b }
            java.util.Map<java.lang.String, java.lang.Integer> r0 = d     // Catch:{ all -> 0x012b }
            r0.clear()     // Catch:{ all -> 0x012b }
            throw r11     // Catch:{ all -> 0x012b }
        L_0x0151:
            monitor-exit(r1)     // Catch:{ all -> 0x012b }
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.f.e.c(android.content.Context):boolean");
    }

    public static boolean c(String str) {
        return c.contains(str) || d.containsKey(str);
    }

    private static void d(Context context) {
        b(context, context.getApplicationContext().getSharedPreferences("DEBUG_PREF", 0).getInt("EventCount", 0) + 1);
    }
}
