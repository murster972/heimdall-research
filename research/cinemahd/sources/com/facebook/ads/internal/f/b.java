package com.facebook.ads.internal.f;

import java.util.HashMap;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final String f2704a;
    private final Map<String, String> b;
    private final String c;

    public b(String str, Map<String, String> map) {
        this(str, map, false);
    }

    public b(String str, Map<String, String> map, boolean z) {
        this.f2704a = str;
        this.b = map;
        this.c = z ? DiskLruCache.VERSION_1 : "0";
    }

    public Map<String, String> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("stacktrace", this.f2704a);
        hashMap.put("caught_exception", this.c);
        hashMap.putAll(this.b);
        return hashMap;
    }
}
