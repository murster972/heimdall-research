package com.facebook.ads.internal.a;

import android.content.Context;
import com.facebook.ads.internal.j.a;
import com.facebook.ads.internal.m.c;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f2605a;
    protected final c b;
    protected final String c;

    public b(Context context, c cVar, String str) {
        this.f2605a = context;
        this.b = cVar;
        this.c = str;
    }

    @Deprecated
    public abstract a.C0022a a();

    public abstract void b();
}
