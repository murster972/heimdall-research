package com.facebook.ads.internal.a;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.internal.q.a.a;
import com.facebook.common.util.UriUtil;
import com.unity3d.services.purchasing.core.TransactionErrorDetailsUtilities;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2606a = "c";

    public static b a(Context context, com.facebook.ads.internal.m.c cVar, String str, Uri uri, Map<String, String> map) {
        if (uri == null) {
            return null;
        }
        String authority = uri.getAuthority();
        String queryParameter = uri.getQueryParameter(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_URL);
        if (!TextUtils.isEmpty(uri.getQueryParameter(UriUtil.DATA_SCHEME))) {
            try {
                JSONObject jSONObject = new JSONObject(uri.getQueryParameter(UriUtil.DATA_SCHEME));
                Iterator<String> keys = jSONObject.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    map.put(next, jSONObject.getString(next));
                }
            } catch (JSONException e) {
                Log.w(f2606a, "Unable to parse json data in AdActionFactory.", e);
            }
        }
        l a2 = l.a(cVar, a.a());
        char c = 65535;
        int hashCode = authority.hashCode();
        if (hashCode != -1458789996) {
            if (hashCode != 109770977) {
                if (hashCode == 1546100943 && authority.equals("open_link")) {
                    c = 1;
                }
            } else if (authority.equals(TransactionErrorDetailsUtilities.STORE)) {
                c = 0;
            }
        } else if (authority.equals("passthrough")) {
            c = 2;
        }
        if (c != 0) {
            return c != 1 ? c != 2 ? new k(context, cVar, str, uri) : new j(context, cVar, str, uri, map) : new i(context, cVar, str, uri, map, a2);
        }
        if (queryParameter != null) {
            return null;
        }
        return new f(context, cVar, str, uri, map, a2);
    }

    public static boolean a(String str) {
        return TransactionErrorDetailsUtilities.STORE.equalsIgnoreCase(str) || "open_link".equalsIgnoreCase(str);
    }
}
