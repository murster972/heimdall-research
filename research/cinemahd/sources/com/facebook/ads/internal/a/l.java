package com.facebook.ads.internal.a;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import com.facebook.ads.internal.m.c;
import java.lang.ref.WeakReference;
import java.util.HashMap;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private final c f2609a;
    private Application b;
    private a c;
    private long d = 0;
    private String e = null;
    private a f = null;

    @TargetApi(14)
    private static class a implements Application.ActivityLifecycleCallbacks {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<Activity> f2610a;
        private l b;

        public a(Activity activity, l lVar) {
            this.f2610a = new WeakReference<>(activity);
            this.b = lVar;
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public void onActivityResumed(Activity activity) {
            if (this.b != null) {
                Activity activity2 = (Activity) this.f2610a.get();
                if (activity2 == null || (activity2 != null && activity.equals(activity2))) {
                    this.b.a();
                    this.b = null;
                }
            }
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    private l(c cVar, Activity activity, int i) {
        this.f2609a = cVar;
        this.b = activity.getApplication();
        this.c = new a(activity, this);
    }

    public static l a(c cVar, Activity activity) {
        return a(cVar, activity, Build.VERSION.SDK_INT);
    }

    protected static l a(c cVar, Activity activity, int i) {
        if (activity == null || i < 14) {
            return null;
        }
        return new l(cVar, activity, i);
    }

    private void a(String str, long j, long j2, a aVar) {
        HashMap hashMap = new HashMap();
        hashMap.put("leave_time", Long.toString(j));
        hashMap.put("back_time", Long.toString(j2));
        if (aVar != null) {
            hashMap.put("outcome", aVar.name());
        }
        this.f2609a.j(str, hashMap);
    }

    @TargetApi(14)
    public void a() {
        a aVar;
        a(this.e, this.d, System.currentTimeMillis(), this.f);
        Application application = this.b;
        if (application != null && (aVar = this.c) != null) {
            application.unregisterActivityLifecycleCallbacks(aVar);
            this.c = null;
            this.b = null;
        }
    }

    public void a(a aVar) {
        this.f = aVar;
    }

    @TargetApi(14)
    public void a(String str) {
        this.e = str;
        if (this.c == null || this.b == null) {
            a(str, -1, -1, a.CANNOT_TRACK);
            return;
        }
        this.d = System.currentTimeMillis();
        this.b.registerActivityLifecycleCallbacks(this.c);
    }
}
