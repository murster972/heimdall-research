package com.facebook.ads.internal.p.b;

import com.facebook.ads.internal.p.b.a.a;
import java.io.File;

class c {

    /* renamed from: a  reason: collision with root package name */
    public final File f2783a;
    public final com.facebook.ads.internal.p.b.a.c b;
    public final a c;

    c(File file, com.facebook.ads.internal.p.b.a.c cVar, a aVar) {
        this.f2783a = file;
        this.b = cVar;
        this.c = aVar;
    }

    /* access modifiers changed from: package-private */
    public File a(String str) {
        return new File(this.f2783a, this.b.a(str));
    }
}
