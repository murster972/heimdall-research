package com.facebook.ads.internal.p.b;

import android.util.Log;
import java.lang.Thread;
import java.util.concurrent.atomic.AtomicInteger;

class k {

    /* renamed from: a  reason: collision with root package name */
    private final n f2795a;
    private final a b;
    private final Object c = new Object();
    private final Object d = new Object();
    private final AtomicInteger e;
    private volatile Thread f;
    private volatile boolean g;
    private volatile int h = -1;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            k.this.e();
        }
    }

    public k(n nVar, a aVar) {
        this.f2795a = (n) j.a(nVar);
        this.b = (a) j.a(aVar);
        this.e = new AtomicInteger();
    }

    private void b() {
        int i = this.e.get();
        if (i >= 1) {
            this.e.set(0);
            throw new l("Error reading source " + i + " times");
        }
    }

    private void b(long j, long j2) {
        a(j, j2);
        synchronized (this.c) {
            this.c.notifyAll();
        }
    }

    private synchronized void c() {
        boolean z = (this.f == null || this.f.getState() == Thread.State.TERMINATED) ? false : true;
        if (!this.g && !this.b.d() && !z) {
            a aVar = new a();
            this.f = new Thread(aVar, "Source reader for " + this.f2795a);
            this.f.start();
        }
    }

    private void d() {
        synchronized (this.c) {
            try {
                this.c.wait(1000);
            } catch (InterruptedException e2) {
                throw new l("Waiting source data is interrupted!", e2);
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0038, code lost:
        r1 = r1 + r4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void e() {
        /*
            r8 = this;
            r0 = -1
            r1 = 0
            com.facebook.ads.internal.p.b.a r2 = r8.b     // Catch:{ all -> 0x0048 }
            int r1 = r2.a()     // Catch:{ all -> 0x0048 }
            com.facebook.ads.internal.p.b.n r2 = r8.f2795a     // Catch:{ all -> 0x0048 }
            r2.a((int) r1)     // Catch:{ all -> 0x0048 }
            com.facebook.ads.internal.p.b.n r2 = r8.f2795a     // Catch:{ all -> 0x0048 }
            int r2 = r2.a()     // Catch:{ all -> 0x0048 }
            r3 = 8192(0x2000, float:1.14794E-41)
            byte[] r3 = new byte[r3]     // Catch:{ all -> 0x0046 }
        L_0x0017:
            com.facebook.ads.internal.p.b.n r4 = r8.f2795a     // Catch:{ all -> 0x0046 }
            int r4 = r4.a((byte[]) r3)     // Catch:{ all -> 0x0046 }
            if (r4 == r0) goto L_0x0042
            java.lang.Object r5 = r8.d     // Catch:{ all -> 0x0046 }
            monitor-enter(r5)     // Catch:{ all -> 0x0046 }
            boolean r6 = r8.g()     // Catch:{ all -> 0x003f }
            if (r6 == 0) goto L_0x0032
            monitor-exit(r5)     // Catch:{ all -> 0x003f }
            r8.h()
            long r0 = (long) r1
            long r2 = (long) r2
            r8.b(r0, r2)
            return
        L_0x0032:
            com.facebook.ads.internal.p.b.a r6 = r8.b     // Catch:{ all -> 0x003f }
            r6.a(r3, r4)     // Catch:{ all -> 0x003f }
            monitor-exit(r5)     // Catch:{ all -> 0x003f }
            int r1 = r1 + r4
            long r4 = (long) r1
            long r6 = (long) r2
            r8.b(r4, r6)     // Catch:{ all -> 0x0046 }
            goto L_0x0017
        L_0x003f:
            r0 = move-exception
            monitor-exit(r5)     // Catch:{ all -> 0x003f }
            throw r0     // Catch:{ all -> 0x0046 }
        L_0x0042:
            r8.f()     // Catch:{ all -> 0x0046 }
            goto L_0x0053
        L_0x0046:
            r0 = move-exception
            goto L_0x004b
        L_0x0048:
            r2 = move-exception
            r0 = r2
            r2 = -1
        L_0x004b:
            java.util.concurrent.atomic.AtomicInteger r3 = r8.e     // Catch:{ all -> 0x005c }
            r3.incrementAndGet()     // Catch:{ all -> 0x005c }
            r8.a((java.lang.Throwable) r0)     // Catch:{ all -> 0x005c }
        L_0x0053:
            r8.h()
            long r0 = (long) r1
            long r2 = (long) r2
            r8.b(r0, r2)
            return
        L_0x005c:
            r0 = move-exception
            r8.h()
            long r3 = (long) r1
            long r1 = (long) r2
            r8.b(r3, r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.p.b.k.e():void");
    }

    private void f() {
        synchronized (this.d) {
            if (!g() && this.b.a() == this.f2795a.a()) {
                this.b.c();
            }
        }
    }

    private boolean g() {
        return Thread.currentThread().isInterrupted() || this.g;
    }

    private void h() {
        try {
            this.f2795a.b();
        } catch (l e2) {
            a((Throwable) new l("Error closing source " + this.f2795a, e2));
        }
    }

    public int a(byte[] bArr, long j, int i) {
        m.a(bArr, j, i);
        while (!this.b.d() && ((long) this.b.a()) < ((long) i) + j && !this.g) {
            c();
            d();
            b();
        }
        int a2 = this.b.a(bArr, j, i);
        if (this.b.d() && this.h != 100) {
            this.h = 100;
            a(100);
        }
        return a2;
    }

    public void a() {
        synchronized (this.d) {
            Log.d("ProxyCache", "Shutdown proxy for " + this.f2795a);
            try {
                this.g = true;
                if (this.f != null) {
                    this.f.interrupt();
                }
                this.b.b();
            } catch (l e2) {
                a((Throwable) e2);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
    }

    /* access modifiers changed from: protected */
    public void a(long j, long j2) {
        boolean z = true;
        int i = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
        int i2 = i == 0 ? 100 : (int) ((j * 100) / j2);
        boolean z2 = i2 != this.h;
        if (i < 0) {
            z = false;
        }
        if (z && z2) {
            a(i2);
        }
        this.h = i2;
    }

    /* access modifiers changed from: protected */
    public final void a(Throwable th) {
        if (th instanceof i) {
            Log.d("ProxyCache", "ProxyCache is interrupted");
        } else {
            Log.e("ProxyCache", "ProxyCache error", th);
        }
    }
}
