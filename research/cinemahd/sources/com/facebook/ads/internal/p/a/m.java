package com.facebook.ads.internal.p.a;

public class m extends Exception {
    private static final long serialVersionUID = -2413629666163901633L;

    /* renamed from: a  reason: collision with root package name */
    private n f2776a;

    public m(Exception exc, n nVar) {
        super(exc);
        this.f2776a = nVar;
    }

    public n a() {
        return this.f2776a;
    }
}
