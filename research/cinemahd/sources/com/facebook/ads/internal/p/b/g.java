package com.facebook.ads.internal.p.b;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.facebook.ads.internal.p.b.a.b;
import java.io.File;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

final class g {

    /* renamed from: a  reason: collision with root package name */
    private final AtomicInteger f2792a = new AtomicInteger(0);
    private final String b;
    private volatile e c;
    private final List<b> d = new CopyOnWriteArrayList();
    private final b e;
    private final c f;

    private static final class a extends Handler implements b {

        /* renamed from: a  reason: collision with root package name */
        private final String f2793a;
        private final List<b> b;

        public a(String str, List<b> list) {
            super(Looper.getMainLooper());
            this.f2793a = str;
            this.b = list;
        }

        public void a(File file, String str, int i) {
            Message obtainMessage = obtainMessage();
            obtainMessage.arg1 = i;
            obtainMessage.obj = file;
            sendMessage(obtainMessage);
        }

        public void handleMessage(Message message) {
            for (b a2 : this.b) {
                a2.a((File) message.obj, this.f2793a, message.arg1);
            }
        }
    }

    public g(String str, c cVar) {
        this.b = (String) j.a(str);
        this.f = (c) j.a(cVar);
        this.e = new a(str, this.d);
    }

    private synchronized void c() {
        this.c = this.c == null ? e() : this.c;
    }

    private synchronized void d() {
        if (this.f2792a.decrementAndGet() <= 0) {
            this.c.a();
            this.c = null;
        }
    }

    private e e() {
        e eVar = new e(new h(this.b), new b(this.f.a(this.b), this.f.c));
        eVar.a(this.e);
        return eVar;
    }

    public void a() {
        this.d.clear();
        if (this.c != null) {
            this.c.a((b) null);
            this.c.a();
            this.c = null;
        }
        this.f2792a.set(0);
    }

    public void a(d dVar, Socket socket) {
        c();
        try {
            this.f2792a.incrementAndGet();
            this.c.a(dVar, socket);
        } finally {
            d();
        }
    }

    public int b() {
        return this.f2792a.get();
    }
}
