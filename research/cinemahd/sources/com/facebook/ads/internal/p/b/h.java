package com.facebook.ads.internal.p.b;

import android.text.TextUtils;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class h implements n {

    /* renamed from: a  reason: collision with root package name */
    public final String f2794a;
    private HttpURLConnection b;
    private InputStream c;
    private volatile int d;
    private volatile String e;

    public h(h hVar) {
        this.d = Integer.MIN_VALUE;
        this.f2794a = hVar.f2794a;
        this.e = hVar.e;
        this.d = hVar.d;
    }

    public h(String str) {
        this(str, m.a(str));
    }

    public h(String str, String str2) {
        this.d = Integer.MIN_VALUE;
        this.f2794a = (String) j.a(str);
        this.e = str2;
    }

    private int a(HttpURLConnection httpURLConnection, int i, int i2) {
        int contentLength = httpURLConnection.getContentLength();
        return i2 == 200 ? contentLength : i2 == 206 ? contentLength + i : this.d;
    }

    private HttpURLConnection a(int i, int i2) {
        String str;
        HttpURLConnection httpURLConnection;
        boolean z;
        String str2 = this.f2794a;
        int i3 = 0;
        do {
            StringBuilder sb = new StringBuilder();
            sb.append("Open connection ");
            if (i > 0) {
                str = " with offset " + i;
            } else {
                str = "";
            }
            sb.append(str);
            sb.append(" to ");
            sb.append(str2);
            Log.d("ProxyCache", sb.toString());
            httpURLConnection = (HttpURLConnection) new URL(str2).openConnection();
            if (i > 0) {
                httpURLConnection.setRequestProperty("Range", "bytes=" + i + "-");
            }
            if (i2 > 0) {
                httpURLConnection.setConnectTimeout(i2);
                httpURLConnection.setReadTimeout(i2);
            }
            int responseCode = httpURLConnection.getResponseCode();
            z = responseCode == 301 || responseCode == 302 || responseCode == 303;
            if (z) {
                str2 = httpURLConnection.getHeaderField("Location");
                i3++;
                httpURLConnection.disconnect();
            }
            if (i3 > 5) {
                throw new l("Too many redirects: " + i3);
            }
        } while (z);
        return httpURLConnection;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x008d  */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r6 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Read content info from "
            r0.append(r1)
            java.lang.String r1 = r6.f2794a
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            java.lang.String r1 = "ProxyCache"
            android.util.Log.d(r1, r0)
            r0 = 0
            r2 = 10000(0x2710, float:1.4013E-41)
            r3 = 0
            java.net.HttpURLConnection r0 = r6.a(r0, r2)     // Catch:{ IOException -> 0x0067, all -> 0x0064 }
            int r2 = r0.getContentLength()     // Catch:{ IOException -> 0x0062 }
            r6.d = r2     // Catch:{ IOException -> 0x0062 }
            java.lang.String r2 = r0.getContentType()     // Catch:{ IOException -> 0x0062 }
            r6.e = r2     // Catch:{ IOException -> 0x0062 }
            java.io.InputStream r3 = r0.getInputStream()     // Catch:{ IOException -> 0x0062 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0062 }
            r2.<init>()     // Catch:{ IOException -> 0x0062 }
            java.lang.String r4 = "Content info for `"
            r2.append(r4)     // Catch:{ IOException -> 0x0062 }
            java.lang.String r4 = r6.f2794a     // Catch:{ IOException -> 0x0062 }
            r2.append(r4)     // Catch:{ IOException -> 0x0062 }
            java.lang.String r4 = "`: mime: "
            r2.append(r4)     // Catch:{ IOException -> 0x0062 }
            java.lang.String r4 = r6.e     // Catch:{ IOException -> 0x0062 }
            r2.append(r4)     // Catch:{ IOException -> 0x0062 }
            java.lang.String r4 = ", content-length: "
            r2.append(r4)     // Catch:{ IOException -> 0x0062 }
            int r4 = r6.d     // Catch:{ IOException -> 0x0062 }
            r2.append(r4)     // Catch:{ IOException -> 0x0062 }
            java.lang.String r2 = r2.toString()     // Catch:{ IOException -> 0x0062 }
            android.util.Log.i(r1, r2)     // Catch:{ IOException -> 0x0062 }
            com.facebook.ads.internal.p.b.m.a((java.io.Closeable) r3)
            if (r0 == 0) goto L_0x0087
            goto L_0x0084
        L_0x0060:
            r1 = move-exception
            goto L_0x0088
        L_0x0062:
            r2 = move-exception
            goto L_0x0069
        L_0x0064:
            r1 = move-exception
            r0 = r3
            goto L_0x0088
        L_0x0067:
            r2 = move-exception
            r0 = r3
        L_0x0069:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0060 }
            r4.<init>()     // Catch:{ all -> 0x0060 }
            java.lang.String r5 = "Error fetching info from "
            r4.append(r5)     // Catch:{ all -> 0x0060 }
            java.lang.String r5 = r6.f2794a     // Catch:{ all -> 0x0060 }
            r4.append(r5)     // Catch:{ all -> 0x0060 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0060 }
            android.util.Log.e(r1, r4, r2)     // Catch:{ all -> 0x0060 }
            com.facebook.ads.internal.p.b.m.a((java.io.Closeable) r3)
            if (r0 == 0) goto L_0x0087
        L_0x0084:
            r0.disconnect()
        L_0x0087:
            return
        L_0x0088:
            com.facebook.ads.internal.p.b.m.a((java.io.Closeable) r3)
            if (r0 == 0) goto L_0x0090
            r0.disconnect()
        L_0x0090:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.p.b.h.d():void");
    }

    public synchronized int a() {
        if (this.d == Integer.MIN_VALUE) {
            d();
        }
        return this.d;
    }

    public int a(byte[] bArr) {
        InputStream inputStream = this.c;
        if (inputStream != null) {
            try {
                return inputStream.read(bArr, 0, bArr.length);
            } catch (InterruptedIOException e2) {
                throw new i("Reading source " + this.f2794a + " is interrupted", e2);
            } catch (IOException e3) {
                throw new l("Error reading data from " + this.f2794a, e3);
            }
        } else {
            throw new l("Error reading data from " + this.f2794a + ": connection is absent!");
        }
    }

    public void a(int i) {
        try {
            this.b = a(i, -1);
            this.e = this.b.getContentType();
            this.c = new BufferedInputStream(this.b.getInputStream(), 8192);
            this.d = a(this.b, i, this.b.getResponseCode());
        } catch (IOException e2) {
            throw new l("Error opening connection for " + this.f2794a + " with offset " + i, e2);
        }
    }

    public void b() {
        HttpURLConnection httpURLConnection = this.b;
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (NullPointerException e2) {
                throw new l("Error disconnecting HttpUrlConnection", e2);
            }
        }
    }

    public synchronized String c() {
        if (TextUtils.isEmpty(this.e)) {
            d();
        }
        return this.e;
    }

    public String toString() {
        return "HttpUrlSource{url='" + this.f2794a + "}";
    }
}
