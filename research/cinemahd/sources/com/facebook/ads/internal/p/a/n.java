package com.facebook.ads.internal.p.a;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;
import java.util.Map;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private int f2777a;
    private String b;
    private Map<String, List<String>> c;
    private byte[] d;

    public n(HttpURLConnection httpURLConnection, byte[] bArr) {
        try {
            this.f2777a = httpURLConnection.getResponseCode();
            this.b = httpURLConnection.getURL().toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.c = httpURLConnection.getHeaderFields();
        this.d = bArr;
    }

    public int a() {
        return this.f2777a;
    }

    public String b() {
        return this.b;
    }

    public Map<String, List<String>> c() {
        return this.c;
    }

    public byte[] d() {
        return this.d;
    }

    public String e() {
        byte[] bArr = this.d;
        if (bArr != null) {
            return new String(bArr);
        }
        return null;
    }
}
