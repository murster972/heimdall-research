package com.facebook.ads.internal.p.a;

import android.os.Build;
import com.facebook.ads.AdError;
import java.io.OutputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class a {
    private static int[] f = new int[20];
    private static final String g = "a";

    /* renamed from: a  reason: collision with root package name */
    protected final q f2770a = new f() {
    };
    protected final d b = new e();
    protected r c = new g();
    protected int d = AdError.SERVER_ERROR_CODE;
    protected int e = 8000;
    private int h = 3;
    private Map<String, String> i = new TreeMap();
    private boolean j;
    private Set<String> k;
    private Set<String> l;

    static {
        c();
        if (Build.VERSION.SDK_INT > 8) {
            a();
        }
    }

    public static void a() {
        if (CookieHandler.getDefault() == null) {
            CookieHandler.setDefault(new CookieManager());
        }
    }

    private static void c() {
        if (Build.VERSION.SDK_INT < 8) {
            System.setProperty("http.keepAlive", "false");
        }
    }

    private void c(HttpURLConnection httpURLConnection) {
        for (String next : this.i.keySet()) {
            httpURLConnection.setRequestProperty(next, this.i.get(next));
        }
    }

    /* access modifiers changed from: protected */
    public int a(int i2) {
        return f[i2 + 2] * 1000;
    }

    /* access modifiers changed from: protected */
    public int a(HttpURLConnection httpURLConnection, byte[] bArr) {
        OutputStream outputStream = null;
        try {
            outputStream = this.f2770a.a(httpURLConnection);
            if (outputStream != null) {
                this.f2770a.a(outputStream, bArr);
            }
            return httpURLConnection.getResponseCode();
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (Exception unused) {
                }
            }
        }
    }

    public a a(String str, String str2) {
        this.i.put(str, str2);
        return this;
    }

    public n a(l lVar) {
        long currentTimeMillis = System.currentTimeMillis();
        int i2 = 0;
        while (i2 < this.h) {
            try {
                c(a(i2));
                if (this.c.a()) {
                    r rVar = this.c;
                    rVar.a((i2 + 1) + "of" + this.h + ", trying " + lVar.a());
                }
                currentTimeMillis = System.currentTimeMillis();
                n a2 = a(lVar.a(), lVar.b(), lVar.c(), lVar.d());
                if (a2 != null) {
                    return a2;
                }
                i2++;
            } catch (m e2) {
                if (a((Throwable) e2, currentTimeMillis) && i2 < this.h - 1) {
                    continue;
                } else if (!this.f2770a.a(e2) || i2 >= this.h - 1) {
                    throw e2;
                } else {
                    try {
                        Thread.sleep((long) this.d);
                    } catch (InterruptedException unused) {
                        throw e2;
                    }
                }
            }
        }
        return null;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:64|65|66|67|68) */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0087, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:?, code lost:
        r5.printStackTrace();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00bd, code lost:
        throw new com.facebook.ads.internal.p.a.m(r5, (com.facebook.ads.internal.p.a.n) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:71:0x00c3, code lost:
        throw new com.facebook.ads.internal.p.a.m(r5, (com.facebook.ads.internal.p.a.n) null);
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:52:0x0096, B:64:0x00b5] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:64:0x00b5 */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0096 A[SYNTHETIC, Splitter:B:52:0x0096] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00cc  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x00d3  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.ads.internal.p.a.n a(java.lang.String r4, com.facebook.ads.internal.p.a.j r5, java.lang.String r6, byte[] r7) {
        /*
            r3 = this;
            r0 = 0
            r1 = 0
            r3.j = r0     // Catch:{ Exception -> 0x008e, all -> 0x008b }
            java.net.HttpURLConnection r4 = r3.a((java.lang.String) r4)     // Catch:{ Exception -> 0x008e, all -> 0x008b }
            r3.a((java.net.HttpURLConnection) r4, (com.facebook.ads.internal.p.a.j) r5, (java.lang.String) r6)     // Catch:{ Exception -> 0x0089 }
            r3.c((java.net.HttpURLConnection) r4)     // Catch:{ Exception -> 0x0089 }
            com.facebook.ads.internal.p.a.r r5 = r3.c     // Catch:{ Exception -> 0x0089 }
            boolean r5 = r5.a()     // Catch:{ Exception -> 0x0089 }
            if (r5 == 0) goto L_0x001b
            com.facebook.ads.internal.p.a.r r5 = r3.c     // Catch:{ Exception -> 0x0089 }
            r5.a(r4, r7)     // Catch:{ Exception -> 0x0089 }
        L_0x001b:
            r4.connect()     // Catch:{ Exception -> 0x0089 }
            r5 = 1
            r3.j = r5     // Catch:{ Exception -> 0x0089 }
            java.util.Set<java.lang.String> r6 = r3.l     // Catch:{ Exception -> 0x0089 }
            if (r6 == 0) goto L_0x002f
            java.util.Set<java.lang.String> r6 = r3.l     // Catch:{ Exception -> 0x0089 }
            boolean r6 = r6.isEmpty()     // Catch:{ Exception -> 0x0089 }
            if (r6 != 0) goto L_0x002f
            r6 = 1
            goto L_0x0030
        L_0x002f:
            r6 = 0
        L_0x0030:
            java.util.Set<java.lang.String> r2 = r3.k     // Catch:{ Exception -> 0x0089 }
            if (r2 == 0) goto L_0x003d
            java.util.Set<java.lang.String> r2 = r3.k     // Catch:{ Exception -> 0x0089 }
            boolean r2 = r2.isEmpty()     // Catch:{ Exception -> 0x0089 }
            if (r2 != 0) goto L_0x003d
            goto L_0x003e
        L_0x003d:
            r5 = 0
        L_0x003e:
            boolean r0 = r4 instanceof javax.net.ssl.HttpsURLConnection     // Catch:{ Exception -> 0x0089 }
            if (r0 == 0) goto L_0x0059
            if (r6 != 0) goto L_0x0046
            if (r5 == 0) goto L_0x0059
        L_0x0046:
            r5 = r4
            javax.net.ssl.HttpsURLConnection r5 = (javax.net.ssl.HttpsURLConnection) r5     // Catch:{ Exception -> 0x0051 }
            java.util.Set<java.lang.String> r6 = r3.l     // Catch:{ Exception -> 0x0051 }
            java.util.Set<java.lang.String> r0 = r3.k     // Catch:{ Exception -> 0x0051 }
            com.facebook.ads.internal.p.a.o.a(r5, r6, r0)     // Catch:{ Exception -> 0x0051 }
            goto L_0x0059
        L_0x0051:
            r5 = move-exception
            java.lang.String r6 = g     // Catch:{ Exception -> 0x0089 }
            java.lang.String r0 = "Unable to validate SSL certificates."
            android.util.Log.e(r6, r0, r5)     // Catch:{ Exception -> 0x0089 }
        L_0x0059:
            boolean r5 = r4.getDoOutput()     // Catch:{ Exception -> 0x0089 }
            if (r5 == 0) goto L_0x0064
            if (r7 == 0) goto L_0x0064
            r3.a((java.net.HttpURLConnection) r4, (byte[]) r7)     // Catch:{ Exception -> 0x0089 }
        L_0x0064:
            boolean r5 = r4.getDoInput()     // Catch:{ Exception -> 0x0089 }
            if (r5 == 0) goto L_0x006f
            com.facebook.ads.internal.p.a.n r5 = r3.a((java.net.HttpURLConnection) r4)     // Catch:{ Exception -> 0x0089 }
            goto L_0x0074
        L_0x006f:
            com.facebook.ads.internal.p.a.n r5 = new com.facebook.ads.internal.p.a.n     // Catch:{ Exception -> 0x0089 }
            r5.<init>(r4, r1)     // Catch:{ Exception -> 0x0089 }
        L_0x0074:
            com.facebook.ads.internal.p.a.r r6 = r3.c
            boolean r6 = r6.a()
            if (r6 == 0) goto L_0x0081
            com.facebook.ads.internal.p.a.r r6 = r3.c
            r6.a((com.facebook.ads.internal.p.a.n) r5)
        L_0x0081:
            if (r4 == 0) goto L_0x0086
            r4.disconnect()
        L_0x0086:
            return r5
        L_0x0087:
            r5 = move-exception
            goto L_0x00c4
        L_0x0089:
            r5 = move-exception
            goto L_0x0090
        L_0x008b:
            r5 = move-exception
            r4 = r1
            goto L_0x00c4
        L_0x008e:
            r5 = move-exception
            r4 = r1
        L_0x0090:
            com.facebook.ads.internal.p.a.n r1 = r3.b((java.net.HttpURLConnection) r4)     // Catch:{ Exception -> 0x00b5 }
            if (r1 == 0) goto L_0x00af
            int r6 = r1.a()     // Catch:{ all -> 0x0087 }
            if (r6 <= 0) goto L_0x00af
            com.facebook.ads.internal.p.a.r r5 = r3.c
            boolean r5 = r5.a()
            if (r5 == 0) goto L_0x00a9
            com.facebook.ads.internal.p.a.r r5 = r3.c
            r5.a((com.facebook.ads.internal.p.a.n) r1)
        L_0x00a9:
            if (r4 == 0) goto L_0x00ae
            r4.disconnect()
        L_0x00ae:
            return r1
        L_0x00af:
            com.facebook.ads.internal.p.a.m r6 = new com.facebook.ads.internal.p.a.m     // Catch:{ all -> 0x0087 }
            r6.<init>(r5, r1)     // Catch:{ all -> 0x0087 }
            throw r6     // Catch:{ all -> 0x0087 }
        L_0x00b5:
            r5.printStackTrace()     // Catch:{ all -> 0x00be }
            com.facebook.ads.internal.p.a.m r6 = new com.facebook.ads.internal.p.a.m     // Catch:{ all -> 0x0087 }
            r6.<init>(r5, r1)     // Catch:{ all -> 0x0087 }
            throw r6     // Catch:{ all -> 0x0087 }
        L_0x00be:
            com.facebook.ads.internal.p.a.m r6 = new com.facebook.ads.internal.p.a.m     // Catch:{ all -> 0x0087 }
            r6.<init>(r5, r1)     // Catch:{ all -> 0x0087 }
            throw r6     // Catch:{ all -> 0x0087 }
        L_0x00c4:
            com.facebook.ads.internal.p.a.r r6 = r3.c
            boolean r6 = r6.a()
            if (r6 == 0) goto L_0x00d1
            com.facebook.ads.internal.p.a.r r6 = r3.c
            r6.a((com.facebook.ads.internal.p.a.n) r1)
        L_0x00d1:
            if (r4 == 0) goto L_0x00d6
            r4.disconnect()
        L_0x00d6:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.p.a.a.a(java.lang.String, com.facebook.ads.internal.p.a.j, java.lang.String, byte[]):com.facebook.ads.internal.p.a.n");
    }

    public n a(String str, p pVar) {
        return b((l) new i(str, pVar));
    }

    public n a(String str, String str2, byte[] bArr) {
        return b((l) new k(str, (p) null, str2, bArr));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0020 A[SYNTHETIC, Splitter:B:16:0x0020] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.ads.internal.p.a.n a(java.net.HttpURLConnection r4) {
        /*
            r3 = this;
            r0 = 0
            com.facebook.ads.internal.p.a.q r1 = r3.f2770a     // Catch:{ all -> 0x001c }
            java.io.InputStream r1 = r1.b(r4)     // Catch:{ all -> 0x001c }
            if (r1 == 0) goto L_0x000f
            com.facebook.ads.internal.p.a.q r0 = r3.f2770a     // Catch:{ all -> 0x001a }
            byte[] r0 = r0.a((java.io.InputStream) r1)     // Catch:{ all -> 0x001a }
        L_0x000f:
            com.facebook.ads.internal.p.a.n r2 = new com.facebook.ads.internal.p.a.n     // Catch:{ all -> 0x001a }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x001a }
            if (r1 == 0) goto L_0x0019
            r1.close()     // Catch:{ Exception -> 0x0019 }
        L_0x0019:
            return r2
        L_0x001a:
            r4 = move-exception
            goto L_0x001e
        L_0x001c:
            r4 = move-exception
            r1 = r0
        L_0x001e:
            if (r1 == 0) goto L_0x0023
            r1.close()     // Catch:{ Exception -> 0x0023 }
        L_0x0023:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.p.a.a.a(java.net.HttpURLConnection):com.facebook.ads.internal.p.a.n");
    }

    /* access modifiers changed from: protected */
    public HttpURLConnection a(String str) {
        try {
            new URL(str);
            return this.f2770a.a(str);
        } catch (MalformedURLException e2) {
            throw new IllegalArgumentException(str + " is not a valid URL", e2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(l lVar, b bVar) {
        this.b.a(this, bVar).a(lVar);
    }

    public void a(String str, p pVar, b bVar) {
        a((l) new k(str, pVar), bVar);
    }

    /* access modifiers changed from: protected */
    public void a(HttpURLConnection httpURLConnection, j jVar, String str) {
        httpURLConnection.setConnectTimeout(this.d);
        httpURLConnection.setReadTimeout(this.e);
        this.f2770a.a(httpURLConnection, jVar, str);
    }

    public void a(Set<String> set) {
        this.l = set;
    }

    /* access modifiers changed from: protected */
    public boolean a(Throwable th, long j2) {
        long currentTimeMillis = (System.currentTimeMillis() - j2) + 10;
        if (this.c.a()) {
            r rVar = this.c;
            rVar.a("ELAPSED TIME = " + currentTimeMillis + ", CT = " + this.d + ", RT = " + this.e);
        }
        return this.j ? currentTimeMillis >= ((long) this.e) : currentTimeMillis >= ((long) this.d);
    }

    public n b(l lVar) {
        try {
            return a(lVar.a(), lVar.b(), lVar.c(), lVar.d());
        } catch (m e2) {
            this.f2770a.a(e2);
            return null;
        } catch (Exception e3) {
            this.f2770a.a(new m(e3, (n) null));
            return null;
        }
    }

    public n b(String str, p pVar) {
        return b((l) new k(str, pVar));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x001e A[SYNTHETIC, Splitter:B:16:0x001e] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.facebook.ads.internal.p.a.n b(java.net.HttpURLConnection r4) {
        /*
            r3 = this;
            r0 = 0
            java.io.InputStream r1 = r4.getErrorStream()     // Catch:{ all -> 0x001a }
            if (r1 == 0) goto L_0x000d
            com.facebook.ads.internal.p.a.q r0 = r3.f2770a     // Catch:{ all -> 0x0018 }
            byte[] r0 = r0.a((java.io.InputStream) r1)     // Catch:{ all -> 0x0018 }
        L_0x000d:
            com.facebook.ads.internal.p.a.n r2 = new com.facebook.ads.internal.p.a.n     // Catch:{ all -> 0x0018 }
            r2.<init>(r4, r0)     // Catch:{ all -> 0x0018 }
            if (r1 == 0) goto L_0x0017
            r1.close()     // Catch:{ Exception -> 0x0017 }
        L_0x0017:
            return r2
        L_0x0018:
            r4 = move-exception
            goto L_0x001c
        L_0x001a:
            r4 = move-exception
            r1 = r0
        L_0x001c:
            if (r1 == 0) goto L_0x0021
            r1.close()     // Catch:{ Exception -> 0x0021 }
        L_0x0021:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.p.a.a.b(java.net.HttpURLConnection):com.facebook.ads.internal.p.a.n");
    }

    public p b() {
        return new p();
    }

    public void b(int i2) {
        if (i2 < 1 || i2 > 18) {
            throw new IllegalArgumentException("Maximum retries must be between 1 and 18");
        }
        this.h = i2;
    }

    public void b(Set<String> set) {
        this.k = set;
    }

    public void c(int i2) {
        this.d = i2;
    }
}
