package com.facebook.ads.internal.p.b.a;

import java.io.File;

public class g extends e {

    /* renamed from: a  reason: collision with root package name */
    private final long f2782a;

    public g(long j) {
        if (j > 0) {
            this.f2782a = j;
            return;
        }
        throw new IllegalArgumentException("Max size must be positive number!");
    }

    public /* bridge */ /* synthetic */ void a(File file) {
        super.a(file);
    }

    /* access modifiers changed from: protected */
    public boolean a(File file, long j, int i) {
        return j <= this.f2782a;
    }
}
