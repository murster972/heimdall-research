package com.facebook.ads.internal.p.a;

public abstract class l {

    /* renamed from: a  reason: collision with root package name */
    protected String f2775a = "";
    protected j b;
    protected String c;
    protected byte[] d;

    public l(String str, p pVar) {
        if (str != null) {
            this.f2775a = str;
        }
        if (pVar != null) {
            this.f2775a += "?" + pVar.a();
        }
    }

    public String a() {
        return this.f2775a;
    }

    public j b() {
        return this.b;
    }

    public String c() {
        return this.c;
    }

    public byte[] d() {
        return this.d;
    }
}
