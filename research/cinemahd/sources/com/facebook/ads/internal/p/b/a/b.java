package com.facebook.ads.internal.p.b.a;

import com.facebook.ads.internal.p.b.a;
import com.facebook.ads.internal.p.b.l;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;

public class b implements a {

    /* renamed from: a  reason: collision with root package name */
    public File f2779a;
    private final a b;
    private RandomAccessFile c;

    public b(File file, a aVar) {
        File file2;
        if (aVar != null) {
            try {
                this.b = aVar;
                d.a(file.getParentFile());
                boolean exists = file.exists();
                if (exists) {
                    file2 = file;
                } else {
                    File parentFile = file.getParentFile();
                    file2 = new File(parentFile, file.getName() + ".download");
                }
                this.f2779a = file2;
                this.c = new RandomAccessFile(this.f2779a, exists ? "r" : "rw");
            } catch (IOException e) {
                throw new l("Error using file " + file + " as disc cache", e);
            }
        } else {
            throw new NullPointerException();
        }
    }

    private boolean a(File file) {
        return file.getName().endsWith(".download");
    }

    public synchronized int a() {
        try {
        } catch (IOException e) {
            throw new l("Error reading length of file " + this.f2779a, e);
        }
        return (int) this.c.length();
    }

    public synchronized int a(byte[] bArr, long j, int i) {
        try {
            this.c.seek(j);
        } catch (IOException e) {
            throw new l(String.format("Error reading %d bytes with offset %d from file[%d bytes] to buffer[%d bytes]", new Object[]{Integer.valueOf(i), Long.valueOf(j), Integer.valueOf(a()), Integer.valueOf(bArr.length)}), e);
        }
        return this.c.read(bArr, 0, i);
    }

    public synchronized void a(byte[] bArr, int i) {
        try {
            if (!d()) {
                this.c.seek((long) a());
                this.c.write(bArr, 0, i);
            } else {
                throw new l("Error append cache: cache file " + this.f2779a + " is completed!");
            }
        } catch (IOException e) {
            throw new l(String.format("Error writing %d bytes to %s from buffer with size %d", new Object[]{Integer.valueOf(i), this.c, Integer.valueOf(bArr.length)}), e);
        }
    }

    public synchronized void b() {
        try {
            this.c.close();
            this.b.a(this.f2779a);
        } catch (IOException e) {
            throw new l("Error closing file " + this.f2779a, e);
        }
    }

    public synchronized void c() {
        if (!d()) {
            b();
            File file = new File(this.f2779a.getParentFile(), this.f2779a.getName().substring(0, this.f2779a.getName().length() - 9));
            if (this.f2779a.renameTo(file)) {
                this.f2779a = file;
                try {
                    this.c = new RandomAccessFile(this.f2779a, "r");
                } catch (IOException e) {
                    throw new l("Error opening " + this.f2779a + " as disc cache", e);
                }
            } else {
                throw new l("Error renaming file " + this.f2779a + " to " + file + " for completion!");
            }
        }
    }

    public synchronized boolean d() {
        return !a(this.f2779a);
    }
}
