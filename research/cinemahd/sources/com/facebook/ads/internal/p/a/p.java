package com.facebook.ads.internal.p.a;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class p implements Map<String, String> {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, String> f2778a = new HashMap();

    public p a(Map<? extends String, ? extends String> map) {
        putAll(map);
        return this;
    }

    public String a() {
        StringBuilder sb = new StringBuilder();
        for (String next : this.f2778a.keySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(next);
            String str = this.f2778a.get(next);
            if (str != null) {
                sb.append("=");
                try {
                    sb.append(URLEncoder.encode(str, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    /* renamed from: a */
    public String get(Object obj) {
        return this.f2778a.get(obj);
    }

    /* renamed from: a */
    public String put(String str, String str2) {
        return this.f2778a.put(str, str2);
    }

    /* renamed from: b */
    public String remove(Object obj) {
        return this.f2778a.remove(obj);
    }

    public byte[] b() {
        try {
            return a().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void clear() {
        this.f2778a.clear();
    }

    public boolean containsKey(Object obj) {
        return this.f2778a.containsKey(obj);
    }

    public boolean containsValue(Object obj) {
        return this.f2778a.containsValue(obj);
    }

    public Set<Map.Entry<String, String>> entrySet() {
        return this.f2778a.entrySet();
    }

    public boolean isEmpty() {
        return this.f2778a.isEmpty();
    }

    public Set<String> keySet() {
        return this.f2778a.keySet();
    }

    public void putAll(Map<? extends String, ? extends String> map) {
        this.f2778a.putAll(map);
    }

    public int size() {
        return this.f2778a.size();
    }

    public Collection<String> values() {
        return this.f2778a.values();
    }
}
