package com.facebook.ads.internal;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import com.facebook.ads.AdError;
import com.facebook.ads.AdSettings;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.ads.CacheFlag;
import com.facebook.ads.NativeAdBase;
import com.facebook.ads.RewardData;
import com.facebook.ads.internal.adapters.AdAdapter;
import com.facebook.ads.internal.adapters.BannerAdapter;
import com.facebook.ads.internal.adapters.BannerAdapterListener;
import com.facebook.ads.internal.adapters.InterstitialAdapter;
import com.facebook.ads.internal.adapters.InterstitialAdapterListener;
import com.facebook.ads.internal.adapters.ab;
import com.facebook.ads.internal.adapters.ac;
import com.facebook.ads.internal.adapters.s;
import com.facebook.ads.internal.adapters.u;
import com.facebook.ads.internal.adapters.v;
import com.facebook.ads.internal.adapters.z;
import com.facebook.ads.internal.o.c;
import com.facebook.ads.internal.o.g;
import com.facebook.ads.internal.protocol.AdErrorType;
import com.facebook.ads.internal.protocol.AdPlacementType;
import com.facebook.ads.internal.protocol.d;
import com.facebook.ads.internal.protocol.e;
import com.facebook.ads.internal.protocol.f;
import com.facebook.ads.internal.protocol.h;
import com.facebook.ads.internal.q.a.l;
import com.facebook.ads.internal.q.a.o;
import com.facebook.ads.internal.q.a.y;
import com.facebook.common.util.UriUtil;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DisplayAdController implements c.a {
    private static final String b = DisplayAdController.class.getSimpleName();
    private static final Handler h = new Handler(Looper.getMainLooper());
    private static boolean i = false;
    private boolean A;
    private final com.facebook.ads.internal.m.c B;
    private final EnumSet<CacheFlag> C;

    /* renamed from: a  reason: collision with root package name */
    protected com.facebook.ads.internal.adapters.a f2588a;
    /* access modifiers changed from: private */
    public final Context c;
    private final String d;
    private final AdPlacementType e;
    private final com.facebook.ads.internal.o.c f;
    /* access modifiers changed from: private */
    public final Handler g;
    private final Runnable j;
    private final Runnable k;
    /* access modifiers changed from: private */
    public volatile boolean l;
    private boolean m;
    /* access modifiers changed from: private */
    public volatile boolean n;
    /* access modifiers changed from: private */
    public AdAdapter o;
    /* access modifiers changed from: private */
    public AdAdapter p;
    /* access modifiers changed from: private */
    public View q;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.h.c r;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.o.b s;
    private f t;
    private d u;
    private e v;
    private int w;
    private boolean x;
    private int y;
    private final c z;

    /* renamed from: com.facebook.ads.internal.DisplayAdController$4  reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2595a = new int[AdPlacementType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.facebook.ads.internal.protocol.AdPlacementType[] r0 = com.facebook.ads.internal.protocol.AdPlacementType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2595a = r0
                int[] r0 = f2595a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.INTERSTITIAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2595a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.BANNER     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2595a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.NATIVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2595a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.NATIVE_BANNER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f2595a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.INSTREAM     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f2595a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.facebook.ads.internal.protocol.AdPlacementType r1 = com.facebook.ads.internal.protocol.AdPlacementType.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.DisplayAdController.AnonymousClass4.<clinit>():void");
        }
    }

    private static final class a extends y<DisplayAdController> {
        public a(DisplayAdController displayAdController) {
            super(displayAdController);
        }

        public void run() {
            DisplayAdController displayAdController = (DisplayAdController) a();
            if (displayAdController != null) {
                boolean unused = displayAdController.l = false;
                displayAdController.b((String) null);
            }
        }
    }

    private static final class b extends y<DisplayAdController> {
        public b(DisplayAdController displayAdController) {
            super(displayAdController);
        }

        public void run() {
            DisplayAdController displayAdController = (DisplayAdController) a();
            if (displayAdController != null) {
                displayAdController.l();
            }
        }
    }

    private class c extends BroadcastReceiver {
        private c() {
        }

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.intent.action.SCREEN_OFF".equals(action)) {
                DisplayAdController.this.m();
            } else if ("android.intent.action.SCREEN_ON".equals(action)) {
                DisplayAdController.this.l();
            }
        }
    }

    static {
        com.facebook.ads.internal.q.a.d.a();
    }

    public DisplayAdController(Context context, String str, f fVar, AdPlacementType adPlacementType, e eVar, d dVar, int i2, boolean z2) {
        this(context, str, fVar, adPlacementType, eVar, dVar, i2, z2, EnumSet.of(CacheFlag.NONE));
    }

    public DisplayAdController(Context context, String str, f fVar, AdPlacementType adPlacementType, e eVar, d dVar, int i2, boolean z2, EnumSet<CacheFlag> enumSet) {
        this.g = new Handler();
        this.x = false;
        this.y = -1;
        this.c = context.getApplicationContext();
        this.d = str;
        this.t = fVar;
        this.e = adPlacementType;
        this.v = eVar;
        this.u = dVar;
        this.w = i2;
        this.z = new c();
        this.C = enumSet;
        this.f = new com.facebook.ads.internal.o.c(this.c);
        this.f.a((c.a) this);
        this.j = new a(this);
        this.k = new b(this);
        this.m = z2;
        g();
        try {
            CookieManager.getInstance();
            if (Build.VERSION.SDK_INT < 21) {
                CookieSyncManager.createInstance(this.c);
            }
        } catch (Exception e2) {
            Log.w(b, "Failed to initialize CookieManager.", e2);
        }
        com.facebook.ads.internal.i.a.a(this.c).a();
        this.B = com.facebook.ads.internal.m.d.a(this.c);
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(long j2) {
        HashMap hashMap = new HashMap();
        hashMap.put(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_DELAY, String.valueOf(System.currentTimeMillis() - j2));
        return hashMap;
    }

    /* access modifiers changed from: private */
    public void a(AdAdapter adAdapter) {
        if (adAdapter != null) {
            adAdapter.onDestroy();
        }
    }

    private void a(final BannerAdapter bannerAdapter, com.facebook.ads.internal.h.c cVar, Map<String, Object> map) {
        final AnonymousClass8 r0 = new Runnable() {
            public void run() {
                DisplayAdController.this.a((AdAdapter) bannerAdapter);
                DisplayAdController.this.j();
            }
        };
        this.g.postDelayed(r0, (long) cVar.a().j());
        bannerAdapter.loadBannerAd(this.c, this.B, this.v, new BannerAdapterListener() {
            public void onBannerAdClicked(BannerAdapter bannerAdapter) {
                DisplayAdController.this.f2588a.a();
            }

            public void onBannerAdExpanded(BannerAdapter bannerAdapter) {
            }

            public void onBannerAdLoaded(BannerAdapter bannerAdapter, View view) {
                if (bannerAdapter == DisplayAdController.this.o) {
                    DisplayAdController.this.g.removeCallbacks(r0);
                    AdAdapter f = DisplayAdController.this.p;
                    AdAdapter unused = DisplayAdController.this.p = bannerAdapter;
                    View unused2 = DisplayAdController.this.q = view;
                    if (!DisplayAdController.this.n) {
                        DisplayAdController.this.f2588a.a((AdAdapter) bannerAdapter);
                        return;
                    }
                    DisplayAdController.this.f2588a.a(view);
                    DisplayAdController.this.a(f);
                }
            }

            public void onBannerAdMinimized(BannerAdapter bannerAdapter) {
            }

            public void onBannerError(BannerAdapter bannerAdapter, AdError adError) {
                if (bannerAdapter == DisplayAdController.this.o) {
                    DisplayAdController.this.g.removeCallbacks(r0);
                    DisplayAdController.this.a((AdAdapter) bannerAdapter);
                    DisplayAdController.this.j();
                }
            }

            public void onBannerLoggingImpression(BannerAdapter bannerAdapter) {
                DisplayAdController.this.f2588a.b();
            }
        }, map);
    }

    private void a(final InterstitialAdapter interstitialAdapter, com.facebook.ads.internal.h.c cVar, Map<String, Object> map) {
        final AnonymousClass10 r0 = new Runnable() {
            public void run() {
                DisplayAdController.this.a((AdAdapter) interstitialAdapter);
                DisplayAdController.this.j();
            }
        };
        this.g.postDelayed(r0, (long) cVar.a().j());
        interstitialAdapter.loadInterstitialAd(this.c, new InterstitialAdapterListener() {
            public void onInterstitialActivityDestroyed() {
                DisplayAdController.this.f2588a.f();
            }

            public void onInterstitialAdClicked(InterstitialAdapter interstitialAdapter, String str, boolean z) {
                DisplayAdController.this.f2588a.a();
                boolean z2 = !TextUtils.isEmpty(str);
                if (z && z2) {
                    Intent intent = new Intent("android.intent.action.VIEW");
                    if (!(DisplayAdController.this.s.b instanceof Activity)) {
                        intent.addFlags(268435456);
                    }
                    intent.setData(Uri.parse(str));
                    DisplayAdController.this.s.b.startActivity(intent);
                }
            }

            public void onInterstitialAdDismissed(InterstitialAdapter interstitialAdapter) {
                DisplayAdController.this.f2588a.e();
            }

            public void onInterstitialAdDisplayed(InterstitialAdapter interstitialAdapter) {
                DisplayAdController.this.f2588a.d();
            }

            public void onInterstitialAdLoaded(InterstitialAdapter interstitialAdapter) {
                if (interstitialAdapter == DisplayAdController.this.o) {
                    DisplayAdController displayAdController = DisplayAdController.this;
                    if (interstitialAdapter == null) {
                        com.facebook.ads.internal.q.d.a.a(displayAdController.c, "api", com.facebook.ads.internal.q.d.b.b, (Exception) new com.facebook.ads.internal.protocol.b(AdErrorType.NO_ADAPTER_ON_LOAD, "Adapter is null on loadInterstitialAd"));
                        onInterstitialError(interstitialAdapter, AdError.INTERNAL_ERROR);
                        return;
                    }
                    displayAdController.g.removeCallbacks(r0);
                    AdAdapter unused = DisplayAdController.this.p = interstitialAdapter;
                    DisplayAdController.this.f2588a.a((AdAdapter) interstitialAdapter);
                    DisplayAdController.this.l();
                }
            }

            public void onInterstitialError(InterstitialAdapter interstitialAdapter, AdError adError) {
                if (interstitialAdapter == DisplayAdController.this.o) {
                    DisplayAdController.this.g.removeCallbacks(r0);
                    DisplayAdController.this.a((AdAdapter) interstitialAdapter);
                    DisplayAdController.this.j();
                    DisplayAdController.this.f2588a.a(new com.facebook.ads.internal.protocol.a(adError.getErrorCode(), adError.getErrorMessage()));
                }
            }

            public void onInterstitialLoggingImpression(InterstitialAdapter interstitialAdapter) {
                DisplayAdController.this.f2588a.b();
            }
        }, map, this.B, this.C);
    }

    private void a(ab abVar, com.facebook.ads.internal.h.c cVar, Map<String, Object> map) {
        abVar.a(this.c, new ac() {
            public void a() {
                DisplayAdController.this.f2588a.h();
            }

            public void a(ab abVar) {
                AdAdapter unused = DisplayAdController.this.p = abVar;
                DisplayAdController.this.f2588a.a((AdAdapter) abVar);
            }

            public void a(ab abVar, AdError adError) {
                DisplayAdController.this.f2588a.a(new com.facebook.ads.internal.protocol.a(AdErrorType.INTERNAL_ERROR, (String) null));
                DisplayAdController.this.a((AdAdapter) abVar);
                DisplayAdController.this.j();
            }

            public void b() {
                DisplayAdController.this.f2588a.k();
            }

            public void b(ab abVar) {
                DisplayAdController.this.f2588a.a();
            }

            public void c(ab abVar) {
                DisplayAdController.this.f2588a.b();
            }

            public void d(ab abVar) {
                DisplayAdController.this.f2588a.g();
            }

            public void e(ab abVar) {
                DisplayAdController.this.f2588a.i();
            }

            public void f(ab abVar) {
                DisplayAdController.this.f2588a.j();
            }
        }, map, this.x);
    }

    private void a(s sVar, com.facebook.ads.internal.h.c cVar, Map<String, Object> map) {
        sVar.a(this.c, new com.facebook.ads.a.a() {
            public void a(s sVar) {
                AdAdapter unused = DisplayAdController.this.p = sVar;
                boolean unused2 = DisplayAdController.this.n = false;
                DisplayAdController.this.f2588a.a((AdAdapter) sVar);
            }

            public void a(s sVar, View view) {
                DisplayAdController.this.f2588a.a(view);
            }

            public void a(s sVar, AdError adError) {
                DisplayAdController.this.f2588a.a(new com.facebook.ads.internal.protocol.a(adError.getErrorCode(), adError.getErrorMessage()));
            }

            public void b(s sVar) {
                DisplayAdController.this.f2588a.a();
            }

            public void c(s sVar) {
                DisplayAdController.this.f2588a.b();
            }

            public void d(s sVar) {
                DisplayAdController.this.f2588a.c();
            }
        }, map, this.B, this.C);
    }

    private void a(com.facebook.ads.internal.adapters.y yVar, com.facebook.ads.internal.h.c cVar, com.facebook.ads.internal.h.a aVar, Map<String, Object> map) {
        final com.facebook.ads.internal.adapters.y yVar2 = yVar;
        final long currentTimeMillis = System.currentTimeMillis();
        final com.facebook.ads.internal.h.a aVar2 = aVar;
        AnonymousClass12 r0 = new Runnable() {
            public void run() {
                DisplayAdController.this.a((AdAdapter) yVar2);
                if (yVar2 instanceof u) {
                    Context h = DisplayAdController.this.c;
                    com.facebook.ads.internal.q.a.d.a(h, v.a(((u) yVar2).J()) + " Failed. Ad request timed out");
                }
                Map a2 = DisplayAdController.this.a(currentTimeMillis);
                a2.put("error", "-1");
                a2.put("msg", "timeout");
                DisplayAdController.this.a(aVar2.a(com.facebook.ads.internal.h.e.REQUEST), (Map<String, String>) a2);
                DisplayAdController.this.j();
            }
        };
        this.g.postDelayed(r0, (long) cVar.a().j());
        final AnonymousClass12 r2 = r0;
        yVar.a(this.c, new z() {

            /* renamed from: a  reason: collision with root package name */
            boolean f2593a = false;
            boolean b = false;
            boolean c = false;

            public void a(com.facebook.ads.internal.adapters.y yVar) {
                if (yVar == DisplayAdController.this.o) {
                    DisplayAdController.this.g.removeCallbacks(r2);
                    AdAdapter unused = DisplayAdController.this.p = yVar;
                    DisplayAdController.this.f2588a.a((AdAdapter) yVar);
                    if (!this.f2593a) {
                        this.f2593a = true;
                        DisplayAdController.this.a(aVar2.a(com.facebook.ads.internal.h.e.REQUEST), (Map<String, String>) DisplayAdController.this.a(currentTimeMillis));
                    }
                }
            }

            public void a(com.facebook.ads.internal.adapters.y yVar, com.facebook.ads.internal.protocol.a aVar) {
                if (yVar == DisplayAdController.this.o) {
                    DisplayAdController.this.g.removeCallbacks(r2);
                    DisplayAdController.this.a((AdAdapter) yVar);
                    if (!this.f2593a) {
                        this.f2593a = true;
                        Map a2 = DisplayAdController.this.a(currentTimeMillis);
                        a2.put("error", String.valueOf(aVar.a().getErrorCode()));
                        a2.put("msg", String.valueOf(aVar.b()));
                        DisplayAdController.this.a(aVar2.a(com.facebook.ads.internal.h.e.REQUEST), (Map<String, String>) a2);
                    }
                    DisplayAdController.this.j();
                }
            }

            public void b(com.facebook.ads.internal.adapters.y yVar) {
                if (!this.b) {
                    this.b = true;
                    DisplayAdController.this.a(aVar2.a(com.facebook.ads.internal.h.e.IMPRESSION), (Map<String, String>) null);
                }
            }

            public void c(com.facebook.ads.internal.adapters.y yVar) {
                if (!this.c) {
                    this.c = true;
                    DisplayAdController.this.a(aVar2.a(com.facebook.ads.internal.h.e.CLICK), (Map<String, String>) null);
                }
                com.facebook.ads.internal.adapters.a aVar = DisplayAdController.this.f2588a;
                if (aVar != null) {
                    aVar.a();
                }
            }
        }, this.B, map, NativeAdBase.getViewTraversalPredicate());
    }

    /* access modifiers changed from: private */
    public void a(List<String> list, Map<String, String> map) {
        if (list != null && !list.isEmpty()) {
            for (String str : list) {
                new com.facebook.ads.internal.q.c.e(this.c, map).execute(new String[]{str});
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        try {
            h hVar = new h(this.c, str, this.d, this.t);
            Context context = this.c;
            com.facebook.ads.internal.i.c cVar = new com.facebook.ads.internal.i.c(context, false);
            String str2 = this.d;
            e eVar = this.v;
            this.s = new com.facebook.ads.internal.o.b(context, cVar, str2, eVar != null ? new l(eVar.b(), this.v.a()) : null, this.t, this.u, AdSettings.getTestAdType() != AdSettings.TestAdType.DEFAULT ? AdSettings.getTestAdType().getAdTypeString() : null, com.facebook.ads.internal.adapters.e.a(com.facebook.ads.internal.protocol.c.a(this.t).a()), this.w, AdSettings.isTestMode(this.c), AdSettings.isChildDirected(), hVar, o.a(com.facebook.ads.internal.l.a.q(this.c)));
            this.f.a(this.s);
        } catch (com.facebook.ads.internal.protocol.b e2) {
            a(com.facebook.ads.internal.protocol.a.a(e2));
        }
    }

    private void g() {
        if (!this.m) {
            IntentFilter intentFilter = new IntentFilter("android.intent.action.SCREEN_ON");
            intentFilter.addAction("android.intent.action.SCREEN_OFF");
            this.c.registerReceiver(this.z, intentFilter);
            this.A = true;
        }
    }

    private void h() {
        if (this.A) {
            try {
                this.c.unregisterReceiver(this.z);
                this.A = false;
            } catch (Exception e2) {
                com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a(e2, "Error unregistering screen state receiever"));
            }
        }
    }

    private AdPlacementType i() {
        AdPlacementType adPlacementType = this.e;
        if (adPlacementType != null) {
            return adPlacementType;
        }
        e eVar = this.v;
        return eVar == null ? AdPlacementType.NATIVE : eVar == e.INTERSTITIAL ? AdPlacementType.INTERSTITIAL : AdPlacementType.BANNER;
    }

    /* access modifiers changed from: private */
    public synchronized void j() {
        h.post(new Runnable() {
            public void run() {
                try {
                    DisplayAdController.this.k();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void k() {
        this.o = null;
        com.facebook.ads.internal.h.c cVar = this.r;
        com.facebook.ads.internal.h.a d2 = cVar.d();
        if (d2 == null) {
            this.f2588a.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.NO_FILL, ""));
            l();
            return;
        }
        String a2 = d2.a();
        AdAdapter a3 = com.facebook.ads.internal.adapters.e.a(a2, cVar.a().b());
        if (a3 == null) {
            String str = b;
            Log.e(str, "Adapter does not exist: " + a2);
            j();
        } else if (i() != a3.getPlacementType()) {
            this.f2588a.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.INTERNAL_ERROR, ""));
        } else {
            this.o = a3;
            HashMap hashMap = new HashMap();
            com.facebook.ads.internal.h.d a4 = cVar.a();
            hashMap.put(UriUtil.DATA_SCHEME, d2.b());
            hashMap.put("definition", a4);
            hashMap.put("placementId", this.d);
            hashMap.put(AudienceNetworkActivity.REQUEST_TIME, Long.valueOf(a4.a()));
            if (this.s == null) {
                this.f2588a.a(com.facebook.ads.internal.protocol.a.a(AdErrorType.UNKNOWN_ERROR, "environment is empty"));
                return;
            }
            switch (AnonymousClass4.f2595a[a3.getPlacementType().ordinal()]) {
                case 1:
                    a((InterstitialAdapter) a3, cVar, (Map<String, Object>) hashMap);
                    return;
                case 2:
                    a((BannerAdapter) a3, cVar, (Map<String, Object>) hashMap);
                    return;
                case 3:
                case 4:
                    a((com.facebook.ads.internal.adapters.y) a3, cVar, d2, hashMap);
                    return;
                case 5:
                    a((s) a3, cVar, (Map<String, Object>) hashMap);
                    return;
                case 6:
                    a((ab) a3, cVar, (Map<String, Object>) hashMap);
                    return;
                default:
                    Log.e(b, "attempt unexpected adapter type");
                    return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void l() {
        if (!this.m && !this.l && AnonymousClass4.f2595a[i().ordinal()] == 1) {
            if (!com.facebook.ads.internal.q.e.a.a(this.c)) {
                this.g.postDelayed(this.k, 1000);
            }
            com.facebook.ads.internal.h.c cVar = this.r;
            long c2 = cVar == null ? 30000 : cVar.a().c();
            if (c2 > 0) {
                this.g.postDelayed(this.j, c2);
                this.l = true;
            }
        }
    }

    /* access modifiers changed from: private */
    public void m() {
        if (this.l) {
            this.g.removeCallbacks(this.j);
            this.l = false;
        }
    }

    private Handler n() {
        return !o() ? this.g : h;
    }

    private static synchronized boolean o() {
        boolean z2;
        synchronized (DisplayAdController.class) {
            z2 = i;
        }
        return z2;
    }

    protected static synchronized void setMainThreadForced(boolean z2) {
        synchronized (DisplayAdController.class) {
            String str = b;
            Log.d(str, "DisplayAdController changed main thread forced from " + i + " to " + z2);
            i = z2;
        }
    }

    public com.facebook.ads.internal.h.d a() {
        com.facebook.ads.internal.h.c cVar = this.r;
        if (cVar == null) {
            return null;
        }
        return cVar.a();
    }

    public void a(int i2) {
        this.y = i2;
    }

    public void a(RewardData rewardData) {
        AdAdapter adAdapter = this.p;
        if (adAdapter == null) {
            throw new IllegalStateException("no adapter ready to set reward on");
        } else if (adAdapter.getPlacementType() == AdPlacementType.REWARDED_VIDEO) {
            ((ab) this.p).a(rewardData);
        } else {
            throw new IllegalStateException("can only set on rewarded video ads");
        }
    }

    public void a(com.facebook.ads.internal.adapters.a aVar) {
        this.f2588a = aVar;
    }

    public synchronized void a(final g gVar) {
        n().post(new Runnable() {
            public void run() {
                com.facebook.ads.internal.h.c a2 = gVar.a();
                if (a2 == null || a2.a() == null) {
                    throw new IllegalStateException("invalid placement in response");
                }
                com.facebook.ads.internal.h.c unused = DisplayAdController.this.r = a2;
                DisplayAdController.this.j();
            }
        });
    }

    public synchronized void a(final com.facebook.ads.internal.protocol.a aVar) {
        n().post(new Runnable() {
            public void run() {
                DisplayAdController.this.f2588a.a(aVar);
            }
        });
    }

    public void a(String str) {
        b(str);
    }

    public void a(boolean z2) {
        this.x = z2;
    }

    public void b() {
        com.facebook.ads.internal.adapters.a aVar;
        AdErrorType adErrorType;
        if (this.p == null) {
            com.facebook.ads.internal.q.d.a.a(this.c, "api", com.facebook.ads.internal.q.d.b.e, (Exception) new com.facebook.ads.internal.protocol.b(AdErrorType.NO_ADAPTER_ON_START, "Adapter is null on startAd"));
            aVar = this.f2588a;
            adErrorType = AdErrorType.INTERNAL_ERROR;
        } else if (this.n) {
            com.facebook.ads.internal.q.d.a.a(this.c, "api", com.facebook.ads.internal.q.d.b.c, (Exception) new com.facebook.ads.internal.protocol.b(AdErrorType.AD_ALREADY_STARTED, "ad already started"));
            aVar = this.f2588a;
            adErrorType = AdErrorType.AD_ALREADY_STARTED;
        } else {
            this.n = true;
            switch (AnonymousClass4.f2595a[this.p.getPlacementType().ordinal()]) {
                case 1:
                    ((InterstitialAdapter) this.p).show();
                    return;
                case 2:
                    View view = this.q;
                    if (view != null) {
                        this.f2588a.a(view);
                        return;
                    }
                    return;
                case 3:
                case 4:
                    com.facebook.ads.internal.adapters.y yVar = (com.facebook.ads.internal.adapters.y) this.p;
                    if (yVar.c_()) {
                        this.f2588a.a(yVar);
                        return;
                    }
                    throw new IllegalStateException("ad is not ready or already displayed");
                case 5:
                    ((s) this.p).e();
                    return;
                case 6:
                    ab abVar = (ab) this.p;
                    abVar.a(this.y);
                    abVar.b();
                    return;
                default:
                    Log.e(b, "start unexpected adapter type");
                    return;
            }
        }
        aVar.a(com.facebook.ads.internal.protocol.a.a(adErrorType, adErrorType.getDefaultErrorMessage()));
    }

    public void b(boolean z2) {
        h();
        if (z2 || this.n) {
            m();
            a(this.p);
            this.f.a();
            this.q = null;
            this.n = false;
        }
    }

    public void c() {
        b(false);
    }

    public boolean d() {
        com.facebook.ads.internal.h.c cVar = this.r;
        return cVar == null || cVar.e();
    }

    public com.facebook.ads.internal.m.c e() {
        return this.B;
    }

    public AdAdapter f() {
        return this.p;
    }
}
