package com.facebook.ads.internal.m;

import com.unity3d.services.purchasing.core.TransactionErrorDetailsUtilities;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.ui.JavascriptBridge;

public enum f {
    TEST("test"),
    BROWSER_SESSION("browser_session"),
    CLOSE(JavascriptBridge.MraidHandler.CLOSE_ACTION),
    IMPRESSION("impression"),
    INVALIDATION("invalidation"),
    STORE(TransactionErrorDetailsUtilities.STORE),
    OFF_TARGET_CLICK("off_target_click"),
    OPEN_LINK("open_link"),
    NATIVE_VIEW("native_view"),
    VIDEO(Advertisement.KEY_VIDEO),
    USER_RETURN("user_return");
    
    private String l;

    private f(String str) {
        this.l = str;
    }

    public static f a(String str) {
        for (f fVar : values()) {
            if (fVar.l.equalsIgnoreCase(str)) {
                return fVar;
            }
        }
        return null;
    }

    public String toString() {
        return this.l;
    }
}
