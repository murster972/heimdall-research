package com.facebook.ads.internal.m;

import android.content.Context;
import android.database.Cursor;
import com.facebook.ads.internal.e.c;
import com.facebook.ads.internal.e.d;
import com.facebook.ads.internal.f.e;
import com.facebook.ads.internal.l.a;
import com.facebook.ads.internal.m.b;
import com.facebook.ads.internal.q.a.t;
import com.facebook.common.util.UriUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class g implements b.a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f2736a = "g";
    private Context b;
    private d c;

    public g(Context context, d dVar) {
        this.b = context;
        this.c = dVar;
    }

    private static JSONArray a(JSONArray jSONArray, JSONArray jSONArray2) {
        int i = 0;
        if (jSONArray != null) {
            i = 0 + jSONArray.length();
        }
        if (jSONArray2 != null) {
            i += jSONArray2.length();
        }
        return a(jSONArray, jSONArray2, i);
    }

    private static JSONArray a(JSONArray jSONArray, JSONArray jSONArray2, int i) {
        JSONObject jSONObject;
        JSONObject jSONObject2;
        JSONArray jSONArray3 = jSONArray;
        JSONArray jSONArray4 = jSONArray2;
        if (jSONArray3 == null) {
            return jSONArray4;
        }
        if (jSONArray4 == null) {
            return jSONArray3;
        }
        int length = jSONArray.length();
        int length2 = jSONArray2.length();
        JSONArray jSONArray5 = new JSONArray();
        int i2 = 0;
        int i3 = i;
        JSONObject jSONObject3 = null;
        JSONObject jSONObject4 = null;
        int i4 = 0;
        double d = Double.MAX_VALUE;
        double d2 = Double.MAX_VALUE;
        while (true) {
            if ((i2 < length || i4 < length2) && i3 > 0) {
                if (i2 < length && jSONObject3 == null) {
                    try {
                        jSONObject2 = jSONArray3.getJSONObject(i2);
                        d = jSONObject2.getDouble("time");
                    } catch (JSONException unused) {
                        jSONObject2 = null;
                        d = Double.MAX_VALUE;
                    }
                    i2++;
                    jSONObject3 = jSONObject2;
                }
                if (i4 < length2 && jSONObject4 == null) {
                    try {
                        jSONObject = jSONArray4.getJSONObject(i4);
                        d2 = jSONObject.getDouble("time");
                    } catch (JSONException unused2) {
                        jSONObject = null;
                        d2 = Double.MAX_VALUE;
                    }
                    i4++;
                    jSONObject4 = jSONObject;
                }
                if (jSONObject3 != null || jSONObject4 != null) {
                    if (jSONObject3 == null || d2 < d) {
                        jSONArray5.put(jSONObject4);
                        jSONObject4 = null;
                        d2 = Double.MAX_VALUE;
                    } else {
                        jSONArray5.put(jSONObject3);
                        jSONObject3 = null;
                        d = Double.MAX_VALUE;
                    }
                    i3--;
                }
            }
        }
        if (i3 > 0) {
            if (jSONObject3 != null) {
                jSONArray5.put(jSONObject3);
            } else if (jSONObject4 != null) {
                jSONArray5.put(jSONObject4);
            }
        }
        return jSONArray5;
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0066  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x0078  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject a(int r8) {
        /*
            r7 = this;
            r0 = 0
            com.facebook.ads.internal.e.d r1 = r7.c     // Catch:{ JSONException -> 0x006f, all -> 0x0061 }
            android.database.Cursor r1 = r1.d()     // Catch:{ JSONException -> 0x006f, all -> 0x0061 }
            com.facebook.ads.internal.e.d r2 = r7.c     // Catch:{ JSONException -> 0x005f, all -> 0x005c }
            android.database.Cursor r2 = r2.a((int) r8)     // Catch:{ JSONException -> 0x005f, all -> 0x005c }
            int r3 = r2.getCount()     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            if (r3 <= 0) goto L_0x001c
            org.json.JSONObject r3 = r7.a((android.database.Cursor) r2)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            org.json.JSONArray r4 = r7.c(r2)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            goto L_0x001e
        L_0x001c:
            r3 = r0
            r4 = r3
        L_0x001e:
            android.content.Context r5 = r7.b     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            boolean r5 = com.facebook.ads.internal.l.a.h(r5)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            if (r5 == 0) goto L_0x0038
            android.content.Context r5 = r7.b     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            org.json.JSONArray r5 = com.facebook.ads.internal.f.e.a((android.content.Context) r5, (int) r8)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            if (r5 == 0) goto L_0x0038
            int r6 = r5.length()     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            if (r6 <= 0) goto L_0x0038
            org.json.JSONArray r4 = a(r5, r4, r8)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
        L_0x0038:
            if (r4 == 0) goto L_0x004c
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            r8.<init>()     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            if (r3 == 0) goto L_0x0046
            java.lang.String r5 = "tokens"
            r8.put(r5, r3)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
        L_0x0046:
            java.lang.String r3 = "events"
            r8.put(r3, r4)     // Catch:{ JSONException -> 0x005a, all -> 0x0058 }
            goto L_0x004d
        L_0x004c:
            r8 = r0
        L_0x004d:
            if (r1 == 0) goto L_0x0052
            r1.close()
        L_0x0052:
            if (r2 == 0) goto L_0x0057
            r2.close()
        L_0x0057:
            return r8
        L_0x0058:
            r8 = move-exception
            goto L_0x0064
        L_0x005a:
            goto L_0x0071
        L_0x005c:
            r8 = move-exception
            r2 = r0
            goto L_0x0064
        L_0x005f:
            r2 = r0
            goto L_0x0071
        L_0x0061:
            r8 = move-exception
            r1 = r0
            r2 = r1
        L_0x0064:
            if (r1 == 0) goto L_0x0069
            r1.close()
        L_0x0069:
            if (r2 == 0) goto L_0x006e
            r2.close()
        L_0x006e:
            throw r8
        L_0x006f:
            r1 = r0
            r2 = r1
        L_0x0071:
            if (r1 == 0) goto L_0x0076
            r1.close()
        L_0x0076:
            if (r2 == 0) goto L_0x007b
            r2.close()
        L_0x007b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.m.g.a(int):org.json.JSONObject");
    }

    private JSONObject a(Cursor cursor) {
        JSONObject jSONObject = new JSONObject();
        while (cursor.moveToNext()) {
            jSONObject.put(cursor.getString(0), cursor.getString(1));
        }
        return jSONObject;
    }

    private void a(String str) {
        if (e.c(str)) {
            e.a(str);
        } else {
            this.c.a(str);
        }
    }

    private JSONArray b(Cursor cursor) {
        JSONObject jSONObject;
        JSONArray jSONArray = new JSONArray();
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("id", cursor.getString(c.f2695a.f2694a));
            jSONObject2.put("token_id", cursor.getString(c.b.f2694a));
            jSONObject2.put("type", cursor.getString(c.d.f2694a));
            jSONObject2.put("time", t.a(cursor.getDouble(c.e.f2694a)));
            jSONObject2.put("session_time", t.a(cursor.getDouble(c.f.f2694a)));
            jSONObject2.put("session_id", cursor.getString(c.g.f2694a));
            String string = cursor.getString(c.h.f2694a);
            if (string == null) {
                jSONObject = new JSONObject();
            }
            jSONObject2.put(UriUtil.DATA_SCHEME, jSONObject);
            jSONObject2.put("attempt", cursor.getString(c.i.f2694a));
            jSONArray.put(jSONObject2);
        }
        return jSONArray;
    }

    private JSONArray c(Cursor cursor) {
        JSONObject jSONObject;
        JSONArray jSONArray = new JSONArray();
        cursor.moveToPosition(-1);
        while (cursor.moveToNext()) {
            JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("id", cursor.getString(2));
            jSONObject2.put("token_id", cursor.getString(0));
            jSONObject2.put("type", cursor.getString(4));
            jSONObject2.put("time", t.a(cursor.getDouble(5)));
            jSONObject2.put("session_time", t.a(cursor.getDouble(6)));
            jSONObject2.put("session_id", cursor.getString(7));
            String string = cursor.getString(8);
            if (string == null) {
                jSONObject = new JSONObject();
            }
            jSONObject2.put(UriUtil.DATA_SCHEME, jSONObject);
            jSONObject2.put("attempt", cursor.getString(9));
            jSONArray.put(jSONObject2);
        }
        return jSONArray;
    }

    /* JADX WARNING: Removed duplicated region for block: B:39:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x0073  */
    /* JADX WARNING: Removed duplicated region for block: B:46:0x007b  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0080  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject d() {
        /*
            r8 = this;
            r0 = 0
            com.facebook.ads.internal.e.d r1 = r8.c     // Catch:{ JSONException -> 0x0077, all -> 0x0068 }
            android.database.Cursor r1 = r1.f()     // Catch:{ JSONException -> 0x0077, all -> 0x0068 }
            com.facebook.ads.internal.e.d r2 = r8.c     // Catch:{ JSONException -> 0x0066, all -> 0x0061 }
            android.database.Cursor r2 = r2.e()     // Catch:{ JSONException -> 0x0066, all -> 0x0061 }
            int r3 = r1.getCount()     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            if (r3 <= 0) goto L_0x0022
            int r3 = r2.getCount()     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            if (r3 <= 0) goto L_0x0022
            org.json.JSONObject r3 = r8.a((android.database.Cursor) r1)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            org.json.JSONArray r4 = r8.b((android.database.Cursor) r2)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            goto L_0x0024
        L_0x0022:
            r3 = r0
            r4 = r3
        L_0x0024:
            android.content.Context r5 = r8.b     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            boolean r5 = com.facebook.ads.internal.l.a.h(r5)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            if (r5 == 0) goto L_0x003e
            android.content.Context r5 = r8.b     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            org.json.JSONArray r5 = com.facebook.ads.internal.f.e.a((android.content.Context) r5)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            if (r5 == 0) goto L_0x003e
            int r6 = r5.length()     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            if (r6 <= 0) goto L_0x003e
            org.json.JSONArray r4 = a(r5, r4)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
        L_0x003e:
            if (r4 == 0) goto L_0x0052
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            r5.<init>()     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            if (r3 == 0) goto L_0x004c
            java.lang.String r6 = "tokens"
            r5.put(r6, r3)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
        L_0x004c:
            java.lang.String r3 = "events"
            r5.put(r3, r4)     // Catch:{ JSONException -> 0x005f, all -> 0x005d }
            r0 = r5
        L_0x0052:
            if (r1 == 0) goto L_0x0057
            r1.close()
        L_0x0057:
            if (r2 == 0) goto L_0x005c
            r2.close()
        L_0x005c:
            return r0
        L_0x005d:
            r0 = move-exception
            goto L_0x006c
        L_0x005f:
            goto L_0x0079
        L_0x0061:
            r2 = move-exception
            r7 = r2
            r2 = r0
            r0 = r7
            goto L_0x006c
        L_0x0066:
            r2 = r0
            goto L_0x0079
        L_0x0068:
            r1 = move-exception
            r2 = r0
            r0 = r1
            r1 = r2
        L_0x006c:
            if (r1 == 0) goto L_0x0071
            r1.close()
        L_0x0071:
            if (r2 == 0) goto L_0x0076
            r2.close()
        L_0x0076:
            throw r0
        L_0x0077:
            r1 = r0
            r2 = r1
        L_0x0079:
            if (r1 == 0) goto L_0x007e
            r1.close()
        L_0x007e:
            if (r2 == 0) goto L_0x0083
            r2.close()
        L_0x0083:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.m.g.d():org.json.JSONObject");
    }

    public JSONObject a() {
        int n = a.n(this.b);
        return n > 0 ? a(n) : d();
    }

    public boolean a(JSONArray jSONArray) {
        boolean h = a.h(this.b);
        boolean z = true;
        for (int i = 0; i < jSONArray.length(); i++) {
            try {
                JSONObject jSONObject = jSONArray.getJSONObject(i);
                String string = jSONObject.getString("id");
                int i2 = jSONObject.getInt("code");
                if (i2 == 1) {
                    if (!this.c.b(string)) {
                        if (!h) {
                        }
                    }
                } else if (i2 < 1000 || i2 >= 2000) {
                    if (i2 >= 2000) {
                        if (i2 < 3000) {
                            if (!this.c.b(string)) {
                                if (!h) {
                                }
                            }
                        }
                    }
                } else {
                    a(string);
                    z = false;
                }
                e.b(string);
            } catch (JSONException unused) {
            }
        }
        return z;
    }

    public void b() {
        this.c.g();
        this.c.b();
        e.c(this.b);
    }

    public void b(JSONArray jSONArray) {
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            try {
                a(jSONArray.getJSONObject(i).getString("id"));
            } catch (JSONException unused) {
            }
        }
    }

    public boolean c() {
        int n = a.n(this.b);
        boolean z = true;
        if (n < 1) {
            return false;
        }
        Cursor cursor = null;
        try {
            cursor = this.c.d();
            if ((cursor.moveToFirst() ? cursor.getInt(0) : 0) + e.b(this.b) <= n) {
                z = false;
            }
            return z;
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }
}
