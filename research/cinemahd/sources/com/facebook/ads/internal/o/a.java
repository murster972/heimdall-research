package com.facebook.ads.internal.o;

import com.facebook.ads.internal.protocol.c;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static Map<String, Long> f2759a = new ConcurrentHashMap();
    private static Map<String, Long> b = new ConcurrentHashMap();
    private static Map<String, String> c = new ConcurrentHashMap();

    /* renamed from: com.facebook.ads.internal.o.a$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2760a = new int[c.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.facebook.ads.internal.protocol.c[] r0 = com.facebook.ads.internal.protocol.c.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2760a = r0
                int[] r0 = f2760a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.protocol.c r1 = com.facebook.ads.internal.protocol.c.BANNER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2760a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.protocol.c r1 = com.facebook.ads.internal.protocol.c.INTERSTITIAL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2760a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.protocol.c r1 = com.facebook.ads.internal.protocol.c.NATIVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2760a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.protocol.c r1 = com.facebook.ads.internal.protocol.c.UNKNOWN     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.o.a.AnonymousClass1.<clinit>():void");
        }
    }

    private static long a(String str, c cVar) {
        if (f2759a.containsKey(str)) {
            return f2759a.get(str).longValue();
        }
        int i = AnonymousClass1.f2760a[cVar.ordinal()];
        if (i == 1) {
            return 15000;
        }
        if (i == 2 || i != 3) {
        }
        return -1000;
    }

    public static void a(long j, b bVar) {
        f2759a.put(d(bVar), Long.valueOf(j));
    }

    public static void a(String str, b bVar) {
        c.put(d(bVar), str);
    }

    public static boolean a(b bVar) {
        String d = d(bVar);
        if (!b.containsKey(d)) {
            return false;
        }
        return System.currentTimeMillis() - b.get(d).longValue() < a(d, bVar.b());
    }

    public static void b(b bVar) {
        b.put(d(bVar), Long.valueOf(System.currentTimeMillis()));
    }

    public static String c(b bVar) {
        return c.get(d(bVar));
    }

    private static String d(b bVar) {
        Object[] objArr = new Object[6];
        int i = 0;
        objArr[0] = bVar.a();
        objArr[1] = bVar.b();
        objArr[2] = bVar.c;
        objArr[3] = Integer.valueOf(bVar.c() == null ? 0 : bVar.c().a());
        if (bVar.c() != null) {
            i = bVar.c().b();
        }
        objArr[4] = Integer.valueOf(i);
        objArr[5] = Integer.valueOf(bVar.d());
        return String.format("%s:%s:%s:%d:%d:%d", objArr);
    }
}
