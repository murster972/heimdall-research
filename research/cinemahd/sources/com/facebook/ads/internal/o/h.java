package com.facebook.ads.internal.o;

import com.facebook.ads.internal.h.c;
import com.facebook.ads.internal.o.f;

public class h extends f {

    /* renamed from: a  reason: collision with root package name */
    private final String f2769a;
    private final int b;

    public h(String str, int i, c cVar) {
        super(f.a.ERROR, cVar, (String) null, (String) null, (String) null);
        this.b = i;
        this.f2769a = str;
    }

    public /* bridge */ /* synthetic */ c a() {
        return super.a();
    }

    public String f() {
        return this.f2769a;
    }

    public int g() {
        return this.b;
    }
}
