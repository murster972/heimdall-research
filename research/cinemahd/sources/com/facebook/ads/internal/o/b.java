package com.facebook.ads.internal.o;

import android.content.Context;
import com.facebook.ads.internal.protocol.AdPlacementType;
import com.facebook.ads.internal.protocol.c;
import com.facebook.ads.internal.protocol.d;
import com.facebook.ads.internal.protocol.f;
import com.facebook.ads.internal.protocol.h;
import com.facebook.ads.internal.q.a.l;
import com.facebook.ads.internal.q.a.t;
import com.facebook.ads.internal.q.a.z;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import okhttp3.internal.cache.DiskLruCache;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected String f2761a;
    public Context b;
    public f c;
    private c d;
    private final AdPlacementType e = this.d.a();
    private final String f;
    private final String g;
    private d h;
    private boolean i;
    private boolean j;
    private int k;
    private l l;
    private final Map<String, String> m;
    private final h n;
    private String o;

    public b(Context context, com.facebook.ads.internal.i.c cVar, String str, l lVar, f fVar, d dVar, String str2, String str3, int i2, boolean z, boolean z2, h hVar, String str4) {
        this.f2761a = str;
        this.l = lVar;
        this.c = fVar;
        this.d = c.a(fVar);
        this.h = dVar;
        this.f = str2;
        this.g = str3;
        this.k = i2;
        this.i = z;
        this.j = z2;
        this.m = cVar.b();
        this.n = hVar;
        this.b = context;
        this.o = str4;
    }

    private void a(Map<String, String> map, String str, String str2) {
        map.put(str, str2);
    }

    public String a() {
        return this.f2761a;
    }

    public c b() {
        return this.d;
    }

    public l c() {
        return this.l;
    }

    public int d() {
        return this.k;
    }

    public h e() {
        return this.n;
    }

    public Map<String, String> f() {
        HashMap hashMap = new HashMap(this.m);
        a(hashMap, "IDFA", com.facebook.ads.internal.c.b.b);
        a(hashMap, "IDFA_FLAG", com.facebook.ads.internal.c.b.c ? "0" : DiskLruCache.VERSION_1);
        a(hashMap, "COPPA", String.valueOf(this.j));
        a(hashMap, "PLACEMENT_ID", this.f2761a);
        AdPlacementType adPlacementType = this.e;
        if (adPlacementType != AdPlacementType.UNKNOWN) {
            a(hashMap, "PLACEMENT_TYPE", adPlacementType.toString().toLowerCase());
        }
        l lVar = this.l;
        if (lVar != null) {
            a(hashMap, "WIDTH", String.valueOf(lVar.b()));
            a(hashMap, "HEIGHT", String.valueOf(this.l.a()));
        }
        a(hashMap, "ADAPTERS", this.g);
        f fVar = this.c;
        if (fVar != null) {
            a(hashMap, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_TEMPLATE_ID, String.valueOf(fVar.a()));
        }
        d dVar = this.h;
        if (dVar != null) {
            a(hashMap, "REQUEST_TYPE", String.valueOf(dVar.a()));
        }
        if (this.i) {
            a(hashMap, "TEST_MODE", DiskLruCache.VERSION_1);
        }
        String str = this.f;
        if (str != null) {
            a(hashMap, "DEMO_AD_ID", str);
        }
        int i2 = this.k;
        if (i2 != 0) {
            a(hashMap, "NUM_ADS_REQUESTED", String.valueOf(i2));
        }
        a(hashMap, "CLIENT_EVENTS", com.facebook.ads.internal.j.b.a());
        a(hashMap, "KG_RESTRICTED", String.valueOf(z.a(this.b)));
        a(hashMap, "REQUEST_TIME", t.a(System.currentTimeMillis()));
        if (this.n.c()) {
            a(hashMap, "BID_ID", this.n.d());
        }
        String str2 = this.o;
        if (str2 != null) {
            a(hashMap, "STACK_TRACE", str2);
        }
        a(hashMap, "CLIENT_REQUEST_ID", UUID.randomUUID().toString());
        return hashMap;
    }
}
