package com.facebook.ads.internal.protocol;

public class b extends Exception {

    /* renamed from: a  reason: collision with root package name */
    private final AdErrorType f2800a;
    private final String b;

    public b(AdErrorType adErrorType, String str) {
        this(adErrorType, str, (Throwable) null);
    }

    public b(AdErrorType adErrorType, String str, Throwable th) {
        super(str, th);
        this.f2800a = adErrorType;
        this.b = str;
    }

    public AdErrorType a() {
        return this.f2800a;
    }

    public String b() {
        return this.b;
    }
}
