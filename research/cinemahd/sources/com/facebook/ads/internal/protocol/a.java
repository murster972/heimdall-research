package com.facebook.ads.internal.protocol;

import android.text.TextUtils;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final AdErrorType f2799a;
    private final String b;

    public a(int i, String str) {
        this(AdErrorType.adErrorTypeFromCode(i), str);
    }

    public a(AdErrorType adErrorType, String str) {
        str = TextUtils.isEmpty(str) ? adErrorType.getDefaultErrorMessage() : str;
        this.f2799a = adErrorType;
        this.b = str;
    }

    public static a a(AdErrorType adErrorType, String str) {
        return new a(adErrorType, str);
    }

    public static a a(b bVar) {
        return new a(bVar.a(), bVar.b());
    }

    public AdErrorType a() {
        return this.f2799a;
    }

    public String b() {
        return this.b;
    }
}
