package com.facebook.ads.internal.protocol;

public enum c {
    UNKNOWN,
    BANNER,
    INTERSTITIAL,
    NATIVE,
    REWARDED_VIDEO;

    /* renamed from: com.facebook.ads.internal.protocol.c$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2802a = null;
        static final /* synthetic */ int[] b = null;

        /* JADX WARNING: Can't wrap try/catch for region: R(30:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|38) */
        /* JADX WARNING: Can't wrap try/catch for region: R(31:0|(2:1|2)|3|5|6|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|38) */
        /* JADX WARNING: Can't wrap try/catch for region: R(33:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|38) */
        /* JADX WARNING: Code restructure failed: missing block: B:39:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0093 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x009f */
        static {
            /*
                com.facebook.ads.internal.protocol.c[] r0 = com.facebook.ads.internal.protocol.c.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.protocol.c r2 = com.facebook.ads.internal.protocol.c.INTERSTITIAL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.protocol.c r3 = com.facebook.ads.internal.protocol.c.BANNER     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.protocol.c r4 = com.facebook.ads.internal.protocol.c.NATIVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.protocol.c r5 = com.facebook.ads.internal.protocol.c.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                com.facebook.ads.internal.protocol.f[] r4 = com.facebook.ads.internal.protocol.f.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                f2802a = r4
                int[] r4 = f2802a     // Catch:{ NoSuchFieldError -> 0x0048 }
                com.facebook.ads.internal.protocol.f r5 = com.facebook.ads.internal.protocol.f.NATIVE_UNKNOWN     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x0052 }
                com.facebook.ads.internal.protocol.f r4 = com.facebook.ads.internal.protocol.f.WEBVIEW_BANNER_50     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x005c }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_BANNER_90     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x0066 }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_BANNER_LEGACY     // Catch:{ NoSuchFieldError -> 0x0066 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
            L_0x0066:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x0071 }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_BANNER_250     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x007c }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_INTERSTITIAL_HORIZONTAL     // Catch:{ NoSuchFieldError -> 0x007c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
            L_0x007c:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x0087 }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_INTERSTITIAL_VERTICAL     // Catch:{ NoSuchFieldError -> 0x0087 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0087 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0087 }
            L_0x0087:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x0093 }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_INTERSTITIAL_TABLET     // Catch:{ NoSuchFieldError -> 0x0093 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0093 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0093 }
            L_0x0093:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x009f }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.WEBVIEW_INTERSTITIAL_UNKNOWN     // Catch:{ NoSuchFieldError -> 0x009f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009f }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009f }
            L_0x009f:
                int[] r0 = f2802a     // Catch:{ NoSuchFieldError -> 0x00ab }
                com.facebook.ads.internal.protocol.f r1 = com.facebook.ads.internal.protocol.f.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x00ab }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ab }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ab }
            L_0x00ab:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.protocol.c.AnonymousClass1.<clinit>():void");
        }
    }

    public static c a(f fVar) {
        switch (AnonymousClass1.f2802a[fVar.ordinal()]) {
            case 1:
                return NATIVE;
            case 2:
            case 3:
            case 4:
            case 5:
                return BANNER;
            case 6:
            case 7:
            case 8:
            case 9:
                return INTERSTITIAL;
            case 10:
                return REWARDED_VIDEO;
            default:
                return UNKNOWN;
        }
    }

    public AdPlacementType a() {
        int i = AnonymousClass1.b[ordinal()];
        return i != 1 ? i != 2 ? i != 3 ? i != 4 ? AdPlacementType.UNKNOWN : AdPlacementType.REWARDED_VIDEO : AdPlacementType.NATIVE : AdPlacementType.BANNER : AdPlacementType.INTERSTITIAL;
    }
}
