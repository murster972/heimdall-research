package com.facebook.ads.internal.protocol;

import java.io.Serializable;

public enum e implements Serializable {
    BANNER_320_50(320, 50),
    INTERSTITIAL(0, 0),
    BANNER_HEIGHT_50(-1, 50),
    BANNER_HEIGHT_90(-1, 90),
    RECTANGLE_HEIGHT_250(-1, 250);
    
    private final int f;
    private final int g;

    private e(int i, int i2) {
        this.f = i;
        this.g = i2;
    }

    public static e a(int i, int i2) {
        e eVar = INTERSTITIAL;
        if (eVar.g == i2 && eVar.f == i) {
            return eVar;
        }
        e eVar2 = BANNER_320_50;
        if (eVar2.g == i2 && eVar2.f == i) {
            return eVar2;
        }
        e eVar3 = BANNER_HEIGHT_50;
        if (eVar3.g == i2 && eVar3.f == i) {
            return eVar3;
        }
        e eVar4 = BANNER_HEIGHT_90;
        if (eVar4.g == i2 && eVar4.f == i) {
            return eVar4;
        }
        e eVar5 = RECTANGLE_HEIGHT_250;
        if (eVar5.g == i2 && eVar5.f == i) {
            return eVar5;
        }
        return null;
    }

    public int a() {
        return this.f;
    }

    public int b() {
        return this.g;
    }
}
