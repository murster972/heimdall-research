package com.facebook.ads.internal.protocol;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.ads.internal.q.d.b;
import com.vungle.warren.model.Advertisement;
import org.json.JSONException;
import org.json.JSONObject;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private final a f2807a;
    private final Long b;
    private final String c;
    private final String d;

    /* renamed from: com.facebook.ads.internal.protocol.h$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2808a = new int[a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.facebook.ads.internal.protocol.h$a[] r0 = com.facebook.ads.internal.protocol.h.a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2808a = r0
                int[] r0 = f2808a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.protocol.h$a r1 = com.facebook.ads.internal.protocol.h.a.ID     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2808a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.protocol.h$a r1 = com.facebook.ads.internal.protocol.h.a.CREATIVE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.internal.protocol.h.AnonymousClass1.<clinit>():void");
        }
    }

    private enum a {
        ID,
        CREATIVE,
        NONE
    }

    public h(Context context, String str, String str2, f fVar) {
        if (TextUtils.isEmpty(str)) {
            this.f2807a = a.NONE;
            this.b = null;
            this.d = null;
            this.c = null;
            return;
        }
        try {
            JSONObject jSONObject = new JSONObject(str);
            int i = AnonymousClass1.f2808a[a.valueOf(jSONObject.getString("type").toUpperCase()).ordinal()];
            if (i == 1) {
                this.f2807a = a.ID;
                this.b = Long.valueOf(jSONObject.getString("bid_id"));
                this.d = jSONObject.getString("device_id");
                this.c = null;
            } else if (i == 2) {
                this.f2807a = a.CREATIVE;
                this.b = Long.valueOf(jSONObject.getString("bid_id"));
                this.d = jSONObject.getString("device_id");
                this.c = new JSONObject(jSONObject.getString("payload")).toString();
            } else {
                AdErrorType adErrorType = AdErrorType.BID_PAYLOAD_ERROR;
                throw new b(adErrorType, "Unsupported BidPayload type " + jSONObject.getString("type"));
            }
            if (!jSONObject.getString("sdk_version").equals("4.99.1")) {
                throw new b(AdErrorType.BID_IMPRESSION_MISMATCH, String.format("Bid %d for SDK version %s being used on SDK version %s", new Object[]{this.b, jSONObject.getString("sdk_version"), "4.99.1"}));
            } else if (!jSONObject.getString("resolved_placement_id").equals(str2)) {
                throw new b(AdErrorType.BID_IMPRESSION_MISMATCH, String.format("Bid %d for placement %s being used on placement %s", new Object[]{this.b, jSONObject.getString("resolved_placement_id"), str2}));
            } else if (jSONObject.getInt(Advertisement.KEY_TEMPLATE) != fVar.a()) {
                throw new b(AdErrorType.BID_IMPRESSION_MISMATCH, String.format("Bid %d for template %s being used on template %s", new Object[]{this.b, Integer.valueOf(jSONObject.getInt(Advertisement.KEY_TEMPLATE)), fVar}));
            }
        } catch (JSONException e) {
            com.facebook.ads.internal.q.d.a.a(context, "api", b.d, (Exception) e);
            throw new b(AdErrorType.BID_PAYLOAD_ERROR, "Invalid BidPayload", e);
        }
    }

    public void a(String str) {
        if (!this.d.equals(str)) {
            throw new b(AdErrorType.BID_IMPRESSION_MISMATCH, String.format("Bid %d for IDFA %s being used on IDFA %s", new Object[]{this.b, this.d, str}));
        }
    }

    public boolean a() {
        return this.f2807a == a.CREATIVE;
    }

    public String b() {
        return this.c;
    }

    public boolean c() {
        return this.f2807a != a.NONE;
    }

    public String d() {
        Long l = this.b;
        if (l == null) {
            return null;
        }
        return l.toString();
    }
}
