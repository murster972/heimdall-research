package com.facebook.ads;

import com.facebook.ads.internal.n.m;

public enum VideoAutoplayBehavior {
    DEFAULT,
    ON,
    OFF;

    /* renamed from: com.facebook.ads.VideoAutoplayBehavior$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2586a = null;

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.facebook.ads.internal.n.m[] r0 = com.facebook.ads.internal.n.m.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2586a = r0
                int[] r0 = f2586a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.n.m r1 = com.facebook.ads.internal.n.m.DEFAULT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2586a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.n.m r1 = com.facebook.ads.internal.n.m.ON     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2586a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.n.m r1 = com.facebook.ads.internal.n.m.OFF     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.VideoAutoplayBehavior.AnonymousClass1.<clinit>():void");
        }
    }

    public static VideoAutoplayBehavior fromInternalAutoplayBehavior(m mVar) {
        if (mVar == null) {
            return DEFAULT;
        }
        int i = AnonymousClass1.f2586a[mVar.ordinal()];
        return i != 1 ? i != 2 ? i != 3 ? DEFAULT : OFF : ON : DEFAULT;
    }
}
