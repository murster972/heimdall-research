package com.facebook.ads;

import android.graphics.Typeface;
import com.facebook.ads.internal.j.a;
import com.facebook.ads.internal.j.b;
import com.facebook.ads.internal.n.k;
import org.json.JSONObject;

public class NativeAdViewAttributes {

    /* renamed from: a  reason: collision with root package name */
    private k f2576a;

    public NativeAdViewAttributes() {
        this.f2576a = new k();
    }

    NativeAdViewAttributes(k kVar) {
        this.f2576a = kVar;
    }

    public NativeAdViewAttributes(JSONObject jSONObject) {
        try {
            this.f2576a = new k(jSONObject);
        } catch (Exception e) {
            this.f2576a = new k();
            b.a(a.a(e, "Error retrieving native ui configuration data"));
        }
    }

    /* access modifiers changed from: package-private */
    public k a() {
        return this.f2576a;
    }

    public boolean getAutoplay() {
        return this.f2576a.j();
    }

    public boolean getAutoplayOnMobile() {
        return this.f2576a.k();
    }

    public int getBackgroundColor() {
        return this.f2576a.b();
    }

    public int getButtonBorderColor() {
        return this.f2576a.g();
    }

    public int getButtonColor() {
        return this.f2576a.e();
    }

    public int getButtonTextColor() {
        return this.f2576a.f();
    }

    public int getDescriptionTextColor() {
        return this.f2576a.d();
    }

    public int getDescriptionTextSize() {
        return this.f2576a.i();
    }

    public int getTitleTextColor() {
        return this.f2576a.c();
    }

    public int getTitleTextSize() {
        return this.f2576a.h();
    }

    public Typeface getTypeface() {
        return this.f2576a.a();
    }

    public NativeAdViewAttributes setAutoplay(boolean z) {
        this.f2576a.b(z);
        return this;
    }

    public NativeAdViewAttributes setAutoplayOnMobile(boolean z) {
        this.f2576a.a(z);
        return this;
    }

    public NativeAdViewAttributes setBackgroundColor(int i) {
        this.f2576a.a(i);
        return this;
    }

    public NativeAdViewAttributes setButtonBorderColor(int i) {
        this.f2576a.f(i);
        return this;
    }

    public NativeAdViewAttributes setButtonColor(int i) {
        this.f2576a.d(i);
        return this;
    }

    public NativeAdViewAttributes setButtonTextColor(int i) {
        this.f2576a.e(i);
        return this;
    }

    public NativeAdViewAttributes setDescriptionTextColor(int i) {
        this.f2576a.c(i);
        return this;
    }

    public NativeAdViewAttributes setTitleTextColor(int i) {
        this.f2576a.b(i);
        return this;
    }

    public NativeAdViewAttributes setTypeface(Typeface typeface) {
        this.f2576a.a(typeface);
        return this;
    }
}
