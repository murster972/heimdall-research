package com.facebook.ads;

public final class R {

    public static final class array {
        public static final int cast_expanded_controller_default_control_buttons = 2130903041;
        public static final int cast_mini_controller_default_control_buttons = 2130903043;

        private array() {
        }
    }

    public static final class attr {
        public static final int adSize = 2130968613;
        public static final int adSizes = 2130968614;
        public static final int adUnitId = 2130968615;
        public static final int buttonSize = 2130968693;
        public static final int castBackground = 2130968711;
        public static final int castBackgroundColor = 2130968712;
        public static final int castButtonBackgroundColor = 2130968713;
        public static final int castButtonColor = 2130968714;
        public static final int castButtonText = 2130968715;
        public static final int castButtonTextAppearance = 2130968716;
        public static final int castClosedCaptionsButtonDrawable = 2130968717;
        public static final int castControlButtons = 2130968718;
        public static final int castExpandedControllerStyle = 2130968720;
        public static final int castExpandedControllerToolbarStyle = 2130968721;
        public static final int castFocusRadius = 2130968722;
        public static final int castForward30ButtonDrawable = 2130968723;
        public static final int castIntroOverlayStyle = 2130968724;
        public static final int castLargePauseButtonDrawable = 2130968725;
        public static final int castLargePlayButtonDrawable = 2130968726;
        public static final int castLargeStopButtonDrawable = 2130968727;
        public static final int castMiniControllerStyle = 2130968730;
        public static final int castMuteToggleButtonDrawable = 2130968731;
        public static final int castPauseButtonDrawable = 2130968732;
        public static final int castPlayButtonDrawable = 2130968733;
        public static final int castProgressBarColor = 2130968734;
        public static final int castRewind30ButtonDrawable = 2130968735;
        public static final int castSeekBarProgressDrawable = 2130968737;
        public static final int castSeekBarThumbDrawable = 2130968738;
        public static final int castShowImageThumbnail = 2130968739;
        public static final int castSkipNextButtonDrawable = 2130968740;
        public static final int castSkipPreviousButtonDrawable = 2130968741;
        public static final int castStopButtonDrawable = 2130968742;
        public static final int castSubtitleTextAppearance = 2130968743;
        public static final int castTitleTextAppearance = 2130968744;
        public static final int circleCrop = 2130968773;
        public static final int colorScheme = 2130968797;
        public static final int fastScrollEnabled = 2130968891;
        public static final int fastScrollHorizontalThumbDrawable = 2130968892;
        public static final int fastScrollHorizontalTrackDrawable = 2130968893;
        public static final int fastScrollVerticalThumbDrawable = 2130968894;
        public static final int fastScrollVerticalTrackDrawable = 2130968895;
        public static final int font = 2130968899;
        public static final int fontProviderAuthority = 2130968901;
        public static final int fontProviderCerts = 2130968902;
        public static final int fontProviderFetchStrategy = 2130968903;
        public static final int fontProviderFetchTimeout = 2130968904;
        public static final int fontProviderPackage = 2130968905;
        public static final int fontProviderQuery = 2130968906;
        public static final int fontStyle = 2130968907;
        public static final int fontWeight = 2130968909;
        public static final int imageAspectRatio = 2130968940;
        public static final int imageAspectRatioAdjust = 2130968941;
        public static final int layoutManager = 2130968967;
        public static final int reverseLayout = 2130969147;
        public static final int scopeUris = 2130969166;
        public static final int spanCount = 2130969203;
        public static final int stackFromEnd = 2130969209;

        private attr() {
        }
    }

    public static final class bool {
        public static final int abc_action_bar_embed_tabs = 2131034112;

        private bool() {
        }
    }

    public static final class color {
        public static final int cast_expanded_controller_ad_container_white_stripe_color = 2131099725;
        public static final int cast_expanded_controller_ad_label_background_color = 2131099727;
        public static final int cast_expanded_controller_background_color = 2131099729;
        public static final int cast_expanded_controller_progress_text_color = 2131099732;
        public static final int cast_expanded_controller_seek_bar_progress_background_tint_color = 2131099733;
        public static final int cast_expanded_controller_text_color = 2131099734;
        public static final int cast_intro_overlay_background_color = 2131099735;
        public static final int cast_intro_overlay_button_background_color = 2131099736;
        public static final int cast_libraries_material_featurehighlight_outer_highlight_default_color = 2131099737;
        public static final int cast_libraries_material_featurehighlight_text_body_color = 2131099738;
        public static final int cast_libraries_material_featurehighlight_text_header_color = 2131099739;
        public static final int common_google_signin_btn_text_dark = 2131099752;
        public static final int common_google_signin_btn_text_dark_default = 2131099753;
        public static final int common_google_signin_btn_text_dark_disabled = 2131099754;
        public static final int common_google_signin_btn_text_dark_focused = 2131099755;
        public static final int common_google_signin_btn_text_dark_pressed = 2131099756;
        public static final int common_google_signin_btn_text_light = 2131099757;
        public static final int common_google_signin_btn_text_light_default = 2131099758;
        public static final int common_google_signin_btn_text_light_disabled = 2131099759;
        public static final int common_google_signin_btn_text_light_focused = 2131099760;
        public static final int common_google_signin_btn_text_light_pressed = 2131099761;
        public static final int notification_action_color_filter = 2131100106;
        public static final int notification_icon_bg_color = 2131100107;
        public static final int notification_material_background_media_default_color = 2131100108;
        public static final int primary_text_default_material_dark = 2131100116;
        public static final int ripple_material_light = 2131100121;
        public static final int secondary_text_default_material_dark = 2131100123;
        public static final int secondary_text_default_material_light = 2131100124;

        private color() {
        }
    }

    public static final class dimen {
        public static final int cast_expanded_controller_ad_background_layout_height = 2131165282;
        public static final int cast_expanded_controller_ad_background_layout_width = 2131165283;
        public static final int cast_expanded_controller_ad_layout_height = 2131165286;
        public static final int cast_expanded_controller_ad_layout_width = 2131165287;
        public static final int cast_expanded_controller_control_button_margin = 2131165288;
        public static final int cast_expanded_controller_control_toolbar_min_height = 2131165289;
        public static final int cast_expanded_controller_margin_between_seek_bar_and_control_buttons = 2131165290;
        public static final int cast_expanded_controller_margin_between_status_text_and_seek_bar = 2131165291;
        public static final int cast_expanded_controller_seekbar_disabled_alpha = 2131165292;
        public static final int cast_intro_overlay_button_margin_bottom = 2131165293;
        public static final int cast_intro_overlay_focus_radius = 2131165294;
        public static final int cast_intro_overlay_title_margin_top = 2131165295;
        public static final int cast_libraries_material_featurehighlight_center_horizontal_offset = 2131165296;
        public static final int cast_libraries_material_featurehighlight_center_threshold = 2131165297;
        public static final int cast_libraries_material_featurehighlight_inner_margin = 2131165298;
        public static final int cast_libraries_material_featurehighlight_inner_radius = 2131165299;
        public static final int cast_libraries_material_featurehighlight_outer_padding = 2131165300;
        public static final int cast_libraries_material_featurehighlight_text_body_size = 2131165301;
        public static final int cast_libraries_material_featurehighlight_text_header_size = 2131165302;
        public static final int cast_libraries_material_featurehighlight_text_horizontal_margin = 2131165303;
        public static final int cast_libraries_material_featurehighlight_text_horizontal_offset = 2131165304;
        public static final int cast_libraries_material_featurehighlight_text_max_width = 2131165305;
        public static final int cast_libraries_material_featurehighlight_text_vertical_space = 2131165306;
        public static final int cast_mini_controller_control_button_margin = 2131165307;
        public static final int cast_mini_controller_icon_height = 2131165308;
        public static final int cast_mini_controller_icon_width = 2131165309;
        public static final int cast_notification_image_size = 2131165310;
        public static final int cast_tracks_chooser_dialog_no_message_text_size = 2131165311;
        public static final int cast_tracks_chooser_dialog_row_text_size = 2131165312;
        public static final int compat_button_inset_horizontal_material = 2131165317;
        public static final int compat_button_inset_vertical_material = 2131165318;
        public static final int compat_button_padding_horizontal_material = 2131165319;
        public static final int compat_button_padding_vertical_material = 2131165320;
        public static final int compat_control_corner_material = 2131165321;
        public static final int fastscroll_default_thickness = 2131165387;
        public static final int fastscroll_margin = 2131165388;
        public static final int fastscroll_minimum_range = 2131165389;
        public static final int item_touch_helper_max_drag_scroll_per_frame = 2131165407;
        public static final int item_touch_helper_swipe_escape_max_velocity = 2131165408;
        public static final int item_touch_helper_swipe_escape_velocity = 2131165409;
        public static final int notification_action_icon_size = 2131165485;
        public static final int notification_action_text_size = 2131165486;
        public static final int notification_big_circle_margin = 2131165487;
        public static final int notification_content_margin_start = 2131165488;
        public static final int notification_large_icon_height = 2131165489;
        public static final int notification_large_icon_width = 2131165490;
        public static final int notification_main_column_padding_top = 2131165491;
        public static final int notification_media_narrow_margin = 2131165492;
        public static final int notification_right_icon_size = 2131165493;
        public static final int notification_right_side_padding_top = 2131165494;
        public static final int notification_small_icon_background_padding = 2131165495;
        public static final int notification_small_icon_size_as_large = 2131165496;
        public static final int notification_subtext_size = 2131165497;
        public static final int notification_top_pad = 2131165498;
        public static final int notification_top_pad_large_text = 2131165499;

        private dimen() {
        }
    }

    public static final class drawable {
        public static final int cast_abc_scrubber_control_off_mtrl_alpha = 2131230886;
        public static final int cast_abc_scrubber_control_to_pressed_mtrl_000 = 2131230887;
        public static final int cast_abc_scrubber_control_to_pressed_mtrl_005 = 2131230888;
        public static final int cast_abc_scrubber_primary_mtrl_alpha = 2131230889;
        public static final int cast_album_art_placeholder = 2131230890;
        public static final int cast_album_art_placeholder_large = 2131230891;
        public static final int cast_expanded_controller_actionbar_bg_gradient_light = 2131230892;
        public static final int cast_expanded_controller_bg_gradient_light = 2131230893;
        public static final int cast_expanded_controller_seekbar_thumb = 2131230895;
        public static final int cast_expanded_controller_seekbar_track = 2131230896;
        public static final int cast_ic_expanded_controller_closed_caption = 2131230897;
        public static final int cast_ic_expanded_controller_forward30 = 2131230898;
        public static final int cast_ic_expanded_controller_mute = 2131230899;
        public static final int cast_ic_expanded_controller_pause = 2131230900;
        public static final int cast_ic_expanded_controller_play = 2131230901;
        public static final int cast_ic_expanded_controller_rewind30 = 2131230902;
        public static final int cast_ic_expanded_controller_skip_next = 2131230903;
        public static final int cast_ic_expanded_controller_skip_previous = 2131230904;
        public static final int cast_ic_expanded_controller_stop = 2131230905;
        public static final int cast_ic_mini_controller_closed_caption = 2131230906;
        public static final int cast_ic_mini_controller_forward30 = 2131230907;
        public static final int cast_ic_mini_controller_mute = 2131230908;
        public static final int cast_ic_mini_controller_pause = 2131230909;
        public static final int cast_ic_mini_controller_pause_large = 2131230910;
        public static final int cast_ic_mini_controller_play = 2131230911;
        public static final int cast_ic_mini_controller_play_large = 2131230912;
        public static final int cast_ic_mini_controller_rewind30 = 2131230913;
        public static final int cast_ic_mini_controller_skip_next = 2131230914;
        public static final int cast_ic_mini_controller_skip_prev = 2131230915;
        public static final int cast_ic_mini_controller_stop = 2131230916;
        public static final int cast_ic_mini_controller_stop_large = 2131230917;
        public static final int cast_ic_notification_0 = 2131230918;
        public static final int cast_ic_notification_1 = 2131230919;
        public static final int cast_ic_notification_2 = 2131230920;
        public static final int cast_ic_notification_connecting = 2131230921;
        public static final int cast_ic_notification_disconnect = 2131230922;
        public static final int cast_ic_notification_forward = 2131230923;
        public static final int cast_ic_notification_forward10 = 2131230924;
        public static final int cast_ic_notification_forward30 = 2131230925;
        public static final int cast_ic_notification_on = 2131230926;
        public static final int cast_ic_notification_pause = 2131230927;
        public static final int cast_ic_notification_play = 2131230928;
        public static final int cast_ic_notification_rewind = 2131230929;
        public static final int cast_ic_notification_rewind10 = 2131230930;
        public static final int cast_ic_notification_rewind30 = 2131230931;
        public static final int cast_ic_notification_skip_next = 2131230932;
        public static final int cast_ic_notification_skip_prev = 2131230933;
        public static final int cast_ic_notification_small_icon = 2131230934;
        public static final int cast_ic_notification_stop_live_stream = 2131230935;
        public static final int cast_ic_stop_circle_filled_grey600 = 2131230936;
        public static final int cast_ic_stop_circle_filled_white = 2131230937;
        public static final int cast_mini_controller_gradient_light = 2131230938;
        public static final int cast_mini_controller_progress_drawable = 2131230939;
        public static final int cast_skip_ad_label_border = 2131230941;
        public static final int common_full_open_on_phone = 2131230947;
        public static final int common_google_signin_btn_icon_dark = 2131230948;
        public static final int common_google_signin_btn_icon_dark_focused = 2131230949;
        public static final int common_google_signin_btn_icon_dark_normal = 2131230950;
        public static final int common_google_signin_btn_icon_light = 2131230953;
        public static final int common_google_signin_btn_icon_light_focused = 2131230954;
        public static final int common_google_signin_btn_icon_light_normal = 2131230955;
        public static final int common_google_signin_btn_text_dark = 2131230957;
        public static final int common_google_signin_btn_text_dark_focused = 2131230958;
        public static final int common_google_signin_btn_text_dark_normal = 2131230959;
        public static final int common_google_signin_btn_text_light = 2131230962;
        public static final int common_google_signin_btn_text_light_focused = 2131230963;
        public static final int common_google_signin_btn_text_light_normal = 2131230964;
        public static final int notification_action_background = 2131231305;
        public static final int notification_bg = 2131231306;
        public static final int notification_bg_low = 2131231307;
        public static final int notification_bg_low_normal = 2131231308;
        public static final int notification_bg_low_pressed = 2131231309;
        public static final int notification_bg_normal = 2131231310;
        public static final int notification_bg_normal_pressed = 2131231311;
        public static final int notification_icon_background = 2131231312;
        public static final int notification_template_icon_bg = 2131231313;
        public static final int notification_template_icon_low_bg = 2131231314;
        public static final int notification_tile_bg = 2131231315;
        public static final int notify_panel_notification_icon_bg = 2131231316;
        public static final int quantum_ic_art_track_grey600_48 = 2131231320;
        public static final int quantum_ic_bigtop_updates_white_24 = 2131231321;
        public static final int quantum_ic_cast_connected_white_24 = 2131231322;
        public static final int quantum_ic_cast_white_36 = 2131231323;
        public static final int quantum_ic_clear_white_24 = 2131231324;
        public static final int quantum_ic_closed_caption_grey600_36 = 2131231325;
        public static final int quantum_ic_closed_caption_white_36 = 2131231326;
        public static final int quantum_ic_forward_10_white_24 = 2131231327;
        public static final int quantum_ic_forward_30_grey600_36 = 2131231328;
        public static final int quantum_ic_forward_30_white_24 = 2131231329;
        public static final int quantum_ic_forward_30_white_36 = 2131231330;
        public static final int quantum_ic_keyboard_arrow_down_white_36 = 2131231331;
        public static final int quantum_ic_pause_circle_filled_grey600_36 = 2131231332;
        public static final int quantum_ic_pause_circle_filled_white_36 = 2131231333;
        public static final int quantum_ic_pause_grey600_36 = 2131231334;
        public static final int quantum_ic_pause_grey600_48 = 2131231335;
        public static final int quantum_ic_pause_white_24 = 2131231336;
        public static final int quantum_ic_play_arrow_grey600_36 = 2131231337;
        public static final int quantum_ic_play_arrow_grey600_48 = 2131231338;
        public static final int quantum_ic_play_arrow_white_24 = 2131231339;
        public static final int quantum_ic_play_circle_filled_grey600_36 = 2131231340;
        public static final int quantum_ic_play_circle_filled_white_36 = 2131231341;
        public static final int quantum_ic_refresh_white_24 = 2131231342;
        public static final int quantum_ic_replay_10_white_24 = 2131231343;
        public static final int quantum_ic_replay_30_grey600_36 = 2131231344;
        public static final int quantum_ic_replay_30_white_24 = 2131231345;
        public static final int quantum_ic_replay_30_white_36 = 2131231346;
        public static final int quantum_ic_replay_white_24 = 2131231347;
        public static final int quantum_ic_skip_next_grey600_36 = 2131231348;
        public static final int quantum_ic_skip_next_white_24 = 2131231349;
        public static final int quantum_ic_skip_next_white_36 = 2131231350;
        public static final int quantum_ic_skip_previous_grey600_36 = 2131231351;
        public static final int quantum_ic_skip_previous_white_24 = 2131231352;
        public static final int quantum_ic_skip_previous_white_36 = 2131231353;
        public static final int quantum_ic_stop_grey600_36 = 2131231354;
        public static final int quantum_ic_stop_grey600_48 = 2131231355;
        public static final int quantum_ic_stop_white_24 = 2131231356;
        public static final int quantum_ic_volume_off_grey600_36 = 2131231357;
        public static final int quantum_ic_volume_off_white_36 = 2131231358;
        public static final int quantum_ic_volume_up_grey600_36 = 2131231359;
        public static final int quantum_ic_volume_up_white_36 = 2131231360;

        private drawable() {
        }
    }

    public static final class id {
        public static final int action0 = 2131296312;
        public static final int action_container = 2131296320;
        public static final int action_divider = 2131296323;
        public static final int action_image = 2131296324;
        public static final int action_text = 2131296334;
        public static final int actions = 2131296335;
        public static final int ad_container = 2131296346;
        public static final int ad_image_view = 2131296351;
        public static final int ad_in_progress_label = 2131296352;
        public static final int ad_label = 2131296353;
        public static final int adjust_height = 2131296367;
        public static final int adjust_width = 2131296368;
        public static final int async = 2131296384;
        public static final int audio_list_view = 2131296385;
        public static final int auto = 2131296386;
        public static final int background_image_view = 2131296391;
        public static final int background_place_holder_image_view = 2131296392;
        public static final int blocking = 2131296406;
        public static final int blurred_background_image_view = 2131296407;
        public static final int button = 2131296438;
        public static final int button_0 = 2131296441;
        public static final int button_1 = 2131296442;
        public static final int button_2 = 2131296443;
        public static final int button_3 = 2131296444;
        public static final int button_play_pause_toggle = 2131296451;
        public static final int cancel_action = 2131296461;
        public static final int cast_button_type_closed_caption = 2131296463;
        public static final int cast_button_type_custom = 2131296464;
        public static final int cast_button_type_empty = 2131296465;
        public static final int cast_button_type_forward_30_seconds = 2131296466;
        public static final int cast_button_type_mute_toggle = 2131296467;
        public static final int cast_button_type_play_pause_toggle = 2131296468;
        public static final int cast_button_type_rewind_30_seconds = 2131296469;
        public static final int cast_button_type_skip_next = 2131296470;
        public static final int cast_button_type_skip_previous = 2131296471;
        public static final int cast_featurehighlight_help_text_body_view = 2131296472;
        public static final int cast_featurehighlight_help_text_header_view = 2131296473;
        public static final int cast_featurehighlight_view = 2131296474;
        public static final int cast_notification_id = 2131296475;
        public static final int center = 2131296485;
        public static final int chronometer = 2131296502;
        public static final int container_all = 2131296512;
        public static final int container_current = 2131296513;
        public static final int controllers = 2131296523;
        public static final int dark = 2131296530;
        public static final int email = 2131296580;
        public static final int end_padder = 2131296588;
        public static final int end_text = 2131296589;
        public static final int expanded_controller_layout = 2131296619;
        public static final int forever = 2131296650;
        public static final int icon = 2131296680;
        public static final int icon_group = 2131296682;
        public static final int icon_only = 2131296683;
        public static final int icon_view = 2131296684;

        /* renamed from: info  reason: collision with root package name */
        public static final int f2581info = 2131296704;
        public static final int italic = 2131296711;
        public static final int item_touch_helper_previous_elevation = 2131296721;
        public static final int light = 2131296746;
        public static final int line1 = 2131296747;
        public static final int line3 = 2131296748;
        public static final int loading_indicator = 2131296767;
        public static final int media_actions = 2131296777;
        public static final int none = 2131296922;
        public static final int normal = 2131296923;
        public static final int notification_background = 2131296925;
        public static final int notification_main_column = 2131296926;
        public static final int notification_main_column_container = 2131296927;
        public static final int progressBar = 2131296953;
        public static final int radio = 2131296964;
        public static final int right_icon = 2131296986;
        public static final int right_side = 2131296987;
        public static final int seek_bar = 2131297029;
        public static final int standard = 2131297065;
        public static final int start_text = 2131297068;
        public static final int status_bar_latest_event_content = 2131297071;
        public static final int status_text = 2131297072;
        public static final int subtitle_view = 2131297080;
        public static final int tab_host = 2131297091;
        public static final int text = 2131297106;
        public static final int text1 = 2131297107;
        public static final int text2 = 2131297108;
        public static final int textTitle = 2131297112;
        public static final int text_list_view = 2131297135;
        public static final int time = 2131297150;
        public static final int title = 2131297151;
        public static final int title_view = 2131297157;
        public static final int toolbar = 2131297158;
        public static final int wide = 2131297239;
        public static final int wrap_content = 2131297243;

        private id() {
        }
    }

    public static final class integer {
        public static final int cancel_button_image_alpha = 2131361796;
        public static final int cast_libraries_material_featurehighlight_pulse_base_alpha = 2131361797;
        public static final int google_play_services_version = 2131361804;
        public static final int status_bar_notification_info_maxnum = 2131361818;

        private integer() {
        }
    }

    public static final class layout {
        public static final int cast_expanded_controller_activity = 2131492938;
        public static final int cast_help_text = 2131492939;
        public static final int cast_intro_overlay = 2131492940;
        public static final int cast_mini_controller = 2131492942;
        public static final int cast_tracks_chooser_dialog_layout = 2131492943;
        public static final int cast_tracks_chooser_dialog_row_layout = 2131492944;
        public static final int notification_action = 2131493112;
        public static final int notification_action_tombstone = 2131493113;
        public static final int notification_media_action = 2131493115;
        public static final int notification_media_cancel_action = 2131493116;
        public static final int notification_template_big_media = 2131493117;
        public static final int notification_template_big_media_custom = 2131493118;
        public static final int notification_template_big_media_narrow = 2131493119;
        public static final int notification_template_big_media_narrow_custom = 2131493120;
        public static final int notification_template_custom_big = 2131493121;
        public static final int notification_template_icon_group = 2131493122;
        public static final int notification_template_lines_media = 2131493123;
        public static final int notification_template_media = 2131493124;
        public static final int notification_template_media_custom = 2131493125;
        public static final int notification_template_part_chronometer = 2131493126;
        public static final int notification_template_part_time = 2131493127;

        private layout() {
        }
    }

    public static final class string {
        public static final int cast_ad_label = 2131755100;
        public static final int cast_casting_to_device = 2131755101;
        public static final int cast_closed_captions = 2131755102;
        public static final int cast_closed_captions_unavailable = 2131755103;
        public static final int cast_disconnect = 2131755106;
        public static final int cast_expanded_controller_ad_image_description = 2131755107;
        public static final int cast_expanded_controller_ad_in_progress = 2131755108;
        public static final int cast_expanded_controller_background_image = 2131755109;
        public static final int cast_expanded_controller_live_stream_indicator = 2131755111;
        public static final int cast_expanded_controller_loading = 2131755112;
        public static final int cast_expanded_controller_skip_ad_label = 2131755113;
        public static final int cast_forward = 2131755115;
        public static final int cast_forward_10 = 2131755116;
        public static final int cast_forward_30 = 2131755117;
        public static final int cast_intro_overlay_button_text = 2131755118;
        public static final int cast_invalid_stream_duration_text = 2131755119;
        public static final int cast_invalid_stream_position_text = 2131755120;
        public static final int cast_mute = 2131755122;
        public static final int cast_notification_connected_message = 2131755123;
        public static final int cast_notification_connecting_message = 2131755124;
        public static final int cast_notification_disconnect = 2131755126;
        public static final int cast_pause = 2131755127;
        public static final int cast_play = 2131755128;
        public static final int cast_rewind = 2131755129;
        public static final int cast_rewind_10 = 2131755130;
        public static final int cast_rewind_30 = 2131755131;
        public static final int cast_seek_bar = 2131755132;
        public static final int cast_skip_next = 2131755133;
        public static final int cast_skip_prev = 2131755134;
        public static final int cast_stop = 2131755135;
        public static final int cast_stop_live_stream = 2131755136;
        public static final int cast_tracks_chooser_dialog_audio = 2131755137;
        public static final int cast_tracks_chooser_dialog_cancel = 2131755138;
        public static final int cast_tracks_chooser_dialog_closed_captions = 2131755139;
        public static final int cast_tracks_chooser_dialog_default_track_name = 2131755140;
        public static final int cast_tracks_chooser_dialog_none = 2131755141;
        public static final int cast_tracks_chooser_dialog_ok = 2131755142;
        public static final int cast_tracks_chooser_dialog_subtitles = 2131755143;
        public static final int cast_unmute = 2131755144;
        public static final int common_google_play_services_enable_button = 2131755196;
        public static final int common_google_play_services_enable_text = 2131755197;
        public static final int common_google_play_services_enable_title = 2131755198;
        public static final int common_google_play_services_install_button = 2131755199;
        public static final int common_google_play_services_install_text = 2131755200;
        public static final int common_google_play_services_install_title = 2131755201;
        public static final int common_google_play_services_notification_ticker = 2131755203;
        public static final int common_google_play_services_unknown_issue = 2131755204;
        public static final int common_google_play_services_unsupported_text = 2131755205;
        public static final int common_google_play_services_update_button = 2131755206;
        public static final int common_google_play_services_update_text = 2131755207;
        public static final int common_google_play_services_update_title = 2131755208;
        public static final int common_google_play_services_updating_text = 2131755209;
        public static final int common_google_play_services_wear_update_text = 2131755210;
        public static final int common_open_on_phone = 2131755211;
        public static final int common_signin_button_text = 2131755212;
        public static final int common_signin_button_text_long = 2131755213;
        public static final int status_bar_notification_info_overflow = 2131755551;

        private string() {
        }
    }

    public static final class style {
        public static final int CastExpandedController = 2131820760;
        public static final int CastIntroOverlay = 2131820761;
        public static final int CastMiniController = 2131820762;
        public static final int CustomCastTheme = 2131820767;
        public static final int TextAppearance_CastIntroOverlay_Button = 2131820931;
        public static final int TextAppearance_CastIntroOverlay_Title = 2131820932;
        public static final int TextAppearance_CastMiniController_Subtitle = 2131820933;
        public static final int TextAppearance_CastMiniController_Title = 2131820934;
        public static final int TextAppearance_Compat_Notification = 2131820935;
        public static final int TextAppearance_Compat_Notification_Info = 2131820936;
        public static final int TextAppearance_Compat_Notification_Info_Media = 2131820937;
        public static final int TextAppearance_Compat_Notification_Line2 = 2131820938;
        public static final int TextAppearance_Compat_Notification_Line2_Media = 2131820939;
        public static final int TextAppearance_Compat_Notification_Media = 2131820940;
        public static final int TextAppearance_Compat_Notification_Time = 2131820941;
        public static final int TextAppearance_Compat_Notification_Time_Media = 2131820942;
        public static final int TextAppearance_Compat_Notification_Title = 2131820943;
        public static final int TextAppearance_Compat_Notification_Title_Media = 2131820944;
        public static final int Theme_IAPTheme = 2131821014;
        public static final int Widget_Compat_NotificationActionContainer = 2131821157;
        public static final int Widget_Compat_NotificationActionText = 2131821158;

        private style() {
        }
    }

    public static final class styleable {
        public static final int[] AdsAttrs = {com.yoku.marumovie.R.attr.adSize, com.yoku.marumovie.R.attr.adSizes, com.yoku.marumovie.R.attr.adUnitId};
        public static final int AdsAttrs_adSize = 0;
        public static final int AdsAttrs_adSizes = 1;
        public static final int AdsAttrs_adUnitId = 2;
        public static final int[] CastExpandedController = {com.yoku.marumovie.R.attr.castAdBreakMarkerColor, com.yoku.marumovie.R.attr.castAdInProgressLabelTextAppearance, com.yoku.marumovie.R.attr.castAdInProgressTextColor, com.yoku.marumovie.R.attr.castAdLabelColor, com.yoku.marumovie.R.attr.castAdLabelTextAppearance, com.yoku.marumovie.R.attr.castAdLabelTextColor, com.yoku.marumovie.R.attr.castButtonColor, com.yoku.marumovie.R.attr.castClosedCaptionsButtonDrawable, com.yoku.marumovie.R.attr.castControlButtons, com.yoku.marumovie.R.attr.castExpandedControllerLoadingIndicatorColor, com.yoku.marumovie.R.attr.castForward30ButtonDrawable, com.yoku.marumovie.R.attr.castLiveIndicatorColor, com.yoku.marumovie.R.attr.castMuteToggleButtonDrawable, com.yoku.marumovie.R.attr.castPauseButtonDrawable, com.yoku.marumovie.R.attr.castPlayButtonDrawable, com.yoku.marumovie.R.attr.castRewind30ButtonDrawable, com.yoku.marumovie.R.attr.castSeekBarProgressAndThumbColor, com.yoku.marumovie.R.attr.castSeekBarProgressDrawable, com.yoku.marumovie.R.attr.castSeekBarThumbDrawable, com.yoku.marumovie.R.attr.castSkipNextButtonDrawable, com.yoku.marumovie.R.attr.castSkipPreviousButtonDrawable, com.yoku.marumovie.R.attr.castStopButtonDrawable};
        public static final int CastExpandedController_castAdBreakMarkerColor = 0;
        public static final int CastExpandedController_castAdInProgressLabelTextAppearance = 1;
        public static final int CastExpandedController_castAdInProgressTextColor = 2;
        public static final int CastExpandedController_castAdLabelColor = 3;
        public static final int CastExpandedController_castAdLabelTextAppearance = 4;
        public static final int CastExpandedController_castAdLabelTextColor = 5;
        public static final int CastExpandedController_castButtonColor = 6;
        public static final int CastExpandedController_castClosedCaptionsButtonDrawable = 7;
        public static final int CastExpandedController_castControlButtons = 8;
        public static final int CastExpandedController_castExpandedControllerLoadingIndicatorColor = 9;
        public static final int CastExpandedController_castForward30ButtonDrawable = 10;
        public static final int CastExpandedController_castLiveIndicatorColor = 11;
        public static final int CastExpandedController_castMuteToggleButtonDrawable = 12;
        public static final int CastExpandedController_castPauseButtonDrawable = 13;
        public static final int CastExpandedController_castPlayButtonDrawable = 14;
        public static final int CastExpandedController_castRewind30ButtonDrawable = 15;
        public static final int CastExpandedController_castSeekBarProgressAndThumbColor = 16;
        public static final int CastExpandedController_castSeekBarProgressDrawable = 17;
        public static final int CastExpandedController_castSeekBarThumbDrawable = 18;
        public static final int CastExpandedController_castSkipNextButtonDrawable = 19;
        public static final int CastExpandedController_castSkipPreviousButtonDrawable = 20;
        public static final int CastExpandedController_castStopButtonDrawable = 21;
        public static final int[] CastIntroOverlay = {com.yoku.marumovie.R.attr.castBackgroundColor, com.yoku.marumovie.R.attr.castButtonBackgroundColor, com.yoku.marumovie.R.attr.castButtonText, com.yoku.marumovie.R.attr.castButtonTextAppearance, com.yoku.marumovie.R.attr.castFocusRadius, com.yoku.marumovie.R.attr.castTitleTextAppearance};
        public static final int CastIntroOverlay_castBackgroundColor = 0;
        public static final int CastIntroOverlay_castButtonBackgroundColor = 1;
        public static final int CastIntroOverlay_castButtonText = 2;
        public static final int CastIntroOverlay_castButtonTextAppearance = 3;
        public static final int CastIntroOverlay_castFocusRadius = 4;
        public static final int CastIntroOverlay_castTitleTextAppearance = 5;
        public static final int[] CastMiniController = {com.yoku.marumovie.R.attr.castBackground, com.yoku.marumovie.R.attr.castButtonColor, com.yoku.marumovie.R.attr.castClosedCaptionsButtonDrawable, com.yoku.marumovie.R.attr.castControlButtons, com.yoku.marumovie.R.attr.castForward30ButtonDrawable, com.yoku.marumovie.R.attr.castLargePauseButtonDrawable, com.yoku.marumovie.R.attr.castLargePlayButtonDrawable, com.yoku.marumovie.R.attr.castLargeStopButtonDrawable, com.yoku.marumovie.R.attr.castMiniControllerLoadingIndicatorColor, com.yoku.marumovie.R.attr.castMuteToggleButtonDrawable, com.yoku.marumovie.R.attr.castPauseButtonDrawable, com.yoku.marumovie.R.attr.castPlayButtonDrawable, com.yoku.marumovie.R.attr.castProgressBarColor, com.yoku.marumovie.R.attr.castRewind30ButtonDrawable, com.yoku.marumovie.R.attr.castShowImageThumbnail, com.yoku.marumovie.R.attr.castSkipNextButtonDrawable, com.yoku.marumovie.R.attr.castSkipPreviousButtonDrawable, com.yoku.marumovie.R.attr.castStopButtonDrawable, com.yoku.marumovie.R.attr.castSubtitleTextAppearance, com.yoku.marumovie.R.attr.castTitleTextAppearance};
        public static final int CastMiniController_castBackground = 0;
        public static final int CastMiniController_castButtonColor = 1;
        public static final int CastMiniController_castClosedCaptionsButtonDrawable = 2;
        public static final int CastMiniController_castControlButtons = 3;
        public static final int CastMiniController_castForward30ButtonDrawable = 4;
        public static final int CastMiniController_castLargePauseButtonDrawable = 5;
        public static final int CastMiniController_castLargePlayButtonDrawable = 6;
        public static final int CastMiniController_castLargeStopButtonDrawable = 7;
        public static final int CastMiniController_castMiniControllerLoadingIndicatorColor = 8;
        public static final int CastMiniController_castMuteToggleButtonDrawable = 9;
        public static final int CastMiniController_castPauseButtonDrawable = 10;
        public static final int CastMiniController_castPlayButtonDrawable = 11;
        public static final int CastMiniController_castProgressBarColor = 12;
        public static final int CastMiniController_castRewind30ButtonDrawable = 13;
        public static final int CastMiniController_castShowImageThumbnail = 14;
        public static final int CastMiniController_castSkipNextButtonDrawable = 15;
        public static final int CastMiniController_castSkipPreviousButtonDrawable = 16;
        public static final int CastMiniController_castStopButtonDrawable = 17;
        public static final int CastMiniController_castSubtitleTextAppearance = 18;
        public static final int CastMiniController_castTitleTextAppearance = 19;
        public static final int[] CustomCastTheme = {com.yoku.marumovie.R.attr.castExpandedControllerStyle, com.yoku.marumovie.R.attr.castIntroOverlayStyle, com.yoku.marumovie.R.attr.castMiniControllerStyle};
        public static final int CustomCastTheme_castExpandedControllerStyle = 0;
        public static final int CustomCastTheme_castIntroOverlayStyle = 1;
        public static final int CustomCastTheme_castMiniControllerStyle = 2;
        public static final int[] FontFamily = {com.yoku.marumovie.R.attr.fontProviderAuthority, com.yoku.marumovie.R.attr.fontProviderCerts, com.yoku.marumovie.R.attr.fontProviderFetchStrategy, com.yoku.marumovie.R.attr.fontProviderFetchTimeout, com.yoku.marumovie.R.attr.fontProviderPackage, com.yoku.marumovie.R.attr.fontProviderQuery};
        public static final int[] FontFamilyFont = {16844082, 16844083, 16844095, 16844143, 16844144, com.yoku.marumovie.R.attr.font, com.yoku.marumovie.R.attr.fontStyle, com.yoku.marumovie.R.attr.fontVariationSettings, com.yoku.marumovie.R.attr.fontWeight, com.yoku.marumovie.R.attr.ttcIndex};
        public static final int FontFamilyFont_android_font = 0;
        public static final int FontFamilyFont_android_fontStyle = 2;
        public static final int FontFamilyFont_android_fontVariationSettings = 4;
        public static final int FontFamilyFont_android_fontWeight = 1;
        public static final int FontFamilyFont_android_ttcIndex = 3;
        public static final int FontFamilyFont_font = 5;
        public static final int FontFamilyFont_fontStyle = 6;
        public static final int FontFamilyFont_fontVariationSettings = 7;
        public static final int FontFamilyFont_fontWeight = 8;
        public static final int FontFamilyFont_ttcIndex = 9;
        public static final int FontFamily_fontProviderAuthority = 0;
        public static final int FontFamily_fontProviderCerts = 1;
        public static final int FontFamily_fontProviderFetchStrategy = 2;
        public static final int FontFamily_fontProviderFetchTimeout = 3;
        public static final int FontFamily_fontProviderPackage = 4;
        public static final int FontFamily_fontProviderQuery = 5;
        public static final int[] LoadingImageView = {com.yoku.marumovie.R.attr.circleCrop, com.yoku.marumovie.R.attr.imageAspectRatio, com.yoku.marumovie.R.attr.imageAspectRatioAdjust};
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] RecyclerView = {16842948, 16842993, com.yoku.marumovie.R.attr.fastScrollEnabled, com.yoku.marumovie.R.attr.fastScrollHorizontalThumbDrawable, com.yoku.marumovie.R.attr.fastScrollHorizontalTrackDrawable, com.yoku.marumovie.R.attr.fastScrollVerticalThumbDrawable, com.yoku.marumovie.R.attr.fastScrollVerticalTrackDrawable, com.yoku.marumovie.R.attr.layoutManager, com.yoku.marumovie.R.attr.reverseLayout, com.yoku.marumovie.R.attr.spanCount, com.yoku.marumovie.R.attr.stackFromEnd};
        public static final int RecyclerView_android_descendantFocusability = 1;
        public static final int RecyclerView_android_orientation = 0;
        public static final int RecyclerView_fastScrollEnabled = 2;
        public static final int RecyclerView_fastScrollHorizontalThumbDrawable = 3;
        public static final int RecyclerView_fastScrollHorizontalTrackDrawable = 4;
        public static final int RecyclerView_fastScrollVerticalThumbDrawable = 5;
        public static final int RecyclerView_fastScrollVerticalTrackDrawable = 6;
        public static final int RecyclerView_layoutManager = 7;
        public static final int RecyclerView_reverseLayout = 8;
        public static final int RecyclerView_spanCount = 9;
        public static final int RecyclerView_stackFromEnd = 10;
        public static final int[] SignInButton = {com.yoku.marumovie.R.attr.buttonSize, com.yoku.marumovie.R.attr.colorScheme, com.yoku.marumovie.R.attr.scopeUris};
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;

        private styleable() {
        }
    }

    private R() {
    }
}
