package com.facebook.ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import com.facebook.ads.internal.adapters.k;
import com.facebook.ads.internal.adapters.l;
import com.facebook.ads.internal.q.a.x;
import com.facebook.ads.internal.settings.a;
import com.facebook.ads.internal.view.a;
import com.facebook.ads.internal.view.c.a.f;
import com.facebook.ads.internal.view.e;
import com.facebook.ads.internal.view.f.b.z;
import com.facebook.ads.internal.view.g;
import com.facebook.ads.internal.view.h;
import com.facebook.ads.internal.view.n;
import com.facebook.ads.internal.view.o;
import com.facebook.ads.internal.view.u;
import com.facebook.common.util.ByteConstants;
import com.original.tase.model.socket.UserResponces;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class AudienceNetworkActivity extends Activity {
    @Deprecated
    public static final String AD_ICON_URL = "adIconUrl";
    @Deprecated
    public static final String AD_SUBTITLE = "adSubtitle";
    @Deprecated
    public static final String AD_TITLE = "adTitle";
    public static final String AUDIENCE_NETWORK_UNIQUE_ID_EXTRA = "uniqueId";
    public static final String AUTOPLAY = "autoplay";
    public static final String BROWSER_URL = "browserURL";
    public static final String CLIENT_TOKEN = "clientToken";
    @Deprecated
    public static final String CONTEXT_SWITCH_BEHAVIOR = "contextSwitchBehavior";
    @Deprecated
    public static final String END_CARD_ACTIVATION_COMMAND = "facebookRewardedVideoEndCardActivationCommand";
    @Deprecated
    public static final String END_CARD_MARKUP = "facebookRewardedVideoEndCardMarkup";
    public static final String HANDLER_TIME = "handlerTime";
    public static final String PLACEMENT_ID = "placementId";
    public static final String PREDEFINED_ORIENTATION_KEY = "predefinedOrientationKey";
    public static final String REQUEST_TIME = "requestTime";
    public static final String REWARD_SERVER_URL = "rewardServerURL";
    public static final String SKIP_DELAY_SECONDS_KEY = "skipAfterSeconds";
    public static final String USE_CACHE = "useCache";
    public static final String VIDEO_LOGGER = "videoLogger";
    public static final String VIDEO_MPD = "videoMPD";
    @Deprecated
    public static final String VIDEO_REPORT_URL = "videoReportURL";
    public static final String VIDEO_SEEK_TIME = "videoSeekTime";
    public static final String VIDEO_URL = "videoURL";
    public static final String VIEW_TYPE = "viewType";
    @Deprecated
    public static final String WEBVIEW_ENCODING = "utf-8";
    @Deprecated
    public static final String WEBVIEW_MIME_TYPE = "text/html";

    /* renamed from: a  reason: collision with root package name */
    private final List<BackButtonInterceptor> f2539a = new ArrayList();
    /* access modifiers changed from: private */
    public RelativeLayout b;
    private int c = -1;
    private String d;
    private a.C0025a e;
    private long f;
    private long g;
    private int h;
    private com.facebook.ads.internal.view.a i;
    /* access modifiers changed from: private */
    public com.facebook.ads.internal.view.b.c j;

    /* renamed from: com.facebook.ads.AudienceNetworkActivity$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2540a = new int[a.C0025a.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|(3:15|16|18)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|1|2|3|4|5|6|7|8|9|10|11|12|13|14|15|16|18) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x004b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:15:0x0056 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.facebook.ads.internal.settings.a$a[] r0 = com.facebook.ads.internal.settings.a.C0025a.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2540a = r0
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.FULL_SCREEN_VIDEO     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.INTERSTITIAL_WEB_VIEW     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.BROWSER     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.INTERSTITIAL_OLD_NATIVE_VIDEO     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.INTERSTITIAL_NATIVE_VIDEO     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x0056 }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.INTERSTITIAL_NATIVE_IMAGE     // Catch:{ NoSuchFieldError -> 0x0056 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0056 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0056 }
            L_0x0056:
                int[] r0 = f2540a     // Catch:{ NoSuchFieldError -> 0x0062 }
                com.facebook.ads.internal.settings.a$a r1 = com.facebook.ads.internal.settings.a.C0025a.INTERSTITIAL_NATIVE_CAROUSEL     // Catch:{ NoSuchFieldError -> 0x0062 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0062 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0062 }
            L_0x0062:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.facebook.ads.AudienceNetworkActivity.AnonymousClass1.<clinit>():void");
        }
    }

    public interface BackButtonInterceptor {
        boolean interceptBackButton();
    }

    public enum Type {
        INTERSTITIAL_WEB_VIEW(a.C0025a.INTERSTITIAL_WEB_VIEW),
        INTERSTITIAL_OLD_NATIVE_VIDEO(a.C0025a.INTERSTITIAL_OLD_NATIVE_VIDEO),
        INTERSTITIAL_NATIVE_VIDEO(a.C0025a.INTERSTITIAL_NATIVE_VIDEO),
        INTERSTITIAL_NATIVE_IMAGE(a.C0025a.INTERSTITIAL_NATIVE_IMAGE),
        INTERSTITIAL_NATIVE_CAROUSEL(a.C0025a.INTERSTITIAL_NATIVE_CAROUSEL),
        FULL_SCREEN_VIDEO(a.C0025a.FULL_SCREEN_VIDEO),
        REWARDED_VIDEO(a.C0025a.REWARDED_VIDEO),
        BROWSER(a.C0025a.BROWSER);
        

        /* renamed from: a  reason: collision with root package name */
        a.C0025a f2541a;

        private Type(a.C0025a aVar) {
            this.f2541a = aVar;
        }
    }

    private static class a implements a.C0026a {

        /* renamed from: a  reason: collision with root package name */
        final WeakReference<AudienceNetworkActivity> f2542a;

        private a(AudienceNetworkActivity audienceNetworkActivity) {
            this.f2542a = new WeakReference<>(audienceNetworkActivity);
        }

        /* synthetic */ a(AudienceNetworkActivity audienceNetworkActivity, AnonymousClass1 r2) {
            this(audienceNetworkActivity);
        }

        public void a(View view) {
            if (this.f2542a.get() != null) {
                ((AudienceNetworkActivity) this.f2542a.get()).b.addView(view);
            }
        }

        public void a(String str) {
            if (this.f2542a.get() != null) {
                ((AudienceNetworkActivity) this.f2542a.get()).a(str);
            }
        }

        public void a(String str, com.facebook.ads.internal.j.d dVar) {
            if (this.f2542a.get() != null) {
                ((AudienceNetworkActivity) this.f2542a.get()).a(str, dVar);
            }
        }
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final AudienceNetworkActivity f2543a;
        private final Intent b;
        private final com.facebook.ads.internal.m.c c;

        private b(AudienceNetworkActivity audienceNetworkActivity, Intent intent, com.facebook.ads.internal.m.c cVar) {
            this.f2543a = audienceNetworkActivity;
            this.b = intent;
            this.c = cVar;
        }

        /* synthetic */ b(AudienceNetworkActivity audienceNetworkActivity, Intent intent, com.facebook.ads.internal.m.c cVar, AnonymousClass1 r4) {
            this(audienceNetworkActivity, intent, cVar);
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a a() {
            h hVar = new h(this.f2543a, this.c, i(), h() ? new com.facebook.ads.internal.d.b(this.f2543a) : null);
            a((com.facebook.ads.internal.view.a) hVar);
            return hVar;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a a(RelativeLayout relativeLayout) {
            AudienceNetworkActivity audienceNetworkActivity = this.f2543a;
            u uVar = new u(audienceNetworkActivity, this.c, new a(audienceNetworkActivity, (AnonymousClass1) null));
            uVar.a((View) relativeLayout);
            uVar.a(this.b.getIntExtra("video_time_polling_interval", UserResponces.USER_RESPONCE_SUCCSES));
            return uVar;
        }

        private void a(com.facebook.ads.internal.view.a aVar) {
            aVar.setListener(new a(this.f2543a, (AnonymousClass1) null));
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a b() {
            com.facebook.ads.internal.view.a a2 = k.a(this.b.getStringExtra(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA));
            if (a2 == null) {
                return null;
            }
            a(a2);
            return a2;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a c() {
            AudienceNetworkActivity audienceNetworkActivity = this.f2543a;
            return new com.facebook.ads.internal.view.b(audienceNetworkActivity, this.c, new a(audienceNetworkActivity, (AnonymousClass1) null));
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a d() {
            com.facebook.ads.internal.adapters.a.k kVar = (com.facebook.ads.internal.adapters.a.k) this.b.getSerializableExtra("rewardedVideoAdDataBundle");
            if (kVar.e().j() != null) {
                AudienceNetworkActivity audienceNetworkActivity = this.f2543a;
                return new n(audienceNetworkActivity, this.c, new d(audienceNetworkActivity, (AnonymousClass1) null), kVar);
            }
            AudienceNetworkActivity audienceNetworkActivity2 = this.f2543a;
            return new o(audienceNetworkActivity2, this.c, new com.facebook.ads.internal.view.f.a(audienceNetworkActivity2), new d(this.f2543a, (AnonymousClass1) null), kVar);
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a e() {
            AudienceNetworkActivity audienceNetworkActivity = this.f2543a;
            return new e(audienceNetworkActivity, this.c, new a(audienceNetworkActivity, (AnonymousClass1) null));
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a f() {
            f fVar = new f(this.f2543a, this.c, h() ? new com.facebook.ads.internal.d.b(this.f2543a) : null);
            a((com.facebook.ads.internal.view.a) fVar);
            return fVar;
        }

        /* access modifiers changed from: private */
        public com.facebook.ads.internal.view.a g() {
            g gVar = new g(this.f2543a, i(), this.c);
            a((com.facebook.ads.internal.view.a) gVar);
            return gVar;
        }

        private boolean h() {
            return this.b.getBooleanExtra(AudienceNetworkActivity.USE_CACHE, false);
        }

        private com.facebook.ads.internal.adapters.a.g i() {
            return (com.facebook.ads.internal.adapters.a.g) this.b.getSerializableExtra("ad_data_bundle");
        }
    }

    private class c implements View.OnLongClickListener {
        private c() {
        }

        /* synthetic */ c(AudienceNetworkActivity audienceNetworkActivity, AnonymousClass1 r2) {
            this();
        }

        public boolean onLongClick(View view) {
            if (!(AudienceNetworkActivity.this.j == null || AudienceNetworkActivity.this.b == null)) {
                AudienceNetworkActivity.this.j.setBounds(0, 0, AudienceNetworkActivity.this.b.getWidth(), AudienceNetworkActivity.this.b.getHeight());
                AudienceNetworkActivity.this.j.a(!AudienceNetworkActivity.this.j.a());
            }
            return true;
        }
    }

    private static class d extends a {
        private d(AudienceNetworkActivity audienceNetworkActivity) {
            super(audienceNetworkActivity, (AnonymousClass1) null);
        }

        /* synthetic */ d(AudienceNetworkActivity audienceNetworkActivity, AnonymousClass1 r2) {
            this(audienceNetworkActivity);
        }

        public void a(String str) {
            if (this.f2542a.get() != null) {
                ((AudienceNetworkActivity) this.f2542a.get()).a(str);
                String a2 = z.REWARDED_VIDEO_END_ACTIVITY.a();
                String a3 = z.REWARDED_VIDEO_ERROR.a();
                if (str.equals(a2) || str.equals(a3)) {
                    ((AudienceNetworkActivity) this.f2542a.get()).finish();
                }
            }
        }
    }

    private com.facebook.ads.internal.view.a a() {
        b bVar = new b(this, getIntent(), com.facebook.ads.internal.m.d.a((Context) this), (AnonymousClass1) null);
        a.C0025a aVar = this.e;
        if (aVar == null) {
            return null;
        }
        switch (AnonymousClass1.f2540a[aVar.ordinal()]) {
            case 1:
                return bVar.a(this.b);
            case 2:
                return bVar.d();
            case 3:
                return bVar.e();
            case 4:
                return bVar.c();
            case 5:
                return bVar.b();
            case 6:
                return bVar.a();
            case 7:
                return bVar.g();
            case 8:
                return bVar.f();
            default:
                return null;
        }
    }

    private void a(Intent intent, Bundle bundle) {
        if (bundle != null) {
            this.c = bundle.getInt(PREDEFINED_ORIENTATION_KEY, -1);
            this.d = bundle.getString(AUDIENCE_NETWORK_UNIQUE_ID_EXTRA);
            this.e = (a.C0025a) bundle.getSerializable(VIEW_TYPE);
            return;
        }
        this.c = intent.getIntExtra(PREDEFINED_ORIENTATION_KEY, -1);
        this.d = intent.getStringExtra(AUDIENCE_NETWORK_UNIQUE_ID_EXTRA);
        this.e = (a.C0025a) intent.getSerializableExtra(VIEW_TYPE);
        this.h = intent.getIntExtra(SKIP_DELAY_SECONDS_KEY, 0) * 1000;
    }

    private void a(Intent intent, boolean z) {
        if (com.facebook.ads.internal.l.a.b(this) && this.e != a.C0025a.BROWSER) {
            this.j = new com.facebook.ads.internal.view.b.c();
            this.j.a(intent.getStringExtra("placementId"));
            this.j.b(getPackageName());
            long longExtra = intent.getLongExtra(REQUEST_TIME, 0);
            if (longExtra != 0) {
                this.j.a(longExtra);
            }
            TextView textView = new TextView(this);
            textView.setText("Debug");
            textView.setTextColor(-1);
            x.a((View) textView, Color.argb(160, 0, 0, 0));
            textView.setPadding(5, 5, 5, 5);
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(12, -1);
            layoutParams.addRule(11, -1);
            textView.setLayoutParams(layoutParams);
            c cVar = new c(this, (AnonymousClass1) null);
            textView.setOnLongClickListener(cVar);
            if (z) {
                this.b.addView(textView);
            } else {
                this.b.setOnLongClickListener(cVar);
            }
            this.b.getOverlay().add(this.j);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        LocalBroadcastManager.a((Context) this).a(new Intent(str + ":" + this.d));
    }

    /* access modifiers changed from: private */
    public void a(String str, com.facebook.ads.internal.j.d dVar) {
        Intent intent = new Intent(str + ":" + this.d);
        intent.putExtra("event", dVar);
        LocalBroadcastManager.a((Context) this).a(intent);
    }

    public void addBackButtonInterceptor(BackButtonInterceptor backButtonInterceptor) {
        this.f2539a.add(backButtonInterceptor);
    }

    public void finish() {
        if (!isFinishing()) {
            a(this.e == a.C0025a.REWARDED_VIDEO ? z.REWARDED_VIDEO_CLOSED.a() : "com.facebook.ads.interstitial.dismissed");
            super.finish();
        }
    }

    public void onBackPressed() {
        long currentTimeMillis = System.currentTimeMillis();
        this.g += currentTimeMillis - this.f;
        this.f = currentTimeMillis;
        if (this.g > ((long) this.h)) {
            boolean z = false;
            for (BackButtonInterceptor interceptBackButton : this.f2539a) {
                if (interceptBackButton.interceptBackButton()) {
                    z = true;
                }
            }
            if (!z) {
                super.onBackPressed();
            }
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        com.facebook.ads.internal.view.a aVar = this.i;
        if (aVar instanceof l) {
            ((l) aVar).a(configuration);
        } else if (aVar instanceof o) {
            ((o) aVar).onConfigurationChanged(configuration);
        }
        super.onConfigurationChanged(configuration);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        com.facebook.ads.internal.q.a.d.a();
        boolean z = true;
        requestWindowFeature(1);
        getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
        this.b = new RelativeLayout(this);
        x.a((View) this.b, -16777216);
        setContentView(this.b, new RelativeLayout.LayoutParams(-1, -1));
        Intent intent = getIntent();
        a(intent, bundle);
        this.i = a();
        com.facebook.ads.internal.view.a aVar = this.i;
        if (aVar == null) {
            com.facebook.ads.internal.j.b.a(com.facebook.ads.internal.j.a.a((Throwable) null, "Unable to infer viewType from intent or savedInstanceState"));
            a("com.facebook.ads.interstitial.error");
            finish();
            return;
        }
        aVar.a(intent, bundle, this);
        a("com.facebook.ads.interstitial.displayed");
        this.f = System.currentTimeMillis();
        if (this.e != a.C0025a.INTERSTITIAL_WEB_VIEW) {
            z = false;
        }
        a(intent, z);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        a(this.e == a.C0025a.REWARDED_VIDEO ? z.REWARDED_VIDEO_ACTIVITY_DESTROYED.a() : "com.facebook.ads.interstitial.activity_destroyed");
        RelativeLayout relativeLayout = this.b;
        if (relativeLayout != null) {
            relativeLayout.removeAllViews();
        }
        com.facebook.ads.internal.view.a aVar = this.i;
        if (aVar != null) {
            k.a(aVar);
            this.i.onDestroy();
            this.i = null;
        }
        if (this.j != null && com.facebook.ads.internal.l.a.b(this)) {
            this.j.b();
        }
        super.onDestroy();
    }

    public void onPause() {
        this.g += System.currentTimeMillis() - this.f;
        com.facebook.ads.internal.view.a aVar = this.i;
        if (aVar != null) {
            aVar.i();
        }
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f = System.currentTimeMillis();
        com.facebook.ads.internal.view.a aVar = this.i;
        if (aVar != null) {
            aVar.j();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        com.facebook.ads.internal.view.a aVar = this.i;
        if (aVar != null) {
            aVar.a(bundle);
        }
        bundle.putInt(PREDEFINED_ORIENTATION_KEY, this.c);
        bundle.putString(AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.d);
        bundle.putSerializable(VIEW_TYPE, this.e);
    }

    public void onStart() {
        super.onStart();
        int i2 = this.c;
        if (i2 != -1) {
            setRequestedOrientation(i2);
        }
    }

    public void removeBackButtonInterceptor(BackButtonInterceptor backButtonInterceptor) {
        this.f2539a.remove(backButtonInterceptor);
    }
}
