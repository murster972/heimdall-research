package com.ads.videoreward;

import android.content.Context;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.ads.videoreward.AdsBase;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.original.tase.Logger;
import com.yoku.house.ads.HouseAdsDialog;
import com.yoku.house.ads.HouseAdsInterstitial;
import com.yoku.house.ads.HouseAdsNative;
import com.yoku.house.ads.helper.JsonPullerTask;
import com.yoku.house.ads.helper.cacheImages.PicassoHelper;
import com.yoku.house.ads.listener.AdListener;
import com.yoku.house.ads.listener.NativeAdListener;

public class HouseAds extends AdsBase {
    private HouseAdsInterstitial e;
    private HouseAdsNative f;
    private HouseAdsDialog g;
    private boolean h = false;

    private void c(String str) {
        this.g = new HouseAdsDialog(c(), str);
        this.g.b(true);
        this.g.a(true);
        this.g.a((AdListener) new AdListener(this) {
            public void a(Exception exc) {
                Logger.a("HouseAds", "setupDialog onAdLoadFailed " + exc.getMessage());
            }

            public void b() {
                Logger.a("HouseAds", "setupDialog onAdShown");
            }

            public void onAdClosed() {
                Logger.a("HouseAds", "setupDialog onAdClosed");
            }

            public void onAdLoaded() {
                Logger.a("HouseAds", "setupDialog onAdLoaded");
            }

            public void a() {
                Logger.a("HouseAds", "Intertisial onAdShown");
            }
        });
        this.g.a();
    }

    private void e(String str) {
        this.f = new HouseAdsNative(c(), str);
        this.f.a(true);
        this.f.a((NativeAdListener) new NativeAdListener(this) {
            public void a(Exception exc) {
                Logger.a("HouseAds", "setupNative onAdLoadFailed " + exc.getMessage());
            }

            public void onAdLoaded() {
                Logger.a("HouseAds", "setupNative onAdLoaded");
            }
        });
        this.f.a();
    }

    public void a() {
        super.a();
    }

    public /* synthetic */ void b(String str) {
        if (!str.trim().isEmpty()) {
            d(str);
            c(str);
            e(str);
            this.h = true;
        }
    }

    public void d() {
        super.d();
        PicassoHelper.a((Context) c());
        AppConfig.AdsBean.HouseAdsBean house_ads = GlobalVariable.c().a().getAds().getHouse_ads();
        String config = house_ads.getConfig();
        a(house_ads.getEcmp());
        if (!config.trim().isEmpty()) {
            new JsonPullerTask(config, new a(this)).execute(new String[0]);
            return;
        }
        throw new IllegalArgumentException("Url is Blank!");
    }

    public void j() {
        if (!this.h || !this.e.a()) {
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
            return;
        }
        this.e.c();
        this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
    }

    public void a(ViewGroup viewGroup) {
        if (!this.h || !this.f.a(viewGroup, true)) {
            this.d.a(this, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
        } else {
            this.d.a(this, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
        }
    }

    public void a(FrameLayout frameLayout) {
        if (!this.h || !this.f.a((ViewGroup) frameLayout, false)) {
            this.d.a(this, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
        } else {
            this.d.a(this, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
        }
    }

    public void a(int i, int i2, Intent intent) {
        super.a(i, i2, intent);
    }

    private void d(String str) {
        this.e = new HouseAdsInterstitial(c(), str);
        this.e.a((AdListener) new AdListener(this) {
            public void a(Exception exc) {
                Logger.a("HouseAds", "onAdLoadFailed " + exc.getMessage());
            }

            public void b() {
                Logger.a("HouseAds", "Intertisial onAdShown");
            }

            public void onAdClosed() {
                Logger.a("HouseAds", "Intertisial onAdClosed");
            }

            public void onAdLoaded() {
                Logger.a("HouseAds", "Intertisial onAdLoaded");
            }

            public void a() {
                Logger.a("HouseAds", "Intertisial onApplicationLeft");
            }
        });
        this.e.b();
    }
}
