package com.ads.videoreward;

import com.ads.videoreward.AdsBase;
import com.movie.data.api.GlobalVariable;
import com.original.tase.Logger;
import com.unity3d.ads.IUnityAdsListener;
import com.unity3d.ads.UnityAds;
import com.unity3d.ads.metadata.MediationMetaData;
import com.unity3d.ads.metadata.MetaData;
import com.unity3d.ads.metadata.PlayerMetaData;
import com.unity3d.services.core.misc.Utilities;

public class Unity_Ads extends AdsBase implements IUnityAdsListener {
    private static int h = 1;
    private String e = "";
    /* access modifiers changed from: private */
    public String f;
    /* access modifiers changed from: private */
    public String g;

    public void d() {
        super.d();
        a(GlobalVariable.c().a().getAds().getUnity_ads().getEcmp());
        UnityAds.addListener(this);
        UnityAds.setDebugMode(false);
        MediationMetaData mediationMetaData = new MediationMetaData(c());
        mediationMetaData.setName("mediationPartner");
        mediationMetaData.setVersion("v12345");
        mediationMetaData.commit();
        MetaData metaData = new MetaData(c());
        metaData.set("test.debugOverlayEnabled", false);
        metaData.commit();
        this.e = GlobalVariable.c().a().getAds().getUnity_ads().getGame_id();
        UnityAds.initialize(c(), this.e, false);
    }

    public void j() {
        if (UnityAds.isReady(this.f)) {
            PlayerMetaData playerMetaData = new PlayerMetaData(c());
            playerMetaData.setServerId("rikshot");
            playerMetaData.commit();
            MediationMetaData mediationMetaData = new MediationMetaData(c());
            int i = h;
            h = i + 1;
            mediationMetaData.setOrdinal(i);
            mediationMetaData.commit();
            UnityAds.show(c(), this.f);
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
        } else if (UnityAds.isReady(this.g)) {
            PlayerMetaData playerMetaData2 = new PlayerMetaData(c());
            playerMetaData2.setServerId("rikshot");
            playerMetaData2.commit();
            MediationMetaData mediationMetaData2 = new MediationMetaData(c());
            int i2 = h;
            h = i2 + 1;
            mediationMetaData2.setOrdinal(i2);
            mediationMetaData2.commit();
            UnityAds.show(c(), this.g);
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
        } else {
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
        }
    }

    public void k() {
        if (UnityAds.isReady(this.g)) {
            PlayerMetaData playerMetaData = new PlayerMetaData(c());
            playerMetaData.setServerId("rikshot");
            playerMetaData.commit();
            MediationMetaData mediationMetaData = new MediationMetaData(c());
            int i = h;
            h = i + 1;
            mediationMetaData.setOrdinal(i);
            mediationMetaData.commit();
            UnityAds.show(c(), this.g);
            this.d.a(this, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.SHOWED);
            return;
        }
        this.d.a(this, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.NOT_SHOW);
    }

    public void onUnityAdsError(UnityAds.UnityAdsError unityAdsError, String str) {
        Logger.a("UnityAds", "error" + str);
        Logger.a("UnityAds", "error" + unityAdsError.name());
        Logger.a("UnityAds", "error" + unityAdsError.toString());
    }

    public void onUnityAdsFinish(String str, UnityAds.FinishState finishState) {
        Logger.a("onUnityAdsFinish: " + str + " - " + finishState);
    }

    public void onUnityAdsReady(final String str) {
        Utilities.runOnUiThread(new Runnable() {
            /* JADX WARNING: Can't fix incorrect switch cases order */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r7 = this;
                    java.lang.String r0 = r2
                    int r1 = r0.hashCode()
                    r2 = 5
                    r3 = 4
                    r4 = 3
                    r5 = 2
                    r6 = 1
                    switch(r1) {
                        case -436771443: goto L_0x0041;
                        case 112202875: goto L_0x0037;
                        case 778580237: goto L_0x002d;
                        case 1124615373: goto L_0x0023;
                        case 1716236694: goto L_0x0019;
                        case 1841920601: goto L_0x000f;
                        default: goto L_0x000e;
                    }
                L_0x000e:
                    goto L_0x004b
                L_0x000f:
                    java.lang.String r1 = "rewardedVideoZone"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x004b
                    r0 = 4
                    goto L_0x004c
                L_0x0019:
                    java.lang.String r1 = "incentivizedZone"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x004b
                    r0 = 5
                    goto L_0x004c
                L_0x0023:
                    java.lang.String r1 = "defaultVideoAndPictureZone"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x004b
                    r0 = 2
                    goto L_0x004c
                L_0x002d:
                    java.lang.String r1 = "rewardedVideo"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x004b
                    r0 = 3
                    goto L_0x004c
                L_0x0037:
                    java.lang.String r1 = "video"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x004b
                    r0 = 0
                    goto L_0x004c
                L_0x0041:
                    java.lang.String r1 = "defaultZone"
                    boolean r0 = r0.equals(r1)
                    if (r0 == 0) goto L_0x004b
                    r0 = 1
                    goto L_0x004c
                L_0x004b:
                    r0 = -1
                L_0x004c:
                    if (r0 == 0) goto L_0x0061
                    if (r0 == r6) goto L_0x0061
                    if (r0 == r5) goto L_0x0061
                    if (r0 == r4) goto L_0x0059
                    if (r0 == r3) goto L_0x0059
                    if (r0 == r2) goto L_0x0059
                    goto L_0x0068
                L_0x0059:
                    com.ads.videoreward.Unity_Ads r0 = com.ads.videoreward.Unity_Ads.this
                    java.lang.String r1 = r2
                    java.lang.String unused = r0.g = r1
                    goto L_0x0068
                L_0x0061:
                    com.ads.videoreward.Unity_Ads r0 = com.ads.videoreward.Unity_Ads.this
                    java.lang.String r1 = r2
                    java.lang.String unused = r0.f = r1
                L_0x0068:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.ads.videoreward.Unity_Ads.AnonymousClass1.run():void");
            }
        });
    }

    public void onUnityAdsStart(String str) {
        Logger.a("onUnityAdsStart: " + str);
    }
}
