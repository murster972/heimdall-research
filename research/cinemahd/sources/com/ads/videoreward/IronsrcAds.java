package com.ads.videoreward;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.ads.videoreward.AdsBase;
import com.ironsource.mediationsdk.ISBannerSize;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.sdk.BannerListener;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;

public class IronsrcAds extends AdsBase {
    IronSourceBannerLayout e;

    public void a(final ViewGroup viewGroup) {
        c();
        IronSourceBannerLayout ironSourceBannerLayout = this.e;
        if (ironSourceBannerLayout == null) {
            this.e = IronSource.a(c(), ISBannerSize.d);
            this.e.setBannerListener(new BannerListener() {
                public void a() {
                }

                public void a(IronSourceError ironSourceError) {
                    IronsrcAds.this.c().runOnUiThread(new Runnable() {
                        public void run() {
                            viewGroup.removeAllViews();
                            IronsrcAds ironsrcAds = IronsrcAds.this;
                            ironsrcAds.d.a(ironsrcAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
                        }
                    });
                }

                public void b() {
                    IronsrcAds ironsrcAds = IronsrcAds.this;
                    ironsrcAds.d.a(ironsrcAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
                }
            });
            IronSource.b(this.e);
        } else {
            ((ViewGroup) ironSourceBannerLayout.getParent()).removeAllViews();
        }
        viewGroup.addView(this.e, 0, new FrameLayout.LayoutParams(-1, -2));
    }

    public void d() {
        super.d();
        AppConfig.AdsBean.IronSrcBean ironsrc = GlobalVariable.c().a().getAds().getIronsrc();
        String appkey = ironsrc.getAppkey();
        a(ironsrc.getEcmp());
        IronSource.a(c(), appkey, IronSource.AD_UNIT.INTERSTITIAL);
        IronSource.a(c(), appkey, IronSource.AD_UNIT.BANNER);
        IronSource.b();
        IronSource.a((InterstitialListener) new InterstitialListener() {
            public void a() {
            }

            public void a(IronSourceError ironSourceError) {
            }

            public void b() {
                IronSource.b();
            }

            public void c() {
            }

            public void d() {
                IronsrcAds.this.c().runOnUiThread(new Runnable() {
                    public void run() {
                        IronsrcAds ironsrcAds = IronsrcAds.this;
                        ironsrcAds.d.a(ironsrcAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
                    }
                });
            }

            public void onInterstitialAdClicked() {
            }

            public void b(IronSourceError ironSourceError) {
                IronsrcAds.this.c().runOnUiThread(new Runnable() {
                    public void run() {
                        IronsrcAds ironsrcAds = IronsrcAds.this;
                        ironsrcAds.d.a(ironsrcAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
                    }
                });
            }
        });
    }

    public void e() {
        super.e();
        IronSource.a(this.e);
        this.e = null;
    }

    public void f() {
        super.f();
        IronSource.a(c());
    }

    public void g() {
        super.g();
        IronSource.b(c());
    }

    public void j() {
        c();
        if (IronSource.a()) {
            IronSource.c();
        } else {
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
        }
    }
}
