package com.ads.videoreward;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import com.ads.videoreward.AdsBase;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.VideoController;
import com.google.android.gms.ads.VideoOptions;
import com.google.android.gms.ads.formats.MediaView;
import com.google.android.gms.ads.formats.NativeAdOptions;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.original.tase.Logger;
import com.yoku.marumovie.R;

public class Admob extends AdsBase {
    /* access modifiers changed from: private */
    public InterstitialAd e;
    private boolean f = false;

    private void l() {
        if (!this.f) {
            AppConfig.AdsBean.AdmobBean admob = GlobalVariable.c().a().getAds().getAdmob();
            if (admob != null) {
                MobileAds.initialize((Context) c(), admob.getApp_id());
            } else {
                MobileAds.initialize((Context) c(), c().getString(R.string.ad_app_id));
            }
            this.f = true;
        }
    }

    private void m() {
        this.e = new InterstitialAd(c());
        this.e.setAdUnitId(GlobalVariable.c().a().getAds().getAdmob().getInterstitial());
        this.e.loadAd(new AdRequest.Builder().build());
        this.e.setAdListener(new AdListener() {
            public void onAdClosed() {
                super.onAdClosed();
                Admob.this.e.loadAd(new AdRequest.Builder().build());
            }

            public void onAdFailedToLoad(int i) {
                super.onAdFailedToLoad(i);
                Logger.a("requestIntertialAds", "admobIntertisial loaded faild");
                FreeMoviesApp.l().edit().putInt("pref_alf_count", FreeMoviesApp.l().getInt("pref_alf_count", 10) + 1).apply();
            }

            public void onAdLoaded() {
                super.onAdLoaded();
                Logger.a("requestIntertialAds", "admobIntertisial loaded");
                FreeMoviesApp.l().edit().putInt("pref_alf_count", FreeMoviesApp.l().getInt("pref_alf_count", 10) - 10).apply();
            }
        });
    }

    public void d() {
        super.d();
        l();
        m();
        a(GlobalVariable.c().a().getAds().getAdmob().getEcmp());
    }

    public void e() {
        super.e();
    }

    public void f() {
        InterstitialAd interstitialAd = this.e;
        if (interstitialAd != null) {
            interstitialAd.setAdListener((AdListener) null);
        }
        super.f();
    }

    public void j() {
        InterstitialAd interstitialAd = this.e;
        if (interstitialAd == null || !interstitialAd.isLoaded()) {
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
            return;
        }
        this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
        this.e.show();
    }

    public void a() {
        try {
            d();
            this.d.a(this, true);
        } catch (Exception unused) {
            this.d.a(this, false);
        }
    }

    public void a(ViewGroup viewGroup) {
        AppConfig.AdsBean.AdmobBean admob = GlobalVariable.c().a().getAds().getAdmob();
        AdView adView = new AdView(c());
        if (c().getResources().getConfiguration().orientation == 1) {
            adView.setAdSize(AdSize.SMART_BANNER);
        } else {
            adView.setAdSize(AdSize.BANNER);
        }
        adView.setAdUnitId(admob.getBanner());
        adView.loadAd(new AdRequest.Builder().build());
        adView.setAdListener(new AdListener() {
            public void onAdFailedToLoad(int i) {
                Admob admob = Admob.this;
                admob.d.a(admob, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
            }

            public void onAdLoaded() {
                Admob admob = Admob.this;
                admob.d.a(admob, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
                super.onAdLoaded();
            }
        });
        viewGroup.addView(adView);
    }

    public void a(final FrameLayout frameLayout) {
        AdLoader.Builder builder = new AdLoader.Builder((Context) c(), GlobalVariable.c().a().getAds().getAdmob().getNativeAdvance());
        builder.forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
            public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                Admob admob = Admob.this;
                admob.d.a(admob, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.SHOWED);
                UnifiedNativeAdView unifiedNativeAdView = (UnifiedNativeAdView) Admob.this.c().getLayoutInflater().inflate(R.layout.ad_unifield, (ViewGroup) null);
                Admob.this.a(unifiedNativeAd, unifiedNativeAdView);
                frameLayout.removeAllViews();
                frameLayout.addView(unifiedNativeAdView);
            }
        });
        builder.withNativeAdOptions(new NativeAdOptions.Builder().setVideoOptions(new VideoOptions.Builder().setStartMuted(true).build()).build());
        builder.withAdListener(new AdListener() {
            public void onAdFailedToLoad(int i) {
                FreeMoviesApp.l().edit().putInt("pref_alf_count", FreeMoviesApp.l().getInt("pref_alf_count", 10) + 1).apply();
                Admob admob = Admob.this;
                admob.d.a(admob, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.NOT_SHOW);
            }

            public void onAdLoaded() {
                FreeMoviesApp.l().edit().putInt("pref_alf_count", FreeMoviesApp.l().getInt("pref_alf_count", 10) - 10).apply();
            }
        }).build().loadAd(new AdRequest.Builder().build());
    }

    /* access modifiers changed from: private */
    public void a(UnifiedNativeAd unifiedNativeAd, UnifiedNativeAdView unifiedNativeAdView) {
        unifiedNativeAdView.setMediaView((MediaView) unifiedNativeAdView.findViewById(R.id.ad_media));
        unifiedNativeAdView.setHeadlineView(unifiedNativeAdView.findViewById(R.id.ad_headline));
        unifiedNativeAdView.setBodyView(unifiedNativeAdView.findViewById(R.id.ad_body));
        unifiedNativeAdView.setCallToActionView(unifiedNativeAdView.findViewById(R.id.ad_call_to_action));
        unifiedNativeAdView.setIconView(unifiedNativeAdView.findViewById(R.id.ad_app_icon));
        unifiedNativeAdView.setPriceView(unifiedNativeAdView.findViewById(R.id.ad_price));
        unifiedNativeAdView.setStarRatingView(unifiedNativeAdView.findViewById(R.id.ad_stars));
        unifiedNativeAdView.setStoreView(unifiedNativeAdView.findViewById(R.id.ad_store));
        unifiedNativeAdView.setAdvertiserView(unifiedNativeAdView.findViewById(R.id.ad_advertiser));
        ((TextView) unifiedNativeAdView.getHeadlineView()).setText(unifiedNativeAd.getHeadline());
        if (unifiedNativeAd.getBody() == null) {
            unifiedNativeAdView.getBodyView().setVisibility(4);
        } else {
            unifiedNativeAdView.getBodyView().setVisibility(0);
            ((TextView) unifiedNativeAdView.getBodyView()).setText(unifiedNativeAd.getBody());
        }
        if (unifiedNativeAd.getCallToAction() == null) {
            unifiedNativeAdView.getCallToActionView().setVisibility(4);
        } else {
            unifiedNativeAdView.getCallToActionView().setVisibility(0);
            ((Button) unifiedNativeAdView.getCallToActionView()).setText(unifiedNativeAd.getCallToAction());
        }
        if (unifiedNativeAd.getIcon() == null) {
            unifiedNativeAdView.getIconView().setVisibility(8);
        } else {
            ((ImageView) unifiedNativeAdView.getIconView()).setImageDrawable(unifiedNativeAd.getIcon().getDrawable());
            unifiedNativeAdView.getIconView().setVisibility(0);
        }
        if (unifiedNativeAd.getPrice() == null) {
            unifiedNativeAdView.getPriceView().setVisibility(8);
        } else {
            unifiedNativeAdView.getPriceView().setVisibility(0);
            ((TextView) unifiedNativeAdView.getPriceView()).setText(unifiedNativeAd.getPrice());
        }
        if (unifiedNativeAd.getStore() == null) {
            unifiedNativeAdView.getStoreView().setVisibility(8);
        } else {
            unifiedNativeAdView.getStoreView().setVisibility(0);
            ((TextView) unifiedNativeAdView.getStoreView()).setText(unifiedNativeAd.getStore());
        }
        if (unifiedNativeAd.getStarRating() == null) {
            unifiedNativeAdView.getStarRatingView().setVisibility(4);
        } else {
            ((RatingBar) unifiedNativeAdView.getStarRatingView()).setRating(unifiedNativeAd.getStarRating().floatValue());
            unifiedNativeAdView.getStarRatingView().setVisibility(0);
        }
        if (unifiedNativeAd.getAdvertiser() == null) {
            unifiedNativeAdView.getAdvertiserView().setVisibility(4);
        } else {
            ((TextView) unifiedNativeAdView.getAdvertiserView()).setText(unifiedNativeAd.getAdvertiser());
            unifiedNativeAdView.getAdvertiserView().setVisibility(0);
        }
        unifiedNativeAdView.setNativeAd(unifiedNativeAd);
        VideoController videoController = unifiedNativeAd.getVideoController();
        if (videoController.hasVideoContent()) {
            videoController.setVideoLifecycleCallbacks(new VideoController.VideoLifecycleCallbacks(this) {
                public void onVideoEnd() {
                    super.onVideoEnd();
                }
            });
        }
    }
}
