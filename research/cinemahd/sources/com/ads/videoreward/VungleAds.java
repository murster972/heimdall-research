package com.ads.videoreward;

import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.ads.videoreward.AdsBase;
import com.facebook.common.statfs.StatFsHelper;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.original.tase.Logger;
import com.original.tase.utils.DeviceUtils;
import com.vungle.warren.AdConfig;
import com.vungle.warren.Banners;
import com.vungle.warren.InitCallback;
import com.vungle.warren.LoadAdCallback;
import com.vungle.warren.PlayAdCallback;
import com.vungle.warren.Vungle;
import com.vungle.warren.VungleBanner;
import com.vungle.warren.VungleNativeAd;
import com.vungle.warren.error.VungleException;

public class VungleAds extends AdsBase {
    AppConfig.AdsBean.VungleBean e = null;

    public void a(FrameLayout frameLayout) {
        String placement_ref_native_id = this.e.getPlacement_ref_native_id();
        if (!Vungle.isInitialized() || placement_ref_native_id == null) {
            this.d.a(this, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.NOT_SHOW);
            return;
        }
        VungleNativeAd nativeAd = Vungle.getNativeAd(placement_ref_native_id, new AdConfig(), new PlayAdCallback() {
            public void onAdEnd(String str, boolean z, boolean z2) {
            }

            public void onAdStart(String str) {
                VungleAds vungleAds = VungleAds.this;
                vungleAds.d.a(vungleAds, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.SHOWED);
            }

            public void onError(String str, VungleException vungleException) {
                VungleAds vungleAds = VungleAds.this;
                vungleAds.d.a(vungleAds, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.NOT_SHOW);
            }
        });
        if (nativeAd != null) {
            View renderNativeView = nativeAd.renderNativeView();
            renderNativeView.setLayoutParams(new FrameLayout.LayoutParams(-1, StatFsHelper.DEFAULT_DISK_YELLOW_LEVEL_IN_MB));
            if (frameLayout != null && renderNativeView != null) {
                frameLayout.addView(renderNativeView);
            }
        }
    }

    public void d() {
        super.d();
        if (DeviceUtils.b()) {
            this.e = GlobalVariable.c().a().getAds().getVungle_amz();
        } else {
            this.e = GlobalVariable.c().a().getAds().getVungle();
        }
        a(this.e.getEcmp());
        Vungle.init(this.e.getApp_id(), c().getApplicationContext(), new InitCallback() {
            public void onAutoCacheAdAvailable(String str) {
                Logger.a("VungleSampleApp", "InitCallback - onAutoCacheAdAvailable\n\tPlacement Reference ID = " + str);
            }

            public void onError(VungleException vungleException) {
                Logger.a("VungleSampleApp", "InitCallback - onError: " + vungleException.getLocalizedMessage());
            }

            public void onSuccess() {
                Logger.a("VungleSampleApp", "InitCallback - onSuccess");
                String placement_interstitial = VungleAds.this.e.getPlacement_interstitial();
                if (placement_interstitial != null) {
                    Vungle.loadAd(placement_interstitial, new LoadAdCallback(this) {
                        public void onAdLoad(String str) {
                            Logger.a("VungleSampleApp", "load placeInterstitial - onSuccess - " + str);
                        }

                        public void onError(String str, VungleException vungleException) {
                            Logger.a("VungleSampleApp", "LoadAdCallback - onError\n\tPlacement Reference ID = " + str + "\n\tError = " + vungleException.getLocalizedMessage());
                        }
                    });
                }
            }
        });
    }

    public void j() {
        String placement_interstitial = this.e.getPlacement_interstitial();
        if (!Vungle.isInitialized() || placement_interstitial == null || !Vungle.canPlayAd(placement_interstitial)) {
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
            return;
        }
        AdConfig adConfig = new AdConfig();
        adConfig.setMuted(GlobalVariable.c().a().getAds().isMute());
        Vungle.playAd(placement_interstitial, adConfig, new PlayAdCallback() {
            public void onAdEnd(String str, boolean z, boolean z2) {
                if (z) {
                    VungleAds vungleAds = VungleAds.this;
                    vungleAds.d.a(vungleAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.COMPLETED);
                }
                if (z2) {
                    VungleAds vungleAds2 = VungleAds.this;
                    vungleAds2.d.a(vungleAds2, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.CLICKED);
                }
                Logger.a("Vungle", "showInterstisialonAdEnd");
            }

            public void onAdStart(String str) {
                VungleAds vungleAds = VungleAds.this;
                vungleAds.d.a(vungleAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
            }

            public void onError(String str, VungleException vungleException) {
                VungleAds vungleAds = VungleAds.this;
                vungleAds.d.a(vungleAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
            }
        });
    }

    public void k() {
        final String placement_ref_id = this.e.getPlacement_ref_id();
        if (Vungle.isInitialized()) {
            Vungle.loadAd(placement_ref_id, new LoadAdCallback() {
                public void onAdLoad(String str) {
                    Vungle.playAd(placement_ref_id, (AdConfig) null, new PlayAdCallback() {
                        public void onAdEnd(String str, boolean z, boolean z2) {
                            Logger.a("Vungle", "showVideoAdEnd");
                        }

                        public void onAdStart(String str) {
                            VungleAds vungleAds = VungleAds.this;
                            vungleAds.d.a(vungleAds, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.SHOWED);
                        }

                        public void onError(String str, VungleException vungleException) {
                            VungleAds vungleAds = VungleAds.this;
                            vungleAds.d.a(vungleAds, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.NOT_SHOW);
                        }
                    });
                    VungleAds.this.getClass();
                    Logger.a("VungleSampleApp", "LoadAdCallback - onAdLoad\n\tPlacement Reference ID = " + str);
                }

                public void onError(String str, VungleException vungleException) {
                    Logger.a("VungleSampleApp", "LoadAdCallback - onError\n\tPlacement Reference ID = " + str + "\n\tError = " + vungleException.getLocalizedMessage());
                    VungleAds vungleAds = VungleAds.this;
                    vungleAds.d.a(vungleAds, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.NOT_SHOW);
                }
            });
        } else {
            this.d.a(this, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.NOT_SHOW);
        }
    }

    public void a(final ViewGroup viewGroup) {
        final String placement_ref_native_id = this.e.getPlacement_ref_native_id();
        if (!Vungle.isInitialized() || placement_ref_native_id == null) {
            this.d.a(this, AdsBase.AdBaseType.NATIVE, AdsBase.AdsStatus.NOT_SHOW);
        } else {
            Banners.loadBanner(placement_ref_native_id, AdConfig.AdSize.BANNER, new LoadAdCallback() {
                public void onAdLoad(String str) {
                    Logger.a("VungleSampleApp", "onAdLoadBanner - onSuccess");
                    if (Banners.canPlayAd(placement_ref_native_id, AdConfig.AdSize.BANNER)) {
                        VungleBanner banner = Banners.getBanner(placement_ref_native_id, AdConfig.AdSize.BANNER, new PlayAdCallback() {
                            public void onAdEnd(String str, boolean z, boolean z2) {
                            }

                            public void onAdStart(String str) {
                                VungleAds vungleAds = VungleAds.this;
                                vungleAds.d.a(vungleAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
                            }

                            public void onError(String str, VungleException vungleException) {
                                VungleAds vungleAds = VungleAds.this;
                                vungleAds.d.a(vungleAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
                            }
                        });
                        ViewGroup viewGroup = viewGroup;
                        if (viewGroup != null && banner != null) {
                            viewGroup.addView(banner);
                        }
                    }
                }

                public void onError(String str, VungleException vungleException) {
                    Logger.a("VungleSampleApp", "onAdLoadBanner - onError" + str + " - " + vungleException.getLocalizedMessage());
                    VungleAds vungleAds = VungleAds.this;
                    vungleAds.d.a(vungleAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
                }
            });
        }
    }
}
