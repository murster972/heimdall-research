package com.ads.videoreward;

import android.util.Log;
import android.view.ViewGroup;
import com.adcolony.sdk.AdColony;
import com.adcolony.sdk.AdColonyAdOptions;
import com.adcolony.sdk.AdColonyAdSize;
import com.adcolony.sdk.AdColonyAdView;
import com.adcolony.sdk.AdColonyAdViewListener;
import com.adcolony.sdk.AdColonyAppOptions;
import com.adcolony.sdk.AdColonyInterstitial;
import com.adcolony.sdk.AdColonyInterstitialListener;
import com.adcolony.sdk.AdColonyReward;
import com.adcolony.sdk.AdColonyRewardListener;
import com.adcolony.sdk.AdColonyZone;
import com.ads.videoreward.AdsBase;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.original.tase.utils.DeviceUtils;
import com.utils.Utils;

public class AcolonyAds extends AdsBase {
    private String e = "";
    /* access modifiers changed from: private */
    public String f = "";
    /* access modifiers changed from: private */
    public AdColonyInterstitial g;
    private AdColonyInterstitialListener h;
    private AdColonyAdViewListener i;
    /* access modifiers changed from: private */
    public AdColonyAdView j;
    /* access modifiers changed from: private */
    public AdColonyAdOptions k;
    private AppConfig.AdsBean.AdcolonyBean l = null;

    public void d() {
        super.d();
        if (DeviceUtils.b()) {
            this.l = GlobalVariable.c().a().getAds().getAdcolony_amz();
        } else {
            this.l = GlobalVariable.c().a().getAds().getAdcolony();
        }
        AdColonyAppOptions a2 = new AdColonyAppOptions().c(Utils.f()).a(true);
        this.e = this.l.getApp_id();
        this.f = this.l.getInterstitial_id();
        a(this.l.getEcmp());
        AdColony.a(c(), a2, this.e, this.f);
        this.k = new AdColonyAdOptions().a(true).b(true);
        AdColony.a((AdColonyRewardListener) new AdColonyRewardListener() {
            public void a(AdColonyReward adColonyReward) {
                Log.d("AdColonyDemo", "onReward");
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.SHOWED);
            }
        });
        this.h = new AdColonyInterstitialListener() {
            public void a(AdColonyZone adColonyZone) {
                Log.d("AdColonyDemo", "onRequestNotFilled");
            }

            public void c(AdColonyInterstitial adColonyInterstitial) {
                super.c(adColonyInterstitial);
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.CLICKED);
            }

            public void d(AdColonyInterstitial adColonyInterstitial) {
                super.d(adColonyInterstitial);
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
            }

            public void e(AdColonyInterstitial adColonyInterstitial) {
                AdColony.a(AcolonyAds.this.f, this, AcolonyAds.this.k);
            }

            public void g(AdColonyInterstitial adColonyInterstitial) {
                Log.d("AdColonyDemo", "onOpened");
            }

            public void h(AdColonyInterstitial adColonyInterstitial) {
                AdColonyInterstitial unused = AcolonyAds.this.g = adColonyInterstitial;
                Log.d("AdColonyDemo", "onRequestFilled");
            }
        };
    }

    public void g() {
        super.g();
        AdColonyInterstitial adColonyInterstitial = this.g;
        if (adColonyInterstitial == null || adColonyInterstitial.j()) {
            AdColony.a(this.f, this.h, this.k);
        }
    }

    public void j() {
        AdColonyInterstitial adColonyInterstitial = this.g;
        if (adColonyInterstitial == null || adColonyInterstitial.j()) {
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
            return;
        }
        this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
        this.g.l();
    }

    public void k() {
        AdColonyInterstitial adColonyInterstitial = this.g;
        if (adColonyInterstitial == null || adColonyInterstitial.j()) {
            this.d.a(this, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.NOT_SHOW);
        } else {
            this.g.l();
        }
    }

    public void a(final ViewGroup viewGroup) {
        AdColonyAdOptions adColonyAdOptions = new AdColonyAdOptions();
        this.i = new AdColonyAdViewListener() {
            public void a(AdColonyZone adColonyZone) {
                super.a(adColonyZone);
                Log.d("AdColonyDemo", "onRequestNotFilled");
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
            }

            public void b(AdColonyAdView adColonyAdView) {
                super.b(adColonyAdView);
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
                Log.d("AdColonyDemo", "onClosed");
            }

            public void c(AdColonyAdView adColonyAdView) {
                super.c(adColonyAdView);
                Log.d("AdColonyDemo", "onLeftApplication");
            }

            public void d(AdColonyAdView adColonyAdView) {
                super.d(adColonyAdView);
                Log.d("AdColonyDemo", "onOpened");
            }

            public void e(AdColonyAdView adColonyAdView) {
                AdColonyAdView unused = AcolonyAds.this.j = adColonyAdView;
                viewGroup.addView(AcolonyAds.this.j);
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
            }

            public void a(AdColonyAdView adColonyAdView) {
                super.a(adColonyAdView);
                Log.d("AdColonyDemo", "onClicked");
                AcolonyAds acolonyAds = AcolonyAds.this;
                acolonyAds.d.a(acolonyAds, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.CLICKED);
            }
        };
        AdColony.a("vzad8e90f278884c8a9b", this.i, AdColonyAdSize.c, adColonyAdOptions);
    }
}
