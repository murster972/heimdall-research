package com.ads.videoreward;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.original.tase.Logger;

public abstract class AdsBase implements Comparable<AdsBase> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public boolean f1385a;
    private InitTask b = null;
    private String c = "0";
    protected AdBaseListener d = null;

    public interface AdBaseListener {
        void a(AdsBase adsBase, Boolean bool);

        boolean a(AdsBase adsBase);

        boolean a(AdsBase adsBase, AdBaseType adBaseType, AdsStatus adsStatus);
    }

    public enum AdBaseType {
        BANNER,
        INTERTISIAL,
        NATIVE,
        VIDEO,
        RETURN
    }

    public enum AdsStatus {
        NOT_SHOW,
        SHOWED,
        COMPLETED,
        CLICKED
    }

    public enum Tag {
        ADMOB,
        FACEBOOK,
        ADCOLONY,
        VUNGLE,
        UNITY,
        CHARTBOOST,
        HOUSEADS,
        COUNT
    }

    public void a(int i, int i2, Intent intent) {
    }

    public void b() {
        InitTask initTask = this.b;
        if (initTask != null && !initTask.isCancelled()) {
            this.b.cancel(true);
        }
    }

    /* access modifiers changed from: protected */
    public Activity c() {
        return AdsManager.l().b();
    }

    public void d() {
    }

    public void e() {
    }

    public void f() {
    }

    public void g() {
    }

    public void h() {
    }

    public void i() {
    }

    public void j() {
        this.d.a(this, AdBaseType.INTERTISIAL, AdsStatus.NOT_SHOW);
    }

    public void k() {
        this.d.a(this, AdBaseType.VIDEO, AdsStatus.NOT_SHOW);
    }

    public void a() {
        InitTask initTask = this.b;
        if (initTask == null || initTask.isCancelled()) {
            this.b = new InitTask();
            this.b.execute(new AdsBase[]{this});
        }
    }

    private class InitTask extends AsyncTask<AdsBase, Integer, Boolean> {
        private InitTask() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(AdsBase... adsBaseArr) {
            try {
                adsBaseArr[0].d();
                Logger.a("InitTask", adsBaseArr[0].getClass().getSimpleName());
                return true;
            } catch (Exception e) {
                Logger.a("InitTask", adsBaseArr[0].getClass().getSimpleName() + " : " + e.getMessage());
                return false;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            boolean unused = AdsBase.this.f1385a = bool.booleanValue();
            AdsBase adsBase = AdsBase.this;
            adsBase.d.a(adsBase, bool);
        }
    }

    public void a(ViewGroup viewGroup) {
        this.d.a(this, AdBaseType.BANNER, AdsStatus.NOT_SHOW);
    }

    public void a(FrameLayout frameLayout) {
        this.d.a(this, AdBaseType.NATIVE, AdsStatus.NOT_SHOW);
    }

    public void a(String str) {
        this.c = str;
    }

    /* renamed from: a */
    public int compareTo(AdsBase adsBase) {
        if (Float.valueOf(this.c) == Float.valueOf(adsBase.c)) {
            return 0;
        }
        return Float.valueOf(this.c).floatValue() > Float.valueOf(adsBase.c).floatValue() ? -1 : 1;
    }

    public void a(AdBaseListener adBaseListener) {
        this.d = adBaseListener;
    }
}
