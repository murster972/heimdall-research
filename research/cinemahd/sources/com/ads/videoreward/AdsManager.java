package com.ads.videoreward;

import android.app.Activity;
import android.content.Intent;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.ads.videoreward.AdsBase;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.original.tase.Logger;
import com.original.tase.utils.DeviceUtils;
import com.utils.Utils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AdsManager implements AdsBase.AdBaseListener {
    private static AdsManager m;

    /* renamed from: a  reason: collision with root package name */
    private boolean f1390a = false;
    private Activity b;
    private List<AdsBase> c = new ArrayList();
    private List<AdsBase> d = new ArrayList();
    private boolean e = false;
    long f = 0;
    private int g = 0;
    private int h = 0;
    private ViewGroup i = null;
    private int j = 0;
    private int k = 0;
    private FrameLayout l = null;

    /* renamed from: com.ads.videoreward.AdsManager$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f1391a = new int[AdsBase.AdBaseType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.ads.videoreward.AdsBase$AdBaseType[] r0 = com.ads.videoreward.AdsBase.AdBaseType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f1391a = r0
                int[] r0 = f1391a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ads.videoreward.AdsBase$AdBaseType r1 = com.ads.videoreward.AdsBase.AdBaseType.VIDEO     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f1391a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ads.videoreward.AdsBase$AdBaseType r1 = com.ads.videoreward.AdsBase.AdBaseType.BANNER     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f1391a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ads.videoreward.AdsBase$AdBaseType r1 = com.ads.videoreward.AdsBase.AdBaseType.INTERTISIAL     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f1391a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.ads.videoreward.AdsBase$AdBaseType r1 = com.ads.videoreward.AdsBase.AdBaseType.NATIVE     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f1391a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.ads.videoreward.AdsBase$AdBaseType r1 = com.ads.videoreward.AdsBase.AdBaseType.RETURN     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ads.videoreward.AdsManager.AnonymousClass1.<clinit>():void");
        }
    }

    public static AdsManager l() {
        if (m == null) {
            m = new AdsManager();
        }
        return m;
    }

    public void a(Activity activity) {
        boolean c2 = Utils.c();
        if (GlobalVariable.c().a().getAds() != null) {
            this.b = activity;
            this.e = FreeMoviesApp.o();
            if (!this.f1390a) {
                this.d.clear();
                b(c2);
                a(c2);
                for (AdsBase next : this.d) {
                    next.a((AdsBase.AdBaseListener) this);
                    next.a();
                }
                this.f1390a = true;
                return;
            }
            return;
        }
        this.f1390a = false;
    }

    public boolean a(AdsBase adsBase) {
        return false;
    }

    public Activity b() {
        return this.b;
    }

    public List<AdsBase> c() {
        return this.c;
    }

    public void d() {
        ViewGroup viewGroup = this.i;
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
        FrameLayout frameLayout = this.l;
        if (frameLayout != null) {
            frameLayout.removeAllViews();
        }
        this.l = null;
        this.i = null;
        for (AdsBase e2 : this.c) {
            e2.e();
        }
    }

    public void e() {
        for (AdsBase f2 : this.c) {
            f2.f();
        }
    }

    public void f() {
        for (AdsBase g2 : this.c) {
            g2.g();
        }
    }

    public void g() {
        for (AdsBase h2 : this.c) {
            h2.h();
        }
    }

    public void h() {
        for (AdsBase i2 : this.c) {
            i2.i();
        }
    }

    public void i() {
        if (this.c.size() != 0) {
            AdsBase adsBase = this.c.get(this.g);
            adsBase.j();
            Logger.a("AdsManager", "show INTERTISIAL " + adsBase.getClass().getName());
        }
    }

    public void j() {
        long currentTimeMillis = System.currentTimeMillis() - this.f;
        if (this.c.size() != 0 && currentTimeMillis >= GlobalVariable.c().a().getAds().getLimitAdsTime()) {
            AdsBase adsBase = this.c.get(this.g);
            adsBase.j();
            Logger.a("AdsManager", "show INTERTISIAL " + adsBase.getClass().getName());
        }
    }

    public void k() {
        if (this.c.size() > 0) {
            AdsBase adsBase = this.c.get(this.j);
            adsBase.k();
            Logger.a("AdsManager", "show VIDEO " + adsBase.getClass().getName());
        }
    }

    private void b(boolean z) {
        if (!z) {
            if (!DeviceUtils.b()) {
                AppConfig.AdsBean.AdcolonyBean adcolony = GlobalVariable.c().a().getAds().getAdcolony();
                if (adcolony != null && adcolony.isEnable()) {
                    this.d.add(new AcolonyAds());
                }
                AppConfig.AdsBean.VungleBean vungle = GlobalVariable.c().a().getAds().getVungle();
                if (vungle != null && vungle.isEnable()) {
                    this.d.add(new VungleAds());
                }
                AppConfig.AdsBean.ChartBoostBean chartBoost = GlobalVariable.c().a().getAds().getChartBoost();
                if (chartBoost != null && chartBoost.isEnable()) {
                    this.d.add(new ChartboostAds());
                }
            } else {
                AppConfig.AdsBean.AdcolonyBean adcolony_amz = GlobalVariable.c().a().getAds().getAdcolony_amz();
                if (adcolony_amz != null && adcolony_amz.isEnable()) {
                    this.d.add(new AcolonyAds());
                }
                AppConfig.AdsBean.VungleBean vungle_amz = GlobalVariable.c().a().getAds().getVungle_amz();
                if (vungle_amz != null && vungle_amz.isEnable()) {
                    this.d.add(new VungleAds());
                }
                AppConfig.AdsBean.ChartBoostBean chartBoost_amz = GlobalVariable.c().a().getAds().getChartBoost_amz();
                if (chartBoost_amz != null && chartBoost_amz.isEnable()) {
                    this.d.add(new ChartboostAds());
                }
            }
            AppConfig.AdsBean.UnityAdsBean unity_ads = GlobalVariable.c().a().getAds().getUnity_ads();
            if (unity_ads != null && unity_ads.isEnable()) {
                this.d.add(new Unity_Ads());
            }
        }
    }

    public void a() {
        this.f1390a = false;
        for (AdsBase next : this.d) {
            next.b();
            next.e();
        }
        this.d.clear();
        for (AdsBase next2 : this.c) {
            next2.b();
            next2.e();
        }
        this.c.clear();
        ViewGroup viewGroup = this.i;
        if (viewGroup != null) {
            viewGroup.removeAllViews();
        }
    }

    private void a(boolean z) {
        AppConfig.AdsBean.AdmobBean admob = GlobalVariable.c().a().getAds().getAdmob();
        GlobalVariable.c().a().getAds().getFacebookAds();
        AppConfig.AdsBean.StartAppBean startApp = GlobalVariable.c().a().getAds().getStartApp();
        AppConfig.AdsBean.HouseAdsBean house_ads = GlobalVariable.c().a().getAds().getHouse_ads();
        AppConfig.AdsBean.ApplovinBean applovin = GlobalVariable.c().a().getAds().getApplovin();
        AppConfig.AdsBean.IronSrcBean ironsrc = GlobalVariable.c().a().getAds().getIronsrc();
        boolean b2 = DeviceUtils.b();
        if (!z && admob != null && admob.isEnable()) {
            this.d.add(new Admob());
        }
        if (!z && startApp != null && startApp.isEnable()) {
            this.d.add(new StartApp());
        }
        if (!b2 && house_ads != null && house_ads.isEnable()) {
            this.d.add(new HouseAds());
        }
        if (applovin != null && applovin.isEnable()) {
            this.d.add(new ApplovinAds());
        }
        if (ironsrc != null && ironsrc.isEnable()) {
            this.d.add(new IronsrcAds());
        }
    }

    public void a(ViewGroup viewGroup) {
        this.i = viewGroup;
        if (this.c.size() > 0) {
            AdsBase adsBase = this.c.get(this.h);
            adsBase.a(viewGroup);
            Logger.a("AdsManager", "show BANNER " + adsBase.getClass().getName());
        }
    }

    public void a(FrameLayout frameLayout) {
        this.l = frameLayout;
        if (!this.e && this.c.size() > 0) {
            AdsBase adsBase = this.c.get(this.k);
            adsBase.a(this.l);
            Logger.a("AdsManager", "show NATIVE " + adsBase.getClass().getName());
        }
    }

    public void a(int i2, int i3, Intent intent) {
        for (AdsBase a2 : this.c) {
            a2.a(i2, i3, intent);
        }
    }

    public void a(AdsBase adsBase, Boolean bool) {
        if (bool.booleanValue()) {
            this.c.add(adsBase);
            Collections.sort(this.c);
            this.k = 0;
            this.h = 0;
            this.g = 0;
        } else if (this.c.contains(adsBase)) {
            this.c.remove(adsBase);
        }
    }

    public boolean a(AdsBase adsBase, AdsBase.AdBaseType adBaseType, AdsBase.AdsStatus adsStatus) {
        if (adsStatus == AdsBase.AdsStatus.NOT_SHOW) {
            int i2 = AnonymousClass1.f1391a[adBaseType.ordinal()];
            if (i2 == 1) {
                this.j++;
                if (this.j < this.c.size()) {
                    k();
                    Logger.a("AdsManager", "show VIDEO " + adsBase.getClass().getName() + " status " + adsStatus);
                } else {
                    this.j = 0;
                }
            } else if (i2 == 2) {
                this.h++;
                if (this.i == null || this.h >= this.c.size()) {
                    this.h = 0;
                } else {
                    a(this.i);
                    Logger.a("AdsManager", "show fallback BANNER " + adsBase.getClass().getName() + " status " + adsStatus);
                }
            } else if (i2 == 3) {
                this.g++;
                if (this.g < this.c.size()) {
                    i();
                    Logger.a("AdsManager", "show fallback INTERTISIAL " + adsBase.getClass().getName() + " status " + adsStatus);
                } else {
                    this.g = 0;
                }
            } else if (i2 == 4) {
                this.k++;
                if (this.l == null || this.k >= this.c.size()) {
                    this.k = 0;
                } else {
                    a(this.l);
                    Logger.a("AdsManager", "show fallback NATIVE " + adsBase.getClass().getName() + " status " + adsStatus);
                }
            }
        } else {
            int i3 = AnonymousClass1.f1391a[adBaseType.ordinal()];
            if (i3 == 1) {
                this.j = 0;
            } else if (i3 == 2) {
                this.h = 0;
            } else if (i3 == 3) {
                this.f = System.currentTimeMillis();
                this.g = 0;
            } else if (i3 == 4) {
                this.k = 0;
            }
        }
        return false;
    }
}
