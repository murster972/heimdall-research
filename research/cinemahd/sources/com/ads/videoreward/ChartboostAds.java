package com.ads.videoreward;

import android.content.Intent;
import android.util.Log;
import com.ads.videoreward.AdsBase;
import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Libraries.CBLogging;
import com.chartboost.sdk.Model.CBError;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.original.tase.utils.DeviceUtils;

public class ChartboostAds extends AdsBase {
    private String e = CBLocation.LOCATION_DEFAULT;
    AppConfig.AdsBean.ChartBoostBean f = null;
    public ChartboostDelegate g = new ChartboostDelegate() {
        public void didCacheInPlay(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("In Play loaded at " + str);
        }

        public void didCacheInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Interstitial cached at " + str);
        }

        public void didCacheRewardedVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Did cache rewarded video " + str);
        }

        public void didClickInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Interstitial clicked at " + str);
        }

        public void didClickRewardedVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Rewarded video clicked at " + str);
        }

        public void didCloseInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Interstitial closed at " + str);
        }

        public void didCloseRewardedVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Rewarded video closed at " + str);
        }

        public void didCompleteRewardedVideo(String str, int i) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Rewarded video completed at " + str + "for reward: " + i);
            ChartboostAds chartboostAds2 = ChartboostAds.this;
            chartboostAds2.d.a(chartboostAds2);
        }

        public void didDismissInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Interstitial dismissed at " + str);
        }

        public void didDismissRewardedVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Rewarded video dismissed at " + str);
        }

        public void didDisplayInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Interstitial displayed at " + str);
        }

        public void didDisplayRewardedVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Rewarded video displayed at " + str);
        }

        public void didFailToLoadInPlay(String str, CBError.CBImpressionError cBImpressionError) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("In play failed to load at " + str + ", with error: " + cBImpressionError);
        }

        public void didFailToLoadInterstitial(String str, CBError.CBImpressionError cBImpressionError) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Interstitial failed to load at " + str + " with error: " + cBImpressionError.name());
        }

        public void didFailToLoadRewardedVideo(String str, CBError.CBImpressionError cBImpressionError) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Rewarded Video failed to load at " + str + " with error: " + cBImpressionError.name());
        }

        public void didFailToRecordClick(String str, CBError.CBClickError cBClickError) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            StringBuilder sb = new StringBuilder();
            sb.append("Failed to record click ");
            if (str == null) {
                str = "null";
            }
            sb.append(str);
            sb.append(", error: ");
            sb.append(cBClickError.name());
            chartboostAds.b(sb.toString());
        }

        public void didInitialize() {
            ChartboostAds.this.b("Chartboost SDK is initialized and ready!");
        }

        public boolean shouldDisplayInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Should display interstitial at " + str + "?");
            return true;
        }

        public boolean shouldDisplayRewardedVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Should display rewarded video at " + str + "?");
            return true;
        }

        public boolean shouldRequestInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Should request interstitial at " + str + "?");
            return true;
        }

        public void willDisplayInterstitial(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Will display interstitial at " + str);
        }

        public void willDisplayVideo(String str) {
            ChartboostAds chartboostAds = ChartboostAds.this;
            chartboostAds.b("Will display video at " + str);
        }
    };

    private void l() {
        Chartboost.setShouldPrefetchVideoContent(true);
        Chartboost.setShouldRequestInterstitialsInFirstSession(true);
        Chartboost.setAutoCacheAds(true);
    }

    public void a(int i, int i2, Intent intent) {
        super.a(i, i2, intent);
    }

    public void b(String str) {
        Log.i("ChartboostSample", str);
    }

    public void d() {
        super.d();
        if (DeviceUtils.b()) {
            this.f = GlobalVariable.c().a().getAds().getChartBoost_amz();
        } else {
            this.f = GlobalVariable.c().a().getAds().getChartBoost();
        }
        a(this.f.getEcmp());
        Chartboost.startWithAppId(c(), this.f.getApp_id(), this.f.getSignature());
        l();
        Chartboost.setActivityCallbacks(true);
        Chartboost.setDelegate(this.g);
        Chartboost.setLoggingLevel(CBLogging.Level.ALL);
        Chartboost.onCreate(c());
        Chartboost.cacheInterstitial(this.e);
        Chartboost.cacheRewardedVideo(this.e);
    }

    public void e() {
        super.e();
        Chartboost.onDestroy(c());
    }

    public void f() {
        super.f();
        Chartboost.onPause(c());
    }

    public void g() {
        super.g();
        Chartboost.onResume(c());
    }

    public void h() {
        super.h();
        Chartboost.onStart(c());
    }

    public void i() {
        super.i();
        Chartboost.onStop(c());
    }

    public void j() {
        Chartboost.setActivityAttrs(c());
        if (Chartboost.hasInterstitial(this.e)) {
            Chartboost.showInterstitial(this.e);
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
            return;
        }
        this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
    }

    public void k() {
        if (Chartboost.hasRewardedVideo(this.e)) {
            Chartboost.showRewardedVideo(this.e);
            this.d.a(this, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.SHOWED);
            return;
        }
        this.d.a(this, AdsBase.AdBaseType.VIDEO, AdsBase.AdsStatus.NOT_SHOW);
    }
}
