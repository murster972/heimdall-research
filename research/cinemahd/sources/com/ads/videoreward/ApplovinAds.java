package com.ads.videoreward;

import com.ads.videoreward.AdsBase;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.movie.data.api.GlobalVariable;
import com.original.tase.Logger;

public class ApplovinAds extends AdsBase {
    /* access modifiers changed from: private */
    public AppLovinAd e = null;

    /* access modifiers changed from: private */
    public void l() {
        AppLovinSdk.getInstance(c()).getAdService().loadNextAd(AppLovinAdSize.INTERSTITIAL, new AppLovinAdLoadListener() {
            public void adReceived(AppLovinAd appLovinAd) {
                Logger.a("ApplovinAds", "loadNextAd - " + appLovinAd.getZoneId());
                AppLovinAd unused = ApplovinAds.this.e = appLovinAd;
            }

            public void failedToReceiveAd(int i) {
                AppLovinAd unused = ApplovinAds.this.e = null;
                Logger.a("ApplovinAds", "failedToReceiveAd - " + i);
            }
        });
    }

    public void d() {
        super.d();
        AppLovinSdk.initializeSdk(c(), new AppLovinSdk.SdkInitializationListener() {
            public void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration) {
                Logger.a("ApplovinAds", "onSdkInitialized - " + appLovinSdkConfiguration.toString());
                ApplovinAds.this.l();
            }
        });
        a(GlobalVariable.c().a().getAds().getApplovin().getEcmp());
    }

    public void j() {
        if (this.e != null) {
            AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(AppLovinSdk.getInstance(c()), c());
            create.setAdDisplayListener(new AppLovinAdDisplayListener() {
                public void adDisplayed(AppLovinAd appLovinAd) {
                    AppLovinAd unused = ApplovinAds.this.e = null;
                    ApplovinAds.this.l();
                }

                public void adHidden(AppLovinAd appLovinAd) {
                }
            });
            create.showAndRender(this.e);
            this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
            return;
        }
        this.d.a(this, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
    }
}
