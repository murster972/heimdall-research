package com.ads.videoreward;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.ads.videoreward.AdsBase;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.AppConfig;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.ads.banner.BannerListener;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.StartAppSDK;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;

public class StartApp extends AdsBase {
    StartAppAd e = null;

    public void a(ViewGroup viewGroup) {
        Banner banner = new Banner(c());
        banner.setBannerListener(new BannerListener() {
            public void onClick(View view) {
            }

            public void onFailedToReceiveAd(View view) {
                StartApp startApp = StartApp.this;
                startApp.d.a(startApp, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
            }

            public void onImpression(View view) {
            }

            public void onReceiveAd(View view) {
                StartApp startApp = StartApp.this;
                startApp.d.a(startApp, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
            }
        });
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        viewGroup.addView(banner, layoutParams);
    }

    public void d() {
        super.d();
        AppConfig.AdsBean.StartAppBean startApp = GlobalVariable.c().a().getAds().getStartApp();
        if (startApp != null) {
            StartAppSDK.init((Context) c(), startApp.getApp_id(), true);
            if (!startApp.isEnable_splash_ads()) {
                StartAppAd.disableSplash();
            }
            StartAppSDK.setUserConsent(c(), "ACCESS_FINE_LOCATION ", System.currentTimeMillis(), true);
            StartAppSDK.enableReturnAds(false);
            this.e = new StartAppAd(c());
        }
        a(startApp.getEcmp());
    }

    public void j() {
        this.e.loadAd((AdEventListener) new AdEventListener() {
            public void onFailedToReceiveAd(Ad ad) {
                StartApp startApp = StartApp.this;
                startApp.d.a(startApp, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.NOT_SHOW);
            }

            public void onReceiveAd(Ad ad) {
                StartAppAd.showAd((Context) StartApp.this.c());
                StartApp startApp = StartApp.this;
                startApp.d.a(startApp, AdsBase.AdBaseType.INTERTISIAL, AdsBase.AdsStatus.SHOWED);
            }
        });
    }

    public void a(FrameLayout frameLayout) {
        Banner banner = new Banner(c());
        banner.setBannerListener(new BannerListener() {
            public void onClick(View view) {
            }

            public void onFailedToReceiveAd(View view) {
                StartApp startApp = StartApp.this;
                startApp.d.a(startApp, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.NOT_SHOW);
            }

            public void onImpression(View view) {
            }

            public void onReceiveAd(View view) {
                StartApp startApp = StartApp.this;
                startApp.d.a(startApp, AdsBase.AdBaseType.BANNER, AdsBase.AdsStatus.SHOWED);
            }
        });
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams.addRule(14);
        layoutParams.addRule(12);
        frameLayout.addView(banner, layoutParams);
    }
}
