package com.androidadvance.topsnackbar;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import java.lang.ref.WeakReference;

class SnackbarManager {
    private static SnackbarManager e;

    /* renamed from: a  reason: collision with root package name */
    private final Object f1415a = new Object();
    private final Handler b = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            if (message.what != 0) {
                return false;
            }
            SnackbarManager.this.a((SnackbarRecord) message.obj);
            return true;
        }
    });
    private SnackbarRecord c;
    private SnackbarRecord d;

    interface Callback {
        void a(int i);

        void show();
    }

    private SnackbarManager() {
    }

    private boolean f(Callback callback) {
        SnackbarRecord snackbarRecord = this.c;
        return snackbarRecord != null && snackbarRecord.a(callback);
    }

    private boolean g(Callback callback) {
        SnackbarRecord snackbarRecord = this.d;
        return snackbarRecord != null && snackbarRecord.a(callback);
    }

    public boolean b(Callback callback) {
        boolean z;
        synchronized (this.f1415a) {
            if (!f(callback)) {
                if (!g(callback)) {
                    z = false;
                }
            }
            z = true;
        }
        return z;
    }

    public void c(Callback callback) {
        synchronized (this.f1415a) {
            if (f(callback)) {
                this.c = null;
                if (this.d != null) {
                    b();
                }
            }
        }
    }

    public void d(Callback callback) {
        synchronized (this.f1415a) {
            if (f(callback)) {
                b(this.c);
            }
        }
    }

    public void e(Callback callback) {
        synchronized (this.f1415a) {
            if (f(callback)) {
                b(this.c);
            }
        }
    }

    private static class SnackbarRecord {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final WeakReference<Callback> f1417a;
        /* access modifiers changed from: private */
        public int b;

        SnackbarRecord(int i, Callback callback) {
            this.f1417a = new WeakReference<>(callback);
            this.b = i;
        }

        /* access modifiers changed from: package-private */
        public boolean a(Callback callback) {
            return callback != null && this.f1417a.get() == callback;
        }
    }

    static SnackbarManager a() {
        if (e == null) {
            e = new SnackbarManager();
        }
        return e;
    }

    private void b() {
        SnackbarRecord snackbarRecord = this.d;
        if (snackbarRecord != null) {
            this.c = snackbarRecord;
            this.d = null;
            Callback callback = (Callback) this.c.f1417a.get();
            if (callback != null) {
                callback.show();
            } else {
                this.c = null;
            }
        }
    }

    public void a(int i, Callback callback) {
        synchronized (this.f1415a) {
            if (f(callback)) {
                int unused = this.c.b = i;
                this.b.removeCallbacksAndMessages(this.c);
                b(this.c);
                return;
            }
            if (g(callback)) {
                int unused2 = this.d.b = i;
            } else {
                this.d = new SnackbarRecord(i, callback);
            }
            if (this.c == null || !a(this.c, 4)) {
                this.c = null;
                b();
            }
        }
    }

    private void b(SnackbarRecord snackbarRecord) {
        if (snackbarRecord.b != -2) {
            int i = 2750;
            if (snackbarRecord.b > 0) {
                i = snackbarRecord.b;
            } else if (snackbarRecord.b == -1) {
                i = 1500;
            }
            this.b.removeCallbacksAndMessages(snackbarRecord);
            Handler handler = this.b;
            handler.sendMessageDelayed(Message.obtain(handler, 0, snackbarRecord), (long) i);
        }
    }

    public void a(Callback callback, int i) {
        synchronized (this.f1415a) {
            if (f(callback)) {
                a(this.c, i);
            } else if (g(callback)) {
                a(this.d, i);
            }
        }
    }

    public void a(Callback callback) {
        synchronized (this.f1415a) {
            if (f(callback)) {
                this.b.removeCallbacksAndMessages(this.c);
            }
        }
    }

    private boolean a(SnackbarRecord snackbarRecord, int i) {
        Callback callback = (Callback) snackbarRecord.f1417a.get();
        if (callback == null) {
            return false;
        }
        callback.a(i);
        return true;
    }

    /* access modifiers changed from: private */
    public void a(SnackbarRecord snackbarRecord) {
        synchronized (this.f1415a) {
            if (this.c == snackbarRecord || this.d == snackbarRecord) {
                a(snackbarRecord, 2);
            }
        }
    }
}
