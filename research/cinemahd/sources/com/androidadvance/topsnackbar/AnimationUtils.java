package com.androidadvance.topsnackbar;

import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import androidx.interpolator.view.animation.FastOutSlowInInterpolator;

public class AnimationUtils {

    /* renamed from: a  reason: collision with root package name */
    public static final Interpolator f1413a = new FastOutSlowInInterpolator();

    static {
        new LinearInterpolator();
        new DecelerateInterpolator();
    }

    AnimationUtils() {
    }
}
