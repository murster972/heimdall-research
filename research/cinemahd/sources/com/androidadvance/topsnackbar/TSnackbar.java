package com.androidadvance.topsnackbar;

import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import com.androidadvance.topsnackbar.SnackbarManager;
import com.facebook.imagepipeline.common.RotationOptions;
import com.google.android.material.behavior.SwipeDismissBehavior;

public final class TSnackbar {
    /* access modifiers changed from: private */
    public static final Handler g = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            int i = message.what;
            if (i == 0) {
                ((TSnackbar) message.obj).d();
                return true;
            } else if (i != 1) {
                return false;
            } else {
                ((TSnackbar) message.obj).a(message.arg1);
                return true;
            }
        }
    });

    /* renamed from: a  reason: collision with root package name */
    private final ViewGroup f1418a;
    private final Context b;
    /* access modifiers changed from: private */
    public final SnackbarLayout c;
    private int d;
    /* access modifiers changed from: private */
    public Callback e;
    /* access modifiers changed from: private */
    public final SnackbarManager.Callback f = new SnackbarManager.Callback() {
        public void a(int i) {
            TSnackbar.g.sendMessage(TSnackbar.g.obtainMessage(1, i, 0, TSnackbar.this));
        }

        public void show() {
            TSnackbar.g.sendMessage(TSnackbar.g.obtainMessage(0, TSnackbar.this));
        }
    };

    final class Behavior extends SwipeDismissBehavior<SnackbarLayout> {
        Behavior() {
        }

        public boolean a(View view) {
            return view instanceof SnackbarLayout;
        }

        /* renamed from: a */
        public boolean onInterceptTouchEvent(CoordinatorLayout coordinatorLayout, SnackbarLayout snackbarLayout, MotionEvent motionEvent) {
            if (coordinatorLayout.a((View) snackbarLayout, (int) motionEvent.getX(), (int) motionEvent.getY())) {
                int actionMasked = motionEvent.getActionMasked();
                if (actionMasked == 0) {
                    SnackbarManager.a().a(TSnackbar.this.f);
                } else if (actionMasked == 1 || actionMasked == 3) {
                    SnackbarManager.a().e(TSnackbar.this.f);
                }
            }
            return super.onInterceptTouchEvent(coordinatorLayout, snackbarLayout, motionEvent);
        }
    }

    public static abstract class Callback {
        public void a(TSnackbar tSnackbar) {
        }

        public void a(TSnackbar tSnackbar, int i) {
        }
    }

    public static class SnackbarLayout extends LinearLayout {

        /* renamed from: a  reason: collision with root package name */
        private TextView f1428a;
        private Button b;
        private int c;
        private int d;
        private OnLayoutChangeListener e;
        private OnAttachStateChangeListener f;

        interface OnAttachStateChangeListener {
            void onViewAttachedToWindow(View view);

            void onViewDetachedFromWindow(View view);
        }

        interface OnLayoutChangeListener {
            void a(View view, int i, int i2, int i3, int i4);
        }

        public SnackbarLayout(Context context) {
            this(context, (AttributeSet) null);
        }

        /* access modifiers changed from: package-private */
        public void a(int i, int i2) {
            ViewCompat.a((View) this.f1428a, 0.0f);
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.f1428a);
            a2.a(1.0f);
            long j = (long) i2;
            a2.a(j);
            long j2 = (long) i;
            a2.b(j2);
            a2.c();
            if (this.b.getVisibility() == 0) {
                ViewCompat.a((View) this.b, 0.0f);
                ViewPropertyAnimatorCompat a3 = ViewCompat.a(this.b);
                a3.a(1.0f);
                a3.a(j);
                a3.b(j2);
                a3.c();
            }
        }

        /* access modifiers changed from: package-private */
        public void b(int i, int i2) {
            ViewCompat.a((View) this.f1428a, 1.0f);
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.f1428a);
            a2.a(0.0f);
            long j = (long) i2;
            a2.a(j);
            long j2 = (long) i;
            a2.b(j2);
            a2.c();
            if (this.b.getVisibility() == 0) {
                ViewCompat.a((View) this.b, 1.0f);
                ViewPropertyAnimatorCompat a3 = ViewCompat.a(this.b);
                a3.a(0.0f);
                a3.a(j);
                a3.b(j2);
                a3.c();
            }
        }

        /* access modifiers changed from: package-private */
        public Button getActionView() {
            return this.b;
        }

        /* access modifiers changed from: package-private */
        public TextView getMessageView() {
            return this.f1428a;
        }

        /* access modifiers changed from: protected */
        public void onAttachedToWindow() {
            super.onAttachedToWindow();
            OnAttachStateChangeListener onAttachStateChangeListener = this.f;
            if (onAttachStateChangeListener != null) {
                onAttachStateChangeListener.onViewAttachedToWindow(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            OnAttachStateChangeListener onAttachStateChangeListener = this.f;
            if (onAttachStateChangeListener != null) {
                onAttachStateChangeListener.onViewDetachedFromWindow(this);
            }
        }

        /* access modifiers changed from: protected */
        public void onFinishInflate() {
            super.onFinishInflate();
            this.f1428a = (TextView) findViewById(R$id.snackbar_text);
            this.b = (Button) findViewById(R$id.snackbar_action);
        }

        /* access modifiers changed from: protected */
        public void onLayout(boolean z, int i, int i2, int i3, int i4) {
            OnLayoutChangeListener onLayoutChangeListener;
            super.onLayout(z, i, i2, i3, i4);
            if (z && (onLayoutChangeListener = this.e) != null) {
                onLayoutChangeListener.a(this, i, i2, i3, i4);
            }
        }

        /* access modifiers changed from: protected */
        /* JADX WARNING: Code restructure failed: missing block: B:15:0x0053, code lost:
            if (a(1, r0, r0 - r1) != false) goto L_0x0062;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:19:0x005e, code lost:
            if (a(0, r0, r0) != false) goto L_0x0062;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void onMeasure(int r8, int r9) {
            /*
                r7 = this;
                super.onMeasure(r8, r9)
                int r0 = r7.c
                if (r0 <= 0) goto L_0x0018
                int r0 = r7.getMeasuredWidth()
                int r1 = r7.c
                if (r0 <= r1) goto L_0x0018
                r8 = 1073741824(0x40000000, float:2.0)
                int r8 = android.view.View.MeasureSpec.makeMeasureSpec(r1, r8)
                super.onMeasure(r8, r9)
            L_0x0018:
                android.content.res.Resources r0 = r7.getResources()
                int r1 = com.androidadvance.topsnackbar.R$dimen.design_snackbar_padding_vertical_2lines
                int r0 = r0.getDimensionPixelSize(r1)
                android.content.res.Resources r1 = r7.getResources()
                int r2 = com.androidadvance.topsnackbar.R$dimen.design_snackbar_padding_vertical
                int r1 = r1.getDimensionPixelSize(r2)
                android.widget.TextView r2 = r7.f1428a
                android.text.Layout r2 = r2.getLayout()
                int r2 = r2.getLineCount()
                r3 = 0
                r4 = 1
                if (r2 <= r4) goto L_0x003c
                r2 = 1
                goto L_0x003d
            L_0x003c:
                r2 = 0
            L_0x003d:
                if (r2 == 0) goto L_0x0056
                int r5 = r7.d
                if (r5 <= 0) goto L_0x0056
                android.widget.Button r5 = r7.b
                int r5 = r5.getMeasuredWidth()
                int r6 = r7.d
                if (r5 <= r6) goto L_0x0056
                int r1 = r0 - r1
                boolean r0 = r7.a((int) r4, (int) r0, (int) r1)
                if (r0 == 0) goto L_0x0061
                goto L_0x0062
            L_0x0056:
                if (r2 == 0) goto L_0x0059
                goto L_0x005a
            L_0x0059:
                r0 = r1
            L_0x005a:
                boolean r0 = r7.a((int) r3, (int) r0, (int) r0)
                if (r0 == 0) goto L_0x0061
                goto L_0x0062
            L_0x0061:
                r4 = 0
            L_0x0062:
                if (r4 == 0) goto L_0x0067
                super.onMeasure(r8, r9)
            L_0x0067:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.androidadvance.topsnackbar.TSnackbar.SnackbarLayout.onMeasure(int, int):void");
        }

        /* access modifiers changed from: package-private */
        public void setOnAttachStateChangeListener(OnAttachStateChangeListener onAttachStateChangeListener) {
            this.f = onAttachStateChangeListener;
        }

        /* access modifiers changed from: package-private */
        public void setOnLayoutChangeListener(OnLayoutChangeListener onLayoutChangeListener) {
            this.e = onLayoutChangeListener;
        }

        public SnackbarLayout(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.SnackbarLayout);
            this.c = obtainStyledAttributes.getDimensionPixelSize(R$styleable.SnackbarLayout_android_maxWidth, -1);
            this.d = obtainStyledAttributes.getDimensionPixelSize(R$styleable.SnackbarLayout_maxActionInlineWidth, -1);
            if (obtainStyledAttributes.hasValue(R$styleable.SnackbarLayout_elevation)) {
                ViewCompat.b((View) this, (float) obtainStyledAttributes.getDimensionPixelSize(R$styleable.SnackbarLayout_elevation, 0));
            }
            obtainStyledAttributes.recycle();
            setClickable(true);
            LayoutInflater.from(context).inflate(R$layout.tsnackbar_layout_include, this);
            ViewCompat.g(this, 1);
        }

        private boolean a(int i, int i2, int i3) {
            boolean z;
            if (i != getOrientation()) {
                setOrientation(i);
                z = true;
            } else {
                z = false;
            }
            if (this.f1428a.getPaddingTop() == i2 && this.f1428a.getPaddingBottom() == i3) {
                return z;
            }
            a((View) this.f1428a, i2, i3);
            return true;
        }

        private static void a(View view, int i, int i2) {
            if (ViewCompat.G(view)) {
                ViewCompat.b(view, ViewCompat.q(view), i, ViewCompat.p(view), i2);
            } else {
                view.setPadding(view.getPaddingLeft(), i, view.getPaddingRight(), i2);
            }
        }
    }

    private TSnackbar(ViewGroup viewGroup) {
        this.f1418a = viewGroup;
        this.b = viewGroup.getContext();
        this.c = (SnackbarLayout) LayoutInflater.from(this.b).inflate(R$layout.tsnackbar_layout, this.f1418a, false);
    }

    /* access modifiers changed from: private */
    public void f() {
        if (Build.VERSION.SDK_INT >= 14) {
            SnackbarLayout snackbarLayout = this.c;
            ViewCompat.e((View) snackbarLayout, (float) (-snackbarLayout.getHeight()));
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.c);
            a2.d(0.0f);
            a2.a(AnimationUtils.f1413a);
            a2.a(250);
            a2.a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    if (TSnackbar.this.e != null) {
                        TSnackbar.this.e.a(TSnackbar.this);
                    }
                    SnackbarManager.a().d(TSnackbar.this.f);
                }

                public void onAnimationStart(View view) {
                    TSnackbar.this.c.a(70, RotationOptions.ROTATE_180);
                }
            });
            a2.c();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.c.getContext(), R$anim.top_in);
        loadAnimation.setInterpolator(AnimationUtils.f1413a);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                if (TSnackbar.this.e != null) {
                    TSnackbar.this.e.a(TSnackbar.this);
                }
                SnackbarManager.a().d(TSnackbar.this.f);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.c.startAnimation(loadAnimation);
    }

    private boolean g() {
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        if (!(layoutParams instanceof CoordinatorLayout.LayoutParams)) {
            return false;
        }
        CoordinatorLayout.Behavior d2 = ((CoordinatorLayout.LayoutParams) layoutParams).d();
        if (!(d2 instanceof SwipeDismissBehavior) || ((SwipeDismissBehavior) d2).a() == 0) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void d(int i) {
        SnackbarManager.a().a(this.f, i);
    }

    /* access modifiers changed from: private */
    public void e(int i) {
        SnackbarManager.a().c(this.f);
        Callback callback = this.e;
        if (callback != null) {
            callback.a(this, i);
        }
        ViewParent parent = this.c.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.c);
        }
    }

    public void c() {
        SnackbarManager.a().a(this.d, this.f);
    }

    public static TSnackbar a(View view, CharSequence charSequence, int i) {
        TSnackbar tSnackbar = new TSnackbar(a(view));
        tSnackbar.a(charSequence);
        tSnackbar.b(i);
        return tSnackbar;
    }

    public TSnackbar b(int i) {
        this.d = i;
        return this;
    }

    private void c(final int i) {
        if (Build.VERSION.SDK_INT >= 14) {
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.c);
            a2.d((float) (-this.c.getHeight()));
            a2.a(AnimationUtils.f1413a);
            a2.a(250);
            a2.a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
                public void onAnimationEnd(View view) {
                    TSnackbar.this.e(i);
                }

                public void onAnimationStart(View view) {
                    TSnackbar.this.c.b(0, RotationOptions.ROTATE_180);
                }
            });
            a2.c();
            return;
        }
        Animation loadAnimation = AnimationUtils.loadAnimation(this.c.getContext(), R$anim.top_out);
        loadAnimation.setInterpolator(AnimationUtils.f1413a);
        loadAnimation.setDuration(250);
        loadAnimation.setAnimationListener(new Animation.AnimationListener() {
            public void onAnimationEnd(Animation animation) {
                TSnackbar.this.e(i);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
            }
        });
        this.c.startAnimation(loadAnimation);
    }

    public boolean b() {
        return SnackbarManager.a().b(this.f);
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (this.c.getParent() == null) {
            ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
            if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
                Behavior behavior = new Behavior();
                behavior.b(0.1f);
                behavior.a(0.6f);
                behavior.a(0);
                behavior.a((SwipeDismissBehavior.OnDismissListener) new SwipeDismissBehavior.OnDismissListener() {
                    public void a(View view) {
                        TSnackbar.this.d(0);
                    }

                    public void a(int i) {
                        if (i == 0) {
                            SnackbarManager.a().e(TSnackbar.this.f);
                        } else if (i == 1 || i == 2) {
                            SnackbarManager.a().a(TSnackbar.this.f);
                        }
                    }
                });
                ((CoordinatorLayout.LayoutParams) layoutParams).a((CoordinatorLayout.Behavior) behavior);
            }
            this.f1418a.addView(this.c);
        }
        this.c.setOnAttachStateChangeListener(new SnackbarLayout.OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View view) {
            }

            public void onViewDetachedFromWindow(View view) {
                if (TSnackbar.this.b()) {
                    TSnackbar.g.post(new Runnable() {
                        public void run() {
                            TSnackbar.this.e(3);
                        }
                    });
                }
            }
        });
        if (ViewCompat.E(this.c)) {
            f();
        } else {
            this.c.setOnLayoutChangeListener(new SnackbarLayout.OnLayoutChangeListener() {
                public void a(View view, int i, int i2, int i3, int i4) {
                    TSnackbar.this.f();
                    TSnackbar.this.c.setOnLayoutChangeListener((SnackbarLayout.OnLayoutChangeListener) null);
                }
            });
        }
    }

    private static ViewGroup a(View view) {
        ViewGroup viewGroup = null;
        while (!(view instanceof CoordinatorLayout)) {
            if (view instanceof FrameLayout) {
                if (view.getId() == 16908290) {
                    return (ViewGroup) view;
                }
                viewGroup = (ViewGroup) view;
            }
            if (view != null) {
                ViewParent parent = view.getParent();
                if (parent instanceof View) {
                    view = (View) parent;
                    continue;
                } else {
                    view = null;
                    continue;
                }
            }
            if (view == null) {
                return viewGroup;
            }
        }
        return (ViewGroup) view;
    }

    public TSnackbar a(CharSequence charSequence) {
        this.c.getMessageView().setText(charSequence);
        return this;
    }

    public void a() {
        d(3);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        if (this.c.getVisibility() != 0 || g()) {
            e(i);
        } else {
            c(i);
        }
    }
}
