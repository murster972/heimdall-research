package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import java.util.HashMap;
import java.util.Map;

public class CallbackThrottler {
    private static final CallbackThrottler e = new CallbackThrottler();

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Long> f4597a = new HashMap();
    /* access modifiers changed from: private */
    public Map<String, Boolean> b = new HashMap();
    private InterstitialListener c = null;
    private int d;

    private CallbackThrottler() {
    }

    public static synchronized CallbackThrottler b() {
        CallbackThrottler callbackThrottler;
        synchronized (CallbackThrottler.class) {
            callbackThrottler = e;
        }
        return callbackThrottler;
    }

    private void b(final String str, final IronSourceError ironSourceError) {
        if (!a(str)) {
            if (!this.f4597a.containsKey(str)) {
                a(str, ironSourceError);
                return;
            }
            long currentTimeMillis = System.currentTimeMillis() - this.f4597a.get(str).longValue();
            if (currentTimeMillis > ((long) (this.d * 1000))) {
                a(str, ironSourceError);
                return;
            }
            this.b.put(str, true);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                public void run() {
                    CallbackThrottler.this.a(str, ironSourceError);
                    CallbackThrottler.this.b.put(str, false);
                }
            }, ((long) (this.d * 1000)) - currentTimeMillis);
        }
    }

    public void a(InterstitialListener interstitialListener) {
        this.c = interstitialListener;
    }

    public void a(IronSourceError ironSourceError) {
        synchronized (this) {
            b("mediation", ironSourceError);
        }
    }

    public boolean a() {
        boolean a2;
        synchronized (this) {
            a2 = a("mediation");
        }
        return a2;
    }

    private boolean a(String str) {
        if (!TextUtils.isEmpty(str) && this.b.containsKey(str)) {
            return this.b.get(str).booleanValue();
        }
        return false;
    }

    /* access modifiers changed from: private */
    public void a(String str, IronSourceError ironSourceError) {
        this.f4597a.put(str, Long.valueOf(System.currentTimeMillis()));
        InterstitialListener interstitialListener = this.c;
        if (interstitialListener != null) {
            interstitialListener.a(ironSourceError);
            IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
            c2.b(ironSourceTag, "onInterstitialAdLoadFailed(" + ironSourceError.toString() + ")", 1);
        }
    }

    public void a(int i) {
        this.d = i;
    }
}
