package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class DemandOnlySmash {

    /* renamed from: a  reason: collision with root package name */
    protected AbstractAdapter f4603a;
    protected AdapterConfig b;
    protected JSONObject c;
    private SMASH_STATE d;
    private Timer e;
    int f;
    private final Object g = new Object();
    private final Object h = new Object();

    protected enum SMASH_STATE {
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    public DemandOnlySmash(AdapterConfig adapterConfig, AbstractAdapter abstractAdapter) {
        this.b = adapterConfig;
        this.f4603a = abstractAdapter;
        this.c = adapterConfig.b();
        this.d = SMASH_STATE.NOT_LOADED;
        this.e = null;
    }

    public void a(Activity activity) {
        this.f4603a.onPause(activity);
    }

    public void b(Activity activity) {
        this.f4603a.onResume(activity);
    }

    public String k() {
        return this.b.d();
    }

    public Map<String, Object> l() {
        HashMap hashMap = new HashMap();
        try {
            String str = "";
            hashMap.put("providerAdapterVersion", this.f4603a != null ? this.f4603a.getVersion() : str);
            if (this.f4603a != null) {
                str = this.f4603a.getCoreSDKVersion();
            }
            hashMap.put("providerSDKVersion", str);
            hashMap.put("spId", this.b.e());
            hashMap.put("provider", this.b.a());
            hashMap.put("isDemandOnly", 1);
        } catch (Exception e2) {
            IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            c2.a(ironSourceTag, "getProviderEventData " + k() + ")", (Throwable) e2);
        }
        return hashMap;
    }

    /* access modifiers changed from: package-private */
    public String m() {
        SMASH_STATE smash_state = this.d;
        return smash_state == null ? "null" : smash_state.toString();
    }

    public String n() {
        return this.b.e();
    }

    /* access modifiers changed from: package-private */
    public void o() {
        synchronized (this.h) {
            if (this.e != null) {
                this.e.cancel();
                this.e = null;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(SMASH_STATE smash_state, SMASH_STATE smash_state2) {
        synchronized (this.g) {
            if (this.d != smash_state) {
                return false;
            }
            a(smash_state2);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public SMASH_STATE a(SMASH_STATE[] smash_stateArr, SMASH_STATE smash_state) {
        SMASH_STATE smash_state2;
        synchronized (this.g) {
            smash_state2 = this.d;
            if (Arrays.asList(smash_stateArr).contains(this.d)) {
                a(smash_state);
            }
        }
        return smash_state2;
    }

    /* access modifiers changed from: package-private */
    public void a(SMASH_STATE smash_state) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlySmash " + this.b.d() + ": current state=" + this.d + ", new state=" + smash_state, 0);
        synchronized (this.g) {
            this.d = smash_state;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(TimerTask timerTask) {
        synchronized (this.h) {
            o();
            this.e = new Timer();
            this.e.schedule(timerTask, (long) (this.f * 1000));
        }
    }
}
