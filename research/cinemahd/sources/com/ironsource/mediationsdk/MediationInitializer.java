package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.config.ConfigValidationResult;
import com.ironsource.mediationsdk.integration.IntegrationHelper;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.mediationsdk.sdk.SegmentListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

class MediationInitializer implements NetworkStateReceiver.NetworkStateReceiverListener {
    private static MediationInitializer z;

    /* renamed from: a  reason: collision with root package name */
    private final String f4631a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public int d;
    /* access modifiers changed from: private */
    public int e;
    /* access modifiers changed from: private */
    public int f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    private HandlerThread i;
    /* access modifiers changed from: private */
    public Handler j;
    private boolean k;
    private AtomicBoolean l;
    private NetworkStateReceiver m;
    /* access modifiers changed from: private */
    public CountDownTimer n;
    /* access modifiers changed from: private */
    public List<OnMediationInitializationListener> o;
    /* access modifiers changed from: private */
    public Activity p;
    /* access modifiers changed from: private */
    public String q;
    /* access modifiers changed from: private */
    public String r;
    /* access modifiers changed from: private */
    public ServerResponseWrapper s;
    private EInitStatus t;
    /* access modifiers changed from: private */
    public String u;
    /* access modifiers changed from: private */
    public SegmentListener v;
    /* access modifiers changed from: private */
    public boolean w;
    /* access modifiers changed from: private */
    public long x;
    private InitRunnable y;

    enum EInitStatus {
        NOT_INIT,
        INIT_IN_PROGRESS,
        INIT_FAILED,
        INITIATED
    }

    abstract class InitRunnable implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        boolean f4635a = true;
        String b;
        protected IronSourceObject.IResponseListener c = new IronSourceObject.IResponseListener() {
            public void a(String str) {
                InitRunnable initRunnable = InitRunnable.this;
                initRunnable.f4635a = false;
                initRunnable.b = str;
            }
        };

        InitRunnable(MediationInitializer mediationInitializer) {
        }
    }

    interface OnMediationInitializationListener {
        void a();

        void a(String str);

        void a(List<IronSource.AD_UNIT> list, boolean z);
    }

    private MediationInitializer() {
        this.f4631a = MediationInitializer.class.getSimpleName();
        this.h = false;
        this.i = null;
        this.k = false;
        this.o = new ArrayList();
        this.y = new InitRunnable() {
            public void run() {
                ServerSegmetData c;
                try {
                    IronSourceObject q = IronSourceObject.q();
                    if (MediationInitializer.this.a(MediationInitializer.this.q).b()) {
                        String unused = MediationInitializer.this.u = "userGenerated";
                    } else {
                        String unused2 = MediationInitializer.this.q = q.a((Context) MediationInitializer.this.p);
                        if (!TextUtils.isEmpty(MediationInitializer.this.q)) {
                            String unused3 = MediationInitializer.this.u = "GAID";
                        } else {
                            String unused4 = MediationInitializer.this.q = DeviceStatus.j(MediationInitializer.this.p);
                            if (!TextUtils.isEmpty(MediationInitializer.this.q)) {
                                String unused5 = MediationInitializer.this.u = "UUID";
                            } else {
                                String unused6 = MediationInitializer.this.q = "";
                            }
                        }
                        q.c(MediationInitializer.this.q);
                    }
                    GeneralProperties.b().a("userIdType", MediationInitializer.this.u);
                    if (!TextUtils.isEmpty(MediationInitializer.this.q)) {
                        GeneralProperties.b().a("userId", MediationInitializer.this.q);
                    }
                    if (!TextUtils.isEmpty(MediationInitializer.this.r)) {
                        GeneralProperties.b().a("appKey", MediationInitializer.this.r);
                    }
                    long unused7 = MediationInitializer.this.x = new Date().getTime();
                    ServerResponseWrapper unused8 = MediationInitializer.this.s = q.a((Context) MediationInitializer.this.p, MediationInitializer.this.q, this.c);
                    if (MediationInitializer.this.s != null) {
                        MediationInitializer.this.j.removeCallbacks(this);
                        if (MediationInitializer.this.s.g()) {
                            MediationInitializer.this.a(EInitStatus.INITIATED);
                            q.a(new Date().getTime() - MediationInitializer.this.x);
                            if (MediationInitializer.this.s.a().a().a()) {
                                IntegrationHelper.b(MediationInitializer.this.p);
                            }
                            List<IronSource.AD_UNIT> b = MediationInitializer.this.s.b();
                            for (OnMediationInitializationListener a2 : MediationInitializer.this.o) {
                                a2.a(b, MediationInitializer.this.e());
                            }
                            if (MediationInitializer.this.v != null && (c = MediationInitializer.this.s.a().a().c()) != null && !TextUtils.isEmpty(c.c())) {
                                MediationInitializer.this.v.a(c.c());
                            }
                        } else if (!MediationInitializer.this.h) {
                            MediationInitializer.this.a(EInitStatus.INIT_FAILED);
                            boolean unused9 = MediationInitializer.this.h = true;
                            for (OnMediationInitializationListener a3 : MediationInitializer.this.o) {
                                a3.a("serverResponseIsNotValid");
                            }
                        }
                    } else {
                        if (MediationInitializer.this.c == 3) {
                            boolean unused10 = MediationInitializer.this.w = true;
                            for (OnMediationInitializationListener a4 : MediationInitializer.this.o) {
                                a4.a();
                            }
                        }
                        if (this.f4635a && MediationInitializer.this.c < MediationInitializer.this.d) {
                            boolean unused11 = MediationInitializer.this.g = true;
                            MediationInitializer.this.j.postDelayed(this, (long) (MediationInitializer.this.b * 1000));
                            if (MediationInitializer.this.c < MediationInitializer.this.e) {
                                int unused12 = MediationInitializer.this.b = MediationInitializer.this.b * 2;
                            }
                        }
                        if ((!this.f4635a || MediationInitializer.this.c == MediationInitializer.this.f) && !MediationInitializer.this.h) {
                            boolean unused13 = MediationInitializer.this.h = true;
                            if (TextUtils.isEmpty(this.b)) {
                                this.b = "noServerResponse";
                            }
                            for (OnMediationInitializationListener a5 : MediationInitializer.this.o) {
                                a5.a(this.b);
                            }
                            MediationInitializer.this.a(EInitStatus.INIT_FAILED);
                            IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.API, "Mediation availability false reason: No server response", 1);
                        }
                        MediationInitializer.f(MediationInitializer.this);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        this.t = EInitStatus.NOT_INIT;
        this.i = new HandlerThread("IronSourceInitiatorHandler");
        this.i.start();
        this.j = new Handler(this.i.getLooper());
        this.b = 1;
        this.c = 0;
        this.d = 62;
        this.e = 12;
        this.f = 5;
        this.l = new AtomicBoolean(true);
        this.g = false;
        this.w = false;
    }

    static /* synthetic */ int f(MediationInitializer mediationInitializer) {
        int i2 = mediationInitializer.c;
        mediationInitializer.c = i2 + 1;
        return i2;
    }

    public static synchronized MediationInitializer d() {
        MediationInitializer mediationInitializer;
        synchronized (MediationInitializer.class) {
            if (z == null) {
                z = new MediationInitializer();
            }
            mediationInitializer = z;
        }
        return mediationInitializer;
    }

    /* access modifiers changed from: private */
    public boolean e() {
        return this.g;
    }

    public synchronized boolean b() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a(EInitStatus.INIT_FAILED);
    }

    /* access modifiers changed from: private */
    public synchronized void a(EInitStatus eInitStatus) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "setInitStatus(old status: " + this.t + ", new status: " + eInitStatus + ")", 0);
        this.t = eInitStatus;
    }

    public synchronized void a(Activity activity, String str, String str2, IronSource.AD_UNIT... ad_unitArr) {
        try {
            if (this.l == null || !this.l.compareAndSet(true, false)) {
                IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                c2.b(ironSourceTag, this.f4631a + ": Multiple calls to init are not allowed", 2);
            } else {
                a(EInitStatus.INIT_IN_PROGRESS);
                this.p = activity;
                this.q = str2;
                this.r = str;
                if (IronSourceUtils.c((Context) activity)) {
                    this.j.post(this.y);
                } else {
                    this.k = true;
                    if (this.m == null) {
                        this.m = new NetworkStateReceiver(activity, this);
                    }
                    activity.getApplicationContext().registerReceiver(this.m, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            CountDownTimer unused = MediationInitializer.this.n = new CountDownTimer(60000, 15000) {
                                public void onFinish() {
                                    if (!MediationInitializer.this.h) {
                                        boolean unused = MediationInitializer.this.h = true;
                                        for (OnMediationInitializationListener a2 : MediationInitializer.this.o) {
                                            a2.a("noInternetConnection");
                                        }
                                        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.API, "Mediation availability false reason: No internet connection", 1);
                                    }
                                }

                                public void onTick(long j) {
                                    if (j <= 45000) {
                                        boolean unused = MediationInitializer.this.w = true;
                                        for (OnMediationInitializationListener a2 : MediationInitializer.this.o) {
                                            a2.a();
                                        }
                                    }
                                }
                            }.start();
                        }
                    });
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return;
    }

    public void a(boolean z2) {
        if (this.k && z2) {
            CountDownTimer countDownTimer = this.n;
            if (countDownTimer != null) {
                countDownTimer.cancel();
            }
            this.k = false;
            this.g = true;
            this.j.post(this.y);
        }
    }

    public synchronized EInitStatus a() {
        return this.t;
    }

    public void a(OnMediationInitializationListener onMediationInitializationListener) {
        if (onMediationInitializationListener != null) {
            this.o.add(onMediationInitializationListener);
        }
    }

    /* access modifiers changed from: private */
    public ConfigValidationResult a(String str) {
        ConfigValidationResult configValidationResult = new ConfigValidationResult();
        if (str == null) {
            configValidationResult.a(ErrorBuilder.a("userId", str, "it's missing"));
        } else if (!a(str, 1, 64)) {
            configValidationResult.a(ErrorBuilder.a("userId", str, (String) null));
        }
        return configValidationResult;
    }

    private boolean a(String str, int i2, int i3) {
        return str != null && str.length() >= i2 && str.length() <= i3;
    }
}
