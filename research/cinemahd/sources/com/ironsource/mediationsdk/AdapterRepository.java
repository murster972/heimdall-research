package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class AdapterRepository {
    private static AdapterRepository j = new AdapterRepository();
    private static final Object k = new Object();

    /* renamed from: a  reason: collision with root package name */
    private ConcurrentHashMap<String, AbstractAdapter> f4584a = new ConcurrentHashMap<>();
    private String b;
    private String c;
    private Boolean d;
    private Boolean e;
    private Integer f;
    private String g;
    private ConcurrentHashMap<String, String> h = new ConcurrentHashMap<>();
    private AtomicBoolean i = new AtomicBoolean(false);

    private AdapterRepository() {
    }

    public static AdapterRepository a() {
        return j;
    }

    private AbstractAdapter b(String str, String str2) {
        try {
            Class<?> cls = Class.forName("com.ironsource.adapters." + str2.toLowerCase() + "." + str2 + "Adapter");
            return (AbstractAdapter) cls.getMethod("startAdapter", new Class[]{String.class}).invoke(cls, new Object[]{str});
        } catch (Exception e2) {
            a("Error while loading adapter: " + e2.getLocalizedMessage());
            return null;
        }
    }

    private void c(AbstractAdapter abstractAdapter) {
        try {
            if (this.d != null) {
                abstractAdapter.setConsent(this.d.booleanValue());
            }
        } catch (Throwable th) {
            b("error while setting consent of " + abstractAdapter.getProviderName() + ": " + th.getLocalizedMessage());
            th.printStackTrace();
        }
    }

    private void d(AbstractAdapter abstractAdapter) {
        String str = this.g;
        if (str != null) {
            try {
                abstractAdapter.setGender(str);
            } catch (Throwable th) {
                b("error while setting gender of " + abstractAdapter.getProviderName() + ": " + th.getLocalizedMessage());
                th.printStackTrace();
            }
        }
    }

    private void e(AbstractAdapter abstractAdapter) {
        for (String next : this.h.keySet()) {
            try {
                abstractAdapter.setMetaData(next, this.h.get(next));
            } catch (Throwable th) {
                b("error while setting metadata of " + abstractAdapter.getProviderName() + ": " + th.getLocalizedMessage());
                th.printStackTrace();
            }
        }
    }

    public void a(String str, String str2) {
        this.b = str;
        this.c = str2;
    }

    public AbstractAdapter a(ProviderSettings providerSettings, JSONObject jSONObject, Activity activity) {
        return a(providerSettings, jSONObject, activity, false);
    }

    private void b(String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "AdapterRepository: " + str, 0);
    }

    public AbstractAdapter a(ProviderSettings providerSettings, JSONObject jSONObject, Activity activity, boolean z) {
        String str;
        String i2 = providerSettings.m() ? providerSettings.i() : providerSettings.h();
        if (z) {
            str = "IronSource";
        } else {
            str = providerSettings.i();
        }
        return a(i2, str, jSONObject, activity);
    }

    private void b(AbstractAdapter abstractAdapter) {
        Integer num = this.f;
        if (num != null) {
            try {
                abstractAdapter.setAge(num.intValue());
            } catch (Throwable th) {
                b("error while setting age of " + abstractAdapter.getProviderName() + ": " + th.getLocalizedMessage());
                th.printStackTrace();
            }
        }
    }

    private AbstractAdapter a(String str, String str2, JSONObject jSONObject, Activity activity) {
        b(str + " (" + str2 + ") - Getting adapter");
        synchronized (k) {
            if (this.f4584a.containsKey(str)) {
                b(str + " was already allocated");
                AbstractAdapter abstractAdapter = this.f4584a.get(str);
                return abstractAdapter;
            }
            AbstractAdapter b2 = b(str, str2);
            if (b2 == null) {
                a(str + " adapter was not loaded");
                return null;
            }
            b(str + " was allocated (adapter version: " + b2.getVersion() + ", sdk version: " + b2.getCoreSDKVersion() + ")");
            b2.setLogListener(IronSourceLoggerManager.c());
            e(b2);
            c(b2);
            b(b2);
            d(b2);
            a(b2);
            a(jSONObject, b2, str2, activity);
            this.f4584a.put(str, b2);
            return b2;
        }
    }

    private void a(JSONObject jSONObject, AbstractAdapter abstractAdapter, String str, Activity activity) {
        if ((str.equalsIgnoreCase("SupersonicAds") || str.equalsIgnoreCase("IronSource")) && this.i.compareAndSet(false, true)) {
            b("SDK5 earlyInit  <" + str + ">");
            abstractAdapter.earlyInit(activity, this.b, this.c, jSONObject);
        }
    }

    private void a(String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "AdapterRepository: " + str, 3);
    }

    private void a(AbstractAdapter abstractAdapter) {
        Boolean bool = this.e;
        if (bool != null) {
            try {
                abstractAdapter.setAdapterDebug(bool);
            } catch (Throwable th) {
                b("error while setting adapterDebug of " + abstractAdapter.getProviderName() + ": " + th.getLocalizedMessage());
                th.printStackTrace();
            }
        }
    }
}
