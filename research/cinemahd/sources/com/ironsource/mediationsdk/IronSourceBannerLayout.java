package com.ironsource.mediationsdk;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BannerListener;

public class IronSourceBannerLayout extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public View f4625a;
    private ISBannerSize b;
    private String c;
    private Activity d;
    private boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public BannerListener g;

    public IronSourceBannerLayout(Activity activity, ISBannerSize iSBannerSize) {
        super(activity);
        this.d = activity;
        this.b = iSBannerSize == null ? ISBannerSize.d : iSBannerSize;
    }

    public Activity getActivity() {
        return this.d;
    }

    public BannerListener getBannerListener() {
        return this.g;
    }

    public View getBannerView() {
        return this.f4625a;
    }

    public String getPlacementName() {
        return this.c;
    }

    public ISBannerSize getSize() {
        return this.b;
    }

    public void setBannerListener(BannerListener bannerListener) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.API, "setBannerListener()", 1);
        this.g = bannerListener;
    }

    public void setPlacementName(String str) {
        this.c = str;
    }

    public boolean b() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (this.g != null) {
            IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onBannerAdClicked()", 1);
            this.g.a();
        }
    }

    /* access modifiers changed from: protected */
    public void a() {
        this.e = true;
        this.g = null;
        this.d = null;
        this.b = null;
        this.c = null;
        this.f4625a = null;
    }

    /* access modifiers changed from: package-private */
    public void a(BannerSmash bannerSmash) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "onBannerAdLoaded() | internal | adapter: " + bannerSmash.e(), 0);
        if (this.g != null && !this.f) {
            IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onBannerAdLoaded()", 1);
            this.g.b();
        }
        this.f = true;
    }

    /* access modifiers changed from: package-private */
    public void a(final IronSourceError ironSourceError) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onBannerAdLoadFailed()  error=" + ironSourceError, 1);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                if (IronSourceBannerLayout.this.f) {
                    IronSourceBannerLayout.this.g.a(ironSourceError);
                    return;
                }
                try {
                    if (IronSourceBannerLayout.this.f4625a != null) {
                        IronSourceBannerLayout.this.removeView(IronSourceBannerLayout.this.f4625a);
                        View unused = IronSourceBannerLayout.this.f4625a = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (IronSourceBannerLayout.this.g != null) {
                    IronSourceBannerLayout.this.g.a(ironSourceError);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(final View view, final FrameLayout.LayoutParams layoutParams) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public void run() {
                IronSourceBannerLayout.this.removeAllViews();
                ViewParent parent = view.getParent();
                if (parent instanceof ViewGroup) {
                    ((ViewGroup) parent).removeView(view);
                }
                View unused = IronSourceBannerLayout.this.f4625a = view;
                IronSourceBannerLayout.this.addView(view, 0, layoutParams);
            }
        });
    }
}
