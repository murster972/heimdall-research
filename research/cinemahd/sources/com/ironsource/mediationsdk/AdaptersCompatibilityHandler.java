package com.ironsource.mediationsdk;

import com.applovin.sdk.AppLovinMediationProvider;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AdaptersCompatibilityHandler {
    private static final AdaptersCompatibilityHandler c = new AdaptersCompatibilityHandler();

    /* renamed from: a  reason: collision with root package name */
    private final ConcurrentHashMap<String, String> f4585a = new ConcurrentHashMap<>();
    private final ConcurrentHashMap<String, String> b;

    private AdaptersCompatibilityHandler() {
        this.f4585a.put("adcolony", "4.1.6");
        this.f4585a.put("vungle", "4.1.5");
        this.f4585a.put("applovin", "4.3.3");
        this.f4585a.put(AppLovinMediationProvider.ADMOB, "4.3.2");
        this.b = new ConcurrentHashMap<>();
        this.b.put("adcolony", "4.1.6");
        this.b.put(AppLovinMediationProvider.ADMOB, "4.3.2");
        this.b.put("applovin", "4.3.3");
        this.b.put("chartboost", "4.1.9");
        this.b.put(AppLovinMediationProvider.FYBER, "4.1.0");
        this.b.put("hyprmx", "4.1.2");
        this.b.put("inmobi", "4.3.1");
        this.b.put("maio", "4.1.3");
        this.b.put("mediabrix", "4.1.1");
        this.b.put(AppLovinMediationProvider.MOPUB, "3.2.0");
        this.b.put("tapjoy", "4.0.0");
        this.b.put("unityads", "4.1.4");
        this.b.put("vungle", "4.1.5");
    }

    public static AdaptersCompatibilityHandler a() {
        return c;
    }

    public boolean b(AbstractAdapter abstractAdapter) {
        return a(abstractAdapter, this.b, "rewarded video");
    }

    public boolean c(AbstractAdapter abstractAdapter) {
        if (abstractAdapter == null) {
            return false;
        }
        String version = abstractAdapter.getVersion();
        boolean a2 = a("4.3.0", version);
        if (!a2) {
            IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.API, abstractAdapter.getProviderName() + " adapter " + version + " is incompatible with SDK version " + IronSourceUtils.b() + ", please update your adapter to the latest version", 3);
        }
        return a2;
    }

    public boolean a(AbstractAdapter abstractAdapter) {
        return a(abstractAdapter, this.f4585a, "interstitial");
    }

    private boolean a(AbstractAdapter abstractAdapter, Map<String, String> map, String str) {
        if (abstractAdapter == null) {
            return false;
        }
        String lowerCase = abstractAdapter.getProviderName().toLowerCase();
        if (!map.containsKey(lowerCase)) {
            return true;
        }
        String version = abstractAdapter.getVersion();
        boolean a2 = a(map.get(lowerCase), version);
        if (!a2) {
            IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.API, abstractAdapter.getProviderName() + " adapter " + version + " is incompatible with SDK version " + IronSourceUtils.b() + " for " + str + " ad unit, please update your adapter to the latest version", 3);
        }
        return a2;
    }

    private boolean a(String str, String str2) {
        if (str.equalsIgnoreCase(str2)) {
            return true;
        }
        String[] split = str.split("\\.");
        String[] split2 = str2.split("\\.");
        for (int i = 0; i < 3; i++) {
            int parseInt = Integer.parseInt(split[i]);
            int parseInt2 = Integer.parseInt(split2[i]);
            if (parseInt2 < parseInt) {
                return false;
            }
            if (parseInt2 > parseInt) {
                return true;
            }
        }
        return true;
    }
}
