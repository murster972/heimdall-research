package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.DemandOnlyIsSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;

public interface DemandOnlyIsManagerListener {
    void a(DemandOnlyIsSmash demandOnlyIsSmash);

    void a(DemandOnlyIsSmash demandOnlyIsSmash, long j);

    void a(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash);

    void a(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash, long j);

    void b(DemandOnlyIsSmash demandOnlyIsSmash);

    void c(DemandOnlyIsSmash demandOnlyIsSmash);

    void d(DemandOnlyIsSmash demandOnlyIsSmash);
}
