package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;

public interface RewardedVideoListener {
    void a(Placement placement);

    void a(boolean z);

    void b(Placement placement);

    void c(IronSourceError ironSourceError);

    void onRewardedVideoAdClosed();

    void onRewardedVideoAdOpened();
}
