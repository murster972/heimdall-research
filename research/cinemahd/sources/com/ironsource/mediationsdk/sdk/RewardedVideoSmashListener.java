package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.logger.IronSourceError;

public interface RewardedVideoSmashListener {
    void a(boolean z);

    void c(IronSourceError ironSourceError);

    void d(IronSourceError ironSourceError);

    void f();

    void g();

    void h();

    void i();

    void j();

    void onRewardedVideoAdClosed();

    void onRewardedVideoAdOpened();
}
