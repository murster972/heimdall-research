package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.DemandOnlyRvSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;

public interface DemandOnlyRvManagerListener {
    void a(DemandOnlyRvSmash demandOnlyRvSmash);

    void a(DemandOnlyRvSmash demandOnlyRvSmash, long j);

    void a(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash);

    void a(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash, long j);

    void b(DemandOnlyRvSmash demandOnlyRvSmash);

    void c(DemandOnlyRvSmash demandOnlyRvSmash);

    void d(DemandOnlyRvSmash demandOnlyRvSmash);

    void e(DemandOnlyRvSmash demandOnlyRvSmash);
}
