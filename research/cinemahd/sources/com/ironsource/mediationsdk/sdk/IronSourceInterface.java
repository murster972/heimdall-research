package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.logger.LoggingApi;

public interface IronSourceInterface extends BaseApi, RewardedVideoApi, InterstitialApi, OfferwallApi, LoggingApi, RewardedInterstitialApi {
}
