package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.InterstitialSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;

public interface InterstitialManagerListener {
    void a(InterstitialSmash interstitialSmash);

    void a(InterstitialSmash interstitialSmash, long j);

    void a(IronSourceError ironSourceError, InterstitialSmash interstitialSmash);

    void a(IronSourceError ironSourceError, InterstitialSmash interstitialSmash, long j);

    void b(InterstitialSmash interstitialSmash);

    void b(IronSourceError ironSourceError, InterstitialSmash interstitialSmash);

    void c(InterstitialSmash interstitialSmash);

    void e(InterstitialSmash interstitialSmash);

    void f(InterstitialSmash interstitialSmash);

    void g(InterstitialSmash interstitialSmash);
}
