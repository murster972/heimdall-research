package com.ironsource.mediationsdk.sdk;

import android.app.Activity;
import org.json.JSONObject;

public interface OfferwallAdapterApi {
    void initOfferwall(Activity activity, String str, String str2, JSONObject jSONObject);

    void setInternalOfferwallListener(InternalOfferwallListener internalOfferwallListener);
}
