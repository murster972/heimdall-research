package com.ironsource.mediationsdk.sdk;

import android.view.View;
import android.widget.FrameLayout;
import com.ironsource.mediationsdk.logger.IronSourceError;

public interface BannerSmashListener {
    void a();

    void a(View view, FrameLayout.LayoutParams layoutParams);

    void a(IronSourceError ironSourceError);

    void b(IronSourceError ironSourceError);

    void onBannerInitSuccess();
}
