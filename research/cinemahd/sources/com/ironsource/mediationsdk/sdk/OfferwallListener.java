package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.logger.IronSourceError;

public interface OfferwallListener {
    boolean a(int i, int i2, boolean z);

    void b(boolean z);

    void d(IronSourceError ironSourceError);

    void e();

    void e(IronSourceError ironSourceError);

    void f();
}
