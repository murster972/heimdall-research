package com.ironsource.mediationsdk.sdk;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

public class ListenersWrapper implements RewardedVideoListener, InterstitialListener, InternalOfferwallListener, RewardedInterstitialListener, SegmentListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RewardedVideoListener f4705a;
    /* access modifiers changed from: private */
    public InterstitialListener b;
    /* access modifiers changed from: private */
    public OfferwallListener c;
    /* access modifiers changed from: private */
    public RewardedInterstitialListener d;
    /* access modifiers changed from: private */
    public SegmentListener e;
    private CallbackHandlerThread f = new CallbackHandlerThread();
    private InterstitialPlacement g = null;
    private String h = null;
    private long i;

    private class CallbackHandlerThread extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private Handler f4726a;

        private CallbackHandlerThread(ListenersWrapper listenersWrapper) {
        }

        public Handler a() {
            return this.f4726a;
        }

        public void run() {
            Looper.prepare();
            this.f4726a = new Handler();
            Looper.loop();
        }
    }

    public ListenersWrapper() {
        this.f.start();
        this.i = new Date().getTime();
    }

    public void f() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onOfferwallClosed()", 1);
        if (a((Object) this.c)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.c.f();
                }
            });
        }
    }

    public void k() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onInterstitialAdRewarded()", 1);
        if (a((Object) this.d)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.d.k();
                }
            });
        }
    }

    public void onInterstitialAdClicked() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onInterstitialAdClicked()", 1);
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.onInterstitialAdClicked();
                }
            });
        }
    }

    public void onRewardedVideoAdClosed() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onRewardedVideoAdClosed()", 1);
        if (a((Object) this.f4705a)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.f4705a.onRewardedVideoAdClosed();
                }
            });
        }
    }

    public void onRewardedVideoAdOpened() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onRewardedVideoAdOpened()", 1);
        if (a((Object) this.f4705a)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.f4705a.onRewardedVideoAdOpened();
                }
            });
        }
    }

    public void a(InterstitialPlacement interstitialPlacement) {
        this.g = interstitialPlacement;
    }

    public void b(String str) {
        this.h = str;
    }

    public void c(final IronSourceError ironSourceError) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onRewardedVideoAdShowFailed(" + ironSourceError.toString() + ")", 1);
        JSONObject a2 = IronSourceUtils.a(false);
        try {
            a2.put("errorCode", ironSourceError.a());
            a2.put("reason", ironSourceError.b());
            if (!TextUtils.isEmpty(this.h)) {
                a2.put("placement", this.h);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        RewardedVideoEventsManager.g().d(new EventData(1113, a2));
        if (a((Object) this.f4705a)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.f4705a.c(ironSourceError);
                }
            });
        }
    }

    public void d() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onInterstitialAdShowSucceeded()", 1);
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.d();
                }
            });
        }
    }

    public void e() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onOfferwallOpened()", 1);
        if (a((Object) this.c)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.c.e();
                }
            });
        }
    }

    private boolean a(Object obj) {
        return (obj == null || this.f == null) ? false : true;
    }

    public void b(final Placement placement) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onRewardedVideoAdClicked(" + placement.c() + ")", 1);
        if (a((Object) this.f4705a)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.f4705a.b(placement);
                }
            });
        }
    }

    private void a(Runnable runnable) {
        Handler a2;
        CallbackHandlerThread callbackHandlerThread = this.f;
        if (callbackHandlerThread != null && (a2 = callbackHandlerThread.a()) != null) {
            a2.post(runnable);
        }
    }

    public void d(final IronSourceError ironSourceError) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onGetOfferwallCreditsFailed(" + ironSourceError + ")", 1);
        if (a((Object) this.c)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.c.d(ironSourceError);
                }
            });
        }
    }

    public void e(final IronSourceError ironSourceError) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onOfferwallShowFailed(" + ironSourceError + ")", 1);
        if (a((Object) this.c)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.c.e(ironSourceError);
                }
            });
        }
    }

    public void b(final IronSourceError ironSourceError) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onInterstitialAdShowFailed(" + ironSourceError + ")", 1);
        JSONObject a2 = IronSourceUtils.a(false);
        try {
            a2.put("errorCode", ironSourceError.a());
            if (this.g != null && !TextUtils.isEmpty(this.g.c())) {
                a2.put("placement", this.g.c());
            }
            if (ironSourceError.b() != null) {
                a2.put("reason", ironSourceError.b());
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        InterstitialEventsManager.g().d(new EventData(2111, a2));
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.b(ironSourceError);
                }
            });
        }
    }

    public void a(InterstitialListener interstitialListener) {
        this.b = interstitialListener;
    }

    public void a(final String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onSegmentReceived(" + str + ")", 1);
        if (a((Object) this.e)) {
            a((Runnable) new Runnable() {
                public void run() {
                    if (!TextUtils.isEmpty(str)) {
                        ListenersWrapper.this.e.a(str);
                    }
                }
            });
        }
    }

    public void a(final boolean z) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onRewardedVideoAvailabilityChanged(available:" + z + ")", 1);
        long time = new Date().getTime() - this.i;
        this.i = new Date().getTime();
        JSONObject a2 = IronSourceUtils.a(false);
        try {
            a2.put("duration", time);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        RewardedVideoEventsManager.g().d(new EventData(z ? 1111 : 1112, a2));
        if (a((Object) this.f4705a)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.f4705a.a(z);
                }
            });
        }
    }

    public void c() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onInterstitialAdOpened()", 1);
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.c();
                }
            });
        }
    }

    public void b() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onInterstitialAdClosed()", 1);
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.b();
                }
            });
        }
    }

    public void a(final Placement placement) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onRewardedVideoAdRewarded(" + placement.toString() + ")", 1);
        if (a((Object) this.f4705a)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.f4705a.a(placement);
                }
            });
        }
    }

    public void b(boolean z) {
        a(z, (IronSourceError) null);
    }

    public void a() {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, "onInterstitialAdReady()", 1);
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.a();
                }
            });
        }
    }

    public void a(final IronSourceError ironSourceError) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onInterstitialAdLoadFailed(" + ironSourceError + ")", 1);
        if (a((Object) this.b)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.b.a(ironSourceError);
                }
            });
        }
    }

    public boolean a(int i2, int i3, boolean z) {
        OfferwallListener offerwallListener = this.c;
        boolean a2 = offerwallListener != null ? offerwallListener.a(i2, i3, z) : false;
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.CALLBACK;
        c2.b(ironSourceTag, "onOfferwallAdCredited(credits:" + i2 + ", totalCredits:" + i3 + ", totalCreditsFlag:" + z + "):" + a2, 1);
        return a2;
    }

    public void a(final boolean z, IronSourceError ironSourceError) {
        String str = "onOfferwallAvailable(isAvailable: " + z + ")";
        if (ironSourceError != null) {
            str = str + ", error: " + ironSourceError.b();
        }
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, str, 1);
        JSONObject a2 = IronSourceUtils.a(false);
        try {
            a2.put(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, String.valueOf(z));
            if (ironSourceError != null) {
                a2.put("errorCode", ironSourceError.a());
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        RewardedVideoEventsManager.g().d(new EventData(302, a2));
        if (a((Object) this.c)) {
            a((Runnable) new Runnable() {
                public void run() {
                    ListenersWrapper.this.c.b(z);
                }
            });
        }
    }
}
