package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.RewardedVideoSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;

public interface RewardedVideoManagerListener {
    void a(RewardedVideoSmash rewardedVideoSmash);

    void a(IronSourceError ironSourceError, RewardedVideoSmash rewardedVideoSmash);

    void a(boolean z, RewardedVideoSmash rewardedVideoSmash);

    void b(RewardedVideoSmash rewardedVideoSmash);

    void c(RewardedVideoSmash rewardedVideoSmash);

    void d(RewardedVideoSmash rewardedVideoSmash);

    void e(RewardedVideoSmash rewardedVideoSmash);
}
