package com.ironsource.mediationsdk.sdk;

import java.util.Map;
import org.json.JSONObject;

public class GeneralProperties {
    private static GeneralProperties b;

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f4704a = new JSONObject();

    private GeneralProperties() {
    }

    public static synchronized GeneralProperties b() {
        GeneralProperties generalProperties;
        synchronized (GeneralProperties.class) {
            if (b == null) {
                b = new GeneralProperties();
            }
            generalProperties = b;
        }
        return generalProperties;
    }

    public synchronized void a(Map<String, Object> map) {
        if (map != null) {
            for (String next : map.keySet()) {
                a(next, map.get(next));
            }
        }
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.lang.String r2, java.lang.Object r3) {
        /*
            r1 = this;
            monitor-enter(r1)
            org.json.JSONObject r0 = r1.f4704a     // Catch:{ Exception -> 0x000a, all -> 0x0007 }
            r0.put(r2, r3)     // Catch:{ Exception -> 0x000a, all -> 0x0007 }
            goto L_0x000a
        L_0x0007:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        L_0x000a:
            monitor-exit(r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.sdk.GeneralProperties.a(java.lang.String, java.lang.Object):void");
    }

    public synchronized JSONObject a() {
        return this.f4704a;
    }
}
