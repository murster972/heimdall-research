package com.ironsource.mediationsdk.sdk;

import android.view.View;
import android.widget.FrameLayout;
import com.ironsource.mediationsdk.BannerSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;

public interface BannerManagerListener {
    void a(BannerSmash bannerSmash);

    void a(BannerSmash bannerSmash, View view, FrameLayout.LayoutParams layoutParams);

    void a(IronSourceError ironSourceError, BannerSmash bannerSmash, boolean z);

    void b(BannerSmash bannerSmash);

    void b(IronSourceError ironSourceError, BannerSmash bannerSmash, boolean z);
}
