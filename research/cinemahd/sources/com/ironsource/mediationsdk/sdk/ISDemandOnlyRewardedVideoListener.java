package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.logger.IronSourceError;

public interface ISDemandOnlyRewardedVideoListener {
    void a(String str);

    void a(String str, IronSourceError ironSourceError);

    void b(String str);

    void b(String str, IronSourceError ironSourceError);

    void c(String str);

    void d(String str);

    void e(String str);
}
