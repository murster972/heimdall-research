package com.ironsource.mediationsdk.sdk;

import com.ironsource.mediationsdk.logger.IronSourceError;

public interface InterstitialListener {
    void a();

    void a(IronSourceError ironSourceError);

    void b();

    void b(IronSourceError ironSourceError);

    void c();

    void d();

    void onInterstitialAdClicked();
}
