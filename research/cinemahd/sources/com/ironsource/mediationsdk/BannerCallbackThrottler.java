package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;

public class BannerCallbackThrottler {
    private static BannerCallbackThrottler d;

    /* renamed from: a  reason: collision with root package name */
    private long f4589a = 0;
    private boolean b = false;
    private int c;

    private BannerCallbackThrottler() {
    }

    public static synchronized BannerCallbackThrottler b() {
        BannerCallbackThrottler bannerCallbackThrottler;
        synchronized (BannerCallbackThrottler.class) {
            if (d == null) {
                d = new BannerCallbackThrottler();
            }
            bannerCallbackThrottler = d;
        }
        return bannerCallbackThrottler;
    }

    public void a(final IronSourceBannerLayout ironSourceBannerLayout, final IronSourceError ironSourceError) {
        synchronized (this) {
            if (!this.b) {
                long currentTimeMillis = System.currentTimeMillis() - this.f4589a;
                if (currentTimeMillis > ((long) (this.c * 1000))) {
                    b(ironSourceBannerLayout, ironSourceError);
                    return;
                }
                this.b = true;
                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        BannerCallbackThrottler.this.b(ironSourceBannerLayout, ironSourceError);
                    }
                }, ((long) (this.c * 1000)) - currentTimeMillis);
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(IronSourceBannerLayout ironSourceBannerLayout, IronSourceError ironSourceError) {
        this.f4589a = System.currentTimeMillis();
        this.b = false;
        ironSourceBannerLayout.a(ironSourceError);
    }

    public boolean a() {
        boolean z;
        synchronized (this) {
            z = this.b;
        }
        return z;
    }

    public void a(int i) {
        this.c = i;
    }
}
