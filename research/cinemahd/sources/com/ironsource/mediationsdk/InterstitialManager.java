package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.MediationInitializer;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.InterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialApi;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialManagerListener;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.DailyCappingListener;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;
import org.json.JSONObject;

class InterstitialManager extends AbstractAdUnitManager implements InterstitialManagerListener, MediationInitializer.OnMediationInitializationListener, RewardedInterstitialManagerListener, RewardedInterstitialApi, DailyCappingListener {
    private final String p = InterstitialManager.class.getName();
    private ListenersWrapper q;
    private RewardedInterstitialListener r;
    private boolean s;
    private boolean t;
    private boolean u;
    private InterstitialPlacement v;
    private CallbackThrottler w;
    private boolean x;
    private long y;
    private boolean z;

    InterstitialManager() {
        new CopyOnWriteArraySet();
        new ConcurrentHashMap();
        this.w = CallbackThrottler.b();
        this.x = false;
        this.t = false;
        this.s = false;
        this.f4581a = new DailyCappingManager("interstitial", this);
        this.z = false;
    }

    private synchronized AbstractAdapter i(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.b(ironSourceTag, this.p + ":startAdapter(" + interstitialSmash.u() + ")", 1);
        AbstractAdapter a2 = AdapterRepository.a().a(interstitialSmash.c, interstitialSmash.c.f(), this.f);
        if (a2 == null) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager2.b(ironSourceTag2, interstitialSmash.p() + " is configured in IronSource's platform, but the adapter is not integrated", 2);
            return null;
        }
        interstitialSmash.a(a2);
        interstitialSmash.a(AbstractSmash.MEDIATION_STATE.INIT_PENDING);
        if (this.r != null) {
            interstitialSmash.a((RewardedInterstitialManagerListener) this);
        }
        c(interstitialSmash);
        try {
            interstitialSmash.a(this.f, this.h, this.g);
            return a2;
        } catch (Throwable th) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager3.a(ironSourceTag3, this.p + "failed to init adapter: " + interstitialSmash.u() + "v", th);
            interstitialSmash.a(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            return null;
        }
    }

    private void j() {
        if (k()) {
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "Reset Iteration", 0);
            Iterator<AbstractSmash> it2 = this.c.iterator();
            while (it2.hasNext()) {
                AbstractSmash next = it2.next();
                if (next.t() == AbstractSmash.MEDIATION_STATE.EXHAUSTED) {
                    next.l();
                }
            }
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "End of Reset Iteration", 0);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x000c  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean k() {
        /*
            r4 = this;
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r0 = r4.c
            java.util.Iterator r0 = r0.iterator()
        L_0x0006:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x003c
            java.lang.Object r1 = r0.next()
            com.ironsource.mediationsdk.AbstractSmash r1 = (com.ironsource.mediationsdk.AbstractSmash) r1
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_INITIATED
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INIT_PENDING
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING
            if (r2 == r3) goto L_0x003a
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = r1.t()
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE
            if (r1 != r2) goto L_0x0006
        L_0x003a:
            r0 = 0
            return r0
        L_0x003c:
            r0 = 1
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.InterstitialManager.k():boolean");
    }

    private void l() {
        for (int i = 0; i < this.c.size(); i++) {
            String i2 = this.c.get(i).c.i();
            if (i2.equalsIgnoreCase("IronSource") || i2.equalsIgnoreCase("SupersonicAds")) {
                AdapterRepository.a().a(this.c.get(i).c, this.c.get(i).c.f(), this.f);
                return;
            }
        }
    }

    private AbstractAdapter m() {
        AbstractAdapter abstractAdapter = null;
        int i = 0;
        for (int i2 = 0; i2 < this.c.size() && abstractAdapter == null; i2++) {
            if (this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.AVAILABLE || this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.INITIATED || this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.INIT_PENDING || this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.LOAD_PENDING) {
                i++;
                if (i >= this.b) {
                    break;
                }
            } else if (this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.NOT_INITIATED && (abstractAdapter = i((InterstitialSmash) this.c.get(i2))) == null) {
                this.c.get(i2).a(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            }
        }
        return abstractAdapter;
    }

    public void a(ListenersWrapper listenersWrapper) {
        this.q = listenersWrapper;
        this.w.a((InterstitialListener) listenersWrapper);
    }

    public void a(List<IronSource.AD_UNIT> list, boolean z2) {
    }

    public void b(String str) {
        Activity activity;
        if (this.z) {
            this.i.b(IronSourceLogger.IronSourceTag.API, "showInterstitial error: can't show ad while an ad is already showing", 3);
            this.q.b(new IronSourceError(1036, "showInterstitial error: can't show ad while an ad is already showing"));
        } else if (this.j && (activity = this.f) != null && !IronSourceUtils.c((Context) activity)) {
            this.i.b(IronSourceLogger.IronSourceTag.API, "showInterstitial error: can't show ad when there's no internet connection", 3);
            this.q.b(ErrorBuilder.e("Interstitial"));
        } else if (!this.s) {
            this.i.b(IronSourceLogger.IronSourceTag.API, "showInterstitial failed - You need to load interstitial before showing it", 3);
            this.q.b(ErrorBuilder.b("Interstitial", "showInterstitial failed - You need to load interstitial before showing it"));
        } else {
            for (int i = 0; i < this.c.size(); i++) {
                AbstractSmash abstractSmash = this.c.get(i);
                if (abstractSmash.t() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                    CappingManager.b((Context) this.f, this.v);
                    if (CappingManager.c((Context) this.f, this.v) != CappingManager.ECappingStatus.NOT_CAPPED) {
                        b(2400, (Object[][]) null);
                    }
                    b(2201, abstractSmash, (Object[][]) null);
                    this.z = true;
                    ((InterstitialSmash) abstractSmash).G();
                    if (abstractSmash.y()) {
                        a(2401, abstractSmash);
                    }
                    this.f4581a.b(abstractSmash);
                    if (this.f4581a.c(abstractSmash)) {
                        abstractSmash.a(AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY);
                        a(250, abstractSmash, new Object[][]{new Object[]{ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, "true"}});
                    }
                    this.s = false;
                    if (!abstractSmash.A()) {
                        m();
                        return;
                    }
                    return;
                }
            }
            this.q.b(ErrorBuilder.b("Interstitial", "showInterstitial failed - No adapters ready to show"));
        }
    }

    public void c(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdClicked()", 1);
        b(2006, interstitialSmash, (Object[][]) null);
        this.q.onInterstitialAdClicked();
    }

    public void d(InterstitialSmash interstitialSmash) {
        a(290, (AbstractSmash) interstitialSmash, (Object[][]) null);
        RewardedInterstitialListener rewardedInterstitialListener = this.r;
        if (rewardedInterstitialListener != null) {
            rewardedInterstitialListener.k();
        }
    }

    public void e(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdVisible()", 1);
    }

    public void f(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdClosed()", 1);
        this.z = false;
        f();
        b(2204, interstitialSmash, (Object[][]) null);
        this.q.b();
    }

    public synchronized boolean g() {
        if (this.j && this.f != null && !IronSourceUtils.c((Context) this.f)) {
            return false;
        }
        Iterator<AbstractSmash> it2 = this.c.iterator();
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (next.t() == AbstractSmash.MEDIATION_STATE.AVAILABLE && ((InterstitialSmash) next).E()) {
                return true;
            }
        }
        return false;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x008b, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:75:0x01b7, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void h() {
        /*
            r10 = this;
            monitor-enter(r10)
            r0 = 2110(0x83e, float:2.957E-42)
            r1 = 2
            r2 = 3
            r3 = 0
            r4 = 1
            boolean r5 = r10.z     // Catch:{ Exception -> 0x015e }
            if (r5 == 0) goto L_0x0024
            java.lang.String r5 = "loadInterstitial cannot be invoked while showing an ad"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            r6.b(r7, r5, r2)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceError r6 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Exception -> 0x015e }
            r7 = 1037(0x40d, float:1.453E-42)
            r6.<init>(r7, r5)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.ISListenerWrapper r5 = com.ironsource.mediationsdk.ISListenerWrapper.f()     // Catch:{ Exception -> 0x015e }
            r5.a((com.ironsource.mediationsdk.logger.IronSourceError) r6)     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x0024:
            r5 = 0
            r10.v = r5     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r6 = r10.q     // Catch:{ Exception -> 0x015e }
            r6.a((com.ironsource.mediationsdk.model.InterstitialPlacement) r5)     // Catch:{ Exception -> 0x015e }
            boolean r6 = r10.t     // Catch:{ Exception -> 0x015e }
            if (r6 != 0) goto L_0x0151
            com.ironsource.mediationsdk.CallbackThrottler r6 = r10.w     // Catch:{ Exception -> 0x015e }
            boolean r6 = r6.a()     // Catch:{ Exception -> 0x015e }
            if (r6 == 0) goto L_0x003a
            goto L_0x0151
        L_0x003a:
            com.ironsource.mediationsdk.MediationInitializer r6 = com.ironsource.mediationsdk.MediationInitializer.d()     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r6 = r6.a()     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r7 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.NOT_INIT     // Catch:{ Exception -> 0x015e }
            if (r6 != r7) goto L_0x0051
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            java.lang.String r7 = "init() must be called before loadInterstitial()"
            r5.b(r6, r7, r2)     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x0051:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r7 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_IN_PROGRESS     // Catch:{ Exception -> 0x015e }
            r8 = 2001(0x7d1, float:2.804E-42)
            if (r6 != r7) goto L_0x008c
            com.ironsource.mediationsdk.MediationInitializer r6 = com.ironsource.mediationsdk.MediationInitializer.d()     // Catch:{ Exception -> 0x015e }
            boolean r6 = r6.b()     // Catch:{ Exception -> 0x015e }
            if (r6 == 0) goto L_0x0078
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            java.lang.String r7 = "init() had failed"
            r5.b(r6, r7, r2)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.CallbackThrottler r5 = r10.w     // Catch:{ Exception -> 0x015e }
            java.lang.String r6 = "init() had failed"
            java.lang.String r7 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.a(r6, r7)     // Catch:{ Exception -> 0x015e }
            r5.a((com.ironsource.mediationsdk.logger.IronSourceError) r6)     // Catch:{ Exception -> 0x015e }
            goto L_0x008a
        L_0x0078:
            java.util.Date r6 = new java.util.Date     // Catch:{ Exception -> 0x015e }
            r6.<init>()     // Catch:{ Exception -> 0x015e }
            long r6 = r6.getTime()     // Catch:{ Exception -> 0x015e }
            r10.y = r6     // Catch:{ Exception -> 0x015e }
            r10.a((int) r8, (java.lang.Object[][]) r5)     // Catch:{ Exception -> 0x015e }
            r10.s = r4     // Catch:{ Exception -> 0x015e }
            r10.x = r4     // Catch:{ Exception -> 0x015e }
        L_0x008a:
            monitor-exit(r10)
            return
        L_0x008c:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r7 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_FAILED     // Catch:{ Exception -> 0x015e }
            if (r6 != r7) goto L_0x00a8
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            java.lang.String r7 = "init() had failed"
            r5.b(r6, r7, r2)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.CallbackThrottler r5 = r10.w     // Catch:{ Exception -> 0x015e }
            java.lang.String r6 = "init() had failed"
            java.lang.String r7 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.a(r6, r7)     // Catch:{ Exception -> 0x015e }
            r5.a((com.ironsource.mediationsdk.logger.IronSourceError) r6)     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x00a8:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r6 = r10.c     // Catch:{ Exception -> 0x015e }
            int r6 = r6.size()     // Catch:{ Exception -> 0x015e }
            if (r6 != 0) goto L_0x00c8
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            java.lang.String r7 = "the server response does not contain interstitial data"
            r5.b(r6, r7, r2)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.CallbackThrottler r5 = r10.w     // Catch:{ Exception -> 0x015e }
            java.lang.String r6 = "the server response does not contain interstitial data"
            java.lang.String r7 = "Interstitial"
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.a(r6, r7)     // Catch:{ Exception -> 0x015e }
            r5.a((com.ironsource.mediationsdk.logger.IronSourceError) r6)     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x00c8:
            java.util.Date r6 = new java.util.Date     // Catch:{ Exception -> 0x015e }
            r6.<init>()     // Catch:{ Exception -> 0x015e }
            long r6 = r6.getTime()     // Catch:{ Exception -> 0x015e }
            r10.y = r6     // Catch:{ Exception -> 0x015e }
            r10.a((int) r8, (java.lang.Object[][]) r5)     // Catch:{ Exception -> 0x015e }
            r10.x = r4     // Catch:{ Exception -> 0x015e }
            r10.i()     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r5 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r4]     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r6 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ Exception -> 0x015e }
            r5[r3] = r6     // Catch:{ Exception -> 0x015e }
            int r5 = r10.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[]) r5)     // Catch:{ Exception -> 0x015e }
            if (r5 != 0) goto L_0x0120
            boolean r5 = r10.u     // Catch:{ Exception -> 0x015e }
            if (r5 != 0) goto L_0x00ef
            r10.s = r4     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x00ef:
            java.lang.String r5 = "no ads to load"
            com.ironsource.mediationsdk.logger.IronSourceError r5 = com.ironsource.mediationsdk.utils.ErrorBuilder.b(r5)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            java.lang.String r8 = r5.b()     // Catch:{ Exception -> 0x015e }
            r6.b(r7, r8, r4)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.CallbackThrottler r6 = r10.w     // Catch:{ Exception -> 0x015e }
            r6.a((com.ironsource.mediationsdk.logger.IronSourceError) r5)     // Catch:{ Exception -> 0x015e }
            java.lang.Object[][] r6 = new java.lang.Object[r4][]     // Catch:{ Exception -> 0x015e }
            java.lang.Object[] r7 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x015e }
            java.lang.String r8 = "errorCode"
            r7[r3] = r8     // Catch:{ Exception -> 0x015e }
            int r5 = r5.a()     // Catch:{ Exception -> 0x015e }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ Exception -> 0x015e }
            r7[r4] = r5     // Catch:{ Exception -> 0x015e }
            r6[r3] = r7     // Catch:{ Exception -> 0x015e }
            r10.a((int) r0, (java.lang.Object[][]) r6)     // Catch:{ Exception -> 0x015e }
            r10.x = r3     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x0120:
            r10.s = r4     // Catch:{ Exception -> 0x015e }
            r10.t = r4     // Catch:{ Exception -> 0x015e }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r5 = r10.c     // Catch:{ Exception -> 0x015e }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ Exception -> 0x015e }
            r6 = 0
        L_0x012b:
            boolean r7 = r5.hasNext()     // Catch:{ Exception -> 0x015e }
            if (r7 == 0) goto L_0x01b6
            java.lang.Object r7 = r5.next()     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.AbstractSmash r7 = (com.ironsource.mediationsdk.AbstractSmash) r7     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = r7.t()     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ Exception -> 0x015e }
            if (r8 != r9) goto L_0x012b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ Exception -> 0x015e }
            r7.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE) r8)     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.InterstitialSmash r7 = (com.ironsource.mediationsdk.InterstitialSmash) r7     // Catch:{ Exception -> 0x015e }
            r10.h(r7)     // Catch:{ Exception -> 0x015e }
            int r6 = r6 + 1
            int r7 = r10.b     // Catch:{ Exception -> 0x015e }
            if (r6 < r7) goto L_0x012b
            monitor-exit(r10)
            return
        L_0x0151:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r10.i     // Catch:{ Exception -> 0x015e }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x015e }
            java.lang.String r7 = "Load Interstitial is already in progress"
            r5.b(r6, r7, r2)     // Catch:{ Exception -> 0x015e }
            monitor-exit(r10)
            return
        L_0x015c:
            r0 = move-exception
            goto L_0x01b8
        L_0x015e:
            r5 = move-exception
            r5.printStackTrace()     // Catch:{ all -> 0x015c }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x015c }
            r6.<init>()     // Catch:{ all -> 0x015c }
            java.lang.String r7 = "loadInterstitial exception "
            r6.append(r7)     // Catch:{ all -> 0x015c }
            java.lang.String r7 = r5.getMessage()     // Catch:{ all -> 0x015c }
            r6.append(r7)     // Catch:{ all -> 0x015c }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x015c }
            com.ironsource.mediationsdk.logger.IronSourceError r6 = com.ironsource.mediationsdk.utils.ErrorBuilder.c(r6)     // Catch:{ all -> 0x015c }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r10.i     // Catch:{ all -> 0x015c }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r8 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x015c }
            java.lang.String r9 = r6.b()     // Catch:{ all -> 0x015c }
            r7.b(r8, r9, r2)     // Catch:{ all -> 0x015c }
            com.ironsource.mediationsdk.CallbackThrottler r2 = r10.w     // Catch:{ all -> 0x015c }
            r2.a((com.ironsource.mediationsdk.logger.IronSourceError) r6)     // Catch:{ all -> 0x015c }
            boolean r2 = r10.x     // Catch:{ all -> 0x015c }
            if (r2 == 0) goto L_0x01b6
            r10.x = r3     // Catch:{ all -> 0x015c }
            java.lang.Object[][] r2 = new java.lang.Object[r1][]     // Catch:{ all -> 0x015c }
            java.lang.Object[] r7 = new java.lang.Object[r1]     // Catch:{ all -> 0x015c }
            java.lang.String r8 = "errorCode"
            r7[r3] = r8     // Catch:{ all -> 0x015c }
            int r6 = r6.a()     // Catch:{ all -> 0x015c }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x015c }
            r7[r4] = r6     // Catch:{ all -> 0x015c }
            r2[r3] = r7     // Catch:{ all -> 0x015c }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x015c }
            java.lang.String r6 = "reason"
            r1[r3] = r6     // Catch:{ all -> 0x015c }
            java.lang.String r3 = r5.getMessage()     // Catch:{ all -> 0x015c }
            r1[r4] = r3     // Catch:{ all -> 0x015c }
            r2[r4] = r1     // Catch:{ all -> 0x015c }
            r10.a((int) r0, (java.lang.Object[][]) r2)     // Catch:{ all -> 0x015c }
        L_0x01b6:
            monitor-exit(r10)
            return
        L_0x01b8:
            monitor-exit(r10)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.InterstitialManager.h():void");
    }

    public void a(RewardedInterstitialListener rewardedInterstitialListener) {
        this.r = rewardedInterstitialListener;
    }

    private void c(int i) {
        a(i, (Object[][]) null);
    }

    private void e(AbstractSmash abstractSmash) {
        if (!abstractSmash.A()) {
            m();
            j();
            return;
        }
        abstractSmash.a(AbstractSmash.MEDIATION_STATE.INITIATED);
    }

    public synchronized void a(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.b(ironSourceTag, this.p + ":initInterstitial(appKey: " + str + ", userId: " + str2 + ")", 1);
        long time = new Date().getTime();
        c(82312);
        this.h = str;
        this.g = str2;
        this.f = activity;
        this.f4581a.a((Context) this.f);
        Iterator<AbstractSmash> it2 = this.c.iterator();
        int i = 0;
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (this.f4581a.d(next)) {
                a(250, next, new Object[][]{new Object[]{ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, "false"}});
            }
            if (this.f4581a.c(next)) {
                next.a(AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY);
                i++;
            }
        }
        if (i == this.c.size()) {
            this.u = true;
        }
        l();
        int i2 = 0;
        while (true) {
            if (i2 >= this.b) {
                break;
            } else if (m() == null) {
                break;
            } else {
                i2++;
            }
        }
        a(82313, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
    }

    public void g(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdOpened()", 1);
        b(2005, interstitialSmash, (Object[][]) null);
        this.q.c();
    }

    private synchronized void i() {
        Iterator<AbstractSmash> it2 = this.c.iterator();
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (next.t() == AbstractSmash.MEDIATION_STATE.AVAILABLE || next.t() == AbstractSmash.MEDIATION_STATE.LOAD_PENDING || next.t() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE) {
                next.a(AbstractSmash.MEDIATION_STATE.INITIATED);
            }
        }
    }

    public synchronized void a(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + " :onInterstitialInitSuccess()", 1);
        a(2205, (AbstractSmash) interstitialSmash);
        this.u = true;
        if (this.s) {
            if (a(AbstractSmash.MEDIATION_STATE.AVAILABLE, AbstractSmash.MEDIATION_STATE.LOAD_PENDING) < this.b) {
                interstitialSmash.a(AbstractSmash.MEDIATION_STATE.LOAD_PENDING);
                h(interstitialSmash);
            }
        }
    }

    public synchronized void b(IronSourceError ironSourceError, InterstitialSmash interstitialSmash) {
        try {
            IronSourceLoggerManager ironSourceLoggerManager = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialInitFailed(" + ironSourceError + ")", 1);
            a(2206, (AbstractSmash) interstitialSmash, new Object[][]{new Object[]{"reason", ironSourceError.b()}});
            if (a(AbstractSmash.MEDIATION_STATE.INIT_FAILED) >= this.c.size()) {
                IronSourceLoggerManager ironSourceLoggerManager2 = this.i;
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.NATIVE;
                ironSourceLoggerManager2.b(ironSourceTag2, "Smart Loading - initialization failed - no adapters are initiated and no more left to init, error: " + ironSourceError.b(), 2);
                if (this.s) {
                    this.w.a(ErrorBuilder.b("no ads to show"));
                    a(2110, new Object[][]{new Object[]{"errorCode", 510}});
                    this.x = false;
                }
                this.u = true;
            } else {
                if (m() == null && this.s) {
                    if (a(AbstractSmash.MEDIATION_STATE.INIT_FAILED, AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE, AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION, AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY, AbstractSmash.MEDIATION_STATE.EXHAUSTED) >= this.c.size()) {
                        this.w.a(new IronSourceError(509, "No ads to show"));
                        a(2110, new Object[][]{new Object[]{"errorCode", 509}});
                        this.x = false;
                    }
                }
                j();
            }
        } catch (Exception e) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceLoggerManager3.a(ironSourceTag3, "onInterstitialInitFailed(error:" + ironSourceError + ", provider:" + interstitialSmash.u() + ")", (Throwable) e);
        }
        return;
    }

    public synchronized void a(InterstitialSmash interstitialSmash, long j) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdReady()", 1);
        a(2003, (AbstractSmash) interstitialSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        long time = new Date().getTime() - this.y;
        interstitialSmash.a(AbstractSmash.MEDIATION_STATE.AVAILABLE);
        this.t = false;
        if (this.x) {
            this.x = false;
            this.q.a();
            a(2004, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00ff, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.ironsource.mediationsdk.logger.IronSourceError r8, com.ironsource.mediationsdk.InterstitialSmash r9, long r10) {
        /*
            r7 = this;
            monitor-enter(r7)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r7.i     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r2.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = r9.p()     // Catch:{ all -> 0x0100 }
            r2.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = ":onInterstitialAdLoadFailed("
            r2.append(r3)     // Catch:{ all -> 0x0100 }
            r2.append(r8)     // Catch:{ all -> 0x0100 }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x0100 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0100 }
            r3 = 1
            r0.b(r1, r2, r3)     // Catch:{ all -> 0x0100 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0100 }
            r0.<init>()     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = r9.p()     // Catch:{ all -> 0x0100 }
            r0.append(r1)     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = ":onInterstitialAdLoadFailed("
            r0.append(r1)     // Catch:{ all -> 0x0100 }
            r0.append(r8)     // Catch:{ all -> 0x0100 }
            java.lang.String r1 = ")"
            r0.append(r1)     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.utils.IronSourceUtils.g(r0)     // Catch:{ all -> 0x0100 }
            r0 = 2200(0x898, float:3.083E-42)
            r1 = 3
            java.lang.Object[][] r1 = new java.lang.Object[r1][]     // Catch:{ all -> 0x0100 }
            r2 = 2
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r5 = "errorCode"
            r6 = 0
            r4[r6] = r5     // Catch:{ all -> 0x0100 }
            int r5 = r8.a()     // Catch:{ all -> 0x0100 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x0100 }
            r4[r3] = r5     // Catch:{ all -> 0x0100 }
            r1[r6] = r4     // Catch:{ all -> 0x0100 }
            java.lang.Object[] r4 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r5 = "reason"
            r4[r6] = r5     // Catch:{ all -> 0x0100 }
            java.lang.String r8 = r8.b()     // Catch:{ all -> 0x0100 }
            r4[r3] = r8     // Catch:{ all -> 0x0100 }
            r1[r3] = r4     // Catch:{ all -> 0x0100 }
            java.lang.Object[] r8 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r4 = "duration"
            r8[r6] = r4     // Catch:{ all -> 0x0100 }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x0100 }
            r8[r3] = r10     // Catch:{ all -> 0x0100 }
            r1[r2] = r8     // Catch:{ all -> 0x0100 }
            r7.a((int) r0, (com.ironsource.mediationsdk.AbstractSmash) r9, (java.lang.Object[][]) r1)     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0100 }
            r9.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE) r8)     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r8 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r2]     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x0100 }
            r8[r6] = r9     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r9 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ all -> 0x0100 }
            r8[r3] = r9     // Catch:{ all -> 0x0100 }
            int r8 = r7.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[]) r8)     // Catch:{ all -> 0x0100 }
            int r9 = r7.b     // Catch:{ all -> 0x0100 }
            if (r8 < r9) goto L_0x0097
            monitor-exit(r7)
            return
        L_0x0097:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r9 = r7.c     // Catch:{ all -> 0x0100 }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x0100 }
        L_0x009d:
            boolean r10 = r9.hasNext()     // Catch:{ all -> 0x0100 }
            if (r10 == 0) goto L_0x00bd
            java.lang.Object r10 = r9.next()     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash r10 = (com.ironsource.mediationsdk.AbstractSmash) r10     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r11 = r10.t()     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r0 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ all -> 0x0100 }
            if (r11 != r0) goto L_0x009d
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r8 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ all -> 0x0100 }
            r10.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE) r8)     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.InterstitialSmash r10 = (com.ironsource.mediationsdk.InterstitialSmash) r10     // Catch:{ all -> 0x0100 }
            r7.h(r10)     // Catch:{ all -> 0x0100 }
            monitor-exit(r7)
            return
        L_0x00bd:
            com.ironsource.mediationsdk.AbstractAdapter r9 = r7.m()     // Catch:{ all -> 0x0100 }
            if (r9 == 0) goto L_0x00c5
            monitor-exit(r7)
            return
        L_0x00c5:
            boolean r9 = r7.s     // Catch:{ all -> 0x0100 }
            if (r9 == 0) goto L_0x00fe
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE[] r9 = new com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[r3]     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r10 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INIT_PENDING     // Catch:{ all -> 0x0100 }
            r9[r6] = r10     // Catch:{ all -> 0x0100 }
            int r9 = r7.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE[]) r9)     // Catch:{ all -> 0x0100 }
            int r8 = r8 + r9
            if (r8 != 0) goto L_0x00fe
            r7.j()     // Catch:{ all -> 0x0100 }
            r7.t = r6     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.CallbackThrottler r8 = r7.w     // Catch:{ all -> 0x0100 }
            com.ironsource.mediationsdk.logger.IronSourceError r9 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x0100 }
            java.lang.String r10 = "No ads to show"
            r11 = 509(0x1fd, float:7.13E-43)
            r9.<init>(r11, r10)     // Catch:{ all -> 0x0100 }
            r8.a((com.ironsource.mediationsdk.logger.IronSourceError) r9)     // Catch:{ all -> 0x0100 }
            r8 = 2110(0x83e, float:2.957E-42)
            java.lang.Object[][] r9 = new java.lang.Object[r3][]     // Catch:{ all -> 0x0100 }
            java.lang.Object[] r10 = new java.lang.Object[r2]     // Catch:{ all -> 0x0100 }
            java.lang.String r0 = "errorCode"
            r10[r6] = r0     // Catch:{ all -> 0x0100 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ all -> 0x0100 }
            r10[r3] = r11     // Catch:{ all -> 0x0100 }
            r9[r6] = r10     // Catch:{ all -> 0x0100 }
            r7.a((int) r8, (java.lang.Object[][]) r9)     // Catch:{ all -> 0x0100 }
        L_0x00fe:
            monitor-exit(r7)
            return
        L_0x0100:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.InterstitialManager.a(com.ironsource.mediationsdk.logger.IronSourceError, com.ironsource.mediationsdk.InterstitialSmash, long):void");
    }

    public void b(InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdShowSucceeded()", 1);
        b(2202, interstitialSmash, (Object[][]) null);
        Iterator<AbstractSmash> it2 = this.c.iterator();
        boolean z2 = false;
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (next.t() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                e(next);
                z2 = true;
            }
        }
        if (!z2 && (interstitialSmash.t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION || interstitialSmash.t() == AbstractSmash.MEDIATION_STATE.EXHAUSTED || interstitialSmash.t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY)) {
            j();
        }
        i();
        this.q.d();
    }

    private void b(int i, Object[][] objArr) {
        a(i, objArr, true);
    }

    private void b(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        a(i, abstractSmash, objArr, true);
    }

    public void a(IronSourceError ironSourceError, InterstitialSmash interstitialSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, interstitialSmash.p() + ":onInterstitialAdShowFailed(" + ironSourceError + ")", 1);
        b(2203, interstitialSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}});
        this.z = false;
        e((AbstractSmash) interstitialSmash);
        Iterator<AbstractSmash> it2 = this.c.iterator();
        while (it2.hasNext()) {
            if (it2.next().t() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                this.s = true;
                InterstitialPlacement interstitialPlacement = this.v;
                b(interstitialPlacement != null ? interstitialPlacement.c() : "");
                return;
            }
        }
        this.q.b(ironSourceError);
    }

    public void b() {
        CopyOnWriteArrayList<AbstractSmash> copyOnWriteArrayList = this.c;
        if (copyOnWriteArrayList != null) {
            Iterator<AbstractSmash> it2 = copyOnWriteArrayList.iterator();
            while (it2.hasNext()) {
                AbstractSmash next = it2.next();
                if (next.t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY) {
                    a(250, next, new Object[][]{new Object[]{ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, "false"}});
                    if (next.y()) {
                        next.a(AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION);
                    } else if (next.z()) {
                        next.a(AbstractSmash.MEDIATION_STATE.EXHAUSTED);
                    } else {
                        next.a(AbstractSmash.MEDIATION_STATE.INITIATED);
                    }
                }
            }
        }
    }

    private synchronized void h(InterstitialSmash interstitialSmash) {
        a((int) AdError.CACHE_ERROR_CODE, (AbstractSmash) interstitialSmash, (Object[][]) null);
        interstitialSmash.F();
    }

    public void b(int i) {
        this.w.a(i);
    }

    public void a(String str) {
        if (this.s) {
            this.w.a(ErrorBuilder.a("init() had failed", "Interstitial"));
            this.s = false;
            this.t = false;
        }
    }

    public void a() {
        if (this.s) {
            IronSourceError a2 = ErrorBuilder.a("init() had failed", "Interstitial");
            this.w.a(a2);
            this.s = false;
            this.t = false;
            if (this.x) {
                a(2110, new Object[][]{new Object[]{"errorCode", Integer.valueOf(a2.a())}});
                this.x = false;
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(InterstitialPlacement interstitialPlacement) {
        this.v = interstitialPlacement;
        this.q.a(interstitialPlacement);
    }

    private void a(int i, Object[][] objArr) {
        a(i, objArr, false);
    }

    private void a(int i, Object[][] objArr, boolean z2) {
        JSONObject a2 = IronSourceUtils.a(false);
        if (z2) {
            try {
                if (this.v != null && !TextUtils.isEmpty(this.v.c())) {
                    a2.put("placement", this.v.c());
                }
            } catch (Exception e) {
                this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "InterstitialManager logMediationEvent " + Log.getStackTraceString(e), 3);
            }
        }
        if (objArr != null) {
            for (Object[] objArr2 : objArr) {
                a2.put(objArr2[0].toString(), objArr2[1]);
            }
        }
        InterstitialEventsManager.g().d(new EventData(i, a2));
    }

    private void a(int i, AbstractSmash abstractSmash) {
        a(i, abstractSmash, (Object[][]) null);
    }

    private void a(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        a(i, abstractSmash, objArr, false);
    }

    private void a(int i, AbstractSmash abstractSmash, Object[][] objArr, boolean z2) {
        JSONObject a2 = IronSourceUtils.a(abstractSmash);
        if (z2) {
            try {
                if (this.v != null && !TextUtils.isEmpty(this.v.c())) {
                    a2.put("placement", this.v.c());
                }
            } catch (Exception e) {
                this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "InterstitialManager logProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        if (objArr != null) {
            for (Object[] objArr2 : objArr) {
                a2.put(objArr2[0].toString(), objArr2[1]);
            }
        }
        InterstitialEventsManager.g().d(new EventData(i, a2));
    }

    private int a(AbstractSmash.MEDIATION_STATE... mediation_stateArr) {
        int i;
        synchronized (this.c) {
            Iterator<AbstractSmash> it2 = this.c.iterator();
            i = 0;
            while (it2.hasNext()) {
                AbstractSmash next = it2.next();
                int i2 = i;
                for (AbstractSmash.MEDIATION_STATE mediation_state : mediation_stateArr) {
                    if (next.t() == mediation_state) {
                        i2++;
                    }
                }
                i = i2;
            }
        }
        return i;
    }
}
