package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.DemandOnlySmash;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import java.util.Date;
import java.util.TimerTask;

public class DemandOnlyRvSmash extends DemandOnlySmash implements RewardedVideoSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyRvManagerListener i;
    /* access modifiers changed from: private */
    public long j;

    DemandOnlyRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyRvManagerListener demandOnlyRvManagerListener, int i2, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.f()), abstractAdapter);
        this.b = new AdapterConfig(providerSettings, providerSettings.k());
        this.c = this.b.b();
        this.f4603a = abstractAdapter;
        this.i = demandOnlyRvManagerListener;
        this.f = i2;
        this.f4603a.initRvForDemandOnly(activity, str, str2, this.c, this);
    }

    private void q() {
        b("start timer");
        a((TimerTask) new TimerTask() {
            public void run() {
                DemandOnlyRvSmash demandOnlyRvSmash = DemandOnlyRvSmash.this;
                demandOnlyRvSmash.b("load timed out state=" + DemandOnlyRvSmash.this.m());
                if (DemandOnlyRvSmash.this.a(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
                    DemandOnlyRvSmash.this.i.a(new IronSourceError(1055, "load timed out"), DemandOnlyRvSmash.this, new Date().getTime() - DemandOnlyRvSmash.this.j);
                }
            }
        });
    }

    public void a(boolean z) {
    }

    public void c(IronSourceError ironSourceError) {
        a(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        a("onRewardedVideoAdClosed error=" + ironSourceError);
        this.i.a(ironSourceError, this);
    }

    public void d(IronSourceError ironSourceError) {
        a("onRewardedVideoLoadFailed error=" + ironSourceError.b() + " state=" + m());
        o();
        if (a(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
            this.i.a(ironSourceError, this, new Date().getTime() - this.j);
        }
    }

    public void f() {
        a("onRewardedVideoAdVisible");
        this.i.d(this);
    }

    public void g() {
        a("onRewardedVideoAdClicked");
        this.i.b(this);
    }

    public void h() {
        a("onRewardedVideoAdRewarded");
        this.i.c(this);
    }

    public void i() {
    }

    public void j() {
        a("onRewardedVideoLoadSuccess state=" + m());
        o();
        if (a(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.LOADED)) {
            this.i.a(this, new Date().getTime() - this.j);
        }
    }

    public void onRewardedVideoAdClosed() {
        a(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        a("onRewardedVideoAdClosed");
        this.i.a(this);
    }

    public void onRewardedVideoAdOpened() {
        a("onRewardedVideoAdOpened");
        this.i.e(this);
    }

    public void p() {
        b("loadRewardedVideo state=" + m());
        DemandOnlySmash.SMASH_STATE a2 = a(new DemandOnlySmash.SMASH_STATE[]{DemandOnlySmash.SMASH_STATE.NOT_LOADED, DemandOnlySmash.SMASH_STATE.LOADED}, DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS);
        if (a2 == DemandOnlySmash.SMASH_STATE.NOT_LOADED || a2 == DemandOnlySmash.SMASH_STATE.LOADED) {
            q();
            this.j = new Date().getTime();
            this.f4603a.loadVideoForDemandOnly(this.c, this);
        } else if (a2 == DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS) {
            this.i.a(new IronSourceError(1053, "load already in progress"), this, 0);
        } else {
            this.i.a(new IronSourceError(1056, "cannot load because show is in progress"), this, 0);
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyRewardedVideoSmash " + this.b.d() + " : " + str, 0);
    }

    private void a(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "DemandOnlyRewardedVideoSmash " + this.b.d() + " : " + str, 0);
    }
}
