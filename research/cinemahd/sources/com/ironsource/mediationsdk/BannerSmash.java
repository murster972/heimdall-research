package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.FrameLayout;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.BannerManagerListener;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import java.util.Timer;
import java.util.TimerTask;

public class BannerSmash implements BannerSmashListener {

    /* renamed from: a  reason: collision with root package name */
    private AbstractAdapter f4594a;
    private Timer b;
    private long c;
    private ProviderSettings d;
    /* access modifiers changed from: private */
    public BANNER_SMASH_STATE e = BANNER_SMASH_STATE.NO_INIT;
    /* access modifiers changed from: private */
    public BannerManagerListener f;
    private boolean g;
    private IronSourceBannerLayout h;
    private int i;

    protected enum BANNER_SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        LOAD_IN_PROGRESS,
        LOADED,
        LOAD_FAILED,
        DESTROYED
    }

    BannerSmash(BannerManagerListener bannerManagerListener, ProviderSettings providerSettings, AbstractAdapter abstractAdapter, long j, int i2) {
        this.i = i2;
        this.f = bannerManagerListener;
        this.f4594a = abstractAdapter;
        this.d = providerSettings;
        this.c = j;
        this.f4594a.addBannerListener(this);
    }

    private void j() {
        if (this.f4594a != null) {
            try {
                String i2 = IronSourceObject.q().i();
                if (!TextUtils.isEmpty(i2)) {
                    this.f4594a.setMediationSegment(i2);
                }
                String b2 = ConfigFile.d().b();
                if (!TextUtils.isEmpty(b2)) {
                    this.f4594a.setPluginData(b2, ConfigFile.d().a());
                }
            } catch (Exception e2) {
                a(":setCustomParams():" + e2.toString());
            }
        }
    }

    private void k() {
        try {
            l();
            this.b = new Timer();
            this.b.schedule(new TimerTask() {
                public void run() {
                    cancel();
                    if (BannerSmash.this.e == BANNER_SMASH_STATE.INIT_IN_PROGRESS) {
                        BannerSmash.this.a(BANNER_SMASH_STATE.NO_INIT);
                        BannerSmash.this.a("init timed out");
                        BannerSmash.this.f.a(new IronSourceError(607, "Timed out"), BannerSmash.this, false);
                    } else if (BannerSmash.this.e == BANNER_SMASH_STATE.LOAD_IN_PROGRESS) {
                        BannerSmash.this.a(BANNER_SMASH_STATE.LOAD_FAILED);
                        BannerSmash.this.a("load timed out");
                        BannerSmash.this.f.a(new IronSourceError(608, "Timed out"), BannerSmash.this, false);
                    } else if (BannerSmash.this.e == BANNER_SMASH_STATE.LOADED) {
                        BannerSmash.this.a(BANNER_SMASH_STATE.LOAD_FAILED);
                        BannerSmash.this.a("reload timed out");
                        BannerSmash.this.f.b(new IronSourceError(609, "Timed out"), BannerSmash.this, false);
                    }
                }
            }, this.c);
        } catch (Exception e2) {
            a("startLoadTimer", e2.getLocalizedMessage());
        }
    }

    private void l() {
        try {
            if (this.b != null) {
                this.b.cancel();
            }
        } catch (Exception e2) {
            a("stopLoadTimer", e2.getLocalizedMessage());
        } catch (Throwable th) {
            this.b = null;
            throw th;
        }
        this.b = null;
    }

    public String c() {
        if (!TextUtils.isEmpty(this.d.a())) {
            return this.d.a();
        }
        return e();
    }

    public AbstractAdapter d() {
        return this.f4594a;
    }

    public String e() {
        if (this.d.m()) {
            return this.d.i();
        }
        return this.d.h();
    }

    public int f() {
        return this.i;
    }

    public String g() {
        return this.d.l();
    }

    public boolean h() {
        return this.g;
    }

    public void i() {
        a("reloadBanner()");
        k();
        a(BANNER_SMASH_STATE.LOADED);
        this.f4594a.reloadBanner(this.d.d());
    }

    public void onBannerInitSuccess() {
        l();
        if (this.e == BANNER_SMASH_STATE.INIT_IN_PROGRESS) {
            k();
            a(BANNER_SMASH_STATE.LOAD_IN_PROGRESS);
            this.f4594a.loadBanner(this.h, this.d.d(), this);
        }
    }

    public void b() {
        a("destroyBanner()");
        AbstractAdapter abstractAdapter = this.f4594a;
        if (abstractAdapter == null) {
            a("destroyBanner() mAdapter == null");
            return;
        }
        abstractAdapter.destroyBanner(this.d.d());
        a(BANNER_SMASH_STATE.DESTROYED);
    }

    public void a(boolean z) {
        this.g = z;
    }

    public void a(IronSourceBannerLayout ironSourceBannerLayout, Activity activity, String str, String str2) {
        a("loadBanner()");
        this.g = false;
        if (ironSourceBannerLayout == null || ironSourceBannerLayout.b()) {
            this.f.a(new IronSourceError(610, ironSourceBannerLayout == null ? "banner is null" : "banner is destroyed"), this, false);
        } else if (this.f4594a == null) {
            this.f.a(new IronSourceError(611, "adapter==null"), this, false);
        } else {
            this.h = ironSourceBannerLayout;
            k();
            if (this.e == BANNER_SMASH_STATE.NO_INIT) {
                a(BANNER_SMASH_STATE.INIT_IN_PROGRESS);
                j();
                this.f4594a.initBanners(activity, str, str2, this.d.d(), this);
                return;
            }
            a(BANNER_SMASH_STATE.LOAD_IN_PROGRESS);
            this.f4594a.loadBanner(ironSourceBannerLayout, this.d.d(), this);
        }
    }

    public void b(IronSourceError ironSourceError) {
        l();
        if (this.e == BANNER_SMASH_STATE.INIT_IN_PROGRESS) {
            this.f.a(new IronSourceError(612, "Banner init failed"), this, false);
            a(BANNER_SMASH_STATE.NO_INIT);
        }
    }

    public void b(Activity activity) {
        AbstractAdapter abstractAdapter = this.f4594a;
        if (abstractAdapter != null) {
            abstractAdapter.onResume(activity);
        }
    }

    /* access modifiers changed from: private */
    public void a(BANNER_SMASH_STATE banner_smash_state) {
        this.e = banner_smash_state;
        a("state=" + banner_smash_state.name());
    }

    public void a(View view, FrameLayout.LayoutParams layoutParams) {
        a("onBannerAdLoaded()");
        l();
        BANNER_SMASH_STATE banner_smash_state = this.e;
        if (banner_smash_state == BANNER_SMASH_STATE.LOAD_IN_PROGRESS) {
            a(BANNER_SMASH_STATE.LOADED);
            this.f.a(this, view, layoutParams);
        } else if (banner_smash_state == BANNER_SMASH_STATE.LOADED) {
            this.f.a(this);
        }
    }

    public void a(IronSourceError ironSourceError) {
        a("onBannerAdLoadFailed()");
        l();
        boolean z = ironSourceError.a() == 606;
        BANNER_SMASH_STATE banner_smash_state = this.e;
        if (banner_smash_state == BANNER_SMASH_STATE.LOAD_IN_PROGRESS) {
            a(BANNER_SMASH_STATE.LOAD_FAILED);
            this.f.a(ironSourceError, this, z);
        } else if (banner_smash_state == BANNER_SMASH_STATE.LOADED) {
            this.f.b(ironSourceError, this, z);
        }
    }

    public void a() {
        BannerManagerListener bannerManagerListener = this.f;
        if (bannerManagerListener != null) {
            bannerManagerListener.b(this);
        }
    }

    public void a(Activity activity) {
        AbstractAdapter abstractAdapter = this.f4594a;
        if (abstractAdapter != null) {
            abstractAdapter.onPause(activity);
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        c2.b(ironSourceTag, "BannerSmash " + e() + " " + str, 1);
    }

    private void a(String str, String str2) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, str + " Banner exception: " + e() + " | " + str2, 3);
    }
}
