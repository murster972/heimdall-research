package com.ironsource.mediationsdk;

import android.util.Pair;
import java.util.ArrayList;

public class IntegrationData {

    /* renamed from: a  reason: collision with root package name */
    public String f4621a;
    public String b;
    public String[] c = null;
    public ArrayList<Pair<String, String>> d = null;
    public String[] e = null;
    public boolean f = false;

    public IntegrationData(String str, String str2) {
        this.f4621a = str;
        this.b = str2;
    }
}
