package com.ironsource.mediationsdk;

import android.app.Activity;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class DemandOnlyIsManager implements DemandOnlyIsManagerListener {

    /* renamed from: a  reason: collision with root package name */
    private ConcurrentHashMap<String, DemandOnlyIsSmash> f4599a = new ConcurrentHashMap<>();

    DemandOnlyIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2) {
        for (ProviderSettings next : list) {
            if (next.i().equalsIgnoreCase("SupersonicAds") || next.i().equalsIgnoreCase("IronSource")) {
                AbstractAdapter a2 = AdapterRepository.a().a(next, next.k(), activity, true);
                if (a2 != null) {
                    this.f4599a.put(next.l(), new DemandOnlyIsSmash(activity, str, str2, next, this, interstitialConfigurations.e(), a2));
                }
            } else {
                b("cannot load " + next.i());
            }
        }
    }

    public void a(Activity activity) {
        if (activity != null) {
            for (DemandOnlyIsSmash a2 : this.f4599a.values()) {
                a2.a(activity);
            }
        }
    }

    public void b(Activity activity) {
        if (activity != null) {
            for (DemandOnlyIsSmash b : this.f4599a.values()) {
                b.b(activity);
            }
        }
    }

    public void c(DemandOnlyIsSmash demandOnlyIsSmash) {
        a(demandOnlyIsSmash, "onInterstitialAdClicked");
        a(2006, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.a().a(demandOnlyIsSmash.n());
    }

    public void d(DemandOnlyIsSmash demandOnlyIsSmash) {
        a(2210, demandOnlyIsSmash);
        a(demandOnlyIsSmash, "onInterstitialAdVisible");
    }

    public void a(String str) {
        try {
            if (!this.f4599a.containsKey(str)) {
                a(2500, str);
                ISDemandOnlyListenerWrapper.a().a(str, ErrorBuilder.f("Interstitial"));
                return;
            }
            DemandOnlyIsSmash demandOnlyIsSmash = this.f4599a.get(str);
            a((int) AdError.CACHE_ERROR_CODE, demandOnlyIsSmash);
            demandOnlyIsSmash.p();
        } catch (Exception e) {
            b("loadInterstitial exception " + e.getMessage());
            ISDemandOnlyListenerWrapper.a().a(str, ErrorBuilder.c("loadInterstitial exception"));
        }
    }

    public void b(DemandOnlyIsSmash demandOnlyIsSmash) {
        a(demandOnlyIsSmash, "onInterstitialAdClosed");
        a(2204, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.a().b(demandOnlyIsSmash.n());
    }

    private void b(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c.b(ironSourceTag, "DemandOnlyIsManager " + str, 0);
    }

    public void a(DemandOnlyIsSmash demandOnlyIsSmash, long j) {
        a(demandOnlyIsSmash, "onInterstitialAdReady");
        a(2003, demandOnlyIsSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        ISDemandOnlyListenerWrapper.a().d(demandOnlyIsSmash.n());
    }

    public void a(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash, long j) {
        a(demandOnlyIsSmash, "onInterstitialAdLoadFailed error=" + ironSourceError.toString());
        a(2200, demandOnlyIsSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}, new Object[]{"duration", Long.valueOf(j)}});
        ISDemandOnlyListenerWrapper.a().a(demandOnlyIsSmash.n(), ironSourceError);
    }

    public void a(DemandOnlyIsSmash demandOnlyIsSmash) {
        a(demandOnlyIsSmash, "onInterstitialAdOpened");
        a(2005, demandOnlyIsSmash);
        ISDemandOnlyListenerWrapper.a().c(demandOnlyIsSmash.n());
    }

    public void a(IronSourceError ironSourceError, DemandOnlyIsSmash demandOnlyIsSmash) {
        a(demandOnlyIsSmash, "onInterstitialAdShowFailed error=" + ironSourceError.toString());
        a(2203, demandOnlyIsSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}});
        ISDemandOnlyListenerWrapper.a().b(demandOnlyIsSmash.n(), ironSourceError);
    }

    private void a(int i, DemandOnlyIsSmash demandOnlyIsSmash) {
        a(i, demandOnlyIsSmash, (Object[][]) null);
    }

    private void a(int i, DemandOnlyIsSmash demandOnlyIsSmash, Object[][] objArr) {
        Map<String, Object> l = demandOnlyIsSmash.l();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    l.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "IS sendProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        InterstitialEventsManager.g().d(new EventData(i, new JSONObject(l)));
    }

    private void a(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put("isDemandOnly", 1);
        if (str == null) {
            str = "";
        }
        hashMap.put("spId", str);
        InterstitialEventsManager.g().d(new EventData(i, new JSONObject(hashMap)));
    }

    private void a(DemandOnlyIsSmash demandOnlyIsSmash, String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyIsManager " + demandOnlyIsSmash.k() + " : " + str, 0);
    }
}
