package com.ironsource.mediationsdk;

import android.app.Activity;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.RewardedVideoManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashApi;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class RewardedVideoSmash extends AbstractSmash implements RewardedVideoSmashListener, RewardedVideoSmashApi {
    private JSONObject r;
    /* access modifiers changed from: private */
    public RewardedVideoManagerListener s;
    /* access modifiers changed from: private */
    public AtomicBoolean t = new AtomicBoolean(false);
    /* access modifiers changed from: private */
    public long u;
    private int v;

    RewardedVideoSmash(ProviderSettings providerSettings, int i) {
        super(providerSettings);
        this.r = providerSettings.k();
        this.m = this.r.optInt("maxAdsPerIteration", 99);
        this.n = this.r.optInt("maxAdsPerSession", 99);
        this.o = this.r.optInt("maxAdsPerDay", 99);
        this.r.optString("requestUrl");
        this.v = i;
    }

    public void E() {
        if (this.b != null) {
            if (!(t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY || t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION)) {
                this.t.set(true);
                this.u = new Date().getTime();
            }
            IronSourceLoggerManager ironSourceLoggerManager = this.q;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.b(ironSourceTag, p() + ":fetchRewardedVideo()", 1);
            this.b.fetchRewardedVideo(this.r);
        }
    }

    public boolean F() {
        if (this.b == null) {
            return false;
        }
        IronSourceLoggerManager ironSourceLoggerManager = this.q;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        ironSourceLoggerManager.b(ironSourceTag, p() + ":isRewardedVideoAvailable()", 1);
        return this.b.isRewardedVideoAvailable(this.r);
    }

    /* access modifiers changed from: package-private */
    public void G() {
        try {
            C();
            this.k = new Timer();
            this.k.schedule(new TimerTask() {
                public void run() {
                    synchronized (RewardedVideoSmash.this) {
                        cancel();
                        if (RewardedVideoSmash.this.s != null) {
                            IronSourceLoggerManager ironSourceLoggerManager = RewardedVideoSmash.this.q;
                            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
                            ironSourceLoggerManager.b(ironSourceTag, "Timeout for " + RewardedVideoSmash.this.p(), 0);
                            RewardedVideoSmash.this.a(AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE);
                            long time = new Date().getTime() - RewardedVideoSmash.this.u;
                            if (RewardedVideoSmash.this.t.compareAndSet(true, false)) {
                                RewardedVideoSmash.this.a(1200, new Object[][]{new Object[]{"errorCode", 1025}, new Object[]{"duration", Long.valueOf(time)}});
                                RewardedVideoSmash.this.a(1212, new Object[][]{new Object[]{"errorCode", 1025}, new Object[]{"duration", Long.valueOf(time)}});
                            } else {
                                RewardedVideoSmash.this.a(1208, new Object[][]{new Object[]{"errorCode", 1025}, new Object[]{"duration", Long.valueOf(time)}});
                            }
                            RewardedVideoSmash.this.s.a(false, RewardedVideoSmash.this);
                        }
                    }
                }
            }, (long) (this.v * 1000));
        } catch (Exception e) {
            a("startInitTimer", e.getLocalizedMessage());
        }
    }

    public void d(IronSourceError ironSourceError) {
        long time = new Date().getTime() - this.u;
        a(1212, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}, new Object[]{"duration", Long.valueOf(time)}});
    }

    public void f() {
        RewardedVideoManagerListener rewardedVideoManagerListener = this.s;
        if (rewardedVideoManagerListener != null) {
            rewardedVideoManagerListener.d(this);
        }
    }

    public void g() {
        RewardedVideoManagerListener rewardedVideoManagerListener = this.s;
        if (rewardedVideoManagerListener != null) {
            rewardedVideoManagerListener.a(this);
        }
    }

    public void h() {
        RewardedVideoManagerListener rewardedVideoManagerListener = this.s;
        if (rewardedVideoManagerListener != null) {
            rewardedVideoManagerListener.b(this);
        }
    }

    public void i() {
    }

    public void j() {
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.j = 0;
        a(F() ? AbstractSmash.MEDIATION_STATE.AVAILABLE : AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE);
    }

    /* access modifiers changed from: protected */
    public String n() {
        return "rewardedvideo";
    }

    public void onRewardedVideoAdClosed() {
        RewardedVideoManagerListener rewardedVideoManagerListener = this.s;
        if (rewardedVideoManagerListener != null) {
            rewardedVideoManagerListener.e(this);
        }
        E();
    }

    public void onRewardedVideoAdOpened() {
        RewardedVideoManagerListener rewardedVideoManagerListener = this.s;
        if (rewardedVideoManagerListener != null) {
            rewardedVideoManagerListener.c(this);
        }
    }

    private void b(int i) {
        a(i, (Object[][]) null);
    }

    public void c(IronSourceError ironSourceError) {
        RewardedVideoManagerListener rewardedVideoManagerListener = this.s;
        if (rewardedVideoManagerListener != null) {
            rewardedVideoManagerListener.a(ironSourceError, this);
        }
    }

    public void a(Activity activity, String str, String str2) {
        G();
        if (this.b != null) {
            this.t.set(true);
            this.u = new Date().getTime();
            this.b.addRewardedVideoListener(this);
            IronSourceLoggerManager ironSourceLoggerManager = this.q;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.b(ironSourceTag, p() + ":initRewardedVideo()", 1);
            this.b.initRewardedVideo(activity, str, str2, this.r, this);
        }
    }

    public void a(RewardedVideoManagerListener rewardedVideoManagerListener) {
        this.s = rewardedVideoManagerListener;
    }

    public synchronized void a(boolean z) {
        C();
        if (this.t.compareAndSet(true, false)) {
            a(z ? AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE : 1200, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - this.u)}});
        } else {
            b(z ? 1207 : 1208);
        }
        if (A() && ((z && this.f4582a != AbstractSmash.MEDIATION_STATE.AVAILABLE) || (!z && this.f4582a != AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE))) {
            a(z ? AbstractSmash.MEDIATION_STATE.AVAILABLE : AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE);
            if (this.s != null) {
                this.s.a(z, this);
            }
        }
    }

    /* access modifiers changed from: private */
    public void a(int i, Object[][] objArr) {
        JSONObject a2 = IronSourceUtils.a((AbstractSmash) this);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    a2.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                this.q.b(IronSourceLogger.IronSourceTag.INTERNAL, "RewardedVideoSmash logProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.g().d(new EventData(i, a2));
    }
}
