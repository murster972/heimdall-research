package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyRewardedVideoListener;

public class RVDemandOnlyListenerWrapper {
    private static final RVDemandOnlyListenerWrapper b = new RVDemandOnlyListenerWrapper();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ISDemandOnlyRewardedVideoListener f4651a = null;

    private RVDemandOnlyListenerWrapper() {
    }

    /* access modifiers changed from: private */
    public void f(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, str, 1);
    }

    public void b(final String str) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.b(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdClosed() instanceId=" + str);
                }
            });
        }
    }

    public void c(final String str) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.a(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdOpened() instanceId=" + str);
                }
            });
        }
    }

    public void d(final String str) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.c(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdRewarded() instanceId=" + str);
                }
            });
        }
    }

    public void e(final String str) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.d(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdLoadSuccess() instanceId=" + str);
                }
            });
        }
    }

    public static RVDemandOnlyListenerWrapper a() {
        return b;
    }

    public void b(final String str, final IronSourceError ironSourceError) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.b(str, ironSourceError);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdShowFailed() instanceId=" + str + "error=" + ironSourceError.b());
                }
            });
        }
    }

    public void a(final String str, final IronSourceError ironSourceError) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.a(str, ironSourceError);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdLoadFailed() instanceId=" + str + "error=" + ironSourceError.b());
                }
            });
        }
    }

    public void a(final String str) {
        if (this.f4651a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    RVDemandOnlyListenerWrapper.this.f4651a.e(str);
                    RVDemandOnlyListenerWrapper rVDemandOnlyListenerWrapper = RVDemandOnlyListenerWrapper.this;
                    rVDemandOnlyListenerWrapper.f("onRewardedVideoAdClicked() instanceId=" + str);
                }
            });
        }
    }
}
