package com.ironsource.mediationsdk.config;

public class ConfigFile {
    private static ConfigFile d;

    /* renamed from: a  reason: collision with root package name */
    private String f4671a;
    private String b;
    private String c;

    public ConfigFile() {
        new String[]{"Unity", "AdobeAir", "Xamarin", "Corona", "AdMob", "MoPub"};
    }

    public static synchronized ConfigFile d() {
        ConfigFile configFile;
        synchronized (ConfigFile.class) {
            if (d == null) {
                d = new ConfigFile();
            }
            configFile = d;
        }
        return configFile;
    }

    public String a() {
        return this.c;
    }

    public String b() {
        return this.f4671a;
    }

    public String c() {
        return this.b;
    }
}
