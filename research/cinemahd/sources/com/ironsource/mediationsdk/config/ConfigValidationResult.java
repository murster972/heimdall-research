package com.ironsource.mediationsdk.config;

import com.ironsource.mediationsdk.logger.IronSourceError;

public class ConfigValidationResult {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4672a = true;
    private IronSourceError b = null;

    public void a(IronSourceError ironSourceError) {
        this.f4672a = false;
        this.b = ironSourceError;
    }

    public boolean b() {
        return this.f4672a;
    }

    public String toString() {
        if (b()) {
            return "valid:" + this.f4672a;
        }
        return "valid:" + this.f4672a + ", IronSourceError:" + this.b;
    }

    public IronSourceError a() {
        return this.b;
    }
}
