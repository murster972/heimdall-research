package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialListener;

public class ISListenerWrapper {
    private static final ISListenerWrapper b = new ISListenerWrapper();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public InterstitialListener f4613a = null;

    private ISListenerWrapper() {
    }

    public static synchronized ISListenerWrapper f() {
        ISListenerWrapper iSListenerWrapper;
        synchronized (ISListenerWrapper.class) {
            iSListenerWrapper = b;
        }
        return iSListenerWrapper;
    }

    public synchronized void b() {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.b();
                        ISListenerWrapper.this.a("onInterstitialAdClosed()");
                    }
                }
            });
        }
    }

    public synchronized void c() {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.c();
                        ISListenerWrapper.this.a("onInterstitialAdOpened()");
                    }
                }
            });
        }
    }

    public synchronized void d() {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.a();
                        ISListenerWrapper.this.a("onInterstitialAdReady()");
                    }
                }
            });
        }
    }

    public synchronized void e() {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.d();
                        ISListenerWrapper.this.a("onInterstitialAdShowSucceeded()");
                    }
                }
            });
        }
    }

    public synchronized void a(InterstitialListener interstitialListener) {
        this.f4613a = interstitialListener;
    }

    public synchronized void b(final IronSourceError ironSourceError) {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.b(ironSourceError);
                        ISListenerWrapper iSListenerWrapper = ISListenerWrapper.this;
                        iSListenerWrapper.a("onInterstitialAdShowFailed() error=" + ironSourceError.b());
                    }
                }
            });
        }
    }

    public synchronized void a(final IronSourceError ironSourceError) {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.a(ironSourceError);
                        ISListenerWrapper iSListenerWrapper = ISListenerWrapper.this;
                        iSListenerWrapper.a("onInterstitialAdLoadFailed() error=" + ironSourceError.b());
                    }
                }
            });
        }
    }

    public synchronized void a() {
        if (this.f4613a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        ISListenerWrapper.this.f4613a.onInterstitialAdClicked();
                        ISListenerWrapper.this.a("onInterstitialAdClicked()");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, str, 1);
    }
}
