package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.uwetrottmann.trakt5.TraktV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import okhttp3.internal.cache.DiskLruCache;
import okhttp3.internal.ws.WebSocketProtocol;
import org.json.JSONObject;

class ProgIsManager implements ProgIsManagerListener, AuctionEventListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public SessionCappingManager f4638a;
    private MEDIATION_STATE b;
    /* access modifiers changed from: private */
    public final ConcurrentHashMap<String, ProgIsSmash> c;
    private CopyOnWriteArrayList<ProgIsSmash> d;
    private ConcurrentHashMap<String, AuctionResponseItem> e;
    private String f;
    /* access modifiers changed from: private */
    public String g;
    private int h;
    private boolean i;
    /* access modifiers changed from: private */
    public AuctionHandler j;
    /* access modifiers changed from: private */
    public Context k;
    /* access modifiers changed from: private */
    public long l;
    /* access modifiers changed from: private */
    public long m;
    private long n;
    private int o;
    private String p = "";

    enum MEDIATION_STATE {
        STATE_NOT_INITIALIZED,
        STATE_READY_TO_LOAD,
        STATE_AUCTION,
        STATE_LOADING_SMASHES,
        STATE_READY_TO_SHOW,
        STATE_SHOWING
    }

    public ProgIsManager(Activity activity, List<ProviderSettings> list, InterstitialConfigurations interstitialConfigurations, String str, String str2, int i2) {
        long time = new Date().getTime();
        a(82312);
        a(MEDIATION_STATE.STATE_NOT_INITIALIZED);
        this.c = new ConcurrentHashMap<>();
        this.d = new CopyOnWriteArrayList<>();
        this.e = new ConcurrentHashMap<>();
        this.f = "";
        this.g = "";
        this.k = activity.getApplicationContext();
        this.h = interstitialConfigurations.d();
        CallbackThrottler.b().a(i2);
        AuctionSettings f2 = interstitialConfigurations.f();
        this.m = f2.g();
        this.i = f2.e() > 0;
        if (this.i) {
            this.j = new AuctionHandler("interstitial", f2, this);
        }
        for (ProviderSettings next : list) {
            AbstractAdapter a2 = AdapterRepository.a().a(next, next.f(), activity);
            if (a2 != null && AdaptersCompatibilityHandler.a().a(a2)) {
                ProgIsSmash progIsSmash = new ProgIsSmash(activity, str, str2, next, this, interstitialConfigurations.e(), a2);
                this.c.put(progIsSmash.k(), progIsSmash);
            }
        }
        this.f4638a = new SessionCappingManager(new ArrayList(this.c.values()));
        for (ProgIsSmash next2 : this.c.values()) {
            if (next2.o()) {
                next2.q();
            }
        }
        this.l = new Date().getTime();
        a(MEDIATION_STATE.STATE_READY_TO_LOAD);
        a(82313, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
    }

    private boolean c(int i2) {
        return i2 == 2002 || i2 == 2003 || i2 == 2200 || i2 == 2005 || i2 == 2204 || i2 == 2201 || i2 == 2203 || i2 == 2006 || i2 == 2004 || i2 == 2110 || i2 == 2301 || i2 == 2300;
    }

    private List<AuctionResponseItem> c() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (ProgIsSmash next : this.c.values()) {
            if (!next.o() && !this.f4638a.b(next)) {
                copyOnWriteArrayList.add(new AuctionResponseItem(next.k()));
            }
        }
        return copyOnWriteArrayList;
    }

    private void d() {
        synchronized (this.c) {
            if (this.d.isEmpty()) {
                a(MEDIATION_STATE.STATE_READY_TO_LOAD);
                a(2110, new Object[][]{new Object[]{"errorCode", 1035}, new Object[]{"reason", "Empty waterfall"}});
                CallbackThrottler.b().a(new IronSourceError(1035, "Empty waterfall"));
                return;
            }
            a(MEDIATION_STATE.STATE_LOADING_SMASHES);
            for (int i2 = 0; i2 < Math.min(this.h, this.d.size()); i2++) {
                ProgIsSmash progIsSmash = this.d.get(i2);
                String f2 = this.e.get(progIsSmash.k()).f();
                a((int) AdError.CACHE_ERROR_CODE, progIsSmash);
                progIsSmash.a(f2);
            }
        }
    }

    /* access modifiers changed from: private */
    public void e() {
        a(MEDIATION_STATE.STATE_AUCTION);
        AsyncTask.execute(new Runnable() {
            public void run() {
                String unused = ProgIsManager.this.g = "";
                StringBuilder sb = new StringBuilder();
                long b = ProgIsManager.this.m - (new Date().getTime() - ProgIsManager.this.l);
                if (b > 0) {
                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                        public void run() {
                            ProgIsManager.this.e();
                        }
                    }, b);
                    return;
                }
                ProgIsManager.this.a((int) AdError.SERVER_ERROR_CODE, (Object[][]) null);
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                synchronized (ProgIsManager.this.c) {
                    for (ProgIsSmash progIsSmash : ProgIsManager.this.c.values()) {
                        if (!ProgIsManager.this.f4638a.b(progIsSmash)) {
                            if (progIsSmash.o() && progIsSmash.s()) {
                                Map<String, Object> p = progIsSmash.p();
                                if (p != null) {
                                    hashMap.put(progIsSmash.k(), p);
                                    sb.append(TraktV2.API_VERSION + progIsSmash.k() + ",");
                                }
                            } else if (!progIsSmash.o()) {
                                arrayList.add(progIsSmash.k());
                                sb.append(DiskLruCache.VERSION_1 + progIsSmash.k() + ",");
                            }
                        }
                    }
                }
                if (hashMap.size() == 0 && arrayList.size() == 0) {
                    ProgIsManager.this.a(2300, new Object[][]{new Object[]{"errorCode", Integer.valueOf(WebSocketProtocol.CLOSE_NO_STATUS_CODE)}, new Object[]{"duration", 0}});
                    CallbackThrottler.b().a(new IronSourceError(WebSocketProtocol.CLOSE_NO_STATUS_CODE, "No candidates available for auctioning"));
                    ProgIsManager.this.a(2110, new Object[][]{new Object[]{"errorCode", Integer.valueOf(WebSocketProtocol.CLOSE_NO_STATUS_CODE)}});
                    ProgIsManager.this.a(MEDIATION_STATE.STATE_READY_TO_LOAD);
                    return;
                }
                if (sb.length() > 256) {
                    sb.setLength(256);
                } else if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                ProgIsManager.this.a(2310, new Object[][]{new Object[]{"ext1", sb.toString()}});
                int a2 = SessionDepthManager.a().a(2);
                if (ProgIsManager.this.j != null) {
                    ProgIsManager.this.j.a(ProgIsManager.this.k, hashMap, arrayList, a2);
                }
            }
        });
    }

    private void f() {
        a(c());
    }

    public void b(Activity activity) {
        for (ProgIsSmash b2 : this.c.values()) {
            b2.b(activity);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0067, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b() {
        /*
            r4 = this;
            monitor-enter(r4)
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.b     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_SHOWING     // Catch:{ all -> 0x0068 }
            if (r0 != r1) goto L_0x0023
            java.lang.String r0 = "loadInterstitial: load cannot be invoked while showing an ad"
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.c()     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x0068 }
            r3 = 3
            r1.b(r2, r0, r3)     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.logger.IronSourceError r1 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x0068 }
            r2 = 1037(0x40d, float:1.453E-42)
            r1.<init>(r2, r0)     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ISListenerWrapper r0 = com.ironsource.mediationsdk.ISListenerWrapper.f()     // Catch:{ all -> 0x0068 }
            r0.a((com.ironsource.mediationsdk.logger.IronSourceError) r1)     // Catch:{ all -> 0x0068 }
            monitor-exit(r4)
            return
        L_0x0023:
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.b     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_LOAD     // Catch:{ all -> 0x0068 }
            if (r0 == r1) goto L_0x002f
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.b     // Catch:{ all -> 0x0068 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_SHOW     // Catch:{ all -> 0x0068 }
            if (r0 != r1) goto L_0x0039
        L_0x002f:
            com.ironsource.mediationsdk.CallbackThrottler r0 = com.ironsource.mediationsdk.CallbackThrottler.b()     // Catch:{ all -> 0x0068 }
            boolean r0 = r0.a()     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x0040
        L_0x0039:
            java.lang.String r0 = "loadInterstitial: load is already in progress"
            r4.c((java.lang.String) r0)     // Catch:{ all -> 0x0068 }
            monitor-exit(r4)
            return
        L_0x0040:
            java.lang.String r0 = ""
            r4.g = r0     // Catch:{ all -> 0x0068 }
            java.lang.String r0 = ""
            r4.f = r0     // Catch:{ all -> 0x0068 }
            r0 = 2001(0x7d1, float:2.804E-42)
            r4.a((int) r0)     // Catch:{ all -> 0x0068 }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x0068 }
            r0.<init>()     // Catch:{ all -> 0x0068 }
            long r0 = r0.getTime()     // Catch:{ all -> 0x0068 }
            r4.n = r0     // Catch:{ all -> 0x0068 }
            boolean r0 = r4.i     // Catch:{ all -> 0x0068 }
            if (r0 == 0) goto L_0x0060
            r4.e()     // Catch:{ all -> 0x0068 }
            goto L_0x0066
        L_0x0060:
            r4.f()     // Catch:{ all -> 0x0068 }
            r4.d()     // Catch:{ all -> 0x0068 }
        L_0x0066:
            monitor-exit(r4)
            return
        L_0x0068:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgIsManager.b():void");
    }

    public void e(ProgIsSmash progIsSmash) {
        a(progIsSmash, "onInterstitialAdClicked");
        ISListenerWrapper.f().a();
        b(2006, progIsSmash);
    }

    public void f(ProgIsSmash progIsSmash) {
        a(progIsSmash, "onInterstitialAdShowSucceeded");
        ISListenerWrapper.f().e();
        b(2202, progIsSmash);
    }

    public void a(Activity activity) {
        for (ProgIsSmash a2 : this.c.values()) {
            a2.a(activity);
        }
    }

    public void c(ProgIsSmash progIsSmash) {
        synchronized (this) {
            a(progIsSmash, "onInterstitialAdOpened");
            ISListenerWrapper.f().c();
            b(2005, progIsSmash);
            if (this.i) {
                AuctionResponseItem auctionResponseItem = this.e.get(progIsSmash.k());
                if (auctionResponseItem != null) {
                    this.j.a(auctionResponseItem);
                } else {
                    String k2 = progIsSmash != null ? progIsSmash.k() : "Smash is null";
                    c("onInterstitialAdOpened showing instance " + k2 + " missing from waterfall");
                    StringBuilder sb = new StringBuilder();
                    sb.append("Showing missing ");
                    sb.append(this.b);
                    a(82317, new Object[][]{new Object[]{"errorCode", 1011}, new Object[]{"reason", sb.toString()}, new Object[]{"ext1", k2}});
                }
            }
        }
    }

    public void a(List<AuctionResponseItem> list, String str, int i2, long j2) {
        this.g = str;
        this.o = i2;
        this.p = "";
        a(2301, new Object[][]{new Object[]{"duration", Long.valueOf(j2)}});
        a(list);
        d();
    }

    public void a(int i2, String str, int i3, String str2, long j2) {
        c("Auction failed | moving to fallback waterfall");
        this.o = i3;
        this.p = str2;
        if (TextUtils.isEmpty(str)) {
            a(2300, new Object[][]{new Object[]{"errorCode", Integer.valueOf(i2)}, new Object[]{"duration", Long.valueOf(j2)}});
        } else {
            a(2300, new Object[][]{new Object[]{"errorCode", Integer.valueOf(i2)}, new Object[]{"reason", str}, new Object[]{"duration", Long.valueOf(j2)}});
        }
        f();
        d();
    }

    public void d(ProgIsSmash progIsSmash) {
        synchronized (this) {
            a(progIsSmash, "onInterstitialAdClosed");
            b(2204, progIsSmash);
            ISListenerWrapper.f().b();
            a(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    private void c(String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "ProgIsManager " + str, 0);
    }

    private String a(AuctionResponseItem auctionResponseItem) {
        String str = TextUtils.isEmpty(auctionResponseItem.f()) ? DiskLruCache.VERSION_1 : TraktV2.API_VERSION;
        return str + auctionResponseItem.b();
    }

    private void b(ProgIsSmash progIsSmash, String str) {
        a(MEDIATION_STATE.STATE_SHOWING);
        progIsSmash.v();
        b(2201, progIsSmash);
        this.f4638a.a(progIsSmash);
        if (this.f4638a.b(progIsSmash)) {
            progIsSmash.u();
            a(2401, progIsSmash);
            IronSourceUtils.g(progIsSmash.k() + " was session capped");
        }
        CappingManager.b(this.k, str);
        if (CappingManager.d(this.k, str)) {
            b(2400);
        }
    }

    private void a(List<AuctionResponseItem> list) {
        synchronized (this.c) {
            this.d.clear();
            this.e.clear();
            StringBuilder sb = new StringBuilder();
            for (AuctionResponseItem next : list) {
                sb.append(a(next) + ",");
                ProgIsSmash progIsSmash = this.c.get(next.b());
                if (progIsSmash != null) {
                    progIsSmash.b(true);
                    this.d.add(progIsSmash);
                    this.e.put(progIsSmash.k(), next);
                } else {
                    c("updateWaterfall() - could not find matching smash for auction response item " + next.b());
                }
            }
            c("updateWaterfall() - response waterfall is " + sb.toString());
            if (sb.length() > 256) {
                sb.setLength(256);
            } else if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            } else {
                c("Updated waterfall is empty");
            }
            a(2311, new Object[][]{new Object[]{"ext1", sb.toString()}});
        }
    }

    public void b(ProgIsSmash progIsSmash) {
        a(progIsSmash, "onInterstitialAdVisible");
    }

    public void b(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        a(2206, progIsSmash, new Object[][]{new Object[]{"reason", ironSourceError.b()}});
    }

    private void b(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.API, str, 3);
    }

    private void b(int i2, Object[][] objArr) {
        a(i2, objArr, true);
    }

    private void b(int i2) {
        a(i2, (Object[][]) null, true);
    }

    private void b(int i2, ProgIsSmash progIsSmash, Object[][] objArr) {
        a(i2, progIsSmash, objArr, true);
    }

    private void b(int i2, ProgIsSmash progIsSmash) {
        a(i2, progIsSmash, (Object[][]) null, true);
    }

    public synchronized void a(String str) {
        if (this.b == MEDIATION_STATE.STATE_SHOWING) {
            b("showInterstitial error: can't show ad while an ad is already showing");
            ISListenerWrapper.f().b(new IronSourceError(1036, "showInterstitial error: can't show ad while an ad is already showing"));
            a(2111, new Object[][]{new Object[]{"errorCode", 1036}, new Object[]{"reason", "showInterstitial error: can't show ad while an ad is already showing"}});
        } else if (this.b != MEDIATION_STATE.STATE_READY_TO_SHOW) {
            c("showInterstitial() error state=" + this.b.toString());
            b("showInterstitial error: show called while no ads are available");
            ISListenerWrapper.f().b(new IronSourceError(509, "showInterstitial error: show called while no ads are available"));
            a(2111, new Object[][]{new Object[]{"errorCode", 509}, new Object[]{"reason", "showInterstitial error: show called while no ads are available"}});
        } else if (str == null) {
            b("showInterstitial error: empty default placement");
            ISListenerWrapper.f().b(new IronSourceError(1020, "showInterstitial error: empty default placement"));
            a(2111, new Object[][]{new Object[]{"errorCode", 1020}, new Object[]{"reason", "showInterstitial error: empty default placement"}});
        } else {
            this.f = str;
            b((int) AdError.BROKEN_MEDIA_ERROR_CODE);
            if (CappingManager.d(this.k, this.f)) {
                String str2 = "placement " + this.f + " is capped";
                b(str2);
                ISListenerWrapper.f().b(new IronSourceError(524, str2));
                b(2111, new Object[][]{new Object[]{"errorCode", 524}, new Object[]{"reason", str2}});
                return;
            }
            synchronized (this.c) {
                Iterator<ProgIsSmash> it2 = this.d.iterator();
                while (it2.hasNext()) {
                    ProgIsSmash next = it2.next();
                    if (next.t()) {
                        b(next, this.f);
                        return;
                    }
                    c("showInterstitial " + next.k() + " isReadyToShow() == false");
                }
                ISListenerWrapper.f().b(ErrorBuilder.d("Interstitial"));
                b(2111, new Object[][]{new Object[]{"errorCode", 509}, new Object[]{"reason", "Show Fail - No ads to show"}});
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0037, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized boolean a() {
        /*
            r4 = this;
            monitor-enter(r4)
            android.content.Context r0 = r4.k     // Catch:{ all -> 0x0038 }
            boolean r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.c((android.content.Context) r0)     // Catch:{ all -> 0x0038 }
            r1 = 0
            if (r0 == 0) goto L_0x0036
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r0 = r4.b     // Catch:{ all -> 0x0038 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r2 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_SHOW     // Catch:{ all -> 0x0038 }
            if (r0 == r2) goto L_0x0011
            goto L_0x0036
        L_0x0011:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgIsSmash> r0 = r4.c     // Catch:{ all -> 0x0038 }
            monitor-enter(r0)     // Catch:{ all -> 0x0038 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgIsSmash> r2 = r4.d     // Catch:{ all -> 0x0033 }
            java.util.Iterator r2 = r2.iterator()     // Catch:{ all -> 0x0033 }
        L_0x001a:
            boolean r3 = r2.hasNext()     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x0030
            java.lang.Object r3 = r2.next()     // Catch:{ all -> 0x0033 }
            com.ironsource.mediationsdk.ProgIsSmash r3 = (com.ironsource.mediationsdk.ProgIsSmash) r3     // Catch:{ all -> 0x0033 }
            boolean r3 = r3.t()     // Catch:{ all -> 0x0033 }
            if (r3 == 0) goto L_0x001a
            r1 = 1
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            monitor-exit(r4)
            return r1
        L_0x0030:
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            monitor-exit(r4)
            return r1
        L_0x0033:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0033 }
            throw r1     // Catch:{ all -> 0x0038 }
        L_0x0036:
            monitor-exit(r4)
            return r1
        L_0x0038:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgIsManager.a():boolean");
    }

    public void a(ProgIsSmash progIsSmash, long j2) {
        synchronized (this) {
            a(progIsSmash, "onInterstitialAdReady");
            a(2003, progIsSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j2)}});
            if (this.b == MEDIATION_STATE.STATE_LOADING_SMASHES) {
                a(MEDIATION_STATE.STATE_READY_TO_SHOW);
                ISListenerWrapper.f().d();
                a(2004, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - this.n)}});
                AuctionResponseItem auctionResponseItem = this.e.get(progIsSmash.k());
                if (auctionResponseItem != null) {
                    this.j.b(auctionResponseItem);
                    this.j.a(this.d, this.e, auctionResponseItem);
                } else {
                    String k2 = progIsSmash != null ? progIsSmash.k() : "Smash is null";
                    c("onInterstitialAdReady winner instance " + k2 + " missing from waterfall");
                    a(82317, new Object[][]{new Object[]{"errorCode", 1010}, new Object[]{"reason", "Loaded missing"}, new Object[]{"ext1", k2}});
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00d3, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.ironsource.mediationsdk.logger.IronSourceError r8, com.ironsource.mediationsdk.ProgIsSmash r9, long r10) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d7 }
            r0.<init>()     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = "onInterstitialAdLoadFailed error="
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = r8.b()     // Catch:{ all -> 0x00d7 }
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = " state="
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r1 = r7.b     // Catch:{ all -> 0x00d7 }
            java.lang.String r1 = r1.name()     // Catch:{ all -> 0x00d7 }
            r0.append(r1)     // Catch:{ all -> 0x00d7 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x00d7 }
            r7.a((com.ironsource.mediationsdk.ProgIsSmash) r9, (java.lang.String) r0)     // Catch:{ all -> 0x00d7 }
            r0 = 2200(0x898, float:3.083E-42)
            r1 = 3
            java.lang.Object[][] r1 = new java.lang.Object[r1][]     // Catch:{ all -> 0x00d7 }
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d7 }
            java.lang.String r4 = "errorCode"
            r5 = 0
            r3[r5] = r4     // Catch:{ all -> 0x00d7 }
            int r4 = r8.a()     // Catch:{ all -> 0x00d7 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ all -> 0x00d7 }
            r6 = 1
            r3[r6] = r4     // Catch:{ all -> 0x00d7 }
            r1[r5] = r3     // Catch:{ all -> 0x00d7 }
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d7 }
            java.lang.String r4 = "reason"
            r3[r5] = r4     // Catch:{ all -> 0x00d7 }
            java.lang.String r8 = r8.b()     // Catch:{ all -> 0x00d7 }
            r3[r6] = r8     // Catch:{ all -> 0x00d7 }
            r1[r6] = r3     // Catch:{ all -> 0x00d7 }
            java.lang.Object[] r8 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d7 }
            java.lang.String r3 = "duration"
            r8[r5] = r3     // Catch:{ all -> 0x00d7 }
            java.lang.Long r10 = java.lang.Long.valueOf(r10)     // Catch:{ all -> 0x00d7 }
            r8[r6] = r10     // Catch:{ all -> 0x00d7 }
            r1[r2] = r8     // Catch:{ all -> 0x00d7 }
            r7.a((int) r0, (com.ironsource.mediationsdk.ProgIsSmash) r9, (java.lang.Object[][]) r1)     // Catch:{ all -> 0x00d7 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgIsSmash> r8 = r7.c     // Catch:{ all -> 0x00d7 }
            monitor-enter(r8)     // Catch:{ all -> 0x00d7 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgIsSmash> r9 = r7.d     // Catch:{ all -> 0x00d4 }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x00d4 }
            r10 = 0
        L_0x006a:
            boolean r11 = r9.hasNext()     // Catch:{ all -> 0x00d4 }
            if (r11 == 0) goto L_0x009f
            java.lang.Object r11 = r9.next()     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.ProgIsSmash r11 = (com.ironsource.mediationsdk.ProgIsSmash) r11     // Catch:{ all -> 0x00d4 }
            boolean r0 = r11.l()     // Catch:{ all -> 0x00d4 }
            if (r0 == 0) goto L_0x0097
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r9 = r7.e     // Catch:{ all -> 0x00d4 }
            java.lang.String r10 = r11.k()     // Catch:{ all -> 0x00d4 }
            java.lang.Object r9 = r9.get(r10)     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.AuctionResponseItem r9 = (com.ironsource.mediationsdk.AuctionResponseItem) r9     // Catch:{ all -> 0x00d4 }
            java.lang.String r9 = r9.f()     // Catch:{ all -> 0x00d4 }
            r10 = 2002(0x7d2, float:2.805E-42)
            r7.a((int) r10, (com.ironsource.mediationsdk.ProgIsSmash) r11)     // Catch:{ all -> 0x00d4 }
            r11.a((java.lang.String) r9)     // Catch:{ all -> 0x00d4 }
            monitor-exit(r8)     // Catch:{ all -> 0x00d4 }
            monitor-exit(r7)     // Catch:{ all -> 0x00d7 }
            return
        L_0x0097:
            boolean r11 = r11.r()     // Catch:{ all -> 0x00d4 }
            if (r11 == 0) goto L_0x006a
            r10 = 1
            goto L_0x006a
        L_0x009f:
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r9 = r7.b     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r11 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_LOADING_SMASHES     // Catch:{ all -> 0x00d4 }
            if (r9 != r11) goto L_0x00d1
            if (r10 != 0) goto L_0x00d1
            com.ironsource.mediationsdk.CallbackThrottler r9 = com.ironsource.mediationsdk.CallbackThrottler.b()     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.logger.IronSourceError r10 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x00d4 }
            java.lang.String r11 = "No ads to show"
            r0 = 509(0x1fd, float:7.13E-43)
            r10.<init>(r0, r11)     // Catch:{ all -> 0x00d4 }
            r9.a((com.ironsource.mediationsdk.logger.IronSourceError) r10)     // Catch:{ all -> 0x00d4 }
            r9 = 2110(0x83e, float:2.957E-42)
            java.lang.Object[][] r10 = new java.lang.Object[r6][]     // Catch:{ all -> 0x00d4 }
            java.lang.Object[] r11 = new java.lang.Object[r2]     // Catch:{ all -> 0x00d4 }
            java.lang.String r1 = "errorCode"
            r11[r5] = r1     // Catch:{ all -> 0x00d4 }
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)     // Catch:{ all -> 0x00d4 }
            r11[r6] = r0     // Catch:{ all -> 0x00d4 }
            r10[r5] = r11     // Catch:{ all -> 0x00d4 }
            r7.a((int) r9, (java.lang.Object[][]) r10)     // Catch:{ all -> 0x00d4 }
            com.ironsource.mediationsdk.ProgIsManager$MEDIATION_STATE r9 = com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE.STATE_READY_TO_LOAD     // Catch:{ all -> 0x00d4 }
            r7.a((com.ironsource.mediationsdk.ProgIsManager.MEDIATION_STATE) r9)     // Catch:{ all -> 0x00d4 }
        L_0x00d1:
            monitor-exit(r8)     // Catch:{ all -> 0x00d4 }
            monitor-exit(r7)     // Catch:{ all -> 0x00d7 }
            return
        L_0x00d4:
            r9 = move-exception
            monitor-exit(r8)     // Catch:{ all -> 0x00d4 }
            throw r9     // Catch:{ all -> 0x00d7 }
        L_0x00d7:
            r8 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x00d7 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgIsManager.a(com.ironsource.mediationsdk.logger.IronSourceError, com.ironsource.mediationsdk.ProgIsSmash, long):void");
    }

    public void a(IronSourceError ironSourceError, ProgIsSmash progIsSmash) {
        synchronized (this) {
            a(progIsSmash, "onInterstitialAdShowFailed error=" + ironSourceError.b());
            ISListenerWrapper.f().b(ironSourceError);
            b(2203, progIsSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}});
            a(MEDIATION_STATE.STATE_READY_TO_LOAD);
        }
    }

    public void a(ProgIsSmash progIsSmash) {
        a(2205, progIsSmash);
    }

    /* access modifiers changed from: private */
    public void a(MEDIATION_STATE mediation_state) {
        this.b = mediation_state;
        c("state=" + mediation_state);
    }

    private void a(ProgIsSmash progIsSmash, String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "ProgIsManager " + progIsSmash.k() + " : " + str, 0);
    }

    private void a(int i2) {
        a(i2, (Object[][]) null, false);
    }

    /* access modifiers changed from: private */
    public void a(int i2, Object[][] objArr) {
        a(i2, objArr, false);
    }

    private void a(int i2, Object[][] objArr, boolean z) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put("programmatic", 1);
        if (!TextUtils.isEmpty(this.g)) {
            hashMap.put("auctionId", this.g);
        }
        if (z && !TextUtils.isEmpty(this.f)) {
            hashMap.put("placement", this.f);
        }
        if (c(i2)) {
            InterstitialEventsManager.g().a((Map<String, Object>) hashMap, this.o, this.p);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e2) {
                c("sendMediationEvent " + e2.getMessage());
            }
        }
        InterstitialEventsManager.g().d(new EventData(i2, new JSONObject(hashMap)));
    }

    private void a(int i2, ProgIsSmash progIsSmash) {
        a(i2, progIsSmash, (Object[][]) null, false);
    }

    private void a(int i2, ProgIsSmash progIsSmash, Object[][] objArr) {
        a(i2, progIsSmash, objArr, false);
    }

    private void a(int i2, ProgIsSmash progIsSmash, Object[][] objArr, boolean z) {
        Map<String, Object> n2 = progIsSmash.n();
        if (!TextUtils.isEmpty(this.g)) {
            n2.put("auctionId", this.g);
        }
        if (z && !TextUtils.isEmpty(this.f)) {
            n2.put("placement", this.f);
        }
        if (c(i2)) {
            InterstitialEventsManager.g().a(n2, this.o, this.p);
        }
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    n2.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e2) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "IS sendProviderEvent " + Log.getStackTraceString(e2), 3);
            }
        }
        InterstitialEventsManager.g().d(new EventData(i2, new JSONObject(n2)));
    }
}
