package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.ISDemandOnlyInterstitialListener;

public class ISDemandOnlyListenerWrapper {
    private static final ISDemandOnlyListenerWrapper b = new ISDemandOnlyListenerWrapper();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ISDemandOnlyInterstitialListener f4606a = null;

    private ISDemandOnlyListenerWrapper() {
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, str, 1);
    }

    public void b(final String str) {
        if (this.f4606a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.f4606a.c(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    iSDemandOnlyListenerWrapper.e("onInterstitialAdClosed() instanceId=" + str);
                }
            });
        }
    }

    public void c(final String str) {
        if (this.f4606a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.f4606a.b(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    iSDemandOnlyListenerWrapper.e("onInterstitialAdOpened() instanceId=" + str);
                }
            });
        }
    }

    public void d(final String str) {
        if (this.f4606a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.f4606a.a(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    iSDemandOnlyListenerWrapper.e("onInterstitialAdReady() instanceId=" + str);
                }
            });
        }
    }

    public static ISDemandOnlyListenerWrapper a() {
        return b;
    }

    public void b(final String str, final IronSourceError ironSourceError) {
        if (this.f4606a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.f4606a.b(str, ironSourceError);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    iSDemandOnlyListenerWrapper.e("onInterstitialAdShowFailed() instanceId=" + str + " error=" + ironSourceError.b());
                }
            });
        }
    }

    public void a(final String str, final IronSourceError ironSourceError) {
        if (this.f4606a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.f4606a.a(str, ironSourceError);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    iSDemandOnlyListenerWrapper.e("onInterstitialAdLoadFailed() instanceId=" + str + " error=" + ironSourceError.b());
                }
            });
        }
    }

    public void a(final String str) {
        if (this.f4606a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    ISDemandOnlyListenerWrapper.this.f4606a.d(str);
                    ISDemandOnlyListenerWrapper iSDemandOnlyListenerWrapper = ISDemandOnlyListenerWrapper.this;
                    iSDemandOnlyListenerWrapper.e("onInterstitialAdClicked() instanceId=" + str);
                }
            });
        }
    }
}
