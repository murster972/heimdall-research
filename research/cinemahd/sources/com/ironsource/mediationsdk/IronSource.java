package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.sdk.InterstitialListener;

public abstract class IronSource {

    public enum AD_UNIT {
        REWARDED_VIDEO("rewardedVideo"),
        INTERSTITIAL("interstitial"),
        OFFERWALL("offerwall"),
        BANNER("banner");
        
        private String mValue;

        private AD_UNIT(String str) {
            this.mValue = str;
        }

        public String toString() {
            return this.mValue;
        }
    }

    public static void a(Activity activity, String str, AD_UNIT... ad_unitArr) {
        IronSourceObject.q().a(activity, str, false, ad_unitArr);
    }

    public static void b(Activity activity) {
        IronSourceObject.q().b(activity);
    }

    public static void c() {
        IronSourceObject.q().o();
    }

    public static void a(Activity activity) {
        IronSourceObject.q().a(activity);
    }

    public static void b() {
        IronSourceObject.q().n();
    }

    public static boolean a() {
        return IronSourceObject.q().m();
    }

    public static void b(IronSourceBannerLayout ironSourceBannerLayout) {
        IronSourceObject.q().b(ironSourceBannerLayout);
    }

    public static void a(InterstitialListener interstitialListener) {
        IronSourceObject.q().a(interstitialListener);
    }

    public static IronSourceBannerLayout a(Activity activity, ISBannerSize iSBannerSize) {
        return IronSourceObject.q().a(activity, iSBannerSize);
    }

    public static void a(IronSourceBannerLayout ironSourceBannerLayout) {
        IronSourceObject.q().a(ironSourceBannerLayout);
    }
}
