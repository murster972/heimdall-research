package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InternalOfferwallApi;
import com.ironsource.mediationsdk.sdk.InternalOfferwallListener;
import com.ironsource.mediationsdk.sdk.OfferwallAdapterApi;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

class OfferwallManager implements InternalOfferwallApi, InternalOfferwallListener {

    /* renamed from: a  reason: collision with root package name */
    private final String f4637a = OfferwallManager.class.getName();
    private OfferwallAdapterApi b;
    private InternalOfferwallListener c;
    private IronSourceLoggerManager d = IronSourceLoggerManager.c();
    private AtomicBoolean e = new AtomicBoolean(true);
    private AtomicBoolean f = new AtomicBoolean(false);
    private ServerResponseWrapper g;
    private ProviderSettings h;
    private String i;

    OfferwallManager() {
    }

    public synchronized void a(Activity activity, String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.d;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.b(ironSourceTag, this.f4637a + ":initOfferwall(appKey: " + str + ", userId: " + str2 + ")", 1);
        this.g = IronSourceObject.q().d();
        if (this.g == null) {
            a(ErrorBuilder.a("Please check configurations for Offerwall adapters", "Offerwall"));
            return;
        }
        this.h = this.g.d().b("SupersonicAds");
        if (this.h == null) {
            a(ErrorBuilder.a("Please check configurations for Offerwall adapters", "Offerwall"));
            return;
        }
        AbstractAdapter a2 = a();
        if (a2 == null) {
            a(ErrorBuilder.a("Please check configurations for Offerwall adapters", "Offerwall"));
            return;
        }
        a(a2);
        a2.setLogListener(this.d);
        this.b = (OfferwallAdapterApi) a2;
        this.b.setInternalOfferwallListener(this);
        this.b.initOfferwall(activity, str, str2, this.h.k());
    }

    public void b(boolean z) {
        a(z, (IronSourceError) null);
    }

    public void d(IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.d;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, "onGetOfferwallCreditsFailed(" + ironSourceError + ")", 1);
        InternalOfferwallListener internalOfferwallListener = this.c;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.d(ironSourceError);
        }
    }

    public void e() {
        this.d.b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onOfferwallOpened()", 1);
        JSONObject a2 = IronSourceUtils.a(false);
        try {
            if (!TextUtils.isEmpty(this.i)) {
                a2.put("placement", this.i);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        RewardedVideoEventsManager.g().d(new EventData(305, a2));
        InternalOfferwallListener internalOfferwallListener = this.c;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.e();
        }
    }

    public void f() {
        this.d.b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onOfferwallClosed()", 1);
        InternalOfferwallListener internalOfferwallListener = this.c;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.f();
        }
    }

    public void e(IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.d;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, "onOfferwallShowFailed(" + ironSourceError + ")", 1);
        InternalOfferwallListener internalOfferwallListener = this.c;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.e(ironSourceError);
        }
    }

    public void a(InternalOfferwallListener internalOfferwallListener) {
        this.c = internalOfferwallListener;
    }

    public void a(boolean z, IronSourceError ironSourceError) {
        IronSourceLoggerManager ironSourceLoggerManager = this.d;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, "onOfferwallAvailable(isAvailable: " + z + ")", 1);
        if (z) {
            this.f.set(true);
            InternalOfferwallListener internalOfferwallListener = this.c;
            if (internalOfferwallListener != null) {
                internalOfferwallListener.b(true);
                return;
            }
            return;
        }
        a(ironSourceError);
    }

    public boolean a(int i2, int i3, boolean z) {
        this.d.b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "onOfferwallAdCredited()", 1);
        InternalOfferwallListener internalOfferwallListener = this.c;
        if (internalOfferwallListener != null) {
            return internalOfferwallListener.a(i2, i3, z);
        }
        return false;
    }

    private synchronized void a(IronSourceError ironSourceError) {
        if (this.f != null) {
            this.f.set(false);
        }
        if (this.e != null) {
            this.e.set(true);
        }
        if (this.c != null) {
            this.c.a(false, ironSourceError);
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v7, resolved type: com.ironsource.mediationsdk.AbstractAdapter} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.ironsource.mediationsdk.AbstractAdapter a() {
        /*
            r9 = this;
            java.lang.String r0 = "SupersonicAds"
            r1 = 0
            com.ironsource.mediationsdk.IronSourceObject r2 = com.ironsource.mediationsdk.IronSourceObject.q()     // Catch:{ all -> 0x0053 }
            com.ironsource.mediationsdk.AbstractAdapter r3 = r2.b((java.lang.String) r0)     // Catch:{ all -> 0x0053 }
            if (r3 != 0) goto L_0x004f
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0053 }
            r3.<init>()     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "com.ironsource.adapters."
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = r0.toLowerCase()     // Catch:{ all -> 0x0053 }
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "."
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            r3.append(r0)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "Adapter"
            r3.append(r4)     // Catch:{ all -> 0x0053 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0053 }
            java.lang.Class r3 = java.lang.Class.forName(r3)     // Catch:{ all -> 0x0053 }
            java.lang.String r4 = "startAdapter"
            r5 = 1
            java.lang.Class[] r6 = new java.lang.Class[r5]     // Catch:{ all -> 0x0053 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r8 = 0
            r6[r8] = r7     // Catch:{ all -> 0x0053 }
            java.lang.reflect.Method r4 = r3.getMethod(r4, r6)     // Catch:{ all -> 0x0053 }
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ all -> 0x0053 }
            r5[r8] = r0     // Catch:{ all -> 0x0053 }
            java.lang.Object r0 = r4.invoke(r3, r5)     // Catch:{ all -> 0x0053 }
            r3 = r0
            com.ironsource.mediationsdk.AbstractAdapter r3 = (com.ironsource.mediationsdk.AbstractAdapter) r3     // Catch:{ all -> 0x0053 }
            if (r3 != 0) goto L_0x004f
            return r1
        L_0x004f:
            r2.a((com.ironsource.mediationsdk.AbstractAdapter) r3)     // Catch:{ all -> 0x0053 }
            return r3
        L_0x0053:
            r0 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r2 = r9.d
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            r4 = 2
            java.lang.String r5 = "SupersonicAds initialization failed - please verify that required dependencies are in you build path."
            r2.b(r3, r5, r4)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r2 = r9.d
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = r9.f4637a
            r4.append(r5)
            java.lang.String r5 = ":startOfferwallAdapter"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r2.a((com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag) r3, (java.lang.String) r4, (java.lang.Throwable) r0)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.OfferwallManager.a():com.ironsource.mediationsdk.AbstractAdapter");
    }

    private void a(AbstractAdapter abstractAdapter) {
        try {
            Integer b2 = IronSourceObject.q().b();
            if (b2 != null) {
                abstractAdapter.setAge(b2.intValue());
            }
            String f2 = IronSourceObject.q().f();
            if (f2 != null) {
                abstractAdapter.setGender(f2);
            }
            String i2 = IronSourceObject.q().i();
            if (i2 != null) {
                abstractAdapter.setMediationSegment(i2);
            }
            Boolean c2 = IronSourceObject.q().c();
            if (c2 != null) {
                IronSourceLoggerManager ironSourceLoggerManager = this.d;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                ironSourceLoggerManager.b(ironSourceTag, "Offerwall | setConsent(consent:" + c2 + ")", 1);
                abstractAdapter.setConsent(c2.booleanValue());
            }
        } catch (Exception e2) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.d;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager2.b(ironSourceTag2, ":setCustomParams():" + e2.toString(), 3);
        }
    }
}
