package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.DemandOnlySmash;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.DemandOnlyIsManagerListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import java.util.Date;
import java.util.TimerTask;

public class DemandOnlyIsSmash extends DemandOnlySmash implements InterstitialSmashListener {
    /* access modifiers changed from: private */
    public DemandOnlyIsManagerListener i;
    /* access modifiers changed from: private */
    public long j;

    public DemandOnlyIsSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, DemandOnlyIsManagerListener demandOnlyIsManagerListener, int i2, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.f()), abstractAdapter);
        this.i = demandOnlyIsManagerListener;
        this.f = i2;
        this.f4603a.initInterstitial(activity, str, str2, this.c, this);
    }

    private void q() {
        b("start timer");
        a((TimerTask) new TimerTask() {
            public void run() {
                DemandOnlyIsSmash demandOnlyIsSmash = DemandOnlyIsSmash.this;
                demandOnlyIsSmash.b("load timed out state=" + DemandOnlyIsSmash.this.m());
                if (DemandOnlyIsSmash.this.a(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
                    DemandOnlyIsSmash.this.i.a(new IronSourceError(1052, "load timed out"), DemandOnlyIsSmash.this, new Date().getTime() - DemandOnlyIsSmash.this.j);
                }
            }
        });
    }

    public void c() {
        a("onInterstitialAdOpened");
        this.i.a(this);
    }

    public void d() {
    }

    public void e() {
        a("onInterstitialAdVisible");
        this.i.d(this);
    }

    public void e(IronSourceError ironSourceError) {
    }

    public void onInterstitialAdClicked() {
        a("onInterstitialAdClicked");
        this.i.c(this);
    }

    public void onInterstitialInitSuccess() {
    }

    public void p() {
        b("loadInterstitial state=" + m());
        DemandOnlySmash.SMASH_STATE a2 = a(new DemandOnlySmash.SMASH_STATE[]{DemandOnlySmash.SMASH_STATE.NOT_LOADED, DemandOnlySmash.SMASH_STATE.LOADED}, DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS);
        if (a2 == DemandOnlySmash.SMASH_STATE.NOT_LOADED || a2 == DemandOnlySmash.SMASH_STATE.LOADED) {
            q();
            this.j = new Date().getTime();
            this.f4603a.loadInterstitial(this.c, this);
        } else if (a2 == DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS) {
            this.i.a(new IronSourceError(1050, "load already in progress"), this, 0);
        } else {
            this.i.a(new IronSourceError(1050, "cannot load because show is in progress"), this, 0);
        }
    }

    public void b() {
        a(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        a("onInterstitialAdClosed");
        this.i.b(this);
    }

    public void a() {
        a("onInterstitialAdReady state=" + m());
        o();
        if (a(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.LOADED)) {
            this.i.a(this, new Date().getTime() - this.j);
        }
    }

    public void b(IronSourceError ironSourceError) {
        a(DemandOnlySmash.SMASH_STATE.NOT_LOADED);
        a("onInterstitialAdShowFailed error=" + ironSourceError.b());
        this.i.a(ironSourceError, this);
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyInterstitialSmash " + this.b.d() + " : " + str, 0);
    }

    public void a(IronSourceError ironSourceError) {
        a("onInterstitialAdLoadFailed error=" + ironSourceError.b() + " state=" + m());
        o();
        if (a(DemandOnlySmash.SMASH_STATE.LOAD_IN_PROGRESS, DemandOnlySmash.SMASH_STATE.NOT_LOADED)) {
            this.i.a(ironSourceError, this, new Date().getTime() - this.j);
        }
    }

    private void a(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "DemandOnlyInterstitialSmash " + this.b.d() + " : " + str, 0);
    }
}
