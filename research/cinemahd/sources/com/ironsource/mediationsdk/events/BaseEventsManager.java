package com.ironsource.mediationsdk.events;

import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;
import android.text.TextUtils;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.eventsmodule.DataBaseEventsStorage;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.eventsmodule.EventsSender;
import com.ironsource.eventsmodule.IEventsManager;
import com.ironsource.eventsmodule.IEventsSenderResultListener;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.IronSourceSegment;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

public abstract class BaseEventsManager implements IEventsManager {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4674a;
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public DataBaseEventsStorage c;
    private AbstractEventsFormatter d;
    /* access modifiers changed from: private */
    public ArrayList<EventData> e;
    /* access modifiers changed from: private */
    public boolean f = true;
    /* access modifiers changed from: private */
    public int g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public Context i;
    private int j = 100;
    private int k = 5000;
    private int l = 1;
    private int[] m;
    private Map<String, String> n = new HashMap();
    private Map<String, String> o = new HashMap();
    private String p = "";
    int q;
    String r;
    String s;
    Set<Integer> t;
    /* access modifiers changed from: private */
    public EventThread u;
    private IronSourceSegment v;
    private ServerSegmetData w;
    /* access modifiers changed from: private */
    public IronSourceLoggerManager x;
    private final Object y = new Object();

    private class EventThread extends HandlerThread {

        /* renamed from: a  reason: collision with root package name */
        private Handler f4678a;

        EventThread(BaseEventsManager baseEventsManager, String str) {
            super(str);
        }

        /* access modifiers changed from: package-private */
        public void a(Runnable runnable) {
            this.f4678a.post(runnable);
        }

        /* access modifiers changed from: package-private */
        public void a() {
            this.f4678a = new Handler(getLooper());
        }
    }

    static /* synthetic */ int d(BaseEventsManager baseEventsManager) {
        int i2 = baseEventsManager.g;
        baseEventsManager.g = i2 + 1;
        return i2;
    }

    /* access modifiers changed from: protected */
    public abstract int a(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract String a(int i2);

    /* access modifiers changed from: protected */
    public abstract void b(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract boolean c(EventData eventData);

    /* access modifiers changed from: protected */
    public void d() {
    }

    /* access modifiers changed from: protected */
    public abstract void e(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract boolean f(EventData eventData);

    /* access modifiers changed from: protected */
    public abstract boolean g(EventData eventData);

    /* access modifiers changed from: private */
    public void g() {
        synchronized (this.y) {
            this.c.a((List<EventData>) this.e, this.s);
            this.e.clear();
        }
    }

    /* access modifiers changed from: private */
    public synchronized int h(EventData eventData) {
        return eventData.c() + 90000;
    }

    /* access modifiers changed from: private */
    public boolean i() {
        return (this.g >= this.j || this.b) && this.f4674a;
    }

    /* access modifiers changed from: private */
    public boolean j(EventData eventData) {
        return (eventData.c() == 40 || eventData.c() == 41 || eventData.c() == 50) ? false : true;
    }

    /* access modifiers changed from: private */
    public boolean k(EventData eventData) {
        return (eventData.c() == 14 || eventData.c() == 514 || eventData.c() == 140 || eventData.c() == 40 || eventData.c() == 41 || eventData.c() == 50) ? false : true;
    }

    /* access modifiers changed from: private */
    public boolean l(EventData eventData) {
        int[] iArr;
        if (eventData != null && (iArr = this.m) != null && iArr.length > 0) {
            int c2 = eventData.c();
            int i2 = 0;
            while (true) {
                int[] iArr2 = this.m;
                if (i2 >= iArr2.length) {
                    break;
                } else if (c2 == iArr2[i2]) {
                    return false;
                } else {
                    i2++;
                }
            }
        }
        return true;
    }

    public void f() {
        h();
    }

    private void b(String str) {
        AbstractEventsFormatter abstractEventsFormatter = this.d;
        if (abstractEventsFormatter == null || !abstractEventsFormatter.c().equals(str)) {
            this.d = EventsFormatterFactory.a(str, this.q);
        }
    }

    /* access modifiers changed from: private */
    public void h() {
        ArrayList<EventData> a2;
        this.b = false;
        synchronized (this.y) {
            a2 = a(this.e, this.c.b(this.s), this.k);
            this.e.clear();
            this.c.a(this.s);
        }
        this.g = 0;
        if (a2.size() > 0) {
            JSONObject a3 = GeneralProperties.b().a();
            try {
                a(a3);
                String a4 = a();
                if (!TextUtils.isEmpty(a4)) {
                    a3.put("abt", a4);
                }
                Map<String, String> b2 = b();
                if (!b2.isEmpty()) {
                    for (Map.Entry next : b2.entrySet()) {
                        if (!a3.has((String) next.getKey())) {
                            a3.put((String) next.getKey(), next.getValue());
                        }
                    }
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
            String a5 = this.d.a(a2, a3);
            new EventsSender(new IEventsSenderResultListener() {
                public synchronized void a(final ArrayList<EventData> arrayList, final boolean z) {
                    BaseEventsManager.this.u.a(new Runnable() {
                        public void run() {
                            if (z) {
                                int unused = BaseEventsManager.this.g = BaseEventsManager.this.c.b(BaseEventsManager.this.s).size() + BaseEventsManager.this.e.size();
                            } else if (arrayList != null) {
                                BaseEventsManager.this.x.b(IronSourceLogger.IronSourceTag.INTERNAL, "Failed to send events", 0);
                                BaseEventsManager.this.c.a((List<EventData>) arrayList, BaseEventsManager.this.s);
                                int unused2 = BaseEventsManager.this.g = BaseEventsManager.this.c.b(BaseEventsManager.this.s).size() + BaseEventsManager.this.e.size();
                            }
                        }
                    });
                }
            }).execute(new Object[]{a5, this.d.b(), a2});
        }
    }

    /* access modifiers changed from: private */
    public boolean i(EventData eventData) {
        JSONObject b2 = eventData.b();
        if (b2 == null) {
            return false;
        }
        return b2.has("sessionDepth");
    }

    public void c(int i2) {
        if (i2 > 0) {
            this.k = i2;
        }
    }

    public synchronized void d(final EventData eventData) {
        this.u.a(new Runnable() {
            public void run() {
                if (eventData != null && BaseEventsManager.this.f) {
                    eventData.a("eventSessionId", BaseEventsManager.this.h);
                    String a2 = IronSourceUtils.a(BaseEventsManager.this.i);
                    if (BaseEventsManager.this.j(eventData)) {
                        eventData.a("connectionType", a2);
                    }
                    if (BaseEventsManager.this.a(a2, eventData)) {
                        EventData eventData = eventData;
                        eventData.a(BaseEventsManager.this.h(eventData));
                    }
                    JSONObject b2 = eventData.b();
                    if (b2 != null && b2.has("reason")) {
                        try {
                            String string = b2.getString("reason");
                            eventData.a("reason", string.substring(0, Math.min(string.length(), 50)));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    if (!BaseEventsManager.this.c().isEmpty()) {
                        for (Map.Entry next : BaseEventsManager.this.c().entrySet()) {
                            if (!(eventData.b().has((String) next.getKey()) || next.getKey() == "eventId" || next.getKey() == VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP)) {
                                eventData.a((String) next.getKey(), next.getValue());
                            }
                        }
                    }
                    try {
                        BaseEventsManager.this.x.b(IronSourceLogger.IronSourceTag.EVENT, ("{\"eventId\":" + eventData.c() + ",\"timestamp\":" + eventData.d() + "," + eventData.a().substring(1)).replace(",", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE), 0);
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                    if (BaseEventsManager.this.l(eventData)) {
                        if (BaseEventsManager.this.k(eventData) && !BaseEventsManager.this.i(eventData)) {
                            eventData.a("sessionDepth", Integer.valueOf(BaseEventsManager.this.a(eventData)));
                        }
                        BaseEventsManager.this.b(eventData);
                        if (BaseEventsManager.this.f(eventData)) {
                            BaseEventsManager.this.e(eventData);
                        } else if (!TextUtils.isEmpty(BaseEventsManager.this.a(eventData.c())) && BaseEventsManager.this.g(eventData)) {
                            EventData eventData2 = eventData;
                            eventData2.a("placement", BaseEventsManager.this.a(eventData2.c()));
                        }
                        BaseEventsManager.this.e.add(eventData);
                        BaseEventsManager.d(BaseEventsManager.this);
                    }
                    boolean c = BaseEventsManager.this.c(eventData);
                    if (!BaseEventsManager.this.b && c) {
                        boolean unused = BaseEventsManager.this.b = true;
                    }
                    if (BaseEventsManager.this.c == null) {
                        return;
                    }
                    if (BaseEventsManager.this.i()) {
                        BaseEventsManager.this.h();
                        return;
                    }
                    BaseEventsManager baseEventsManager = BaseEventsManager.this;
                    if (baseEventsManager.a((ArrayList<EventData>) baseEventsManager.e) || c) {
                        BaseEventsManager.this.g();
                    }
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.e = new ArrayList<>();
        this.g = 0;
        this.d = EventsFormatterFactory.a(this.r, this.q);
        this.u = new EventThread(this, this.s + "EventThread");
        this.u.start();
        this.u.a();
        this.x = IronSourceLoggerManager.c();
        this.h = IronSourceObject.q().l();
        this.t = new HashSet();
        d();
    }

    public Map<String, String> c() {
        return this.o;
    }

    public void b(int i2) {
        if (i2 > 0) {
            this.l = i2;
        }
    }

    public void d(int i2) {
        if (i2 > 0) {
            this.j = i2;
        }
    }

    public void b(String str, Context context) {
        if (!TextUtils.isEmpty(str)) {
            this.r = str;
            IronSourceUtils.d(context, this.s, str);
            b(str);
        }
    }

    public synchronized void a(Context context, IronSourceSegment ironSourceSegment) {
        this.r = IronSourceUtils.a(context, this.s, this.r);
        b(this.r);
        this.d.a(IronSourceUtils.b(context, this.s, (String) null));
        this.c = DataBaseEventsStorage.a(context, "supersonic_sdk.db", 5);
        g();
        this.m = IronSourceUtils.a(context, this.s);
        this.v = ironSourceSegment;
        this.i = context;
    }

    public void b(boolean z) {
        this.f = z;
    }

    public Map<String, String> b() {
        return this.n;
    }

    public void b(Map<String, String> map) {
        this.o.putAll(map);
    }

    public synchronized void a(ServerSegmetData serverSegmetData) {
        this.w = serverSegmetData;
    }

    /* access modifiers changed from: private */
    public synchronized boolean a(String str, EventData eventData) {
        return str.equalsIgnoreCase(ViewProps.NONE) && this.t.contains(Integer.valueOf(eventData.c()));
    }

    private ArrayList<EventData> a(ArrayList<EventData> arrayList, ArrayList<EventData> arrayList2, int i2) {
        ArrayList arrayList3 = new ArrayList();
        arrayList3.addAll(arrayList);
        arrayList3.addAll(arrayList2);
        Collections.sort(arrayList3, new Comparator<EventData>(this) {
            /* renamed from: a */
            public int compare(EventData eventData, EventData eventData2) {
                return eventData.d() >= eventData2.d() ? 1 : -1;
            }
        });
        if (arrayList3.size() <= i2) {
            return new ArrayList<>(arrayList3);
        }
        ArrayList<EventData> arrayList4 = new ArrayList<>(arrayList3.subList(0, i2));
        this.c.a((List<EventData>) arrayList3.subList(i2, arrayList3.size()), this.s);
        return arrayList4;
    }

    public void a(int[] iArr, Context context) {
        this.m = iArr;
        IronSourceUtils.a(context, this.s, iArr);
    }

    public void a(String str, Context context) {
        if (!TextUtils.isEmpty(str)) {
            AbstractEventsFormatter abstractEventsFormatter = this.d;
            if (abstractEventsFormatter != null) {
                abstractEventsFormatter.a(str);
            }
            IronSourceUtils.e(context, this.s, str);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(ArrayList<EventData> arrayList) {
        return arrayList != null && arrayList.size() >= this.l;
    }

    public void a(boolean z) {
        this.f4674a = z;
    }

    public void a(EventData eventData, String str) {
        try {
            ArrayList arrayList = new ArrayList();
            arrayList.add(eventData);
            String a2 = this.d.a(arrayList, GeneralProperties.b().a());
            new EventsSender().execute(new Object[]{a2, str, null});
        } catch (Exception unused) {
        }
    }

    private void a(JSONObject jSONObject) {
        try {
            if (this.v != null) {
                if (this.v.a() > 0) {
                    jSONObject.put("age", this.v.a());
                }
                if (!TextUtils.isEmpty(this.v.b())) {
                    jSONObject.put("gen", this.v.b());
                }
                if (this.v.e() > 0) {
                    jSONObject.put("lvl", this.v.e());
                }
                if (this.v.d() != null) {
                    jSONObject.put("pay", this.v.d().get());
                }
                if (this.v.c() > 0.0d) {
                    jSONObject.put("iapt", this.v.c());
                }
                if (this.v.g() > 0) {
                    jSONObject.put("ucd", this.v.g());
                }
            }
            if (this.w != null) {
                String b2 = this.w.b();
                if (!TextUtils.isEmpty(b2)) {
                    jSONObject.put("segmentId", b2);
                }
                JSONObject a2 = this.w.a();
                Iterator<String> keys = a2.keys();
                while (keys.hasNext()) {
                    String next = keys.next();
                    jSONObject.put(next, a2.get(next));
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void a(String str) {
        this.p = str;
    }

    public String a() {
        return this.p;
    }

    public void a(Map<String, String> map) {
        this.n.putAll(map);
    }

    public void a(Map<String, Object> map, int i2, String str) {
        map.put("auctionTrials", Integer.valueOf(i2));
        if (!TextUtils.isEmpty(str)) {
            map.put("auctionFallback", str);
        }
    }
}
