package com.ironsource.mediationsdk.events;

import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;

class EventsFormatterFactory {
    EventsFormatterFactory() {
    }

    static AbstractEventsFormatter a(String str, int i) {
        if ("ironbeast".equals(str)) {
            return new IronbeastEventsFormatter(i);
        }
        if ("outcome".equals(str)) {
            return new OutcomeEventsFormatter(i);
        }
        if (i == 2) {
            return new IronbeastEventsFormatter(i);
        }
        if (i == 3) {
            return new OutcomeEventsFormatter(i);
        }
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        c.b(ironSourceTag, "EventsFormatterFactory failed to instantiate a formatter (type: " + str + ", adUnit: " + i + ")", 2);
        return null;
    }
}
