package com.ironsource.mediationsdk.events;

import android.text.TextUtils;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

abstract class AbstractEventsFormatter {

    /* renamed from: a  reason: collision with root package name */
    JSONObject f4673a;
    int b;
    private String c;

    AbstractEventsFormatter() {
    }

    private String a(int i) {
        if (i == 2) {
            return "InterstitialEvents";
        }
        if (i != 3) {
        }
        return "events";
    }

    /* access modifiers changed from: protected */
    public abstract String a();

    public abstract String a(ArrayList<EventData> arrayList, JSONObject jSONObject);

    /* access modifiers changed from: package-private */
    public JSONObject a(EventData eventData) {
        try {
            JSONObject jSONObject = new JSONObject(eventData.a());
            jSONObject.put("eventId", eventData.c());
            jSONObject.put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, eventData.d());
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return TextUtils.isEmpty(this.c) ? a() : this.c;
    }

    public abstract String c();

    /* access modifiers changed from: package-private */
    public String a(JSONArray jSONArray) {
        try {
            if (this.f4673a == null) {
                return "";
            }
            JSONObject jSONObject = new JSONObject(this.f4673a.toString());
            jSONObject.put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, IronSourceUtils.d());
            jSONObject.put("adUnit", this.b);
            jSONObject.put(a(this.b), jSONArray);
            return jSONObject.toString();
        } catch (Exception unused) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.c = str;
    }
}
