package com.ironsource.mediationsdk.events;

import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.utils.SessionDepthManager;

public class InterstitialEventsManager extends BaseEventsManager {
    private static InterstitialEventsManager A;
    private String z = "";

    private InterstitialEventsManager() {
        this.r = "ironbeast";
        this.q = 2;
        this.s = "IS";
    }

    public static synchronized InterstitialEventsManager g() {
        InterstitialEventsManager interstitialEventsManager;
        synchronized (InterstitialEventsManager.class) {
            if (A == null) {
                A = new InterstitialEventsManager();
                A.e();
            }
            interstitialEventsManager = A;
        }
        return interstitialEventsManager;
    }

    /* access modifiers changed from: protected */
    public int a(EventData eventData) {
        return SessionDepthManager.a().a(eventData.c() >= 3000 && eventData.c() < 4000 ? 3 : 2);
    }

    /* access modifiers changed from: protected */
    public void b(EventData eventData) {
        if (eventData.c() == 2204) {
            SessionDepthManager.a().b(2);
        } else if (eventData.c() == 3305) {
            SessionDepthManager.a().b(3);
        }
    }

    /* access modifiers changed from: protected */
    public boolean c(EventData eventData) {
        int c = eventData.c();
        return c == 2204 || c == 2004 || c == 2005 || c == 2301 || c == 2300 || c == 3005 || c == 3015;
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.t.add(Integer.valueOf(AdError.INTERNAL_ERROR_CODE));
        this.t.add(Integer.valueOf(AdError.CACHE_ERROR_CODE));
        this.t.add(2004);
        this.t.add(2211);
        this.t.add(2212);
    }

    /* access modifiers changed from: protected */
    public void e(EventData eventData) {
        this.z = eventData.b().optString("placement");
    }

    /* access modifiers changed from: protected */
    public boolean f(EventData eventData) {
        return false;
    }

    /* access modifiers changed from: protected */
    public boolean g(EventData eventData) {
        return false;
    }

    /* access modifiers changed from: protected */
    public String a(int i) {
        return this.z;
    }
}
