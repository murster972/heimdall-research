package com.ironsource.mediationsdk.events;

import com.facebook.common.util.UriUtil;
import com.ironsource.eventsmodule.EventData;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class IronbeastEventsFormatter extends AbstractEventsFormatter {
    IronbeastEventsFormatter(int i) {
        this.b = i;
    }

    public String a() {
        return "https://outcome-ssp.supersonicads.com/mediation?adUnit=2";
    }

    public String a(ArrayList<EventData> arrayList, JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        if (jSONObject == null) {
            this.f4673a = new JSONObject();
        } else {
            this.f4673a = jSONObject;
        }
        try {
            JSONArray jSONArray = new JSONArray();
            if (arrayList != null && !arrayList.isEmpty()) {
                Iterator<EventData> it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    JSONObject a2 = a(it2.next());
                    if (a2 != null) {
                        jSONArray.put(a2);
                    }
                }
            }
            jSONObject2.put("table", "super.dwh.mediation_events");
            jSONObject2.put(UriUtil.DATA_SCHEME, a(jSONArray));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject2.toString();
    }

    public String c() {
        return "ironbeast";
    }
}
