package com.ironsource.mediationsdk.events;

import android.os.Handler;
import android.os.HandlerThread;
import com.ironsource.mediationsdk.logger.ThreadExceptionHandler;

public class SuperLooper extends Thread {
    private static SuperLooper b;

    /* renamed from: a  reason: collision with root package name */
    private SupersonicSdkThread f4679a = new SupersonicSdkThread(this, SuperLooper.class.getSimpleName());

    private class SupersonicSdkThread extends HandlerThread {

        /* renamed from: a  reason: collision with root package name */
        private Handler f4680a;

        SupersonicSdkThread(SuperLooper superLooper, String str) {
            super(str);
            setUncaughtExceptionHandler(new ThreadExceptionHandler());
        }

        /* access modifiers changed from: package-private */
        public Handler a() {
            return this.f4680a;
        }

        /* access modifiers changed from: package-private */
        public void b() {
            this.f4680a = new Handler(getLooper());
        }
    }

    private SuperLooper() {
        this.f4679a.start();
        this.f4679a.b();
    }

    public static synchronized SuperLooper a() {
        SuperLooper superLooper;
        synchronized (SuperLooper.class) {
            if (b == null) {
                b = new SuperLooper();
            }
            superLooper = b;
        }
        return superLooper;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0013, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(java.lang.Runnable r2) {
        /*
            r1 = this;
            monitor-enter(r1)
            com.ironsource.mediationsdk.events.SuperLooper$SupersonicSdkThread r0 = r1.f4679a     // Catch:{ all -> 0x0014 }
            if (r0 != 0) goto L_0x0007
            monitor-exit(r1)
            return
        L_0x0007:
            com.ironsource.mediationsdk.events.SuperLooper$SupersonicSdkThread r0 = r1.f4679a     // Catch:{ all -> 0x0014 }
            android.os.Handler r0 = r0.a()     // Catch:{ all -> 0x0014 }
            if (r0 == 0) goto L_0x0012
            r0.post(r2)     // Catch:{ all -> 0x0014 }
        L_0x0012:
            monitor-exit(r1)
            return
        L_0x0014:
            r2 = move-exception
            monitor-exit(r1)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.events.SuperLooper.a(java.lang.Runnable):void");
    }
}
