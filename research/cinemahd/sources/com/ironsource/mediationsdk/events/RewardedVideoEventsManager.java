package com.ironsource.mediationsdk.events;

import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.utils.SessionDepthManager;

public class RewardedVideoEventsManager extends BaseEventsManager {
    private static RewardedVideoEventsManager A;
    private String z = "";

    private RewardedVideoEventsManager() {
        this.r = "outcome";
        this.q = 3;
        this.s = "RV";
    }

    public static synchronized RewardedVideoEventsManager g() {
        RewardedVideoEventsManager rewardedVideoEventsManager;
        synchronized (RewardedVideoEventsManager.class) {
            if (A == null) {
                A = new RewardedVideoEventsManager();
                A.e();
            }
            rewardedVideoEventsManager = A;
        }
        return rewardedVideoEventsManager;
    }

    /* access modifiers changed from: protected */
    public int a(EventData eventData) {
        if (eventData.c() == 15 || (eventData.c() >= 300 && eventData.c() < 400)) {
            return SessionDepthManager.a().a(0);
        }
        return SessionDepthManager.a().a(1);
    }

    /* access modifiers changed from: protected */
    public void b(EventData eventData) {
        if (eventData.c() == 1203) {
            SessionDepthManager.a().b(1);
        } else if (eventData.c() == 305) {
            SessionDepthManager.a().b(0);
        }
    }

    /* access modifiers changed from: protected */
    public boolean c(EventData eventData) {
        int c = eventData.c();
        return c == 14 || c == 514 || c == 305 || c == 1003 || c == 1005 || c == 1203 || c == 1010 || c == 1301 || c == 1302;
    }

    /* access modifiers changed from: protected */
    public void d() {
        this.t.add(1001);
        this.t.add(1209);
        this.t.add(1210);
        this.t.add(1211);
    }

    /* access modifiers changed from: protected */
    public void e(EventData eventData) {
        if (eventData.c() == 15 || (eventData.c() >= 300 && eventData.c() < 400)) {
            this.z = eventData.b().optString("placement");
        }
    }

    /* access modifiers changed from: protected */
    public boolean f(EventData eventData) {
        return false;
    }

    /* access modifiers changed from: protected */
    public String a(int i) {
        return (i == 15 || (i >= 300 && i < 400)) ? this.z : "";
    }

    /* access modifiers changed from: protected */
    public boolean g(EventData eventData) {
        return eventData.c() == 305;
    }
}
