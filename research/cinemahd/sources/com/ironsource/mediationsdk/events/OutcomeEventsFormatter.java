package com.ironsource.mediationsdk.events;

import com.ironsource.eventsmodule.EventData;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

class OutcomeEventsFormatter extends AbstractEventsFormatter {
    OutcomeEventsFormatter(int i) {
        this.b = i;
    }

    public String a() {
        return "https://outcome-ssp.supersonicads.com/mediation?adUnit=3";
    }

    public String a(ArrayList<EventData> arrayList, JSONObject jSONObject) {
        if (jSONObject == null) {
            this.f4673a = new JSONObject();
        } else {
            this.f4673a = jSONObject;
        }
        JSONArray jSONArray = new JSONArray();
        if (arrayList != null && !arrayList.isEmpty()) {
            Iterator<EventData> it2 = arrayList.iterator();
            while (it2.hasNext()) {
                JSONObject a2 = a(it2.next());
                if (a2 != null) {
                    jSONArray.put(a2);
                }
            }
        }
        return a(jSONArray);
    }

    public String c() {
        return "outcome";
    }
}
