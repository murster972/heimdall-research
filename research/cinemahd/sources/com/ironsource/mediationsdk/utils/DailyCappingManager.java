package com.ironsource.mediationsdk.utils;

import android.content.Context;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;

public class DailyCappingManager {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, Integer> f4731a;
    private Map<String, Integer> b;
    private Map<String, String> c;
    private String d;
    private Context e;
    private Timer f = null;
    private DailyCappingListener g;
    private IronSourceLoggerManager h;

    public DailyCappingManager(String str, DailyCappingListener dailyCappingListener) {
        this.d = str;
        this.g = dailyCappingListener;
        this.f4731a = new HashMap();
        this.b = new HashMap();
        this.c = new HashMap();
        this.h = IronSourceLoggerManager.c();
        d();
    }

    private int e(String str) {
        if (!a().equalsIgnoreCase(b(str))) {
            f(str);
        }
        return a(str);
    }

    private void f(String str) {
        this.b.put(str, 0);
        this.c.put(str, a());
        IronSourceUtils.b(this.e, c(str), 0);
        IronSourceUtils.f(this.e, d(str), a());
    }

    public void b(AbstractSmash abstractSmash) {
        synchronized (this) {
            try {
                String e2 = e(abstractSmash);
                if (this.f4731a.containsKey(e2)) {
                    a(e2, e(e2) + 1);
                }
            } catch (Exception e3) {
                this.h.a(IronSourceLogger.IronSourceTag.INTERNAL, "increaseShowCounter", (Throwable) e3);
            }
        }
    }

    public boolean c(AbstractSmash abstractSmash) {
        synchronized (this) {
            boolean z = false;
            try {
                String e2 = e(abstractSmash);
                if (!this.f4731a.containsKey(e2)) {
                    return false;
                }
                if (this.f4731a.get(e2).intValue() <= e(e2)) {
                    z = true;
                }
                return z;
            } catch (Exception e3) {
                this.h.a(IronSourceLogger.IronSourceTag.INTERNAL, "isCapped", (Throwable) e3);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    public boolean d(AbstractSmash abstractSmash) {
        synchronized (this) {
            boolean z = false;
            try {
                String e2 = e(abstractSmash);
                if (!this.f4731a.containsKey(e2)) {
                    return false;
                }
                if (a().equalsIgnoreCase(b(e2))) {
                    return false;
                }
                if (this.f4731a.get(e2).intValue() <= a(e2)) {
                    z = true;
                }
                return z;
            } catch (Exception e3) {
                this.h.a(IronSourceLogger.IronSourceTag.INTERNAL, "shouldSendCapReleasedEvent", (Throwable) e3);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
    }

    public void a(Context context) {
        this.e = context;
    }

    public void a(AbstractSmash abstractSmash) {
        synchronized (this) {
            try {
                if (abstractSmash.q() != 99) {
                    this.f4731a.put(e(abstractSmash), Integer.valueOf(abstractSmash.q()));
                }
            } catch (Exception e2) {
                this.h.a(IronSourceLogger.IronSourceTag.INTERNAL, "addSmash", (Throwable) e2);
            }
        }
    }

    private String e(AbstractSmash abstractSmash) {
        return this.d + "_" + abstractSmash.w() + "_" + abstractSmash.u();
    }

    private int a(String str) {
        if (this.b.containsKey(str)) {
            return this.b.get(str).intValue();
        }
        int a2 = IronSourceUtils.a(this.e, c(str), 0);
        this.b.put(str, Integer.valueOf(a2));
        return a2;
    }

    private Date b() {
        Random random = new Random();
        GregorianCalendar gregorianCalendar = new GregorianCalendar(TimeZone.getTimeZone("UTC"), Locale.US);
        gregorianCalendar.set(11, 0);
        gregorianCalendar.set(12, random.nextInt(10));
        gregorianCalendar.set(13, random.nextInt(60));
        gregorianCalendar.set(14, random.nextInt(1000));
        gregorianCalendar.add(5, 1);
        return gregorianCalendar.getTime();
    }

    /* access modifiers changed from: private */
    public void c() {
        synchronized (this) {
            try {
                for (String f2 : this.f4731a.keySet()) {
                    f(f2);
                }
                this.g.b();
                d();
            } catch (Exception e2) {
                this.h.a(IronSourceLogger.IronSourceTag.INTERNAL, "onTimerTick", (Throwable) e2);
            }
        }
    }

    private void d() {
        Timer timer = this.f;
        if (timer != null) {
            timer.cancel();
        }
        this.f = new Timer();
        this.f.schedule(new TimerTask() {
            public void run() {
                DailyCappingManager.this.c();
            }
        }, b());
    }

    private void a(String str, int i) {
        this.b.put(str, Integer.valueOf(i));
        this.c.put(str, a());
        IronSourceUtils.b(this.e, c(str), i);
        IronSourceUtils.f(this.e, d(str), a());
    }

    private String a() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return simpleDateFormat.format(new Date());
    }

    private String c(String str) {
        return str + "_counter";
    }

    private String b(String str) {
        if (this.c.containsKey(str)) {
            return this.c.get(str);
        }
        String c2 = IronSourceUtils.c(this.e, d(str), a());
        this.c.put(str, c2);
        return c2;
    }

    private String d(String str) {
        return str + "_day";
    }
}
