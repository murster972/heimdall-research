package com.ironsource.mediationsdk.utils;

public class SessionDepthManager {
    private static SessionDepthManager e;

    /* renamed from: a  reason: collision with root package name */
    private int f4738a = 1;
    private int b = 1;
    private int c = 1;
    private int d = 1;

    public static synchronized SessionDepthManager a() {
        SessionDepthManager sessionDepthManager;
        synchronized (SessionDepthManager.class) {
            if (e == null) {
                e = new SessionDepthManager();
            }
            sessionDepthManager = e;
        }
        return sessionDepthManager;
    }

    public synchronized void b(int i) {
        if (i == 0) {
            this.c++;
        } else if (i == 1) {
            this.f4738a++;
        } else if (i == 2) {
            this.b++;
        } else if (i == 3) {
            this.d++;
        }
    }

    public synchronized int a(int i) {
        if (i == 0) {
            return this.c;
        } else if (i == 1) {
            return this.f4738a;
        } else if (i == 2) {
            return this.b;
        } else if (i != 3) {
            return -1;
        } else {
            return this.d;
        }
    }
}
