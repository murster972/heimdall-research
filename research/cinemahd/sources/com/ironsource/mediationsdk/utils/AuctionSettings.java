package com.ironsource.mediationsdk.utils;

public class AuctionSettings {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4728a;
    private String b;
    private String c;
    private int d;
    private long e;
    private long f;
    private long g;
    private long h;
    private boolean i;

    AuctionSettings() {
        this.b = "";
        this.c = "";
        this.f4728a = false;
        this.f = 0;
        this.g = 0;
        this.h = 0;
        this.i = true;
    }

    public String a() {
        return this.b;
    }

    public long b() {
        return this.g;
    }

    public boolean c() {
        return this.i;
    }

    public boolean d() {
        return this.f4728a;
    }

    public int e() {
        return this.d;
    }

    public long f() {
        return this.h;
    }

    public long g() {
        return this.f;
    }

    public long h() {
        return this.e;
    }

    public String i() {
        return this.c;
    }

    AuctionSettings(String str, String str2, int i2, long j, boolean z, long j2, long j3, long j4, boolean z2) {
        this.b = str;
        this.c = str2;
        this.d = i2;
        this.e = j;
        this.f4728a = z;
        this.f = j2;
        this.g = j3;
        this.h = j4;
        this.i = z2;
    }
}
