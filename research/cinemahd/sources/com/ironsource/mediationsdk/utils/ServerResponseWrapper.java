package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ApplicationConfigurations;
import com.ironsource.mediationsdk.model.ApplicationEvents;
import com.ironsource.mediationsdk.model.ApplicationLogger;
import com.ironsource.mediationsdk.model.BannerConfigurations;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.Configurations;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.OfferwallConfigurations;
import com.ironsource.mediationsdk.model.OfferwallPlacement;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.PlacementAvailabilitySettings;
import com.ironsource.mediationsdk.model.PlacementCappingType;
import com.ironsource.mediationsdk.model.ProviderOrder;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.ProviderSettingsHolder;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.model.ServerSegmetData;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ServerResponseWrapper {

    /* renamed from: a  reason: collision with root package name */
    private ProviderOrder f4736a;
    private ProviderSettingsHolder b;
    private Configurations c;
    private String d;
    private String e;
    private JSONObject f;
    private Context g;

    public ServerResponseWrapper(Context context, String str, String str2, String str3) {
        this.g = context;
        try {
            if (TextUtils.isEmpty(str3)) {
                this.f = new JSONObject();
            } else {
                this.f = new JSONObject(str3);
            }
            l();
            j();
            k();
            this.d = TextUtils.isEmpty(str) ? "" : str;
            this.e = TextUtils.isEmpty(str2) ? "" : str2;
        } catch (JSONException e2) {
            e2.printStackTrace();
            h();
        }
    }

    private boolean a(String str) {
        String lowerCase = str.toLowerCase();
        return this.b.a("Mediation") && ("SupersonicAds".toLowerCase().equals(lowerCase) || "IronSource".toLowerCase().equals(lowerCase) || "RIS".toLowerCase().equals(lowerCase));
    }

    private InterstitialPlacement c(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            PlacementAvailabilitySettings a2 = a(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                InterstitialPlacement interstitialPlacement = new InterstitialPlacement(optInt, optString, optBoolean, a2);
                if (a2 == null) {
                    return interstitialPlacement;
                }
                CappingManager.a(this.g, interstitialPlacement);
                return interstitialPlacement;
            }
        }
        return null;
    }

    private OfferwallPlacement d(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                return new OfferwallPlacement(optInt, optString, optBoolean);
            }
        }
        return null;
    }

    private Placement e(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            String optString2 = jSONObject.optString("virtualItemName", "");
            int optInt2 = jSONObject.optInt("virtualItemCount", -1);
            PlacementAvailabilitySettings a2 = a(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString) && !TextUtils.isEmpty(optString2) && optInt2 > 0) {
                Placement placement = new Placement(optInt, optString, optBoolean, optString2, optInt2, a2);
                if (a2 == null) {
                    return placement;
                }
                CappingManager.a(this.g, placement);
                return placement;
            }
        }
        return null;
    }

    private void h() {
        this.f = new JSONObject();
        this.d = "";
        this.e = "";
        this.f4736a = new ProviderOrder();
        this.b = ProviderSettingsHolder.b();
        this.c = new Configurations();
    }

    private Context i() {
        return this.g;
    }

    private void j() {
        String str;
        String str2;
        JSONObject jSONObject;
        String str3;
        JSONObject jSONObject2;
        String str4;
        RewardedVideoConfigurations rewardedVideoConfigurations;
        String str5;
        String str6;
        JSONObject jSONObject3;
        InterstitialConfigurations interstitialConfigurations;
        String str7;
        String str8;
        JSONObject jSONObject4;
        String str9;
        String str10;
        String str11;
        BannerConfigurations bannerConfigurations;
        String str12;
        String str13;
        String str14;
        String str15;
        String str16;
        String str17;
        OfferwallConfigurations offerwallConfigurations;
        ServerSegmetData serverSegmetData;
        JSONObject a2;
        int[] iArr;
        int[] iArr2;
        int[] iArr3;
        AuctionSettings auctionSettings;
        String str18;
        String str19;
        int[] iArr4;
        AuctionSettings auctionSettings2;
        try {
            JSONObject a3 = a(this.f, "configurations");
            JSONObject a4 = a(a3, "adUnits");
            JSONObject a5 = a(a3, "application");
            JSONObject a6 = a(a4, "rewardedVideo");
            JSONObject a7 = a(a4, "interstitial");
            JSONObject a8 = a(a4, "offerwall");
            JSONObject a9 = a(a4, "banner");
            JSONObject a10 = a(a5, "events");
            JSONObject a11 = a(a5, "loggers");
            JSONObject a12 = a(a5, "segment");
            JSONObject a13 = a(a5, "auction");
            if (a5 != null) {
                IronSourceUtils.b(this.g, "uuidEnabled", a5.optBoolean("uuidEnabled", true));
            }
            if (a10 != null) {
                String optString = a10.optString("abt");
                if (!TextUtils.isEmpty(optString)) {
                    InterstitialEventsManager.g().a(optString);
                    RewardedVideoEventsManager.g().a(optString);
                }
            }
            JSONObject jSONObject5 = a12;
            JSONObject jSONObject6 = a11;
            JSONObject jSONObject7 = a8;
            if (a6 != null) {
                JSONArray optJSONArray = a6.optJSONArray("placements");
                jSONObject3 = a9;
                JSONObject a14 = a(a6, "events");
                str6 = "events";
                str5 = "placements";
                int a15 = a(a6, a5, "maxNumOfAdaptersToLoadOnStart", 2);
                int a16 = a(a6, a5, "adapterTimeOutInSeconds", 60);
                int a17 = a(a6, a5, "loadRVInterval", 300);
                JSONObject a18 = IronSourceUtils.a(a14, a10);
                boolean optBoolean = a18.optBoolean("sendUltraEvents", false);
                boolean optBoolean2 = a18.optBoolean("sendEventsToggle", false);
                String optString2 = a18.optString("serverEventsURL", "");
                String optString3 = a18.optString("serverEventsType", "");
                int optInt = a18.optInt("backupThreshold", -1);
                int optInt2 = a18.optInt("maxNumberOfEvents", -1);
                int optInt3 = a18.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray2 = a18.optJSONArray("optOut");
                if (optJSONArray2 != null) {
                    int[] iArr5 = new int[optJSONArray2.length()];
                    str = "maxNumberOfEvents";
                    for (int i = 0; i < optJSONArray2.length(); i++) {
                        iArr5[i] = optJSONArray2.optInt(i);
                    }
                    iArr4 = iArr5;
                } else {
                    str = "maxNumberOfEvents";
                    iArr4 = null;
                }
                ApplicationEvents applicationEvents = new ApplicationEvents(optBoolean, optBoolean2, optString2, optString3, optInt, optInt2, optInt3, iArr4);
                if (a13 != null) {
                    JSONObject a19 = a(a13, "rewardedVideo");
                    String optString4 = a13.optString("auctionData", "");
                    String optString5 = a13.optString("auctioneerURL", "");
                    int optInt4 = a13.optInt("auctionTrials", 2);
                    str4 = "backupThreshold";
                    long optLong = a13.optLong("auctionTimeout", 10000);
                    boolean optBoolean3 = a19.optBoolean("programmatic", false);
                    int optInt5 = a19.optInt("minTimeBeforeFirstAuction", AdError.SERVER_ERROR_CODE);
                    jSONObject2 = a13;
                    str3 = "serverEventsType";
                    long j = (long) optInt5;
                    str2 = "serverEventsURL";
                    jSONObject = a10;
                    auctionSettings2 = new AuctionSettings(optString4, optString5, optInt4, optLong, optBoolean3, j, (long) a19.optInt("auctionRetryInterval", HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT), (long) a19.optInt("timeToWaitBeforeAuction", 5000), a19.optBoolean("isAuctionOnShowStart", true));
                } else {
                    jSONObject2 = a13;
                    str3 = "serverEventsType";
                    str2 = "serverEventsURL";
                    jSONObject = a10;
                    str4 = "backupThreshold";
                    auctionSettings2 = new AuctionSettings();
                }
                RewardedVideoConfigurations rewardedVideoConfigurations2 = new RewardedVideoConfigurations(a15, a16, a17, applicationEvents, auctionSettings2);
                if (optJSONArray != null) {
                    for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                        Placement e2 = e(optJSONArray.optJSONObject(i2));
                        if (e2 != null) {
                            rewardedVideoConfigurations2.a(e2);
                        }
                    }
                }
                String optString6 = a6.optString("backFill");
                if (!TextUtils.isEmpty(optString6)) {
                    rewardedVideoConfigurations2.a(optString6);
                }
                String optString7 = a6.optString("premium");
                if (!TextUtils.isEmpty(optString7)) {
                    rewardedVideoConfigurations2.b(optString7);
                }
                rewardedVideoConfigurations = rewardedVideoConfigurations2;
            } else {
                str6 = "events";
                jSONObject2 = a13;
                str3 = "serverEventsType";
                str2 = "serverEventsURL";
                jSONObject3 = a9;
                jSONObject = a10;
                str5 = "placements";
                str = "maxNumberOfEvents";
                str4 = "backupThreshold";
                rewardedVideoConfigurations = null;
            }
            if (a7 != null) {
                str10 = str5;
                JSONArray optJSONArray3 = a7.optJSONArray(str10);
                str9 = str6;
                JSONObject a20 = a(a7, str9);
                int a21 = a(a7, a5, "maxNumOfAdaptersToLoadOnStart", 2);
                int a22 = a(a7, a5, "adapterTimeOutInSeconds", 60);
                int a23 = a(a7, a5, "delayLoadFailure", 3);
                jSONObject4 = jSONObject;
                JSONObject a24 = IronSourceUtils.a(a20, jSONObject4);
                boolean optBoolean4 = a24.optBoolean("sendEventsToggle", false);
                String str20 = str2;
                String optString8 = a24.optString(str20, "");
                str11 = str3;
                String optString9 = a24.optString(str11, "");
                str8 = str4;
                int optInt6 = a24.optInt(str8, -1);
                String str21 = str;
                int optInt7 = a24.optInt(str21, -1);
                int optInt8 = a24.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray4 = a24.optJSONArray("optOut");
                if (optJSONArray4 != null) {
                    int[] iArr6 = new int[optJSONArray4.length()];
                    str7 = "sendEventsToggle";
                    for (int i3 = 0; i3 < optJSONArray4.length(); i3++) {
                        iArr6[i3] = optJSONArray4.optInt(i3);
                    }
                    iArr3 = iArr6;
                } else {
                    str7 = "sendEventsToggle";
                    iArr3 = null;
                }
                ApplicationEvents applicationEvents2 = new ApplicationEvents(false, optBoolean4, optString8, optString9, optInt6, optInt7, optInt8, iArr3);
                if (jSONObject2 != null) {
                    JSONObject jSONObject8 = jSONObject2;
                    JSONObject a25 = a(jSONObject8, "interstitial");
                    str18 = str20;
                    str19 = str21;
                    auctionSettings = new AuctionSettings(jSONObject8.optString("auctionData", ""), jSONObject8.optString("auctioneerURL", ""), jSONObject8.optInt("auctionTrials", 2), jSONObject8.optLong("auctionTimeout", 10000), a25.optBoolean("programmatic", false), (long) a25.optInt("minTimeBeforeFirstAuction", AdError.SERVER_ERROR_CODE), 0, 0, true);
                } else {
                    str18 = str20;
                    str19 = str21;
                    auctionSettings = new AuctionSettings();
                }
                InterstitialConfigurations interstitialConfigurations2 = new InterstitialConfigurations(a21, a22, applicationEvents2, auctionSettings, a23);
                if (optJSONArray3 != null) {
                    for (int i4 = 0; i4 < optJSONArray3.length(); i4++) {
                        InterstitialPlacement c2 = c(optJSONArray3.optJSONObject(i4));
                        if (c2 != null) {
                            interstitialConfigurations2.a(c2);
                        }
                    }
                }
                String optString10 = a7.optString("backFill");
                if (!TextUtils.isEmpty(optString10)) {
                    interstitialConfigurations2.b(optString10);
                }
                String optString11 = a7.optString("premium");
                if (!TextUtils.isEmpty(optString11)) {
                    interstitialConfigurations2.c(optString11);
                }
                interstitialConfigurations = interstitialConfigurations2;
            } else {
                str9 = str6;
                str10 = str5;
                str8 = str4;
                str11 = str3;
                jSONObject4 = jSONObject;
                str7 = "sendEventsToggle";
                interstitialConfigurations = null;
            }
            if (jSONObject3 != null) {
                JSONObject jSONObject9 = jSONObject3;
                JSONArray optJSONArray5 = jSONObject9.optJSONArray(str10);
                JSONObject a26 = a(jSONObject9, str9);
                int a27 = a(jSONObject9, a5, "maxNumOfAdaptersToLoadOnStart", 1);
                String str22 = str7;
                str12 = str10;
                str13 = str9;
                JSONArray jSONArray = optJSONArray5;
                str15 = str2;
                str14 = str22;
                str16 = str11;
                long a28 = a(jSONObject9, a5, "atim", 10000);
                int a29 = a(jSONObject9, a5, "delayLoadFailure", 3);
                int a30 = a(jSONObject9, a5, "bannerInterval", 60);
                JSONObject a31 = IronSourceUtils.a(a26, jSONObject4);
                boolean optBoolean5 = a31.optBoolean(str14, false);
                String optString12 = a31.optString(str15, "");
                String optString13 = a31.optString(str16, "");
                int optInt9 = a31.optInt(str8, -1);
                str17 = str;
                int optInt10 = a31.optInt(str17, -1);
                int optInt11 = a31.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray6 = a31.optJSONArray("optOut");
                if (optJSONArray6 != null) {
                    int[] iArr7 = new int[optJSONArray6.length()];
                    for (int i5 = 0; i5 < optJSONArray6.length(); i5++) {
                        iArr7[i5] = optJSONArray6.optInt(i5);
                    }
                    iArr2 = iArr7;
                } else {
                    iArr2 = null;
                }
                BannerConfigurations bannerConfigurations2 = new BannerConfigurations(a27, a28, new ApplicationEvents(false, optBoolean5, optString12, optString13, optInt9, optInt10, optInt11, iArr2), a30, a29);
                if (jSONArray != null) {
                    int i6 = 0;
                    while (i6 < jSONArray.length()) {
                        JSONArray jSONArray2 = jSONArray;
                        BannerPlacement b2 = b(jSONArray2.optJSONObject(i6));
                        if (b2 != null) {
                            bannerConfigurations2.a(b2);
                        }
                        i6++;
                        jSONArray = jSONArray2;
                    }
                }
                bannerConfigurations = bannerConfigurations2;
            } else {
                str13 = str9;
                str14 = str7;
                str15 = str2;
                str17 = str;
                str12 = str10;
                str16 = str11;
                bannerConfigurations = null;
            }
            String str23 = str13;
            if (jSONObject7 != null) {
                JSONObject jSONObject10 = jSONObject7;
                JSONObject a32 = IronSourceUtils.a(a(jSONObject10, str23), jSONObject4);
                boolean optBoolean6 = a32.optBoolean(str14, false);
                String optString14 = a32.optString(str15, "");
                String optString15 = a32.optString(str16, "");
                int optInt12 = a32.optInt(str8, -1);
                int optInt13 = a32.optInt(str17, -1);
                int optInt14 = a32.optInt("maxEventsPerBatch", 5000);
                JSONArray optJSONArray7 = a32.optJSONArray("optOut");
                if (optJSONArray7 != null) {
                    int[] iArr8 = new int[optJSONArray7.length()];
                    for (int i7 = 0; i7 < optJSONArray7.length(); i7++) {
                        iArr8[i7] = optJSONArray7.optInt(i7);
                    }
                    iArr = iArr8;
                } else {
                    iArr = null;
                }
                OfferwallConfigurations offerwallConfigurations2 = new OfferwallConfigurations(new ApplicationEvents(false, optBoolean6, optString14, optString15, optInt12, optInt13, optInt14, iArr));
                JSONArray optJSONArray8 = jSONObject10.optJSONArray(str12);
                if (optJSONArray8 != null) {
                    for (int i8 = 0; i8 < optJSONArray8.length(); i8++) {
                        OfferwallPlacement d2 = d(optJSONArray8.optJSONObject(i8));
                        if (d2 != null) {
                            offerwallConfigurations2.a(d2);
                        }
                    }
                }
                offerwallConfigurations = offerwallConfigurations2;
            } else {
                offerwallConfigurations = null;
            }
            JSONObject jSONObject11 = jSONObject6;
            ApplicationLogger applicationLogger = new ApplicationLogger(jSONObject11.optInt("server", 3), jSONObject11.optInt("publisher", 3), jSONObject11.optInt("console", 3));
            if (jSONObject5 != null) {
                JSONObject jSONObject12 = jSONObject5;
                serverSegmetData = new ServerSegmetData(jSONObject12.optString(MediationMetaData.KEY_NAME, ""), jSONObject12.optString("id", "-1"), jSONObject12.optJSONObject("custom"));
            } else {
                serverSegmetData = null;
            }
            this.c = new Configurations(rewardedVideoConfigurations, interstitialConfigurations, offerwallConfigurations, bannerConfigurations, new ApplicationConfigurations(applicationLogger, serverSegmetData, a5.optBoolean("integration", false)));
            JSONObject a33 = a(jSONObject4, "genericParams");
            if (!(a33 == null || (a2 = a(a33, str23)) == null)) {
                a33.remove(str23);
                Map<String, String> a34 = IronSourceUtils.a(a2);
                RewardedVideoEventsManager.g().b(a34);
                InterstitialEventsManager.g().b(a34);
            }
            if (a33 != null) {
                Map<String, String> a35 = IronSourceUtils.a(a33);
                RewardedVideoEventsManager.g().a(a35);
                InterstitialEventsManager.g().a(a35);
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void k() {
        try {
            JSONObject a2 = a(this.f, "providerOrder");
            JSONArray optJSONArray = a2.optJSONArray("rewardedVideo");
            JSONArray optJSONArray2 = a2.optJSONArray("interstitial");
            JSONArray optJSONArray3 = a2.optJSONArray("banner");
            this.f4736a = new ProviderOrder();
            if (!(optJSONArray == null || a() == null || a().e() == null)) {
                String a3 = a().e().a();
                String d2 = a().e().d();
                for (int i = 0; i < optJSONArray.length(); i++) {
                    String optString = optJSONArray.optString(i);
                    if (optString.equals(a3)) {
                        this.f4736a.f(a3);
                    } else {
                        if (optString.equals(d2)) {
                            this.f4736a.g(d2);
                        }
                        this.f4736a.c(optString);
                        ProviderSettings b2 = ProviderSettingsHolder.b().b(optString);
                        if (b2 != null) {
                            b2.c(i);
                        }
                    }
                }
            }
            if (!(optJSONArray2 == null || a() == null || a().c() == null)) {
                String a4 = a().c().a();
                String h = a().c().h();
                for (int i2 = 0; i2 < optJSONArray2.length(); i2++) {
                    String optString2 = optJSONArray2.optString(i2);
                    if (optString2.equals(a4)) {
                        this.f4736a.d(a4);
                    } else {
                        if (optString2.equals(h)) {
                            this.f4736a.e(h);
                        }
                        this.f4736a.b(optString2);
                        ProviderSettings b3 = ProviderSettingsHolder.b().b(optString2);
                        if (b3 != null) {
                            b3.b(i2);
                        }
                    }
                }
            }
            if (optJSONArray3 != null) {
                for (int i3 = 0; i3 < optJSONArray3.length(); i3++) {
                    String optString3 = optJSONArray3.optString(i3);
                    this.f4736a.a(optString3);
                    ProviderSettings b4 = ProviderSettingsHolder.b().b(optString3);
                    if (b4 != null) {
                        b4.a(i3);
                    }
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void l() {
        try {
            this.b = ProviderSettingsHolder.b();
            JSONObject a2 = a(this.f, "providerSettings");
            Iterator<String> keys = a2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                JSONObject optJSONObject = a2.optJSONObject(next);
                if (optJSONObject != null) {
                    boolean optBoolean = optJSONObject.optBoolean("mpis", false);
                    String optString = optJSONObject.optString("spId", "0");
                    String optString2 = optJSONObject.optString("adSourceName", (String) null);
                    String optString3 = optJSONObject.optString("providerLoadName", next);
                    JSONObject a3 = a(optJSONObject, "adUnits");
                    JSONObject a4 = a(optJSONObject, "application");
                    JSONObject a5 = a(a3, "rewardedVideo");
                    JSONObject a6 = a(a3, "interstitial");
                    JSONObject a7 = a(a3, "banner");
                    JSONObject a8 = IronSourceUtils.a(a5, a4);
                    JSONObject a9 = IronSourceUtils.a(a6, a4);
                    JSONObject a10 = IronSourceUtils.a(a7, a4);
                    if (this.b.a(next)) {
                        ProviderSettings b2 = this.b.b(next);
                        JSONObject k = b2.k();
                        JSONObject f2 = b2.f();
                        JSONObject d2 = b2.d();
                        b2.c(IronSourceUtils.a(k, a8));
                        b2.b(IronSourceUtils.a(f2, a9));
                        b2.a(IronSourceUtils.a(d2, a10));
                        b2.a(optBoolean);
                        b2.b(optString);
                        b2.a(optString2);
                    } else if (a(optString3)) {
                        ProviderSettings b3 = this.b.b("Mediation");
                        JSONObject k2 = b3.k();
                        JSONObject f3 = b3.f();
                        JSONObject d3 = b3.d();
                        JSONObject jSONObject = new JSONObject(k2.toString());
                        JSONObject jSONObject2 = new JSONObject(f3.toString());
                        JSONObject jSONObject3 = new JSONObject(d3.toString());
                        ProviderSettings providerSettings = new ProviderSettings(next, optString3, a4, IronSourceUtils.a(jSONObject, a8), IronSourceUtils.a(jSONObject2, a9), IronSourceUtils.a(jSONObject3, a10));
                        providerSettings.a(optBoolean);
                        providerSettings.b(optString);
                        providerSettings.a(optString2);
                        this.b.a(providerSettings);
                    } else {
                        ProviderSettings providerSettings2 = new ProviderSettings(next, optString3, a4, a8, a9, a10);
                        providerSettings2.a(optBoolean);
                        providerSettings2.b(optString);
                        providerSettings2.a(optString2);
                        this.b.a(providerSettings2);
                    }
                }
            }
            this.b.a();
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public List<IronSource.AD_UNIT> b() {
        ProviderOrder providerOrder;
        ProviderOrder providerOrder2;
        if (this.f == null || this.c == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        if (!(this.c.e() == null || (providerOrder2 = this.f4736a) == null || providerOrder2.e().size() <= 0)) {
            arrayList.add(IronSource.AD_UNIT.REWARDED_VIDEO);
        }
        if (!(this.c.c() == null || (providerOrder = this.f4736a) == null || providerOrder.b().size() <= 0)) {
            arrayList.add(IronSource.AD_UNIT.INTERSTITIAL);
        }
        if (this.c.d() != null) {
            arrayList.add(IronSource.AD_UNIT.OFFERWALL);
        }
        if (this.c.b() != null) {
            arrayList.add(IronSource.AD_UNIT.BANNER);
        }
        return arrayList;
    }

    public String f() {
        try {
            return this.f4736a.d();
        } catch (Exception e2) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.INTERNAL, "getRVPremiumProvider", (Throwable) e2);
            return null;
        }
    }

    public boolean g() {
        if (!((((this.f != null) && !this.f.has("error")) && this.f4736a != null) && this.b != null) || this.c == null) {
            return false;
        }
        return true;
    }

    public String toString() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("appKey", this.d);
            jSONObject.put("userId", this.e);
            jSONObject.put("response", this.f);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject.toString();
    }

    private int a(JSONObject jSONObject, JSONObject jSONObject2, String str, int i) {
        int i2;
        if (jSONObject.has(str)) {
            i2 = jSONObject.optInt(str, 0);
        } else {
            i2 = jSONObject2.has(str) ? jSONObject2.optInt(str, 0) : 0;
        }
        return i2 == 0 ? i : i2;
    }

    public ProviderSettingsHolder d() {
        return this.b;
    }

    public ProviderOrder c() {
        return this.f4736a;
    }

    private long a(JSONObject jSONObject, JSONObject jSONObject2, String str, long j) {
        long j2;
        if (jSONObject.has(str)) {
            j2 = jSONObject.optLong(str, 0);
        } else {
            j2 = jSONObject2.has(str) ? jSONObject2.optLong(str, 0) : 0;
        }
        return j2 == 0 ? j : j2;
    }

    public String e() {
        try {
            return this.f4736a.c();
        } catch (Exception e2) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.INTERNAL, "getRVBackFillProvider", (Throwable) e2);
            return null;
        }
    }

    public ServerResponseWrapper(ServerResponseWrapper serverResponseWrapper) {
        try {
            this.g = serverResponseWrapper.i();
            this.f = new JSONObject(serverResponseWrapper.f.toString());
            this.d = serverResponseWrapper.d;
            this.e = serverResponseWrapper.e;
            this.f4736a = serverResponseWrapper.c();
            this.b = serverResponseWrapper.d();
            this.c = serverResponseWrapper.a();
        } catch (Exception unused) {
            h();
        }
    }

    private BannerPlacement b(JSONObject jSONObject) {
        if (jSONObject != null) {
            int optInt = jSONObject.optInt("placementId", -1);
            String optString = jSONObject.optString("placementName", "");
            boolean optBoolean = jSONObject.optBoolean("isDefault", false);
            PlacementAvailabilitySettings a2 = a(jSONObject);
            if (optInt >= 0 && !TextUtils.isEmpty(optString)) {
                BannerPlacement bannerPlacement = new BannerPlacement(optInt, optString, optBoolean, a2);
                if (a2 == null) {
                    return bannerPlacement;
                }
                CappingManager.a(this.g, bannerPlacement);
                return bannerPlacement;
            }
        }
        return null;
    }

    private PlacementAvailabilitySettings a(JSONObject jSONObject) {
        PlacementCappingType placementCappingType = null;
        if (jSONObject == null) {
            return null;
        }
        PlacementAvailabilitySettings.PlacementAvailabilitySettingsBuilder placementAvailabilitySettingsBuilder = new PlacementAvailabilitySettings.PlacementAvailabilitySettingsBuilder();
        boolean z = true;
        placementAvailabilitySettingsBuilder.a(jSONObject.optBoolean("delivery", true));
        JSONObject optJSONObject = jSONObject.optJSONObject("capping");
        if (optJSONObject != null) {
            String optString = optJSONObject.optString("unit");
            if (!TextUtils.isEmpty(optString)) {
                if (PlacementCappingType.PER_DAY.toString().equals(optString)) {
                    placementCappingType = PlacementCappingType.PER_DAY;
                } else if (PlacementCappingType.PER_HOUR.toString().equals(optString)) {
                    placementCappingType = PlacementCappingType.PER_HOUR;
                }
            }
            int optInt = optJSONObject.optInt("maxImpressions", 0);
            placementAvailabilitySettingsBuilder.a(optJSONObject.optBoolean(ViewProps.ENABLED, false) && optInt > 0, placementCappingType, optInt);
        }
        JSONObject optJSONObject2 = jSONObject.optJSONObject("pacing");
        if (optJSONObject2 != null) {
            int optInt2 = optJSONObject2.optInt("numOfSeconds", 0);
            if (!optJSONObject2.optBoolean(ViewProps.ENABLED, false) || optInt2 <= 0) {
                z = false;
            }
            placementAvailabilitySettingsBuilder.a(z, optInt2);
        }
        return placementAvailabilitySettingsBuilder.a();
    }

    private JSONObject a(JSONObject jSONObject, String str) {
        if (jSONObject != null) {
            return jSONObject.optJSONObject(str);
        }
        return null;
    }

    public Configurations a() {
        return this.c;
    }
}
