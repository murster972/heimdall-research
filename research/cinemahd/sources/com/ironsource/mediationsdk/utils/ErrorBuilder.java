package com.ironsource.mediationsdk.utils;

import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceError;

public class ErrorBuilder {
    public static IronSourceError a(String str, String str2, String str3) {
        String str4;
        StringBuilder sb = new StringBuilder();
        sb.append("Init Fail - ");
        sb.append(str);
        sb.append(" value ");
        sb.append(str2);
        sb.append(" is not valid");
        if (!TextUtils.isEmpty(str3)) {
            str4 = " - " + str3;
        } else {
            str4 = "";
        }
        sb.append(str4);
        return new IronSourceError(506, sb.toString());
    }

    public static IronSourceError b(String str, String str2) {
        return new IronSourceError(509, str + " Show Fail - " + str2);
    }

    public static IronSourceError c(String str, String str2) {
        return new IronSourceError(502, "Mediation - Unable to retrieve configurations from IronSource server, using cached configurations with appKey:" + str + " and userId:" + str2);
    }

    public static IronSourceError d(String str) {
        return new IronSourceError(509, str + " Show Fail - No ads to show");
    }

    public static IronSourceError e(String str) {
        return new IronSourceError(520, "" + str + " Show Fail - No Internet connection");
    }

    public static IronSourceError f(String str) {
        return new IronSourceError(527, str + " The requested instance does not exist");
    }

    public static IronSourceError g(String str) {
        return new IronSourceError(616, str + " unsupported banner size");
    }

    public static IronSourceError b(String str) {
        if (TextUtils.isEmpty(str)) {
            str = "An error occurred";
        }
        return new IronSourceError(510, str);
    }

    public static IronSourceError c(String str) {
        String str2;
        if (TextUtils.isEmpty(str)) {
            str2 = "Load failed due to an unknown error";
        } else {
            str2 = "Load failed - " + str;
        }
        return new IronSourceError(510, str2);
    }

    public static IronSourceError a(String str, String str2) {
        String str3;
        if (TextUtils.isEmpty(str)) {
            str3 = str2 + " init failed due to an unknown error";
        } else {
            str3 = str2 + " - " + str;
        }
        return new IronSourceError(508, str3);
    }

    public static IronSourceError a(String str) {
        return new IronSourceError(524, str);
    }
}
