package com.ironsource.mediationsdk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Environment;
import android.os.StatFs;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import com.applovin.sdk.AppLovinEventTypes;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.GeneralProperties;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class GeneralPropertiesWorker implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final String f4733a = GeneralPropertiesWorker.class.getSimpleName();
    private Context b;

    private GeneralPropertiesWorker() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006e  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:32:0x00a8  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00bd  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x00d5  */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00e4  */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f3  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x0102  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x014b  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x0169  */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x017c  */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x018b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.Map<java.lang.String, java.lang.Object> a() {
        /*
            r6 = this;
            java.lang.String r0 = ""
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            com.ironsource.mediationsdk.IronSourceObject r2 = com.ironsource.mediationsdk.IronSourceObject.q()
            java.lang.String r2 = r2.l()
            java.lang.String r3 = "sessionId"
            r1.put(r3, r2)
            java.lang.String r2 = r6.e()
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L_0x0034
            java.lang.String r3 = "bundleId"
            r1.put(r3, r2)
            android.content.Context r3 = r6.b
            java.lang.String r2 = com.ironsource.environment.ApplicationContext.a((android.content.Context) r3, (java.lang.String) r2)
            boolean r3 = android.text.TextUtils.isEmpty(r2)
            if (r3 != 0) goto L_0x0034
            java.lang.String r3 = "appVersion"
            r1.put(r3, r2)
        L_0x0034:
            java.lang.String r2 = r6.c()
            java.lang.String r3 = "appKey"
            r1.put(r3, r2)
            r2 = 0
            android.content.Context r3 = r6.b     // Catch:{ Exception -> 0x0064 }
            java.lang.String[] r3 = com.ironsource.environment.DeviceStatus.b((android.content.Context) r3)     // Catch:{ Exception -> 0x0064 }
            if (r3 == 0) goto L_0x0064
            int r4 = r3.length     // Catch:{ Exception -> 0x0064 }
            r5 = 2
            if (r4 != r5) goto L_0x0064
            r4 = r3[r2]     // Catch:{ Exception -> 0x0064 }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0064 }
            if (r4 != 0) goto L_0x0055
            r4 = r3[r2]     // Catch:{ Exception -> 0x0064 }
            goto L_0x0056
        L_0x0055:
            r4 = r0
        L_0x0056:
            r5 = 1
            r3 = r3[r5]     // Catch:{ Exception -> 0x0062 }
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r3)     // Catch:{ Exception -> 0x0062 }
            boolean r2 = r3.booleanValue()     // Catch:{ Exception -> 0x0062 }
            goto L_0x0065
        L_0x0062:
            goto L_0x0065
        L_0x0064:
            r4 = r0
        L_0x0065:
            boolean r3 = android.text.TextUtils.isEmpty(r4)
            if (r3 != 0) goto L_0x006e
            java.lang.String r0 = "GAID"
            goto L_0x007c
        L_0x006e:
            android.content.Context r3 = r6.b
            java.lang.String r4 = com.ironsource.environment.DeviceStatus.j(r3)
            boolean r3 = android.text.TextUtils.isEmpty(r4)
            if (r3 != 0) goto L_0x007c
            java.lang.String r0 = "UUID"
        L_0x007c:
            boolean r3 = android.text.TextUtils.isEmpty(r4)
            if (r3 != 0) goto L_0x0095
            java.lang.String r3 = "advertisingId"
            r1.put(r3, r4)
            java.lang.String r3 = "advertisingIdType"
            r1.put(r3, r0)
            java.lang.Boolean r0 = java.lang.Boolean.valueOf(r2)
            java.lang.String r2 = "isLimitAdTrackingEnabled"
            r1.put(r2, r0)
        L_0x0095:
            java.lang.String r0 = r6.h()
            java.lang.String r2 = "deviceOS"
            r1.put(r2, r0)
            java.lang.String r0 = r6.b()
            boolean r0 = android.text.TextUtils.isEmpty(r0)
            if (r0 != 0) goto L_0x00b1
            java.lang.String r0 = r6.b()
            java.lang.String r2 = "osVersion"
            r1.put(r2, r0)
        L_0x00b1:
            android.content.Context r0 = r6.b
            java.lang.String r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.a((android.content.Context) r0)
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x00c2
            java.lang.String r2 = "connectionType"
            r1.put(r2, r0)
        L_0x00c2:
            java.lang.String r0 = r6.r()
            java.lang.String r2 = "sdkVersion"
            r1.put(r2, r0)
            java.lang.String r0 = r6.l()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x00da
            java.lang.String r2 = "language"
            r1.put(r2, r0)
        L_0x00da:
            java.lang.String r0 = r6.g()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x00e9
            java.lang.String r2 = "deviceOEM"
            r1.put(r2, r0)
        L_0x00e9:
            java.lang.String r0 = r6.f()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x00f8
            java.lang.String r2 = "deviceModel"
            r1.put(r2, r0)
        L_0x00f8:
            java.lang.String r0 = r6.n()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0107
            java.lang.String r2 = "mobileCarrier"
            r1.put(r2, r0)
        L_0x0107:
            long r2 = r6.k()
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            java.lang.String r2 = "internalFreeMemory"
            r1.put(r2, r0)
            long r2 = r6.i()
            java.lang.Long r0 = java.lang.Long.valueOf(r2)
            java.lang.String r2 = "externalFreeMemory"
            r1.put(r2, r0)
            int r0 = r6.d()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r2 = "battery"
            r1.put(r2, r0)
            int r0 = r6.j()
            boolean r2 = r6.a(r0)
            if (r2 == 0) goto L_0x0141
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r2 = "gmtMinutesOffset"
            r1.put(r2, r0)
        L_0x0141:
            java.lang.String r0 = r6.p()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0150
            java.lang.String r2 = "pluginType"
            r1.put(r2, r0)
        L_0x0150:
            java.lang.String r0 = r6.q()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x015f
            java.lang.String r2 = "pluginVersion"
            r1.put(r2, r0)
        L_0x015f:
            java.lang.String r0 = r6.o()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x016e
            java.lang.String r2 = "plugin_fw_v"
            r1.put(r2, r0)
        L_0x016e:
            boolean r0 = com.ironsource.environment.DeviceStatus.m()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0181
            java.lang.String r2 = "jb"
            r1.put(r2, r0)
        L_0x0181:
            java.lang.String r0 = r6.m()
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0190
            java.lang.String r2 = "mt"
            r1.put(r2, r0)
        L_0x0190:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.GeneralPropertiesWorker.a():java.util.Map");
    }

    private String b() {
        try {
            String str = Build.VERSION.RELEASE;
            int i = Build.VERSION.SDK_INT;
            return "" + i + "(" + str + ")";
        } catch (Exception unused) {
            return "";
        }
    }

    private String c() {
        return IronSourceObject.q().g();
    }

    private int d() {
        try {
            Intent registerReceiver = this.b.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = 0;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra(AppLovinEventTypes.USER_COMPLETED_LEVEL, -1) : 0;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("scale", -1);
            }
            if (intExtra == -1 || i == -1) {
                return -1;
            }
            return (int) ((((float) intExtra) / ((float) i)) * 100.0f);
        } catch (Exception e) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            c.a(ironSourceTag, this.f4733a + ":getBatteryLevel()", (Throwable) e);
            return -1;
        }
    }

    private String e() {
        try {
            return this.b.getPackageName();
        } catch (Exception unused) {
            return "";
        }
    }

    private String f() {
        try {
            return Build.MODEL;
        } catch (Exception unused) {
            return "";
        }
    }

    private String g() {
        try {
            return Build.MANUFACTURER;
        } catch (Exception unused) {
            return "";
        }
    }

    private String h() {
        return "Android";
    }

    private long i() {
        if (!s()) {
            return -1;
        }
        StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getPath());
        return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private int j() {
        try {
            TimeZone timeZone = TimeZone.getDefault();
            return Math.round((float) (((timeZone.getOffset(GregorianCalendar.getInstance(timeZone).getTimeInMillis()) / 1000) / 60) / 15)) * 15;
        } catch (Exception e) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            c.a(ironSourceTag, this.f4733a + ":getGmtMinutesOffset()", (Throwable) e);
            return 0;
        }
    }

    private long k() {
        try {
            StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
            return (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize())) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
        } catch (Exception unused) {
            return -1;
        }
    }

    private String l() {
        try {
            return Locale.getDefault().getLanguage();
        } catch (Exception unused) {
            return "";
        }
    }

    private String m() {
        return IronSourceObject.q().j();
    }

    private String n() {
        try {
            TelephonyManager telephonyManager = (TelephonyManager) this.b.getSystemService("phone");
            if (telephonyManager == null) {
                return "";
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (!networkOperatorName.equals("")) {
                return networkOperatorName;
            }
            return "";
        } catch (Exception e) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            c.a(ironSourceTag, this.f4733a + ":getMobileCarrier()", (Throwable) e);
            return "";
        }
    }

    private String o() {
        try {
            return ConfigFile.d().a();
        } catch (Exception e) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.NATIVE, "getPluginFrameworkVersion()", (Throwable) e);
            return "";
        }
    }

    private String p() {
        try {
            return ConfigFile.d().b();
        } catch (Exception e) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.NATIVE, "getPluginType()", (Throwable) e);
            return "";
        }
    }

    private String q() {
        try {
            return ConfigFile.d().c();
        } catch (Exception e) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.NATIVE, "getPluginVersion()", (Throwable) e);
            return "";
        }
    }

    private String r() {
        return IronSourceUtils.b();
    }

    private boolean s() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception unused) {
            return false;
        }
    }

    public void run() {
        try {
            GeneralProperties.b().a(a());
            IronSourceUtils.a(this.b, GeneralProperties.b().a());
        } catch (Exception e) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            c.a(ironSourceTag, "Thread name = " + GeneralPropertiesWorker.class.getSimpleName(), (Throwable) e);
        }
    }

    public GeneralPropertiesWorker(Context context) {
        this.b = context.getApplicationContext();
    }

    private boolean a(int i) {
        return i <= 840 && i >= -720 && i % 15 == 0;
    }
}
