package com.ironsource.mediationsdk.utils;

import android.text.TextUtils;
import android.util.Base64;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceAES {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f4734a = false;

    public static synchronized String a(String str, String str2) {
        synchronized (IronSourceAES.class) {
            if (TextUtils.isEmpty(str)) {
                return "";
            }
            if (TextUtils.isEmpty(str2)) {
                return "";
            }
            try {
                SecretKeySpec a2 = a(str);
                byte[] bArr = new byte[16];
                Arrays.fill(bArr, (byte) 0);
                IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
                byte[] decode = Base64.decode(str2, 0);
                Cipher instance = Cipher.getInstance("AES/CBC/PKCS7Padding");
                instance.init(2, a2, ivParameterSpec);
                String str3 = new String(instance.doFinal(decode));
                return str3;
            } catch (Exception e) {
                e.printStackTrace();
                if (!f4734a) {
                    f4734a = true;
                    JSONObject a3 = IronSourceUtils.a(false);
                    try {
                        a3.put(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, "false");
                        a3.put("errorCode", 1);
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                    InterstitialEventsManager.g().a(new EventData(114, a3), "https://outcome-ssp.supersonicads.com/mediation?adUnit=2");
                }
                return "";
            }
        }
    }

    public static synchronized String b(String str, String str2) {
        synchronized (IronSourceAES.class) {
            if (TextUtils.isEmpty(str)) {
                return "";
            }
            if (TextUtils.isEmpty(str2)) {
                return "";
            }
            try {
                SecretKeySpec a2 = a(str);
                byte[] bytes = str2.getBytes("UTF8");
                byte[] bArr = new byte[16];
                Arrays.fill(bArr, (byte) 0);
                IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr);
                Cipher instance = Cipher.getInstance("AES/CBC/PKCS7Padding");
                instance.init(1, a2, ivParameterSpec);
                String replaceAll = Base64.encodeToString(instance.doFinal(bytes), 0).replaceAll(System.getProperty("line.separator"), "");
                return replaceAll;
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }
    }

    private static SecretKeySpec a(String str) throws UnsupportedEncodingException {
        byte[] bArr = new byte[32];
        Arrays.fill(bArr, (byte) 0);
        byte[] bytes = str.getBytes("UTF-8");
        System.arraycopy(bytes, 0, bArr, 0, bytes.length < bArr.length ? bytes.length : bArr.length);
        return new SecretKeySpec(bArr, "AES");
    }
}
