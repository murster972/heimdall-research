package com.ironsource.mediationsdk.utils;

import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.PlacementAvailabilitySettings;
import com.ironsource.mediationsdk.model.PlacementCappingType;
import java.util.Calendar;
import java.util.TimeZone;

public class CappingManager {

    /* renamed from: com.ironsource.mediationsdk.utils.CappingManager$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4729a = new int[PlacementCappingType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.ironsource.mediationsdk.model.PlacementCappingType[] r0 = com.ironsource.mediationsdk.model.PlacementCappingType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f4729a = r0
                int[] r0 = f4729a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.mediationsdk.model.PlacementCappingType r1 = com.ironsource.mediationsdk.model.PlacementCappingType.PER_DAY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f4729a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.mediationsdk.model.PlacementCappingType r1 = com.ironsource.mediationsdk.model.PlacementCappingType.PER_HOUR     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.AnonymousClass1.<clinit>():void");
        }
    }

    public enum ECappingStatus {
        CAPPED_PER_DELIVERY,
        CAPPED_PER_COUNT,
        CAPPED_PER_PACE,
        NOT_CAPPED
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void a(android.content.Context r3, com.ironsource.mediationsdk.model.InterstitialPlacement r4) {
        /*
            java.lang.Class<com.ironsource.mediationsdk.utils.CappingManager> r0 = com.ironsource.mediationsdk.utils.CappingManager.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x001e
            if (r4 != 0) goto L_0x0008
            goto L_0x001e
        L_0x0008:
            com.ironsource.mediationsdk.model.PlacementAvailabilitySettings r1 = r4.a()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)
            return
        L_0x0010:
            java.lang.String r2 = "Interstitial"
            java.lang.String r4 = r4.c()     // Catch:{ all -> 0x001b }
            a(r3, r2, r4, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        L_0x001e:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.a(android.content.Context, com.ironsource.mediationsdk.model.InterstitialPlacement):void");
    }

    public static synchronized void b(Context context, InterstitialPlacement interstitialPlacement) {
        synchronized (CappingManager.class) {
            if (interstitialPlacement != null) {
                a(context, "Interstitial", interstitialPlacement.c());
            }
        }
    }

    public static synchronized ECappingStatus c(Context context, InterstitialPlacement interstitialPlacement) {
        synchronized (CappingManager.class) {
            if (!(context == null || interstitialPlacement == null)) {
                if (interstitialPlacement.a() != null) {
                    ECappingStatus b = b(context, "Interstitial", interstitialPlacement.c());
                    return b;
                }
            }
            ECappingStatus eCappingStatus = ECappingStatus.NOT_CAPPED;
            return eCappingStatus;
        }
    }

    public static synchronized boolean d(Context context, String str) {
        boolean z;
        synchronized (CappingManager.class) {
            z = b(context, "Interstitial", str) != ECappingStatus.NOT_CAPPED;
        }
        return z;
    }

    public static synchronized void b(Context context, String str) {
        synchronized (CappingManager.class) {
            a(context, "Interstitial", str);
        }
    }

    public static synchronized boolean c(Context context, String str) {
        boolean z;
        synchronized (CappingManager.class) {
            z = b(context, "Banner", str) != ECappingStatus.NOT_CAPPED;
        }
        return z;
    }

    private static ECappingStatus b(Context context, String str, String str2) {
        long currentTimeMillis = System.currentTimeMillis();
        if (!IronSourceUtils.a(context, a(str, "CappingManager.IS_DELIVERY_ENABLED", str2), true)) {
            return ECappingStatus.CAPPED_PER_DELIVERY;
        }
        if (IronSourceUtils.a(context, a(str, "CappingManager.IS_PACING_ENABLED", str2), false)) {
            if (currentTimeMillis - IronSourceUtils.a(context, a(str, "CappingManager.TIME_OF_THE_PREVIOUS_SHOW", str2), 0) < ((long) (IronSourceUtils.a(context, a(str, "CappingManager.SECONDS_BETWEEN_SHOWS", str2), 0) * 1000))) {
                return ECappingStatus.CAPPED_PER_PACE;
            }
        }
        if (IronSourceUtils.a(context, a(str, "CappingManager.IS_CAPPING_ENABLED", str2), false)) {
            int a2 = IronSourceUtils.a(context, a(str, "CappingManager.MAX_NUMBER_OF_SHOWS", str2), 0);
            String a3 = a(str, "CappingManager.CURRENT_NUMBER_OF_SHOWS", str2);
            int a4 = IronSourceUtils.a(context, a3, 0);
            String a5 = a(str, "CappingManager.CAPPING_TIME_THRESHOLD", str2);
            if (currentTimeMillis >= IronSourceUtils.a(context, a5, 0)) {
                IronSourceUtils.b(context, a3, 0);
                IronSourceUtils.b(context, a5, 0);
            } else if (a4 >= a2) {
                return ECappingStatus.CAPPED_PER_COUNT;
            }
        }
        return ECappingStatus.NOT_CAPPED;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void a(android.content.Context r3, com.ironsource.mediationsdk.model.Placement r4) {
        /*
            java.lang.Class<com.ironsource.mediationsdk.utils.CappingManager> r0 = com.ironsource.mediationsdk.utils.CappingManager.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x001e
            if (r4 != 0) goto L_0x0008
            goto L_0x001e
        L_0x0008:
            com.ironsource.mediationsdk.model.PlacementAvailabilitySettings r1 = r4.a()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)
            return
        L_0x0010:
            java.lang.String r2 = "Rewarded Video"
            java.lang.String r4 = r4.c()     // Catch:{ all -> 0x001b }
            a(r3, r2, r4, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        L_0x001e:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.a(android.content.Context, com.ironsource.mediationsdk.model.Placement):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized void a(android.content.Context r3, com.ironsource.mediationsdk.model.BannerPlacement r4) {
        /*
            java.lang.Class<com.ironsource.mediationsdk.utils.CappingManager> r0 = com.ironsource.mediationsdk.utils.CappingManager.class
            monitor-enter(r0)
            if (r3 == 0) goto L_0x001e
            if (r4 != 0) goto L_0x0008
            goto L_0x001e
        L_0x0008:
            com.ironsource.mediationsdk.model.PlacementAvailabilitySettings r1 = r4.a()     // Catch:{ all -> 0x001b }
            if (r1 != 0) goto L_0x0010
            monitor-exit(r0)
            return
        L_0x0010:
            java.lang.String r2 = "Banner"
            java.lang.String r4 = r4.c()     // Catch:{ all -> 0x001b }
            a(r3, r2, r4, r1)     // Catch:{ all -> 0x001b }
            monitor-exit(r0)
            return
        L_0x001b:
            r3 = move-exception
            monitor-exit(r0)
            throw r3
        L_0x001e:
            monitor-exit(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.utils.CappingManager.a(android.content.Context, com.ironsource.mediationsdk.model.BannerPlacement):void");
    }

    public static synchronized void a(Context context, String str) {
        synchronized (CappingManager.class) {
            if (!TextUtils.isEmpty(str)) {
                a(context, "Banner", str);
            }
        }
    }

    private static String a(String str, String str2, String str3) {
        return str + "_" + str2 + "_" + str3;
    }

    private static void a(Context context, String str, String str2) {
        int i = 0;
        if (IronSourceUtils.a(context, a(str, "CappingManager.IS_PACING_ENABLED", str2), false)) {
            IronSourceUtils.b(context, a(str, "CappingManager.TIME_OF_THE_PREVIOUS_SHOW", str2), System.currentTimeMillis());
        }
        if (IronSourceUtils.a(context, a(str, "CappingManager.IS_CAPPING_ENABLED", str2), false)) {
            IronSourceUtils.a(context, a(str, "CappingManager.MAX_NUMBER_OF_SHOWS", str2), 0);
            String a2 = a(str, "CappingManager.CURRENT_NUMBER_OF_SHOWS", str2);
            int a3 = IronSourceUtils.a(context, a2, 0);
            if (a3 == 0) {
                String c = IronSourceUtils.c(context, a(str, "CappingManager.CAPPING_TYPE", str2), PlacementCappingType.PER_DAY.toString());
                PlacementCappingType placementCappingType = null;
                PlacementCappingType[] values = PlacementCappingType.values();
                int length = values.length;
                while (true) {
                    if (i >= length) {
                        break;
                    }
                    PlacementCappingType placementCappingType2 = values[i];
                    if (placementCappingType2.value.equals(c)) {
                        placementCappingType = placementCappingType2;
                        break;
                    }
                    i++;
                }
                IronSourceUtils.b(context, a(str, "CappingManager.CAPPING_TIME_THRESHOLD", str2), a(placementCappingType));
            }
            IronSourceUtils.b(context, a2, a3 + 1);
        }
    }

    private static long a(PlacementCappingType placementCappingType) {
        Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        int i = AnonymousClass1.f4729a[placementCappingType.ordinal()];
        if (i == 1) {
            instance.set(14, 0);
            instance.set(13, 0);
            instance.set(12, 0);
            instance.set(11, 0);
            instance.add(6, 1);
        } else if (i == 2) {
            instance.set(14, 0);
            instance.set(13, 0);
            instance.set(12, 0);
            instance.add(11, 1);
        }
        return instance.getTimeInMillis();
    }

    private static void a(Context context, String str, String str2, PlacementAvailabilitySettings placementAvailabilitySettings) {
        boolean e = placementAvailabilitySettings.e();
        IronSourceUtils.b(context, a(str, "CappingManager.IS_DELIVERY_ENABLED", str2), e);
        if (e) {
            boolean d = placementAvailabilitySettings.d();
            IronSourceUtils.b(context, a(str, "CappingManager.IS_CAPPING_ENABLED", str2), d);
            if (d) {
                IronSourceUtils.b(context, a(str, "CappingManager.MAX_NUMBER_OF_SHOWS", str2), placementAvailabilitySettings.b());
                IronSourceUtils.f(context, a(str, "CappingManager.CAPPING_TYPE", str2), placementAvailabilitySettings.a().toString());
            }
            boolean f = placementAvailabilitySettings.f();
            IronSourceUtils.b(context, a(str, "CappingManager.IS_PACING_ENABLED", str2), f);
            if (f) {
                IronSourceUtils.b(context, a(str, "CappingManager.SECONDS_BETWEEN_SHOWS", str2), placementAvailabilitySettings.c());
            }
        }
    }
}
