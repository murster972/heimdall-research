package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.BaseApi;
import java.util.Timer;

public abstract class AbstractSmash implements BaseApi {

    /* renamed from: a  reason: collision with root package name */
    MEDIATION_STATE f4582a = MEDIATION_STATE.NOT_INITIATED;
    AbstractAdapter b;
    ProviderSettings c;
    String d;
    String e;
    boolean f;
    String g;
    String h;
    int i = 0;
    int j = 0;
    Timer k;
    Timer l;
    int m;
    int n;
    int o;
    int p;
    IronSourceLoggerManager q = IronSourceLoggerManager.c();

    public enum MEDIATION_STATE {
        NOT_INITIATED(0),
        INIT_FAILED(1),
        INITIATED(2),
        AVAILABLE(3),
        NOT_AVAILABLE(4),
        EXHAUSTED(5),
        CAPPED_PER_SESSION(6),
        INIT_PENDING(7),
        LOAD_PENDING(8),
        CAPPED_PER_DAY(9);
        
        private int mValue;

        private MEDIATION_STATE(int i) {
            this.mValue = i;
        }

        public int a() {
            return this.mValue;
        }
    }

    AbstractSmash(ProviderSettings providerSettings) {
        this.d = providerSettings.i();
        this.e = providerSettings.g();
        this.f = providerSettings.m();
        this.c = providerSettings;
        this.g = providerSettings.l();
        this.h = providerSettings.a();
    }

    /* access modifiers changed from: package-private */
    public boolean A() {
        return !z() && !y() && !x();
    }

    /* access modifiers changed from: package-private */
    public void B() {
        this.j++;
        this.i++;
        if (y()) {
            a(MEDIATION_STATE.CAPPED_PER_SESSION);
        } else if (z()) {
            a(MEDIATION_STATE.EXHAUSTED);
        }
    }

    /* access modifiers changed from: package-private */
    public void C() {
        try {
            if (this.k != null) {
                this.k.cancel();
            }
        } catch (Exception e2) {
            a("stopInitTimer", e2.getLocalizedMessage());
        } catch (Throwable th) {
            this.k = null;
            throw th;
        }
        this.k = null;
    }

    /* access modifiers changed from: package-private */
    public void D() {
        try {
            if (this.l != null) {
                this.l.cancel();
            }
        } catch (Exception e2) {
            a("stopLoadTimer", e2.getLocalizedMessage());
        } catch (Throwable th) {
            this.l = null;
            throw th;
        }
        this.l = null;
    }

    /* access modifiers changed from: package-private */
    public void a(AbstractAdapter abstractAdapter) {
        this.b = abstractAdapter;
    }

    /* access modifiers changed from: package-private */
    public void b(String str, String str2) {
        AbstractAdapter abstractAdapter = this.b;
        if (abstractAdapter != null) {
            abstractAdapter.setPluginData(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    public abstract void l();

    public String m() {
        if (!TextUtils.isEmpty(this.h)) {
            return this.h;
        }
        return u();
    }

    /* access modifiers changed from: protected */
    public abstract String n();

    public AbstractAdapter o() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String p() {
        return this.e;
    }

    public int q() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public int r() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int s() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public MEDIATION_STATE t() {
        return this.f4582a;
    }

    public String u() {
        if (this.f) {
            return this.d;
        }
        return this.e;
    }

    public int v() {
        return this.p;
    }

    public String w() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean x() {
        return this.f4582a == MEDIATION_STATE.CAPPED_PER_DAY;
    }

    /* access modifiers changed from: package-private */
    public boolean y() {
        return this.i >= this.n;
    }

    /* access modifiers changed from: package-private */
    public boolean z() {
        return this.j >= this.m;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0048, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE r5) {
        /*
            r4 = this;
            monitor-enter(r4)
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r0 = r4.f4582a     // Catch:{ all -> 0x0049 }
            if (r0 != r5) goto L_0x0007
            monitor-exit(r4)
            return
        L_0x0007:
            r4.f4582a = r5     // Catch:{ all -> 0x0049 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r4.q     // Catch:{ all -> 0x0049 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL     // Catch:{ all -> 0x0049 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0049 }
            r2.<init>()     // Catch:{ all -> 0x0049 }
            java.lang.String r3 = "Smart Loading - "
            r2.append(r3)     // Catch:{ all -> 0x0049 }
            java.lang.String r3 = r4.p()     // Catch:{ all -> 0x0049 }
            r2.append(r3)     // Catch:{ all -> 0x0049 }
            java.lang.String r3 = " state changed to "
            r2.append(r3)     // Catch:{ all -> 0x0049 }
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x0049 }
            r2.append(r3)     // Catch:{ all -> 0x0049 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0049 }
            r3 = 0
            r0.b(r1, r2, r3)     // Catch:{ all -> 0x0049 }
            com.ironsource.mediationsdk.AbstractAdapter r0 = r4.b     // Catch:{ all -> 0x0049 }
            if (r0 == 0) goto L_0x0047
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r0 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION     // Catch:{ all -> 0x0049 }
            if (r5 == r0) goto L_0x003e
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r0 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY     // Catch:{ all -> 0x0049 }
            if (r5 != r0) goto L_0x0047
        L_0x003e:
            com.ironsource.mediationsdk.AbstractAdapter r0 = r4.b     // Catch:{ all -> 0x0049 }
            java.lang.String r1 = r4.n()     // Catch:{ all -> 0x0049 }
            r0.setMediationState(r5, r1)     // Catch:{ all -> 0x0049 }
        L_0x0047:
            monitor-exit(r4)
            return
        L_0x0049:
            r5 = move-exception
            monitor-exit(r4)
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.AbstractSmash.a(com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE):void");
    }

    public void b(Activity activity) {
        AbstractAdapter abstractAdapter = this.b;
        if (abstractAdapter != null) {
            abstractAdapter.onResume(activity);
        }
    }

    public void a(Activity activity) {
        AbstractAdapter abstractAdapter = this.b;
        if (abstractAdapter != null) {
            abstractAdapter.onPause(activity);
        }
    }

    public void a(String str) {
        if (this.b != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.q;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.b(ironSourceTag, u() + ":setMediationSegment(segment:" + str + ")", 1);
            this.b.setMediationSegment(str);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.p = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        IronSourceLoggerManager ironSourceLoggerManager = this.q;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.b(ironSourceTag, str + " exception: " + p() + " | " + str2, 3);
    }
}
