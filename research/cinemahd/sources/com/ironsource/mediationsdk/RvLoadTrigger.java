package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.utils.AuctionSettings;
import java.util.Timer;
import java.util.TimerTask;

public class RvLoadTrigger {

    /* renamed from: a  reason: collision with root package name */
    private AuctionSettings f4668a;
    /* access modifiers changed from: private */
    public RvLoadTriggerCallback b;
    private Timer c = null;

    public RvLoadTrigger(AuctionSettings auctionSettings, RvLoadTriggerCallback rvLoadTriggerCallback) {
        this.f4668a = auctionSettings;
        this.b = rvLoadTriggerCallback;
    }

    private void d() {
        Timer timer = this.c;
        if (timer != null) {
            timer.cancel();
            this.c = null;
        }
    }

    public synchronized void b() {
        if (!this.f4668a.c()) {
            d();
            this.c = new Timer();
            this.c.schedule(new TimerTask() {
                public void run() {
                    RvLoadTrigger.this.b.a();
                }
            }, this.f4668a.f());
        }
    }

    public synchronized void c() {
        d();
        this.b.a();
    }

    public synchronized void a() {
        d();
        this.c = new Timer();
        this.c.schedule(new TimerTask() {
            public void run() {
                RvLoadTrigger.this.b.a();
            }
        }, this.f4668a.b());
    }
}
