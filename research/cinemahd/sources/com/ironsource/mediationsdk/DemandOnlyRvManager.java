package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.sdk.DemandOnlyRvManagerListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import okhttp3.internal.ws.WebSocketProtocol;
import org.json.JSONObject;

class DemandOnlyRvManager implements DemandOnlyRvManagerListener {

    /* renamed from: a  reason: collision with root package name */
    private ConcurrentHashMap<String, DemandOnlyRvSmash> f4601a = new ConcurrentHashMap<>();
    private String b;

    DemandOnlyRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        this.b = str;
        for (ProviderSettings next : list) {
            if (next.i().equalsIgnoreCase("SupersonicAds") || next.i().equalsIgnoreCase("IronSource")) {
                AbstractAdapter a2 = AdapterRepository.a().a(next, next.k(), activity, true);
                if (a2 != null) {
                    this.f4601a.put(next.l(), new DemandOnlyRvSmash(activity, str, str2, next, this, rewardedVideoConfigurations.f(), a2));
                }
            } else {
                b("cannot load " + next.i());
            }
        }
    }

    public void a(Activity activity) {
        if (activity != null) {
            for (DemandOnlyRvSmash a2 : this.f4601a.values()) {
                a2.a(activity);
            }
        }
    }

    public void b(Activity activity) {
        if (activity != null) {
            for (DemandOnlyRvSmash b2 : this.f4601a.values()) {
                b2.b(activity);
            }
        }
    }

    public void c(DemandOnlyRvSmash demandOnlyRvSmash) {
        a(demandOnlyRvSmash, "onRewardedVideoAdRewarded");
        Map<String, Object> l = demandOnlyRvSmash.l();
        if (!TextUtils.isEmpty(IronSourceObject.q().e())) {
            l.put("dynamicUserId", IronSourceObject.q().e());
        }
        if (IronSourceObject.q().k() != null) {
            for (String next : IronSourceObject.q().k().keySet()) {
                l.put("custom_" + next, IronSourceObject.q().k().get(next));
            }
        }
        Placement b2 = IronSourceObject.q().d().a().e().b();
        if (b2 != null) {
            l.put("placement", b2.c());
            l.put("rewardName", b2.e());
            l.put("rewardAmount", Integer.valueOf(b2.d()));
        } else {
            IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "defaultPlacement is null", 3);
        }
        EventData eventData = new EventData(1010, new JSONObject(l));
        eventData.a("transId", IronSourceUtils.f("" + Long.toString(eventData.d()) + this.b + demandOnlyRvSmash.k()));
        RewardedVideoEventsManager.g().d(eventData);
        RVDemandOnlyListenerWrapper.a().d(demandOnlyRvSmash.n());
    }

    public void d(DemandOnlyRvSmash demandOnlyRvSmash) {
        a(demandOnlyRvSmash, "onRewardedVideoAdVisible");
        a(1206, demandOnlyRvSmash);
    }

    public void e(DemandOnlyRvSmash demandOnlyRvSmash) {
        a(demandOnlyRvSmash, "onRewardedVideoAdOpened");
        a((int) WebSocketProtocol.CLOSE_NO_STATUS_CODE, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.a().c(demandOnlyRvSmash.n());
    }

    public void a(String str) {
        try {
            if (!this.f4601a.containsKey(str)) {
                a(1500, str);
                RVDemandOnlyListenerWrapper.a().a(str, ErrorBuilder.f("Rewarded Video"));
                return;
            }
            DemandOnlyRvSmash demandOnlyRvSmash = this.f4601a.get(str);
            a(1001, demandOnlyRvSmash);
            demandOnlyRvSmash.p();
        } catch (Exception e) {
            b("loadRewardedVideo exception " + e.getMessage());
            RVDemandOnlyListenerWrapper.a().a(str, ErrorBuilder.c("loadRewardedVideo exception"));
        }
    }

    public void b(DemandOnlyRvSmash demandOnlyRvSmash) {
        a(demandOnlyRvSmash, "onRewardedVideoAdClicked");
        a(1006, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.a().a(demandOnlyRvSmash.n());
    }

    private void b(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c.b(ironSourceTag, "DemandOnlyRvManager " + str, 0);
    }

    public void a(DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        a(demandOnlyRvSmash, "onRewardedVideoLoadSuccess");
        a((int) AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE, demandOnlyRvSmash, new Object[][]{new Object[]{"duration", Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.a().e(demandOnlyRvSmash.n());
    }

    public void a(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash, long j) {
        a(demandOnlyRvSmash, "onRewardedVideoAdLoadFailed error=" + ironSourceError);
        a(1200, demandOnlyRvSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}, new Object[]{"duration", Long.valueOf(j)}});
        RVDemandOnlyListenerWrapper.a().a(demandOnlyRvSmash.n(), ironSourceError);
    }

    public void a(DemandOnlyRvSmash demandOnlyRvSmash) {
        a(demandOnlyRvSmash, "onRewardedVideoAdClosed");
        a(1203, demandOnlyRvSmash);
        RVDemandOnlyListenerWrapper.a().b(demandOnlyRvSmash.n());
    }

    public void a(IronSourceError ironSourceError, DemandOnlyRvSmash demandOnlyRvSmash) {
        a(demandOnlyRvSmash, "onRewardedVideoAdShowFailed error=" + ironSourceError);
        a(1202, demandOnlyRvSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}});
        RVDemandOnlyListenerWrapper.a().b(demandOnlyRvSmash.n(), ironSourceError);
    }

    private void a(int i, DemandOnlyRvSmash demandOnlyRvSmash) {
        a(i, demandOnlyRvSmash, (Object[][]) null);
    }

    private void a(int i, DemandOnlyRvSmash demandOnlyRvSmash, Object[][] objArr) {
        Map<String, Object> l = demandOnlyRvSmash.l();
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    l.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "RV sendProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.g().d(new EventData(i, new JSONObject(l)));
    }

    private void a(int i, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put("isDemandOnly", 1);
        if (str == null) {
            str = "";
        }
        hashMap.put("spId", str);
        RewardedVideoEventsManager.g().d(new EventData(i, new JSONObject(hashMap)));
    }

    private void a(DemandOnlyRvSmash demandOnlyRvSmash, String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "DemandOnlyRvManager " + demandOnlyRvSmash.k() + " : " + str, 0);
    }
}
