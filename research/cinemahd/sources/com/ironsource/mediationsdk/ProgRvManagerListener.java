package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.model.Placement;

public interface ProgRvManagerListener {
    void a(ProgRvSmash progRvSmash);

    void a(ProgRvSmash progRvSmash, Placement placement);

    void a(ProgRvSmash progRvSmash, String str);

    void a(IronSourceError ironSourceError, ProgRvSmash progRvSmash);

    void b(ProgRvSmash progRvSmash);

    void b(ProgRvSmash progRvSmash, Placement placement);

    void b(ProgRvSmash progRvSmash, String str);
}
