package com.ironsource.mediationsdk;

import android.content.Context;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.text.TextUtils;
import com.facebook.ads.AdError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AuctionHandler {

    /* renamed from: a  reason: collision with root package name */
    private String f4586a;
    private String b = IronSourceObject.q().l();
    private AuctionSettings c;
    private AuctionEventListener d;

    static class AuctionHttpRequestTask extends AsyncTask<Object, Void, Boolean> {

        /* renamed from: a  reason: collision with root package name */
        private WeakReference<AuctionEventListener> f4587a;
        private JSONObject b;
        private int c;
        private String d;
        private String e;
        private List<AuctionResponseItem> f;
        private long g;
        private int h;
        private String i = "other";

        AuctionHttpRequestTask(AuctionEventListener auctionEventListener) {
            this.f4587a = new WeakReference<>(auctionEventListener);
        }

        private void a(long j, long j2) {
            long time = j - (new Date().getTime() - j2);
            if (time > 0) {
                SystemClock.sleep(time);
            }
        }

        /* access modifiers changed from: protected */
        public Boolean doInBackground(Object... objArr) {
            this.g = new Date().getTime();
            try {
                URL url = new URL(objArr[0]);
                this.b = objArr[1];
                boolean booleanValue = objArr[2].booleanValue();
                int intValue = objArr[3].intValue();
                long longValue = objArr[4].longValue();
                this.h = 0;
                HttpURLConnection httpURLConnection = null;
                while (this.h < intValue) {
                    try {
                        long time = new Date().getTime();
                        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "Auction Handler: auction trial " + (this.h + 1) + " out of " + intValue + " max trials", 0);
                        httpURLConnection = a(url, longValue);
                        a(httpURLConnection, this.b);
                        int responseCode = httpURLConnection.getResponseCode();
                        if (responseCode != 200) {
                            this.c = 1001;
                            this.d = String.valueOf(responseCode);
                            httpURLConnection.disconnect();
                            if (this.h < intValue - 1) {
                                a(longValue, time);
                            }
                            this.h++;
                        } else {
                            try {
                                a(a(httpURLConnection), booleanValue);
                                httpURLConnection.disconnect();
                                return true;
                            } catch (JSONException unused) {
                                this.c = AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE;
                                this.d = "failed parsing auction response";
                                this.i = "parsing";
                                httpURLConnection.disconnect();
                                return false;
                            }
                        }
                    } catch (SocketTimeoutException unused2) {
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.c = 1006;
                        this.d = "Connection timed out";
                    } catch (Exception e2) {
                        if (httpURLConnection != null) {
                            httpURLConnection.disconnect();
                        }
                        this.c = 1000;
                        this.d = e2.getMessage();
                        this.i = "other";
                        return false;
                    }
                }
                this.h = intValue - 1;
                this.i = "trials_fail";
                return false;
            } catch (Exception e3) {
                this.c = 1007;
                this.d = e3.getMessage();
                this.h = 0;
                this.i = "other";
                return false;
            }
        }

        private void a(HttpURLConnection httpURLConnection, JSONObject jSONObject) throws IOException {
            OutputStream outputStream = httpURLConnection.getOutputStream();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
            BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);
            bufferedWriter.write(String.format("{\"request\" : \"%1$s\"}", new Object[]{IronSourceAES.b("C38FB23A402222A0C17D34A92F971D1F", jSONObject.toString())}));
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStreamWriter.close();
            outputStream.close();
        }

        private HttpURLConnection a(URL url, long j) throws IOException {
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty(TraktV2.HEADER_CONTENT_TYPE, "application/json; charset=utf-8");
            httpURLConnection.setReadTimeout((int) j);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            return httpURLConnection;
        }

        private void a(String str, boolean z) throws JSONException {
            if (!TextUtils.isEmpty(str)) {
                JSONObject jSONObject = new JSONObject(str);
                if (z) {
                    jSONObject = new JSONObject(IronSourceAES.a("C38FB23A402222A0C17D34A92F971D1F", jSONObject.getString("response")));
                }
                this.e = jSONObject.getString("auctionId");
                this.f = new ArrayList();
                JSONArray jSONArray = jSONObject.getJSONArray("waterfall");
                int i2 = 0;
                while (i2 < jSONArray.length()) {
                    AuctionResponseItem auctionResponseItem = new AuctionResponseItem(jSONArray.getJSONObject(i2));
                    if (auctionResponseItem.g()) {
                        this.f.add(auctionResponseItem);
                        i2++;
                    } else {
                        this.c = AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE;
                        this.d = "waterfall " + i2;
                        throw new JSONException("invalid response");
                    }
                }
                return;
            }
            throw new JSONException("empty response");
        }

        private String a(HttpURLConnection httpURLConnection) throws IOException {
            InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuilder sb = new StringBuilder();
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine != null) {
                    sb.append(readLine);
                } else {
                    bufferedReader.close();
                    inputStreamReader.close();
                    return sb.toString();
                }
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            AuctionEventListener auctionEventListener = (AuctionEventListener) this.f4587a.get();
            if (auctionEventListener != null) {
                long time = new Date().getTime() - this.g;
                if (bool.booleanValue()) {
                    auctionEventListener.a(this.f, this.e, this.h + 1, time);
                } else {
                    auctionEventListener.a(this.c, this.d, this.h + 1, this.i, time);
                }
            }
        }
    }

    public AuctionHandler(String str, AuctionSettings auctionSettings, AuctionEventListener auctionEventListener) {
        this.f4586a = str;
        this.c = auctionSettings;
        this.d = auctionEventListener;
    }

    public void a(Context context, Map<String, Object> map, List<String> list, int i) {
        try {
            boolean z = IronSourceUtils.c() == 1;
            new AuctionHttpRequestTask(this.d).execute(new Object[]{this.c.i(), a(context, map, list, i, z), Boolean.valueOf(z), Integer.valueOf(this.c.e()), Long.valueOf(this.c.h())});
        } catch (Exception e) {
            this.d.a(1000, e.getMessage(), 0, "other", 0);
        }
    }

    public void b(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.d()) {
            new ImpressionHttpTask().execute(new String[]{str});
        }
    }

    public void a(AuctionResponseItem auctionResponseItem) {
        for (String str : auctionResponseItem.a()) {
            new ImpressionHttpTask().execute(new String[]{str});
        }
    }

    public void a(CopyOnWriteArrayList<ProgSmash> copyOnWriteArrayList, ConcurrentHashMap<String, AuctionResponseItem> concurrentHashMap, AuctionResponseItem auctionResponseItem) {
        String e = auctionResponseItem.e();
        Iterator<ProgSmash> it2 = copyOnWriteArrayList.iterator();
        boolean z = false;
        boolean z2 = false;
        while (it2.hasNext()) {
            ProgSmash next = it2.next();
            String k = next.k();
            if (k.equals(auctionResponseItem.b())) {
                z2 = next.o();
                z = true;
            } else {
                AuctionResponseItem auctionResponseItem2 = concurrentHashMap.get(k);
                String str = z ? z2 ? "102" : "103" : DiskLruCache.VERSION_1;
                for (String replace : auctionResponseItem2.c()) {
                    new ImpressionHttpTask().execute(new String[]{replace.replace("${AUCTION_PRICE}", e).replace("${AUCTION_LOSS}", str)});
                }
            }
        }
    }

    static class ImpressionHttpTask extends AsyncTask<String, Void, Boolean> {
        ImpressionHttpTask() {
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Boolean doInBackground(String... strArr) {
            try {
                HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(strArr[0]).openConnection();
                httpURLConnection.setRequestMethod("GET");
                httpURLConnection.setReadTimeout(15000);
                httpURLConnection.setConnectTimeout(15000);
                httpURLConnection.connect();
                int responseCode = httpURLConnection.getResponseCode();
                httpURLConnection.disconnect();
                return Boolean.valueOf(responseCode == 200);
            } catch (Exception unused) {
                return false;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Boolean bool) {
            super.onPostExecute(bool);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x015a  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x015d  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0173  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01c8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private org.json.JSONObject a(android.content.Context r8, java.util.Map<java.lang.String, java.lang.Object> r9, java.util.List<java.lang.String> r10, int r11, boolean r12) throws org.json.JSONException {
        /*
            r7 = this;
            java.lang.String r0 = ""
            org.json.JSONObject r1 = new org.json.JSONObject
            r1.<init>()
            java.util.Set r2 = r9.keySet()
            java.util.Iterator r2 = r2.iterator()
        L_0x000f:
            boolean r3 = r2.hasNext()
            r4 = 2
            java.lang.String r5 = "instanceType"
            if (r3 == 0) goto L_0x003a
            java.lang.Object r3 = r2.next()
            java.lang.String r3 = (java.lang.String) r3
            org.json.JSONObject r6 = new org.json.JSONObject
            r6.<init>()
            r6.put(r5, r4)
            org.json.JSONObject r4 = new org.json.JSONObject
            java.lang.Object r5 = r9.get(r3)
            java.util.Map r5 = (java.util.Map) r5
            r4.<init>(r5)
            java.lang.String r5 = "biddingAdditionalData"
            r6.put(r5, r4)
            r1.put(r3, r6)
            goto L_0x000f
        L_0x003a:
            java.util.Iterator r9 = r10.iterator()
        L_0x003e:
            boolean r10 = r9.hasNext()
            r2 = 1
            if (r10 == 0) goto L_0x0057
            java.lang.Object r10 = r9.next()
            java.lang.String r10 = (java.lang.String) r10
            org.json.JSONObject r3 = new org.json.JSONObject
            r3.<init>()
            r3.put(r5, r2)
            r1.put(r10, r3)
            goto L_0x003e
        L_0x0057:
            org.json.JSONObject r9 = new org.json.JSONObject
            r9.<init>()
            com.ironsource.mediationsdk.IronSourceObject r10 = com.ironsource.mediationsdk.IronSourceObject.q()
            java.lang.String r10 = r10.h()
            java.lang.String r3 = "applicationUserId"
            r9.put(r3, r10)
            com.ironsource.mediationsdk.IronSourceObject r10 = com.ironsource.mediationsdk.IronSourceObject.q()
            java.lang.String r10 = r10.f()
            boolean r3 = android.text.TextUtils.isEmpty(r10)
            if (r3 == 0) goto L_0x0079
            java.lang.String r10 = "unknown"
        L_0x0079:
            java.lang.String r3 = "applicationUserGender"
            r9.put(r3, r10)
            com.ironsource.mediationsdk.IronSourceObject r10 = com.ironsource.mediationsdk.IronSourceObject.q()
            java.lang.Integer r10 = r10.b()
            if (r10 != 0) goto L_0x008d
            r10 = -1
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
        L_0x008d:
            java.lang.String r3 = "applicationUserAge"
            r9.put(r3, r10)
            com.ironsource.mediationsdk.IronSourceObject r10 = com.ironsource.mediationsdk.IronSourceObject.q()
            java.lang.Boolean r10 = r10.c()
            if (r10 == 0) goto L_0x00a5
            boolean r10 = r10.booleanValue()
            java.lang.String r3 = "consent"
            r9.put(r3, r10)
        L_0x00a5:
            java.lang.String r10 = com.ironsource.environment.DeviceStatus.i(r8)
            java.lang.String r3 = "mobileCarrier"
            r9.put(r3, r10)
            java.lang.String r10 = com.ironsource.mediationsdk.utils.IronSourceUtils.a((android.content.Context) r8)
            java.lang.String r3 = "connectionType"
            r9.put(r3, r10)
            java.lang.String r10 = "deviceOS"
            java.lang.String r3 = "android"
            r9.put(r10, r3)
            android.content.res.Resources r10 = r8.getResources()
            android.content.res.Configuration r10 = r10.getConfiguration()
            int r10 = r10.screenWidthDp
            java.lang.String r3 = "deviceWidth"
            r9.put(r3, r10)
            android.content.res.Resources r10 = r8.getResources()
            android.content.res.Configuration r10 = r10.getConfiguration()
            int r10 = r10.screenHeightDp
            java.lang.String r3 = "deviceHeight"
            r9.put(r3, r10)
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            int r3 = android.os.Build.VERSION.SDK_INT
            r10.append(r3)
            java.lang.String r3 = "("
            r10.append(r3)
            java.lang.String r3 = android.os.Build.VERSION.RELEASE
            r10.append(r3)
            java.lang.String r3 = ")"
            r10.append(r3)
            java.lang.String r10 = r10.toString()
            java.lang.String r3 = "deviceOSVersion"
            r9.put(r3, r10)
            java.lang.String r10 = android.os.Build.MODEL
            java.lang.String r3 = "deviceModel"
            r9.put(r3, r10)
            java.lang.String r10 = android.os.Build.MANUFACTURER
            java.lang.String r3 = "deviceMake"
            r9.put(r3, r10)
            java.lang.String r10 = r8.getPackageName()
            java.lang.String r3 = "bundleId"
            r9.put(r3, r10)
            java.lang.String r10 = r8.getPackageName()
            java.lang.String r10 = com.ironsource.environment.ApplicationContext.a((android.content.Context) r8, (java.lang.String) r10)
            java.lang.String r3 = "appVersion"
            r9.put(r3, r10)
            java.util.Date r10 = new java.util.Date
            r10.<init>()
            long r5 = r10.getTime()
            java.lang.String r10 = "clientTimestamp"
            r9.put(r10, r5)
            r10 = 0
            java.lang.String[] r3 = com.ironsource.environment.DeviceStatus.b((android.content.Context) r8)     // Catch:{ Exception -> 0x0153 }
            if (r3 == 0) goto L_0x0153
            int r5 = r3.length     // Catch:{ Exception -> 0x0153 }
            if (r5 != r4) goto L_0x0153
            r4 = r3[r10]     // Catch:{ Exception -> 0x0153 }
            boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ Exception -> 0x0153 }
            if (r4 != 0) goto L_0x0145
            r4 = r3[r10]     // Catch:{ Exception -> 0x0153 }
            goto L_0x0146
        L_0x0145:
            r4 = r0
        L_0x0146:
            r2 = r3[r2]     // Catch:{ Exception -> 0x0151 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0151 }
            boolean r10 = r2.booleanValue()     // Catch:{ Exception -> 0x0151 }
            goto L_0x0154
        L_0x0151:
            goto L_0x0154
        L_0x0153:
            r4 = r0
        L_0x0154:
            boolean r2 = android.text.TextUtils.isEmpty(r4)
            if (r2 != 0) goto L_0x015d
            java.lang.String r0 = "GAID"
            goto L_0x0169
        L_0x015d:
            java.lang.String r4 = com.ironsource.environment.DeviceStatus.j(r8)
            boolean r8 = android.text.TextUtils.isEmpty(r4)
            if (r8 != 0) goto L_0x0169
            java.lang.String r0 = "UUID"
        L_0x0169:
            boolean r8 = android.text.TextUtils.isEmpty(r4)
            java.lang.String r2 = "false"
            java.lang.String r3 = "true"
            if (r8 != 0) goto L_0x0187
            java.lang.String r8 = "advId"
            r9.put(r8, r4)
            java.lang.String r8 = "advIdType"
            r9.put(r8, r0)
            if (r10 == 0) goto L_0x0181
            r8 = r3
            goto L_0x0182
        L_0x0181:
            r8 = r2
        L_0x0182:
            java.lang.String r10 = "isLimitAdTrackingEnabled"
            r9.put(r10, r8)
        L_0x0187:
            org.json.JSONObject r8 = new org.json.JSONObject
            r8.<init>()
            java.lang.String r10 = r7.f4586a
            java.lang.String r0 = "adUnit"
            r8.put(r0, r10)
            com.ironsource.mediationsdk.utils.AuctionSettings r10 = r7.c
            java.lang.String r10 = r10.a()
            java.lang.String r0 = "auctionData"
            r8.put(r0, r10)
            com.ironsource.mediationsdk.IronSourceObject r10 = com.ironsource.mediationsdk.IronSourceObject.q()
            java.lang.String r10 = r10.g()
            java.lang.String r0 = "applicationKey"
            r8.put(r0, r10)
            java.lang.String r10 = com.ironsource.mediationsdk.utils.IronSourceUtils.b()
            java.lang.String r0 = "SDKVersion"
            r8.put(r0, r10)
            java.lang.String r10 = "clientParams"
            r8.put(r10, r9)
            java.lang.String r9 = "sessionDepth"
            r8.put(r9, r11)
            java.lang.String r9 = r7.b
            java.lang.String r10 = "sessionId"
            r8.put(r10, r9)
            if (r12 == 0) goto L_0x01c8
            goto L_0x01c9
        L_0x01c8:
            r2 = r3
        L_0x01c9:
            java.lang.String r9 = "doNotEncryptResponse"
            r8.put(r9, r2)
            java.lang.String r9 = "instances"
            r8.put(r9, r1)
            return r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.AuctionHandler.a(android.content.Context, java.util.Map, java.util.List, int, boolean):org.json.JSONObject");
    }
}
