package com.ironsource.mediationsdk.integration;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import com.google.ar.core.ImageMetadata;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.ArrayList;
import java.util.Iterator;

public class IntegrationHelper {
    private static boolean a(ArrayList<Pair<String, String>> arrayList) {
        boolean z = true;
        if (arrayList == null) {
            return true;
        }
        Log.i("IntegrationHelper", "*** External Libraries ***");
        Iterator<Pair<String, String>> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            Pair next = it2.next();
            try {
                Class.forName((String) next.first);
                Log.i("IntegrationHelper", ((String) next.second) + " - VERIFIED");
            } catch (ClassNotFoundException unused) {
                z = false;
                Log.e("IntegrationHelper", ((String) next.second) + " - MISSING");
            }
        }
        return z;
    }

    public static void b(Activity activity) {
        Log.i("IntegrationHelper", "Verifying Integration:");
        c(activity);
        for (String str : new String[]{"AdColony", "AdMob", "Amazon", "AppLovin", "Chartboost", "Facebook", "Fyber", "HyprMX", "InMobi", "SupersonicAds", "Maio", "Mintegral", "MyTarget", "Tapjoy", "UnityAds", "Vungle"}) {
            if (b(activity, str)) {
                if (str.equalsIgnoreCase("SupersonicAds")) {
                    Log.i("IntegrationHelper", ">>>> IronSource - VERIFIED");
                } else {
                    Log.i("IntegrationHelper", ">>>> " + str + " - VERIFIED");
                }
            } else if (str.equalsIgnoreCase("SupersonicAds")) {
                Log.e("IntegrationHelper", ">>>> IronSource - NOT VERIFIED");
            } else {
                Log.e("IntegrationHelper", ">>>> " + str + " - NOT VERIFIED");
            }
        }
        Activity activity2 = activity;
        a(activity);
    }

    private static void c(Activity activity) {
        Log.i("IntegrationHelper", "*** Permissions ***");
        PackageManager packageManager = activity.getPackageManager();
        if (packageManager.checkPermission("android.permission.INTERNET", activity.getPackageName()) == 0) {
            Log.i("IntegrationHelper", "android.permission.INTERNET - VERIFIED");
        } else {
            Log.e("IntegrationHelper", "android.permission.INTERNET - MISSING");
        }
        if (packageManager.checkPermission("android.permission.ACCESS_NETWORK_STATE", activity.getPackageName()) == 0) {
            Log.i("IntegrationHelper", "android.permission.ACCESS_NETWORK_STATE - VERIFIED");
        } else {
            Log.e("IntegrationHelper", "android.permission.ACCESS_NETWORK_STATE - MISSING");
        }
    }

    private static boolean a(Activity activity, String[] strArr) {
        if (strArr == null) {
            return true;
        }
        Log.i("IntegrationHelper", "*** Activities ***");
        int length = strArr.length;
        int i = 0;
        boolean z = true;
        while (i < length) {
            String str = strArr[i];
            try {
                if (activity.getPackageManager().queryIntentActivities(new Intent(activity, Class.forName(str)), ImageMetadata.CONTROL_AE_ANTIBANDING_MODE).size() > 0) {
                    Log.i("IntegrationHelper", str + " - VERIFIED");
                    i++;
                } else {
                    Log.e("IntegrationHelper", str + " - MISSING");
                    z = false;
                    i++;
                }
            } catch (ClassNotFoundException unused) {
                Log.e("IntegrationHelper", str + " - MISSING");
            }
        }
        return z;
    }

    private static boolean b(Activity activity, String str) {
        try {
            if (str.equalsIgnoreCase("SupersonicAds")) {
                Log.i("IntegrationHelper", "--------------- IronSource  --------------");
            } else {
                Log.i("IntegrationHelper", "--------------- " + str + " --------------");
            }
            String str2 = "com.ironsource.adapters." + str.toLowerCase() + "." + str + "Adapter";
            IntegrationData a2 = a(activity, str2);
            if (a2 == null || !b(a2)) {
                return false;
            }
            if (!str.equalsIgnoreCase("SupersonicAds") && !a(a2)) {
                return false;
            }
            a(str2);
            boolean a3 = a(activity, a2.c);
            if (!a(a2.d)) {
                a3 = false;
            }
            if (!b(activity, a2.e)) {
                a3 = false;
            }
            if (!a2.f || Build.VERSION.SDK_INT > 18) {
                return a3;
            }
            if (activity.getPackageManager().checkPermission("android.permission.WRITE_EXTERNAL_STORAGE", activity.getPackageName()) == 0) {
                Log.i("IntegrationHelper", "android.permission.WRITE_EXTERNAL_STORAGE - VERIFIED");
                return a3;
            }
            Log.e("IntegrationHelper", "android.permission.WRITE_EXTERNAL_STORAGE - MISSING");
            return false;
        } catch (Exception e) {
            Log.e("IntegrationHelper", "isAdapterValid " + str, e);
            return false;
        }
    }

    private static boolean a(IntegrationData integrationData) {
        if (integrationData.b.startsWith("4.1") || integrationData.b.startsWith("4.3")) {
            Log.i("IntegrationHelper", "Adapter - VERIFIED");
            return true;
        }
        Log.e("IntegrationHelper", integrationData.f4621a + " adapter " + integrationData.b + " is incompatible with SDK version " + IronSourceUtils.b() + ", please update your adapter to version " + "4.1" + ".*");
        return false;
    }

    private static IntegrationData a(Activity activity, String str) {
        try {
            IntegrationData integrationData = (IntegrationData) Class.forName(str).getMethod("getIntegrationData", new Class[]{Activity.class}).invoke((Object) null, new Object[]{activity});
            Log.i("IntegrationHelper", "Adapter " + integrationData.b + " - VERIFIED");
            return integrationData;
        } catch (ClassNotFoundException unused) {
            Log.e("IntegrationHelper", "Adapter - MISSING");
            return null;
        } catch (Exception unused2) {
            Log.e("IntegrationHelper", "Adapter version - NOT VERIFIED");
            return null;
        }
    }

    private static void a(final Activity activity) {
        new Thread() {
            public void run() {
                try {
                    Log.w("IntegrationHelper", "--------------- Google Play Services --------------");
                    if (activity.getPackageManager().getApplicationInfo(activity.getPackageName(), 128).metaData.containsKey("com.google.android.gms.version")) {
                        Log.i("IntegrationHelper", "Google Play Services - VERIFIED");
                        String a2 = IronSourceObject.q().a((Context) activity);
                        if (!TextUtils.isEmpty(a2)) {
                            Log.i("IntegrationHelper", "GAID is: " + a2 + " (use this for test devices)");
                            return;
                        }
                        return;
                    }
                    Log.e("IntegrationHelper", "Google Play Services - MISSING");
                } catch (Exception unused) {
                    Log.e("IntegrationHelper", "Google Play Services - MISSING");
                }
            }
        }.start();
    }

    private static void a(String str) {
        try {
            Log.i("IntegrationHelper", "SDK Version - " + ((String) Class.forName(str).getMethod("getAdapterSDKVersion", new Class[0]).invoke((Object) null, new Object[0])));
        } catch (Exception unused) {
            Log.w("validateSDKVersion", "Unable to get SDK version");
        }
    }

    private static boolean b(Activity activity, String[] strArr) {
        if (strArr == null) {
            return true;
        }
        PackageManager packageManager = activity.getPackageManager();
        Log.i("IntegrationHelper", "*** Services ***");
        int length = strArr.length;
        int i = 0;
        boolean z = true;
        while (i < length) {
            String str = strArr[i];
            try {
                if (packageManager.queryIntentServices(new Intent(activity, Class.forName(str)), ImageMetadata.CONTROL_AE_ANTIBANDING_MODE).size() > 0) {
                    Log.i("IntegrationHelper", str + " - VERIFIED");
                    i++;
                } else {
                    Log.e("IntegrationHelper", str + " - MISSING");
                    z = false;
                    i++;
                }
            } catch (ClassNotFoundException unused) {
                Log.e("IntegrationHelper", str + " - MISSING");
            }
        }
        return z;
    }

    private static boolean b(IntegrationData integrationData) {
        if ((!integrationData.f4621a.equalsIgnoreCase("AppLovin") && !integrationData.f4621a.equalsIgnoreCase("AdMob") && !integrationData.f4621a.equalsIgnoreCase("Facebook") && !integrationData.f4621a.equalsIgnoreCase("Amazon") && !integrationData.f4621a.equalsIgnoreCase("InMobi") && !integrationData.f4621a.equalsIgnoreCase("Amazon") && !integrationData.f4621a.equalsIgnoreCase("Fyber")) || integrationData.b.startsWith("4.3")) {
            return true;
        }
        Log.e("IntegrationHelper", integrationData.f4621a + " adapter " + integrationData.b + " is incompatible for showing banners with SDK version " + IronSourceUtils.b() + ", please update your adapter to version " + "4.3" + ".*");
        return false;
    }
}
