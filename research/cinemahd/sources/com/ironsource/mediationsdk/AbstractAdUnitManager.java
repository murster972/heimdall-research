package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.BaseApi;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

abstract class AbstractAdUnitManager implements BaseApi {

    /* renamed from: a  reason: collision with root package name */
    DailyCappingManager f4581a = null;
    int b;
    final CopyOnWriteArrayList<AbstractSmash> c = new CopyOnWriteArrayList<>();
    private AbstractSmash d;
    private AbstractSmash e;
    Activity f;
    String g;
    String h;
    IronSourceLoggerManager i = IronSourceLoggerManager.c();
    boolean j = false;
    Boolean k;
    boolean l;
    boolean m = true;
    AtomicBoolean n = new AtomicBoolean();
    AtomicBoolean o = new AtomicBoolean();

    AbstractAdUnitManager() {
    }

    public void a(Activity activity) {
        this.o.set(true);
        synchronized (this.c) {
            if (this.c != null) {
                Iterator<AbstractSmash> it2 = this.c.iterator();
                while (it2.hasNext()) {
                    it2.next().a(activity);
                }
            }
        }
    }

    public void b(Activity activity) {
        this.n.set(true);
        if (activity != null) {
            this.f = activity;
        }
        synchronized (this.c) {
            if (this.c != null) {
                Iterator<AbstractSmash> it2 = this.c.iterator();
                while (it2.hasNext()) {
                    it2.next().b(activity);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void c(AbstractSmash abstractSmash) {
        try {
            String i2 = IronSourceObject.q().i();
            if (!TextUtils.isEmpty(i2)) {
                abstractSmash.a(i2);
            }
            String b2 = ConfigFile.d().b();
            if (!TextUtils.isEmpty(b2)) {
                abstractSmash.b(b2, ConfigFile.d().a());
            }
        } catch (Exception e2) {
            IronSourceLoggerManager ironSourceLoggerManager = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager.b(ironSourceTag, ":setCustomParams():" + e2.toString(), 3);
        }
    }

    /* access modifiers changed from: package-private */
    public void d(AbstractSmash abstractSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.b(ironSourceTag, abstractSmash.p() + " is set as premium", 0);
        this.e = abstractSmash;
    }

    /* access modifiers changed from: package-private */
    public AbstractSmash e() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (!this.o.get()) {
            this.i.b(IronSourceLogger.IronSourceTag.NATIVE, "IronSource.onPause() wasn't overridden in your activity lifecycle!", 3);
        }
        if (!this.n.get()) {
            this.i.b(IronSourceLogger.IronSourceTag.NATIVE, "IronSource.onResume() wasn't overridden in your activity lifecycle!", 3);
        }
    }

    /* access modifiers changed from: package-private */
    public AbstractSmash d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.b = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(AbstractSmash abstractSmash) {
        this.c.add(abstractSmash);
        DailyCappingManager dailyCappingManager = this.f4581a;
        if (dailyCappingManager != null) {
            dailyCappingManager.a(abstractSmash);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(AbstractSmash abstractSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.b(ironSourceTag, abstractSmash.p() + " is set as backfill", 0);
        this.d = abstractSmash;
    }

    /* access modifiers changed from: package-private */
    public synchronized boolean c() {
        return this.m;
    }
}
