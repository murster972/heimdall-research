package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import okhttp3.internal.ws.WebSocketProtocol;
import org.json.JSONObject;

public class ProgRvSmash extends ProgSmash implements RewardedVideoSmashListener {
    /* access modifiers changed from: private */
    public final Object A = new Object();
    /* access modifiers changed from: private */
    public SMASH_STATE e = SMASH_STATE.NO_INIT;
    /* access modifiers changed from: private */
    public ProgRvManagerListener f;
    private Timer g;
    private int h;
    private Activity i;
    private String j;
    private String k;
    private String l;
    private boolean m;
    private boolean n;
    private boolean o;
    private Placement p;
    private int q;
    private long r;
    /* access modifiers changed from: private */
    public String s;
    private String t;
    private int u;
    private String v;
    private int w;
    private int x;
    private String y;
    private final Object z = new Object();

    protected enum SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        NOT_LOADED,
        LOAD_IN_PROGRESS,
        LOADED,
        SHOW_IN_PROGRESS
    }

    public ProgRvSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, ProgRvManagerListener progRvManagerListener, int i2, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.k()), abstractAdapter);
        this.i = activity;
        this.j = str;
        this.k = str2;
        this.f = progRvManagerListener;
        this.g = null;
        this.h = i2;
        this.f4650a.addRewardedVideoListener(this);
        this.m = false;
        this.n = false;
        this.o = false;
        this.p = null;
        this.s = "";
        this.q = 1;
        x();
    }

    private void A() {
        synchronized (this.z) {
            if (this.g != null) {
                this.g.cancel();
                this.g = null;
            }
        }
    }

    private boolean c(int i2) {
        return i2 == 1001 || i2 == 1002 || i2 == 1200 || i2 == 1005 || i2 == 1203 || i2 == 1201 || i2 == 1202 || i2 == 1006 || i2 == 1010;
    }

    /* access modifiers changed from: private */
    public long w() {
        return new Date().getTime() - this.r;
    }

    private void x() {
        this.t = "";
        this.w = -1;
        this.y = "";
        this.l = "";
        this.x = this.q;
    }

    private void y() {
        try {
            String i2 = IronSourceObject.q().i();
            if (!TextUtils.isEmpty(i2)) {
                this.f4650a.setMediationSegment(i2);
            }
            String b = ConfigFile.d().b();
            if (!TextUtils.isEmpty(b)) {
                this.f4650a.setPluginData(b, ConfigFile.d().a());
            }
        } catch (Exception e2) {
            b("setCustomParams() " + e2.getMessage());
        }
    }

    private void z() {
        synchronized (this.z) {
            A();
            this.g = new Timer();
            this.g.schedule(new TimerTask() {
                public void run() {
                    int i;
                    boolean z;
                    ProgRvSmash.this.b("Rewarded Video - load instance time out");
                    synchronized (ProgRvSmash.this.A) {
                        if (ProgRvSmash.this.e != SMASH_STATE.LOAD_IN_PROGRESS) {
                            if (ProgRvSmash.this.e != SMASH_STATE.INIT_IN_PROGRESS) {
                                z = false;
                                i = 510;
                            }
                        }
                        int i2 = ProgRvSmash.this.e == SMASH_STATE.LOAD_IN_PROGRESS ? 1025 : 1032;
                        ProgRvSmash.this.a(SMASH_STATE.NOT_LOADED);
                        i = i2;
                        z = true;
                    }
                    if (z) {
                        ProgRvSmash.this.a(1200, new Object[][]{new Object[]{"errorCode", Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(ProgRvSmash.this.w())}});
                        ProgRvSmash.this.a(1212, new Object[][]{new Object[]{"errorCode", Integer.valueOf(i)}, new Object[]{"duration", Long.valueOf(ProgRvSmash.this.w())}});
                        ProgRvManagerListener e = ProgRvSmash.this.f;
                        ProgRvSmash progRvSmash = ProgRvSmash.this;
                        e.b(progRvSmash, progRvSmash.s);
                        return;
                    }
                    ProgRvSmash.this.a(1208, new Object[][]{new Object[]{"errorCode", 1025}, new Object[]{"duration", Long.valueOf(ProgRvSmash.this.w())}, new Object[]{"ext1", ProgRvSmash.this.e.name()}});
                }
            }, (long) (this.h * 1000));
        }
    }

    public void f(IronSourceError ironSourceError) {
        a("onRewardedVideoInitFailed error=" + ironSourceError.b());
        A();
        a(1200, new Object[][]{new Object[]{"errorCode", 1033}, new Object[]{"duration", Long.valueOf(w())}});
        a(1212, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}, new Object[]{"duration", Long.valueOf(w())}});
        synchronized (this.A) {
            if (this.e != SMASH_STATE.INIT_IN_PROGRESS) {
                a(81316, new Object[][]{new Object[]{"errorCode", 5008}, new Object[]{"reason", "initFailed: " + this.e}});
                return;
            }
            a(SMASH_STATE.NO_INIT);
            this.f.b(this, this.s);
        }
    }

    public void g() {
        a("onRewardedVideoAdClicked");
        this.f.b(this, this.p);
        b(1006);
    }

    public void h() {
        a("onRewardedVideoAdRewarded");
        this.f.a(this, this.p);
        Map<String, Object> n2 = n();
        Placement placement = this.p;
        if (placement != null) {
            n2.put("placement", placement.c());
            n2.put("rewardName", this.p.e());
            n2.put("rewardAmount", Integer.valueOf(this.p.d()));
        }
        if (!TextUtils.isEmpty(IronSourceObject.q().e())) {
            n2.put("dynamicUserId", IronSourceObject.q().e());
        }
        if (IronSourceObject.q().k() != null) {
            for (String next : IronSourceObject.q().k().keySet()) {
                n2.put("custom_" + next, IronSourceObject.q().k().get(next));
            }
        }
        if (!TextUtils.isEmpty(this.s)) {
            n2.put("auctionId", this.s);
        }
        if (c(1010)) {
            RewardedVideoEventsManager.g().a(n2, this.u, this.v);
        }
        n2.put("sessionDepth", Integer.valueOf(this.q));
        EventData eventData = new EventData(1010, new JSONObject(n2));
        eventData.a("transId", IronSourceUtils.f("" + Long.toString(eventData.d()) + this.j + k()));
        RewardedVideoEventsManager.g().d(eventData);
    }

    public void i() {
        a("onRewardedVideoInitSuccess");
        synchronized (this.A) {
            if (this.e != SMASH_STATE.INIT_IN_PROGRESS) {
                a(81316, new Object[][]{new Object[]{"errorCode", 5007}, new Object[]{"reason", "initSuccess: " + this.e}});
                return;
            }
            a(SMASH_STATE.NOT_LOADED);
        }
    }

    public void j() {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0053, code lost:
        r10.f.b(r10);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x005a, code lost:
        if (r10.m == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x005c, code lost:
        b("onRewardedVideoAdClosed and mShouldLoadAfterClose is true - calling loadVideo");
        r10.m = false;
        a(r10.l, r10.t, r10.w, r10.y, r10.x);
        x();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onRewardedVideoAdClosed() {
        /*
            r10 = this;
            java.lang.String r0 = "onRewardedVideoAdClosed"
            r10.a((java.lang.String) r0)
            java.lang.Object r0 = r10.A
            monitor-enter(r0)
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r1 = r10.e     // Catch:{ all -> 0x0075 }
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r2 = com.ironsource.mediationsdk.ProgRvSmash.SMASH_STATE.SHOW_IN_PROGRESS     // Catch:{ all -> 0x0075 }
            r3 = 0
            if (r1 == r2) goto L_0x004d
            r1 = 1203(0x4b3, float:1.686E-42)
            r10.b((int) r1)     // Catch:{ all -> 0x0075 }
            r1 = 81316(0x13da4, float:1.13948E-40)
            r2 = 2
            java.lang.Object[][] r4 = new java.lang.Object[r2][]     // Catch:{ all -> 0x0075 }
            java.lang.Object[] r5 = new java.lang.Object[r2]     // Catch:{ all -> 0x0075 }
            java.lang.String r6 = "errorCode"
            r5[r3] = r6     // Catch:{ all -> 0x0075 }
            r6 = 5009(0x1391, float:7.019E-42)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0075 }
            r7 = 1
            r5[r7] = r6     // Catch:{ all -> 0x0075 }
            r4[r3] = r5     // Catch:{ all -> 0x0075 }
            java.lang.Object[] r2 = new java.lang.Object[r2]     // Catch:{ all -> 0x0075 }
            java.lang.String r5 = "reason"
            r2[r3] = r5     // Catch:{ all -> 0x0075 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0075 }
            r3.<init>()     // Catch:{ all -> 0x0075 }
            java.lang.String r5 = "adClosed: "
            r3.append(r5)     // Catch:{ all -> 0x0075 }
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r5 = r10.e     // Catch:{ all -> 0x0075 }
            r3.append(r5)     // Catch:{ all -> 0x0075 }
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0075 }
            r2[r7] = r3     // Catch:{ all -> 0x0075 }
            r4[r7] = r2     // Catch:{ all -> 0x0075 }
            r10.a((int) r1, (java.lang.Object[][]) r4)     // Catch:{ all -> 0x0075 }
            monitor-exit(r0)     // Catch:{ all -> 0x0075 }
            return
        L_0x004d:
            com.ironsource.mediationsdk.ProgRvSmash$SMASH_STATE r1 = com.ironsource.mediationsdk.ProgRvSmash.SMASH_STATE.NOT_LOADED     // Catch:{ all -> 0x0075 }
            r10.a((com.ironsource.mediationsdk.ProgRvSmash.SMASH_STATE) r1)     // Catch:{ all -> 0x0075 }
            monitor-exit(r0)     // Catch:{ all -> 0x0075 }
            com.ironsource.mediationsdk.ProgRvManagerListener r0 = r10.f
            r0.b(r10)
            boolean r0 = r10.m
            if (r0 == 0) goto L_0x0074
            java.lang.String r0 = "onRewardedVideoAdClosed and mShouldLoadAfterClose is true - calling loadVideo"
            r10.b((java.lang.String) r0)
            r10.m = r3
            java.lang.String r5 = r10.l
            java.lang.String r6 = r10.t
            int r7 = r10.w
            java.lang.String r8 = r10.y
            int r9 = r10.x
            r4 = r10
            r4.a(r5, r6, r7, r8, r9)
            r10.x()
        L_0x0074:
            return
        L_0x0075:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0075 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvSmash.onRewardedVideoAdClosed():void");
    }

    public void onRewardedVideoAdOpened() {
        a("onRewardedVideoAdOpened");
        this.f.a(this);
        b((int) WebSocketProtocol.CLOSE_NO_STATUS_CODE);
    }

    public Map<String, Object> p() {
        try {
            if (o()) {
                return this.f4650a.getRvBiddingData(this.d);
            }
            return null;
        } catch (Throwable th) {
            c("getBiddingData exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            a(81316, new Object[][]{new Object[]{"errorCode", 5001}, new Object[]{"reason", th.getLocalizedMessage()}});
            return null;
        }
    }

    public void q() {
        b("initForBidding()");
        a(SMASH_STATE.INIT_IN_PROGRESS);
        y();
        try {
            this.f4650a.initRvForBidding(this.i, this.j, this.k, this.d, this);
        } catch (Throwable th) {
            c("initForBidding exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            f(new IronSourceError(1040, th.getLocalizedMessage()));
        }
    }

    public boolean r() {
        SMASH_STATE smash_state = this.e;
        return smash_state == SMASH_STATE.INIT_IN_PROGRESS || smash_state == SMASH_STATE.LOAD_IN_PROGRESS;
    }

    public boolean s() {
        SMASH_STATE smash_state = this.e;
        return (smash_state == SMASH_STATE.NO_INIT || smash_state == SMASH_STATE.INIT_IN_PROGRESS) ? false : true;
    }

    public boolean t() {
        try {
            if (!o()) {
                return u();
            }
            if (!this.o || this.e != SMASH_STATE.LOADED || !u()) {
                return false;
            }
            return true;
        } catch (Throwable th) {
            c("isReadyToShow exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            a(81316, new Object[][]{new Object[]{"errorCode", 5002}, new Object[]{"reason", th.getLocalizedMessage()}});
            return false;
        }
    }

    public boolean u() {
        return this.f4650a.isRewardedVideoAvailable(this.d);
    }

    public void v() {
        if (o()) {
            this.o = false;
        }
    }

    private void b(String str, String str2, int i2, String str3, int i3) {
        this.t = str2;
        this.l = str;
        this.w = i2;
        this.y = str3;
        this.x = i3;
    }

    public void c(IronSourceError ironSourceError) {
        a("onRewardedVideoAdShowFailed error=" + ironSourceError.b());
        b(1202, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}});
        synchronized (this.A) {
            if (this.e != SMASH_STATE.SHOW_IN_PROGRESS) {
                a(81316, new Object[][]{new Object[]{"errorCode", 5006}, new Object[]{"reason", "showFailed: " + this.e}});
                return;
            }
            a(SMASH_STATE.NOT_LOADED);
            this.f.a(ironSourceError, this);
        }
    }

    public void d(IronSourceError ironSourceError) {
        a(1212, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}, new Object[]{"duration", Long.valueOf(w())}});
    }

    public void a(String str, String str2, int i2, String str3, int i3) {
        SMASH_STATE smash_state;
        String str4 = str2;
        b("loadVideo() auctionId: " + str2 + " state: " + this.e);
        b(false);
        this.o = true;
        synchronized (this.A) {
            smash_state = this.e;
            if (!(this.e == SMASH_STATE.LOAD_IN_PROGRESS || this.e == SMASH_STATE.SHOW_IN_PROGRESS)) {
                a(SMASH_STATE.LOAD_IN_PROGRESS);
            }
        }
        if (smash_state == SMASH_STATE.LOAD_IN_PROGRESS) {
            a(81316, new Object[][]{new Object[]{"errorCode", 5003}, new Object[]{"reason", "load during load"}});
            this.n = true;
            b(str, str2, i2, str3, i3);
            this.f.b(this, str2);
        } else if (smash_state == SMASH_STATE.SHOW_IN_PROGRESS) {
            a(81316, new Object[][]{new Object[]{"errorCode", 5004}, new Object[]{"reason", "load during show"}});
            this.m = true;
            b(str, str2, i2, str3, i3);
        } else {
            this.s = str4;
            this.u = i2;
            this.v = str3;
            this.q = i3;
            z();
            this.r = new Date().getTime();
            a(1001);
            try {
                if (o()) {
                    String str5 = str;
                    this.f4650a.loadVideo(this.d, this, str);
                } else if (smash_state == SMASH_STATE.NO_INIT) {
                    y();
                    this.f4650a.initRewardedVideo(this.i, this.j, this.k, this.d, this);
                } else {
                    this.f4650a.fetchRewardedVideo(this.d);
                }
            } catch (Throwable th) {
                c("loadVideo exception: " + th.getLocalizedMessage());
                th.printStackTrace();
                a(81316, new Object[][]{new Object[]{"errorCode", 5005}, new Object[]{"reason", th.getLocalizedMessage()}});
            }
        }
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "ProgRvSmash " + k() + " : " + str, 0);
    }

    private void b(int i2) {
        b(i2, (Object[][]) null);
    }

    public void b(int i2, Object[][] objArr) {
        a(i2, objArr, true);
    }

    private void c(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "ProgRvSmash " + k() + " : " + str, 3);
    }

    public void f() {
        a("onRewardedVideoAdVisible");
        b(1206);
    }

    /* access modifiers changed from: private */
    public void a(SMASH_STATE smash_state) {
        b("current state=" + this.e + ", new state=" + smash_state);
        synchronized (this.A) {
            this.e = smash_state;
        }
    }

    public void a(boolean z2) {
        boolean z3;
        A();
        a("onRewardedVideoAvailabilityChanged available=" + z2 + " state=" + this.e.name());
        synchronized (this.A) {
            if (this.e == SMASH_STATE.LOAD_IN_PROGRESS) {
                a(z2 ? SMASH_STATE.LOADED : SMASH_STATE.NOT_LOADED);
                z3 = false;
            } else {
                z3 = true;
            }
        }
        if (!z3) {
            a(z2 ? AdError.LOAD_TOO_FREQUENTLY_ERROR_CODE : 1200, new Object[][]{new Object[]{"duration", Long.valueOf(w())}});
            if (this.n) {
                this.n = false;
                b("onRewardedVideoAvailabilityChanged to " + z2 + "and mShouldLoadAfterLoad is true - calling loadVideo");
                a(this.l, this.t, this.w, this.y, this.x);
                x();
            } else if (z2) {
                this.f.a(this, this.s);
            } else {
                this.f.b(this, this.s);
            }
        } else if (z2) {
            a(1207, new Object[][]{new Object[]{"ext1", this.e.name()}});
        } else {
            a(1208, new Object[][]{new Object[]{"errorCode", 1034}, new Object[]{"duration", Long.valueOf(w())}, new Object[]{"ext1", this.e.name()}});
        }
    }

    private void a(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "ProgRvSmash " + k() + " : " + str, 0);
    }

    private void a(int i2) {
        a(i2, (Object[][]) null, false);
    }

    public void a(int i2, Object[][] objArr) {
        a(i2, objArr, false);
    }

    private void a(int i2, Object[][] objArr, boolean z2) {
        Placement placement;
        Map<String, Object> n2 = n();
        if (!TextUtils.isEmpty(this.s)) {
            n2.put("auctionId", this.s);
        }
        if (z2 && (placement = this.p) != null && !TextUtils.isEmpty(placement.c())) {
            n2.put("placement", this.p.c());
        }
        if (c(i2)) {
            RewardedVideoEventsManager.g().a(n2, this.u, this.v);
        }
        n2.put("sessionDepth", Integer.valueOf(this.q));
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    n2.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e2) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, k() + " smash: RV sendMediationEvent " + Log.getStackTraceString(e2), 3);
            }
        }
        RewardedVideoEventsManager.g().d(new EventData(i2, new JSONObject(n2)));
    }
}
