package com.ironsource.mediationsdk;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class AuctionResponseItem {

    /* renamed from: a  reason: collision with root package name */
    private String f4588a;
    private String b;
    private String c;
    private List<String> d;
    private List<String> e;
    private List<String> f;
    private boolean g;

    public AuctionResponseItem(String str) {
        this.f4588a = str;
        this.b = "";
        this.c = "";
        this.d = new ArrayList();
        this.e = new ArrayList();
        this.f = new ArrayList();
        this.g = true;
    }

    public List<String> a() {
        return this.d;
    }

    public String b() {
        return this.f4588a;
    }

    public List<String> c() {
        return this.e;
    }

    public List<String> d() {
        return this.f;
    }

    public String e() {
        return this.c;
    }

    public String f() {
        return this.b;
    }

    public boolean g() {
        return this.g;
    }

    public AuctionResponseItem(JSONObject jSONObject) {
        this.g = false;
        try {
            this.f4588a = jSONObject.getString("instance");
            if (jSONObject.has("adMarkup")) {
                this.b = jSONObject.getString("adMarkup");
            } else {
                this.b = "";
            }
            if (jSONObject.has("price")) {
                this.c = jSONObject.getString("price");
            } else {
                this.c = "0";
            }
            JSONObject optJSONObject = jSONObject.optJSONObject("notifications");
            this.d = new ArrayList();
            if (optJSONObject.has("burl")) {
                JSONArray jSONArray = optJSONObject.getJSONArray("burl");
                for (int i = 0; i < jSONArray.length(); i++) {
                    this.d.add(jSONArray.getString(i));
                }
            }
            this.e = new ArrayList();
            if (optJSONObject.has("lurl")) {
                JSONArray jSONArray2 = optJSONObject.getJSONArray("lurl");
                for (int i2 = 0; i2 < jSONArray2.length(); i2++) {
                    this.e.add(jSONArray2.getString(i2));
                }
            }
            this.f = new ArrayList();
            if (optJSONObject.has("nurl")) {
                JSONArray jSONArray3 = optJSONObject.getJSONArray("nurl");
                for (int i3 = 0; i3 < jSONArray3.length(); i3++) {
                    this.f.add(jSONArray3.getString(i3));
                }
            }
            this.g = true;
        } catch (Exception unused) {
        }
    }
}
