package com.ironsource.mediationsdk;

import android.text.TextUtils;
import android.util.Pair;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

public class IronSourceSegment {

    /* renamed from: a  reason: collision with root package name */
    private String f4630a;
    private int b = -1;
    private String c;
    private int d = -1;
    private AtomicBoolean e = null;
    private double f = -1.0d;
    private long g = 0;
    private Vector<Pair<String, String>> h = new Vector<>();

    public int a() {
        return this.b;
    }

    public String b() {
        return this.c;
    }

    public double c() {
        return this.f;
    }

    public AtomicBoolean d() {
        return this.e;
    }

    public int e() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public Vector<Pair<String, String>> f() {
        Vector<Pair<String, String>> vector = new Vector<>();
        if (this.b != -1) {
            vector.add(new Pair("age", this.b + ""));
        }
        if (!TextUtils.isEmpty(this.c)) {
            vector.add(new Pair("gen", this.c));
        }
        if (this.d != -1) {
            vector.add(new Pair("lvl", this.d + ""));
        }
        if (this.e != null) {
            vector.add(new Pair("pay", this.e + ""));
        }
        if (this.f != -1.0d) {
            vector.add(new Pair("iapt", this.f + ""));
        }
        if (this.g != 0) {
            vector.add(new Pair("ucd", this.g + ""));
        }
        if (!TextUtils.isEmpty(this.f4630a)) {
            vector.add(new Pair("segName", this.f4630a));
        }
        vector.addAll(this.h);
        return vector;
    }

    public long g() {
        return this.g;
    }
}
