package com.ironsource.mediationsdk;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import java.util.Date;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class ProgIsSmash extends ProgSmash implements InterstitialSmashListener {
    /* access modifiers changed from: private */
    public SMASH_STATE e = SMASH_STATE.NO_INIT;
    /* access modifiers changed from: private */
    public ProgIsManagerListener f;
    private Timer g;
    private int h;
    private Activity i;
    private String j;
    private String k;
    /* access modifiers changed from: private */
    public long l;
    private final Object m = new Object();

    protected enum SMASH_STATE {
        NO_INIT,
        INIT_IN_PROGRESS,
        INIT_SUCCESS,
        LOAD_IN_PROGRESS,
        LOADED,
        LOAD_FAILED
    }

    public ProgIsSmash(Activity activity, String str, String str2, ProviderSettings providerSettings, ProgIsManagerListener progIsManagerListener, int i2, AbstractAdapter abstractAdapter) {
        super(new AdapterConfig(providerSettings, providerSettings.f()), abstractAdapter);
        this.i = activity;
        this.j = str;
        this.k = str2;
        this.f = progIsManagerListener;
        this.g = null;
        this.h = i2;
        this.f4650a.addInterstitialListener(this);
    }

    private void w() {
        try {
            String i2 = IronSourceObject.q().i();
            if (!TextUtils.isEmpty(i2)) {
                this.f4650a.setMediationSegment(i2);
            }
            String b = ConfigFile.d().b();
            if (!TextUtils.isEmpty(b)) {
                this.f4650a.setPluginData(b, ConfigFile.d().a());
            }
        } catch (Exception e2) {
            c("setCustomParams() " + e2.getMessage());
        }
    }

    private void x() {
        synchronized (this.m) {
            c("start timer");
            y();
            this.g = new Timer();
            this.g.schedule(new TimerTask() {
                public void run() {
                    ProgIsSmash progIsSmash = ProgIsSmash.this;
                    progIsSmash.c("timed out state=" + ProgIsSmash.this.e.name() + " isBidder=" + ProgIsSmash.this.o());
                    if (ProgIsSmash.this.e != SMASH_STATE.INIT_IN_PROGRESS || !ProgIsSmash.this.o()) {
                        ProgIsSmash.this.a(SMASH_STATE.LOAD_FAILED);
                        ProgIsSmash.this.f.a(ErrorBuilder.c("timed out"), ProgIsSmash.this, new Date().getTime() - ProgIsSmash.this.l);
                        return;
                    }
                    ProgIsSmash.this.a(SMASH_STATE.NO_INIT);
                }
            }, (long) (this.h * 1000));
        }
    }

    private void y() {
        synchronized (this.m) {
            if (this.g != null) {
                this.g.cancel();
                this.g = null;
            }
        }
    }

    public void d() {
        b("onInterstitialAdShowSucceeded");
        this.f.f(this);
    }

    public void e(IronSourceError ironSourceError) {
        b("onInterstitialInitFailed error" + ironSourceError.b() + " state=" + this.e.name());
        if (this.e == SMASH_STATE.INIT_IN_PROGRESS) {
            y();
            a(SMASH_STATE.NO_INIT);
            this.f.b(ironSourceError, this);
            if (!o()) {
                this.f.a(ironSourceError, this, new Date().getTime() - this.l);
            }
        }
    }

    public void onInterstitialAdClicked() {
        b("onInterstitialAdClicked");
        this.f.e(this);
    }

    public void onInterstitialInitSuccess() {
        b("onInterstitialInitSuccess state=" + this.e.name());
        if (this.e == SMASH_STATE.INIT_IN_PROGRESS) {
            y();
            if (o()) {
                a(SMASH_STATE.INIT_SUCCESS);
            } else {
                a(SMASH_STATE.LOAD_IN_PROGRESS);
                x();
                try {
                    this.f4650a.loadInterstitial(this.d, this);
                } catch (Throwable th) {
                    d("onInterstitialInitSuccess exception: " + th.getLocalizedMessage());
                    th.printStackTrace();
                }
            }
            this.f.a(this);
        }
    }

    public Map<String, Object> p() {
        try {
            if (o()) {
                return this.f4650a.getIsBiddingData(this.d);
            }
            return null;
        } catch (Throwable th) {
            d("getBiddingData exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            return null;
        }
    }

    public void q() {
        c("initForBidding()");
        a(SMASH_STATE.INIT_IN_PROGRESS);
        w();
        try {
            this.f4650a.initInterstitialForBidding(this.i, this.j, this.k, this.d, this);
        } catch (Throwable th) {
            d(k() + "loadInterstitial exception : " + th.getLocalizedMessage());
            th.printStackTrace();
            e(new IronSourceError(1041, th.getLocalizedMessage()));
        }
    }

    public boolean r() {
        SMASH_STATE smash_state = this.e;
        return smash_state == SMASH_STATE.INIT_IN_PROGRESS || smash_state == SMASH_STATE.LOAD_IN_PROGRESS;
    }

    public boolean s() {
        SMASH_STATE smash_state = this.e;
        return smash_state == SMASH_STATE.INIT_SUCCESS || smash_state == SMASH_STATE.LOADED || smash_state == SMASH_STATE.LOAD_FAILED;
    }

    public boolean t() {
        try {
            return this.f4650a.isInterstitialReady(this.d);
        } catch (Throwable th) {
            d("isReadyToShow exception: " + th.getLocalizedMessage());
            th.printStackTrace();
            return false;
        }
    }

    public void u() {
        this.f4650a.setMediationState(AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION, "interstitial");
    }

    public void v() {
        try {
            this.f4650a.showInterstitial(this.d, this);
        } catch (Throwable th) {
            d(k() + "showInterstitial exception : " + th.getLocalizedMessage());
            th.printStackTrace();
            this.f.a(new IronSourceError(1039, th.getLocalizedMessage()), this);
        }
    }

    public void b() {
        b("onInterstitialAdClosed");
        this.f.d(this);
    }

    public void c() {
        b("onInterstitialAdOpened");
        this.f.c(this);
    }

    private void d(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "ProgIsSmash " + k() + " : " + str, 3);
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "ProgIsSmash " + k() + " : " + str, 0);
    }

    public void a(String str) {
        try {
            this.l = new Date().getTime();
            c("loadInterstitial");
            b(false);
            if (o()) {
                x();
                a(SMASH_STATE.LOAD_IN_PROGRESS);
                this.f4650a.loadInterstitial(this.d, this, str);
            } else if (this.e == SMASH_STATE.NO_INIT) {
                x();
                a(SMASH_STATE.INIT_IN_PROGRESS);
                w();
                this.f4650a.initInterstitial(this.i, this.j, this.k, this.d, this);
            } else {
                x();
                a(SMASH_STATE.LOAD_IN_PROGRESS);
                this.f4650a.loadInterstitial(this.d, this);
            }
        } catch (Throwable th) {
            d("loadInterstitial exception: " + th.getLocalizedMessage());
            th.printStackTrace();
        }
    }

    public void b(IronSourceError ironSourceError) {
        b("onInterstitialAdShowFailed error=" + ironSourceError.b());
        this.f.a(ironSourceError, this);
    }

    private void b(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "ProgIsSmash " + k() + " : " + str, 0);
    }

    public void e() {
        b("onInterstitialAdVisible");
        this.f.b(this);
    }

    /* access modifiers changed from: private */
    public void a(SMASH_STATE smash_state) {
        c("current state=" + this.e + ", new state=" + smash_state);
        this.e = smash_state;
    }

    public void a() {
        b("onInterstitialAdReady state=" + this.e.name());
        y();
        if (this.e == SMASH_STATE.LOAD_IN_PROGRESS) {
            a(SMASH_STATE.LOADED);
            this.f.a(this, new Date().getTime() - this.l);
        }
    }

    public void a(IronSourceError ironSourceError) {
        b("onInterstitialAdLoadFailed error=" + ironSourceError.b() + " state=" + this.e.name());
        y();
        if (this.e == SMASH_STATE.LOAD_IN_PROGRESS) {
            a(SMASH_STATE.LOAD_FAILED);
            this.f.a(ironSourceError, this, new Date().getTime() - this.l);
        }
    }
}
