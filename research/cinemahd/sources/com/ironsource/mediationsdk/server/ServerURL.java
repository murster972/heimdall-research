package com.ironsource.mediationsdk.server;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.mediationsdk.config.ConfigFile;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Vector;

public class ServerURL {

    /* renamed from: a  reason: collision with root package name */
    private static String f4727a = "https://init.supersonicads.com/sdk/v";
    private static String b = "?request=";

    public static String a(Context context, String str, String str2, String str3, String str4, Vector<Pair<String, String>> vector) throws UnsupportedEncodingException {
        Vector vector2 = new Vector();
        vector2.add(new Pair("platform", "android"));
        vector2.add(new Pair("applicationKey", str));
        vector2.add(new Pair("applicationUserId", str2));
        vector2.add(new Pair("sdkVersion", IronSourceUtils.b()));
        if (IronSourceUtils.c() == 0) {
            vector2.add(new Pair("serr", String.valueOf(IronSourceUtils.c())));
        }
        if (!TextUtils.isEmpty(ConfigFile.d().b())) {
            vector2.add(new Pair("pluginType", ConfigFile.d().b()));
        }
        if (!TextUtils.isEmpty(ConfigFile.d().c())) {
            vector2.add(new Pair("pluginVersion", ConfigFile.d().c()));
        }
        if (!TextUtils.isEmpty(ConfigFile.d().a())) {
            vector2.add(new Pair("plugin_fw_v", ConfigFile.d().a()));
        }
        if (!TextUtils.isEmpty(str3)) {
            vector2.add(new Pair("advId", str3));
        }
        if (!TextUtils.isEmpty(str4)) {
            vector2.add(new Pair("mt", str4));
        }
        String a2 = ApplicationContext.a(context, context.getPackageName());
        if (!TextUtils.isEmpty(a2)) {
            vector2.add(new Pair("appVer", a2));
        }
        int i = Build.VERSION.SDK_INT;
        vector2.add(new Pair("osVer", i + ""));
        vector2.add(new Pair("devMake", Build.MANUFACTURER));
        vector2.add(new Pair("devModel", Build.MODEL));
        String a3 = IronSourceUtils.a(context);
        if (!TextUtils.isEmpty(a3)) {
            vector2.add(new Pair("connType", a3));
        }
        if (vector != null) {
            vector2.addAll(vector);
        }
        String encode = URLEncoder.encode(IronSourceAES.b("C38FB23A402222A0C17D34A92F971D1F", a((Vector<Pair<String, String>>) vector2)), "UTF-8");
        return a(IronSourceUtils.b()) + encode;
    }

    private static String a(Vector<Pair<String, String>> vector) throws UnsupportedEncodingException {
        Iterator<Pair<String, String>> it2 = vector.iterator();
        String str = "";
        while (it2.hasNext()) {
            Pair next = it2.next();
            if (str.length() > 0) {
                str = str + "&";
            }
            str = str + ((String) next.first) + "=" + URLEncoder.encode((String) next.second, "UTF-8");
        }
        return str;
    }

    private static String a(String str) {
        return f4727a + str + b;
    }
}
