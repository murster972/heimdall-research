package com.ironsource.mediationsdk.server;

public class HttpFunctions {
    /* JADX WARNING: Removed duplicated region for block: B:38:0x007d  */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x008a  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x008f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r4, com.ironsource.mediationsdk.IronSourceObject.IResponseListener r5) throws java.lang.Exception {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x0086, all -> 0x0079 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x0086, all -> 0x0079 }
            java.net.URLConnection r4 = r1.openConnection()     // Catch:{ Exception -> 0x0086, all -> 0x0079 }
            java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ Exception -> 0x0086, all -> 0x0079 }
            r1 = 15000(0x3a98, float:2.102E-41)
            r4.setReadTimeout(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            r4.setConnectTimeout(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            java.lang.String r1 = "GET"
            r4.setRequestMethod(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            r1 = 1
            r4.setDoInput(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            r4.connect()     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            int r1 = r4.getResponseCode()     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            r2 = 400(0x190, float:5.6E-43)
            if (r1 != r2) goto L_0x0035
            if (r5 == 0) goto L_0x002f
            java.lang.String r1 = "Bad Request - 400"
            r5.a(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
        L_0x002f:
            if (r4 == 0) goto L_0x0034
            r4.disconnect()
        L_0x0034:
            return r0
        L_0x0035:
            java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            java.io.InputStream r2 = r4.getInputStream()     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            r5.<init>(r1)     // Catch:{ Exception -> 0x0077, all -> 0x0075 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0073, all -> 0x006e }
            r1.<init>()     // Catch:{ Exception -> 0x0073, all -> 0x006e }
        L_0x0048:
            java.lang.String r2 = r5.readLine()     // Catch:{ Exception -> 0x0073, all -> 0x006e }
            if (r2 == 0) goto L_0x0052
            r1.append(r2)     // Catch:{ Exception -> 0x0073, all -> 0x006e }
            goto L_0x0048
        L_0x0052:
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0073, all -> 0x006e }
            boolean r2 = android.text.TextUtils.isEmpty(r1)     // Catch:{ Exception -> 0x0073, all -> 0x006e }
            if (r2 == 0) goto L_0x0065
            if (r4 == 0) goto L_0x0061
            r4.disconnect()
        L_0x0061:
            r5.close()
            return r0
        L_0x0065:
            if (r4 == 0) goto L_0x006a
            r4.disconnect()
        L_0x006a:
            r5.close()
            return r1
        L_0x006e:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x007b
        L_0x0073:
            goto L_0x0088
        L_0x0075:
            r5 = move-exception
            goto L_0x007b
        L_0x0077:
            r5 = r0
            goto L_0x0088
        L_0x0079:
            r5 = move-exception
            r4 = r0
        L_0x007b:
            if (r4 == 0) goto L_0x0080
            r4.disconnect()
        L_0x0080:
            if (r0 == 0) goto L_0x0085
            r0.close()
        L_0x0085:
            throw r5
        L_0x0086:
            r4 = r0
            r5 = r4
        L_0x0088:
            if (r4 == 0) goto L_0x008d
            r4.disconnect()
        L_0x008d:
            if (r5 == 0) goto L_0x0092
            r5.close()
        L_0x0092:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.server.HttpFunctions.a(java.lang.String, com.ironsource.mediationsdk.IronSourceObject$IResponseListener):java.lang.String");
    }
}
