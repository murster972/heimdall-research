package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.model.RewardedVideoConfigurations;
import com.ironsource.mediationsdk.utils.AuctionSettings;
import com.ironsource.mediationsdk.utils.SessionCappingManager;
import com.uwetrottmann.trakt5.TraktV2;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONObject;

class ProgRvManager implements ProgRvManagerListener, RvLoadTriggerCallback, AuctionEventListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final ConcurrentHashMap<String, ProgRvSmash> f4644a;
    private CopyOnWriteArrayList<ProgRvSmash> b;
    private ConcurrentHashMap<String, AuctionResponseItem> c;
    /* access modifiers changed from: private */
    public SessionCappingManager d;
    private RvLoadTrigger e;
    private boolean f;
    /* access modifiers changed from: private */
    public AuctionHandler g;
    /* access modifiers changed from: private */
    public Context h;
    private String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public int k = 1;
    /* access modifiers changed from: private */
    public long l;
    private long m;
    private int n;
    private Boolean o;
    private RV_MEDIATION_STATE p;
    private int q;
    private String r = "";

    private enum RV_MEDIATION_STATE {
        RV_STATE_INITIATING,
        RV_STATE_AUCTION_IN_PROGRESS,
        RV_STATE_NOT_LOADED,
        RV_STATE_LOADING_SMASHES,
        RV_STATE_READY_TO_SHOW
    }

    public ProgRvManager(Activity activity, List<ProviderSettings> list, RewardedVideoConfigurations rewardedVideoConfigurations, String str, String str2) {
        long time = new Date().getTime();
        a(81312);
        a(RV_MEDIATION_STATE.RV_STATE_INITIATING);
        this.h = activity.getApplicationContext();
        this.o = null;
        this.n = rewardedVideoConfigurations.e();
        this.i = "";
        AuctionSettings g2 = rewardedVideoConfigurations.g();
        this.b = new CopyOnWriteArrayList<>();
        this.c = new ConcurrentHashMap<>();
        this.m = new Date().getTime();
        this.f = g2.e() > 0;
        if (this.f) {
            this.g = new AuctionHandler("rewardedVideo", g2, this);
        }
        this.e = new RvLoadTrigger(g2, this);
        this.f4644a = new ConcurrentHashMap<>();
        for (ProviderSettings next : list) {
            AbstractAdapter a2 = AdapterRepository.a().a(next, next.k(), activity);
            if (a2 != null && AdaptersCompatibilityHandler.a().b(a2)) {
                ProgRvSmash progRvSmash = r0;
                ProgRvSmash progRvSmash2 = new ProgRvSmash(activity, str, str2, next, this, rewardedVideoConfigurations.f(), a2);
                this.f4644a.put(progRvSmash.k(), progRvSmash);
            }
        }
        this.d = new SessionCappingManager(new ArrayList(this.f4644a.values()));
        for (ProgRvSmash next2 : this.f4644a.values()) {
            if (next2.o()) {
                next2.q();
            }
        }
        c(81313, new Object[][]{new Object[]{"duration", Long.valueOf(new Date().getTime() - time)}});
        a(g2.g());
    }

    private boolean b(int i2) {
        return i2 == 1003 || i2 == 1302 || i2 == 1301;
    }

    /* access modifiers changed from: private */
    public void c() {
        a(RV_MEDIATION_STATE.RV_STATE_NOT_LOADED);
        a(false);
        this.e.a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007a, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void d() {
        /*
            r10 = this;
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgRvSmash> r0 = r10.f4644a
            monitor-enter(r0)
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r1 = r10.b     // Catch:{ all -> 0x007b }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x007b }
            r2 = 0
            if (r1 == 0) goto L_0x0038
            r1 = 81001(0x13c69, float:1.13507E-40)
            r3 = 2
            java.lang.Object[][] r4 = new java.lang.Object[r3][]     // Catch:{ all -> 0x007b }
            java.lang.Object[] r5 = new java.lang.Object[r3]     // Catch:{ all -> 0x007b }
            java.lang.String r6 = "errorCode"
            r5[r2] = r6     // Catch:{ all -> 0x007b }
            r6 = 80004(0x13884, float:1.1211E-40)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x007b }
            r7 = 1
            r5[r7] = r6     // Catch:{ all -> 0x007b }
            r4[r2] = r5     // Catch:{ all -> 0x007b }
            java.lang.Object[] r3 = new java.lang.Object[r3]     // Catch:{ all -> 0x007b }
            java.lang.String r5 = "reason"
            r3[r2] = r5     // Catch:{ all -> 0x007b }
            java.lang.String r2 = "waterfall is empty"
            r3[r7] = r2     // Catch:{ all -> 0x007b }
            r4[r7] = r3     // Catch:{ all -> 0x007b }
            r10.a((int) r1, (java.lang.Object[][]) r4)     // Catch:{ all -> 0x007b }
            r10.c()     // Catch:{ all -> 0x007b }
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            return
        L_0x0038:
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r1 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_LOADING_SMASHES     // Catch:{ all -> 0x007b }
            r10.a((com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE) r1)     // Catch:{ all -> 0x007b }
            r1 = 0
        L_0x003e:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r3 = r10.b     // Catch:{ all -> 0x007b }
            int r3 = r3.size()     // Catch:{ all -> 0x007b }
            if (r2 >= r3) goto L_0x0079
            int r3 = r10.n     // Catch:{ all -> 0x007b }
            if (r1 >= r3) goto L_0x0079
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r3 = r10.b     // Catch:{ all -> 0x007b }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ all -> 0x007b }
            r4 = r3
            com.ironsource.mediationsdk.ProgRvSmash r4 = (com.ironsource.mediationsdk.ProgRvSmash) r4     // Catch:{ all -> 0x007b }
            boolean r3 = r4.l()     // Catch:{ all -> 0x007b }
            if (r3 == 0) goto L_0x0076
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r3 = r10.c     // Catch:{ all -> 0x007b }
            java.lang.String r5 = r4.k()     // Catch:{ all -> 0x007b }
            java.lang.Object r3 = r3.get(r5)     // Catch:{ all -> 0x007b }
            com.ironsource.mediationsdk.AuctionResponseItem r3 = (com.ironsource.mediationsdk.AuctionResponseItem) r3     // Catch:{ all -> 0x007b }
            java.lang.String r5 = r3.f()     // Catch:{ all -> 0x007b }
            java.lang.String r6 = r10.j     // Catch:{ all -> 0x007b }
            int r7 = r10.q     // Catch:{ all -> 0x007b }
            java.lang.String r8 = r10.r     // Catch:{ all -> 0x007b }
            int r9 = r10.k     // Catch:{ all -> 0x007b }
            r4.a(r5, r6, r7, r8, r9)     // Catch:{ all -> 0x007b }
            int r1 = r1 + 1
        L_0x0076:
            int r2 = r2 + 1
            goto L_0x003e
        L_0x0079:
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            return
        L_0x007b:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x007b }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.d():void");
    }

    /* access modifiers changed from: private */
    public void e() {
        a(RV_MEDIATION_STATE.RV_STATE_AUCTION_IN_PROGRESS);
        AsyncTask.execute(new Runnable() {
            public void run() {
                ProgRvManager.this.b("makeAuction()");
                String unused = ProgRvManager.this.j = "";
                long unused2 = ProgRvManager.this.l = new Date().getTime();
                HashMap hashMap = new HashMap();
                ArrayList arrayList = new ArrayList();
                StringBuilder sb = new StringBuilder();
                synchronized (ProgRvManager.this.f4644a) {
                    for (ProgRvSmash progRvSmash : ProgRvManager.this.f4644a.values()) {
                        progRvSmash.v();
                        if (!ProgRvManager.this.d.b(progRvSmash)) {
                            if (progRvSmash.o() && progRvSmash.s()) {
                                Map<String, Object> p = progRvSmash.p();
                                if (p != null) {
                                    hashMap.put(progRvSmash.k(), p);
                                    sb.append(TraktV2.API_VERSION + progRvSmash.k() + ",");
                                }
                            } else if (!progRvSmash.o()) {
                                arrayList.add(progRvSmash.k());
                                sb.append(DiskLruCache.VERSION_1 + progRvSmash.k() + ",");
                            }
                        }
                    }
                }
                if (hashMap.keySet().size() == 0 && arrayList.size() == 0) {
                    ProgRvManager.this.b("makeAuction() failed - request waterfall is empty");
                    ProgRvManager.this.a(81001, new Object[][]{new Object[]{"errorCode", 80003}, new Object[]{"reason", "waterfall request is empty"}});
                    ProgRvManager.this.c();
                    return;
                }
                ProgRvManager progRvManager = ProgRvManager.this;
                progRvManager.b("makeAuction() - request waterfall is: " + sb);
                if (sb.length() > 256) {
                    sb.setLength(256);
                } else if (sb.length() > 0) {
                    sb.deleteCharAt(sb.length() - 1);
                }
                ProgRvManager.this.a(1000);
                ProgRvManager.this.a(1300);
                ProgRvManager.this.c(1310, new Object[][]{new Object[]{"ext1", sb.toString()}});
                ProgRvManager.this.g.a(ProgRvManager.this.h, hashMap, arrayList, ProgRvManager.this.k);
            }
        });
    }

    private void f() {
        a(b());
    }

    public void b(Activity activity) {
        for (ProgRvSmash b2 : this.f4644a.values()) {
            b2.b(activity);
        }
    }

    private void c(ProgRvSmash progRvSmash, String str) {
        String str2 = progRvSmash.k() + " : " + str;
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK, "ProgRvManager: " + str2, 0);
    }

    private void a(long j2) {
        if (this.d.a()) {
            a(81001, new Object[][]{new Object[]{"errorCode", 80001}, new Object[]{"reason", "all smashes are capped"}});
            c();
        } else if (this.f) {
            new Timer().schedule(new TimerTask() {
                public void run() {
                    ProgRvManager.this.e();
                }
            }, j2);
        } else {
            f();
            if (this.b.isEmpty()) {
                a(81001, new Object[][]{new Object[]{"errorCode", 80002}, new Object[]{"reason", "waterfall is empty"}});
                c();
                return;
            }
            a(1000);
            d();
        }
    }

    private List<AuctionResponseItem> b() {
        CopyOnWriteArrayList copyOnWriteArrayList = new CopyOnWriteArrayList();
        for (ProgRvSmash next : this.f4644a.values()) {
            if (!next.o() && !this.d.b(next)) {
                copyOnWriteArrayList.add(new AuctionResponseItem(next.k()));
            }
        }
        return copyOnWriteArrayList;
    }

    /* access modifiers changed from: private */
    public void c(int i2, Object[][] objArr) {
        a(i2, objArr, false, false);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d5, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b(com.ironsource.mediationsdk.ProgRvSmash r12, java.lang.String r13) {
        /*
            r11 = this;
            monitor-enter(r11)
            java.lang.String r0 = "onLoadError "
            r11.c((com.ironsource.mediationsdk.ProgRvSmash) r12, (java.lang.String) r0)     // Catch:{ all -> 0x00d9 }
            java.lang.String r0 = r11.j     // Catch:{ all -> 0x00d9 }
            boolean r0 = r13.equalsIgnoreCase(r0)     // Catch:{ all -> 0x00d9 }
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0065
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d9 }
            r0.<init>()     // Catch:{ all -> 0x00d9 }
            java.lang.String r3 = "onLoadError was invoked with auctionId:"
            r0.append(r3)     // Catch:{ all -> 0x00d9 }
            r0.append(r13)     // Catch:{ all -> 0x00d9 }
            java.lang.String r13 = " and the current id is "
            r0.append(r13)     // Catch:{ all -> 0x00d9 }
            java.lang.String r13 = r11.j     // Catch:{ all -> 0x00d9 }
            r0.append(r13)     // Catch:{ all -> 0x00d9 }
            java.lang.String r13 = r0.toString()     // Catch:{ all -> 0x00d9 }
            r11.b((java.lang.String) r13)     // Catch:{ all -> 0x00d9 }
            r13 = 81315(0x13da3, float:1.13947E-40)
            r0 = 2
            java.lang.Object[][] r3 = new java.lang.Object[r0][]     // Catch:{ all -> 0x00d9 }
            java.lang.Object[] r4 = new java.lang.Object[r0]     // Catch:{ all -> 0x00d9 }
            java.lang.String r5 = "errorCode"
            r4[r2] = r5     // Catch:{ all -> 0x00d9 }
            r5 = 4
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ all -> 0x00d9 }
            r4[r1] = r5     // Catch:{ all -> 0x00d9 }
            r3[r2] = r4     // Catch:{ all -> 0x00d9 }
            java.lang.Object[] r0 = new java.lang.Object[r0]     // Catch:{ all -> 0x00d9 }
            java.lang.String r4 = "reason"
            r0[r2] = r4     // Catch:{ all -> 0x00d9 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d9 }
            r2.<init>()     // Catch:{ all -> 0x00d9 }
            java.lang.String r4 = "loadError wrong auction ID "
            r2.append(r4)     // Catch:{ all -> 0x00d9 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r4 = r11.p     // Catch:{ all -> 0x00d9 }
            r2.append(r4)     // Catch:{ all -> 0x00d9 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00d9 }
            r0[r1] = r2     // Catch:{ all -> 0x00d9 }
            r3[r1] = r0     // Catch:{ all -> 0x00d9 }
            r12.a((int) r13, (java.lang.Object[][]) r3)     // Catch:{ all -> 0x00d9 }
            monitor-exit(r11)
            return
        L_0x0065:
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.ProgRvSmash> r12 = r11.f4644a     // Catch:{ all -> 0x00d9 }
            monitor-enter(r12)     // Catch:{ all -> 0x00d9 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r13 = r11.b     // Catch:{ all -> 0x00d6 }
            java.util.Iterator r13 = r13.iterator()     // Catch:{ all -> 0x00d6 }
            r0 = 0
            r3 = 0
        L_0x0070:
            boolean r4 = r13.hasNext()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x00bd
            java.lang.Object r4 = r13.next()     // Catch:{ all -> 0x00d6 }
            r5 = r4
            com.ironsource.mediationsdk.ProgRvSmash r5 = (com.ironsource.mediationsdk.ProgRvSmash) r5     // Catch:{ all -> 0x00d6 }
            boolean r4 = r5.l()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x00ad
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r4 = r11.c     // Catch:{ all -> 0x00d6 }
            java.lang.String r6 = r5.k()     // Catch:{ all -> 0x00d6 }
            java.lang.Object r4 = r4.get(r6)     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x0070
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r13 = r11.c     // Catch:{ all -> 0x00d6 }
            java.lang.String r0 = r5.k()     // Catch:{ all -> 0x00d6 }
            java.lang.Object r13 = r13.get(r0)     // Catch:{ all -> 0x00d6 }
            com.ironsource.mediationsdk.AuctionResponseItem r13 = (com.ironsource.mediationsdk.AuctionResponseItem) r13     // Catch:{ all -> 0x00d6 }
            java.lang.String r6 = r13.f()     // Catch:{ all -> 0x00d6 }
            java.lang.String r7 = r11.j     // Catch:{ all -> 0x00d6 }
            int r8 = r11.q     // Catch:{ all -> 0x00d6 }
            java.lang.String r9 = r11.r     // Catch:{ all -> 0x00d6 }
            int r10 = r11.k     // Catch:{ all -> 0x00d6 }
            r5.a(r6, r7, r8, r9, r10)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r12)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r11)
            return
        L_0x00ad:
            boolean r4 = r5.r()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x00b5
            r3 = 1
            goto L_0x0070
        L_0x00b5:
            boolean r4 = r5.t()     // Catch:{ all -> 0x00d6 }
            if (r4 == 0) goto L_0x0070
            r0 = 1
            goto L_0x0070
        L_0x00bd:
            if (r0 != 0) goto L_0x00d3
            if (r3 != 0) goto L_0x00d3
            java.lang.String r13 = "onLoadError(): No other available smashes"
            r11.b((java.lang.String) r13)     // Catch:{ all -> 0x00d6 }
            r11.a((boolean) r2)     // Catch:{ all -> 0x00d6 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r13 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_NOT_LOADED     // Catch:{ all -> 0x00d6 }
            r11.a((com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE) r13)     // Catch:{ all -> 0x00d6 }
            com.ironsource.mediationsdk.RvLoadTrigger r13 = r11.e     // Catch:{ all -> 0x00d6 }
            r13.a()     // Catch:{ all -> 0x00d6 }
        L_0x00d3:
            monitor-exit(r12)     // Catch:{ all -> 0x00d6 }
            monitor-exit(r11)
            return
        L_0x00d6:
            r13 = move-exception
            monitor-exit(r12)     // Catch:{ all -> 0x00d6 }
            throw r13     // Catch:{ all -> 0x00d9 }
        L_0x00d9:
            r12 = move-exception
            monitor-exit(r11)
            throw r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.b(com.ironsource.mediationsdk.ProgRvSmash, java.lang.String):void");
    }

    public void a(Activity activity) {
        for (ProgRvSmash a2 : this.f4644a.values()) {
            a2.a(activity);
        }
    }

    private String a(AuctionResponseItem auctionResponseItem) {
        String str = TextUtils.isEmpty(auctionResponseItem.f()) ? DiskLruCache.VERSION_1 : TraktV2.API_VERSION;
        return str + auctionResponseItem.b();
    }

    private void a(List<AuctionResponseItem> list) {
        synchronized (this.f4644a) {
            this.b.clear();
            this.c.clear();
            StringBuilder sb = new StringBuilder();
            for (AuctionResponseItem next : list) {
                sb.append(a(next) + ",");
                ProgRvSmash progRvSmash = this.f4644a.get(next.b());
                if (progRvSmash != null) {
                    progRvSmash.b(true);
                    this.b.add(progRvSmash);
                    this.c.put(progRvSmash.k(), next);
                } else {
                    b("updateWaterfall() - could not find matching smash for auction response item " + next.b());
                }
            }
            b("updateWaterfall() - response waterfall is " + sb.toString());
            if (sb.length() > 256) {
                sb.setLength(256);
            } else if (sb.length() > 0) {
                sb.deleteCharAt(sb.length() - 1);
            } else {
                b("Updated waterfall is empty");
            }
            a(1311, new Object[][]{new Object[]{"ext1", sb.toString()}});
        }
    }

    public void b(ProgRvSmash progRvSmash) {
        boolean z;
        synchronized (this) {
            Iterator<ProgRvSmash> it2 = this.f4644a.values().iterator();
            while (true) {
                if (!it2.hasNext()) {
                    z = false;
                    break;
                }
                ProgRvSmash next = it2.next();
                if (!next.equals(progRvSmash) && next.u()) {
                    z = true;
                    break;
                }
            }
            Object[][] objArr = new Object[1][];
            Object[] objArr2 = new Object[2];
            objArr2[0] = "ext1";
            StringBuilder sb = new StringBuilder();
            sb.append("otherRVAvailable = ");
            sb.append(z ? "true" : "false");
            objArr2[1] = sb.toString();
            objArr[0] = objArr2;
            progRvSmash.b(1203, objArr);
            c(progRvSmash, "onRewardedVideoAdClosed, mediation state: " + this.p.name());
            RVListenerWrapper.c().a();
            if (this.p != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                a(false);
            }
            this.e.b();
        }
    }

    private void a(RV_MEDIATION_STATE rv_mediation_state) {
        b("current state=" + this.p + ", new state=" + rv_mediation_state);
        this.p = rv_mediation_state;
    }

    public void b(ProgRvSmash progRvSmash, Placement placement) {
        c(progRvSmash, "onRewardedVideoAdClicked");
        RVListenerWrapper.c().a(placement);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0124, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(com.ironsource.mediationsdk.ProgRvSmash r11, java.lang.String r12) {
        /*
            r10 = this;
            monitor-enter(r10)
            java.lang.String r0 = "onLoadSuccess "
            r10.c((com.ironsource.mediationsdk.ProgRvSmash) r11, (java.lang.String) r0)     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = r10.j     // Catch:{ all -> 0x0125 }
            r1 = 2
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x0068
            java.lang.String r0 = r10.j     // Catch:{ all -> 0x0125 }
            boolean r0 = r12.equalsIgnoreCase(r0)     // Catch:{ all -> 0x0125 }
            if (r0 != 0) goto L_0x0068
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r0.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r4 = "onLoadSuccess was invoked with auctionId: "
            r0.append(r4)     // Catch:{ all -> 0x0125 }
            r0.append(r12)     // Catch:{ all -> 0x0125 }
            java.lang.String r12 = " and the current id is "
            r0.append(r12)     // Catch:{ all -> 0x0125 }
            java.lang.String r12 = r10.j     // Catch:{ all -> 0x0125 }
            r0.append(r12)     // Catch:{ all -> 0x0125 }
            java.lang.String r12 = r0.toString()     // Catch:{ all -> 0x0125 }
            r10.b((java.lang.String) r12)     // Catch:{ all -> 0x0125 }
            r12 = 81315(0x13da3, float:1.13947E-40)
            java.lang.Object[][] r0 = new java.lang.Object[r1][]     // Catch:{ all -> 0x0125 }
            java.lang.Object[] r4 = new java.lang.Object[r1]     // Catch:{ all -> 0x0125 }
            java.lang.String r5 = "errorCode"
            r4[r2] = r5     // Catch:{ all -> 0x0125 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r1)     // Catch:{ all -> 0x0125 }
            r4[r3] = r5     // Catch:{ all -> 0x0125 }
            r0[r2] = r4     // Catch:{ all -> 0x0125 }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ all -> 0x0125 }
            java.lang.String r4 = "reason"
            r1[r2] = r4     // Catch:{ all -> 0x0125 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r2.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r4 = "onLoadSuccess wrong auction ID "
            r2.append(r4)     // Catch:{ all -> 0x0125 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r4 = r10.p     // Catch:{ all -> 0x0125 }
            r2.append(r4)     // Catch:{ all -> 0x0125 }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0125 }
            r1[r3] = r2     // Catch:{ all -> 0x0125 }
            r0[r3] = r1     // Catch:{ all -> 0x0125 }
            r11.a((int) r12, (java.lang.Object[][]) r0)     // Catch:{ all -> 0x0125 }
            monitor-exit(r10)
            return
        L_0x0068:
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r0 = r10.p     // Catch:{ all -> 0x0125 }
            r10.a((boolean) r3)     // Catch:{ all -> 0x0125 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r4 = r10.p     // Catch:{ all -> 0x0125 }
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r5 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW     // Catch:{ all -> 0x0125 }
            if (r4 == r5) goto L_0x0123
            com.ironsource.mediationsdk.ProgRvManager$RV_MEDIATION_STATE r4 = com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW     // Catch:{ all -> 0x0125 }
            r10.a((com.ironsource.mediationsdk.ProgRvManager.RV_MEDIATION_STATE) r4)     // Catch:{ all -> 0x0125 }
            java.util.Date r4 = new java.util.Date     // Catch:{ all -> 0x0125 }
            r4.<init>()     // Catch:{ all -> 0x0125 }
            long r4 = r4.getTime()     // Catch:{ all -> 0x0125 }
            long r6 = r10.l     // Catch:{ all -> 0x0125 }
            long r4 = r4 - r6
            r6 = 1003(0x3eb, float:1.406E-42)
            java.lang.Object[][] r7 = new java.lang.Object[r3][]     // Catch:{ all -> 0x0125 }
            java.lang.Object[] r8 = new java.lang.Object[r1]     // Catch:{ all -> 0x0125 }
            java.lang.String r9 = "duration"
            r8[r2] = r9     // Catch:{ all -> 0x0125 }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ all -> 0x0125 }
            r8[r3] = r4     // Catch:{ all -> 0x0125 }
            r7[r2] = r8     // Catch:{ all -> 0x0125 }
            r10.a((int) r6, (java.lang.Object[][]) r7)     // Catch:{ all -> 0x0125 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r4 = r10.c     // Catch:{ all -> 0x0125 }
            java.lang.String r5 = r11.k()     // Catch:{ all -> 0x0125 }
            java.lang.Object r4 = r4.get(r5)     // Catch:{ all -> 0x0125 }
            com.ironsource.mediationsdk.AuctionResponseItem r4 = (com.ironsource.mediationsdk.AuctionResponseItem) r4     // Catch:{ all -> 0x0125 }
            if (r4 == 0) goto L_0x00b6
            com.ironsource.mediationsdk.AuctionHandler r11 = r10.g     // Catch:{ all -> 0x0125 }
            r11.b(r4)     // Catch:{ all -> 0x0125 }
            com.ironsource.mediationsdk.AuctionHandler r11 = r10.g     // Catch:{ all -> 0x0125 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.ProgRvSmash> r12 = r10.b     // Catch:{ all -> 0x0125 }
            java.util.concurrent.ConcurrentHashMap<java.lang.String, com.ironsource.mediationsdk.AuctionResponseItem> r0 = r10.c     // Catch:{ all -> 0x0125 }
            r11.a(r12, r0, r4)     // Catch:{ all -> 0x0125 }
            goto L_0x0123
        L_0x00b6:
            if (r11 == 0) goto L_0x00bd
            java.lang.String r11 = r11.k()     // Catch:{ all -> 0x0125 }
            goto L_0x00bf
        L_0x00bd:
            java.lang.String r11 = "Smash is null"
        L_0x00bf:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r4.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r5 = "onLoadSuccess winner instance "
            r4.append(r5)     // Catch:{ all -> 0x0125 }
            r4.append(r11)     // Catch:{ all -> 0x0125 }
            java.lang.String r5 = " missing from waterfall. auctionId: "
            r4.append(r5)     // Catch:{ all -> 0x0125 }
            r4.append(r12)     // Catch:{ all -> 0x0125 }
            java.lang.String r12 = " and the current id is "
            r4.append(r12)     // Catch:{ all -> 0x0125 }
            java.lang.String r12 = r10.j     // Catch:{ all -> 0x0125 }
            r4.append(r12)     // Catch:{ all -> 0x0125 }
            java.lang.String r12 = r4.toString()     // Catch:{ all -> 0x0125 }
            r10.a((java.lang.String) r12)     // Catch:{ all -> 0x0125 }
            r12 = 81317(0x13da5, float:1.1395E-40)
            r4 = 3
            java.lang.Object[][] r4 = new java.lang.Object[r4][]     // Catch:{ all -> 0x0125 }
            java.lang.Object[] r5 = new java.lang.Object[r1]     // Catch:{ all -> 0x0125 }
            java.lang.String r6 = "errorCode"
            r5[r2] = r6     // Catch:{ all -> 0x0125 }
            r6 = 1010(0x3f2, float:1.415E-42)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ all -> 0x0125 }
            r5[r3] = r6     // Catch:{ all -> 0x0125 }
            r4[r2] = r5     // Catch:{ all -> 0x0125 }
            java.lang.Object[] r5 = new java.lang.Object[r1]     // Catch:{ all -> 0x0125 }
            java.lang.String r6 = "reason"
            r5[r2] = r6     // Catch:{ all -> 0x0125 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0125 }
            r6.<init>()     // Catch:{ all -> 0x0125 }
            java.lang.String r7 = "Loaded missing "
            r6.append(r7)     // Catch:{ all -> 0x0125 }
            r6.append(r0)     // Catch:{ all -> 0x0125 }
            java.lang.String r0 = r6.toString()     // Catch:{ all -> 0x0125 }
            r5[r3] = r0     // Catch:{ all -> 0x0125 }
            r4[r3] = r5     // Catch:{ all -> 0x0125 }
            java.lang.Object[] r0 = new java.lang.Object[r1]     // Catch:{ all -> 0x0125 }
            java.lang.String r5 = "ext1"
            r0[r2] = r5     // Catch:{ all -> 0x0125 }
            r0[r3] = r11     // Catch:{ all -> 0x0125 }
            r4[r1] = r0     // Catch:{ all -> 0x0125 }
            r10.a((int) r12, (java.lang.Object[][]) r4)     // Catch:{ all -> 0x0125 }
        L_0x0123:
            monitor-exit(r10)
            return
        L_0x0125:
            r11 = move-exception
            monitor-exit(r10)
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.ProgRvManager.a(com.ironsource.mediationsdk.ProgRvSmash, java.lang.String):void");
    }

    /* access modifiers changed from: private */
    public void b(String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "ProgRvManager: " + str, 0);
    }

    private void b(int i2, Object[][] objArr) {
        a(i2, objArr, true, true);
    }

    public void a(ProgRvSmash progRvSmash) {
        synchronized (this) {
            this.k++;
            c(progRvSmash, "onRewardedVideoAdOpened");
            RVListenerWrapper.c().b();
            if (this.f) {
                AuctionResponseItem auctionResponseItem = this.c.get(progRvSmash.k());
                if (auctionResponseItem != null) {
                    this.g.a(auctionResponseItem);
                } else {
                    String k2 = progRvSmash != null ? progRvSmash.k() : "Smash is null";
                    a("onRewardedVideoAdOpened showing instance " + k2 + " missing from waterfall");
                    StringBuilder sb = new StringBuilder();
                    sb.append("Showing missing ");
                    sb.append(this.p);
                    a(81317, new Object[][]{new Object[]{"errorCode", 1011}, new Object[]{"reason", sb.toString()}, new Object[]{"ext1", k2}});
                }
            }
        }
    }

    public void a(IronSourceError ironSourceError, ProgRvSmash progRvSmash) {
        synchronized (this) {
            c(progRvSmash, "onRewardedVideoAdShowFailed error=" + ironSourceError.b());
            b(1113, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}});
            RVListenerWrapper.c().a(ironSourceError);
            if (this.p != RV_MEDIATION_STATE.RV_STATE_READY_TO_SHOW) {
                a(false);
            }
            this.e.c();
        }
    }

    public void a(ProgRvSmash progRvSmash, Placement placement) {
        c(progRvSmash, "onRewardedVideoAdRewarded");
        RVListenerWrapper.c().b(placement);
    }

    private void a(boolean z) {
        Boolean bool = this.o;
        if (bool == null || bool.booleanValue() != z) {
            this.o = Boolean.valueOf(z);
            long time = new Date().getTime() - this.m;
            this.m = new Date().getTime();
            if (z) {
                a(1111, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
            } else {
                a(1112, new Object[][]{new Object[]{"duration", Long.valueOf(time)}});
            }
            RVListenerWrapper.c().a(z);
        }
    }

    private void a(String str) {
        IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        c2.b(ironSourceTag, "ProgRvManager: " + str, 3);
    }

    /* access modifiers changed from: private */
    public void a(int i2) {
        a(i2, (Object[][]) null, false, false);
    }

    /* access modifiers changed from: private */
    public void a(int i2, Object[][] objArr) {
        a(i2, objArr, false, true);
    }

    private void a(int i2, Object[][] objArr, boolean z, boolean z2) {
        HashMap hashMap = new HashMap();
        hashMap.put("provider", "Mediation");
        hashMap.put("programmatic", 1);
        if (z2 && !TextUtils.isEmpty(this.j)) {
            hashMap.put("auctionId", this.j);
        }
        if (z && !TextUtils.isEmpty(this.i)) {
            hashMap.put("placement", this.i);
        }
        if (b(i2)) {
            RewardedVideoEventsManager.g().a((Map<String, Object>) hashMap, this.q, this.r);
        }
        hashMap.put("sessionDepth", Integer.valueOf(this.k));
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    hashMap.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e2) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "ProgRvManager: RV sendMediationEvent " + Log.getStackTraceString(e2), 3);
            }
        }
        RewardedVideoEventsManager.g().d(new EventData(i2, new JSONObject(hashMap)));
    }

    public synchronized void a() {
        b("onLoadTriggered: RV load was triggered in " + this.p + " state");
        a(0);
    }

    public void a(List<AuctionResponseItem> list, String str, int i2, long j2) {
        b("makeAuction(): success");
        this.j = str;
        this.q = i2;
        this.r = "";
        a(1302, new Object[][]{new Object[]{"duration", Long.valueOf(j2)}});
        a(list);
        d();
    }

    public void a(int i2, String str, int i3, String str2, long j2) {
        b("Auction failed | moving to fallback waterfall");
        this.q = i3;
        this.r = str2;
        if (TextUtils.isEmpty(str)) {
            c(1301, new Object[][]{new Object[]{"errorCode", Integer.valueOf(i2)}, new Object[]{"duration", Long.valueOf(j2)}});
        } else {
            c(1301, new Object[][]{new Object[]{"errorCode", Integer.valueOf(i2)}, new Object[]{"reason", str}, new Object[]{"duration", Long.valueOf(j2)}});
        }
        f();
        d();
    }
}
