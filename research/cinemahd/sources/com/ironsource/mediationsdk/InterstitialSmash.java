package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashApi;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialSmashApi;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONObject;

public class InterstitialSmash extends AbstractSmash implements InterstitialSmashListener, RewardedInterstitialListener, InterstitialSmashApi, RewardedInterstitialSmashApi {
    private JSONObject r;
    /* access modifiers changed from: private */
    public InterstitialManagerListener s;
    private RewardedInterstitialManagerListener t;
    /* access modifiers changed from: private */
    public long u;
    private int v;

    InterstitialSmash(ProviderSettings providerSettings, int i) {
        super(providerSettings);
        this.r = providerSettings.f();
        this.m = this.r.optInt("maxAdsPerIteration", 99);
        this.n = this.r.optInt("maxAdsPerSession", 99);
        this.o = this.r.optInt("maxAdsPerDay", 99);
        this.f = providerSettings.m();
        this.g = providerSettings.l();
        this.v = i;
    }

    public boolean E() {
        if (this.b == null) {
            return false;
        }
        IronSourceLoggerManager ironSourceLoggerManager = this.q;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        ironSourceLoggerManager.b(ironSourceTag, p() + ":isInterstitialReady()", 1);
        return this.b.isInterstitialReady(this.r);
    }

    public void F() {
        I();
        if (this.b != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.q;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.b(ironSourceTag, p() + ":loadInterstitial()", 1);
            this.u = new Date().getTime();
            this.b.loadInterstitial(this.r, this);
        }
    }

    public void G() {
        if (this.b != null) {
            IronSourceLoggerManager ironSourceLoggerManager = this.q;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.b(ironSourceTag, p() + ":showInterstitial()", 1);
            B();
            this.b.showInterstitial(this.r, this);
        }
    }

    /* access modifiers changed from: package-private */
    public void H() {
        try {
            C();
            this.k = new Timer();
            this.k.schedule(new TimerTask() {
                public void run() {
                    cancel();
                    InterstitialSmash interstitialSmash = InterstitialSmash.this;
                    if (interstitialSmash.f4582a == AbstractSmash.MEDIATION_STATE.INIT_PENDING && interstitialSmash.s != null) {
                        InterstitialSmash.this.a(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
                        InterstitialSmash.this.s.b(ErrorBuilder.a("Timeout", "Interstitial"), InterstitialSmash.this);
                    }
                }
            }, (long) (this.v * 1000));
        } catch (Exception e) {
            a("startInitTimer", e.getLocalizedMessage());
        }
    }

    /* access modifiers changed from: package-private */
    public void I() {
        try {
            D();
            this.l = new Timer();
            this.l.schedule(new TimerTask() {
                public void run() {
                    cancel();
                    InterstitialSmash interstitialSmash = InterstitialSmash.this;
                    if (interstitialSmash.f4582a == AbstractSmash.MEDIATION_STATE.LOAD_PENDING && interstitialSmash.s != null) {
                        InterstitialSmash.this.a(AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE);
                        InterstitialSmash.this.s.a(ErrorBuilder.c("Timeout"), InterstitialSmash.this, new Date().getTime() - InterstitialSmash.this.u);
                    }
                }
            }, (long) (this.v * 1000));
        } catch (Exception e) {
            a("startLoadTimer", e.getLocalizedMessage());
        }
    }

    public void c() {
        InterstitialManagerListener interstitialManagerListener = this.s;
        if (interstitialManagerListener != null) {
            interstitialManagerListener.g(this);
        }
    }

    public void d() {
        InterstitialManagerListener interstitialManagerListener = this.s;
        if (interstitialManagerListener != null) {
            interstitialManagerListener.b(this);
        }
    }

    public void e(IronSourceError ironSourceError) {
        C();
        if (this.f4582a == AbstractSmash.MEDIATION_STATE.INIT_PENDING) {
            a(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            InterstitialManagerListener interstitialManagerListener = this.s;
            if (interstitialManagerListener != null) {
                interstitialManagerListener.b(ironSourceError, this);
            }
        }
    }

    public void k() {
        RewardedInterstitialManagerListener rewardedInterstitialManagerListener = this.t;
        if (rewardedInterstitialManagerListener != null) {
            rewardedInterstitialManagerListener.d(this);
        }
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.j = 0;
        a(AbstractSmash.MEDIATION_STATE.INITIATED);
    }

    /* access modifiers changed from: protected */
    public String n() {
        return "interstitial";
    }

    public void onInterstitialAdClicked() {
        InterstitialManagerListener interstitialManagerListener = this.s;
        if (interstitialManagerListener != null) {
            interstitialManagerListener.c(this);
        }
    }

    public void onInterstitialInitSuccess() {
        C();
        if (this.f4582a == AbstractSmash.MEDIATION_STATE.INIT_PENDING) {
            a(AbstractSmash.MEDIATION_STATE.INITIATED);
            InterstitialManagerListener interstitialManagerListener = this.s;
            if (interstitialManagerListener != null) {
                interstitialManagerListener.a(this);
            }
        }
    }

    public void a(Activity activity, String str, String str2) {
        H();
        AbstractAdapter abstractAdapter = this.b;
        if (abstractAdapter != null) {
            abstractAdapter.addInterstitialListener(this);
            if (this.t != null) {
                this.b.setRewardedInterstitialListener(this);
            }
            IronSourceLoggerManager ironSourceLoggerManager = this.q;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            ironSourceLoggerManager.b(ironSourceTag, p() + ":initInterstitial()", 1);
            this.b.initInterstitial(activity, str, str2, this.r, this);
        }
    }

    public void b() {
        InterstitialManagerListener interstitialManagerListener = this.s;
        if (interstitialManagerListener != null) {
            interstitialManagerListener.f(this);
        }
    }

    public void b(IronSourceError ironSourceError) {
        InterstitialManagerListener interstitialManagerListener = this.s;
        if (interstitialManagerListener != null) {
            interstitialManagerListener.a(ironSourceError, this);
        }
    }

    public void e() {
        InterstitialManagerListener interstitialManagerListener = this.s;
        if (interstitialManagerListener != null) {
            interstitialManagerListener.e(this);
        }
    }

    public void a(InterstitialManagerListener interstitialManagerListener) {
        this.s = interstitialManagerListener;
    }

    public void a(RewardedInterstitialManagerListener rewardedInterstitialManagerListener) {
        this.t = rewardedInterstitialManagerListener;
    }

    public void a() {
        D();
        if (this.f4582a == AbstractSmash.MEDIATION_STATE.LOAD_PENDING && this.s != null) {
            this.s.a(this, new Date().getTime() - this.u);
        }
    }

    public void a(IronSourceError ironSourceError) {
        D();
        if (this.f4582a == AbstractSmash.MEDIATION_STATE.LOAD_PENDING && this.s != null) {
            this.s.a(ironSourceError, this, new Date().getTime() - this.u);
        }
    }
}
