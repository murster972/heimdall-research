package com.ironsource.mediationsdk;

import java.util.List;

interface AuctionEventListener {
    void a(int i, String str, int i2, String str2, long j);

    void a(List<AuctionResponseItem> list, String str, int i, long j);
}
