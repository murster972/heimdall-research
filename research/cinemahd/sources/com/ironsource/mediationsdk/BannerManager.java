package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.BannerManagerListener;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class BannerManager implements BannerManagerListener {

    /* renamed from: a  reason: collision with root package name */
    private BannerSmash f4591a;
    private IronSourceBannerLayout b;
    private BannerPlacement c;
    private BANNER_STATE d = BANNER_STATE.NOT_INITIATED;
    private IronSourceLoggerManager e = IronSourceLoggerManager.c();
    private String f;
    private String g;
    private Activity h;
    private final CopyOnWriteArrayList<BannerSmash> i = new CopyOnWriteArrayList<>();
    private long j;
    private Timer k;
    private Boolean l = true;

    private enum BANNER_STATE {
        NOT_INITIATED,
        READY_TO_LOAD,
        FIRST_LOAD_IN_PROGRESS,
        LOAD_IN_PROGRESS,
        RELOAD_IN_PROGRESS
    }

    public BannerManager(List<ProviderSettings> list, Activity activity, String str, String str2, long j2, int i2, int i3) {
        new AtomicBoolean();
        new AtomicBoolean();
        this.f = str;
        this.g = str2;
        this.h = activity;
        this.j = (long) i2;
        BannerCallbackThrottler.b().a(i3);
        for (int i4 = 0; i4 < list.size(); i4++) {
            ProviderSettings providerSettings = list.get(i4);
            AbstractAdapter a2 = AdapterRepository.a().a(providerSettings, providerSettings.d(), this.h);
            if (a2 == null || !AdaptersCompatibilityHandler.a().c(a2)) {
                a(providerSettings.g() + " can't load adapter or wrong version");
            } else {
                this.i.add(new BannerSmash(this, providerSettings, a2, j2, i4 + 1));
            }
        }
        this.c = null;
        a(BANNER_STATE.READY_TO_LOAD);
    }

    private void b(BannerSmash bannerSmash, View view, FrameLayout.LayoutParams layoutParams) {
        this.f4591a = bannerSmash;
        this.b.a(view, layoutParams);
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.d != BANNER_STATE.RELOAD_IN_PROGRESS) {
            a("onReloadTimer wrong state=" + this.d.name());
        } else if (this.l.booleanValue()) {
            a(3011);
            a(3012, this.f4591a);
            this.f4591a.i();
        } else {
            a(3200, new Object[][]{new Object[]{"errorCode", 614}});
            e();
        }
    }

    private void d() {
        synchronized (this.i) {
            Iterator<BannerSmash> it2 = this.i.iterator();
            while (it2.hasNext()) {
                it2.next().a(true);
            }
        }
    }

    private void e() {
        try {
            f();
            this.k = new Timer();
            this.k.schedule(new TimerTask() {
                public void run() {
                    BannerManager.this.c();
                }
            }, this.j * 1000);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    private void f() {
        Timer timer = this.k;
        if (timer != null) {
            timer.cancel();
            this.k = null;
        }
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:698)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processIf(RegionMaker.java:693)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:123)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    public synchronized void a(com.ironsource.mediationsdk.IronSourceBannerLayout r9, com.ironsource.mediationsdk.model.BannerPlacement r10) {
        /*
            r8 = this;
            monitor-enter(r8)
            r0 = 3111(0xc27, float:4.36E-42)
            r1 = 3
            r2 = 2
            r3 = 0
            r4 = 1
            if (r9 == 0) goto L_0x00e7
            boolean r5 = r9.b()     // Catch:{ Exception -> 0x0103 }
            if (r5 == 0) goto L_0x0011
            goto L_0x00e7
        L_0x0011:
            if (r10 == 0) goto L_0x00cd
            java.lang.String r5 = r10.c()     // Catch:{ Exception -> 0x0103 }
            boolean r5 = android.text.TextUtils.isEmpty(r5)     // Catch:{ Exception -> 0x0103 }
            if (r5 == 0) goto L_0x001f
            goto L_0x00cd
        L_0x001f:
            com.ironsource.mediationsdk.BannerManager$BANNER_STATE r5 = r8.d     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.BannerManager$BANNER_STATE r6 = com.ironsource.mediationsdk.BannerManager.BANNER_STATE.READY_TO_LOAD     // Catch:{ Exception -> 0x0103 }
            if (r5 != r6) goto L_0x00c2
            com.ironsource.mediationsdk.BannerCallbackThrottler r5 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()     // Catch:{ Exception -> 0x0103 }
            boolean r5 = r5.a()     // Catch:{ Exception -> 0x0103 }
            if (r5 == 0) goto L_0x0031
            goto L_0x00c2
        L_0x0031:
            com.ironsource.mediationsdk.BannerManager$BANNER_STATE r1 = com.ironsource.mediationsdk.BannerManager.BANNER_STATE.FIRST_LOAD_IN_PROGRESS     // Catch:{ Exception -> 0x0103 }
            r8.a((com.ironsource.mediationsdk.BannerManager.BANNER_STATE) r1)     // Catch:{ Exception -> 0x0103 }
            r8.b = r9     // Catch:{ Exception -> 0x0103 }
            r8.c = r10     // Catch:{ Exception -> 0x0103 }
            r1 = 3001(0xbb9, float:4.205E-42)
            r8.a((int) r1)     // Catch:{ Exception -> 0x0103 }
            android.app.Activity r1 = r8.h     // Catch:{ Exception -> 0x0103 }
            java.lang.String r5 = r10.c()     // Catch:{ Exception -> 0x0103 }
            boolean r1 = com.ironsource.mediationsdk.utils.CappingManager.c((android.content.Context) r1, (java.lang.String) r5)     // Catch:{ Exception -> 0x0103 }
            if (r1 == 0) goto L_0x008d
            com.ironsource.mediationsdk.BannerCallbackThrottler r1 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.logger.IronSourceError r5 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ Exception -> 0x0103 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0103 }
            r6.<init>()     // Catch:{ Exception -> 0x0103 }
            java.lang.String r7 = "placement "
            r6.append(r7)     // Catch:{ Exception -> 0x0103 }
            java.lang.String r10 = r10.c()     // Catch:{ Exception -> 0x0103 }
            r6.append(r10)     // Catch:{ Exception -> 0x0103 }
            java.lang.String r10 = " is capped"
            r6.append(r10)     // Catch:{ Exception -> 0x0103 }
            java.lang.String r10 = r6.toString()     // Catch:{ Exception -> 0x0103 }
            r6 = 604(0x25c, float:8.46E-43)
            r5.<init>(r6, r10)     // Catch:{ Exception -> 0x0103 }
            r1.a(r9, r5)     // Catch:{ Exception -> 0x0103 }
            java.lang.Object[][] r10 = new java.lang.Object[r4][]     // Catch:{ Exception -> 0x0103 }
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0103 }
            java.lang.String r5 = "errorCode"
            r1[r3] = r5     // Catch:{ Exception -> 0x0103 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0103 }
            r1[r4] = r5     // Catch:{ Exception -> 0x0103 }
            r10[r3] = r1     // Catch:{ Exception -> 0x0103 }
            r8.a((int) r0, (java.lang.Object[][]) r10)     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.BannerManager$BANNER_STATE r10 = com.ironsource.mediationsdk.BannerManager.BANNER_STATE.READY_TO_LOAD     // Catch:{ Exception -> 0x0103 }
            r8.a((com.ironsource.mediationsdk.BannerManager.BANNER_STATE) r10)     // Catch:{ Exception -> 0x0103 }
            monitor-exit(r8)
            return
        L_0x008d:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.BannerSmash> r10 = r8.i     // Catch:{ Exception -> 0x0103 }
            monitor-enter(r10)     // Catch:{ Exception -> 0x0103 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.BannerSmash> r1 = r8.i     // Catch:{ all -> 0x00bf }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x00bf }
        L_0x0096:
            boolean r5 = r1.hasNext()     // Catch:{ all -> 0x00bf }
            if (r5 == 0) goto L_0x00a6
            java.lang.Object r5 = r1.next()     // Catch:{ all -> 0x00bf }
            com.ironsource.mediationsdk.BannerSmash r5 = (com.ironsource.mediationsdk.BannerSmash) r5     // Catch:{ all -> 0x00bf }
            r5.a((boolean) r4)     // Catch:{ all -> 0x00bf }
            goto L_0x0096
        L_0x00a6:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.BannerSmash> r1 = r8.i     // Catch:{ all -> 0x00bf }
            java.lang.Object r1 = r1.get(r3)     // Catch:{ all -> 0x00bf }
            com.ironsource.mediationsdk.BannerSmash r1 = (com.ironsource.mediationsdk.BannerSmash) r1     // Catch:{ all -> 0x00bf }
            r5 = 3002(0xbba, float:4.207E-42)
            r8.a((int) r5, (com.ironsource.mediationsdk.BannerSmash) r1)     // Catch:{ all -> 0x00bf }
            android.app.Activity r5 = r8.h     // Catch:{ all -> 0x00bf }
            java.lang.String r6 = r8.f     // Catch:{ all -> 0x00bf }
            java.lang.String r7 = r8.g     // Catch:{ all -> 0x00bf }
            r1.a(r9, r5, r6, r7)     // Catch:{ all -> 0x00bf }
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            goto L_0x014d
        L_0x00bf:
            r1 = move-exception
            monitor-exit(r10)     // Catch:{ all -> 0x00bf }
            throw r1     // Catch:{ Exception -> 0x0103 }
        L_0x00c2:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r10 = r8.e     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r5 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0103 }
            java.lang.String r6 = "A banner is already loaded"
            r10.b(r5, r6, r1)     // Catch:{ Exception -> 0x0103 }
            monitor-exit(r8)
            return
        L_0x00cd:
            java.lang.String r5 = "can't load banner - %s"
            java.lang.Object[] r6 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0103 }
            if (r10 != 0) goto L_0x00d6
            java.lang.String r10 = "placement is null"
            goto L_0x00d8
        L_0x00d6:
            java.lang.String r10 = "placement name is empty"
        L_0x00d8:
            r6[r3] = r10     // Catch:{ Exception -> 0x0103 }
            java.lang.String r10 = java.lang.String.format(r5, r6)     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r8.e     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0103 }
            r5.b(r6, r10, r1)     // Catch:{ Exception -> 0x0103 }
            monitor-exit(r8)
            return
        L_0x00e7:
            java.lang.String r10 = "can't load banner - %s"
            java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0103 }
            if (r9 != 0) goto L_0x00f0
            java.lang.String r6 = "banner is null"
            goto L_0x00f2
        L_0x00f0:
            java.lang.String r6 = "banner is destroyed"
        L_0x00f2:
            r5[r3] = r6     // Catch:{ Exception -> 0x0103 }
            java.lang.String r10 = java.lang.String.format(r10, r5)     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r5 = r8.e     // Catch:{ Exception -> 0x0103 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r6 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ Exception -> 0x0103 }
            r5.b(r6, r10, r1)     // Catch:{ Exception -> 0x0103 }
            monitor-exit(r8)
            return
        L_0x0101:
            r9 = move-exception
            goto L_0x014f
        L_0x0103:
            r10 = move-exception
            com.ironsource.mediationsdk.BannerCallbackThrottler r1 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()     // Catch:{ all -> 0x0101 }
            com.ironsource.mediationsdk.logger.IronSourceError r5 = new com.ironsource.mediationsdk.logger.IronSourceError     // Catch:{ all -> 0x0101 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0101 }
            r6.<init>()     // Catch:{ all -> 0x0101 }
            java.lang.String r7 = "loadBanner() failed "
            r6.append(r7)     // Catch:{ all -> 0x0101 }
            java.lang.String r7 = r10.getMessage()     // Catch:{ all -> 0x0101 }
            r6.append(r7)     // Catch:{ all -> 0x0101 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0101 }
            r7 = 605(0x25d, float:8.48E-43)
            r5.<init>(r7, r6)     // Catch:{ all -> 0x0101 }
            r1.a(r9, r5)     // Catch:{ all -> 0x0101 }
            java.lang.Object[][] r9 = new java.lang.Object[r2][]     // Catch:{ all -> 0x0101 }
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ all -> 0x0101 }
            java.lang.String r5 = "errorCode"
            r1[r3] = r5     // Catch:{ all -> 0x0101 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r7)     // Catch:{ all -> 0x0101 }
            r1[r4] = r5     // Catch:{ all -> 0x0101 }
            r9[r3] = r1     // Catch:{ all -> 0x0101 }
            java.lang.Object[] r1 = new java.lang.Object[r2]     // Catch:{ all -> 0x0101 }
            java.lang.String r2 = "reason"
            r1[r3] = r2     // Catch:{ all -> 0x0101 }
            java.lang.String r10 = r10.getMessage()     // Catch:{ all -> 0x0101 }
            r1[r4] = r10     // Catch:{ all -> 0x0101 }
            r9[r4] = r1     // Catch:{ all -> 0x0101 }
            r8.a((int) r0, (java.lang.Object[][]) r9)     // Catch:{ all -> 0x0101 }
            com.ironsource.mediationsdk.BannerManager$BANNER_STATE r9 = com.ironsource.mediationsdk.BannerManager.BANNER_STATE.READY_TO_LOAD     // Catch:{ all -> 0x0101 }
            r8.a((com.ironsource.mediationsdk.BannerManager.BANNER_STATE) r9)     // Catch:{ all -> 0x0101 }
        L_0x014d:
            monitor-exit(r8)
            return
        L_0x014f:
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.BannerManager.a(com.ironsource.mediationsdk.IronSourceBannerLayout, com.ironsource.mediationsdk.model.BannerPlacement):void");
    }

    public void b(IronSourceError ironSourceError, BannerSmash bannerSmash, boolean z) {
        a("onBannerAdReloadFailed " + ironSourceError.b(), bannerSmash);
        if (this.d != BANNER_STATE.RELOAD_IN_PROGRESS) {
            a("onBannerAdReloadFailed " + bannerSmash.e() + " wrong state=" + this.d.name());
            return;
        }
        if (z) {
            a(3307, bannerSmash);
        } else {
            a(3301, bannerSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}});
        }
        synchronized (this.i) {
            if (this.i.size() == 1) {
                a(3201);
                e();
                return;
            }
            a(BANNER_STATE.LOAD_IN_PROGRESS);
            d();
            b();
        }
    }

    public void b(BannerSmash bannerSmash) {
        Object[][] objArr;
        a("onBannerAdClicked", bannerSmash);
        if (a()) {
            this.b.c();
            objArr = null;
        } else {
            objArr = new Object[][]{new Object[]{"reason", "banner is destroyed"}};
        }
        a(3112, objArr);
        a(3008, bannerSmash, objArr);
    }

    private boolean b() {
        synchronized (this.i) {
            Iterator<BannerSmash> it2 = this.i.iterator();
            while (it2.hasNext()) {
                BannerSmash next = it2.next();
                if (next.h() && this.f4591a != next) {
                    if (this.d == BANNER_STATE.FIRST_LOAD_IN_PROGRESS) {
                        a(3002, next);
                    } else {
                        a(3012, next);
                    }
                    next.a(this.b, this.h, this.f, this.g);
                    return true;
                }
            }
            return false;
        }
    }

    public synchronized void a(IronSourceBannerLayout ironSourceBannerLayout) {
        if (ironSourceBannerLayout == null) {
            this.e.b(IronSourceLogger.IronSourceTag.API, "destroyBanner banner cannot be null", 3);
        } else if (ironSourceBannerLayout.b()) {
            this.e.b(IronSourceLogger.IronSourceTag.API, "Banner is already destroyed and can't be used anymore. Please create a new one using IronSource.createBanner API", 3);
        } else {
            a(3100);
            f();
            ironSourceBannerLayout.a();
            this.b = null;
            this.c = null;
            if (this.f4591a != null) {
                a(3305, this.f4591a);
                this.f4591a.b();
                this.f4591a = null;
            }
            a(BANNER_STATE.READY_TO_LOAD);
        }
    }

    public void b(Activity activity) {
        synchronized (this.i) {
            this.l = true;
            Iterator<BannerSmash> it2 = this.i.iterator();
            while (it2.hasNext()) {
                it2.next().b(activity);
            }
        }
    }

    private void a(String str) {
        IronSourceLoggerManager ironSourceLoggerManager = this.e;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.b(ironSourceTag, "BannerManager " + str, 0);
    }

    private void a(BANNER_STATE banner_state) {
        this.d = banner_state;
        a("state=" + banner_state.name());
    }

    private void a(String str, BannerSmash bannerSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.e;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, "BannerManager " + str + " " + bannerSmash.e(), 0);
    }

    public void a(BannerSmash bannerSmash, View view, FrameLayout.LayoutParams layoutParams) {
        a("onBannerAdLoaded", bannerSmash);
        BANNER_STATE banner_state = this.d;
        if (banner_state == BANNER_STATE.FIRST_LOAD_IN_PROGRESS) {
            a(3005, bannerSmash);
            b(bannerSmash, view, layoutParams);
            BannerPlacement bannerPlacement = this.c;
            String c2 = bannerPlacement != null ? bannerPlacement.c() : "";
            CappingManager.a((Context) this.h, c2);
            if (CappingManager.c((Context) this.h, c2)) {
                a(3400);
            }
            this.b.a(bannerSmash);
            a(3110);
            a(BANNER_STATE.RELOAD_IN_PROGRESS);
            e();
        } else if (banner_state == BANNER_STATE.LOAD_IN_PROGRESS) {
            a(3015, bannerSmash);
            b(bannerSmash, view, layoutParams);
            a(BANNER_STATE.RELOAD_IN_PROGRESS);
            e();
        }
    }

    public void a(IronSourceError ironSourceError, BannerSmash bannerSmash, boolean z) {
        a("onBannerAdLoadFailed " + ironSourceError.b(), bannerSmash);
        BANNER_STATE banner_state = this.d;
        if (banner_state == BANNER_STATE.FIRST_LOAD_IN_PROGRESS || banner_state == BANNER_STATE.LOAD_IN_PROGRESS) {
            if (z) {
                a(3306, bannerSmash);
            } else {
                a(3300, bannerSmash, new Object[][]{new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}});
            }
            if (!b()) {
                if (this.d == BANNER_STATE.FIRST_LOAD_IN_PROGRESS) {
                    BannerCallbackThrottler.b().a(this.b, new IronSourceError(606, "No ads to show"));
                    a(3111, new Object[][]{new Object[]{"errorCode", 606}});
                    a(BANNER_STATE.READY_TO_LOAD);
                    return;
                }
                a(3201);
                a(BANNER_STATE.RELOAD_IN_PROGRESS);
                e();
                return;
            }
            return;
        }
        a("onBannerAdLoadFailed " + bannerSmash.e() + " wrong state=" + this.d.name());
    }

    public void a(BannerSmash bannerSmash) {
        a("onBannerAdReloaded", bannerSmash);
        if (this.d != BANNER_STATE.RELOAD_IN_PROGRESS) {
            a("onBannerAdReloaded " + bannerSmash.e() + " wrong state=" + this.d.name());
            return;
        }
        IronSourceUtils.g("bannerReloadSucceeded");
        a(3015, bannerSmash);
        e();
    }

    private boolean a() {
        IronSourceBannerLayout ironSourceBannerLayout = this.b;
        return ironSourceBannerLayout != null && !ironSourceBannerLayout.b();
    }

    private void a(int i2) {
        a(i2, (Object[][]) null);
    }

    private void a(int i2, Object[][] objArr) {
        JSONObject a2 = IronSourceUtils.a(false);
        try {
            if (this.b != null) {
                a(a2, this.b.getSize());
            }
            if (this.c != null) {
                a2.put("placement", this.c.c());
            }
            if (objArr != null) {
                for (Object[] objArr2 : objArr) {
                    a2.put(objArr2[0].toString(), objArr2[1]);
                }
            }
        } catch (Exception e2) {
            this.e.b(IronSourceLogger.IronSourceTag.INTERNAL, "sendMediationEvent " + Log.getStackTraceString(e2), 3);
        }
        InterstitialEventsManager.g().d(new EventData(i2, a2));
    }

    private void a(int i2, BannerSmash bannerSmash) {
        a(i2, bannerSmash, (Object[][]) null);
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(org.json.JSONObject r8, com.ironsource.mediationsdk.ISBannerSize r9) {
        /*
            r7 = this;
            r0 = 3
            java.lang.String r1 = r9.a()     // Catch:{ Exception -> 0x0088 }
            r2 = -1
            int r3 = r1.hashCode()     // Catch:{ Exception -> 0x0088 }
            r4 = 4
            r5 = 2
            r6 = 1
            switch(r3) {
                case -387072689: goto L_0x0039;
                case 72205083: goto L_0x002f;
                case 79011241: goto L_0x0025;
                case 1951953708: goto L_0x001b;
                case 1999208305: goto L_0x0011;
                default: goto L_0x0010;
            }     // Catch:{ Exception -> 0x0088 }
        L_0x0010:
            goto L_0x0043
        L_0x0011:
            java.lang.String r3 = "CUSTOM"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0088 }
            if (r1 == 0) goto L_0x0043
            r1 = 4
            goto L_0x0044
        L_0x001b:
            java.lang.String r3 = "BANNER"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0088 }
            if (r1 == 0) goto L_0x0043
            r1 = 0
            goto L_0x0044
        L_0x0025:
            java.lang.String r3 = "SMART"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0088 }
            if (r1 == 0) goto L_0x0043
            r1 = 3
            goto L_0x0044
        L_0x002f:
            java.lang.String r3 = "LARGE"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0088 }
            if (r1 == 0) goto L_0x0043
            r1 = 1
            goto L_0x0044
        L_0x0039:
            java.lang.String r3 = "RECTANGLE"
            boolean r1 = r1.equals(r3)     // Catch:{ Exception -> 0x0088 }
            if (r1 == 0) goto L_0x0043
            r1 = 2
            goto L_0x0044
        L_0x0043:
            r1 = -1
        L_0x0044:
            java.lang.String r2 = "bannerAdSize"
            if (r1 == 0) goto L_0x0084
            if (r1 == r6) goto L_0x0080
            if (r1 == r5) goto L_0x007c
            if (r1 == r0) goto L_0x0077
            if (r1 == r4) goto L_0x0051
            goto L_0x00a5
        L_0x0051:
            r1 = 6
            r8.put(r2, r1)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r1 = "custom_banner_size"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0088 }
            r2.<init>()     // Catch:{ Exception -> 0x0088 }
            int r3 = r9.c()     // Catch:{ Exception -> 0x0088 }
            r2.append(r3)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r3 = "x"
            r2.append(r3)     // Catch:{ Exception -> 0x0088 }
            int r9 = r9.b()     // Catch:{ Exception -> 0x0088 }
            r2.append(r9)     // Catch:{ Exception -> 0x0088 }
            java.lang.String r9 = r2.toString()     // Catch:{ Exception -> 0x0088 }
            r8.put(r1, r9)     // Catch:{ Exception -> 0x0088 }
            goto L_0x00a5
        L_0x0077:
            r9 = 5
            r8.put(r2, r9)     // Catch:{ Exception -> 0x0088 }
            goto L_0x00a5
        L_0x007c:
            r8.put(r2, r0)     // Catch:{ Exception -> 0x0088 }
            goto L_0x00a5
        L_0x0080:
            r8.put(r2, r5)     // Catch:{ Exception -> 0x0088 }
            goto L_0x00a5
        L_0x0084:
            r8.put(r2, r6)     // Catch:{ Exception -> 0x0088 }
            goto L_0x00a5
        L_0x0088:
            r8 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r9 = r7.e
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "sendProviderEvent "
            r2.append(r3)
            java.lang.String r8 = android.util.Log.getStackTraceString(r8)
            r2.append(r8)
            java.lang.String r8 = r2.toString()
            r9.b(r1, r8, r0)
        L_0x00a5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.BannerManager.a(org.json.JSONObject, com.ironsource.mediationsdk.ISBannerSize):void");
    }

    private void a(int i2, BannerSmash bannerSmash, Object[][] objArr) {
        JSONObject a2 = IronSourceUtils.a(bannerSmash);
        try {
            if (this.b != null) {
                a(a2, this.b.getSize());
            }
            if (this.c != null) {
                a2.put("placement", this.c.c());
            }
            if (objArr != null) {
                for (Object[] objArr2 : objArr) {
                    a2.put(objArr2[0].toString(), objArr2[1]);
                }
            }
        } catch (Exception e2) {
            this.e.b(IronSourceLogger.IronSourceTag.INTERNAL, "sendProviderEvent " + Log.getStackTraceString(e2), 3);
        }
        InterstitialEventsManager.g().d(new EventData(i2, a2));
    }

    public void a(Activity activity) {
        synchronized (this.i) {
            this.l = false;
            Iterator<BannerSmash> it2 = this.i.iterator();
            while (it2.hasNext()) {
                it2.next().a(activity);
            }
        }
    }
}
