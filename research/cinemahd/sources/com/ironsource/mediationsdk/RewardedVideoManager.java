package com.ironsource.mediationsdk;

import android.text.TextUtils;
import android.util.Log;
import com.ironsource.environment.NetworkStateReceiver;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedVideoManagerListener;
import com.ironsource.mediationsdk.utils.DailyCappingListener;
import com.ironsource.mediationsdk.utils.DailyCappingManager;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import okhttp3.internal.ws.WebSocketProtocol;
import org.json.JSONException;
import org.json.JSONObject;

class RewardedVideoManager extends AbstractAdUnitManager implements RewardedVideoManagerListener, NetworkStateReceiver.NetworkStateReceiverListener, DailyCappingListener {
    private final String p = RewardedVideoManager.class.getSimpleName();
    private ListenersWrapper q;
    private boolean r = false;
    private Placement s;
    private Timer t = null;
    private int u;
    private boolean v = false;
    private long w = new Date().getTime();

    RewardedVideoManager() {
        Arrays.asList(new AbstractSmash.MEDIATION_STATE[]{AbstractSmash.MEDIATION_STATE.INIT_FAILED, AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION, AbstractSmash.MEDIATION_STATE.EXHAUSTED, AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY});
        this.f4581a = new DailyCappingManager("rewarded_video", this);
    }

    private synchronized AbstractAdapter f(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
        ironSourceLoggerManager.b(ironSourceTag, this.p + ":startAdapter(" + rewardedVideoSmash.p() + ")", 1);
        AbstractAdapter a2 = AdapterRepository.a().a(rewardedVideoSmash.c, rewardedVideoSmash.c.k(), this.f);
        if (a2 == null) {
            IronSourceLoggerManager ironSourceLoggerManager2 = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager2.b(ironSourceTag2, rewardedVideoSmash.p() + " is configured in IronSource's platform, but the adapter is not integrated", 2);
            return null;
        }
        rewardedVideoSmash.a(a2);
        rewardedVideoSmash.a(AbstractSmash.MEDIATION_STATE.INITIATED);
        c(rewardedVideoSmash);
        a(1001, (AbstractSmash) rewardedVideoSmash, (Object[][]) null);
        try {
            rewardedVideoSmash.a(this.f, this.h, this.g);
            return a2;
        } catch (Throwable th) {
            IronSourceLoggerManager ironSourceLoggerManager3 = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager3.a(ironSourceTag3, this.p + "failed to init adapter: " + rewardedVideoSmash.u() + "v", th);
            rewardedVideoSmash.a(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            return null;
        }
    }

    private synchronized void h() {
        if (n()) {
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "Reset Iteration", 0);
            Iterator<AbstractSmash> it2 = this.c.iterator();
            boolean z = false;
            while (it2.hasNext()) {
                AbstractSmash next = it2.next();
                if (next.t() == AbstractSmash.MEDIATION_STATE.EXHAUSTED) {
                    next.l();
                }
                if (next.t() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                    z = true;
                }
            }
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "End of Reset Iteration", 0);
            if (c(z)) {
                this.q.a(this.k.booleanValue());
            }
        }
    }

    private String i() {
        Placement placement = this.s;
        if (placement == null) {
            return "";
        }
        return placement.c();
    }

    private synchronized boolean j() {
        boolean z;
        z = false;
        Iterator<AbstractSmash> it2 = this.c.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (it2.next().t() == AbstractSmash.MEDIATION_STATE.AVAILABLE) {
                    z = true;
                    break;
                }
            } else {
                break;
            }
        }
        return z;
    }

    private synchronized boolean k() {
        boolean z;
        Iterator<AbstractSmash> it2 = this.c.iterator();
        z = false;
        int i = 0;
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (next.t() == AbstractSmash.MEDIATION_STATE.INIT_FAILED || next.t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY || next.t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION || next.t() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE || next.t() == AbstractSmash.MEDIATION_STATE.EXHAUSTED) {
                i++;
            }
        }
        if (this.c.size() == i) {
            z = true;
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean l() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r0 = r4.c     // Catch:{ all -> 0x0041 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0041 }
        L_0x0007:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0041 }
            if (r1 == 0) goto L_0x003e
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash r1 = (com.ironsource.mediationsdk.AbstractSmash) r1     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INIT_PENDING     // Catch:{ all -> 0x0041 }
            if (r2 == r3) goto L_0x003b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = r1.t()     // Catch:{ all -> 0x0041 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.LOAD_PENDING     // Catch:{ all -> 0x0041 }
            if (r1 != r2) goto L_0x0007
        L_0x003b:
            r0 = 1
            monitor-exit(r4)
            return r0
        L_0x003e:
            r0 = 0
            monitor-exit(r4)
            return r0
        L_0x0041:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.l():boolean");
    }

    private synchronized boolean m() {
        if (d() == null) {
            return false;
        }
        return ((RewardedVideoSmash) d()).F();
    }

    /* JADX WARNING: Removed duplicated region for block: B:5:0x000d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private synchronized boolean n() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r0 = r4.c     // Catch:{ all -> 0x0031 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0031 }
        L_0x0007:
            boolean r1 = r0.hasNext()     // Catch:{ all -> 0x0031 }
            if (r1 == 0) goto L_0x002e
            java.lang.Object r1 = r0.next()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash r1 = (com.ironsource.mediationsdk.AbstractSmash) r1     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_INITIATED     // Catch:{ all -> 0x0031 }
            if (r2 == r3) goto L_0x002b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = r1.t()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.INITIATED     // Catch:{ all -> 0x0031 }
            if (r2 == r3) goto L_0x002b
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = r1.t()     // Catch:{ all -> 0x0031 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r2 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.AVAILABLE     // Catch:{ all -> 0x0031 }
            if (r1 != r2) goto L_0x0007
        L_0x002b:
            r0 = 0
            monitor-exit(r4)
            return r0
        L_0x002e:
            r0 = 1
            monitor-exit(r4)
            return r0
        L_0x0031:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.n():boolean");
    }

    private AbstractAdapter o() {
        AbstractAdapter abstractAdapter = null;
        int i = 0;
        for (int i2 = 0; i2 < this.c.size() && abstractAdapter == null; i2++) {
            if (this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.AVAILABLE || this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.INITIATED) {
                i++;
                if (i >= this.b) {
                    break;
                }
            } else if (this.c.get(i2).t() == AbstractSmash.MEDIATION_STATE.NOT_INITIATED && (abstractAdapter = f((RewardedVideoSmash) this.c.get(i2))) == null) {
                this.c.get(i2).a(AbstractSmash.MEDIATION_STATE.INIT_FAILED);
            }
        }
        return abstractAdapter;
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0092, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0094, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void p() {
        /*
            r7 = this;
            monitor-enter(r7)
            android.app.Activity r0 = r7.f     // Catch:{ all -> 0x0095 }
            boolean r0 = com.ironsource.mediationsdk.utils.IronSourceUtils.c((android.content.Context) r0)     // Catch:{ all -> 0x0095 }
            if (r0 == 0) goto L_0x0093
            java.lang.Boolean r0 = r7.k     // Catch:{ all -> 0x0095 }
            if (r0 != 0) goto L_0x000f
            goto L_0x0093
        L_0x000f:
            java.lang.Boolean r0 = r7.k     // Catch:{ all -> 0x0095 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0095 }
            if (r0 != 0) goto L_0x0091
            r0 = 102(0x66, float:1.43E-43)
            r7.c((int) r0)     // Catch:{ all -> 0x0095 }
            r0 = 1000(0x3e8, float:1.401E-42)
            r7.c((int) r0)     // Catch:{ all -> 0x0095 }
            r0 = 1
            r7.v = r0     // Catch:{ all -> 0x0095 }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r1 = r7.c     // Catch:{ all -> 0x0095 }
            java.util.Iterator r1 = r1.iterator()     // Catch:{ all -> 0x0095 }
        L_0x002a:
            boolean r2 = r1.hasNext()     // Catch:{ all -> 0x0095 }
            if (r2 == 0) goto L_0x0091
            java.lang.Object r2 = r1.next()     // Catch:{ all -> 0x0095 }
            com.ironsource.mediationsdk.AbstractSmash r2 = (com.ironsource.mediationsdk.AbstractSmash) r2     // Catch:{ all -> 0x0095 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r3 = r2.t()     // Catch:{ all -> 0x0095 }
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r4 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE     // Catch:{ all -> 0x0095 }
            if (r3 != r4) goto L_0x002a
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r3 = r7.i     // Catch:{ all -> 0x006c }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r4 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.INTERNAL     // Catch:{ all -> 0x006c }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x006c }
            r5.<init>()     // Catch:{ all -> 0x006c }
            java.lang.String r6 = "Fetch from timer: "
            r5.append(r6)     // Catch:{ all -> 0x006c }
            java.lang.String r6 = r2.p()     // Catch:{ all -> 0x006c }
            r5.append(r6)     // Catch:{ all -> 0x006c }
            java.lang.String r6 = ":reload smash"
            r5.append(r6)     // Catch:{ all -> 0x006c }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x006c }
            r3.b(r4, r5, r0)     // Catch:{ all -> 0x006c }
            r3 = 1001(0x3e9, float:1.403E-42)
            r4 = 0
            r7.a((int) r3, (com.ironsource.mediationsdk.AbstractSmash) r2, (java.lang.Object[][]) r4)     // Catch:{ all -> 0x006c }
            r3 = r2
            com.ironsource.mediationsdk.RewardedVideoSmash r3 = (com.ironsource.mediationsdk.RewardedVideoSmash) r3     // Catch:{ all -> 0x006c }
            r3.E()     // Catch:{ all -> 0x006c }
            goto L_0x002a
        L_0x006c:
            r3 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r4 = r7.i     // Catch:{ all -> 0x0095 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r5 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.NATIVE     // Catch:{ all -> 0x0095 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x0095 }
            r6.<init>()     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = r2.p()     // Catch:{ all -> 0x0095 }
            r6.append(r2)     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = " Failed to call fetchVideo(), "
            r6.append(r2)     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = r3.getLocalizedMessage()     // Catch:{ all -> 0x0095 }
            r6.append(r2)     // Catch:{ all -> 0x0095 }
            java.lang.String r2 = r6.toString()     // Catch:{ all -> 0x0095 }
            r4.b(r5, r2, r0)     // Catch:{ all -> 0x0095 }
            goto L_0x002a
        L_0x0091:
            monitor-exit(r7)
            return
        L_0x0093:
            monitor-exit(r7)
            return
        L_0x0095:
            r0 = move-exception
            monitor-exit(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.p():void");
    }

    private synchronized void q() {
        if (d() != null && !this.l) {
            this.l = true;
            if (f((RewardedVideoSmash) d()) == null) {
                this.q.a(this.k.booleanValue());
            }
        } else if (!m()) {
            this.q.a(this.k.booleanValue());
        } else if (c(true)) {
            this.q.a(this.k.booleanValue());
        }
    }

    private void r() {
        for (int i = 0; i < this.c.size(); i++) {
            String i2 = this.c.get(i).c.i();
            if (i2.equalsIgnoreCase("IronSource") || i2.equalsIgnoreCase("SupersonicAds")) {
                AdapterRepository.a().a(this.c.get(i).c, this.c.get(i).c.k(), this.f);
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void s() {
        if (this.u <= 0) {
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "load interval is not set, ignoring", 1);
            return;
        }
        Timer timer = this.t;
        if (timer != null) {
            timer.cancel();
        }
        this.t = new Timer();
        this.t.schedule(new TimerTask() {
            public void run() {
                cancel();
                RewardedVideoManager.this.p();
                RewardedVideoManager.this.s();
            }
        }, (long) (this.u * 1000));
    }

    private void t() {
        if (g()) {
            c(1000);
            a(1003, new Object[][]{new Object[]{"duration", 0}});
            this.v = false;
        } else if (l()) {
            c(1000);
            this.v = true;
            this.w = new Date().getTime();
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
    }

    public void c(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, rewardedVideoSmash.p() + ":onRewardedVideoAdOpened()", 1);
        a((int) WebSocketProtocol.CLOSE_NO_STATUS_CODE, (AbstractSmash) rewardedVideoSmash, new Object[][]{new Object[]{"placement", i()}});
        this.q.onRewardedVideoAdOpened();
    }

    public void d(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, rewardedVideoSmash.p() + ":onRewardedVideoAdVisible()", 1);
        if (this.s != null) {
            a(1206, (AbstractSmash) rewardedVideoSmash, new Object[][]{new Object[]{"placement", i()}});
            return;
        }
        this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
    }

    public void e(RewardedVideoSmash rewardedVideoSmash) {
        boolean z;
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, rewardedVideoSmash.p() + ":onRewardedVideoAdClosed()", 1);
        f();
        Iterator<AbstractSmash> it2 = this.c.iterator();
        while (true) {
            if (!it2.hasNext()) {
                z = false;
                break;
            }
            AbstractSmash next = it2.next();
            if (!next.equals(rewardedVideoSmash) && ((RewardedVideoSmash) next).F()) {
                z = true;
                break;
            }
        }
        Object[][] objArr = new Object[2][];
        objArr[0] = new Object[]{"placement", i()};
        Object[] objArr2 = new Object[2];
        objArr2[0] = "ext1";
        StringBuilder sb = new StringBuilder();
        sb.append("otherRVAvailable = ");
        sb.append(z ? "true" : "false");
        objArr2[1] = sb.toString();
        objArr[1] = objArr2;
        a(1203, (AbstractSmash) rewardedVideoSmash, objArr);
        if (!rewardedVideoSmash.y() && !this.f4581a.c((AbstractSmash) rewardedVideoSmash)) {
            a(1001, (AbstractSmash) rewardedVideoSmash, (Object[][]) null);
        }
        t();
        this.q.onRewardedVideoAdClosed();
        Iterator<AbstractSmash> it3 = this.c.iterator();
        while (it3.hasNext()) {
            AbstractSmash next2 = it3.next();
            IronSourceLoggerManager ironSourceLoggerManager2 = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager2.b(ironSourceTag2, "Fetch on ad closed, iterating on: " + next2.p() + ", Status: " + next2.t(), 0);
            if (next2.t() == AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE) {
                try {
                    if (!next2.p().equals(rewardedVideoSmash.p())) {
                        IronSourceLoggerManager ironSourceLoggerManager3 = this.i;
                        IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.INTERNAL;
                        ironSourceLoggerManager3.b(ironSourceTag3, next2.p() + ":reload smash", 1);
                        ((RewardedVideoSmash) next2).E();
                        a(1001, next2, (Object[][]) null);
                    }
                } catch (Throwable th) {
                    IronSourceLoggerManager ironSourceLoggerManager4 = this.i;
                    IronSourceLogger.IronSourceTag ironSourceTag4 = IronSourceLogger.IronSourceTag.NATIVE;
                    ironSourceLoggerManager4.b(ironSourceTag4, next2.p() + " Failed to call fetchVideo(), " + th.getLocalizedMessage(), 1);
                }
            }
        }
    }

    public synchronized boolean g() {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
        ironSourceLoggerManager.b(ironSourceTag, this.p + ":isRewardedVideoAvailable()", 1);
        if (this.r) {
            return false;
        }
        Iterator<AbstractSmash> it2 = this.c.iterator();
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (next.A() && ((RewardedVideoSmash) next).F()) {
                return true;
            }
        }
        return false;
    }

    public void a(ListenersWrapper listenersWrapper) {
        this.q = listenersWrapper;
    }

    public void b(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, rewardedVideoSmash.p() + ":onRewardedVideoAdRewarded()", 1);
        if (this.s == null) {
            this.s = IronSourceObject.q().d().a().e().b();
        }
        JSONObject a2 = IronSourceUtils.a((AbstractSmash) rewardedVideoSmash);
        try {
            if (this.s != null) {
                a2.put("placement", i());
                a2.put("rewardName", this.s.e());
                a2.put("rewardAmount", this.s.d());
            } else {
                this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        EventData eventData = new EventData(1010, a2);
        if (!TextUtils.isEmpty(this.h)) {
            eventData.a("transId", IronSourceUtils.f("" + Long.toString(eventData.d()) + this.h + rewardedVideoSmash.u()));
            if (!TextUtils.isEmpty(IronSourceObject.q().e())) {
                eventData.a("dynamicUserId", IronSourceObject.q().e());
            }
            Map<String, String> k = IronSourceObject.q().k();
            if (k != null) {
                for (String next : k.keySet()) {
                    eventData.a("custom_" + next, k.get(next));
                }
            }
        }
        RewardedVideoEventsManager.g().d(eventData);
        Placement placement = this.s;
        if (placement != null) {
            this.q.a(placement);
        } else {
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:27:0x00e9, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(android.app.Activity r9, java.lang.String r10, java.lang.String r11) {
        /*
            r8 = this;
            monitor-enter(r8)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r8.i     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x00ea }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00ea }
            r2.<init>()     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = r8.p     // Catch:{ all -> 0x00ea }
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = ":initRewardedVideo(appKey: "
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            r2.append(r10)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = ", userId: "
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            r2.append(r11)     // Catch:{ all -> 0x00ea }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x00ea }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x00ea }
            r3 = 1
            r0.b(r1, r2, r3)     // Catch:{ all -> 0x00ea }
            java.util.Date r0 = new java.util.Date     // Catch:{ all -> 0x00ea }
            r0.<init>()     // Catch:{ all -> 0x00ea }
            long r0 = r0.getTime()     // Catch:{ all -> 0x00ea }
            r2 = 81312(0x13da0, float:1.13942E-40)
            r8.c((int) r2)     // Catch:{ all -> 0x00ea }
            r8.h = r10     // Catch:{ all -> 0x00ea }
            r8.g = r11     // Catch:{ all -> 0x00ea }
            r8.f = r9     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.utils.DailyCappingManager r9 = r8.f4581a     // Catch:{ all -> 0x00ea }
            android.app.Activity r10 = r8.f     // Catch:{ all -> 0x00ea }
            r9.a((android.content.Context) r10)     // Catch:{ all -> 0x00ea }
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r9 = r8.c     // Catch:{ all -> 0x00ea }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x00ea }
            r10 = 0
            r11 = 0
        L_0x0050:
            boolean r2 = r9.hasNext()     // Catch:{ all -> 0x00ea }
            r4 = 2
            if (r2 == 0) goto L_0x0088
            java.lang.Object r2 = r9.next()     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.AbstractSmash r2 = (com.ironsource.mediationsdk.AbstractSmash) r2     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.utils.DailyCappingManager r5 = r8.f4581a     // Catch:{ all -> 0x00ea }
            boolean r5 = r5.d((com.ironsource.mediationsdk.AbstractSmash) r2)     // Catch:{ all -> 0x00ea }
            if (r5 == 0) goto L_0x0078
            r5 = 150(0x96, float:2.1E-43)
            java.lang.Object[][] r6 = new java.lang.Object[r3][]     // Catch:{ all -> 0x00ea }
            java.lang.Object[] r4 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ea }
            java.lang.String r7 = "status"
            r4[r10] = r7     // Catch:{ all -> 0x00ea }
            java.lang.String r7 = "false"
            r4[r3] = r7     // Catch:{ all -> 0x00ea }
            r6[r10] = r4     // Catch:{ all -> 0x00ea }
            r8.a((int) r5, (com.ironsource.mediationsdk.AbstractSmash) r2, (java.lang.Object[][]) r6)     // Catch:{ all -> 0x00ea }
        L_0x0078:
            com.ironsource.mediationsdk.utils.DailyCappingManager r4 = r8.f4581a     // Catch:{ all -> 0x00ea }
            boolean r4 = r4.c((com.ironsource.mediationsdk.AbstractSmash) r2)     // Catch:{ all -> 0x00ea }
            if (r4 == 0) goto L_0x0050
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r4 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY     // Catch:{ all -> 0x00ea }
            r2.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE) r4)     // Catch:{ all -> 0x00ea }
            int r11 = r11 + 1
            goto L_0x0050
        L_0x0088:
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r9 = r8.c     // Catch:{ all -> 0x00ea }
            int r9 = r9.size()     // Catch:{ all -> 0x00ea }
            if (r11 != r9) goto L_0x0097
            com.ironsource.mediationsdk.sdk.ListenersWrapper r9 = r8.q     // Catch:{ all -> 0x00ea }
            r9.a((boolean) r10)     // Catch:{ all -> 0x00ea }
            monitor-exit(r8)
            return
        L_0x0097:
            r9 = 1000(0x3e8, float:1.401E-42)
            r8.c((int) r9)     // Catch:{ all -> 0x00ea }
            com.ironsource.mediationsdk.sdk.ListenersWrapper r9 = r8.q     // Catch:{ all -> 0x00ea }
            r11 = 0
            r9.b((java.lang.String) r11)     // Catch:{ all -> 0x00ea }
            r8.v = r3     // Catch:{ all -> 0x00ea }
            java.util.Date r9 = new java.util.Date     // Catch:{ all -> 0x00ea }
            r9.<init>()     // Catch:{ all -> 0x00ea }
            long r5 = r9.getTime()     // Catch:{ all -> 0x00ea }
            r8.w = r5     // Catch:{ all -> 0x00ea }
            java.util.Date r9 = new java.util.Date     // Catch:{ all -> 0x00ea }
            r9.<init>()     // Catch:{ all -> 0x00ea }
            long r5 = r9.getTime()     // Catch:{ all -> 0x00ea }
            long r5 = r5 - r0
            r9 = 81313(0x13da1, float:1.13944E-40)
            java.lang.Object[][] r11 = new java.lang.Object[r3][]     // Catch:{ all -> 0x00ea }
            java.lang.Object[] r0 = new java.lang.Object[r4]     // Catch:{ all -> 0x00ea }
            java.lang.String r1 = "duration"
            r0[r10] = r1     // Catch:{ all -> 0x00ea }
            java.lang.Long r1 = java.lang.Long.valueOf(r5)     // Catch:{ all -> 0x00ea }
            r0[r3] = r1     // Catch:{ all -> 0x00ea }
            r11[r10] = r0     // Catch:{ all -> 0x00ea }
            r8.a((int) r9, (java.lang.Object[][]) r11)     // Catch:{ all -> 0x00ea }
            r8.r()     // Catch:{ all -> 0x00ea }
        L_0x00d2:
            int r9 = r8.b     // Catch:{ all -> 0x00ea }
            if (r10 >= r9) goto L_0x00e8
            java.util.concurrent.CopyOnWriteArrayList<com.ironsource.mediationsdk.AbstractSmash> r9 = r8.c     // Catch:{ all -> 0x00ea }
            int r9 = r9.size()     // Catch:{ all -> 0x00ea }
            if (r10 >= r9) goto L_0x00e8
            com.ironsource.mediationsdk.AbstractAdapter r9 = r8.o()     // Catch:{ all -> 0x00ea }
            if (r9 != 0) goto L_0x00e5
            goto L_0x00e8
        L_0x00e5:
            int r10 = r10 + 1
            goto L_0x00d2
        L_0x00e8:
            monitor-exit(r8)
            return
        L_0x00ea:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.a(android.app.Activity, java.lang.String, java.lang.String):void");
    }

    private synchronized boolean c(boolean z) {
        boolean z2;
        z2 = true;
        if (this.k == null) {
            s();
            if (z) {
                this.k = true;
            } else if (!m() && k()) {
                this.k = false;
            }
        } else if (z && !this.k.booleanValue()) {
            this.k = true;
        } else if (!z && this.k.booleanValue() && !j() && !m()) {
            this.k = false;
        }
        z2 = false;
        return z2;
    }

    private boolean d(boolean z) {
        Boolean bool = this.k;
        if (bool == null) {
            return false;
        }
        if (z && !bool.booleanValue() && j()) {
            this.k = true;
            return true;
        } else if (z || !this.k.booleanValue()) {
            return false;
        } else {
            this.k = false;
            return true;
        }
    }

    private void c(int i) {
        a(i, (Object[][]) null);
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        this.u = i;
    }

    public void b() {
        Iterator<AbstractSmash> it2 = this.c.iterator();
        boolean z = false;
        while (it2.hasNext()) {
            AbstractSmash next = it2.next();
            if (next.t() == AbstractSmash.MEDIATION_STATE.CAPPED_PER_DAY) {
                a(150, next, new Object[][]{new Object[]{ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, "false"}});
                next.a(AbstractSmash.MEDIATION_STATE.NOT_AVAILABLE);
                if (((RewardedVideoSmash) next).F() && next.A()) {
                    next.a(AbstractSmash.MEDIATION_STATE.AVAILABLE);
                    z = true;
                }
            }
        }
        if (z && c(true)) {
            this.q.a(true);
        }
    }

    public void a(IronSourceError ironSourceError, RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, rewardedVideoSmash.p() + ":onRewardedVideoAdShowFailed(" + ironSourceError + ")", 1);
        a(1202, (AbstractSmash) rewardedVideoSmash, new Object[][]{new Object[]{"placement", i()}, new Object[]{"errorCode", Integer.valueOf(ironSourceError.a())}, new Object[]{"reason", ironSourceError.b()}});
        t();
        this.q.c(ironSourceError);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0073, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x00be, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(boolean r9, com.ironsource.mediationsdk.RewardedVideoSmash r10) {
        /*
            r8 = this;
            monitor-enter(r8)
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r8.i     // Catch:{ all -> 0x011d }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x011d }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x011d }
            r2.<init>()     // Catch:{ all -> 0x011d }
            java.lang.String r3 = r10.p()     // Catch:{ all -> 0x011d }
            r2.append(r3)     // Catch:{ all -> 0x011d }
            java.lang.String r3 = ": onRewardedVideoAvailabilityChanged(available:"
            r2.append(r3)     // Catch:{ all -> 0x011d }
            r2.append(r9)     // Catch:{ all -> 0x011d }
            java.lang.String r3 = ")"
            r2.append(r3)     // Catch:{ all -> 0x011d }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x011d }
            r3 = 1
            r0.b(r1, r2, r3)     // Catch:{ all -> 0x011d }
            boolean r0 = r8.r     // Catch:{ all -> 0x011d }
            if (r0 == 0) goto L_0x002c
            monitor-exit(r8)
            return
        L_0x002c:
            r0 = 0
            if (r9 == 0) goto L_0x0057
            boolean r1 = r8.v     // Catch:{ all -> 0x011d }
            if (r1 == 0) goto L_0x0057
            r8.v = r0     // Catch:{ all -> 0x011d }
            java.util.Date r1 = new java.util.Date     // Catch:{ all -> 0x011d }
            r1.<init>()     // Catch:{ all -> 0x011d }
            long r1 = r1.getTime()     // Catch:{ all -> 0x011d }
            long r4 = r8.w     // Catch:{ all -> 0x011d }
            long r1 = r1 - r4
            r4 = 1003(0x3eb, float:1.406E-42)
            java.lang.Object[][] r5 = new java.lang.Object[r3][]     // Catch:{ all -> 0x011d }
            r6 = 2
            java.lang.Object[] r6 = new java.lang.Object[r6]     // Catch:{ all -> 0x011d }
            java.lang.String r7 = "duration"
            r6[r0] = r7     // Catch:{ all -> 0x011d }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x011d }
            r6[r3] = r1     // Catch:{ all -> 0x011d }
            r5[r0] = r6     // Catch:{ all -> 0x011d }
            r8.a((int) r4, (java.lang.Object[][]) r5)     // Catch:{ all -> 0x011d }
        L_0x0057:
            com.ironsource.mediationsdk.AbstractSmash r1 = r8.d()     // Catch:{ all -> 0x00f1 }
            boolean r1 = r10.equals(r1)     // Catch:{ all -> 0x00f1 }
            if (r1 == 0) goto L_0x0074
            boolean r0 = r8.c((boolean) r9)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x0072
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r8.q     // Catch:{ all -> 0x00f1 }
            java.lang.Boolean r1 = r8.k     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x00f1 }
            r0.a((boolean) r1)     // Catch:{ all -> 0x00f1 }
        L_0x0072:
            monitor-exit(r8)
            return
        L_0x0074:
            com.ironsource.mediationsdk.AbstractSmash r1 = r8.e()     // Catch:{ all -> 0x00f1 }
            boolean r1 = r10.equals(r1)     // Catch:{ all -> 0x00f1 }
            if (r1 == 0) goto L_0x00bf
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r8.i     // Catch:{ all -> 0x00f1 }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x00f1 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x00f1 }
            r4.<init>()     // Catch:{ all -> 0x00f1 }
            java.lang.String r5 = r10.p()     // Catch:{ all -> 0x00f1 }
            r4.append(r5)     // Catch:{ all -> 0x00f1 }
            java.lang.String r5 = " is a premium adapter, canShowPremium: "
            r4.append(r5)     // Catch:{ all -> 0x00f1 }
            boolean r5 = r8.c()     // Catch:{ all -> 0x00f1 }
            r4.append(r5)     // Catch:{ all -> 0x00f1 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x00f1 }
            r1.b(r2, r4, r3)     // Catch:{ all -> 0x00f1 }
            boolean r1 = r8.c()     // Catch:{ all -> 0x00f1 }
            if (r1 != 0) goto L_0x00bf
            com.ironsource.mediationsdk.AbstractSmash$MEDIATION_STATE r1 = com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE.CAPPED_PER_SESSION     // Catch:{ all -> 0x00f1 }
            r10.a((com.ironsource.mediationsdk.AbstractSmash.MEDIATION_STATE) r1)     // Catch:{ all -> 0x00f1 }
            boolean r0 = r8.c((boolean) r0)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x00bd
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r8.q     // Catch:{ all -> 0x00f1 }
            java.lang.Boolean r1 = r8.k     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x00f1 }
            r0.a((boolean) r1)     // Catch:{ all -> 0x00f1 }
        L_0x00bd:
            monitor-exit(r8)
            return
        L_0x00bf:
            boolean r1 = r10.A()     // Catch:{ all -> 0x00f1 }
            if (r1 == 0) goto L_0x011b
            com.ironsource.mediationsdk.utils.DailyCappingManager r1 = r8.f4581a     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.c((com.ironsource.mediationsdk.AbstractSmash) r10)     // Catch:{ all -> 0x00f1 }
            if (r1 != 0) goto L_0x011b
            if (r9 == 0) goto L_0x00e1
            boolean r0 = r8.c((boolean) r3)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x011b
            com.ironsource.mediationsdk.sdk.ListenersWrapper r0 = r8.q     // Catch:{ all -> 0x00f1 }
            java.lang.Boolean r1 = r8.k     // Catch:{ all -> 0x00f1 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x00f1 }
            r0.a((boolean) r1)     // Catch:{ all -> 0x00f1 }
            goto L_0x011b
        L_0x00e1:
            boolean r0 = r8.c((boolean) r0)     // Catch:{ all -> 0x00f1 }
            if (r0 == 0) goto L_0x00ea
            r8.q()     // Catch:{ all -> 0x00f1 }
        L_0x00ea:
            r8.o()     // Catch:{ all -> 0x00f1 }
            r8.h()     // Catch:{ all -> 0x00f1 }
            goto L_0x011b
        L_0x00f1:
            r0 = move-exception
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r1 = r8.i     // Catch:{ all -> 0x011d }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r2 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK     // Catch:{ all -> 0x011d }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x011d }
            r3.<init>()     // Catch:{ all -> 0x011d }
            java.lang.String r4 = "onRewardedVideoAvailabilityChanged(available:"
            r3.append(r4)     // Catch:{ all -> 0x011d }
            r3.append(r9)     // Catch:{ all -> 0x011d }
            java.lang.String r9 = ", provider:"
            r3.append(r9)     // Catch:{ all -> 0x011d }
            java.lang.String r9 = r10.u()     // Catch:{ all -> 0x011d }
            r3.append(r9)     // Catch:{ all -> 0x011d }
            java.lang.String r9 = ")"
            r3.append(r9)     // Catch:{ all -> 0x011d }
            java.lang.String r9 = r3.toString()     // Catch:{ all -> 0x011d }
            r1.a((com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag) r2, (java.lang.String) r9, (java.lang.Throwable) r0)     // Catch:{ all -> 0x011d }
        L_0x011b:
            monitor-exit(r8)
            return
        L_0x011d:
            r9 = move-exception
            monitor-exit(r8)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.RewardedVideoManager.a(boolean, com.ironsource.mediationsdk.RewardedVideoSmash):void");
    }

    public void a(RewardedVideoSmash rewardedVideoSmash) {
        IronSourceLoggerManager ironSourceLoggerManager = this.i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        ironSourceLoggerManager.b(ironSourceTag, rewardedVideoSmash.p() + ":onRewardedVideoAdClicked()", 1);
        if (this.s == null) {
            this.s = IronSourceObject.q().d().a().e().b();
        }
        if (this.s == null) {
            this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "mCurrentPlacement is null", 3);
            return;
        }
        a(1006, (AbstractSmash) rewardedVideoSmash, new Object[][]{new Object[]{"placement", i()}});
        this.q.b(this.s);
    }

    public void a(boolean z) {
        if (this.j) {
            IronSourceLoggerManager ironSourceLoggerManager = this.i;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager.b(ironSourceTag, "Network Availability Changed To: " + z, 0);
            if (d(z)) {
                this.r = !z;
                this.q.a(z);
            }
        }
    }

    private void a(int i, Object[][] objArr) {
        JSONObject a2 = IronSourceUtils.a(false);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    a2.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "RewardedVideoManager logMediationEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.g().d(new EventData(i, a2));
    }

    private void a(int i, AbstractSmash abstractSmash, Object[][] objArr) {
        JSONObject a2 = IronSourceUtils.a(abstractSmash);
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    a2.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e) {
                this.i.b(IronSourceLogger.IronSourceTag.INTERNAL, "RewardedVideoManager logProviderEvent " + Log.getStackTraceString(e), 3);
            }
        }
        RewardedVideoEventsManager.g().d(new EventData(i, a2));
    }
}
