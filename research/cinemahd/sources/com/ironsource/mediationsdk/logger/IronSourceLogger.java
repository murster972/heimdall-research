package com.ironsource.mediationsdk.logger;

public abstract class IronSourceLogger {

    /* renamed from: a  reason: collision with root package name */
    int f4683a;
    private String b;

    public enum IronSourceTag {
        API,
        ADAPTER_API,
        CALLBACK,
        ADAPTER_CALLBACK,
        NETWORK,
        INTERNAL,
        NATIVE,
        EVENT
    }

    IronSourceLogger(String str) {
        this.b = str;
        this.f4683a = 0;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.f4683a;
    }

    public abstract void a(IronSourceTag ironSourceTag, String str, Throwable th);

    /* access modifiers changed from: package-private */
    public String b() {
        return this.b;
    }

    public abstract void b(IronSourceTag ironSourceTag, String str, int i);

    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof IronSourceLogger)) {
            return false;
        }
        IronSourceLogger ironSourceLogger = (IronSourceLogger) obj;
        String str = this.b;
        if (str == null || !str.equals(ironSourceLogger.b)) {
            return false;
        }
        return true;
    }

    public void a(int i) {
        this.f4683a = i;
    }

    IronSourceLogger(String str, int i) {
        this.b = str;
        this.f4683a = i;
    }
}
