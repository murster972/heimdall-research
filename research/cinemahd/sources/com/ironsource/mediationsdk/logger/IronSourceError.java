package com.ironsource.mediationsdk.logger;

public class IronSourceError {

    /* renamed from: a  reason: collision with root package name */
    private String f4682a;
    private int b;

    public IronSourceError(int i, String str) {
        this.b = i;
        this.f4682a = str == null ? "" : str;
    }

    public int a() {
        return this.b;
    }

    public String b() {
        return this.f4682a;
    }

    public String toString() {
        return "errorCode:" + this.b + ", errorMessage:" + this.f4682a;
    }
}
