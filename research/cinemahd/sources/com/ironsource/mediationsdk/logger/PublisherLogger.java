package com.ironsource.mediationsdk.logger;

import com.ironsource.mediationsdk.logger.IronSourceLogger;

public class PublisherLogger extends IronSourceLogger {
    private LogListener c;

    private PublisherLogger() {
        super("publisher");
    }

    public void a(IronSourceLogger.IronSourceTag ironSourceTag, String str, Throwable th) {
        if (th != null) {
            b(ironSourceTag, th.getMessage(), 3);
        }
    }

    public synchronized void b(IronSourceLogger.IronSourceTag ironSourceTag, String str, int i) {
        if (!(this.c == null || str == null)) {
            this.c.a(ironSourceTag, str, i);
        }
    }

    public PublisherLogger(LogListener logListener, int i) {
        super("publisher", i);
        this.c = logListener;
    }
}
