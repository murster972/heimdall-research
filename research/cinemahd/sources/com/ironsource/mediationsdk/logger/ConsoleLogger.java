package com.ironsource.mediationsdk.logger;

import android.util.Log;
import com.ironsource.mediationsdk.logger.IronSourceLogger;

public class ConsoleLogger extends IronSourceLogger {
    private ConsoleLogger() {
        super("console");
    }

    public void a(IronSourceLogger.IronSourceTag ironSourceTag, String str, Throwable th) {
        b(ironSourceTag, str + ":stacktrace[" + Log.getStackTraceString(th) + "]", 3);
    }

    public void b(IronSourceLogger.IronSourceTag ironSourceTag, String str, int i) {
        if (i == 0) {
            Log.v("ironSourceSDK: " + ironSourceTag, str);
        } else if (i == 1) {
            Log.i("ironSourceSDK: " + ironSourceTag, str);
        } else if (i == 2) {
            Log.w("ironSourceSDK: " + ironSourceTag, str);
        } else if (i == 3) {
            Log.e("ironSourceSDK: " + ironSourceTag, str);
        }
    }

    public ConsoleLogger(int i) {
        super("console", i);
    }
}
