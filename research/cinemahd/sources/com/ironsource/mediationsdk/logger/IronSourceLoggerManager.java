package com.ironsource.mediationsdk.logger;

import com.ironsource.mediationsdk.logger.IronSourceLogger;
import java.util.ArrayList;
import java.util.Iterator;

public class IronSourceLoggerManager extends IronSourceLogger implements LogListener {
    private static IronSourceLoggerManager d;
    private ArrayList<IronSourceLogger> c = new ArrayList<>();

    private IronSourceLoggerManager(String str) {
        super(str);
        d();
    }

    public static synchronized IronSourceLoggerManager b(int i) {
        IronSourceLoggerManager ironSourceLoggerManager;
        Class<IronSourceLoggerManager> cls = IronSourceLoggerManager.class;
        synchronized (cls) {
            if (d == null) {
                d = new IronSourceLoggerManager(cls.getSimpleName());
            } else {
                d.f4683a = i;
            }
            ironSourceLoggerManager = d;
        }
        return ironSourceLoggerManager;
    }

    public static synchronized IronSourceLoggerManager c() {
        IronSourceLoggerManager ironSourceLoggerManager;
        Class<IronSourceLoggerManager> cls = IronSourceLoggerManager.class;
        synchronized (cls) {
            if (d == null) {
                d = new IronSourceLoggerManager(cls.getSimpleName());
            }
            ironSourceLoggerManager = d;
        }
        return ironSourceLoggerManager;
    }

    private void d() {
        this.c.add(new ConsoleLogger(0));
    }

    public void a(IronSourceLogger ironSourceLogger) {
        this.c.add(ironSourceLogger);
    }

    public synchronized void a(IronSourceLogger.IronSourceTag ironSourceTag, String str, int i) {
        b(ironSourceTag, str, i);
    }

    public synchronized void a(IronSourceLogger.IronSourceTag ironSourceTag, String str, Throwable th) {
        if (th == null) {
            Iterator<IronSourceLogger> it2 = this.c.iterator();
            while (it2.hasNext()) {
                it2.next().b(ironSourceTag, str, 3);
            }
        } else {
            Iterator<IronSourceLogger> it3 = this.c.iterator();
            while (it3.hasNext()) {
                it3.next().a(ironSourceTag, str, th);
            }
        }
    }

    public synchronized void b(IronSourceLogger.IronSourceTag ironSourceTag, String str, int i) {
        if (i >= this.f4683a) {
            Iterator<IronSourceLogger> it2 = this.c.iterator();
            while (it2.hasNext()) {
                IronSourceLogger next = it2.next();
                if (next.a() <= i) {
                    next.b(ironSourceTag, str, i);
                }
            }
        }
    }

    private IronSourceLogger a(String str) {
        Iterator<IronSourceLogger> it2 = this.c.iterator();
        while (it2.hasNext()) {
            IronSourceLogger next = it2.next();
            if (next.b().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public void a(String str, int i) {
        if (str != null) {
            IronSourceLogger a2 = a(str);
            if (a2 == null) {
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
                b(ironSourceTag, "Failed to find logger:setLoggerDebugLevel(loggerName:" + str + " ,debugLevel:" + i + ")", 0);
            } else if (i < 0 || i > 3) {
                this.c.remove(a2);
            } else {
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.NATIVE;
                b(ironSourceTag2, "setLoggerDebugLevel(loggerName:" + str + " ,debugLevel:" + i + ")", 0);
                a2.a(i);
            }
        }
    }
}
