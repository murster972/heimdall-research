package com.ironsource.mediationsdk;

import com.startapp.sdk.adsbase.model.AdPreferences;

public class ISBannerSize {
    public static final ISBannerSize d = new ISBannerSize(AdPreferences.TYPE_BANNER);

    /* renamed from: a  reason: collision with root package name */
    private int f4605a;
    private int b;
    private String c;

    static {
        new ISBannerSize("LARGE");
        new ISBannerSize("RECTANGLE");
        new ISBannerSize("SMART");
    }

    public ISBannerSize(String str) {
        this.c = str;
    }

    public String a() {
        return this.c;
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.f4605a;
    }
}
