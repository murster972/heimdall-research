package com.ironsource.mediationsdk;

import android.os.Handler;
import android.os.Looper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.Placement;
import com.ironsource.mediationsdk.sdk.RewardedVideoListener;

public class RVListenerWrapper {
    private static final RVListenerWrapper b = new RVListenerWrapper();
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public RewardedVideoListener f4659a = null;

    private RVListenerWrapper() {
    }

    public static synchronized RVListenerWrapper c() {
        RVListenerWrapper rVListenerWrapper;
        synchronized (RVListenerWrapper.class) {
            rVListenerWrapper = b;
        }
        return rVListenerWrapper;
    }

    public synchronized void b() {
        if (this.f4659a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.f4659a.onRewardedVideoAdOpened();
                        RVListenerWrapper.this.a("onRewardedVideoAdOpened()");
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public String c(Placement placement) {
        return placement == null ? "" : placement.c();
    }

    public synchronized void a() {
        if (this.f4659a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.f4659a.onRewardedVideoAdClosed();
                        RVListenerWrapper.this.a("onRewardedVideoAdClosed()");
                    }
                }
            });
        }
    }

    public synchronized void b(final Placement placement) {
        if (this.f4659a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.f4659a.a(placement);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.a("onRewardedVideoAdRewarded() placement=" + RVListenerWrapper.this.c(placement));
                    }
                }
            });
        }
    }

    public synchronized void a(final boolean z) {
        if (this.f4659a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.f4659a.a(z);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.a("onRewardedVideoAvailabilityChanged() available=" + z);
                    }
                }
            });
        }
    }

    public synchronized void a(final IronSourceError ironSourceError) {
        if (this.f4659a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.f4659a.c(ironSourceError);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.a("onRewardedVideoAdShowFailed() error=" + ironSourceError.b());
                    }
                }
            });
        }
    }

    public synchronized void a(final Placement placement) {
        if (this.f4659a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public void run() {
                    synchronized (this) {
                        RVListenerWrapper.this.f4659a.b(placement);
                        RVListenerWrapper rVListenerWrapper = RVListenerWrapper.this;
                        rVListenerWrapper.a("onRewardedVideoAdClicked() placement=" + RVListenerWrapper.this.c(placement));
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.CALLBACK, str, 1);
    }
}
