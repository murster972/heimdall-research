package com.ironsource.mediationsdk.model;

import com.ironsource.mediationsdk.utils.AuctionSettings;
import java.util.ArrayList;
import java.util.Iterator;

public class InterstitialConfigurations {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<InterstitialPlacement> f4691a;
    private ApplicationEvents b;
    private int c;
    private int d;
    private int e;
    private String f;
    private String g;
    private AuctionSettings h;
    private InterstitialPlacement i;

    public InterstitialConfigurations() {
        this.f4691a = new ArrayList<>();
        this.b = new ApplicationEvents();
    }

    public void a(InterstitialPlacement interstitialPlacement) {
        if (interstitialPlacement != null) {
            this.f4691a.add(interstitialPlacement);
            if (this.i == null) {
                this.i = interstitialPlacement;
            } else if (interstitialPlacement.b() == 0) {
                this.i = interstitialPlacement;
            }
        }
    }

    public InterstitialPlacement b() {
        Iterator<InterstitialPlacement> it2 = this.f4691a.iterator();
        while (it2.hasNext()) {
            InterstitialPlacement next = it2.next();
            if (next.d()) {
                return next;
            }
        }
        return this.i;
    }

    public void c(String str) {
        this.g = str;
    }

    public int d() {
        return this.c;
    }

    public int e() {
        return this.d;
    }

    public AuctionSettings f() {
        return this.h;
    }

    public ApplicationEvents g() {
        return this.b;
    }

    public String h() {
        return this.g;
    }

    public int c() {
        return this.e;
    }

    public InterstitialConfigurations(int i2, int i3, ApplicationEvents applicationEvents, AuctionSettings auctionSettings, int i4) {
        this.f4691a = new ArrayList<>();
        this.c = i2;
        this.d = i3;
        this.b = applicationEvents;
        this.h = auctionSettings;
        this.e = i4;
    }

    public void b(String str) {
        this.f = str;
    }

    public InterstitialPlacement a(String str) {
        Iterator<InterstitialPlacement> it2 = this.f4691a.iterator();
        while (it2.hasNext()) {
            InterstitialPlacement next = it2.next();
            if (next.c().equals(str)) {
                return next;
            }
        }
        return null;
    }

    public String a() {
        return this.f;
    }
}
