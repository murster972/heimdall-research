package com.ironsource.mediationsdk.model;

public class Configurations {

    /* renamed from: a  reason: collision with root package name */
    private RewardedVideoConfigurations f4690a;
    private InterstitialConfigurations b;
    private OfferwallConfigurations c;
    private BannerConfigurations d;
    private ApplicationConfigurations e;

    public Configurations() {
    }

    public ApplicationConfigurations a() {
        return this.e;
    }

    public BannerConfigurations b() {
        return this.d;
    }

    public InterstitialConfigurations c() {
        return this.b;
    }

    public OfferwallConfigurations d() {
        return this.c;
    }

    public RewardedVideoConfigurations e() {
        return this.f4690a;
    }

    public Configurations(RewardedVideoConfigurations rewardedVideoConfigurations, InterstitialConfigurations interstitialConfigurations, OfferwallConfigurations offerwallConfigurations, BannerConfigurations bannerConfigurations, ApplicationConfigurations applicationConfigurations) {
        if (rewardedVideoConfigurations != null) {
            this.f4690a = rewardedVideoConfigurations;
        }
        if (interstitialConfigurations != null) {
            this.b = interstitialConfigurations;
        }
        if (offerwallConfigurations != null) {
            this.c = offerwallConfigurations;
        }
        if (bannerConfigurations != null) {
            this.d = bannerConfigurations;
        }
        this.e = applicationConfigurations;
    }
}
