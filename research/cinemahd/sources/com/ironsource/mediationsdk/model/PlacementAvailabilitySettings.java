package com.ironsource.mediationsdk.model;

public class PlacementAvailabilitySettings {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4696a;
    private boolean b;
    private boolean c;
    private PlacementCappingType d;
    private int e;
    private int f;

    public static class PlacementAvailabilitySettingsBuilder {

        /* renamed from: a  reason: collision with root package name */
        private boolean f4697a = true;
        private boolean b = false;
        private boolean c = false;
        private PlacementCappingType d = null;
        private int e = 0;
        private int f = 0;

        public PlacementAvailabilitySettingsBuilder a(boolean z) {
            this.f4697a = z;
            return this;
        }

        public PlacementAvailabilitySettingsBuilder a(boolean z, PlacementCappingType placementCappingType, int i) {
            this.b = z;
            if (placementCappingType == null) {
                placementCappingType = PlacementCappingType.PER_DAY;
            }
            this.d = placementCappingType;
            this.e = i;
            return this;
        }

        public PlacementAvailabilitySettingsBuilder a(boolean z, int i) {
            this.c = z;
            this.f = i;
            return this;
        }

        public PlacementAvailabilitySettings a() {
            return new PlacementAvailabilitySettings(this.f4697a, this.b, this.c, this.d, this.e, this.f);
        }
    }

    public PlacementCappingType a() {
        return this.d;
    }

    public int b() {
        return this.e;
    }

    public int c() {
        return this.f;
    }

    public boolean d() {
        return this.b;
    }

    public boolean e() {
        return this.f4696a;
    }

    public boolean f() {
        return this.c;
    }

    private PlacementAvailabilitySettings(boolean z, boolean z2, boolean z3, PlacementCappingType placementCappingType, int i, int i2) {
        this.f4696a = z;
        this.b = z2;
        this.c = z3;
        this.d = placementCappingType;
        this.e = i;
        this.f = i2;
    }
}
