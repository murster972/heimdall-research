package com.ironsource.mediationsdk.model;

import java.util.ArrayList;

public class OfferwallConfigurations {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<OfferwallPlacement> f4693a = new ArrayList<>();
    private OfferwallPlacement b;
    private ApplicationEvents c;

    public OfferwallConfigurations(ApplicationEvents applicationEvents) {
        this.c = applicationEvents;
    }

    public void a(OfferwallPlacement offerwallPlacement) {
        if (offerwallPlacement != null) {
            this.f4693a.add(offerwallPlacement);
            if (this.b == null) {
                this.b = offerwallPlacement;
            } else if (offerwallPlacement.a() == 0) {
                this.b = offerwallPlacement;
            }
        }
    }

    public ApplicationEvents a() {
        return this.c;
    }
}
