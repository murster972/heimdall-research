package com.ironsource.mediationsdk.model;

import org.json.JSONException;
import org.json.JSONObject;

public class ProviderSettings {

    /* renamed from: a  reason: collision with root package name */
    private String f4700a;
    private String b;
    private JSONObject c;
    private JSONObject d;
    private JSONObject e;
    private JSONObject f;
    private String g;
    private String h;
    private boolean i;
    private String j;
    private int k;
    private int l;
    private int m;

    public ProviderSettings(String str) {
        this.f4700a = str;
        this.j = str;
        this.b = str;
        this.d = new JSONObject();
        this.e = new JSONObject();
        this.f = new JSONObject();
        this.c = new JSONObject();
        this.k = -1;
        this.l = -1;
        this.m = -1;
    }

    public void a(JSONObject jSONObject) {
        this.f = jSONObject;
    }

    public void b(JSONObject jSONObject) {
        this.e = jSONObject;
    }

    public void c(JSONObject jSONObject) {
        this.d = jSONObject;
    }

    public JSONObject d() {
        return this.f;
    }

    public int e() {
        return this.l;
    }

    public JSONObject f() {
        return this.e;
    }

    public String g() {
        return this.j;
    }

    public String h() {
        return this.f4700a;
    }

    public String i() {
        return this.b;
    }

    public int j() {
        return this.k;
    }

    public JSONObject k() {
        return this.d;
    }

    public String l() {
        return this.g;
    }

    public boolean m() {
        return this.i;
    }

    public void a(String str, Object obj) {
        try {
            this.f.put(str, obj);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void b(String str, Object obj) {
        try {
            this.e.put(str, obj);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void c(String str, Object obj) {
        try {
            this.d.put(str, obj);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }

    public void a(String str) {
        this.h = str;
    }

    public void b(String str) {
        this.g = str;
    }

    public void c(int i2) {
        this.k = i2;
    }

    public String a() {
        return this.h;
    }

    public JSONObject b() {
        return this.c;
    }

    public int c() {
        return this.m;
    }

    public void a(boolean z) {
        this.i = z;
    }

    public void b(int i2) {
        this.l = i2;
    }

    public void a(int i2) {
        this.m = i2;
    }

    public ProviderSettings(String str, String str2, JSONObject jSONObject, JSONObject jSONObject2, JSONObject jSONObject3, JSONObject jSONObject4) {
        this.f4700a = str;
        this.j = str;
        this.b = str2;
        this.d = jSONObject2;
        this.e = jSONObject3;
        this.f = jSONObject4;
        this.c = jSONObject;
        this.k = -1;
        this.l = -1;
        this.m = -1;
    }

    public ProviderSettings(ProviderSettings providerSettings) {
        this.f4700a = providerSettings.h();
        this.j = providerSettings.h();
        this.b = providerSettings.i();
        this.d = providerSettings.k();
        this.e = providerSettings.f();
        this.f = providerSettings.d();
        this.c = providerSettings.b();
        this.k = providerSettings.j();
        this.l = providerSettings.e();
        this.m = providerSettings.c();
    }
}
