package com.ironsource.mediationsdk.model;

public class ApplicationEvents {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4687a;
    private boolean b;
    private String c;
    private String d;
    private int e;
    private int f;
    private int g;
    private int[] h;

    public ApplicationEvents() {
    }

    public int a() {
        return this.e;
    }

    public String b() {
        return this.d;
    }

    public String c() {
        return this.c;
    }

    public int d() {
        return this.g;
    }

    public int e() {
        return this.f;
    }

    public int[] f() {
        return this.h;
    }

    public boolean g() {
        return this.b;
    }

    public boolean h() {
        return this.f4687a;
    }

    public ApplicationEvents(boolean z, boolean z2, String str, String str2, int i, int i2, int i3, int[] iArr) {
        this.f4687a = z;
        this.b = z2;
        this.c = str;
        this.d = str2;
        this.e = i;
        this.f = i2;
        this.g = i3;
        this.h = iArr;
    }
}
