package com.ironsource.mediationsdk.model;

import android.text.TextUtils;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import java.util.ArrayList;
import java.util.Iterator;

public class ProviderSettingsHolder {
    private static ProviderSettingsHolder b;

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<ProviderSettings> f4701a = new ArrayList<>();

    private ProviderSettingsHolder() {
    }

    public static synchronized ProviderSettingsHolder b() {
        ProviderSettingsHolder providerSettingsHolder;
        synchronized (ProviderSettingsHolder.class) {
            if (b == null) {
                b = new ProviderSettingsHolder();
            }
            providerSettingsHolder = b;
        }
        return providerSettingsHolder;
    }

    public void a(ProviderSettings providerSettings) {
        if (providerSettings != null) {
            this.f4701a.add(providerSettings);
        }
    }

    public boolean a(String str) {
        Iterator<ProviderSettings> it2 = this.f4701a.iterator();
        while (it2.hasNext()) {
            if (it2.next().h().equals(str)) {
                return true;
            }
        }
        return false;
    }

    public void a() {
        Iterator<ProviderSettings> it2 = this.f4701a.iterator();
        while (it2.hasNext()) {
            ProviderSettings next = it2.next();
            if (next.m() && !TextUtils.isEmpty(next.i())) {
                ProviderSettings b2 = b(next.i());
                next.b(IronSourceUtils.a(next.f(), b2.f()));
                next.c(IronSourceUtils.a(next.k(), b2.k()));
                next.a(IronSourceUtils.a(next.d(), b2.d()));
            }
        }
    }

    public ProviderSettings b(String str) {
        Iterator<ProviderSettings> it2 = this.f4701a.iterator();
        while (it2.hasNext()) {
            ProviderSettings next = it2.next();
            if (next.h().equals(str)) {
                return next;
            }
        }
        ProviderSettings providerSettings = new ProviderSettings(str);
        a(providerSettings);
        return providerSettings;
    }
}
