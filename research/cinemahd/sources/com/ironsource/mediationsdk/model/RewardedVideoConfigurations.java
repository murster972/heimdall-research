package com.ironsource.mediationsdk.model;

import com.ironsource.mediationsdk.utils.AuctionSettings;
import java.util.ArrayList;
import java.util.Iterator;

public class RewardedVideoConfigurations {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<Placement> f4702a;
    private ApplicationEvents b;
    private int c;
    private int d;
    private String e;
    private String f;
    private int g;
    private Placement h;
    private AuctionSettings i;

    public RewardedVideoConfigurations() {
        this.f4702a = new ArrayList<>();
        this.b = new ApplicationEvents();
    }

    public void a(Placement placement) {
        if (placement != null) {
            this.f4702a.add(placement);
            if (this.h == null) {
                this.h = placement;
            } else if (placement.b() == 0) {
                this.h = placement;
            }
        }
    }

    public Placement b() {
        Iterator<Placement> it2 = this.f4702a.iterator();
        while (it2.hasNext()) {
            Placement next = it2.next();
            if (next.f()) {
                return next;
            }
        }
        return this.h;
    }

    public int c() {
        return this.g;
    }

    public String d() {
        return this.f;
    }

    public int e() {
        return this.c;
    }

    public int f() {
        return this.d;
    }

    public AuctionSettings g() {
        return this.i;
    }

    public ApplicationEvents h() {
        return this.b;
    }

    public RewardedVideoConfigurations(int i2, int i3, int i4, ApplicationEvents applicationEvents, AuctionSettings auctionSettings) {
        this.f4702a = new ArrayList<>();
        this.c = i2;
        this.d = i3;
        this.g = i4;
        this.b = applicationEvents;
        this.i = auctionSettings;
    }

    public void b(String str) {
        this.f = str;
    }

    public String a() {
        return this.e;
    }

    public void a(String str) {
        this.e = str;
    }
}
