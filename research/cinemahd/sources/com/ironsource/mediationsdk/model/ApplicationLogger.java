package com.ironsource.mediationsdk.model;

public class ApplicationLogger {

    /* renamed from: a  reason: collision with root package name */
    private int f4688a;
    private int b;

    public ApplicationLogger() {
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.f4688a;
    }

    public ApplicationLogger(int i, int i2, int i3) {
        this.f4688a = i2;
        this.b = i3;
    }
}
