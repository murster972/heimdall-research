package com.ironsource.mediationsdk.model;

public class Placement {

    /* renamed from: a  reason: collision with root package name */
    private int f4695a;
    private String b;
    private boolean c;
    private String d;
    private int e;
    private PlacementAvailabilitySettings f;

    public Placement(int i, String str, boolean z, String str2, int i2, PlacementAvailabilitySettings placementAvailabilitySettings) {
        this.f4695a = i;
        this.b = str;
        this.c = z;
        this.d = str2;
        this.e = i2;
        this.f = placementAvailabilitySettings;
    }

    public PlacementAvailabilitySettings a() {
        return this.f;
    }

    public int b() {
        return this.f4695a;
    }

    public String c() {
        return this.b;
    }

    public int d() {
        return this.e;
    }

    public String e() {
        return this.d;
    }

    public boolean f() {
        return this.c;
    }

    public String toString() {
        return "placement name: " + this.b + ", reward name: " + this.d + " , amount:" + this.e;
    }
}
