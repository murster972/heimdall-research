package com.ironsource.mediationsdk.model;

public class InterstitialPlacement {

    /* renamed from: a  reason: collision with root package name */
    private int f4692a;
    private String b;
    private boolean c;
    private PlacementAvailabilitySettings d;

    public InterstitialPlacement(int i, String str, boolean z, PlacementAvailabilitySettings placementAvailabilitySettings) {
        this.f4692a = i;
        this.b = str;
        this.c = z;
        this.d = placementAvailabilitySettings;
    }

    public PlacementAvailabilitySettings a() {
        return this.d;
    }

    public int b() {
        return this.f4692a;
    }

    public String c() {
        return this.b;
    }

    public boolean d() {
        return this.c;
    }

    public String toString() {
        return "placement name: " + this.b;
    }
}
