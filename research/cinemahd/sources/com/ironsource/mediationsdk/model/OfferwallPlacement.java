package com.ironsource.mediationsdk.model;

public class OfferwallPlacement {

    /* renamed from: a  reason: collision with root package name */
    private int f4694a;
    private String b;

    public OfferwallPlacement(int i, String str, boolean z) {
        this.f4694a = i;
        this.b = str;
    }

    public int a() {
        return this.f4694a;
    }

    public String toString() {
        return "placement name: " + this.b + ", placement id: " + this.f4694a;
    }
}
