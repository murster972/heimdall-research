package com.ironsource.mediationsdk.model;

import org.json.JSONObject;

public class AdapterConfig {

    /* renamed from: a  reason: collision with root package name */
    private ProviderSettings f4685a;
    private JSONObject b;
    private boolean c;
    private int d;

    public AdapterConfig(ProviderSettings providerSettings, JSONObject jSONObject) {
        this.f4685a = providerSettings;
        this.b = jSONObject;
        this.c = jSONObject.optInt("instanceType") == 2;
        this.d = jSONObject.optInt("maxAdsPerSession", 99);
    }

    public String a() {
        return this.f4685a.a();
    }

    public JSONObject b() {
        return this.b;
    }

    public int c() {
        return this.d;
    }

    public String d() {
        return this.f4685a.h();
    }

    public String e() {
        return this.f4685a.l();
    }

    public boolean f() {
        return this.c;
    }
}
