package com.ironsource.mediationsdk.model;

import org.json.JSONObject;

public class ServerSegmetData {

    /* renamed from: a  reason: collision with root package name */
    private String f4703a;
    private String b;
    private JSONObject c;

    public ServerSegmetData(String str, String str2, JSONObject jSONObject) {
        this.f4703a = str;
        this.b = str2;
        this.c = jSONObject;
    }

    public JSONObject a() {
        return this.c;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.f4703a;
    }
}
