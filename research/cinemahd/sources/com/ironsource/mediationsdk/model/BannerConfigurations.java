package com.ironsource.mediationsdk.model;

import java.util.ArrayList;
import java.util.Iterator;

public class BannerConfigurations {

    /* renamed from: a  reason: collision with root package name */
    private ApplicationEvents f4689a;
    private long b;
    private ArrayList<BannerPlacement> c;
    private BannerPlacement d;
    private int e;
    private int f;

    public BannerConfigurations() {
        this.f4689a = new ApplicationEvents();
        this.c = new ArrayList<>();
    }

    public long a() {
        return this.b;
    }

    public int b() {
        return this.f;
    }

    public ApplicationEvents c() {
        return this.f4689a;
    }

    public int d() {
        return this.e;
    }

    public BannerPlacement e() {
        Iterator<BannerPlacement> it2 = this.c.iterator();
        while (it2.hasNext()) {
            BannerPlacement next = it2.next();
            if (next.d()) {
                return next;
            }
        }
        return this.d;
    }

    public void a(BannerPlacement bannerPlacement) {
        if (bannerPlacement != null) {
            this.c.add(bannerPlacement);
            if (this.d == null) {
                this.d = bannerPlacement;
            } else if (bannerPlacement.b() == 0) {
                this.d = bannerPlacement;
            }
        }
    }

    public BannerConfigurations(int i, long j, ApplicationEvents applicationEvents, int i2, int i3) {
        this.c = new ArrayList<>();
        this.b = j;
        this.f4689a = applicationEvents;
        this.e = i2;
        this.f = i3;
    }

    public BannerPlacement a(String str) {
        Iterator<BannerPlacement> it2 = this.c.iterator();
        while (it2.hasNext()) {
            BannerPlacement next = it2.next();
            if (next.c().equals(str)) {
                return next;
            }
        }
        return null;
    }
}
