package com.ironsource.mediationsdk.model;

public class ApplicationConfigurations {

    /* renamed from: a  reason: collision with root package name */
    private ApplicationLogger f4686a;
    private ServerSegmetData b;
    private boolean c;

    public ApplicationConfigurations() {
        this.f4686a = new ApplicationLogger();
    }

    public boolean a() {
        return this.c;
    }

    public ApplicationLogger b() {
        return this.f4686a;
    }

    public ServerSegmetData c() {
        return this.b;
    }

    public ApplicationConfigurations(ApplicationLogger applicationLogger, ServerSegmetData serverSegmetData, boolean z) {
        this.f4686a = applicationLogger;
        this.b = serverSegmetData;
        this.c = z;
    }
}
