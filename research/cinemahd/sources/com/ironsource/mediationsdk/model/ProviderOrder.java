package com.ironsource.mediationsdk.model;

import android.text.TextUtils;
import java.util.ArrayList;

public class ProviderOrder {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<String> f4699a = new ArrayList<>();
    private String b;
    private String c;
    private ArrayList<String> d = new ArrayList<>();
    private ArrayList<String> e = new ArrayList<>();

    public ArrayList<String> a() {
        return this.e;
    }

    public ArrayList<String> b() {
        return this.d;
    }

    public void c(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.f4699a.add(str);
        }
    }

    public String d() {
        return this.c;
    }

    public void d(String str) {
    }

    public ArrayList<String> e() {
        return this.f4699a;
    }

    public void e(String str) {
    }

    public void f(String str) {
        this.b = str;
    }

    public void g(String str) {
        this.c = str;
    }

    public void a(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.e.add(str);
        }
    }

    public void b(String str) {
        if (!TextUtils.isEmpty(str)) {
            this.d.add(str);
        }
    }

    public String c() {
        return this.b;
    }
}
