package com.ironsource.mediationsdk;

import com.ironsource.mediationsdk.logger.IronSourceError;

public interface ProgIsManagerListener {
    void a(ProgIsSmash progIsSmash);

    void a(ProgIsSmash progIsSmash, long j);

    void a(IronSourceError ironSourceError, ProgIsSmash progIsSmash);

    void a(IronSourceError ironSourceError, ProgIsSmash progIsSmash, long j);

    void b(ProgIsSmash progIsSmash);

    void b(IronSourceError ironSourceError, ProgIsSmash progIsSmash);

    void c(ProgIsSmash progIsSmash);

    void d(ProgIsSmash progIsSmash);

    void e(ProgIsSmash progIsSmash);

    void f(ProgIsSmash progIsSmash);
}
