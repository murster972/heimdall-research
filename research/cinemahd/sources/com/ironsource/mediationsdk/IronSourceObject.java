package com.ironsource.mediationsdk;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.facebook.ads.AdError;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.eventsmodule.EventData;
import com.ironsource.mediationsdk.IronSource;
import com.ironsource.mediationsdk.MediationInitializer;
import com.ironsource.mediationsdk.config.ConfigValidationResult;
import com.ironsource.mediationsdk.events.InterstitialEventsManager;
import com.ironsource.mediationsdk.events.RewardedVideoEventsManager;
import com.ironsource.mediationsdk.events.SuperLooper;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.logger.LogListener;
import com.ironsource.mediationsdk.logger.PublisherLogger;
import com.ironsource.mediationsdk.model.ApplicationEvents;
import com.ironsource.mediationsdk.model.BannerConfigurations;
import com.ironsource.mediationsdk.model.BannerPlacement;
import com.ironsource.mediationsdk.model.InterstitialConfigurations;
import com.ironsource.mediationsdk.model.InterstitialPlacement;
import com.ironsource.mediationsdk.model.ProviderSettings;
import com.ironsource.mediationsdk.sdk.InternalOfferwallListener;
import com.ironsource.mediationsdk.sdk.InterstitialListener;
import com.ironsource.mediationsdk.sdk.InterstitialManagerListener;
import com.ironsource.mediationsdk.sdk.IronSourceInterface;
import com.ironsource.mediationsdk.sdk.ListenersWrapper;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoManagerListener;
import com.ironsource.mediationsdk.server.HttpFunctions;
import com.ironsource.mediationsdk.server.ServerURL;
import com.ironsource.mediationsdk.utils.CappingManager;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.GeneralPropertiesWorker;
import com.ironsource.mediationsdk.utils.IronSourceAES;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.ServerResponseWrapper;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceObject implements IronSourceInterface, MediationInitializer.OnMediationInitializationListener {
    private static IronSourceObject U;
    private boolean A = true;
    private IronSourceSegment B;
    private int C;
    private boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    private Boolean H;
    private IronSourceBannerLayout I;
    private String J;
    private Boolean K = null;
    private ProgRvManager L;
    private ProgIsManager M;
    private boolean N;
    private boolean O;
    private boolean P;
    private CopyOnWriteArraySet<String> Q;
    private CopyOnWriteArraySet<String> R;
    private DemandOnlyIsManager S;
    private DemandOnlyRvManager T;

    /* renamed from: a  reason: collision with root package name */
    private AbstractAdapter f4628a;
    private RewardedVideoManager b;
    private InterstitialManager c;
    private OfferwallManager d;
    private BannerManager e;
    private IronSourceLoggerManager f;
    private ListenersWrapper g;
    private PublisherLogger h;
    private AtomicBoolean i;
    private final Object j = new Object();
    private ServerResponseWrapper k = null;
    private String l = null;
    private String m = null;
    private Integer n = null;
    private String o = null;
    private String p = null;
    private String q = null;
    private Map<String, String> r = null;
    private String s = null;
    private AtomicBoolean t;
    private boolean u = false;
    private List<IronSource.AD_UNIT> v;
    private String w = null;
    private Activity x;
    private Set<IronSource.AD_UNIT> y;
    private Set<IronSource.AD_UNIT> z;

    /* renamed from: com.ironsource.mediationsdk.IronSourceObject$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4629a = new int[IronSource.AD_UNIT.values().length];
        static final /* synthetic */ int[] b = new int[CappingManager.ECappingStatus.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|1|2|3|(2:5|6)|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(21:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|26) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        static {
            /*
                com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus[] r0 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r2 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.CAPPED_PER_DELIVERY     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r3 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.CAPPED_PER_COUNT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r4 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.CAPPED_PER_PACE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.ironsource.mediationsdk.utils.CappingManager$ECappingStatus r5 = com.ironsource.mediationsdk.utils.CappingManager.ECappingStatus.NOT_CAPPED     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                com.ironsource.mediationsdk.IronSource$AD_UNIT[] r4 = com.ironsource.mediationsdk.IronSource.AD_UNIT.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                f4629a = r4
                int[] r4 = f4629a     // Catch:{ NoSuchFieldError -> 0x0048 }
                com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                int[] r0 = f4629a     // Catch:{ NoSuchFieldError -> 0x0052 }
                com.ironsource.mediationsdk.IronSource$AD_UNIT r4 = com.ironsource.mediationsdk.IronSource.AD_UNIT.INTERSTITIAL     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r0 = f4629a     // Catch:{ NoSuchFieldError -> 0x005c }
                com.ironsource.mediationsdk.IronSource$AD_UNIT r1 = com.ironsource.mediationsdk.IronSource.AD_UNIT.OFFERWALL     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                int[] r0 = f4629a     // Catch:{ NoSuchFieldError -> 0x0066 }
                com.ironsource.mediationsdk.IronSource$AD_UNIT r1 = com.ironsource.mediationsdk.IronSource.AD_UNIT.BANNER     // Catch:{ NoSuchFieldError -> 0x0066 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
            L_0x0066:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.AnonymousClass1.<clinit>():void");
        }
    }

    public interface IResponseListener {
        void a(String str);
    }

    private IronSourceObject() {
        r();
        this.i = new AtomicBoolean();
        this.y = new HashSet();
        this.z = new HashSet();
        this.E = false;
        this.D = false;
        this.t = new AtomicBoolean(true);
        this.C = 0;
        this.F = false;
        this.G = false;
        this.w = UUID.randomUUID().toString();
        this.H = false;
        this.P = false;
        this.J = null;
        this.L = null;
        this.M = null;
        this.N = false;
        this.O = false;
        this.Q = new CopyOnWriteArraySet<>();
        this.R = new CopyOnWriteArraySet<>();
        this.S = null;
        this.T = null;
        this.e = null;
    }

    private void A() {
        this.f.b(IronSourceLogger.IronSourceTag.INTERNAL, "Interstitial started in programmatic mode", 0);
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.k.c().b().size(); i2++) {
            String str = this.k.c().b().get(i2);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(this.k.d().b(str));
            }
        }
        if (arrayList.size() > 0) {
            this.M = new ProgIsManager(this.x, arrayList, this.k.a().c(), g(), h(), this.k.a().c().c());
            if (this.P) {
                this.P = false;
                this.M.b();
                return;
            }
            return;
        }
        JSONObject a2 = IronSourceUtils.a(false, true);
        a(a2, new Object[][]{new Object[]{"errorCode", 1010}});
        a(82314, a2);
        a(IronSource.AD_UNIT.INTERSTITIAL, false);
    }

    private void B() {
        this.f.b(IronSourceLogger.IronSourceTag.INTERNAL, "Rewarded Video started in programmatic mode", 0);
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.k.c().e().size(); i2++) {
            String str = this.k.c().e().get(i2);
            if (!TextUtils.isEmpty(str)) {
                arrayList.add(this.k.d().b(str));
            }
        }
        if (arrayList.size() > 0) {
            this.L = new ProgRvManager(this.x, arrayList, this.k.a().e(), g(), h());
            return;
        }
        JSONObject a2 = IronSourceUtils.a(false, true);
        a(a2, new Object[][]{new Object[]{"errorCode", 1010}});
        b(81314, a2);
        a(IronSource.AD_UNIT.REWARDED_VIDEO, false);
    }

    private void C() {
        ProviderSettings b2;
        ProviderSettings b3;
        ProviderSettings b4;
        if (this.D) {
            y();
            return;
        }
        this.N = this.k.a().e().g().d();
        b(81000, IronSourceUtils.a(false, this.N));
        if (this.N) {
            B();
            return;
        }
        int f2 = this.k.a().e().f();
        for (int i2 = 0; i2 < this.k.c().e().size(); i2++) {
            String str = this.k.c().e().get(i2);
            if (!TextUtils.isEmpty(str) && (b4 = this.k.d().b(str)) != null) {
                RewardedVideoSmash rewardedVideoSmash = new RewardedVideoSmash(b4, f2);
                if (a((AbstractSmash) rewardedVideoSmash)) {
                    rewardedVideoSmash.a((RewardedVideoManagerListener) this.b);
                    rewardedVideoSmash.a(i2 + 1);
                    this.b.a((AbstractSmash) rewardedVideoSmash);
                }
            }
        }
        if (this.b.c.size() > 0) {
            this.b.b(this.k.a().e().h().h());
            this.b.a(this.k.a().e().e());
            this.b.b(this.k.a().e().c());
            String e2 = this.k.e();
            if (!TextUtils.isEmpty(e2) && (b3 = this.k.d().b(e2)) != null) {
                RewardedVideoSmash rewardedVideoSmash2 = new RewardedVideoSmash(b3, f2);
                if (a((AbstractSmash) rewardedVideoSmash2)) {
                    rewardedVideoSmash2.a((RewardedVideoManagerListener) this.b);
                    this.b.b((AbstractSmash) rewardedVideoSmash2);
                }
            }
            String f3 = this.k.f();
            if (!TextUtils.isEmpty(f3) && (b2 = this.k.d().b(f3)) != null) {
                RewardedVideoSmash rewardedVideoSmash3 = new RewardedVideoSmash(b2, f2);
                if (a((AbstractSmash) rewardedVideoSmash3)) {
                    rewardedVideoSmash3.a((RewardedVideoManagerListener) this.b);
                    this.b.d(rewardedVideoSmash3);
                }
            }
            this.b.a(this.x, g(), h());
            return;
        }
        JSONObject a2 = IronSourceUtils.a(false, false);
        a(a2, new Object[][]{new Object[]{"errorCode", 1010}});
        b(81314, a2);
        a(IronSource.AD_UNIT.REWARDED_VIDEO, false);
    }

    private void c(Activity activity) {
        AtomicBoolean atomicBoolean = this.i;
        if (atomicBoolean != null && atomicBoolean.compareAndSet(false, true)) {
            SuperLooper.a().a(new GeneralPropertiesWorker(activity.getApplicationContext()));
            InterstitialEventsManager.g().a(activity.getApplicationContext(), this.B);
            RewardedVideoEventsManager.g().a(activity.getApplicationContext(), this.B);
        }
    }

    private InterstitialPlacement g(String str) {
        InterstitialConfigurations c2 = this.k.a().c();
        if (c2 != null) {
            return c2.a(str);
        }
        return null;
    }

    private InterstitialPlacement h(String str) {
        InterstitialPlacement g2 = g(str);
        if (g2 == null) {
            this.f.b(IronSourceLogger.IronSourceTag.API, "Placement is not valid, please make sure you are using the right placements, using the default placement.", 3);
            g2 = p();
            if (g2 == null) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "Default placement was not found, please make sure you are using the right placements.", 3);
                return null;
            }
        }
        String a2 = a(g2.c(), f(g2.c()));
        if (TextUtils.isEmpty(a2)) {
            return g2;
        }
        this.f.b(IronSourceLogger.IronSourceTag.API, a2, 1);
        this.g.a(g2);
        this.g.b(ErrorBuilder.a(a2));
        return null;
    }

    private InterstitialPlacement p() {
        InterstitialConfigurations c2 = this.k.a().c();
        if (c2 != null) {
            return c2.b();
        }
        return null;
    }

    public static synchronized IronSourceObject q() {
        IronSourceObject ironSourceObject;
        synchronized (IronSourceObject.class) {
            if (U == null) {
                U = new IronSourceObject();
            }
            ironSourceObject = U;
        }
        return ironSourceObject;
    }

    private void r() {
        this.f = IronSourceLoggerManager.b(0);
        this.h = new PublisherLogger((LogListener) null, 1);
        this.f.a((IronSourceLogger) this.h);
        this.g = new ListenersWrapper();
        this.b = new RewardedVideoManager();
        this.b.a(this.g);
        this.c = new InterstitialManager();
        this.c.a(this.g);
        this.c.a((RewardedInterstitialListener) this.g);
        this.d = new OfferwallManager();
        this.d.a((InternalOfferwallListener) this.g);
    }

    private boolean s() {
        ServerResponseWrapper serverResponseWrapper = this.k;
        return (serverResponseWrapper == null || serverResponseWrapper.a() == null || this.k.a().b() == null) ? false : true;
    }

    private boolean t() {
        ServerResponseWrapper serverResponseWrapper = this.k;
        return (serverResponseWrapper == null || serverResponseWrapper.a() == null || this.k.a().c() == null) ? false : true;
    }

    private boolean u() {
        ServerResponseWrapper serverResponseWrapper = this.k;
        return (serverResponseWrapper == null || serverResponseWrapper.a() == null || this.k.a().d() == null) ? false : true;
    }

    private boolean v() {
        ServerResponseWrapper serverResponseWrapper = this.k;
        return (serverResponseWrapper == null || serverResponseWrapper.a() == null || this.k.a().e() == null) ? false : true;
    }

    private void w() {
        ProviderSettings b2;
        synchronized (this.H) {
            long a2 = this.k.a().b().a();
            int d2 = this.k.a().b().d();
            int b3 = this.k.a().b().b();
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < this.k.c().a().size(); i2++) {
                String str = this.k.c().a().get(i2);
                if (!TextUtils.isEmpty(str) && (b2 = this.k.d().b(str)) != null) {
                    arrayList.add(b2);
                }
            }
            this.e = new BannerManager(arrayList, this.x, g(), h(), a2, d2, b3);
            if (this.H.booleanValue()) {
                this.H = false;
                a(this.I, this.J);
                this.I = null;
                this.J = null;
            }
        }
    }

    private void x() {
        synchronized (this.Q) {
            this.f.b(IronSourceLogger.IronSourceTag.INTERNAL, "Interstitial started in demand only mode", 0);
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < this.k.c().b().size(); i2++) {
                String str = this.k.c().b().get(i2);
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(this.k.d().b(str));
                }
            }
            if (arrayList.size() > 0) {
                this.S = new DemandOnlyIsManager(this.x, arrayList, this.k.a().c(), g(), h());
                Iterator<String> it2 = this.Q.iterator();
                while (it2.hasNext()) {
                    this.S.a(it2.next());
                }
                this.Q.clear();
            } else {
                JSONObject a2 = IronSourceUtils.a(false, false);
                a(a2, new Object[][]{new Object[]{"errorCode", 1010}});
                a(82314, a2);
                a(IronSource.AD_UNIT.INTERSTITIAL, false);
            }
        }
    }

    private void y() {
        synchronized (this.R) {
            this.f.b(IronSourceLogger.IronSourceTag.INTERNAL, "Rewarded Video started in demand only mode", 0);
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < this.k.c().e().size(); i2++) {
                String str = this.k.c().e().get(i2);
                if (!TextUtils.isEmpty(str)) {
                    arrayList.add(this.k.d().b(str));
                }
            }
            if (arrayList.size() > 0) {
                this.T = new DemandOnlyRvManager(this.x, arrayList, this.k.a().e(), g(), h());
                Iterator<String> it2 = this.R.iterator();
                while (it2.hasNext()) {
                    this.T.a(it2.next());
                }
                this.R.clear();
            } else {
                a(IronSource.AD_UNIT.REWARDED_VIDEO, false);
            }
        }
    }

    private void z() {
        ProviderSettings b2;
        if (this.E) {
            x();
            return;
        }
        this.O = this.k.a().c().f().d();
        a(82000, IronSourceUtils.a(false, this.O));
        if (this.O) {
            A();
            return;
        }
        int e2 = this.k.a().c().e();
        this.c.b(this.k.a().c().c());
        for (int i2 = 0; i2 < this.k.c().b().size(); i2++) {
            String str = this.k.c().b().get(i2);
            if (!TextUtils.isEmpty(str) && (b2 = this.k.d().b(str)) != null) {
                InterstitialSmash interstitialSmash = new InterstitialSmash(b2, e2);
                if (a((AbstractSmash) interstitialSmash)) {
                    interstitialSmash.a((InterstitialManagerListener) this.c);
                    interstitialSmash.a(i2 + 1);
                    this.c.a((AbstractSmash) interstitialSmash);
                }
            }
        }
        if (this.c.c.size() > 0) {
            this.c.a(this.k.a().c().d());
            this.c.a(this.x, g(), h());
            if (this.P) {
                this.P = false;
                this.c.h();
                return;
            }
            return;
        }
        JSONObject a2 = IronSourceUtils.a(false, false);
        a(a2, new Object[][]{new Object[]{"errorCode", 1010}});
        a(82314, a2);
        a(IronSource.AD_UNIT.INTERSTITIAL, false);
    }

    public void a(long j2) {
        JSONObject a2 = IronSourceUtils.a(this.D || this.E);
        try {
            a2.put("duration", j2);
            a2.put("sessionDepth", this.C);
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        RewardedVideoEventsManager.g().d(new EventData(514, a2));
    }

    /* access modifiers changed from: package-private */
    public synchronized AbstractAdapter b(String str) {
        try {
            if (this.f4628a != null && this.f4628a.getProviderName().equals(str)) {
                return this.f4628a;
            }
        } catch (Exception e2) {
            IronSourceLoggerManager ironSourceLoggerManager = this.f;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            ironSourceLoggerManager.b(ironSourceTag, "getOfferwallAdapter exception: " + e2, 1);
        }
        return null;
    }

    public void d(String str) {
        String str2 = "showInterstitial(" + str + ")";
        this.f.b(IronSourceLogger.IronSourceTag.API, str2, 1);
        try {
            if (this.E) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead", 3);
                this.g.b(new IronSourceError(510, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead"));
            } else if (!t()) {
                this.g.b(ErrorBuilder.a("showInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
            } else if (this.O) {
                i(str);
            } else {
                InterstitialPlacement h2 = h(str);
                JSONObject a2 = IronSourceUtils.a(false);
                if (h2 != null) {
                    try {
                        a2.put("placement", h2.c());
                    } catch (JSONException e2) {
                        e2.printStackTrace();
                    }
                } else if (!TextUtils.isEmpty(str)) {
                    a2.put("placement", str);
                }
                InterstitialEventsManager.g().d(new EventData(AdError.BROKEN_MEDIA_ERROR_CODE, a2));
                if (h2 != null) {
                    this.c.a(h2);
                    this.c.b(h2.c());
                }
            }
        } catch (Exception e3) {
            this.f.a(IronSourceLogger.IronSourceTag.API, str2, (Throwable) e3);
            this.g.b(new IronSourceError(510, e3.getMessage()));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized String e() {
        return this.q;
    }

    public synchronized String f() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public synchronized String i() {
        return this.p;
    }

    public synchronized String j() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public synchronized Map<String, String> k() {
        return this.r;
    }

    public synchronized String l() {
        return this.w;
    }

    public boolean m() {
        Throwable th;
        boolean z2;
        try {
            if (this.E) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use isISDemandOnlyInterstitialReady instead", 3);
                return false;
            }
            z2 = !this.O ? !(this.c == null || !this.c.g()) : !(this.M == null || !this.M.a());
            try {
                InterstitialEventsManager.g().d(new EventData(z2 ? 2101 : 2102, IronSourceUtils.a(false, this.O)));
                IronSourceLoggerManager ironSourceLoggerManager = this.f;
                IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager.b(ironSourceTag, "isInterstitialReady():" + z2, 1);
                return z2;
            } catch (Throwable th2) {
                th = th2;
                IronSourceLoggerManager ironSourceLoggerManager2 = this.f;
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.API;
                ironSourceLoggerManager2.b(ironSourceTag2, "isInterstitialReady():" + z2, 1);
                this.f.a(IronSourceLogger.IronSourceTag.API, "isInterstitialReady()", th);
                return false;
            }
        } catch (Throwable th3) {
            th = th3;
            z2 = false;
            IronSourceLoggerManager ironSourceLoggerManager22 = this.f;
            IronSourceLogger.IronSourceTag ironSourceTag22 = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager22.b(ironSourceTag22, "isInterstitialReady():" + z2, 1);
            this.f.a(IronSourceLogger.IronSourceTag.API, "isInterstitialReady()", th);
            return false;
        }
    }

    public void n() {
        this.f.b(IronSourceLogger.IronSourceTag.API, "loadInterstitial()", 1);
        try {
            if (this.E) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use loadISDemandOnlyInterstitial instead", 3);
                CallbackThrottler.b().a(ErrorBuilder.a("Interstitial was initialized in demand only mode. Use loadISDemandOnlyInterstitial instead", "Interstitial"));
            } else if (!this.F) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "init() must be called before loadInterstitial()", 3);
                CallbackThrottler.b().a(ErrorBuilder.a("init() must be called before loadInterstitial()", "Interstitial"));
            } else {
                MediationInitializer.EInitStatus a2 = MediationInitializer.d().a();
                if (a2 == MediationInitializer.EInitStatus.INIT_FAILED) {
                    this.f.b(IronSourceLogger.IronSourceTag.API, "init() had failed", 3);
                    CallbackThrottler.b().a(ErrorBuilder.a("init() had failed", "Interstitial"));
                } else if (a2 != MediationInitializer.EInitStatus.INIT_IN_PROGRESS) {
                    if (!(this.k == null || this.k.a() == null)) {
                        if (this.k.a().c() != null) {
                            if (!this.O) {
                                this.c.h();
                                return;
                            } else if (this.M == null) {
                                this.P = true;
                                return;
                            } else {
                                this.M.b();
                                return;
                            }
                        }
                    }
                    this.f.b(IronSourceLogger.IronSourceTag.API, "No interstitial configurations found", 3);
                    CallbackThrottler.b().a(ErrorBuilder.a("the server response does not contain interstitial data", "Interstitial"));
                } else if (MediationInitializer.d().b()) {
                    this.f.b(IronSourceLogger.IronSourceTag.API, "init() had failed", 3);
                    CallbackThrottler.b().a(ErrorBuilder.a("init() had failed", "Interstitial"));
                } else {
                    this.P = true;
                }
            }
        } catch (Throwable th) {
            this.f.a(IronSourceLogger.IronSourceTag.API, "loadInterstitial()", th);
            CallbackThrottler.b().a(new IronSourceError(510, th.getMessage()));
        }
    }

    public void o() {
        this.f.b(IronSourceLogger.IronSourceTag.API, "showInterstitial()", 1);
        try {
            if (this.E) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead", 3);
                this.g.b(new IronSourceError(510, "Interstitial was initialized in demand only mode. Use showISDemandOnlyInterstitial instead"));
            } else if (!t()) {
                this.g.b(ErrorBuilder.a("showInterstitial can't be called before the Interstitial ad unit initialization completed successfully", "Interstitial"));
            } else {
                InterstitialPlacement p2 = p();
                if (p2 != null) {
                    d(p2.c());
                } else {
                    this.g.b(new IronSourceError(1020, "showInterstitial error: empty default placement in response"));
                }
            }
        } catch (Exception e2) {
            this.f.a(IronSourceLogger.IronSourceTag.API, "showInterstitial()", (Throwable) e2);
            this.g.b(new IronSourceError(510, e2.getMessage()));
        }
    }

    private BannerPlacement e(String str) {
        BannerConfigurations b2 = this.k.a().b();
        if (b2 == null) {
            return null;
        }
        if (TextUtils.isEmpty(str)) {
            return b2.e();
        }
        BannerPlacement a2 = b2.a(str);
        if (a2 != null) {
            return a2;
        }
        return b2.e();
    }

    private CappingManager.ECappingStatus f(String str) {
        ServerResponseWrapper serverResponseWrapper = this.k;
        if (serverResponseWrapper == null || serverResponseWrapper.a() == null || this.k.a().c() == null) {
            return CappingManager.ECappingStatus.NOT_CAPPED;
        }
        InterstitialPlacement interstitialPlacement = null;
        try {
            interstitialPlacement = g(str);
            if (interstitialPlacement == null && (interstitialPlacement = p()) == null) {
                this.f.b(IronSourceLogger.IronSourceTag.API, "Default placement was not found", 3);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        if (interstitialPlacement == null) {
            return CappingManager.ECappingStatus.NOT_CAPPED;
        }
        return CappingManager.c((Context) this.x, interstitialPlacement);
    }

    private void i(String str) {
        String str2 = null;
        try {
            InterstitialPlacement g2 = g(str);
            if (g2 == null) {
                g2 = p();
            }
            if (g2 != null) {
                str2 = g2.c();
            }
        } catch (Exception e2) {
            this.f.a(IronSourceLogger.IronSourceTag.API, "showProgrammaticInterstitial()", (Throwable) e2);
        }
        this.M.a(str2);
    }

    private boolean j(String str) {
        if (str == null) {
            return false;
        }
        return str.matches("^[a-zA-Z0-9]*$");
    }

    private ConfigValidationResult k(String str) {
        ConfigValidationResult configValidationResult = new ConfigValidationResult();
        if (str == null) {
            configValidationResult.a(new IronSourceError(506, "Init Fail - appKey is missing"));
        } else if (!a(str, 5, 10)) {
            configValidationResult.a(ErrorBuilder.a("appKey", str, "length should be between 5-10 characters"));
        } else if (!j(str)) {
            configValidationResult.a(ErrorBuilder.a("appKey", str, "should contain only english characters and numbers"));
        }
        return configValidationResult;
    }

    public synchronized String g() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(String str) {
        this.m = str;
    }

    public void b(Activity activity) {
        try {
            this.x = activity;
            this.f.b(IronSourceLogger.IronSourceTag.API, "onResume()", 1);
            if (this.b != null) {
                this.b.b(activity);
            }
            if (this.c != null) {
                this.c.b(activity);
            }
            if (this.e != null) {
                this.e.b(activity);
            }
            if (this.L != null) {
                this.L.b(activity);
            }
            if (this.M != null) {
                this.M.b(activity);
            }
            if (this.S != null) {
                this.S.b(activity);
            }
            if (this.T != null) {
                this.T.b(activity);
            }
        } catch (Throwable th) {
            this.f.a(IronSourceLogger.IronSourceTag.API, "onResume()", th);
        }
    }

    /* access modifiers changed from: package-private */
    public Boolean c() {
        return this.K;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:62:0x013e, code lost:
        return;
     */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0077 A[Catch:{ Exception -> 0x00b9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0083 A[SYNTHETIC, Splitter:B:28:0x0083] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void a(android.app.Activity r8, java.lang.String r9, boolean r10, com.ironsource.mediationsdk.IronSource.AD_UNIT... r11) {
        /*
            r7 = this;
            monitor-enter(r7)
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.t     // Catch:{ all -> 0x013f }
            if (r0 == 0) goto L_0x012d
            java.util.concurrent.atomic.AtomicBoolean r0 = r7.t     // Catch:{ all -> 0x013f }
            r1 = 0
            r2 = 1
            boolean r0 = r0.compareAndSet(r2, r1)     // Catch:{ all -> 0x013f }
            if (r0 == 0) goto L_0x012d
            if (r11 == 0) goto L_0x0042
            int r0 = r11.length     // Catch:{ all -> 0x013f }
            if (r0 != 0) goto L_0x0015
            goto L_0x0042
        L_0x0015:
            int r0 = r11.length     // Catch:{ all -> 0x013f }
            r3 = 0
        L_0x0017:
            if (r3 >= r0) goto L_0x0058
            r4 = r11[r3]     // Catch:{ all -> 0x013f }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r5 = r7.y     // Catch:{ all -> 0x013f }
            r5.add(r4)     // Catch:{ all -> 0x013f }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r5 = r7.z     // Catch:{ all -> 0x013f }
            r5.add(r4)     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.INTERSTITIAL     // Catch:{ all -> 0x013f }
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x013f }
            if (r5 == 0) goto L_0x002f
            r7.F = r2     // Catch:{ all -> 0x013f }
        L_0x002f:
            com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.BANNER     // Catch:{ all -> 0x013f }
            boolean r5 = r4.equals(r5)     // Catch:{ all -> 0x013f }
            if (r5 == 0) goto L_0x0039
            r7.G = r2     // Catch:{ all -> 0x013f }
        L_0x0039:
            com.ironsource.mediationsdk.IronSource$AD_UNIT r5 = com.ironsource.mediationsdk.IronSource.AD_UNIT.REWARDED_VIDEO     // Catch:{ all -> 0x013f }
            boolean r4 = r4.equals(r5)     // Catch:{ all -> 0x013f }
            int r3 = r3 + 1
            goto L_0x0017
        L_0x0042:
            com.ironsource.mediationsdk.IronSource$AD_UNIT[] r0 = com.ironsource.mediationsdk.IronSource.AD_UNIT.values()     // Catch:{ all -> 0x013f }
            int r3 = r0.length     // Catch:{ all -> 0x013f }
            r4 = 0
        L_0x0048:
            if (r4 >= r3) goto L_0x0054
            r5 = r0[r4]     // Catch:{ all -> 0x013f }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r6 = r7.y     // Catch:{ all -> 0x013f }
            r6.add(r5)     // Catch:{ all -> 0x013f }
            int r4 = r4 + 1
            goto L_0x0048
        L_0x0054:
            r7.F = r2     // Catch:{ all -> 0x013f }
            r7.G = r2     // Catch:{ all -> 0x013f }
        L_0x0058:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r7.f     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r3 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x013f }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x013f }
            r4.<init>()     // Catch:{ all -> 0x013f }
            java.lang.String r5 = "init(appKey:"
            r4.append(r5)     // Catch:{ all -> 0x013f }
            r4.append(r9)     // Catch:{ all -> 0x013f }
            java.lang.String r5 = ")"
            r4.append(r5)     // Catch:{ all -> 0x013f }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x013f }
            r0.b(r3, r4, r2)     // Catch:{ all -> 0x013f }
            if (r8 != 0) goto L_0x0083
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r8 = r7.f     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r9 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x013f }
            java.lang.String r10 = "Init Fail - provided activity is null"
            r11 = 2
            r8.b(r9, r10, r11)     // Catch:{ all -> 0x013f }
            monitor-exit(r7)
            return
        L_0x0083:
            r7.x = r8     // Catch:{ all -> 0x013f }
            r7.c((android.app.Activity) r8)     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.config.ConfigValidationResult r0 = r7.k(r9)     // Catch:{ all -> 0x013f }
            boolean r3 = r0.b()     // Catch:{ all -> 0x013f }
            if (r3 == 0) goto L_0x00f1
            r7.l = r9     // Catch:{ all -> 0x013f }
            boolean r0 = r7.A     // Catch:{ all -> 0x013f }
            if (r0 == 0) goto L_0x00cd
            org.json.JSONObject r10 = com.ironsource.mediationsdk.utils.IronSourceUtils.a((boolean) r10)     // Catch:{ all -> 0x013f }
            if (r11 == 0) goto L_0x00ae
            int r0 = r11.length     // Catch:{ Exception -> 0x00b9 }
            r3 = 0
        L_0x00a0:
            if (r3 >= r0) goto L_0x00ae
            r4 = r11[r3]     // Catch:{ Exception -> 0x00b9 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00b9 }
            r10.put(r4, r2)     // Catch:{ Exception -> 0x00b9 }
            int r3 = r3 + 1
            goto L_0x00a0
        L_0x00ae:
            java.lang.String r0 = "sessionDepth"
            int r3 = r7.C     // Catch:{ Exception -> 0x00b9 }
            int r3 = r3 + r2
            r7.C = r3     // Catch:{ Exception -> 0x00b9 }
            r10.put(r0, r3)     // Catch:{ Exception -> 0x00b9 }
            goto L_0x00bd
        L_0x00b9:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ all -> 0x013f }
        L_0x00bd:
            com.ironsource.eventsmodule.EventData r0 = new com.ironsource.eventsmodule.EventData     // Catch:{ all -> 0x013f }
            r2 = 14
            r0.<init>(r2, r10)     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.events.RewardedVideoEventsManager r10 = com.ironsource.mediationsdk.events.RewardedVideoEventsManager.g()     // Catch:{ all -> 0x013f }
            r10.d((com.ironsource.eventsmodule.EventData) r0)     // Catch:{ all -> 0x013f }
            r7.A = r1     // Catch:{ all -> 0x013f }
        L_0x00cd:
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r10 = r7.y     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r0 = com.ironsource.mediationsdk.IronSource.AD_UNIT.INTERSTITIAL     // Catch:{ all -> 0x013f }
            boolean r10 = r10.contains(r0)     // Catch:{ all -> 0x013f }
            if (r10 == 0) goto L_0x00e0
            com.ironsource.mediationsdk.MediationInitializer r10 = com.ironsource.mediationsdk.MediationInitializer.d()     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.InterstitialManager r0 = r7.c     // Catch:{ all -> 0x013f }
            r10.a((com.ironsource.mediationsdk.MediationInitializer.OnMediationInitializationListener) r0)     // Catch:{ all -> 0x013f }
        L_0x00e0:
            com.ironsource.mediationsdk.MediationInitializer r10 = com.ironsource.mediationsdk.MediationInitializer.d()     // Catch:{ all -> 0x013f }
            r10.a((com.ironsource.mediationsdk.MediationInitializer.OnMediationInitializationListener) r7)     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.MediationInitializer r10 = com.ironsource.mediationsdk.MediationInitializer.d()     // Catch:{ all -> 0x013f }
            java.lang.String r0 = r7.m     // Catch:{ all -> 0x013f }
            r10.a(r8, r9, r0, r11)     // Catch:{ all -> 0x013f }
            goto L_0x013d
        L_0x00f1:
            com.ironsource.mediationsdk.MediationInitializer r8 = com.ironsource.mediationsdk.MediationInitializer.d()     // Catch:{ all -> 0x013f }
            r8.c()     // Catch:{ all -> 0x013f }
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r8 = r7.y     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r9 = com.ironsource.mediationsdk.IronSource.AD_UNIT.REWARDED_VIDEO     // Catch:{ all -> 0x013f }
            boolean r8 = r8.contains(r9)     // Catch:{ all -> 0x013f }
            if (r8 == 0) goto L_0x0107
            com.ironsource.mediationsdk.sdk.ListenersWrapper r8 = r7.g     // Catch:{ all -> 0x013f }
            r8.a((boolean) r1)     // Catch:{ all -> 0x013f }
        L_0x0107:
            java.util.Set<com.ironsource.mediationsdk.IronSource$AD_UNIT> r8 = r7.y     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.IronSource$AD_UNIT r9 = com.ironsource.mediationsdk.IronSource.AD_UNIT.OFFERWALL     // Catch:{ all -> 0x013f }
            boolean r8 = r8.contains(r9)     // Catch:{ all -> 0x013f }
            if (r8 == 0) goto L_0x011a
            com.ironsource.mediationsdk.sdk.ListenersWrapper r8 = r7.g     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.logger.IronSourceError r9 = r0.a()     // Catch:{ all -> 0x013f }
            r8.a(r1, r9)     // Catch:{ all -> 0x013f }
        L_0x011a:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r8 = com.ironsource.mediationsdk.logger.IronSourceLoggerManager.c()     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r9 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.logger.IronSourceError r10 = r0.a()     // Catch:{ all -> 0x013f }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x013f }
            r8.b(r9, r10, r2)     // Catch:{ all -> 0x013f }
            monitor-exit(r7)
            return
        L_0x012d:
            if (r11 == 0) goto L_0x0133
            r7.a((boolean) r10, (com.ironsource.mediationsdk.IronSource.AD_UNIT[]) r11)     // Catch:{ all -> 0x013f }
            goto L_0x013d
        L_0x0133:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r8 = r7.f     // Catch:{ all -> 0x013f }
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r9 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API     // Catch:{ all -> 0x013f }
            java.lang.String r10 = "Multiple calls to init without ad units are not allowed"
            r11 = 3
            r8.b(r9, r10, r11)     // Catch:{ all -> 0x013f }
        L_0x013d:
            monitor-exit(r7)
            return
        L_0x013f:
            r8 = move-exception
            monitor-exit(r7)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.a(android.app.Activity, java.lang.String, boolean, com.ironsource.mediationsdk.IronSource$AD_UNIT[]):void");
    }

    public synchronized String h() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public ServerResponseWrapper d() {
        return this.k;
    }

    public synchronized Integer b() {
        return this.n;
    }

    public void b(IronSourceBannerLayout ironSourceBannerLayout) {
        a(ironSourceBannerLayout, "");
    }

    private ServerResponseWrapper b(Context context, String str, IResponseListener iResponseListener) {
        ServerResponseWrapper serverResponseWrapper;
        if (!IronSourceUtils.c(context)) {
            return null;
        }
        try {
            String a2 = a(context);
            if (TextUtils.isEmpty(a2)) {
                a2 = DeviceStatus.j(context);
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "using custom identifier", 1);
            }
            String a3 = HttpFunctions.a(ServerURL.a(context, g(), str, a2, j(), this.B != null ? this.B.f() : null), iResponseListener);
            if (a3 == null) {
                return null;
            }
            if (IronSourceUtils.c() == 1) {
                String optString = new JSONObject(a3).optString("response", (String) null);
                if (TextUtils.isEmpty(optString)) {
                    return null;
                }
                a3 = IronSourceAES.a("C38FB23A402222A0C17D34A92F971D1F", optString);
            }
            serverResponseWrapper = new ServerResponseWrapper(context, g(), str, a3);
            try {
                if (!serverResponseWrapper.g()) {
                    return null;
                }
                return serverResponseWrapper;
            } catch (Exception e2) {
                e = e2;
                e.printStackTrace();
                return serverResponseWrapper;
            }
        } catch (Exception e3) {
            e = e3;
            serverResponseWrapper = null;
            e.printStackTrace();
            return serverResponseWrapper;
        }
    }

    private void b(ServerResponseWrapper serverResponseWrapper, Context context) {
        a(serverResponseWrapper);
        a(serverResponseWrapper, context);
    }

    private void b(int i2, JSONObject jSONObject) {
        RewardedVideoEventsManager.g().d(new EventData(i2, jSONObject));
    }

    private synchronized void a(boolean z2, IronSource.AD_UNIT... ad_unitArr) {
        int i2 = 0;
        for (IronSource.AD_UNIT ad_unit : ad_unitArr) {
            if (ad_unit.equals(IronSource.AD_UNIT.INTERSTITIAL)) {
                this.F = true;
            } else if (ad_unit.equals(IronSource.AD_UNIT.BANNER)) {
                this.G = true;
            }
        }
        if (MediationInitializer.d().a() == MediationInitializer.EInitStatus.INIT_FAILED) {
            try {
                if (this.g != null) {
                    int length = ad_unitArr.length;
                    while (i2 < length) {
                        IronSource.AD_UNIT ad_unit2 = ad_unitArr[i2];
                        if (!this.y.contains(ad_unit2)) {
                            a(ad_unit2, true);
                        }
                        i2++;
                    }
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        } else if (!this.u) {
            JSONObject a2 = IronSourceUtils.a(z2);
            int length2 = ad_unitArr.length;
            boolean z3 = false;
            while (i2 < length2) {
                IronSource.AD_UNIT ad_unit3 = ad_unitArr[i2];
                if (!this.y.contains(ad_unit3)) {
                    this.y.add(ad_unit3);
                    this.z.add(ad_unit3);
                    try {
                        a2.put(ad_unit3.toString(), true);
                    } catch (Exception e3) {
                        e3.printStackTrace();
                    }
                    z3 = true;
                } else {
                    this.f.b(IronSourceLogger.IronSourceTag.API, ad_unit3 + " ad unit has started initializing.", 3);
                }
                i2++;
            }
            if (z3) {
                try {
                    int i3 = this.C + 1;
                    this.C = i3;
                    a2.put("sessionDepth", i3);
                } catch (Exception e4) {
                    e4.printStackTrace();
                }
                RewardedVideoEventsManager.g().d(new EventData(14, a2));
            }
        } else if (this.v != null) {
            JSONObject a3 = IronSourceUtils.a(z2);
            boolean z4 = false;
            for (IronSource.AD_UNIT ad_unit4 : ad_unitArr) {
                if (!this.y.contains(ad_unit4)) {
                    this.y.add(ad_unit4);
                    this.z.add(ad_unit4);
                    try {
                        a3.put(ad_unit4.toString(), true);
                    } catch (Exception e5) {
                        e5.printStackTrace();
                    }
                    if (this.v == null || !this.v.contains(ad_unit4)) {
                        a(ad_unit4, false);
                    } else {
                        a(ad_unit4);
                    }
                    z4 = true;
                } else {
                    this.f.b(IronSourceLogger.IronSourceTag.API, ad_unit4 + " ad unit has already been initialized", 3);
                }
            }
            if (z4) {
                try {
                    int i4 = this.C + 1;
                    this.C = i4;
                    a3.put("sessionDepth", i4);
                } catch (Exception e6) {
                    e6.printStackTrace();
                }
                RewardedVideoEventsManager.g().d(new EventData(14, a3));
            }
        }
    }

    public void a(List<IronSource.AD_UNIT> list, boolean z2) {
        try {
            this.v = list;
            this.u = true;
            this.f.b(IronSourceLogger.IronSourceTag.API, "onInitSuccess()", 1);
            IronSourceUtils.g("init success");
            if (z2) {
                JSONObject a2 = IronSourceUtils.a(false);
                try {
                    a2.put("revived", true);
                } catch (JSONException e2) {
                    e2.printStackTrace();
                }
                RewardedVideoEventsManager.g().d(new EventData(114, a2));
            }
            InterstitialEventsManager.g().f();
            RewardedVideoEventsManager.g().f();
            AdapterRepository.a().a(g(), h());
            for (IronSource.AD_UNIT ad_unit : IronSource.AD_UNIT.values()) {
                if (this.y.contains(ad_unit)) {
                    if (list.contains(ad_unit)) {
                        a(ad_unit);
                    } else {
                        a(ad_unit, false);
                    }
                }
            }
        } catch (Exception e3) {
            e3.printStackTrace();
        }
    }

    private void a(IronSource.AD_UNIT ad_unit) {
        int i2 = AnonymousClass1.f4629a[ad_unit.ordinal()];
        if (i2 == 1) {
            C();
        } else if (i2 == 2) {
            z();
        } else if (i2 == 3) {
            this.d.a(this.x, g(), h());
        } else if (i2 == 4) {
            w();
        }
    }

    private boolean a(AbstractSmash abstractSmash) {
        return abstractSmash.r() >= 1 && abstractSmash.s() >= 1;
    }

    public void a(String str) {
        try {
            IronSourceLoggerManager ironSourceLoggerManager = this.f;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.API;
            ironSourceLoggerManager.b(ironSourceTag, "onInitFailed(reason:" + str + ")", 1);
            IronSourceUtils.g("Mediation init failed");
            if (this.g != null) {
                for (IronSource.AD_UNIT a2 : this.y) {
                    a(a2, true);
                }
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }

    public void a() {
        synchronized (this.H) {
            if (this.H.booleanValue()) {
                this.H = false;
                BannerCallbackThrottler.b().a(this.I, new IronSourceError(603, "init had failed"));
                this.I = null;
                this.J = null;
            }
        }
        if (this.P) {
            this.P = false;
            CallbackThrottler.b().a(ErrorBuilder.a("init() had failed", "Interstitial"));
        }
        synchronized (this.Q) {
            Iterator<String> it2 = this.Q.iterator();
            while (it2.hasNext()) {
                ISDemandOnlyListenerWrapper.a().a(it2.next(), ErrorBuilder.a("init() had failed", "Interstitial"));
            }
            this.Q.clear();
        }
        synchronized (this.R) {
            Iterator<String> it3 = this.R.iterator();
            while (it3.hasNext()) {
                RVDemandOnlyListenerWrapper.a().a(it3.next(), ErrorBuilder.a("init() had failed", "Rewarded Video"));
            }
            this.R.clear();
        }
    }

    private void a(IronSource.AD_UNIT ad_unit, boolean z2) {
        int i2 = AnonymousClass1.f4629a[ad_unit.ordinal()];
        if (i2 != 1) {
            if (i2 != 2) {
                if (i2 != 3) {
                    if (i2 == 4) {
                        synchronized (this.H) {
                            if (this.H.booleanValue()) {
                                this.H = false;
                                BannerCallbackThrottler.b().a(this.I, new IronSourceError(602, "Init had failed"));
                                this.I = null;
                                this.J = null;
                            }
                        }
                    }
                } else if (z2 || u() || this.z.contains(ad_unit)) {
                    this.g.b(false);
                }
            } else if (this.E) {
                Iterator<String> it2 = this.Q.iterator();
                while (it2.hasNext()) {
                    ISDemandOnlyListenerWrapper.a().a(it2.next(), ErrorBuilder.a("initISDemandOnly() had failed", "Interstitial"));
                }
                this.Q.clear();
            } else if (this.P) {
                this.P = false;
                CallbackThrottler.b().a(ErrorBuilder.a("init() had failed", "Interstitial"));
            }
        } else if (this.D) {
            Iterator<String> it3 = this.R.iterator();
            while (it3.hasNext()) {
                RVDemandOnlyListenerWrapper.a().a(it3.next(), ErrorBuilder.a("initISDemandOnly() had failed", "Rewarded Video"));
            }
            this.R.clear();
        } else if (z2 || v() || this.z.contains(ad_unit)) {
            this.g.a(false);
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(AbstractAdapter abstractAdapter) {
        this.f4628a = abstractAdapter;
    }

    public void a(Activity activity) {
        try {
            this.f.b(IronSourceLogger.IronSourceTag.API, "onPause()", 1);
            if (this.b != null) {
                this.b.a(activity);
            }
            if (this.c != null) {
                this.c.a(activity);
            }
            if (this.e != null) {
                this.e.a(activity);
            }
            if (this.L != null) {
                this.L.a(activity);
            }
            if (this.M != null) {
                this.M.a(activity);
            }
            if (this.S != null) {
                this.S.a(activity);
            }
            if (this.T != null) {
                this.T.a(activity);
            }
        } catch (Throwable th) {
            this.f.a(IronSourceLogger.IronSourceTag.API, "onPause()", th);
        }
    }

    public void a(InterstitialListener interstitialListener) {
        if (interstitialListener == null) {
            this.f.b(IronSourceLogger.IronSourceTag.API, "setInterstitialListener(ISListener:null)", 1);
        } else {
            this.f.b(IronSourceLogger.IronSourceTag.API, "setInterstitialListener(ISListener)", 1);
        }
        this.g.a(interstitialListener);
        ISListenerWrapper.f().a(interstitialListener);
        CallbackThrottler.b().a(interstitialListener);
    }

    public IronSourceBannerLayout a(Activity activity, ISBannerSize iSBannerSize) {
        this.f.b(IronSourceLogger.IronSourceTag.API, "createBanner()", 1);
        if (activity != null) {
            return new IronSourceBannerLayout(activity, iSBannerSize);
        }
        this.f.b(IronSourceLogger.IronSourceTag.API, "createBanner() : Activity cannot be null", 3);
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x00d9, code lost:
        r0 = r5.k;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x00db, code lost:
        if (r0 == null) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00e1, code lost:
        if (r0.a() == null) goto L_0x00fa;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00ed, code lost:
        if (r5.k.a().b() != null) goto L_0x00f0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x00f0, code lost:
        r5.e.a(r6, e(r7));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f9, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00fa, code lost:
        r5.f.b(com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API, "No banner configurations found", 3);
        com.ironsource.mediationsdk.BannerCallbackThrottler.b().a(r6, new com.ironsource.mediationsdk.logger.IronSourceError(615, "No banner configurations found"));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0113, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.ironsource.mediationsdk.IronSourceBannerLayout r6, java.lang.String r7) {
        /*
            r5 = this;
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r0 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r1 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "loadBanner("
            r2.append(r3)
            r2.append(r7)
            java.lang.String r3 = ")"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            r3 = 1
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)
            r0.b(r1, r2, r3)
            if (r6 != 0) goto L_0x002e
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r0 = "loadBanner can't be called with a null parameter"
            r6.b(r7, r0, r3)
            return
        L_0x002e:
            boolean r0 = r5.G
            r1 = 3
            if (r0 != 0) goto L_0x003d
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r6 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r7 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r0 = "init() must be called before loadBanner()"
            r6.b(r7, r0, r1)
            return
        L_0x003d:
            com.ironsource.mediationsdk.ISBannerSize r0 = r6.getSize()
            java.lang.String r0 = r0.a()
            java.lang.String r2 = "CUSTOM"
            boolean r0 = r0.equals(r2)
            if (r0 == 0) goto L_0x0078
            com.ironsource.mediationsdk.ISBannerSize r0 = r6.getSize()
            int r0 = r0.c()
            if (r0 <= 0) goto L_0x0061
            com.ironsource.mediationsdk.ISBannerSize r0 = r6.getSize()
            int r0 = r0.b()
            if (r0 > 0) goto L_0x0078
        L_0x0061:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "loadBanner: Unsupported banner size. Height and width must be bigger than 0"
            r7.b(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r7 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()
            java.lang.String r0 = ""
            com.ironsource.mediationsdk.logger.IronSourceError r0 = com.ironsource.mediationsdk.utils.ErrorBuilder.g(r0)
            r7.a(r6, r0)
            return
        L_0x0078:
            com.ironsource.mediationsdk.MediationInitializer r0 = com.ironsource.mediationsdk.MediationInitializer.d()
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r0 = r0.a()
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r2 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_FAILED
            if (r0 != r2) goto L_0x009e
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "init() had failed"
            r7.b(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r7 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()
            com.ironsource.mediationsdk.logger.IronSourceError r0 = new com.ironsource.mediationsdk.logger.IronSourceError
            r1 = 600(0x258, float:8.41E-43)
            java.lang.String r2 = "Init() had failed"
            r0.<init>(r1, r2)
            r7.a(r6, r0)
            return
        L_0x009e:
            com.ironsource.mediationsdk.MediationInitializer$EInitStatus r2 = com.ironsource.mediationsdk.MediationInitializer.EInitStatus.INIT_IN_PROGRESS
            if (r0 != r2) goto L_0x00cd
            com.ironsource.mediationsdk.MediationInitializer r0 = com.ironsource.mediationsdk.MediationInitializer.d()
            boolean r0 = r0.b()
            if (r0 == 0) goto L_0x00c6
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "init() had failed"
            r7.b(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r7 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()
            com.ironsource.mediationsdk.logger.IronSourceError r0 = new com.ironsource.mediationsdk.logger.IronSourceError
            r1 = 601(0x259, float:8.42E-43)
            java.lang.String r2 = "Init had failed"
            r0.<init>(r1, r2)
            r7.a(r6, r0)
            goto L_0x00cc
        L_0x00c6:
            r5.I = r6
            r5.H = r4
            r5.J = r7
        L_0x00cc:
            return
        L_0x00cd:
            java.lang.Boolean r0 = r5.H
            monitor-enter(r0)
            com.ironsource.mediationsdk.BannerManager r2 = r5.e     // Catch:{ all -> 0x0114 }
            if (r2 != 0) goto L_0x00d8
            r5.H = r4     // Catch:{ all -> 0x0114 }
            monitor-exit(r0)     // Catch:{ all -> 0x0114 }
            return
        L_0x00d8:
            monitor-exit(r0)     // Catch:{ all -> 0x0114 }
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.k
            if (r0 == 0) goto L_0x00fa
            com.ironsource.mediationsdk.model.Configurations r0 = r0.a()
            if (r0 == 0) goto L_0x00fa
            com.ironsource.mediationsdk.utils.ServerResponseWrapper r0 = r5.k
            com.ironsource.mediationsdk.model.Configurations r0 = r0.a()
            com.ironsource.mediationsdk.model.BannerConfigurations r0 = r0.b()
            if (r0 != 0) goto L_0x00f0
            goto L_0x00fa
        L_0x00f0:
            com.ironsource.mediationsdk.BannerManager r0 = r5.e
            com.ironsource.mediationsdk.model.BannerPlacement r7 = r5.e(r7)
            r0.a((com.ironsource.mediationsdk.IronSourceBannerLayout) r6, (com.ironsource.mediationsdk.model.BannerPlacement) r7)
            return
        L_0x00fa:
            com.ironsource.mediationsdk.logger.IronSourceLoggerManager r7 = r5.f
            com.ironsource.mediationsdk.logger.IronSourceLogger$IronSourceTag r0 = com.ironsource.mediationsdk.logger.IronSourceLogger.IronSourceTag.API
            java.lang.String r2 = "No banner configurations found"
            r7.b(r0, r2, r1)
            com.ironsource.mediationsdk.BannerCallbackThrottler r7 = com.ironsource.mediationsdk.BannerCallbackThrottler.b()
            com.ironsource.mediationsdk.logger.IronSourceError r0 = new com.ironsource.mediationsdk.logger.IronSourceError
            r1 = 615(0x267, float:8.62E-43)
            java.lang.String r2 = "No banner configurations found"
            r0.<init>(r1, r2)
            r7.a(r6, r0)
            return
        L_0x0114:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0114 }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.mediationsdk.IronSourceObject.a(com.ironsource.mediationsdk.IronSourceBannerLayout, java.lang.String):void");
    }

    public void a(IronSourceBannerLayout ironSourceBannerLayout) {
        this.f.b(IronSourceLogger.IronSourceTag.API, "destroyBanner()", 1);
        try {
            if (this.e != null) {
                this.e.a(ironSourceBannerLayout);
            }
        } catch (Throwable th) {
            this.f.a(IronSourceLogger.IronSourceTag.API, "destroyBanner()", th);
        }
    }

    /* access modifiers changed from: package-private */
    public ServerResponseWrapper a(Context context, String str, IResponseListener iResponseListener) {
        synchronized (this.j) {
            if (this.k != null) {
                ServerResponseWrapper serverResponseWrapper = new ServerResponseWrapper(this.k);
                return serverResponseWrapper;
            }
            ServerResponseWrapper b2 = b(context, str, iResponseListener);
            if (b2 == null || !b2.g()) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "Null or invalid response. Trying to get cached response", 0);
                b2 = a(context, str);
            }
            if (b2 != null) {
                this.k = b2;
                IronSourceUtils.b(context, b2.toString());
                b(this.k, context);
            }
            InterstitialEventsManager.g().a(true);
            RewardedVideoEventsManager.g().a(true);
            return b2;
        }
    }

    private ServerResponseWrapper a(Context context, String str) {
        JSONObject jSONObject;
        try {
            jSONObject = new JSONObject(IronSourceUtils.b(context));
        } catch (JSONException unused) {
            jSONObject = new JSONObject();
        }
        String optString = jSONObject.optString("appKey");
        String optString2 = jSONObject.optString("userId");
        String optString3 = jSONObject.optString("response");
        if (TextUtils.isEmpty(optString) || TextUtils.isEmpty(optString2) || TextUtils.isEmpty(optString3) || g() == null || !optString.equals(g()) || !optString2.equals(str)) {
            return null;
        }
        ServerResponseWrapper serverResponseWrapper = new ServerResponseWrapper(context, optString, optString2, optString3);
        IronSourceError c2 = ErrorBuilder.c(optString, optString2);
        this.f.b(IronSourceLogger.IronSourceTag.INTERNAL, c2.toString(), 1);
        IronSourceLoggerManager ironSourceLoggerManager = this.f;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        ironSourceLoggerManager.b(ironSourceTag, c2.toString() + ": " + serverResponseWrapper.toString(), 1);
        RewardedVideoEventsManager.g().d(new EventData(140, IronSourceUtils.a(false)));
        return serverResponseWrapper;
    }

    private void a(ServerResponseWrapper serverResponseWrapper, Context context) {
        boolean g2 = v() ? serverResponseWrapper.a().e().h().g() : false;
        boolean g3 = t() ? serverResponseWrapper.a().c().g().g() : false;
        boolean g4 = s() ? serverResponseWrapper.a().b().c().g() : false;
        boolean g5 = u() ? serverResponseWrapper.a().d().a().g() : false;
        if (g2) {
            RewardedVideoEventsManager.g().b(serverResponseWrapper.a().e().h().b(), context);
            RewardedVideoEventsManager.g().a(serverResponseWrapper.a().e().h().c(), context);
            RewardedVideoEventsManager.g().d(serverResponseWrapper.a().e().h().e());
            RewardedVideoEventsManager.g().c(serverResponseWrapper.a().e().h().d());
            RewardedVideoEventsManager.g().b(serverResponseWrapper.a().e().h().a());
            RewardedVideoEventsManager.g().a(serverResponseWrapper.a().e().h().f(), context);
            RewardedVideoEventsManager.g().a(serverResponseWrapper.a().a().c());
        } else if (g5) {
            RewardedVideoEventsManager.g().b(serverResponseWrapper.a().d().a().b(), context);
            RewardedVideoEventsManager.g().a(serverResponseWrapper.a().d().a().c(), context);
            RewardedVideoEventsManager.g().d(serverResponseWrapper.a().d().a().e());
            RewardedVideoEventsManager.g().c(serverResponseWrapper.a().d().a().d());
            RewardedVideoEventsManager.g().b(serverResponseWrapper.a().d().a().a());
            RewardedVideoEventsManager.g().a(serverResponseWrapper.a().d().a().f(), context);
            RewardedVideoEventsManager.g().a(serverResponseWrapper.a().a().c());
        } else {
            RewardedVideoEventsManager.g().b(false);
        }
        if (g3) {
            InterstitialEventsManager.g().b(serverResponseWrapper.a().c().g().b(), context);
            InterstitialEventsManager.g().a(serverResponseWrapper.a().c().g().c(), context);
            InterstitialEventsManager.g().d(serverResponseWrapper.a().c().g().e());
            InterstitialEventsManager.g().c(serverResponseWrapper.a().c().g().d());
            InterstitialEventsManager.g().b(serverResponseWrapper.a().c().g().a());
            InterstitialEventsManager.g().a(serverResponseWrapper.a().c().g().f(), context);
            InterstitialEventsManager.g().a(serverResponseWrapper.a().a().c());
        } else if (g4) {
            ApplicationEvents c2 = serverResponseWrapper.a().b().c();
            InterstitialEventsManager.g().b(c2.b(), context);
            InterstitialEventsManager.g().a(c2.c(), context);
            InterstitialEventsManager.g().d(c2.e());
            InterstitialEventsManager.g().c(c2.d());
            InterstitialEventsManager.g().b(c2.a());
            InterstitialEventsManager.g().a(c2.f(), context);
            InterstitialEventsManager.g().a(serverResponseWrapper.a().a().c());
        } else {
            InterstitialEventsManager.g().b(false);
        }
    }

    private void a(ServerResponseWrapper serverResponseWrapper) {
        this.h.a(serverResponseWrapper.a().a().b().b());
        this.f.a("console", serverResponseWrapper.a().a().b().a());
    }

    private boolean a(String str, int i2, int i3) {
        return str != null && str.length() >= i2 && str.length() <= i3;
    }

    public String a(Context context) {
        try {
            String[] b2 = DeviceStatus.b(context);
            if (b2.length > 0 && b2[0] != null) {
                return b2[0];
            }
        } catch (Exception unused) {
        }
        return "";
    }

    private void a(int i2, JSONObject jSONObject) {
        InterstitialEventsManager.g().d(new EventData(i2, jSONObject));
    }

    /* access modifiers changed from: package-private */
    public String a(String str, CappingManager.ECappingStatus eCappingStatus) {
        if (eCappingStatus == null) {
            return null;
        }
        int i2 = AnonymousClass1.b[eCappingStatus.ordinal()];
        if (i2 != 1 && i2 != 2 && i2 != 3) {
            return null;
        }
        return "placement " + str + " is capped";
    }

    private void a(JSONObject jSONObject, Object[][] objArr) {
        if (objArr != null) {
            try {
                for (Object[] objArr2 : objArr) {
                    jSONObject.put(objArr2[0].toString(), objArr2[1]);
                }
            } catch (Exception e2) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "IronSourceObject addToDictionary: " + Log.getStackTraceString(e2), 3);
            }
        }
    }
}
