package com.ironsource.mediationsdk;

import android.app.Activity;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.model.AdapterConfig;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public abstract class ProgSmash {

    /* renamed from: a  reason: collision with root package name */
    protected AbstractAdapter f4650a;
    protected AdapterConfig b;
    private boolean c;
    protected JSONObject d;

    ProgSmash(AdapterConfig adapterConfig, AbstractAdapter abstractAdapter) {
        this.b = adapterConfig;
        this.f4650a = abstractAdapter;
        this.d = adapterConfig.b();
    }

    public void a(Activity activity) {
        this.f4650a.onPause(activity);
    }

    public void b(boolean z) {
        this.c = z;
    }

    public String k() {
        return this.b.d();
    }

    public boolean l() {
        return this.c;
    }

    public int m() {
        return this.b.c();
    }

    public Map<String, Object> n() {
        HashMap hashMap = new HashMap();
        try {
            String str = "";
            hashMap.put("providerAdapterVersion", this.f4650a != null ? this.f4650a.getVersion() : str);
            if (this.f4650a != null) {
                str = this.f4650a.getCoreSDKVersion();
            }
            hashMap.put("providerSDKVersion", str);
            hashMap.put("spId", this.b.e());
            hashMap.put("provider", this.b.a());
            hashMap.put("instanceType", Integer.valueOf(o() ? 2 : 1));
            hashMap.put("programmatic", 1);
        } catch (Exception e) {
            IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            c2.a(ironSourceTag, "getProviderEventData " + k() + ")", (Throwable) e);
        }
        return hashMap;
    }

    public boolean o() {
        return this.b.f();
    }

    public void b(Activity activity) {
        this.f4650a.onResume(activity);
    }
}
