package com.ironsource.environment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkStateReceiver extends BroadcastReceiver {

    /* renamed from: a  reason: collision with root package name */
    private ConnectivityManager f4577a;
    private NetworkStateReceiverListener b;
    private boolean c;

    public interface NetworkStateReceiverListener {
        void a(boolean z);
    }

    public NetworkStateReceiver(Context context, NetworkStateReceiverListener networkStateReceiverListener) {
        this.b = networkStateReceiverListener;
        this.f4577a = (ConnectivityManager) context.getSystemService("connectivity");
        a();
    }

    private boolean a() {
        boolean z = this.c;
        NetworkInfo activeNetworkInfo = this.f4577a.getActiveNetworkInfo();
        this.c = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
        if (z != this.c) {
            return true;
        }
        return false;
    }

    private void b() {
        NetworkStateReceiverListener networkStateReceiverListener = this.b;
        if (networkStateReceiverListener == null) {
            return;
        }
        if (this.c) {
            networkStateReceiverListener.a(true);
        } else {
            networkStateReceiverListener.a(false);
        }
    }

    public void onReceive(Context context, Intent intent) {
        if (intent != null && intent.getExtras() != null && a()) {
            b();
        }
    }
}
