package com.ironsource.environment;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.StatFs;
import android.provider.Settings;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.WindowManager;
import com.applovin.sdk.AppLovinEventTypes;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.UUID;
import org.joda.time.DateTimeConstants;
import org.json.JSONObject;

public class DeviceStatus {

    /* renamed from: a  reason: collision with root package name */
    private static String f4576a;

    private static long a(File file) {
        long j;
        long j2;
        StatFs statFs = new StatFs(file.getPath());
        if (Build.VERSION.SDK_INT < 19) {
            j2 = (long) statFs.getAvailableBlocks();
            j = (long) statFs.getBlockSize();
        } else {
            j2 = statFs.getAvailableBlocksLong();
            j = statFs.getBlockSizeLong();
        }
        return (j2 * j) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    public static String[] b(Context context) throws ClassNotFoundException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Class<?> cls = Class.forName("com.google.android.gms.ads.identifier.AdvertisingIdClient");
        Object invoke = cls.getMethod("getAdvertisingIdInfo", new Class[]{Context.class}).invoke(cls, new Object[]{context});
        Method method = invoke.getClass().getMethod("getId", new Class[0]);
        Method method2 = invoke.getClass().getMethod("isLimitAdTrackingEnabled", new Class[0]);
        String obj = method.invoke(invoke, new Object[0]).toString();
        boolean booleanValue = ((Boolean) method2.invoke(invoke, new Object[0])).booleanValue();
        return new String[]{obj, "" + booleanValue};
    }

    public static int c(Context context) {
        return ((WindowManager) context.getSystemService("window")).getDefaultDisplay().getRotation();
    }

    public static int d() {
        return k();
    }

    public static long e() {
        return Calendar.getInstance(TimeZone.getDefault()).getTime().getTime();
    }

    public static String f() {
        return Build.MODEL;
    }

    public static String g() {
        return Build.MANUFACTURER;
    }

    public static String h() {
        return "android";
    }

    public static String h(Context context) {
        File cacheDir = context.getCacheDir();
        if (cacheDir != null) {
            return cacheDir.getPath();
        }
        return null;
    }

    public static int i() {
        return -(TimeZone.getDefault().getOffset(e()) / DateTimeConstants.MILLIS_PER_MINUTE);
    }

    public static int j() {
        return l();
    }

    public static float k(Context context) {
        AudioManager audioManager = (AudioManager) context.getSystemService("audio");
        return ((float) audioManager.getStreamVolume(3)) / ((float) audioManager.getStreamMaxVolume(3));
    }

    public static int l() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static boolean m() {
        return a("su");
    }

    public static int d(Context context) {
        try {
            Intent registerReceiver = context.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            int i = 0;
            int intExtra = registerReceiver != null ? registerReceiver.getIntExtra(AppLovinEventTypes.USER_COMPLETED_LEVEL, -1) : 0;
            if (registerReceiver != null) {
                i = registerReceiver.getIntExtra("scale", -1);
            }
            if (intExtra == -1 || i == -1) {
                return -1;
            }
            return (int) ((((float) intExtra) / ((float) i)) * 100.0f);
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static File f(Context context) {
        return context.getExternalCacheDir();
    }

    public static List<ApplicationInfo> g(Context context) {
        return context.getPackageManager().getInstalledApplications(0);
    }

    public static String i(Context context) {
        return ((TelephonyManager) context.getSystemService("phone")).getNetworkOperatorName();
    }

    public static synchronized String j(Context context) {
        synchronized (DeviceStatus.class) {
            if (!TextUtils.isEmpty(f4576a)) {
                String str = f4576a;
                return str;
            }
            try {
                SharedPreferences sharedPreferences = context.getSharedPreferences("Mediation_Shared_Preferences", 0);
                if (sharedPreferences.getBoolean("uuidEnabled", true)) {
                    String string = sharedPreferences.getString("cachedUUID", "");
                    if (TextUtils.isEmpty(string)) {
                        f4576a = UUID.randomUUID().toString();
                        SharedPreferences.Editor edit = sharedPreferences.edit();
                        edit.putString("cachedUUID", f4576a);
                        edit.apply();
                    } else {
                        f4576a = string;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            String str2 = f4576a;
            return str2;
        }
    }

    public static float c() {
        return Resources.getSystem().getDisplayMetrics().density;
    }

    public static int k() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static boolean l(Context context) {
        return Settings.System.getInt(context.getContentResolver(), "accelerometer_rotation", 0) != 1;
    }

    public static int e(Context context) {
        return context.getResources().getConfiguration().orientation;
    }

    public static int a() {
        return Build.VERSION.SDK_INT;
    }

    private static boolean a(String str) {
        try {
            for (String str2 : new String[]{"/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/"}) {
                if (new File(str2 + str).exists()) {
                    return true;
                }
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    public static String b() {
        return Build.VERSION.RELEASE;
    }

    public static long b(String str) {
        return a(new File(str));
    }

    public static int a(Context context) {
        if (context instanceof Activity) {
            return ((Activity) context).getRequestedOrientation();
        }
        return -1;
    }

    @TargetApi(19)
    public static boolean a(Activity activity) {
        int systemUiVisibility = activity.getWindow().getDecorView().getSystemUiVisibility();
        return (systemUiVisibility | 4096) == systemUiVisibility || (systemUiVisibility | 2048) == systemUiVisibility;
    }

    private static boolean a(ResolveInfo resolveInfo) {
        return (resolveInfo.activityInfo.applicationInfo.flags & 1) != 0;
    }

    public static JSONObject a(Context context, boolean z) {
        Intent intent = new Intent("android.intent.action.MAIN", (Uri) null);
        intent.addCategory("android.intent.category.LAUNCHER");
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        JSONObject jSONObject = new JSONObject();
        PackageManager packageManager = context.getPackageManager();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        for (int i = 0; i < queryIntentActivities.size(); i++) {
            ResolveInfo resolveInfo = queryIntentActivities.get(i);
            if (!z) {
                try {
                    if (a(resolveInfo)) {
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            String format = simpleDateFormat.format(new Date(packageManager.getPackageInfo(resolveInfo.activityInfo.packageName, 4096).firstInstallTime));
            jSONObject.put(format, jSONObject.optInt(format, 0) + 1);
        }
        return jSONObject;
    }
}
