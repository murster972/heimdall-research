package com.ironsource.eventsmodule;

import org.json.JSONException;
import org.json.JSONObject;

public class EventData {

    /* renamed from: a  reason: collision with root package name */
    private int f4579a = -1;
    private long b = -1;
    private JSONObject c;

    public EventData(int i, long j, JSONObject jSONObject) {
        this.f4579a = i;
        this.b = j;
        if (jSONObject == null) {
            this.c = new JSONObject();
        } else {
            this.c = jSONObject;
        }
    }

    public String a() {
        return this.c.toString();
    }

    public JSONObject b() {
        return this.c;
    }

    public int c() {
        return this.f4579a;
    }

    public long d() {
        return this.b;
    }

    public void a(int i) {
        this.f4579a = i;
    }

    public void a(String str, Object obj) {
        try {
            this.c.put(str, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public EventData(int i, JSONObject jSONObject) {
        this.f4579a = i;
        this.b = System.currentTimeMillis();
        if (jSONObject == null) {
            this.c = new JSONObject();
        } else {
            this.c = jSONObject;
        }
    }
}
