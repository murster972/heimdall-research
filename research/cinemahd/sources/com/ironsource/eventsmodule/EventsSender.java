package com.ironsource.eventsmodule;

import android.os.AsyncTask;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class EventsSender extends AsyncTask<Object, Void, Boolean> {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList f4580a;
    private IEventsSenderResultListener b;

    public EventsSender() {
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(Boolean bool) {
        IEventsSenderResultListener iEventsSenderResultListener = this.b;
        if (iEventsSenderResultListener != null) {
            iEventsSenderResultListener.a(this.f4580a, bool.booleanValue());
        }
    }

    public EventsSender(IEventsSenderResultListener iEventsSenderResultListener) {
        this.b = iEventsSenderResultListener;
    }

    /* access modifiers changed from: protected */
    public Boolean doInBackground(Object... objArr) {
        try {
            boolean z = true;
            URL url = new URL(objArr[1]);
            this.f4580a = objArr[2];
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(15000);
            httpURLConnection.setConnectTimeout(15000);
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty(TraktV2.HEADER_CONTENT_TYPE, TraktV2.CONTENT_TYPE_JSON);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setDoOutput(true);
            OutputStream outputStream = httpURLConnection.getOutputStream();
            BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));
            bufferedWriter.write(objArr[0]);
            bufferedWriter.flush();
            bufferedWriter.close();
            outputStream.close();
            int responseCode = httpURLConnection.getResponseCode();
            httpURLConnection.disconnect();
            if (responseCode != 200) {
                z = false;
            }
            return Boolean.valueOf(z);
        } catch (Exception unused) {
            return false;
        }
    }
}
