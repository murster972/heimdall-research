package com.ironsource.adapters.supersonicads;

import android.app.Activity;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.FrameLayout;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.IntegrationData;
import com.ironsource.mediationsdk.IronSourceBannerLayout;
import com.ironsource.mediationsdk.IronSourceObject;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.metadata.MetaDataUtils;
import com.ironsource.mediationsdk.sdk.BannerSmashListener;
import com.ironsource.mediationsdk.sdk.InternalOfferwallListener;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.OfferwallAdapterApi;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.SSAFactory;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.utils.SDKUtils;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONException;
import org.json.JSONObject;

class SupersonicAdsAdapter extends AbstractAdapter implements OfferwallAdapterApi, OnOfferWallListener, OnInterstitialListener, OnRewardedVideoListener, OnBannerListener {
    private static final String VERSION = "6.14.0";
    /* access modifiers changed from: private */
    public static AtomicBoolean mDidInitSdk = new AtomicBoolean(false);
    private static Handler mUIThreadHandler;
    private final String AD_VISIBLE_EVENT_NAME = "impressions";
    private final String APPLICATION_PRIVATE_KEY = "privateKey";
    private final String APPLICATION_USER_AGE_GROUP = "applicationUserAgeGroup";
    private final String APPLICATION_USER_GENDER = "applicationUserGender";
    private final String CAMPAIGN_ID = "campaignId";
    private final String CLIENT_SIDE_CALLBACKS = "useClientSideCallbacks";
    private final String CUSTOM_PARAM_PREFIX = "custom_";
    private final String CUSTOM_SEGMENT = "custom_Segment";
    private final String DYNAMIC_CONTROLLER_CONFIG = "controllerConfig";
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String ITEM_COUNT = "itemCount";
    private final String ITEM_NAME = "itemName";
    private final String ITEM_SIGNATURE = "itemSignature";
    private final String LANGUAGE = "language";
    private final String MAX_VIDEO_LENGTH = "maxVideoLength";
    private final String OW_PLACEMENT_ID = "placementId";
    private final String SDK_PLUGIN_TYPE = "SDKPluginType";
    private final String SUPERSONIC_ADS = "SupersonicAds";
    private final String TIMESTAMP = VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP;
    /* access modifiers changed from: private */
    public boolean mConsent;
    /* access modifiers changed from: private */
    public boolean mDidSetConsent;
    private AtomicBoolean mDidSetInitParams = new AtomicBoolean(false);
    private boolean mIsRVAvailable = false;
    /* access modifiers changed from: private */
    public ISNAdView mIsnAdView;
    private String mMediationSegment;
    /* access modifiers changed from: private */
    public InternalOfferwallListener mOfferwallListener;
    /* access modifiers changed from: private */
    public SSAPublisher mSSAPublisher;
    private String mUserAgeGroup;
    private String mUserGender;

    private SupersonicAdsAdapter(String str) {
        super(str);
    }

    private void addItemNameCountSignature(HashMap<String, String> hashMap, JSONObject jSONObject) {
        try {
            String optString = jSONObject.optString("itemName");
            int optInt = jSONObject.optInt("itemCount", -1);
            String optString2 = jSONObject.optString("privateKey");
            boolean z = true;
            if (TextUtils.isEmpty(optString)) {
                z = false;
            } else {
                hashMap.put("itemName", optString);
            }
            if (TextUtils.isEmpty(optString2)) {
                z = false;
            }
            if (optInt == -1) {
                z = false;
            } else {
                hashMap.put("itemCount", String.valueOf(optInt));
            }
            if (z) {
                int a2 = IronSourceUtils.a();
                hashMap.put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, String.valueOf(a2));
                hashMap.put("itemSignature", createItemSig(a2, optString, optInt, optString2));
            }
        } catch (Exception e) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.ADAPTER_API, " addItemNameCountSignature", (Throwable) e);
        }
    }

    /* access modifiers changed from: private */
    public void applyConsent(boolean z) {
        if (this.mSSAPublisher != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("gdprConsentStatus", String.valueOf(z));
                jSONObject.put("demandSourceName", getProviderName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.a(jSONObject);
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0083, code lost:
        if (r10 != false) goto L_0x0088;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.ironsource.sdk.ISNAdView.ISNAdView createBanner(android.app.Activity r9, com.ironsource.mediationsdk.ISBannerSize r10, com.ironsource.mediationsdk.sdk.BannerSmashListener r11) {
        /*
            r8 = this;
            java.lang.String r0 = r10.a()
            int r1 = r0.hashCode()
            r2 = 3
            r3 = 2
            r4 = 1
            switch(r1) {
                case -387072689: goto L_0x0037;
                case 72205083: goto L_0x002d;
                case 79011241: goto L_0x0023;
                case 1951953708: goto L_0x0019;
                case 1999208305: goto L_0x000f;
                default: goto L_0x000e;
            }
        L_0x000e:
            goto L_0x0041
        L_0x000f:
            java.lang.String r1 = "CUSTOM"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0041
            r1 = 3
            goto L_0x0042
        L_0x0019:
            java.lang.String r1 = "BANNER"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0041
            r1 = 0
            goto L_0x0042
        L_0x0023:
            java.lang.String r1 = "SMART"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0041
            r1 = 2
            goto L_0x0042
        L_0x002d:
            java.lang.String r1 = "LARGE"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0041
            r1 = 1
            goto L_0x0042
        L_0x0037:
            java.lang.String r1 = "RECTANGLE"
            boolean r1 = r0.equals(r1)
            if (r1 == 0) goto L_0x0041
            r1 = 4
            goto L_0x0042
        L_0x0041:
            r1 = -1
        L_0x0042:
            r5 = 90
            r6 = 50
            r7 = 320(0x140, float:4.48E-43)
            if (r1 == 0) goto L_0x0086
            if (r1 == r4) goto L_0x0088
            if (r1 == r3) goto L_0x0079
            java.lang.String r3 = "SupersonicAds"
            r4 = 0
            if (r1 == r2) goto L_0x005d
            if (r11 == 0) goto L_0x005c
            com.ironsource.mediationsdk.logger.IronSourceError r9 = com.ironsource.mediationsdk.utils.ErrorBuilder.g(r3)
            r11.a(r9)
        L_0x005c:
            return r4
        L_0x005d:
            int r1 = r10.c()
            int r10 = r10.b()
            if (r1 < r7) goto L_0x006f
            if (r10 == r6) goto L_0x006c
            if (r10 == r5) goto L_0x006c
            goto L_0x006f
        L_0x006c:
            r5 = r10
            r7 = r1
            goto L_0x0088
        L_0x006f:
            if (r11 == 0) goto L_0x0078
            com.ironsource.mediationsdk.logger.IronSourceError r9 = com.ironsource.mediationsdk.utils.ErrorBuilder.g(r3)
            r11.a(r9)
        L_0x0078:
            return r4
        L_0x0079:
            boolean r10 = com.ironsource.mediationsdk.AdapterUtils.a(r9)
            if (r10 == 0) goto L_0x0083
            r11 = 728(0x2d8, float:1.02E-42)
            r7 = 728(0x2d8, float:1.02E-42)
        L_0x0083:
            if (r10 == 0) goto L_0x0086
            goto L_0x0088
        L_0x0086:
            r5 = 50
        L_0x0088:
            int r10 = com.ironsource.mediationsdk.AdapterUtils.a(r9, r7)
            int r11 = com.ironsource.mediationsdk.AdapterUtils.a(r9, r5)
            com.ironsource.sdk.ISAdSize r1 = new com.ironsource.sdk.ISAdSize
            r1.<init>(r10, r11, r0)
            com.ironsource.sdk.SSAPublisher r10 = r8.mSSAPublisher
            com.ironsource.sdk.ISNAdView.ISNAdView r9 = r10.a(r9, r1)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.adapters.supersonicads.SupersonicAdsAdapter.createBanner(android.app.Activity, com.ironsource.mediationsdk.ISBannerSize, com.ironsource.mediationsdk.sdk.BannerSmashListener):com.ironsource.sdk.ISNAdView.ISNAdView");
    }

    private String createItemSig(int i, String str, int i2, String str2) {
        return IronSourceUtils.d(i + str + i2 + str2);
    }

    private String createMinimumOfferCommissionSig(double d, String str) {
        return IronSourceUtils.d(d + str);
    }

    private String createUserCreationDateSig(String str, String str2, String str3) {
        return IronSourceUtils.d(str + str2 + str3);
    }

    public static String getAdapterSDKVersion() {
        return SDKUtils.j();
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getBannerExtraParams(JSONObject jSONObject) {
        return getGenenralExtraParams();
    }

    private HashMap<String, String> getGenenralExtraParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        return hashMap;
    }

    private HashMap<String, String> getInitParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            hashMap.put("custom_Segment", this.mMediationSegment);
        }
        return hashMap;
    }

    public static IntegrationData getIntegrationData(Activity activity) {
        IntegrationData integrationData = new IntegrationData("SupersonicAds", VERSION);
        integrationData.c = new String[]{"com.ironsource.sdk.controller.ControllerActivity", "com.ironsource.sdk.controller.InterstitialActivity", "com.ironsource.sdk.controller.OpenUrlActivity"};
        return integrationData;
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getInterstitialExtraParams() {
        return getGenenralExtraParams();
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getOfferwallExtraParams(JSONObject jSONObject) {
        HashMap<String, String> genenralExtraParams = getGenenralExtraParams();
        String optString = jSONObject.optString("language");
        if (!TextUtils.isEmpty(optString)) {
            genenralExtraParams.put("language", optString);
        }
        genenralExtraParams.put("useClientSideCallbacks", String.valueOf(SupersonicConfig.getConfigObj().getClientSideCallbacks()));
        Map<String, String> offerwallCustomParams = SupersonicConfig.getConfigObj().getOfferwallCustomParams();
        if (offerwallCustomParams != null && !offerwallCustomParams.isEmpty()) {
            genenralExtraParams.putAll(offerwallCustomParams);
        }
        addItemNameCountSignature(genenralExtraParams, jSONObject);
        return genenralExtraParams;
    }

    /* access modifiers changed from: private */
    public HashMap<String, String> getRewardedVideoExtraParams(JSONObject jSONObject) {
        HashMap<String, String> genenralExtraParams = getGenenralExtraParams();
        String optString = jSONObject.optString("language");
        if (!TextUtils.isEmpty(optString)) {
            genenralExtraParams.put("language", optString);
        }
        String optString2 = jSONObject.optString("maxVideoLength");
        if (!TextUtils.isEmpty(optString2)) {
            genenralExtraParams.put("maxVideoLength", optString2);
        }
        String optString3 = jSONObject.optString("campaignId");
        if (!TextUtils.isEmpty(optString3)) {
            genenralExtraParams.put("campaignId", optString3);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            genenralExtraParams.put("custom_Segment", this.mMediationSegment);
        }
        addItemNameCountSignature(genenralExtraParams, jSONObject);
        Map<String, String> rewardedVideoCustomParams = SupersonicConfig.getConfigObj().getRewardedVideoCustomParams();
        if (rewardedVideoCustomParams != null && !rewardedVideoCustomParams.isEmpty()) {
            genenralExtraParams.putAll(rewardedVideoCustomParams);
        }
        return genenralExtraParams;
    }

    private boolean isValidMetaData(String str, String str2) {
        if (str.equals("do_not_sell")) {
            return MetaDataUtils.a(str, str2);
        }
        return true;
    }

    private void setParamsBeforeInit(JSONObject jSONObject) {
        if (this.mDidSetInitParams.compareAndSet(false, true)) {
            SDKUtils.g(jSONObject.optString("controllerUrl"));
            if (isAdaptersDebugEnabled()) {
                SDKUtils.a(3);
            } else {
                SDKUtils.a(jSONObject.optInt("debugMode", 0));
            }
            SDKUtils.f(jSONObject.optString("controllerConfig", ""));
        }
    }

    public static SupersonicAdsAdapter startAdapter(String str) {
        return new SupersonicAdsAdapter(str);
    }

    /* access modifiers changed from: protected */
    public void addBannerListener(BannerSmashListener bannerSmashListener) {
        this.mAllBannerSmashes.add(bannerSmashListener);
    }

    public void destroyBanner(JSONObject jSONObject) {
        ISNAdView iSNAdView = this.mIsnAdView;
        if (iSNAdView != null) {
            iSNAdView.a();
            this.mIsnAdView = null;
        }
    }

    public void earlyInit(Activity activity, String str, String str2, JSONObject jSONObject) {
        IronSourceUtils.g(getProviderName() + ": earlyInit");
        if (activity == null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            c.b(ironSourceTag, getProviderName() + ": null activity", 2);
        } else if (mDidInitSdk.compareAndSet(false, true)) {
            if (isAdaptersDebugEnabled()) {
                SDKUtils.a(3);
            } else {
                SDKUtils.a(jSONObject.optInt("debugMode", 0));
            }
            SDKUtils.g(jSONObject.optString("controllerUrl"));
            SDKUtils.f(jSONObject.optString("controllerConfig", ""));
            IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
            c2.b(ironSourceTag2, getProviderName() + "IronSourceNetwork.initSDK", 1);
            IronSourceNetwork.a(activity, str, str2, getInitParams());
        }
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        c.b(ironSourceTag, getProviderName() + ": fetchRewardedVideo", 1);
        Iterator<RewardedVideoSmashListener> it2 = this.mAllRewardedVideoSmashes.iterator();
        while (it2.hasNext()) {
            RewardedVideoSmashListener next = it2.next();
            if (next != null) {
                next.a(this.mIsRVAvailable);
            }
        }
    }

    public String getCoreSDKVersion() {
        return SDKUtils.j();
    }

    public void getOfferwallCredits() {
        if (this.mSSAPublisher != null) {
            this.mSSAPublisher.a(IronSourceObject.q().g(), IronSourceObject.q().h(), (OnOfferWallListener) this);
            return;
        }
        log(IronSourceLogger.IronSourceTag.NATIVE, "Please call init before calling getOfferwallCredits", 2);
    }

    public String getVersion() {
        return VERSION;
    }

    public void initBanners(Activity activity, String str, String str2, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ": initBanners", 0);
        setParamsBeforeInit(jSONObject);
        final JSONObject jSONObject2 = jSONObject;
        final Activity activity2 = activity;
        final String str3 = str;
        final String str4 = str2;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    HashMap access$900 = SupersonicAdsAdapter.this.getBannerExtraParams(jSONObject2);
                    SSAPublisher unused = SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.a(activity2);
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter.this.mSSAPublisher.a(str3, str4, SupersonicAdsAdapter.this.getProviderName(), (Map<String, String>) access$900, (OnBannerListener) SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    SupersonicAdsAdapter.this.onBannerInitFailed(e.getMessage());
                }
            }
        });
    }

    public void initInterstitial(final Activity activity, final String str, final String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ": initInterstitial", 0);
        setParamsBeforeInit(jSONObject);
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    SSAPublisher unused = SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.a(activity);
                    HashMap access$600 = SupersonicAdsAdapter.this.getInterstitialExtraParams();
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter.this.mSSAPublisher.a(str, str2, SupersonicAdsAdapter.this.getProviderName(), (Map<String, String>) access$600, (OnInterstitialListener) SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    SupersonicAdsAdapter.this.onInterstitialInitFailed(e.getMessage());
                }
            }
        });
    }

    public void initOfferwall(Activity activity, String str, String str2, JSONObject jSONObject) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ": initOfferwall", 0);
        setParamsBeforeInit(jSONObject);
        final JSONObject jSONObject2 = jSONObject;
        final Activity activity2 = activity;
        final String str3 = str;
        final String str4 = str2;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    HashMap access$700 = SupersonicAdsAdapter.this.getOfferwallExtraParams(jSONObject2);
                    SSAPublisher unused = SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.a(activity2);
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter.this.mSSAPublisher.a(str3, str4, access$700, SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    IronSourceLoggerManager c = IronSourceLoggerManager.c();
                    IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
                    c.a(ironSourceTag, SupersonicAdsAdapter.this.getProviderName() + ":initOfferwall(userId:" + str4 + ")", (Throwable) e);
                    InternalOfferwallListener access$800 = SupersonicAdsAdapter.this.mOfferwallListener;
                    access$800.a(false, ErrorBuilder.a("Adapter initialization failure - " + SupersonicAdsAdapter.this.getProviderName() + " - " + e.getMessage(), "Offerwall"));
                }
            }
        });
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ": initRewardedVideo", 0);
        setParamsBeforeInit(jSONObject);
        final Activity activity2 = activity;
        final JSONObject jSONObject2 = jSONObject;
        final String str3 = str;
        final String str4 = str2;
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    SSAPublisher unused = SupersonicAdsAdapter.this.mSSAPublisher = SSAFactory.a(activity2);
                    HashMap access$100 = SupersonicAdsAdapter.this.getRewardedVideoExtraParams(jSONObject2);
                    if (SupersonicAdsAdapter.this.mDidSetConsent) {
                        SupersonicAdsAdapter.this.applyConsent(SupersonicAdsAdapter.this.mConsent);
                    }
                    SupersonicAdsAdapter.this.mSSAPublisher.a(str3, str4, SupersonicAdsAdapter.this.getProviderName(), (Map<String, String>) access$100, (OnRewardedVideoListener) SupersonicAdsAdapter.this);
                    SupersonicAdsAdapter.mDidInitSdk.set(true);
                } catch (Exception e) {
                    e.printStackTrace();
                    SupersonicAdsAdapter.this.onRVInitFail("initRewardedVideo");
                }
            }
        });
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        SSAPublisher sSAPublisher = this.mSSAPublisher;
        return sSAPublisher != null && sSAPublisher.a(getProviderName());
    }

    public boolean isOfferwallAvailable() {
        return true;
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return this.mIsRVAvailable;
    }

    public void loadBanner(final IronSourceBannerLayout ironSourceBannerLayout, JSONObject jSONObject, BannerSmashListener bannerSmashListener) {
        try {
            if (this.mSSAPublisher == null) {
                log(IronSourceLogger.IronSourceTag.NATIVE, "Please call initBanner before calling loadBanner", 2);
                Iterator<BannerSmashListener> it2 = this.mAllBannerSmashes.iterator();
                while (it2.hasNext()) {
                    BannerSmashListener next = it2.next();
                    if (next != null) {
                        next.a(ErrorBuilder.c("Load was called before Init"));
                    }
                }
            }
            if (ironSourceBannerLayout == null) {
                IronSourceLoggerManager.c().b(IronSourceLogger.IronSourceTag.INTERNAL, "SupersonicAds loadBanner banner == null", 3);
                return;
            }
            this.mActiveBannerSmash = bannerSmashListener;
            if (this.mIsnAdView != null) {
                this.mIsnAdView.a();
                this.mIsnAdView = null;
            }
            final JSONObject jSONObject2 = new JSONObject();
            jSONObject2.put("demandSourceName", getProviderName());
            jSONObject2.put("productType", SSAEnums$ProductType.Banner);
            if (mUIThreadHandler == null) {
                mUIThreadHandler = new Handler(Looper.getMainLooper());
            }
            mUIThreadHandler.post(new Runnable() {
                public void run() {
                    try {
                        ISNAdView unused = SupersonicAdsAdapter.this.mIsnAdView = SupersonicAdsAdapter.this.createBanner(ironSourceBannerLayout.getActivity(), ironSourceBannerLayout.getSize(), SupersonicAdsAdapter.this.mActiveBannerSmash);
                        if (SupersonicAdsAdapter.this.mIsnAdView != null) {
                            SupersonicAdsAdapter.this.mIsnAdView.a(jSONObject2);
                        }
                    } catch (Exception e) {
                        IronSourceError c = ErrorBuilder.c("Banner Load Fail, " + SupersonicAdsAdapter.this.getProviderName() + " - " + e.getMessage());
                        if (SupersonicAdsAdapter.this.mActiveBannerSmash != null) {
                            SupersonicAdsAdapter.this.mActiveBannerSmash.a(c);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        if (this.mSSAPublisher != null) {
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.d(jSONObject2);
            return;
        }
        log(IronSourceLogger.IronSourceTag.NATIVE, "Please call initInterstitial before calling loadInterstitial", 2);
        Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
        while (it2.hasNext()) {
            InterstitialSmashListener next = it2.next();
            if (next != null) {
                next.a(ErrorBuilder.c("Load was called before Init"));
            }
        }
    }

    public void onBannerClick() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onBannerAdClicked ", 1);
        BannerSmashListener bannerSmashListener = this.mActiveBannerSmash;
        if (bannerSmashListener != null) {
            bannerSmashListener.a();
        }
    }

    public void onBannerInitFailed(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onBannerInitFailed ", 1);
        Iterator<BannerSmashListener> it2 = this.mAllBannerSmashes.iterator();
        while (it2.hasNext()) {
            BannerSmashListener next = it2.next();
            if (next != null) {
                next.b(ErrorBuilder.a(str, "Banner"));
            }
        }
    }

    public void onBannerInitSuccess() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onBannerInitSuccess ", 1);
        Iterator<BannerSmashListener> it2 = this.mAllBannerSmashes.iterator();
        while (it2.hasNext()) {
            BannerSmashListener next = it2.next();
            if (next != null) {
                next.onBannerInitSuccess();
            }
        }
    }

    public void onBannerLoadFail(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onBannerLoadFail", 1);
        Iterator<BannerSmashListener> it2 = this.mAllBannerSmashes.iterator();
        while (it2.hasNext()) {
            BannerSmashListener next = it2.next();
            if (next != null) {
                next.a(ErrorBuilder.a(str, "Banner"));
            }
        }
    }

    public void onBannerLoadSuccess() {
        ISNAdView iSNAdView;
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onBannerLoadSuccess ", 1);
        Iterator<BannerSmashListener> it2 = this.mAllBannerSmashes.iterator();
        while (it2.hasNext()) {
            BannerSmashListener next = it2.next();
            if (!(next == null || (iSNAdView = this.mIsnAdView) == null)) {
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(iSNAdView.getAdViewSize().b(), this.mIsnAdView.getAdViewSize().a());
                layoutParams.gravity = 17;
                next.a(this.mIsnAdView, layoutParams);
            }
        }
    }

    public void onGetOWCreditsFailed(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onGetOWCreditsFailed ", 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.d(ErrorBuilder.b(str));
        }
    }

    public void onInterstitialAdRewarded(String str, int i) {
    }

    public void onInterstitialClick() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialAdClicked ", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.onInterstitialAdClicked();
        }
    }

    public void onInterstitialClose() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialAdClosed ", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.b();
        }
    }

    public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
        InterstitialSmashListener interstitialSmashListener;
        if (jSONObject != null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            c.b(ironSourceTag, getProviderName() + ": onInterstitialEventNotificationReceived: " + str + " extData: " + jSONObject.toString(), 1);
            if (!TextUtils.isEmpty(str) && "impressions".equals(str) && (interstitialSmashListener = this.mActiveInterstitialSmash) != null) {
                interstitialSmashListener.e();
            }
        }
    }

    public void onInterstitialInitFailed(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialInitFailed ", 1);
        Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
        while (it2.hasNext()) {
            InterstitialSmashListener next = it2.next();
            if (next != null) {
                next.e(ErrorBuilder.a(str, "Interstitial"));
            }
        }
    }

    public void onInterstitialInitSuccess() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialInitSuccess ", 1);
        Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
        while (it2.hasNext()) {
            InterstitialSmashListener next = it2.next();
            if (next != null) {
                next.onInterstitialInitSuccess();
            }
        }
    }

    public void onInterstitialLoadFailed(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialAdLoadFailed ", 1);
        Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
        while (it2.hasNext()) {
            InterstitialSmashListener next = it2.next();
            if (next != null) {
                next.a(ErrorBuilder.c(str));
            }
        }
    }

    public void onInterstitialLoadSuccess() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialLoadSuccess ", 1);
        Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
        while (it2.hasNext()) {
            InterstitialSmashListener next = it2.next();
            if (next != null) {
                next.a();
            }
        }
    }

    public void onInterstitialOpen() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialAdOpened ", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.c();
        }
    }

    public void onInterstitialShowFailed(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialAdShowFailed ", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.b(ErrorBuilder.b("Interstitial", str));
        }
    }

    public void onInterstitialShowSuccess() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onInterstitialAdShowSucceeded ", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.d();
        }
    }

    public void onOWAdClosed() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onOWAdClosed ", 1);
        InternalOfferwallListener internalOfferwallListener = this.mOfferwallListener;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.f();
        }
    }

    public boolean onOWAdCredited(int i, int i2, boolean z) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onOWAdCredited ", 1);
        InternalOfferwallListener internalOfferwallListener = this.mOfferwallListener;
        if (internalOfferwallListener == null || !internalOfferwallListener.a(i, i2, z)) {
            return false;
        }
        return true;
    }

    public void onOWGeneric(String str, String str2) {
    }

    public void onOWShowFail(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onOWShowFail ", 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.e(ErrorBuilder.b(str));
        }
    }

    public void onOWShowSuccess(String str) {
        if (TextUtils.isEmpty(str)) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            log(ironSourceTag, getProviderName() + ":onOWShowSuccess()", 1);
        } else {
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
            log(ironSourceTag2, getProviderName() + ":onOWShowSuccess(placementId:" + str + ")", 1);
        }
        InternalOfferwallListener internalOfferwallListener = this.mOfferwallListener;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.e();
        }
    }

    public void onOfferwallEventNotificationReceived(String str, JSONObject jSONObject) {
        if (jSONObject != null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            c.b(ironSourceTag, getProviderName() + " :onOfferwallEventNotificationReceived ", 1);
        }
    }

    public void onOfferwallInitFail(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onOfferwallInitFail ", 1);
        if (this.mOfferwallListener != null) {
            this.mOfferwallListener.a(false, ErrorBuilder.b(str));
        }
    }

    public void onOfferwallInitSuccess() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onOfferwallInitSuccess ", 1);
        InternalOfferwallListener internalOfferwallListener = this.mOfferwallListener;
        if (internalOfferwallListener != null) {
            internalOfferwallListener.b(true);
        }
    }

    public void onPause(Activity activity) {
        SSAPublisher sSAPublisher = this.mSSAPublisher;
        if (sSAPublisher != null) {
            sSAPublisher.onPause(activity);
        }
    }

    public void onRVAdClicked() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVAdClicked ", 1);
        RewardedVideoSmashListener rewardedVideoSmashListener = this.mActiveRewardedVideoSmash;
        if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.g();
        }
    }

    public void onRVAdClosed() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVAdClosed ", 1);
        RewardedVideoSmashListener rewardedVideoSmashListener = this.mActiveRewardedVideoSmash;
        if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.onRewardedVideoAdClosed();
        }
    }

    public void onRVAdCredited(int i) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVAdCredited ", 1);
        RewardedVideoSmashListener rewardedVideoSmashListener = this.mActiveRewardedVideoSmash;
        if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.h();
        }
    }

    public void onRVAdOpened() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVAdOpened ", 1);
        RewardedVideoSmashListener rewardedVideoSmashListener = this.mActiveRewardedVideoSmash;
        if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.onRewardedVideoAdOpened();
        }
    }

    public void onRVEventNotificationReceived(String str, JSONObject jSONObject) {
        RewardedVideoSmashListener rewardedVideoSmashListener;
        if (jSONObject != null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            c.b(ironSourceTag, getProviderName() + ": onRVEventNotificationReceived: " + str + " extData: " + jSONObject.toString(), 1);
        }
        if (!TextUtils.isEmpty(str) && "impressions".equals(str) && (rewardedVideoSmashListener = this.mActiveRewardedVideoSmash) != null) {
            rewardedVideoSmashListener.f();
        }
    }

    public void onRVInitFail(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVInitFail ", 1);
        Iterator<RewardedVideoSmashListener> it2 = this.mAllRewardedVideoSmashes.iterator();
        while (it2.hasNext()) {
            RewardedVideoSmashListener next = it2.next();
            if (next != null) {
                next.a(false);
            }
        }
    }

    public void onRVInitSuccess(AdUnitsReady adUnitsReady) {
        int i;
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVInitSuccess ", 1);
        boolean z = false;
        try {
            i = Integer.parseInt(adUnitsReady.b());
        } catch (NumberFormatException e) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.NATIVE, ": onRVInitSuccess: parseInt()", (Throwable) e);
            i = 0;
        }
        if (i > 0) {
            z = true;
        }
        this.mIsRVAvailable = z;
        Iterator<RewardedVideoSmashListener> it2 = this.mAllRewardedVideoSmashes.iterator();
        while (it2.hasNext()) {
            RewardedVideoSmashListener next = it2.next();
            if (next != null) {
                next.a(z);
            }
        }
    }

    public void onRVNoMoreOffers() {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVNoMoreOffers ", 1);
        this.mIsRVAvailable = false;
        Iterator<RewardedVideoSmashListener> it2 = this.mAllRewardedVideoSmashes.iterator();
        while (it2.hasNext()) {
            RewardedVideoSmashListener next = it2.next();
            if (next != null) {
                next.a(false);
            }
        }
    }

    public void onRVShowFail(String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
        c.b(ironSourceTag, getProviderName() + ": onRVShowFail ", 1);
        RewardedVideoSmashListener rewardedVideoSmashListener = this.mActiveRewardedVideoSmash;
        if (rewardedVideoSmashListener != null) {
            rewardedVideoSmashListener.c(new IronSourceError(509, str));
        }
    }

    public void onResume(Activity activity) {
        SSAPublisher sSAPublisher = this.mSSAPublisher;
        if (sSAPublisher != null) {
            sSAPublisher.onResume(activity);
        }
    }

    public void reloadBanner(JSONObject jSONObject) {
        try {
            if (this.mIsnAdView != null) {
                this.mIsnAdView.a(jSONObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.NATIVE;
            log(ironSourceTag, getProviderName() + " reloadBanner Failed to reload banner ad", 2);
        }
    }

    /* access modifiers changed from: protected */
    public void removeBannerListener(BannerSmashListener bannerSmashListener) {
        this.mAllBannerSmashes.remove(bannerSmashListener);
    }

    public void setAge(int i) {
        if (i >= 13 && i <= 17) {
            this.mUserAgeGroup = DiskLruCache.VERSION_1;
        } else if (i >= 18 && i <= 20) {
            this.mUserAgeGroup = TraktV2.API_VERSION;
        } else if (i >= 21 && i <= 24) {
            this.mUserAgeGroup = "3";
        } else if (i >= 25 && i <= 34) {
            this.mUserAgeGroup = "4";
        } else if (i >= 35 && i <= 44) {
            this.mUserAgeGroup = "5";
        } else if (i >= 45 && i <= 54) {
            this.mUserAgeGroup = "6";
        } else if (i >= 55 && i <= 64) {
            this.mUserAgeGroup = "7";
        } else if (i <= 65 || i > 120) {
            this.mUserAgeGroup = "0";
        } else {
            this.mUserAgeGroup = "8";
        }
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        StringBuilder sb = new StringBuilder();
        sb.append(getProviderName());
        sb.append(": setConsent (");
        sb.append(z ? "true" : "false");
        sb.append(")");
        c.b(ironSourceTag, sb.toString(), 1);
        this.mDidSetConsent = true;
        this.mConsent = z;
        applyConsent(z);
    }

    public void setGender(String str) {
        this.mUserGender = str;
    }

    public void setInternalOfferwallListener(InternalOfferwallListener internalOfferwallListener) {
        this.mOfferwallListener = internalOfferwallListener;
    }

    public void setMediationSegment(String str) {
        this.mMediationSegment = str;
    }

    /* access modifiers changed from: protected */
    public void setMediationState(AbstractSmash.MEDIATION_STATE mediation_state, String str) {
        if (this.mSSAPublisher != null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            c.b(ironSourceTag, getProviderName() + ": setMediationState(" + str + " , " + getProviderName() + " , " + mediation_state.a() + ")", 1);
            this.mSSAPublisher.a(str, getProviderName(), mediation_state.a());
        }
    }

    /* access modifiers changed from: protected */
    public void setMetaData(String str, String str2) {
        if (!mDidInitSdk.get()) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            c.b(ironSourceTag, getProviderName() + " setMetaData: key=" + str + ", value=" + str2, 1);
            if (!isValidMetaData(str, str2)) {
                IronSourceLoggerManager c2 = IronSourceLoggerManager.c();
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
                c2.b(ironSourceTag2, getProviderName() + " MetaData not valid", 1);
                return;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(str, str2);
                IronSourceNetwork.c(jSONObject);
            } catch (JSONException e) {
                IronSourceLoggerManager c3 = IronSourceLoggerManager.c();
                IronSourceLogger.IronSourceTag ironSourceTag3 = IronSourceLogger.IronSourceTag.ADAPTER_API;
                c3.b(ironSourceTag3, getProviderName() + " setMetaData error - " + e, 3);
                e.printStackTrace();
            }
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        this.mActiveInterstitialSmash = interstitialSmashListener;
        if (this.mSSAPublisher != null) {
            int a2 = SessionDepthManager.a().a(2);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
                jSONObject2.put("sessionDepth", a2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.b(jSONObject2);
            return;
        }
        log(IronSourceLogger.IronSourceTag.NATIVE, "Please call loadInterstitial before calling showInterstitial", 2);
        InterstitialSmashListener interstitialSmashListener2 = this.mActiveInterstitialSmash;
        if (interstitialSmashListener2 != null) {
            interstitialSmashListener2.b(ErrorBuilder.d("Interstitial"));
        }
    }

    public void showOfferwall(String str, JSONObject jSONObject) {
        HashMap<String, String> offerwallExtraParams = getOfferwallExtraParams(jSONObject);
        if (offerwallExtraParams != null) {
            offerwallExtraParams.put("placementId", str);
        }
        SSAPublisher sSAPublisher = this.mSSAPublisher;
        if (sSAPublisher != null) {
            sSAPublisher.a((Map<String, String>) offerwallExtraParams);
        } else {
            log(IronSourceLogger.IronSourceTag.NATIVE, "Please call init before calling showOfferwall", 2);
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        this.mActiveRewardedVideoSmash = rewardedVideoSmashListener;
        if (this.mSSAPublisher != null) {
            int a2 = SessionDepthManager.a().a(1);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
                jSONObject2.put("sessionDepth", a2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.e(jSONObject2);
            return;
        }
        this.mIsRVAvailable = false;
        RewardedVideoSmashListener rewardedVideoSmashListener2 = this.mActiveRewardedVideoSmash;
        if (rewardedVideoSmashListener2 != null) {
            rewardedVideoSmashListener2.c(ErrorBuilder.d("Rewarded Video"));
        }
        Iterator<RewardedVideoSmashListener> it2 = this.mAllRewardedVideoSmashes.iterator();
        while (it2.hasNext()) {
            RewardedVideoSmashListener next = it2.next();
            if (next != null) {
                next.a(false);
            }
        }
    }
}
