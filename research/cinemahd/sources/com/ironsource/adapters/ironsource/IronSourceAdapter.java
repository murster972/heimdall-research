package com.ironsource.adapters.ironsource;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.metadata.MetaDataUtils;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceAdInstanceBuilder;
import com.ironsource.sdk.IronSourceNetwork;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.utils.SDKUtils;
import com.uwetrottmann.trakt5.TraktV2;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceAdapter extends AbstractAdapter {
    private static final int IS_LOAD_EXCEPTION = 1000;
    private static final int IS_SHOW_EXCEPTION = 1001;
    private static final int RV_LOAD_EXCEPTION = 1002;
    private static final int RV_SHOW_EXCEPTION = 1003;
    private static final String VERSION = "6.14.0";
    private static AtomicBoolean mDidInitSdk = new AtomicBoolean(false);
    private final String ADM_KEY = "adm";
    private final String APPLICATION_USER_AGE_GROUP = "applicationUserAgeGroup";
    private final String APPLICATION_USER_GENDER = "applicationUserGender";
    private final String CUSTOM_SEGMENT = "custom_Segment";
    private final String DEMAND_SOURCE_NAME = "demandSourceName";
    private final String DYNAMIC_CONTROLLER_CONFIG = "controllerConfig";
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private final String SDK_PLUGIN_TYPE = "SDKPluginType";
    private Context mContext;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToISAd;
    private ConcurrentHashMap<String, IronSourceAdInstance> mDemandSourceToRvAd;
    private String mMediationSegment;
    private String mUserAgeGroup;
    private String mUserGender;

    private class IronSourceInterstitialListener implements OnInterstitialListener {
        private String mDemandSourceName;
        private InterstitialSmashListener mListener;

        IronSourceInterstitialListener(InterstitialSmashListener interstitialSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = interstitialSmashListener;
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialAdRewarded demandSourceId=" + str + " amount=" + i);
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialClick");
            this.mListener.onInterstitialAdClicked();
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialClose");
            this.mListener.b();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialEventNotificationReceived eventName=" + str);
            this.mListener.e();
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialInitFailed");
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialInitSuccess");
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialLoadFailed " + str);
            this.mListener.a(ErrorBuilder.c(str));
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialLoadSuccess");
            this.mListener.a();
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialOpen");
            this.mListener.c();
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialShowFailed " + str);
            this.mListener.b(ErrorBuilder.b("Interstitial", str));
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " interstitialListener onInterstitialShowSuccess");
            this.mListener.d();
        }
    }

    private IronSourceAdapter(String str) {
        super(str);
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, str + ": new instance");
        this.mDemandSourceToISAd = new ConcurrentHashMap<>();
        this.mDemandSourceToRvAd = new ConcurrentHashMap<>();
        this.mUserAgeGroup = null;
        this.mUserGender = null;
        this.mMediationSegment = null;
    }

    private String getDemandSourceName(JSONObject jSONObject) {
        if (!TextUtils.isEmpty(jSONObject.optString("demandSourceName"))) {
            return jSONObject.optString("demandSourceName");
        }
        return getProviderName();
    }

    private HashMap<String, String> getInitParams() {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(this.mUserAgeGroup)) {
            hashMap.put("applicationUserAgeGroup", this.mUserAgeGroup);
        }
        if (!TextUtils.isEmpty(this.mUserGender)) {
            hashMap.put("applicationUserGender", this.mUserGender);
        }
        String pluginType = getPluginType();
        if (!TextUtils.isEmpty(pluginType)) {
            hashMap.put("SDKPluginType", pluginType);
        }
        if (!TextUtils.isEmpty(this.mMediationSegment)) {
            hashMap.put("custom_Segment", this.mMediationSegment);
        }
        return hashMap;
    }

    private void initSDK(Activity activity, String str, String str2, JSONObject jSONObject) {
        if (activity == null) {
            logError(IronSourceLogger.IronSourceTag.INTERNAL, "initSDK: null activity");
            return;
        }
        this.mContext = activity.getApplicationContext();
        if (mDidInitSdk.compareAndSet(false, true)) {
            if (isAdaptersDebugEnabled()) {
                log(IronSourceLogger.IronSourceTag.INTERNAL, "IronSourceNetwork.initSDK debug mode enabled");
                SDKUtils.a(3);
            } else {
                SDKUtils.a(jSONObject.optInt("debugMode", 0));
            }
            SDKUtils.g(jSONObject.optString("controllerUrl"));
            SDKUtils.f(jSONObject.optString("controllerConfig", ""));
            HashMap<String, String> initParams = getInitParams();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            log(ironSourceTag, "IronSourceNetwork.initSDK with " + initParams);
            IronSourceNetwork.a(activity, str, str2, initParams);
        }
    }

    private boolean isValidMetaData(String str, String str2) {
        if (str.equals("do_not_sell")) {
            return MetaDataUtils.a(str, str2);
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void log(IronSourceLogger.IronSourceTag ironSourceTag, String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        c.b(ironSourceTag, "IronSourceAdapter: " + str, 0);
    }

    private void logError(IronSourceLogger.IronSourceTag ironSourceTag, String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        c.b(ironSourceTag, "IronSourceAdapter: " + str, 3);
    }

    public static IronSourceAdapter startAdapter(String str) {
        return new IronSourceAdapter(str);
    }

    public void earlyInit(Activity activity, String str, String str2, JSONObject jSONObject) {
        IronSourceUtils.g(getDemandSourceName(jSONObject) + ": earlyInit");
        initSDK(activity, str, str2, jSONObject);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "fetchRewardedVideo");
        IronSourceAdInstance ironSourceAdInstance = this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        if (ironSourceAdInstance == null) {
            logError(IronSourceLogger.IronSourceTag.ADAPTER_API, "fetchRewardedVideo exception: null adInstance ");
            return;
        }
        try {
            IronSourceNetwork.b(ironSourceAdInstance);
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "fetchRewardedVideo exception " + e.getMessage());
            OnInterstitialListener b = ironSourceAdInstance.b();
            if (b != null) {
                b.onInterstitialLoadFailed("fetch exception");
            }
        }
    }

    public String getCoreSDKVersion() {
        return SDKUtils.j();
    }

    public Map<String, Object> getIsBiddingData(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        String a2 = IronSourceNetwork.a(this.mContext);
        if (a2 != null) {
            hashMap.put("token", a2);
        } else {
            logError(IronSourceLogger.IronSourceTag.ADAPTER_API, "IS bidding token is null");
            hashMap.put("token", "");
        }
        return hashMap;
    }

    public Map<String, Object> getRvBiddingData(JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        String a2 = IronSourceNetwork.a(this.mContext);
        if (a2 != null) {
            hashMap.put("token", a2);
        } else {
            logError(IronSourceLogger.IronSourceTag.ADAPTER_API, "RV bidding token is null");
            hashMap.put("token", "");
        }
        return hashMap;
    }

    public String getVersion() {
        return VERSION;
    }

    public void initInterstitial(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(IronSourceLogger.IronSourceTag.INTERNAL, jSONObject, "initInterstitial");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceAdInstanceBuilder ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceInterstitialListener(interstitialSmashListener, demandSourceName));
        ironSourceAdInstanceBuilder.a(getInitParams());
        this.mDemandSourceToISAd.put(demandSourceName, ironSourceAdInstanceBuilder.a());
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void initInterstitialForBidding(Activity activity, String str, String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(IronSourceLogger.IronSourceTag.INTERNAL, jSONObject, "initInterstitialForBidding");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceAdInstanceBuilder ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceInterstitialListener(interstitialSmashListener, demandSourceName));
        ironSourceAdInstanceBuilder.a(getInitParams());
        ironSourceAdInstanceBuilder.b();
        this.mDemandSourceToISAd.put(demandSourceName, ironSourceAdInstanceBuilder.a());
        interstitialSmashListener.onInterstitialInitSuccess();
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(IronSourceLogger.IronSourceTag.INTERNAL, jSONObject, "initRewardedVideo");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceAdInstanceBuilder ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName));
        ironSourceAdInstanceBuilder.c();
        ironSourceAdInstanceBuilder.a(getInitParams());
        this.mDemandSourceToRvAd.put(demandSourceName, ironSourceAdInstanceBuilder.a());
        fetchRewardedVideo(jSONObject);
    }

    public void initRvForBidding(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(IronSourceLogger.IronSourceTag.INTERNAL, jSONObject, "initRvForBidding");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceAdInstanceBuilder ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName));
        ironSourceAdInstanceBuilder.b();
        ironSourceAdInstanceBuilder.c();
        ironSourceAdInstanceBuilder.a(getInitParams());
        this.mDemandSourceToRvAd.put(demandSourceName, ironSourceAdInstanceBuilder.a());
        rewardedVideoSmashListener.i();
    }

    public void initRvForDemandOnly(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(IronSourceLogger.IronSourceTag.INTERNAL, jSONObject, "initRvForDemandOnly");
        initSDK(activity, str, str2, jSONObject);
        String demandSourceName = getDemandSourceName(jSONObject);
        IronSourceAdInstanceBuilder ironSourceAdInstanceBuilder = new IronSourceAdInstanceBuilder(demandSourceName, new IronSourceRewardedVideoListener(rewardedVideoSmashListener, demandSourceName, true));
        ironSourceAdInstanceBuilder.c();
        ironSourceAdInstanceBuilder.a(getInitParams());
        this.mDemandSourceToRvAd.put(demandSourceName, ironSourceAdInstanceBuilder.a());
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.a(ironSourceAdInstance);
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        IronSourceAdInstance ironSourceAdInstance = this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject));
        return ironSourceAdInstance != null && IronSourceNetwork.a(ironSourceAdInstance);
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener, String str) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "loadInterstitial");
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("adm", str);
            IronSourceNetwork.a(this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "loadInterstitial for bidding exception " + e.getMessage());
            interstitialSmashListener.a(new IronSourceError(1000, e.getMessage()));
        }
    }

    public void loadVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "loadVideo (RV in bidding mode)");
        try {
            HashMap hashMap = new HashMap();
            hashMap.put("adm", str);
            IronSourceNetwork.a(this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "loadVideo exception " + e.getMessage());
            rewardedVideoSmashListener.d(new IronSourceError(1002, e.getMessage()));
            rewardedVideoSmashListener.a(false);
        }
    }

    public void loadVideoForDemandOnly(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "loadVideoForDemandOnly");
        try {
            IronSourceNetwork.b(this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "loadVideoForDemandOnly exception " + e.getMessage());
            rewardedVideoSmashListener.d(new IronSourceError(1002, e.getMessage()));
        }
    }

    public void onPause(Activity activity) {
        IronSourceNetwork.a(activity);
    }

    public void onResume(Activity activity) {
        IronSourceNetwork.b(activity);
    }

    public void setAge(int i) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, "setAge: " + i);
        if (i >= 13 && i <= 17) {
            this.mUserAgeGroup = DiskLruCache.VERSION_1;
        } else if (i >= 18 && i <= 20) {
            this.mUserAgeGroup = TraktV2.API_VERSION;
        } else if (i >= 21 && i <= 24) {
            this.mUserAgeGroup = "3";
        } else if (i >= 25 && i <= 34) {
            this.mUserAgeGroup = "4";
        } else if (i >= 35 && i <= 44) {
            this.mUserAgeGroup = "5";
        } else if (i >= 45 && i <= 54) {
            this.mUserAgeGroup = "6";
        } else if (i >= 55 && i <= 64) {
            this.mUserAgeGroup = "7";
        } else if (i <= 65 || i > 120) {
            this.mUserAgeGroup = "0";
        } else {
            this.mUserAgeGroup = "8";
        }
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
        StringBuilder sb = new StringBuilder();
        sb.append("setConsent (");
        sb.append(z ? "true" : "false");
        sb.append(")");
        log(ironSourceTag, sb.toString());
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("gdprConsentStatus", String.valueOf(z));
            IronSourceNetwork.b(jSONObject);
        } catch (JSONException e) {
            IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag2, "setConsent exception " + e.getMessage());
        }
    }

    public void setGender(String str) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, "setGender: " + str);
        this.mUserGender = str;
    }

    public void setMediationSegment(String str) {
        this.mMediationSegment = str;
    }

    /* access modifiers changed from: protected */
    public void setMetaData(String str, String str2) {
        if (!mDidInitSdk.get()) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            log(ironSourceTag, "setMetaData: key=" + str + ", value=" + str2);
            if (!isValidMetaData(str, str2)) {
                log(IronSourceLogger.IronSourceTag.ADAPTER_API, "MetaData not valid");
                return;
            }
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put(str, str2);
                IronSourceNetwork.c(jSONObject);
            } catch (JSONException e) {
                IronSourceLogger.IronSourceTag ironSourceTag2 = IronSourceLogger.IronSourceTag.ADAPTER_API;
                logError(ironSourceTag2, "setMetaData error - " + e);
                e.printStackTrace();
            }
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "showInterstitial");
        try {
            int a2 = SessionDepthManager.a().a(2);
            HashMap hashMap = new HashMap();
            hashMap.put("sessionDepth", String.valueOf(a2));
            IronSourceNetwork.b(this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "showInterstitial exception " + e.getMessage());
            interstitialSmashListener.b(new IronSourceError(1001, e.getMessage()));
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "showRewardedVideo");
        try {
            int a2 = SessionDepthManager.a().a(1);
            HashMap hashMap = new HashMap();
            hashMap.put("sessionDepth", String.valueOf(a2));
            IronSourceNetwork.b(this.mDemandSourceToRvAd.get(getDemandSourceName(jSONObject)), hashMap);
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "showRewardedVideo exception " + e.getMessage());
            rewardedVideoSmashListener.c(new IronSourceError(RV_SHOW_EXCEPTION, e.getMessage()));
        }
    }

    private void log(IronSourceLogger.IronSourceTag ironSourceTag, JSONObject jSONObject, String str) {
        IronSourceLoggerManager c = IronSourceLoggerManager.c();
        c.b(ironSourceTag, "IronSourceAdapter " + getDemandSourceName(jSONObject) + ": " + str, 0);
    }

    private class IronSourceRewardedVideoListener implements OnInterstitialListener {
        private String mDemandSourceName;
        boolean mIsRvDemandOnly;
        RewardedVideoSmashListener mListener;

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = false;
        }

        public void onInterstitialAdRewarded(String str, int i) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialAdRewarded demandSourceId=" + str + " amount=" + i);
            this.mListener.h();
        }

        public void onInterstitialClick() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialClick");
            this.mListener.g();
        }

        public void onInterstitialClose() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialClose");
            this.mListener.onRewardedVideoAdClosed();
        }

        public void onInterstitialEventNotificationReceived(String str, JSONObject jSONObject) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialEventNotificationReceived eventName=" + str);
            this.mListener.f();
        }

        public void onInterstitialInitFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialInitFailed");
        }

        public void onInterstitialInitSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialInitSuccess");
        }

        public void onInterstitialLoadFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialLoadFailed " + str);
            if (this.mIsRvDemandOnly) {
                this.mListener.d(ErrorBuilder.c(str));
            } else {
                this.mListener.a(false);
            }
        }

        public void onInterstitialLoadSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialLoadSuccess");
            if (this.mIsRvDemandOnly) {
                this.mListener.j();
            } else {
                this.mListener.a(true);
            }
        }

        public void onInterstitialOpen() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialOpen");
            this.mListener.onRewardedVideoAdOpened();
        }

        public void onInterstitialShowFailed(String str) {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, "rewardedVideoListener onInterstitialShowSuccess " + str);
            this.mListener.c(ErrorBuilder.b("Rewarded Video", str));
        }

        public void onInterstitialShowSuccess() {
            IronSourceAdapter ironSourceAdapter = IronSourceAdapter.this;
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_CALLBACK;
            ironSourceAdapter.log(ironSourceTag, this.mDemandSourceName + " rewardedVideoListener onInterstitialShowSuccess");
        }

        IronSourceRewardedVideoListener(RewardedVideoSmashListener rewardedVideoSmashListener, String str, boolean z) {
            this.mDemandSourceName = str;
            this.mListener = rewardedVideoSmashListener;
            this.mIsRvDemandOnly = z;
        }
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        log(IronSourceLogger.IronSourceTag.ADAPTER_API, jSONObject, "loadInterstitial");
        try {
            IronSourceNetwork.b(this.mDemandSourceToISAd.get(getDemandSourceName(jSONObject)));
        } catch (Exception e) {
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.ADAPTER_API;
            logError(ironSourceTag, "loadInterstitial exception " + e.getMessage());
            interstitialSmashListener.a(new IronSourceError(1000, e.getMessage()));
        }
    }
}
