package com.ironsource.adapters.ris;

import android.app.Activity;
import android.text.TextUtils;
import com.ironsource.mediationsdk.AbstractAdapter;
import com.ironsource.mediationsdk.AbstractSmash;
import com.ironsource.mediationsdk.logger.IronSourceError;
import com.ironsource.mediationsdk.logger.IronSourceLogger;
import com.ironsource.mediationsdk.logger.IronSourceLoggerManager;
import com.ironsource.mediationsdk.sdk.InterstitialSmashListener;
import com.ironsource.mediationsdk.sdk.RewardedInterstitialListener;
import com.ironsource.mediationsdk.sdk.RewardedVideoSmashListener;
import com.ironsource.mediationsdk.utils.ErrorBuilder;
import com.ironsource.mediationsdk.utils.IronSourceUtils;
import com.ironsource.mediationsdk.utils.SessionDepthManager;
import com.ironsource.sdk.SSAFactory;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class RISAdapter extends AbstractAdapter implements OnRewardedVideoListener {
    private final String AD_VISIBLE_EVENT_NAME = "impressions";
    private final String DYNAMIC_CONTROLLER_CONFIG = "controllerConfig";
    private final String DYNAMIC_CONTROLLER_DEBUG_MODE = "debugMode";
    private final String DYNAMIC_CONTROLLER_URL = "controllerUrl";
    private boolean hasAdAvailable = false;
    /* access modifiers changed from: private */
    public boolean mConsent;
    private boolean mDidReportInitStatus = false;
    /* access modifiers changed from: private */
    public boolean mDidSetConsent;
    /* access modifiers changed from: private */
    public SSAPublisher mSSAPublisher;

    private RISAdapter(String str) {
        super(str);
    }

    /* access modifiers changed from: private */
    public void applyConsent(boolean z) {
        if (this.mSSAPublisher != null) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("gdprConsentStatus", String.valueOf(z));
                jSONObject.put("demandSourceName", getProviderName());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.a(jSONObject);
        }
    }

    public static RISAdapter startAdapter(String str) {
        return new RISAdapter(str);
    }

    public void fetchRewardedVideo(JSONObject jSONObject) {
    }

    public String getCoreSDKVersion() {
        return SDKUtils.j();
    }

    public String getVersion() {
        return IronSourceUtils.b();
    }

    public void initInterstitial(final Activity activity, final String str, final String str2, JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        SDKUtils.g(jSONObject.optString("controllerUrl"));
        if (isAdaptersDebugEnabled()) {
            SDKUtils.a(3);
        } else {
            SDKUtils.a(jSONObject.optInt("debugMode", 0));
        }
        SDKUtils.f(jSONObject.optString("controllerConfig", ""));
        activity.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    SSAPublisher unused = RISAdapter.this.mSSAPublisher = SSAFactory.a(activity);
                    if (RISAdapter.this.mDidSetConsent) {
                        RISAdapter.this.applyConsent(RISAdapter.this.mConsent);
                    }
                    SSAFactory.a(activity).a(str, str2, RISAdapter.this.getProviderName(), (Map<String, String>) new HashMap(), (OnRewardedVideoListener) RISAdapter.this);
                } catch (Exception e) {
                    RISAdapter.this.onRVInitFail(e.getMessage());
                }
            }
        });
    }

    public void initRewardedVideo(Activity activity, String str, String str2, JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
    }

    public boolean isInterstitialReady(JSONObject jSONObject) {
        return this.hasAdAvailable;
    }

    public boolean isRewardedVideoAvailable(JSONObject jSONObject) {
        return false;
    }

    public void loadInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        if (this.hasAdAvailable) {
            Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
            while (it2.hasNext()) {
                InterstitialSmashListener next = it2.next();
                if (next != null) {
                    next.a();
                }
            }
            return;
        }
        Iterator<InterstitialSmashListener> it3 = this.mAllInterstitialSmashes.iterator();
        while (it3.hasNext()) {
            InterstitialSmashListener next2 = it3.next();
            if (next2 != null) {
                next2.a(ErrorBuilder.c("No Ads to Load"));
            }
        }
    }

    public void onPause(Activity activity) {
        SSAPublisher sSAPublisher = this.mSSAPublisher;
        if (sSAPublisher != null) {
            sSAPublisher.onPause(activity);
        }
    }

    public void onRVAdClicked() {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVAdClicked()", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.onInterstitialAdClicked();
        }
    }

    public void onRVAdClosed() {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVAdClosed()", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.b();
        }
    }

    public void onRVAdCredited(int i) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVAdCredited()", 1);
        RewardedInterstitialListener rewardedInterstitialListener = this.mRewardedInterstitial;
        if (rewardedInterstitialListener != null) {
            rewardedInterstitialListener.k();
        }
    }

    public void onRVAdOpened() {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVAdOpened()", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.d();
            this.mActiveInterstitialSmash.c();
        }
    }

    public void onRVEventNotificationReceived(String str, JSONObject jSONObject) {
        InterstitialSmashListener interstitialSmashListener;
        if (jSONObject != null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            c.b(ironSourceTag, getProviderName() + " :onRISEventNotificationReceived: " + str + " extData: " + jSONObject.toString(), 1);
            if (!TextUtils.isEmpty(str) && "impressions".equals(str) && (interstitialSmashListener = this.mActiveInterstitialSmash) != null) {
                interstitialSmashListener.e();
            }
        }
    }

    public void onRVInitFail(String str) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVInitFail()", 1);
        this.hasAdAvailable = false;
        if (!this.mDidReportInitStatus) {
            this.mDidReportInitStatus = true;
            Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
            while (it2.hasNext()) {
                InterstitialSmashListener next = it2.next();
                if (next != null) {
                    next.e(ErrorBuilder.a(str, "Interstitial"));
                }
            }
        }
    }

    public void onRVInitSuccess(AdUnitsReady adUnitsReady) {
        int i;
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVInitSuccess()", 1);
        boolean z = false;
        try {
            i = Integer.parseInt(adUnitsReady.b());
        } catch (NumberFormatException e) {
            IronSourceLoggerManager.c().a(IronSourceLogger.IronSourceTag.NATIVE, "onRVInitSuccess:parseInt()", (Throwable) e);
            i = 0;
        }
        if (i > 0) {
            z = true;
        }
        this.hasAdAvailable = z;
        if (!this.mDidReportInitStatus) {
            this.mDidReportInitStatus = true;
            Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
            while (it2.hasNext()) {
                InterstitialSmashListener next = it2.next();
                if (next != null) {
                    next.onInterstitialInitSuccess();
                }
            }
        }
    }

    public void onRVNoMoreOffers() {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVNoMoreOffers()", 1);
        if (!this.mDidReportInitStatus) {
            this.mDidReportInitStatus = true;
            Iterator<InterstitialSmashListener> it2 = this.mAllInterstitialSmashes.iterator();
            while (it2.hasNext()) {
                InterstitialSmashListener next = it2.next();
                if (next != null) {
                    next.onInterstitialInitSuccess();
                }
            }
        }
    }

    public void onRVShowFail(String str) {
        IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
        log(ironSourceTag, getProviderName() + ":onRVShowFail()", 1);
        InterstitialSmashListener interstitialSmashListener = this.mActiveInterstitialSmash;
        if (interstitialSmashListener != null) {
            interstitialSmashListener.b(new IronSourceError(509, "Show Failed"));
        }
    }

    public void onResume(Activity activity) {
        SSAPublisher sSAPublisher = this.mSSAPublisher;
        if (sSAPublisher != null) {
            sSAPublisher.onResume(activity);
        }
    }

    /* access modifiers changed from: protected */
    public void setConsent(boolean z) {
        this.mConsent = z;
        this.mDidSetConsent = true;
        applyConsent(z);
    }

    /* access modifiers changed from: protected */
    public void setMediationState(AbstractSmash.MEDIATION_STATE mediation_state, String str) {
        if (this.mSSAPublisher != null) {
            IronSourceLoggerManager c = IronSourceLoggerManager.c();
            IronSourceLogger.IronSourceTag ironSourceTag = IronSourceLogger.IronSourceTag.INTERNAL;
            c.b(ironSourceTag, getProviderName() + " :setMediationState(RIS:(rewardedvideo)):" + str + " , " + getProviderName() + " , " + mediation_state.a() + ")", 1);
            this.mSSAPublisher.a("rewardedvideo", getProviderName(), mediation_state.a());
        }
    }

    public void showInterstitial(JSONObject jSONObject, InterstitialSmashListener interstitialSmashListener) {
        this.mActiveInterstitialSmash = interstitialSmashListener;
        if (this.mSSAPublisher != null) {
            int a2 = SessionDepthManager.a().a(2);
            JSONObject jSONObject2 = new JSONObject();
            try {
                jSONObject2.put("demandSourceName", getProviderName());
                jSONObject2.put("sessionDepth", a2);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            this.mSSAPublisher.e(jSONObject2);
            return;
        }
        InterstitialSmashListener interstitialSmashListener2 = this.mActiveInterstitialSmash;
        if (interstitialSmashListener2 != null) {
            interstitialSmashListener2.b(new IronSourceError(509, "Please call init before calling showRewardedVideo"));
        }
    }

    public void showRewardedVideo(JSONObject jSONObject, RewardedVideoSmashListener rewardedVideoSmashListener) {
    }
}
