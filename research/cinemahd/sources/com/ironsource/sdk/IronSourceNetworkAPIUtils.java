package com.ironsource.sdk;

import com.unity3d.ads.metadata.MediationMetaData;
import org.json.JSONObject;

public class IronSourceNetworkAPIUtils {

    /* renamed from: a  reason: collision with root package name */
    static String f4754a = "ManRewInst_";

    public static String a(JSONObject jSONObject) {
        if (!jSONObject.optBoolean("rewarded")) {
            return jSONObject.optString(MediationMetaData.KEY_NAME);
        }
        return f4754a + jSONObject.optString(MediationMetaData.KEY_NAME);
    }
}
