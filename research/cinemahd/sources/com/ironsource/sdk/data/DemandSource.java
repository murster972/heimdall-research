package com.ironsource.sdk.data;

import com.ironsource.sdk.listeners.OnAdProductListener;
import java.util.HashMap;
import java.util.Map;

public class DemandSource {

    /* renamed from: a  reason: collision with root package name */
    private String f4880a;
    private String b;
    private int c = -1;
    private Map<String, String> d;
    private int e;
    private boolean f;
    private OnAdProductListener g;

    public DemandSource(String str, String str2, Map<String, String> map, OnAdProductListener onAdProductListener) {
        this.b = str;
        this.f4880a = str2;
        this.d = map;
        this.g = onAdProductListener;
        this.e = 0;
        this.f = false;
    }

    public void a(boolean z) {
        this.f = z;
    }

    public boolean b() {
        return this.f;
    }

    public int c() {
        return this.e;
    }

    public String d() {
        return this.f4880a;
    }

    public Map<String, String> e() {
        return this.d;
    }

    public String f() {
        return this.b;
    }

    public OnAdProductListener g() {
        return this.g;
    }

    public int h() {
        return this.c;
    }

    public boolean i() {
        Map<String, String> map = this.d;
        if (map == null || !map.containsKey("rewarded")) {
            return false;
        }
        return Boolean.parseBoolean(this.d.get("rewarded"));
    }

    public boolean a(int i) {
        return this.c == i;
    }

    public synchronized void b(int i) {
        this.e = i;
    }

    public void c(int i) {
        this.c = i;
    }

    public Map<String, String> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("demandSourceId", this.b);
        hashMap.put("demandSourceName", this.f4880a);
        Map<String, String> map = this.d;
        if (map != null) {
            hashMap.putAll(map);
        }
        return hashMap;
    }
}
