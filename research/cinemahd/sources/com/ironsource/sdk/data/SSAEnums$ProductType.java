package com.ironsource.sdk.data;

public enum SSAEnums$ProductType {
    Banner,
    OfferWall,
    Interstitial,
    OfferWallCredits,
    RewardedVideo
}
