package com.ironsource.sdk.data;

public class SSABCParameters extends SSAObj {
    private String b = "connectionRetries";
    private String c;

    public SSABCParameters() {
    }

    public String b() {
        return this.c;
    }

    public void f(String str) {
        this.c = str;
    }

    public SSABCParameters(String str) {
        super(str);
        if (a(this.b)) {
            f(d(this.b));
        }
    }
}
