package com.ironsource.sdk.data;

public enum SSAEnums$DebugMode {
    MODE_0(0),
    MODE_1(1),
    MODE_2(2),
    MODE_3(3);
    
    private int value;

    private SSAEnums$DebugMode(int i) {
        this.value = i;
    }

    public int a() {
        return this.value;
    }
}
