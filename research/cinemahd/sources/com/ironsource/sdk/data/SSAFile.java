package com.ironsource.sdk.data;

import com.facebook.common.util.UriUtil;

public class SSAFile extends SSAObj {
    private String b = UriUtil.LOCAL_FILE_SCHEME;
    private String c = "path";
    private String d = "lastUpdateTime";
    private String e;
    private String f;
    private String g;
    private String h;

    public SSAFile(String str) {
        super(str);
        if (a(this.b)) {
            h(d(this.b));
        }
        if (a(this.c)) {
            i(d(this.c));
        }
        if (a(this.d)) {
            g(d(this.d));
        }
    }

    private void h(String str) {
        this.e = str;
    }

    private void i(String str) {
        this.f = str;
    }

    public String b() {
        return this.g;
    }

    public String c() {
        return this.e;
    }

    public String d() {
        return this.h;
    }

    public String e() {
        return this.f;
    }

    public void f(String str) {
        this.g = str;
    }

    public void g(String str) {
        this.h = str;
    }

    public SSAFile(String str, String str2) {
        h(str);
        i(str2);
    }
}
