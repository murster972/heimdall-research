package com.ironsource.sdk.data;

import android.content.Context;
import com.ironsource.sdk.service.Connectivity.ConnectivityUtils;
import com.ironsource.sdk.utils.SDKUtils;

public class SSASession {

    /* renamed from: a  reason: collision with root package name */
    private long f4886a;
    private long b;
    private SessionType c;
    private String d;

    public enum SessionType {
        launched,
        backFromBG
    }

    public SSASession(Context context, SessionType sessionType) {
        b(SDKUtils.g().longValue());
        a(sessionType);
        a(ConnectivityUtils.b(context));
    }

    public void a() {
        a(SDKUtils.g().longValue());
    }

    public void b(long j) {
        this.f4886a = j;
    }

    public long c() {
        return this.b;
    }

    public long d() {
        return this.f4886a;
    }

    public SessionType e() {
        return this.c;
    }

    public void a(long j) {
        this.b = j;
    }

    public String b() {
        return this.d;
    }

    public void a(SessionType sessionType) {
        this.c = sessionType;
    }

    public void a(String str) {
        this.d = str;
    }
}
