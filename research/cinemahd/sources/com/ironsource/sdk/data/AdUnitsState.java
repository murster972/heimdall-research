package com.ironsource.sdk.data;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class AdUnitsState implements Parcelable {
    public static final Parcelable.Creator<AdUnitsState> CREATOR = new Parcelable.Creator<AdUnitsState>() {
        public AdUnitsState createFromParcel(Parcel parcel) {
            return new AdUnitsState(parcel);
        }

        public AdUnitsState[] newArray(int i) {
            return new AdUnitsState[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private String f4879a;
    private String b;
    private String c;
    private boolean d;
    private int e;
    private ArrayList<String> f;
    private ArrayList<String> g;
    private ArrayList<String> h;
    private String i;
    private String j;
    private Map<String, String> k;
    private boolean l;
    private boolean m;
    private Map<String, String> n;

    private void j() {
        this.d = false;
        this.e = -1;
        this.f = new ArrayList<>();
        this.g = new ArrayList<>();
        this.h = new ArrayList<>();
        new ArrayList();
        this.l = true;
        this.m = false;
        this.j = "";
        this.i = "";
        this.k = new HashMap();
        this.n = new HashMap();
    }

    public void a(Map<String, String> map) {
        this.n = map;
    }

    public String b() {
        return this.c;
    }

    public int c() {
        return this.e;
    }

    public String d() {
        return this.i;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.j;
    }

    public String f() {
        return this.f4879a;
    }

    public String g() {
        return this.b;
    }

    public boolean h() {
        return this.l;
    }

    public boolean i() {
        return this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        try {
            sb.append("shouldRestore:");
            sb.append(this.d);
            sb.append(", ");
            sb.append("displayedProduct:");
            sb.append(this.e);
            sb.append(", ");
            sb.append("ISReportInit:");
            sb.append(this.f);
            sb.append(", ");
            sb.append("ISInitSuccess:");
            sb.append(this.g);
            sb.append(", ");
            sb.append("ISAppKey");
            sb.append(this.i);
            sb.append(", ");
            sb.append("ISUserId");
            sb.append(this.j);
            sb.append(", ");
            sb.append("ISExtraParams");
            sb.append(this.k);
            sb.append(", ");
            sb.append("OWReportInit");
            sb.append(this.l);
            sb.append(", ");
            sb.append("OWInitSuccess");
            sb.append(this.m);
            sb.append(", ");
            sb.append("OWExtraParams");
            sb.append(this.n);
            sb.append(", ");
        } catch (Throwable unused) {
        }
        return sb.toString();
    }

    public void writeToParcel(Parcel parcel, int i2) {
        try {
            int i3 = 1;
            parcel.writeByte((byte) (this.d ? 1 : 0));
            parcel.writeInt(this.e);
            parcel.writeString(this.f4879a);
            parcel.writeString(this.b);
            parcel.writeString(this.c);
            parcel.writeString(this.i);
            parcel.writeString(this.j);
            parcel.writeString(new JSONObject(this.k).toString());
            parcel.writeByte((byte) (this.m ? 1 : 0));
            if (!this.l) {
                i3 = 0;
            }
            parcel.writeByte((byte) i3);
            parcel.writeString(new JSONObject(this.n).toString());
        } catch (Throwable unused) {
        }
    }

    public AdUnitsState() {
        j();
    }

    private Map<String, String> f(String str) {
        HashMap hashMap = new HashMap();
        try {
            JSONObject jSONObject = new JSONObject(str);
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                hashMap.put(next, jSONObject.getString(next));
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        } catch (Throwable th) {
            th.printStackTrace();
        }
        return hashMap;
    }

    public void a(String str, boolean z) {
        if (TextUtils.isEmpty(str)) {
            return;
        }
        if (!z) {
            this.h.remove(str);
        } else if (this.h.indexOf(str) == -1) {
            this.h.add(str);
        }
    }

    public void b(String str) {
        this.i = str;
    }

    public void c(String str) {
        this.j = str;
    }

    public void d(String str) {
        this.f4879a = str;
    }

    public void e(String str) {
        this.b = str;
    }

    public void b(boolean z) {
        this.l = z;
    }

    public void c(boolean z) {
        this.d = z;
    }

    private AdUnitsState(Parcel parcel) {
        j();
        try {
            boolean z = true;
            this.d = parcel.readByte() != 0;
            this.e = parcel.readInt();
            this.f4879a = parcel.readString();
            this.b = parcel.readString();
            this.c = parcel.readString();
            this.i = parcel.readString();
            this.j = parcel.readString();
            this.k = f(parcel.readString());
            this.m = parcel.readByte() != 0;
            if (parcel.readByte() == 0) {
                z = false;
            }
            this.l = z;
            this.n = f(parcel.readString());
        } catch (Throwable unused) {
            j();
        }
    }

    public void a(int i2) {
        this.e = i2;
    }

    public void a() {
        this.e = -1;
    }

    public void a(boolean z) {
        this.m = z;
    }

    public void a(String str) {
        this.c = str;
    }
}
