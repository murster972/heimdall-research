package com.ironsource.sdk.data;

public class AdUnitsReady extends SSAObj {
    private static String e = "type";
    private static String f = "numOfAdUnits";
    private static String g = "firstCampaignCredits";
    private static String h = "totalNumberCredits";
    private static String i = "productType";
    private String b;
    private String c;
    private boolean d;

    public AdUnitsReady(String str) {
        super(str);
        if (a(e)) {
            j(d(e));
        }
        if (a(f)) {
            g(d(f));
            a(true);
        } else {
            a(false);
        }
        if (a(g)) {
            f(d(g));
        }
        if (a(h)) {
            i(d(h));
        }
        if (a(i)) {
            h(d(i));
        }
    }

    private void a(boolean z) {
        this.d = z;
    }

    public String b() {
        return this.c;
    }

    public String c() {
        return this.b;
    }

    public boolean d() {
        return this.d;
    }

    public void f(String str) {
    }

    public void g(String str) {
        this.c = str;
    }

    public void h(String str) {
        this.b = str;
    }

    public void i(String str) {
    }

    public void j(String str) {
    }
}
