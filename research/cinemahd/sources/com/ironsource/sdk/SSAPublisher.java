package com.ironsource.sdk;

import android.app.Activity;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import java.util.Map;
import org.json.JSONObject;

public interface SSAPublisher {
    ISNAdView a(Activity activity, ISAdSize iSAdSize);

    void a(String str, String str2, int i);

    void a(String str, String str2, OnOfferWallListener onOfferWallListener);

    void a(String str, String str2, String str3, Map<String, String> map, OnBannerListener onBannerListener);

    void a(String str, String str2, String str3, Map<String, String> map, OnInterstitialListener onInterstitialListener);

    void a(String str, String str2, String str3, Map<String, String> map, OnRewardedVideoListener onRewardedVideoListener);

    void a(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener);

    void a(Map<String, String> map);

    void a(JSONObject jSONObject);

    boolean a(String str);

    void b(JSONObject jSONObject);

    void c(JSONObject jSONObject);

    void d(JSONObject jSONObject);

    void e(JSONObject jSONObject);

    void onPause(Activity activity);

    void onResume(Activity activity);
}
