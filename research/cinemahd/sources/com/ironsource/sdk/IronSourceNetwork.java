package com.ironsource.sdk;

import android.app.Activity;
import android.content.Context;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Map;
import org.json.JSONObject;

public class IronSourceNetwork {

    /* renamed from: a  reason: collision with root package name */
    private static IronSourceNetworkAPI f4753a;
    private static JSONObject b;

    public static synchronized void a(Activity activity, String str, String str2, Map<String, String> map) {
        synchronized (IronSourceNetwork.class) {
            if (f4753a == null) {
                SDKUtils.b(map);
                f4753a = IronSourceAdsPublisherAgent.a(activity, str, str2);
                a(b);
            }
        }
    }

    public static synchronized void b(IronSourceAdInstance ironSourceAdInstance) throws Exception {
        synchronized (IronSourceNetwork.class) {
            a(ironSourceAdInstance, (Map<String, String>) null);
        }
    }

    public static synchronized void c(JSONObject jSONObject) {
        synchronized (IronSourceNetwork.class) {
            TokenService.d().b(jSONObject);
        }
    }

    public static synchronized void b(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) throws Exception {
        synchronized (IronSourceNetwork.class) {
            a();
            f4753a.a(ironSourceAdInstance, map);
        }
    }

    private static synchronized void a() throws Exception {
        synchronized (IronSourceNetwork.class) {
            if (f4753a == null) {
                throw new NullPointerException("Call initSDK first");
            }
        }
    }

    public static synchronized void b(Activity activity) {
        synchronized (IronSourceNetwork.class) {
            if (f4753a != null) {
                f4753a.onResume(activity);
            }
        }
    }

    public static synchronized void a(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) throws Exception {
        synchronized (IronSourceNetwork.class) {
            a();
            f4753a.b(ironSourceAdInstance, map);
        }
    }

    public static synchronized void b(JSONObject jSONObject) {
        synchronized (IronSourceNetwork.class) {
            b = jSONObject;
            a(jSONObject);
        }
    }

    public static synchronized boolean a(IronSourceAdInstance ironSourceAdInstance) {
        synchronized (IronSourceNetwork.class) {
            if (f4753a == null) {
                return false;
            }
            boolean a2 = f4753a.a(ironSourceAdInstance);
            return a2;
        }
    }

    public static synchronized void a(Activity activity) {
        synchronized (IronSourceNetwork.class) {
            if (f4753a != null) {
                f4753a.onPause(activity);
            }
        }
    }

    public static synchronized void a(JSONObject jSONObject) {
        synchronized (IronSourceNetwork.class) {
            if (f4753a != null) {
                if (jSONObject != null) {
                    f4753a.a(jSONObject);
                }
            }
        }
    }

    public static synchronized String a(Context context) {
        String c;
        synchronized (IronSourceNetwork.class) {
            c = TokenService.d().c(context);
        }
        return c;
    }
}
