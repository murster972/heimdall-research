package com.ironsource.sdk;

import com.ironsource.sdk.listeners.OnInterstitialListener;
import java.util.HashMap;
import java.util.Map;

public class IronSourceAdInstance {

    /* renamed from: a  reason: collision with root package name */
    private String f4751a;
    private String b;
    private boolean c;
    private boolean d;
    private Map<String, String> e;
    private OnInterstitialListener f;
    private boolean g = false;

    IronSourceAdInstance(String str, String str2, boolean z, boolean z2, Map<String, String> map, OnInterstitialListener onInterstitialListener) {
        this.f4751a = str;
        this.b = str2;
        this.c = z;
        this.d = z2;
        this.e = map;
        this.f = onInterstitialListener;
    }

    public void a(boolean z) {
        this.g = z;
    }

    public final OnInterstitialListener b() {
        return this.f;
    }

    public String c() {
        return this.f4751a;
    }

    public String d() {
        return this.b;
    }

    public boolean e() {
        return this.d;
    }

    public boolean f() {
        return this.g;
    }

    public Map<String, String> a() {
        HashMap hashMap = new HashMap();
        hashMap.put("instanceId", this.f4751a);
        hashMap.put("instanceName", this.b);
        hashMap.put("rewarded", Boolean.toString(this.c));
        hashMap.put("inAppBidding", Boolean.toString(this.d));
        hashMap.put("apiVersion", String.valueOf(2));
        Map<String, String> map = this.e;
        if (map != null) {
            hashMap.putAll(map);
        }
        return hashMap;
    }
}
