package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums$ControllerState;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.Logger;
import java.util.Map;
import org.json.JSONObject;

public class ControllerManager implements ControllerEventListener {
    /* access modifiers changed from: private */
    public static final Handler g = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f4783a = ControllerManager.class.getSimpleName();
    /* access modifiers changed from: private */
    public IronSourceController b;
    private SSAEnums$ControllerState c = SSAEnums$ControllerState.None;
    private CountDownTimer d;
    private CommandExecutor e = new CommandExecutor();
    private CommandExecutor f = new CommandExecutor();

    public ControllerManager(Activity activity, TokenService tokenService, DemandSourceManager demandSourceManager) {
        a(activity, tokenService, demandSourceManager);
    }

    /* access modifiers changed from: private */
    public void g() {
        IronSourceController ironSourceController = this.b;
        if (ironSourceController != null) {
            ironSourceController.destroy();
        }
    }

    private boolean h() {
        return SSAEnums$ControllerState.Ready.equals(this.c);
    }

    public void d() {
        if (h()) {
            this.b.c();
        }
    }

    public IronSourceController e() {
        return this.b;
    }

    /* access modifiers changed from: private */
    public void b(Activity activity, TokenService tokenService, DemandSourceManager demandSourceManager) {
        this.b = new WebController(activity, demandSourceManager, this);
        WebController webController = (WebController) this.b;
        webController.a(new TokenJSAdapter(activity.getApplicationContext(), tokenService));
        webController.a(new OMIDJSAdapter(activity.getApplicationContext()));
        webController.a(new PermissionsJSAdapter(activity.getApplicationContext()));
        webController.a(new BannerJSAdapter());
        this.d = new CountDownTimer(200000, 1000) {
            public void onFinish() {
                Logger.c(ControllerManager.this.f4783a, "Global Controller Timer Finish");
                ControllerManager.this.g();
                ControllerManager.g.post(new Runnable() {
                    public void run() {
                        ControllerManager.this.c("Controller download timeout");
                    }
                });
            }

            public void onTick(long j) {
                String a2 = ControllerManager.this.f4783a;
                Logger.c(a2, "Global Controller Timer Tick " + j);
            }
        }.start();
        webController.e();
        this.e.b();
        this.e.a();
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        this.b = new NativeController(this);
        ((NativeController) this.b).b(str);
        this.e.b();
        this.e.a();
    }

    private void a(final Activity activity, final TokenService tokenService, final DemandSourceManager demandSourceManager) {
        g.post(new Runnable() {
            public void run() {
                ControllerManager.this.b(activity, tokenService, demandSourceManager);
            }
        });
    }

    public void a(Runnable runnable) {
        this.e.a(runnable);
    }

    public void a() {
        this.c = SSAEnums$ControllerState.Ready;
        CountDownTimer countDownTimer = this.d;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        this.f.b();
        this.f.a();
        this.b.b();
    }

    public void c() {
        if (h()) {
            this.b.a();
        }
    }

    public void a(final String str) {
        CountDownTimer countDownTimer = this.d;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        g();
        g.post(new Runnable() {
            public void run() {
                ControllerManager.this.c(str);
            }
        });
    }

    public void b() {
        this.c = SSAEnums$ControllerState.Loaded;
    }

    public void b(final DemandSource demandSource, final Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(demandSource, (Map<String, String>) map, dSInterstitialListener);
            }
        });
    }

    public boolean b(String str) {
        if (!h()) {
            return false;
        }
        return this.b.a(str);
    }

    public void a(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        final OnOfferWallListener onOfferWallListener2 = onOfferWallListener;
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(str3, str4, (Map<String, String>) map2, onOfferWallListener2);
            }
        });
    }

    public void a(final Map<String, String> map) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a((Map<String, String>) map);
            }
        });
    }

    public void b(Activity activity) {
        if (h()) {
            this.b.a((Context) activity);
        }
    }

    public void a(final String str, final String str2, final OnOfferWallListener onOfferWallListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(str, str2, onOfferWallListener);
            }
        });
    }

    public void a(String str, String str2, DemandSource demandSource, DSRewardedVideoListener dSRewardedVideoListener) {
        final String str3 = str;
        final String str4 = str2;
        final DemandSource demandSource2 = demandSource;
        final DSRewardedVideoListener dSRewardedVideoListener2 = dSRewardedVideoListener;
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(str3, str4, demandSource2, dSRewardedVideoListener2);
            }
        });
    }

    public void a(final JSONObject jSONObject, final DSRewardedVideoListener dSRewardedVideoListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(jSONObject, dSRewardedVideoListener);
            }
        });
    }

    public void a(String str, String str2, DemandSource demandSource, DSInterstitialListener dSInterstitialListener) {
        final String str3 = str;
        final String str4 = str2;
        final DemandSource demandSource2 = demandSource;
        final DSInterstitialListener dSInterstitialListener2 = dSInterstitialListener;
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(str3, str4, demandSource2, dSInterstitialListener2);
            }
        });
    }

    public void a(final String str, final DSInterstitialListener dSInterstitialListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(str, dSInterstitialListener);
            }
        });
    }

    public void a(final DemandSource demandSource, final Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.b(demandSource, map, dSInterstitialListener);
            }
        });
    }

    public void a(final JSONObject jSONObject, final DSInterstitialListener dSInterstitialListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(jSONObject, dSInterstitialListener);
            }
        });
    }

    public void a(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        final String str3 = str;
        final String str4 = str2;
        final DemandSource demandSource2 = demandSource;
        final DSBannerListener dSBannerListener2 = dSBannerListener;
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(str3, str4, demandSource2, dSBannerListener2);
            }
        });
    }

    public void a(final JSONObject jSONObject, final DSBannerListener dSBannerListener) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(jSONObject, dSBannerListener);
            }
        });
    }

    public void a(final JSONObject jSONObject) {
        this.f.a(new Runnable() {
            public void run() {
                ControllerManager.this.b.a(jSONObject);
            }
        });
    }

    public void a(Activity activity) {
        if (h()) {
            this.b.b(activity);
        }
    }

    public void a(ISNAdView iSNAdView) {
        IronSourceController ironSourceController = this.b;
        if (ironSourceController != null) {
            ironSourceController.setCommunicationWithAdView(iSNAdView);
        }
    }
}
