package com.ironsource.sdk.controller;

import android.text.TextUtils;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.ISNAdView.ISNAdViewDelegate;
import com.ironsource.sdk.utils.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class BannerJSAdapter implements ISNAdViewDelegate {
    private static final String c = "BannerJSAdapter";

    /* renamed from: a  reason: collision with root package name */
    private ISNAdView f4773a;
    private WebViewMessagingMediator b;

    public void a(WebViewMessagingMediator webViewMessagingMediator) {
        this.b = webViewMessagingMediator;
    }

    public void a(ISNAdView iSNAdView) {
        this.f4773a = iSNAdView;
        this.f4773a.setControllerDelegate(this);
    }

    public void a(String str, JSONObject jSONObject) {
        if (this.b != null && !TextUtils.isEmpty(str)) {
            this.b.a(str, jSONObject);
        }
    }

    public void a(String str, final String str2) {
        a(str, (JSONObject) new JSONObject(this) {
            {
                try {
                    put("errMsg", str2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("functionName");
            JSONObject optJSONObject = jSONObject.optJSONObject("functionParams");
            String optString2 = jSONObject.optString("success");
            String optString3 = jSONObject.optString("fail");
            if (TextUtils.isEmpty(optString)) {
                String str2 = c;
                Logger.c(str2, "BannerJSAdapter | sendMessageToISNAdView | Invalid message format: " + str);
            } else if (this.f4773a == null) {
                a(optString3, "Send message to ISNAdView failed");
            } else {
                this.f4773a.a(optString, optJSONObject, optString2, optString3);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
