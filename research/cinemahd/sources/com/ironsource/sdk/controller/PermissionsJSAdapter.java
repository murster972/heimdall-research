package com.ironsource.sdk.controller;

import android.content.Context;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.utils.Logger;
import com.vungle.warren.model.ReportDBAdapter;
import org.json.JSONException;
import org.json.JSONObject;

public class PermissionsJSAdapter {
    private static final String b = "PermissionsJSAdapter";

    /* renamed from: a  reason: collision with root package name */
    private Context f4825a;

    private static class FunctionCall {

        /* renamed from: a  reason: collision with root package name */
        String f4826a;
        JSONObject b;
        String c;
        String d;

        private FunctionCall() {
        }
    }

    public PermissionsJSAdapter(Context context) {
        this.f4825a = context;
    }

    private FunctionCall a(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        FunctionCall functionCall = new FunctionCall();
        functionCall.f4826a = jSONObject.optString("functionName");
        functionCall.b = jSONObject.optJSONObject("functionParams");
        functionCall.c = jSONObject.optString("success");
        functionCall.d = jSONObject.optString("fail");
        return functionCall;
    }

    public void b(JSONObject jSONObject, FunctionCall functionCall, WebController.NativeAPI.JSCallbackTask jSCallbackTask) {
        SSAObj sSAObj = new SSAObj();
        try {
            String string = jSONObject.getString("permission");
            sSAObj.a("permission", string);
            if (ApplicationContext.c(this.f4825a, string)) {
                sSAObj.a(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, String.valueOf(ApplicationContext.b(this.f4825a, string)));
                jSCallbackTask.a(true, functionCall.c, sSAObj);
                return;
            }
            sSAObj.a(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, "unhandledPermission");
            jSCallbackTask.a(false, functionCall.d, sSAObj);
        } catch (Exception e) {
            e.printStackTrace();
            sSAObj.a("errMsg", e.getMessage());
            jSCallbackTask.a(false, functionCall.d, sSAObj);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, WebController.NativeAPI.JSCallbackTask jSCallbackTask) throws Exception {
        FunctionCall a2 = a(str);
        if ("getPermissions".equals(a2.f4826a)) {
            a(a2.b, a2, jSCallbackTask);
        } else if ("isPermissionGranted".equals(a2.f4826a)) {
            b(a2.b, a2, jSCallbackTask);
        } else {
            String str2 = b;
            Logger.c(str2, "PermissionsJSAdapter unhandled API request " + str);
        }
    }

    public void a(JSONObject jSONObject, FunctionCall functionCall, WebController.NativeAPI.JSCallbackTask jSCallbackTask) {
        SSAObj sSAObj = new SSAObj();
        try {
            sSAObj.a("permissions", ApplicationContext.a(this.f4825a, jSONObject.getJSONArray("permissions")));
            jSCallbackTask.a(true, functionCall.c, sSAObj);
        } catch (Exception e) {
            e.printStackTrace();
            String str = b;
            Logger.c(str, "PermissionsJSAdapter getPermissions JSON Exception when getting permissions parameter " + e.getMessage());
            sSAObj.a("errMsg", e.getMessage());
            jSCallbackTask.a(false, functionCall.d, sSAObj);
        }
    }
}
