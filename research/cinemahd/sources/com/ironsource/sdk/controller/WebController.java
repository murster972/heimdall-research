package com.ironsource.sdk.controller;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.MutableContextWrapper;
import android.content.pm.ApplicationInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.DownloadListener;
import android.webkit.JavascriptInterface;
import android.webkit.ValueCallback;
import android.webkit.WebBackForwardList;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.Toast;
import com.facebook.common.util.UriUtil;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.environment.UrlHandler;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.constants.Constants$JSMethods;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.AdUnitsState;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSABCParameters;
import com.ironsource.sdk.data.SSAEnums$DebugMode;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.listeners.OnGenericFunctionListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnWebViewChangeListener;
import com.ironsource.sdk.listeners.internals.DSAdProductListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.precache.DownloadManager;
import com.ironsource.sdk.service.Connectivity.ConnectivityUtils;
import com.ironsource.sdk.service.ConnectivityAdapter;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import com.unity3d.ads.metadata.InAppPurchaseMetaData;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import com.unity3d.services.purchasing.core.TransactionErrorDetailsUtilities;
import com.vungle.warren.AdLoader;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WebController extends WebView implements IronSourceController, DownloadManager.OnPreCacheCompletion, DownloadListener {
    public static int R = 0;
    public static String S = "is_store";
    public static String T = "external_url";
    public static String U = "secondary_web_view";
    public static String V = "appIds";
    public static String W = "requestId";
    public static String c0 = "isInstalled";
    public static String d0 = "result";
    /* access modifiers changed from: private */
    public static String e0 = "success";
    /* access modifiers changed from: private */
    public static String f0 = "fail";
    /* access modifiers changed from: private */
    public Boolean A = null;
    /* access modifiers changed from: private */
    public String B;
    /* access modifiers changed from: private */
    public VideoEventsListener C;
    /* access modifiers changed from: private */
    public AdUnitsState D;
    private Object E = new Object();
    Context F;
    Handler G;
    /* access modifiers changed from: private */
    public boolean H = false;
    /* access modifiers changed from: private */
    public DemandSourceManager I;
    /* access modifiers changed from: private */
    public OMIDJSAdapter J;
    /* access modifiers changed from: private */
    public PermissionsJSAdapter K;
    /* access modifiers changed from: private */
    public BannerJSAdapter L;
    /* access modifiers changed from: private */
    public TokenJSAdapter M;
    private WebViewMessagingMediator N;
    /* access modifiers changed from: private */
    public ControllerEventListener O;
    /* access modifiers changed from: private */
    public ConnectivityAdapter P;
    /* access modifiers changed from: private */
    public OnWebViewChangeListener Q;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f4831a = WebController.class.getSimpleName();
    /* access modifiers changed from: private */
    public String b = "IronSource";
    /* access modifiers changed from: private */
    public String c;
    /* access modifiers changed from: private */
    public String d;
    private Map<String, String> e;
    /* access modifiers changed from: private */
    public DownloadManager f;
    /* access modifiers changed from: private */
    public boolean g;
    /* access modifiers changed from: private */
    public boolean h;
    private String i = "interrupt";
    /* access modifiers changed from: private */
    public CountDownTimer j;
    public CountDownTimer k;
    /* access modifiers changed from: private */
    public int l = 50;
    /* access modifiers changed from: private */
    public int m = 50;
    /* access modifiers changed from: private */
    public String n = "top-right";
    private ChromeClient o;
    /* access modifiers changed from: private */
    public View p;
    /* access modifiers changed from: private */
    public FrameLayout q;
    /* access modifiers changed from: private */
    public WebChromeClient.CustomViewCallback r;
    /* access modifiers changed from: private */
    public FrameLayout s;
    /* access modifiers changed from: private */
    public State t;
    private String u;
    /* access modifiers changed from: private */
    public DSRewardedVideoListener v;
    /* access modifiers changed from: private */
    public OnGenericFunctionListener w;
    /* access modifiers changed from: private */
    public DSInterstitialListener x;
    /* access modifiers changed from: private */
    public OnOfferWallListener y;
    /* access modifiers changed from: private */
    public DSBannerListener z;

    private class ChromeClient extends WebChromeClient {
        private ChromeClient() {
        }

        public View getVideoLoadingProgressView() {
            FrameLayout frameLayout = new FrameLayout(WebController.this.getCurrentActivityContext());
            frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
            return frameLayout;
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            Logger.c("MyApplication", consoleMessage.message() + " -- From line " + consoleMessage.lineNumber() + " of " + consoleMessage.sourceId());
            return true;
        }

        public boolean onCreateWindow(WebView webView, boolean z, boolean z2, Message message) {
            WebView webView2 = new WebView(webView.getContext());
            webView2.setWebChromeClient(this);
            webView2.setWebViewClient(new FrameBustWebViewClient());
            ((WebView.WebViewTransport) message.obj).setWebView(webView2);
            message.sendToTarget();
            Logger.c("onCreateWindow", "onCreateWindow");
            return true;
        }

        public void onHideCustomView() {
            Logger.c("Test", "onHideCustomView");
            if (WebController.this.p != null) {
                WebController.this.p.setVisibility(8);
                WebController.this.q.removeView(WebController.this.p);
                View unused = WebController.this.p = null;
                WebController.this.q.setVisibility(8);
                WebController.this.r.onCustomViewHidden();
                WebController.this.setVisibility(0);
            }
        }

        public void onShowCustomView(View view, WebChromeClient.CustomViewCallback customViewCallback) {
            Logger.c("Test", "onShowCustomView");
            WebController.this.setVisibility(8);
            if (WebController.this.p != null) {
                Logger.c("Test", "mCustomView != null");
                customViewCallback.onCustomViewHidden();
                return;
            }
            Logger.c("Test", "mCustomView == null");
            WebController.this.q.addView(view);
            View unused = WebController.this.p = view;
            WebChromeClient.CustomViewCallback unused2 = WebController.this.r = customViewCallback;
            WebController.this.q.setVisibility(0);
        }
    }

    private class FrameBustWebViewClient extends WebViewClient {
        private FrameBustWebViewClient() {
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Context currentActivityContext = WebController.this.getCurrentActivityContext();
            Intent intent = new Intent(currentActivityContext, OpenUrlActivity.class);
            intent.putExtra(WebController.T, str);
            intent.putExtra(WebController.U, false);
            currentActivityContext.startActivity(intent);
            return true;
        }
    }

    private interface OnInitProductHandler {
        void a(String str, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource);
    }

    static class Result {

        /* renamed from: a  reason: collision with root package name */
        String f4874a;

        Result() {
        }
    }

    public enum State {
        Display,
        Gone
    }

    private class SupersonicWebViewTouchListener implements View.OnTouchListener {
        private SupersonicWebViewTouchListener() {
        }

        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == 1) {
                float x = motionEvent.getX();
                float y = motionEvent.getY();
                String G = WebController.this.f4831a;
                StringBuilder sb = new StringBuilder();
                sb.append("X:");
                int i = (int) x;
                sb.append(i);
                sb.append(" Y:");
                int i2 = (int) y;
                sb.append(i2);
                Logger.c(G, sb.toString());
                int j = DeviceStatus.j();
                int d = DeviceStatus.d();
                String G2 = WebController.this.f4831a;
                Logger.c(G2, "Width:" + j + " Height:" + d);
                int a2 = SDKUtils.a((long) WebController.this.l);
                int a3 = SDKUtils.a((long) WebController.this.m);
                if ("top-right".equalsIgnoreCase(WebController.this.n)) {
                    i = j - i;
                } else if (!"top-left".equalsIgnoreCase(WebController.this.n)) {
                    if ("bottom-right".equalsIgnoreCase(WebController.this.n)) {
                        i = j - i;
                    } else if (!"bottom-left".equalsIgnoreCase(WebController.this.n)) {
                        i = 0;
                        i2 = 0;
                    }
                    i2 = d - i2;
                }
                if (i <= a2 && i2 <= a3) {
                    boolean unused = WebController.this.h = false;
                    if (WebController.this.j != null) {
                        WebController.this.j.cancel();
                    }
                    CountDownTimer unused2 = WebController.this.j = new CountDownTimer(AdLoader.RETRY_DELAY, 500) {
                        public void onFinish() {
                            Logger.c(WebController.this.f4831a, "Close Event Timer Finish");
                            if (WebController.this.h) {
                                boolean unused = WebController.this.h = false;
                            } else {
                                WebController.this.b("forceClose");
                            }
                        }

                        public void onTick(long j) {
                            String G = WebController.this.f4831a;
                            Logger.c(G, "Close Event Timer Tick " + j);
                        }
                    }.start();
                }
            }
            return false;
        }
    }

    private class ViewClient extends WebViewClient {
        private ViewClient() {
        }

        public void onPageFinished(WebView webView, String str) {
            Logger.c("onPageFinished", str);
            if (str.contains("adUnit") || str.contains("index.html")) {
                WebController.this.i();
            }
            super.onPageFinished(webView, str);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            Logger.c("onPageStarted", str);
            super.onPageStarted(webView, str, bitmap);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            Logger.c("onReceivedError", str2 + " " + str);
            super.onReceivedError(webView, i, str, str2);
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            boolean z;
            Logger.c("shouldInterceptRequest", str);
            try {
                z = new URL(str).getFile().contains("mraid.js");
            } catch (MalformedURLException unused) {
                z = false;
            }
            if (z) {
                String str2 = "file://" + WebController.this.B + File.separator + "mraid.js";
                try {
                    new FileInputStream(new File(str2));
                    return new WebResourceResponse("text/javascript", "UTF-8", getClass().getResourceAsStream(str2));
                } catch (FileNotFoundException unused2) {
                }
            }
            return super.shouldInterceptRequest(webView, str);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            Logger.c("shouldOverrideUrlLoading", str);
            try {
                if (WebController.this.c(str)) {
                    WebController.this.h();
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }
    }

    public WebController(Activity activity, DemandSourceManager demandSourceManager, ControllerEventListener controllerEventListener) {
        super(activity.getApplicationContext());
        this.F = new MutableContextWrapper(activity);
        Logger.c(this.f4831a, "C'tor");
        this.O = controllerEventListener;
        this.B = c(this.F.getApplicationContext());
        this.I = demandSourceManager;
        f(this.F);
        this.D = new AdUnitsState();
        this.f = getDownloadManager();
        this.f.a((DownloadManager.OnPreCacheCompletion) this);
        this.o = new ChromeClient();
        setWebViewClient(new ViewClient());
        setWebChromeClient(this.o);
        r();
        p();
        setDownloadListener(this);
        setOnTouchListener(new SupersonicWebViewTouchListener());
        this.G = d();
        this.P = d((Context) activity);
        b((Context) activity);
        setDebugMode(SDKUtils.h());
    }

    /* access modifiers changed from: private */
    public WebView getWebview() {
        return this;
    }

    private void setDisplayZoomControls(WebSettings webSettings) {
        if (Build.VERSION.SDK_INT > 11) {
            webSettings.setDisplayZoomControls(false);
        }
    }

    @SuppressLint({"NewApi"})
    private void setMediaPlaybackJellyBean(WebSettings webSettings) {
        if (Build.VERSION.SDK_INT >= 17) {
            webSettings.setMediaPlaybackRequiresUserGesture(false);
        }
    }

    private void setWebDebuggingEnabled(JSONObject jSONObject) {
        if (jSONObject.optBoolean("inspectWebview")) {
            q();
        }
    }

    /* access modifiers changed from: private */
    public void setWebviewBackground(String str) {
        String d2 = new SSAObj(str).d(ViewProps.COLOR);
        setBackgroundColor(!"transparent".equalsIgnoreCase(d2) ? Color.parseColor(d2) : 0);
    }

    private void setWebviewCache(String str) {
        if (str.equalsIgnoreCase("0")) {
            getSettings().setCacheMode(2);
        } else {
            getSettings().setCacheMode(-1);
        }
    }

    public void destroy() {
        super.destroy();
        DownloadManager downloadManager = this.f;
        if (downloadManager != null) {
            downloadManager.d();
        }
        ConnectivityAdapter connectivityAdapter = this.P;
        if (connectivityAdapter != null) {
            connectivityAdapter.a();
        }
        this.G = null;
        this.F = null;
    }

    public WebViewMessagingMediator getControllerDelegate() {
        if (this.N == null) {
            this.N = new WebViewMessagingMediator() {
                public void a(String str, JSONObject jSONObject) {
                    WebController.this.k(WebController.this.d(str, jSONObject.toString()));
                }
            };
        }
        return this.N;
    }

    public String getControllerKeyPressed() {
        String str = this.i;
        setControllerKeyPressed("interrupt");
        return str;
    }

    public Context getCurrentActivityContext() {
        return ((MutableContextWrapper) this.F).getBaseContext();
    }

    public int getDebugMode() {
        return R;
    }

    /* access modifiers changed from: package-private */
    public DownloadManager getDownloadManager() {
        return DownloadManager.a(this.B);
    }

    public FrameLayout getLayout() {
        return this.s;
    }

    public String getOrientationState() {
        return this.u;
    }

    public AdUnitsState getSavedState() {
        return this.D;
    }

    public State getState() {
        return this.t;
    }

    public void onDownloadStart(String str, String str2, String str3, String str4, long j2) {
        String str5 = this.f4831a;
        Logger.c(str5, str + " " + str4);
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4) {
            return super.onKeyDown(i2, keyEvent);
        }
        if (!this.Q.a()) {
            return super.onKeyDown(i2, keyEvent);
        }
        return true;
    }

    public WebBackForwardList saveState(Bundle bundle) {
        return super.saveState(bundle);
    }

    public void setCommunicationWithAdView(ISNAdView iSNAdView) {
        BannerJSAdapter bannerJSAdapter = this.L;
        if (bannerJSAdapter != null) {
            bannerJSAdapter.a(iSNAdView);
        }
    }

    public void setControllerKeyPressed(String str) {
        this.i = str;
    }

    public void setDebugMode(int i2) {
        R = i2;
    }

    public void setOnWebViewControllerChangeListener(OnWebViewChangeListener onWebViewChangeListener) {
        this.Q = onWebViewChangeListener;
    }

    public void setOrientationState(String str) {
        this.u = str;
    }

    public void setState(State state) {
        this.t = state;
    }

    public void setVideoEventsListener(VideoEventsListener videoEventsListener) {
        this.C = videoEventsListener;
    }

    /* access modifiers changed from: private */
    public SSAEnums$ProductType j(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase(SSAEnums$ProductType.Interstitial.toString())) {
            return SSAEnums$ProductType.Interstitial;
        }
        if (str.equalsIgnoreCase(SSAEnums$ProductType.RewardedVideo.toString())) {
            return SSAEnums$ProductType.RewardedVideo;
        }
        if (str.equalsIgnoreCase(SSAEnums$ProductType.OfferWall.toString())) {
            return SSAEnums$ProductType.OfferWall;
        }
        if (str.equalsIgnoreCase(SSAEnums$ProductType.Banner.toString())) {
            return SSAEnums$ProductType.Banner;
        }
        return null;
    }

    /* access modifiers changed from: private */
    public boolean l(String str) {
        boolean z2 = false;
        if (TextUtils.isEmpty(str)) {
            Logger.a(this.f4831a, "Trying to trigger a listener - no product was found");
            return false;
        }
        if (!str.equalsIgnoreCase(SSAEnums$ProductType.Interstitial.toString()) ? !str.equalsIgnoreCase(SSAEnums$ProductType.RewardedVideo.toString()) ? !str.equalsIgnoreCase(SSAEnums$ProductType.Banner.toString()) ? (str.equalsIgnoreCase(SSAEnums$ProductType.OfferWall.toString()) || str.equalsIgnoreCase(SSAEnums$ProductType.OfferWallCredits.toString())) && this.y != null : this.z != null : this.v != null : this.x != null) {
            z2 = true;
        }
        if (!z2) {
            String str2 = this.f4831a;
            Logger.a(str2, "Trying to trigger a listener - no listener was found for product " + str);
        }
        return z2;
    }

    /* access modifiers changed from: private */
    public void o() {
        OnWebViewChangeListener onWebViewChangeListener = this.Q;
        if (onWebViewChangeListener != null) {
            onWebViewChangeListener.b();
        }
    }

    private void p() {
        SecureMessagingService secureMessagingService = new SecureMessagingService(SecureMessagingService.b());
        addJavascriptInterface(a(secureMessagingService), "Android");
        addJavascriptInterface(b(secureMessagingService), "GenerateTokenForMessaging");
    }

    @SuppressLint({"NewApi"})
    private void q() {
        if (Build.VERSION.SDK_INT >= 19) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
    }

    private void r() {
        WebSettings settings = getSettings();
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);
        setVerticalScrollBarEnabled(false);
        setHorizontalScrollBarEnabled(false);
        settings.setBuiltInZoomControls(false);
        settings.setJavaScriptEnabled(true);
        settings.setSupportMultipleWindows(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);
        settings.setGeolocationEnabled(true);
        settings.setGeolocationDatabasePath("/data/data/org.itri.html5webview/databases/");
        settings.setDomStorageEnabled(true);
        try {
            setDisplayZoomControls(settings);
            setMediaPlaybackJellyBean(settings);
        } catch (Throwable th) {
            String str = this.f4831a;
            Logger.b(str, "setWebSettings - " + th.toString());
        }
    }

    public void i() {
        k(i("pageFinished"));
    }

    public void k() {
        this.C = null;
    }

    public class NativeAPI {
        public NativeAPI() {
        }

        private void b(String str, String str2) {
            if (!TextUtils.isEmpty(str)) {
                WebController.this.k(WebController.this.d(str, str2));
            }
        }

        /* access modifiers changed from: package-private */
        public String a(String str, String str2) throws JSONException {
            JSONObject jSONObject = new JSONObject(str);
            JSONObject jSONObject2 = new JSONObject(str2);
            jSONObject.putOpt("testerABGroup", jSONObject2.get("testerABGroup"));
            jSONObject.putOpt("testFriendlyName", jSONObject2.get("testFriendlyName"));
            return jSONObject.toString();
        }

        @JavascriptInterface
        public void adClicked(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "adClicked(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d("productType");
            final String a2 = SDKUtils.a(sSAObj);
            if (!TextUtils.isEmpty(a2)) {
                final SSAEnums$ProductType d2 = WebController.this.j(d);
                final DSAdProductListener a3 = WebController.this.a(d2);
                if (d2 != null && a3 != null) {
                    WebController.this.a((Runnable) new Runnable(this) {
                        public void run() {
                            a3.c(d2, a2);
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void adCredited(String str) {
            final String str2;
            final boolean z;
            final boolean z2;
            Log.d(WebController.this.b, "adCredited(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d("credits");
            boolean z3 = false;
            final int parseInt = d != null ? Integer.parseInt(d) : 0;
            final String a2 = SDKUtils.a(sSAObj);
            final String d2 = sSAObj.d("productType");
            if (TextUtils.isEmpty(d2)) {
                Log.d(WebController.this.b, "adCredited | not product NAME !!!!");
            }
            if (SSAEnums$ProductType.Interstitial.toString().equalsIgnoreCase(d2)) {
                a(a2, parseInt);
                return;
            }
            String d3 = sSAObj.d("total");
            final int parseInt2 = d3 != null ? Integer.parseInt(d3) : 0;
            sSAObj.c("externalPoll");
            if (!SSAEnums$ProductType.OfferWall.toString().equalsIgnoreCase(d2)) {
                str2 = null;
                z2 = false;
                z = false;
            } else if (sSAObj.e(InAppPurchaseMetaData.KEY_SIGNATURE) || sSAObj.e(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP) || sSAObj.e("totalCreditsFlag")) {
                WebController.this.a(str, false, "One of the keys are missing: signature/timestamp/totalCreditsFlag", (String) null);
                return;
            } else {
                if (sSAObj.d(InAppPurchaseMetaData.KEY_SIGNATURE).equalsIgnoreCase(SDKUtils.d(d3 + WebController.this.c + WebController.this.d))) {
                    z3 = true;
                } else {
                    WebController.this.a(str, false, "Controller signature is not equal to SDK signature", (String) null);
                }
                boolean c = sSAObj.c("totalCreditsFlag");
                str2 = sSAObj.d(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP);
                z = c;
                z2 = z3;
            }
            if (WebController.this.l(d2)) {
                final String str3 = str;
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        if (d2.equalsIgnoreCase(SSAEnums$ProductType.RewardedVideo.toString())) {
                            WebController.this.v.a(a2, parseInt);
                        } else if (d2.equalsIgnoreCase(SSAEnums$ProductType.OfferWall.toString()) && z2 && WebController.this.y.onOWAdCredited(parseInt, parseInt2, z) && !TextUtils.isEmpty(str2)) {
                            if (IronSourceSharedPrefHelper.h().a(str2, WebController.this.c, WebController.this.d)) {
                                WebController.this.a(str3, true, (String) null, (String) null);
                            } else {
                                WebController.this.a(str3, false, "Time Stamp could not be stored", (String) null);
                            }
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void adUnitsReady(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "adUnitsReady(" + str + ")");
            final String a2 = SDKUtils.a(new SSAObj(str));
            final AdUnitsReady adUnitsReady = new AdUnitsReady(str);
            if (!adUnitsReady.d()) {
                WebController.this.a(str, false, "Num Of Ad Units Do Not Exist", (String) null);
                return;
            }
            WebController.this.a(str, true, (String) null, (String) null);
            String c = adUnitsReady.c();
            if (SSAEnums$ProductType.RewardedVideo.toString().equalsIgnoreCase(c) && WebController.this.l(c)) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        if (Integer.parseInt(adUnitsReady.b()) > 0) {
                            Log.d(WebController.this.f4831a, "onRVInitSuccess()");
                            WebController.this.v.a(SSAEnums$ProductType.RewardedVideo, a2, adUnitsReady);
                            return;
                        }
                        WebController.this.v.c(a2);
                    }
                });
            }
        }

        @JavascriptInterface
        public void bannerViewAPI(String str) {
            try {
                WebController.this.L.a(str);
            } catch (Exception e) {
                e.printStackTrace();
                String G = WebController.this.f4831a;
                Logger.b(G, "bannerViewAPI failed with exception " + e.getMessage());
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x005e, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0062;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0068  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void checkInstalledApps(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r0 = r0.f4831a
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "checkInstalledApps("
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.ironsource.sdk.utils.Logger.c(r0, r1)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r0 = r0.h((java.lang.String) r5)
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r1 = r1.g((java.lang.String) r5)
                com.ironsource.sdk.data.SSAObj r2 = new com.ironsource.sdk.data.SSAObj
                r2.<init>(r5)
                java.lang.String r5 = com.ironsource.sdk.controller.WebController.V
                java.lang.String r5 = r2.d(r5)
                java.lang.String r3 = com.ironsource.sdk.controller.WebController.W
                java.lang.String r2 = r2.d(r3)
                com.ironsource.sdk.controller.WebController r3 = com.ironsource.sdk.controller.WebController.this
                java.lang.Object[] r5 = r3.f((java.lang.String) r5, (java.lang.String) r2)
                r2 = 0
                r2 = r5[r2]
                java.lang.String r2 = (java.lang.String) r2
                r3 = 1
                r5 = r5[r3]
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x005a
                boolean r5 = android.text.TextUtils.isEmpty(r1)
                if (r5 != 0) goto L_0x0061
                r0 = r1
                goto L_0x0062
            L_0x005a:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0061
                goto L_0x0062
            L_0x0061:
                r0 = 0
            L_0x0062:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0077
                com.ironsource.sdk.controller.WebController r5 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r1 = "onCheckInstalledAppsSuccess"
                java.lang.String r3 = "onCheckInstalledAppsFail"
                java.lang.String r5 = r5.a((java.lang.String) r0, (java.lang.String) r2, (java.lang.String) r1, (java.lang.String) r3)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                r0.k((java.lang.String) r5)
            L_0x0077:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.WebController.NativeAPI.checkInstalledApps(java.lang.String):void");
        }

        @JavascriptInterface
        public void deleteFile(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "deleteFile(" + str + ")");
            SSAFile sSAFile = new SSAFile(str);
            if (!IronSourceStorageUtils.d(WebController.this.B, sSAFile.e())) {
                WebController.this.a(str, false, "File not exist", DiskLruCache.VERSION_1);
                return;
            }
            WebController.this.a(str, IronSourceStorageUtils.a(WebController.this.B, sSAFile.e(), sSAFile.c()), (String) null, (String) null);
        }

        @JavascriptInterface
        public void deleteFolder(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "deleteFolder(" + str + ")");
            SSAFile sSAFile = new SSAFile(str);
            if (!IronSourceStorageUtils.d(WebController.this.B, sSAFile.e())) {
                WebController.this.a(str, false, "Folder not exist", DiskLruCache.VERSION_1);
                return;
            }
            WebController.this.a(str, IronSourceStorageUtils.b(WebController.this.B, sSAFile.e()), (String) null, (String) null);
        }

        @JavascriptInterface
        public void displayWebView(String str) {
            Intent intent;
            String G = WebController.this.f4831a;
            Logger.c(G, "displayWebView(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            SSAObj sSAObj = new SSAObj(str);
            boolean booleanValue = ((Boolean) sSAObj.b(ViewProps.DISPLAY)).booleanValue();
            String d = sSAObj.d("productType");
            boolean c = sSAObj.c("standaloneView");
            String a2 = SDKUtils.a(sSAObj);
            if (booleanValue) {
                boolean unused = WebController.this.H = sSAObj.c("immersive");
                boolean c2 = sSAObj.c("activityThemeTranslucent");
                State state = WebController.this.getState();
                State state2 = State.Display;
                if (state != state2) {
                    WebController.this.setState(state2);
                    String G2 = WebController.this.f4831a;
                    Logger.c(G2, "State: " + WebController.this.t);
                    Context currentActivityContext = WebController.this.getCurrentActivityContext();
                    String orientationState = WebController.this.getOrientationState();
                    int c3 = DeviceStatus.c(currentActivityContext);
                    if (c) {
                        ControllerView controllerView = new ControllerView(currentActivityContext);
                        controllerView.addView(WebController.this.s);
                        controllerView.a(WebController.this);
                        return;
                    }
                    if (c2) {
                        intent = new Intent(currentActivityContext, InterstitialActivity.class);
                    } else {
                        intent = new Intent(currentActivityContext, ControllerActivity.class);
                    }
                    if (SSAEnums$ProductType.RewardedVideo.toString().equalsIgnoreCase(d)) {
                        if ("application".equals(orientationState)) {
                            orientationState = SDKUtils.c(DeviceStatus.a(WebController.this.getCurrentActivityContext()));
                        }
                        intent.putExtra("productType", SSAEnums$ProductType.RewardedVideo.toString());
                        WebController.this.D.a(SSAEnums$ProductType.RewardedVideo.ordinal());
                        WebController.this.D.a(a2);
                        if (WebController.this.l(SSAEnums$ProductType.RewardedVideo.toString())) {
                            WebController.this.v.a(SSAEnums$ProductType.RewardedVideo, a2);
                        }
                    } else if (SSAEnums$ProductType.OfferWall.toString().equalsIgnoreCase(d)) {
                        intent.putExtra("productType", SSAEnums$ProductType.OfferWall.toString());
                        WebController.this.D.a(SSAEnums$ProductType.OfferWall.ordinal());
                    } else if (SSAEnums$ProductType.Interstitial.toString().equalsIgnoreCase(d)) {
                        if ("application".equals(orientationState)) {
                            orientationState = SDKUtils.c(DeviceStatus.a(WebController.this.getCurrentActivityContext()));
                        }
                        intent.putExtra("productType", SSAEnums$ProductType.Interstitial.toString());
                    }
                    intent.setFlags(536870912);
                    intent.putExtra("immersive", WebController.this.H);
                    intent.putExtra("orientation_set_flag", orientationState);
                    intent.putExtra("rotation_set_flag", c3);
                    currentActivityContext.startActivity(intent);
                    return;
                }
                String G3 = WebController.this.f4831a;
                Logger.c(G3, "State: " + WebController.this.t);
                return;
            }
            WebController.this.setState(State.Gone);
            WebController.this.o();
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x005c, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0060;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x0066  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void getApplicationInfo(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r0 = r0.f4831a
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getApplicationInfo("
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.ironsource.sdk.utils.Logger.c(r0, r1)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r0 = r0.h((java.lang.String) r5)
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r1 = r1.g((java.lang.String) r5)
                com.ironsource.sdk.data.SSAObj r2 = new com.ironsource.sdk.data.SSAObj
                r2.<init>(r5)
                java.lang.String r5 = "productType"
                java.lang.String r5 = r2.d(r5)
                java.lang.String r2 = com.ironsource.sdk.utils.SDKUtils.a((com.ironsource.sdk.data.SSAObj) r2)
                com.ironsource.sdk.controller.WebController r3 = com.ironsource.sdk.controller.WebController.this
                java.lang.Object[] r5 = r3.e((java.lang.String) r5, (java.lang.String) r2)
                r2 = 0
                r2 = r5[r2]
                java.lang.String r2 = (java.lang.String) r2
                r3 = 1
                r5 = r5[r3]
                java.lang.Boolean r5 = (java.lang.Boolean) r5
                boolean r5 = r5.booleanValue()
                if (r5 == 0) goto L_0x0058
                boolean r5 = android.text.TextUtils.isEmpty(r1)
                if (r5 != 0) goto L_0x005f
                r0 = r1
                goto L_0x0060
            L_0x0058:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x005f
                goto L_0x0060
            L_0x005f:
                r0 = 0
            L_0x0060:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0075
                com.ironsource.sdk.controller.WebController r5 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r1 = "onGetApplicationInfoSuccess"
                java.lang.String r3 = "onGetApplicationInfoFail"
                java.lang.String r5 = r5.a((java.lang.String) r0, (java.lang.String) r2, (java.lang.String) r1, (java.lang.String) r3)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                r0.k((java.lang.String) r5)
            L_0x0075:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.WebController.NativeAPI.getApplicationInfo(java.lang.String):void");
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x0062, code lost:
            if (android.text.TextUtils.isEmpty(r5) == false) goto L_0x0064;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0055, code lost:
            if (android.text.TextUtils.isEmpty(r5) == false) goto L_0x0064;
         */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void getAppsInstallTime(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.data.SSAObj r0 = new com.ironsource.sdk.data.SSAObj
                r0.<init>(r5)
                java.lang.String r1 = "systemApps"
                java.lang.String r0 = r0.d(r1)     // Catch:{ Exception -> 0x001f }
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this     // Catch:{ Exception -> 0x001f }
                android.content.Context r1 = r1.getContext()     // Catch:{ Exception -> 0x001f }
                boolean r0 = java.lang.Boolean.parseBoolean(r0)     // Catch:{ Exception -> 0x001f }
                org.json.JSONObject r0 = com.ironsource.environment.DeviceStatus.a(r1, r0)     // Catch:{ Exception -> 0x001f }
                java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x001f }
                r1 = 0
                goto L_0x0048
            L_0x001f:
                r0 = move-exception
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r1 = r1.f4831a
                java.lang.StringBuilder r2 = new java.lang.StringBuilder
                r2.<init>()
                java.lang.String r3 = "getAppsInstallTime failed("
                r2.append(r3)
                java.lang.String r3 = r0.getLocalizedMessage()
                r2.append(r3)
                java.lang.String r3 = ")"
                r2.append(r3)
                java.lang.String r2 = r2.toString()
                com.ironsource.sdk.utils.Logger.c(r1, r2)
                java.lang.String r0 = r0.getLocalizedMessage()
                r1 = 1
            L_0x0048:
                r2 = 0
                if (r1 == 0) goto L_0x0058
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r5 = r1.g((java.lang.String) r5)
                boolean r1 = android.text.TextUtils.isEmpty(r5)
                if (r1 != 0) goto L_0x0065
                goto L_0x0064
            L_0x0058:
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r5 = r1.h((java.lang.String) r5)
                boolean r1 = android.text.TextUtils.isEmpty(r5)
                if (r1 != 0) goto L_0x0065
            L_0x0064:
                r2 = r5
            L_0x0065:
                boolean r5 = android.text.TextUtils.isEmpty(r2)
                if (r5 != 0) goto L_0x0087
                java.nio.charset.Charset r5 = java.nio.charset.Charset.defaultCharset()     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                java.lang.String r5 = r5.name()     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                java.lang.String r0 = java.net.URLDecoder.decode(r0, r5)     // Catch:{ UnsupportedEncodingException -> 0x0078 }
                goto L_0x007c
            L_0x0078:
                r5 = move-exception
                r5.printStackTrace()
            L_0x007c:
                com.ironsource.sdk.controller.WebController r5 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r5 = r5.d((java.lang.String) r2, (java.lang.String) r0)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                r0.k((java.lang.String) r5)
            L_0x0087:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.WebController.NativeAPI.getAppsInstallTime(java.lang.String):void");
        }

        @JavascriptInterface
        public void getCachedFilesMap(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "getCachedFilesMap(" + str + ")");
            String a2 = WebController.this.h(str);
            if (!TextUtils.isEmpty(a2)) {
                SSAObj sSAObj = new SSAObj(str);
                if (!sSAObj.a("path")) {
                    WebController.this.a(str, false, "path key does not exist", (String) null);
                    return;
                }
                String str2 = (String) sSAObj.b("path");
                if (!IronSourceStorageUtils.d(WebController.this.B, str2)) {
                    WebController.this.a(str, false, "path file does not exist on disk", (String) null);
                    return;
                }
                WebController.this.k(WebController.this.a(a2, IronSourceStorageUtils.c(WebController.this.B, str2), "onGetCachedFilesMapSuccess", "onGetCachedFilesMapFail"));
            }
        }

        @JavascriptInterface
        public void getConnectivityInfo(String str) {
            String str2;
            String G = WebController.this.f4831a;
            Logger.c(G, "getConnectivityInfo(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d(WebController.e0);
            String d2 = sSAObj.d(WebController.f0);
            JSONObject jSONObject = new JSONObject();
            if (WebController.this.P != null) {
                jSONObject = WebController.this.P.a(WebController.this.getContext());
            }
            if (jSONObject.length() > 0) {
                str2 = WebController.this.d(d, jSONObject.toString());
            } else {
                str2 = WebController.this.d(d2, WebController.this.a("errMsg", "failed to retrieve connection info", (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false));
            }
            WebController.this.k(str2);
        }

        @JavascriptInterface
        public void getControllerConfig(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "getControllerConfig(" + str + ")");
            String d = new SSAObj(str).d(WebController.e0);
            if (!TextUtils.isEmpty(d)) {
                String d2 = SDKUtils.d();
                String k = SDKUtils.k();
                if (a(k)) {
                    try {
                        d2 = a(d2, k);
                    } catch (JSONException unused) {
                        Logger.a(WebController.this.f4831a, "getControllerConfig Error while parsing Tester AB Group parameters");
                    }
                }
                WebController.this.k(WebController.this.d(d, d2));
            }
        }

        @JavascriptInterface
        public void getDemandSourceState(String str) {
            String str2;
            String G = WebController.this.f4831a;
            Logger.c(G, "getMediationState(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d("demandSourceName");
            String a2 = SDKUtils.a(sSAObj);
            String d2 = sSAObj.d("productType");
            if (d2 != null && d != null) {
                try {
                    SSAEnums$ProductType e = SDKUtils.e(d2);
                    if (e != null) {
                        DemandSource a3 = WebController.this.I.a(e, a2);
                        JSONObject jSONObject = new JSONObject();
                        jSONObject.put("productType", d2);
                        jSONObject.put("demandSourceName", d);
                        jSONObject.put("demandSourceId", a2);
                        if (a3 == null || a3.a(-1)) {
                            str2 = WebController.this.g(str);
                        } else {
                            str2 = WebController.this.h(str);
                            jSONObject.put(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, a3.h());
                        }
                        b(str2, jSONObject.toString());
                    }
                } catch (Exception e2) {
                    WebController.this.a(str, false, e2.getMessage(), (String) null);
                    e2.printStackTrace();
                }
            }
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0051, code lost:
            if (android.text.TextUtils.isEmpty(r0) == false) goto L_0x0055;
         */
        /* JADX WARNING: Removed duplicated region for block: B:10:0x005b  */
        /* JADX WARNING: Removed duplicated region for block: B:12:? A[RETURN, SYNTHETIC] */
        @android.webkit.JavascriptInterface
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void getDeviceStatus(java.lang.String r5) {
            /*
                r4 = this;
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r0 = r0.f4831a
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r2 = "getDeviceStatus("
                r1.append(r2)
                r1.append(r5)
                java.lang.String r2 = ")"
                r1.append(r2)
                java.lang.String r1 = r1.toString()
                com.ironsource.sdk.utils.Logger.c(r0, r1)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r0 = r0.h((java.lang.String) r5)
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r5 = r1.g((java.lang.String) r5)
                com.ironsource.sdk.controller.WebController r1 = com.ironsource.sdk.controller.WebController.this
                android.content.Context r2 = r1.getContext()
                java.lang.Object[] r1 = r1.e((android.content.Context) r2)
                r2 = 0
                r2 = r1[r2]
                java.lang.String r2 = (java.lang.String) r2
                r3 = 1
                r1 = r1[r3]
                java.lang.Boolean r1 = (java.lang.Boolean) r1
                boolean r1 = r1.booleanValue()
                if (r1 == 0) goto L_0x004d
                boolean r0 = android.text.TextUtils.isEmpty(r5)
                if (r0 != 0) goto L_0x0054
                r0 = r5
                goto L_0x0055
            L_0x004d:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x0054
                goto L_0x0055
            L_0x0054:
                r0 = 0
            L_0x0055:
                boolean r5 = android.text.TextUtils.isEmpty(r0)
                if (r5 != 0) goto L_0x006a
                com.ironsource.sdk.controller.WebController r5 = com.ironsource.sdk.controller.WebController.this
                java.lang.String r1 = "onGetDeviceStatusSuccess"
                java.lang.String r3 = "onGetDeviceStatusFail"
                java.lang.String r5 = r5.a((java.lang.String) r0, (java.lang.String) r2, (java.lang.String) r1, (java.lang.String) r3)
                com.ironsource.sdk.controller.WebController r0 = com.ironsource.sdk.controller.WebController.this
                r0.k((java.lang.String) r5)
            L_0x006a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.WebController.NativeAPI.getDeviceStatus(java.lang.String):void");
        }

        @JavascriptInterface
        public void getDeviceVolume(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "getDeviceVolume(" + str + ")");
            try {
                float a2 = DeviceProperties.b(WebController.this.getCurrentActivityContext()).a(WebController.this.getCurrentActivityContext());
                SSAObj sSAObj = new SSAObj(str);
                sSAObj.a("deviceVolume", String.valueOf(a2));
                WebController.this.a(sSAObj.toString(), true, (String) null, (String) null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void getOrientation(String str) {
            String a2 = WebController.this.h(str);
            String jSONObject = SDKUtils.a(WebController.this.getCurrentActivityContext()).toString();
            if (!TextUtils.isEmpty(a2)) {
                WebController.this.k(WebController.this.a(a2, jSONObject, "onGetOrientationSuccess", "onGetOrientationFail"));
            }
        }

        @JavascriptInterface
        public void getUDIA(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "getUDIA(" + str + ")");
            String unused = WebController.this.h(str);
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.a("getByFlag")) {
                WebController.this.a(str, false, "getByFlag key does not exist", (String) null);
                return;
            }
            int parseInt = Integer.parseInt(sSAObj.d("getByFlag"));
            if (parseInt != 0) {
                String binaryString = Integer.toBinaryString(parseInt);
                if (TextUtils.isEmpty(binaryString)) {
                    WebController.this.a(str, false, "fialed to convert getByFlag", (String) null);
                    return;
                }
                char[] charArray = new StringBuilder(binaryString).reverse().toString().toCharArray();
                JSONArray jSONArray = new JSONArray();
                if (charArray[3] == '0') {
                    JSONObject jSONObject = new JSONObject();
                    try {
                        jSONObject.put("sessions", IronSourceSharedPrefHelper.h().f());
                        IronSourceSharedPrefHelper.h().a();
                        jSONArray.put(jSONObject);
                    } catch (JSONException unused2) {
                    }
                }
            }
        }

        @JavascriptInterface
        public void getUserData(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "getUserData(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.a("key")) {
                WebController.this.a(str, false, "key does not exist", (String) null);
                return;
            }
            String a2 = WebController.this.h(str);
            String d = sSAObj.d("key");
            WebController.this.k(WebController.this.d(a2, WebController.this.a(d, IronSourceSharedPrefHelper.h().c(d), (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
        }

        @JavascriptInterface
        public void getUserUniqueId(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "getUserUniqueId(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.a("productType")) {
                WebController.this.a(str, false, "productType does not exist", (String) null);
                return;
            }
            String a2 = WebController.this.h(str);
            if (!TextUtils.isEmpty(a2)) {
                String d = sSAObj.d("productType");
                WebController.this.k(WebController.this.a(a2, WebController.this.a("userUniqueId", IronSourceSharedPrefHelper.h().b(d), "productType", d, (String) null, (String) null, (String) null, (String) null, (String) null, false), "onGetUserUniqueIdSuccess", "onGetUserUniqueIdFail"));
            }
        }

        @JavascriptInterface
        public void iabTokenAPI(String str) {
            try {
                String G = WebController.this.f4831a;
                Logger.c(G, "iabTokenAPI(" + str + ")");
                WebController.this.M.a(new SSAObj(str).toString(), new JSCallbackTask());
            } catch (Exception e) {
                e.printStackTrace();
                String G2 = WebController.this.f4831a;
                Logger.c(G2, "iabTokenAPI failed with exception " + e.getMessage());
            }
        }

        @JavascriptInterface
        public void initController(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "initController(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            CountDownTimer countDownTimer = WebController.this.k;
            if (countDownTimer != null) {
                countDownTimer.cancel();
                WebController.this.k = null;
            }
            if (sSAObj.a("stage")) {
                String d = sSAObj.d("stage");
                if ("ready".equalsIgnoreCase(d)) {
                    boolean unused = WebController.this.g = true;
                    WebController.this.O.a();
                } else if ("loaded".equalsIgnoreCase(d)) {
                    WebController.this.O.b();
                } else if ("failed".equalsIgnoreCase(d)) {
                    WebController.this.O.a("controller failed to initialize");
                } else {
                    Logger.c(WebController.this.f4831a, "No STAGE mentioned! Should not get here!");
                }
            }
        }

        @JavascriptInterface
        public void omidAPI(final String str) {
            WebController.this.a((Runnable) new Runnable() {
                public void run() {
                    try {
                        String G = WebController.this.f4831a;
                        Logger.c(G, "omidAPI(" + str + ")");
                        WebController.this.J.a(new SSAObj(str).toString(), new JSCallbackTask(), WebController.this.getWebview());
                    } catch (Exception e) {
                        e.printStackTrace();
                        String G2 = WebController.this.f4831a;
                        Logger.c(G2, "omidAPI failed with exception " + e.getMessage());
                    }
                }
            });
        }

        @JavascriptInterface
        public void onAdWindowsClosed(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onAdWindowsClosed(" + str + ")");
            WebController.this.D.a();
            WebController.this.D.a((String) null);
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d("productType");
            final String a2 = SDKUtils.a(sSAObj);
            final SSAEnums$ProductType d2 = WebController.this.j(d);
            String t = WebController.this.b;
            Log.d(t, "onAdClosed() with type " + d2);
            if (WebController.this.l(d)) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        SSAEnums$ProductType sSAEnums$ProductType = d2;
                        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo || sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
                            DSAdProductListener a2 = WebController.this.a(d2);
                            if (a2 != null) {
                                a2.b(d2, a2);
                            }
                        } else if (sSAEnums$ProductType == SSAEnums$ProductType.OfferWall) {
                            WebController.this.y.onOWAdClosed();
                        }
                    }
                });
            }
        }

        @JavascriptInterface
        public void onGenericFunctionFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGenericFunctionFail(" + str + ")");
            if (WebController.this.w == null) {
                Logger.a(WebController.this.f4831a, "genericFunctionListener was not found");
                return;
            }
            final String d = new SSAObj(str).d("errMsg");
            WebController.this.a((Runnable) new Runnable() {
                public void run() {
                    WebController.this.w.a(d);
                }
            });
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGenericFunctionFail", str);
        }

        @JavascriptInterface
        public void onGenericFunctionSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGenericFunctionSuccess(" + str + ")");
            if (WebController.this.w == null) {
                Logger.a(WebController.this.f4831a, "genericFunctionListener was not found");
                return;
            }
            WebController.this.a((Runnable) new Runnable() {
                public void run() {
                    WebController.this.w.a();
                }
            });
            WebController.this.a(str, true, (String) null, (String) null);
        }

        @JavascriptInterface
        public void onGetApplicationInfoFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetApplicationInfoFail(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetApplicationInfoFail", str);
        }

        @JavascriptInterface
        public void onGetApplicationInfoSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetApplicationInfoSuccess(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetApplicationInfoSuccess", str);
        }

        @JavascriptInterface
        public void onGetCachedFilesMapFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetCachedFilesMapFail(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetCachedFilesMapFail", str);
        }

        @JavascriptInterface
        public void onGetCachedFilesMapSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetCachedFilesMapSuccess(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetCachedFilesMapSuccess", str);
        }

        @JavascriptInterface
        public void onGetDeviceStatusFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetDeviceStatusFail(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetDeviceStatusFail", str);
        }

        @JavascriptInterface
        public void onGetDeviceStatusSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetDeviceStatusSuccess(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetDeviceStatusSuccess", str);
        }

        @JavascriptInterface
        public void onGetUserCreditsFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetUserCreditsFail(" + str + ")");
            final String d = new SSAObj(str).d("errMsg");
            if (WebController.this.l(SSAEnums$ProductType.OfferWall.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        WebController.this.y.onGetOWCreditsFailed(str);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onGetUserCreditsFail", str);
        }

        @JavascriptInterface
        public void onGetUserUniqueIdFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetUserUniqueIdFail(" + str + ")");
        }

        @JavascriptInterface
        public void onGetUserUniqueIdSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onGetUserUniqueIdSuccess(" + str + ")");
        }

        @JavascriptInterface
        public void onInitBannerFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onInitBannerFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            if (TextUtils.isEmpty(a2)) {
                Logger.c(WebController.this.f4831a, "onInitBannerFail failed with no demand source");
                return;
            }
            DemandSource a3 = WebController.this.I.a(SSAEnums$ProductType.Banner, a2);
            if (a3 != null) {
                a3.b(3);
            }
            if (WebController.this.l(SSAEnums$ProductType.Banner.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String G = WebController.this.f4831a;
                        Log.d(G, "onBannerInitFail(message:" + str + ")");
                        WebController.this.z.a(SSAEnums$ProductType.Banner, a2, str);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onInitBannerFail", str);
        }

        @JavascriptInterface
        public void onInitBannerSuccess(String str) {
            Logger.c(WebController.this.f4831a, "onInitBannerSuccess()");
            WebController.this.g("onInitBannerSuccess", "true");
            final String a2 = SDKUtils.a(new SSAObj(str));
            if (TextUtils.isEmpty(a2)) {
                Logger.c(WebController.this.f4831a, "onInitBannerSuccess failed with no demand source");
            } else if (WebController.this.l(SSAEnums$ProductType.Banner.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        Log.d(WebController.this.f4831a, "onBannerInitSuccess()");
                        WebController.this.z.a(SSAEnums$ProductType.Banner, a2, (AdUnitsReady) null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onInitInterstitialFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onInitInterstitialFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            if (TextUtils.isEmpty(a2)) {
                Logger.c(WebController.this.f4831a, "onInitInterstitialSuccess failed with no demand source");
                return;
            }
            DemandSource a3 = WebController.this.I.a(SSAEnums$ProductType.Interstitial, a2);
            if (a3 != null) {
                a3.b(3);
            }
            if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String G = WebController.this.f4831a;
                        Log.d(G, "onInterstitialInitFail(message:" + str + ")");
                        WebController.this.x.a(SSAEnums$ProductType.Interstitial, a2, str);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onInitInterstitialFail", str);
        }

        @JavascriptInterface
        public void onInitInterstitialSuccess(String str) {
            Logger.c(WebController.this.f4831a, "onInitInterstitialSuccess()");
            WebController.this.g("onInitInterstitialSuccess", "true");
            final String a2 = SDKUtils.a(new SSAObj(str));
            if (TextUtils.isEmpty(a2)) {
                Logger.c(WebController.this.f4831a, "onInitInterstitialSuccess failed with no demand source");
            } else if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        Log.d(WebController.this.f4831a, "onInterstitialInitSuccess()");
                        WebController.this.x.a(SSAEnums$ProductType.Interstitial, a2, (AdUnitsReady) null);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onInitOfferWallFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onInitOfferWallFail(" + str + ")");
            WebController.this.D.a(false);
            final String d = new SSAObj(str).d("errMsg");
            if (WebController.this.D.h()) {
                WebController.this.D.b(false);
                if (WebController.this.l(SSAEnums$ProductType.OfferWall.toString())) {
                    WebController.this.a((Runnable) new Runnable() {
                        public void run() {
                            String str = d;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            String G = WebController.this.f4831a;
                            Log.d(G, "onOfferWallInitFail(message:" + str + ")");
                            WebController.this.y.onOfferwallInitFail(str);
                        }
                    });
                }
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onInitOfferWallFail", str);
        }

        @JavascriptInterface
        public void onInitOfferWallSuccess(String str) {
            WebController.this.g("onInitOfferWallSuccess", "true");
            WebController.this.D.a(true);
            if (WebController.this.D.h()) {
                WebController.this.D.b(false);
                if (WebController.this.l(SSAEnums$ProductType.OfferWall.toString())) {
                    WebController.this.a((Runnable) new Runnable() {
                        public void run() {
                            Log.d(WebController.this.f4831a, "onOfferWallInitSuccess()");
                            WebController.this.y.onOfferwallInitSuccess();
                        }
                    });
                }
            }
        }

        @JavascriptInterface
        public void onInitRewardedVideoFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onInitRewardedVideoFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            DemandSource a3 = WebController.this.I.a(SSAEnums$ProductType.RewardedVideo, a2);
            if (a3 != null) {
                a3.b(3);
            }
            if (WebController.this.l(SSAEnums$ProductType.RewardedVideo.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String G = WebController.this.f4831a;
                        Log.d(G, "onRVInitFail(message:" + str + ")");
                        WebController.this.v.a(SSAEnums$ProductType.RewardedVideo, a2, str);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onInitRewardedVideoFail", str);
        }

        @JavascriptInterface
        public void onInitRewardedVideoSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onInitRewardedVideoSuccess(" + str + ")");
            IronSourceSharedPrefHelper.h().a(new SSABCParameters(str));
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onInitRewardedVideoSuccess", str);
        }

        @JavascriptInterface
        public void onLoadBannerFail(String str) {
            Logger.c(WebController.this.f4831a, "onLoadBannerFail()");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            WebController.this.a(str, true, (String) null, (String) null);
            if (!TextUtils.isEmpty(a2) && WebController.this.l(SSAEnums$ProductType.Banner.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        Log.d(WebController.this.f4831a, "onLoadBannerFail()");
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        WebController.this.z.c(a2, str);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onLoadBannerSuccess(String str) {
            Logger.c(WebController.this.f4831a, "onLoadBannerSuccess()");
            final String a2 = SDKUtils.a(new SSAObj(str));
            WebController.this.a(str, true, (String) null, (String) null);
            if (WebController.this.l(SSAEnums$ProductType.Banner.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        Log.d(WebController.this.f4831a, "onBannerLoadSuccess()");
                        WebController.this.z.b(a2);
                    }
                });
            }
        }

        @JavascriptInterface
        public void onLoadInterstitialFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onLoadInterstitialFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            WebController.this.a(str, true, (String) null, (String) null);
            if (!TextUtils.isEmpty(a2)) {
                a(a2, false);
                if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString())) {
                    WebController.this.a((Runnable) new Runnable() {
                        public void run() {
                            String str = d;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            WebController.this.x.b(a2, str);
                        }
                    });
                }
                WebController.this.g("onLoadInterstitialFail", "true");
            }
        }

        @JavascriptInterface
        public void onLoadInterstitialSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onLoadInterstitialSuccess(" + str + ")");
            final String a2 = SDKUtils.a(new SSAObj(str));
            a(a2, true);
            WebController.this.a(str, true, (String) null, (String) null);
            if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        WebController.this.x.d(a2);
                    }
                });
            }
            WebController.this.g("onLoadInterstitialSuccess", "true");
        }

        @JavascriptInterface
        public void onOfferWallGeneric(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onOfferWallGeneric(" + str + ")");
            if (WebController.this.l(SSAEnums$ProductType.OfferWall.toString())) {
                WebController.this.y.onOWGeneric("", "");
            }
        }

        @JavascriptInterface
        public void onShowInterstitialFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onShowInterstitialFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            WebController.this.a(str, true, (String) null, (String) null);
            if (!TextUtils.isEmpty(a2)) {
                a(a2, false);
                if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString())) {
                    WebController.this.a((Runnable) new Runnable() {
                        public void run() {
                            String str = d;
                            if (str == null) {
                                str = "We're sorry, some error occurred. we will investigate it";
                            }
                            WebController.this.x.a(a2, str);
                        }
                    });
                }
                WebController.this.g("onShowInterstitialFail", str);
            }
        }

        @JavascriptInterface
        public void onShowInterstitialSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onShowInterstitialSuccess(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            final String a2 = SDKUtils.a(new SSAObj(str));
            if (TextUtils.isEmpty(a2)) {
                Logger.c(WebController.this.f4831a, "onShowInterstitialSuccess called with no demand");
                return;
            }
            WebController.this.D.a(SSAEnums$ProductType.Interstitial.ordinal());
            WebController.this.D.a(a2);
            if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        WebController.this.x.a(SSAEnums$ProductType.Interstitial, a2);
                        WebController.this.x.e(a2);
                    }
                });
                WebController.this.g("onShowInterstitialSuccess", str);
            }
            a(a2, false);
        }

        @JavascriptInterface
        public void onShowOfferWallFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onShowOfferWallFail(" + str + ")");
            final String d = new SSAObj(str).d("errMsg");
            if (WebController.this.l(SSAEnums$ProductType.OfferWall.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        WebController.this.y.onOWShowFail(str);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onShowOfferWallFail", str);
        }

        @JavascriptInterface
        public void onShowOfferWallSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onShowOfferWallSuccess(" + str + ")");
            WebController.this.D.a(SSAEnums$ProductType.OfferWall.ordinal());
            final String a2 = SDKUtils.a(str, "placementId");
            if (WebController.this.l(SSAEnums$ProductType.OfferWall.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        WebController.this.y.onOWShowSuccess(a2);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onShowOfferWallSuccess", str);
        }

        @JavascriptInterface
        public void onShowRewardedVideoFail(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onShowRewardedVideoFail(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            final String d = sSAObj.d("errMsg");
            final String a2 = SDKUtils.a(sSAObj);
            if (WebController.this.l(SSAEnums$ProductType.RewardedVideo.toString())) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        String str = d;
                        if (str == null) {
                            str = "We're sorry, some error occurred. we will investigate it";
                        }
                        String G = WebController.this.f4831a;
                        Log.d(G, "onRVShowFail(message:" + d + ")");
                        WebController.this.v.d(a2, str);
                    }
                });
            }
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onShowRewardedVideoFail", str);
        }

        @JavascriptInterface
        public void onShowRewardedVideoSuccess(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "onShowRewardedVideoSuccess(" + str + ")");
            WebController.this.a(str, true, (String) null, (String) null);
            WebController.this.g("onShowRewardedVideoSuccess", str);
        }

        @JavascriptInterface
        public void onVideoStatusChanged(String str) {
            String G = WebController.this.f4831a;
            Log.d(G, "onVideoStatusChanged(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d("productType");
            if (WebController.this.C != null && !TextUtils.isEmpty(d)) {
                String d2 = sSAObj.d(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
                if ("started".equalsIgnoreCase(d2)) {
                    WebController.this.C.c();
                } else if ("paused".equalsIgnoreCase(d2)) {
                    WebController.this.C.d();
                } else if ("playing".equalsIgnoreCase(d2)) {
                    WebController.this.C.g();
                } else if ("ended".equalsIgnoreCase(d2)) {
                    WebController.this.C.e();
                } else if ("stopped".equalsIgnoreCase(d2)) {
                    WebController.this.C.f();
                } else {
                    String G2 = WebController.this.f4831a;
                    Logger.c(G2, "onVideoStatusChanged: unknown status: " + d2);
                }
            }
        }

        @JavascriptInterface
        public void openUrl(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "openUrl(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d(ReportDBAdapter.ReportColumns.COLUMN_URL);
            String d2 = sSAObj.d("method");
            Context currentActivityContext = WebController.this.getCurrentActivityContext();
            try {
                if (d2.equalsIgnoreCase("external_browser")) {
                    UrlHandler.a(currentActivityContext, d);
                } else if (d2.equalsIgnoreCase("webview")) {
                    Intent intent = new Intent(currentActivityContext, OpenUrlActivity.class);
                    intent.putExtra(WebController.T, d);
                    intent.putExtra(WebController.U, true);
                    intent.putExtra("immersive", WebController.this.H);
                    currentActivityContext.startActivity(intent);
                } else if (d2.equalsIgnoreCase(TransactionErrorDetailsUtilities.STORE)) {
                    Intent intent2 = new Intent(currentActivityContext, OpenUrlActivity.class);
                    intent2.putExtra(WebController.T, d);
                    intent2.putExtra(WebController.S, true);
                    intent2.putExtra(WebController.U, true);
                    currentActivityContext.startActivity(intent2);
                }
            } catch (Exception e) {
                WebController.this.a(str, false, e.getMessage(), (String) null);
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void permissionsAPI(String str) {
            try {
                String G = WebController.this.f4831a;
                Logger.c(G, "permissionsAPI(" + str + ")");
                WebController.this.K.a(new SSAObj(str).toString(), new JSCallbackTask());
            } catch (Exception e) {
                e.printStackTrace();
                String G2 = WebController.this.f4831a;
                Logger.c(G2, "permissionsAPI failed with exception " + e.getMessage());
            }
        }

        @JavascriptInterface
        public void postAdEventNotification(String str) {
            String str2 = str;
            try {
                String G = WebController.this.f4831a;
                Logger.c(G, "postAdEventNotification(" + str2 + ")");
                SSAObj sSAObj = new SSAObj(str2);
                final String d = sSAObj.d("eventName");
                if (TextUtils.isEmpty(d)) {
                    WebController.this.a(str2, false, "eventName does not exist", (String) null);
                    return;
                }
                String d2 = sSAObj.d("dsName");
                String a2 = SDKUtils.a(sSAObj);
                String str3 = !TextUtils.isEmpty(a2) ? a2 : d2;
                JSONObject jSONObject = (JSONObject) sSAObj.b("extData");
                String d3 = sSAObj.d("productType");
                SSAEnums$ProductType d4 = WebController.this.j(d3);
                if (WebController.this.l(d3)) {
                    String a3 = WebController.this.h(str2);
                    if (!TextUtils.isEmpty(a3)) {
                        WebController.this.k(WebController.this.a(a3, WebController.this.a("productType", d3, "eventName", d, "demandSourceName", d2, "demandSourceId", str3, (String) null, false), "postAdEventNotificationSuccess", "postAdEventNotificationFail"));
                    }
                    final SSAEnums$ProductType sSAEnums$ProductType = d4;
                    final String str4 = str3;
                    final JSONObject jSONObject2 = jSONObject;
                    WebController.this.a((Runnable) new Runnable() {
                        public void run() {
                            SSAEnums$ProductType sSAEnums$ProductType = sSAEnums$ProductType;
                            if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial || sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
                                DSAdProductListener a2 = WebController.this.a(sSAEnums$ProductType);
                                if (a2 != null) {
                                    a2.a(sSAEnums$ProductType, str4, d, jSONObject2);
                                }
                            } else if (sSAEnums$ProductType == SSAEnums$ProductType.OfferWall) {
                                WebController.this.y.onOfferwallEventNotificationReceived(d, jSONObject2);
                            }
                        }
                    });
                    return;
                }
                WebController.this.a(str2, false, "productType does not exist", (String) null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @JavascriptInterface
        public void removeCloseEventHandler(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "removeCloseEventHandler(" + str + ")");
            if (WebController.this.j != null) {
                WebController.this.j.cancel();
            }
            boolean unused = WebController.this.h = true;
        }

        @JavascriptInterface
        public void removeMessagingInterface(String str) {
            WebController.this.a((Runnable) new Runnable() {
                public void run() {
                    WebController.this.removeJavascriptInterface("GenerateTokenForMessaging");
                }
            });
        }

        @JavascriptInterface
        public void saveFile(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "saveFile(" + str + ")");
            SSAFile sSAFile = new SSAFile(str);
            if (DeviceStatus.b(WebController.this.B) <= 0) {
                WebController.this.a(str, false, "no_disk_space", (String) null);
            } else if (!SDKUtils.l()) {
                WebController.this.a(str, false, "sotrage_unavailable", (String) null);
            } else if (IronSourceStorageUtils.a(WebController.this.B, sSAFile)) {
                WebController.this.a(str, false, "file_already_exist", (String) null);
            } else if (!ConnectivityService.g(WebController.this.getContext())) {
                WebController.this.a(str, false, "no_network_connection", (String) null);
            } else {
                WebController.this.a(str, true, (String) null, (String) null);
                String d = sSAFile.d();
                if (d != null) {
                    String valueOf = String.valueOf(d);
                    if (!TextUtils.isEmpty(valueOf)) {
                        String e = sSAFile.e();
                        if (e.contains("/")) {
                            String[] split = sSAFile.e().split("/");
                            e = split[split.length - 1];
                        }
                        IronSourceSharedPrefHelper.h().a(e, valueOf);
                    }
                }
                WebController.this.f.a(sSAFile);
            }
        }

        @JavascriptInterface
        public void setBackButtonState(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setBackButtonState(" + str + ")");
            IronSourceSharedPrefHelper.h().e(new SSAObj(str).d(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE));
        }

        @JavascriptInterface
        public void setForceClose(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setForceClose(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            String d = sSAObj.d("width");
            String d2 = sSAObj.d("height");
            int unused = WebController.this.l = Integer.parseInt(d);
            int unused2 = WebController.this.m = Integer.parseInt(d2);
            String unused3 = WebController.this.n = sSAObj.d(ViewProps.POSITION);
        }

        @JavascriptInterface
        public void setMixedContentAlwaysAllow(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setMixedContentAlwaysAllow(" + str + ")");
            WebController.this.a((Runnable) new Runnable() {
                public void run() {
                    if (Build.VERSION.SDK_INT >= 21) {
                        WebController.this.getSettings().setMixedContentMode(0);
                    }
                }
            });
        }

        @JavascriptInterface
        public void setOrientation(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setOrientation(" + str + ")");
            String d = new SSAObj(str).d(AdUnitActivity.EXTRA_ORIENTATION);
            WebController.this.setOrientationState(d);
            int c = DeviceStatus.c(WebController.this.getCurrentActivityContext());
            if (WebController.this.Q != null) {
                WebController.this.Q.a(d, c);
            }
        }

        @JavascriptInterface
        public void setStoreSearchKeys(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setStoreSearchKeys(" + str + ")");
            IronSourceSharedPrefHelper.h().g(str);
        }

        @JavascriptInterface
        public void setUserData(String str) {
            String str2 = str;
            String G = WebController.this.f4831a;
            Logger.c(G, "setUserData(" + str2 + ")");
            SSAObj sSAObj = new SSAObj(str2);
            if (!sSAObj.a("key")) {
                WebController.this.a(str2, false, "key does not exist", (String) null);
            } else if (!sSAObj.a("value")) {
                WebController.this.a(str2, false, "value does not exist", (String) null);
            } else {
                String d = sSAObj.d("key");
                String d2 = sSAObj.d("value");
                if (IronSourceSharedPrefHelper.h().b(d, d2)) {
                    WebController.this.k(WebController.this.d(WebController.this.h(str2), WebController.this.a(d, d2, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
                    return;
                }
                WebController.this.a(str2, false, "SetUserData failed writing to shared preferences", (String) null);
            }
        }

        @JavascriptInterface
        public void setUserUniqueId(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setUserUniqueId(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.a("userUniqueId") || !sSAObj.a("productType")) {
                WebController.this.a(str, false, "uniqueId or productType does not exist", (String) null);
                return;
            }
            if (IronSourceSharedPrefHelper.h().h(sSAObj.d("userUniqueId"))) {
                WebController.this.a(str, true, (String) null, (String) null);
            } else {
                WebController.this.a(str, false, "setUserUniqueId failed", (String) null);
            }
        }

        @JavascriptInterface
        public void setWebviewBackgroundColor(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "setWebviewBackgroundColor(" + str + ")");
            WebController.this.setWebviewBackground(str);
        }

        @JavascriptInterface
        public void toggleUDIA(String str) {
            String G = WebController.this.f4831a;
            Logger.c(G, "toggleUDIA(" + str + ")");
            SSAObj sSAObj = new SSAObj(str);
            if (!sSAObj.a("toggle")) {
                WebController.this.a(str, false, "toggle key does not exist", (String) null);
                return;
            }
            int parseInt = Integer.parseInt(sSAObj.d("toggle"));
            if (parseInt != 0) {
                String binaryString = Integer.toBinaryString(parseInt);
                if (TextUtils.isEmpty(binaryString)) {
                    WebController.this.a(str, false, "fialed to convert toggle", (String) null);
                } else if (binaryString.toCharArray()[3] == '0') {
                    IronSourceSharedPrefHelper.h().a(true);
                } else {
                    IronSourceSharedPrefHelper.h().a(false);
                }
            }
        }

        class JSCallbackTask {
            JSCallbackTask() {
            }

            /* access modifiers changed from: package-private */
            public void a(boolean z, String str, String str2) {
                SSAObj sSAObj = new SSAObj();
                sSAObj.a(z ? WebController.e0 : WebController.f0, str);
                sSAObj.a(UriUtil.DATA_SCHEME, str2);
                WebController.this.a(sSAObj.toString(), z, (String) null, (String) null);
            }

            /* access modifiers changed from: package-private */
            public void a(boolean z, String str, SSAObj sSAObj) {
                sSAObj.a(z ? WebController.e0 : WebController.f0, str);
                WebController.this.a(sSAObj.toString(), z, (String) null, (String) null);
            }

            /* access modifiers changed from: package-private */
            public void a(boolean z, String str, JSONObject jSONObject) {
                String str2;
                if (z) {
                    try {
                        str2 = WebController.e0;
                    } catch (JSONException e) {
                        e.printStackTrace();
                        e.getMessage();
                        return;
                    }
                } else {
                    str2 = WebController.f0;
                }
                jSONObject.put(str2, str);
                WebController.this.a(jSONObject.toString(), z, (String) null, (String) null);
            }
        }

        /* access modifiers changed from: package-private */
        public void b(String str) {
            WebController.this.k(WebController.this.a("unauthorizedMessage", str, (String) null, (String) null));
        }

        /* access modifiers changed from: package-private */
        public boolean a(String str) {
            if (TextUtils.isEmpty(str) || str.contains("-1")) {
                return false;
            }
            try {
                JSONObject jSONObject = new JSONObject(str);
                if (jSONObject.getString("testerABGroup").isEmpty() || jSONObject.getString("testFriendlyName").isEmpty()) {
                    return false;
                }
                return true;
            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        private void a(final String str, final int i) {
            DemandSource a2;
            if (WebController.this.l(SSAEnums$ProductType.Interstitial.toString()) && (a2 = WebController.this.I.a(SSAEnums$ProductType.Interstitial, str)) != null && a2.i()) {
                WebController.this.a((Runnable) new Runnable() {
                    public void run() {
                        WebController.this.x.onInterstitialAdRewarded(str, i);
                    }
                });
            }
        }

        private void a(String str, boolean z) {
            DemandSource a2 = WebController.this.I.a(SSAEnums$ProductType.Interstitial, str);
            if (a2 != null) {
                a2.a(z);
            }
        }
    }

    private void f(Context context) {
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        this.s = new FrameLayout(context);
        this.q = new FrameLayout(context);
        this.q.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.q.setVisibility(8);
        FrameLayout frameLayout = new FrameLayout(context);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        frameLayout.addView(this);
        this.s.addView(this.q, layoutParams);
        this.s.addView(frameLayout);
    }

    /* access modifiers changed from: private */
    public String g(String str) {
        return new SSAObj(str).d(f0);
    }

    /* access modifiers changed from: private */
    public void k(String str) {
        if (!TextUtils.isEmpty(str)) {
            String str2 = "console.log(\"JS exeption: \" + JSON.stringify(e));";
            if (getDebugMode() != SSAEnums$DebugMode.MODE_0.a() && (getDebugMode() < SSAEnums$DebugMode.MODE_1.a() || getDebugMode() > SSAEnums$DebugMode.MODE_3.a())) {
                str2 = "empty";
            }
            final StringBuilder sb = new StringBuilder();
            sb.append("try{");
            sb.append(str);
            sb.append("}catch(e){");
            sb.append(str2);
            sb.append("}");
            final String str3 = "javascript:" + sb.toString();
            a((Runnable) new Runnable() {
                public void run() {
                    Logger.c(WebController.this.f4831a, str3);
                    try {
                        if (WebController.this.A != null) {
                            if (WebController.this.A.booleanValue()) {
                                WebController.this.f(sb.toString());
                            } else {
                                WebController.this.loadUrl(str3);
                            }
                        } else if (Build.VERSION.SDK_INT >= 19) {
                            WebController.this.f(sb.toString());
                            Boolean unused = WebController.this.A = true;
                        } else {
                            WebController.this.loadUrl(str3);
                            Boolean unused2 = WebController.this.A = false;
                        }
                    } catch (NoSuchMethodError e) {
                        String G = WebController.this.f4831a;
                        Logger.b(G, "evaluateJavascrip NoSuchMethodError: SDK version=" + Build.VERSION.SDK_INT + " " + e);
                        WebController.this.loadUrl(str3);
                        Boolean unused3 = WebController.this.A = false;
                    } catch (Throwable th) {
                        String G2 = WebController.this.f4831a;
                        Logger.b(G2, "injectJavascript: " + th.toString());
                        new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=injectJavaScript"});
                    }
                }
            });
        }
    }

    public void e() {
        IronSourceStorageUtils.a(this.B, "", "mobileController.html");
        String f2 = SDKUtils.f();
        SSAFile sSAFile = new SSAFile(f2, "");
        if (!this.f.c()) {
            String str = this.f4831a;
            Logger.c(str, "Download Mobile Controller: " + f2);
            this.f.b(sSAFile);
            return;
        }
        Logger.c(this.f4831a, "Download Mobile Controller: already alive");
    }

    public void h() {
        k(i("interceptedUrlToStore"));
    }

    private ConnectivityAdapter d(Context context) {
        return new ConnectivityAdapter(SDKUtils.e(), context) {
            public void a(String str, JSONObject jSONObject) {
                if (jSONObject != null && WebController.this.g) {
                    try {
                        jSONObject.put("connectionType", str);
                        WebController.this.b(jSONObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            public void b(String str, JSONObject jSONObject) {
                if (WebController.this.g) {
                    WebController.this.e(str);
                }
            }

            public void onDisconnected() {
                if (WebController.this.g) {
                    WebController.this.e(ViewProps.NONE);
                }
            }
        };
    }

    private String i(String str) {
        return "SSA_CORE.SDKController.runFunction('" + str + "');";
    }

    /* access modifiers changed from: private */
    public void g(final String str, String str2) {
        final String d2 = new SSAObj(str2).d("errMsg");
        if (!TextUtils.isEmpty(d2)) {
            a((Runnable) new Runnable() {
                public void run() {
                    if (WebController.this.getDebugMode() == SSAEnums$DebugMode.MODE_3.a()) {
                        Context currentActivityContext = WebController.this.getCurrentActivityContext();
                        Toast.makeText(currentActivityContext, str + " : " + d2, 1).show();
                    }
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public String h(String str) {
        return new SSAObj(str).d(e0);
    }

    /* access modifiers changed from: package-private */
    public String c(Context context) {
        return IronSourceStorageUtils.c(context.getApplicationContext());
    }

    /* access modifiers changed from: package-private */
    public Handler d() {
        return new Handler(Looper.getMainLooper());
    }

    private Map<String, String> b(SSAEnums$ProductType sSAEnums$ProductType) {
        if (sSAEnums$ProductType == SSAEnums$ProductType.OfferWall) {
            return this.e;
        }
        return null;
    }

    public void c(String str, String str2) {
        k(d("onNativeLifeCycleEvent", a("lifeCycleEvent", str2, "productType", str, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    public void d(String str) {
        k(d("nativeNavigationPressed", a("action", str, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    /* access modifiers changed from: package-private */
    public SecureMessagingInterface b(SecureMessagingService secureMessagingService) {
        return new SecureMessagingInterface(secureMessagingService);
    }

    /* access modifiers changed from: private */
    public String d(String str, String str2) {
        return "SSA_CORE.SDKController.runFunction('" + str + "?parameters=" + str2 + "');";
    }

    public void b(DemandSource demandSource, Map<String, String> map, DSInterstitialListener dSInterstitialListener) {
        a(demandSource, map);
    }

    public void c() {
        k(i("enterForeground"));
    }

    public boolean g() {
        return this.p != null;
    }

    /* access modifiers changed from: private */
    public Object[] e(Context context) {
        boolean z2;
        DeviceProperties b2 = DeviceProperties.b(context);
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("appOrientation", SDKUtils.c(DeviceStatus.a(getCurrentActivityContext())));
            String d2 = b2.d();
            if (d2 != null) {
                jSONObject.put(SDKUtils.b("deviceOEM"), SDKUtils.b(d2));
            }
            String c2 = b2.c();
            if (c2 != null) {
                jSONObject.put(SDKUtils.b("deviceModel"), SDKUtils.b(c2));
                z2 = false;
            } else {
                z2 = true;
            }
            try {
                SDKUtils.b(context);
                String c3 = SDKUtils.c();
                Boolean valueOf = Boolean.valueOf(SDKUtils.m());
                if (!TextUtils.isEmpty(c3)) {
                    Logger.c(this.f4831a, "add AID and LAT");
                    jSONObject.put("isLimitAdTrackingEnabled", valueOf);
                    jSONObject.put("deviceIds" + "[" + "AID" + "]", SDKUtils.b(c3));
                }
                String e2 = b2.e();
                if (e2 != null) {
                    jSONObject.put(SDKUtils.b("deviceOs"), SDKUtils.b(e2));
                } else {
                    z2 = true;
                }
                String f2 = b2.f();
                if (f2 != null) {
                    jSONObject.put(SDKUtils.b("deviceOSVersion"), f2.replaceAll("[^0-9/.]", ""));
                } else {
                    z2 = true;
                }
                String f3 = b2.f();
                if (f3 != null) {
                    jSONObject.put(SDKUtils.b("deviceOSVersionFull"), SDKUtils.b(f3));
                }
                String valueOf2 = String.valueOf(b2.a());
                if (valueOf2 != null) {
                    jSONObject.put(SDKUtils.b("deviceApiLevel"), valueOf2);
                } else {
                    z2 = true;
                }
                String g2 = DeviceProperties.g();
                if (g2 != null) {
                    jSONObject.put(SDKUtils.b("SDKVersion"), SDKUtils.b(g2));
                }
                if (b2.b() != null && b2.b().length() > 0) {
                    jSONObject.put(SDKUtils.b("mobileCarrier"), SDKUtils.b(b2.b()));
                }
                String b3 = ConnectivityUtils.b(context);
                if (!b3.equals(ViewProps.NONE)) {
                    jSONObject.put(SDKUtils.b("connectionType"), SDKUtils.b(b3));
                } else {
                    z2 = true;
                }
                if (Build.VERSION.SDK_INT >= 23) {
                    int a2 = ConnectivityService.a(context);
                    if (a2 != 0) {
                        jSONObject.put(SDKUtils.b("cellularNetworkType"), a2);
                    }
                    jSONObject.put(SDKUtils.b("hasVPN"), ConnectivityUtils.d(context));
                }
                String language = context.getResources().getConfiguration().locale.getLanguage();
                if (!TextUtils.isEmpty(language)) {
                    jSONObject.put(SDKUtils.b("deviceLanguage"), SDKUtils.b(language.toUpperCase()));
                }
                if (SDKUtils.l()) {
                    jSONObject.put(SDKUtils.b("diskFreeSize"), SDKUtils.b(String.valueOf(DeviceStatus.b(this.B))));
                } else {
                    z2 = true;
                }
                String valueOf3 = String.valueOf(DeviceStatus.j());
                if (!TextUtils.isEmpty(valueOf3)) {
                    jSONObject.put(SDKUtils.b("deviceScreenSize") + "[" + SDKUtils.b("width") + "]", SDKUtils.b(valueOf3));
                } else {
                    z2 = true;
                }
                String valueOf4 = String.valueOf(DeviceStatus.d());
                jSONObject.put(SDKUtils.b("deviceScreenSize") + "[" + SDKUtils.b("height") + "]", SDKUtils.b(valueOf4));
                String f4 = ApplicationContext.f(getContext());
                if (!TextUtils.isEmpty(f4)) {
                    jSONObject.put(SDKUtils.b("bundleId"), SDKUtils.b(f4));
                }
                String valueOf5 = String.valueOf(DeviceStatus.c());
                if (!TextUtils.isEmpty(valueOf5)) {
                    jSONObject.put(SDKUtils.b("deviceScreenScale"), SDKUtils.b(valueOf5));
                }
                String valueOf6 = String.valueOf(DeviceStatus.m());
                if (!TextUtils.isEmpty(valueOf6)) {
                    jSONObject.put(SDKUtils.b("unLocked"), SDKUtils.b(valueOf6));
                }
                jSONObject.put(SDKUtils.b("deviceVolume"), (double) DeviceProperties.b(context).a(context));
                Context currentActivityContext = getCurrentActivityContext();
                if (Build.VERSION.SDK_INT >= 19 && (currentActivityContext instanceof Activity)) {
                    jSONObject.put(SDKUtils.b("immersiveMode"), DeviceStatus.a((Activity) currentActivityContext));
                }
                jSONObject.put(SDKUtils.b("batteryLevel"), DeviceStatus.d(currentActivityContext));
                jSONObject.put(SDKUtils.b("mcc"), ConnectivityService.c(currentActivityContext));
                jSONObject.put(SDKUtils.b("mnc"), ConnectivityService.d(currentActivityContext));
                jSONObject.put(SDKUtils.b("phoneType"), ConnectivityService.e(currentActivityContext));
                jSONObject.put(SDKUtils.b("simOperator"), SDKUtils.b(ConnectivityService.f(currentActivityContext)));
                jSONObject.put(SDKUtils.b("lastUpdateTime"), ApplicationContext.e(currentActivityContext));
                jSONObject.put(SDKUtils.b("firstInstallTime"), ApplicationContext.c(currentActivityContext));
                jSONObject.put(SDKUtils.b("appVersion"), SDKUtils.b(ApplicationContext.b(currentActivityContext)));
                String d3 = ApplicationContext.d(currentActivityContext);
                if (!TextUtils.isEmpty(d3)) {
                    jSONObject.put(SDKUtils.b("installerPackageName"), SDKUtils.b(d3));
                }
            } catch (JSONException e3) {
                e = e3;
                e.printStackTrace();
                IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                ironSourceAsyncHttpRequestTask.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e.getStackTrace()[0].getMethodName()});
                return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
            }
        } catch (JSONException e4) {
            e = e4;
            z2 = false;
            e.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask2 = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask2.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e.getStackTrace()[0].getMethodName()});
            return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
        }
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
    }

    public void b(String str, String str2) {
        if (TextUtils.isEmpty(str2)) {
            str2 = "unknown url";
        }
        String str3 = str2;
        if (TextUtils.isEmpty(str)) {
            str = "activity failed to open with unspecified reason";
        }
        k(d("failedToStartStoreActivity", a("errMsg", str, ReportDBAdapter.ReportColumns.COLUMN_URL, str3, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    private String c(JSONObject jSONObject) {
        DeviceProperties b2 = DeviceProperties.b(getContext());
        StringBuilder sb = new StringBuilder();
        String g2 = DeviceProperties.g();
        if (!TextUtils.isEmpty(g2)) {
            sb.append("SDKVersion");
            sb.append("=");
            sb.append(g2);
            sb.append("&");
        }
        String e2 = b2.e();
        if (!TextUtils.isEmpty(e2)) {
            sb.append("deviceOs");
            sb.append("=");
            sb.append(e2);
        }
        Uri parse = Uri.parse(SDKUtils.f());
        if (parse != null) {
            String str = parse.getScheme() + ":";
            String host = parse.getHost();
            int port = parse.getPort();
            if (port != -1) {
                host = host + ":" + port;
            }
            sb.append("&");
            sb.append("protocol");
            sb.append("=");
            sb.append(str);
            sb.append("&");
            sb.append("domain");
            sb.append("=");
            sb.append(host);
            if (jSONObject.keys().hasNext()) {
                try {
                    String jSONObject2 = new JSONObject(jSONObject, new String[]{"isSecured", "applicationKey"}).toString();
                    if (!TextUtils.isEmpty(jSONObject2)) {
                        sb.append("&");
                        sb.append("controllerConfig");
                        sb.append("=");
                        sb.append(jSONObject2);
                    }
                } catch (JSONException e3) {
                    e3.printStackTrace();
                }
            }
            sb.append("&");
            sb.append("debug");
            sb.append("=");
            sb.append(getDebugMode());
        }
        return sb.toString();
    }

    public void j() {
        if (Build.VERSION.SDK_INT > 10) {
            try {
                onPause();
            } catch (Throwable th) {
                String str = this.f4831a;
                Logger.c(str, "WebViewController: pause() - " + th);
                new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewPause"});
            }
        }
    }

    public void b(SSAFile sSAFile) {
        if (sSAFile.c().contains("mobileController.html")) {
            this.O.a("controller failed to download");
        } else {
            a(sSAFile.c(), sSAFile.e(), sSAFile.b());
        }
    }

    public void l() {
        if (Build.VERSION.SDK_INT > 10) {
            try {
                onResume();
            } catch (Throwable th) {
                String str = this.f4831a;
                Logger.c(str, "WebViewController: onResume() - " + th);
                new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewResume"});
            }
        }
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void f(String str) {
        evaluateJavascript(str, (ValueCallback) null);
    }

    /* access modifiers changed from: private */
    public Object[] f(String str, String str2) {
        boolean z2;
        boolean z3;
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(str)) {
                if (!str.equalsIgnoreCase("null")) {
                    if (TextUtils.isEmpty(str2) || str2.equalsIgnoreCase("null")) {
                        jSONObject.put("error", "requestId is null or empty");
                        z2 = true;
                        return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
                    }
                    List<ApplicationInfo> g2 = DeviceStatus.g(getContext());
                    JSONArray jSONArray = new JSONArray(str);
                    JSONObject jSONObject2 = new JSONObject();
                    for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                        String trim = jSONArray.getString(i2).trim();
                        if (!TextUtils.isEmpty(trim)) {
                            JSONObject jSONObject3 = new JSONObject();
                            Iterator<ApplicationInfo> it2 = g2.iterator();
                            while (true) {
                                if (it2.hasNext()) {
                                    if (trim.equalsIgnoreCase(it2.next().packageName)) {
                                        jSONObject3.put(c0, true);
                                        jSONObject2.put(trim, jSONObject3);
                                        z3 = true;
                                        break;
                                    }
                                } else {
                                    z3 = false;
                                    break;
                                }
                            }
                            if (!z3) {
                                jSONObject3.put(c0, false);
                                jSONObject2.put(trim, jSONObject3);
                            }
                        }
                    }
                    jSONObject.put(d0, jSONObject2);
                    jSONObject.put(W, str2);
                    z2 = false;
                    return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
                }
            }
            jSONObject.put("error", "appIds is null or empty");
        } catch (Exception unused) {
        }
        z2 = true;
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
    }

    /* access modifiers changed from: package-private */
    public ControllerMessageHandler a(SecureMessagingService secureMessagingService) {
        return new ControllerMessageHandler(new ControllerAdapter(new NativeAPI()), secureMessagingService);
    }

    public void b(JSONObject jSONObject) {
        String str = this.f4831a;
        Logger.c(str, "device connection info changed: " + jSONObject.toString());
        k(d("connectionInfoChanged", a("connectionInfo", jSONObject.toString(), (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    public void a(OMIDJSAdapter oMIDJSAdapter) {
        this.J = oMIDJSAdapter;
    }

    public void a(PermissionsJSAdapter permissionsJSAdapter) {
        this.K = permissionsJSAdapter;
    }

    public void a(BannerJSAdapter bannerJSAdapter) {
        this.L = bannerJSAdapter;
        this.L.a(getControllerDelegate());
    }

    public void b(String str) {
        if (str.equals("forceClose")) {
            o();
        }
        k(d("engageEnd", a("action", str, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    public void a(TokenJSAdapter tokenJSAdapter) {
        this.M = tokenJSAdapter;
    }

    /* access modifiers changed from: private */
    public DSAdProductListener a(SSAEnums$ProductType sSAEnums$ProductType) {
        if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
            return this.x;
        }
        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
            return this.v;
        }
        if (sSAEnums$ProductType == SSAEnums$ProductType.Banner) {
            return this.z;
        }
        return null;
    }

    public void b(Context context) {
        ConnectivityAdapter connectivityAdapter = this.P;
        if (connectivityAdapter != null) {
            connectivityAdapter.b(context);
        }
    }

    private String b(String str, String str2, String str3) {
        return "SSA_CORE.SDKController.runFunction('" + str + "','" + str2 + "','" + str3 + "');";
    }

    public void a(int i2) {
        try {
            loadUrl("about:blank");
        } catch (Throwable th) {
            Logger.b(this.f4831a, "WebViewController:: load: " + th.toString());
            new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewLoadBlank"});
        }
        String str = "file://" + this.B + File.separator + "mobileController.html";
        if (new File(this.B + File.separator + "mobileController.html").exists()) {
            JSONObject e2 = SDKUtils.e();
            setWebDebuggingEnabled(e2);
            String str2 = str + "?" + c(e2);
            final int i3 = i2;
            this.k = new CountDownTimer(50000, 1000) {
                public void onFinish() {
                    Logger.c(WebController.this.f4831a, "Loading Controller Timer Finish");
                    int i = i3;
                    if (i == 3) {
                        WebController.this.O.a("controller failed to load");
                    } else {
                        WebController.this.a(i + 1);
                    }
                }

                public void onTick(long j) {
                    String G = WebController.this.f4831a;
                    Logger.c(G, "Loading Controller Timer Tick " + j);
                }
            }.start();
            try {
                loadUrl(str2);
            } catch (Throwable th2) {
                Logger.b(this.f4831a, "WebViewController:: load: " + th2.toString());
                new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=webviewLoadWithPath"});
            }
            Logger.c(this.f4831a, "load(): " + str2);
            return;
        }
        Logger.c(this.f4831a, "load(): Mobile Controller HTML Does not exist");
        new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=htmlControllerDoesNotExistOnFileSystem"});
    }

    public void b() {
        a(this.D);
    }

    public void f() {
        this.o.onHideCustomView();
    }

    private void a(String str, String str2, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource, OnInitProductHandler onInitProductHandler) {
        if (TextUtils.isEmpty(str2) || TextUtils.isEmpty(str)) {
            onInitProductHandler.a("User id or Application key are missing", sSAEnums$ProductType, demandSource);
            return;
        }
        IronSourceSharedPrefHelper.h().d(str);
        k(a(sSAEnums$ProductType, demandSource).f4874a);
    }

    public boolean c(String str) {
        List<String> e2 = IronSourceSharedPrefHelper.h().e();
        if (e2 == null) {
            return false;
        }
        try {
            if (e2.isEmpty()) {
                return false;
            }
            for (String contains : e2) {
                if (str.contains(contains)) {
                    UrlHandler.a(getCurrentActivityContext(), str);
                    return true;
                }
            }
            return false;
        } catch (Exception e3) {
            e3.printStackTrace();
            return false;
        }
    }

    public void a(String str, String str2, DemandSource demandSource, DSRewardedVideoListener dSRewardedVideoListener) {
        this.c = str;
        this.d = str2;
        this.v = dSRewardedVideoListener;
        this.D.d(str);
        this.D.e(str2);
        a(str, str2, SSAEnums$ProductType.RewardedVideo, demandSource, (OnInitProductHandler) new OnInitProductHandler() {
            public void a(String str, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource) {
                WebController.this.a(str, sSAEnums$ProductType, demandSource);
            }
        });
    }

    public void a(String str, String str2, DemandSource demandSource, DSInterstitialListener dSInterstitialListener) {
        this.c = str;
        this.d = str2;
        this.x = dSInterstitialListener;
        this.D.b(this.c);
        this.D.c(this.d);
        a(this.c, this.d, SSAEnums$ProductType.Interstitial, demandSource, (OnInitProductHandler) new OnInitProductHandler() {
            public void a(String str, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource) {
                WebController.this.a(str, sSAEnums$ProductType, demandSource);
            }
        });
    }

    public void a(String str, DSInterstitialListener dSInterstitialListener) {
        HashMap hashMap = new HashMap();
        hashMap.put("demandSourceName", str);
        String a2 = SDKUtils.a((Map<String, String>) hashMap);
        this.D.a(str, true);
        k(a("loadInterstitial", a2, "onLoadInterstitialSuccess", "onLoadInterstitialFail"));
    }

    private void a(DemandSource demandSource, Map<String, String> map) {
        Map<String, String> a2 = SDKUtils.a((Map<String, String>[]) new Map[]{map, demandSource.a()});
        this.D.a(demandSource.f(), true);
        k(a("loadInterstitial", SDKUtils.a(a2), "onLoadInterstitialSuccess", "onLoadInterstitialFail"));
    }

    public void a(JSONObject jSONObject, DSInterstitialListener dSInterstitialListener) {
        k(a(SSAEnums$ProductType.Interstitial, jSONObject));
    }

    public void a(DemandSource demandSource, Map<String, String> map, DSInterstitialListener dSInterstitialListener) {
        k(a(SSAEnums$ProductType.Interstitial, new JSONObject(SDKUtils.a((Map<String, String>[]) new Map[]{map, demandSource.a()}))));
    }

    public boolean a(String str) {
        DemandSource a2 = this.I.a(SSAEnums$ProductType.Interstitial, str);
        return a2 != null && a2.b();
    }

    public void a(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.c = str;
        this.d = str2;
        this.e = map;
        this.y = onOfferWallListener;
        this.D.a(this.e);
        this.D.b(true);
        a(this.c, this.d, SSAEnums$ProductType.OfferWall, (DemandSource) null, (OnInitProductHandler) new OnInitProductHandler() {
            public void a(String str, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource) {
                WebController.this.a(str, sSAEnums$ProductType, demandSource);
            }
        });
    }

    public void a(Map<String, String> map) {
        this.e = map;
        k(b("showOfferWall", "onShowOfferWallSuccess", "onShowOfferWallFail"));
    }

    public void a(String str, String str2, OnOfferWallListener onOfferWallListener) {
        this.c = str;
        this.d = str2;
        this.y = onOfferWallListener;
        a(this.c, this.d, SSAEnums$ProductType.OfferWallCredits, (DemandSource) null, (OnInitProductHandler) new OnInitProductHandler() {
            public void a(String str, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource) {
                WebController.this.a(str, sSAEnums$ProductType, demandSource);
            }
        });
    }

    public void a(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        this.c = str;
        this.d = str2;
        this.z = dSBannerListener;
        a(str, str2, SSAEnums$ProductType.Banner, demandSource, (OnInitProductHandler) new OnInitProductHandler() {
            public void a(String str, SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource) {
                WebController.this.a(str, sSAEnums$ProductType, demandSource);
            }
        });
    }

    public void a(JSONObject jSONObject, DSBannerListener dSBannerListener) {
        if (jSONObject != null) {
            k(a("loadBanner", jSONObject.toString(), "onLoadBannerSuccess", "onLoadBannerFail"));
        }
    }

    public void a(JSONObject jSONObject) {
        k(d("updateConsentInfo", jSONObject != null ? jSONObject.toString() : null));
    }

    private Result a(SSAEnums$ProductType sSAEnums$ProductType, DemandSource demandSource) {
        Result result = new Result();
        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo || sSAEnums$ProductType == SSAEnums$ProductType.Interstitial || sSAEnums$ProductType == SSAEnums$ProductType.OfferWall || sSAEnums$ProductType == SSAEnums$ProductType.Banner) {
            HashMap hashMap = new HashMap();
            hashMap.put("applicationKey", this.c);
            hashMap.put("applicationUserId", this.d);
            if (demandSource != null) {
                if (demandSource.e() != null) {
                    hashMap.putAll(demandSource.e());
                }
                hashMap.put("demandSourceName", demandSource.d());
                hashMap.put("demandSourceId", demandSource.f());
            }
            Map<String, String> b2 = b(sSAEnums$ProductType);
            if (b2 != null) {
                hashMap.putAll(b2);
            }
            String a2 = SDKUtils.a((Map<String, String>) hashMap);
            Constants$JSMethods a3 = Constants$JSMethods.a(sSAEnums$ProductType);
            String a4 = a(a3.f4772a, a2, a3.b, a3.c);
            String str = a3.f4772a;
            result.f4874a = a4;
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.OfferWallCredits) {
            result.f4874a = a("getUserCredits", a("productType", "OfferWall", "applicationKey", this.c, "applicationUserId", this.d, (String) null, (String) null, (String) null, false), "null", "onGetUserCreditsFail");
        }
        return result;
    }

    /* access modifiers changed from: private */
    public Object[] e(String str, String str2) {
        boolean z2;
        JSONObject jSONObject = new JSONObject();
        Map<String, String> map = null;
        if (!TextUtils.isEmpty(str)) {
            SSAEnums$ProductType j2 = j(str);
            if (j2 == SSAEnums$ProductType.OfferWall) {
                map = this.e;
            } else {
                DemandSource a2 = this.I.a(j2, str2);
                if (a2 != null) {
                    Map<String, String> e2 = a2.e();
                    e2.put("demandSourceName", a2.d());
                    e2.put("demandSourceId", a2.f());
                    map = e2;
                }
            }
            try {
                jSONObject.put("productType", str);
            } catch (JSONException e3) {
                e3.printStackTrace();
            }
            try {
                Map<String, String> i2 = SDKUtils.i();
                if (i2 != null) {
                    jSONObject = SDKUtils.a(jSONObject, new JSONObject(i2));
                }
            } catch (Exception e4) {
                e4.printStackTrace();
            }
            z2 = false;
        } else {
            z2 = true;
        }
        if (!TextUtils.isEmpty(this.d)) {
            try {
                jSONObject.put(SDKUtils.b("applicationUserId"), SDKUtils.b(this.d));
            } catch (JSONException e5) {
                e5.printStackTrace();
            }
        } else {
            z2 = true;
        }
        if (!TextUtils.isEmpty(this.c)) {
            try {
                jSONObject.put(SDKUtils.b("applicationKey"), SDKUtils.b(this.c));
            } catch (JSONException e6) {
                e6.printStackTrace();
            }
        } else {
            z2 = true;
        }
        if (map != null && !map.isEmpty()) {
            for (Map.Entry next : map.entrySet()) {
                if (((String) next.getKey()).equalsIgnoreCase("sdkWebViewCache")) {
                    setWebviewCache((String) next.getValue());
                }
                try {
                    jSONObject.put(SDKUtils.b((String) next.getKey()), SDKUtils.b((String) next.getValue()));
                } catch (JSONException e7) {
                    e7.printStackTrace();
                }
            }
        }
        return new Object[]{jSONObject.toString(), Boolean.valueOf(z2)};
    }

    private String a(SSAEnums$ProductType sSAEnums$ProductType, JSONObject jSONObject) {
        HashMap hashMap = new HashMap();
        hashMap.put("sessionDepth", Integer.toString(jSONObject.optInt("sessionDepth")));
        String optString = jSONObject.optString("demandSourceName");
        String a2 = SDKUtils.a(jSONObject);
        DemandSource a3 = this.I.a(sSAEnums$ProductType, a2);
        if (a3 != null) {
            if (a3.e() != null) {
                hashMap.putAll(a3.e());
            }
            if (!TextUtils.isEmpty(optString)) {
                hashMap.put("demandSourceName", optString);
            }
            if (!TextUtils.isEmpty(a2)) {
                hashMap.put("demandSourceId", a2);
            }
        }
        Map<String, String> b2 = b(sSAEnums$ProductType);
        if (b2 != null) {
            hashMap.putAll(b2);
        }
        String a4 = SDKUtils.a((Map<String, String>) hashMap);
        Constants$JSMethods b3 = Constants$JSMethods.b(sSAEnums$ProductType);
        return a(b3.f4772a, a4, b3.b, b3.c);
    }

    /* access modifiers changed from: private */
    public void a(final String str, final SSAEnums$ProductType sSAEnums$ProductType, final DemandSource demandSource) {
        if (l(sSAEnums$ProductType.toString())) {
            a((Runnable) new Runnable() {
                public void run() {
                    SSAEnums$ProductType sSAEnums$ProductType = SSAEnums$ProductType.RewardedVideo;
                    SSAEnums$ProductType sSAEnums$ProductType2 = sSAEnums$ProductType;
                    if (sSAEnums$ProductType == sSAEnums$ProductType2 || SSAEnums$ProductType.Interstitial == sSAEnums$ProductType2 || SSAEnums$ProductType.Banner == sSAEnums$ProductType2) {
                        DemandSource demandSource = demandSource;
                        if (demandSource != null && !TextUtils.isEmpty(demandSource.f())) {
                            DSAdProductListener a2 = WebController.this.a(sSAEnums$ProductType);
                            String G = WebController.this.f4831a;
                            Log.d(G, "onAdProductInitFailed (message:" + str + ")(" + sSAEnums$ProductType + ")");
                            if (a2 != null) {
                                a2.a(sSAEnums$ProductType, demandSource.f(), str);
                            }
                        }
                    } else if (SSAEnums$ProductType.OfferWall == sSAEnums$ProductType2) {
                        WebController.this.y.onOfferwallInitFail(str);
                    } else if (SSAEnums$ProductType.OfferWallCredits == sSAEnums$ProductType2) {
                        WebController.this.y.onGetOWCreditsFailed(str);
                    }
                }
            });
        }
    }

    public void a(JSONObject jSONObject, DSRewardedVideoListener dSRewardedVideoListener) {
        k(a(SSAEnums$ProductType.RewardedVideo, jSONObject));
    }

    public void a(String str, String str2) {
        k(d("assetCached", a(UriUtil.LOCAL_FILE_SCHEME, str, "path", str2, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    public void a(String str, String str2, String str3) {
        k(d("assetCachedFailed", a(UriUtil.LOCAL_FILE_SCHEME, str, "path", str2, "errMsg", str3, (String) null, (String) null, (String) null, false)));
    }

    public void a() {
        k(i("enterBackground"));
    }

    public void a(boolean z2, String str) {
        k(d("viewableChange", a("webview", str, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, "isViewable", z2)));
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0017, code lost:
        if (android.text.TextUtils.isEmpty(r1) == false) goto L_0x0023;
     */
    /* JADX WARNING: Removed duplicated region for block: B:10:0x0029  */
    /* JADX WARNING: Removed duplicated region for block: B:22:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.String r4, boolean r5, java.lang.String r6, java.lang.String r7) {
        /*
            r3 = this;
            com.ironsource.sdk.data.SSAObj r0 = new com.ironsource.sdk.data.SSAObj
            r0.<init>(r4)
            java.lang.String r1 = e0
            java.lang.String r1 = r0.d(r1)
            java.lang.String r2 = f0
            java.lang.String r0 = r0.d(r2)
            if (r5 == 0) goto L_0x001a
            boolean r5 = android.text.TextUtils.isEmpty(r1)
            if (r5 != 0) goto L_0x0022
            goto L_0x0023
        L_0x001a:
            boolean r5 = android.text.TextUtils.isEmpty(r0)
            if (r5 != 0) goto L_0x0022
            r1 = r0
            goto L_0x0023
        L_0x0022:
            r1 = 0
        L_0x0023:
            boolean r5 = android.text.TextUtils.isEmpty(r1)
            if (r5 != 0) goto L_0x005c
            boolean r5 = android.text.TextUtils.isEmpty(r6)
            if (r5 != 0) goto L_0x0040
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x003f }
            r5.<init>(r4)     // Catch:{ JSONException -> 0x003f }
            java.lang.String r0 = "errMsg"
            org.json.JSONObject r5 = r5.put(r0, r6)     // Catch:{ JSONException -> 0x003f }
            java.lang.String r4 = r5.toString()     // Catch:{ JSONException -> 0x003f }
            goto L_0x0040
        L_0x003f:
        L_0x0040:
            boolean r5 = android.text.TextUtils.isEmpty(r7)
            if (r5 != 0) goto L_0x0055
            org.json.JSONObject r5 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0055 }
            r5.<init>(r4)     // Catch:{ JSONException -> 0x0055 }
            java.lang.String r6 = "errCode"
            org.json.JSONObject r5 = r5.put(r6, r7)     // Catch:{ JSONException -> 0x0055 }
            java.lang.String r4 = r5.toString()     // Catch:{ JSONException -> 0x0055 }
        L_0x0055:
            java.lang.String r4 = r3.d((java.lang.String) r1, (java.lang.String) r4)
            r3.k((java.lang.String) r4)
        L_0x005c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.controller.WebController.a(java.lang.String, boolean, java.lang.String, java.lang.String):void");
    }

    public void e(String str) {
        String str2 = this.f4831a;
        Logger.c(str2, "device status changed, connection type " + str);
        k(d("deviceStatusChanged", a("connectionType", str, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, false)));
    }

    /* access modifiers changed from: private */
    public String a(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, boolean z2) {
        JSONObject jSONObject = new JSONObject();
        try {
            if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2)) {
                jSONObject.put(str, SDKUtils.b(str2));
            }
            if (!TextUtils.isEmpty(str3) && !TextUtils.isEmpty(str4)) {
                jSONObject.put(str3, SDKUtils.b(str4));
            }
            if (!TextUtils.isEmpty(str5) && !TextUtils.isEmpty(str6)) {
                jSONObject.put(str5, SDKUtils.b(str6));
            }
            if (!TextUtils.isEmpty(str7) && !TextUtils.isEmpty(str8)) {
                jSONObject.put(str7, SDKUtils.b(str8));
            }
            if (!TextUtils.isEmpty(str9)) {
                jSONObject.put(str9, z2);
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e2.getStackTrace()[0].getMethodName()});
        }
        return jSONObject.toString();
    }

    public void a(SSAFile sSAFile) {
        if (sSAFile.c().contains("mobileController.html")) {
            a(1);
        } else {
            a(sSAFile.c(), sSAFile.e());
        }
    }

    public void a(Context context) {
        ConnectivityAdapter connectivityAdapter = this.P;
        if (connectivityAdapter != null) {
            connectivityAdapter.c(context);
        }
    }

    /* access modifiers changed from: private */
    public String a(String str, String str2, String str3, String str4) {
        return "SSA_CORE.SDKController.runFunction('" + str + "?parameters=" + str2 + "','" + str3 + "','" + str4 + "');";
    }

    public void a(AdUnitsState adUnitsState) {
        synchronized (this.E) {
            if (adUnitsState.i() && this.g) {
                String str = this.f4831a;
                Log.d(str, "restoreState(state:" + adUnitsState + ")");
                int c2 = adUnitsState.c();
                if (c2 != -1) {
                    if (c2 == SSAEnums$ProductType.RewardedVideo.ordinal()) {
                        Log.d(this.f4831a, "onRVAdClosed()");
                        SSAEnums$ProductType sSAEnums$ProductType = SSAEnums$ProductType.RewardedVideo;
                        String b2 = adUnitsState.b();
                        DSAdProductListener a2 = a(sSAEnums$ProductType);
                        if (a2 != null && !TextUtils.isEmpty(b2)) {
                            a2.b(sSAEnums$ProductType, b2);
                        }
                    } else if (c2 == SSAEnums$ProductType.Interstitial.ordinal()) {
                        Log.d(this.f4831a, "onInterstitialAdClosed()");
                        SSAEnums$ProductType sSAEnums$ProductType2 = SSAEnums$ProductType.Interstitial;
                        String b3 = adUnitsState.b();
                        DSAdProductListener a3 = a(sSAEnums$ProductType2);
                        if (a3 != null && !TextUtils.isEmpty(b3)) {
                            a3.b(sSAEnums$ProductType2, b3);
                        }
                    } else if (c2 == SSAEnums$ProductType.OfferWall.ordinal()) {
                        Log.d(this.f4831a, "onOWAdClosed()");
                        if (this.y != null) {
                            this.y.onOWAdClosed();
                        }
                    }
                    adUnitsState.a(-1);
                    adUnitsState.a((String) null);
                } else {
                    Log.d(this.f4831a, "No ad was opened");
                }
                String d2 = adUnitsState.d();
                String e2 = adUnitsState.e();
                for (DemandSource next : this.I.a(SSAEnums$ProductType.Interstitial)) {
                    if (next.c() == 2) {
                        String str2 = this.f4831a;
                        Log.d(str2, "initInterstitial(appKey:" + d2 + ", userId:" + e2 + ", demandSource:" + next.d() + ")");
                        a(d2, e2, next, this.x);
                    }
                }
                String f2 = adUnitsState.f();
                String g2 = adUnitsState.g();
                for (DemandSource next2 : this.I.a(SSAEnums$ProductType.RewardedVideo)) {
                    if (next2.c() == 2) {
                        String d3 = next2.d();
                        Log.d(this.f4831a, "onRVNoMoreOffers()");
                        this.v.c(d3);
                        String str3 = this.f4831a;
                        Log.d(str3, "initRewardedVideo(appKey:" + f2 + ", userId:" + g2 + ", demandSource:" + d3 + ")");
                        a(f2, g2, next2, this.v);
                    }
                }
                adUnitsState.c(false);
            }
            this.D = adUnitsState;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(Runnable runnable) {
        this.G.post(runnable);
    }
}
