package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.facebook.common.util.ByteConstants;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.data.AdUnitsState;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.handlers.BackButtonHandler;
import com.ironsource.sdk.listeners.OnWebViewChangeListener;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import com.vungle.warren.model.AdvertisementDBAdapter;

public class ControllerActivity extends Activity implements OnWebViewChangeListener, VideoEventsListener {
    private static final String m = ControllerActivity.class.getSimpleName();

    /* renamed from: a  reason: collision with root package name */
    public int f4777a = -1;
    private WebController b;
    private RelativeLayout c;
    private FrameLayout d;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public Handler f = new Handler();
    /* access modifiers changed from: private */
    public final Runnable g = new Runnable() {
        public void run() {
            ControllerActivity.this.getWindow().getDecorView().setSystemUiVisibility(SDKUtils.a(ControllerActivity.this.e));
        }
    };
    final RelativeLayout.LayoutParams h = new RelativeLayout.LayoutParams(-1, -1);
    private boolean i = false;
    private String j;
    private AdUnitsState k;
    private boolean l;

    private void h() {
        runOnUiThread(new Runnable() {
            public void run() {
                ControllerActivity.this.getWindow().clearFlags(128);
            }
        });
    }

    private void i() {
        if (this.b != null) {
            Logger.c(m, "clearWebviewController");
            this.b.setState(WebController.State.Gone);
            this.b.k();
            this.b.c(this.j, "onDestroy");
        }
    }

    private void j() {
        requestWindowFeature(1);
    }

    private void k() {
        getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
    }

    private void l() {
        Intent intent = getIntent();
        b(intent.getStringExtra("orientation_set_flag"), intent.getIntExtra("rotation_set_flag", 0));
    }

    private void m() {
        runOnUiThread(new Runnable() {
            public void run() {
                ControllerActivity.this.getWindow().addFlags(128);
            }
        });
    }

    private void n() {
        if (this.c != null) {
            ViewGroup viewGroup = (ViewGroup) this.d.getParent();
            if (viewGroup.findViewById(1) != null) {
                viewGroup.removeView(this.d);
            }
        }
    }

    private void o() {
        int c2 = DeviceStatus.c(this);
        Logger.c(m, "setInitiateLandscapeOrientation");
        if (c2 == 0) {
            Logger.c(m, "ROTATION_0");
            setRequestedOrientation(0);
        } else if (c2 == 2) {
            Logger.c(m, "ROTATION_180");
            setRequestedOrientation(8);
        } else if (c2 == 3) {
            Logger.c(m, "ROTATION_270 Right Landscape");
            setRequestedOrientation(8);
        } else if (c2 == 1) {
            Logger.c(m, "ROTATION_90 Left Landscape");
            setRequestedOrientation(0);
        } else {
            Logger.c(m, "No Rotation");
        }
    }

    private void p() {
        int c2 = DeviceStatus.c(this);
        Logger.c(m, "setInitiatePortraitOrientation");
        if (c2 == 0) {
            Logger.c(m, "ROTATION_0");
            setRequestedOrientation(1);
        } else if (c2 == 2) {
            Logger.c(m, "ROTATION_180");
            setRequestedOrientation(9);
        } else if (c2 == 1) {
            Logger.c(m, "ROTATION_270 Right Landscape");
            setRequestedOrientation(1);
        } else if (c2 == 3) {
            Logger.c(m, "ROTATION_90 Left Landscape");
            setRequestedOrientation(1);
        } else {
            Logger.c(m, "No Rotation");
        }
    }

    public void d() {
        a(false);
    }

    public void e() {
        a(false);
    }

    public void f() {
        a(false);
    }

    public void g() {
        a(true);
    }

    public void onBackPressed() {
        Logger.c(m, "onBackPressed");
        if (!BackButtonHandler.a().a(this)) {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        try {
            Logger.c(m, "onCreate");
            j();
            k();
            this.b = (WebController) IronSourceAdsPublisherAgent.b((Activity) this).a().e();
            this.b.setId(1);
            this.b.setOnWebViewControllerChangeListener(this);
            this.b.setVideoEventsListener(this);
            Intent intent = getIntent();
            this.j = intent.getStringExtra("productType");
            this.e = intent.getBooleanExtra("immersive", false);
            this.l = false;
            if (this.e) {
                getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                    public void onSystemUiVisibilityChange(int i) {
                        if ((i & 4098) == 0) {
                            ControllerActivity.this.f.removeCallbacks(ControllerActivity.this.g);
                            ControllerActivity.this.f.postDelayed(ControllerActivity.this.g, 500);
                        }
                    }
                });
                runOnUiThread(this.g);
            }
            if (!TextUtils.isEmpty(this.j) && SSAEnums$ProductType.OfferWall.toString().equalsIgnoreCase(this.j)) {
                if (bundle != null) {
                    AdUnitsState adUnitsState = (AdUnitsState) bundle.getParcelable(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE);
                    if (adUnitsState != null) {
                        this.k = adUnitsState;
                        this.b.a(adUnitsState);
                    }
                    finish();
                } else {
                    this.k = this.b.getSavedState();
                }
            }
            this.c = new RelativeLayout(this);
            setContentView(this.c, this.h);
            this.d = this.b.getLayout();
            if (this.c.findViewById(1) == null && this.d.getParent() != null) {
                this.i = true;
                finish();
            }
            l();
        } catch (Exception e2) {
            e2.printStackTrace();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        Logger.c(m, "onDestroy");
        if (this.i) {
            n();
        }
        if (!this.l) {
            Logger.c(m, "onDestroy | destroyedFromBackground");
            i();
        }
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (i2 != 4 || !this.b.g()) {
            if (this.e && (i2 == 25 || i2 == 24)) {
                this.f.removeCallbacks(this.g);
                this.f.postDelayed(this.g, 500);
            }
            return super.onKeyDown(i2, keyEvent);
        }
        this.b.f();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Logger.c(m, "onPause");
        ((AudioManager) getSystemService("audio")).abandonAudioFocus((AudioManager.OnAudioFocusChangeListener) null);
        WebController webController = this.b;
        if (webController != null) {
            webController.a((Context) this);
            this.b.j();
            this.b.a(false, "main");
        }
        n();
        if (isFinishing()) {
            this.l = true;
            Logger.c(m, "onPause | isFinishing");
            i();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Logger.c(m, "onResume");
        this.c.addView(this.d, this.h);
        WebController webController = this.b;
        if (webController != null) {
            webController.b((Context) this);
            this.b.l();
            this.b.a(true, "main");
        }
        ((AudioManager) getSystemService("audio")).requestAudioFocus((AudioManager.OnAudioFocusChangeListener) null, 3, 2);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!TextUtils.isEmpty(this.j) && SSAEnums$ProductType.OfferWall.toString().equalsIgnoreCase(this.j)) {
            this.k.c(true);
            bundle.putParcelable(AdvertisementDBAdapter.AdvertisementColumns.COLUMN_STATE, this.k);
        }
    }

    /* access modifiers changed from: protected */
    public void onUserLeaveHint() {
        super.onUserLeaveHint();
        Logger.c(m, "onUserLeaveHint");
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.e && z) {
            runOnUiThread(this.g);
        }
    }

    public void setRequestedOrientation(int i2) {
        if (this.f4777a != i2) {
            String str = m;
            Logger.c(str, "Rotation: Req = " + i2 + " Curr = " + this.f4777a);
            this.f4777a = i2;
            super.setRequestedOrientation(i2);
        }
    }

    private void b(String str, int i2) {
        if (str == null) {
            return;
        }
        if ("landscape".equalsIgnoreCase(str)) {
            o();
        } else if ("portrait".equalsIgnoreCase(str)) {
            p();
        } else if ("device".equalsIgnoreCase(str)) {
            if (DeviceStatus.l(this)) {
                setRequestedOrientation(1);
            }
        } else if (getRequestedOrientation() == -1) {
            setRequestedOrientation(4);
        }
    }

    public void a(String str, int i2) {
        b(str, i2);
    }

    public void c() {
        a(true);
    }

    public boolean a() {
        onBackPressed();
        return true;
    }

    public void a(boolean z) {
        if (z) {
            m();
        } else {
            h();
        }
    }

    public void b() {
        finish();
    }
}
