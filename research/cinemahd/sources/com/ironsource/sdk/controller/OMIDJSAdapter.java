package com.ironsource.sdk.controller;

import android.content.Context;
import android.webkit.WebView;
import com.ironsource.sdk.analytics.omid.OMIDManager;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.utils.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class OMIDJSAdapter {
    private static final String b = "OMIDJSAdapter";

    /* renamed from: a  reason: collision with root package name */
    private Context f4819a;

    private static class FunctionCall {

        /* renamed from: a  reason: collision with root package name */
        String f4820a;
        JSONObject b;
        String c;
        String d;

        private FunctionCall() {
        }
    }

    public OMIDJSAdapter(Context context) {
        this.f4819a = context;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, WebController.NativeAPI.JSCallbackTask jSCallbackTask, WebView webView) throws Exception {
        FunctionCall a2 = a(str);
        SSAObj sSAObj = new SSAObj();
        try {
            String str2 = a2.f4820a;
            char c = 65535;
            switch (str2.hashCode()) {
                case -1655974669:
                    if (str2.equals("activate")) {
                        c = 0;
                        break;
                    }
                    break;
                case -984459207:
                    if (str2.equals("getOmidData")) {
                        c = 4;
                        break;
                    }
                    break;
                case 70701699:
                    if (str2.equals("finishSession")) {
                        c = 2;
                        break;
                    }
                    break;
                case 1208109646:
                    if (str2.equals("impressionOccurred")) {
                        c = 3;
                        break;
                    }
                    break;
                case 1850541012:
                    if (str2.equals("startSession")) {
                        c = 1;
                        break;
                    }
                    break;
            }
            if (c == 0) {
                OMIDManager.a(this.f4819a);
                sSAObj = OMIDManager.c();
            } else if (c == 1) {
                OMIDManager.a(a2.b, webView);
            } else if (c == 2) {
                OMIDManager.b();
            } else if (c == 3) {
                OMIDManager.d();
            } else if (c == 4) {
                sSAObj = OMIDManager.c();
            } else {
                throw new IllegalArgumentException(String.format("%s | unsupported OMID API", new Object[]{a2.f4820a}));
            }
            jSCallbackTask.a(true, a2.c, sSAObj);
        } catch (Exception e) {
            sSAObj.a("errMsg", e.getMessage());
            String str3 = b;
            Logger.c(str3, "OMIDJSAdapter " + a2.f4820a + " Exception: " + e.getMessage());
            jSCallbackTask.a(false, a2.d, sSAObj);
        }
    }

    private FunctionCall a(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        FunctionCall functionCall = new FunctionCall();
        functionCall.f4820a = jSONObject.optString("omidFunction");
        functionCall.b = jSONObject.optJSONObject("omidParams");
        functionCall.c = jSONObject.optString("success");
        functionCall.d = jSONObject.optString("fail");
        return functionCall;
    }
}
