package com.ironsource.sdk.controller;

import java.util.ArrayList;

public class CommandExecutor {

    /* renamed from: a  reason: collision with root package name */
    private CommandExecutorState f4775a = CommandExecutorState.NOT_READY;
    private ArrayList b = new ArrayList();

    enum CommandExecutorState {
        NOT_READY,
        READY
    }

    public synchronized void a(Runnable runnable) {
        if (this.f4775a != CommandExecutorState.READY) {
            this.b.add(runnable);
        } else {
            runnable.run();
        }
    }

    public synchronized void b() {
        this.f4775a = CommandExecutorState.READY;
    }

    public synchronized void a() {
        Object[] array = this.b.toArray();
        for (int i = 0; i < array.length; i++) {
            ((Runnable) array[i]).run();
            array[i] = null;
        }
        this.b.clear();
    }
}
