package com.ironsource.sdk.controller;

import android.os.Bundle;
import com.ironsource.sdk.utils.Logger;

public class InterstitialActivity extends ControllerActivity {
    private static final String n = InterstitialActivity.class.getSimpleName();

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Logger.c(n, "onCreate");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        Logger.c(n, "onPause");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        Logger.c(n, "onResume");
    }
}
