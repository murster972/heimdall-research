package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.ContextThemeWrapper;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.facebook.common.util.ByteConstants;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.List;

public class OpenUrlActivity extends Activity {
    private static final int j = SDKUtils.a();
    private static final int k = SDKUtils.a();

    /* renamed from: a  reason: collision with root package name */
    private WebView f4821a = null;
    /* access modifiers changed from: private */
    public WebController b;
    /* access modifiers changed from: private */
    public ProgressBar c;
    boolean d;
    private RelativeLayout e;
    private String f;
    /* access modifiers changed from: private */
    public Handler g = new Handler();
    /* access modifiers changed from: private */
    public boolean h = false;
    /* access modifiers changed from: private */
    public final Runnable i = new Runnable() {
        public void run() {
            OpenUrlActivity.this.getWindow().getDecorView().setSystemUiVisibility(SDKUtils.a(OpenUrlActivity.this.h));
        }
    };

    private class Client extends WebViewClient {
        private Client() {
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            OpenUrlActivity.this.c.setVisibility(4);
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            OpenUrlActivity.this.c.setVisibility(0);
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            super.onReceivedError(webView, i, str, str2);
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            List<String> e = IronSourceSharedPrefHelper.h().e();
            if (e != null && !e.isEmpty()) {
                for (String contains : e) {
                    if (str.contains(contains)) {
                        try {
                            OpenUrlActivity.this.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(str)));
                            OpenUrlActivity.this.b.h();
                        } catch (Exception e2) {
                            StringBuilder sb = new StringBuilder();
                            if (e2 instanceof ActivityNotFoundException) {
                                sb.append("no activity to handle url");
                            } else {
                                sb.append("activity failed to open with unspecified reason");
                            }
                            if (OpenUrlActivity.this.b != null) {
                                OpenUrlActivity.this.b.b(sb.toString(), str);
                            }
                        }
                        OpenUrlActivity.this.finish();
                        return true;
                    }
                }
            }
            return super.shouldOverrideUrlLoading(webView, str);
        }
    }

    private void f() {
        ViewGroup viewGroup;
        WebController webController = this.b;
        if (webController != null) {
            webController.a(false, "secondary");
            if (this.e != null && (viewGroup = (ViewGroup) this.f4821a.getParent()) != null) {
                if (viewGroup.findViewById(j) != null) {
                    viewGroup.removeView(this.f4821a);
                }
                if (viewGroup.findViewById(k) != null) {
                    viewGroup.removeView(this.c);
                }
            }
        }
    }

    public void finish() {
        WebController webController;
        if (this.d && (webController = this.b) != null) {
            webController.b("secondaryClose");
        }
        super.finish();
    }

    public void onBackPressed() {
        if (this.f4821a.canGoBack()) {
            this.f4821a.goBack();
        } else {
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Logger.c("OpenUrlActivity", "onCreate()");
        try {
            this.b = (WebController) IronSourceAdsPublisherAgent.b((Activity) this).a().e();
            d();
            e();
            Bundle extras = getIntent().getExtras();
            this.f = extras.getString(WebController.T);
            this.d = extras.getBoolean(WebController.U);
            this.h = getIntent().getBooleanExtra("immersive", false);
            if (this.h) {
                getWindow().getDecorView().setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {
                    public void onSystemUiVisibilityChange(int i) {
                        if ((i & 4098) == 0) {
                            OpenUrlActivity.this.g.removeCallbacks(OpenUrlActivity.this.i);
                            OpenUrlActivity.this.g.postDelayed(OpenUrlActivity.this.i, 500);
                        }
                    }
                });
                runOnUiThread(this.i);
            }
            this.e = new RelativeLayout(this);
            setContentView(this.e, new ViewGroup.LayoutParams(-1, -1));
        } catch (Exception e2) {
            e2.printStackTrace();
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        c();
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        if (this.h && (i2 == 25 || i2 == 24)) {
            this.g.postDelayed(this.i, 500);
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        f();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        b();
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (this.h && z) {
            runOnUiThread(this.i);
        }
    }

    private void a() {
        if (this.c == null) {
            if (Build.VERSION.SDK_INT >= 11) {
                this.c = new ProgressBar(new ContextThemeWrapper(this, 16973939));
            } else {
                this.c = new ProgressBar(this);
            }
            this.c.setId(k);
        }
        if (findViewById(k) == null) {
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            this.c.setLayoutParams(layoutParams);
            this.c.setVisibility(4);
            this.e.addView(this.c);
        }
    }

    private void b() {
        if (this.f4821a == null) {
            this.f4821a = new WebView(getApplicationContext());
            this.f4821a.setId(j);
            this.f4821a.getSettings().setJavaScriptEnabled(true);
            this.f4821a.setWebViewClient(new Client());
            a(this.f);
        }
        if (findViewById(j) == null) {
            this.e.addView(this.f4821a, new RelativeLayout.LayoutParams(-1, -1));
        }
        a();
        WebController webController = this.b;
        if (webController != null) {
            webController.a(true, "secondary");
        }
    }

    private void c() {
        WebView webView = this.f4821a;
        if (webView != null) {
            webView.destroy();
        }
    }

    private void d() {
        requestWindowFeature(1);
    }

    private void e() {
        getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
    }

    public void a(String str) {
        this.f4821a.stopLoading();
        this.f4821a.clearHistory();
        try {
            this.f4821a.loadUrl(str);
        } catch (Throwable th) {
            Logger.b("OpenUrlActivity", "OpenUrlActivity:: loadUrl: " + th.toString());
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + th.getStackTrace()[0].getMethodName()});
        }
    }
}
