package com.ironsource.sdk.controller;

import android.os.Build;
import android.webkit.JavascriptInterface;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.utils.Logger;
import java.lang.reflect.Method;
import java.security.AccessControlException;

class ControllerAdapter {
    private static final String b = "ControllerAdapter";

    /* renamed from: a  reason: collision with root package name */
    private final WebController.NativeAPI f4782a;

    ControllerAdapter(WebController.NativeAPI nativeAPI) {
        this.f4782a = nativeAPI;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str, String str2) throws Exception {
        if (this.f4782a == null) {
            Logger.b(b, "!!! nativeAPI == null !!!");
            return;
        }
        Method declaredMethod = WebController.NativeAPI.class.getDeclaredMethod(str, new Class[]{String.class});
        if (Build.VERSION.SDK_INT >= 17) {
            if (!declaredMethod.isAnnotationPresent(JavascriptInterface.class)) {
                throw new AccessControlException("Trying to access a private function: " + str);
            }
        }
        declaredMethod.invoke(this.f4782a, new Object[]{str2});
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        WebController.NativeAPI nativeAPI = this.f4782a;
        if (nativeAPI != null) {
            nativeAPI.b(str);
        }
    }
}
