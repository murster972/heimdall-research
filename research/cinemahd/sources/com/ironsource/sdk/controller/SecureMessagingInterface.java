package com.ironsource.sdk.controller;

import android.webkit.JavascriptInterface;

class SecureMessagingInterface {

    /* renamed from: a  reason: collision with root package name */
    private SecureMessagingService f4827a;
    private boolean b = false;

    SecureMessagingInterface(SecureMessagingService secureMessagingService) {
        this.f4827a = secureMessagingService;
    }

    @JavascriptInterface
    public String getTokenForMessaging() {
        if (this.b) {
            return "";
        }
        this.b = true;
        return this.f4827a.a();
    }
}
