package com.ironsource.sdk.controller;

import com.ironsource.sdk.utils.MD5Hashing;
import java.security.MessageDigest;
import java.util.UUID;

final class SecureMessagingService {

    /* renamed from: a  reason: collision with root package name */
    private String f4828a;

    SecureMessagingService(String str) {
        this.f4828a = str;
    }

    private String a(String str) {
        try {
            return MD5Hashing.b(str);
        } catch (Exception e) {
            e.printStackTrace();
            return b(str);
        }
    }

    static String b() {
        return UUID.randomUUID().toString();
    }

    private String b(String str) {
        try {
            MessageDigest instance = MessageDigest.getInstance("MD5");
            instance.update(str.getBytes());
            return a(instance.digest());
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    private String a(byte[] bArr) throws Exception {
        StringBuilder sb = new StringBuilder();
        for (byte b : bArr) {
            String hexString = Integer.toHexString(b & 255);
            if (hexString.length() < 2) {
                hexString = "0" + hexString;
            }
            sb.append(hexString);
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public boolean a(String str, String str2, String str3) {
        try {
            return str3.equalsIgnoreCase(a(str + str2 + this.f4828a));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f4828a;
    }
}
