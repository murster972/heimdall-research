package com.ironsource.sdk.controller;

import android.content.Context;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.Logger;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenJSAdapter {
    private static final String c = "TokenJSAdapter";

    /* renamed from: a  reason: collision with root package name */
    private TokenService f4829a;
    private Context b;

    private static class FunctionCall {

        /* renamed from: a  reason: collision with root package name */
        String f4830a;
        JSONObject b;
        String c;
        String d;

        private FunctionCall() {
        }
    }

    public TokenJSAdapter(Context context, TokenService tokenService) {
        this.f4829a = tokenService;
        this.b = context;
    }

    private FunctionCall a(String str) throws JSONException {
        JSONObject jSONObject = new JSONObject(str);
        FunctionCall functionCall = new FunctionCall();
        functionCall.f4830a = jSONObject.optString("functionName");
        functionCall.b = jSONObject.optJSONObject("functionParams");
        functionCall.c = jSONObject.optString("success");
        functionCall.d = jSONObject.optString("fail");
        return functionCall;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, WebController.NativeAPI.JSCallbackTask jSCallbackTask) throws Exception {
        FunctionCall a2 = a(str);
        if ("updateToken".equals(a2.f4830a)) {
            a(a2.b, a2, jSCallbackTask);
        } else if ("getToken".equals(a2.f4830a)) {
            a(a2, jSCallbackTask);
        } else {
            String str2 = c;
            Logger.c(str2, "unhandled API request " + str);
        }
    }

    public void a(JSONObject jSONObject, FunctionCall functionCall, WebController.NativeAPI.JSCallbackTask jSCallbackTask) {
        SSAObj sSAObj = new SSAObj();
        try {
            this.f4829a.a(jSONObject);
            jSCallbackTask.a(true, functionCall.c, sSAObj);
        } catch (Exception e) {
            e.printStackTrace();
            String str = c;
            Logger.c(str, "updateToken exception " + e.getMessage());
            jSCallbackTask.a(false, functionCall.d, sSAObj);
        }
    }

    private void a(FunctionCall functionCall, WebController.NativeAPI.JSCallbackTask jSCallbackTask) {
        try {
            jSCallbackTask.a(true, functionCall.c, this.f4829a.b(this.b));
        } catch (Exception e) {
            jSCallbackTask.a(false, functionCall.d, e.getMessage());
        }
    }
}
