package com.ironsource.sdk.controller;

import android.webkit.JavascriptInterface;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import org.json.JSONException;
import org.json.JSONObject;

final class ControllerMessageHandler {
    private static final String c = "com.ironsource.sdk.controller.ControllerMessageHandler";

    /* renamed from: a  reason: collision with root package name */
    private final ControllerAdapter f4801a;
    private final SecureMessagingService b;

    ControllerMessageHandler(ControllerAdapter controllerAdapter, SecureMessagingService secureMessagingService) {
        this.f4801a = controllerAdapter;
        this.b = secureMessagingService;
    }

    private void a(Exception exc) {
        exc.printStackTrace();
        String str = c;
        Logger.c(str, "messageHandler failed with exception " + exc.getMessage());
    }

    private void b(String str, String str2, String str3) {
        this.f4801a.a(a(str, str2, str3));
    }

    @JavascriptInterface
    public void messageHandler(String str, String str2, String str3) {
        try {
            String str4 = c;
            Logger.c(str4, "messageHandler(" + str + " " + str3 + ")");
            if (this.b.a(str, str2, str3)) {
                a(str, str2);
            } else {
                b(str, str2, str3);
            }
        } catch (Exception e) {
            a(e);
        }
    }

    private void a(String str, String str2) throws Exception {
        this.f4801a.a(str, str2);
    }

    private String a(String str, String str2, String str3) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("functionName", SDKUtils.b(str));
            jSONObject.put("params", SDKUtils.b(str2));
            jSONObject.put("hash", SDKUtils.b(str3));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jSONObject.toString();
    }
}
