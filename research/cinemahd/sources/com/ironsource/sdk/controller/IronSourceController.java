package com.ironsource.sdk.controller;

import android.content.Context;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import java.util.Map;
import org.json.JSONObject;

public interface IronSourceController {
    void a();

    void a(Context context);

    void a(DemandSource demandSource, Map<String, String> map, DSInterstitialListener dSInterstitialListener);

    void a(String str, DSInterstitialListener dSInterstitialListener);

    void a(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener);

    void a(String str, String str2, DemandSource demandSource, DSInterstitialListener dSInterstitialListener);

    void a(String str, String str2, DemandSource demandSource, DSRewardedVideoListener dSRewardedVideoListener);

    void a(String str, String str2, OnOfferWallListener onOfferWallListener);

    void a(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener);

    void a(Map<String, String> map);

    void a(JSONObject jSONObject);

    void a(JSONObject jSONObject, DSBannerListener dSBannerListener);

    void a(JSONObject jSONObject, DSInterstitialListener dSInterstitialListener);

    void a(JSONObject jSONObject, DSRewardedVideoListener dSRewardedVideoListener);

    boolean a(String str);

    void b();

    void b(Context context);

    void b(DemandSource demandSource, Map<String, String> map, DSInterstitialListener dSInterstitialListener);

    void c();

    void destroy();

    void setCommunicationWithAdView(ISNAdView iSNAdView);
}
