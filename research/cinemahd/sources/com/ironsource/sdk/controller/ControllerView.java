package com.ironsource.sdk.controller;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.facebook.common.util.ByteConstants;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.handlers.BackButtonHandler;
import com.ironsource.sdk.listeners.OnWebViewChangeListener;

public class ControllerView extends FrameLayout implements OnWebViewChangeListener {

    /* renamed from: a  reason: collision with root package name */
    private Context f4802a;
    private WebController b;

    public ControllerView(Context context) {
        super(context);
        this.f4802a = context;
        setClickable(true);
    }

    private void c() {
        ((Activity) this.f4802a).runOnUiThread(new Runnable() {
            public void run() {
                ViewGroup a2 = ControllerView.this.getWindowDecorViewGroup();
                if (a2 != null) {
                    a2.addView(ControllerView.this);
                }
            }
        });
    }

    private void d() {
        ((Activity) this.f4802a).runOnUiThread(new Runnable() {
            public void run() {
                ViewGroup a2 = ControllerView.this.getWindowDecorViewGroup();
                if (a2 != null) {
                    a2.removeView(ControllerView.this);
                }
            }
        });
    }

    private int getNavigationBarPadding() {
        Activity activity = (Activity) this.f4802a;
        try {
            if (Build.VERSION.SDK_INT <= 9) {
                return 0;
            }
            Rect rect = new Rect();
            activity.getWindow().getDecorView().getDrawingRect(rect);
            Rect rect2 = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect2);
            if (DeviceStatus.e(activity) == 1) {
                if (rect.bottom - rect2.bottom > 0) {
                    return rect.bottom - rect2.bottom;
                }
                return 0;
            } else if (rect.right - rect2.right > 0) {
                return rect.right - rect2.right;
            } else {
                return 0;
            }
        } catch (Exception unused) {
            return 0;
        }
    }

    private int getStatusBarHeight() {
        int identifier;
        try {
            if (this.f4802a == null || (identifier = this.f4802a.getResources().getIdentifier("status_bar_height", "dimen", "android")) <= 0) {
                return 0;
            }
            return this.f4802a.getResources().getDimensionPixelSize(identifier);
        } catch (Exception unused) {
            return 0;
        }
    }

    private int getStatusBarPadding() {
        int statusBarHeight;
        if (!((((Activity) this.f4802a).getWindow().getAttributes().flags & ByteConstants.KB) != 0) && (statusBarHeight = getStatusBarHeight()) > 0) {
            return statusBarHeight;
        }
        return 0;
    }

    /* access modifiers changed from: private */
    public ViewGroup getWindowDecorViewGroup() {
        Activity activity = (Activity) this.f4802a;
        if (activity != null) {
            return (ViewGroup) activity.getWindow().getDecorView();
        }
        return null;
    }

    public void a(String str, int i) {
    }

    public void b() {
        d();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.b.l();
        this.b.a(true, "main");
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.b.j();
        this.b.a(false, "main");
        WebController webController = this.b;
        if (webController != null) {
            webController.setState(WebController.State.Gone);
            this.b.k();
        }
        removeAllViews();
    }

    public void a(WebController webController) {
        this.b = webController;
        this.b.setOnWebViewControllerChangeListener(this);
        this.b.requestFocus();
        this.f4802a = this.b.getCurrentActivityContext();
        a(getStatusBarPadding(), getNavigationBarPadding());
        c();
    }

    private void a(int i, int i2) {
        try {
            if (this.f4802a != null) {
                int e = DeviceStatus.e(this.f4802a);
                if (e == 1) {
                    setPadding(0, i, 0, i2);
                } else if (e == 2) {
                    setPadding(0, i, i2, 0);
                }
            }
        } catch (Exception unused) {
        }
    }

    public boolean a() {
        return BackButtonHandler.a().a((Activity) this.f4802a);
    }
}
