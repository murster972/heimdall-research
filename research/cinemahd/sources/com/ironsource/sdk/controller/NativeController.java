package com.ironsource.sdk.controller;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import java.util.Map;
import org.json.JSONObject;

public class NativeController implements IronSourceController {
    private static final Handler c = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public String f4806a = "";
    /* access modifiers changed from: private */
    public OnOfferWallListener b;

    NativeController(final ControllerEventListener controllerEventListener) {
        c.post(new Runnable(this) {
            public void run() {
                controllerEventListener.a();
            }
        });
    }

    public void a() {
    }

    public void a(Context context) {
    }

    public void a(JSONObject jSONObject) {
    }

    public boolean a(String str) {
        return false;
    }

    public void b() {
    }

    public void b(Context context) {
    }

    public void c() {
    }

    public void destroy() {
    }

    public void setCommunicationWithAdView(ISNAdView iSNAdView) {
    }

    public void a(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        if (onOfferWallListener != null) {
            this.b = onOfferWallListener;
            c.post(new Runnable() {
                public void run() {
                    NativeController.this.b.onOfferwallInitFail(NativeController.this.f4806a);
                }
            });
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.f4806a = str;
    }

    public void b(final DemandSource demandSource, Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.b(demandSource.d(), NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(Map<String, String> map) {
        if (this.b != null) {
            c.post(new Runnable() {
                public void run() {
                    NativeController.this.b.onOWShowFail(NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(String str, String str2, final OnOfferWallListener onOfferWallListener) {
        if (onOfferWallListener != null) {
            c.post(new Runnable() {
                public void run() {
                    onOfferWallListener.onGetOWCreditsFailed(NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(String str, String str2, final DemandSource demandSource, final DSRewardedVideoListener dSRewardedVideoListener) {
        if (dSRewardedVideoListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSRewardedVideoListener.a(SSAEnums$ProductType.RewardedVideo, demandSource.d(), NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(final JSONObject jSONObject, final DSRewardedVideoListener dSRewardedVideoListener) {
        if (dSRewardedVideoListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSRewardedVideoListener.d(jSONObject.optString("demandSourceName"), NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(String str, String str2, final DemandSource demandSource, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.a(SSAEnums$ProductType.Interstitial, demandSource.d(), NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(final String str, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.b(str, NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(final JSONObject jSONObject, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.a(jSONObject.optString("demandSourceName"), NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(final DemandSource demandSource, Map<String, String> map, final DSInterstitialListener dSInterstitialListener) {
        if (dSInterstitialListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSInterstitialListener.a(demandSource.d(), NativeController.this.f4806a);
                }
            });
        }
    }

    public void a(String str, String str2, DemandSource demandSource, DSBannerListener dSBannerListener) {
        if (dSBannerListener != null) {
            dSBannerListener.a(SSAEnums$ProductType.Banner, demandSource.d(), this.f4806a);
        }
    }

    public void a(final JSONObject jSONObject, final DSBannerListener dSBannerListener) {
        if (dSBannerListener != null) {
            c.post(new Runnable() {
                public void run() {
                    dSBannerListener.c(jSONObject.optString("demandSourceName"), NativeController.this.f4806a);
                }
            });
        }
    }
}
