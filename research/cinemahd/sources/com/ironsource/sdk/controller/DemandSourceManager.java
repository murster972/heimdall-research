package com.ironsource.sdk.controller;

import android.text.TextUtils;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.listeners.OnAdProductListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class DemandSourceManager {

    /* renamed from: a  reason: collision with root package name */
    private Map<String, DemandSource> f4805a = new LinkedHashMap();
    private Map<String, DemandSource> b = new LinkedHashMap();
    private Map<String, DemandSource> c = new LinkedHashMap();

    private Map<String, DemandSource> b(SSAEnums$ProductType sSAEnums$ProductType) {
        if (sSAEnums$ProductType.name().equalsIgnoreCase(SSAEnums$ProductType.RewardedVideo.name())) {
            return this.f4805a;
        }
        if (sSAEnums$ProductType.name().equalsIgnoreCase(SSAEnums$ProductType.Interstitial.name())) {
            return this.b;
        }
        if (sSAEnums$ProductType.name().equalsIgnoreCase(SSAEnums$ProductType.Banner.name())) {
            return this.c;
        }
        return null;
    }

    public Collection<DemandSource> a(SSAEnums$ProductType sSAEnums$ProductType) {
        Map<String, DemandSource> b2 = b(sSAEnums$ProductType);
        if (b2 != null) {
            return b2.values();
        }
        return new ArrayList();
    }

    public DemandSource a(SSAEnums$ProductType sSAEnums$ProductType, String str) {
        Map<String, DemandSource> b2;
        if (TextUtils.isEmpty(str) || (b2 = b(sSAEnums$ProductType)) == null) {
            return null;
        }
        return b2.get(str);
    }

    private void a(SSAEnums$ProductType sSAEnums$ProductType, String str, DemandSource demandSource) {
        Map<String, DemandSource> b2;
        if (!TextUtils.isEmpty(str) && demandSource != null && (b2 = b(sSAEnums$ProductType)) != null) {
            b2.put(str, demandSource);
        }
    }

    public DemandSource a(SSAEnums$ProductType sSAEnums$ProductType, IronSourceAdInstance ironSourceAdInstance) {
        String c2 = ironSourceAdInstance.c();
        DemandSource demandSource = new DemandSource(c2, ironSourceAdInstance.d(), ironSourceAdInstance.a(), ironSourceAdInstance.b());
        a(sSAEnums$ProductType, c2, demandSource);
        return demandSource;
    }

    public DemandSource a(SSAEnums$ProductType sSAEnums$ProductType, String str, Map<String, String> map, OnAdProductListener onAdProductListener) {
        DemandSource demandSource = new DemandSource(str, str, map, onAdProductListener);
        a(sSAEnums$ProductType, str, demandSource);
        return demandSource;
    }
}
