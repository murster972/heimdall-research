package com.ironsource.sdk;

import android.app.Activity;
import org.json.JSONObject;

public interface IronSourceNetworkAPI extends IronSourceNetworkAds {
    void a(JSONObject jSONObject);

    void onPause(Activity activity);

    void onResume(Activity activity);
}
