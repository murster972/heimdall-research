package com.ironsource.sdk.constants;

import com.ironsource.sdk.data.SSAEnums$ProductType;

public class Constants$JSMethods {

    /* renamed from: a  reason: collision with root package name */
    public String f4772a;
    public String b;
    public String c;

    public static Constants$JSMethods a(SSAEnums$ProductType sSAEnums$ProductType) {
        Constants$JSMethods constants$JSMethods = new Constants$JSMethods();
        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
            constants$JSMethods.f4772a = "initRewardedVideo";
            constants$JSMethods.b = "onInitRewardedVideoSuccess";
            constants$JSMethods.c = "onInitRewardedVideoFail";
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
            constants$JSMethods.f4772a = "initInterstitial";
            constants$JSMethods.b = "onInitInterstitialSuccess";
            constants$JSMethods.c = "onInitInterstitialFail";
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.OfferWall) {
            constants$JSMethods.f4772a = "initOfferWall";
            constants$JSMethods.b = "onInitOfferWallSuccess";
            constants$JSMethods.c = "onInitOfferWallFail";
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.Banner) {
            constants$JSMethods.f4772a = "initBanner";
            constants$JSMethods.b = "onInitBannerSuccess";
            constants$JSMethods.c = "onInitBannerFail";
        }
        return constants$JSMethods;
    }

    public static Constants$JSMethods b(SSAEnums$ProductType sSAEnums$ProductType) {
        Constants$JSMethods constants$JSMethods = new Constants$JSMethods();
        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
            constants$JSMethods.f4772a = "showRewardedVideo";
            constants$JSMethods.b = "onShowRewardedVideoSuccess";
            constants$JSMethods.c = "onShowRewardedVideoFail";
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
            constants$JSMethods.f4772a = "showInterstitial";
            constants$JSMethods.b = "onShowInterstitialSuccess";
            constants$JSMethods.c = "onShowInterstitialFail";
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.OfferWall) {
            constants$JSMethods.f4772a = "showOfferWall";
            constants$JSMethods.b = "onShowOfferWallSuccess";
            constants$JSMethods.c = "onInitOfferWallFail";
        }
        return constants$JSMethods;
    }
}
