package com.ironsource.sdk.precache;

import android.os.Handler;
import android.os.Message;
import com.ironsource.sdk.data.SSAFile;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.Callable;
import okhttp3.internal.ws.WebSocketProtocol;

public class DownloadManager {
    private static DownloadManager d;

    /* renamed from: a  reason: collision with root package name */
    private DownloadHandler f4890a = a();
    private Thread b;
    private String c;

    static class FileWorkerThread implements Callable<Result> {

        /* renamed from: a  reason: collision with root package name */
        private String f4892a;
        private String b;
        private String c;
        private long d;
        private String e;

        public FileWorkerThread(String str, String str2, String str3, long j, String str4) {
            this.f4892a = str;
            this.b = str2;
            this.c = str3;
            this.d = j;
            this.e = str4;
        }

        /* access modifiers changed from: package-private */
        public int a(byte[] bArr, String str) throws Exception {
            return IronSourceStorageUtils.a(bArr, str);
        }

        /* access modifiers changed from: package-private */
        public boolean a(String str, String str2) throws Exception {
            return IronSourceStorageUtils.f(str, str2);
        }

        /* JADX WARNING: Code restructure failed: missing block: B:6:0x0019, code lost:
            r2 = a(r9.f4892a, r1);
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.ironsource.sdk.precache.DownloadManager.Result call() {
            /*
                r9 = this;
                java.lang.String r0 = "DownloadManager"
                long r1 = r9.d
                r3 = 0
                int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
                if (r5 != 0) goto L_0x000e
                r1 = 1
                r9.d = r1
            L_0x000e:
                r1 = 0
                r2 = 0
            L_0x0010:
                long r3 = (long) r1
                long r5 = r9.d
                r7 = 1009(0x3f1, float:1.414E-42)
                int r8 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r8 >= 0) goto L_0x002b
                java.lang.String r2 = r9.f4892a
                com.ironsource.sdk.precache.DownloadManager$Result r2 = r9.a((java.lang.String) r2, (int) r1)
                int r3 = r2.f4893a
                r4 = 1008(0x3f0, float:1.413E-42)
                if (r3 == r4) goto L_0x0028
                if (r3 == r7) goto L_0x0028
                goto L_0x002b
            L_0x0028:
                int r1 = r1 + 1
                goto L_0x0010
            L_0x002b:
                if (r2 == 0) goto L_0x00ae
                byte[] r1 = r2.b
                if (r1 == 0) goto L_0x00ae
                java.lang.StringBuilder r1 = new java.lang.StringBuilder
                r1.<init>()
                java.lang.String r3 = r9.b
                r1.append(r3)
                java.lang.String r3 = java.io.File.separator
                r1.append(r3)
                java.lang.String r3 = r9.c
                r1.append(r3)
                java.lang.String r1 = r1.toString()
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                r3.<init>()
                java.lang.String r4 = r9.e
                r3.append(r4)
                java.lang.String r4 = java.io.File.separator
                r3.append(r4)
                java.lang.String r4 = "tmp_"
                r3.append(r4)
                java.lang.String r4 = r9.c
                r3.append(r4)
                java.lang.String r3 = r3.toString()
                byte[] r4 = r2.b     // Catch:{ FileNotFoundException -> 0x00aa, Exception -> 0x0095, Error -> 0x007e }
                int r4 = r9.a((byte[]) r4, (java.lang.String) r3)     // Catch:{ FileNotFoundException -> 0x00aa, Exception -> 0x0095, Error -> 0x007e }
                if (r4 != 0) goto L_0x0073
                r1 = 1006(0x3ee, float:1.41E-42)
                r2.f4893a = r1     // Catch:{ FileNotFoundException -> 0x00aa, Exception -> 0x0095, Error -> 0x007e }
                goto L_0x00ae
            L_0x0073:
                boolean r1 = r9.a((java.lang.String) r3, (java.lang.String) r1)     // Catch:{ FileNotFoundException -> 0x00aa, Exception -> 0x0095, Error -> 0x007e }
                if (r1 != 0) goto L_0x00ae
                r1 = 1020(0x3fc, float:1.43E-42)
                r2.f4893a = r1     // Catch:{ FileNotFoundException -> 0x00aa, Exception -> 0x0095, Error -> 0x007e }
                goto L_0x00ae
            L_0x007e:
                r1 = move-exception
                java.lang.String r3 = r1.getMessage()
                boolean r3 = android.text.TextUtils.isEmpty(r3)
                if (r3 != 0) goto L_0x0090
                java.lang.String r1 = r1.getMessage()
                com.ironsource.sdk.utils.Logger.c(r0, r1)
            L_0x0090:
                r0 = 1019(0x3fb, float:1.428E-42)
                r2.f4893a = r0
                goto L_0x00ae
            L_0x0095:
                r1 = move-exception
                java.lang.String r3 = r1.getMessage()
                boolean r3 = android.text.TextUtils.isEmpty(r3)
                if (r3 != 0) goto L_0x00a7
                java.lang.String r1 = r1.getMessage()
                com.ironsource.sdk.utils.Logger.c(r0, r1)
            L_0x00a7:
                r2.f4893a = r7
                goto L_0x00ae
            L_0x00aa:
                r0 = 1018(0x3fa, float:1.427E-42)
                r2.f4893a = r0
            L_0x00ae:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.precache.DownloadManager.FileWorkerThread.call():com.ironsource.sdk.precache.DownloadManager$Result");
        }

        /* access modifiers changed from: package-private */
        public byte[] a(InputStream inputStream) throws IOException {
            return DownloadManager.a(inputStream);
        }

        /* JADX WARNING: type inference failed for: r2v1, types: [java.io.InputStream] */
        /* JADX WARNING: type inference failed for: r2v2, types: [java.io.InputStream] */
        /* JADX WARNING: type inference failed for: r2v3, types: [java.net.HttpURLConnection] */
        /* JADX WARNING: type inference failed for: r2v4 */
        /* JADX WARNING: type inference failed for: r2v5 */
        /* JADX WARNING: type inference failed for: r2v6, types: [java.io.InputStream] */
        /* JADX WARNING: type inference failed for: r2v8 */
        /* access modifiers changed from: package-private */
        /* JADX WARNING: Code restructure failed: missing block: B:109:0x0129, code lost:
            if (r4 != null) goto L_0x012b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:110:0x012b, code lost:
            r4.disconnect();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:111:0x012e, code lost:
            r1.f4893a = r8;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:59:0x00d6, code lost:
            if (r4 != null) goto L_0x012b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:79:0x00f9, code lost:
            if (r4 != null) goto L_0x012b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:89:0x0109, code lost:
            if (r4 != null) goto L_0x012b;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:99:0x0119, code lost:
            if (r4 != null) goto L_0x012b;
         */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:105:0x0121 A[SYNTHETIC, Splitter:B:105:0x0121] */
        /* JADX WARNING: Removed duplicated region for block: B:17:0x004e A[Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }] */
        /* JADX WARNING: Removed duplicated region for block: B:19:0x0074 A[SYNTHETIC, Splitter:B:19:0x0074] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x007e  */
        /* JADX WARNING: Removed duplicated region for block: B:36:0x0099 A[Catch:{ all -> 0x00b3 }] */
        /* JADX WARNING: Removed duplicated region for block: B:38:0x00a2 A[SYNTHETIC, Splitter:B:38:0x00a2] */
        /* JADX WARNING: Removed duplicated region for block: B:43:0x00ac  */
        /* JADX WARNING: Removed duplicated region for block: B:52:0x00c3 A[Catch:{ all -> 0x00d9 }] */
        /* JADX WARNING: Removed duplicated region for block: B:55:0x00ce A[SYNTHETIC, Splitter:B:55:0x00ce] */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x00dc A[SYNTHETIC, Splitter:B:62:0x00dc] */
        /* JADX WARNING: Removed duplicated region for block: B:67:0x00e6  */
        /* JADX WARNING: Removed duplicated region for block: B:75:0x00f1 A[SYNTHETIC, Splitter:B:75:0x00f1] */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x0101 A[SYNTHETIC, Splitter:B:85:0x0101] */
        /* JADX WARNING: Removed duplicated region for block: B:95:0x0111 A[SYNTHETIC, Splitter:B:95:0x0111] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.ironsource.sdk.precache.DownloadManager.Result a(java.lang.String r8, int r9) {
            /*
                r7 = this;
                java.lang.String r0 = "DownloadManager"
                com.ironsource.sdk.precache.DownloadManager$Result r1 = new com.ironsource.sdk.precache.DownloadManager$Result
                r1.<init>()
                boolean r2 = android.text.TextUtils.isEmpty(r8)
                if (r2 == 0) goto L_0x0012
                r8 = 1007(0x3ef, float:1.411E-42)
                r1.f4893a = r8
                return r1
            L_0x0012:
                r2 = 0
                r3 = 0
                java.net.URL r4 = new java.net.URL     // Catch:{ MalformedURLException -> 0x011c, URISyntaxException -> 0x010c, SocketTimeoutException -> 0x00fc, FileNotFoundException -> 0x00ec, Exception -> 0x00b7, Error -> 0x008b, all -> 0x0088 }
                r4.<init>(r8)     // Catch:{ MalformedURLException -> 0x011c, URISyntaxException -> 0x010c, SocketTimeoutException -> 0x00fc, FileNotFoundException -> 0x00ec, Exception -> 0x00b7, Error -> 0x008b, all -> 0x0088 }
                r4.toURI()     // Catch:{ MalformedURLException -> 0x011c, URISyntaxException -> 0x010c, SocketTimeoutException -> 0x00fc, FileNotFoundException -> 0x00ec, Exception -> 0x00b7, Error -> 0x008b, all -> 0x0088 }
                java.net.URLConnection r4 = r4.openConnection()     // Catch:{ MalformedURLException -> 0x011c, URISyntaxException -> 0x010c, SocketTimeoutException -> 0x00fc, FileNotFoundException -> 0x00ec, Exception -> 0x00b7, Error -> 0x008b, all -> 0x0088 }
                java.net.HttpURLConnection r4 = (java.net.HttpURLConnection) r4     // Catch:{ MalformedURLException -> 0x011c, URISyntaxException -> 0x010c, SocketTimeoutException -> 0x00fc, FileNotFoundException -> 0x00ec, Exception -> 0x00b7, Error -> 0x008b, all -> 0x0088 }
                java.lang.String r5 = "GET"
                r4.setRequestMethod(r5)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r5 = 5000(0x1388, float:7.006E-42)
                r4.setConnectTimeout(r5)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r4.setReadTimeout(r5)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r4.connect()     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                int r3 = r4.getResponseCode()     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r5 = 200(0xc8, float:2.8E-43)
                if (r3 < r5) goto L_0x004a
                r6 = 400(0x190, float:5.6E-43)
                if (r3 < r6) goto L_0x003f
                goto L_0x004a
            L_0x003f:
                java.io.InputStream r2 = r4.getInputStream()     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                byte[] r6 = r7.a(r2)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r1.b = r6     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                goto L_0x004c
            L_0x004a:
                r3 = 1011(0x3f3, float:1.417E-42)
            L_0x004c:
                if (r3 == r5) goto L_0x0072
                java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r5.<init>()     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                java.lang.String r6 = " RESPONSE CODE: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r5.append(r3)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                java.lang.String r6 = " URL: "
                r5.append(r6)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r5.append(r8)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                java.lang.String r8 = " ATTEMPT: "
                r5.append(r8)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                r5.append(r9)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                java.lang.String r8 = r5.toString()     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
                com.ironsource.sdk.utils.Logger.c(r0, r8)     // Catch:{ MalformedURLException -> 0x011d, URISyntaxException -> 0x010d, SocketTimeoutException -> 0x00fd, FileNotFoundException -> 0x00ed, Exception -> 0x0086, Error -> 0x0082 }
            L_0x0072:
                if (r2 == 0) goto L_0x007c
                r2.close()     // Catch:{ IOException -> 0x0078 }
                goto L_0x007c
            L_0x0078:
                r8 = move-exception
                r8.printStackTrace()
            L_0x007c:
                if (r4 == 0) goto L_0x00af
                r4.disconnect()
                goto L_0x00af
            L_0x0082:
                r8 = move-exception
                r9 = r2
                r2 = r4
                goto L_0x008d
            L_0x0086:
                r8 = move-exception
                goto L_0x00b9
            L_0x0088:
                r8 = move-exception
                r4 = r2
                goto L_0x00da
            L_0x008b:
                r8 = move-exception
                r9 = r2
            L_0x008d:
                r3 = 1019(0x3fb, float:1.428E-42)
                java.lang.String r4 = r8.getMessage()     // Catch:{ all -> 0x00b3 }
                boolean r4 = android.text.TextUtils.isEmpty(r4)     // Catch:{ all -> 0x00b3 }
                if (r4 != 0) goto L_0x00a0
                java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x00b3 }
                com.ironsource.sdk.utils.Logger.c(r0, r8)     // Catch:{ all -> 0x00b3 }
            L_0x00a0:
                if (r9 == 0) goto L_0x00aa
                r9.close()     // Catch:{ IOException -> 0x00a6 }
                goto L_0x00aa
            L_0x00a6:
                r8 = move-exception
                r8.printStackTrace()
            L_0x00aa:
                if (r2 == 0) goto L_0x00af
                r2.disconnect()
            L_0x00af:
                r1.f4893a = r3
                goto L_0x0130
            L_0x00b3:
                r8 = move-exception
                r4 = r2
                r2 = r9
                goto L_0x00da
            L_0x00b7:
                r8 = move-exception
                r4 = r2
            L_0x00b9:
                java.lang.String r9 = r8.getMessage()     // Catch:{ all -> 0x00d9 }
                boolean r9 = android.text.TextUtils.isEmpty(r9)     // Catch:{ all -> 0x00d9 }
                if (r9 != 0) goto L_0x00ca
                java.lang.String r8 = r8.getMessage()     // Catch:{ all -> 0x00d9 }
                com.ironsource.sdk.utils.Logger.c(r0, r8)     // Catch:{ all -> 0x00d9 }
            L_0x00ca:
                r8 = 1009(0x3f1, float:1.414E-42)
                if (r2 == 0) goto L_0x00d6
                r2.close()     // Catch:{ IOException -> 0x00d2 }
                goto L_0x00d6
            L_0x00d2:
                r9 = move-exception
                r9.printStackTrace()
            L_0x00d6:
                if (r4 == 0) goto L_0x012e
                goto L_0x012b
            L_0x00d9:
                r8 = move-exception
            L_0x00da:
                if (r2 == 0) goto L_0x00e4
                r2.close()     // Catch:{ IOException -> 0x00e0 }
                goto L_0x00e4
            L_0x00e0:
                r9 = move-exception
                r9.printStackTrace()
            L_0x00e4:
                if (r4 == 0) goto L_0x00e9
                r4.disconnect()
            L_0x00e9:
                r1.f4893a = r3
                throw r8
            L_0x00ec:
                r4 = r2
            L_0x00ed:
                r8 = 1018(0x3fa, float:1.427E-42)
                if (r2 == 0) goto L_0x00f9
                r2.close()     // Catch:{ IOException -> 0x00f5 }
                goto L_0x00f9
            L_0x00f5:
                r9 = move-exception
                r9.printStackTrace()
            L_0x00f9:
                if (r4 == 0) goto L_0x012e
                goto L_0x012b
            L_0x00fc:
                r4 = r2
            L_0x00fd:
                r8 = 1008(0x3f0, float:1.413E-42)
                if (r2 == 0) goto L_0x0109
                r2.close()     // Catch:{ IOException -> 0x0105 }
                goto L_0x0109
            L_0x0105:
                r9 = move-exception
                r9.printStackTrace()
            L_0x0109:
                if (r4 == 0) goto L_0x012e
                goto L_0x012b
            L_0x010c:
                r4 = r2
            L_0x010d:
                r8 = 1010(0x3f2, float:1.415E-42)
                if (r2 == 0) goto L_0x0119
                r2.close()     // Catch:{ IOException -> 0x0115 }
                goto L_0x0119
            L_0x0115:
                r9 = move-exception
                r9.printStackTrace()
            L_0x0119:
                if (r4 == 0) goto L_0x012e
                goto L_0x012b
            L_0x011c:
                r4 = r2
            L_0x011d:
                r8 = 1004(0x3ec, float:1.407E-42)
                if (r2 == 0) goto L_0x0129
                r2.close()     // Catch:{ IOException -> 0x0125 }
                goto L_0x0129
            L_0x0125:
                r9 = move-exception
                r9.printStackTrace()
            L_0x0129:
                if (r4 == 0) goto L_0x012e
            L_0x012b:
                r4.disconnect()
            L_0x012e:
                r1.f4893a = r8
            L_0x0130:
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.precache.DownloadManager.FileWorkerThread.a(java.lang.String, int):com.ironsource.sdk.precache.DownloadManager$Result");
        }
    }

    public interface OnPreCacheCompletion {
        void a(SSAFile sSAFile);

        void b(SSAFile sSAFile);
    }

    static class Result {

        /* renamed from: a  reason: collision with root package name */
        int f4893a;
        byte[] b;

        Result() {
        }
    }

    static class SingleFileWorkerThread implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final String f4894a;
        private String b;
        private String c;
        private String d = a(this.b);
        private long e = a();
        private String f;
        Handler g;

        SingleFileWorkerThread(SSAFile sSAFile, Handler handler, String str, String str2) {
            this.b = sSAFile.c();
            this.c = sSAFile.e();
            this.f = str;
            this.g = handler;
            this.f4894a = str2;
        }

        /* access modifiers changed from: package-private */
        public String a(String str) {
            return SDKUtils.c(this.b);
        }

        /* access modifiers changed from: package-private */
        public Message b() {
            return new Message();
        }

        public void run() {
            SSAFile sSAFile = new SSAFile(this.d, this.c);
            Message b2 = b();
            b2.obj = sSAFile;
            String a2 = a(this.f, this.c);
            if (a2 == null) {
                b2.what = 1017;
                sSAFile.f("unable_to_create_folder");
                this.g.sendMessage(b2);
                return;
            }
            int i = a(this.b, a2, sSAFile.c(), this.e, this.f4894a).call().f4893a;
            if (i != 200) {
                if (!(i == 404 || i == 1018 || i == 1019)) {
                    switch (i) {
                        case 1004:
                        case WebSocketProtocol.CLOSE_NO_STATUS_CODE:
                        case 1006:
                            break;
                        default:
                            switch (i) {
                                case 1008:
                                case 1009:
                                case 1010:
                                case 1011:
                                    break;
                                default:
                                    return;
                            }
                    }
                }
                String a3 = a(i);
                b2.what = 1017;
                sSAFile.f(a3);
                this.g.sendMessage(b2);
                return;
            }
            b2.what = 1016;
            this.g.sendMessage(b2);
        }

        /* access modifiers changed from: package-private */
        public FileWorkerThread a(String str, String str2, String str3, long j, String str4) {
            return new FileWorkerThread(str, str2, str3, j, str4);
        }

        /* access modifiers changed from: package-private */
        public String a(String str, String str2) {
            return IronSourceStorageUtils.e(str, str2);
        }

        /* access modifiers changed from: package-private */
        public String a(int i) {
            String str = "not defined message for " + i;
            if (i != 404) {
                if (i == 1018) {
                    return "file not found exception";
                }
                if (i == 1019) {
                    return "out of memory exception";
                }
                switch (i) {
                    case 1004:
                        return "malformed url exception";
                    case WebSocketProtocol.CLOSE_NO_STATUS_CODE:
                        break;
                    case 1006:
                        return "http empty response";
                    default:
                        switch (i) {
                            case 1008:
                                return "socket timeout exception";
                            case 1009:
                                return "io exception";
                            case 1010:
                                return "uri syntax exception";
                            case 1011:
                                return "http error code";
                            default:
                                return str;
                        }
                }
            }
            return "http not found";
        }

        public long a() {
            return Long.parseLong(IronSourceSharedPrefHelper.h().c());
        }
    }

    private DownloadManager(String str) {
        this.c = str;
        IronSourceStorageUtils.b(this.c, "temp");
        IronSourceStorageUtils.e(this.c, "temp");
    }

    public static synchronized DownloadManager a(String str) {
        DownloadManager downloadManager;
        synchronized (DownloadManager.class) {
            if (d == null) {
                d = new DownloadManager(str);
            }
            downloadManager = d;
        }
        return downloadManager;
    }

    public void b(SSAFile sSAFile) {
        this.b = new Thread(new SingleFileWorkerThread(sSAFile, this.f4890a, this.c, b()));
        this.b.start();
    }

    public boolean c() {
        Thread thread = this.b;
        return thread != null && thread.isAlive();
    }

    public void d() {
        d = null;
        this.f4890a.a();
        this.f4890a = null;
    }

    static class DownloadHandler extends Handler {

        /* renamed from: a  reason: collision with root package name */
        OnPreCacheCompletion f4891a;

        DownloadHandler() {
        }

        /* access modifiers changed from: package-private */
        public void a(OnPreCacheCompletion onPreCacheCompletion) {
            if (onPreCacheCompletion != null) {
                this.f4891a = onPreCacheCompletion;
                return;
            }
            throw new IllegalArgumentException();
        }

        public void handleMessage(Message message) {
            OnPreCacheCompletion onPreCacheCompletion = this.f4891a;
            if (onPreCacheCompletion == null) {
                Logger.c("DownloadManager", "OnPreCacheCompletion listener is null, msg: " + message.toString());
                return;
            }
            try {
                int i = message.what;
                if (i == 1016) {
                    onPreCacheCompletion.a((SSAFile) message.obj);
                } else if (i == 1017) {
                    onPreCacheCompletion.b((SSAFile) message.obj);
                }
            } catch (Throwable th) {
                Logger.c("DownloadManager", "handleMessage | Got exception: " + th.getMessage());
                th.printStackTrace();
            }
        }

        public void a() {
            this.f4891a = null;
        }
    }

    /* access modifiers changed from: package-private */
    public DownloadHandler a() {
        return new DownloadHandler();
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.c + File.separator + "temp";
    }

    public void a(OnPreCacheCompletion onPreCacheCompletion) {
        this.f4890a.a(onPreCacheCompletion);
    }

    public void a(SSAFile sSAFile) {
        new Thread(new SingleFileWorkerThread(sSAFile, this.f4890a, this.c, b())).start();
    }

    static byte[] a(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[8192];
        while (true) {
            int read = inputStream.read(bArr, 0, bArr.length);
            if (read != -1) {
                byteArrayOutputStream.write(bArr, 0, read);
            } else {
                byteArrayOutputStream.flush();
                return byteArrayOutputStream.toByteArray();
            }
        }
    }
}
