package com.ironsource.sdk.agent;

import android.app.Activity;
import android.content.Context;
import android.content.MutableContextWrapper;
import android.text.TextUtils;
import com.ironsource.sdk.ISAdSize;
import com.ironsource.sdk.ISNAdView.ISNAdView;
import com.ironsource.sdk.IronSourceAdInstance;
import com.ironsource.sdk.IronSourceNetworkAPI;
import com.ironsource.sdk.SSAPublisher;
import com.ironsource.sdk.controller.ControllerManager;
import com.ironsource.sdk.controller.DemandSourceManager;
import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.DemandSource;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.data.SSASession;
import com.ironsource.sdk.listeners.OnBannerListener;
import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.listeners.OnOfferWallListener;
import com.ironsource.sdk.listeners.OnRewardedVideoListener;
import com.ironsource.sdk.listeners.internals.DSAdProductListener;
import com.ironsource.sdk.listeners.internals.DSBannerListener;
import com.ironsource.sdk.listeners.internals.DSInterstitialListener;
import com.ironsource.sdk.listeners.internals.DSRewardedVideoListener;
import com.ironsource.sdk.service.TokenService;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public final class IronSourceAdsPublisherAgent implements SSAPublisher, DSRewardedVideoListener, DSInterstitialListener, DSAdProductListener, DSBannerListener, IronSourceNetworkAPI {
    private static IronSourceAdsPublisherAgent h;
    private static MutableContextWrapper i;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public ControllerManager f4755a;
    /* access modifiers changed from: private */
    public String b;
    /* access modifiers changed from: private */
    public String c;
    private SSASession d;
    private long e;
    /* access modifiers changed from: private */
    public DemandSourceManager f;
    private TokenService g;

    private IronSourceAdsPublisherAgent(Activity activity, int i2) {
        c(activity);
    }

    private void f(JSONObject jSONObject) {
        if (jSONObject != null && jSONObject.has("gdprConsentStatus")) {
            try {
                JSONObject jSONObject2 = new JSONObject();
                jSONObject2.put("consent", Boolean.valueOf(jSONObject.getString("gdprConsentStatus")).booleanValue());
                this.g.a(jSONObject2);
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void e(final JSONObject jSONObject) {
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(jSONObject, (DSRewardedVideoListener) IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void onInterstitialAdRewarded(String str, int i2) {
        DemandSource d2 = d(SSAEnums$ProductType.Interstitial, str);
        OnInterstitialListener b2 = b(d2);
        if (d2 != null && b2 != null) {
            b2.onInterstitialAdRewarded(str, i2);
        }
    }

    public void onPause(Activity activity) {
        try {
            this.f4755a.c();
            this.f4755a.b(activity);
            b();
        } catch (Exception e2) {
            e2.printStackTrace();
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e2.getStackTrace()[0].getMethodName()});
        }
    }

    public void onResume(Activity activity) {
        i.setBaseContext(activity);
        this.f4755a.d();
        this.f4755a.a(activity);
        if (this.d == null) {
            a((Context) activity);
        }
    }

    private TokenService a(Activity activity) {
        TokenService d2 = TokenService.d();
        d2.c();
        d2.a(activity, this.b, this.c);
        return d2;
    }

    public static synchronized IronSourceAdsPublisherAgent b(Activity activity) throws Exception {
        IronSourceAdsPublisherAgent a2;
        synchronized (IronSourceAdsPublisherAgent.class) {
            a2 = a(activity, 0);
        }
        return a2;
    }

    private void c(Activity activity) {
        IronSourceSharedPrefHelper.a((Context) activity);
        this.g = a(activity);
        this.f = new DemandSourceManager();
        this.f4755a = new ControllerManager(activity, this.g, this.f);
        Logger.a(SDKUtils.h());
        Logger.c("IronSourceAdsPublisherAgent", "C'tor");
        i = new MutableContextWrapper(activity);
        this.e = 0;
        b((Context) activity);
    }

    public void d(JSONObject jSONObject) {
        if (jSONObject != null) {
            final String optString = jSONObject.optString("demandSourceName");
            if (!TextUtils.isEmpty(optString)) {
                this.f4755a.a((Runnable) new Runnable() {
                    public void run() {
                        IronSourceAdsPublisherAgent.this.f4755a.a(optString, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
                    }
                });
            }
        }
    }

    public void e(String str) {
        OnInterstitialListener b2;
        DemandSource d2 = d(SSAEnums$ProductType.Interstitial, str);
        if (d2 != null && (b2 = b(d2)) != null) {
            b2.onInterstitialShowSuccess();
        }
    }

    IronSourceAdsPublisherAgent(String str, String str2, Activity activity) {
        this.b = str;
        this.c = str2;
        c(activity);
    }

    private OnInterstitialListener b(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnInterstitialListener) demandSource.g();
    }

    private void b(Context context) {
        this.d = new SSASession(context, SSASession.SessionType.launched);
    }

    public static IronSourceNetworkAPI a(Activity activity, String str, String str2) {
        return a(str, str2, activity);
    }

    private void b() {
        SSASession sSASession = this.d;
        if (sSASession != null) {
            sSASession.a();
            IronSourceSharedPrefHelper.h().a(this.d);
            this.d = null;
        }
    }

    private void e(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        if (ironSourceAdInstance.f()) {
            d(ironSourceAdInstance, map);
        } else {
            f(ironSourceAdInstance, map);
        }
    }

    public void d(String str, String str2) {
        OnRewardedVideoListener c2;
        DemandSource d2 = d(SSAEnums$ProductType.RewardedVideo, str);
        if (d2 != null && (c2 = c(d2)) != null) {
            c2.onRVShowFail(str2);
        }
    }

    public static synchronized IronSourceNetworkAPI a(String str, String str2, Activity activity) {
        IronSourceAdsPublisherAgent ironSourceAdsPublisherAgent;
        synchronized (IronSourceAdsPublisherAgent.class) {
            if (h == null) {
                h = new IronSourceAdsPublisherAgent(str, str2, activity);
            } else {
                i.setBaseContext(activity);
                TokenService.d().a(str);
                TokenService.d().b(str2);
            }
            ironSourceAdsPublisherAgent = h;
        }
        return ironSourceAdsPublisherAgent;
    }

    private void f(final IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        Logger.a("IronSourceAdsPublisherAgent", "loadOnNewInstance " + ironSourceAdInstance.c());
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                DemandSource a2 = IronSourceAdsPublisherAgent.this.f.a(SSAEnums$ProductType.Interstitial, ironSourceAdInstance);
                IronSourceAdsPublisherAgent.this.f4755a.a(IronSourceAdsPublisherAgent.this.b, IronSourceAdsPublisherAgent.this.c, a2, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
                ironSourceAdInstance.a(true);
                IronSourceAdsPublisherAgent.this.f4755a.a(a2, (Map<String, String>) map, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void d(String str) {
        OnInterstitialListener b2;
        DemandSource d2 = d(SSAEnums$ProductType.Interstitial, str);
        if (d2 != null && (b2 = b(d2)) != null) {
            b2.onInterstitialLoadSuccess();
        }
    }

    public void b(final JSONObject jSONObject) {
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(jSONObject, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void b(SSAEnums$ProductType sSAEnums$ProductType, String str) {
        OnInterstitialListener b2;
        DemandSource d2 = d(sSAEnums$ProductType, str);
        if (d2 == null) {
            return;
        }
        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
            OnRewardedVideoListener c2 = c(d2);
            if (c2 != null) {
                c2.onRVAdClosed();
            }
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial && (b2 = b(d2)) != null) {
            b2.onInterstitialClose();
        }
    }

    private OnRewardedVideoListener c(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnRewardedVideoListener) demandSource.g();
    }

    private DemandSource d(SSAEnums$ProductType sSAEnums$ProductType, String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        return this.f.a(sSAEnums$ProductType, str);
    }

    public static synchronized IronSourceAdsPublisherAgent a(Activity activity, int i2) throws Exception {
        IronSourceAdsPublisherAgent ironSourceAdsPublisherAgent;
        synchronized (IronSourceAdsPublisherAgent.class) {
            Logger.c("IronSourceAdsPublisherAgent", "getInstance()");
            if (h == null) {
                h = new IronSourceAdsPublisherAgent(activity, i2);
            } else {
                i.setBaseContext(activity);
            }
            ironSourceAdsPublisherAgent = h;
        }
        return ironSourceAdsPublisherAgent;
    }

    public void c(final JSONObject jSONObject) {
        if (jSONObject != null) {
            this.f4755a.a((Runnable) new Runnable() {
                public void run() {
                    IronSourceAdsPublisherAgent.this.f4755a.a(jSONObject, (DSBannerListener) IronSourceAdsPublisherAgent.this);
                }
            });
        }
    }

    private void d(final IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        Logger.a("IronSourceAdsPublisherAgent", "loadOnInitializedInstance " + ironSourceAdInstance.c());
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                DemandSource a2 = IronSourceAdsPublisherAgent.this.f.a(SSAEnums$ProductType.Interstitial, ironSourceAdInstance.c());
                if (a2 != null) {
                    IronSourceAdsPublisherAgent.this.f4755a.a(a2, (Map<String, String>) map, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
                }
            }
        });
    }

    public void c(String str) {
        OnRewardedVideoListener c2;
        DemandSource d2 = d(SSAEnums$ProductType.RewardedVideo, str);
        if (d2 != null && (c2 = c(d2)) != null) {
            c2.onRVNoMoreOffers();
        }
    }

    public void c(SSAEnums$ProductType sSAEnums$ProductType, String str) {
        OnBannerListener a2;
        DemandSource d2 = d(sSAEnums$ProductType, str);
        if (d2 == null) {
            return;
        }
        if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
            OnRewardedVideoListener c2 = c(d2);
            if (c2 != null) {
                c2.onRVAdClicked();
            }
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
            OnInterstitialListener b2 = b(d2);
            if (b2 != null) {
                b2.onInterstitialClick();
            }
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.Banner && (a2 = a(d2)) != null) {
            a2.onBannerClick();
        }
    }

    public ControllerManager a() {
        return this.f4755a;
    }

    public void b(String str, String str2) {
        OnInterstitialListener b2;
        DemandSource d2 = d(SSAEnums$ProductType.Interstitial, str);
        if (d2 != null && (b2 = b(d2)) != null) {
            b2.onInterstitialLoadFailed(str2);
        }
    }

    private OnBannerListener a(DemandSource demandSource) {
        if (demandSource == null) {
            return null;
        }
        return (OnBannerListener) demandSource.g();
    }

    public void a(Context context) {
        this.d = new SSASession(context, SSASession.SessionType.backFromBG);
    }

    public void a(final String str, final String str2, String str3, Map<String, String> map, OnRewardedVideoListener onRewardedVideoListener) {
        this.b = str;
        this.c = str2;
        final DemandSource a2 = this.f.a(SSAEnums$ProductType.RewardedVideo, str3, map, onRewardedVideoListener);
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(str, str2, a2, (DSRewardedVideoListener) IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void b(String str) {
        OnBannerListener a2;
        DemandSource d2 = d(SSAEnums$ProductType.Banner, str);
        if (d2 != null && (a2 = a(d2)) != null) {
            a2.onBannerLoadSuccess();
        }
    }

    public void b(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        Logger.a("IronSourceAdsPublisherAgent", "loadAd " + ironSourceAdInstance.c());
        if (ironSourceAdInstance.e()) {
            c(ironSourceAdInstance, map);
        } else {
            e(ironSourceAdInstance, map);
        }
    }

    public void a(String str, String str2, Map<String, String> map, OnOfferWallListener onOfferWallListener) {
        this.b = str;
        this.c = str2;
        final String str3 = str;
        final String str4 = str2;
        final Map<String, String> map2 = map;
        final OnOfferWallListener onOfferWallListener2 = onOfferWallListener;
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(str3, str4, (Map<String, String>) map2, onOfferWallListener2);
            }
        });
    }

    public void c(String str, String str2) {
        OnBannerListener a2;
        DemandSource d2 = d(SSAEnums$ProductType.Banner, str);
        if (d2 != null && (a2 = a(d2)) != null) {
            a2.onBannerLoadFail(str2);
        }
    }

    private Map<String, String> b(Map<String, String> map) {
        map.put("adm", SDKUtils.a(map.get("adm")));
        return map;
    }

    public void a(final Map<String, String> map) {
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a((Map<String, String>) map);
            }
        });
    }

    private void c(IronSourceAdInstance ironSourceAdInstance, Map<String, String> map) {
        try {
            b(map);
        } catch (Exception e2) {
            e2.printStackTrace();
            Logger.a("IronSourceAdsPublisherAgent", "loadInAppBiddingAd failed decoding ADM " + e2.getMessage());
        }
        e(ironSourceAdInstance, map);
    }

    public void a(final String str, final String str2, final OnOfferWallListener onOfferWallListener) {
        this.b = str;
        this.c = str2;
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(str, str2, onOfferWallListener);
            }
        });
    }

    public void a(final String str, final String str2, String str3, Map<String, String> map, OnInterstitialListener onInterstitialListener) {
        this.b = str;
        this.c = str2;
        final DemandSource a2 = this.f.a(SSAEnums$ProductType.Interstitial, str3, map, onInterstitialListener);
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(str, str2, a2, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public boolean a(String str) {
        return this.f4755a.b(str);
    }

    public void a(final String str, final String str2, String str3, Map<String, String> map, OnBannerListener onBannerListener) {
        this.b = str;
        this.c = str2;
        final DemandSource a2 = this.f.a(SSAEnums$ProductType.Banner, str3, map, onBannerListener);
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(str, str2, a2, (DSBannerListener) IronSourceAdsPublisherAgent.this);
            }
        });
    }

    public void a(SSAEnums$ProductType sSAEnums$ProductType, String str, AdUnitsReady adUnitsReady) {
        OnBannerListener a2;
        DemandSource d2 = d(sSAEnums$ProductType, str);
        if (d2 != null) {
            d2.b(2);
            if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
                OnRewardedVideoListener c2 = c(d2);
                if (c2 != null) {
                    c2.onRVInitSuccess(adUnitsReady);
                }
            } else if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
                OnInterstitialListener b2 = b(d2);
                if (b2 != null) {
                    b2.onInterstitialInitSuccess();
                }
            } else if (sSAEnums$ProductType == SSAEnums$ProductType.Banner && (a2 = a(d2)) != null) {
                a2.onBannerInitSuccess();
            }
        }
    }

    public void a(SSAEnums$ProductType sSAEnums$ProductType, String str, String str2) {
        OnBannerListener a2;
        DemandSource d2 = d(sSAEnums$ProductType, str);
        if (d2 != null) {
            d2.b(3);
            if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo) {
                OnRewardedVideoListener c2 = c(d2);
                if (c2 != null) {
                    c2.onRVInitFail(str2);
                }
            } else if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
                OnInterstitialListener b2 = b(d2);
                if (b2 != null) {
                    b2.onInterstitialInitFailed(str2);
                }
            } else if (sSAEnums$ProductType == SSAEnums$ProductType.Banner && (a2 = a(d2)) != null) {
                a2.onBannerInitFailed(str2);
            }
        }
    }

    public void a(String str, int i2) {
        OnRewardedVideoListener c2;
        DemandSource d2 = d(SSAEnums$ProductType.RewardedVideo, str);
        if (d2 != null && (c2 = c(d2)) != null) {
            c2.onRVAdCredited(i2);
        }
    }

    public void a(SSAEnums$ProductType sSAEnums$ProductType, String str, String str2, JSONObject jSONObject) {
        OnRewardedVideoListener c2;
        DemandSource d2 = d(sSAEnums$ProductType, str);
        if (d2 != null) {
            try {
                if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
                    OnInterstitialListener b2 = b(d2);
                    if (b2 != null) {
                        jSONObject.put("demandSourceName", str);
                        b2.onInterstitialEventNotificationReceived(str2, jSONObject);
                    }
                } else if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo && (c2 = c(d2)) != null) {
                    jSONObject.put("demandSourceName", str);
                    c2.onRVEventNotificationReceived(str2, jSONObject);
                }
            } catch (JSONException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void a(SSAEnums$ProductType sSAEnums$ProductType, String str) {
        OnRewardedVideoListener c2;
        DemandSource d2 = d(sSAEnums$ProductType, str);
        if (d2 == null) {
            return;
        }
        if (sSAEnums$ProductType == SSAEnums$ProductType.Interstitial) {
            OnInterstitialListener b2 = b(d2);
            if (b2 != null) {
                b2.onInterstitialOpen();
            }
        } else if (sSAEnums$ProductType == SSAEnums$ProductType.RewardedVideo && (c2 = c(d2)) != null) {
            c2.onRVAdOpened();
        }
    }

    public void a(String str, String str2) {
        OnInterstitialListener b2;
        DemandSource d2 = d(SSAEnums$ProductType.Interstitial, str);
        if (d2 != null && (b2 = b(d2)) != null) {
            b2.onInterstitialShowFailed(str2);
        }
    }

    public void a(String str, String str2, int i2) {
        SSAEnums$ProductType e2;
        DemandSource a2;
        if (!TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && (e2 = SDKUtils.e(str)) != null && (a2 = this.f.a(e2, str2)) != null) {
            a2.c(i2);
        }
    }

    public void a(final JSONObject jSONObject) {
        f(jSONObject);
        this.f4755a.a((Runnable) new Runnable() {
            public void run() {
                IronSourceAdsPublisherAgent.this.f4755a.a(jSONObject);
            }
        });
    }

    public ISNAdView a(Activity activity, ISAdSize iSAdSize) {
        this.e++;
        ISNAdView iSNAdView = new ISNAdView(activity, "SupersonicAds_" + this.e, iSAdSize);
        this.f4755a.a(iSNAdView);
        return iSNAdView;
    }

    public void a(IronSourceAdInstance ironSourceAdInstance, final Map<String, String> map) {
        Logger.c("IronSourceAdsPublisherAgent", "showAd " + ironSourceAdInstance.c());
        final DemandSource a2 = this.f.a(SSAEnums$ProductType.Interstitial, ironSourceAdInstance.c());
        if (a2 != null) {
            this.f4755a.a((Runnable) new Runnable() {
                public void run() {
                    IronSourceAdsPublisherAgent.this.f4755a.b(a2, (Map<String, String>) map, (DSInterstitialListener) IronSourceAdsPublisherAgent.this);
                }
            });
        }
    }

    public boolean a(IronSourceAdInstance ironSourceAdInstance) {
        Logger.a("IronSourceAdsPublisherAgent", "isAdAvailable " + ironSourceAdInstance.c());
        DemandSource a2 = this.f.a(SSAEnums$ProductType.Interstitial, ironSourceAdInstance.c());
        if (a2 == null) {
            return false;
        }
        return a2.b();
    }
}
