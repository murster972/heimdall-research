package com.ironsource.sdk.utils;

import android.content.Context;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceStorageUtils {
    private static void a(String str) {
        File[] listFiles = new File(str).listFiles();
        if (listFiles != null) {
            for (File file : listFiles) {
                if (file.isDirectory()) {
                    a(file.getAbsolutePath());
                    file.delete();
                } else {
                    file.delete();
                }
            }
        }
    }

    public static String b(Context context) {
        if (!SDKUtils.l()) {
            return DeviceStatus.h(context);
        }
        File f = DeviceStatus.f(context);
        if (f == null || !f.canWrite()) {
            return DeviceStatus.h(context);
        }
        return f.getPath();
    }

    public static String c(Context context) {
        a(context);
        return d(context);
    }

    private static String d(Context context) {
        String d = IronSourceSharedPrefHelper.a(context).d();
        String g = DeviceProperties.g();
        if (d.equalsIgnoreCase(g)) {
            return a(context, "supersonicads").getPath();
        }
        IronSourceSharedPrefHelper.h().f(g);
        File f = DeviceStatus.f(context);
        if (f != null) {
            a(f.getAbsolutePath() + File.separator + "supersonicads" + File.separator);
        }
        a(DeviceStatus.h(context) + File.separator + "supersonicads" + File.separator);
        return a(context);
    }

    public static String e(String str, String str2) {
        File file = new File(str, str2);
        if (file.exists() || file.mkdirs()) {
            return file.getPath();
        }
        return null;
    }

    public static boolean f(String str, String str2) throws Exception {
        return new File(str).renameTo(new File(str2));
    }

    public static String c(String str, String str2) {
        JSONObject a2 = a(str, str2);
        try {
            a2.put("path", str2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return a2.toString();
    }

    public static synchronized boolean b(String str, String str2) {
        boolean z;
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, str2);
            z = a(file) && file.delete();
        }
        return z;
    }

    private static File a(Context context, String str) {
        return new File(b(context) + File.separator + str);
    }

    private static String a(Context context) {
        File a2 = a(context, "supersonicads");
        if (!a2.exists()) {
            a2.mkdir();
        }
        return a2.getPath();
    }

    private static Object b(File file) {
        String a2;
        JSONObject jSONObject = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        try {
            if (file.isFile()) {
                jSONArray.put(file.getName());
                return jSONArray;
            }
            for (File file2 : file.listFiles()) {
                if (file2.isDirectory()) {
                    jSONObject.put(file2.getName(), b(file2));
                } else {
                    jSONArray.put(file2.getName());
                    jSONObject.put("files", jSONArray);
                }
            }
            if (file.isDirectory() && (a2 = IronSourceSharedPrefHelper.h().a(file.getName())) != null) {
                jSONObject.put("lastUpdateTime", a2);
            }
            String lowerCase = file.getName().toLowerCase();
            SSAEnums$ProductType sSAEnums$ProductType = null;
            if (lowerCase.startsWith(SSAEnums$ProductType.RewardedVideo.toString().toLowerCase())) {
                sSAEnums$ProductType = SSAEnums$ProductType.RewardedVideo;
            } else if (lowerCase.startsWith(SSAEnums$ProductType.OfferWall.toString().toLowerCase())) {
                sSAEnums$ProductType = SSAEnums$ProductType.OfferWall;
            } else if (lowerCase.startsWith(SSAEnums$ProductType.Interstitial.toString().toLowerCase())) {
                sSAEnums$ProductType = SSAEnums$ProductType.Interstitial;
            }
            if (sSAEnums$ProductType != null) {
                jSONObject.put(SDKUtils.b("applicationUserId"), SDKUtils.b(IronSourceSharedPrefHelper.h().b(sSAEnums$ProductType)));
                jSONObject.put(SDKUtils.b("applicationKey"), SDKUtils.b(IronSourceSharedPrefHelper.h().a(sSAEnums$ProductType)));
            }
            return jSONObject;
        } catch (JSONException e) {
            e.printStackTrace();
            new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e.getStackTrace()[0].getMethodName()});
        }
    }

    public static boolean d(String str, String str2) {
        return new File(str, str2).exists();
    }

    public static synchronized boolean a(String str, String str2, String str3) {
        synchronized (IronSourceStorageUtils.class) {
            File file = new File(str, str2);
            if (!file.exists()) {
                return false;
            }
            File[] listFiles = file.listFiles();
            if (listFiles == null) {
                return false;
            }
            int length = listFiles.length;
            int i = 0;
            while (i < length) {
                File file2 = listFiles[i];
                if (!file2.isFile() || !file2.getName().equalsIgnoreCase(str3)) {
                    i++;
                } else {
                    boolean delete = file2.delete();
                    return delete;
                }
            }
            return false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x003c, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static synchronized boolean a(java.lang.String r6, com.ironsource.sdk.data.SSAFile r7) {
        /*
            java.lang.Class<com.ironsource.sdk.utils.IronSourceStorageUtils> r0 = com.ironsource.sdk.utils.IronSourceStorageUtils.class
            monitor-enter(r0)
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x003d }
            java.lang.String r2 = r7.e()     // Catch:{ all -> 0x003d }
            r1.<init>(r6, r2)     // Catch:{ all -> 0x003d }
            java.io.File[] r6 = r1.listFiles()     // Catch:{ all -> 0x003d }
            r2 = 0
            if (r6 == 0) goto L_0x003b
            java.io.File[] r6 = r1.listFiles()     // Catch:{ all -> 0x003d }
            int r1 = r6.length     // Catch:{ all -> 0x003d }
            r3 = 0
        L_0x0019:
            if (r3 >= r1) goto L_0x003b
            r4 = r6[r3]     // Catch:{ all -> 0x003d }
            boolean r5 = r4.isFile()     // Catch:{ all -> 0x003d }
            if (r5 == 0) goto L_0x0038
            java.lang.String r4 = r4.getName()     // Catch:{ all -> 0x003d }
            java.lang.String r5 = r7.c()     // Catch:{ all -> 0x003d }
            java.lang.String r5 = com.ironsource.sdk.utils.SDKUtils.c((java.lang.String) r5)     // Catch:{ all -> 0x003d }
            boolean r4 = r4.equalsIgnoreCase(r5)     // Catch:{ all -> 0x003d }
            if (r4 == 0) goto L_0x0038
            monitor-exit(r0)
            r6 = 1
            return r6
        L_0x0038:
            int r3 = r3 + 1
            goto L_0x0019
        L_0x003b:
            monitor-exit(r0)
            return r2
        L_0x003d:
            r6 = move-exception
            monitor-exit(r0)
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.IronSourceStorageUtils.a(java.lang.String, com.ironsource.sdk.data.SSAFile):boolean");
    }

    private static boolean a(File file) {
        File[] listFiles = file.listFiles();
        if (listFiles == null) {
            return true;
        }
        boolean z = true;
        for (File file2 : listFiles) {
            if (file2.isDirectory()) {
                z &= a(file2);
            }
            if (!file2.delete()) {
                z = false;
            }
        }
        return z;
    }

    private static JSONObject a(String str, String str2) {
        File file = new File(str, str2);
        JSONObject jSONObject = new JSONObject();
        File[] listFiles = file.listFiles();
        if (listFiles != null) {
            for (File file2 : listFiles) {
                try {
                    Object b = b(file2);
                    if (b instanceof JSONArray) {
                        jSONObject.put("files", b(file2));
                    } else if (b instanceof JSONObject) {
                        jSONObject.put(file2.getName(), b(file2));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    new IronSourceAsyncHttpRequestTask().execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e.getStackTrace()[0].getMethodName()});
                }
            }
        }
        return jSONObject;
    }

    public static int a(byte[] bArr, String str) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(new File(str));
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bArr);
        try {
            byte[] bArr2 = new byte[102400];
            int i = 0;
            while (true) {
                int read = byteArrayInputStream.read(bArr2);
                if (read == -1) {
                    return i;
                }
                fileOutputStream.write(bArr2, 0, read);
                i += read;
            }
        } finally {
            fileOutputStream.close();
            byteArrayInputStream.close();
        }
    }
}
