package com.ironsource.sdk.utils;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.view.View;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.data.SSAObj;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SDKUtils {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4910a = "SDKUtils";
    private static String b = null;
    private static boolean c = true;
    private static String d = null;
    private static int e = 0;
    private static String f = null;
    private static Map<String, String> g = null;
    private static String h = "";
    private static final AtomicInteger i = new AtomicInteger(1);

    public static int a(long j) {
        return (int) ((((float) j) * Resources.getSystem().getDisplayMetrics().density) + 0.5f);
    }

    public static String b(int i2) {
        return i2 != 1 ? i2 != 2 ? ViewProps.NONE : "landscape" : "portrait";
    }

    public static String b(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8").replace("+", "%20");
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    public static String c(int i2) {
        if (i2 != 0) {
            if (i2 != 1) {
                if (i2 != 11) {
                    if (i2 != 12) {
                        switch (i2) {
                            case 6:
                            case 8:
                                break;
                            case 7:
                            case 9:
                                break;
                            default:
                                return ViewProps.NONE;
                        }
                    }
                }
            }
            return "portrait";
        }
        return "landscape";
    }

    public static String c(String str) {
        String[] split = str.split(File.separator);
        try {
            return URLEncoder.encode(split[split.length - 1].split("\\?")[0], "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            e2.printStackTrace();
            return null;
        }
    }

    public static String d(String str) {
        try {
            String bigInteger = new BigInteger(1, MessageDigest.getInstance("MD5").digest(str.getBytes())).toString(16);
            while (bigInteger.length() < 32) {
                bigInteger = "0" + bigInteger;
            }
            return bigInteger;
        } catch (NoSuchAlgorithmException e2) {
            throw new RuntimeException(e2);
        }
    }

    public static JSONObject e() {
        try {
            return new JSONObject(d());
        } catch (JSONException e2) {
            e2.printStackTrace();
            return new JSONObject();
        }
    }

    public static String f() {
        return !TextUtils.isEmpty(d) ? d : "";
    }

    public static Long g() {
        return Long.valueOf(System.currentTimeMillis());
    }

    public static int h() {
        return e;
    }

    public static Map<String, String> i() {
        return g;
    }

    public static String j() {
        return "5.76";
    }

    public static String k() {
        return h;
    }

    public static boolean l() {
        String externalStorageState = Environment.getExternalStorageState();
        return "mounted".equals(externalStorageState) || "mounted_ro".equals(externalStorageState);
    }

    public static boolean m() {
        return c;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x004b, code lost:
        r0 = f4910a;
        com.ironsource.sdk.utils.Logger.c(r0, r4.getClass().getSimpleName() + ": " + r4.getCause());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0019, code lost:
        r4 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0020, code lost:
        if (r4.getMessage() != null) goto L_0x0022;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0022, code lost:
        r0 = f4910a;
        com.ironsource.sdk.utils.Logger.c(r0, r4.getClass().getSimpleName() + ": " + r4.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0049, code lost:
        if (r4.getCause() != null) goto L_0x004b;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(android.content.Context r4) {
        /*
            java.lang.String[] r4 = com.ironsource.environment.DeviceStatus.b((android.content.Context) r4)     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            r0 = 0
            r0 = r4[r0]     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            b = r0     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            r0 = 1
            r4 = r4[r0]     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r4)     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            boolean r4 = r4.booleanValue()     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            c = r4     // Catch:{ Exception -> 0x0019, all -> 0x0017 }
            goto L_0x006e
        L_0x0017:
            r4 = move-exception
            throw r4
        L_0x0019:
            r4 = move-exception
            java.lang.String r0 = r4.getMessage()
            java.lang.String r1 = ": "
            if (r0 == 0) goto L_0x0045
            java.lang.String r0 = f4910a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r4.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            r2.append(r1)
            java.lang.String r3 = r4.getMessage()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.ironsource.sdk.utils.Logger.c(r0, r2)
        L_0x0045:
            java.lang.Throwable r0 = r4.getCause()
            if (r0 == 0) goto L_0x006e
            java.lang.String r0 = f4910a
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.Class r3 = r4.getClass()
            java.lang.String r3 = r3.getSimpleName()
            r2.append(r3)
            r2.append(r1)
            java.lang.Throwable r4 = r4.getCause()
            r2.append(r4)
            java.lang.String r4 = r2.toString()
            com.ironsource.sdk.utils.Logger.c(r0, r4)
        L_0x006e:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.SDKUtils.b(android.content.Context):void");
    }

    public static void g(String str) {
        d = str;
    }

    public static JSONObject a(Context context) {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(AdUnitActivity.EXTRA_ORIENTATION, b(DeviceStatus.e(context)));
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    public static void f(String str) {
        f = str;
    }

    public static SSAEnums$ProductType e(String str) {
        if (str.equalsIgnoreCase(SSAEnums$ProductType.RewardedVideo.toString())) {
            return SSAEnums$ProductType.RewardedVideo;
        }
        if (str.equalsIgnoreCase(SSAEnums$ProductType.Interstitial.toString())) {
            return SSAEnums$ProductType.Interstitial;
        }
        if (str.equalsIgnoreCase(SSAEnums$ProductType.OfferWall.toString())) {
            return SSAEnums$ProductType.OfferWall;
        }
        return null;
    }

    public static String a(String str) {
        try {
            return URLDecoder.decode(str, "UTF-8");
        } catch (UnsupportedEncodingException e2) {
            String str2 = f4910a;
            Logger.a(str2, "Failed decoding string " + e2.getMessage());
            return "";
        }
    }

    public static String c() {
        return b;
    }

    public static void a(int i2) {
        e = i2;
    }

    public static String d() {
        return f;
    }

    public static String a(String str, String str2) {
        try {
            return new JSONObject(str).getString(str2);
        } catch (Exception unused) {
            return null;
        }
    }

    public static void b(Map<String, String> map) {
        g = map;
    }

    public static int a(boolean z) {
        int i2 = Build.VERSION.SDK_INT >= 14 ? 2 : 0;
        if (Build.VERSION.SDK_INT >= 16) {
            i2 |= 1796;
        }
        return (Build.VERSION.SDK_INT < 19 || !z) ? i2 : i2 | 4096;
    }

    private static int b() {
        int i2;
        int i3;
        do {
            i2 = i.get();
            i3 = i2 + 1;
            if (i3 > 16777215) {
                i3 = 1;
            }
        } while (!i.compareAndSet(i2, i3));
        return i2;
    }

    public static String b(String str, String str2) {
        if (str != null) {
            return str;
        }
        throw new NullPointerException(str2);
    }

    public static int a() {
        if (Build.VERSION.SDK_INT < 17) {
            return b();
        }
        return View.generateViewId();
    }

    public static JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) throws Exception {
        JSONObject jSONObject3 = new JSONObject();
        JSONArray jSONArray = new JSONArray();
        if (jSONObject != null) {
            jSONObject3 = new JSONObject(jSONObject.toString());
        }
        if (jSONObject2 != null) {
            jSONArray = jSONObject2.names();
        }
        if (jSONArray != null) {
            for (int i2 = 0; i2 < jSONArray.length(); i2++) {
                String string = jSONArray.getString(i2);
                jSONObject3.putOpt(string, jSONObject2.opt(string));
            }
        }
        return jSONObject3;
    }

    public static String a(Map<String, String> map) {
        JSONObject jSONObject = new JSONObject();
        if (map != null) {
            Iterator<Map.Entry<String, String>> it2 = map.entrySet().iterator();
            while (it2.hasNext()) {
                Map.Entry next = it2.next();
                try {
                    jSONObject.putOpt((String) next.getKey(), b((String) next.getValue()));
                } catch (JSONException e2) {
                    String str = f4910a;
                    Logger.c(str, "flatMapToJsonAsStringfailed " + e2.toString());
                }
                it2.remove();
            }
        }
        return jSONObject.toString();
    }

    public static Map<String, String> a(Map<String, String>[] mapArr) {
        HashMap hashMap = new HashMap();
        if (mapArr == null) {
            return hashMap;
        }
        for (Map<String, String> map : mapArr) {
            if (map != null) {
                hashMap.putAll(map);
            }
        }
        return hashMap;
    }

    public static String a(SSAObj sSAObj) {
        return a(sSAObj.a());
    }

    public static String a(JSONObject jSONObject) {
        String optString = jSONObject.optString("demandSourceId");
        return !TextUtils.isEmpty(optString) ? optString : jSONObject.optString("demandSourceName");
    }

    public static <T> T a(T t, String str) {
        if (t != null) {
            return t;
        }
        throw new NullPointerException(str);
    }
}
