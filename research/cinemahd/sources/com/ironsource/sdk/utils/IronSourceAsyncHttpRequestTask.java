package com.ironsource.sdk.utils;

import android.os.AsyncTask;

public class IronSourceAsyncHttpRequestTask extends AsyncTask<String, Integer, Integer> {
    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002f  */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x003a  */
    /* renamed from: a */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Integer doInBackground(java.lang.String... r5) {
        /*
            r4 = this;
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x0029 }
            r2 = 0
            r5 = r5[r2]     // Catch:{ Exception -> 0x0029 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x0029 }
            java.net.URLConnection r5 = r1.openConnection()     // Catch:{ Exception -> 0x0029 }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ Exception -> 0x0029 }
            r0 = 3000(0xbb8, float:4.204E-42)
            r5.setConnectTimeout(r0)     // Catch:{ Exception -> 0x0022, all -> 0x001d }
            r5.getInputStream()     // Catch:{ Exception -> 0x0022, all -> 0x001d }
            if (r5 == 0) goto L_0x0032
            r5.disconnect()
            goto L_0x0032
        L_0x001d:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x0038
        L_0x0022:
            r0 = move-exception
            r3 = r0
            r0 = r5
            r5 = r3
            goto L_0x002a
        L_0x0027:
            r5 = move-exception
            goto L_0x0038
        L_0x0029:
            r5 = move-exception
        L_0x002a:
            r5.printStackTrace()     // Catch:{ all -> 0x0027 }
            if (r0 == 0) goto L_0x0032
            r0.disconnect()
        L_0x0032:
            r5 = 1
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            return r5
        L_0x0038:
            if (r0 == 0) goto L_0x003d
            r0.disconnect()
        L_0x003d:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask.doInBackground(java.lang.String[]):java.lang.Integer");
    }
}
