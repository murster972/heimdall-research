package com.ironsource.sdk.utils;

import java.util.HashMap;
import java.util.Map;

public class IronSourceQaProperties {

    /* renamed from: a  reason: collision with root package name */
    private static IronSourceQaProperties f4904a;
    private static Map<String, String> b = new HashMap();

    private IronSourceQaProperties() {
    }

    public static IronSourceQaProperties b() {
        if (f4904a == null) {
            f4904a = new IronSourceQaProperties();
        }
        return f4904a;
    }

    public static boolean c() {
        return f4904a != null;
    }

    public Map<String, String> a() {
        return b;
    }
}
