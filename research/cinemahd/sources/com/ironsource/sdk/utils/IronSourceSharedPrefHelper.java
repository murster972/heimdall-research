package com.ironsource.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import com.ironsource.sdk.data.SSABCParameters;
import com.ironsource.sdk.data.SSAEnums$BackButtonState;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.data.SSASession;
import com.unity3d.ads.metadata.MediationMetaData;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.VisionDataDBAdapter;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceSharedPrefHelper {
    private static IronSourceSharedPrefHelper b;

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f4905a;

    /* renamed from: com.ironsource.sdk.utils.IronSourceSharedPrefHelper$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4906a = new int[SSAEnums$ProductType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.ironsource.sdk.data.SSAEnums$ProductType[] r0 = com.ironsource.sdk.data.SSAEnums$ProductType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f4906a = r0
                int[] r0 = f4906a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums$ProductType.RewardedVideo     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f4906a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums$ProductType.OfferWall     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f4906a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ironsource.sdk.data.SSAEnums$ProductType r1 = com.ironsource.sdk.data.SSAEnums$ProductType.Interstitial     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.utils.IronSourceSharedPrefHelper.AnonymousClass1.<clinit>():void");
        }
    }

    private IronSourceSharedPrefHelper(Context context) {
        this.f4905a = context.getSharedPreferences("supersonic_shared_preferen", 0);
    }

    public static synchronized IronSourceSharedPrefHelper a(Context context) {
        IronSourceSharedPrefHelper ironSourceSharedPrefHelper;
        synchronized (IronSourceSharedPrefHelper.class) {
            if (b == null) {
                b = new IronSourceSharedPrefHelper(context);
            }
            ironSourceSharedPrefHelper = b;
        }
        return ironSourceSharedPrefHelper;
    }

    public static synchronized IronSourceSharedPrefHelper h() {
        IronSourceSharedPrefHelper ironSourceSharedPrefHelper;
        synchronized (IronSourceSharedPrefHelper.class) {
            ironSourceSharedPrefHelper = b;
        }
        return ironSourceSharedPrefHelper;
    }

    public SSAEnums$BackButtonState b() {
        int parseInt = Integer.parseInt(this.f4905a.getString("back_button_state", TraktV2.API_VERSION));
        if (parseInt == 0) {
            return SSAEnums$BackButtonState.None;
        }
        if (parseInt == 1) {
            return SSAEnums$BackButtonState.Device;
        }
        if (parseInt == 2) {
            return SSAEnums$BackButtonState.Controller;
        }
        return SSAEnums$BackButtonState.Controller;
    }

    public String c() {
        return this.f4905a.getString("ssa_rv_parameter_connection_retries", "3");
    }

    public void d(String str) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString("application_key", str);
        edit.commit();
    }

    public void e(String str) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString("back_button_state", str);
        edit.commit();
    }

    public JSONArray f() {
        String string = this.f4905a.getString("sessions", (String) null);
        if (string == null) {
            return new JSONArray();
        }
        try {
            return new JSONArray(string);
        } catch (JSONException unused) {
            return new JSONArray();
        }
    }

    public void g(String str) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString("search_keys", str);
        edit.commit();
    }

    public String c(String str) {
        String string = this.f4905a.getString(str, (String) null);
        return string != null ? string : "{}";
    }

    public boolean h(String str) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString("unique_id", str);
        return edit.commit();
    }

    private boolean g() {
        return this.f4905a.getBoolean("register_sessions", true);
    }

    public void a(SSABCParameters sSABCParameters) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString("ssa_rv_parameter_connection_retries", sSABCParameters.b());
        edit.commit();
    }

    public String d() {
        return this.f4905a.getString(MediationMetaData.KEY_VERSION, "UN_VERSIONED");
    }

    public List<String> e() {
        String string = this.f4905a.getString("search_keys", (String) null);
        ArrayList arrayList = new ArrayList();
        if (string != null) {
            SSAObj sSAObj = new SSAObj(string);
            if (sSAObj.a("searchKeys")) {
                try {
                    arrayList.addAll(sSAObj.a((JSONArray) sSAObj.b("searchKeys")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return arrayList;
    }

    public void f(String str) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString(MediationMetaData.KEY_VERSION, str);
        edit.commit();
    }

    public void a() {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString("sessions", (String) null);
        edit.commit();
    }

    public boolean b(String str, String str2) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString(str, str2);
        return edit.commit();
    }

    public void a(SSASession sSASession) {
        if (g()) {
            JSONObject jSONObject = new JSONObject();
            try {
                jSONObject.put("sessionStartTime", sSASession.d());
                jSONObject.put("sessionEndTime", sSASession.c());
                jSONObject.put("sessionType", sSASession.e());
                jSONObject.put("connectivity", sSASession.b());
            } catch (JSONException unused) {
            }
            JSONArray f = f();
            if (f == null) {
                f = new JSONArray();
            }
            f.put(jSONObject);
            SharedPreferences.Editor edit = this.f4905a.edit();
            edit.putString("sessions", f.toString());
            edit.commit();
        }
    }

    public String b(String str) {
        String str2 = null;
        if (str.equalsIgnoreCase(SSAEnums$ProductType.RewardedVideo.toString())) {
            str2 = this.f4905a.getString("unique_id_rv", (String) null);
        } else if (str.equalsIgnoreCase(SSAEnums$ProductType.OfferWall.toString())) {
            str2 = this.f4905a.getString("unique_id_ow", (String) null);
        } else if (str.equalsIgnoreCase(SSAEnums$ProductType.Interstitial.toString())) {
            str2 = this.f4905a.getString("unique_id_is", (String) null);
        }
        return str2 == null ? this.f4905a.getString("unique_id", "EMPTY_UNIQUE_ID") : str2;
    }

    public String b(SSAEnums$ProductType sSAEnums$ProductType) {
        return b(sSAEnums$ProductType.toString());
    }

    public void a(boolean z) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putBoolean("register_sessions", z);
        edit.commit();
    }

    public String a(SSAEnums$ProductType sSAEnums$ProductType) {
        int i = AnonymousClass1.f4906a[sSAEnums$ProductType.ordinal()];
        String str = null;
        if (i == 1) {
            str = this.f4905a.getString("application_key_rv", (String) null);
        } else if (i == 2) {
            str = this.f4905a.getString("application_key_ow", (String) null);
        } else if (i == 3) {
            str = this.f4905a.getString("application_key_is", (String) null);
        }
        return str == null ? this.f4905a.getString("application_key", "EMPTY_APPLICATION_KEY") : str;
    }

    public String a(String str) {
        return this.f4905a.getString(str, (String) null);
    }

    public void a(String str, String str2) {
        SharedPreferences.Editor edit = this.f4905a.edit();
        edit.putString(str, str2);
        edit.commit();
    }

    public boolean a(String str, String str2, String str3) {
        String string = this.f4905a.getString("ssaUserData", (String) null);
        if (!TextUtils.isEmpty(string)) {
            try {
                JSONObject jSONObject = new JSONObject(string);
                if (!jSONObject.isNull(str2)) {
                    JSONObject jSONObject2 = jSONObject.getJSONObject(str2);
                    if (!jSONObject2.isNull(str3)) {
                        jSONObject2.getJSONObject(str3).put(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, str);
                        SharedPreferences.Editor edit = this.f4905a.edit();
                        edit.putString("ssaUserData", jSONObject.toString());
                        return edit.commit();
                    }
                }
            } catch (JSONException e) {
                IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
                ironSourceAsyncHttpRequestTask.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e.getStackTrace()[0].getMethodName()});
            }
        }
        return false;
    }
}
