package com.ironsource.sdk.utils;

import android.content.Context;
import com.ironsource.environment.DeviceStatus;

public class DeviceProperties {
    private static DeviceProperties g;

    /* renamed from: a  reason: collision with root package name */
    private String f4903a = DeviceStatus.g();
    private String b = DeviceStatus.f();
    private String c = DeviceStatus.h();
    private String d = DeviceStatus.b();
    private int e = DeviceStatus.a();
    private String f;

    private DeviceProperties(Context context) {
        this.f = DeviceStatus.i(context);
    }

    public static DeviceProperties b(Context context) {
        if (g == null) {
            g = new DeviceProperties(context);
        }
        return g;
    }

    public static String g() {
        return "5.76";
    }

    public int a() {
        return this.e;
    }

    public String c() {
        return this.b;
    }

    public String d() {
        return this.f4903a;
    }

    public String e() {
        return this.c;
    }

    public String f() {
        return this.d;
    }

    public float a(Context context) {
        return DeviceStatus.k(context);
    }

    public String b() {
        return this.f;
    }
}
