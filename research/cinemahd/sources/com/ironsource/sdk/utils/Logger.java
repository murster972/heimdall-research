package com.ironsource.sdk.utils;

import android.util.Log;
import com.ironsource.sdk.data.SSAEnums$DebugMode;

public class Logger {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f4907a;

    public static void a(int i) {
        if (SSAEnums$DebugMode.MODE_0.a() == i) {
            f4907a = false;
        } else {
            f4907a = true;
        }
    }

    public static void b(String str, String str2) {
        if (f4907a) {
            Log.e(str, str2);
        }
    }

    public static void c(String str, String str2) {
        if (f4907a) {
            Log.i(str, str2);
        }
    }

    public static void a(String str, String str2) {
        if (f4907a) {
            Log.d(str, str2);
        }
    }
}
