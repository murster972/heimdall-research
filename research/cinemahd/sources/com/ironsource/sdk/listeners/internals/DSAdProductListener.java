package com.ironsource.sdk.listeners.internals;

import com.ironsource.sdk.data.AdUnitsReady;
import com.ironsource.sdk.data.SSAEnums$ProductType;
import org.json.JSONObject;

public interface DSAdProductListener {
    void a(SSAEnums$ProductType sSAEnums$ProductType, String str);

    void a(SSAEnums$ProductType sSAEnums$ProductType, String str, AdUnitsReady adUnitsReady);

    void a(SSAEnums$ProductType sSAEnums$ProductType, String str, String str2);

    void a(SSAEnums$ProductType sSAEnums$ProductType, String str, String str2, JSONObject jSONObject);

    void b(SSAEnums$ProductType sSAEnums$ProductType, String str);

    void c(SSAEnums$ProductType sSAEnums$ProductType, String str);
}
