package com.ironsource.sdk.handlers;

import android.app.Activity;
import com.ironsource.sdk.agent.IronSourceAdsPublisherAgent;
import com.ironsource.sdk.controller.WebController;
import com.ironsource.sdk.data.SSAEnums$BackButtonState;
import com.ironsource.sdk.utils.IronSourceSharedPrefHelper;

public class BackButtonHandler {

    /* renamed from: a  reason: collision with root package name */
    public static BackButtonHandler f4888a;

    /* renamed from: com.ironsource.sdk.handlers.BackButtonHandler$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f4889a = new int[SSAEnums$BackButtonState.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.ironsource.sdk.data.SSAEnums$BackButtonState[] r0 = com.ironsource.sdk.data.SSAEnums$BackButtonState.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f4889a = r0
                int[] r0 = f4889a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.ironsource.sdk.data.SSAEnums$BackButtonState r1 = com.ironsource.sdk.data.SSAEnums$BackButtonState.None     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f4889a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.ironsource.sdk.data.SSAEnums$BackButtonState r1 = com.ironsource.sdk.data.SSAEnums$BackButtonState.Device     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f4889a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.ironsource.sdk.data.SSAEnums$BackButtonState r1 = com.ironsource.sdk.data.SSAEnums$BackButtonState.Controller     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.ironsource.sdk.handlers.BackButtonHandler.AnonymousClass1.<clinit>():void");
        }
    }

    public static BackButtonHandler a() {
        BackButtonHandler backButtonHandler = f4888a;
        return backButtonHandler == null ? new BackButtonHandler() : backButtonHandler;
    }

    public boolean a(Activity activity) {
        int i = AnonymousClass1.f4889a[IronSourceSharedPrefHelper.h().b().ordinal()];
        if (i == 1 || i == 2 || i != 3) {
            return false;
        }
        try {
            WebController webController = (WebController) IronSourceAdsPublisherAgent.b(activity).a().e();
            if (webController != null) {
                webController.d("back");
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
