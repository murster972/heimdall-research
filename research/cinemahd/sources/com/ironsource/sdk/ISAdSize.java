package com.ironsource.sdk;

public class ISAdSize {

    /* renamed from: a  reason: collision with root package name */
    private int f4739a;
    private int b;
    private String c;

    public ISAdSize(int i, int i2, String str) {
        this.f4739a = i;
        this.b = i2;
        this.c = str;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.f4739a;
    }

    public String toString() {
        return this.c;
    }
}
