package com.ironsource.sdk.service;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.environment.ConnectivityService;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.service.Connectivity.ConnectivityUtils;
import com.ironsource.sdk.utils.DeviceProperties;
import com.ironsource.sdk.utils.IronSourceStorageUtils;
import com.ironsource.sdk.utils.Logger;
import com.ironsource.sdk.utils.SDKUtils;
import org.json.JSONObject;

public class DeviceData {

    /* renamed from: a  reason: collision with root package name */
    private static final String f4900a = "DeviceData";

    public static JSONObject a(Context context) {
        SDKUtils.b(context);
        String c = SDKUtils.c();
        Boolean valueOf = Boolean.valueOf(SDKUtils.m());
        JSONObject jSONObject = new JSONObject();
        if (!TextUtils.isEmpty(c)) {
            try {
                Logger.c(f4900a, "add AID and LAT");
                jSONObject.put("isLimitAdTrackingEnabled", valueOf);
                jSONObject.put("deviceIds" + "[" + "AID" + "]", SDKUtils.b(c));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return jSONObject;
    }

    public static JSONObject b(Context context) {
        JSONObject jSONObject = new JSONObject();
        a(jSONObject);
        b(context, jSONObject);
        d(context, jSONObject);
        a(context, jSONObject);
        c(context, jSONObject);
        return jSONObject;
    }

    private static void c(Context context, JSONObject jSONObject) {
        try {
            jSONObject.put(SDKUtils.b("deviceVolume"), (double) DeviceProperties.b(context).a(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void d(Context context, JSONObject jSONObject) {
        try {
            jSONObject.put(SDKUtils.b("diskFreeSize"), SDKUtils.b(String.valueOf(DeviceStatus.b(IronSourceStorageUtils.b(context)))));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static JSONObject c(Context context) {
        DeviceProperties b = DeviceProperties.b(context);
        JSONObject jSONObject = new JSONObject();
        try {
            String d = b.d();
            if (d != null) {
                jSONObject.put(SDKUtils.b("deviceOEM"), SDKUtils.b(d));
            }
            String c = b.c();
            if (c != null) {
                jSONObject.put(SDKUtils.b("deviceModel"), SDKUtils.b(c));
            }
            String e = b.e();
            if (e != null) {
                jSONObject.put(SDKUtils.b("deviceOs"), SDKUtils.b(e));
            }
            String f = b.f();
            if (f != null) {
                jSONObject.put(SDKUtils.b("deviceOSVersion"), f.replaceAll("[^0-9/.]", ""));
            }
            String f2 = b.f();
            if (f2 != null) {
                jSONObject.put(SDKUtils.b("deviceOSVersionFull"), SDKUtils.b(f2));
            }
            jSONObject.put(SDKUtils.b("deviceApiLevel"), String.valueOf(b.a()));
            String g = DeviceProperties.g();
            if (g != null) {
                jSONObject.put(SDKUtils.b("SDKVersion"), SDKUtils.b(g));
            }
            if (b.b() != null && b.b().length() > 0) {
                jSONObject.put(SDKUtils.b("mobileCarrier"), SDKUtils.b(b.b()));
            }
            String language = context.getResources().getConfiguration().locale.getLanguage();
            if (!TextUtils.isEmpty(language)) {
                jSONObject.put(SDKUtils.b("deviceLanguage"), SDKUtils.b(language.toUpperCase()));
            }
            String f3 = ApplicationContext.f(context);
            if (!TextUtils.isEmpty(f3)) {
                jSONObject.put(SDKUtils.b("bundleId"), SDKUtils.b(f3));
            }
            String valueOf = String.valueOf(DeviceStatus.c());
            if (!TextUtils.isEmpty(valueOf)) {
                jSONObject.put(SDKUtils.b("deviceScreenScale"), SDKUtils.b(valueOf));
            }
            String valueOf2 = String.valueOf(DeviceStatus.m());
            if (!TextUtils.isEmpty(valueOf2)) {
                jSONObject.put(SDKUtils.b("unLocked"), SDKUtils.b(valueOf2));
            }
            jSONObject.put(SDKUtils.b("mcc"), ConnectivityService.c(context));
            jSONObject.put(SDKUtils.b("mnc"), ConnectivityService.d(context));
            jSONObject.put(SDKUtils.b("phoneType"), ConnectivityService.e(context));
            jSONObject.put(SDKUtils.b("simOperator"), SDKUtils.b(ConnectivityService.f(context)));
            jSONObject.put(SDKUtils.b("lastUpdateTime"), ApplicationContext.e(context));
            jSONObject.put(SDKUtils.b("firstInstallTime"), ApplicationContext.c(context));
            jSONObject.put(SDKUtils.b("appVersion"), SDKUtils.b(ApplicationContext.b(context)));
            String d2 = ApplicationContext.d(context);
            if (!TextUtils.isEmpty(d2)) {
                jSONObject.put(SDKUtils.b("installerPackageName"), SDKUtils.b(d2));
            }
            jSONObject.put("localTime", SDKUtils.b(String.valueOf(DeviceStatus.e())));
            jSONObject.put("timezoneOffset", SDKUtils.b(String.valueOf(DeviceStatus.i())));
        } catch (Exception e2) {
            e2.printStackTrace();
        }
        return jSONObject;
    }

    private static void b(Context context, JSONObject jSONObject) {
        try {
            String b = ConnectivityUtils.b(context);
            if (!TextUtils.isEmpty(b) && !b.equals(ViewProps.NONE)) {
                jSONObject.put(SDKUtils.b("connectionType"), SDKUtils.b(b));
            }
            if (Build.VERSION.SDK_INT >= 23) {
                jSONObject.put(SDKUtils.b("cellularNetworkType"), ConnectivityService.a(context));
                jSONObject.put(SDKUtils.b("hasVPN"), ConnectivityUtils.d(context));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void a(JSONObject jSONObject, String str, String str2) {
        try {
            if (!TextUtils.isEmpty(str2)) {
                jSONObject.put(str, SDKUtils.b(str2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void a(JSONObject jSONObject) {
        try {
            a(jSONObject, "displaySizeWidth", String.valueOf(DeviceStatus.l()));
            a(jSONObject, "displaySizeHeight", String.valueOf(DeviceStatus.k()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void a(Context context, JSONObject jSONObject) {
        try {
            jSONObject.put(SDKUtils.b("batteryLevel"), DeviceStatus.d(context));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
