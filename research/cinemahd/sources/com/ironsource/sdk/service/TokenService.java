package com.ironsource.sdk.service;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.iab.omid.library.ironsrc.Omid;
import com.ironsource.environment.DeviceStatus;
import com.ironsource.sdk.utils.IronSourceQaProperties;
import com.ironsource.sdk.utils.SDKUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class TokenService {
    private static TokenService b;

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f4901a = new JSONObject();

    private TokenService() {
    }

    public static synchronized TokenService d() {
        TokenService tokenService;
        synchronized (TokenService.class) {
            if (b == null) {
                b = new TokenService();
            }
            tokenService = b;
        }
        return tokenService;
    }

    public void a() {
        HashMap hashMap = new HashMap();
        hashMap.put("omidVersion", Omid.a());
        hashMap.put("omidPartnerVersion", "6");
        b.a((Map<String, String>) hashMap);
    }

    public void b() {
        if (IronSourceQaProperties.c()) {
            b.a(IronSourceQaProperties.b().a());
        }
    }

    public void c(String str) {
        if (!TextUtils.isEmpty(str)) {
            try {
                a("chinaCDN", new JSONObject(str).opt("chinaCDN"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void b(String str) {
        if (str != null) {
            a("applicationUserId", SDKUtils.b(str));
        }
    }

    public void b(Activity activity) {
        if (activity != null) {
            if (Build.VERSION.SDK_INT >= 19) {
                a(SDKUtils.b("immersiveMode"), Boolean.valueOf(DeviceStatus.a(activity)));
            }
            a("appOrientation", SDKUtils.c(DeviceStatus.a((Context) activity)));
        }
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str, Object obj) {
        try {
            this.f4901a.put(str, obj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return;
    }

    public String c(Context context) {
        try {
            return Gibberish.a(b(context).toString());
        } catch (Exception unused) {
            return Gibberish.a(new JSONObject().toString());
        }
    }

    public void a(String str) {
        if (str != null) {
            a("applicationKey", SDKUtils.b(str));
        }
    }

    public void c() {
        c(SDKUtils.d());
        a(SDKUtils.i());
        b();
        a();
    }

    public void a(final Activity activity) {
        if (activity != null) {
            try {
                new Thread(new Runnable() {
                    public void run() {
                        try {
                            TokenService.this.a(DeviceData.a((Context) activity));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void b(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            a("metadata_" + next, jSONObject.opt(next));
        }
    }

    public void a(Context context) {
        if (context != null) {
            a(DeviceData.c(context));
            a(DeviceData.b(context));
        }
    }

    public void a(Map<String, String> map) {
        if (map == null) {
            Log.d("TokenService", "collectDataFromExternalParams params=null");
            return;
        }
        for (String next : map.keySet()) {
            a(next, SDKUtils.b(map.get(next)));
        }
    }

    public JSONObject b(Context context) {
        c();
        a(context);
        return this.f4901a;
    }

    public void a(JSONObject jSONObject) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            a(next, jSONObject.opt(next));
        }
    }

    public void a(Activity activity, String str, String str2) {
        a(activity);
        b(activity);
        a((Context) activity);
        b(str2);
        a(str);
    }
}
