package com.ironsource.sdk.service;

import android.content.Context;
import android.os.Build;
import com.ironsource.environment.ApplicationContext;
import com.ironsource.sdk.service.Connectivity.BroadcastReceiverStrategy;
import com.ironsource.sdk.service.Connectivity.IConnectivity;
import com.ironsource.sdk.service.Connectivity.IConnectivityStatus;
import com.ironsource.sdk.service.Connectivity.NetworkCallbackStrategy;
import com.ironsource.sdk.utils.Logger;
import org.json.JSONObject;

public abstract class ConnectivityAdapter implements IConnectivityStatus {

    /* renamed from: a  reason: collision with root package name */
    private IConnectivity f4899a;

    protected ConnectivityAdapter(JSONObject jSONObject, Context context) {
        this.f4899a = a(jSONObject, context);
        String simpleName = ConnectivityAdapter.class.getSimpleName();
        Logger.c(simpleName, "created ConnectivityAdapter with strategy " + this.f4899a.getClass().getSimpleName());
    }

    public JSONObject a(Context context) {
        return this.f4899a.a(context);
    }

    public void a(String str, JSONObject jSONObject) {
    }

    public void b(Context context) {
        this.f4899a.b(context);
    }

    public void b(String str, JSONObject jSONObject) {
    }

    public void c(Context context) {
        this.f4899a.c(context);
    }

    public void onDisconnected() {
    }

    public void a() {
        this.f4899a.release();
    }

    private IConnectivity a(JSONObject jSONObject, Context context) {
        if (jSONObject.optInt("connectivityStrategy") == 1) {
            return new BroadcastReceiverStrategy(this);
        }
        boolean b = ApplicationContext.b(context, "android.permission.ACCESS_NETWORK_STATE");
        if (Build.VERSION.SDK_INT < 23 || !b) {
            return new BroadcastReceiverStrategy(this);
        }
        return new NetworkCallbackStrategy(this);
    }
}
