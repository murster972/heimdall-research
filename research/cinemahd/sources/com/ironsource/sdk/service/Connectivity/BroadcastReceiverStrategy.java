package com.ironsource.sdk.service.Connectivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.sdk.utils.IronSourceAsyncHttpRequestTask;
import org.json.JSONObject;

public class BroadcastReceiverStrategy implements IConnectivity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final IConnectivityStatus f4895a;
    private BroadcastReceiver b = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String b = ConnectivityUtils.b(context);
            if (b.equals(ViewProps.NONE)) {
                BroadcastReceiverStrategy.this.f4895a.onDisconnected();
            } else {
                BroadcastReceiverStrategy.this.f4895a.b(b, new JSONObject());
            }
        }
    };

    public BroadcastReceiverStrategy(IConnectivityStatus iConnectivityStatus) {
        this.f4895a = iConnectivityStatus;
    }

    public void b(Context context) {
        try {
            context.registerReceiver(this.b, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void c(Context context) {
        try {
            context.unregisterReceiver(this.b);
        } catch (IllegalArgumentException unused) {
        } catch (Exception e) {
            Log.e("ContentValues", "unregisterConnectionReceiver - " + e);
            IronSourceAsyncHttpRequestTask ironSourceAsyncHttpRequestTask = new IronSourceAsyncHttpRequestTask();
            ironSourceAsyncHttpRequestTask.execute(new String[]{"https://www.supersonicads.com/mobile/sdk5/log?method=" + e.getStackTrace()[0].getMethodName()});
        }
    }

    public void release() {
        this.b = null;
    }

    public JSONObject a(Context context) {
        return new JSONObject();
    }
}
