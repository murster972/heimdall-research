package com.ironsource.sdk.service.Connectivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;
import android.text.TextUtils;
import com.facebook.react.uimanager.ViewProps;
import com.ironsource.environment.ConnectivityService;
import org.json.JSONException;
import org.json.JSONObject;

public class ConnectivityUtils {
    @SuppressLint({"MissingPermission"})
    static String a(Network network, Context context) {
        String c;
        if (context == null) {
            return ViewProps.NONE;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (network == null || connectivityManager == null) {
            return ViewProps.NONE;
        }
        try {
            if (Build.VERSION.SDK_INT < 21) {
                return c(context);
            }
            NetworkCapabilities networkCapabilities = connectivityManager.getNetworkCapabilities(network);
            if (networkCapabilities == null) {
                return c(context);
            }
            if (networkCapabilities.hasTransport(1)) {
                c = "wifi";
            } else if (networkCapabilities.hasTransport(0)) {
                c = "3g";
            } else {
                c = c(context);
            }
            return c;
        } catch (Exception e) {
            e.printStackTrace();
            return ViewProps.NONE;
        }
    }

    public static String b(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return a(a(context), context);
        }
        return c(context);
    }

    private static String c(Context context) {
        String b = ConnectivityService.b(context);
        return TextUtils.isEmpty(b) ? ViewProps.NONE : b;
    }

    public static boolean d(Context context) {
        return b(context, a(context)).equals("vpn");
    }

    @SuppressLint({"MissingPermission"})
    private static String b(Context context, Network network) {
        NetworkCapabilities networkCapabilities;
        if (!(Build.VERSION.SDK_INT < 23 || network == null || context == null)) {
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                if (connectivityManager == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(network)) == null) {
                    return "";
                }
                if (networkCapabilities.hasTransport(1)) {
                    return "wifi";
                }
                if (networkCapabilities.hasTransport(0)) {
                    return "cellular";
                }
                if (networkCapabilities.hasTransport(4)) {
                    return "vpn";
                }
                if (networkCapabilities.hasTransport(3)) {
                    return "ethernet";
                }
                if (networkCapabilities.hasTransport(5)) {
                    return "wifiAware";
                }
                if (networkCapabilities.hasTransport(6)) {
                    return "lowpan";
                }
                if (networkCapabilities.hasTransport(2)) {
                    return "bluetooth";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return "";
    }

    @SuppressLint({"MissingPermission"})
    static Network a(Context context) {
        if (context == null) {
            return null;
        }
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (Build.VERSION.SDK_INT < 23 || connectivityManager == null) {
            return null;
        }
        return connectivityManager.getActiveNetwork();
    }

    @SuppressLint({"MissingPermission"})
    static JSONObject a(Context context, Network network) {
        NetworkCapabilities networkCapabilities;
        if (context == null) {
            return new JSONObject();
        }
        JSONObject jSONObject = new JSONObject();
        if (Build.VERSION.SDK_INT >= 23 && network != null) {
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                if (!(connectivityManager == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(network)) == null)) {
                    jSONObject.put("networkCapabilities", networkCapabilities.toString());
                    jSONObject.put("downloadSpeed", networkCapabilities.getLinkDownstreamBandwidthKbps());
                    jSONObject.put("uploadSpeed", networkCapabilities.getLinkUpstreamBandwidthKbps());
                    jSONObject.put("hasVPN", d(context));
                }
                if (a(network, context).equals("3g")) {
                    jSONObject.put("cellularNetworkType", ConnectivityService.a(context));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return jSONObject;
    }
}
