package com.ironsource.sdk.service.Connectivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Build;
import android.util.Log;
import com.facebook.react.uimanager.ViewProps;
import org.json.JSONObject;

public class NetworkCallbackStrategy implements IConnectivity {

    /* renamed from: a  reason: collision with root package name */
    private String f4897a = NetworkCallbackStrategy.class.getSimpleName();
    private int b = 23;
    /* access modifiers changed from: private */
    public final IConnectivityStatus c;
    private ConnectivityManager.NetworkCallback d;

    public NetworkCallbackStrategy(IConnectivityStatus iConnectivityStatus) {
        this.c = iConnectivityStatus;
    }

    @SuppressLint({"NewApi", "MissingPermission"})
    public void b(final Context context) {
        if (Build.VERSION.SDK_INT >= this.b) {
            c(context);
            if (ConnectivityUtils.b(context).equals(ViewProps.NONE)) {
                this.c.onDisconnected();
            }
            if (this.d == null) {
                this.d = new ConnectivityManager.NetworkCallback() {
                    public void onAvailable(Network network) {
                        if (network != null) {
                            NetworkCallbackStrategy.this.c.b(ConnectivityUtils.a(network, context), ConnectivityUtils.a(context, network));
                            return;
                        }
                        IConnectivityStatus a2 = NetworkCallbackStrategy.this.c;
                        String b2 = ConnectivityUtils.b(context);
                        Context context = context;
                        a2.b(b2, ConnectivityUtils.a(context, ConnectivityUtils.a(context)));
                    }

                    public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
                        if (network != null) {
                            NetworkCallbackStrategy.this.c.a(ConnectivityUtils.a(network, context), ConnectivityUtils.a(context, network));
                        }
                    }

                    public void onLinkPropertiesChanged(Network network, LinkProperties linkProperties) {
                        if (network != null) {
                            NetworkCallbackStrategy.this.c.a(ConnectivityUtils.a(network, context), ConnectivityUtils.a(context, network));
                        }
                    }

                    public void onLost(Network network) {
                        if (ConnectivityUtils.b(context).equals(ViewProps.NONE)) {
                            NetworkCallbackStrategy.this.c.onDisconnected();
                        }
                    }
                };
            }
            NetworkRequest build = new NetworkRequest.Builder().addCapability(12).build();
            try {
                ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
                if (connectivityManager != null) {
                    connectivityManager.registerNetworkCallback(build, this.d);
                }
            } catch (Exception unused) {
                Log.e(this.f4897a, "NetworkCallback was not able to register");
            }
        }
    }

    @SuppressLint({"NewApi"})
    public void c(Context context) {
        ConnectivityManager connectivityManager;
        if (Build.VERSION.SDK_INT >= this.b && this.d != null && context != null && (connectivityManager = (ConnectivityManager) context.getSystemService("connectivity")) != null) {
            try {
                connectivityManager.unregisterNetworkCallback(this.d);
            } catch (Exception unused) {
                Log.e(this.f4897a, "NetworkCallback for was not registered or already unregistered");
            }
        }
    }

    public void release() {
        this.d = null;
    }

    public JSONObject a(Context context) {
        return ConnectivityUtils.a(context, ConnectivityUtils.a(context));
    }
}
