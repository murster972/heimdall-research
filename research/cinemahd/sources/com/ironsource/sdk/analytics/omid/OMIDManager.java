package com.ironsource.sdk.analytics.omid;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.WebView;
import com.iab.omid.library.ironsrc.Omid;
import com.iab.omid.library.ironsrc.adsession.AdEvents;
import com.iab.omid.library.ironsrc.adsession.AdSession;
import com.iab.omid.library.ironsrc.adsession.AdSessionConfiguration;
import com.iab.omid.library.ironsrc.adsession.AdSessionContext;
import com.iab.omid.library.ironsrc.adsession.Owner;
import com.iab.omid.library.ironsrc.adsession.Partner;
import com.ironsource.sdk.data.SSAObj;
import com.ironsource.sdk.utils.SDKUtils;
import org.json.JSONObject;

public class OMIDManager {

    /* renamed from: a  reason: collision with root package name */
    private static final Partner f4770a = Partner.a("Ironsrc", "6");
    private static AdSession b;
    private static boolean c = false;

    static class OMIDOptions {

        /* renamed from: a  reason: collision with root package name */
        public boolean f4771a;
        public Owner b;
        public Owner c;
        public String d;

        public static OMIDOptions a(JSONObject jSONObject) throws IllegalArgumentException {
            OMIDOptions oMIDOptions = new OMIDOptions();
            oMIDOptions.f4771a = jSONObject.optBoolean("isolateVerificationScripts", false);
            String optString = jSONObject.optString("impressionOwner", "");
            if (!TextUtils.isEmpty(optString)) {
                try {
                    oMIDOptions.b = Owner.valueOf(optString.toUpperCase());
                    String optString2 = jSONObject.optString("videoEventsOwner", "");
                    if (!TextUtils.isEmpty(optString)) {
                        try {
                            oMIDOptions.c = Owner.valueOf(optString2.toUpperCase());
                            oMIDOptions.d = jSONObject.optString("customReferenceData", "");
                            return oMIDOptions;
                        } catch (IllegalArgumentException unused) {
                            throw new IllegalArgumentException(String.format("%s | Invalid OMID videoEventsOwner", new Object[]{optString2}));
                        }
                    } else {
                        throw new IllegalArgumentException(String.format("Missing OMID videoEventsOwner", new Object[]{optString2}));
                    }
                } catch (IllegalArgumentException unused2) {
                    throw new IllegalArgumentException(String.format("%s | Invalid OMID impressionOwner", new Object[]{optString}));
                }
            } else {
                throw new IllegalArgumentException(String.format("Missing OMID impressionOwner", new Object[]{optString}));
            }
        }
    }

    public static void a(Context context) throws IllegalArgumentException {
        if (!c) {
            c = Omid.a(Omid.a(), context);
        }
    }

    public static void b(OMIDOptions oMIDOptions, WebView webView) throws IllegalStateException, IllegalArgumentException {
        if (!c) {
            throw new IllegalStateException("OMID has not been activated");
        } else if (b == null) {
            b = a(oMIDOptions, webView);
            b.b();
        } else {
            throw new IllegalStateException("OMID Session has already started");
        }
    }

    public static SSAObj c() {
        SSAObj sSAObj = new SSAObj();
        sSAObj.a(SDKUtils.b("omidVersion"), SDKUtils.b(Omid.a()));
        sSAObj.a(SDKUtils.b("omidPartnerName"), SDKUtils.b("Ironsrc"));
        sSAObj.a(SDKUtils.b("omidPartnerVersion"), SDKUtils.b("6"));
        return sSAObj;
    }

    public static void d() throws IllegalArgumentException, IllegalStateException {
        a();
        AdEvents.a(b).a();
    }

    public static void a(JSONObject jSONObject, WebView webView) throws IllegalStateException, IllegalArgumentException {
        b(OMIDOptions.a(jSONObject), webView);
    }

    private static AdSession a(OMIDOptions oMIDOptions, WebView webView) throws IllegalArgumentException {
        AdSession a2 = AdSession.a(AdSessionConfiguration.a(oMIDOptions.b, oMIDOptions.c, oMIDOptions.f4771a), AdSessionContext.a(f4770a, webView, oMIDOptions.d));
        a2.a(webView);
        return a2;
    }

    public static void b() throws IllegalStateException {
        a();
        b.a();
        b = null;
    }

    private static void a() throws IllegalStateException {
        if (!c) {
            throw new IllegalStateException("OMID has not been activated");
        } else if (b == null) {
            throw new IllegalStateException("OMID Session has not started");
        }
    }
}
