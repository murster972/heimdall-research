package com.ironsource.sdk.ISNAdView;

import android.webkit.JavascriptInterface;

public class ISNAdViewWebViewJSInterface {

    /* renamed from: a  reason: collision with root package name */
    private ISNAdView f4749a;

    ISNAdViewWebViewJSInterface(ISNAdView iSNAdView) {
        this.f4749a = iSNAdView;
    }

    @JavascriptInterface
    public void receiveMessageFromExternal(String str) {
        this.f4749a.a(str);
    }
}
