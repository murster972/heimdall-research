package com.ironsource.sdk.ISNAdView;

import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import com.ironsource.sdk.ISAdSize;
import com.ironsource.sdk.SSAFactory;
import org.json.JSONObject;

public class ISNAdView extends FrameLayout {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public WebView f4740a;
    /* access modifiers changed from: private */
    public Activity b;
    /* access modifiers changed from: private */
    public ISAdSize c;
    /* access modifiers changed from: private */
    public String d;
    /* access modifiers changed from: private */
    public ISNAdViewLogic e;
    /* access modifiers changed from: private */
    public String f = ISNAdView.class.getSimpleName();

    interface IErrorReportDelegate {
        void a(String str);
    }

    public ISNAdView(Activity activity, String str, ISAdSize iSAdSize) {
        super(activity);
        this.b = activity;
        this.c = iSAdSize;
        this.d = str;
        this.e = new ISNAdViewLogic();
    }

    public ISAdSize getAdViewSize() {
        return this.c;
    }

    /* access modifiers changed from: protected */
    public void onVisibilityChanged(View view, int i) {
        ISNAdViewLogic iSNAdViewLogic = this.e;
        if (iSNAdViewLogic != null) {
            iSNAdViewLogic.a("isVisible", i, isShown());
        }
    }

    /* access modifiers changed from: protected */
    public void onWindowVisibilityChanged(int i) {
        ISNAdViewLogic iSNAdViewLogic = this.e;
        if (iSNAdViewLogic != null) {
            iSNAdViewLogic.a("isWindowVisible", i, isShown());
        }
    }

    public void setControllerDelegate(ISNAdViewDelegate iSNAdViewDelegate) {
        this.e.a(iSNAdViewDelegate);
    }

    /* access modifiers changed from: private */
    public void b(final String str) {
        this.f4740a = new WebView(this.b);
        this.f4740a.getSettings().setJavaScriptEnabled(true);
        this.f4740a.addJavascriptInterface(new ISNAdViewWebViewJSInterface(this), "containerMsgHandler");
        this.f4740a.setWebViewClient(new ISNAdViewWebClient(new IErrorReportDelegate() {
            public void a(String str) {
                ISNAdView.this.e.a(str, str);
            }
        }));
        this.f4740a.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
        this.e.a(this.f4740a);
    }

    public void a(JSONObject jSONObject) throws Exception {
        try {
            try {
                SSAFactory.a(this.b).c(this.e.a(jSONObject, this.d));
            } catch (Exception unused) {
                throw new Exception("ISNAdView | Failed to instantiate IronSourceAdsPublisherAgent");
            }
        } catch (Exception unused2) {
            throw new Exception("ISNAdView | loadAd | Failed to build load parameters");
        }
    }

    public void a() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    ISNAdView.this.e.b();
                    ISNAdView.this.removeView(ISNAdView.this.f4740a);
                    if (ISNAdView.this.f4740a != null) {
                        ISNAdView.this.f4740a.destroy();
                    }
                    Activity unused = ISNAdView.this.b = null;
                    ISAdSize unused2 = ISNAdView.this.c = null;
                    String unused3 = ISNAdView.this.d = null;
                    ISNAdView.this.e.a();
                    ISNAdViewLogic unused4 = ISNAdView.this.e = null;
                } catch (Exception e) {
                    Log.e(ISNAdView.this.f, "performCleanup | could not destroy ISNAdView");
                    e.printStackTrace();
                }
            }
        });
    }

    public void a(final String str, final String str2) {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                if (ISNAdView.this.f4740a == null) {
                    ISNAdView.this.b(str2);
                }
                ISNAdView iSNAdView = ISNAdView.this;
                iSNAdView.addView(iSNAdView.f4740a);
                ISNAdView.this.f4740a.loadUrl(str);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.e.a(str);
    }

    public void a(String str, JSONObject jSONObject, String str2, String str3) {
        try {
            if (str.equalsIgnoreCase("loadWithUrl")) {
                a(jSONObject.getString("urlForWebView"), str3);
            } else {
                this.e.a(str, jSONObject, str2, str3);
            }
        } catch (Exception e2) {
            e2.printStackTrace();
            ISNAdViewLogic iSNAdViewLogic = this.e;
            iSNAdViewLogic.a(str3, "Could not handle message from controller: " + str + " with params: " + jSONObject.toString());
        }
    }
}
