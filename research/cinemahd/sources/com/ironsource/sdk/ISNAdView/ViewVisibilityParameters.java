package com.ironsource.sdk.ISNAdView;

import java.util.HashMap;
import org.json.JSONObject;

class ViewVisibilityParameters {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, Boolean> f4750a = new HashMap<String, Boolean>() {
        {
            boolean z = true;
            put("isVisible", Boolean.valueOf(ViewVisibilityParameters.this.b == 0));
            put("isWindowVisible", Boolean.valueOf(ViewVisibilityParameters.this.c != 0 ? false : z));
            put("isShown", false);
            put("isViewVisible", false);
        }
    };
    /* access modifiers changed from: private */
    public int b = 4;
    /* access modifiers changed from: private */
    public int c = 4;

    ViewVisibilityParameters() {
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i, boolean z) {
        boolean z2 = true;
        if (this.f4750a.containsKey(str)) {
            this.f4750a.put(str, Boolean.valueOf(i == 0));
        }
        this.f4750a.put("isShown", Boolean.valueOf(z));
        if ((!this.f4750a.get("isWindowVisible").booleanValue() && !this.f4750a.get("isVisible").booleanValue()) || !this.f4750a.get("isShown").booleanValue()) {
            z2 = false;
        }
        this.f4750a.put("isViewVisible", Boolean.valueOf(z2));
    }

    /* access modifiers changed from: package-private */
    public JSONObject a() {
        return new JSONObject(this.f4750a);
    }
}
