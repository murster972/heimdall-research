package com.ironsource.sdk.ISNAdView;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import com.facebook.common.util.UriUtil;
import org.json.JSONException;
import org.json.JSONObject;

class ISNAdViewLogic {
    private static Handler h;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public JSONObject f4744a = null;
    /* access modifiers changed from: private */
    public ISNAdViewDelegate b;
    /* access modifiers changed from: private */
    public ViewVisibilityParameters c = new ViewVisibilityParameters();
    private WebView d;
    /* access modifiers changed from: private */
    public String e = ISNAdViewLogic.class.getSimpleName();
    private String[] f = {"handleGetViewVisibility"};
    private final String[] g = {"loadWithUrl", "updateAd", "isExternalAdViewInitiated", "handleGetViewVisibility", "sendMessage"};

    ISNAdViewLogic() {
    }

    private boolean e() {
        return this.f4744a != null;
    }

    private void f() {
        if (this.b != null && this.c != null) {
            a("containerIsVisible", c());
        }
    }

    private boolean g(String str) {
        int i = 0;
        while (true) {
            String[] strArr = this.f;
            if (i >= strArr.length) {
                return false;
            }
            if (strArr[i].equalsIgnoreCase(str)) {
                return true;
            }
            i++;
        }
    }

    private boolean h(String str) {
        if (Build.VERSION.SDK_INT <= 22) {
            return str.equalsIgnoreCase("isWindowVisible");
        }
        return str.equalsIgnoreCase("isVisible");
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        a(str, this.c.a());
    }

    /* access modifiers changed from: private */
    public boolean c(String str) {
        int i = 0;
        boolean z = false;
        while (true) {
            String[] strArr = this.g;
            if (i >= strArr.length || z) {
                return z;
            }
            if (strArr[i].equalsIgnoreCase(str)) {
                z = true;
            }
            i++;
        }
        return z;
    }

    private Handler d() {
        try {
            if (h == null) {
                h = new Handler(Looper.getMainLooper());
            }
        } catch (Exception e2) {
            Log.e(this.e, "Error while trying execute method getUIThreadHandler");
            e2.printStackTrace();
        }
        return h;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        if (this.b != null && this.c != null) {
            a("containerWasRemoved", c());
        }
    }

    /* access modifiers changed from: private */
    public void f(String str) {
        try {
            boolean z = (this.d == null || this.d.getUrl() == null) ? false : true;
            JSONObject jSONObject = new JSONObject();
            jSONObject.put("isExternalAdViewInitiated", z);
            a(str, jSONObject);
        } catch (Exception e2) {
            Log.e(this.e, "Error while trying execute method sendIsExternalAdViewInitiated");
            e2.printStackTrace();
        }
    }

    private JSONObject c() {
        return new JSONObject() {
            {
                try {
                    put("configs", ISNAdViewLogic.this.a(ISNAdViewLogic.this.f4744a, ISNAdViewLogic.this.c.a()));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };
    }

    /* access modifiers changed from: package-private */
    public void a(ISNAdViewDelegate iSNAdViewDelegate) {
        this.b = iSNAdViewDelegate;
    }

    private String b(String str) {
        return String.format("window.ssa.onMessageReceived(%1$s)", new Object[]{str});
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView) {
        this.d = webView;
    }

    private void b(JSONObject jSONObject) {
        a(a(jSONObject).toString(), (String) null, (String) null);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.f4744a = null;
        this.b = null;
        this.c = null;
        h = null;
    }

    /* access modifiers changed from: private */
    public void d(String str) {
        try {
            String str2 = "javascript:try{" + str + "}catch(e){console.log(\"JS exception: \" + JSON.stringify(e));}";
            if (Build.VERSION.SDK_INT >= 19) {
                this.d.evaluateJavascript(str2, (ValueCallback) null);
            } else {
                this.d.loadUrl(str2);
            }
        } catch (Throwable th) {
            Log.e(this.e, "injectJavaScriptIntoWebView | Error while trying inject JS into external adunit: " + str + "Android API level: " + Build.VERSION.SDK_INT);
            th.printStackTrace();
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(JSONObject jSONObject, String str) throws Exception {
        try {
            boolean e2 = e();
            if (this.f4744a == null) {
                this.f4744a = new JSONObject(jSONObject.toString());
            }
            this.f4744a.put("externalAdViewId", str);
            this.f4744a.put("isInReload", e2);
            return this.f4744a;
        } catch (Exception unused) {
            throw new Exception("ISNAdViewLogic | buildDataForLoadingAd | Could not build load parameters");
        }
    }

    private void a(String str, JSONObject jSONObject) {
        ISNAdViewDelegate iSNAdViewDelegate = this.b;
        if (iSNAdViewDelegate != null) {
            iSNAdViewDelegate.a(str, jSONObject);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        ISNAdViewDelegate iSNAdViewDelegate = this.b;
        if (iSNAdViewDelegate != null) {
            iSNAdViewDelegate.a(str, str2);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject, String str2, String str3) {
        final String str4 = str;
        final String str5 = str3;
        final String str6 = str2;
        final JSONObject jSONObject2 = jSONObject;
        d().post(new Runnable() {
            public void run() {
                try {
                    if (!ISNAdViewLogic.this.c(str4)) {
                        String str = "ISNAdViewLogic | handleMessageFromController | cannot handle command: " + str4;
                        Log.e(ISNAdViewLogic.this.e, str);
                        ISNAdViewLogic.this.b.a(str5, str);
                    } else if (str4.equalsIgnoreCase("isExternalAdViewInitiated")) {
                        ISNAdViewLogic.this.f(str6);
                    } else if (str4.equalsIgnoreCase("handleGetViewVisibility")) {
                        ISNAdViewLogic.this.e(str6);
                    } else {
                        if (!str4.equalsIgnoreCase("sendMessage")) {
                            if (!str4.equalsIgnoreCase("updateAd")) {
                                String str2 = "ISNAdViewLogic | handleMessageFromController | unhandled API request " + str4 + " " + jSONObject2.toString();
                                Log.e(ISNAdViewLogic.this.e, str2);
                                ISNAdViewLogic.this.b.a(str5, str2);
                                return;
                            }
                        }
                        ISNAdViewLogic.this.a(jSONObject2.getString("params"), str6, str5);
                    }
                } catch (Exception e2) {
                    e2.printStackTrace();
                    String str3 = "ISNAdViewLogic | handleMessageFromController | Error while trying handle message: " + str4;
                    Log.e(ISNAdViewLogic.this.e, str3);
                    ISNAdViewLogic.this.b.a(str5, str3);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(String str, int i, boolean z) {
        this.c.a(str, i, z);
        if (h(str)) {
            f();
        }
    }

    /* access modifiers changed from: private */
    public JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            JSONObject jSONObject3 = new JSONObject(jSONObject.toString());
            jSONObject3.put("visibilityParams", jSONObject2);
            return jSONObject3;
        } catch (JSONException e2) {
            e2.printStackTrace();
            return jSONObject;
        }
    }

    /* access modifiers changed from: private */
    public void a(String str, String str2, String str3) {
        if (this.d == null) {
            String str4 = "No external adunit attached to ISNAdView while trying to send message: " + str;
            Log.e(this.e, str4);
            this.b.a(str3, str4);
            return;
        }
        try {
            new JSONObject(str);
        } catch (JSONException unused) {
            str = "\"" + str + "\"";
        }
        final String b2 = b(str);
        d().post(new Runnable() {
            public void run() {
                ISNAdViewLogic.this.d(b2);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            String optString = jSONObject.optString("method");
            if (TextUtils.isEmpty(optString) || !g(optString)) {
                a("containerSendMessage", jSONObject);
            } else if (optString.equalsIgnoreCase("handleGetViewVisibility")) {
                b(jSONObject);
            }
        } catch (JSONException e2) {
            String str2 = this.e;
            Log.e(str2, "ISNAdViewLogic | receiveMessageFromExternal | Error while trying handle message: " + str);
            e2.printStackTrace();
        }
    }

    private JSONObject a(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        try {
            jSONObject2.put("id", jSONObject.getString("id"));
            jSONObject2.put(UriUtil.DATA_SCHEME, this.c.a());
        } catch (Exception e2) {
            String str = this.e;
            Log.e(str, "Error while trying execute method buildVisibilityMessageForAdunit | params: " + jSONObject);
            e2.printStackTrace();
        }
        return jSONObject2;
    }
}
