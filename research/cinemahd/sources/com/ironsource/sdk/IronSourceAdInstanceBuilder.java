package com.ironsource.sdk;

import com.ironsource.sdk.listeners.OnInterstitialListener;
import com.ironsource.sdk.utils.SDKUtils;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class IronSourceAdInstanceBuilder {

    /* renamed from: a  reason: collision with root package name */
    private String f4752a;
    private boolean b = false;
    private boolean c = false;
    private Map<String, String> d;
    private OnInterstitialListener e;

    public IronSourceAdInstanceBuilder(String str, OnInterstitialListener onInterstitialListener) throws NullPointerException {
        SDKUtils.b(str, "Instance name can't be null");
        this.f4752a = str;
        SDKUtils.a(onInterstitialListener, "InterstitialListener name can't be null");
        this.e = onInterstitialListener;
    }

    public IronSourceAdInstanceBuilder a(Map<String, String> map) {
        this.d = map;
        return this;
    }

    public IronSourceAdInstanceBuilder b() {
        this.c = true;
        return this;
    }

    public IronSourceAdInstanceBuilder c() {
        this.b = true;
        return this;
    }

    public IronSourceAdInstance a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put(MediationMetaData.KEY_NAME, this.f4752a);
            jSONObject.put("rewarded", this.b);
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        return new IronSourceAdInstance(IronSourceNetworkAPIUtils.a(jSONObject), this.f4752a, this.b, this.c, this.d, this.e);
    }
}
