package com.roughike.bottombar;

import android.content.Context;
import android.content.res.XmlResourceParser;
import android.graphics.Color;
import androidx.core.content.ContextCompat;
import com.roughike.bottombar.BottomBarTab;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.xmlpull.v1.XmlPullParserException;

class TabParser {
    private final Context context;
    private final BottomBarTab.Config defaultTabConfig;
    private final XmlResourceParser parser;
    private ArrayList<BottomBarTab> tabs = new ArrayList<>();
    private BottomBarTab workingTab;

    private class TabParserException extends RuntimeException {
        private TabParserException() {
        }
    }

    TabParser(Context context2, BottomBarTab.Config config, int i) {
        this.context = context2;
        this.defaultTabConfig = config;
        this.parser = context2.getResources().getXml(i);
        parse();
    }

    private Integer getColorValue(int i, XmlResourceParser xmlResourceParser) {
        int attributeResourceValue = xmlResourceParser.getAttributeResourceValue(i, 0);
        if (attributeResourceValue != 0) {
            return Integer.valueOf(ContextCompat.a(this.context, attributeResourceValue));
        }
        try {
            return Integer.valueOf(Color.parseColor(xmlResourceParser.getAttributeValue(i)));
        } catch (Exception unused) {
            return null;
        }
    }

    private String getTitleValue(int i, XmlResourceParser xmlResourceParser) {
        int attributeResourceValue = xmlResourceParser.getAttributeResourceValue(i, 0);
        if (attributeResourceValue != 0) {
            return this.context.getString(attributeResourceValue);
        }
        return xmlResourceParser.getAttributeValue(i);
    }

    private void parse() {
        try {
            this.parser.next();
            int eventType = this.parser.getEventType();
            while (eventType != 1) {
                if (eventType == 2) {
                    parseNewTab(this.parser);
                } else if (eventType == 3 && this.parser.getName().equals("tab") && this.workingTab != null) {
                    this.tabs.add(this.workingTab);
                    this.workingTab = null;
                }
                eventType = this.parser.next();
            }
        } catch (IOException | XmlPullParserException e) {
            e.printStackTrace();
            throw new TabParserException();
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void parseNewTab(android.content.res.XmlResourceParser r6) {
        /*
            r5 = this;
            com.roughike.bottombar.BottomBarTab r0 = r5.workingTab
            if (r0 != 0) goto L_0x000a
            com.roughike.bottombar.BottomBarTab r0 = r5.tabWithDefaults()
            r5.workingTab = r0
        L_0x000a:
            com.roughike.bottombar.BottomBarTab r0 = r5.workingTab
            java.util.ArrayList<com.roughike.bottombar.BottomBarTab> r1 = r5.tabs
            int r1 = r1.size()
            r0.setIndexInContainer(r1)
            r0 = 0
            r1 = 0
        L_0x0017:
            int r2 = r6.getAttributeCount()
            if (r1 >= r2) goto L_0x00d6
            java.lang.String r2 = r6.getAttributeName(r1)
            r3 = -1
            int r4 = r2.hashCode()
            switch(r4) {
                case -1765033179: goto L_0x0066;
                case -1077332995: goto L_0x005c;
                case -424740686: goto L_0x0052;
                case 3355: goto L_0x0048;
                case 3226745: goto L_0x003e;
                case 110371416: goto L_0x0034;
                case 1162188184: goto L_0x002a;
                default: goto L_0x0029;
            }
        L_0x0029:
            goto L_0x0070
        L_0x002a:
            java.lang.String r4 = "inActiveColor"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 3
            goto L_0x0071
        L_0x0034:
            java.lang.String r4 = "title"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 2
            goto L_0x0071
        L_0x003e:
            java.lang.String r4 = "icon"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 1
            goto L_0x0071
        L_0x0048:
            java.lang.String r4 = "id"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 0
            goto L_0x0071
        L_0x0052:
            java.lang.String r4 = "badgeBackgroundColor"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 6
            goto L_0x0071
        L_0x005c:
            java.lang.String r4 = "activeColor"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 4
            goto L_0x0071
        L_0x0066:
            java.lang.String r4 = "barColorWhenSelected"
            boolean r2 = r2.equals(r4)
            if (r2 == 0) goto L_0x0070
            r2 = 5
            goto L_0x0071
        L_0x0070:
            r2 = -1
        L_0x0071:
            switch(r2) {
                case 0: goto L_0x00c9;
                case 1: goto L_0x00bf;
                case 2: goto L_0x00b5;
                case 3: goto L_0x00a5;
                case 4: goto L_0x0095;
                case 5: goto L_0x0085;
                case 6: goto L_0x0075;
                default: goto L_0x0074;
            }
        L_0x0074:
            goto L_0x00d2
        L_0x0075:
            java.lang.Integer r2 = r5.getColorValue(r1, r6)
            if (r2 == 0) goto L_0x00d2
            com.roughike.bottombar.BottomBarTab r3 = r5.workingTab
            int r2 = r2.intValue()
            r3.setBadgeBackgroundColor(r2)
            goto L_0x00d2
        L_0x0085:
            java.lang.Integer r2 = r5.getColorValue(r1, r6)
            if (r2 == 0) goto L_0x00d2
            com.roughike.bottombar.BottomBarTab r3 = r5.workingTab
            int r2 = r2.intValue()
            r3.setBarColorWhenSelected(r2)
            goto L_0x00d2
        L_0x0095:
            java.lang.Integer r2 = r5.getColorValue(r1, r6)
            if (r2 == 0) goto L_0x00d2
            com.roughike.bottombar.BottomBarTab r3 = r5.workingTab
            int r2 = r2.intValue()
            r3.setActiveColor(r2)
            goto L_0x00d2
        L_0x00a5:
            java.lang.Integer r2 = r5.getColorValue(r1, r6)
            if (r2 == 0) goto L_0x00d2
            com.roughike.bottombar.BottomBarTab r3 = r5.workingTab
            int r2 = r2.intValue()
            r3.setInActiveColor(r2)
            goto L_0x00d2
        L_0x00b5:
            com.roughike.bottombar.BottomBarTab r2 = r5.workingTab
            java.lang.String r3 = r5.getTitleValue(r1, r6)
            r2.setTitle(r3)
            goto L_0x00d2
        L_0x00bf:
            com.roughike.bottombar.BottomBarTab r2 = r5.workingTab
            int r3 = r6.getAttributeResourceValue(r1, r0)
            r2.setIconResId(r3)
            goto L_0x00d2
        L_0x00c9:
            com.roughike.bottombar.BottomBarTab r2 = r5.workingTab
            int r3 = r6.getIdAttributeResourceValue(r1)
            r2.setId(r3)
        L_0x00d2:
            int r1 = r1 + 1
            goto L_0x0017
        L_0x00d6:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.roughike.bottombar.TabParser.parseNewTab(android.content.res.XmlResourceParser):void");
    }

    private BottomBarTab tabWithDefaults() {
        BottomBarTab bottomBarTab = new BottomBarTab(this.context);
        bottomBarTab.setConfig(this.defaultTabConfig);
        return bottomBarTab;
    }

    /* access modifiers changed from: package-private */
    public List<BottomBarTab> getTabs() {
        return this.tabs;
    }
}
