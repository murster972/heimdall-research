package com.roughike.bottombar;

import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.interpolator.view.animation.LinearOutSlowInInterpolator;
import com.google.android.material.snackbar.Snackbar;

class BottomNavigationBehavior<V extends View> extends VerticalScrollingBehavior<V> {
    private static final Interpolator INTERPOLATOR = new LinearOutSlowInInterpolator();
    /* access modifiers changed from: private */
    public final int bottomNavHeight;
    /* access modifiers changed from: private */
    public final int defaultOffset;
    private boolean hidden = false;
    /* access modifiers changed from: private */
    public boolean isTablet = false;
    private boolean mScrollingEnabled;
    /* access modifiers changed from: private */
    public int mSnackbarHeight = -1;
    private ViewPropertyAnimatorCompat mTranslationAnimator;
    private final BottomNavigationWithSnackbar mWithSnackBarImpl;

    private interface BottomNavigationWithSnackbar {
        void updateSnackbar(CoordinatorLayout coordinatorLayout, View view, View view2);
    }

    private class LollipopBottomNavWithSnackBarImpl implements BottomNavigationWithSnackbar {
        private LollipopBottomNavWithSnackBarImpl() {
        }

        public void updateSnackbar(CoordinatorLayout coordinatorLayout, View view, View view2) {
            if (!BottomNavigationBehavior.this.isTablet && (view instanceof Snackbar.SnackbarLayout)) {
                if (BottomNavigationBehavior.this.mSnackbarHeight == -1) {
                    int unused = BottomNavigationBehavior.this.mSnackbarHeight = view.getHeight();
                }
                if (ViewCompat.t(view2) == 0.0f) {
                    view.setPadding(view.getPaddingLeft(), view.getPaddingTop(), view.getPaddingRight(), (BottomNavigationBehavior.this.mSnackbarHeight + BottomNavigationBehavior.this.bottomNavHeight) - BottomNavigationBehavior.this.defaultOffset);
                }
            }
        }
    }

    private class PreLollipopBottomNavWithSnackBarImpl implements BottomNavigationWithSnackbar {
        private PreLollipopBottomNavWithSnackBarImpl() {
        }

        public void updateSnackbar(CoordinatorLayout coordinatorLayout, View view, View view2) {
            if (!BottomNavigationBehavior.this.isTablet && (view instanceof Snackbar.SnackbarLayout)) {
                if (BottomNavigationBehavior.this.mSnackbarHeight == -1) {
                    int unused = BottomNavigationBehavior.this.mSnackbarHeight = view.getHeight();
                }
                if (ViewCompat.t(view2) == 0.0f) {
                    ((ViewGroup.MarginLayoutParams) view.getLayoutParams()).bottomMargin = (BottomNavigationBehavior.this.bottomNavHeight + BottomNavigationBehavior.this.mSnackbarHeight) - BottomNavigationBehavior.this.defaultOffset;
                    view2.bringToFront();
                    view2.getParent().requestLayout();
                    if (Build.VERSION.SDK_INT < 19) {
                        ((View) view2.getParent()).invalidate();
                    }
                }
            }
        }
    }

    BottomNavigationBehavior(int i, int i2, boolean z) {
        this.mWithSnackBarImpl = Build.VERSION.SDK_INT >= 21 ? new LollipopBottomNavWithSnackBarImpl() : new PreLollipopBottomNavWithSnackBarImpl();
        this.mScrollingEnabled = true;
        this.bottomNavHeight = i;
        this.defaultOffset = i2;
        this.isTablet = z;
    }

    private void animateOffset(V v, int i) {
        ensureOrCancelAnimator(v);
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.mTranslationAnimator;
        viewPropertyAnimatorCompat.d((float) i);
        viewPropertyAnimatorCompat.c();
    }

    private void ensureOrCancelAnimator(V v) {
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = this.mTranslationAnimator;
        if (viewPropertyAnimatorCompat == null) {
            this.mTranslationAnimator = ViewCompat.a(v);
            this.mTranslationAnimator.a(300);
            this.mTranslationAnimator.a(INTERPOLATOR);
            return;
        }
        viewPropertyAnimatorCompat.a();
    }

    static <V extends View> BottomNavigationBehavior<V> from(V v) {
        ViewGroup.LayoutParams layoutParams = v.getLayoutParams();
        if (layoutParams instanceof CoordinatorLayout.LayoutParams) {
            CoordinatorLayout.Behavior d = ((CoordinatorLayout.LayoutParams) layoutParams).d();
            if (d instanceof BottomNavigationBehavior) {
                return (BottomNavigationBehavior) d;
            }
            throw new IllegalArgumentException("The view is not associated with BottomNavigationBehavior");
        }
        throw new IllegalArgumentException("The view is not a child of CoordinatorLayout");
    }

    private void handleDirection(V v, int i) {
        if (this.mScrollingEnabled) {
            if (i == -1 && this.hidden) {
                this.hidden = false;
                animateOffset(v, this.defaultOffset);
            } else if (i == 1 && !this.hidden) {
                this.hidden = true;
                animateOffset(v, this.bottomNavHeight + this.defaultOffset);
            }
        }
    }

    private void updateScrollingForSnackbar(View view, boolean z) {
        if (!this.isTablet && (view instanceof Snackbar.SnackbarLayout)) {
            this.mScrollingEnabled = z;
        }
    }

    public boolean layoutDependsOn(CoordinatorLayout coordinatorLayout, V v, View view) {
        this.mWithSnackBarImpl.updateSnackbar(coordinatorLayout, view, v);
        return view instanceof Snackbar.SnackbarLayout;
    }

    public boolean onDependentViewChanged(CoordinatorLayout coordinatorLayout, V v, View view) {
        updateScrollingForSnackbar(view, false);
        return super.onDependentViewChanged(coordinatorLayout, v, view);
    }

    public void onDependentViewRemoved(CoordinatorLayout coordinatorLayout, V v, View view) {
        updateScrollingForSnackbar(view, true);
        super.onDependentViewRemoved(coordinatorLayout, v, view);
    }

    public void onDirectionNestedPreScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr, int i3) {
        handleDirection(v, i3);
    }

    /* access modifiers changed from: protected */
    public boolean onNestedDirectionFling(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2, int i) {
        handleDirection(v, i);
        return true;
    }

    public void onNestedVerticalOverScroll(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3) {
    }

    /* access modifiers changed from: package-private */
    public void setHidden(V v, boolean z) {
        if (!z && this.hidden) {
            animateOffset(v, this.defaultOffset);
        } else if (z && !this.hidden) {
            animateOffset(v, this.bottomNavHeight + this.defaultOffset);
        }
        this.hidden = z;
    }
}
