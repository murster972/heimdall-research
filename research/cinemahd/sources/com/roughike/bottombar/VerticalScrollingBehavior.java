package com.roughike.bottombar;

import android.content.Context;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.WindowInsetsCompat;

abstract class VerticalScrollingBehavior<V extends View> extends CoordinatorLayout.Behavior<V> {
    private int overScrollDirection = 0;
    private int scrollDirection = 0;
    private int totalDy = 0;
    private int totalDyUnconsumed = 0;

    VerticalScrollingBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    /* access modifiers changed from: package-private */
    public int getOverScrollDirection() {
        return this.overScrollDirection;
    }

    /* access modifiers changed from: package-private */
    public int getScrollDirection() {
        return this.scrollDirection;
    }

    public WindowInsetsCompat onApplyWindowInsets(CoordinatorLayout coordinatorLayout, V v, WindowInsetsCompat windowInsetsCompat) {
        return super.onApplyWindowInsets(coordinatorLayout, v, windowInsetsCompat);
    }

    /* access modifiers changed from: package-private */
    public abstract void onDirectionNestedPreScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr, int i3);

    /* access modifiers changed from: package-private */
    public abstract boolean onNestedDirectionFling(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2, int i);

    public boolean onNestedFling(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2, boolean z) {
        super.onNestedFling(coordinatorLayout, v, view, f, f2, z);
        this.scrollDirection = f2 > 0.0f ? 1 : -1;
        return onNestedDirectionFling(coordinatorLayout, v, view, f, f2, this.scrollDirection);
    }

    public boolean onNestedPreFling(CoordinatorLayout coordinatorLayout, V v, View view, float f, float f2) {
        return super.onNestedPreFling(coordinatorLayout, v, view, f, f2);
    }

    public void onNestedPreScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int[] iArr) {
        super.onNestedPreScroll(coordinatorLayout, v, view, i, i2, iArr);
        if (i2 > 0 && this.totalDy < 0) {
            this.totalDy = 0;
            this.scrollDirection = 1;
        } else if (i2 < 0 && this.totalDy > 0) {
            this.totalDy = 0;
            this.scrollDirection = -1;
        }
        this.totalDy += i2;
        onDirectionNestedPreScroll(coordinatorLayout, v, view, i, i2, iArr, this.scrollDirection);
    }

    public void onNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, int i, int i2, int i3, int i4) {
        super.onNestedScroll(coordinatorLayout, v, view, i, i2, i3, i4);
        if (i4 > 0 && this.totalDyUnconsumed < 0) {
            this.totalDyUnconsumed = 0;
            this.overScrollDirection = 1;
        } else if (i4 < 0 && this.totalDyUnconsumed > 0) {
            this.totalDyUnconsumed = 0;
            this.overScrollDirection = -1;
        }
        this.totalDyUnconsumed += i4;
        onNestedVerticalOverScroll(coordinatorLayout, v, this.overScrollDirection, i2, this.totalDyUnconsumed);
    }

    public void onNestedScrollAccepted(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        super.onNestedScrollAccepted(coordinatorLayout, v, view, view2, i);
    }

    /* access modifiers changed from: package-private */
    public abstract void onNestedVerticalOverScroll(CoordinatorLayout coordinatorLayout, V v, int i, int i2, int i3);

    public Parcelable onSaveInstanceState(CoordinatorLayout coordinatorLayout, V v) {
        return super.onSaveInstanceState(coordinatorLayout, v);
    }

    public boolean onStartNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view, View view2, int i) {
        return (i & 2) != 0;
    }

    public void onStopNestedScroll(CoordinatorLayout coordinatorLayout, V v, View view) {
        super.onStopNestedScroll(coordinatorLayout, v, view);
    }

    VerticalScrollingBehavior() {
    }
}
