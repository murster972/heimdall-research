package com.roughike.bottombar;

import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;

public class BottomBarTab extends LinearLayout {
    private static final float ACTIVE_TITLE_SCALE = 1.0f;
    private static final long ANIMATION_DURATION = 150;
    private static final float INACTIVE_FIXED_TITLE_SCALE = 0.86f;
    private float activeAlpha;
    private int activeColor;
    BottomBarBadge badge;
    private int badgeBackgroundColor;
    private int barColorWhenSelected;
    private final int eightDps;
    private int iconResId;
    /* access modifiers changed from: private */
    public AppCompatImageView iconView;
    private float inActiveAlpha;
    private int inActiveColor;
    private int indexInContainer;
    /* access modifiers changed from: private */
    public boolean isActive;
    private final int sixDps;
    private final int sixteenDps;
    private String title;
    private int titleTextAppearanceResId;
    private Typeface titleTypeFace;
    private TextView titleView;
    private Type type = Type.FIXED;

    /* renamed from: com.roughike.bottombar.BottomBarTab$5  reason: invalid class name */
    static /* synthetic */ class AnonymousClass5 {
        static final /* synthetic */ int[] $SwitchMap$com$roughike$bottombar$BottomBarTab$Type = new int[Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.roughike.bottombar.BottomBarTab$Type[] r0 = com.roughike.bottombar.BottomBarTab.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                $SwitchMap$com$roughike$bottombar$BottomBarTab$Type = r0
                int[] r0 = $SwitchMap$com$roughike$bottombar$BottomBarTab$Type     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.roughike.bottombar.BottomBarTab$Type r1 = com.roughike.bottombar.BottomBarTab.Type.FIXED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = $SwitchMap$com$roughike$bottombar$BottomBarTab$Type     // Catch:{ NoSuchFieldError -> 0x001f }
                com.roughike.bottombar.BottomBarTab$Type r1 = com.roughike.bottombar.BottomBarTab.Type.SHIFTING     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = $SwitchMap$com$roughike$bottombar$BottomBarTab$Type     // Catch:{ NoSuchFieldError -> 0x002a }
                com.roughike.bottombar.BottomBarTab$Type r1 = com.roughike.bottombar.BottomBarTab.Type.TABLET     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.roughike.bottombar.BottomBarTab.AnonymousClass5.<clinit>():void");
        }
    }

    public static class Config {
        /* access modifiers changed from: private */
        public final float activeTabAlpha;
        /* access modifiers changed from: private */
        public final int activeTabColor;
        /* access modifiers changed from: private */
        public final int badgeBackgroundColor;
        /* access modifiers changed from: private */
        public final int barColorWhenSelected;
        /* access modifiers changed from: private */
        public final float inActiveTabAlpha;
        /* access modifiers changed from: private */
        public final int inActiveTabColor;
        /* access modifiers changed from: private */
        public final int titleTextAppearance;
        /* access modifiers changed from: private */
        public final Typeface titleTypeFace;

        public static class Builder {
            /* access modifiers changed from: private */
            public float activeTabAlpha;
            /* access modifiers changed from: private */
            public int activeTabColor;
            /* access modifiers changed from: private */
            public int badgeBackgroundColor;
            /* access modifiers changed from: private */
            public int barColorWhenSelected;
            /* access modifiers changed from: private */
            public float inActiveTabAlpha;
            /* access modifiers changed from: private */
            public int inActiveTabColor;
            /* access modifiers changed from: private */
            public int titleTextAppearance;
            /* access modifiers changed from: private */
            public Typeface titleTypeFace;

            public Builder activeTabAlpha(float f) {
                this.activeTabAlpha = f;
                return this;
            }

            public Builder activeTabColor(int i) {
                this.activeTabColor = i;
                return this;
            }

            public Builder badgeBackgroundColor(int i) {
                this.badgeBackgroundColor = i;
                return this;
            }

            public Builder barColorWhenSelected(int i) {
                this.barColorWhenSelected = i;
                return this;
            }

            public Config build() {
                return new Config(this);
            }

            public Builder inActiveTabAlpha(float f) {
                this.inActiveTabAlpha = f;
                return this;
            }

            public Builder inActiveTabColor(int i) {
                this.inActiveTabColor = i;
                return this;
            }

            public Builder titleTextAppearance(int i) {
                this.titleTextAppearance = i;
                return this;
            }

            public Builder titleTypeFace(Typeface typeface) {
                this.titleTypeFace = typeface;
                return this;
            }
        }

        private Config(Builder builder) {
            this.inActiveTabAlpha = builder.inActiveTabAlpha;
            this.activeTabAlpha = builder.activeTabAlpha;
            this.inActiveTabColor = builder.inActiveTabColor;
            this.activeTabColor = builder.activeTabColor;
            this.barColorWhenSelected = builder.barColorWhenSelected;
            this.badgeBackgroundColor = builder.badgeBackgroundColor;
            this.titleTextAppearance = builder.titleTextAppearance;
            this.titleTypeFace = builder.titleTypeFace;
        }
    }

    enum Type {
        FIXED,
        SHIFTING,
        TABLET
    }

    BottomBarTab(Context context) {
        super(context);
        this.sixDps = MiscUtils.dpToPixel(context, 6.0f);
        this.eightDps = MiscUtils.dpToPixel(context, 8.0f);
        this.sixteenDps = MiscUtils.dpToPixel(context, 16.0f);
    }

    private void animateColors(int i, int i2) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setIntValues(new int[]{i, i2});
        valueAnimator.setEvaluator(new ArgbEvaluator());
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                BottomBarTab.this.setColors(((Integer) valueAnimator.getAnimatedValue()).intValue());
            }
        });
        valueAnimator.setDuration(ANIMATION_DURATION);
        valueAnimator.start();
    }

    private void animateIcon(float f) {
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.iconView);
        a2.a((long) ANIMATION_DURATION);
        a2.a(f);
        a2.c();
    }

    private void animateTitle(float f, float f2) {
        if (this.type != Type.TABLET) {
            ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.titleView);
            a2.a((long) ANIMATION_DURATION);
            a2.b(f);
            a2.c(f);
            a2.a(f2);
            a2.c();
        }
    }

    private void setAlphas(float f) {
        AppCompatImageView appCompatImageView = this.iconView;
        if (appCompatImageView != null) {
            ViewCompat.a((View) appCompatImageView, f);
        }
        TextView textView = this.titleView;
        if (textView != null) {
            ViewCompat.a((View) textView, f);
        }
    }

    /* access modifiers changed from: private */
    public void setColors(int i) {
        AppCompatImageView appCompatImageView = this.iconView;
        if (appCompatImageView != null) {
            appCompatImageView.setColorFilter(i);
            this.iconView.setTag(Integer.valueOf(i));
        }
        TextView textView = this.titleView;
        if (textView != null) {
            textView.setTextColor(i);
        }
    }

    private void setTitleScale(float f) {
        if (this.type != Type.TABLET) {
            ViewCompat.c((View) this.titleView, f);
            ViewCompat.d((View) this.titleView, f);
        }
    }

    private void setTopPadding(int i) {
        if (this.type != Type.TABLET) {
            AppCompatImageView appCompatImageView = this.iconView;
            appCompatImageView.setPadding(appCompatImageView.getPaddingLeft(), i, this.iconView.getPaddingRight(), this.iconView.getPaddingBottom());
        }
    }

    private void setTopPaddingAnimated(int i, int i2) {
        if (this.type != Type.TABLET) {
            ValueAnimator ofInt = ValueAnimator.ofInt(new int[]{i, i2});
            ofInt.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    BottomBarTab.this.iconView.setPadding(BottomBarTab.this.iconView.getPaddingLeft(), ((Integer) valueAnimator.getAnimatedValue()).intValue(), BottomBarTab.this.iconView.getPaddingRight(), BottomBarTab.this.iconView.getPaddingBottom());
                }
            });
            ofInt.setDuration(ANIMATION_DURATION);
            ofInt.start();
        }
    }

    private void updateBadgePosition() {
        BottomBarBadge bottomBarBadge = this.badge;
        if (bottomBarBadge != null) {
            bottomBarBadge.adjustPositionAndSize(this);
        }
    }

    private void updateCustomTextAppearance() {
        int i;
        TextView textView = this.titleView;
        if (textView != null && (i = this.titleTextAppearanceResId) != 0) {
            if (Build.VERSION.SDK_INT >= 23) {
                textView.setTextAppearance(i);
            } else {
                textView.setTextAppearance(getContext(), this.titleTextAppearanceResId);
            }
            this.titleView.setTag(Integer.valueOf(this.titleTextAppearanceResId));
        }
    }

    private void updateCustomTypeface() {
        TextView textView;
        Typeface typeface = this.titleTypeFace;
        if (typeface != null && (textView = this.titleView) != null) {
            textView.setTypeface(typeface);
        }
    }

    /* access modifiers changed from: package-private */
    public void deselect(boolean z) {
        BottomBarBadge bottomBarBadge;
        boolean z2 = false;
        this.isActive = false;
        if (this.type == Type.SHIFTING) {
            z2 = true;
        }
        float f = z2 ? 0.0f : INACTIVE_FIXED_TITLE_SCALE;
        int i = z2 ? this.sixteenDps : this.eightDps;
        if (z) {
            setTopPaddingAnimated(this.iconView.getPaddingTop(), i);
            animateTitle(f, this.inActiveAlpha);
            animateIcon(this.inActiveAlpha);
            animateColors(this.activeColor, this.inActiveColor);
        } else {
            setTitleScale(f);
            setTopPadding(i);
            setColors(this.inActiveColor);
            setAlphas(this.inActiveAlpha);
        }
        if (!z2 && (bottomBarBadge = this.badge) != null) {
            bottomBarBadge.show();
        }
    }

    /* access modifiers changed from: package-private */
    public float getActiveAlpha() {
        return this.activeAlpha;
    }

    /* access modifiers changed from: package-private */
    public int getActiveColor() {
        return this.activeColor;
    }

    /* access modifiers changed from: package-private */
    public int getBadgeBackgroundColor() {
        return this.badgeBackgroundColor;
    }

    /* access modifiers changed from: package-private */
    public int getBarColorWhenSelected() {
        return this.barColorWhenSelected;
    }

    /* access modifiers changed from: package-private */
    public int getCurrentDisplayedIconColor() {
        if (this.iconView.getTag() instanceof Integer) {
            return ((Integer) this.iconView.getTag()).intValue();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getCurrentDisplayedTextAppearance() {
        Object tag = this.titleView.getTag();
        TextView textView = this.titleView;
        if (textView == null || !(tag instanceof Integer)) {
            return 0;
        }
        return ((Integer) textView.getTag()).intValue();
    }

    /* access modifiers changed from: package-private */
    public int getCurrentDisplayedTitleColor() {
        TextView textView = this.titleView;
        if (textView != null) {
            return textView.getCurrentTextColor();
        }
        return 0;
    }

    /* access modifiers changed from: package-private */
    public int getIconResId() {
        return this.iconResId;
    }

    /* access modifiers changed from: package-private */
    public AppCompatImageView getIconView() {
        return this.iconView;
    }

    /* access modifiers changed from: package-private */
    public float getInActiveAlpha() {
        return this.inActiveAlpha;
    }

    /* access modifiers changed from: package-private */
    public int getInActiveColor() {
        return this.inActiveColor;
    }

    /* access modifiers changed from: package-private */
    public int getIndexInTabContainer() {
        return this.indexInContainer;
    }

    /* access modifiers changed from: package-private */
    public int getLayoutResource() {
        int i = AnonymousClass5.$SwitchMap$com$roughike$bottombar$BottomBarTab$Type[this.type.ordinal()];
        if (i == 1) {
            return R.layout.bb_bottom_bar_item_fixed;
        }
        if (i == 2) {
            return R.layout.bb_bottom_bar_item_shifting;
        }
        if (i == 3) {
            return R.layout.bb_bottom_bar_item_fixed_tablet;
        }
        throw new RuntimeException("Unknown BottomBarTab type.");
    }

    public ViewGroup getOuterView() {
        return (ViewGroup) getParent();
    }

    /* access modifiers changed from: package-private */
    public String getTitle() {
        return this.title;
    }

    public int getTitleTextAppearance() {
        return this.titleTextAppearanceResId;
    }

    /* access modifiers changed from: package-private */
    public Typeface getTitleTypeFace() {
        return this.titleTypeFace;
    }

    /* access modifiers changed from: package-private */
    public TextView getTitleView() {
        return this.titleView;
    }

    /* access modifiers changed from: package-private */
    public Type getType() {
        return this.type;
    }

    /* access modifiers changed from: package-private */
    public boolean hasActiveBadge() {
        return this.badge != null;
    }

    /* access modifiers changed from: package-private */
    public boolean isActive() {
        return this.isActive;
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        BottomBarBadge bottomBarBadge = this.badge;
        if (bottomBarBadge != null && (parcelable instanceof Bundle)) {
            Bundle bundle = (Bundle) parcelable;
            bottomBarBadge.restoreState(bundle, this.indexInContainer);
            parcelable = bundle.getParcelable("superstate");
        }
        super.onRestoreInstanceState(parcelable);
    }

    public Parcelable onSaveInstanceState() {
        BottomBarBadge bottomBarBadge = this.badge;
        if (bottomBarBadge == null) {
            return super.onSaveInstanceState();
        }
        Bundle saveState = bottomBarBadge.saveState(this.indexInContainer);
        saveState.putParcelable("superstate", super.onSaveInstanceState());
        return saveState;
    }

    /* access modifiers changed from: package-private */
    public void prepareLayout() {
        LinearLayout.inflate(getContext(), getLayoutResource(), this);
        setOrientation(1);
        setGravity(1);
        setLayoutParams(new LinearLayout.LayoutParams(-2, -2));
        this.iconView = (AppCompatImageView) findViewById(R.id.bb_bottom_bar_icon);
        this.iconView.setImageResource(this.iconResId);
        if (this.type != Type.TABLET) {
            this.titleView = (TextView) findViewById(R.id.bb_bottom_bar_title);
            this.titleView.setText(this.title);
        }
        updateCustomTextAppearance();
        updateCustomTypeface();
    }

    public void removeBadge() {
        setBadgeCount(0);
    }

    /* access modifiers changed from: package-private */
    public void select(boolean z) {
        this.isActive = true;
        if (z) {
            setTopPaddingAnimated(this.iconView.getPaddingTop(), this.sixDps);
            animateIcon(this.activeAlpha);
            animateTitle(ACTIVE_TITLE_SCALE, this.activeAlpha);
            animateColors(this.inActiveColor, this.activeColor);
        } else {
            setTitleScale(ACTIVE_TITLE_SCALE);
            setTopPadding(this.sixDps);
            setColors(this.activeColor);
            setAlphas(this.activeAlpha);
        }
        BottomBarBadge bottomBarBadge = this.badge;
        if (bottomBarBadge != null) {
            bottomBarBadge.hide();
        }
    }

    /* access modifiers changed from: package-private */
    public void setActiveAlpha(float f) {
        this.activeAlpha = f;
        if (this.isActive) {
            setAlphas(f);
        }
    }

    /* access modifiers changed from: package-private */
    public void setActiveColor(int i) {
        this.activeColor = i;
        if (this.isActive) {
            setColors(this.activeColor);
        }
    }

    /* access modifiers changed from: package-private */
    public void setBadgeBackgroundColor(int i) {
        this.badgeBackgroundColor = i;
        BottomBarBadge bottomBarBadge = this.badge;
        if (bottomBarBadge != null) {
            bottomBarBadge.setColoredCircleBackground(i);
        }
    }

    public void setBadgeCount(int i) {
        if (i <= 0) {
            BottomBarBadge bottomBarBadge = this.badge;
            if (bottomBarBadge != null) {
                bottomBarBadge.removeFromTab(this);
                this.badge = null;
                return;
            }
            return;
        }
        if (this.badge == null) {
            this.badge = new BottomBarBadge(getContext());
            this.badge.attachToTab(this, this.badgeBackgroundColor);
        }
        this.badge.setCount(i);
    }

    /* access modifiers changed from: package-private */
    public void setBarColorWhenSelected(int i) {
        this.barColorWhenSelected = i;
    }

    /* access modifiers changed from: package-private */
    public void setConfig(Config config) {
        setInActiveAlpha(config.inActiveTabAlpha);
        setActiveAlpha(config.activeTabAlpha);
        setInActiveColor(config.inActiveTabColor);
        setActiveColor(config.activeTabColor);
        setBarColorWhenSelected(config.barColorWhenSelected);
        setBadgeBackgroundColor(config.badgeBackgroundColor);
        setTitleTextAppearance(config.titleTextAppearance);
        setTitleTypeface(config.titleTypeFace);
    }

    /* access modifiers changed from: package-private */
    public void setIconResId(int i) {
        this.iconResId = i;
    }

    /* access modifiers changed from: package-private */
    public void setIconTint(int i) {
        this.iconView.setColorFilter(i);
    }

    /* access modifiers changed from: package-private */
    public void setInActiveAlpha(float f) {
        this.inActiveAlpha = f;
        if (!this.isActive) {
            setAlphas(f);
        }
    }

    /* access modifiers changed from: package-private */
    public void setInActiveColor(int i) {
        this.inActiveColor = i;
        if (!this.isActive) {
            setColors(i);
        }
    }

    /* access modifiers changed from: package-private */
    public void setIndexInContainer(int i) {
        this.indexInContainer = i;
    }

    /* access modifiers changed from: package-private */
    public void setTitle(String str) {
        this.title = str;
    }

    /* access modifiers changed from: package-private */
    public void setTitleTextAppearance(int i) {
        this.titleTextAppearanceResId = i;
        updateCustomTextAppearance();
    }

    /* access modifiers changed from: package-private */
    public void setTitleTypeface(Typeface typeface) {
        this.titleTypeFace = typeface;
        updateCustomTypeface();
    }

    /* access modifiers changed from: package-private */
    public void setType(Type type2) {
        this.type = type2;
    }

    /* access modifiers changed from: package-private */
    public void updateWidth(float f, boolean z) {
        BottomBarBadge bottomBarBadge;
        if (!z) {
            getLayoutParams().width = (int) f;
            if (!this.isActive && (bottomBarBadge = this.badge) != null) {
                bottomBarBadge.adjustPositionAndSize(this);
                this.badge.show();
                return;
            }
            return;
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(new float[]{(float) getWidth(), f});
        ofFloat.setDuration(ANIMATION_DURATION);
        ofFloat.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ViewGroup.LayoutParams layoutParams = BottomBarTab.this.getLayoutParams();
                if (layoutParams != null) {
                    layoutParams.width = Math.round(((Float) valueAnimator.getAnimatedValue()).floatValue());
                    BottomBarTab.this.setLayoutParams(layoutParams);
                }
            }
        });
        ofFloat.addListener(new AnimatorListenerAdapter() {
            /* JADX WARNING: Code restructure failed: missing block: B:2:0x0008, code lost:
                r2 = r1.this$0;
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void onAnimationEnd(android.animation.Animator r2) {
                /*
                    r1 = this;
                    com.roughike.bottombar.BottomBarTab r2 = com.roughike.bottombar.BottomBarTab.this
                    boolean r2 = r2.isActive
                    if (r2 != 0) goto L_0x0018
                    com.roughike.bottombar.BottomBarTab r2 = com.roughike.bottombar.BottomBarTab.this
                    com.roughike.bottombar.BottomBarBadge r0 = r2.badge
                    if (r0 == 0) goto L_0x0018
                    r0.adjustPositionAndSize(r2)
                    com.roughike.bottombar.BottomBarTab r2 = com.roughike.bottombar.BottomBarTab.this
                    com.roughike.bottombar.BottomBarBadge r2 = r2.badge
                    r2.show()
                L_0x0018:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.roughike.bottombar.BottomBarTab.AnonymousClass3.onAnimationEnd(android.animation.Animator):void");
            }
        });
        ofFloat.start();
    }
}
