package com.roughike.bottombar;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import androidx.core.view.ViewPropertyAnimatorListener;
import androidx.core.view.ViewPropertyAnimatorListenerAdapter;
import com.roughike.bottombar.BottomBarTab;
import java.util.List;

public class BottomBar extends LinearLayout implements View.OnClickListener, View.OnLongClickListener {
    private static final int BEHAVIOR_DRAW_UNDER_NAV = 4;
    private static final int BEHAVIOR_NONE = 0;
    private static final int BEHAVIOR_SHIFTING = 1;
    private static final int BEHAVIOR_SHY = 2;
    private static final float DEFAULT_INACTIVE_SHIFTING_TAB_ALPHA = 0.6f;
    private static final String STATE_CURRENT_SELECTED_TAB = "STATE_CURRENT_SELECTED_TAB";
    private int activeShiftingItemWidth;
    private float activeTabAlpha;
    private int activeTabColor;
    /* access modifiers changed from: private */
    public View backgroundOverlay;
    private int badgeBackgroundColor;
    private int behaviors;
    private int currentBackgroundColor;
    private int currentTabPosition;
    private int defaultBackgroundColor = -1;
    private boolean ignoreTabReselectionListener;
    private int inActiveShiftingItemWidth;
    private float inActiveTabAlpha;
    private int inActiveTabColor;
    private boolean isComingFromRestoredState;
    private boolean isTabletMode;
    private int maxFixedItemWidth;
    private boolean navBarAccountedHeightCalculated;
    private OnTabReselectListener onTabReselectListener;
    private OnTabSelectListener onTabSelectListener;
    /* access modifiers changed from: private */
    public ViewGroup outerContainer;
    private int primaryColor;
    private int screenWidth;
    private View shadowView;
    private boolean showShadow;
    private boolean shyHeightAlreadyCalculated;
    private ViewGroup tabContainer;
    private int tabXmlResource;
    private int tenDp;
    private int titleTextAppearance;
    private Typeface titleTypeFace;

    public BottomBar(Context context) {
        super(context);
        init(context, (AttributeSet) null);
    }

    private void animateBGColorChange(View view, int i) {
        prepareForBackgroundColorAnimation(i);
        if (Build.VERSION.SDK_INT < 21) {
            backgroundCrossfadeAnimation(i);
        } else if (this.outerContainer.isAttachedToWindow()) {
            backgroundCircularRevealAnimation(view, i);
        }
    }

    @TargetApi(21)
    private void backgroundCircularRevealAnimation(View view, final int i) {
        Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(this.backgroundOverlay, (int) (ViewCompat.v(view) + ((float) (view.getMeasuredWidth() / 2))), (this.isTabletMode ? (int) ViewCompat.w(view) : 0) + (view.getMeasuredHeight() / 2), (float) 0, (float) (this.isTabletMode ? this.outerContainer.getHeight() : this.outerContainer.getWidth()));
        if (this.isTabletMode) {
            createCircularReveal.setDuration(500);
        }
        createCircularReveal.addListener(new AnimatorListenerAdapter() {
            private void onEnd() {
                BottomBar.this.outerContainer.setBackgroundColor(i);
                BottomBar.this.backgroundOverlay.setVisibility(4);
                ViewCompat.a(BottomBar.this.backgroundOverlay, 1.0f);
            }

            public void onAnimationCancel(Animator animator) {
                onEnd();
            }

            public void onAnimationEnd(Animator animator) {
                onEnd();
            }
        });
        createCircularReveal.start();
    }

    private void backgroundCrossfadeAnimation(final int i) {
        ViewCompat.a(this.backgroundOverlay, 0.0f);
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this.backgroundOverlay);
        a2.a(1.0f);
        a2.a((ViewPropertyAnimatorListener) new ViewPropertyAnimatorListenerAdapter() {
            private void onEnd() {
                BottomBar.this.outerContainer.setBackgroundColor(i);
                BottomBar.this.backgroundOverlay.setVisibility(4);
                ViewCompat.a(BottomBar.this.backgroundOverlay, 1.0f);
            }

            public void onAnimationCancel(View view) {
                onEnd();
            }

            public void onAnimationEnd(View view) {
                onEnd();
            }
        });
        a2.c();
    }

    private void determineInitialBackgroundColor() {
        if (isShiftingMode()) {
            this.defaultBackgroundColor = this.primaryColor;
        }
        Drawable background = getBackground();
        if (background != null && (background instanceof ColorDrawable)) {
            this.defaultBackgroundColor = ((ColorDrawable) background).getColor();
            setBackgroundColor(0);
        }
    }

    private boolean drawUnderNav() {
        return !this.isTabletMode && hasBehavior(4) && NavbarUtils.shouldDrawBehindNavbar(getContext());
    }

    private BottomBarTab findTabInLayout(ViewGroup viewGroup) {
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            View childAt = viewGroup.getChildAt(i);
            if (childAt instanceof BottomBarTab) {
                return (BottomBarTab) childAt;
            }
        }
        return null;
    }

    private BottomBarTab.Config getTabConfig() {
        return new BottomBarTab.Config.Builder().inActiveTabAlpha(this.inActiveTabAlpha).activeTabAlpha(this.activeTabAlpha).inActiveTabColor(this.inActiveTabColor).activeTabColor(this.activeTabColor).barColorWhenSelected(this.defaultBackgroundColor).badgeBackgroundColor(this.badgeBackgroundColor).titleTextAppearance(this.titleTextAppearance).titleTypeFace(this.titleTypeFace).build();
    }

    private Typeface getTypeFaceFromAsset(String str) {
        if (str != null) {
            return Typeface.createFromAsset(getContext().getAssets(), str);
        }
        return null;
    }

    private void handleBackgroundColorChange(BottomBarTab bottomBarTab, boolean z) {
        int barColorWhenSelected = bottomBarTab.getBarColorWhenSelected();
        if (this.currentBackgroundColor != barColorWhenSelected) {
            if (!z) {
                this.outerContainer.setBackgroundColor(barColorWhenSelected);
                return;
            }
            boolean hasActiveBadge = bottomBarTab.hasActiveBadge();
            View view = bottomBarTab;
            if (hasActiveBadge) {
                view = bottomBarTab.getOuterView();
            }
            animateBGColorChange(view, barColorWhenSelected);
            this.currentBackgroundColor = barColorWhenSelected;
        }
    }

    private void handleClick(View view) {
        BottomBarTab currentTab = getCurrentTab();
        BottomBarTab bottomBarTab = (BottomBarTab) view;
        currentTab.deselect(true);
        bottomBarTab.select(true);
        shiftingMagic(currentTab, bottomBarTab, true);
        handleBackgroundColorChange(bottomBarTab, true);
        updateSelectedTab(bottomBarTab.getIndexInTabContainer());
    }

    private boolean handleLongClick(View view) {
        if (!(view instanceof BottomBarTab)) {
            return true;
        }
        BottomBarTab bottomBarTab = (BottomBarTab) view;
        if ((!isShiftingMode() && !this.isTabletMode) || bottomBarTab.isActive()) {
            return true;
        }
        Toast.makeText(getContext(), bottomBarTab.getTitle(), 0).show();
        return true;
    }

    private boolean hasBehavior(int i) {
        int i2 = this.behaviors;
        return (i | i2) == i2;
    }

    private void init(Context context, AttributeSet attributeSet) {
        populateAttributes(context, attributeSet);
        initializeViews();
        determineInitialBackgroundColor();
    }

    private void initializeShyBehavior() {
        int height;
        ViewParent parent = getParent();
        if (!(parent != null && (parent instanceof CoordinatorLayout))) {
            throw new RuntimeException("In order to have shy behavior, the BottomBar must be a direct child of a CoordinatorLayout.");
        } else if (!this.shyHeightAlreadyCalculated && (height = getHeight()) != 0) {
            updateShyHeight(height);
            this.shyHeightAlreadyCalculated = true;
        }
    }

    private void initializeViews() {
        int i = -2;
        int i2 = this.isTabletMode ? -2 : -1;
        if (this.isTabletMode) {
            i = -1;
        }
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(i2, i);
        setLayoutParams(layoutParams);
        setOrientation(this.isTabletMode ^ true ? 1 : 0);
        ViewCompat.b((View) this, (float) MiscUtils.dpToPixel(getContext(), 8.0f));
        View inflate = LinearLayout.inflate(getContext(), this.isTabletMode ? R.layout.bb_bottom_bar_item_container_tablet : R.layout.bb_bottom_bar_item_container, this);
        inflate.setLayoutParams(layoutParams);
        this.backgroundOverlay = inflate.findViewById(R.id.bb_bottom_bar_background_overlay);
        this.outerContainer = (ViewGroup) inflate.findViewById(R.id.bb_bottom_bar_outer_container);
        this.tabContainer = (ViewGroup) inflate.findViewById(R.id.bb_bottom_bar_item_container);
        this.shadowView = inflate.findViewById(R.id.bb_bottom_bar_shadow);
        if (!this.showShadow) {
            this.shadowView.setVisibility(8);
        }
    }

    private boolean isShiftingMode() {
        return !this.isTabletMode && hasBehavior(1);
    }

    private boolean isShy() {
        return !this.isTabletMode && hasBehavior(2);
    }

    private void populateAttributes(Context context, AttributeSet attributeSet) {
        int i;
        this.primaryColor = MiscUtils.getColor(getContext(), R.attr.colorPrimary);
        this.screenWidth = MiscUtils.getScreenWidth(getContext());
        this.tenDp = MiscUtils.dpToPixel(getContext(), 10.0f);
        this.maxFixedItemWidth = MiscUtils.dpToPixel(getContext(), 168.0f);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.BottomBar, 0, 0);
        try {
            this.tabXmlResource = obtainStyledAttributes.getResourceId(R.styleable.BottomBar_bb_tabXmlResource, 0);
            this.isTabletMode = obtainStyledAttributes.getBoolean(R.styleable.BottomBar_bb_tabletMode, false);
            this.behaviors = obtainStyledAttributes.getInteger(R.styleable.BottomBar_bb_behavior, 0);
            this.inActiveTabAlpha = obtainStyledAttributes.getFloat(R.styleable.BottomBar_bb_inActiveTabAlpha, isShiftingMode() ? DEFAULT_INACTIVE_SHIFTING_TAB_ALPHA : 1.0f);
            this.activeTabAlpha = obtainStyledAttributes.getFloat(R.styleable.BottomBar_bb_activeTabAlpha, 1.0f);
            int i2 = -1;
            if (isShiftingMode()) {
                i = -1;
            } else {
                i = ContextCompat.a(context, R.color.bb_inActiveBottomBarItemColor);
            }
            if (!isShiftingMode()) {
                i2 = this.primaryColor;
            }
            this.inActiveTabColor = obtainStyledAttributes.getColor(R.styleable.BottomBar_bb_inActiveTabColor, i);
            this.activeTabColor = obtainStyledAttributes.getColor(R.styleable.BottomBar_bb_activeTabColor, i2);
            this.badgeBackgroundColor = obtainStyledAttributes.getColor(R.styleable.BottomBar_bb_badgeBackgroundColor, -65536);
            this.titleTextAppearance = obtainStyledAttributes.getResourceId(R.styleable.BottomBar_bb_titleTextAppearance, 0);
            this.titleTypeFace = getTypeFaceFromAsset(obtainStyledAttributes.getString(R.styleable.BottomBar_bb_titleTypeFace));
            this.showShadow = obtainStyledAttributes.getBoolean(R.styleable.BottomBar_bb_showShadow, true);
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    private void prepareForBackgroundColorAnimation(int i) {
        this.outerContainer.clearAnimation();
        this.backgroundOverlay.clearAnimation();
        this.backgroundOverlay.setBackgroundColor(i);
        this.backgroundOverlay.setVisibility(0);
    }

    private void refreshTabs() {
        if (getTabCount() > 0) {
            BottomBarTab.Config tabConfig = getTabConfig();
            for (int i = 0; i < getTabCount(); i++) {
                getTabAtPosition(i).setConfig(tabConfig);
            }
        }
    }

    private void resizeForDrawingUnderNavbar() {
        int height;
        if (Build.VERSION.SDK_INT >= 19 && (height = getHeight()) != 0 && !this.navBarAccountedHeightCalculated) {
            this.navBarAccountedHeightCalculated = true;
            this.tabContainer.getLayoutParams().height = height;
            int navbarHeight = height + NavbarUtils.getNavbarHeight(getContext());
            getLayoutParams().height = navbarHeight;
            if (isShy()) {
                updateShyHeight(navbarHeight);
            }
        }
    }

    private void resizeTabsToCorrectSizes(List<BottomBarTab> list, BottomBarTab[] bottomBarTabArr) {
        LinearLayout.LayoutParams layoutParams;
        int min = Math.min(MiscUtils.dpToPixel(getContext(), (float) (this.screenWidth / list.size())), this.maxFixedItemWidth);
        double d = (double) min;
        this.inActiveShiftingItemWidth = (int) (0.9d * d);
        this.activeShiftingItemWidth = (int) (d + (((double) list.size()) * 0.1d * d));
        int round = Math.round(getContext().getResources().getDimension(R.dimen.bb_height));
        for (BottomBarTab bottomBarTab : bottomBarTabArr) {
            if (!isShiftingMode()) {
                layoutParams = new LinearLayout.LayoutParams(min, round);
            } else if (bottomBarTab.isActive()) {
                layoutParams = new LinearLayout.LayoutParams(this.activeShiftingItemWidth, round);
            } else {
                layoutParams = new LinearLayout.LayoutParams(this.inActiveShiftingItemWidth, round);
            }
            bottomBarTab.setLayoutParams(layoutParams);
            this.tabContainer.addView(bottomBarTab);
        }
    }

    private void shiftingMagic(BottomBarTab bottomBarTab, BottomBarTab bottomBarTab2, boolean z) {
        if (isShiftingMode()) {
            bottomBarTab.updateWidth((float) this.inActiveShiftingItemWidth, z);
            bottomBarTab2.updateWidth((float) this.activeShiftingItemWidth, z);
        }
    }

    private void toggleShyVisibility(boolean z) {
        BottomNavigationBehavior from = BottomNavigationBehavior.from(this);
        if (from != null) {
            from.setHidden(this, z);
        }
    }

    private void updateItems(List<BottomBarTab> list) {
        BottomBarTab.Type type;
        BottomBarTab[] bottomBarTabArr = new BottomBarTab[list.size()];
        int i = 0;
        int i2 = 0;
        for (BottomBarTab next : list) {
            if (isShiftingMode()) {
                type = BottomBarTab.Type.SHIFTING;
            } else if (this.isTabletMode) {
                type = BottomBarTab.Type.TABLET;
            } else {
                type = BottomBarTab.Type.FIXED;
            }
            next.setType(type);
            next.prepareLayout();
            if (i == this.currentTabPosition) {
                next.select(false);
                handleBackgroundColorChange(next, false);
            } else {
                next.deselect(false);
            }
            if (!this.isTabletMode) {
                if (next.getWidth() > i2) {
                    i2 = next.getWidth();
                }
                bottomBarTabArr[i] = next;
            } else {
                this.tabContainer.addView(next);
            }
            next.setOnClickListener(this);
            next.setOnLongClickListener(this);
            i++;
        }
        if (!this.isTabletMode) {
            resizeTabsToCorrectSizes(list, bottomBarTabArr);
        }
    }

    private void updateSelectedTab(int i) {
        int id = getTabAtPosition(i).getId();
        if (i != this.currentTabPosition) {
            OnTabSelectListener onTabSelectListener2 = this.onTabSelectListener;
            if (onTabSelectListener2 != null) {
                onTabSelectListener2.onTabSelected(id);
            }
        } else {
            OnTabReselectListener onTabReselectListener2 = this.onTabReselectListener;
            if (onTabReselectListener2 != null && !this.ignoreTabReselectionListener) {
                onTabReselectListener2.onTabReSelected(id);
            }
        }
        this.currentTabPosition = i;
        if (this.ignoreTabReselectionListener) {
            this.ignoreTabReselectionListener = false;
        }
    }

    private void updateShyHeight(int i) {
        ((CoordinatorLayout.LayoutParams) getLayoutParams()).a((CoordinatorLayout.Behavior) new BottomNavigationBehavior(i, 0, false));
    }

    private void updateTitleBottomPadding() {
        int height;
        if (this.tabContainer != null) {
            int tabCount = getTabCount();
            for (int i = 0; i < tabCount; i++) {
                TextView textView = (TextView) this.tabContainer.getChildAt(i).findViewById(R.id.bb_bottom_bar_title);
                if (textView != null && (height = this.tenDp - (textView.getHeight() - textView.getBaseline())) > 0) {
                    textView.setPadding(textView.getPaddingLeft(), textView.getPaddingTop(), textView.getPaddingRight(), height + textView.getPaddingBottom());
                }
            }
        }
    }

    public int findPositionForTabWithId(int i) {
        return getTabWithId(i).getIndexInTabContainer();
    }

    public BottomBarTab getCurrentTab() {
        return getTabAtPosition(getCurrentTabPosition());
    }

    public int getCurrentTabId() {
        return getCurrentTab().getId();
    }

    public int getCurrentTabPosition() {
        return this.currentTabPosition;
    }

    public BottomBarTab getTabAtPosition(int i) {
        View childAt = this.tabContainer.getChildAt(i);
        if (childAt instanceof FrameLayout) {
            return findTabInLayout((FrameLayout) childAt);
        }
        return (BottomBarTab) childAt;
    }

    public int getTabCount() {
        return this.tabContainer.getChildCount();
    }

    public BottomBarTab getTabWithId(int i) {
        return (BottomBarTab) this.tabContainer.findViewById(i);
    }

    public void onClick(View view) {
        handleClick(view);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z, int i, int i2, int i3, int i4) {
        super.onLayout(z, i, i2, i3, i4);
        if (z) {
            updateTitleBottomPadding();
            if (isShy()) {
                initializeShyBehavior();
            }
            if (drawUnderNav()) {
                resizeForDrawingUnderNavbar();
            }
        }
    }

    public boolean onLongClick(View view) {
        return handleLongClick(view);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        if (parcelable instanceof Bundle) {
            Bundle bundle = (Bundle) parcelable;
            restoreState(bundle);
            parcelable = bundle.getParcelable("superstate");
        }
        super.onRestoreInstanceState(parcelable);
    }

    public Parcelable onSaveInstanceState() {
        Bundle saveState = saveState();
        saveState.putParcelable("superstate", super.onSaveInstanceState());
        return saveState;
    }

    /* access modifiers changed from: package-private */
    public void restoreState(Bundle bundle) {
        if (bundle != null) {
            this.isComingFromRestoredState = true;
            this.ignoreTabReselectionListener = true;
            selectTabAtPosition(bundle.getInt(STATE_CURRENT_SELECTED_TAB, this.currentTabPosition), false);
        }
    }

    /* access modifiers changed from: package-private */
    public Bundle saveState() {
        Bundle bundle = new Bundle();
        bundle.putInt(STATE_CURRENT_SELECTED_TAB, this.currentTabPosition);
        return bundle;
    }

    public void selectTabAtPosition(int i) {
        if (i > getTabCount() - 1 || i < 0) {
            throw new IndexOutOfBoundsException("Can't select tab at position " + i + ". This BottomBar has no items at that position.");
        }
        selectTabAtPosition(i, false);
    }

    public void selectTabWithId(int i) {
        selectTabAtPosition(findPositionForTabWithId(i));
    }

    public void setActiveTabAlpha(float f) {
        this.activeTabAlpha = f;
        refreshTabs();
    }

    public void setActiveTabColor(int i) {
        this.activeTabColor = i;
        refreshTabs();
    }

    public void setBadgeBackgroundColor(int i) {
        this.badgeBackgroundColor = i;
        refreshTabs();
    }

    public void setDefaultTab(int i) {
        setDefaultTabPosition(findPositionForTabWithId(i));
    }

    public void setDefaultTabPosition(int i) {
        if (!this.isComingFromRestoredState) {
            selectTabAtPosition(i);
        }
    }

    public void setInActiveTabAlpha(float f) {
        this.inActiveTabAlpha = f;
        refreshTabs();
    }

    public void setInActiveTabColor(int i) {
        this.inActiveTabColor = i;
        refreshTabs();
    }

    public void setItems(int i) {
        setItems(i, (BottomBarTab.Config) null);
    }

    public void setOnTabReselectListener(OnTabReselectListener onTabReselectListener2) {
        this.onTabReselectListener = onTabReselectListener2;
    }

    public void setOnTabSelectListener(OnTabSelectListener onTabSelectListener2) {
        this.onTabSelectListener = onTabSelectListener2;
        if (this.onTabSelectListener != null && getTabCount() > 0) {
            onTabSelectListener2.onTabSelected(getCurrentTabId());
        }
    }

    public void setTabTitleTextAppearance(int i) {
        this.titleTextAppearance = i;
        refreshTabs();
    }

    public void setTabTitleTypeface(String str) {
        setTabTitleTypeface(getTypeFaceFromAsset(str));
    }

    public void setItems(int i, BottomBarTab.Config config) {
        if (i != 0) {
            if (config == null) {
                config = getTabConfig();
            }
            updateItems(new TabParser(getContext(), config, i).getTabs());
            return;
        }
        throw new RuntimeException("No items specified for the BottomBar!");
    }

    public void setTabTitleTypeface(Typeface typeface) {
        this.titleTypeFace = typeface;
        refreshTabs();
    }

    public BottomBar(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        init(context, attributeSet);
        setItems(this.tabXmlResource);
    }

    private void selectTabAtPosition(int i, boolean z) {
        BottomBarTab currentTab = getCurrentTab();
        BottomBarTab tabAtPosition = getTabAtPosition(i);
        currentTab.deselect(z);
        tabAtPosition.select(z);
        updateSelectedTab(i);
        shiftingMagic(currentTab, tabAtPosition, z);
        handleBackgroundColorChange(tabAtPosition, false);
    }
}
