package com.roughike.bottombar;

import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.ViewConfiguration;
import android.view.WindowManager;

final class NavbarUtils {
    NavbarUtils() {
    }

    static int getNavbarHeight(Context context) {
        Resources resources = context.getResources();
        int identifier = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (identifier > 0) {
            return resources.getDimensionPixelSize(identifier);
        }
        return 0;
    }

    private static boolean hasSoftKeys(Context context) {
        int i = Build.VERSION.SDK_INT;
        if (i >= 17) {
            Display defaultDisplay = ((WindowManager) context.getSystemService("window")).getDefaultDisplay();
            DisplayMetrics displayMetrics = new DisplayMetrics();
            defaultDisplay.getRealMetrics(displayMetrics);
            int i2 = displayMetrics.heightPixels;
            int i3 = displayMetrics.widthPixels;
            DisplayMetrics displayMetrics2 = new DisplayMetrics();
            defaultDisplay.getMetrics(displayMetrics2);
            int i4 = displayMetrics2.heightPixels;
            if (i3 - displayMetrics2.widthPixels > 0 || i2 - i4 > 0) {
                return true;
            }
        } else if (i < 14) {
            return true;
        } else {
            boolean hasPermanentMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
            boolean deviceHasKey = KeyCharacterMap.deviceHasKey(4);
            if (hasPermanentMenuKey || deviceHasKey) {
                return false;
            }
            return true;
        }
        return false;
    }

    private static boolean isPortrait(Context context) {
        return context.getResources().getBoolean(R.bool.bb_bottom_bar_is_portrait_mode);
    }

    static boolean shouldDrawBehindNavbar(Context context) {
        return isPortrait(context) && hasSoftKeys(context);
    }
}
