package com.roughike.bottombar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.view.ViewCompat;
import androidx.core.view.ViewPropertyAnimatorCompat;
import com.roughike.bottombar.BottomBarTab;

class BottomBarBadge extends TextView {
    static final String STATE_COUNT = "STATE_BADGE_COUNT_FOR_TAB_";
    private int count;
    private boolean isVisible = false;

    BottomBarBadge(Context context) {
        super(context);
    }

    private void setBackgroundCompat(Drawable drawable) {
        if (Build.VERSION.SDK_INT >= 16) {
            setBackground(drawable);
        } else {
            setBackgroundDrawable(drawable);
        }
    }

    private void wrapTabAndBadgeInSameContainer(final BottomBarTab bottomBarTab) {
        ViewGroup viewGroup = (ViewGroup) bottomBarTab.getParent();
        viewGroup.removeView(bottomBarTab);
        final FrameLayout frameLayout = new FrameLayout(getContext());
        frameLayout.setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        frameLayout.addView(bottomBarTab);
        frameLayout.addView(this);
        viewGroup.addView(frameLayout, bottomBarTab.getIndexInTabContainer());
        frameLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                frameLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                BottomBarBadge.this.adjustPositionAndSize(bottomBarTab);
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void adjustPositionAndSize(BottomBarTab bottomBarTab) {
        AppCompatImageView iconView = bottomBarTab.getIconView();
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        int max = Math.max(getWidth(), getHeight());
        float width = (float) iconView.getWidth();
        if (bottomBarTab.getType() == BottomBarTab.Type.TABLET) {
            width = (float) (((double) width) / 1.25d);
        }
        setX(iconView.getX() + width);
        setTranslationY(10.0f);
        if (layoutParams.width != max || layoutParams.height != max) {
            layoutParams.width = max;
            layoutParams.height = max;
            setLayoutParams(layoutParams);
        }
    }

    /* access modifiers changed from: package-private */
    public void attachToTab(BottomBarTab bottomBarTab, int i) {
        setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        setGravity(17);
        MiscUtils.setTextAppearance(this, R.style.BB_BottomBarBadge_Text);
        setColoredCircleBackground(i);
        wrapTabAndBadgeInSameContainer(bottomBarTab);
    }

    /* access modifiers changed from: package-private */
    public int getCount() {
        return this.count;
    }

    /* access modifiers changed from: package-private */
    public void hide() {
        this.isVisible = false;
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this);
        a2.a(150);
        a2.a(0.0f);
        a2.b(0.0f);
        a2.c(0.0f);
        a2.c();
    }

    /* access modifiers changed from: package-private */
    public boolean isVisible() {
        return this.isVisible;
    }

    /* access modifiers changed from: package-private */
    public void removeFromTab(BottomBarTab bottomBarTab) {
        FrameLayout frameLayout = (FrameLayout) getParent();
        ViewGroup viewGroup = (ViewGroup) frameLayout.getParent();
        frameLayout.removeView(bottomBarTab);
        viewGroup.removeView(frameLayout);
        viewGroup.addView(bottomBarTab, bottomBarTab.getIndexInTabContainer());
    }

    /* access modifiers changed from: package-private */
    public void restoreState(Bundle bundle, int i) {
        setCount(bundle.getInt(STATE_COUNT + i, this.count));
    }

    /* access modifiers changed from: package-private */
    public Bundle saveState(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt(STATE_COUNT + i, this.count);
        return bundle;
    }

    /* access modifiers changed from: package-private */
    public void setColoredCircleBackground(int i) {
        int dpToPixel = MiscUtils.dpToPixel(getContext(), 1.0f);
        ShapeDrawable make = BadgeCircle.make(dpToPixel * 3, i);
        setPadding(dpToPixel, dpToPixel, dpToPixel, dpToPixel);
        setBackgroundCompat(make);
    }

    /* access modifiers changed from: package-private */
    public void setCount(int i) {
        this.count = i;
        setText(String.valueOf(i));
    }

    /* access modifiers changed from: package-private */
    public void show() {
        this.isVisible = true;
        ViewPropertyAnimatorCompat a2 = ViewCompat.a(this);
        a2.a(150);
        a2.a(1.0f);
        a2.b(1.0f);
        a2.c(1.0f);
        a2.c();
    }
}
