package com.adcolony.sdk;

class e {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1219a;

    e() {
    }

    public synchronized void a(long j) {
        if (!this.f1219a) {
            try {
                wait(j);
            } catch (InterruptedException unused) {
            }
        }
    }

    public synchronized void a(boolean z) {
        this.f1219a = z;
        if (z) {
            notifyAll();
        }
    }

    public boolean a() {
        return this.f1219a;
    }
}
