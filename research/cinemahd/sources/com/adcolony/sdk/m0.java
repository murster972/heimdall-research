package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import com.adcolony.sdk.v;
import com.facebook.common.util.UriUtil;
import com.facebook.imageutils.JfifUtil;
import com.facebook.react.uimanager.ViewProps;
import com.vungle.warren.model.AdvertisementDBAdapter;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONObject;

@TargetApi(14)
class m0 extends TextureView implements MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnCompletionListener, MediaPlayer.OnSeekCompleteListener, TextureView.SurfaceTextureListener {
    /* access modifiers changed from: private */
    public boolean A;
    private boolean B;
    private String C;
    /* access modifiers changed from: private */
    public String D;
    private y E;
    /* access modifiers changed from: private */
    public c F;
    private SurfaceTexture G;
    /* access modifiers changed from: private */
    public RectF H = new RectF();
    /* access modifiers changed from: private */
    public j I;
    private ProgressBar J;
    /* access modifiers changed from: private */
    public MediaPlayer K;
    /* access modifiers changed from: private */
    public JSONObject L = t.b();
    private ExecutorService M = Executors.newSingleThreadExecutor();
    /* access modifiers changed from: private */
    public y N;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public float f1295a;
    /* access modifiers changed from: private */
    public float b;
    private float c;
    private float d;
    /* access modifiers changed from: private */
    public int e;
    private boolean f = true;
    /* access modifiers changed from: private */
    public Paint g = new Paint();
    /* access modifiers changed from: private */
    public Paint h = new Paint(1);
    private int i;
    private int j;
    private int k;
    private int l;
    /* access modifiers changed from: private */
    public int m;
    private int n;
    private int o;
    /* access modifiers changed from: private */
    public double p;
    /* access modifiers changed from: private */
    public double q;
    /* access modifiers changed from: private */
    public long r;
    /* access modifiers changed from: private */
    public boolean s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public boolean v;
    private boolean w;
    /* access modifiers changed from: private */
    public boolean x;
    private boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            if (m0.this.a(yVar)) {
                m0.this.h();
            }
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            if (m0.this.a(yVar)) {
                m0.this.c(yVar);
            }
        }
    }

    class c implements a0 {
        c() {
        }

        public void a(y yVar) {
            if (m0.this.a(yVar)) {
                m0.this.d(yVar);
            }
        }
    }

    class d implements a0 {
        d() {
        }

        public void a(y yVar) {
            if (m0.this.a(yVar)) {
                m0.this.g();
            }
        }
    }

    class e implements a0 {
        e() {
        }

        public void a(y yVar) {
            if (m0.this.a(yVar)) {
                boolean unused = m0.this.b(yVar);
            }
        }
    }

    class f implements a0 {
        f() {
        }

        public void a(y yVar) {
            if (m0.this.a(yVar)) {
                boolean unused = m0.this.e(yVar);
            }
        }
    }

    class g implements Runnable {
        g() {
        }

        public void run() {
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (m0.this.N != null) {
                JSONObject b = t.b();
                t.b(b, "id", m0.this.m);
                t.a(b, "ad_session_id", m0.this.D);
                t.a(b, "success", true);
                m0.this.N.a(b).c();
                y unused = m0.this.N = null;
            }
        }
    }

    class h implements Runnable {
        h() {
        }

        public void run() {
            long unused = m0.this.r = 0;
            while (!m0.this.s && !m0.this.v && a.d()) {
                Context b = a.b();
                if (!m0.this.s && !m0.this.x && b != null && (b instanceof Activity)) {
                    if (m0.this.K.isPlaying()) {
                        if (m0.this.r == 0 && a.d) {
                            long unused2 = m0.this.r = System.currentTimeMillis();
                        }
                        boolean unused3 = m0.this.u = true;
                        m0 m0Var = m0.this;
                        double unused4 = m0Var.p = ((double) m0Var.K.getCurrentPosition()) / 1000.0d;
                        m0 m0Var2 = m0.this;
                        double unused5 = m0Var2.q = ((double) m0Var2.K.getDuration()) / 1000.0d;
                        if (System.currentTimeMillis() - m0.this.r > 1000 && !m0.this.A && a.d) {
                            if (m0.this.p == 0.0d) {
                                new v.a().a("getCurrentPosition() not working, firing ").a("AdSession.on_error").a(v.i);
                                m0.this.k();
                            } else {
                                boolean unused6 = m0.this.A = true;
                            }
                        }
                        if (m0.this.z) {
                            m0.this.e();
                        }
                    }
                    if (m0.this.u && !m0.this.s && !m0.this.v) {
                        t.b(m0.this.L, "id", m0.this.m);
                        t.b(m0.this.L, "container_id", m0.this.F.c());
                        t.a(m0.this.L, "ad_session_id", m0.this.D);
                        t.a(m0.this.L, "elapsed", m0.this.p);
                        t.a(m0.this.L, "duration", m0.this.q);
                        new y("VideoView.on_progress", m0.this.F.k(), m0.this.L).c();
                    }
                    if (m0.this.t || ((Activity) b).isFinishing()) {
                        boolean unused7 = m0.this.t = false;
                        m0.this.i();
                        return;
                    }
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException unused8) {
                        m0.this.k();
                        new v.a().a("InterruptedException in ADCVideoView's update thread.").a(v.h);
                    }
                } else {
                    return;
                }
            }
            if (m0.this.t) {
                m0.this.i();
            }
        }
    }

    class i implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1304a;

        i(Context context) {
            this.f1304a = context;
        }

        public void run() {
            m0 m0Var = m0.this;
            j unused = m0Var.I = new j(this.f1304a);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) (m0.this.f1295a * 4.0f), (int) (m0.this.f1295a * 4.0f));
            layoutParams.setMargins(0, m0.this.F.b() - ((int) (m0.this.f1295a * 4.0f)), 0, 0);
            layoutParams.gravity = 0;
            m0.this.F.addView(m0.this.I, layoutParams);
        }
    }

    private class j extends View {
        j(Context context) {
            super(context);
            setWillNotDraw(false);
            try {
                Class<?> cls = getClass();
                cls.getMethod("setLayerType", new Class[]{Integer.TYPE, Paint.class}).invoke(this, new Object[]{1, null});
            } catch (Exception unused) {
            }
        }

        public void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawArc(m0.this.H, 270.0f, m0.this.b, false, m0.this.g);
            canvas.drawText("" + m0.this.e, m0.this.H.centerX(), (float) (((double) m0.this.H.centerY()) + (((double) m0.this.h.getFontMetrics().bottom) * 1.35d)), m0.this.h);
            invalidate();
        }
    }

    m0(Context context, y yVar, int i2, c cVar) {
        super(context);
        this.F = cVar;
        this.E = yVar;
        this.m = i2;
        setSurfaceTextureListener(this);
    }

    public void onCompletion(MediaPlayer mediaPlayer) {
        this.s = true;
        this.p = this.q;
        t.b(this.L, "id", this.m);
        t.b(this.L, "container_id", this.F.c());
        t.a(this.L, "ad_session_id", this.D);
        t.a(this.L, "elapsed", this.p);
        t.a(this.L, "duration", this.q);
        new y("VideoView.on_progress", this.F.k(), this.L).c();
    }

    public boolean onError(MediaPlayer mediaPlayer, int i2, int i3) {
        k();
        v.a aVar = new v.a();
        aVar.a("MediaPlayer error: " + i2 + "," + i3).a(v.h);
        return true;
    }

    public void onMeasure(int i2, int i3) {
        l();
    }

    public void onPrepared(MediaPlayer mediaPlayer) {
        this.w = true;
        if (this.B) {
            this.F.removeView(this.J);
        }
        if (this.y) {
            this.n = mediaPlayer.getVideoWidth();
            this.o = mediaPlayer.getVideoHeight();
            l();
            new v.a().a("MediaPlayer getVideoWidth = ").a(mediaPlayer.getVideoWidth()).a(v.e);
            new v.a().a("MediaPlayer getVideoHeight = ").a(mediaPlayer.getVideoHeight()).a(v.e);
        }
        JSONObject b2 = t.b();
        t.b(b2, "id", this.m);
        t.b(b2, "container_id", this.F.c());
        t.a(b2, "ad_session_id", this.D);
        new y("VideoView.on_ready", this.F.k(), b2).c();
    }

    public void onSeekComplete(MediaPlayer mediaPlayer) {
        ExecutorService executorService = this.M;
        if (executorService != null && !executorService.isShutdown()) {
            try {
                this.M.submit(new g());
            } catch (RejectedExecutionException unused) {
                k();
            }
        }
    }

    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i2, int i3) {
        if (surfaceTexture == null || this.x) {
            new v.a().a("Null texture provided by system's onSurfaceTextureAvailable or ").a("MediaPlayer has been destroyed.").a(v.i);
            return;
        }
        try {
            this.K.setSurface(new Surface(surfaceTexture));
        } catch (IllegalStateException unused) {
            new v.a().a("IllegalStateException thrown when calling MediaPlayer.setSurface()").a(v.h);
            k();
        }
        this.G = surfaceTexture;
    }

    public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
        this.G = surfaceTexture;
        if (!this.x) {
            return false;
        }
        surfaceTexture.release();
        return true;
    }

    public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i2, int i3) {
        this.G = surfaceTexture;
    }

    public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
        this.G = surfaceTexture;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        i c2 = a.c();
        d e2 = c2.e();
        int action = motionEvent.getAction() & JfifUtil.MARKER_FIRST_BYTE;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject b2 = t.b();
        t.b(b2, "view_id", this.m);
        t.a(b2, "ad_session_id", this.D);
        t.b(b2, "container_x", this.i + x2);
        t.b(b2, "container_y", this.j + y2);
        t.b(b2, "view_x", x2);
        t.b(b2, "view_y", y2);
        t.b(b2, "id", this.F.c());
        if (action == 0) {
            new y("AdContainer.on_touch_began", this.F.k(), b2).c();
        } else if (action == 1) {
            if (!this.F.p()) {
                c2.a(e2.b().get(this.D));
            }
            new y("AdContainer.on_touch_ended", this.F.k(), b2).c();
        } else if (action == 2) {
            new y("AdContainer.on_touch_moved", this.F.k(), b2).c();
        } else if (action == 3) {
            new y("AdContainer.on_touch_cancelled", this.F.k(), b2).c();
        } else if (action == 5) {
            int action2 = (motionEvent.getAction() & 65280) >> 8;
            t.b(b2, "container_x", ((int) motionEvent2.getX(action2)) + this.i);
            t.b(b2, "container_y", ((int) motionEvent2.getY(action2)) + this.j);
            t.b(b2, "view_x", (int) motionEvent2.getX(action2));
            t.b(b2, "view_y", (int) motionEvent2.getY(action2));
            new y("AdContainer.on_touch_began", this.F.k(), b2).c();
        } else if (action == 6) {
            int action3 = (motionEvent.getAction() & 65280) >> 8;
            t.b(b2, "container_x", ((int) motionEvent2.getX(action3)) + this.i);
            t.b(b2, "container_y", ((int) motionEvent2.getY(action3)) + this.j);
            t.b(b2, "view_x", (int) motionEvent2.getX(action3));
            t.b(b2, "view_y", (int) motionEvent2.getY(action3));
            if (!this.F.p()) {
                c2.a(e2.b().get(this.D));
            }
            new y("AdContainer.on_touch_ended", this.F.k(), b2).c();
        }
        return true;
    }

    /* access modifiers changed from: private */
    public void k() {
        JSONObject b2 = t.b();
        t.a(b2, "id", this.D);
        new y("AdSession.on_error", this.F.k(), b2).c();
        this.s = true;
    }

    private void l() {
        double min = Math.min(((double) this.k) / ((double) this.n), ((double) this.l) / ((double) this.o));
        int i2 = (int) (((double) this.n) * min);
        int i3 = (int) (((double) this.o) * min);
        new v.a().a("setMeasuredDimension to ").a(i2).a(" by ").a(i3).a(v.e);
        setMeasuredDimension(i2, i3);
        if (this.y) {
            FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
            layoutParams.width = i2;
            layoutParams.height = i3;
            layoutParams.gravity = 17;
            layoutParams.setMargins(0, 0, 0, 0);
            setLayoutParams(layoutParams);
        }
    }

    private void m() {
        try {
            this.M.submit(new h());
        } catch (RejectedExecutionException unused) {
            k();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        if (!this.w) {
            new v.a().a("ADCVideoView pause() called while MediaPlayer is not prepared.").a(v.g);
            return false;
        } else if (!this.u) {
            return false;
        } else {
            this.K.getCurrentPosition();
            this.q = (double) this.K.getDuration();
            this.K.pause();
            this.v = true;
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        if (!this.w) {
            return false;
        }
        if (!this.v && a.d) {
            this.K.start();
            m();
        } else if (!this.s && a.d) {
            this.K.start();
            this.v = false;
            if (!this.M.isShutdown()) {
                m();
            }
            j jVar = this.I;
            if (jVar != null) {
                jVar.invalidate();
            }
        }
        setWillNotDraw(false);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void i() {
        new v.a().a("MediaPlayer stopped and released.").a(v.e);
        try {
            if (!this.s && this.w && this.K.isPlaying()) {
                this.K.stop();
            }
        } catch (IllegalStateException unused) {
            new v.a().a("Caught IllegalStateException when calling stop on MediaPlayer").a(v.g);
        }
        ProgressBar progressBar = this.J;
        if (progressBar != null) {
            this.F.removeView(progressBar);
        }
        this.s = true;
        this.w = false;
        this.K.release();
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.t = true;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        Context b2;
        JSONObject a2 = this.E.a();
        this.D = t.g(a2, "ad_session_id");
        this.i = t.e(a2, "x");
        this.j = t.e(a2, "y");
        this.k = t.e(a2, "width");
        this.l = t.e(a2, "height");
        this.z = t.c(a2, "enable_timer");
        this.B = t.c(a2, "enable_progress");
        this.C = t.g(a2, "filepath");
        this.n = t.e(a2, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_WIDTH);
        this.o = t.e(a2, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_VIDEO_HEIGHT);
        this.d = a.c().k().x();
        new v.a().a("Original video dimensions = ").a(this.n).a("x").a(this.o).a(v.c);
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.k, this.l);
        layoutParams.setMargins(this.i, this.j, 0, 0);
        layoutParams.gravity = 0;
        this.F.addView(this, layoutParams);
        if (this.B && (b2 = a.b()) != null) {
            ProgressBar progressBar = new ProgressBar(b2);
            this.J = progressBar;
            c cVar = this.F;
            int i2 = (int) (this.d * 100.0f);
            cVar.addView(progressBar, new FrameLayout.LayoutParams(i2, i2, 17));
        }
        this.K = new MediaPlayer();
        this.w = false;
        try {
            if (!this.C.startsWith(UriUtil.HTTP_SCHEME)) {
                this.K.setDataSource(new FileInputStream(this.C).getFD());
            } else {
                this.y = true;
                this.K.setDataSource(this.C);
            }
            this.K.setOnErrorListener(this);
            this.K.setOnPreparedListener(this);
            this.K.setOnCompletionListener(this);
            this.K.prepareAsync();
        } catch (IOException e2) {
            new v.a().a("Failed to create/prepare MediaPlayer: ").a(e2.toString()).a(v.h);
            k();
        }
        ArrayList<a0> i3 = this.F.i();
        a aVar = new a();
        a.a("VideoView.play", (a0) aVar, true);
        i3.add(aVar);
        ArrayList<a0> i4 = this.F.i();
        b bVar = new b();
        a.a("VideoView.set_bounds", (a0) bVar, true);
        i4.add(bVar);
        ArrayList<a0> i5 = this.F.i();
        c cVar2 = new c();
        a.a("VideoView.set_visible", (a0) cVar2, true);
        i5.add(cVar2);
        ArrayList<a0> i6 = this.F.i();
        d dVar = new d();
        a.a("VideoView.pause", (a0) dVar, true);
        i6.add(dVar);
        ArrayList<a0> i7 = this.F.i();
        e eVar = new e();
        a.a("VideoView.seek_to_time", (a0) eVar, true);
        i7.add(eVar);
        ArrayList<a0> i8 = this.F.i();
        f fVar = new f();
        a.a("VideoView.set_volume", (a0) fVar, true);
        i8.add(fVar);
        this.F.j().add("VideoView.play");
        this.F.j().add("VideoView.set_bounds");
        this.F.j().add("VideoView.set_visible");
        this.F.j().add("VideoView.pause");
        this.F.j().add("VideoView.seek_to_time");
        this.F.j().add("VideoView.set_volume");
    }

    /* access modifiers changed from: package-private */
    public void e() {
        if (this.f) {
            this.c = (float) (360.0d / this.q);
            this.h.setColor(-3355444);
            this.h.setShadowLayer((float) ((int) (this.d * 2.0f)), 0.0f, 0.0f, -16777216);
            this.h.setTextAlign(Paint.Align.CENTER);
            this.h.setLinearText(true);
            this.h.setTextSize(this.d * 12.0f);
            this.g.setStyle(Paint.Style.STROKE);
            float f2 = this.d * 2.0f;
            if (f2 > 6.0f) {
                f2 = 6.0f;
            }
            if (f2 < 4.0f) {
                f2 = 4.0f;
            }
            this.g.setStrokeWidth(f2);
            this.g.setShadowLayer((float) ((int) (this.d * 3.0f)), 0.0f, 0.0f, -16777216);
            this.g.setColor(-3355444);
            Rect rect = new Rect();
            this.h.getTextBounds("0123456789", 0, 9, rect);
            this.f1295a = (float) rect.height();
            Context b2 = a.b();
            if (b2 != null) {
                l0.a((Runnable) new i(b2));
            }
            this.f = false;
        }
        this.e = (int) (this.q - this.p);
        float f3 = this.f1295a;
        float f4 = (float) ((int) f3);
        float f5 = (float) ((int) (3.0f * f3));
        float f6 = f3 / 2.0f;
        float f7 = f3 * 2.0f;
        this.H.set(f4 - f6, f5 - f7, f4 + f7, f5 + f6);
        this.b = (float) (((double) this.c) * (this.q - this.p));
    }

    /* access modifiers changed from: package-private */
    public boolean f() {
        return this.s;
    }

    /* access modifiers changed from: private */
    public void c(y yVar) {
        JSONObject a2 = yVar.a();
        this.i = t.e(a2, "x");
        this.j = t.e(a2, "y");
        this.k = t.e(a2, "width");
        this.l = t.e(a2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.i, this.j, 0, 0);
        layoutParams.width = this.k;
        layoutParams.height = this.l;
        setLayoutParams(layoutParams);
        if (this.z && this.I != null) {
            int i2 = (int) (this.f1295a * 4.0f);
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(i2, i2);
            layoutParams2.setMargins(0, this.F.b() - ((int) (this.f1295a * 4.0f)), 0, 0);
            layoutParams2.gravity = 0;
            this.I.setLayoutParams(layoutParams2);
        }
    }

    /* access modifiers changed from: private */
    public boolean b(y yVar) {
        if (!this.w) {
            return false;
        }
        if (this.s) {
            this.s = false;
        }
        this.N = yVar;
        int e2 = t.e(yVar.a(), "time");
        int duration = this.K.getDuration() / 1000;
        this.K.setOnSeekCompleteListener(this);
        this.K.seekTo(e2 * 1000);
        if (duration == e2) {
            this.s = true;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.G != null) {
            this.x = true;
        }
        this.M.shutdown();
    }

    /* access modifiers changed from: private */
    public boolean a(y yVar) {
        JSONObject a2 = yVar.a();
        return t.e(a2, "id") == this.m && t.e(a2, "container_id") == this.F.c() && t.g(a2, "ad_session_id").equals(this.F.a());
    }

    /* access modifiers changed from: package-private */
    public MediaPlayer b() {
        return this.K;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.K != null;
    }

    /* access modifiers changed from: private */
    public boolean e(y yVar) {
        boolean z2 = false;
        if (!this.w) {
            return false;
        }
        float d2 = (float) t.d(yVar.a(), "volume");
        AdColonyInterstitial g2 = a.c().g();
        if (g2 != null) {
            if (((double) d2) <= 0.0d) {
                z2 = true;
            }
            g2.b(z2);
        }
        this.K.setVolume(d2, d2);
        JSONObject b2 = t.b();
        t.a(b2, "success", true);
        yVar.a(b2).c();
        return true;
    }

    /* access modifiers changed from: private */
    public void d(y yVar) {
        j jVar;
        j jVar2;
        if (t.c(yVar.a(), ViewProps.VISIBLE)) {
            setVisibility(0);
            if (this.z && (jVar2 = this.I) != null) {
                jVar2.setVisibility(0);
                return;
            }
            return;
        }
        setVisibility(4);
        if (this.z && (jVar = this.I) != null) {
            jVar.setVisibility(4);
        }
    }
}
