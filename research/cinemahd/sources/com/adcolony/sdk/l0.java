package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.adcolony.sdk.v;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.zip.CRC32;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class l0 {

    /* renamed from: a  reason: collision with root package name */
    static ExecutorService f1291a = Executors.newSingleThreadExecutor();
    static Handler b;

    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1292a;
        final /* synthetic */ String b;
        final /* synthetic */ int c;

        a(Context context, String str, int i) {
            this.f1292a = context;
            this.b = str;
            this.c = i;
        }

        public void run() {
            Toast.makeText(this.f1292a, this.b, this.c).show();
        }
    }

    l0() {
    }

    static int a(Context context, String str) {
        return a(b(context), str);
    }

    static SharedPreferences b(Context context) {
        try {
            Class.forName("androidx.preference.PreferenceManager");
            return PreferenceManager.a(context);
        } catch (ClassNotFoundException unused) {
            return context.getSharedPreferences(context.getPackageName() + "_preferences", 0);
        }
    }

    static boolean c(String str) {
        Application application;
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        try {
            if (b2 instanceof Application) {
                application = (Application) b2;
            } else {
                application = ((Activity) b2).getApplication();
            }
            application.getPackageManager().getApplicationInfo(str, 0);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    @Deprecated
    static int d() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        try {
            return b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0).versionCode;
        } catch (Exception unused) {
            new v.a().a("Failed to retrieve package info.").a(v.i);
            return 0;
        }
    }

    static boolean e(String str) {
        if (str != null && str.length() <= 128) {
            return true;
        }
        new v.a().a("String must be non-null and the max length is 128 characters.").a(v.f);
        return false;
    }

    static int f(String str) {
        try {
            return (int) Long.parseLong(str, 16);
        } catch (NumberFormatException unused) {
            new v.a().a("Unable to parse '").a(str).a("' as a color.").a(v.g);
            return -16777216;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(3:4|5|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0026, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0020, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", r1).parse(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0025, code lost:
        return new java.text.SimpleDateFormat("yyyy-MM-dd", r1).parse(r5);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:4:0x001c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0021 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static java.util.Date g(java.lang.String r5) {
        /*
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat
            java.util.Locale r1 = java.util.Locale.US
            java.lang.String r2 = "yyyy-MM-dd'T'HH:mmZ"
            r0.<init>(r2, r1)
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat
            java.lang.String r3 = "yyyy-MM-dd'T'HH:mm:ssZ"
            r2.<init>(r3, r1)
            java.text.SimpleDateFormat r3 = new java.text.SimpleDateFormat
            java.lang.String r4 = "yyyy-MM-dd"
            r3.<init>(r4, r1)
            java.util.Date r5 = r0.parse(r5)     // Catch:{ Exception -> 0x001c }
            return r5
        L_0x001c:
            java.util.Date r5 = r2.parse(r5)     // Catch:{ Exception -> 0x0021 }
            return r5
        L_0x0021:
            java.util.Date r5 = r3.parse(r5)     // Catch:{ Exception -> 0x0026 }
            return r5
        L_0x0026:
            r5 = 0
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.l0.g(java.lang.String):java.util.Date");
    }

    static class b {

        /* renamed from: a  reason: collision with root package name */
        double f1293a;
        double b = ((double) System.currentTimeMillis());

        b(double d) {
            a(d);
        }

        /* access modifiers changed from: package-private */
        public void a(double d) {
            this.f1293a = d;
            this.b = (((double) System.currentTimeMillis()) / 1000.0d) + this.f1293a;
        }

        /* access modifiers changed from: package-private */
        public double b() {
            double currentTimeMillis = this.b - (((double) System.currentTimeMillis()) / 1000.0d);
            if (currentTimeMillis <= 0.0d) {
                return 0.0d;
            }
            return currentTimeMillis;
        }

        public String toString() {
            return l0.a(b(), 2);
        }

        /* access modifiers changed from: package-private */
        public boolean a() {
            return b() == 0.0d;
        }
    }

    static int a(SharedPreferences sharedPreferences, String str) {
        try {
            return sharedPreferences.getInt(str, -1);
        } catch (ClassCastException unused) {
            v.a aVar = new v.a();
            aVar.a("Key " + str + " in SharedPreferences ").a("does not have an int value.").a(v.g);
            return -1;
        }
    }

    static String b(Context context, String str) {
        return b(b(context), str);
    }

    static String b(SharedPreferences sharedPreferences, String str) {
        try {
            return sharedPreferences.getString(str, (String) null);
        } catch (ClassCastException unused) {
            v.a aVar = new v.a();
            aVar.a("Key " + str + " in SharedPreferences ").a("does not have a String value.").a(v.g);
            return null;
        }
    }

    static int e(Context context) {
        int identifier;
        if (context != null && (identifier = context.getResources().getIdentifier("status_bar_height", "dimen", "android")) > 0) {
            return context.getResources().getDimensionPixelSize(identifier);
        }
        return 0;
    }

    static boolean f() {
        Context b2 = a.b();
        return b2 != null && Build.VERSION.SDK_INT >= 24 && (b2 instanceof Activity) && ((Activity) b2).isInMultiWindowMode();
    }

    static String c() {
        Context b2 = a.b();
        if (b2 == null) {
            return "1.0";
        }
        try {
            return b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0).versionName;
        } catch (Exception unused) {
            new v.a().a("Failed to retrieve package info.").a(v.i);
            return "1.0";
        }
    }

    static JSONObject a(JSONObject jSONObject) {
        jSONObject.remove("permissions");
        return jSONObject;
    }

    static JSONArray d(Context context) {
        JSONArray a2 = t.a();
        if (context != null) {
            try {
                PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
                if (packageInfo.requestedPermissions != null) {
                    a2 = t.a();
                    int i = 0;
                    while (true) {
                        String[] strArr = packageInfo.requestedPermissions;
                        if (i >= strArr.length) {
                            break;
                        }
                        a2.put(strArr[i]);
                        i++;
                    }
                }
            } catch (Exception unused) {
            }
        }
        return a2;
    }

    static String e() {
        Context b2 = a.b();
        return (!(b2 instanceof Activity) || b2.getResources().getConfiguration().orientation != 1) ? "landscape" : "portrait";
    }

    static int a(String str) {
        CRC32 crc32 = new CRC32();
        int length = str.length();
        for (int i = 0; i < length; i++) {
            crc32.update(str.charAt(i));
        }
        return (int) crc32.getValue();
    }

    static String b() {
        Application application;
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        if (b2 instanceof Application) {
            application = (Application) b2;
        } else {
            application = ((Activity) b2).getApplication();
        }
        PackageManager packageManager = application.getPackageManager();
        try {
            return packageManager.getApplicationLabel(packageManager.getApplicationInfo(b2.getPackageName(), 0)).toString();
        } catch (Exception unused) {
            new v.a().a("Failed to retrieve application label.").a(v.i);
            return "";
        }
    }

    static String a() {
        return UUID.randomUUID().toString();
    }

    static String c(Context context) {
        try {
            return context.getPackageName();
        } catch (Exception unused) {
            return "unknown";
        }
    }

    static JSONArray a(int i) {
        JSONArray a2 = t.a();
        for (int i2 = 0; i2 < i; i2++) {
            t.b(a2, a());
        }
        return a2;
    }

    static boolean a(String[] strArr, String[] strArr2) {
        if (strArr == null || strArr2 == null || strArr.length != strArr2.length) {
            return false;
        }
        Arrays.sort(strArr);
        Arrays.sort(strArr2);
        return Arrays.equals(strArr, strArr2);
    }

    static int d(String str) {
        str.hashCode();
        if (!str.equals("portrait")) {
            return !str.equals("landscape") ? -1 : 1;
        }
        return 0;
    }

    static boolean a(Runnable runnable) {
        Looper mainLooper = Looper.getMainLooper();
        if (mainLooper == null) {
            return false;
        }
        if (b == null) {
            b = new Handler(mainLooper);
        }
        if (mainLooper == Looper.myLooper()) {
            runnable.run();
            return true;
        }
        b.post(runnable);
        return true;
    }

    static String b(String str) {
        try {
            return o0.a(str);
        } catch (Exception unused) {
            return null;
        }
    }

    static boolean b(AudioManager audioManager) {
        if (audioManager == null) {
            new v.a().a("isAudioEnabled() called with a null AudioManager").a(v.i);
            return false;
        }
        try {
            if (audioManager.getStreamVolume(3) > 0) {
                return true;
            }
            return false;
        } catch (Exception e) {
            new v.a().a("Exception occurred when accessing AudioManager.getStreamVolume: ").a(e.toString()).a(v.i);
            return false;
        }
    }

    static double a(AudioManager audioManager) {
        if (audioManager == null) {
            new v.a().a("getAudioVolume() called with a null AudioManager").a(v.i);
            return 0.0d;
        }
        try {
            double streamVolume = (double) audioManager.getStreamVolume(3);
            double streamMaxVolume = (double) audioManager.getStreamMaxVolume(3);
            if (streamMaxVolume == 0.0d) {
                return 0.0d;
            }
            return streamVolume / streamMaxVolume;
        } catch (Exception e) {
            new v.a().a("Exception occurred when accessing AudioManager: ").a(e.toString()).a(v.i);
            return 0.0d;
        }
    }

    static String b(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                str = str + ",";
            }
            str = str + jSONArray.getInt(i);
        }
        return str;
    }

    static int b(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[1]) / a.c().k().x());
    }

    static AudioManager a(Context context) {
        if (context != null) {
            return (AudioManager) context.getSystemService("audio");
        }
        new v.a().a("getAudioManager called with a null Context").a(v.i);
        return null;
    }

    static String a(double d, int i) {
        StringBuilder sb = new StringBuilder();
        a(d, i, sb);
        return sb.toString();
    }

    static void a(double d, int i, StringBuilder sb) {
        if (Double.isNaN(d) || Double.isInfinite(d)) {
            sb.append(d);
            return;
        }
        if (d < 0.0d) {
            d = -d;
            sb.append('-');
        }
        if (i == 0) {
            sb.append(Math.round(d));
            return;
        }
        long pow = (long) Math.pow(10.0d, (double) i);
        long round = Math.round(d * ((double) pow));
        sb.append(round / pow);
        sb.append('.');
        long j = round % pow;
        if (j == 0) {
            for (int i2 = 0; i2 < i; i2++) {
                sb.append('0');
            }
            return;
        }
        for (long j2 = j * 10; j2 < pow; j2 *= 10) {
            sb.append('0');
        }
        sb.append(j);
    }

    static boolean a(String str, File file) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA1");
            try {
                FileInputStream fileInputStream = new FileInputStream(file);
                byte[] bArr = new byte[8192];
                while (true) {
                    try {
                        int read = fileInputStream.read(bArr);
                        if (read <= 0) {
                            break;
                        }
                        instance.update(bArr, 0, read);
                    } catch (IOException e) {
                        throw new RuntimeException("Unable to process file for MD5", e);
                    } catch (Throwable th) {
                        try {
                            fileInputStream.close();
                        } catch (IOException unused) {
                            new v.a().a("Exception on closing MD5 input stream").a(v.i);
                        }
                        throw th;
                    }
                }
                boolean equals = str.equals(String.format("%40s", new Object[]{new BigInteger(1, instance.digest()).toString(16)}).replace(' ', '0'));
                try {
                    fileInputStream.close();
                } catch (IOException unused2) {
                    new v.a().a("Exception on closing MD5 input stream").a(v.i);
                }
                return equals;
            } catch (FileNotFoundException unused3) {
                new v.a().a("Exception while getting FileInputStream").a(v.i);
                return false;
            }
        } catch (NoSuchAlgorithmException unused4) {
            new v.a().a("Exception while getting Digest").a(v.i);
            return false;
        }
    }

    static String a(JSONArray jSONArray) throws JSONException {
        String str = "";
        for (int i = 0; i < jSONArray.length(); i++) {
            if (i > 0) {
                str = str + ",";
            }
            switch (jSONArray.getInt(i)) {
                case 1:
                    str = str + "MO";
                    break;
                case 2:
                    str = str + "TU";
                    break;
                case 3:
                    str = str + "WE";
                    break;
                case 4:
                    str = str + "TH";
                    break;
                case 5:
                    str = str + "FR";
                    break;
                case 6:
                    str = str + "SA";
                    break;
                case 7:
                    str = str + "SU";
                    break;
            }
        }
        return str;
    }

    static boolean a(Intent intent, boolean z) {
        try {
            Context b2 = a.b();
            if (b2 == null) {
                return false;
            }
            if (!(b2 instanceof Activity)) {
                intent.addFlags(268435456);
            }
            AdColonyInterstitial g = a.c().g();
            if (g != null && g.i()) {
                g.e().e();
            }
            if (z) {
                b2.startActivity(Intent.createChooser(intent, "Handle this via..."));
                return true;
            }
            b2.startActivity(intent);
            return true;
        } catch (Exception e) {
            new v.a().a(e.toString()).a(v.g);
            return false;
        }
    }

    static boolean a(Intent intent) {
        return a(intent, false);
    }

    static boolean a(String str, int i) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        a((Runnable) new a(b2, str, i));
        return true;
    }

    static int a(h0 h0Var) {
        int i = 0;
        try {
            Context b2 = a.b();
            if (b2 != null) {
                int i2 = (int) (b2.getPackageManager().getPackageInfo(b2.getPackageName(), 0).lastUpdateTime / 1000);
                boolean z = true;
                if (new File(h0Var.a() + "AppVersion").exists()) {
                    if (t.e(t.c(h0Var.a() + "AppVersion"), "last_update") != i2) {
                        i = 1;
                    } else {
                        z = false;
                    }
                } else {
                    i = 2;
                }
                if (z) {
                    JSONObject b3 = t.b();
                    t.b(b3, "last_update", i2);
                    t.h(b3, h0Var.a() + "AppVersion");
                }
            }
        } catch (Exception unused) {
        }
        return i;
    }

    static int a(View view) {
        if (view == null) {
            return 0;
        }
        int[] iArr = {0, 0};
        view.getLocationOnScreen(iArr);
        return (int) (((float) iArr[0]) / a.c().k().x());
    }
}
