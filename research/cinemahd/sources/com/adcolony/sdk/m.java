package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.v;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import org.joda.time.DateTimeConstants;
import org.json.JSONObject;

class m implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private HttpURLConnection f1294a;
    private InputStream b;
    private y c;
    private a d;
    private String e;
    private int f = 0;
    private boolean g = false;
    private Map<String, List<String>> h;
    private String i = "";
    private String j = "";
    String k = "";
    String l = "";
    boolean m;
    int n;
    int o;

    interface a {
        void a(m mVar, y yVar, Map<String, List<String>> map);
    }

    m(y yVar, a aVar) {
        this.c = yVar;
        this.d = aVar;
    }

    private boolean b() throws IOException {
        JSONObject a2 = this.c.a();
        String g2 = t.g(a2, "content_type");
        String g3 = t.g(a2, "content");
        int a3 = t.a(a2, "read_timeout", (int) DateTimeConstants.MILLIS_PER_MINUTE);
        int a4 = t.a(a2, "connect_timeout", (int) DateTimeConstants.MILLIS_PER_MINUTE);
        boolean c2 = t.c(a2, "no_redirect");
        this.k = t.g(a2, ReportDBAdapter.ReportColumns.COLUMN_URL);
        this.i = t.g(a2, "filepath");
        StringBuilder sb = new StringBuilder();
        sb.append(a.c().x().d());
        String str = this.i;
        sb.append(str.substring(str.lastIndexOf("/") + 1));
        this.j = sb.toString();
        this.e = t.g(a2, "encoding");
        int a5 = t.a(a2, "max_size", 0);
        this.f = a5;
        this.g = a5 != 0;
        this.n = 0;
        this.b = null;
        this.f1294a = null;
        this.h = null;
        if (!this.k.startsWith("file://")) {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(this.k).openConnection();
            this.f1294a = httpURLConnection;
            httpURLConnection.setReadTimeout(a3);
            this.f1294a.setConnectTimeout(a4);
            this.f1294a.setInstanceFollowRedirects(!c2);
            this.f1294a.setRequestProperty("Accept-Charset", "UTF-8");
            String g4 = a.c().k().g();
            if (g4 != null && !g4.equals("")) {
                this.f1294a.setRequestProperty("User-Agent", g4);
            }
            if (!g2.equals("")) {
                this.f1294a.setRequestProperty(TraktV2.HEADER_CONTENT_TYPE, g2);
            }
            if (this.c.b().equals("WebServices.post")) {
                this.f1294a.setDoOutput(true);
                this.f1294a.setFixedLengthStreamingMode(g3.getBytes("UTF-8").length);
                new PrintStream(this.f1294a.getOutputStream()).print(g3);
            }
        } else if (this.k.startsWith("file:///android_asset/")) {
            Context b2 = a.b();
            if (b2 != null) {
                this.b = b2.getAssets().open(this.k.substring(22));
            }
        } else {
            this.b = new FileInputStream(this.k.substring(7));
        }
        if (this.f1294a == null && this.b == null) {
            return false;
        }
        return true;
    }

    private boolean c() throws Exception {
        OutputStream outputStream;
        InputStream inputStream;
        String b2 = this.c.b();
        if (this.b != null) {
            outputStream = this.i.length() == 0 ? new ByteArrayOutputStream(4096) : new FileOutputStream(new File(this.i).getAbsolutePath());
        } else if (b2.equals("WebServices.download")) {
            this.b = this.f1294a.getInputStream();
            outputStream = new FileOutputStream(this.j);
        } else if (b2.equals("WebServices.get")) {
            this.b = this.f1294a.getInputStream();
            outputStream = new ByteArrayOutputStream(4096);
        } else if (b2.equals("WebServices.post")) {
            this.f1294a.connect();
            if (this.f1294a.getResponseCode() < 200 || this.f1294a.getResponseCode() > 299) {
                inputStream = this.f1294a.getErrorStream();
            } else {
                inputStream = this.f1294a.getInputStream();
            }
            this.b = inputStream;
            outputStream = new ByteArrayOutputStream(4096);
        } else {
            outputStream = null;
        }
        HttpURLConnection httpURLConnection = this.f1294a;
        if (httpURLConnection != null) {
            this.o = httpURLConnection.getResponseCode();
            this.h = this.f1294a.getHeaderFields();
        }
        return a(this.b, outputStream);
    }

    /* access modifiers changed from: package-private */
    public y a() {
        return this.c;
    }

    public void run() {
        boolean z = false;
        this.m = false;
        try {
            if (b()) {
                this.m = c();
                if (this.c.b().equals("WebServices.post") && this.o != 200) {
                    this.m = false;
                }
            }
        } catch (MalformedURLException e2) {
            new v.a().a("MalformedURLException: ").a(e2.toString()).a(v.i);
            this.m = true;
        } catch (OutOfMemoryError unused) {
            v.a a2 = new v.a().a("Out of memory error - disabling AdColony. (").a(this.n).a("/").a(this.f);
            a2.a("): " + this.k).a(v.h);
            a.c().a(true);
        } catch (IOException e3) {
            new v.a().a("Download of ").a(this.k).a(" failed: ").a(e3.toString()).a(v.g);
            int i2 = this.o;
            if (i2 == 0) {
                i2 = 504;
            }
            this.o = i2;
        } catch (IllegalStateException e4) {
            new v.a().a("okhttp error: ").a(e4.toString()).a(v.h);
            e4.printStackTrace();
        } catch (Exception e5) {
            new v.a().a("Exception: ").a(e5.toString()).a(v.h);
            e5.printStackTrace();
        }
        z = true;
        if (z) {
            if (this.c.b().equals("WebServices.download")) {
                a(this.j, this.i);
            }
            this.d.a(this, this.c, this.h);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0059, code lost:
        r0 = r8.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x005b, code lost:
        if (r0 == null) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0061, code lost:
        if (r0.isEmpty() != false) goto L_0x0066;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0063, code lost:
        r0 = r8.e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x0066, code lost:
        r0 = "UTF-8";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x006a, code lost:
        if ((r10 instanceof java.io.ByteArrayOutputStream) == false) goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x006c, code lost:
        r8.l = ((java.io.ByteArrayOutputStream) r10).toString(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0076, code lost:
        if (r10 == null) goto L_0x007b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0078, code lost:
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x007b, code lost:
        if (r9 == null) goto L_0x0080;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x007d, code lost:
        r9.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x0080, code lost:
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0083, code lost:
        return true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(java.io.InputStream r9, java.io.OutputStream r10) throws java.lang.Exception {
        /*
            r8 = this;
            r0 = 0
            java.io.BufferedInputStream r1 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x0088, all -> 0x0086 }
            r0 = 4096(0x1000, float:5.74E-42)
            byte[] r2 = new byte[r0]     // Catch:{ Exception -> 0x0084 }
        L_0x000a:
            r3 = 0
            int r4 = r1.read(r2, r3, r0)     // Catch:{ Exception -> 0x0084 }
            r5 = -1
            if (r4 == r5) goto L_0x0059
            int r5 = r8.n     // Catch:{ Exception -> 0x0084 }
            int r5 = r5 + r4
            r8.n = r5     // Catch:{ Exception -> 0x0084 }
            boolean r6 = r8.g     // Catch:{ Exception -> 0x0084 }
            if (r6 == 0) goto L_0x0055
            int r6 = r8.f     // Catch:{ Exception -> 0x0084 }
            if (r5 > r6) goto L_0x0020
            goto L_0x0055
        L_0x0020:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ Exception -> 0x0084 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0084 }
            r2.<init>()     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = "Data exceeds expected maximum ("
            r2.append(r3)     // Catch:{ Exception -> 0x0084 }
            int r3 = r8.n     // Catch:{ Exception -> 0x0084 }
            r2.append(r3)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = "/"
            r2.append(r3)     // Catch:{ Exception -> 0x0084 }
            int r3 = r8.f     // Catch:{ Exception -> 0x0084 }
            r2.append(r3)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = "): "
            r2.append(r3)     // Catch:{ Exception -> 0x0084 }
            java.net.HttpURLConnection r3 = r8.f1294a     // Catch:{ Exception -> 0x0084 }
            java.net.URL r3 = r3.getURL()     // Catch:{ Exception -> 0x0084 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0084 }
            r2.append(r3)     // Catch:{ Exception -> 0x0084 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0084 }
            r0.<init>(r2)     // Catch:{ Exception -> 0x0084 }
            throw r0     // Catch:{ Exception -> 0x0084 }
        L_0x0055:
            r10.write(r2, r3, r4)     // Catch:{ Exception -> 0x0084 }
            goto L_0x000a
        L_0x0059:
            java.lang.String r0 = r8.e     // Catch:{ Exception -> 0x0084 }
            if (r0 == 0) goto L_0x0066
            boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x0084 }
            if (r0 != 0) goto L_0x0066
            java.lang.String r0 = r8.e     // Catch:{ Exception -> 0x0084 }
            goto L_0x0068
        L_0x0066:
            java.lang.String r0 = "UTF-8"
        L_0x0068:
            boolean r2 = r10 instanceof java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0084 }
            if (r2 == 0) goto L_0x0075
            r2 = r10
            java.io.ByteArrayOutputStream r2 = (java.io.ByteArrayOutputStream) r2     // Catch:{ Exception -> 0x0084 }
            java.lang.String r0 = r2.toString(r0)     // Catch:{ Exception -> 0x0084 }
            r8.l = r0     // Catch:{ Exception -> 0x0084 }
        L_0x0075:
            r0 = 1
            if (r10 == 0) goto L_0x007b
            r10.close()
        L_0x007b:
            if (r9 == 0) goto L_0x0080
            r9.close()
        L_0x0080:
            r1.close()
            return r0
        L_0x0084:
            r0 = move-exception
            goto L_0x008c
        L_0x0086:
            r1 = move-exception
            goto L_0x0091
        L_0x0088:
            r1 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x008c:
            throw r0     // Catch:{ all -> 0x008d }
        L_0x008d:
            r0 = move-exception
            r7 = r1
            r1 = r0
            r0 = r7
        L_0x0091:
            if (r10 == 0) goto L_0x0096
            r10.close()
        L_0x0096:
            if (r9 == 0) goto L_0x009b
            r9.close()
        L_0x009b:
            if (r0 == 0) goto L_0x00a0
            r0.close()
        L_0x00a0:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.m.a(java.io.InputStream, java.io.OutputStream):boolean");
    }

    private void a(String str, String str2) {
        try {
            String substring = str2.substring(0, str2.lastIndexOf("/") + 1);
            if (!str2.equals("") && !substring.equals(a.c().x().d()) && !new File(str).renameTo(new File(str2))) {
                new v.a().a("Moving of ").a(str).a(" failed.").a(v.g);
            }
        } catch (Exception e2) {
            new v.a().a("Exception: ").a(e2.toString()).a(v.h);
            e2.printStackTrace();
        }
    }
}
