package com.adcolony.sdk;

import org.json.JSONObject;

class j implements a0 {

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1249a;
        final /* synthetic */ String b;

        a(j jVar, String str, String str2) {
            this.f1249a = str;
            this.b = str2;
        }

        public void run() {
            try {
                AdColonyCustomMessageListener adColonyCustomMessageListener = a.c().j().get(this.f1249a);
                if (adColonyCustomMessageListener != null) {
                    adColonyCustomMessageListener.a(new AdColonyCustomMessage(this.f1249a, this.b));
                }
            } catch (RuntimeException unused) {
            }
        }
    }

    j() {
        a.a("CustomMessage.controller_send", (a0) this);
    }

    public void a(y yVar) {
        JSONObject a2 = yVar.a();
        l0.a((Runnable) new a(this, t.g(a2, "type"), t.g(a2, "message")));
    }
}
