package com.adcolony.sdk;

public class AdColonyAdSize {
    public static final AdColonyAdSize c = new AdColonyAdSize(320, 50);

    /* renamed from: a  reason: collision with root package name */
    int f1161a;
    int b;

    static {
        new AdColonyAdSize(300, 250);
        new AdColonyAdSize(728, 90);
        new AdColonyAdSize(160, 600);
    }

    public AdColonyAdSize(int i, int i2) {
        this.f1161a = i;
        this.b = i2;
    }

    public int a() {
        return this.b;
    }

    public int b() {
        return this.f1161a;
    }
}
