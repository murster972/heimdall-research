package com.adcolony.sdk;

import com.adcolony.sdk.v;
import com.vungle.warren.model.ReportDBAdapter;
import org.json.JSONObject;

public class AdColonyZone {

    /* renamed from: a  reason: collision with root package name */
    private int f1169a = 5;
    private int b;
    private int c;
    private int d;
    private boolean e;

    AdColonyZone(String str) {
    }

    private int c(int i) {
        if (a.e() && !a.c().a() && !a.c().b()) {
            return i;
        }
        e();
        return 0;
    }

    private void e() {
        new v.a().a("The AdColonyZone API is not available while AdColony is disabled.").a(v.h);
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.d = i;
    }

    /* access modifiers changed from: package-private */
    public void b(int i) {
        this.f1169a = i;
    }

    public boolean d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        JSONObject a2 = yVar.a();
        JSONObject f = t.f(a2, "reward");
        t.g(f, "reward_name");
        t.e(f, "reward_amount");
        t.e(f, "views_per_reward");
        t.e(f, "views_until_reward");
        this.e = t.c(a2, "rewarded");
        this.f1169a = t.e(a2, ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
        this.b = t.e(a2, "type");
        this.c = t.e(a2, "play_interval");
        t.g(a2, "zone_id");
        int i = this.f1169a;
    }

    public int b() {
        return c(this.c);
    }

    public int c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.d;
    }
}
