package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import com.adcolony.sdk.v;
import org.json.JSONObject;

class l {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public y f1285a;
    /* access modifiers changed from: private */
    public AlertDialog b;
    /* access modifiers changed from: private */
    public boolean c;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            if (!a.d() || !(a.b() instanceof Activity)) {
                new v.a().a("Missing Activity reference, can't build AlertDialog.").a(v.i);
            } else if (t.c(yVar.a(), "on_resume")) {
                y unused = l.this.f1285a = yVar;
            } else {
                l.this.a(yVar);
            }
        }
    }

    class b implements DialogInterface.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ y f1287a;

        b(y yVar) {
            this.f1287a = yVar;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            AlertDialog unused = l.this.b = null;
            dialogInterface.dismiss();
            JSONObject b2 = t.b();
            t.a(b2, "positive", true);
            boolean unused2 = l.this.c = false;
            this.f1287a.a(b2).c();
        }
    }

    class c implements DialogInterface.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ y f1288a;

        c(y yVar) {
            this.f1288a = yVar;
        }

        public void onClick(DialogInterface dialogInterface, int i) {
            AlertDialog unused = l.this.b = null;
            dialogInterface.dismiss();
            JSONObject b2 = t.b();
            t.a(b2, "positive", false);
            boolean unused2 = l.this.c = false;
            this.f1288a.a(b2).c();
        }
    }

    class d implements DialogInterface.OnCancelListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ y f1289a;

        d(y yVar) {
            this.f1289a = yVar;
        }

        public void onCancel(DialogInterface dialogInterface) {
            AlertDialog unused = l.this.b = null;
            boolean unused2 = l.this.c = false;
            JSONObject b2 = t.b();
            t.a(b2, "positive", false);
            this.f1289a.a(b2).c();
        }
    }

    class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AlertDialog.Builder f1290a;

        e(AlertDialog.Builder builder) {
            this.f1290a = builder;
        }

        public void run() {
            boolean unused = l.this.c = true;
            AlertDialog unused2 = l.this.b = this.f1290a.show();
        }
    }

    l() {
        a.a("Alert.show", (a0) new a());
    }

    /* access modifiers changed from: package-private */
    public void c() {
        y yVar = this.f1285a;
        if (yVar != null) {
            a(yVar);
            this.f1285a = null;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.c;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"InlinedApi"})
    public void a(y yVar) {
        Context b2 = a.b();
        if (b2 != null) {
            AlertDialog.Builder builder = Build.VERSION.SDK_INT >= 21 ? new AlertDialog.Builder(b2, 16974374) : new AlertDialog.Builder(b2, 16974126);
            JSONObject a2 = yVar.a();
            String g = t.g(a2, "message");
            String g2 = t.g(a2, "title");
            String g3 = t.g(a2, "positive");
            String g4 = t.g(a2, "negative");
            builder.setMessage(g);
            builder.setTitle(g2);
            builder.setPositiveButton(g3, new b(yVar));
            if (!g4.equals("")) {
                builder.setNegativeButton(g4, new c(yVar));
            }
            builder.setOnCancelListener(new d(yVar));
            l0.a((Runnable) new e(builder));
        }
    }

    /* access modifiers changed from: package-private */
    public AlertDialog a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void a(AlertDialog alertDialog) {
        this.b = alertDialog;
    }
}
