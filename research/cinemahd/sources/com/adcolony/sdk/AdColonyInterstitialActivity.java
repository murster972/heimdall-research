package com.adcolony.sdk;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyInterstitialActivity extends b {
    AdColonyInterstitial j;
    private h k;

    public AdColonyInterstitialActivity() {
        AdColonyInterstitial adColonyInterstitial;
        if (!a.e()) {
            adColonyInterstitial = null;
        } else {
            adColonyInterstitial = a.c().g();
        }
        this.j = adColonyInterstitial;
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        AdColonyInterstitial adColonyInterstitial;
        super.a(yVar);
        d e = a.c().e();
        JSONObject f = t.f(yVar.a(), "v4iap");
        JSONArray b = t.b(f, "product_ids");
        if (!(f == null || (adColonyInterstitial = this.j) == null || adColonyInterstitial.g() == null || b.length() <= 0)) {
            this.j.g().a(this.j, t.b(b, 0), t.e(f, "engagement_type"));
        }
        e.a(this.f1172a);
        if (this.j != null) {
            e.a().remove(this.j.b());
        }
        AdColonyInterstitial adColonyInterstitial2 = this.j;
        if (!(adColonyInterstitial2 == null || adColonyInterstitial2.g() == null)) {
            this.j.g().d(this.j);
            this.j.a((c) null);
            this.j.a((AdColonyInterstitialListener) null);
            this.j = null;
        }
        h hVar = this.k;
        if (hVar != null) {
            hVar.a();
            this.k = null;
        }
    }

    public void onCreate(Bundle bundle) {
        int i;
        AdColonyInterstitial adColonyInterstitial;
        AdColonyInterstitial adColonyInterstitial2 = this.j;
        if (adColonyInterstitial2 == null) {
            i = -1;
        } else {
            i = adColonyInterstitial2.f();
        }
        this.b = i;
        super.onCreate(bundle);
        if (a.e() && (adColonyInterstitial = this.j) != null) {
            d0 e = adColonyInterstitial.e();
            if (e != null) {
                e.a(this.f1172a);
            }
            this.k = new h(new Handler(Looper.getMainLooper()), this.j);
            if (this.j.g() != null) {
                this.j.g().g(this.j);
            }
        }
    }
}
