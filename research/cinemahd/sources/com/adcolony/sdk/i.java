package com.adcolony.sdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.adcolony.sdk.m;
import com.adcolony.sdk.v;
import com.facebook.common.util.UriUtil;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.iab.omid.library.adcolony.Omid;
import com.iab.omid.library.adcolony.adsession.Partner;
import com.unity3d.ads.metadata.MediationMetaData;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import org.json.JSONArray;
import org.json.JSONObject;

class i implements m.a {
    static String P = "https://adc3-launch.adcolony.com/v4/launch";
    private static volatile String Q = "";
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    private boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    /* access modifiers changed from: private */
    public boolean E;
    private boolean F;
    private boolean G;
    private boolean H;
    /* access modifiers changed from: private */
    public boolean I;
    /* access modifiers changed from: private */
    public boolean J;
    /* access modifiers changed from: private */
    public boolean K;
    private int L;
    /* access modifiers changed from: private */
    public int M = 1;
    private Application.ActivityLifecycleCallbacks N;
    /* access modifiers changed from: private */
    public Partner O = null;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public z f1228a;
    /* access modifiers changed from: private */
    public n b;
    /* access modifiers changed from: private */
    public g0 c;
    private d d;
    /* access modifiers changed from: private */
    public l e;
    private q f;
    private j0 g;
    private h0 h;
    private x i;
    private k j;
    private c0 k;
    private c l;
    private AdColonyAdView m;
    private AdColonyInterstitial n;
    /* access modifiers changed from: private */
    public AdColonyRewardListener o;
    private HashMap<String, AdColonyCustomMessageListener> p = new HashMap<>();
    /* access modifiers changed from: private */
    public AdColonyAppOptions q;
    /* access modifiers changed from: private */
    public y r;
    private JSONObject s;
    private HashMap<String, AdColonyZone> t = new HashMap<>();
    /* access modifiers changed from: private */
    public HashMap<Integer, n0> u = new HashMap<>();
    private String v;
    private String w;
    private String x;
    private String y;
    private String z = "";

    class a implements a0 {
        a(i iVar) {
        }

        public void a(y yVar) {
            int e = t.e(yVar.a(), "number");
            JSONObject b = t.b();
            t.a(b, "uuids", l0.a(e));
            yVar.a(b).c();
        }
    }

    class b implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ Context f1230a;
            final /* synthetic */ y b;

            a(Context context, y yVar) {
                this.f1230a = context;
                this.b = yVar;
            }

            public void run() {
                i.this.a(this.f1230a, this.b);
            }
        }

        b() {
        }

        public void a(y yVar) {
            Context b = a.b();
            if (b != null) {
                l0.f1291a.execute(new a(b, yVar));
            }
        }
    }

    class c implements a0 {
        c() {
        }

        public void a(y yVar) {
            f0 a2 = i.this.o().a();
            i.this.k().b(t.g(yVar.a(), MediationMetaData.KEY_VERSION));
            if (a2 != null) {
                a2.e(i.this.k().t());
            }
        }
    }

    class d implements Runnable {
        d() {
        }

        public void run() {
            Context b = a.b();
            if (!i.this.K && b != null) {
                try {
                    Omid.a(b.getApplicationContext());
                    boolean unused = i.this.K = true;
                } catch (IllegalArgumentException unused2) {
                    new v.a().a("IllegalArgumentException when activating Omid").a(v.i);
                    boolean unused3 = i.this.K = false;
                }
            }
            if (i.this.K && i.this.O == null) {
                try {
                    Partner unused4 = i.this.O = Partner.a("AdColony", "4.4.0");
                } catch (IllegalArgumentException unused5) {
                    new v.a().a("IllegalArgumentException when creating Omid Partner").a(v.i);
                    boolean unused6 = i.this.K = false;
                }
            }
        }
    }

    class e implements Runnable {
        e() {
        }

        public void run() {
            JSONObject b = t.b();
            t.a(b, ReportDBAdapter.ReportColumns.COLUMN_URL, i.P);
            t.a(b, "content_type", TraktV2.CONTENT_TYPE_JSON);
            JSONObject a2 = i.this.k().a(true);
            l0.a(a2);
            t.a(b, "content", a2.toString());
            i.this.b.a(new m(new y("WebServices.post", 0, b), i.this));
        }
    }

    class f implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1234a;
        final /* synthetic */ boolean b;
        final /* synthetic */ y c;

        f(Context context, boolean z, y yVar) {
            this.f1234a = context;
            this.b = z;
            this.c = yVar;
        }

        public void run() {
            n0 n0Var = new n0(this.f1234a.getApplicationContext(), i.this.f1228a.d(), this.b);
            n0Var.a(true, this.c);
            i.this.u.put(Integer.valueOf(n0Var.d()), n0Var);
        }
    }

    class g implements Runnable {

        class a implements Runnable {
            a() {
            }

            public void run() {
                if (a.c().w().c()) {
                    i.this.C();
                }
            }
        }

        g() {
        }

        public void run() {
            new Handler().postDelayed(new a(), (long) (i.this.M * 1000));
        }
    }

    class h implements Runnable {
        h() {
        }

        public void run() {
            boolean unused = i.this.D();
        }
    }

    /* renamed from: com.adcolony.sdk.i$i  reason: collision with other inner class name */
    class C0004i implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ n0 f1238a;

        C0004i(i iVar, n0 n0Var) {
            this.f1238a = n0Var;
        }

        public void run() {
            n0 n0Var = this.f1238a;
            if (n0Var != null && n0Var.w()) {
                this.f1238a.loadUrl("about:blank");
                this.f1238a.clearCache(true);
                this.f1238a.removeAllViews();
                this.f1238a.a(true);
                this.f1238a.destroy();
            }
        }
    }

    class j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ y f1239a;

        j(y yVar) {
            this.f1239a = yVar;
        }

        public void run() {
            i.this.o.a(new AdColonyReward(this.f1239a));
        }
    }

    class k implements a0 {
        k() {
        }

        public void a(y yVar) {
            boolean unused = i.this.d(yVar);
        }
    }

    class l implements Application.ActivityLifecycleCallbacks {
        l() {
        }

        public void onActivityCreated(Activity activity, Bundle bundle) {
            if (!i.this.c.c()) {
                i.this.c.c(true);
            }
            a.a(activity);
        }

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
            a.d = false;
            i.this.c.d(false);
            i.this.c.e(true);
        }

        public void onActivityResumed(Activity activity) {
            ScheduledExecutorService scheduledExecutorService;
            a.d = true;
            a.a(activity);
            f0 a2 = i.this.o().a();
            Context b = a.b();
            if (b == null || !i.this.c.b() || !(b instanceof b) || ((b) b).d) {
                a.a(activity);
                if (i.this.r != null) {
                    i.this.r.a(i.this.r.a()).c();
                    y unused = i.this.r = null;
                }
                boolean unused2 = i.this.B = false;
                i.this.c.d(true);
                i.this.c.e(true);
                i.this.c.f(false);
                if (i.this.E && !i.this.c.c()) {
                    i.this.c.c(true);
                }
                i.this.e.c();
                if (a2 == null || (scheduledExecutorService = a2.b) == null || scheduledExecutorService.isShutdown() || a2.b.isTerminated()) {
                    AdColony.a((Context) activity, a.c().q);
                }
            }
        }

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public void onActivityStarted(Activity activity) {
        }

        public void onActivityStopped(Activity activity) {
        }
    }

    class m implements a0 {
        m() {
        }

        public void a(y yVar) {
            i.this.b(yVar);
        }
    }

    class n implements a0 {
        n() {
        }

        public void a(y yVar) {
            f0 a2 = i.this.o().a();
            boolean unused = i.this.D = true;
            if (i.this.I) {
                JSONObject b = t.b();
                JSONObject b2 = t.b();
                t.a(b2, "app_version", l0.c());
                t.a(b, "app_bundle_info", b2);
                new y("AdColony.on_update", 1, b).c();
                boolean unused2 = i.this.I = false;
            }
            if (i.this.J) {
                new y("AdColony.on_install", 1).c();
            }
            if (a2 != null) {
                a2.f(t.g(yVar.a(), "app_session_id"));
            }
            if (AdColonyEventTracker.a()) {
                AdColonyEventTracker.b();
            }
            int a3 = t.a(yVar.a(), "concurrent_requests", 4);
            if (a3 != i.this.b.a()) {
                i.this.b.a(a3);
            }
            i.this.E();
        }
    }

    class o implements a0 {
        o() {
        }

        public void a(y yVar) {
            i.this.c(yVar);
        }
    }

    class p implements a0 {
        p() {
        }

        public void a(y yVar) {
            boolean unused = i.this.e(yVar);
        }
    }

    class q implements a0 {
        q() {
        }

        public void a(y yVar) {
            i.this.f(yVar);
        }
    }

    class r implements a0 {
        r() {
        }

        public void a(y yVar) {
            boolean unused = i.this.a(true, true);
        }
    }

    class s implements a0 {
        s(i iVar) {
        }

        public void a(y yVar) {
            JSONObject b = t.b();
            t.a(b, "sha1", l0.b(t.g(yVar.a(), UriUtil.DATA_SCHEME)));
            yVar.a(b).c();
        }
    }

    class t implements a0 {
        t(i iVar) {
        }

        public void a(y yVar) {
            JSONObject b = t.b();
            t.b(b, "crc32", l0.a(t.g(yVar.a(), UriUtil.DATA_SCHEME)));
            yVar.a(b).c();
        }
    }

    i() {
    }

    /* access modifiers changed from: private */
    public void C() {
        new Thread(new e()).start();
    }

    /* access modifiers changed from: private */
    public boolean D() {
        this.f1228a.a();
        return true;
    }

    /* access modifiers changed from: private */
    public void E() {
        JSONObject b2 = t.b();
        t.a(b2, "type", "AdColony.on_configuration_completed");
        JSONArray jSONArray = new JSONArray();
        for (String put : A().keySet()) {
            jSONArray.put(put);
        }
        JSONObject b3 = t.b();
        t.a(b3, "zone_ids", jSONArray);
        t.a(b2, "message", b3);
        new y("CustomMessage.controller_send", 0, b2).c();
    }

    private void F() {
        Application application;
        Context b2 = a.b();
        if (b2 != null && this.N == null && Build.VERSION.SDK_INT > 14) {
            this.N = new l();
            if (b2 instanceof Application) {
                application = (Application) b2;
            } else {
                application = ((Activity) b2).getApplication();
            }
            application.registerActivityLifecycleCallbacks(this.N);
        }
    }

    private void G() {
        if (a.c().w().c()) {
            int i2 = this.L + 1;
            this.L = i2;
            this.M = Math.min(this.M * i2, 120);
            l0.a((Runnable) new g());
            return;
        }
        new v.a().a("Max launch server download attempts hit, or AdColony is no longer").a(" active.").a(v.g);
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyZone> A() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public boolean B() {
        return this.q != null;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.B;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.C;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.D;
    }

    /* access modifiers changed from: package-private */
    public boolean d() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public String r() {
        return this.v;
    }

    /* access modifiers changed from: package-private */
    public Partner s() {
        return this.O;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions t() {
        if (this.q == null) {
            this.q = new AdColonyAppOptions();
        }
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public String u() {
        return Q;
    }

    /* access modifiers changed from: package-private */
    public AdColonyRewardListener v() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public g0 w() {
        if (this.c == null) {
            g0 g0Var = new g0();
            this.c = g0Var;
            g0Var.a();
        }
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public h0 x() {
        if (this.h == null) {
            h0 h0Var = new h0();
            this.h = h0Var;
            h0Var.e();
        }
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public j0 y() {
        if (this.g == null) {
            j0 j0Var = new j0();
            this.g = j0Var;
            j0Var.a();
        }
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, n0> z() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public AdColonyInterstitial g() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public AdColonyAdView h() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public c i() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyCustomMessageListener> j() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public k k() {
        if (this.j == null) {
            k kVar = new k();
            this.j = kVar;
            kVar.h();
        }
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public l l() {
        if (this.e == null) {
            this.e = new l();
        }
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public n m() {
        if (this.b == null) {
            this.b = new n();
        }
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public q n() {
        if (this.f == null) {
            q qVar = new q();
            this.f = qVar;
            qVar.a();
        }
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public x o() {
        if (this.i == null) {
            x xVar = new x();
            this.i = xVar;
            xVar.b();
        }
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public z p() {
        if (this.f1228a == null) {
            z zVar = new z();
            this.f1228a = zVar;
            zVar.a();
        }
        return this.f1228a;
    }

    /* access modifiers changed from: package-private */
    public c0 q() {
        if (this.k == null) {
            this.k = new c0();
        }
        return this.k;
    }

    /* access modifiers changed from: private */
    public boolean e(y yVar) {
        if (this.o == null) {
            return false;
        }
        l0.a((Runnable) new j(yVar));
        return true;
    }

    /* access modifiers changed from: private */
    public void f(y yVar) {
        AdColonyZone adColonyZone;
        if (!this.C) {
            String g2 = t.g(yVar.a(), "zone_id");
            if (this.t.containsKey(g2)) {
                adColonyZone = this.t.get(g2);
            } else {
                AdColonyZone adColonyZone2 = new AdColonyZone(g2);
                this.t.put(g2, adColonyZone2);
                adColonyZone = adColonyZone2;
            }
            adColonyZone.a(yVar);
        }
    }

    private void b(JSONObject jSONObject) {
        if (!n0.O) {
            JSONObject f2 = t.f(jSONObject, "logging");
            x.g = t.a(f2, "send_level", 1);
            x.e = t.c(f2, "log_private");
            x.f = t.a(f2, "print_level", 3);
            this.i.b(t.b(f2, "modules"));
        }
        JSONObject f3 = t.f(jSONObject, "metadata");
        k().a(f3);
        w().a(t.e(f3, "session_timeout"));
        this.z = t.g(t.f(jSONObject, "controller"), MediationMetaData.KEY_VERSION);
    }

    /* access modifiers changed from: private */
    public void c(y yVar) {
        JSONObject b2 = this.q.b();
        t.a(b2, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_APP_ID, this.q.a());
        t.a(b2, "zone_ids", this.q.d());
        JSONObject b3 = t.b();
        t.a(b3, "options", b2);
        yVar.a(b3).c();
    }

    /* access modifiers changed from: private */
    public boolean d(y yVar) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        try {
            int e2 = yVar.a().has("id") ? t.e(yVar.a(), "id") : 0;
            if (e2 <= 0) {
                e2 = this.f1228a.d();
            }
            a(e2);
            l0.a((Runnable) new f(b2, t.c(yVar.a(), "is_display_module"), yVar));
            return true;
        } catch (RuntimeException e3) {
            v.a aVar = new v.a();
            aVar.a(e3.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(v.h);
            AdColony.d();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public d e() {
        if (this.d == null) {
            d dVar = new d();
            this.d = dVar;
            dVar.e();
        }
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.C = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        this.r = yVar;
    }

    private boolean e(boolean z2) {
        return a(z2, false);
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x00fb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.adcolony.sdk.AdColonyAppOptions r5, boolean r6) {
        /*
            r4 = this;
            r4.C = r6
            r4.q = r5
            com.adcolony.sdk.z r0 = new com.adcolony.sdk.z
            r0.<init>()
            r4.f1228a = r0
            com.adcolony.sdk.j r0 = new com.adcolony.sdk.j
            r0.<init>()
            com.adcolony.sdk.k r0 = new com.adcolony.sdk.k
            r0.<init>()
            r4.j = r0
            r0.h()
            com.adcolony.sdk.n r0 = new com.adcolony.sdk.n
            r0.<init>()
            r4.b = r0
            r0.b()
            com.adcolony.sdk.g0 r0 = new com.adcolony.sdk.g0
            r0.<init>()
            r4.c = r0
            r0.a()
            com.adcolony.sdk.d r0 = new com.adcolony.sdk.d
            r0.<init>()
            r4.d = r0
            r0.e()
            com.adcolony.sdk.l r0 = new com.adcolony.sdk.l
            r0.<init>()
            r4.e = r0
            com.adcolony.sdk.q r0 = new com.adcolony.sdk.q
            r0.<init>()
            r4.f = r0
            r0.a()
            com.adcolony.sdk.x r0 = new com.adcolony.sdk.x
            r0.<init>()
            r4.i = r0
            r0.b()
            com.adcolony.sdk.h0 r0 = new com.adcolony.sdk.h0
            r0.<init>()
            r4.h = r0
            r0.e()
            com.adcolony.sdk.j0 r0 = new com.adcolony.sdk.j0
            r0.<init>()
            r4.g = r0
            r0.a()
            com.adcolony.sdk.c0 r0 = new com.adcolony.sdk.c0
            r0.<init>()
            r4.k = r0
            java.lang.String r0 = r0.a()
            r4.v = r0
            android.content.Context r0 = com.adcolony.sdk.a.b()
            com.adcolony.sdk.AdColony.a((android.content.Context) r0, (com.adcolony.sdk.AdColonyAppOptions) r5)
            r5 = 0
            r0 = 1
            if (r6 != 0) goto L_0x0121
            java.io.File r6 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.adcolony.sdk.h0 r2 = r4.h
            java.lang.String r2 = r2.a()
            r1.append(r2)
            java.lang.String r2 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r6.<init>(r1)
            boolean r6 = r6.exists()
            r4.G = r6
            java.io.File r6 = new java.io.File
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            com.adcolony.sdk.h0 r3 = r4.h
            java.lang.String r3 = r3.a()
            r1.append(r3)
            java.lang.String r3 = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5"
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r6.<init>(r1)
            boolean r6 = r6.exists()
            boolean r1 = r4.G
            if (r1 == 0) goto L_0x00f4
            if (r6 == 0) goto L_0x00f4
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.adcolony.sdk.h0 r1 = r4.h
            java.lang.String r1 = r1.a()
            r6.append(r1)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            org.json.JSONObject r6 = com.adcolony.sdk.t.c(r6)
            java.lang.String r1 = "sdkVersion"
            java.lang.String r6 = com.adcolony.sdk.t.g(r6, r1)
            com.adcolony.sdk.k r1 = r4.j
            java.lang.String r1 = r1.c()
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00f4
            r6 = 1
            goto L_0x00f5
        L_0x00f4:
            r6 = 0
        L_0x00f5:
            r4.F = r6
            boolean r6 = r4.G
            if (r6 == 0) goto L_0x0119
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            com.adcolony.sdk.h0 r1 = r4.h
            java.lang.String r1 = r1.a()
            r6.append(r1)
            r6.append(r2)
            java.lang.String r6 = r6.toString()
            org.json.JSONObject r6 = com.adcolony.sdk.t.c(r6)
            r4.s = r6
            r4.b((org.json.JSONObject) r6)
        L_0x0119:
            boolean r6 = r4.F
            r4.e((boolean) r6)
            r4.F()
        L_0x0121:
            com.adcolony.sdk.i$k r6 = new com.adcolony.sdk.i$k
            r6.<init>()
            java.lang.String r1 = "Module.load"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$m r6 = new com.adcolony.sdk.i$m
            r6.<init>()
            java.lang.String r1 = "Module.unload"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$n r6 = new com.adcolony.sdk.i$n
            r6.<init>()
            java.lang.String r1 = "AdColony.on_configured"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$o r6 = new com.adcolony.sdk.i$o
            r6.<init>()
            java.lang.String r1 = "AdColony.get_app_info"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$p r6 = new com.adcolony.sdk.i$p
            r6.<init>()
            java.lang.String r1 = "AdColony.v4vc_reward"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$q r6 = new com.adcolony.sdk.i$q
            r6.<init>()
            java.lang.String r1 = "AdColony.zone_info"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$r r6 = new com.adcolony.sdk.i$r
            r6.<init>()
            java.lang.String r1 = "AdColony.probe_launch_server"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$s r6 = new com.adcolony.sdk.i$s
            r6.<init>(r4)
            java.lang.String r1 = "Crypto.sha1"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$t r6 = new com.adcolony.sdk.i$t
            r6.<init>(r4)
            java.lang.String r1 = "Crypto.crc32"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$a r6 = new com.adcolony.sdk.i$a
            r6.<init>(r4)
            java.lang.String r1 = "Crypto.uuid"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$b r6 = new com.adcolony.sdk.i$b
            r6.<init>()
            java.lang.String r1 = "Device.query_advertiser_info"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.i$c r6 = new com.adcolony.sdk.i$c
            r6.<init>()
            java.lang.String r1 = "AdColony.controller_version"
            com.adcolony.sdk.a.a((java.lang.String) r1, (com.adcolony.sdk.a0) r6)
            com.adcolony.sdk.h0 r6 = r4.h
            int r6 = com.adcolony.sdk.l0.a((com.adcolony.sdk.h0) r6)
            if (r6 != r0) goto L_0x01a3
            r1 = 1
            goto L_0x01a4
        L_0x01a3:
            r1 = 0
        L_0x01a4:
            r4.I = r1
            r1 = 2
            if (r6 != r1) goto L_0x01aa
            r5 = 1
        L_0x01aa:
            r4.J = r5
            com.adcolony.sdk.i$d r5 = new com.adcolony.sdk.i$d
            r5.<init>()
            com.adcolony.sdk.l0.a((java.lang.Runnable) r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.i.a(com.adcolony.sdk.AdColonyAppOptions, boolean):void");
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return this.z;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        this.E = z2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        new java.io.File(r3.h.a() + "026ae9c9824b3e483fa6c71fa88f57ae27816141").delete();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0037 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean c(org.json.JSONObject r4) {
        /*
            r3 = this;
            r0 = 0
            if (r4 != 0) goto L_0x0004
            return r0
        L_0x0004:
            java.lang.String r1 = "controller"
            org.json.JSONObject r1 = com.adcolony.sdk.t.f(r4, r1)     // Catch:{ Exception -> 0x0037 }
            java.lang.String r2 = "url"
            java.lang.String r2 = com.adcolony.sdk.t.g(r1, r2)     // Catch:{ Exception -> 0x0037 }
            r3.w = r2     // Catch:{ Exception -> 0x0037 }
            java.lang.String r2 = "sha1"
            java.lang.String r1 = com.adcolony.sdk.t.g(r1, r2)     // Catch:{ Exception -> 0x0037 }
            r3.x = r1     // Catch:{ Exception -> 0x0037 }
            java.lang.String r1 = "status"
            java.lang.String r1 = com.adcolony.sdk.t.g(r4, r1)     // Catch:{ Exception -> 0x0037 }
            r3.y = r1     // Catch:{ Exception -> 0x0037 }
            java.lang.String r1 = "pie"
            java.lang.String r1 = com.adcolony.sdk.t.g(r4, r1)     // Catch:{ Exception -> 0x0037 }
            Q = r1     // Catch:{ Exception -> 0x0037 }
            boolean r1 = com.adcolony.sdk.AdColonyEventTracker.a()     // Catch:{ Exception -> 0x0037 }
            if (r1 == 0) goto L_0x0033
            com.adcolony.sdk.AdColonyEventTracker.b()     // Catch:{ Exception -> 0x0037 }
        L_0x0033:
            r3.b((org.json.JSONObject) r4)     // Catch:{ Exception -> 0x0037 }
            goto L_0x0058
        L_0x0037:
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0057 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0057 }
            r1.<init>()     // Catch:{ Exception -> 0x0057 }
            com.adcolony.sdk.h0 r2 = r3.h     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = r2.a()     // Catch:{ Exception -> 0x0057 }
            r1.append(r2)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r2 = "026ae9c9824b3e483fa6c71fa88f57ae27816141"
            r1.append(r2)     // Catch:{ Exception -> 0x0057 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0057 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0057 }
            r4.delete()     // Catch:{ Exception -> 0x0057 }
            goto L_0x0058
        L_0x0057:
        L_0x0058:
            java.lang.String r4 = r3.y
            java.lang.String r1 = "disable"
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x009f
            boolean r4 = com.adcolony.sdk.n0.O
            if (r4 != 0) goto L_0x009f
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x0085 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0085 }
            r1.<init>()     // Catch:{ Exception -> 0x0085 }
            com.adcolony.sdk.h0 r2 = r3.h     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = r2.a()     // Catch:{ Exception -> 0x0085 }
            r1.append(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r2 = "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5"
            r1.append(r2)     // Catch:{ Exception -> 0x0085 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x0085 }
            r4.<init>(r1)     // Catch:{ Exception -> 0x0085 }
            r4.delete()     // Catch:{ Exception -> 0x0085 }
        L_0x0085:
            com.adcolony.sdk.v$a r4 = new com.adcolony.sdk.v$a
            r4.<init>()
            java.lang.String r1 = "Launch server response with disabled status. Disabling AdColony "
            com.adcolony.sdk.v$a r4 = r4.a((java.lang.String) r1)
            java.lang.String r1 = "until next launch."
            com.adcolony.sdk.v$a r4 = r4.a((java.lang.String) r1)
            com.adcolony.sdk.v r1 = com.adcolony.sdk.v.g
            r4.a((com.adcolony.sdk.v) r1)
            com.adcolony.sdk.AdColony.d()
            return r0
        L_0x009f:
            java.lang.String r4 = r3.w
            java.lang.String r1 = ""
            boolean r4 = r4.equals(r1)
            if (r4 != 0) goto L_0x00b1
            java.lang.String r4 = r3.y
            boolean r4 = r4.equals(r1)
            if (r4 == 0) goto L_0x00cc
        L_0x00b1:
            boolean r4 = com.adcolony.sdk.n0.O
            if (r4 != 0) goto L_0x00cc
            com.adcolony.sdk.v$a r4 = new com.adcolony.sdk.v$a
            r4.<init>()
            java.lang.String r1 = "Missing controller status or URL. Disabling AdColony until next "
            com.adcolony.sdk.v$a r4 = r4.a((java.lang.String) r1)
            java.lang.String r1 = "launch."
            com.adcolony.sdk.v$a r4 = r4.a((java.lang.String) r1)
            com.adcolony.sdk.v r1 = com.adcolony.sdk.v.i
            r4.a((com.adcolony.sdk.v) r1)
            return r0
        L_0x00cc:
            r4 = 1
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.i.c(org.json.JSONObject):boolean");
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.B = z2;
    }

    /* access modifiers changed from: private */
    public void b(y yVar) {
        a(t.e(yVar.a(), "id"));
    }

    private boolean b(String str) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        File file = new File(b2.getFilesDir().getAbsolutePath() + "/adc3/" + "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
        if (file.exists()) {
            return l0.a(str, file);
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z2) {
        this.A = z2;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.v = str;
    }

    /* access modifiers changed from: private */
    public boolean a(boolean z2, boolean z3) {
        if (!a.d()) {
            return false;
        }
        this.H = z3;
        this.F = z2;
        if (z2 && !z3 && !D()) {
            return false;
        }
        C();
        return true;
    }

    private boolean a(JSONObject jSONObject) {
        if (!this.F) {
            return true;
        }
        JSONObject jSONObject2 = this.s;
        if (jSONObject2 != null && t.g(t.f(jSONObject2, "controller"), "sha1").equals(t.g(t.f(jSONObject, "controller"), "sha1"))) {
            return false;
        }
        new v.a().a("Controller sha1 does not match, downloading new controller.").a(v.g);
        return true;
    }

    public void a(m mVar, y yVar, Map<String, List<String>> map) {
        if (mVar.k.equals(P)) {
            if (mVar.m) {
                JSONObject a2 = t.a(mVar.l, "Parsing launch response");
                t.a(a2, "sdkVersion", k().c());
                t.h(a2, this.h.a() + "026ae9c9824b3e483fa6c71fa88f57ae27816141");
                if (c(a2)) {
                    if (a(a2)) {
                        JSONObject b2 = t.b();
                        t.a(b2, ReportDBAdapter.ReportColumns.COLUMN_URL, this.w);
                        t.a(b2, "filepath", this.h.a() + "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
                        this.b.a(new m(new y("WebServices.download", 0, b2), this));
                    }
                    this.s = a2;
                } else if (!this.F) {
                    new v.a().a("Incomplete or disabled launch server response. ").a("Disabling AdColony until next launch.").a(v.h);
                    a(true);
                }
            } else {
                G();
            }
        } else if (!mVar.k.equals(this.w)) {
        } else {
            if (!b(this.x) && !n0.O) {
                new v.a().a("Downloaded controller sha1 does not match, retrying.").a(v.f);
                G();
            } else if (!this.F && !this.H) {
                l0.a((Runnable) new h());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(Context context, y yVar) {
        boolean z2;
        if (context == null) {
            return false;
        }
        String str = "";
        AdvertisingIdClient.Info info2 = null;
        f0 a2 = o().a();
        try {
            info2 = AdvertisingIdClient.getAdvertisingIdInfo(context);
        } catch (NoClassDefFoundError unused) {
            new v.a().a("Google Play Services ads dependencies are missing. Collecting ").a("Android ID instead of Advertising ID.").a(v.f);
            return false;
        } catch (NoSuchMethodError unused2) {
            new v.a().a("Google Play Services is out of date, please update to GPS 4.0+. ").a("Collecting Android ID instead of Advertising ID.").a(v.f);
        } catch (Exception e2) {
            e2.printStackTrace();
            if (!Build.MANUFACTURER.equals("Amazon")) {
                new v.a().a("Advertising ID is not available. Collecting Android ID instead of").a(" Advertising ID.").a(v.f);
                return false;
            }
            str = k().m();
            z2 = k().n();
        }
        z2 = false;
        String str2 = Build.MANUFACTURER;
        if (!str2.equals("Amazon") && info2 == null) {
            return false;
        }
        if (!str2.equals("Amazon")) {
            str = info2.getId();
            z2 = info2.isLimitAdTrackingEnabled();
        }
        k().a(str);
        if (a2 != null) {
            a2.e.put("advertisingId", k().l());
        }
        k().c(z2);
        k().b(true);
        if (yVar != null) {
            JSONObject b2 = t.b();
            t.a(b2, "advertiser_id", k().l());
            t.a(b2, "limit_ad_tracking", k().E());
            yVar.a(b2).c();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAppOptions adColonyAppOptions) {
        synchronized (this.d.a()) {
            for (Map.Entry<String, AdColonyInterstitial> value : this.d.a().entrySet()) {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) value.getValue();
                AdColonyInterstitialListener g2 = adColonyInterstitial.g();
                adColonyInterstitial.a(true);
                if (g2 != null) {
                    g2.e(adColonyInterstitial);
                }
            }
            this.d.a().clear();
        }
        this.D = false;
        AdColony.a(a.b(), adColonyAppOptions);
        a(1);
        this.t.clear();
        this.q = adColonyAppOptions;
        this.f1228a.a();
        a(true, true);
    }

    /* access modifiers changed from: package-private */
    public boolean a(int i2) {
        b0 a2 = this.f1228a.a(i2);
        n0 remove = this.u.remove(Integer.valueOf(i2));
        boolean z2 = false;
        if (a2 == null) {
            return false;
        }
        if (remove != null && remove.x()) {
            z2 = true;
        }
        C0004i iVar = new C0004i(this, remove);
        if (z2) {
            if (Looper.myLooper() == null) {
                Looper.prepare();
            }
            new Handler().postDelayed(iVar, 1000);
        } else {
            iVar.run();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyRewardListener adColonyRewardListener) {
        this.o = adColonyRewardListener;
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        this.l = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAdView adColonyAdView) {
        this.m = adColonyAdView;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyInterstitial adColonyInterstitial) {
        this.n = adColonyInterstitial;
    }
}
