package com.adcolony.sdk;

public abstract class AdColonyInterstitialListener {
    @Deprecated
    public void a(AdColonyInterstitial adColonyInterstitial) {
    }

    public void a(AdColonyInterstitial adColonyInterstitial, String str, int i) {
    }

    public void a(AdColonyZone adColonyZone) {
    }

    @Deprecated
    public void b(AdColonyInterstitial adColonyInterstitial) {
    }

    public void c(AdColonyInterstitial adColonyInterstitial) {
    }

    public void d(AdColonyInterstitial adColonyInterstitial) {
    }

    public void e(AdColonyInterstitial adColonyInterstitial) {
    }

    public void f(AdColonyInterstitial adColonyInterstitial) {
    }

    public void g(AdColonyInterstitial adColonyInterstitial) {
    }

    public abstract void h(AdColonyInterstitial adColonyInterstitial);
}
