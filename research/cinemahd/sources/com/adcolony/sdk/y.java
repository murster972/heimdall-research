package com.adcolony.sdk;

import com.adcolony.sdk.v;
import org.json.JSONException;
import org.json.JSONObject;

class y {

    /* renamed from: a  reason: collision with root package name */
    private String f1375a;
    private JSONObject b;

    y(JSONObject jSONObject) {
        try {
            this.b = jSONObject;
            this.f1375a = jSONObject.getString("m_type");
        } catch (JSONException e) {
            new v.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(v.i);
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.f1375a;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        a.a(this.f1375a, this.b);
    }

    /* access modifiers changed from: package-private */
    public y a(JSONObject jSONObject) {
        try {
            y yVar = new y("reply", this.b.getInt("m_origin"), jSONObject);
            yVar.b.put("m_id", this.b.getInt("m_id"));
            return yVar;
        } catch (JSONException e) {
            new v.a().a("JSON error in ADCMessage's createReply(): ").a(e.toString()).a(v.i);
            return new y("JSONException", 0);
        }
    }

    /* access modifiers changed from: package-private */
    public void b(JSONObject jSONObject) {
        this.b = jSONObject;
    }

    y(String str, int i) {
        try {
            this.f1375a = str;
            JSONObject jSONObject = new JSONObject();
            this.b = jSONObject;
            jSONObject.put("m_target", i);
        } catch (JSONException e) {
            new v.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(v.i);
        }
    }

    y(String str, int i, JSONObject jSONObject) {
        try {
            this.f1375a = str;
            jSONObject = jSONObject == null ? new JSONObject() : jSONObject;
            this.b = jSONObject;
            jSONObject.put("m_target", i);
        } catch (JSONException e) {
            new v.a().a("JSON Error in ADCMessage constructor: ").a(e.toString()).a(v.i);
        }
    }
}
