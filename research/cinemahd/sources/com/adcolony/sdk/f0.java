package com.adcolony.sdk;

import android.util.Log;
import com.adcolony.sdk.w;
import com.applovin.sdk.AppLovinEventTypes;
import com.unity3d.ads.metadata.MediationMetaData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class f0 {

    /* renamed from: a  reason: collision with root package name */
    u f1221a;
    ScheduledExecutorService b;
    List<w> c = new ArrayList();
    List<w> d = new ArrayList();
    HashMap<String, Object> e;
    private s f = new s("adcolony_android", "4.4.0", "Production");
    private s g = new s("adcolony_fatal_reports", "4.4.0", "Production");

    class a implements Runnable {
        a() {
        }

        public void run() {
            f0.this.a();
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ w f1223a;

        b(w wVar) {
            this.f1223a = wVar;
        }

        public void run() {
            f0.this.c.add(this.f1223a);
        }
    }

    f0(u uVar, ScheduledExecutorService scheduledExecutorService, HashMap<String, Object> hashMap) {
        this.f1221a = uVar;
        this.b = scheduledExecutorService;
        this.e = hashMap;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(long j, TimeUnit timeUnit) {
        try {
            if (!this.b.isShutdown() && !this.b.isTerminated()) {
                this.b.scheduleAtFixedRate(new a(), j, j, timeUnit);
            }
        } catch (RuntimeException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(2:9|10) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:?, code lost:
        r4.b.shutdownNow();
        java.lang.Thread.currentThread().interrupt();
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x003e */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public synchronized void b() {
        /*
            r4 = this;
            monitor-enter(r4)
            java.util.concurrent.ScheduledExecutorService r0 = r4.b     // Catch:{ all -> 0x004c }
            r0.shutdown()     // Catch:{ all -> 0x004c }
            java.util.concurrent.ScheduledExecutorService r0 = r4.b     // Catch:{ InterruptedException -> 0x003e }
            java.util.concurrent.TimeUnit r1 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x003e }
            r2 = 1
            boolean r0 = r0.awaitTermination(r2, r1)     // Catch:{ InterruptedException -> 0x003e }
            if (r0 != 0) goto L_0x004a
            java.util.concurrent.ScheduledExecutorService r0 = r4.b     // Catch:{ InterruptedException -> 0x003e }
            r0.shutdownNow()     // Catch:{ InterruptedException -> 0x003e }
            java.util.concurrent.ScheduledExecutorService r0 = r4.b     // Catch:{ InterruptedException -> 0x003e }
            boolean r0 = r0.awaitTermination(r2, r1)     // Catch:{ InterruptedException -> 0x003e }
            if (r0 != 0) goto L_0x004a
            java.io.PrintStream r0 = java.lang.System.err     // Catch:{ InterruptedException -> 0x003e }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ InterruptedException -> 0x003e }
            r1.<init>()     // Catch:{ InterruptedException -> 0x003e }
            java.lang.Class r2 = r4.getClass()     // Catch:{ InterruptedException -> 0x003e }
            java.lang.String r2 = r2.getSimpleName()     // Catch:{ InterruptedException -> 0x003e }
            r1.append(r2)     // Catch:{ InterruptedException -> 0x003e }
            java.lang.String r2 = ": ScheduledExecutorService did not terminate"
            r1.append(r2)     // Catch:{ InterruptedException -> 0x003e }
            java.lang.String r1 = r1.toString()     // Catch:{ InterruptedException -> 0x003e }
            r0.println(r1)     // Catch:{ InterruptedException -> 0x003e }
            goto L_0x004a
        L_0x003e:
            java.util.concurrent.ScheduledExecutorService r0 = r4.b     // Catch:{ all -> 0x004c }
            r0.shutdownNow()     // Catch:{ all -> 0x004c }
            java.lang.Thread r0 = java.lang.Thread.currentThread()     // Catch:{ all -> 0x004c }
            r0.interrupt()     // Catch:{ all -> 0x004c }
        L_0x004a:
            monitor-exit(r4)
            return
        L_0x004c:
            r0 = move-exception
            monitor-exit(r4)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.f0.b():void");
    }

    /* access modifiers changed from: package-private */
    public synchronized void c(String str) {
        a(new w.a().a(2).a(this.f).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public synchronized void d(String str) {
        a(new w.a().a(1).a(this.f).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public synchronized void e(String str) {
        this.e.put("controllerVersion", str);
    }

    /* access modifiers changed from: package-private */
    public synchronized void f(String str) {
        this.e.put("sessionId", str);
    }

    /*  JADX ERROR: IndexOutOfBoundsException in pass: RegionMakerVisitor
        java.lang.IndexOutOfBoundsException: Index: 0, Size: 0
        	at java.util.ArrayList.rangeCheck(ArrayList.java:659)
        	at java.util.ArrayList.get(ArrayList.java:435)
        	at jadx.core.dex.nodes.InsnNode.getArg(InsnNode.java:101)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:611)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverseMonitorExits(RegionMaker.java:619)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:561)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMaker.processMonitorEnter(RegionMaker.java:598)
        	at jadx.core.dex.visitors.regions.RegionMaker.traverse(RegionMaker.java:133)
        	at jadx.core.dex.visitors.regions.RegionMaker.makeRegion(RegionMaker.java:86)
        	at jadx.core.dex.visitors.regions.RegionMakerVisitor.visit(RegionMakerVisitor.java:49)
        */
    synchronized void a() {
        /*
            r2 = this;
            monitor-enter(r2)
            monitor-enter(r2)     // Catch:{ all -> 0x0049 }
            java.util.List<com.adcolony.sdk.w> r0 = r2.c     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            if (r0 <= 0) goto L_0x001c
            com.adcolony.sdk.s r0 = r2.f     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.w> r1 = r2.c     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.lang.String r0 = r2.a((com.adcolony.sdk.s) r0, (java.util.List<com.adcolony.sdk.w>) r1)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            com.adcolony.sdk.u r1 = r2.f1221a     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r1.a(r0)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.w> r0 = r2.c     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r0.clear()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
        L_0x001c:
            java.util.List<com.adcolony.sdk.w> r0 = r2.d     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            int r0 = r0.size()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            if (r0 <= 0) goto L_0x0044
            com.adcolony.sdk.s r0 = r2.g     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.w> r1 = r2.d     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.lang.String r0 = r2.a((com.adcolony.sdk.s) r0, (java.util.List<com.adcolony.sdk.w>) r1)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            com.adcolony.sdk.u r1 = r2.f1221a     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r1.a(r0)     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            java.util.List<com.adcolony.sdk.w> r0 = r2.d     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            r0.clear()     // Catch:{ IOException -> 0x003f, JSONException -> 0x0039 }
            goto L_0x0044
        L_0x0037:
            r0 = move-exception
            goto L_0x0047
        L_0x0039:
            java.util.List<com.adcolony.sdk.w> r0 = r2.c     // Catch:{ all -> 0x0037 }
            r0.clear()     // Catch:{ all -> 0x0037 }
            goto L_0x0044
        L_0x003f:
            java.util.List<com.adcolony.sdk.w> r0 = r2.c     // Catch:{ all -> 0x0037 }
            r0.clear()     // Catch:{ all -> 0x0037 }
        L_0x0044:
            monitor-exit(r2)     // Catch:{ all -> 0x0037 }
            monitor-exit(r2)
            return
        L_0x0047:
            monitor-exit(r2)     // Catch:{ all -> 0x0037 }
            throw r0     // Catch:{ all -> 0x0049 }
        L_0x0049:
            r0 = move-exception
            monitor-exit(r2)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.f0.a():void");
    }

    private synchronized JSONObject b(w wVar) throws JSONException {
        JSONObject jSONObject;
        jSONObject = new JSONObject(this.e);
        jSONObject.put("environment", wVar.a().a());
        jSONObject.put(AppLovinEventTypes.USER_COMPLETED_LEVEL, wVar.b());
        jSONObject.put("message", wVar.c());
        jSONObject.put("clientTimestamp", wVar.d());
        JSONObject f2 = a.c().t().f();
        JSONObject h = a.c().t().h();
        double q = a.c().k().q();
        jSONObject.put("mediation_network", t.g(f2, MediationMetaData.KEY_NAME));
        jSONObject.put("mediation_network_version", t.g(f2, MediationMetaData.KEY_VERSION));
        jSONObject.put("plugin", t.g(h, MediationMetaData.KEY_NAME));
        jSONObject.put("plugin_version", t.g(h, MediationMetaData.KEY_VERSION));
        jSONObject.put("batteryInfo", q);
        if (wVar instanceof p) {
            t.a(jSONObject, ((p) wVar).e());
            jSONObject.put("platform", "android");
        }
        return jSONObject;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(w wVar) {
        try {
            if (!this.b.isShutdown() && !this.b.isTerminated()) {
                this.b.submit(new b(wVar));
            }
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting remote log to executor service");
        }
        return;
    }

    /* access modifiers changed from: package-private */
    public synchronized void a(String str) {
        a(new w.a().a(3).a(this.f).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public synchronized void b(String str) {
        a(new w.a().a(0).a(this.f).a(str).a());
    }

    /* access modifiers changed from: package-private */
    public String a(s sVar, List<w> list) throws IOException, JSONException {
        String l = a.c().k().l();
        String str = this.e.get("advertiserId") != null ? (String) this.e.get("advertiserId") : "unknown";
        if (l != null && l.length() > 0 && !l.equals(str)) {
            this.e.put("advertiserId", l);
        }
        JSONObject jSONObject = new JSONObject();
        jSONObject.put("index", sVar.b());
        jSONObject.put("environment", sVar.a());
        jSONObject.put(MediationMetaData.KEY_VERSION, sVar.c());
        JSONArray jSONArray = new JSONArray();
        for (w b2 : list) {
            jSONArray.put(b(b2));
        }
        jSONObject.put("logs", jSONArray);
        return jSONObject.toString();
    }
}
