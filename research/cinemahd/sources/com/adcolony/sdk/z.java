package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import com.adcolony.sdk.v;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@SuppressLint({"UseSparseArrays"})
class z {

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<b0> f1376a = new ArrayList<>();
    private HashMap<Integer, b0> b = new HashMap<>();
    private int c = 2;
    private HashMap<String, ArrayList<a0>> d = new HashMap<>();
    private JSONArray e = t.a();
    private int f = 1;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1377a;

        a(Context context) {
            this.f1377a = context;
        }

        public void run() {
            JSONObject b2 = a.c().t().b();
            JSONObject b3 = t.b();
            t.a(b2, "os_name", "android");
            t.a(b3, "filepath", a.c().x().a() + "7bf3a1e7bbd31e612eda3310c2cdb8075c43c6b5");
            t.a(b3, "info", b2);
            t.b(b3, "m_origin", 0);
            t.b(b3, "m_id", z.a(z.this));
            t.a(b3, "m_type", "Controller.create");
            try {
                new n0(this.f1377a, 1, false).a(true, new y(b3));
            } catch (RuntimeException e) {
                v.a aVar = new v.a();
                aVar.a(e.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(v.h);
                AdColony.d();
            }
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1378a;
        final /* synthetic */ JSONObject b;

        b(String str, JSONObject jSONObject) {
            this.f1378a = str;
            this.b = jSONObject;
        }

        public void run() {
            z.this.a(this.f1378a, this.b);
        }
    }

    z() {
    }

    static /* synthetic */ int a(z zVar) {
        int i = zVar.f;
        zVar.f = i + 1;
        return i;
    }

    /* access modifiers changed from: package-private */
    public void b(String str, a0 a0Var) {
        synchronized (this.d) {
            ArrayList arrayList = this.d.get(str);
            if (arrayList != null) {
                arrayList.remove(a0Var);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, b0> c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        int i = this.c;
        this.c = i + 1;
        return i;
    }

    /* access modifiers changed from: package-private */
    public synchronized void e() {
        synchronized (this.f1376a) {
            for (int size = this.f1376a.size() - 1; size >= 0; size--) {
                this.f1376a.get(size).a();
            }
        }
        JSONArray jSONArray = null;
        if (this.e.length() > 0) {
            jSONArray = this.e;
            this.e = t.a();
        }
        if (jSONArray != null) {
            int length = jSONArray.length();
            for (int i = 0; i < length; i++) {
                try {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString("m_type");
                    if (jSONObject.getInt("m_origin") >= 2) {
                        l0.a((Runnable) new b(string, jSONObject));
                    } else {
                        a(string, jSONObject);
                    }
                } catch (JSONException e2) {
                    new v.a().a("JSON error from message dispatcher's updateModules(): ").a(e2.toString()).a(v.i);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, a0 a0Var) {
        ArrayList arrayList = this.d.get(str);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.d.put(str, arrayList);
        }
        arrayList.add(a0Var);
    }

    /* access modifiers changed from: package-private */
    public ArrayList<b0> b() {
        return this.f1376a;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Context b2;
        i c2 = a.c();
        if (!c2.a() && !c2.b() && (b2 = a.b()) != null) {
            l0.a((Runnable) new a(b2));
        }
    }

    /* access modifiers changed from: package-private */
    public b0 a(b0 b0Var) {
        synchronized (this.f1376a) {
            int c2 = b0Var.c();
            if (c2 <= 0) {
                c2 = b0Var.d();
            }
            this.f1376a.add(b0Var);
            this.b.put(Integer.valueOf(c2), b0Var);
        }
        return b0Var;
    }

    /* access modifiers changed from: package-private */
    public b0 a(int i) {
        synchronized (this.f1376a) {
            b0 b0Var = this.b.get(Integer.valueOf(i));
            if (b0Var == null) {
                return null;
            }
            this.f1376a.remove(b0Var);
            this.b.remove(Integer.valueOf(i));
            b0Var.b();
            return b0Var;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, JSONObject jSONObject) {
        synchronized (this.d) {
            ArrayList arrayList = this.d.get(str);
            if (arrayList != null) {
                y yVar = new y(jSONObject);
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    try {
                        ((a0) it2.next()).a(yVar);
                    } catch (RuntimeException e2) {
                        new v.a().a((Object) e2).a(v.i);
                        e2.printStackTrace();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        try {
            if (!jSONObject.has("m_id")) {
                int i = this.f;
                this.f = i + 1;
                jSONObject.put("m_id", i);
            }
            if (!jSONObject.has("m_origin")) {
                jSONObject.put("m_origin", 0);
            }
            int i2 = jSONObject.getInt("m_target");
            if (i2 == 0) {
                synchronized (this) {
                    this.e.put(jSONObject);
                }
                return;
            }
            b0 b0Var = this.b.get(Integer.valueOf(i2));
            if (b0Var != null) {
                b0Var.a(jSONObject);
            }
        } catch (JSONException e2) {
            new v.a().a("JSON error in ADCMessageDispatcher's sendMessage(): ").a(e2.toString()).a(v.i);
        }
    }
}
