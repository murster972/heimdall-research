package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import com.adcolony.sdk.l0;
import com.adcolony.sdk.v;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColony {

    /* renamed from: a  reason: collision with root package name */
    static ExecutorService f1150a = Executors.newSingleThreadExecutor();

    static class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyInterstitialListener f1151a;
        final /* synthetic */ String b;
        final /* synthetic */ AdColonyAdOptions c;

        /* renamed from: com.adcolony.sdk.AdColony$a$a  reason: collision with other inner class name */
        class C0000a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ AdColonyZone f1152a;

            C0000a(AdColonyZone adColonyZone) {
                this.f1152a = adColonyZone;
            }

            public void run() {
                a.this.f1151a.a(this.f1152a);
            }
        }

        a(AdColonyInterstitialListener adColonyInterstitialListener, String str, AdColonyAdOptions adColonyAdOptions) {
            this.f1151a = adColonyInterstitialListener;
            this.b = str;
            this.c = adColonyAdOptions;
        }

        public void run() {
            i c2 = a.c();
            if (c2.a() || c2.b()) {
                AdColony.c();
                AdColony.a(this.f1151a, this.b);
            } else if (AdColony.b() || !a.d()) {
                AdColonyZone adColonyZone = c2.A().get(this.b);
                if (adColonyZone == null) {
                    adColonyZone = new AdColonyZone(this.b);
                }
                if (adColonyZone.c() == 2 || adColonyZone.c() == 1) {
                    l0.a((Runnable) new C0000a(adColonyZone));
                } else {
                    c2.e().a(this.b, this.f1151a, this.c);
                }
            } else {
                AdColony.a(this.f1151a, this.b);
            }
        }
    }

    static class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1153a;
        final /* synthetic */ AdColonyInterstitialListener b;

        b(String str, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.f1153a = str;
            this.b = adColonyInterstitialListener;
        }

        public void run() {
            AdColonyZone adColonyZone = a.c().A().get(this.f1153a);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(this.f1153a);
            }
            this.b.a(adColonyZone);
        }
    }

    static class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1154a;
        final /* synthetic */ AdColonyAdViewListener b;

        c(String str, AdColonyAdViewListener adColonyAdViewListener) {
            this.f1154a = str;
            this.b = adColonyAdViewListener;
        }

        public void run() {
            AdColonyZone adColonyZone;
            if (!a.e()) {
                adColonyZone = null;
            } else {
                adColonyZone = a.c().A().get(this.f1154a);
            }
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(this.f1154a);
            }
            this.b.a(adColonyZone);
        }
    }

    static class d implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyInterstitial f1155a;

        d(AdColonyInterstitial adColonyInterstitial) {
            this.f1155a = adColonyInterstitial;
        }

        public void run() {
            AdColonyInterstitialListener g = this.f1155a.g();
            this.f1155a.a(true);
            if (g != null) {
                g.e(this.f1155a);
            }
        }
    }

    static class e implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ i f1156a;

        e(i iVar) {
            this.f1156a = iVar;
        }

        public void run() {
            ArrayList arrayList = new ArrayList();
            Iterator<b0> it2 = this.f1156a.p().b().iterator();
            while (it2.hasNext()) {
                arrayList.add(it2.next());
            }
            Iterator it3 = arrayList.iterator();
            while (it3.hasNext()) {
                b0 b0Var = (b0) it3.next();
                this.f1156a.a(b0Var.d());
                if (b0Var instanceof n0) {
                    n0 n0Var = (n0) b0Var;
                    if (!n0Var.w()) {
                        n0Var.loadUrl("about:blank");
                        n0Var.clearCache(true);
                        n0Var.removeAllViews();
                        n0Var.a(true);
                    }
                }
            }
        }
    }

    static class h implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyAdViewListener f1157a;
        final /* synthetic */ String b;
        final /* synthetic */ AdColonyAdSize c;
        final /* synthetic */ AdColonyAdOptions d;

        h(AdColonyAdViewListener adColonyAdViewListener, String str, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
            this.f1157a = adColonyAdViewListener;
            this.b = str;
            this.c = adColonyAdSize;
            this.d = adColonyAdOptions;
        }

        public void run() {
            i c2 = a.c();
            if (c2.a() || c2.b()) {
                AdColony.c();
                AdColony.a(this.f1157a, this.b);
            }
            if (!AdColony.b() && a.d()) {
                AdColony.a(this.f1157a, this.b);
            }
            c2.e().a(this.b, this.f1157a, this.c, this.d);
        }
    }

    static class j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1158a;

        j(String str) {
            this.f1158a = str;
        }

        public void run() {
            AdColony.b();
            JSONObject b = t.b();
            t.a(b, "type", this.f1158a);
            new y("CustomMessage.register", 1, b).c();
        }
    }

    static class k implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1159a;

        k(String str) {
            this.f1159a = str;
        }

        public void run() {
            AdColony.b();
            JSONObject b = t.b();
            t.a(b, "type", this.f1159a);
            new y("CustomMessage.unregister", 1, b).c();
        }
    }

    public static boolean a(AdColonyCustomMessageListener adColonyCustomMessageListener, String str) {
        if (!a.f()) {
            new v.a().a("Ignoring call to AdColony.addCustomMessageListener as AdColony ").a("has not yet been configured.").a(v.f);
            return false;
        } else if (!l0.e(str)) {
            new v.a().a("Ignoring call to AdColony.addCustomMessageListener.").a(v.f);
            return false;
        } else {
            try {
                a.c().j().put(str, adColonyCustomMessageListener);
                f1150a.execute(new j(str));
                return true;
            } catch (RejectedExecutionException unused) {
                return false;
            }
        }
    }

    static boolean b() {
        l0.b bVar = new l0.b(15.0d);
        i c2 = a.c();
        while (!c2.c() && !bVar.a()) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException unused) {
            }
        }
        return c2.c();
    }

    /* access modifiers changed from: private */
    public static void c() {
        new v.a().a("The AdColony API is not available while AdColony is disabled.").a(v.h);
    }

    public static boolean d() {
        if (!a.f()) {
            return false;
        }
        Context b2 = a.b();
        if (b2 != null && (b2 instanceof b)) {
            ((Activity) b2).finish();
        }
        i c2 = a.c();
        for (AdColonyInterstitial dVar : c2.e().a().values()) {
            l0.a((Runnable) new d(dVar));
        }
        l0.a((Runnable) new e(c2));
        a.c().a(true);
        return true;
    }

    public static boolean a(String str) {
        if (!a.f()) {
            new v.a().a("Ignoring call to AdColony.removeCustomMessageListener as AdColony").a(" has not yet been configured.").a(v.f);
            return false;
        }
        a.c().j().remove(str);
        f1150a.execute(new k(str));
        return true;
    }

    public static boolean a(AdColonyRewardListener adColonyRewardListener) {
        if (!a.f()) {
            new v.a().a("Ignoring call to AdColony.setRewardListener() as AdColony has not").a(" yet been configured.").a(v.f);
            return false;
        }
        a.c().a(adColonyRewardListener);
        return true;
    }

    public static boolean a(Activity activity, AdColonyAppOptions adColonyAppOptions, String str, String... strArr) {
        return a((Context) activity, adColonyAppOptions, str, strArr);
    }

    public static boolean a(String str, AdColonyAdViewListener adColonyAdViewListener, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
        if (!a.f()) {
            new v.a().a("Ignoring call to requestAdView as AdColony has not yet been").a(" configured.").a(v.f);
            a(adColonyAdViewListener, str);
            return false;
        } else if (adColonyAdSize.a() <= 0 || adColonyAdSize.b() <= 0) {
            new v.a().a("Ignoring call to requestAdView as you've provided an AdColonyAdSize").a(" object with an invalid width or height.").a(v.f);
            return false;
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("zone_id", str);
            if (e0.a(1, bundle)) {
                a(adColonyAdViewListener, str);
                return false;
            }
            try {
                f1150a.execute(new h(adColonyAdViewListener, str, adColonyAdSize, adColonyAdOptions));
                return true;
            } catch (RejectedExecutionException unused) {
                a(adColonyAdViewListener, str);
                return false;
            }
        }
    }

    public static boolean a(String str, AdColonyInterstitialListener adColonyInterstitialListener, AdColonyAdOptions adColonyAdOptions) {
        if (!a.f()) {
            new v.a().a("Ignoring call to AdColony.requestInterstitial as AdColony has not").a(" yet been configured.").a(v.f);
            adColonyInterstitialListener.a(new AdColonyZone(str));
            return false;
        }
        Bundle bundle = new Bundle();
        bundle.putString("zone_id", str);
        if (e0.a(1, bundle)) {
            AdColonyZone adColonyZone = a.c().A().get(str);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(str);
            }
            adColonyInterstitialListener.a(adColonyZone);
            return false;
        }
        try {
            f1150a.execute(new a(adColonyInterstitialListener, str, adColonyAdOptions));
            return true;
        } catch (RejectedExecutionException unused) {
            a(adColonyInterstitialListener, str);
            return false;
        }
    }

    @SuppressLint({"ObsoleteSdkInt"})
    private static boolean a(Context context, AdColonyAppOptions adColonyAppOptions, String str, String... strArr) {
        if (e0.a(0, (Bundle) null)) {
            new v.a().a("Cannot configure AdColony; configuration mechanism requires 5 ").a("seconds between attempts.").a(v.f);
            return false;
        }
        if (context == null) {
            context = a.b();
        }
        if (context == null) {
            new v.a().a("Ignoring call to AdColony.configure() as the provided Activity or ").a("Application context is null and we do not currently hold a ").a("reference to either for our use.").a(v.f);
            return false;
        }
        if (Looper.myLooper() == null) {
            Looper.prepare();
        }
        if (adColonyAppOptions == null) {
            adColonyAppOptions = new AdColonyAppOptions();
        }
        if (a.e() && !t.c(a.c().t().b(), "reconfigurable")) {
            i c2 = a.c();
            if (!c2.t().a().equals(str)) {
                new v.a().a("Ignoring call to AdColony.configure() as the app id does not ").a("match what was used during the initial configuration.").a(v.f);
                return false;
            } else if (l0.a(strArr, c2.t().c())) {
                new v.a().a("Ignoring call to AdColony.configure() as the same zone ids ").a("were used during the previous configuration.").a(v.f);
                return true;
            }
        }
        new SimpleDateFormat("HH:mm:ss:SSS", Locale.US).format(new Date(System.currentTimeMillis()));
        boolean z = true;
        for (int i = 0; i < strArr.length; i++) {
            if (strArr[i] != null && !strArr[i].equals("")) {
                z = false;
            }
        }
        if (str.equals("") || z) {
            new v.a().a("AdColony.configure() called with an empty app or zone id String.").a(v.h);
            return false;
        }
        a.c = true;
        adColonyAppOptions.a(str);
        adColonyAppOptions.a(strArr);
        if (Build.VERSION.SDK_INT < 14) {
            new v.a().a("The minimum API level for the AdColony SDK is 14.").a(v.f);
            a.a(context, adColonyAppOptions, true);
        } else {
            a.a(context, adColonyAppOptions, false);
        }
        String str2 = a.c().x().f() + "/adc3/AppInfo";
        JSONObject b2 = t.b();
        if (new File(str2).exists()) {
            b2 = t.c(str2);
        }
        JSONObject b3 = t.b();
        if (t.g(b2, "appId").equals(str)) {
            JSONArray b4 = t.b(b2, "zoneIds");
            t.a(b4, strArr, true);
            t.a(b3, "zoneIds", b4);
            t.a(b3, "appId", str);
        } else {
            t.a(b3, "zoneIds", t.a(strArr));
            t.a(b3, "appId", str);
        }
        t.h(b3, str2);
        return true;
    }

    static boolean a(AdColonyInterstitialListener adColonyInterstitialListener, String str) {
        if (adColonyInterstitialListener == null || !a.d()) {
            return false;
        }
        l0.a((Runnable) new b(str, adColonyInterstitialListener));
        return false;
    }

    static boolean a(AdColonyAdViewListener adColonyAdViewListener, String str) {
        if (adColonyAdViewListener == null || !a.d()) {
            return false;
        }
        l0.a((Runnable) new c(str, adColonyAdViewListener));
        return false;
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions) {
        i c2 = a.c();
        k k2 = c2.k();
        if (adColonyAppOptions != null && context != null) {
            String c3 = l0.c(context);
            String c4 = l0.c();
            int d2 = l0.d();
            String r = k2.r();
            String a2 = c2.q().a();
            HashMap hashMap = new HashMap();
            hashMap.put("sessionId", "unknown");
            hashMap.put("advertiserId", "unknown");
            hashMap.put("countryLocale", Locale.getDefault().getDisplayLanguage() + " (" + Locale.getDefault().getDisplayCountry() + ")");
            hashMap.put("countryLocaleShort", a.c().k().u());
            hashMap.put("manufacturer", a.c().k().G());
            hashMap.put("model", a.c().k().J());
            hashMap.put("osVersion", a.c().k().b());
            hashMap.put("carrierName", r);
            hashMap.put("networkType", a2);
            hashMap.put("platform", "android");
            hashMap.put("appName", c3);
            hashMap.put("appVersion", c4);
            hashMap.put("appBuildNumber", Integer.valueOf(d2));
            hashMap.put("appId", "" + adColonyAppOptions.a());
            hashMap.put("apiLevel", Integer.valueOf(Build.VERSION.SDK_INT));
            hashMap.put("sdkVersion", a.c().k().c());
            hashMap.put("controllerVersion", "unknown");
            hashMap.put("zoneIds", adColonyAppOptions.d());
            JSONObject f = adColonyAppOptions.f();
            JSONObject h2 = adColonyAppOptions.h();
            if (!t.g(f, "mediation_network").equals("")) {
                hashMap.put("mediationNetwork", t.g(f, "mediation_network"));
                hashMap.put("mediationNetworkVersion", t.g(f, "mediation_network_version"));
            }
            if (!t.g(h2, "plugin").equals("")) {
                hashMap.put("plugin", t.g(h2, "plugin"));
                hashMap.put("pluginVersion", t.g(h2, "plugin_version"));
            }
            c2.o().a((HashMap<String, Object>) hashMap);
        }
    }
}
