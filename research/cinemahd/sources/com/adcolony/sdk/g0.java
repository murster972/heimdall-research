package com.adcolony.sdk;

import android.content.Context;
import com.adcolony.sdk.l0;
import com.adcolony.sdk.v;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.Executors;
import org.json.JSONObject;

class g0 implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private long f1224a = 30000;
    private long b;
    private long c;
    private long d;
    private long e;
    private long f;
    private long g;
    private boolean h = true;
    private boolean i = true;
    private boolean j;
    private boolean k;
    private boolean l;
    /* access modifiers changed from: private */
    public boolean m;
    private boolean n;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            boolean unused = g0.this.m = true;
        }
    }

    g0() {
    }

    private void f() {
        b(false);
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        i c2 = a.c();
        ArrayList<b0> b2 = c2.p().b();
        synchronized (b2) {
            Iterator<b0> it2 = b2.iterator();
            while (it2.hasNext()) {
                b0 next = it2.next();
                JSONObject b3 = t.b();
                t.a(b3, "from_window_focus", z);
                new y("SessionInfo.on_resume", next.d(), b3).c();
            }
        }
        c2.o().c();
        this.i = false;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        i c2 = a.c();
        if (!this.k) {
            if (this.l) {
                c2.b(false);
                this.l = false;
            }
            this.b = 0;
            this.c = 0;
            this.k = true;
            this.h = true;
            this.m = false;
            new Thread(this).start();
            if (z) {
                JSONObject b2 = t.b();
                t.a(b2, "id", l0.a());
                new y("SessionInfo.on_start", 1, b2).c();
                n0 n0Var = (n0) a.c().p().c().get(1);
                if (n0Var != null) {
                    n0Var.g();
                }
            }
            if (AdColony.f1150a.isShutdown()) {
                AdColony.f1150a = Executors.newSingleThreadExecutor();
            }
            c2.o().c();
        }
    }

    /* access modifiers changed from: package-private */
    public void d(boolean z) {
        this.h = z;
    }

    /* access modifiers changed from: package-private */
    public void e(boolean z) {
        this.j = z;
    }

    public void run() {
        long j2;
        while (true) {
            this.d = System.currentTimeMillis();
            a.g();
            if (this.c > this.f1224a) {
                break;
            }
            if (!this.h) {
                if (this.j && !this.i) {
                    this.j = false;
                    e();
                }
                long j3 = this.c;
                if (this.g == 0) {
                    j2 = 0;
                } else {
                    j2 = System.currentTimeMillis() - this.g;
                }
                this.c = j3 + j2;
                this.g = System.currentTimeMillis();
            } else {
                if (this.j && this.i) {
                    this.j = false;
                    f();
                }
                this.c = 0;
                this.g = 0;
            }
            a(17);
            long currentTimeMillis = System.currentTimeMillis() - this.d;
            if (currentTimeMillis > 0 && currentTimeMillis < 6000) {
                this.b += currentTimeMillis;
            }
            i c2 = a.c();
            long currentTimeMillis2 = System.currentTimeMillis();
            if (currentTimeMillis2 - this.f > 15000) {
                this.f = currentTimeMillis2;
            }
            if (a.d() && currentTimeMillis2 - this.e > 1000) {
                this.e = currentTimeMillis2;
                String a2 = c2.q().a();
                if (!a2.equals(c2.r())) {
                    c2.a(a2);
                    JSONObject b2 = t.b();
                    t.a(b2, "network_type", c2.r());
                    new y("Network.on_status_change", 1, b2).c();
                }
            }
        }
        new v.a().a("AdColony session ending, releasing Context.").a(v.d);
        a.c().b(true);
        a.a((Context) null);
        this.l = true;
        this.n = true;
        d();
        l0.b bVar = new l0.b(10.0d);
        while (!this.m && !bVar.a() && this.n) {
            a.g();
            a(100);
        }
    }

    private void e() {
        a(false);
    }

    public void a() {
        a.a("SessionInfo.stopped", (a0) new a());
    }

    /* access modifiers changed from: package-private */
    public void d() {
        f0 a2 = a.c().o().a();
        this.k = false;
        this.h = false;
        if (a2 != null) {
            a2.b();
        }
        JSONObject b2 = t.b();
        t.a(b2, "session_length", ((double) this.b) / 1000.0d);
        new y("SessionInfo.on_stop", 1, b2).c();
        a.g();
        AdColony.f1150a.shutdown();
    }

    /* access modifiers changed from: package-private */
    public void f(boolean z) {
        this.n = z;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        ArrayList<b0> b2 = a.c().p().b();
        synchronized (b2) {
            Iterator<b0> it2 = b2.iterator();
            while (it2.hasNext()) {
                b0 next = it2.next();
                JSONObject b3 = t.b();
                t.a(b3, "from_window_focus", z);
                new y("SessionInfo.on_pause", next.d(), b3).c();
            }
        }
        this.i = true;
        a.g();
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.h;
    }

    private void a(long j2) {
        try {
            Thread.sleep(j2);
        } catch (InterruptedException unused) {
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.f1224a = i2 <= 0 ? this.f1224a : (long) (i2 * 1000);
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.k;
    }
}
