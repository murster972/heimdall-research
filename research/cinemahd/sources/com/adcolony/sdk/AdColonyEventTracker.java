package com.adcolony.sdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

public class AdColonyEventTracker {

    /* renamed from: a  reason: collision with root package name */
    private static final List<JSONObject> f1167a = Collections.synchronizedList(new ArrayList());

    static void a(JSONObject jSONObject) {
        List<JSONObject> list = f1167a;
        synchronized (list) {
            if (200 > list.size()) {
                list.add(jSONObject);
            }
        }
    }

    static void b() {
        i c = a.c();
        if (!c.u().equals("") && c.c()) {
            List<JSONObject> list = f1167a;
            synchronized (list) {
                for (JSONObject b : list) {
                    b(b);
                }
                f1167a.clear();
            }
        }
    }

    private static void c(JSONObject jSONObject) {
        JSONObject f = t.f(jSONObject, "payload");
        if (n0.O) {
            t.a(f, "api_key", "bb2cf0647ba654d7228dd3f9405bbc6a");
        } else {
            t.a(f, "api_key", a.c().u());
        }
        try {
            jSONObject.remove("payload");
            jSONObject.put("payload", f);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    static boolean a() {
        boolean z;
        List<JSONObject> list = f1167a;
        synchronized (list) {
            z = list.size() != 0;
        }
        return z;
    }

    private static void b(JSONObject jSONObject) {
        i c = a.c();
        if (c.u().equals("") || !c.c()) {
            a(jSONObject);
            return;
        }
        c(jSONObject);
        new y("AdColony.log_event", 1, jSONObject).c();
    }
}
