package com.adcolony.sdk;

import com.adcolony.sdk.m;
import com.adcolony.sdk.v;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

class n implements m.a {

    /* renamed from: a  reason: collision with root package name */
    private BlockingQueue<Runnable> f1306a = new LinkedBlockingQueue();
    private ThreadPoolExecutor b = new ThreadPoolExecutor(4, 16, 60, TimeUnit.SECONDS, this.f1306a);
    private LinkedList<m> c = new LinkedList<>();
    private String d;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            n nVar = n.this;
            nVar.a(new m(yVar, nVar));
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            n nVar = n.this;
            nVar.a(new m(yVar, nVar));
        }
    }

    class c implements a0 {
        c() {
        }

        public void a(y yVar) {
            n nVar = n.this;
            nVar.a(new m(yVar, nVar));
        }
    }

    n() {
    }

    /* access modifiers changed from: package-private */
    public void a(m mVar) {
        String str = this.d;
        if (str == null || str.equals("")) {
            this.c.push(mVar);
            return;
        }
        try {
            this.b.execute(mVar);
        } catch (RejectedExecutionException unused) {
            v.a a2 = new v.a().a("RejectedExecutionException: ThreadPoolExecutor unable to ");
            a2.a("execute download for url " + mVar.k).a(v.i);
            a(mVar, mVar.a(), (Map<String, List<String>>) null);
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.c().k().j();
        a.a("WebServices.download", (a0) new a());
        a.a("WebServices.get", (a0) new b());
        a.a("WebServices.post", (a0) new c());
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.d = str;
        while (!this.c.isEmpty()) {
            a(this.c.removeLast());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i) {
        this.b.setCorePoolSize(i);
    }

    /* access modifiers changed from: package-private */
    public int a() {
        return this.b.getCorePoolSize();
    }

    public void a(m mVar, y yVar, Map<String, List<String>> map) {
        JSONObject b2 = t.b();
        t.a(b2, ReportDBAdapter.ReportColumns.COLUMN_URL, mVar.k);
        t.a(b2, "success", mVar.m);
        t.b(b2, ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, mVar.o);
        t.a(b2, "body", mVar.l);
        t.b(b2, "size", mVar.n);
        if (map != null) {
            JSONObject b3 = t.b();
            for (Map.Entry next : map.entrySet()) {
                String obj = ((List) next.getValue()).toString();
                String substring = obj.substring(1, obj.length() - 1);
                if (next.getKey() != null) {
                    t.a(b3, (String) next.getKey(), substring);
                }
            }
            t.a(b2, "headers", b3);
        }
        yVar.a(b2).c();
    }
}
