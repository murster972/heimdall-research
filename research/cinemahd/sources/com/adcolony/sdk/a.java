package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import com.adcolony.sdk.v;
import org.json.JSONArray;
import org.json.JSONObject;

class a {
    @SuppressLint({"StaticFieldLeak"})

    /* renamed from: a  reason: collision with root package name */
    private static Context f1170a;
    /* access modifiers changed from: private */
    public static i b;
    static boolean c;
    static boolean d;

    /* renamed from: com.adcolony.sdk.a$a  reason: collision with other inner class name */
    static class C0001a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1171a;

        C0001a(Context context) {
            this.f1171a = context;
        }

        public void run() {
            a.b.a(this.f1171a, (y) null);
        }
    }

    a() {
    }

    static Context b() {
        return f1170a;
    }

    static i c() {
        if (!e()) {
            Context b2 = b();
            if (b2 == null) {
                return new i();
            }
            b = new i();
            JSONObject c2 = t.c(b2.getFilesDir().getAbsolutePath() + "/adc3/AppInfo");
            JSONArray b3 = t.b(c2, "zoneIds");
            b.a(new AdColonyAppOptions().a(t.g(c2, "appId")).a(t.a(b3)), false);
        }
        return b;
    }

    static boolean d() {
        return f1170a != null;
    }

    static boolean e() {
        return b != null;
    }

    static boolean f() {
        return c;
    }

    static void g() {
        c().p().e();
    }

    static void a(Context context, AdColonyAppOptions adColonyAppOptions, boolean z) {
        a(context);
        d = true;
        if (b == null) {
            b = new i();
            adColonyAppOptions.a(context);
            b.a(adColonyAppOptions, z);
        } else {
            adColonyAppOptions.a(context);
            b.a(adColonyAppOptions);
        }
        l0.f1291a.execute(new C0001a(context));
        new v.a().a("Configuring AdColony").a(v.d);
        b.b(false);
        b.w().d(true);
        b.w().e(true);
        b.w().f(false);
        b.c(true);
        b.w().c(false);
    }

    static void b(String str, a0 a0Var) {
        c().p().b(str, a0Var);
    }

    static void a(Context context) {
        f1170a = context;
    }

    static void a(String str, a0 a0Var) {
        c().p().a(str, a0Var);
    }

    static a0 a(String str, a0 a0Var, boolean z) {
        c().p().a(str, a0Var);
        return a0Var;
    }

    static void a(String str, JSONObject jSONObject) {
        if (jSONObject == null) {
            jSONObject = t.b();
        }
        t.a(jSONObject, "m_type", str);
        c().p().a(jSONObject);
    }
}
