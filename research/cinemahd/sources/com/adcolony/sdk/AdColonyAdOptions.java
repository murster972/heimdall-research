package com.adcolony.sdk;

import org.json.JSONObject;

public class AdColonyAdOptions {

    /* renamed from: a  reason: collision with root package name */
    boolean f1160a;
    boolean b;
    JSONObject c = t.b();

    public AdColonyAdOptions a(boolean z) {
        this.f1160a = z;
        t.a(this.c, "confirmation_enabled", true);
        return this;
    }

    public AdColonyAdOptions b(boolean z) {
        this.b = z;
        t.a(this.c, "results_enabled", true);
        return this;
    }
}
