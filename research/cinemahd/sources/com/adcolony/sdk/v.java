package com.adcolony.sdk;

class v {
    static v c = new v(3, true);
    static v d = new v(2, false);
    static v e = new v(2, true);
    static v f = new v(1, false);
    static v g = new v(1, true);
    static v h = new v(0, false);
    static v i = new v(0, true);

    /* renamed from: a  reason: collision with root package name */
    private int f1361a;
    private boolean b;

    static class a {

        /* renamed from: a  reason: collision with root package name */
        StringBuilder f1362a = new StringBuilder();

        a() {
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.f1362a.append(str);
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(Object obj) {
            if (obj != null) {
                this.f1362a.append(obj.toString());
            } else {
                this.f1362a.append("null");
            }
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(int i) {
            this.f1362a.append(i);
            return this;
        }

        /* access modifiers changed from: package-private */
        public void a(v vVar) {
            vVar.a(this.f1362a.toString());
        }
    }

    static {
        new v(3, false);
    }

    v(int i2, boolean z) {
        this.f1361a = i2;
        this.b = z;
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        a.c().o().a(this.f1361a, str, this.b);
    }
}
