package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AbsoluteLayout;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.adcolony.sdk.v;
import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.common.util.ByteConstants;
import com.facebook.common.util.UriUtil;
import com.facebook.react.uimanager.ViewProps;
import com.iab.omid.library.adcolony.ScriptInjector;
import com.iab.omid.library.adcolony.adsession.FriendlyObstructionPurpose;
import com.vungle.warren.model.Advertisement;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import okhttp3.HttpUrl;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"SetJavaScriptEnabled"})
class n0 extends WebView implements b0 {
    static boolean O = false;
    private boolean A;
    /* access modifiers changed from: private */
    public boolean B;
    private boolean C;
    /* access modifiers changed from: private */
    public boolean D;
    private boolean E;
    private boolean F;
    private boolean G;
    /* access modifiers changed from: private */
    public JSONArray H = t.a();
    /* access modifiers changed from: private */
    public JSONObject I = t.b();
    private JSONObject J = t.b();
    /* access modifiers changed from: private */
    public c K;
    /* access modifiers changed from: private */
    public y L;
    private ImageView M;
    /* access modifiers changed from: private */
    public final Object N = new Object();

    /* renamed from: a  reason: collision with root package name */
    private String f1310a;
    private String b;
    private String c = "";
    private String d = "";
    /* access modifiers changed from: private */
    public String e;
    /* access modifiers changed from: private */
    public String f = "";
    private String g = "";
    private String h = "";
    /* access modifiers changed from: private */
    public String i = "";
    private String j = "";
    /* access modifiers changed from: private */
    public String k = "";
    /* access modifiers changed from: private */
    public int l;
    private int m;
    private int n;
    private int o;
    private int p;
    /* access modifiers changed from: private */
    public int q;
    private int r;
    /* access modifiers changed from: private */
    public int s;
    private int t;
    /* access modifiers changed from: private */
    public int u;
    /* access modifiers changed from: private */
    public int v;
    private int w;
    private int x;
    /* access modifiers changed from: private */
    public boolean y;
    /* access modifiers changed from: private */
    public boolean z;

    class a extends k {
        a() {
            super(n0.this, (a) null);
        }

        public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
            n0.this.a(webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
            if (webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(n0.this.f.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new v.a().a("UTF-8 not supported.").a(v.i);
                }
            }
            return null;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            Uri uri;
            if (!n0.this.B || !webResourceRequest.isForMainFrame()) {
                return false;
            }
            String l = n0.this.l();
            if (l == null) {
                uri = webResourceRequest.getUrl();
            } else {
                uri = Uri.parse(l);
            }
            l0.a(new Intent("android.intent.action.VIEW", uri));
            JSONObject b2 = t.b();
            t.a(b2, ReportDBAdapter.ReportColumns.COLUMN_URL, uri.toString());
            t.a(b2, "ad_session_id", n0.this.e);
            new y("WebView.redirect_detected", n0.this.K.k(), b2).c();
            j0 y = a.c().y();
            y.a(n0.this.e);
            y.b(n0.this.e);
            return true;
        }
    }

    class b extends k {
        b() {
            super(n0.this, (a) null);
        }

        public WebResourceResponse shouldInterceptRequest(WebView webView, WebResourceRequest webResourceRequest) {
            if (webResourceRequest.getUrl().toString().endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(n0.this.f.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new v.a().a("UTF-8 not supported.").a(v.i);
                }
            }
            return null;
        }
    }

    class c implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1312a;

            a(y yVar) {
                this.f1312a = yVar;
            }

            public void run() {
                n0.this.b(this.f1312a);
            }
        }

        c() {
        }

        public void a(y yVar) {
            if (n0.this.c(yVar)) {
                l0.a((Runnable) new a(yVar));
            }
        }
    }

    class d implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1314a;

            a(y yVar) {
                this.f1314a = yVar;
            }

            public void run() {
                n0.this.a(this.f1314a);
            }
        }

        d() {
        }

        public void a(y yVar) {
            if (n0.this.c(yVar)) {
                l0.a((Runnable) new a(yVar));
            }
        }
    }

    class e implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1316a;

            a(y yVar) {
                this.f1316a = yVar;
            }

            public void run() {
                n0.this.a(t.g(this.f1316a.a(), "custom_js"));
            }
        }

        e() {
        }

        public void a(y yVar) {
            if (n0.this.c(yVar)) {
                l0.a((Runnable) new a(yVar));
            }
        }
    }

    class f implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1318a;

            a(y yVar) {
                this.f1318a = yVar;
            }

            public void run() {
                n0.this.b(t.c(this.f1318a.a(), "transparent"));
            }
        }

        f() {
        }

        public void a(y yVar) {
            if (n0.this.c(yVar)) {
                l0.a((Runnable) new a(yVar));
            }
        }
    }

    class g implements View.OnClickListener {
        g() {
        }

        public void onClick(View view) {
            l0.a(new Intent("android.intent.action.VIEW", Uri.parse(n0.this.i)));
            a.c().y().b(n0.this.e);
        }
    }

    class h implements Runnable {
        h() {
        }

        public void run() {
            String str;
            synchronized (n0.this.N) {
                str = "";
                if (n0.this.H.length() > 0) {
                    if (n0.this.y) {
                        str = n0.this.H.toString();
                    }
                    JSONArray unused = n0.this.H = t.a();
                }
            }
            if (n0.this.y) {
                n0 n0Var = n0.this;
                n0Var.a("NativeLayer.dispatch_messages(ADC3_update(" + str + "), '" + n0.this.k + "');");
            }
        }
    }

    private class i {
        private i() {
        }

        @JavascriptInterface
        public void dispatch_messages(String str, String str2) {
            if (str2.equals(n0.this.k)) {
                n0.this.c(str);
            }
        }

        @JavascriptInterface
        public void enable_reverse_messaging(String str) {
            if (str.equals(n0.this.k)) {
                boolean unused = n0.this.D = true;
            }
        }

        @JavascriptInterface
        public String pull_messages(String str) {
            if (!str.equals(n0.this.k)) {
                return HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
            }
            String str2 = HttpUrl.PATH_SEGMENT_ENCODE_SET_URI;
            synchronized (n0.this.N) {
                if (n0.this.H.length() > 0) {
                    if (n0.this.y) {
                        str2 = n0.this.H.toString();
                    }
                    JSONArray unused = n0.this.H = t.a();
                }
            }
            return str2;
        }

        @JavascriptInterface
        public void push_messages(String str, String str2) {
            if (str2.equals(n0.this.k)) {
                n0.this.c(str);
            }
        }

        /* synthetic */ i(n0 n0Var, a aVar) {
            this();
        }
    }

    private class j extends WebChromeClient {
        private j() {
        }

        public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
            AdColonyInterstitial adColonyInterstitial;
            String str;
            ConsoleMessage.MessageLevel messageLevel = consoleMessage.messageLevel();
            d e = a.c().e();
            String message = consoleMessage.message();
            boolean z = false;
            boolean z2 = message.contains("Viewport target-densitydpi is not supported.") || message.contains("Viewport argument key \"shrink-to-fit\" not recognized and ignored");
            boolean z3 = messageLevel == ConsoleMessage.MessageLevel.ERROR;
            if (messageLevel == ConsoleMessage.MessageLevel.WARNING) {
                z = true;
            }
            if (message.contains("ADC3_update is not defined") || message.contains("NativeLayer.dispatch_messages is not a function")) {
                n0 n0Var = n0.this;
                n0Var.a(n0Var.L.a(), "Unable to communicate with AdColony. Please ensure that you have added an exception for our Javascript interface in your ProGuard configuration and that you do not have a faulty proxy enabled on your device.");
            }
            if (!z2 && (z || z3)) {
                if (n0.this.e == null) {
                    adColonyInterstitial = null;
                } else {
                    adColonyInterstitial = e.a().get(n0.this.e);
                }
                if (adColonyInterstitial == null) {
                    str = "unknown";
                } else {
                    str = adColonyInterstitial.a();
                }
                v.a aVar = new v.a();
                aVar.a("onConsoleMessage: " + message + " with ad id: " + str).a(z3 ? v.i : v.g);
            }
            return true;
        }

        public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
            jsResult.confirm();
            return true;
        }

        /* synthetic */ j(n0 n0Var, a aVar) {
            this();
        }
    }

    private class k extends WebViewClient {
        private k() {
        }

        public void onPageFinished(WebView webView, String str) {
            int i;
            JSONObject b = t.b();
            t.b(b, "id", n0.this.l);
            t.a(b, ReportDBAdapter.ReportColumns.COLUMN_URL, str);
            if (n0.this.K == null) {
                new y("WebView.on_load", n0.this.u, b).c();
            } else {
                t.a(b, "ad_session_id", n0.this.e);
                t.b(b, "container_id", n0.this.K.c());
                new y("WebView.on_load", n0.this.K.k(), b).c();
            }
            if ((n0.this.y || n0.this.z) && !n0.this.B) {
                if (n0.this.v > 0) {
                    i = n0.this.v;
                } else {
                    i = n0.this.u;
                }
                if (n0.this.v > 0) {
                    float x = a.c().k().x();
                    t.b(n0.this.I, "app_orientation", l0.d(l0.e()));
                    t.b(n0.this.I, "x", l0.a((View) n0.this));
                    t.b(n0.this.I, "y", l0.b((View) n0.this));
                    t.b(n0.this.I, "width", (int) (((float) n0.this.q) / x));
                    t.b(n0.this.I, "height", (int) (((float) n0.this.s) / x));
                    t.a(n0.this.I, "ad_session_id", n0.this.e);
                }
                String unused = n0.this.k = l0.a();
                JSONObject b2 = t.b();
                t.a(b2, n0.this.I);
                t.a(b2, "message_key", n0.this.k);
                n0 n0Var = n0.this;
                n0Var.a("ADC3_init(" + i + "," + b2.toString() + ");");
                boolean unused2 = n0.this.B = true;
            }
            if (!n0.this.z) {
                return;
            }
            if (n0.this.u != 1 || n0.this.v > 0) {
                JSONObject b3 = t.b();
                t.a(b3, "success", true);
                t.b(b3, "id", n0.this.u);
                n0.this.L.a(b3).c();
            }
        }

        public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            boolean unused = n0.this.B = false;
        }

        public void onReceivedError(WebView webView, int i, String str, String str2) {
            if (Build.VERSION.SDK_INT < 23) {
                n0.this.a(i, str, str2);
            }
        }

        public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
            if (Build.VERSION.SDK_INT < 26) {
                return super.onRenderProcessGone(webView, renderProcessGoneDetail);
            }
            if (!renderProcessGoneDetail.didCrash()) {
                return true;
            }
            n0.this.a(t.b(), "An error occurred while rendering the ad. Ad closing.");
            return true;
        }

        @TargetApi(11)
        public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
            if (Build.VERSION.SDK_INT < 21 && str.endsWith("mraid.js")) {
                try {
                    return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(n0.this.f.getBytes("UTF-8")));
                } catch (UnsupportedEncodingException unused) {
                    new v.a().a("UTF-8 not supported.").a(v.i);
                }
            }
            return null;
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (!n0.this.B) {
                return false;
            }
            String l = n0.this.l();
            if (l == null) {
                l = str;
            }
            l0.a(new Intent("android.intent.action.VIEW", Uri.parse(l)));
            j0 y = a.c().y();
            y.a(n0.this.e);
            y.b(n0.this.e);
            JSONObject b = t.b();
            t.a(b, ReportDBAdapter.ReportColumns.COLUMN_URL, l);
            t.a(b, "ad_session_id", n0.this.e);
            new y("WebView.redirect_detected", n0.this.K.k(), b).c();
            return true;
        }

        /* synthetic */ k(n0 n0Var, a aVar) {
            this();
        }
    }

    n0(Context context, y yVar, int i2, int i3, c cVar) {
        super(context);
        this.L = yVar;
        a(yVar, i2, i3, cVar);
        u();
    }

    private AdColonyAdView A() {
        if (this.e == null) {
            return null;
        }
        return a.c().e().b().get(this.e);
    }

    private AdColonyInterstitial B() {
        if (this.e == null) {
            return null;
        }
        return a.c().e().a().get(this.e);
    }

    private boolean C() {
        return A() != null;
    }

    private boolean y() {
        return B() != null;
    }

    private void z() {
        Context b2 = a.b();
        if (b2 != null && this.K != null && !this.F) {
            GradientDrawable gradientDrawable = new GradientDrawable();
            gradientDrawable.setColor(-1);
            gradientDrawable.setShape(1);
            ImageView imageView = new ImageView(b2);
            this.M = imageView;
            imageView.setImageURI(Uri.fromFile(new File(this.h)));
            this.M.setBackground(gradientDrawable);
            this.M.setOnClickListener(new g());
            f();
            addView(this.M);
        }
    }

    public void b() {
    }

    /* access modifiers changed from: package-private */
    public void e() {
        String str;
        if (this.A) {
            try {
                if (this.j.equals("")) {
                    FileInputStream fileInputStream = new FileInputStream(this.b);
                    StringBuilder sb = new StringBuilder(fileInputStream.available());
                    byte[] bArr = new byte[ByteConstants.KB];
                    while (true) {
                        int read = fileInputStream.read(bArr, 0, ByteConstants.KB);
                        if (read < 0) {
                            break;
                        }
                        sb.append(new String(bArr, 0, read));
                    }
                    if (this.b.contains(".html")) {
                        str = sb.toString();
                    } else {
                        str = "<html><script>" + sb.toString() + "</script></html>";
                    }
                } else {
                    str = this.j.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", "script src=\"file://" + this.g + "\"");
                }
                String g2 = t.g(t.f(this.L.a(), "info"), "metadata");
                loadDataWithBaseURL(this.f1310a.equals("") ? this.d : this.f1310a, a(str, t.g(t.b(g2), "iab_filepath")).replaceFirst("var\\s*ADC_DEVICE_INFO\\s*=\\s*null\\s*;", Matcher.quoteReplacement("var ADC_DEVICE_INFO = " + g2 + ";")), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, (String) null);
            } catch (IOException e2) {
                a((Exception) e2);
            } catch (IllegalArgumentException e3) {
                a((Exception) e3);
            } catch (IndexOutOfBoundsException e4) {
                a((Exception) e4);
            }
        } else if (!this.f1310a.startsWith(UriUtil.HTTP_SCHEME) && !this.f1310a.startsWith(UriUtil.LOCAL_FILE_SCHEME)) {
            loadDataWithBaseURL(this.d, this.f1310a, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, (String) null);
        } else if (this.f1310a.contains(".html") || !this.f1310a.startsWith(UriUtil.LOCAL_FILE_SCHEME)) {
            loadUrl(this.f1310a);
        } else {
            loadDataWithBaseURL(this.f1310a, "<html><script src=\"" + this.f1310a + "\"></script></html>", AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, (String) null);
        }
    }

    /* access modifiers changed from: package-private */
    public void f() {
        if (this.M != null) {
            int B2 = a.c().k().B();
            int A2 = a.c().k().A();
            boolean z2 = this.G;
            if (z2) {
                B2 = this.m + this.q;
            }
            if (z2) {
                A2 = this.o + this.s;
            }
            float x2 = a.c().k().x();
            int i2 = (int) (((float) this.w) * x2);
            int i3 = (int) (((float) this.x) * x2);
            this.M.setLayoutParams(new AbsoluteLayout.LayoutParams(i2, i3, B2 - i2, A2 - i3));
        }
    }

    /* access modifiers changed from: package-private */
    public void g() {
        l0.a((Runnable) new h());
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        AdColonyAdView A2;
        if (motionEvent.getAction() == 1 && (A2 = A()) != null && !A2.getUserInteraction()) {
            JSONObject b2 = t.b();
            t.a(b2, "ad_session_id", this.e);
            new y("WebView.on_first_click", 1, b2).c();
            A2.setUserInteraction(true);
        }
        return super.onTouchEvent(motionEvent);
    }

    /* access modifiers changed from: package-private */
    public int r() {
        return this.r;
    }

    /* access modifiers changed from: package-private */
    public int s() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public int t() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public void u() {
        a(false, (y) null);
    }

    /* access modifiers changed from: package-private */
    public void v() {
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.q, this.s);
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.gravity = 0;
        this.K.addView(this, layoutParams);
        if (!this.h.equals("") && !this.i.equals("")) {
            z();
        }
    }

    /* access modifiers changed from: package-private */
    public boolean w() {
        return this.A;
    }

    /* access modifiers changed from: package-private */
    public boolean x() {
        return this.E;
    }

    public int d() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        ImageView imageView = this.M;
        if (imageView != null) {
            this.K.a((View) imageView, FriendlyObstructionPurpose.OTHER);
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        ArrayList<a0> i2 = this.K.i();
        c cVar = new c();
        a.a("WebView.set_visible", (a0) cVar, true);
        i2.add(cVar);
        ArrayList<a0> i3 = this.K.i();
        d dVar = new d();
        a.a("WebView.set_bounds", (a0) dVar, true);
        i3.add(dVar);
        ArrayList<a0> i4 = this.K.i();
        e eVar = new e();
        a.a("WebView.execute_js", (a0) eVar, true);
        i4.add(eVar);
        ArrayList<a0> i5 = this.K.i();
        f fVar = new f();
        a.a("WebView.set_transparent", (a0) fVar, true);
        i5.add(fVar);
        this.K.j().add("WebView.set_visible");
        this.K.j().add("WebView.set_bounds");
        this.K.j().add("WebView.execute_js");
        this.K.j().add("WebView.set_transparent");
    }

    /* access modifiers changed from: package-private */
    public void j() {
        a.c().e().a(this, this.e, this.K);
    }

    /* access modifiers changed from: package-private */
    public k k() {
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 23) {
            return new a();
        }
        if (i2 >= 21) {
            return new b();
        }
        return new k(this, (a) null);
    }

    /* access modifiers changed from: package-private */
    public String l() {
        return b((String) null);
    }

    /* access modifiers changed from: package-private */
    public int m() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public int n() {
        return this.q;
    }

    /* access modifiers changed from: package-private */
    public int o() {
        return this.m;
    }

    /* access modifiers changed from: package-private */
    public int p() {
        return this.o;
    }

    /* access modifiers changed from: package-private */
    public int q() {
        return this.t;
    }

    /* access modifiers changed from: package-private */
    public boolean c(y yVar) {
        JSONObject a2 = yVar.a();
        return t.e(a2, "id") == this.l && t.e(a2, "container_id") == this.K.c() && t.g(a2, "ad_session_id").equals(this.K.a());
    }

    /* access modifiers changed from: package-private */
    public String b(String str) {
        String c2 = (!y() || B() == null) ? str : B().c();
        return ((c2 == null || c2.equals(str)) && C() && A() != null) ? A().getClickOverride() : c2;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        if (this.C) {
            new v.a().a("Ignoring call to execute_js as WebView has been destroyed.").a(v.c);
        } else if (Build.VERSION.SDK_INT >= 19) {
            try {
                evaluateJavascript(str, (ValueCallback) null);
            } catch (IllegalStateException unused) {
                new v.a().a("Device reporting incorrect OS version, evaluateJavascript ").a("is not available. Disabling AdColony.").a(v.h);
                AdColony.d();
            }
        } else {
            loadUrl("javascript:" + str);
        }
    }

    private void b(Exception exc) {
        new v.a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(t.g(this.I, "metadata")).a(v.i);
        JSONObject b2 = t.b();
        t.a(b2, "id", this.e);
        new y("AdSession.on_error", this.K.k(), b2).c();
    }

    public int c() {
        return this.v;
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        JSONArray a2 = t.a(str);
        for (int i2 = 0; i2 < a2.length(); i2++) {
            a.c().p().a(t.a(a2, i2));
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        setBackgroundColor(z2 ? 0 : -1);
    }

    /* access modifiers changed from: package-private */
    public void b(y yVar) {
        if (t.c(yVar.a(), ViewProps.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
        if (this.z) {
            JSONObject b2 = t.b();
            t.a(b2, "success", true);
            t.b(b2, "id", this.u);
            yVar.a(b2).c();
        }
    }

    n0(Context context, int i2, boolean z2) {
        super(context);
        this.u = i2;
        this.A = z2;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"AddJavascriptInterface"})
    public void a(boolean z2, y yVar) {
        String str;
        this.z = z2;
        y yVar2 = this.L;
        if (yVar2 != null) {
            yVar = yVar2;
        }
        this.L = yVar;
        JSONObject a2 = yVar.a();
        this.A = t.c(a2, "is_display_module");
        setFocusable(true);
        setHorizontalScrollBarEnabled(false);
        setVerticalScrollBarEnabled(false);
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 19) {
            WebView.setWebContentsDebuggingEnabled(false);
        }
        if (z2) {
            this.y = true;
            String g2 = t.g(a2, "filepath");
            this.j = t.g(a2, "interstitial_html");
            this.g = t.g(a2, "mraid_filepath");
            this.d = t.g(a2, "base_url");
            this.J = t.f(a2, "iab");
            this.I = t.f(a2, "info");
            this.e = t.g(a2, "ad_session_id");
            this.b = g2;
            if (O && this.u == 1) {
                this.b = "android_asset/ADCController.js";
            }
            if (this.j.equals("")) {
                str = "file:///" + this.b;
            } else {
                str = "";
            }
            this.f1310a = str;
        }
        setWebChromeClient(new j(this, (a) null));
        WebSettings settings = getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setBuiltInZoomControls(true);
        settings.setDisplayZoomControls(false);
        settings.setGeolocationEnabled(true);
        settings.setUseWideViewPort(true);
        if (i2 >= 17) {
            settings.setMediaPlaybackRequiresUserGesture(false);
        }
        if (i2 >= 16) {
            settings.setAllowFileAccessFromFileURLs(true);
            settings.setAllowUniversalAccessFromFileURLs(true);
            settings.setAllowFileAccess(true);
        }
        addJavascriptInterface(new i(this, (a) null), "NativeLayer");
        setWebViewClient(k());
        e();
        if (!z2) {
            i();
            v();
        }
        if (z2 || this.y) {
            a.c().p().a((b0) this);
        }
        if (!this.c.equals("")) {
            a(this.c);
        }
    }

    private String a(String str, String str2) {
        d0 d0Var;
        d e2 = a.c().e();
        AdColonyInterstitial B2 = B();
        AdColonyAdViewListener adColonyAdViewListener = e2.c().get(this.e);
        if (B2 != null && this.J.length() > 0 && !t.g(this.J, "ad_type").equals(Advertisement.KEY_VIDEO)) {
            B2.a(this.J);
        } else if (adColonyAdViewListener != null && this.J.length() > 0) {
            adColonyAdViewListener.a(new d0(this.J, this.e));
        }
        if (B2 == null) {
            d0Var = null;
        } else {
            d0Var = B2.e();
        }
        if (d0Var == null && adColonyAdViewListener != null) {
            d0Var = adColonyAdViewListener.b();
        }
        if (d0Var != null && d0Var.d() == 2) {
            this.E = true;
            if (!str2.equals("")) {
                try {
                    return ScriptInjector.a(a.c().n().a(str2, false).toString(), str);
                } catch (IOException e3) {
                    a((Exception) e3);
                }
            }
        }
        return str;
    }

    private boolean a(Exception exc) {
        AdColonyInterstitialListener g2;
        new v.a().a(exc.getClass().toString()).a(" during metadata injection w/ metadata = ").a(t.g(this.I, "metadata")).a(v.i);
        AdColonyInterstitial remove = a.c().e().a().remove(t.g(this.I, "ad_session_id"));
        if (remove == null || (g2 = remove.g()) == null) {
            return false;
        }
        g2.e(remove);
        remove.a(true);
        return true;
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar, int i2, c cVar) {
        a(yVar, i2, -1, cVar);
        v();
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar, int i2, int i3, c cVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, ReportDBAdapter.ReportColumns.COLUMN_URL);
        this.f1310a = g2;
        if (g2.equals("")) {
            this.f1310a = t.g(a2, UriUtil.DATA_SCHEME);
        }
        this.d = t.g(a2, "base_url");
        this.c = t.g(a2, "custom_js");
        this.e = t.g(a2, "ad_session_id");
        this.I = t.f(a2, "info");
        this.g = t.g(a2, "mraid_filepath");
        this.v = t.c(a2, "use_mraid_module") ? a.c().p().d() : this.v;
        this.h = t.g(a2, "ad_choices_filepath");
        this.i = t.g(a2, "ad_choices_url");
        this.F = t.c(a2, "disable_ad_choices");
        this.G = t.c(a2, "ad_choices_snap_to_webview");
        this.w = t.e(a2, "ad_choices_width");
        this.x = t.e(a2, "ad_choices_height");
        if (this.J.length() == 0) {
            this.J = t.f(a2, "iab");
        }
        boolean z2 = false;
        if (!this.A && !this.g.equals("")) {
            if (this.v > 0) {
                this.f1310a = a(this.f1310a.replaceFirst("script\\s*src\\s*=\\s*\"mraid.js\"", "script src=\"file://" + this.g + "\""), t.g(t.f(this.I, "device_info"), "iab_filepath"));
            } else {
                try {
                    this.f = a.c().n().a(this.g, false).toString();
                    this.f = this.f.replaceFirst("bridge.os_name\\s*=\\s*\"\"\\s*;", "bridge.os_name = \"\";\nvar ADC_DEVICE_INFO = " + this.I.toString() + ";\n");
                } catch (IOException e2) {
                    b((Exception) e2);
                } catch (IllegalArgumentException e3) {
                    b((Exception) e3);
                } catch (IndexOutOfBoundsException e4) {
                    b((Exception) e4);
                }
            }
        }
        this.l = i2;
        this.K = cVar;
        if (i3 >= 0) {
            this.u = i3;
        } else {
            i();
        }
        this.q = t.e(a2, "width");
        this.s = t.e(a2, "height");
        this.m = t.e(a2, "x");
        int e5 = t.e(a2, "y");
        this.o = e5;
        this.r = this.q;
        this.t = this.s;
        this.p = e5;
        this.n = this.m;
        if (t.c(a2, "enable_messages") || this.z) {
            z2 = true;
        }
        this.y = z2;
        j();
    }

    public void a(JSONObject jSONObject) {
        synchronized (this.N) {
            this.H.put(jSONObject);
        }
    }

    public void a() {
        if (a.d() && this.B && !this.D) {
            g();
        }
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject, String str) {
        Context b2 = a.b();
        if (b2 != null && (b2 instanceof b)) {
            a.c().e().a(b2, jSONObject, str);
        } else if (this.u == 1) {
            new v.a().a("Unable to communicate with controller, disabling AdColony.").a(v.h);
            AdColony.d();
        } else if (this.v > 0) {
            this.y = false;
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, String str2) {
        if (this.K != null) {
            JSONObject b2 = t.b();
            t.b(b2, "id", this.l);
            t.a(b2, "ad_session_id", this.e);
            t.b(b2, "container_id", this.K.c());
            t.b(b2, "code", i2);
            t.a(b2, "error", str);
            t.a(b2, ReportDBAdapter.ReportColumns.COLUMN_URL, str2);
            new y("WebView.on_error", this.K.k(), b2).c();
        }
        new v.a().a("onReceivedError: ").a(str).a(v.i);
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        JSONObject a2 = yVar.a();
        this.m = t.e(a2, "x");
        this.o = t.e(a2, "y");
        this.q = t.e(a2, "width");
        this.s = t.e(a2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.m, this.o, 0, 0);
        layoutParams.width = this.q;
        layoutParams.height = this.s;
        setLayoutParams(layoutParams);
        if (this.z) {
            JSONObject b2 = t.b();
            t.a(b2, "success", true);
            t.b(b2, "id", this.u);
            yVar.a(b2).c();
        }
        f();
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.C = z2;
    }
}
