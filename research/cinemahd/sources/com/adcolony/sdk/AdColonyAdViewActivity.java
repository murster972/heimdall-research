package com.adcolony.sdk;

import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewParent;

public class AdColonyAdViewActivity extends b {
    AdColonyAdView j;

    public AdColonyAdViewActivity() {
        AdColonyAdView adColonyAdView;
        if (!a.e()) {
            adColonyAdView = null;
        } else {
            adColonyAdView = a.c().h();
        }
        this.j = adColonyAdView;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        ViewParent parent = this.f1172a.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.f1172a);
        }
        this.j.a();
        a.c().a((AdColonyAdView) null);
        finish();
    }

    /* access modifiers changed from: package-private */
    public void c() {
        this.j.b();
    }

    public void onBackPressed() {
        b();
    }

    public void onCreate(Bundle bundle) {
        AdColonyAdView adColonyAdView;
        if (!a.e() || (adColonyAdView = this.j) == null) {
            a.c().a((AdColonyAdView) null);
            finish();
            return;
        }
        this.b = adColonyAdView.getOrientation();
        super.onCreate(bundle);
        this.j.b();
        AdColonyAdViewListener listener = this.j.getListener();
        if (listener != null) {
            listener.d(this.j);
        }
    }
}
