package com.adcolony.sdk;

import android.util.Log;
import com.applovin.sdk.AppLovinEventTypes;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

class x {
    static boolean e = false;
    static int f = 3;
    static int g = 1;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public JSONObject f1365a = t.b();
    private ExecutorService b = null;
    private final Queue<Runnable> c = new ConcurrentLinkedQueue();
    f0 d;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 0, t.g(yVar.a(), "message"), true);
        }
    }

    class b implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ int f1367a;
        final /* synthetic */ String b;
        final /* synthetic */ int c;
        final /* synthetic */ boolean d;

        b(int i, String str, int i2, boolean z) {
            this.f1367a = i;
            this.b = str;
            this.c = i2;
            this.d = z;
        }

        public void run() {
            x.this.a(this.f1367a, this.b, this.c);
            int i = 0;
            while (i <= this.b.length() / 4000) {
                int i2 = i * 4000;
                i++;
                int min = Math.min(i * 4000, this.b.length());
                if (this.c == 3) {
                    x xVar = x.this;
                    if (xVar.a(t.f(xVar.f1365a, Integer.toString(this.f1367a)), 3, this.d)) {
                        Log.d("AdColony [TRACE]", this.b.substring(i2, min));
                    }
                }
                if (this.c == 2) {
                    x xVar2 = x.this;
                    if (xVar2.a(t.f(xVar2.f1365a, Integer.toString(this.f1367a)), 2, this.d)) {
                        Log.i("AdColony [INFO]", this.b.substring(i2, min));
                    }
                }
                if (this.c == 1) {
                    x xVar3 = x.this;
                    if (xVar3.a(t.f(xVar3.f1365a, Integer.toString(this.f1367a)), 1, this.d)) {
                        Log.w("AdColony [WARNING]", this.b.substring(i2, min));
                    }
                }
                if (this.c == 0) {
                    x xVar4 = x.this;
                    if (xVar4.a(t.f(xVar4.f1365a, Integer.toString(this.f1367a)), 0, this.d)) {
                        Log.e("AdColony [ERROR]", this.b.substring(i2, min));
                    }
                }
                if (this.c == -1 && x.f >= -1) {
                    Log.e("AdColony [FATAL]", this.b.substring(i2, min));
                }
            }
        }
    }

    class c implements a0 {
        c(x xVar) {
        }

        public void a(y yVar) {
            x.f = t.e(yVar.a(), AppLovinEventTypes.USER_COMPLETED_LEVEL);
        }
    }

    class d implements a0 {
        d() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 3, t.g(yVar.a(), "message"), false);
        }
    }

    class e implements a0 {
        e() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 3, t.g(yVar.a(), "message"), true);
        }
    }

    class f implements a0 {
        f() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 2, t.g(yVar.a(), "message"), false);
        }
    }

    class g implements a0 {
        g() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 2, t.g(yVar.a(), "message"), true);
        }
    }

    class h implements a0 {
        h() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 1, t.g(yVar.a(), "message"), false);
        }
    }

    class i implements a0 {
        i() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 1, t.g(yVar.a(), "message"), true);
        }
    }

    class j implements a0 {
        j() {
        }

        public void a(y yVar) {
            x.this.a(t.e(yVar.a(), "module"), 0, t.g(yVar.a(), "message"), false);
        }
    }

    x() {
    }

    private Runnable b(int i2, int i3, String str, boolean z) {
        return new b(i2, str, i3, z);
    }

    /* access modifiers changed from: package-private */
    public void c() {
        ExecutorService executorService = this.b;
        if (executorService == null || executorService.isShutdown() || this.b.isTerminated()) {
            this.b = Executors.newSingleThreadExecutor();
        }
        synchronized (this.c) {
            while (!this.c.isEmpty()) {
                a(this.c.poll());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, int i3, String str, boolean z) {
        if (!a(b(i2, i3, str, z))) {
            synchronized (this.c) {
                this.c.add(b(i2, i3, str, z));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b() {
        a.a("Log.set_log_level", (a0) new c(this));
        a.a("Log.public.trace", (a0) new d());
        a.a("Log.private.trace", (a0) new e());
        a.a("Log.public.info", (a0) new f());
        a.a("Log.private.info", (a0) new g());
        a.a("Log.public.warning", (a0) new h());
        a.a("Log.private.warning", (a0) new i());
        a.a("Log.public.error", (a0) new j());
        a.a("Log.private.error", (a0) new a());
    }

    /* access modifiers changed from: package-private */
    public void a(int i2, String str, boolean z) {
        a(0, i2, str, z);
    }

    /* access modifiers changed from: package-private */
    public boolean a(JSONObject jSONObject, int i2, boolean z) {
        int e2 = t.e(jSONObject, "print_level");
        boolean c2 = t.c(jSONObject, "log_private");
        if (jSONObject.length() == 0) {
            e2 = f;
            c2 = e;
        }
        return (!z || c2) && e2 != 4 && e2 >= i2;
    }

    /* access modifiers changed from: package-private */
    public void b(JSONArray jSONArray) {
        this.f1365a = a(jSONArray);
    }

    /* access modifiers changed from: package-private */
    public boolean a(JSONObject jSONObject, int i2) {
        int e2 = t.e(jSONObject, "send_level");
        if (jSONObject.length() == 0) {
            e2 = g;
        }
        return e2 >= i2 && e2 != 4;
    }

    /* access modifiers changed from: package-private */
    public void a(HashMap<String, Object> hashMap) {
        try {
            f0 f0Var = new f0(new u(new URL("https://wd.adcolony.com/logs")), Executors.newSingleThreadScheduledExecutor(), hashMap);
            this.d = f0Var;
            f0Var.a(5, TimeUnit.SECONDS);
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        }
    }

    /* access modifiers changed from: private */
    public void a(int i2, String str, int i3) {
        if (this.d != null) {
            if (i3 == 3 && a(t.f(this.f1365a, Integer.toString(i2)), 3)) {
                this.d.a(str);
            } else if (i3 == 2 && a(t.f(this.f1365a, Integer.toString(i2)), 2)) {
                this.d.c(str);
            } else if (i3 == 1 && a(t.f(this.f1365a, Integer.toString(i2)), 1)) {
                this.d.d(str);
            } else if (i3 == 0 && a(t.f(this.f1365a, Integer.toString(i2)), 0)) {
                this.d.b(str);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(JSONArray jSONArray) {
        JSONObject b2 = t.b();
        for (int i2 = 0; i2 < jSONArray.length(); i2++) {
            JSONObject a2 = t.a(jSONArray, i2);
            t.a(b2, Integer.toString(t.e(a2, "id")), a2);
        }
        return b2;
    }

    private boolean a(Runnable runnable) {
        try {
            ExecutorService executorService = this.b;
            if (executorService == null || executorService.isShutdown() || this.b.isTerminated()) {
                return false;
            }
            this.b.submit(runnable);
            return true;
        } catch (RejectedExecutionException unused) {
            Log.e("ADCLogError", "Internal error when submitting log to executor service.");
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public f0 a() {
        return this.d;
    }
}
