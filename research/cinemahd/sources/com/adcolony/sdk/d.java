package com.adcolony.sdk;

import android.app.AlertDialog;
import android.content.Context;
import android.view.View;
import android.webkit.WebView;
import com.adcolony.sdk.v;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;
import org.json.JSONObject;

class d {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<String, c> f1187a;
    /* access modifiers changed from: private */
    public ConcurrentHashMap<String, AdColonyInterstitial> b;
    private HashMap<String, AdColonyAdViewListener> c;
    /* access modifiers changed from: private */
    public HashMap<String, AdColonyAdView> d;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            d.this.a(yVar);
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            d.this.d(yVar);
        }
    }

    class c implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1191a;

            a(y yVar) {
                this.f1191a = yVar;
            }

            public void run() {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(t.g(this.f1191a.a(), "id"));
                if (adColonyInterstitial != null && adColonyInterstitial.g() != null) {
                    adColonyInterstitial.g().b(adColonyInterstitial);
                }
            }
        }

        c() {
        }

        public void a(y yVar) {
            l0.a((Runnable) new a(yVar));
        }
    }

    /* renamed from: com.adcolony.sdk.d$d  reason: collision with other inner class name */
    class C0003d implements a0 {

        /* renamed from: com.adcolony.sdk.d$d$a */
        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1193a;

            a(y yVar) {
                this.f1193a = yVar;
            }

            public void run() {
                AdColonyInterstitial adColonyInterstitial = (AdColonyInterstitial) d.this.b.get(t.g(this.f1193a.a(), "id"));
                if (adColonyInterstitial != null && adColonyInterstitial.g() != null) {
                    adColonyInterstitial.g().a(adColonyInterstitial);
                }
            }
        }

        C0003d() {
        }

        public void a(y yVar) {
            l0.a((Runnable) new a(yVar));
        }
    }

    class e implements a0 {
        e() {
        }

        public void a(y yVar) {
            d.this.f(yVar);
        }
    }

    class f implements a0 {
        f() {
        }

        public void a(y yVar) {
            d.this.e(yVar);
        }
    }

    class g implements a0 {
        g() {
        }

        public void a(y yVar) {
            boolean unused = d.this.i(yVar);
        }
    }

    class h implements a0 {
        h(d dVar) {
        }

        public void a(y yVar) {
            JSONObject b = t.b();
            t.a(b, "success", true);
            yVar.a(b).c();
        }
    }

    class i implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1197a;

            a(i iVar, y yVar) {
                this.f1197a = yVar;
            }

            public void run() {
                y yVar = this.f1197a;
                yVar.a(yVar.a()).c();
            }
        }

        i(d dVar) {
        }

        public void a(y yVar) {
            l0.a((Runnable) new a(this, yVar));
        }
    }

    class j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyInterstitial f1198a;
        final /* synthetic */ AdColonyInterstitialListener b;

        j(d dVar, AdColonyInterstitial adColonyInterstitial, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.f1198a = adColonyInterstitial;
            this.b = adColonyInterstitialListener;
        }

        public void run() {
            this.f1198a.a(true);
            this.b.e(this.f1198a);
            l l = a.c().l();
            if (l.a() != null) {
                l.a().dismiss();
                l.a((AlertDialog) null);
            }
        }
    }

    class k implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1199a;
        final /* synthetic */ y b;
        final /* synthetic */ AdColonyAdViewListener c;
        final /* synthetic */ String d;

        k(Context context, y yVar, AdColonyAdViewListener adColonyAdViewListener, String str) {
            this.f1199a = context;
            this.b = yVar;
            this.c = adColonyAdViewListener;
            this.d = str;
        }

        public void run() {
            AdColonyAdView adColonyAdView = new AdColonyAdView(this.f1199a, this.b, this.c);
            d.this.d.put(this.d, adColonyAdView);
            adColonyAdView.setOmidManager(this.c.b());
            adColonyAdView.d();
            this.c.a((d0) null);
            this.c.e(adColonyAdView);
        }
    }

    class l implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyInterstitial f1200a;
        final /* synthetic */ y b;
        final /* synthetic */ AdColonyInterstitialListener c;

        l(d dVar, AdColonyInterstitial adColonyInterstitial, y yVar, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.f1200a = adColonyInterstitial;
            this.b = yVar;
            this.c = adColonyInterstitialListener;
        }

        public void run() {
            if (this.f1200a.e() == null) {
                this.f1200a.a(t.f(this.b.a(), "iab"));
            }
            this.f1200a.a(t.g(this.b.a(), "ad_id"));
            this.f1200a.c(t.g(this.b.a(), "creative_id"));
            d0 e = this.f1200a.e();
            if (!(e == null || e.d() == 2)) {
                try {
                    e.a();
                } catch (IllegalArgumentException unused) {
                    new v.a().a("IllegalArgumentException when creating omid session").a(v.i);
                }
            }
            this.c.h(this.f1200a);
        }
    }

    class m implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyInterstitial f1201a;
        final /* synthetic */ AdColonyInterstitialListener b;

        m(d dVar, AdColonyInterstitial adColonyInterstitial, AdColonyInterstitialListener adColonyInterstitialListener) {
            this.f1201a = adColonyInterstitial;
            this.b = adColonyInterstitialListener;
        }

        public void run() {
            AdColonyZone adColonyZone = a.c().A().get(this.f1201a.h());
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(this.f1201a.h());
                adColonyZone.b(6);
            }
            this.b.a(adColonyZone);
        }
    }

    class n implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyInterstitialListener f1202a;
        final /* synthetic */ AdColonyInterstitial b;

        n(d dVar, AdColonyInterstitialListener adColonyInterstitialListener, AdColonyInterstitial adColonyInterstitial) {
            this.f1202a = adColonyInterstitialListener;
            this.b = adColonyInterstitial;
        }

        public void run() {
            a.c().d(false);
            this.f1202a.d(this.b);
        }
    }

    class o implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1203a;
        final /* synthetic */ n0 b;
        final /* synthetic */ c c;

        o(String str, n0 n0Var, c cVar) {
            this.f1203a = str;
            this.b = n0Var;
            this.c = cVar;
        }

        public void run() {
            d0 d0Var;
            int i;
            try {
                AdColonyInterstitial adColonyInterstitial = d.this.a().get(this.f1203a);
                AdColonyAdView adColonyAdView = d.this.b().get(this.f1203a);
                if (adColonyInterstitial == null) {
                    d0Var = null;
                } else {
                    d0Var = adColonyInterstitial.e();
                }
                if (d0Var == null && adColonyAdView != null) {
                    d0Var = adColonyAdView.getOmidManager();
                }
                if (d0Var == null) {
                    i = -1;
                } else {
                    i = d0Var.d();
                }
                if (d0Var != null && i == 2) {
                    d0Var.a((WebView) this.b);
                    d0Var.a(this.c);
                }
            } catch (IllegalArgumentException unused) {
                new v.a().a("IllegalArgumentException when creating omid session").a(v.i);
            }
        }
    }

    class p implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ c f1204a;

        p(d dVar, c cVar) {
            this.f1204a = cVar;
        }

        public void run() {
            for (int i = 0; i < this.f1204a.i().size(); i++) {
                a.b(this.f1204a.j().get(i), this.f1204a.i().get(i));
            }
            this.f1204a.j().clear();
            this.f1204a.i().clear();
            this.f1204a.removeAllViews();
            c cVar = this.f1204a;
            cVar.z = null;
            cVar.y = null;
            for (n0 next : cVar.n().values()) {
                if (!next.w()) {
                    int c = next.c();
                    if (c <= 0) {
                        c = next.d();
                    }
                    a.c().a(c);
                    next.loadUrl("about:blank");
                    next.clearCache(true);
                    next.removeAllViews();
                    next.a(true);
                }
            }
            for (m0 next2 : this.f1204a.m().values()) {
                next2.i();
                next2.j();
            }
            this.f1204a.m().clear();
            this.f1204a.l().clear();
            this.f1204a.n().clear();
            this.f1204a.h().clear();
            this.f1204a.e().clear();
            this.f1204a.f().clear();
            this.f1204a.g().clear();
            this.f1204a.m = true;
        }
    }

    class q implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ AdColonyAdViewListener f1205a;

        q(d dVar, AdColonyAdViewListener adColonyAdViewListener) {
            this.f1205a = adColonyAdViewListener;
        }

        public void run() {
            String c = this.f1205a.c();
            AdColonyZone adColonyZone = a.c().A().get(c);
            if (adColonyZone == null) {
                adColonyZone = new AdColonyZone(c);
                adColonyZone.b(6);
            }
            this.f1205a.a(adColonyZone);
        }
    }

    class r implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1207a;

            a(y yVar) {
                this.f1207a = yVar;
            }

            public void run() {
                d.this.c(this.f1207a);
            }
        }

        r() {
        }

        public void a(y yVar) {
            l0.a((Runnable) new a(yVar));
        }
    }

    class s implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1209a;

            a(y yVar) {
                this.f1209a = yVar;
            }

            public void run() {
                boolean unused = d.this.g(this.f1209a);
            }
        }

        s() {
        }

        public void a(y yVar) {
            l0.a((Runnable) new a(yVar));
        }
    }

    class t implements a0 {
        t() {
        }

        public void a(y yVar) {
            boolean unused = d.this.k(yVar);
        }
    }

    class u implements a0 {
        u() {
        }

        public void a(y yVar) {
            boolean unused = d.this.j(yVar);
        }
    }

    class v implements a0 {
        v() {
        }

        public void a(y yVar) {
            boolean unused = d.this.h(yVar);
        }
    }

    class w implements a0 {
        w() {
        }

        public void a(y yVar) {
            boolean unused = d.this.l(yVar);
        }
    }

    class x implements a0 {
        x() {
        }

        public void a(y yVar) {
            d.this.b(yVar);
        }
    }

    d() {
    }

    /* access modifiers changed from: private */
    public boolean g(y yVar) {
        String g2 = t.g(yVar.a(), "ad_session_id");
        c cVar = this.f1187a.get(g2);
        if (cVar == null) {
            a(yVar.b(), g2);
            return false;
        }
        a(cVar);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean h(y yVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject a2 = yVar.a();
        int e2 = t.e(a2, ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
        if (e2 == 5 || e2 == 1 || e2 == 0 || e2 == 6) {
            return false;
        }
        String g2 = t.g(a2, "id");
        AdColonyInterstitial remove = this.b.remove(g2);
        if (remove == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = remove.g();
        }
        if (adColonyInterstitialListener == null) {
            a(yVar.b(), g2);
            return false;
        }
        l0.a((Runnable) new n(this, adColonyInterstitialListener, remove));
        remove.a((c) null);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean i(y yVar) {
        String g2 = t.g(yVar.a(), "id");
        JSONObject b2 = t.b();
        t.a(b2, "id", g2);
        Context b3 = a.b();
        if (b3 == null) {
            t.a(b2, "has_audio", false);
            yVar.a(b2).c();
            return false;
        }
        boolean b4 = l0.b(l0.a(b3));
        double a2 = l0.a(l0.a(b3));
        t.a(b2, "has_audio", b4);
        t.a(b2, "volume", a2);
        yVar.a(b2).c();
        return b4;
    }

    /* access modifiers changed from: private */
    public boolean j(y yVar) {
        JSONObject a2 = yVar.a();
        String b2 = yVar.b();
        String g2 = t.g(a2, "ad_session_id");
        int e2 = t.e(a2, "view_id");
        c cVar = this.f1187a.get(g2);
        if (cVar == null) {
            a(b2, g2);
            return false;
        }
        View view = cVar.e().get(Integer.valueOf(e2));
        if (view == null) {
            a(b2, "" + e2);
            return false;
        }
        cVar.removeView(view);
        cVar.addView(view, view.getLayoutParams());
        return true;
    }

    /* access modifiers changed from: private */
    public boolean k(y yVar) {
        JSONObject a2 = yVar.a();
        String b2 = yVar.b();
        String g2 = t.g(a2, "ad_session_id");
        int e2 = t.e(a2, "view_id");
        c cVar = this.f1187a.get(g2);
        if (cVar == null) {
            a(b2, g2);
            return false;
        }
        View view = cVar.e().get(Integer.valueOf(e2));
        if (view == null) {
            a(b2, "" + e2);
            return false;
        }
        view.bringToFront();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean l(y yVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "id");
        AdColonyInterstitial adColonyInterstitial = this.b.get(g2);
        AdColonyAdView adColonyAdView = this.d.get(g2);
        int a3 = t.a(a2, AdUnitActivity.EXTRA_ORIENTATION, -1);
        boolean z = adColonyAdView != null;
        if (adColonyInterstitial != null || z) {
            t.a(t.b(), "id", g2);
            if (adColonyInterstitial != null) {
                adColonyInterstitial.a(a3);
                adColonyInterstitial.k();
            }
            return true;
        }
        a(yVar.b(), g2);
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(y yVar) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "ad_session_id");
        c cVar = new c(b2.getApplicationContext(), g2);
        cVar.j(yVar);
        this.f1187a.put(g2, cVar);
        if (t.e(a2, "width") == 0) {
            AdColonyInterstitial adColonyInterstitial = this.b.get(g2);
            if (adColonyInterstitial == null) {
                a(yVar.b(), g2);
                return false;
            }
            adColonyInterstitial.a(cVar);
        } else {
            cVar.a(false);
        }
        JSONObject b3 = t.b();
        t.a(b3, "success", true);
        yVar.a(b3).c();
        return true;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, c> d() {
        return this.f1187a;
    }

    /* access modifiers changed from: package-private */
    public boolean e(y yVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        String g2 = t.g(yVar.a(), "id");
        AdColonyInterstitial remove = this.b.remove(g2);
        if (remove == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = remove.g();
        }
        if (adColonyInterstitialListener == null) {
            a(yVar.b(), g2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            l0.a((Runnable) new m(this, remove, adColonyInterstitialListener));
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean f(y yVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        String g2 = t.g(yVar.a(), "id");
        AdColonyInterstitial adColonyInterstitial = this.b.get(g2);
        if (adColonyInterstitial == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = adColonyInterstitial.g();
        }
        if (adColonyInterstitialListener == null) {
            a(yVar.b(), g2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            l0.a((Runnable) new l(this, adColonyInterstitial, yVar, adColonyInterstitialListener));
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public boolean a(y yVar) {
        String g2 = t.g(yVar.a(), "id");
        AdColonyAdViewListener remove = this.c.remove(g2);
        if (remove == null) {
            a(yVar.b(), g2);
            return false;
        }
        l0.a((Runnable) new q(this, remove));
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean b(y yVar) {
        String g2 = t.g(yVar.a(), "id");
        AdColonyAdViewListener remove = this.c.remove(g2);
        if (remove == null) {
            a(yVar.b(), g2);
            return false;
        }
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        l0.a((Runnable) new k(b2, yVar, remove, g2));
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean d(y yVar) {
        AdColonyInterstitialListener adColonyInterstitialListener;
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "id");
        if (t.e(a2, "type") != 0) {
            return true;
        }
        AdColonyInterstitial remove = this.b.remove(g2);
        if (remove == null) {
            adColonyInterstitialListener = null;
        } else {
            adColonyInterstitialListener = remove.g();
        }
        if (adColonyInterstitialListener == null) {
            a(yVar.b(), g2);
            return false;
        } else if (!a.d()) {
            return false;
        } else {
            l0.a((Runnable) new j(this, remove, adColonyInterstitialListener));
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, AdColonyAdViewListener adColonyAdViewListener, AdColonyAdSize adColonyAdSize, AdColonyAdOptions adColonyAdOptions) {
        JSONObject jSONObject;
        String a2 = l0.a();
        JSONObject b2 = t.b();
        float x2 = a.c().k().x();
        t.a(b2, "zone_id", str);
        t.b(b2, "type", 1);
        t.b(b2, "width_pixels", (int) (((float) adColonyAdSize.b()) * x2));
        t.b(b2, "height_pixels", (int) (((float) adColonyAdSize.a()) * x2));
        t.b(b2, "width", adColonyAdSize.b());
        t.b(b2, "height", adColonyAdSize.a());
        t.a(b2, "id", a2);
        adColonyAdViewListener.a(str);
        adColonyAdViewListener.a(adColonyAdSize);
        if (!(adColonyAdOptions == null || (jSONObject = adColonyAdOptions.c) == null)) {
            t.a(b2, "options", jSONObject);
        }
        this.c.put(a2, adColonyAdViewListener);
        new y("AdSession.on_request", 1, b2).c();
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.f1187a = new HashMap<>();
        this.b = new ConcurrentHashMap<>();
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        a.a("AdContainer.create", (a0) new r());
        a.a("AdContainer.destroy", (a0) new s());
        a.a("AdContainer.move_view_to_index", (a0) new t());
        a.a("AdContainer.move_view_to_front", (a0) new u());
        a.a("AdSession.finish_fullscreen_ad", (a0) new v());
        a.a("AdSession.start_fullscreen_ad", (a0) new w());
        a.a("AdSession.ad_view_available", (a0) new x());
        a.a("AdSession.ad_view_unavailable", (a0) new a());
        a.a("AdSession.expiring", (a0) new b());
        a.a("AdSession.audio_stopped", (a0) new c());
        a.a("AdSession.audio_started", (a0) new C0003d());
        a.a("AdSession.interstitial_available", (a0) new e());
        a.a("AdSession.interstitial_unavailable", (a0) new f());
        a.a("AdSession.has_audio", (a0) new g());
        a.a("WebView.prepare", (a0) new h(this));
        a.a("AdSession.expanded", (a0) new i(this));
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyAdView> b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public HashMap<String, AdColonyAdViewListener> c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void a(String str, AdColonyInterstitialListener adColonyInterstitialListener, AdColonyAdOptions adColonyAdOptions) {
        String a2 = l0.a();
        i c2 = a.c();
        JSONObject b2 = t.b();
        t.a(b2, "zone_id", str);
        t.a(b2, "fullscreen", true);
        t.b(b2, "width", c2.k().B());
        t.b(b2, "height", c2.k().A());
        t.b(b2, "type", 0);
        t.a(b2, "id", a2);
        AdColonyInterstitial adColonyInterstitial = new AdColonyInterstitial(a2, adColonyInterstitialListener, str);
        this.b.put(a2, adColonyInterstitial);
        if (!(adColonyAdOptions == null || adColonyAdOptions.c == null)) {
            adColonyInterstitial.a(adColonyAdOptions);
            t.a(b2, "options", adColonyAdOptions.c);
        }
        new y("AdSession.on_request", 1, b2).c();
    }

    /* access modifiers changed from: package-private */
    public void a(Context context, JSONObject jSONObject, String str) {
        y yVar = new y("AdSession.finish_fullscreen_ad", 0);
        t.b(jSONObject, ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, 1);
        new v.a().a(str).a(v.h);
        ((b) context).a(yVar);
    }

    /* access modifiers changed from: package-private */
    public void a(n0 n0Var, String str, c cVar) {
        l0.a((Runnable) new o(str, n0Var, cVar));
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        l0.a((Runnable) new p(this, cVar));
        AdColonyAdView adColonyAdView = this.d.get(cVar.a());
        if (adColonyAdView == null || adColonyAdView.c()) {
            this.f1187a.remove(cVar.a());
            cVar.y = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2) {
        new v.a().a("Message '").a(str).a("' sent with invalid id: ").a(str2).a(v.h);
    }

    /* access modifiers changed from: package-private */
    public ConcurrentHashMap<String, AdColonyInterstitial> a() {
        return this.b;
    }
}
