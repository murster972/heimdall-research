package com.adcolony.sdk;

import com.adcolony.sdk.v;
import com.facebook.common.util.ByteConstants;
import com.facebook.common.util.UriUtil;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.LinkedList;
import java.util.zip.GZIPInputStream;
import org.json.JSONArray;
import org.json.JSONObject;

class q {

    /* renamed from: a  reason: collision with root package name */
    private LinkedList<Runnable> f1336a = new LinkedList<>();
    private boolean b;

    class a implements a0 {

        /* renamed from: com.adcolony.sdk.q$a$a  reason: collision with other inner class name */
        class C0006a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1338a;

            C0006a(y yVar) {
                this.f1338a = yVar;
            }

            public void run() {
                boolean unused = q.this.g(this.f1338a);
                q.this.b();
            }
        }

        a() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new C0006a(yVar));
        }
    }

    class b implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1340a;

            a(y yVar) {
                this.f1340a = yVar;
            }

            public void run() {
                boolean unused = q.this.a(this.f1340a, new File(t.g(this.f1340a.a(), "filepath")));
                q.this.b();
            }
        }

        b() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class c implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1342a;

            a(y yVar) {
                this.f1342a = yVar;
            }

            public void run() {
                boolean unused = q.this.d(this.f1342a);
                q.this.b();
            }
        }

        c() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class d implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1344a;

            a(y yVar) {
                this.f1344a = yVar;
            }

            public void run() {
                String unused = q.this.e(this.f1344a);
                q.this.b();
            }
        }

        d() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class e implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1346a;

            a(y yVar) {
                this.f1346a = yVar;
            }

            public void run() {
                boolean unused = q.this.f(this.f1346a);
                q.this.b();
            }
        }

        e() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class f implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1348a;

            a(y yVar) {
                this.f1348a = yVar;
            }

            public void run() {
                boolean unused = q.this.b(this.f1348a);
                q.this.b();
            }
        }

        f() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class g implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1350a;

            a(y yVar) {
                this.f1350a = yVar;
            }

            public void run() {
                boolean unused = q.this.c(this.f1350a);
                q.this.b();
            }
        }

        g() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class h implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1352a;

            a(y yVar) {
                this.f1352a = yVar;
            }

            public void run() {
                boolean unused = q.this.h(this.f1352a);
                q.this.b();
            }
        }

        h() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    class i implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1354a;

            a(y yVar) {
                this.f1354a = yVar;
            }

            public void run() {
                boolean unused = q.this.a(this.f1354a);
                q.this.b();
            }
        }

        i() {
        }

        public void a(y yVar) {
            q.this.a((Runnable) new a(yVar));
        }
    }

    q() {
    }

    /* access modifiers changed from: private */
    public void b() {
        this.b = false;
        if (!this.f1336a.isEmpty()) {
            this.b = true;
            this.f1336a.removeLast().run();
        }
    }

    /* access modifiers changed from: private */
    public boolean c(y yVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "filepath");
        a.c().x().g();
        JSONObject b2 = t.b();
        try {
            int e2 = t.e(a2, "offset");
            int e3 = t.e(a2, "size");
            boolean c2 = t.c(a2, "gunzip");
            String g3 = t.g(a2, "output_filepath");
            i0 i0Var = new i0(new FileInputStream(g2), e2, e3);
            InputStream gZIPInputStream = c2 ? new GZIPInputStream(i0Var, ByteConstants.KB) : i0Var;
            if (g3.equals("")) {
                StringBuilder sb = new StringBuilder(gZIPInputStream.available());
                byte[] bArr = new byte[ByteConstants.KB];
                while (true) {
                    int read = gZIPInputStream.read(bArr, 0, ByteConstants.KB);
                    if (read < 0) {
                        break;
                    }
                    sb.append(new String(bArr, 0, read, "ISO-8859-1"));
                }
                t.b(b2, "size", sb.length());
                t.a(b2, UriUtil.DATA_SCHEME, sb.toString());
            } else {
                FileOutputStream fileOutputStream = new FileOutputStream(g3);
                byte[] bArr2 = new byte[ByteConstants.KB];
                int i2 = 0;
                while (true) {
                    int read2 = gZIPInputStream.read(bArr2, 0, ByteConstants.KB);
                    if (read2 < 0) {
                        break;
                    }
                    fileOutputStream.write(bArr2, 0, read2);
                    i2 += read2;
                }
                fileOutputStream.close();
                t.b(b2, "size", i2);
            }
            gZIPInputStream.close();
            t.a(b2, "success", true);
            yVar.a(b2).c();
            return true;
        } catch (IOException unused) {
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return false;
        } catch (OutOfMemoryError unused2) {
            new v.a().a("Out of memory error - disabling AdColony.").a(v.h);
            a.c().a(true);
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean d(y yVar) {
        String g2 = t.g(yVar.a(), "filepath");
        a.c().x().g();
        JSONObject b2 = t.b();
        String[] list = new File(g2).list();
        if (list != null) {
            JSONArray a2 = t.a();
            for (String str : list) {
                JSONObject b3 = t.b();
                t.a(b3, "filename", str);
                if (new File(g2 + str).isDirectory()) {
                    t.a(b3, "is_folder", true);
                } else {
                    t.a(b3, "is_folder", false);
                }
                t.a(a2, (Object) b3);
            }
            t.a(b2, "success", true);
            t.a(b2, "entries", a2);
            yVar.a(b2).c();
            return true;
        }
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: private */
    public String e(y yVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "filepath");
        String g3 = t.g(a2, "encoding");
        boolean z = g3 != null && g3.equals("utf8");
        a.c().x().g();
        JSONObject b2 = t.b();
        try {
            StringBuilder a3 = a(g2, z);
            t.a(b2, "success", true);
            t.a(b2, UriUtil.DATA_SCHEME, a3.toString());
            yVar.a(b2).c();
            return a3.toString();
        } catch (IOException unused) {
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return "";
        }
    }

    /* access modifiers changed from: private */
    public boolean f(y yVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "filepath");
        String g3 = t.g(a2, "new_filepath");
        a.c().x().g();
        JSONObject b2 = t.b();
        try {
            if (new File(g2).renameTo(new File(g3))) {
                t.a(b2, "success", true);
                yVar.a(b2).c();
                return true;
            }
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return false;
        } catch (Exception unused) {
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public boolean g(y yVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "filepath");
        String g3 = t.g(a2, UriUtil.DATA_SCHEME);
        boolean equals = t.g(a2, "encoding").equals("utf8");
        a.c().x().g();
        JSONObject b2 = t.b();
        try {
            a(g2, g3, equals);
            t.a(b2, "success", true);
            yVar.a(b2).c();
            return true;
        } catch (IOException unused) {
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return false;
        }
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x00f7, code lost:
        new com.adcolony.sdk.v.a().a("Out of memory error - disabling AdColony.").a(com.adcolony.sdk.v.h);
        com.adcolony.sdk.a.c().a(true);
        com.adcolony.sdk.t.a(r5, "success", false);
        r0.a(r5).c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x011a, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x011b, code lost:
        r2 = false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:24:? A[ExcHandler: OutOfMemoryError (unused java.lang.OutOfMemoryError), SYNTHETIC, Splitter:B:1:0x0029] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean h(com.adcolony.sdk.y r20) {
        /*
            r19 = this;
            r0 = r20
            java.lang.String r1 = "success"
            org.json.JSONObject r2 = r20.a()
            java.lang.String r3 = "filepath"
            java.lang.String r3 = com.adcolony.sdk.t.g(r2, r3)
            java.lang.String r4 = "bundle_path"
            java.lang.String r4 = com.adcolony.sdk.t.g(r2, r4)
            java.lang.String r5 = "bundle_filenames"
            org.json.JSONArray r2 = com.adcolony.sdk.t.b((org.json.JSONObject) r2, (java.lang.String) r5)
            com.adcolony.sdk.i r5 = com.adcolony.sdk.a.c()
            com.adcolony.sdk.h0 r5 = r5.x()
            r5.g()
            org.json.JSONObject r5 = com.adcolony.sdk.t.b()
            java.io.File r8 = new java.io.File     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r8.<init>(r4)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.io.RandomAccessFile r9 = new java.io.RandomAccessFile     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.lang.String r10 = "r"
            r9.<init>(r8, r10)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r10 = 32
            byte[] r10 = new byte[r10]     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r9.readInt()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            int r11 = r9.readInt()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            org.json.JSONArray r12 = new org.json.JSONArray     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r12.<init>()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r13 = 1024(0x400, float:1.435E-42)
            byte[] r14 = new byte[r13]     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r15 = 0
        L_0x004a:
            if (r15 >= r11) goto L_0x00dd
            int r16 = r15 * 44
            int r6 = r16 + 8
            r17 = r8
            long r7 = (long) r6     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r9.seek(r7)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r9.read(r10)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r9.readInt()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            int r6 = r9.readInt()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            int r7 = r9.readInt()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r12.put(r7)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x00b3 }
            r8.<init>()     // Catch:{ JSONException -> 0x00b3 }
            r8.append(r3)     // Catch:{ JSONException -> 0x00b3 }
            java.lang.Object r13 = r2.get(r15)     // Catch:{ JSONException -> 0x00b3 }
            r8.append(r13)     // Catch:{ JSONException -> 0x00b3 }
            java.lang.String r8 = r8.toString()     // Catch:{ JSONException -> 0x00b3 }
            r18 = r2
            r13 = r3
            long r2 = (long) r6
            r9.seek(r2)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.<init>(r8)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            int r3 = r7 / 1024
            int r7 = r7 % 1024
            r6 = 0
        L_0x008b:
            if (r6 >= r3) goto L_0x009d
            r16 = r3
            r3 = 1024(0x400, float:1.435E-42)
            r8 = 0
            r9.read(r14, r8, r3)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.write(r14, r8, r3)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            int r6 = r6 + 1
            r3 = r16
            goto L_0x008b
        L_0x009d:
            r3 = 1024(0x400, float:1.435E-42)
            r8 = 0
            r9.read(r14, r8, r7)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.write(r14, r8, r7)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.close()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            int r15 = r15 + 1
            r3 = r13
            r8 = r17
            r2 = r18
            r13 = 1024(0x400, float:1.435E-42)
            goto L_0x004a
        L_0x00b3:
            com.adcolony.sdk.v$a r2 = new com.adcolony.sdk.v$a     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.<init>()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.lang.String r3 = "Couldn't extract file name at index "
            com.adcolony.sdk.v$a r2 = r2.a((java.lang.String) r3)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            com.adcolony.sdk.v$a r2 = r2.a((int) r15)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.lang.String r3 = " unpacking ad unit bundle at "
            com.adcolony.sdk.v$a r2 = r2.a((java.lang.String) r3)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            com.adcolony.sdk.v$a r2 = r2.a((java.lang.String) r4)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            com.adcolony.sdk.v r3 = com.adcolony.sdk.v.h     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.a((com.adcolony.sdk.v) r3)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2 = 0
            com.adcolony.sdk.t.a((org.json.JSONObject) r5, (java.lang.String) r1, (boolean) r2)     // Catch:{ IOException -> 0x011c, OutOfMemoryError -> 0x00f7 }
            com.adcolony.sdk.y r3 = r0.a(r5)     // Catch:{ IOException -> 0x011c, OutOfMemoryError -> 0x00f7 }
            r3.c()     // Catch:{ IOException -> 0x011c, OutOfMemoryError -> 0x00f7 }
            return r2
        L_0x00dd:
            r17 = r8
            r9.close()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r17.delete()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2 = 1
            com.adcolony.sdk.t.a((org.json.JSONObject) r5, (java.lang.String) r1, (boolean) r2)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            java.lang.String r2 = "file_sizes"
            com.adcolony.sdk.t.a((org.json.JSONObject) r5, (java.lang.String) r2, (org.json.JSONArray) r12)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            com.adcolony.sdk.y r2 = r0.a(r5)     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r2.c()     // Catch:{ IOException -> 0x011b, OutOfMemoryError -> 0x00f7 }
            r0 = 1
            return r0
        L_0x00f7:
            com.adcolony.sdk.v$a r2 = new com.adcolony.sdk.v$a
            r2.<init>()
            java.lang.String r3 = "Out of memory error - disabling AdColony."
            com.adcolony.sdk.v$a r2 = r2.a((java.lang.String) r3)
            com.adcolony.sdk.v r3 = com.adcolony.sdk.v.h
            r2.a((com.adcolony.sdk.v) r3)
            com.adcolony.sdk.i r2 = com.adcolony.sdk.a.c()
            r3 = 1
            r2.a((boolean) r3)
            r2 = 0
            com.adcolony.sdk.t.a((org.json.JSONObject) r5, (java.lang.String) r1, (boolean) r2)
            com.adcolony.sdk.y r0 = r0.a(r5)
            r0.c()
            return r2
        L_0x011b:
            r2 = 0
        L_0x011c:
            com.adcolony.sdk.v$a r3 = new com.adcolony.sdk.v$a
            r3.<init>()
            java.lang.String r6 = "Failed to find or open ad unit bundle at path: "
            com.adcolony.sdk.v$a r3 = r3.a((java.lang.String) r6)
            com.adcolony.sdk.v$a r3 = r3.a((java.lang.String) r4)
            com.adcolony.sdk.v r4 = com.adcolony.sdk.v.i
            r3.a((com.adcolony.sdk.v) r4)
            com.adcolony.sdk.t.a((org.json.JSONObject) r5, (java.lang.String) r1, (boolean) r2)
            com.adcolony.sdk.y r0 = r0.a(r5)
            r0.c()
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.q.h(com.adcolony.sdk.y):boolean");
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("FileSystem.save", (a0) new a());
        a.a("FileSystem.delete", (a0) new b());
        a.a("FileSystem.listing", (a0) new c());
        a.a("FileSystem.load", (a0) new d());
        a.a("FileSystem.rename", (a0) new e());
        a.a("FileSystem.exists", (a0) new f());
        a.a("FileSystem.extract", (a0) new g());
        a.a("FileSystem.unpack_bundle", (a0) new h());
        a.a("FileSystem.create_directory", (a0) new i());
    }

    /* access modifiers changed from: private */
    public boolean b(y yVar) {
        String g2 = t.g(yVar.a(), "filepath");
        a.c().x().g();
        JSONObject b2 = t.b();
        try {
            boolean a2 = a(g2);
            t.a(b2, "result", a2);
            t.a(b2, "success", true);
            yVar.a(b2).c();
            return a2;
        } catch (Exception e2) {
            t.a(b2, "result", false);
            t.a(b2, "success", false);
            yVar.a(b2).c();
            e2.printStackTrace();
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str, String str2, boolean z) throws IOException {
        BufferedWriter bufferedWriter = z ? new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str), "UTF-8")) : new BufferedWriter(new OutputStreamWriter(new FileOutputStream(str)));
        bufferedWriter.write(str2);
        bufferedWriter.flush();
        bufferedWriter.close();
    }

    /* access modifiers changed from: private */
    public boolean a(y yVar, File file) {
        a.c().x().g();
        JSONObject b2 = t.b();
        if (a(file)) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            return true;
        }
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean a(File file) {
        try {
            if (!file.isDirectory()) {
                return file.delete();
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            String[] list = file.list();
            if (list.length > 0) {
                return a(new File(file, list[0]));
            }
            if (file.list().length == 0) {
                return file.delete();
            }
            return false;
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public StringBuilder a(String str, boolean z) throws IOException {
        BufferedReader bufferedReader;
        File file = new File(str);
        StringBuilder sb = new StringBuilder((int) file.length());
        if (z) {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath()), "UTF-8"));
        } else {
            bufferedReader = new BufferedReader(new InputStreamReader(new FileInputStream(file.getAbsolutePath())));
        }
        while (true) {
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                sb.append(readLine);
                sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            } else {
                bufferedReader.close();
                return sb;
            }
        }
    }

    private boolean a(String str) {
        return new File(str).exists();
    }

    /* access modifiers changed from: private */
    public boolean a(y yVar) {
        String g2 = t.g(yVar.a(), "filepath");
        a.c().x().g();
        JSONObject b2 = t.b();
        try {
            if (new File(g2).mkdir()) {
                t.a(b2, "success", true);
                yVar.a(b2).c();
                return true;
            }
            t.a(b2, "success", false);
            return false;
        } catch (Exception unused) {
            t.a(b2, "success", false);
            yVar.a(b2).c();
            return false;
        }
    }

    /* access modifiers changed from: private */
    public void a(Runnable runnable) {
        if (!this.f1336a.isEmpty() || this.b) {
            this.f1336a.push(runnable);
            return;
        }
        this.b = true;
        runnable.run();
    }
}
