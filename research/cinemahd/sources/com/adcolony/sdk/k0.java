package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.FrameLayout;
import com.facebook.imageutils.JfifUtil;
import com.facebook.react.uimanager.ViewProps;
import java.util.ArrayList;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class k0 extends Button {

    /* renamed from: a  reason: collision with root package name */
    private int f1274a;
    private int b;
    private int c;
    private int d;
    private int e;
    private int f;
    private int g;
    private int h;
    private int i;
    private int j;
    private String k;
    private String l;
    private String m;
    private String n;
    private c o;
    private y p;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.a(yVar);
            }
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.k(yVar);
            }
        }
    }

    class c implements a0 {
        c() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.e(yVar);
            }
        }
    }

    class d implements a0 {
        d() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.f(yVar);
            }
        }
    }

    class e implements a0 {
        e() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.d(yVar);
            }
        }
    }

    class f implements a0 {
        f() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.j(yVar);
            }
        }
    }

    class g implements a0 {
        g() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.g(yVar);
            }
        }
    }

    class h implements a0 {
        h() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.h(yVar);
            }
        }
    }

    class i implements a0 {
        i() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.b(yVar);
            }
        }
    }

    class j implements a0 {
        j() {
        }

        public void a(y yVar) {
            if (k0.this.c(yVar)) {
                k0.this.i(yVar);
            }
        }
    }

    k0(Context context, y yVar, int i2, c cVar) {
        super(context);
        this.f1274a = i2;
        this.p = yVar;
        this.o = cVar;
    }

    /* access modifiers changed from: package-private */
    public int a(boolean z, int i2) {
        if (i2 == 0) {
            return z ? 1 : 16;
        }
        if (i2 == 1) {
            return z ? 8388611 : 48;
        }
        if (i2 != 2) {
            return 17;
        }
        return z ? 8388613 : 80;
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        JSONObject a2 = yVar.a();
        this.i = t.e(a2, "x");
        this.j = t.e(a2, "y");
        setGravity(a(true, this.i) | a(false, this.j));
    }

    /* access modifiers changed from: package-private */
    public void b(y yVar) {
        JSONObject b2 = t.b();
        t.a(b2, "text", getText().toString());
        yVar.a(b2).c();
    }

    /* access modifiers changed from: package-private */
    public boolean c(y yVar) {
        JSONObject a2 = yVar.a();
        return t.e(a2, "id") == this.f1274a && t.e(a2, "container_id") == this.o.c() && t.g(a2, "ad_session_id").equals(this.o.a());
    }

    /* access modifiers changed from: package-private */
    public void d(y yVar) {
        String g2 = t.g(yVar.a(), "background_color");
        this.k = g2;
        setBackgroundColor(l0.f(g2));
    }

    /* access modifiers changed from: package-private */
    public void e(y yVar) {
        JSONObject a2 = yVar.a();
        this.b = t.e(a2, "x");
        this.c = t.e(a2, "y");
        this.d = t.e(a2, "width");
        this.e = t.e(a2, "height");
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.width = this.d;
        layoutParams.height = this.e;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: package-private */
    public void f(y yVar) {
        String g2 = t.g(yVar.a(), "font_color");
        this.l = g2;
        setTextColor(l0.f(g2));
    }

    /* access modifiers changed from: package-private */
    public void g(y yVar) {
        int e2 = t.e(yVar.a(), "font_size");
        this.h = e2;
        setTextSize((float) e2);
    }

    /* access modifiers changed from: package-private */
    public void h(y yVar) {
        int e2 = t.e(yVar.a(), "font_style");
        this.f = e2;
        if (e2 == 0) {
            setTypeface(getTypeface(), 0);
        } else if (e2 == 1) {
            setTypeface(getTypeface(), 1);
        } else if (e2 == 2) {
            setTypeface(getTypeface(), 2);
        } else if (e2 == 3) {
            setTypeface(getTypeface(), 3);
        }
    }

    /* access modifiers changed from: package-private */
    public void i(y yVar) {
        String g2 = t.g(yVar.a(), "text");
        this.m = g2;
        setText(g2);
    }

    /* access modifiers changed from: package-private */
    public void j(y yVar) {
        int e2 = t.e(yVar.a(), "font_family");
        this.g = e2;
        if (e2 == 0) {
            setTypeface(Typeface.DEFAULT);
        } else if (e2 == 1) {
            setTypeface(Typeface.SERIF);
        } else if (e2 == 2) {
            setTypeface(Typeface.SANS_SERIF);
        } else if (e2 == 3) {
            setTypeface(Typeface.MONOSPACE);
        }
    }

    /* access modifiers changed from: package-private */
    public void k(y yVar) {
        if (t.c(yVar.a(), ViewProps.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        i c2 = a.c();
        d e2 = c2.e();
        int action = motionEvent.getAction() & JfifUtil.MARKER_FIRST_BYTE;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        JSONObject b2 = t.b();
        t.b(b2, "view_id", this.f1274a);
        t.a(b2, "ad_session_id", this.n);
        t.b(b2, "container_x", this.b + x);
        t.b(b2, "container_y", this.c + y);
        t.b(b2, "view_x", x);
        t.b(b2, "view_y", y);
        t.b(b2, "id", this.o.getId());
        if (action != 0) {
            int i2 = y;
            if (action == 1) {
                if (!this.o.p()) {
                    c2.a(e2.b().get(this.n));
                }
                if (x <= 0 || x >= getWidth() || i2 <= 0 || i2 >= getHeight()) {
                    new y("AdContainer.on_touch_cancelled", this.o.k(), b2).c();
                    return true;
                }
                new y("AdContainer.on_touch_ended", this.o.k(), b2).c();
                return true;
            } else if (action == 2) {
                new y("AdContainer.on_touch_moved", this.o.k(), b2).c();
                return true;
            } else if (action == 3) {
                new y("AdContainer.on_touch_cancelled", this.o.k(), b2).c();
                return true;
            } else if (action == 5) {
                int action2 = (motionEvent.getAction() & 65280) >> 8;
                t.b(b2, "container_x", ((int) motionEvent2.getX(action2)) + this.b);
                t.b(b2, "container_y", ((int) motionEvent2.getY(action2)) + this.c);
                t.b(b2, "view_x", (int) motionEvent2.getX(action2));
                t.b(b2, "view_y", (int) motionEvent2.getY(action2));
                new y("AdContainer.on_touch_began", this.o.k(), b2).c();
                return true;
            } else if (action != 6) {
                return true;
            } else {
                int action3 = (motionEvent.getAction() & 65280) >> 8;
                int x2 = (int) motionEvent2.getX(action3);
                int y2 = (int) motionEvent2.getY(action3);
                t.b(b2, "container_x", ((int) motionEvent2.getX(action3)) + this.b);
                t.b(b2, "container_y", ((int) motionEvent2.getY(action3)) + this.c);
                t.b(b2, "view_x", (int) motionEvent2.getX(action3));
                t.b(b2, "view_y", (int) motionEvent2.getY(action3));
                if (!this.o.p()) {
                    c2.a(e2.b().get(this.n));
                }
                if (x2 <= 0 || x2 >= getWidth() || y2 <= 0 || y2 >= getHeight()) {
                    new y("AdContainer.on_touch_cancelled", this.o.k(), b2).c();
                    return true;
                }
                new y("AdContainer.on_touch_ended", this.o.k(), b2).c();
                return true;
            }
        } else {
            new y("AdContainer.on_touch_began", this.o.k(), b2).c();
            return true;
        }
    }

    k0(Context context, int i2, y yVar, int i3, c cVar) {
        super(context, (AttributeSet) null, i2);
        this.f1274a = i3;
        this.p = yVar;
        this.o = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int i2;
        int i3;
        JSONObject a2 = this.p.a();
        this.n = t.g(a2, "ad_session_id");
        this.b = t.e(a2, "x");
        this.c = t.e(a2, "y");
        this.d = t.e(a2, "width");
        this.e = t.e(a2, "height");
        this.g = t.e(a2, "font_family");
        this.f = t.e(a2, "font_style");
        this.h = t.e(a2, "font_size");
        this.k = t.g(a2, "background_color");
        this.l = t.g(a2, "font_color");
        this.m = t.g(a2, "text");
        this.i = t.e(a2, "align_x");
        this.j = t.e(a2, "align_y");
        i c2 = a.c();
        if (this.m.equals("")) {
            this.m = "Learn More";
        }
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = t.c(a2, "wrap_content") ? new FrameLayout.LayoutParams(-2, -2) : new FrameLayout.LayoutParams(this.d, this.e);
        layoutParams.gravity = 0;
        setText(this.m);
        setTextSize((float) this.h);
        if (t.c(a2, "overlay")) {
            this.b = 0;
            this.c = 0;
            i3 = (int) (c2.k().x() * 6.0f);
            i2 = (int) (c2.k().x() * 6.0f);
            int x = (int) (c2.k().x() * 4.0f);
            setPadding(x, x, x, x);
            layoutParams.gravity = 8388693;
        } else {
            i3 = 0;
            i2 = 0;
        }
        layoutParams.setMargins(this.b, this.c, i3, i2);
        this.o.addView(this, layoutParams);
        int i4 = this.g;
        if (i4 == 0) {
            setTypeface(Typeface.DEFAULT);
        } else if (i4 == 1) {
            setTypeface(Typeface.SERIF);
        } else if (i4 == 2) {
            setTypeface(Typeface.SANS_SERIF);
        } else if (i4 == 3) {
            setTypeface(Typeface.MONOSPACE);
        }
        int i5 = this.f;
        if (i5 == 0) {
            setTypeface(getTypeface(), 0);
        } else if (i5 == 1) {
            setTypeface(getTypeface(), 1);
        } else if (i5 == 2) {
            setTypeface(getTypeface(), 2);
        } else if (i5 == 3) {
            setTypeface(getTypeface(), 3);
        }
        setGravity(a(true, this.i) | a(false, this.j));
        if (!this.k.equals("")) {
            setBackgroundColor(l0.f(this.k));
        }
        if (!this.l.equals("")) {
            setTextColor(l0.f(this.l));
        }
        ArrayList<a0> i6 = this.o.i();
        b bVar = new b();
        a.a("TextView.set_visible", (a0) bVar, true);
        i6.add(bVar);
        ArrayList<a0> i7 = this.o.i();
        c cVar = new c();
        a.a("TextView.set_bounds", (a0) cVar, true);
        i7.add(cVar);
        ArrayList<a0> i8 = this.o.i();
        d dVar = new d();
        a.a("TextView.set_font_color", (a0) dVar, true);
        i8.add(dVar);
        ArrayList<a0> i9 = this.o.i();
        e eVar = new e();
        a.a("TextView.set_background_color", (a0) eVar, true);
        i9.add(eVar);
        ArrayList<a0> i10 = this.o.i();
        f fVar = new f();
        a.a("TextView.set_typeface", (a0) fVar, true);
        i10.add(fVar);
        ArrayList<a0> i11 = this.o.i();
        g gVar = new g();
        a.a("TextView.set_font_size", (a0) gVar, true);
        i11.add(gVar);
        ArrayList<a0> i12 = this.o.i();
        h hVar = new h();
        a.a("TextView.set_font_style", (a0) hVar, true);
        i12.add(hVar);
        ArrayList<a0> i13 = this.o.i();
        i iVar = new i();
        a.a("TextView.get_text", (a0) iVar, true);
        i13.add(iVar);
        ArrayList<a0> i14 = this.o.i();
        j jVar = new j();
        a.a("TextView.set_text", (a0) jVar, true);
        i14.add(jVar);
        ArrayList<a0> i15 = this.o.i();
        a aVar = new a();
        a.a("TextView.align", (a0) aVar, true);
        i15.add(aVar);
        this.o.j().add("TextView.set_visible");
        this.o.j().add("TextView.set_bounds");
        this.o.j().add("TextView.set_font_color");
        this.o.j().add("TextView.set_background_color");
        this.o.j().add("TextView.set_typeface");
        this.o.j().add("TextView.set_font_size");
        this.o.j().add("TextView.set_font_style");
        this.o.j().add("TextView.get_text");
        this.o.j().add("TextView.set_text");
        this.o.j().add("TextView.align");
    }
}
