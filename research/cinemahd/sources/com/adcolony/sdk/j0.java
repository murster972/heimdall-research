package com.adcolony.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Vibrator;
import com.adcolony.sdk.v;
import com.facebook.common.util.UriUtil;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import com.vungle.warren.model.ReportDBAdapter;
import org.json.JSONArray;
import org.json.JSONObject;

class j0 {

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            j0.this.i(yVar);
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            boolean unused = j0.this.m(yVar);
        }
    }

    class c implements a0 {
        c() {
        }

        public void a(y yVar) {
            boolean unused = j0.this.l(yVar);
        }
    }

    class d implements a0 {
        d() {
        }

        public void a(y yVar) {
            j0.this.b(yVar);
        }
    }

    class e implements a0 {
        e() {
        }

        public void a(y yVar) {
            boolean unused = j0.this.p(yVar);
        }
    }

    class f implements a0 {
        f() {
        }

        public void a(y yVar) {
            boolean unused = j0.this.o(yVar);
        }
    }

    class g implements a0 {
        g() {
        }

        public void a(y yVar) {
            boolean unused = j0.this.n(yVar);
        }
    }

    class h implements MediaScannerConnection.OnScanCompletedListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ JSONObject f1257a;
        final /* synthetic */ y b;

        h(j0 j0Var, JSONObject jSONObject, y yVar) {
            this.f1257a = jSONObject;
            this.b = yVar;
        }

        public void onScanCompleted(String str, Uri uri) {
            l0.a("Screenshot saved to Gallery!", 0);
            t.a(this.f1257a, "success", true);
            this.b.a(this.f1257a).c();
        }
    }

    class i implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1258a;

        i(j0 j0Var, String str) {
            this.f1258a = str;
        }

        public void run() {
            JSONObject b = t.b();
            t.a(b, "type", "open_hook");
            t.a(b, "message", this.f1258a);
            new y("CustomMessage.controller_send", 0, b).c();
        }
    }

    class j implements a0 {
        j() {
        }

        public void a(y yVar) {
            j0.this.f(yVar);
        }
    }

    class k implements a0 {
        k() {
        }

        public void a(y yVar) {
            j0.this.g(yVar);
        }
    }

    class l implements a0 {
        l() {
        }

        public void a(y yVar) {
            j0.this.j(yVar);
        }
    }

    class m implements a0 {
        m() {
        }

        public void a(y yVar) {
            j0.this.h(yVar);
        }
    }

    class n implements a0 {
        n() {
        }

        public void a(y yVar) {
            j0.this.k(yVar);
        }
    }

    class o implements a0 {
        o() {
        }

        public void a(y yVar) {
            j0.this.e(yVar);
        }
    }

    class p implements a0 {
        p() {
        }

        public void a(y yVar) {
            j0.this.d(yVar);
        }
    }

    class q implements a0 {
        q() {
        }

        public void a(y yVar) {
            j0.this.c(yVar);
        }
    }

    class r implements a0 {
        r() {
        }

        public void a(y yVar) {
            j0.this.a(yVar);
        }
    }

    j0() {
    }

    /* access modifiers changed from: private */
    public boolean l(y yVar) {
        String g2 = t.g(yVar.a(), "ad_session_id");
        Activity activity = a.b() instanceof Activity ? (Activity) a.b() : null;
        boolean z = activity instanceof AdColonyAdViewActivity;
        if (!(activity instanceof b)) {
            return false;
        }
        if (z) {
            ((AdColonyAdViewActivity) activity).b();
            return true;
        }
        JSONObject b2 = t.b();
        t.a(b2, "id", g2);
        new y("AdSession.on_request_close", ((b) activity).c, b2).c();
        return true;
    }

    /* access modifiers changed from: private */
    public boolean m(y yVar) {
        JSONObject a2 = yVar.a();
        d e2 = a.c().e();
        String g2 = t.g(a2, "ad_session_id");
        AdColonyInterstitial adColonyInterstitial = e2.a().get(g2);
        AdColonyAdView adColonyAdView = e2.b().get(g2);
        if ((adColonyInterstitial == null || adColonyInterstitial.g() == null || adColonyInterstitial.d() == null) && (adColonyAdView == null || adColonyAdView.getListener() == null)) {
            return false;
        }
        if (adColonyAdView == null) {
            new y("AdUnit.make_in_app_purchase", adColonyInterstitial.d().k()).c();
        }
        a(g2);
        c(g2);
        return true;
    }

    /* access modifiers changed from: private */
    public boolean n(y yVar) {
        JSONObject a2 = yVar.a();
        String g2 = t.g(t.f(a2, "clickOverride"), ReportDBAdapter.ReportColumns.COLUMN_URL);
        String g3 = t.g(a2, "ad_session_id");
        d e2 = a.c().e();
        AdColonyInterstitial adColonyInterstitial = e2.a().get(g3);
        AdColonyAdView adColonyAdView = e2.b().get(g3);
        if (adColonyInterstitial != null) {
            adColonyInterstitial.b(g2);
            return true;
        } else if (adColonyAdView == null) {
            return false;
        } else {
            adColonyAdView.setClickOverride(g2);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean o(y yVar) {
        int i2;
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "ad_session_id");
        int e2 = t.e(a2, AdUnitActivity.EXTRA_ORIENTATION);
        d e3 = a.c().e();
        AdColonyAdView adColonyAdView = e3.b().get(g2);
        AdColonyInterstitial adColonyInterstitial = e3.a().get(g2);
        Context b2 = a.b();
        if (adColonyAdView != null) {
            adColonyAdView.setOrientation(e2);
        } else if (adColonyInterstitial != null) {
            adColonyInterstitial.a(e2);
        }
        if (adColonyInterstitial == null && adColonyAdView == null) {
            new v.a().a("Invalid ad session id sent with set orientation properties message: ").a(g2).a(v.i);
            return false;
        } else if (!(b2 instanceof b)) {
            return true;
        } else {
            b bVar = (b) b2;
            if (adColonyAdView == null) {
                i2 = adColonyInterstitial.f();
            } else {
                i2 = adColonyAdView.getOrientation();
            }
            bVar.a(i2);
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean p(y yVar) {
        AdColonyAdView adColonyAdView = a.c().e().b().get(t.g(yVar.a(), "ad_session_id"));
        if (adColonyAdView == null) {
            return false;
        }
        adColonyAdView.setNoCloseButton(t.c(yVar.a(), "use_custom_close"));
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean f(y yVar) {
        JSONObject b2 = t.b();
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "product_id");
        String g3 = t.g(a2, "ad_session_id");
        if (g2.equals("")) {
            g2 = t.g(a2, "handle");
        }
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(g2));
        d(g2);
        if (l0.a(intent)) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g3);
            a(g3);
            c(g3);
            return true;
        }
        l0.a("Unable to open.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Can't wrap try/catch for region: R(6:8|9|10|11|12|13) */
    /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
        com.adcolony.sdk.l0.a("Error saving screenshot.", 0);
        com.adcolony.sdk.t.a(r4, "success", false);
        r13.a(r4).c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x00e1, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x00e2, code lost:
        com.adcolony.sdk.l0.a("Error saving screenshot.", 0);
        com.adcolony.sdk.t.a(r4, "success", false);
        r13.a(r4).c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x00ef, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x00af */
    /* JADX WARNING: Missing exception handler attribute for start block: B:14:0x00d4 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean g(com.adcolony.sdk.y r13) {
        /*
            r12 = this;
            java.lang.String r0 = "success"
            java.lang.String r1 = "Error saving screenshot."
            android.content.Context r2 = com.adcolony.sdk.a.b()
            r3 = 0
            if (r2 == 0) goto L_0x0113
            boolean r4 = r2 instanceof android.app.Activity
            if (r4 != 0) goto L_0x0011
            goto L_0x0113
        L_0x0011:
            java.lang.String r4 = "android.permission.WRITE_EXTERNAL_STORAGE"
            int r4 = androidx.core.content.ContextCompat.a((android.content.Context) r2, (java.lang.String) r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            if (r4 != 0) goto L_0x00f0
            org.json.JSONObject r4 = r13.a()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r5 = "ad_session_id"
            java.lang.String r4 = com.adcolony.sdk.t.g(r4, r5)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r12.a((java.lang.String) r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            org.json.JSONObject r4 = com.adcolony.sdk.t.b()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r5.<init>()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.io.File r6 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r6 = r6.toString()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r6 = "/Pictures/AdColony_Screenshots/AdColony_Screenshot_"
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            long r6 = java.lang.System.currentTimeMillis()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r6 = ".jpg"
            r5.append(r6)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.lang.String r5 = r5.toString()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r6 = r2
            android.app.Activity r6 = (android.app.Activity) r6     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.view.Window r6 = r6.getWindow()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.view.View r6 = r6.getDecorView()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.view.View r6 = r6.getRootView()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r7 = 1
            r6.setDrawingCacheEnabled(r7)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.graphics.Bitmap r8 = r6.getDrawingCache()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            android.graphics.Bitmap r8 = android.graphics.Bitmap.createBitmap(r8)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r6.setDrawingCacheEnabled(r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x00af }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r9.<init>()     // Catch:{ Exception -> 0x00af }
            java.io.File r10 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00af }
            java.lang.String r10 = r10.getPath()     // Catch:{ Exception -> 0x00af }
            r9.append(r10)     // Catch:{ Exception -> 0x00af }
            java.lang.String r10 = "/Pictures"
            r9.append(r10)     // Catch:{ Exception -> 0x00af }
            java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x00af }
            r6.<init>(r9)     // Catch:{ Exception -> 0x00af }
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x00af }
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00af }
            r10.<init>()     // Catch:{ Exception -> 0x00af }
            java.io.File r11 = android.os.Environment.getExternalStorageDirectory()     // Catch:{ Exception -> 0x00af }
            java.lang.String r11 = r11.getPath()     // Catch:{ Exception -> 0x00af }
            r10.append(r11)     // Catch:{ Exception -> 0x00af }
            java.lang.String r11 = "/Pictures/AdColony_Screenshots"
            r10.append(r11)     // Catch:{ Exception -> 0x00af }
            java.lang.String r10 = r10.toString()     // Catch:{ Exception -> 0x00af }
            r9.<init>(r10)     // Catch:{ Exception -> 0x00af }
            r6.mkdirs()     // Catch:{ Exception -> 0x00af }
            r9.mkdirs()     // Catch:{ Exception -> 0x00af }
        L_0x00af:
            java.io.File r6 = new java.io.File     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r6.<init>(r5)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            java.io.FileOutputStream r9 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r9.<init>(r6)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            android.graphics.Bitmap$CompressFormat r6 = android.graphics.Bitmap.CompressFormat.JPEG     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r10 = 90
            r8.compress(r6, r10, r9)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r9.flush()     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r9.close()     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            java.lang.String[] r6 = new java.lang.String[r7]     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r6[r3] = r5     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r5 = 0
            com.adcolony.sdk.j0$h r8 = new com.adcolony.sdk.j0$h     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            r8.<init>(r12, r4, r13)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            android.media.MediaScannerConnection.scanFile(r2, r6, r5, r8)     // Catch:{ FileNotFoundException -> 0x00e2, IOException -> 0x00d4 }
            return r7
        L_0x00d4:
            com.adcolony.sdk.l0.a((java.lang.String) r1, (int) r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.t.a((org.json.JSONObject) r4, (java.lang.String) r0, (boolean) r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.y r2 = r13.a(r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r2.c()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            return r3
        L_0x00e2:
            com.adcolony.sdk.l0.a((java.lang.String) r1, (int) r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.t.a((org.json.JSONObject) r4, (java.lang.String) r0, (boolean) r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.y r2 = r13.a(r4)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r2.c()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            return r3
        L_0x00f0:
            com.adcolony.sdk.l0.a((java.lang.String) r1, (int) r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            org.json.JSONObject r2 = r13.a()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.t.a((org.json.JSONObject) r2, (java.lang.String) r0, (boolean) r3)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            com.adcolony.sdk.y r2 = r13.a(r2)     // Catch:{ NoClassDefFoundError -> 0x0102 }
            r2.c()     // Catch:{ NoClassDefFoundError -> 0x0102 }
            return r3
        L_0x0102:
            com.adcolony.sdk.l0.a((java.lang.String) r1, (int) r3)
            org.json.JSONObject r1 = r13.a()
            com.adcolony.sdk.t.a((org.json.JSONObject) r1, (java.lang.String) r0, (boolean) r3)
            com.adcolony.sdk.y r13 = r13.a(r1)
            r13.c()
        L_0x0113:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.j0.g(com.adcolony.sdk.y):boolean");
    }

    /* access modifiers changed from: package-private */
    public boolean h(y yVar) {
        JSONObject a2 = yVar.a();
        JSONObject b2 = t.b();
        String g2 = t.g(a2, "ad_session_id");
        JSONArray b3 = t.b(a2, "recipients");
        String str = "";
        for (int i2 = 0; i2 < b3.length(); i2++) {
            if (i2 != 0) {
                str = str + ";";
            }
            str = str + t.b(b3, i2);
        }
        if (l0.a(new Intent("android.intent.action.VIEW", Uri.parse("smsto:" + str)).putExtra("sms_body", t.g(a2, "body")))) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g2);
            a(g2);
            c(g2);
            return true;
        }
        l0.a("Failed to create sms.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean i(y yVar) {
        JSONObject b2 = t.b();
        JSONObject a2 = yVar.a();
        Intent type = new Intent("android.intent.action.SEND").setType("text/plain");
        Intent putExtra = type.putExtra("android.intent.extra.TEXT", t.g(a2, "text") + " " + t.g(a2, ReportDBAdapter.ReportColumns.COLUMN_URL));
        String g2 = t.g(a2, "ad_session_id");
        if (l0.a(putExtra, true)) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g2);
            a(g2);
            c(g2);
            return true;
        }
        l0.a("Unable to create social post.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean j(y yVar) {
        JSONObject b2 = t.b();
        JSONObject a2 = yVar.a();
        Intent intent = new Intent("android.intent.action.DIAL");
        Intent data = intent.setData(Uri.parse("tel:" + t.g(a2, "phone_number")));
        String g2 = t.g(a2, "ad_session_id");
        if (l0.a(data)) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g2);
            a(g2);
            c(g2);
            return true;
        }
        l0.a("Failed to dial number.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean k(y yVar) {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        int a2 = t.a(yVar.a(), "length_ms", 500);
        JSONObject b3 = t.b();
        JSONArray d2 = l0.d(b2);
        boolean z = false;
        for (int i2 = 0; i2 < d2.length(); i2++) {
            if (t.b(d2, i2).equals("android.permission.VIBRATE")) {
                z = true;
            }
        }
        if (!z) {
            new v.a().a("No vibrate permission detected.").a(v.f);
            t.a(b3, "success", false);
            yVar.a(b3).c();
            return false;
        }
        try {
            Vibrator vibrator = (Vibrator) b2.getSystemService("vibrator");
            if (vibrator != null) {
                vibrator.vibrate((long) a2);
                t.a(b3, "success", true);
                yVar.a(b3).c();
                return true;
            }
        } catch (Exception unused) {
            new v.a().a("Vibrate command failed.").a(v.f);
        }
        t.a(b3, "success", false);
        yVar.a(b3).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        a.a("System.open_store", (a0) new j());
        a.a("System.save_screenshot", (a0) new k());
        a.a("System.telephone", (a0) new l());
        a.a("System.sms", (a0) new m());
        a.a("System.vibrate", (a0) new n());
        a.a("System.open_browser", (a0) new o());
        a.a("System.mail", (a0) new p());
        a.a("System.launch_app", (a0) new q());
        a.a("System.create_calendar_event", (a0) new r());
        a.a("System.social_post", (a0) new a());
        a.a("System.make_in_app_purchase", (a0) new b());
        a.a("System.close", (a0) new c());
        a.a("System.expand", (a0) new d());
        a.a("System.use_custom_close", (a0) new e());
        a.a("System.set_orientation_properties", (a0) new f());
        a.a("System.click_override", (a0) new g());
    }

    /* access modifiers changed from: package-private */
    public boolean b(y yVar) {
        JSONObject a2 = yVar.a();
        Context b2 = a.b();
        if (b2 != null && a.e()) {
            String g2 = t.g(a2, "ad_session_id");
            i c2 = a.c();
            AdColonyAdView adColonyAdView = c2.e().b().get(g2);
            if (adColonyAdView != null && ((adColonyAdView.getTrustedDemandSource() || adColonyAdView.getUserInteraction()) && c2.h() != adColonyAdView)) {
                adColonyAdView.setExpandMessage(yVar);
                adColonyAdView.setExpandedWidth(t.e(a2, "width"));
                adColonyAdView.setExpandedHeight(t.e(a2, "height"));
                adColonyAdView.setOrientation(t.a(a2, AdUnitActivity.EXTRA_ORIENTATION, -1));
                adColonyAdView.setNoCloseButton(t.c(a2, "use_custom_close"));
                c2.a(adColonyAdView);
                c2.a(adColonyAdView.getContainer());
                Intent intent = new Intent(b2, AdColonyAdViewActivity.class);
                c(g2);
                a(g2);
                l0.a(intent);
                return true;
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c(y yVar) {
        JSONObject b2 = t.b();
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, "ad_session_id");
        if (t.c(a2, "deep_link")) {
            return f(yVar);
        }
        Context b3 = a.b();
        if (b3 == null) {
            return false;
        }
        if (l0.a(b3.getPackageManager().getLaunchIntentForPackage(t.g(a2, "handle")))) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g2);
            a(g2);
            c(g2);
            return true;
        }
        l0.a("Failed to launch external application.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean d(y yVar) {
        JSONObject b2 = t.b();
        JSONObject a2 = yVar.a();
        JSONArray b3 = t.b(a2, "recipients");
        boolean c2 = t.c(a2, "html");
        String g2 = t.g(a2, "subject");
        String g3 = t.g(a2, "body");
        String g4 = t.g(a2, "ad_session_id");
        String[] strArr = new String[b3.length()];
        for (int i2 = 0; i2 < b3.length(); i2++) {
            strArr[i2] = t.b(b3, i2);
        }
        Intent intent = new Intent("android.intent.action.SEND");
        if (!c2) {
            intent.setType("plain/text");
        }
        intent.putExtra("android.intent.extra.SUBJECT", g2).putExtra("android.intent.extra.TEXT", g3).putExtra("android.intent.extra.EMAIL", strArr);
        if (l0.a(intent)) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g4);
            a(g4);
            c(g4);
            return true;
        }
        l0.a("Failed to send email.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean e(y yVar) {
        JSONObject b2 = t.b();
        JSONObject a2 = yVar.a();
        String g2 = t.g(a2, ReportDBAdapter.ReportColumns.COLUMN_URL);
        String g3 = t.g(a2, "ad_session_id");
        AdColonyAdView adColonyAdView = a.c().e().b().get(g3);
        if (adColonyAdView != null && !adColonyAdView.getTrustedDemandSource() && !adColonyAdView.getUserInteraction()) {
            return false;
        }
        if (g2.startsWith("browser")) {
            g2 = g2.replaceFirst("browser", UriUtil.HTTP_SCHEME);
        }
        if (g2.startsWith("safari")) {
            g2 = g2.replaceFirst("safari", UriUtil.HTTP_SCHEME);
        }
        d(g2);
        if (l0.a(new Intent("android.intent.action.VIEW", Uri.parse(g2)))) {
            t.a(b2, "success", true);
            yVar.a(b2).c();
            b(g3);
            a(g3);
            c(g3);
            return true;
        }
        l0.a("Failed to launch browser.", 0);
        t.a(b2, "success", false);
        yVar.a(b2).c();
        return false;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x010f  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01ce  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x01ed  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.adcolony.sdk.y r25) {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            org.json.JSONObject r2 = com.adcolony.sdk.t.b()
            org.json.JSONObject r3 = r25.a()
            java.lang.String r4 = "ad_session_id"
            java.lang.String r4 = com.adcolony.sdk.t.g(r3, r4)
            java.lang.String r5 = "params"
            org.json.JSONObject r3 = com.adcolony.sdk.t.f(r3, r5)
            java.lang.String r5 = "recurrence"
            org.json.JSONObject r5 = com.adcolony.sdk.t.f(r3, r5)
            org.json.JSONArray r6 = com.adcolony.sdk.t.a()
            org.json.JSONArray r7 = com.adcolony.sdk.t.a()
            org.json.JSONArray r8 = com.adcolony.sdk.t.a()
            java.lang.String r9 = "description"
            java.lang.String r10 = com.adcolony.sdk.t.g(r3, r9)
            java.lang.String r11 = "location"
            com.adcolony.sdk.t.g(r3, r11)
            java.lang.String r11 = "start"
            java.lang.String r11 = com.adcolony.sdk.t.g(r3, r11)
            java.lang.String r12 = "end"
            java.lang.String r12 = com.adcolony.sdk.t.g(r3, r12)
            java.lang.String r13 = "summary"
            java.lang.String r3 = com.adcolony.sdk.t.g(r3, r13)
            java.lang.String r13 = ""
            if (r5 == 0) goto L_0x0078
            int r14 = r5.length()
            if (r14 <= 0) goto L_0x0078
            java.lang.String r6 = "expires"
            java.lang.String r6 = com.adcolony.sdk.t.g(r5, r6)
            java.lang.String r7 = "frequency"
            java.lang.String r7 = com.adcolony.sdk.t.g(r5, r7)
            java.util.Locale r8 = java.util.Locale.getDefault()
            java.lang.String r7 = r7.toUpperCase(r8)
            java.lang.String r8 = "daysInWeek"
            org.json.JSONArray r8 = com.adcolony.sdk.t.b((org.json.JSONObject) r5, (java.lang.String) r8)
            java.lang.String r14 = "daysInMonth"
            org.json.JSONArray r14 = com.adcolony.sdk.t.b((org.json.JSONObject) r5, (java.lang.String) r14)
            java.lang.String r15 = "daysInYear"
            org.json.JSONArray r15 = com.adcolony.sdk.t.b((org.json.JSONObject) r5, (java.lang.String) r15)
            goto L_0x007d
        L_0x0078:
            r14 = r7
            r15 = r8
            r7 = r13
            r8 = r6
            r6 = r7
        L_0x007d:
            boolean r13 = r3.equals(r13)
            if (r13 == 0) goto L_0x0084
            r3 = r10
        L_0x0084:
            java.util.Date r11 = com.adcolony.sdk.l0.g(r11)
            java.util.Date r12 = com.adcolony.sdk.l0.g(r12)
            java.util.Date r6 = com.adcolony.sdk.l0.g(r6)
            java.lang.String r13 = "success"
            if (r11 == 0) goto L_0x0206
            if (r12 != 0) goto L_0x0098
            goto L_0x0206
        L_0x0098:
            long r0 = r11.getTime()
            r16 = r13
            long r12 = r12.getTime()
            r17 = 0
            if (r6 == 0) goto L_0x00b5
            long r19 = r6.getTime()
            long r21 = r11.getTime()
            long r19 = r19 - r21
            r21 = 1000(0x3e8, double:4.94E-321)
            long r19 = r19 / r21
            goto L_0x00b7
        L_0x00b5:
            r19 = r17
        L_0x00b7:
            java.lang.String r6 = "DAILY"
            boolean r6 = r7.equals(r6)
            r21 = 1
            if (r6 == 0) goto L_0x00cd
            r17 = 86400(0x15180, double:4.26873E-319)
            long r19 = r19 / r17
        L_0x00c6:
            long r17 = r19 + r21
        L_0x00c8:
            r19 = r12
            r11 = r17
            goto L_0x00f7
        L_0x00cd:
            java.lang.String r6 = "WEEKLY"
            boolean r6 = r7.equals(r6)
            if (r6 == 0) goto L_0x00db
            r17 = 604800(0x93a80, double:2.98811E-318)
            long r19 = r19 / r17
            goto L_0x00c6
        L_0x00db:
            java.lang.String r6 = "MONTHLY"
            boolean r6 = r7.equals(r6)
            if (r6 == 0) goto L_0x00e9
            r17 = 2629800(0x2820a8, double:1.299294E-317)
            long r19 = r19 / r17
            goto L_0x00c6
        L_0x00e9:
            java.lang.String r6 = "YEARLY"
            boolean r6 = r7.equals(r6)
            if (r6 == 0) goto L_0x00c8
            r17 = 31557600(0x1e187e0, double:1.5591526E-316)
            long r19 = r19 / r17
            goto L_0x00c6
        L_0x00f7:
            java.lang.String r6 = "endTime"
            java.lang.String r13 = "beginTime"
            r17 = r4
            java.lang.String r4 = "title"
            r18 = r2
            java.lang.String r2 = "vnd.android.cursor.item/event"
            r21 = r6
            java.lang.String r6 = "android.intent.action.EDIT"
            if (r5 == 0) goto L_0x01aa
            int r5 = r5.length()
            if (r5 <= 0) goto L_0x01aa
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r22 = r0
            java.lang.String r0 = "FREQ="
            r5.append(r0)
            r5.append(r7)
            java.lang.String r0 = ";COUNT="
            r5.append(r0)
            r5.append(r11)
            java.lang.String r0 = r5.toString()
            int r1 = r8.length()     // Catch:{ JSONException -> 0x0184 }
            if (r1 == 0) goto L_0x0148
            java.lang.String r1 = com.adcolony.sdk.l0.a((org.json.JSONArray) r8)     // Catch:{ JSONException -> 0x0184 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0184 }
            r5.<init>()     // Catch:{ JSONException -> 0x0184 }
            r5.append(r0)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r7 = ";BYDAY="
            r5.append(r7)     // Catch:{ JSONException -> 0x0184 }
            r5.append(r1)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r0 = r5.toString()     // Catch:{ JSONException -> 0x0184 }
        L_0x0148:
            int r1 = r14.length()     // Catch:{ JSONException -> 0x0184 }
            if (r1 == 0) goto L_0x0166
            java.lang.String r1 = com.adcolony.sdk.l0.b((org.json.JSONArray) r14)     // Catch:{ JSONException -> 0x0184 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0184 }
            r5.<init>()     // Catch:{ JSONException -> 0x0184 }
            r5.append(r0)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r7 = ";BYMONTHDAY="
            r5.append(r7)     // Catch:{ JSONException -> 0x0184 }
            r5.append(r1)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r0 = r5.toString()     // Catch:{ JSONException -> 0x0184 }
        L_0x0166:
            int r1 = r15.length()     // Catch:{ JSONException -> 0x0184 }
            if (r1 == 0) goto L_0x0184
            java.lang.String r1 = com.adcolony.sdk.l0.b((org.json.JSONArray) r15)     // Catch:{ JSONException -> 0x0184 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0184 }
            r5.<init>()     // Catch:{ JSONException -> 0x0184 }
            r5.append(r0)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r7 = ";BYYEARDAY="
            r5.append(r7)     // Catch:{ JSONException -> 0x0184 }
            r5.append(r1)     // Catch:{ JSONException -> 0x0184 }
            java.lang.String r0 = r5.toString()     // Catch:{ JSONException -> 0x0184 }
        L_0x0184:
            android.content.Intent r1 = new android.content.Intent
            r1.<init>(r6)
            android.content.Intent r1 = r1.setType(r2)
            android.content.Intent r1 = r1.putExtra(r4, r3)
            android.content.Intent r1 = r1.putExtra(r9, r10)
            r7 = r22
            android.content.Intent r1 = r1.putExtra(r13, r7)
            r11 = r19
            r5 = r21
            android.content.Intent r1 = r1.putExtra(r5, r11)
            java.lang.String r2 = "rrule"
            android.content.Intent r0 = r1.putExtra(r2, r0)
            goto L_0x01c8
        L_0x01aa:
            r7 = r0
            r11 = r19
            r5 = r21
            android.content.Intent r0 = new android.content.Intent
            r0.<init>(r6)
            android.content.Intent r0 = r0.setType(r2)
            android.content.Intent r0 = r0.putExtra(r4, r3)
            android.content.Intent r0 = r0.putExtra(r9, r10)
            android.content.Intent r0 = r0.putExtra(r13, r7)
            android.content.Intent r0 = r0.putExtra(r5, r11)
        L_0x01c8:
            boolean r0 = com.adcolony.sdk.l0.a((android.content.Intent) r0)
            if (r0 == 0) goto L_0x01ed
            r0 = 1
            r2 = r16
            r1 = r18
            com.adcolony.sdk.t.a((org.json.JSONObject) r1, (java.lang.String) r2, (boolean) r0)
            r3 = r25
            com.adcolony.sdk.y r1 = r3.a(r1)
            r1.c()
            r4 = r24
            r1 = r17
            r4.b((java.lang.String) r1)
            r4.a((java.lang.String) r1)
            r4.c((java.lang.String) r1)
            return r0
        L_0x01ed:
            r0 = 0
            r4 = r24
            r3 = r25
            r2 = r16
            r1 = r18
            java.lang.String r5 = "Unable to create Calendar Event."
            com.adcolony.sdk.l0.a((java.lang.String) r5, (int) r0)
            com.adcolony.sdk.t.a((org.json.JSONObject) r1, (java.lang.String) r2, (boolean) r0)
            com.adcolony.sdk.y r1 = r3.a(r1)
            r1.c()
            return r0
        L_0x0206:
            r0 = 0
            r4 = r24
            r3 = r1
            r1 = r2
            r2 = r13
            java.lang.String r5 = "Unable to create Calendar Event"
            com.adcolony.sdk.l0.a((java.lang.String) r5, (int) r0)
            com.adcolony.sdk.t.a((org.json.JSONObject) r1, (java.lang.String) r2, (boolean) r0)
            com.adcolony.sdk.y r1 = r3.a(r1)
            r1.c()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.adcolony.sdk.j0.a(com.adcolony.sdk.y):boolean");
    }

    private boolean c(String str) {
        if (a.c().e().b().get(str) == null) {
            return false;
        }
        JSONObject b2 = t.b();
        t.a(b2, "ad_session_id", str);
        new y("MRAID.on_event", 1, b2).c();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        d e2 = a.c().e();
        AdColonyInterstitial adColonyInterstitial = e2.a().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.g() == null) {
            AdColonyAdView adColonyAdView = e2.b().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (adColonyAdView != null && listener != null) {
                listener.c(adColonyAdView);
                return;
            }
            return;
        }
        adColonyInterstitial.g().f(adColonyInterstitial);
    }

    private void d(String str) {
        l0.f1291a.execute(new i(this, str));
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        d e2 = a.c().e();
        AdColonyInterstitial adColonyInterstitial = e2.a().get(str);
        if (adColonyInterstitial == null || adColonyInterstitial.g() == null) {
            AdColonyAdView adColonyAdView = e2.b().get(str);
            AdColonyAdViewListener listener = adColonyAdView != null ? adColonyAdView.getListener() : null;
            if (adColonyAdView != null && listener != null) {
                listener.a(adColonyAdView);
                return;
            }
            return;
        }
        adColonyInterstitial.g().c(adColonyInterstitial);
    }
}
