package com.adcolony.sdk;

import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.iab.omid.library.adcolony.adsession.FriendlyObstructionPurpose;
import java.io.File;
import org.json.JSONObject;

public class AdColonyAdView extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private c f1162a = a.c().e().d().get(this.d);
    private AdColonyAdViewListener b;
    private AdColonyAdSize c;
    private String d;
    private String e;
    private String f;
    private String g;
    private ImageView h;
    private d0 i;
    private y j;
    private boolean k;
    private boolean l;
    private boolean m;
    private boolean n;
    private boolean o;
    private int p;
    private int q;
    private int r;
    private int s;
    private int t;

    class b implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Context f1163a;

        b(AdColonyAdView adColonyAdView, Context context) {
            this.f1163a = context;
        }

        public void onClick(View view) {
            Context context = this.f1163a;
            if (context instanceof AdColonyAdViewActivity) {
                ((AdColonyAdViewActivity) context).b();
            }
        }
    }

    AdColonyAdView(Context context, y yVar, AdColonyAdViewListener adColonyAdViewListener) {
        super(context);
        this.b = adColonyAdViewListener;
        this.e = adColonyAdViewListener.c();
        JSONObject a2 = yVar.a();
        this.d = t.g(a2, "id");
        this.f = t.g(a2, "close_button_filepath");
        this.k = t.c(a2, "trusted_demand_source");
        this.o = t.c(a2, "close_button_snap_to_webview");
        this.s = t.e(a2, "close_button_width");
        this.t = t.e(a2, "close_button_height");
        this.c = adColonyAdViewListener.a();
        setLayoutParams(new FrameLayout.LayoutParams(this.f1162a.d(), this.f1162a.b()));
        setBackgroundColor(0);
        addView(this.f1162a);
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.k || this.n) {
            float x = a.c().k().x();
            this.f1162a.setLayoutParams(new FrameLayout.LayoutParams((int) (((float) this.c.b()) * x), (int) (((float) this.c.a()) * x)));
            n0 webView = getWebView();
            if (webView != null) {
                y yVar = new y("WebView.set_bounds", 0);
                JSONObject b2 = t.b();
                t.b(b2, "x", webView.s());
                t.b(b2, "y", webView.t());
                t.b(b2, "width", webView.r());
                t.b(b2, "height", webView.q());
                yVar.b(b2);
                webView.a(yVar);
                JSONObject b3 = t.b();
                t.a(b3, "ad_session_id", this.d);
                new y("MRAID.on_close", this.f1162a.k(), b3).c();
            }
            ImageView imageView = this.h;
            if (imageView != null) {
                this.f1162a.removeView(imageView);
                this.f1162a.a((View) this.h);
            }
            addView(this.f1162a);
            AdColonyAdViewListener adColonyAdViewListener = this.b;
            if (adColonyAdViewListener != null) {
                adColonyAdViewListener.b(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        if (this.k || this.n) {
            k k2 = a.c().k();
            int B = k2.B();
            int A = k2.A();
            int i2 = this.q;
            if (i2 <= 0) {
                i2 = B;
            }
            int i3 = this.r;
            if (i3 <= 0) {
                i3 = A;
            }
            int i4 = (B - i2) / 2;
            int i5 = (A - i3) / 2;
            this.f1162a.setLayoutParams(new FrameLayout.LayoutParams(B, A));
            n0 webView = getWebView();
            if (webView != null) {
                y yVar = new y("WebView.set_bounds", 0);
                JSONObject b2 = t.b();
                t.b(b2, "x", i4);
                t.b(b2, "y", i5);
                t.b(b2, "width", i2);
                t.b(b2, "height", i3);
                yVar.b(b2);
                webView.a(yVar);
                float x = k2.x();
                JSONObject b3 = t.b();
                t.b(b3, "app_orientation", l0.d(l0.e()));
                t.b(b3, "width", (int) (((float) i2) / x));
                t.b(b3, "height", (int) (((float) i3) / x));
                t.b(b3, "x", l0.a((View) webView));
                t.b(b3, "y", l0.b((View) webView));
                t.a(b3, "ad_session_id", this.d);
                new y("MRAID.on_size_change", this.f1162a.k(), b3).c();
            }
            ImageView imageView = this.h;
            if (imageView != null) {
                this.f1162a.removeView(imageView);
            }
            Context b4 = a.b();
            if (!(b4 == null || this.m || webView == null)) {
                float x2 = a.c().k().x();
                int i6 = (int) (((float) this.s) * x2);
                int i7 = (int) (((float) this.t) * x2);
                if (this.o) {
                    B = webView.o() + webView.n();
                }
                int p2 = this.o ? webView.p() : 0;
                ImageView imageView2 = new ImageView(b4.getApplicationContext());
                this.h = imageView2;
                imageView2.setImageURI(Uri.fromFile(new File(this.f)));
                FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(i6, i7);
                layoutParams.setMargins(B - i6, p2, 0, 0);
                this.h.setOnClickListener(new b(this, b4));
                this.f1162a.addView(this.h, layoutParams);
                this.f1162a.a((View) this.h, FriendlyObstructionPurpose.CLOSE_AD);
            }
            if (this.j != null) {
                JSONObject b5 = t.b();
                t.a(b5, "success", true);
                this.j.a(b5).c();
                this.j = null;
            }
            return true;
        }
        if (this.j != null) {
            JSONObject b6 = t.b();
            t.a(b6, "success", false);
            this.j.a(b6).c();
            this.j = null;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        if (this.i != null) {
            getWebView().j();
        }
    }

    public AdColonyAdSize getAdSize() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String getClickOverride() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public c getContainer() {
        return this.f1162a;
    }

    public AdColonyAdViewListener getListener() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public d0 getOmidManager() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public int getOrientation() {
        return this.p;
    }

    /* access modifiers changed from: package-private */
    public boolean getTrustedDemandSource() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public boolean getUserInteraction() {
        return this.n;
    }

    /* access modifiers changed from: package-private */
    public n0 getWebView() {
        c cVar = this.f1162a;
        if (cVar == null) {
            return null;
        }
        return cVar.n().get(2);
    }

    public String getZoneId() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void setClickOverride(String str) {
        this.g = str;
    }

    /* access modifiers changed from: package-private */
    public void setExpandMessage(y yVar) {
        this.j = yVar;
    }

    /* access modifiers changed from: package-private */
    public void setExpandedHeight(int i2) {
        this.r = (int) (((float) i2) * a.c().k().x());
    }

    /* access modifiers changed from: package-private */
    public void setExpandedWidth(int i2) {
        this.q = (int) (((float) i2) * a.c().k().x());
    }

    public void setListener(AdColonyAdViewListener adColonyAdViewListener) {
        this.b = adColonyAdViewListener;
    }

    /* access modifiers changed from: package-private */
    public void setNoCloseButton(boolean z) {
        this.m = this.k && z;
    }

    /* access modifiers changed from: package-private */
    public void setOmidManager(d0 d0Var) {
        this.i = d0Var;
    }

    /* access modifiers changed from: package-private */
    public void setOrientation(int i2) {
        this.p = i2;
    }

    /* access modifiers changed from: package-private */
    public void setUserInteraction(boolean z) {
        this.n = z;
    }
}
