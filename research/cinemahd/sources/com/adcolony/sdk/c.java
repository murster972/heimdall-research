package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.adcolony.sdk.v;
import com.facebook.imageutils.JfifUtil;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.FriendlyObstructionPurpose;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

class c extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<Integer, m0> f1174a;
    private HashMap<Integer, k0> b;
    private HashMap<Integer, n0> c;
    private HashMap<Integer, o> d;
    private HashMap<Integer, r> e;
    private HashMap<Integer, Boolean> f;
    private HashMap<Integer, View> g;
    private int h;
    private int i;
    private int j;
    private int k;
    /* access modifiers changed from: private */
    public String l;
    boolean m;
    boolean n;
    /* access modifiers changed from: private */
    public float o = 0.0f;
    /* access modifiers changed from: private */
    public double p = 0.0d;
    /* access modifiers changed from: private */
    public int q = 0;
    /* access modifiers changed from: private */
    public int r = 0;
    private ArrayList<a0> s;
    private ArrayList<String> t;
    private boolean u;
    private boolean v;
    private boolean w;
    private AdSession x;
    Context y;
    VideoView z;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ Runnable f1175a;

        a(Runnable runnable) {
            this.f1175a = runnable;
        }

        public void run() {
            while (!c.this.m) {
                l0.a(this.f1175a);
                try {
                    Thread.sleep(200);
                } catch (InterruptedException unused) {
                }
            }
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                c cVar = c.this;
                cVar.a((View) cVar.c(yVar), FriendlyObstructionPurpose.OTHER);
            }
        }
    }

    /* renamed from: com.adcolony.sdk.c$c  reason: collision with other inner class name */
    class C0002c implements a0 {
        C0002c() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                c.this.g(yVar);
            }
        }
    }

    class d implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1179a;

            a(y yVar) {
                this.f1179a = yVar;
            }

            public void run() {
                c cVar = c.this;
                cVar.a((View) cVar.d(this.f1179a), FriendlyObstructionPurpose.OTHER);
            }
        }

        d() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                l0.a((Runnable) new a(yVar));
            }
        }
    }

    class e implements a0 {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1181a;

            a(y yVar) {
                this.f1181a = yVar;
            }

            public void run() {
                c.this.h(this.f1181a);
            }
        }

        e() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                l0.a((Runnable) new a(yVar));
            }
        }
    }

    class f implements a0 {
        f() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                c cVar = c.this;
                cVar.a(cVar.b(yVar), FriendlyObstructionPurpose.OTHER);
            }
        }
    }

    class g implements a0 {
        g() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                c.this.f(yVar);
            }
        }
    }

    class h implements a0 {
        h() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                c cVar = c.this;
                cVar.a((View) cVar.a(yVar), FriendlyObstructionPurpose.OTHER);
            }
        }
    }

    class i implements a0 {
        i() {
        }

        public void a(y yVar) {
            if (c.this.i(yVar)) {
                c.this.e(yVar);
            }
        }
    }

    class j implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ boolean f1186a;

        j(boolean z) {
            this.f1186a = z;
        }

        public void run() {
            n0 n0Var;
            double d;
            View view = (View) c.this.getParent();
            AdColonyAdView adColonyAdView = a.c().e().b().get(c.this.l);
            if (adColonyAdView == null) {
                n0Var = null;
            } else {
                n0Var = adColonyAdView.getWebView();
            }
            n0 n0Var2 = n0Var;
            Context b2 = a.b();
            boolean z = true;
            float a2 = p0.a(view, b2, true, this.f1186a, true, adColonyAdView != null);
            if (b2 == null) {
                d = 0.0d;
            } else {
                d = l0.a(l0.a(b2));
            }
            int a3 = l0.a((View) n0Var2);
            int b3 = l0.b((View) n0Var2);
            if (a3 == c.this.q && b3 == c.this.r) {
                z = false;
            }
            if (z) {
                int unused = c.this.q = a3;
                int unused2 = c.this.r = b3;
                c.this.a(a3, b3, n0Var2);
            }
            if (!(c.this.o == a2 && c.this.p == d && !z)) {
                c.this.a(a2, d);
            }
            float unused3 = c.this.o = a2;
            double unused4 = c.this.p = d;
        }
    }

    c(Context context, String str) {
        super(context);
        this.y = context;
        this.l = str;
        setBackgroundColor(-16777216);
    }

    /* access modifiers changed from: package-private */
    public boolean f(y yVar) {
        TextView textView;
        int e2 = t.e(yVar.a(), "id");
        View remove = this.g.remove(Integer.valueOf(e2));
        if (this.f.remove(Integer.valueOf(e2)).booleanValue()) {
            textView = this.d.remove(Integer.valueOf(e2));
        } else {
            textView = this.b.remove(Integer.valueOf(e2));
        }
        if (remove == null || textView == null) {
            d e3 = a.c().e();
            String b2 = yVar.b();
            e3.a(b2, "" + e2);
            return false;
        }
        removeView(textView);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean g(y yVar) {
        int e2 = t.e(yVar.a(), "id");
        View remove = this.g.remove(Integer.valueOf(e2));
        m0 remove2 = this.f1174a.remove(Integer.valueOf(e2));
        if (remove == null || remove2 == null) {
            d e3 = a.c().e();
            String b2 = yVar.b();
            e3.a(b2, "" + e2);
            return false;
        }
        if (remove2.c()) {
            remove2.i();
        }
        remove2.a();
        removeView(remove2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean h(y yVar) {
        int e2 = t.e(yVar.a(), "id");
        i c2 = a.c();
        View remove = this.g.remove(Integer.valueOf(e2));
        n0 remove2 = this.c.remove(Integer.valueOf(e2));
        if (remove2 == null || remove == null) {
            d e3 = c2.e();
            String b2 = yVar.b();
            e3.a(b2, "" + e2);
            return false;
        }
        c2.p().a(remove2.d());
        removeView(remove2);
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean i(y yVar) {
        JSONObject a2 = yVar.a();
        return t.e(a2, "container_id") == this.j && t.g(a2, "ad_session_id").equals(this.l);
    }

    /* access modifiers changed from: package-private */
    public void j(y yVar) {
        int i2;
        this.f1174a = new HashMap<>();
        this.b = new HashMap<>();
        this.c = new HashMap<>();
        this.d = new HashMap<>();
        this.e = new HashMap<>();
        this.f = new HashMap<>();
        this.g = new HashMap<>();
        this.s = new ArrayList<>();
        this.t = new ArrayList<>();
        JSONObject a2 = yVar.a();
        if (t.c(a2, "transparent")) {
            setBackgroundColor(0);
        }
        this.j = t.e(a2, "id");
        this.h = t.e(a2, "width");
        this.i = t.e(a2, "height");
        this.k = t.e(a2, "module_id");
        this.n = t.c(a2, "viewability_enabled");
        this.u = this.j == 1;
        i c2 = a.c();
        if (this.h == 0 && this.i == 0) {
            this.h = c2.k().B();
            if (c2.t().g()) {
                i2 = c2.k().A() - l0.e(a.b());
            } else {
                i2 = c2.k().A();
            }
            this.i = i2;
        } else {
            setLayoutParams(new FrameLayout.LayoutParams(this.h, this.i));
        }
        ArrayList<a0> arrayList = this.s;
        b bVar = new b();
        a.a("VideoView.create", (a0) bVar, true);
        arrayList.add(bVar);
        ArrayList<a0> arrayList2 = this.s;
        C0002c cVar = new C0002c();
        a.a("VideoView.destroy", (a0) cVar, true);
        arrayList2.add(cVar);
        ArrayList<a0> arrayList3 = this.s;
        d dVar = new d();
        a.a("WebView.create", (a0) dVar, true);
        arrayList3.add(dVar);
        ArrayList<a0> arrayList4 = this.s;
        e eVar = new e();
        a.a("WebView.destroy", (a0) eVar, true);
        arrayList4.add(eVar);
        ArrayList<a0> arrayList5 = this.s;
        f fVar = new f();
        a.a("TextView.create", (a0) fVar, true);
        arrayList5.add(fVar);
        ArrayList<a0> arrayList6 = this.s;
        g gVar = new g();
        a.a("TextView.destroy", (a0) gVar, true);
        arrayList6.add(gVar);
        ArrayList<a0> arrayList7 = this.s;
        h hVar = new h();
        a.a("ImageView.create", (a0) hVar, true);
        arrayList7.add(hVar);
        ArrayList<a0> arrayList8 = this.s;
        i iVar = new i();
        a.a("ImageView.destroy", (a0) iVar, true);
        arrayList8.add(iVar);
        this.t.add("VideoView.create");
        this.t.add("VideoView.destroy");
        this.t.add("WebView.create");
        this.t.add("WebView.destroy");
        this.t.add("TextView.create");
        this.t.add("TextView.destroy");
        this.t.add("ImageView.create");
        this.t.add("ImageView.destroy");
        VideoView videoView = new VideoView(this.y);
        this.z = videoView;
        videoView.setVisibility(8);
        addView(this.z);
        setClipToPadding(false);
        if (this.n) {
            d(t.c(yVar.a(), "advanced_viewability"));
        }
    }

    /* access modifiers changed from: package-private */
    public int k() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, k0> l() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, m0> m() {
        return this.f1174a;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, n0> n() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public boolean o() {
        return this.v;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        return false;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        int action = motionEvent.getAction() & JfifUtil.MARKER_FIRST_BYTE;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        i c2 = a.c();
        d e2 = c2.e();
        int x2 = (int) motionEvent.getX();
        int y2 = (int) motionEvent.getY();
        JSONObject b2 = t.b();
        t.b(b2, "view_id", -1);
        t.a(b2, "ad_session_id", this.l);
        t.b(b2, "container_x", x2);
        t.b(b2, "container_y", y2);
        t.b(b2, "view_x", x2);
        t.b(b2, "view_y", y2);
        t.b(b2, "id", this.j);
        if (action == 0) {
            new y("AdContainer.on_touch_began", this.k, b2).c();
        } else if (action == 1) {
            if (!this.u) {
                c2.a(e2.b().get(this.l));
            }
            new y("AdContainer.on_touch_ended", this.k, b2).c();
        } else if (action == 2) {
            new y("AdContainer.on_touch_moved", this.k, b2).c();
        } else if (action == 3) {
            new y("AdContainer.on_touch_cancelled", this.k, b2).c();
        } else if (action == 5) {
            int action2 = (motionEvent.getAction() & 65280) >> 8;
            t.b(b2, "container_x", (int) motionEvent2.getX(action2));
            t.b(b2, "container_y", (int) motionEvent2.getY(action2));
            t.b(b2, "view_x", (int) motionEvent2.getX(action2));
            t.b(b2, "view_y", (int) motionEvent2.getY(action2));
            new y("AdContainer.on_touch_began", this.k, b2).c();
        } else if (action == 6) {
            int action3 = (motionEvent.getAction() & 65280) >> 8;
            t.b(b2, "container_x", (int) motionEvent2.getX(action3));
            t.b(b2, "container_y", (int) motionEvent2.getY(action3));
            t.b(b2, "view_x", (int) motionEvent2.getX(action3));
            t.b(b2, "view_y", (int) motionEvent2.getY(action3));
            t.b(b2, "x", (int) motionEvent2.getX(action3));
            t.b(b2, "y", (int) motionEvent2.getY(action3));
            if (!this.u) {
                c2.a(e2.b().get(this.l));
            }
            new y("AdContainer.on_touch_ended", this.k, b2).c();
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public boolean p() {
        return this.u;
    }

    /* access modifiers changed from: package-private */
    public boolean q() {
        return this.w;
    }

    /* access modifiers changed from: package-private */
    public m0 c(y yVar) {
        int e2 = t.e(yVar.a(), "id");
        m0 m0Var = new m0(this.y, yVar, e2, this);
        m0Var.d();
        this.f1174a.put(Integer.valueOf(e2), m0Var);
        this.g.put(Integer.valueOf(e2), m0Var);
        return m0Var;
    }

    /* access modifiers changed from: package-private */
    public n0 d(y yVar) {
        n0 n0Var;
        JSONObject a2 = yVar.a();
        int e2 = t.e(a2, "id");
        boolean c2 = t.c(a2, "is_module");
        i c3 = a.c();
        if (c2) {
            n0Var = c3.z().get(Integer.valueOf(t.e(a2, "module_id")));
            if (n0Var == null) {
                new v.a().a("Module WebView created with invalid id").a(v.h);
                return null;
            }
            n0Var.a(yVar, e2, this);
        } else {
            try {
                n0Var = new n0(this.y, yVar, e2, c3.p().d(), this);
            } catch (RuntimeException e3) {
                v.a aVar = new v.a();
                aVar.a(e3.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(v.h);
                AdColony.d();
                return null;
            }
        }
        this.c.put(Integer.valueOf(e2), n0Var);
        this.g.put(Integer.valueOf(e2), n0Var);
        JSONObject b2 = t.b();
        t.b(b2, "module_id", n0Var.d());
        t.b(b2, "mraid_module_id", n0Var.c());
        yVar.a(b2).c();
        return n0Var;
    }

    /* access modifiers changed from: package-private */
    public boolean e(y yVar) {
        int e2 = t.e(yVar.a(), "id");
        View remove = this.g.remove(Integer.valueOf(e2));
        r remove2 = this.e.remove(Integer.valueOf(e2));
        if (remove == null || remove2 == null) {
            d e3 = a.c().e();
            String b2 = yVar.b();
            e3.a(b2, "" + e2);
            return false;
        }
        removeView(remove2);
        return true;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"InlinedApi"})
    public View b(y yVar) {
        JSONObject a2 = yVar.a();
        int e2 = t.e(a2, "id");
        if (t.c(a2, "editable")) {
            o oVar = new o(this.y, yVar, e2, this);
            oVar.a();
            this.d.put(Integer.valueOf(e2), oVar);
            this.g.put(Integer.valueOf(e2), oVar);
            this.f.put(Integer.valueOf(e2), Boolean.TRUE);
            return oVar;
        } else if (!t.c(a2, "button")) {
            k0 k0Var = new k0(this.y, yVar, e2, this);
            k0Var.a();
            this.b.put(Integer.valueOf(e2), k0Var);
            this.g.put(Integer.valueOf(e2), k0Var);
            this.f.put(Integer.valueOf(e2), Boolean.FALSE);
            return k0Var;
        } else {
            k0 k0Var2 = new k0(this.y, 16974145, yVar, e2, this);
            k0Var2.a();
            this.b.put(Integer.valueOf(e2), k0Var2);
            this.g.put(Integer.valueOf(e2), k0Var2);
            this.f.put(Integer.valueOf(e2), Boolean.FALSE);
            return k0Var2;
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<a0> i() {
        return this.s;
    }

    /* access modifiers changed from: package-private */
    public r a(y yVar) {
        int e2 = t.e(yVar.a(), "id");
        r rVar = new r(this.y, yVar, e2, this);
        rVar.a();
        this.e.put(Integer.valueOf(e2), rVar);
        this.g.put(Integer.valueOf(e2), rVar);
        return rVar;
    }

    /* access modifiers changed from: package-private */
    public int c() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, View> e() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z2) {
        this.v = z2;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, o> f() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, r> h() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public HashMap<Integer, Boolean> g() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z2) {
        this.u = z2;
    }

    /* access modifiers changed from: private */
    public void a(int i2, int i3, n0 n0Var) {
        float x2 = a.c().k().x();
        if (n0Var != null) {
            JSONObject b2 = t.b();
            t.b(b2, "app_orientation", l0.d(l0.e()));
            t.b(b2, "width", (int) (((float) n0Var.n()) / x2));
            t.b(b2, "height", (int) (((float) n0Var.m()) / x2));
            t.b(b2, "x", i2);
            t.b(b2, "y", i3);
            t.a(b2, "ad_session_id", this.l);
            new y("MRAID.on_size_change", this.k, b2).c();
        }
    }

    /* access modifiers changed from: private */
    public void a(float f2, double d2) {
        JSONObject b2 = t.b();
        t.b(b2, "id", this.j);
        t.a(b2, "ad_session_id", this.l);
        t.a(b2, "exposure", (double) f2);
        t.a(b2, "volume", d2);
        new y("AdContainer.on_exposure_change", this.k, b2).c();
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z2) {
        this.w = z2;
    }

    /* access modifiers changed from: package-private */
    public int b() {
        return this.i;
    }

    private void d(boolean z2) {
        new Thread(new a(new j(z2))).start();
    }

    /* access modifiers changed from: package-private */
    public void b(int i2) {
        this.h = i2;
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.i = i2;
    }

    /* access modifiers changed from: package-private */
    public void a(AdSession adSession) {
        this.x = adSession;
        a((Map) this.g);
    }

    /* access modifiers changed from: package-private */
    public void a(Map map) {
        if (this.x != null && map != null) {
            for (Map.Entry value : map.entrySet()) {
                a((View) value.getValue(), FriendlyObstructionPurpose.OTHER);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view, FriendlyObstructionPurpose friendlyObstructionPurpose) {
        AdSession adSession = this.x;
        if (adSession != null && view != null) {
            try {
                adSession.a(view, friendlyObstructionPurpose, (String) null);
            } catch (RuntimeException unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(View view) {
        AdSession adSession = this.x;
        if (adSession != null && view != null) {
            try {
                adSession.b(view);
            } catch (RuntimeException unused) {
            }
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<String> j() {
        return this.t;
    }
}
