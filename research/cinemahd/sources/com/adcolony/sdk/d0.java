package com.adcolony.sdk;

import android.webkit.WebView;
import com.adcolony.sdk.v;
import com.facebook.react.uimanager.ViewProps;
import com.iab.omid.library.adcolony.adsession.AdEvents;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.AdSessionConfiguration;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.CreativeType;
import com.iab.omid.library.adcolony.adsession.ImpressionType;
import com.iab.omid.library.adcolony.adsession.Owner;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.adsession.media.InteractionType;
import com.iab.omid.library.adcolony.adsession.media.MediaEvents;
import com.iab.omid.library.adcolony.adsession.media.Position;
import com.iab.omid.library.adcolony.adsession.media.VastProperties;
import com.vungle.warren.model.Advertisement;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

class d0 {

    /* renamed from: a  reason: collision with root package name */
    private AdSession f1215a;
    private AdEvents b;
    private MediaEvents c;
    private List<VerificationScriptResource> d = new ArrayList();
    /* access modifiers changed from: private */
    public int e = -1;
    /* access modifiers changed from: private */
    public String f = "";
    private boolean g;
    private boolean h;
    private boolean i;
    private boolean j;
    /* access modifiers changed from: private */
    public boolean k;
    private int l;
    private int m;
    private String n = "";
    /* access modifiers changed from: private */
    public String o = "";

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        final /* synthetic */ String f1216a;

        a(String str) {
            this.f1216a = str;
        }

        public void run() {
            JSONObject b2 = t.b();
            JSONObject b3 = t.b();
            t.b(b3, "session_type", d0.this.e);
            t.a(b3, "session_id", d0.this.f);
            t.a(b3, "event", this.f1216a);
            t.a(b2, "type", "iab_hook");
            t.a(b2, "message", b3.toString());
            new y("CustomMessage.controller_send", 0, b2).c();
        }
    }

    class b implements AdColonyCustomMessageListener {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ String f1218a;
            final /* synthetic */ String b;
            final /* synthetic */ float c;

            a(String str, String str2, float f) {
                this.f1218a = str;
                this.b = str2;
                this.c = f;
            }

            public void run() {
                if (this.f1218a.equals(d0.this.o)) {
                    d0.this.a(this.b, this.c);
                    return;
                }
                AdColonyAdView adColonyAdView = a.c().e().b().get(this.f1218a);
                d0 omidManager = adColonyAdView != null ? adColonyAdView.getOmidManager() : null;
                if (omidManager != null) {
                    omidManager.a(this.b, this.c);
                }
            }
        }

        b() {
        }

        public void a(AdColonyCustomMessage adColonyCustomMessage) {
            JSONObject b = t.b(adColonyCustomMessage.a());
            String g = t.g(b, "event_type");
            float floatValue = BigDecimal.valueOf(t.d(b, "duration")).floatValue();
            boolean c = t.c(b, "replay");
            boolean equals = t.g(b, "skip_type").equals("dec");
            String g2 = t.g(b, "asi");
            if (g.equals("skip") && equals) {
                boolean unused = d0.this.k = true;
            } else if (!c || (!g.equals(ViewProps.START) && !g.equals("first_quartile") && !g.equals("midpoint") && !g.equals("third_quartile") && !g.equals("complete"))) {
                l0.a((Runnable) new a(g2, g, floatValue));
            }
        }
    }

    d0(JSONObject jSONObject, String str) {
        VerificationScriptResource verificationScriptResource;
        this.e = a(jSONObject);
        this.j = t.c(jSONObject, "skippable");
        this.l = t.e(jSONObject, "skip_offset");
        this.m = t.e(jSONObject, "video_duration");
        JSONArray b2 = t.b(jSONObject, "js_resources");
        JSONArray b3 = t.b(jSONObject, "verification_params");
        JSONArray b4 = t.b(jSONObject, "vendor_keys");
        this.o = str;
        for (int i2 = 0; i2 < b2.length(); i2++) {
            try {
                String b5 = t.b(b3, i2);
                String b6 = t.b(b4, i2);
                URL url = new URL(t.b(b2, i2));
                if (!b5.equals("") && !b6.equals("")) {
                    verificationScriptResource = VerificationScriptResource.a(b6, url, b5);
                } else if (!b6.equals("")) {
                    verificationScriptResource = VerificationScriptResource.a(url);
                } else {
                    verificationScriptResource = VerificationScriptResource.a(url);
                }
                this.d.add(verificationScriptResource);
            } catch (MalformedURLException unused) {
                new v.a().a("Invalid js resource url passed to Omid").a(v.i);
            }
        }
        try {
            this.n = a.c().n().a(t.g(jSONObject, "filepath"), true).toString();
        } catch (IOException unused2) {
            new v.a().a("Error loading IAB JS Client").a(v.i);
        }
    }

    private void f() {
        AdColony.a((AdColonyCustomMessageListener) new b(), "viewability_ad_event");
    }

    /* access modifiers changed from: package-private */
    public int d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.h = true;
    }

    /* access modifiers changed from: package-private */
    public void b() {
        AdColony.a("viewability_ad_event");
        this.f1215a.a();
        b("end_session");
        this.f1215a = null;
    }

    /* access modifiers changed from: package-private */
    public AdSession c() {
        return this.f1215a;
    }

    private int a(JSONObject jSONObject) {
        if (this.e == -1) {
            int e2 = t.e(jSONObject, "ad_unit_type");
            String g2 = t.g(jSONObject, "ad_type");
            if (e2 == 0) {
                return 0;
            }
            if (e2 == 1) {
                if (g2.equals(Advertisement.KEY_VIDEO)) {
                    return 0;
                }
                if (g2.equals(ViewProps.DISPLAY)) {
                    return 1;
                }
                if (g2.equals("banner_display") || g2.equals("interstitial_display")) {
                    return 2;
                }
            }
        }
        return this.e;
    }

    private void b(String str) {
        l0.f1291a.execute(new a(str));
    }

    private void b(c cVar) {
        b("register_ad_view");
        n0 n0Var = a.c().z().get(Integer.valueOf(cVar.k()));
        if (n0Var == null && !cVar.n().isEmpty()) {
            n0Var = (n0) cVar.n().entrySet().iterator().next().getValue();
        }
        AdSession adSession = this.f1215a;
        if (adSession != null && n0Var != null) {
            adSession.a(n0Var);
            n0Var.h();
        } else if (adSession != null) {
            adSession.a(cVar);
            cVar.a(this.f1215a);
            b("register_obstructions");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        MediaEvents mediaEvents;
        VastProperties vastProperties;
        if (!this.i && this.e >= 0 && this.f1215a != null) {
            b(cVar);
            f();
            if (this.e != 0) {
                mediaEvents = null;
            } else {
                mediaEvents = MediaEvents.a(this.f1215a);
            }
            this.c = mediaEvents;
            this.f1215a.c();
            this.b = AdEvents.a(this.f1215a);
            b("start_session");
            if (this.c != null) {
                Position position = Position.PREROLL;
                if (this.j) {
                    vastProperties = VastProperties.a((float) this.l, true, position);
                } else {
                    vastProperties = VastProperties.a(true, position);
                }
                this.b.a(vastProperties);
            } else {
                this.b.b();
            }
            this.i = true;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() throws IllegalArgumentException {
        a((WebView) null);
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView) throws IllegalArgumentException {
        String str;
        List<VerificationScriptResource> list;
        if (this.e >= 0 && (str = this.n) != null && !str.equals("") && (list = this.d) != null) {
            if (!list.isEmpty() || d() == 2) {
                i c2 = a.c();
                Owner owner = Owner.NATIVE;
                ImpressionType impressionType = ImpressionType.BEGIN_TO_RENDER;
                int d2 = d();
                if (d2 == 0) {
                    CreativeType creativeType = CreativeType.VIDEO;
                    AdSession a2 = AdSession.a(AdSessionConfiguration.a(creativeType, impressionType, owner, owner, false), AdSessionContext.a(c2.s(), this.n, this.d, (String) null, (String) null));
                    this.f1215a = a2;
                    this.f = a2.b();
                    b("inject_javascript");
                } else if (d2 == 1) {
                    CreativeType creativeType2 = CreativeType.NATIVE_DISPLAY;
                    AdSession a3 = AdSession.a(AdSessionConfiguration.a(creativeType2, impressionType, owner, (Owner) null, false), AdSessionContext.a(c2.s(), this.n, this.d, (String) null, (String) null));
                    this.f1215a = a3;
                    this.f = a3.b();
                    b("inject_javascript");
                } else if (d2 == 2) {
                    CreativeType creativeType3 = CreativeType.HTML_DISPLAY;
                    AdSession a4 = AdSession.a(AdSessionConfiguration.a(creativeType3, impressionType, owner, (Owner) null, false), AdSessionContext.a(c2.s(), webView, "", (String) null));
                    this.f1215a = a4;
                    this.f = a4.b();
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        a(str, 0.0f);
    }

    /* access modifiers changed from: package-private */
    public void a(String str, float f2) {
        if (a.d() && this.f1215a != null) {
            if (this.c != null || str.equals(ViewProps.START) || str.equals("skip") || str.equals("continue") || str.equals("cancel")) {
                char c2 = 65535;
                try {
                    switch (str.hashCode()) {
                        case -1941887438:
                            if (str.equals("first_quartile")) {
                                c2 = 1;
                                break;
                            }
                            break;
                        case -1710060637:
                            if (str.equals("buffer_start")) {
                                c2 = 12;
                                break;
                            }
                            break;
                        case -1638835128:
                            if (str.equals("midpoint")) {
                                c2 = 2;
                                break;
                            }
                            break;
                        case -1367724422:
                            if (str.equals("cancel")) {
                                c2 = 7;
                                break;
                            }
                            break;
                        case -934426579:
                            if (str.equals("resume")) {
                                c2 = 11;
                                break;
                            }
                            break;
                        case -651914917:
                            if (str.equals("third_quartile")) {
                                c2 = 3;
                                break;
                            }
                            break;
                        case -599445191:
                            if (str.equals("complete")) {
                                c2 = 4;
                                break;
                            }
                            break;
                        case -567202649:
                            if (str.equals("continue")) {
                                c2 = 5;
                                break;
                            }
                            break;
                        case -342650039:
                            if (str.equals("sound_mute")) {
                                c2 = 8;
                                break;
                            }
                            break;
                        case 3532159:
                            if (str.equals("skip")) {
                                c2 = 6;
                                break;
                            }
                            break;
                        case 106440182:
                            if (str.equals("pause")) {
                                c2 = 10;
                                break;
                            }
                            break;
                        case 109757538:
                            if (str.equals(ViewProps.START)) {
                                c2 = 0;
                                break;
                            }
                            break;
                        case 583742045:
                            if (str.equals("in_video_engagement")) {
                                c2 = 14;
                                break;
                            }
                            break;
                        case 823102269:
                            if (str.equals("html5_interaction")) {
                                c2 = 15;
                                break;
                            }
                            break;
                        case 1648173410:
                            if (str.equals("sound_unmute")) {
                                c2 = 9;
                                break;
                            }
                            break;
                        case 1906584668:
                            if (str.equals("buffer_end")) {
                                c2 = 13;
                                break;
                            }
                            break;
                    }
                    switch (c2) {
                        case 0:
                            this.b.a();
                            MediaEvents mediaEvents = this.c;
                            if (mediaEvents != null) {
                                if (f2 <= 0.0f) {
                                    f2 = (float) this.m;
                                }
                                mediaEvents.a(f2, 1.0f);
                            }
                            b(str);
                            return;
                        case 1:
                            this.c.d();
                            b(str);
                            return;
                        case 2:
                            this.c.e();
                            b(str);
                            return;
                        case 3:
                            this.c.i();
                            b(str);
                            return;
                        case 4:
                            this.k = true;
                            this.c.c();
                            b(str);
                            return;
                        case 5:
                            b(str);
                            b();
                            return;
                        case 6:
                        case 7:
                            MediaEvents mediaEvents2 = this.c;
                            if (mediaEvents2 != null) {
                                mediaEvents2.h();
                            }
                            b(str);
                            b();
                            return;
                        case 8:
                            this.c.a(0.0f);
                            b(str);
                            return;
                        case 9:
                            this.c.a(1.0f);
                            b(str);
                            return;
                        case 10:
                            if (!this.g && !this.h && !this.k) {
                                this.c.f();
                                b(str);
                                this.g = true;
                                this.h = false;
                                return;
                            }
                            return;
                        case 11:
                            if (this.g && !this.k) {
                                this.c.g();
                                b(str);
                                this.g = false;
                                return;
                            }
                            return;
                        case 12:
                            this.c.b();
                            b(str);
                            return;
                        case 13:
                            this.c.a();
                            b(str);
                            return;
                        case 14:
                        case 15:
                            this.c.a(InteractionType.CLICK);
                            b(str);
                            if (this.h && !this.g && !this.k) {
                                this.c.f();
                                b("pause");
                                this.g = true;
                                this.h = false;
                                return;
                            }
                            return;
                        default:
                            return;
                    }
                } catch (IllegalArgumentException | IllegalStateException e2) {
                    v.a a2 = new v.a().a("Recording IAB event for ").a(str);
                    a2.a(" caused " + e2.getClass()).a(v.g);
                }
            }
        }
    }
}
