package com.adcolony.sdk;

import android.content.Context;
import android.os.StatFs;
import com.adcolony.sdk.v;
import java.io.File;

class h0 {

    /* renamed from: a  reason: collision with root package name */
    private String f1227a;
    private String b;
    private String c;
    private String d;
    private File e;
    private File f;
    private File g;

    h0() {
    }

    /* access modifiers changed from: package-private */
    public double a(String str) {
        try {
            StatFs statFs = new StatFs(str);
            return (double) (((long) statFs.getAvailableBlocks()) * ((long) statFs.getBlockSize()));
        } catch (RuntimeException unused) {
            return 0.0d;
        }
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public boolean e() {
        i c2 = a.c();
        this.f1227a = f() + "/adc3/";
        this.b = this.f1227a + "media/";
        File file = new File(this.b);
        this.e = file;
        if (!file.isDirectory()) {
            this.e.delete();
            this.e.mkdirs();
        }
        if (!this.e.isDirectory()) {
            c2.a(true);
            return false;
        } else if (a(this.b) < 2.097152E7d) {
            new v.a().a("Not enough memory available at media path, disabling AdColony.").a(v.f);
            c2.a(true);
            return false;
        } else {
            this.c = f() + "/adc3/data/";
            File file2 = new File(this.c);
            this.f = file2;
            if (!file2.isDirectory()) {
                this.f.delete();
            }
            this.f.mkdirs();
            this.d = this.f1227a + "tmp/";
            File file3 = new File(this.d);
            this.g = file3;
            if (!file3.isDirectory()) {
                this.g.delete();
                this.g.mkdirs();
            }
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public String f() {
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        return b2.getFilesDir().getAbsolutePath();
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        File file = this.e;
        if (file == null || this.f == null || this.g == null) {
            return false;
        }
        if (!file.isDirectory()) {
            this.e.delete();
        }
        if (!this.f.isDirectory()) {
            this.f.delete();
        }
        if (!this.g.isDirectory()) {
            this.g.delete();
        }
        this.e.mkdirs();
        this.f.mkdirs();
        this.g.mkdirs();
        return true;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f1227a;
    }
}
