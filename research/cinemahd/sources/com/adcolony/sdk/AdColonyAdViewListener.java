package com.adcolony.sdk;

public abstract class AdColonyAdViewListener {

    /* renamed from: a  reason: collision with root package name */
    String f1164a = "";
    AdColonyAdSize b;
    d0 c;

    /* access modifiers changed from: package-private */
    public AdColonyAdSize a() {
        return this.b;
    }

    public void a(AdColonyAdView adColonyAdView) {
    }

    public void a(AdColonyZone adColonyZone) {
    }

    /* access modifiers changed from: package-private */
    public d0 b() {
        return this.c;
    }

    public void b(AdColonyAdView adColonyAdView) {
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.f1164a;
    }

    public void c(AdColonyAdView adColonyAdView) {
    }

    public void d(AdColonyAdView adColonyAdView) {
    }

    public abstract void e(AdColonyAdView adColonyAdView);

    /* access modifiers changed from: package-private */
    public void a(d0 d0Var) {
        this.c = d0Var;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.f1164a = str;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAdSize adColonyAdSize) {
        this.b = adColonyAdSize;
    }
}
