package com.adcolony.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.FrameLayout;
import com.facebook.common.util.ByteConstants;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONObject;

class b extends Activity {

    /* renamed from: a  reason: collision with root package name */
    c f1172a;
    int b = -1;
    int c;
    boolean d;
    boolean e;
    boolean f;
    boolean g;
    boolean h;
    boolean i;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            b.this.a(yVar);
        }
    }

    b() {
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        Iterator<Map.Entry<Integer, m0>> it2 = this.f1172a.m().entrySet().iterator();
        while (it2.hasNext() && !isFinishing()) {
            m0 m0Var = (m0) it2.next().getValue();
            if (!m0Var.f() && m0Var.b().isPlaying()) {
                m0Var.g();
            }
        }
        AdColonyInterstitial g2 = a.c().g();
        if (g2 != null && g2.i() && g2.e().c() != null && z && this.h) {
            g2.e().a("pause");
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        for (Map.Entry<Integer, m0> value : this.f1172a.m().entrySet()) {
            m0 m0Var = (m0) value.getValue();
            if (!m0Var.f() && !m0Var.b().isPlaying() && !a.c().l().b()) {
                m0Var.h();
            }
        }
        AdColonyInterstitial g2 = a.c().g();
        if (g2 != null && g2.i() && g2.e().c() != null) {
            if ((!z || !this.h) && this.i) {
                g2.e().a("resume");
            }
        }
    }

    public void onBackPressed() {
        JSONObject b2 = t.b();
        t.a(b2, "id", this.f1172a.a());
        new y("AdSession.on_back_button", this.f1172a.k(), b2).c();
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this instanceof AdColonyInterstitialActivity) {
            a();
        } else {
            ((AdColonyAdViewActivity) this).c();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (!a.e() || a.c().i() == null) {
            finish();
            return;
        }
        i c2 = a.c();
        this.f = false;
        c i2 = c2.i();
        this.f1172a = i2;
        i2.b(false);
        if (l0.f()) {
            this.f1172a.b(true);
        }
        this.f1172a.a();
        this.c = this.f1172a.k();
        boolean g2 = c2.t().g();
        this.g = g2;
        if (g2) {
            getWindow().addFlags(2048);
            getWindow().clearFlags(ByteConstants.KB);
        } else {
            getWindow().addFlags(ByteConstants.KB);
            getWindow().clearFlags(2048);
        }
        requestWindowFeature(1);
        getWindow().getDecorView().setBackgroundColor(-16777216);
        if (c2.t().e()) {
            getWindow().addFlags(128);
        }
        ViewParent parent = this.f1172a.getParent();
        if (parent != null) {
            ((ViewGroup) parent).removeView(this.f1172a);
        }
        setContentView(this.f1172a);
        ArrayList<a0> i3 = this.f1172a.i();
        a aVar = new a();
        a.a("AdSession.finish_fullscreen_ad", (a0) aVar, true);
        i3.add(aVar);
        this.f1172a.j().add("AdSession.finish_fullscreen_ad");
        a(this.b);
        if (!this.f1172a.o()) {
            JSONObject b2 = t.b();
            t.a(b2, "id", this.f1172a.a());
            t.b(b2, "screen_width", this.f1172a.d());
            t.b(b2, "screen_height", this.f1172a.b());
            new y("AdSession.on_fullscreen_ad_started", this.f1172a.k(), b2).c();
            this.f1172a.c(true);
            return;
        }
        a();
    }

    public void onDestroy() {
        super.onDestroy();
        if (a.e() && this.f1172a != null && !this.d) {
            if ((Build.VERSION.SDK_INT < 24 || !l0.f()) && !this.f1172a.q()) {
                JSONObject b2 = t.b();
                t.a(b2, "id", this.f1172a.a());
                new y("AdSession.on_error", this.f1172a.k(), b2).c();
                this.f = true;
            }
        }
    }

    public void onPause() {
        super.onPause();
        a(this.e);
        this.e = false;
    }

    public void onResume() {
        super.onResume();
        a();
        b(this.e);
        this.e = true;
        this.i = true;
    }

    public void onWindowFocusChanged(boolean z) {
        if (z && this.e) {
            a.c().w().b(true);
            b(this.e);
            this.h = true;
        } else if (!z && this.e) {
            a.c().w().a(true);
            a(this.e);
            this.h = false;
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        int i2;
        i c2 = a.c();
        if (this.f1172a == null) {
            this.f1172a = c2.i();
        }
        c cVar = this.f1172a;
        if (cVar != null) {
            cVar.b(false);
            if (l0.f()) {
                this.f1172a.b(true);
            }
            int B = c2.k().B();
            if (this.g) {
                i2 = c2.k().A() - l0.e(a.b());
            } else {
                i2 = c2.k().A();
            }
            if (B > 0 && i2 > 0) {
                JSONObject b2 = t.b();
                JSONObject b3 = t.b();
                float x = c2.k().x();
                t.b(b3, "width", (int) (((float) B) / x));
                t.b(b3, "height", (int) (((float) i2) / x));
                t.b(b3, "app_orientation", l0.d(l0.e()));
                t.b(b3, "x", 0);
                t.b(b3, "y", 0);
                t.a(b3, "ad_session_id", this.f1172a.a());
                t.b(b2, "screen_width", B);
                t.b(b2, "screen_height", i2);
                t.a(b2, "ad_session_id", this.f1172a.a());
                t.b(b2, "id", this.f1172a.c());
                this.f1172a.setLayoutParams(new FrameLayout.LayoutParams(B, i2));
                this.f1172a.b(B);
                this.f1172a.a(i2);
                new y("MRAID.on_size_change", this.f1172a.k(), b3).c();
                new y("AdContainer.on_orientation_change", this.f1172a.k(), b2).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(y yVar) {
        int e2 = t.e(yVar.a(), ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
        if ((e2 == 5 || e2 == 0 || e2 == 6 || e2 == 1) && !this.d) {
            i c2 = a.c();
            l l = c2.l();
            c2.a(yVar);
            if (l.a() != null) {
                l.a().dismiss();
                l.a((AlertDialog) null);
            }
            if (!this.f) {
                finish();
            }
            this.d = true;
            ((ViewGroup) getWindow().getDecorView()).removeAllViews();
            c2.d(false);
            JSONObject b2 = t.b();
            t.a(b2, "id", this.f1172a.a());
            new y("AdSession.on_close", this.f1172a.k(), b2).c();
            c2.a((c) null);
            c2.a((AdColonyInterstitial) null);
            c2.a((AdColonyAdView) null);
            a.c().e().a().remove(this.f1172a.a());
        }
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        if (i2 == 0) {
            setRequestedOrientation(7);
        } else if (i2 != 1) {
            setRequestedOrientation(4);
        } else {
            setRequestedOrientation(6);
        }
        this.b = i2;
    }
}
