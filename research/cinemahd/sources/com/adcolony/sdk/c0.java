package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import com.adcolony.sdk.v;
import com.facebook.react.uimanager.ViewProps;

class c0 {
    c0() {
    }

    /* access modifiers changed from: package-private */
    public String a() {
        if (c()) {
            return "wifi";
        }
        return b() ? "cell" : ViewProps.NONE;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"MissingPermission"})
    public boolean b() {
        NetworkInfo networkInfo;
        Context b = a.b();
        if (b == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) b.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo == null) {
                return false;
            }
            int type = networkInfo.getType();
            if (type == 0 || type >= 2) {
                return true;
            }
            return false;
        } catch (SecurityException e) {
            new v.a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(v.h);
            return false;
        } catch (Exception e2) {
            new v.a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_mobile(): ").a(e2.toString()).a(v.i);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"MissingPermission"})
    public boolean c() {
        NetworkInfo networkInfo;
        Context b = a.b();
        if (b == null) {
            return false;
        }
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) b.getApplicationContext().getSystemService("connectivity");
            if (connectivityManager == null) {
                networkInfo = null;
            } else {
                networkInfo = connectivityManager.getActiveNetworkInfo();
            }
            if (networkInfo != null && networkInfo.getType() == 1) {
                return true;
            }
            return false;
        } catch (SecurityException e) {
            new v.a().a("SecurityException - please ensure you added the ").a("ACCESS_NETWORK_STATE permission: ").a(e.toString()).a(v.h);
            return false;
        } catch (Exception e2) {
            new v.a().a("Exception occurred when retrieving activeNetworkInfo in ").a("ADCNetwork.using_wifi(): ").a(e2.toString()).a(v.i);
            return false;
        }
    }
}
