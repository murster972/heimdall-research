package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.MotionEvent;
import android.widget.FrameLayout;
import android.widget.ImageView;
import com.facebook.imageutils.JfifUtil;
import com.facebook.react.uimanager.ViewProps;
import java.io.File;
import java.util.ArrayList;
import org.json.JSONObject;

@SuppressLint({"AppCompatCustomView"})
class r extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private int f1355a;
    private int b;
    private int c;
    private int d;
    private int e;
    private boolean f;
    private boolean g;
    private boolean h;
    private String i;
    private String j;
    private y k;
    private c l;

    class a implements a0 {
        a() {
        }

        public void a(y yVar) {
            if (r.this.a(yVar)) {
                r.this.d(yVar);
            }
        }
    }

    class b implements a0 {
        b() {
        }

        public void a(y yVar) {
            if (r.this.a(yVar)) {
                r.this.b(yVar);
            }
        }
    }

    class c implements a0 {
        c() {
        }

        public void a(y yVar) {
            if (r.this.a(yVar)) {
                r.this.c(yVar);
            }
        }
    }

    r(Context context, y yVar, int i2, c cVar) {
        super(context);
        this.f1355a = i2;
        this.k = yVar;
        this.l = cVar;
    }

    @SuppressLint({"ClickableViewAccessibility"})
    public boolean onTouchEvent(MotionEvent motionEvent) {
        MotionEvent motionEvent2 = motionEvent;
        i c2 = a.c();
        d e2 = c2.e();
        int action = motionEvent.getAction() & JfifUtil.MARKER_FIRST_BYTE;
        if (action != 0 && action != 1 && action != 3 && action != 2 && action != 5 && action != 6) {
            return false;
        }
        int x = (int) motionEvent.getX();
        int y = (int) motionEvent.getY();
        JSONObject b2 = t.b();
        t.b(b2, "view_id", this.f1355a);
        t.a(b2, "ad_session_id", this.j);
        t.b(b2, "container_x", this.b + x);
        t.b(b2, "container_y", this.c + y);
        t.b(b2, "view_x", x);
        t.b(b2, "view_y", y);
        t.b(b2, "id", this.l.getId());
        if (action != 0) {
            int i2 = y;
            if (action == 1) {
                if (!this.l.p()) {
                    c2.a(e2.b().get(this.j));
                }
                if (x <= 0 || x >= this.d || i2 <= 0 || i2 >= this.e) {
                    new y("AdContainer.on_touch_cancelled", this.l.k(), b2).c();
                    return true;
                }
                new y("AdContainer.on_touch_ended", this.l.k(), b2).c();
                return true;
            } else if (action == 2) {
                new y("AdContainer.on_touch_moved", this.l.k(), b2).c();
                return true;
            } else if (action == 3) {
                new y("AdContainer.on_touch_cancelled", this.l.k(), b2).c();
                return true;
            } else if (action == 5) {
                int action2 = (motionEvent.getAction() & 65280) >> 8;
                t.b(b2, "container_x", ((int) motionEvent2.getX(action2)) + this.b);
                t.b(b2, "container_y", ((int) motionEvent2.getY(action2)) + this.c);
                t.b(b2, "view_x", (int) motionEvent2.getX(action2));
                t.b(b2, "view_y", (int) motionEvent2.getY(action2));
                new y("AdContainer.on_touch_began", this.l.k(), b2).c();
                return true;
            } else if (action != 6) {
                return true;
            } else {
                int action3 = (motionEvent.getAction() & 65280) >> 8;
                int x2 = (int) motionEvent2.getX(action3);
                int y2 = (int) motionEvent2.getY(action3);
                t.b(b2, "container_x", ((int) motionEvent2.getX(action3)) + this.b);
                t.b(b2, "container_y", ((int) motionEvent2.getY(action3)) + this.c);
                t.b(b2, "view_x", (int) motionEvent2.getX(action3));
                t.b(b2, "view_y", (int) motionEvent2.getY(action3));
                if (!this.l.p()) {
                    c2.a(e2.b().get(this.j));
                }
                if (x2 <= 0 || x2 >= this.d || y2 <= 0 || y2 >= this.e) {
                    new y("AdContainer.on_touch_cancelled", this.l.k(), b2).c();
                    return true;
                }
                new y("AdContainer.on_touch_ended", this.l.k(), b2).c();
                return true;
            }
        } else {
            new y("AdContainer.on_touch_began", this.l.k(), b2).c();
            return true;
        }
    }

    /* access modifiers changed from: private */
    public boolean a(y yVar) {
        JSONObject a2 = yVar.a();
        return t.e(a2, "id") == this.f1355a && t.e(a2, "container_id") == this.l.c() && t.g(a2, "ad_session_id").equals(this.l.a());
    }

    /* access modifiers changed from: private */
    public void b(y yVar) {
        JSONObject a2 = yVar.a();
        this.b = t.e(a2, "x");
        this.c = t.e(a2, "y");
        this.d = t.e(a2, "width");
        this.e = t.e(a2, "height");
        if (this.f) {
            float x = (((float) this.e) * a.c().k().x()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * x);
            int intrinsicWidth = (int) (((float) getDrawable().getIntrinsicWidth()) * x);
            this.d = intrinsicWidth;
            this.b -= intrinsicWidth;
            this.c -= this.e;
        }
        FrameLayout.LayoutParams layoutParams = (FrameLayout.LayoutParams) getLayoutParams();
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.width = this.d;
        layoutParams.height = this.e;
        setLayoutParams(layoutParams);
    }

    /* access modifiers changed from: private */
    public void c(y yVar) {
        this.i = t.g(yVar.a(), "filepath");
        setImageURI(Uri.fromFile(new File(this.i)));
    }

    /* access modifiers changed from: private */
    public void d(y yVar) {
        if (t.c(yVar.a(), ViewProps.VISIBLE)) {
            setVisibility(0);
        } else {
            setVisibility(4);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        JSONObject a2 = this.k.a();
        this.j = t.g(a2, "ad_session_id");
        this.b = t.e(a2, "x");
        this.c = t.e(a2, "y");
        this.d = t.e(a2, "width");
        this.e = t.e(a2, "height");
        this.i = t.g(a2, "filepath");
        this.f = t.c(a2, "dpi");
        this.g = t.c(a2, "invert_y");
        this.h = t.c(a2, "wrap_content");
        setImageURI(Uri.fromFile(new File(this.i)));
        if (this.f) {
            float x = (((float) this.e) * a.c().k().x()) / ((float) getDrawable().getIntrinsicHeight());
            this.e = (int) (((float) getDrawable().getIntrinsicHeight()) * x);
            int intrinsicWidth = (int) (((float) getDrawable().getIntrinsicWidth()) * x);
            this.d = intrinsicWidth;
            this.b -= intrinsicWidth;
            this.c = this.g ? this.c + this.e : this.c - this.e;
        }
        setVisibility(4);
        FrameLayout.LayoutParams layoutParams = this.h ? new FrameLayout.LayoutParams(-2, -2) : new FrameLayout.LayoutParams(this.d, this.e);
        layoutParams.setMargins(this.b, this.c, 0, 0);
        layoutParams.gravity = 0;
        this.l.addView(this, layoutParams);
        ArrayList<a0> i2 = this.l.i();
        a aVar = new a();
        a.a("ImageView.set_visible", (a0) aVar, true);
        i2.add(aVar);
        ArrayList<a0> i3 = this.l.i();
        b bVar = new b();
        a.a("ImageView.set_bounds", (a0) bVar, true);
        i3.add(bVar);
        ArrayList<a0> i4 = this.l.i();
        c cVar = new c();
        a.a("ImageView.set_image", (a0) cVar, true);
        i4.add(cVar);
        this.l.j().add("ImageView.set_visible");
        this.l.j().add("ImageView.set_bounds");
        this.l.j().add("ImageView.set_image");
    }
}
