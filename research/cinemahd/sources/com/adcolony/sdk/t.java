package com.adcolony.sdk;

import com.adcolony.sdk.v;
import java.io.IOException;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class t {
    t() {
    }

    static JSONObject a(String str, String str2) {
        String str3;
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            if (str2 == null) {
                str3 = "";
            } else {
                str3 = str2 + ": " + e.toString();
            }
            new v.a().a(str3).a(v.i);
            return new JSONObject();
        }
    }

    static JSONObject b() {
        return new JSONObject();
    }

    static boolean c(JSONObject jSONObject, String str) {
        return jSONObject.optBoolean(str);
    }

    static double d(JSONObject jSONObject, String str) {
        return jSONObject.optDouble(str, 0.0d);
    }

    static int e(JSONObject jSONObject, String str) {
        return jSONObject.optInt(str);
    }

    static JSONObject f(JSONObject jSONObject, String str) {
        JSONObject optJSONObject = jSONObject.optJSONObject(str);
        return optJSONObject == null ? new JSONObject() : optJSONObject;
    }

    static String g(JSONObject jSONObject, String str) {
        return jSONObject.optString(str);
    }

    static boolean h(JSONObject jSONObject, String str) {
        try {
            a.c().n().a(str, jSONObject.toString(), false);
            return true;
        } catch (IOException e) {
            new v.a().a("IOException in ADCJSON's saveObject: ").a(e.toString()).a(v.i);
            return false;
        }
    }

    static String b(JSONArray jSONArray, int i) {
        return jSONArray.optString(i);
    }

    static JSONObject c(String str) {
        try {
            String sb = a.c().n().a(str, false).toString();
            return a(sb, "loadObject from filepath " + str);
        } catch (IOException e) {
            new v.a().a("IOException in ADCJSON's loadObject: ").a(e.toString()).a(v.i);
            return b();
        }
    }

    static JSONObject b(String str) {
        return a(str, (String) null);
    }

    static JSONArray b(JSONObject jSONObject, String str) {
        JSONArray optJSONArray = jSONObject.optJSONArray(str);
        return optJSONArray == null ? new JSONArray() : optJSONArray;
    }

    static boolean b(JSONObject jSONObject, String str, int i) {
        try {
            jSONObject.put(str, i);
            return true;
        } catch (JSONException e) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putInteger(): ").a(e.toString());
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + i).a(v.i);
            return false;
        }
    }

    static JSONObject a(JSONArray jSONArray, int i) {
        JSONObject optJSONObject = jSONArray.optJSONObject(i);
        return optJSONObject == null ? new JSONObject() : optJSONObject;
    }

    static JSONArray a() {
        return new JSONArray();
    }

    static JSONArray a(String str) {
        try {
            return new JSONArray(str);
        } catch (JSONException e) {
            new v.a().a(e.toString()).a(v.i);
            return new JSONArray();
        }
    }

    static void b(JSONArray jSONArray, String str) {
        jSONArray.put(str);
    }

    static boolean a(JSONObject jSONObject, String str, boolean z) {
        try {
            jSONObject.put(str, z);
            return true;
        } catch (JSONException e) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putBoolean(): ").a(e.toString());
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + z).a(v.i);
            return false;
        }
    }

    static int a(JSONObject jSONObject, String str, int i) {
        return jSONObject.optInt(str, i);
    }

    static boolean a(JSONObject jSONObject, String str, String str2) {
        try {
            jSONObject.put(str, str2);
            return true;
        } catch (JSONException e) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putString(): ").a(e.toString());
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + str2).a(v.i);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, long j) {
        try {
            jSONObject.put(str, j);
            return true;
        } catch (JSONException e) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putLong(): ").a(e.toString());
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + j).a(v.i);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, JSONArray jSONArray) {
        try {
            jSONObject.put(str, jSONArray);
            return true;
        } catch (JSONException e) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putArray(): ").a(e.toString());
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + jSONArray).a(v.i);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, JSONObject jSONObject2) {
        try {
            jSONObject.put(str, jSONObject2);
            return true;
        } catch (JSONException e) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putObject(): ").a(e.toString());
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + jSONObject2).a(v.i);
            return false;
        }
    }

    static boolean a(JSONObject jSONObject, String str, double d) {
        try {
            jSONObject.put(str, d);
            return true;
        } catch (JSONException unused) {
            v.a a2 = new v.a().a("JSON error in ADCJSON putDouble(): ");
            v.a a3 = a2.a(" with key: " + str);
            a3.a(" and value: " + d).a(v.i);
            return false;
        }
    }

    static void a(JSONArray jSONArray, Object obj) {
        jSONArray.put(obj);
    }

    static String[] a(JSONArray jSONArray) {
        String[] strArr = new String[jSONArray.length()];
        for (int i = 0; i < jSONArray.length(); i++) {
            strArr[i] = b(jSONArray, i);
        }
        return strArr;
    }

    static JSONArray a(String[] strArr) {
        JSONArray a2 = a();
        for (String b : strArr) {
            b(a2, b);
        }
        return a2;
    }

    static boolean a(JSONArray jSONArray, String str) {
        for (int i = 0; i < jSONArray.length(); i++) {
            if (b(jSONArray, i).equals(str)) {
                return true;
            }
        }
        return false;
    }

    static boolean a(JSONObject jSONObject, String str) {
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            if (str.equals(keys.next())) {
                return true;
            }
        }
        return false;
    }

    static JSONArray a(JSONArray jSONArray, String[] strArr, boolean z) {
        for (String str : strArr) {
            if (!z || !a(jSONArray, str)) {
                b(jSONArray, str);
            }
        }
        return jSONArray;
    }

    static JSONObject a(JSONObject jSONObject, JSONObject jSONObject2) {
        try {
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                jSONObject.put(next, jSONObject2.get(next));
            }
        } catch (JSONException unused) {
        }
        return jSONObject;
    }
}
