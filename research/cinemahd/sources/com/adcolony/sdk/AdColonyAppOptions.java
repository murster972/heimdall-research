package com.adcolony.sdk;

import android.content.Context;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import org.json.JSONArray;
import org.json.JSONObject;

public class AdColonyAppOptions {

    /* renamed from: a  reason: collision with root package name */
    private String f1165a = "";
    private String[] b;
    private JSONArray c = t.a();
    private JSONObject d = t.b();

    public AdColonyAppOptions() {
        b("google");
        if (a.e()) {
            i c2 = a.c();
            if (c2.B()) {
                a(c2.t().f1165a);
                a(c2.t().b);
            }
        }
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions a(String str) {
        if (str == null) {
            return this;
        }
        this.f1165a = str;
        t.a(this.d, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_APP_ID, str);
        return this;
    }

    /* access modifiers changed from: package-private */
    public JSONObject b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String[] c() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public JSONArray d() {
        return this.c;
    }

    public boolean e() {
        return t.c(this.d, "keep_screen_on");
    }

    public JSONObject f() {
        JSONObject b2 = t.b();
        t.a(b2, MediationMetaData.KEY_NAME, t.g(this.d, "mediation_network"));
        t.a(b2, MediationMetaData.KEY_VERSION, t.g(this.d, "mediation_network_version"));
        return b2;
    }

    public boolean g() {
        return t.c(this.d, "multi_window_enabled");
    }

    public JSONObject h() {
        JSONObject b2 = t.b();
        t.a(b2, MediationMetaData.KEY_NAME, t.g(this.d, "plugin"));
        t.a(b2, MediationMetaData.KEY_VERSION, t.g(this.d, "plugin_version"));
        return b2;
    }

    public AdColonyAppOptions b(String str) {
        a("origin_store", str);
        return this;
    }

    public AdColonyAppOptions c(String str) {
        a(ReportDBAdapter.ReportColumns.COLUMN_USER_ID, str);
        return this;
    }

    private void b(Context context) {
        a("bundle_id", l0.c(context));
    }

    public AdColonyAppOptions a(boolean z) {
        t.a(this.d, "keep_screen_on", z);
        return this;
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        b(context);
        if (t.a(this.d, "use_forced_controller")) {
            n0.O = t.c(this.d, "use_forced_controller");
        }
        if (t.a(this.d, "use_staging_launch_server") && t.c(this.d, "use_staging_launch_server")) {
            i.P = "https://adc3-launch-staging.adcolony.com/v4/launch";
        }
        String b2 = l0.b(context, "IABUSPrivacy_String");
        String b3 = l0.b(context, "IABTCF_TCString");
        int a2 = l0.a(context, "IABTCF_gdprApplies");
        if (b2 != null) {
            t.a(this.d, "ccpa_consent_string", b2);
        }
        if (b3 != null) {
            t.a(this.d, "gdpr_consent_string", b3);
        }
        boolean z = true;
        if (a2 == 0 || a2 == 1) {
            JSONObject jSONObject = this.d;
            if (a2 != 1) {
                z = false;
            }
            t.a(jSONObject, "gdpr_required", z);
        }
    }

    /* access modifiers changed from: package-private */
    public AdColonyAppOptions a(String... strArr) {
        if (strArr == null) {
            return this;
        }
        this.b = strArr;
        this.c = t.a();
        for (String b2 : strArr) {
            t.b(this.c, b2);
        }
        return this;
    }

    public AdColonyAppOptions a(String str, String str2) {
        t.a(this.d, str, str2);
        return this;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.f1165a;
    }
}
