package com.adcolony.sdk;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings;
import org.json.JSONObject;

class h extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private AudioManager f1226a;
    private AdColonyInterstitial b;

    h(Handler handler, AdColonyInterstitial adColonyInterstitial) {
        super(handler);
        Context b2 = a.b();
        if (b2 != null) {
            this.f1226a = (AudioManager) b2.getSystemService("audio");
            this.b = adColonyInterstitial;
            b2.getApplicationContext().getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this);
        }
    }

    /* access modifiers changed from: package-private */
    public void a() {
        Context b2 = a.b();
        if (b2 != null) {
            b2.getApplicationContext().getContentResolver().unregisterContentObserver(this);
        }
        this.b = null;
        this.f1226a = null;
    }

    public boolean deliverSelfNotifications() {
        return false;
    }

    public void onChange(boolean z) {
        AdColonyInterstitial adColonyInterstitial;
        if (this.f1226a != null && (adColonyInterstitial = this.b) != null && adColonyInterstitial.d() != null) {
            double streamVolume = (double) ((((float) this.f1226a.getStreamVolume(3)) / 15.0f) * 100.0f);
            JSONObject b2 = t.b();
            t.a(b2, "audio_percentage", streamVolume);
            t.a(b2, "ad_session_id", this.b.d().a());
            t.b(b2, "id", this.b.d().c());
            new y("AdContainer.on_audio_change", this.b.d().k(), b2).c();
        }
    }
}
