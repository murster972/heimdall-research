package com.adcolony.sdk;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class w {
    static final SimpleDateFormat e = new SimpleDateFormat("yyyyMMdd'T'HHmmss.SSSZ", Locale.US);
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Date f1363a;
    /* access modifiers changed from: private */
    public int b;
    protected String c;
    /* access modifiers changed from: private */
    public s d;

    static class a {

        /* renamed from: a  reason: collision with root package name */
        protected w f1364a = new w();

        a() {
        }

        /* access modifiers changed from: package-private */
        public a a(int i) {
            int unused = this.f1364a.b = i;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(s sVar) {
            s unused = this.f1364a.d = sVar;
            return this;
        }

        /* access modifiers changed from: package-private */
        public a a(String str) {
            this.f1364a.c = str;
            return this;
        }

        /* access modifiers changed from: package-private */
        public w a() {
            if (this.f1364a.f1363a == null) {
                Date unused = this.f1364a.f1363a = new Date(System.currentTimeMillis());
            }
            return this.f1364a;
        }
    }

    w() {
    }

    /* access modifiers changed from: package-private */
    public String b() {
        int i = this.b;
        if (i == -1) {
            return "Fatal";
        }
        if (i == 0) {
            return "Error";
        }
        if (i == 1) {
            return "Warn";
        }
        if (i != 2) {
            return i != 3 ? "UNKNOWN LOG LEVEL" : "Debug";
        }
        return "Info";
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return e.format(this.f1363a);
    }

    public String toString() {
        return d() + " " + b() + "/" + a().a() + ": " + c();
    }

    /* access modifiers changed from: package-private */
    public s a() {
        return this.d;
    }
}
