package com.adcolony.sdk;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.security.NetworkSecurityPolicy;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import com.adcolony.sdk.v;
import com.applovin.sdk.AppLovinEventTypes;
import com.vungle.warren.AdLoader;
import com.vungle.warren.model.AdvertisementDBAdapter;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import org.joda.time.DateTimeConstants;
import org.json.JSONArray;
import org.json.JSONObject;

@SuppressLint({"ObsoleteSdkInt"})
class k {

    /* renamed from: a  reason: collision with root package name */
    private String f1268a = "";
    /* access modifiers changed from: private */
    public String b;
    private final e c = new e();
    private boolean d;
    private JSONObject e = t.b();
    private String f = "android";
    private String g = "android_native";
    private String h = "";

    class a implements a0 {

        /* renamed from: com.adcolony.sdk.k$a$a  reason: collision with other inner class name */
        class C0005a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ y f1270a;

            C0005a(y yVar) {
                this.f1270a = yVar;
            }

            public void run() {
                try {
                    if (k.this.p() < 14) {
                        new c(this.f1270a, false).execute(new Void[0]);
                    } else {
                        new c(this.f1270a, false).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                } catch (RuntimeException unused) {
                    new v.a().a("Error retrieving device info, disabling AdColony.").a(v.i);
                    AdColony.d();
                } catch (StackOverflowError unused2) {
                    new v.a().a("StackOverflowError on info AsyncTask execution, disabling AdColony").a(v.i);
                    AdColony.d();
                }
            }
        }

        a() {
        }

        public void a(y yVar) {
            l0.a((Runnable) new C0005a(yVar));
        }
    }

    class b implements Runnable {

        class a implements Runnable {

            /* renamed from: a  reason: collision with root package name */
            final /* synthetic */ WebSettings f1272a;

            a(WebSettings webSettings) {
                this.f1272a = webSettings;
            }

            public void run() {
                String unused = k.this.b = this.f1272a.getUserAgentString();
                a.c().m().a(k.this.b);
            }
        }

        b() {
        }

        public void run() {
            Context b;
            if (k.this.b == null && (b = a.b()) != null) {
                try {
                    l0.f1291a.execute(new a(new WebView(b).getSettings()));
                } catch (RuntimeException e) {
                    v.a aVar = new v.a();
                    aVar.a(e.toString() + ": during WebView initialization.").a(" Disabling AdColony.").a(v.h);
                    String unused = k.this.b = "";
                    AdColony.d();
                }
            }
        }
    }

    k() {
    }

    /* access modifiers changed from: package-private */
    public int A() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) b2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.heightPixels;
    }

    /* access modifiers changed from: package-private */
    public int B() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) b2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.widthPixels;
    }

    /* access modifiers changed from: package-private */
    public String C() {
        return Locale.getDefault().getLanguage();
    }

    /* access modifiers changed from: package-private */
    public JSONObject D() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public boolean E() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public String F() {
        return "";
    }

    /* access modifiers changed from: package-private */
    public String G() {
        return Build.MANUFACTURER;
    }

    /* access modifiers changed from: package-private */
    public int H() {
        ActivityManager activityManager;
        Context b2 = a.b();
        if (b2 == null || (activityManager = (ActivityManager) b2.getSystemService("activity")) == null) {
            return 0;
        }
        return activityManager.getMemoryClass();
    }

    /* access modifiers changed from: package-private */
    public long I() {
        Runtime runtime = Runtime.getRuntime();
        return (runtime.totalMemory() - runtime.freeMemory()) / ((long) 1048576);
    }

    /* access modifiers changed from: package-private */
    public String J() {
        return Build.MODEL;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"SwitchIntDef"})
    public int a() {
        Context b2 = a.b();
        if (b2 == null) {
            return 2;
        }
        int i = b2.getResources().getConfiguration().orientation;
        if (i == 1) {
            return 0;
        }
        if (i != 2) {
            return 2;
        }
        return 1;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return Build.VERSION.RELEASE;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return "4.4.0";
    }

    /* access modifiers changed from: package-private */
    public void c(boolean z) {
        this.d = z;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        TelephonyManager telephonyManager;
        Context b2 = a.b();
        if (b2 == null || (telephonyManager = (TelephonyManager) b2.getSystemService("phone")) == null) {
            return "";
        }
        return telephonyManager.getSimCountryIso();
    }

    /* access modifiers changed from: package-private */
    public int e() {
        return TimeZone.getDefault().getOffset(15) / DateTimeConstants.MILLIS_PER_MINUTE;
    }

    /* access modifiers changed from: package-private */
    public String f() {
        return TimeZone.getDefault().getID();
    }

    /* access modifiers changed from: package-private */
    public String g() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public void h() {
        this.c.a(false);
        a.a("Device.get_info", (a0) new a());
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        DisplayMetrics displayMetrics = b2.getResources().getDisplayMetrics();
        float f2 = ((float) displayMetrics.widthPixels) / displayMetrics.xdpi;
        float f3 = ((float) displayMetrics.heightPixels) / displayMetrics.ydpi;
        if (Math.sqrt((double) ((f2 * f2) + (f3 * f3))) >= 6.0d) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    public void j() {
        l0.a((Runnable) new b());
    }

    /* access modifiers changed from: package-private */
    public String k() {
        return System.getProperty("os.arch").toLowerCase(Locale.ENGLISH);
    }

    /* access modifiers changed from: package-private */
    public String l() {
        return this.f1268a;
    }

    /* access modifiers changed from: package-private */
    public String m() {
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        return Settings.Secure.getString(b2.getContentResolver(), "advertising_id");
    }

    /* access modifiers changed from: package-private */
    public boolean n() {
        Context b2 = a.b();
        if (b2 == null) {
            return false;
        }
        try {
            if (Settings.Secure.getInt(b2.getContentResolver(), "limit_ad_tracking") != 0) {
                return true;
            }
            return false;
        } catch (Settings.SettingNotFoundException unused) {
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"HardwareIds"})
    public String o() {
        Context b2 = a.b();
        if (b2 == null) {
            return "";
        }
        return Settings.Secure.getString(b2.getContentResolver(), "android_id");
    }

    /* access modifiers changed from: package-private */
    public int p() {
        return Build.VERSION.SDK_INT;
    }

    /* access modifiers changed from: package-private */
    public double q() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0.0d;
        }
        try {
            Intent registerReceiver = b2.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
            if (registerReceiver == null) {
                return 0.0d;
            }
            int intExtra = registerReceiver.getIntExtra(AppLovinEventTypes.USER_COMPLETED_LEVEL, -1);
            int intExtra2 = registerReceiver.getIntExtra("scale", -1);
            if (intExtra < 0 || intExtra2 < 0) {
                return 0.0d;
            }
            return ((double) intExtra) / ((double) intExtra2);
        } catch (IllegalArgumentException unused) {
            return 0.0d;
        }
    }

    /* access modifiers changed from: package-private */
    public String r() {
        Context b2 = a.b();
        String str = "";
        if (b2 == null) {
            return str;
        }
        TelephonyManager telephonyManager = (TelephonyManager) b2.getSystemService("phone");
        if (telephonyManager != null) {
            str = telephonyManager.getNetworkOperatorName();
        }
        return str.length() == 0 ? "unknown" : str;
    }

    /* access modifiers changed from: package-private */
    public boolean s() {
        return Build.VERSION.SDK_INT < 23 || NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted();
    }

    /* access modifiers changed from: package-private */
    public String t() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public String u() {
        return Locale.getDefault().getCountry();
    }

    /* access modifiers changed from: package-private */
    public int v() {
        TimeZone timeZone = TimeZone.getDefault();
        if (!timeZone.inDaylightTime(new Date())) {
            return 0;
        }
        return timeZone.getDSTSavings() / DateTimeConstants.MILLIS_PER_MINUTE;
    }

    /* access modifiers changed from: package-private */
    public boolean w() {
        int i;
        Context b2 = a.b();
        if (b2 == null || Build.VERSION.SDK_INT < 29 || (i = b2.getResources().getConfiguration().uiMode & 48) == 16 || i != 32) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public float x() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0.0f;
        }
        return b2.getResources().getDisplayMetrics().density;
    }

    /* access modifiers changed from: package-private */
    public String y() {
        return i() ? "tablet" : "phone";
    }

    /* access modifiers changed from: package-private */
    public int z() {
        Context b2 = a.b();
        if (b2 == null) {
            return 0;
        }
        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager windowManager = (WindowManager) b2.getSystemService("window");
        if (windowManager != null) {
            windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        }
        return displayMetrics.densityDpi;
    }

    private static class c extends AsyncTask<Void, Void, JSONObject> {

        /* renamed from: a  reason: collision with root package name */
        private y f1273a;
        private boolean b;

        c(y yVar, boolean z) {
            this.f1273a = yVar;
            this.b = z;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public JSONObject doInBackground(Void... voidArr) {
            if (Build.VERSION.SDK_INT < 14) {
                return null;
            }
            return a.c().k().a(true);
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(JSONObject jSONObject) {
            if (this.b) {
                new y("Device.update_info", 1, jSONObject).c();
            } else {
                this.f1273a.a(jSONObject).c();
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
        this.c.a(z);
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.h = str;
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        this.e = jSONObject;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.f1268a = str;
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(boolean z) {
        JSONObject b2 = t.b();
        i c2 = a.c();
        t.a(b2, "carrier_name", r());
        t.a(b2, "data_path", a.c().x().b());
        t.b(b2, "device_api", p());
        t.b(b2, "display_width", B());
        t.b(b2, "display_height", A());
        t.b(b2, "screen_width", B());
        t.b(b2, "screen_height", A());
        t.b(b2, "display_dpi", z());
        t.a(b2, "device_type", y());
        t.a(b2, "locale_language_code", C());
        t.a(b2, "ln", C());
        t.a(b2, "locale_country_code", u());
        t.a(b2, "locale", u());
        t.a(b2, "mac_address", F());
        t.a(b2, "manufacturer", G());
        t.a(b2, "device_brand", G());
        t.a(b2, "media_path", a.c().x().c());
        t.a(b2, "temp_storage_path", a.c().x().d());
        t.b(b2, "memory_class", H());
        t.b(b2, "network_speed", 20);
        t.a(b2, "memory_used_mb", I());
        t.a(b2, "model", J());
        t.a(b2, "device_model", J());
        t.a(b2, "sdk_type", this.g);
        t.a(b2, "sdk_version", c());
        t.a(b2, "network_type", c2.q().a());
        t.a(b2, "os_version", b());
        t.a(b2, "os_name", this.f);
        t.a(b2, "platform", this.f);
        t.a(b2, "arch", k());
        t.a(b2, ReportDBAdapter.ReportColumns.COLUMN_USER_ID, t.g(c2.t().b(), ReportDBAdapter.ReportColumns.COLUMN_USER_ID));
        t.a(b2, AdvertisementDBAdapter.AdvertisementColumns.COLUMN_APP_ID, c2.t().a());
        t.a(b2, "app_bundle_name", l0.b());
        t.a(b2, "app_bundle_version", l0.c());
        t.a(b2, "battery_level", q());
        t.a(b2, "cell_service_country_code", d());
        t.a(b2, "timezone_ietf", f());
        t.b(b2, "timezone_gmt_m", e());
        t.b(b2, "timezone_dst_m", v());
        t.a(b2, "launch_metadata", D());
        t.a(b2, "controller_version", c2.f());
        t.b(b2, "current_orientation", a());
        t.a(b2, "cleartext_permitted", s());
        t.a(b2, "density", (double) x());
        t.a(b2, "dark_mode", w());
        JSONArray a2 = t.a();
        if (l0.c("com.android.vending")) {
            a2.put("google");
        }
        if (l0.c("com.amazon.venezia")) {
            a2.put("amazon");
        }
        t.a(b2, "available_stores", a2);
        t.a(b2, "permissions", l0.d(a.b()));
        if (!this.c.a() && z) {
            this.c.a((long) AdLoader.RETRY_DELAY);
        }
        t.a(b2, "advertiser_id", l());
        t.a(b2, "limit_tracking", E());
        if (l() == null || l().equals("")) {
            t.a(b2, "android_id_sha1", l0.b(o()));
        }
        return b2;
    }
}
