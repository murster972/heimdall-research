package com.adcolony.sdk;

import android.content.Context;
import android.content.Intent;
import com.adcolony.sdk.v;
import org.json.JSONObject;

public class AdColonyInterstitial {

    /* renamed from: a  reason: collision with root package name */
    private AdColonyInterstitialListener f1168a;
    private c b;
    private AdColonyAdOptions c;
    private d0 d;
    private int e;
    private String f;
    private String g;
    private String h;
    private String i;
    private boolean j;
    private boolean k;

    AdColonyInterstitial(String str, AdColonyInterstitialListener adColonyInterstitialListener, String str2) {
        this.f1168a = adColonyInterstitialListener;
        this.h = str2;
        this.f = str;
    }

    /* access modifiers changed from: package-private */
    public boolean a(AdColonyZone adColonyZone) {
        if (adColonyZone != null) {
            if (adColonyZone.b() <= 1) {
                return false;
            }
            if (adColonyZone.a() == 0) {
                adColonyZone.a(adColonyZone.b() - 1);
                return false;
            }
            adColonyZone.a(adColonyZone.a() - 1);
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.f;
    }

    /* access modifiers changed from: package-private */
    public void b(boolean z) {
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.i;
    }

    /* access modifiers changed from: package-private */
    public void c(String str) {
    }

    /* access modifiers changed from: package-private */
    public c d() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public d0 e() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public int f() {
        return this.e;
    }

    public AdColonyInterstitialListener g() {
        return this.f1168a;
    }

    public String h() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.d != null;
    }

    public boolean j() {
        return this.j || this.k;
    }

    /* access modifiers changed from: package-private */
    public boolean k() {
        Context b2 = a.b();
        if (b2 == null || !a.e()) {
            return false;
        }
        a.c().d(true);
        a.c().a(this.b);
        a.c().a(this);
        l0.a(new Intent(b2, AdColonyInterstitialActivity.class));
        this.k = true;
        return true;
    }

    public boolean l() {
        if (!a.e()) {
            return false;
        }
        i c2 = a.c();
        if (this.k) {
            new v.a().a("This ad object has already been shown. Please request a new ad ").a("via AdColony.requestInterstitial.").a(v.f);
            return false;
        } else if (this.j) {
            new v.a().a("This ad object has expired. Please request a new ad via AdColony").a(".requestInterstitial.").a(v.f);
            return false;
        } else if (c2.d()) {
            new v.a().a("Can not show ad while an interstitial is already active.").a(v.f);
            return false;
        } else if (a(c2.A().get(this.h))) {
            return false;
        } else {
            JSONObject b2 = t.b();
            t.a(b2, "zone_id", this.h);
            t.b(b2, "type", 0);
            t.a(b2, "id", this.f);
            AdColonyAdOptions adColonyAdOptions = this.c;
            if (adColonyAdOptions != null) {
                t.a(b2, "pre_popup", adColonyAdOptions.f1160a);
                t.a(b2, "post_popup", this.c.b);
            }
            AdColonyZone adColonyZone = c2.A().get(this.h);
            if (adColonyZone != null && adColonyZone.d() && c2.v() == null) {
                new v.a().a("Rewarded ad: show() called with no reward listener set.").a(v.f);
            }
            new y("AdSession.launch_ad_unit", 1, b2).c();
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public void b(String str) {
        this.i = str;
    }

    public void a(AdColonyInterstitialListener adColonyInterstitialListener) {
        this.f1168a = adColonyInterstitialListener;
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        if (jSONObject.length() > 0) {
            this.d = new d0(jSONObject, this.f);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(c cVar) {
        this.b = cVar;
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.j = z;
    }

    /* access modifiers changed from: package-private */
    public void a(AdColonyAdOptions adColonyAdOptions) {
        this.c = adColonyAdOptions;
    }

    /* access modifiers changed from: package-private */
    public void a(int i2) {
        this.e = i2;
    }

    /* access modifiers changed from: package-private */
    public String a() {
        String str = this.g;
        return str == null ? "" : str;
    }

    /* access modifiers changed from: package-private */
    public void a(String str) {
        this.g = str;
    }
}
