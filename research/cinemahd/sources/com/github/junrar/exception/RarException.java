package com.github.junrar.exception;

public class RarException extends Exception {
    private static final long serialVersionUID = 1;
    private RarExceptionType type;

    public enum RarExceptionType {
        notImplementedYet,
        crcError,
        notRarArchive,
        badRarArchive,
        unkownError,
        headerNotInArchive,
        wrongHeaderType,
        ioError,
        rarEncryptedException
    }
}
