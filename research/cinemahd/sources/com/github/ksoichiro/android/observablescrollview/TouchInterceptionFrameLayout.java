package com.github.ksoichiro.android.observablescrollview;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.FrameLayout;

public class TouchInterceptionFrameLayout extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private boolean f3135a;
    private boolean b;
    private boolean c;
    private boolean d;
    private PointF e;
    private MotionEvent f;
    private TouchInterceptionListener g;

    public interface TouchInterceptionListener {
        void a(MotionEvent motionEvent);

        void a(MotionEvent motionEvent, float f, float f2);

        boolean a(MotionEvent motionEvent, boolean z, float f, float f2);

        void b(MotionEvent motionEvent);
    }

    public TouchInterceptionFrameLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    private MotionEvent a(MotionEvent motionEvent, int i) {
        MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
        obtainNoHistory.setAction(i);
        return obtainNoHistory;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.g == null) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.e = new PointF(motionEvent.getX(), motionEvent.getY());
            this.f = MotionEvent.obtainNoHistory(motionEvent);
            this.b = true;
            this.f3135a = this.g.a(motionEvent, false, 0.0f, 0.0f);
            boolean z = this.f3135a;
            this.c = z;
            this.d = false;
            return z;
        } else if (actionMasked != 2) {
            return false;
        } else {
            if (this.e == null) {
                this.e = new PointF(motionEvent.getX(), motionEvent.getY());
            }
            this.f3135a = this.g.a(motionEvent, true, motionEvent.getX() - this.e.x, motionEvent.getY() - this.e.y);
            return this.f3135a;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0012, code lost:
        if (r0 != 3) goto L_0x00f9;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r7) {
        /*
            r6 = this;
            com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout$TouchInterceptionListener r0 = r6.g
            if (r0 == 0) goto L_0x00f9
            int r0 = r7.getActionMasked()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x00ea
            if (r0 == r2) goto L_0x00b4
            r3 = 2
            r4 = 3
            if (r0 == r3) goto L_0x0016
            if (r0 == r4) goto L_0x00b4
            goto L_0x00f9
        L_0x0016:
            android.graphics.PointF r0 = r6.e
            if (r0 != 0) goto L_0x0029
            android.graphics.PointF r0 = new android.graphics.PointF
            float r3 = r7.getX()
            float r5 = r7.getY()
            r0.<init>(r3, r5)
            r6.e = r0
        L_0x0029:
            float r0 = r7.getX()
            android.graphics.PointF r3 = r6.e
            float r3 = r3.x
            float r0 = r0 - r3
            float r3 = r7.getY()
            android.graphics.PointF r5 = r6.e
            float r5 = r5.y
            float r3 = r3 - r5
            com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout$TouchInterceptionListener r5 = r6.g
            boolean r5 = r5.a(r7, r2, r0, r3)
            r6.f3135a = r5
            boolean r5 = r6.f3135a
            if (r5 == 0) goto L_0x008b
            boolean r5 = r6.c
            if (r5 != 0) goto L_0x0074
            r6.c = r2
            android.view.MotionEvent r0 = r6.f
            android.view.MotionEvent r0 = android.view.MotionEvent.obtainNoHistory(r0)
            float r3 = r7.getX()
            float r5 = r7.getY()
            r0.setLocation(r3, r5)
            com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout$TouchInterceptionListener r3 = r6.g
            r3.b(r0)
            android.graphics.PointF r0 = new android.graphics.PointF
            float r3 = r7.getX()
            float r5 = r7.getY()
            r0.<init>(r3, r5)
            r6.e = r0
            r0 = 0
            r3 = 0
        L_0x0074:
            boolean r5 = r6.d
            if (r5 != 0) goto L_0x0083
            r6.d = r2
            android.view.MotionEvent r4 = r6.a((android.view.MotionEvent) r7, (int) r4)
            android.view.MotionEvent[] r1 = new android.view.MotionEvent[r1]
            r6.a((android.view.MotionEvent) r4, (android.view.MotionEvent[]) r1)
        L_0x0083:
            com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout$TouchInterceptionListener r1 = r6.g
            r1.a(r7, r0, r3)
            r6.b = r2
            return r2
        L_0x008b:
            boolean r0 = r6.b
            if (r0 == 0) goto L_0x00aa
            r6.b = r1
            android.view.MotionEvent r0 = r6.f
            android.view.MotionEvent r0 = android.view.MotionEvent.obtainNoHistory(r0)
            float r3 = r7.getX()
            float r4 = r7.getY()
            r0.setLocation(r3, r4)
            android.view.MotionEvent[] r2 = new android.view.MotionEvent[r2]
            r2[r1] = r0
            r6.a((android.view.MotionEvent) r7, (android.view.MotionEvent[]) r2)
            goto L_0x00af
        L_0x00aa:
            android.view.MotionEvent[] r0 = new android.view.MotionEvent[r1]
            r6.a((android.view.MotionEvent) r7, (android.view.MotionEvent[]) r0)
        L_0x00af:
            r6.c = r1
            r6.d = r1
            goto L_0x00f9
        L_0x00b4:
            r6.c = r1
            boolean r0 = r6.f3135a
            if (r0 == 0) goto L_0x00bf
            com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout$TouchInterceptionListener r0 = r6.g
            r0.a(r7)
        L_0x00bf:
            boolean r0 = r6.d
            if (r0 != 0) goto L_0x00e9
            r6.d = r2
            boolean r0 = r6.b
            if (r0 == 0) goto L_0x00e4
            r6.b = r1
            android.view.MotionEvent r0 = r6.f
            android.view.MotionEvent r0 = android.view.MotionEvent.obtainNoHistory(r0)
            float r3 = r7.getX()
            float r4 = r7.getY()
            r0.setLocation(r3, r4)
            android.view.MotionEvent[] r3 = new android.view.MotionEvent[r2]
            r3[r1] = r0
            r6.a((android.view.MotionEvent) r7, (android.view.MotionEvent[]) r3)
            goto L_0x00e9
        L_0x00e4:
            android.view.MotionEvent[] r0 = new android.view.MotionEvent[r1]
            r6.a((android.view.MotionEvent) r7, (android.view.MotionEvent[]) r0)
        L_0x00e9:
            return r2
        L_0x00ea:
            boolean r0 = r6.f3135a
            if (r0 == 0) goto L_0x00f9
            com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout$TouchInterceptionListener r0 = r6.g
            r0.b(r7)
            android.view.MotionEvent[] r0 = new android.view.MotionEvent[r1]
            r6.a((android.view.MotionEvent) r7, (android.view.MotionEvent[]) r0)
            return r2
        L_0x00f9:
            boolean r7 = super.onTouchEvent(r7)
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.github.ksoichiro.android.observablescrollview.TouchInterceptionFrameLayout.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setScrollInterceptionListener(TouchInterceptionListener touchInterceptionListener) {
        this.g = touchInterceptionListener;
    }

    private void a(MotionEvent motionEvent, MotionEvent... motionEventArr) {
        if (motionEvent != null) {
            for (int childCount = getChildCount() - 1; childCount >= 0; childCount--) {
                View childAt = getChildAt(childCount);
                if (childAt != null) {
                    Rect rect = new Rect();
                    childAt.getHitRect(rect);
                    MotionEvent obtainNoHistory = MotionEvent.obtainNoHistory(motionEvent);
                    if (!rect.contains((int) obtainNoHistory.getX(), (int) obtainNoHistory.getY())) {
                        continue;
                    } else {
                        float f2 = (float) (-childAt.getLeft());
                        float f3 = (float) (-childAt.getTop());
                        int i = 0;
                        if (motionEventArr != null) {
                            int length = motionEventArr.length;
                            boolean z = false;
                            while (i < length) {
                                MotionEvent motionEvent2 = motionEventArr[i];
                                if (motionEvent2 != null) {
                                    MotionEvent obtainNoHistory2 = MotionEvent.obtainNoHistory(motionEvent2);
                                    obtainNoHistory2.offsetLocation(f2, f3);
                                    z |= childAt.dispatchTouchEvent(obtainNoHistory2);
                                }
                                i++;
                            }
                            i = z;
                        }
                        obtainNoHistory.offsetLocation(f2, f3);
                        if (childAt.dispatchTouchEvent(obtainNoHistory) || i != false) {
                            return;
                        }
                    }
                }
            }
        }
    }
}
