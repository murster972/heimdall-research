package com.github.ksoichiro.android.observablescrollview;

import android.content.Context;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.GridView;

public class ObservableGridView extends GridView implements Scrollable {

    /* renamed from: a  reason: collision with root package name */
    private int f3117a;
    private int b = -1;
    private int c;
    private int d;
    private int e;
    private SparseIntArray f;
    private ObservableScrollViewCallbacks g;
    private ScrollState h;
    private boolean i;
    private boolean j;
    private boolean k;
    private MotionEvent l;
    private ViewGroup m;
    /* access modifiers changed from: private */
    public AbsListView.OnScrollListener n;
    private AbsListView.OnScrollListener o = new AbsListView.OnScrollListener() {
        public void onScroll(AbsListView absListView, int i, int i2, int i3) {
            if (ObservableGridView.this.n != null) {
                ObservableGridView.this.n.onScroll(absListView, i, i2, i3);
            }
            ObservableGridView.this.b();
        }

        public void onScrollStateChanged(AbsListView absListView, int i) {
            if (ObservableGridView.this.n != null) {
                ObservableGridView.this.n.onScrollStateChanged(absListView, i);
            }
        }
    };

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f3120a;
        int b;
        int c;
        int d;
        int e;
        SparseIntArray f;

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f3120a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d);
            parcel.writeInt(this.e);
            SparseIntArray sparseIntArray = this.f;
            int size = sparseIntArray == null ? 0 : sparseIntArray.size();
            parcel.writeInt(size);
            if (size > 0) {
                for (int i2 = 0; i2 < size; i2++) {
                    parcel.writeInt(this.f.keyAt(i2));
                    parcel.writeInt(this.f.valueAt(i2));
                }
            }
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
            this.b = -1;
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.b = -1;
            this.f3120a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            this.d = parcel.readInt();
            this.e = parcel.readInt();
            this.f = new SparseIntArray();
            int readInt = parcel.readInt();
            if (readInt > 0) {
                for (int i = 0; i < readInt; i++) {
                    this.f.put(parcel.readInt(), parcel.readInt());
                }
            }
        }
    }

    public ObservableGridView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        a();
    }

    private int getNumColumnsCompat() {
        int measuredWidth;
        if (Build.VERSION.SDK_INT >= 11) {
            return getNumColumns();
        }
        int i2 = 0;
        if (getChildCount() > 0 && (measuredWidth = getChildAt(0).getMeasuredWidth()) > 0) {
            i2 = getWidth() / measuredWidth;
        }
        if (i2 > 0) {
            return i2;
        }
        return -1;
    }

    public int getCurrentScrollY() {
        return this.e;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.g != null && motionEvent.getActionMasked() == 0) {
            this.j = true;
            this.i = true;
            this.g.d();
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        this.f3117a = savedState.f3120a;
        this.b = savedState.b;
        this.c = savedState.c;
        this.d = savedState.d;
        this.e = savedState.e;
        this.f = savedState.f;
        super.onRestoreInstanceState(savedState.getSuperState());
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f3120a = this.f3117a;
        savedState.b = this.b;
        savedState.c = this.c;
        savedState.d = this.d;
        savedState.e = this.e;
        savedState.f = this.f;
        return savedState;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if (r0 != 3) goto L_0x0094;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r9) {
        /*
            r8 = this;
            com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks r0 = r8.g
            if (r0 == 0) goto L_0x0094
            int r0 = r9.getActionMasked()
            r1 = 1
            r2 = 0
            if (r0 == r1) goto L_0x0089
            r3 = 2
            if (r0 == r3) goto L_0x0014
            r1 = 3
            if (r0 == r1) goto L_0x0089
            goto L_0x0094
        L_0x0014:
            android.view.MotionEvent r0 = r8.l
            if (r0 != 0) goto L_0x001a
            r8.l = r9
        L_0x001a:
            float r0 = r9.getY()
            android.view.MotionEvent r3 = r8.l
            float r3 = r3.getY()
            float r0 = r0 - r3
            android.view.MotionEvent r3 = android.view.MotionEvent.obtainNoHistory(r9)
            r8.l = r3
            int r3 = r8.getCurrentScrollY()
            float r3 = (float) r3
            float r3 = r3 - r0
            r0 = 0
            int r3 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r3 > 0) goto L_0x0094
            boolean r3 = r8.k
            if (r3 == 0) goto L_0x003b
            return r2
        L_0x003b:
            android.view.ViewGroup r3 = r8.m
            if (r3 != 0) goto L_0x0045
            android.view.ViewParent r3 = r8.getParent()
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
        L_0x0045:
            r4 = 0
            r5 = 0
            r0 = r8
        L_0x0048:
            if (r0 == 0) goto L_0x0069
            if (r0 == r3) goto L_0x0069
            int r6 = r0.getLeft()
            int r7 = r0.getScrollX()
            int r6 = r6 - r7
            float r6 = (float) r6
            float r4 = r4 + r6
            int r6 = r0.getTop()
            int r7 = r0.getScrollY()
            int r6 = r6 - r7
            float r6 = (float) r6
            float r5 = r5 + r6
            android.view.ViewParent r0 = r0.getParent()
            android.view.View r0 = (android.view.View) r0
            goto L_0x0048
        L_0x0069:
            android.view.MotionEvent r0 = android.view.MotionEvent.obtainNoHistory(r9)
            r0.offsetLocation(r4, r5)
            boolean r4 = r3.onInterceptTouchEvent(r0)
            if (r4 == 0) goto L_0x0084
            r8.k = r1
            r0.setAction(r2)
            com.github.ksoichiro.android.observablescrollview.ObservableGridView$2 r9 = new com.github.ksoichiro.android.observablescrollview.ObservableGridView$2
            r9.<init>(r8, r3, r0)
            r8.post(r9)
            return r2
        L_0x0084:
            boolean r9 = super.onTouchEvent(r9)
            return r9
        L_0x0089:
            r8.k = r2
            r8.j = r2
            com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks r0 = r8.g
            com.github.ksoichiro.android.observablescrollview.ScrollState r1 = r8.h
            r0.a(r1)
        L_0x0094:
            boolean r9 = super.onTouchEvent(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.github.ksoichiro.android.observablescrollview.ObservableGridView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setOnScrollListener(AbsListView.OnScrollListener onScrollListener) {
        this.n = onScrollListener;
    }

    public void setScrollViewCallbacks(ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        this.g = observableScrollViewCallbacks;
    }

    public void setTouchInterceptionViewGroup(ViewGroup viewGroup) {
        this.m = viewGroup;
    }

    private void a() {
        this.f = new SparseIntArray();
        super.setOnScrollListener(this.o);
    }

    /* access modifiers changed from: private */
    public void b() {
        int i2;
        int i3;
        if (this.g != null && getChildCount() > 0) {
            int firstVisiblePosition = getFirstVisiblePosition();
            int firstVisiblePosition2 = getFirstVisiblePosition();
            int i4 = 0;
            while (firstVisiblePosition2 <= getLastVisiblePosition()) {
                if ((this.f.indexOfKey(firstVisiblePosition2) < 0 || getChildAt(i4).getHeight() != this.f.get(firstVisiblePosition2)) && firstVisiblePosition2 % getNumColumnsCompat() == 0) {
                    this.f.put(firstVisiblePosition2, getChildAt(i4).getHeight());
                }
                firstVisiblePosition2++;
                i4++;
            }
            View childAt = getChildAt(0);
            if (childAt != null) {
                int i5 = this.f3117a;
                if (i5 < firstVisiblePosition) {
                    if (firstVisiblePosition - i5 != 1) {
                        i3 = 0;
                        for (int i6 = firstVisiblePosition - 1; i6 > this.f3117a; i6--) {
                            if (this.f.indexOfKey(i6) > 0) {
                                i3 += this.f.get(i6);
                            }
                        }
                    } else {
                        i3 = 0;
                    }
                    this.c += this.b + i3;
                    this.b = childAt.getHeight();
                } else if (firstVisiblePosition < i5) {
                    if (i5 - firstVisiblePosition != 1) {
                        i2 = 0;
                        for (int i7 = i5 - 1; i7 > firstVisiblePosition; i7--) {
                            if (this.f.indexOfKey(i7) > 0) {
                                i2 += this.f.get(i7);
                            }
                        }
                    } else {
                        i2 = 0;
                    }
                    this.c -= childAt.getHeight() + i2;
                    this.b = childAt.getHeight();
                } else if (firstVisiblePosition == 0) {
                    this.b = childAt.getHeight();
                }
                if (this.b < 0) {
                    this.b = 0;
                }
                this.e = this.c - childAt.getTop();
                this.f3117a = firstVisiblePosition;
                this.g.a(this.e, this.i, this.j);
                if (this.i) {
                    this.i = false;
                }
                int i8 = this.d;
                int i9 = this.e;
                if (i8 < i9) {
                    this.h = ScrollState.UP;
                } else if (i9 < i8) {
                    this.h = ScrollState.DOWN;
                } else {
                    this.h = ScrollState.STOP;
                }
                this.d = this.e;
            }
        }
    }
}
