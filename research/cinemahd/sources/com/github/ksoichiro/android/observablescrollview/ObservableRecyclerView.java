package com.github.ksoichiro.android.observablescrollview;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.SparseIntArray;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;

public class ObservableRecyclerView extends RecyclerView implements Scrollable {

    /* renamed from: a  reason: collision with root package name */
    private int f3125a;
    private int b = -1;
    private int c;
    private int d;
    private int e;
    private SparseIntArray f;
    private ObservableScrollViewCallbacks g;
    private ScrollState h;
    private boolean i;
    private boolean j;
    private boolean k;
    private MotionEvent l;
    private ViewGroup m;

    public ObservableRecyclerView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        b();
    }

    private void b() {
        this.f = new SparseIntArray();
    }

    public int getCurrentScrollY() {
        return this.e;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.g != null && motionEvent.getActionMasked() == 0) {
            this.j = true;
            this.i = true;
            this.g.d();
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        this.f3125a = savedState.f3127a;
        this.b = savedState.b;
        this.c = savedState.c;
        this.d = savedState.d;
        this.e = savedState.e;
        this.f = savedState.f;
        super.onRestoreInstanceState(savedState.a());
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f3127a = this.f3125a;
        savedState.b = this.b;
        savedState.c = this.c;
        savedState.d = this.d;
        savedState.e = this.e;
        savedState.f = this.f;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        super.onScrollChanged(i2, i3, i4, i5);
        if (this.g != null && getChildCount() > 0) {
            int childPosition = getChildPosition(getChildAt(0));
            int childPosition2 = getChildPosition(getChildAt(getChildCount() - 1));
            int i10 = childPosition;
            int i11 = 0;
            while (i10 <= childPosition2) {
                if (this.f.indexOfKey(i10) < 0 || getChildAt(i11).getHeight() != this.f.get(i10)) {
                    this.f.put(i10, getChildAt(i11).getHeight());
                }
                i10++;
                i11++;
            }
            View childAt = getChildAt(0);
            if (childAt != null) {
                int i12 = this.f3125a;
                if (i12 < childPosition) {
                    if (childPosition - i12 != 1) {
                        i8 = 0;
                        for (int i13 = childPosition - 1; i13 > this.f3125a; i13--) {
                            if (this.f.indexOfKey(i13) > 0) {
                                i9 = this.f.get(i13);
                            } else {
                                i9 = childAt.getHeight();
                            }
                            i8 += i9;
                        }
                    } else {
                        i8 = 0;
                    }
                    this.c += this.b + i8;
                    this.b = childAt.getHeight();
                } else if (childPosition < i12) {
                    if (i12 - childPosition != 1) {
                        i6 = 0;
                        for (int i14 = i12 - 1; i14 > childPosition; i14--) {
                            if (this.f.indexOfKey(i14) > 0) {
                                i7 = this.f.get(i14);
                            } else {
                                i7 = childAt.getHeight();
                            }
                            i6 += i7;
                        }
                    } else {
                        i6 = 0;
                    }
                    this.c -= childAt.getHeight() + i6;
                    this.b = childAt.getHeight();
                } else if (childPosition == 0) {
                    this.b = childAt.getHeight();
                    this.c = 0;
                }
                if (this.b < 0) {
                    this.b = 0;
                }
                this.e = this.c - childAt.getTop();
                this.f3125a = childPosition;
                this.g.a(this.e, this.i, this.j);
                if (this.i) {
                    this.i = false;
                }
                int i15 = this.d;
                int i16 = this.e;
                if (i15 < i16) {
                    this.h = ScrollState.UP;
                } else if (i16 < i15) {
                    this.h = ScrollState.DOWN;
                } else {
                    this.h = ScrollState.STOP;
                }
                this.d = this.e;
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0010, code lost:
        if (r0 != 3) goto L_0x0094;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r9) {
        /*
            r8 = this;
            com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks r0 = r8.g
            if (r0 == 0) goto L_0x0094
            int r0 = r9.getActionMasked()
            r1 = 1
            r2 = 0
            if (r0 == r1) goto L_0x0089
            r3 = 2
            if (r0 == r3) goto L_0x0014
            r1 = 3
            if (r0 == r1) goto L_0x0089
            goto L_0x0094
        L_0x0014:
            android.view.MotionEvent r0 = r8.l
            if (r0 != 0) goto L_0x001a
            r8.l = r9
        L_0x001a:
            float r0 = r9.getY()
            android.view.MotionEvent r3 = r8.l
            float r3 = r3.getY()
            float r0 = r0 - r3
            android.view.MotionEvent r3 = android.view.MotionEvent.obtainNoHistory(r9)
            r8.l = r3
            int r3 = r8.getCurrentScrollY()
            float r3 = (float) r3
            float r3 = r3 - r0
            r0 = 0
            int r3 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r3 > 0) goto L_0x0094
            boolean r3 = r8.k
            if (r3 == 0) goto L_0x003b
            return r2
        L_0x003b:
            android.view.ViewGroup r3 = r8.m
            if (r3 != 0) goto L_0x0045
            android.view.ViewParent r3 = r8.getParent()
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
        L_0x0045:
            r4 = 0
            r5 = 0
            r0 = r8
        L_0x0048:
            if (r0 == 0) goto L_0x0069
            if (r0 == r3) goto L_0x0069
            int r6 = r0.getLeft()
            int r7 = r0.getScrollX()
            int r6 = r6 - r7
            float r6 = (float) r6
            float r4 = r4 + r6
            int r6 = r0.getTop()
            int r7 = r0.getScrollY()
            int r6 = r6 - r7
            float r6 = (float) r6
            float r5 = r5 + r6
            android.view.ViewParent r0 = r0.getParent()
            android.view.View r0 = (android.view.View) r0
            goto L_0x0048
        L_0x0069:
            android.view.MotionEvent r0 = android.view.MotionEvent.obtainNoHistory(r9)
            r0.offsetLocation(r4, r5)
            boolean r4 = r3.onInterceptTouchEvent(r0)
            if (r4 == 0) goto L_0x0084
            r8.k = r1
            r0.setAction(r2)
            com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView$1 r9 = new com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView$1
            r9.<init>(r8, r3, r0)
            r8.post(r9)
            return r2
        L_0x0084:
            boolean r9 = super.onTouchEvent(r9)
            return r9
        L_0x0089:
            r8.k = r2
            r8.j = r2
            com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks r0 = r8.g
            com.github.ksoichiro.android.observablescrollview.ScrollState r1 = r8.h
            r0.a(r1)
        L_0x0094:
            boolean r9 = super.onTouchEvent(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.github.ksoichiro.android.observablescrollview.ObservableRecyclerView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setScrollViewCallbacks(ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        this.g = observableScrollViewCallbacks;
    }

    public void setTouchInterceptionViewGroup(ViewGroup viewGroup) {
        this.m = viewGroup;
    }

    static class SavedState implements Parcelable {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        public static final SavedState h = new SavedState() {
        };

        /* renamed from: a  reason: collision with root package name */
        int f3127a;
        int b;
        int c;
        int d;
        int e;
        SparseIntArray f;
        Parcelable g;

        public Parcelable a() {
            return this.g;
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeParcelable(this.g, i);
            parcel.writeInt(this.f3127a);
            parcel.writeInt(this.b);
            parcel.writeInt(this.c);
            parcel.writeInt(this.d);
            parcel.writeInt(this.e);
            SparseIntArray sparseIntArray = this.f;
            int size = sparseIntArray == null ? 0 : sparseIntArray.size();
            parcel.writeInt(size);
            if (size > 0) {
                for (int i2 = 0; i2 < size; i2++) {
                    parcel.writeInt(this.f.keyAt(i2));
                    parcel.writeInt(this.f.valueAt(i2));
                }
            }
        }

        private SavedState() {
            this.b = -1;
            this.g = null;
        }

        SavedState(Parcelable parcelable) {
            this.b = -1;
            this.g = parcelable == h ? null : parcelable;
        }

        private SavedState(Parcel parcel) {
            this.b = -1;
            Parcelable readParcelable = parcel.readParcelable(RecyclerView.class.getClassLoader());
            this.g = readParcelable == null ? h : readParcelable;
            this.f3127a = parcel.readInt();
            this.b = parcel.readInt();
            this.c = parcel.readInt();
            this.d = parcel.readInt();
            this.e = parcel.readInt();
            this.f = new SparseIntArray();
            int readInt = parcel.readInt();
            if (readInt > 0) {
                for (int i = 0; i < readInt; i++) {
                    this.f.put(parcel.readInt(), parcel.readInt());
                }
            }
        }
    }
}
