package com.github.ksoichiro.android.observablescrollview;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class ObservableWebView extends WebView implements Scrollable {

    /* renamed from: a  reason: collision with root package name */
    private int f3131a;
    private int b;
    private ObservableScrollViewCallbacks c;
    private ScrollState d;
    private boolean e;
    private boolean f;
    private boolean g;
    private MotionEvent h;
    private ViewGroup i;

    static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        int f3133a;
        int b;

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            parcel.writeInt(this.f3133a);
            parcel.writeInt(this.b);
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            this.f3133a = parcel.readInt();
            this.b = parcel.readInt();
        }
    }

    public ObservableWebView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
    }

    public int getCurrentScrollY() {
        return this.b;
    }

    public boolean onInterceptTouchEvent(MotionEvent motionEvent) {
        if (this.c != null && motionEvent.getActionMasked() == 0) {
            this.f = true;
            this.e = true;
            this.c.d();
        }
        return super.onInterceptTouchEvent(motionEvent);
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        SavedState savedState = (SavedState) parcelable;
        this.f3131a = savedState.f3133a;
        this.b = savedState.b;
        super.onRestoreInstanceState(savedState.getSuperState());
    }

    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.f3133a = this.f3131a;
        savedState.b = this.b;
        return savedState;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i2, int i3, int i4, int i5) {
        super.onScrollChanged(i2, i3, i4, i5);
        ObservableScrollViewCallbacks observableScrollViewCallbacks = this.c;
        if (observableScrollViewCallbacks != null) {
            this.b = i3;
            observableScrollViewCallbacks.a(i3, this.e, this.f);
            if (this.e) {
                this.e = false;
            }
            int i6 = this.f3131a;
            if (i6 < i3) {
                this.d = ScrollState.UP;
            } else if (i3 < i6) {
                this.d = ScrollState.DOWN;
            } else {
                this.d = ScrollState.STOP;
            }
            this.f3131a = i3;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:9:0x0012, code lost:
        if (r0 != 3) goto L_0x0096;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r9) {
        /*
            r8 = this;
            com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks r0 = r8.c
            if (r0 == 0) goto L_0x0096
            int r0 = r9.getActionMasked()
            if (r0 == 0) goto L_0x0096
            r1 = 1
            r2 = 0
            if (r0 == r1) goto L_0x008b
            r3 = 2
            if (r0 == r3) goto L_0x0016
            r1 = 3
            if (r0 == r1) goto L_0x008b
            goto L_0x0096
        L_0x0016:
            android.view.MotionEvent r0 = r8.h
            if (r0 != 0) goto L_0x001c
            r8.h = r9
        L_0x001c:
            float r0 = r9.getY()
            android.view.MotionEvent r3 = r8.h
            float r3 = r3.getY()
            float r0 = r0 - r3
            android.view.MotionEvent r3 = android.view.MotionEvent.obtainNoHistory(r9)
            r8.h = r3
            int r3 = r8.getCurrentScrollY()
            float r3 = (float) r3
            float r3 = r3 - r0
            r0 = 0
            int r3 = (r3 > r0 ? 1 : (r3 == r0 ? 0 : -1))
            if (r3 > 0) goto L_0x0096
            boolean r3 = r8.g
            if (r3 == 0) goto L_0x003d
            return r2
        L_0x003d:
            android.view.ViewGroup r3 = r8.i
            if (r3 != 0) goto L_0x0047
            android.view.ViewParent r3 = r8.getParent()
            android.view.ViewGroup r3 = (android.view.ViewGroup) r3
        L_0x0047:
            r4 = 0
            r5 = 0
            r0 = r8
        L_0x004a:
            if (r0 == 0) goto L_0x006b
            if (r0 == r3) goto L_0x006b
            int r6 = r0.getLeft()
            int r7 = r0.getScrollX()
            int r6 = r6 - r7
            float r6 = (float) r6
            float r4 = r4 + r6
            int r6 = r0.getTop()
            int r7 = r0.getScrollY()
            int r6 = r6 - r7
            float r6 = (float) r6
            float r5 = r5 + r6
            android.view.ViewParent r0 = r0.getParent()
            android.view.View r0 = (android.view.View) r0
            goto L_0x004a
        L_0x006b:
            android.view.MotionEvent r0 = android.view.MotionEvent.obtainNoHistory(r9)
            r0.offsetLocation(r4, r5)
            boolean r4 = r3.onInterceptTouchEvent(r0)
            if (r4 == 0) goto L_0x0086
            r8.g = r1
            r0.setAction(r2)
            com.github.ksoichiro.android.observablescrollview.ObservableWebView$1 r9 = new com.github.ksoichiro.android.observablescrollview.ObservableWebView$1
            r9.<init>(r8, r3, r0)
            r8.post(r9)
            return r2
        L_0x0086:
            boolean r9 = super.onTouchEvent(r9)
            return r9
        L_0x008b:
            r8.g = r2
            r8.f = r2
            com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks r0 = r8.c
            com.github.ksoichiro.android.observablescrollview.ScrollState r1 = r8.d
            r0.a(r1)
        L_0x0096:
            boolean r9 = super.onTouchEvent(r9)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.github.ksoichiro.android.observablescrollview.ObservableWebView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setScrollViewCallbacks(ObservableScrollViewCallbacks observableScrollViewCallbacks) {
        this.c = observableScrollViewCallbacks;
    }

    public void setTouchInterceptionViewGroup(ViewGroup viewGroup) {
        this.i = viewGroup;
    }
}
