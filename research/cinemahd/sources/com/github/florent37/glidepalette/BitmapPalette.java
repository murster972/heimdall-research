package com.github.florent37.glidepalette;

import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import androidx.collection.LruCache;
import androidx.core.util.Pair;
import androidx.palette.graphics.Palette;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;

public abstract class BitmapPalette {
    /* access modifiers changed from: private */
    public static final LruCache<String, Palette> f = new LruCache<>(40);

    /* renamed from: a  reason: collision with root package name */
    protected String f3113a;
    protected LinkedList<PaletteTarget> b = new LinkedList<>();
    protected ArrayList<CallBack> c = new ArrayList<>();
    private PaletteBuilderInterceptor d;
    private boolean e;

    public interface CallBack {
        void a(Palette palette);
    }

    public interface PaletteBuilderInterceptor {
        Palette.Builder a(Palette.Builder builder);
    }

    /* access modifiers changed from: protected */
    public BitmapPalette a(CallBack callBack) {
        if (callBack != null) {
            this.c.add(callBack);
        }
        return this;
    }

    /* access modifiers changed from: protected */
    public void a(Palette palette, boolean z) {
        Palette.Swatch swatch;
        ArrayList<Pair<View, Integer>> arrayList;
        ArrayList<CallBack> arrayList2 = this.c;
        if (arrayList2 != null) {
            Iterator<CallBack> it2 = arrayList2.iterator();
            while (it2.hasNext()) {
                it2.next().a(palette);
            }
            if (palette != null) {
                Iterator it3 = this.b.iterator();
                while (it3.hasNext()) {
                    PaletteTarget paletteTarget = (PaletteTarget) it3.next();
                    int i = paletteTarget.f3115a;
                    if (i == 0) {
                        swatch = palette.h();
                    } else if (i == 1) {
                        swatch = palette.c();
                    } else if (i == 2) {
                        swatch = palette.e();
                    } else if (i == 3) {
                        swatch = palette.f();
                    } else if (i == 4) {
                        swatch = palette.b();
                    } else if (i != 5) {
                        swatch = null;
                    } else {
                        swatch = palette.d();
                    }
                    if (swatch != null && (arrayList = paletteTarget.b) != null) {
                        Iterator<Pair<View, Integer>> it4 = arrayList.iterator();
                        while (it4.hasNext()) {
                            Pair next = it4.next();
                            int a2 = a(swatch, ((Integer) next.b).intValue());
                            if (z || !paletteTarget.d) {
                                ((View) next.f598a).setBackgroundColor(a2);
                            } else {
                                a(paletteTarget, next, a2);
                            }
                        }
                        ArrayList<Pair<TextView, Integer>> arrayList3 = paletteTarget.c;
                        if (arrayList3 != null) {
                            Iterator<Pair<TextView, Integer>> it5 = arrayList3.iterator();
                            while (it5.hasNext()) {
                                Pair next2 = it5.next();
                                ((TextView) next2.f598a).setTextColor(a(swatch, ((Integer) next2.b).intValue()));
                            }
                            paletteTarget.a();
                            this.c = null;
                        } else {
                            return;
                        }
                    } else {
                        return;
                    }
                }
            }
        }
    }

    private void a(PaletteTarget paletteTarget, Pair<View, Integer> pair, int i) {
        Drawable background = ((View) pair.f598a).getBackground();
        Drawable[] drawableArr = new Drawable[2];
        if (background == null) {
            background = new ColorDrawable(((View) pair.f598a).getSolidColor());
        }
        drawableArr[0] = background;
        drawableArr[1] = new ColorDrawable(i);
        TransitionDrawable transitionDrawable = new TransitionDrawable(drawableArr);
        if (Build.VERSION.SDK_INT >= 16) {
            ((View) pair.f598a).setBackground(transitionDrawable);
        } else {
            ((View) pair.f598a).setBackgroundDrawable(transitionDrawable);
        }
        transitionDrawable.startTransition(paletteTarget.e);
    }

    protected static int a(Palette.Swatch swatch, int i) {
        if (swatch == null) {
            Log.e("BitmapPalette", "error while generating Palette, null palette returned");
            return 0;
        } else if (i == 0) {
            return swatch.d();
        } else {
            if (i == 1) {
                return swatch.e();
            }
            if (i != 2) {
                return 0;
            }
            return swatch.a();
        }
    }

    /* access modifiers changed from: protected */
    public void a(Bitmap bitmap) {
        Palette palette;
        final boolean z = this.e;
        if (z || (palette = f.get(this.f3113a)) == null) {
            Palette.Builder builder = new Palette.Builder(bitmap);
            PaletteBuilderInterceptor paletteBuilderInterceptor = this.d;
            if (paletteBuilderInterceptor != null) {
                builder = paletteBuilderInterceptor.a(builder);
            }
            builder.a((Palette.PaletteAsyncListener) new Palette.PaletteAsyncListener() {
                public void a(Palette palette) {
                    if (!z) {
                        BitmapPalette.f.put(BitmapPalette.this.f3113a, palette);
                    }
                    BitmapPalette.this.a(palette, false);
                }
            });
            return;
        }
        a(palette, true);
    }
}
