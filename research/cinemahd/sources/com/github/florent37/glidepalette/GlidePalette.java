package com.github.florent37.glidepalette;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.github.florent37.glidepalette.BitmapPalette;

public class GlidePalette<TranscodeType> extends BitmapPalette implements RequestListener<TranscodeType> {
    protected RequestListener<TranscodeType> g;

    public interface BitmapHolder {
        Bitmap a();
    }

    protected GlidePalette() {
    }

    public static GlidePalette<Drawable> a(String str) {
        GlidePalette<Drawable> glidePalette = new GlidePalette<>();
        glidePalette.f3113a = str;
        return glidePalette;
    }

    public boolean onLoadFailed(GlideException glideException, Object obj, Target<TranscodeType> target, boolean z) {
        RequestListener<TranscodeType> requestListener = this.g;
        return requestListener != null && requestListener.onLoadFailed(glideException, obj, target, z);
    }

    public boolean onResourceReady(TranscodeType transcodetype, Object obj, Target<TranscodeType> target, DataSource dataSource, boolean z) {
        RequestListener<TranscodeType> requestListener = this.g;
        boolean z2 = requestListener != null && requestListener.onResourceReady(transcodetype, obj, target, dataSource, z);
        Bitmap bitmap = null;
        if (transcodetype instanceof BitmapDrawable) {
            bitmap = ((BitmapDrawable) transcodetype).getBitmap();
        } else if (transcodetype instanceof GifDrawable) {
            bitmap = ((GifDrawable) transcodetype).c();
        } else if (target instanceof BitmapHolder) {
            bitmap = ((BitmapHolder) target).a();
        }
        if (bitmap != null) {
            a(bitmap);
        }
        return z2;
    }

    public GlidePalette<TranscodeType> a(BitmapPalette.CallBack callBack) {
        super.a(callBack);
        return this;
    }
}
