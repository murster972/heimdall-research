package com.github.florent37.glidepalette;

import android.view.View;
import android.widget.TextView;
import androidx.core.util.Pair;
import java.util.ArrayList;

public class PaletteTarget {

    /* renamed from: a  reason: collision with root package name */
    protected int f3115a;
    protected ArrayList<Pair<View, Integer>> b;
    protected ArrayList<Pair<TextView, Integer>> c;
    protected boolean d;
    protected int e;

    public void a() {
        this.b.clear();
        this.c.clear();
        this.b = null;
        this.c = null;
        this.d = false;
        this.e = 300;
    }
}
