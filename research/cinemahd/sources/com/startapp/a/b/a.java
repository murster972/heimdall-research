package com.startapp.a.b;

import android.content.Context;
import android.content.pm.PackageManager;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Scanner;
import okhttp3.internal.cache.DiskLruCache;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f5903a;

    public a(Context context) {
        this.f5903a = context;
    }

    public static boolean c() {
        HashMap hashMap = new HashMap();
        hashMap.put("ro.debuggable", DiskLruCache.VERSION_1);
        hashMap.put("ro.secure", "0");
        boolean z = false;
        for (String str : f()) {
            for (String str2 : hashMap.keySet()) {
                if (str.contains(str2)) {
                    if (str.contains("[" + ((String) hashMap.get(str2)) + "]")) {
                        z = true;
                    }
                }
            }
        }
        return z;
    }

    public static boolean d() {
        boolean z = false;
        for (String split : g()) {
            String[] split2 = split.split(" ");
            if (split2.length >= 4) {
                String str = split2[1];
                String str2 = split2[3];
                String[] strArr = b.d;
                boolean z2 = z;
                for (int i = 0; i < 7; i++) {
                    if (str.equalsIgnoreCase(strArr[i])) {
                        String[] split3 = str2.split(",");
                        int length = split3.length;
                        int i2 = 0;
                        while (true) {
                            if (i2 >= length) {
                                break;
                            } else if (split3[i2].equalsIgnoreCase("rw")) {
                                z2 = true;
                                break;
                            } else {
                                i2++;
                            }
                        }
                    }
                }
                z = z2;
            }
        }
        return z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean e() {
        /*
            r0 = 0
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "which"
            java.lang.String r3 = "su"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3}     // Catch:{ all -> 0x002e }
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ all -> 0x002e }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x002c }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x002c }
            java.io.InputStream r4 = r1.getInputStream()     // Catch:{ all -> 0x002c }
            r3.<init>(r4)     // Catch:{ all -> 0x002c }
            r2.<init>(r3)     // Catch:{ all -> 0x002c }
            java.lang.String r2 = r2.readLine()     // Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x0026
            r0 = 1
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.destroy()
        L_0x002b:
            return r0
        L_0x002c:
            goto L_0x002f
        L_0x002e:
            r1 = 0
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.destroy()
        L_0x0034:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.a.b.a.e():boolean");
    }

    private static String[] f() {
        String[] strArr = new String[0];
        try {
            return new Scanner(Runtime.getRuntime().exec("getprop").getInputStream()).useDelimiter("\\A").next().split(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            return strArr;
        }
    }

    private static String[] g() {
        String[] strArr = new String[0];
        try {
            return new Scanner(Runtime.getRuntime().exec("mount").getInputStream()).useDelimiter("\\A").next().split(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            return strArr;
        }
    }

    public final boolean a() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(b.f5904a));
        return a((List<String>) arrayList);
    }

    public final boolean b() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(Arrays.asList(b.b));
        return a((List<String>) arrayList);
    }

    public static boolean a(String str) {
        String[] strArr = b.c;
        boolean z = false;
        for (int i = 0; i < 14; i++) {
            String str2 = strArr[i];
            StringBuilder sb = new StringBuilder();
            sb.append(str2);
            sb.append(str);
            if (new File(str2, str).exists()) {
                z = true;
            }
        }
        return z;
    }

    private boolean a(List<String> list) {
        PackageManager packageManager = this.f5903a.getPackageManager();
        boolean z = false;
        for (String packageInfo : list) {
            try {
                packageManager.getPackageInfo(packageInfo, 0);
                z = true;
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        return z;
    }
}
