package com.startapp.a.a;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import com.facebook.common.util.ByteConstants;
import com.google.ar.core.ImageMetadata;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import okhttp3.internal.cache.DiskLruCache;

@TargetApi(14)
public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f5901a = {"/dev/socket/genyd", "/dev/socket/baseband_genyd"};
    private static final String[] b = {"goldfish"};
    private static final String[] c = {"/dev/socket/qemud", "/dev/qemu_pipe"};
    private static final String[] d = {"ueventd.android_x86.rc", "x86.prop", "ueventd.ttVM_x86.rc", "init.ttVM_x86.rc", "fstab.ttVM_x86", "fstab.vbox86", "init.vbox86.rc", "ueventd.vbox86.rc"};
    private static final String[] e = {"fstab.andy", "ueventd.andy.rc"};
    private static final String[] f = {"fstab.nox", "init.nox.rc", "ueventd.nox.rc", "/BigNoxGameHD", "/YSLauncher"};
    private static final b[] g = {new b("init.svc.qemud", (String) null), new b("init.svc.qemu-props", (String) null), new b("qemu.hw.mainkeys", (String) null), new b("qemu.sf.fake_camera", (String) null), new b("qemu.sf.lcd_density", (String) null), new b("ro.bootloader", "unknown"), new b("ro.bootmode", "unknown"), new b("ro.hardware", "goldfish"), new b("ro.kernel.android.qemud", (String) null), new b("ro.kernel.qemu.gles", (String) null), new b("ro.kernel.qemu", DiskLruCache.VERSION_1), new b("ro.product.device", "generic"), new b("ro.product.model", "sdk"), new b("ro.product.name", "sdk"), new b("ro.serialno", (String) null), new b("ro.build.description", "72656C656173652D6B657973"), new b("ro.build.fingerprint", "3A757365722F72656C656173652D6B657973"), new b("net.eth0.dns1", (String) null), new b("rild.libpath", "2F73797374656D2F6C69622F6C69627265666572656E63652D72696C2E736F"), new b("ro.radio.use-ppp", (String) null), new b("gsm.version.baseband", (String) null), new b("ro.build.tags", "72656C656173652D6B65"), new b("ro.build.display.id", "746573742D"), new b("init.svc.console", (String) null)};
    @SuppressLint({"StaticFieldLeak"})
    private static a k;
    private static Boolean l;
    private final Context h;
    private boolean i = true;
    private List<String> j = new ArrayList();

    private a(Context context) {
        this.h = context;
        this.j.add("com.google.android.launcher.layouts.genymotion");
        this.j.add("com.bluestacks");
        this.j.add("com.bignox.app");
        this.j.add("com.vphone.launcher");
    }

    public static boolean a(Context context) {
        if (l == null) {
            if (context != null) {
                if (k == null) {
                    k = new a(context.getApplicationContext());
                }
                a aVar = k;
                boolean z = Build.FINGERPRINT.startsWith("generic") || Build.MODEL.contains("google_sdk") || Build.MODEL.toLowerCase().contains("droid4x") || Build.MODEL.contains("Emulator") || Build.MODEL.contains("Android SDK built for") || Build.MANUFACTURER.contains("Genymotion") || Build.HARDWARE.equals("goldfish") || Build.HARDWARE.equals("vbox86") || Build.PRODUCT.equals("sdk") || Build.PRODUCT.equals("google_sdk") || Build.PRODUCT.equals("sdk_x86") || Build.PRODUCT.equals("vbox86p") || Build.BOARD.toLowerCase().contains("nox") || Build.BOOTLOADER.toLowerCase().contains("nox") || Build.HARDWARE.toLowerCase().contains("nox") || Build.PRODUCT.toLowerCase().contains("nox") || Build.SERIAL.toLowerCase().contains("nox") || Build.FINGERPRINT.startsWith("unknown") || Build.FINGERPRINT.contains("Andy") || Build.FINGERPRINT.contains("ttVM_Hdragon") || Build.FINGERPRINT.contains("vbox86p") || Build.HARDWARE.contains("ttVM_x86") || Build.MODEL.equals("sdk") || Build.MODEL.contains("Droid4X") || Build.MODEL.contains("TiantianVM") || Build.MODEL.contains("Andy") || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"));
                if (!z) {
                    z = aVar.a(f5901a, "Geny") || aVar.a(e, "Andy") || aVar.a(f, "Nox") || b() || aVar.a(c, "Pipes") || aVar.d() || (aVar.c() && aVar.a(d, "X86"));
                }
                if (!z) {
                    z = aVar.a();
                }
                l = Boolean.valueOf(z);
            } else {
                throw new IllegalArgumentException("Context must not be null.");
            }
        }
        return l.booleanValue();
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x006c A[SYNTHETIC, Splitter:B:28:0x006c] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x0073 A[SYNTHETIC, Splitter:B:34:0x0073] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b() {
        /*
            r0 = 2
            java.io.File[] r1 = new java.io.File[r0]
            java.io.File r2 = new java.io.File
            java.lang.String r3 = "/proc/tty/drivers"
            r2.<init>(r3)
            r3 = 0
            r1[r3] = r2
            java.io.File r2 = new java.io.File
            java.lang.String r4 = "/proc/cpuinfo"
            r2.<init>(r4)
            r4 = 1
            r1[r4] = r2
            r2 = 0
        L_0x0018:
            if (r2 >= r0) goto L_0x007a
            r5 = r1[r2]
            boolean r6 = r5.exists()
            if (r6 == 0) goto L_0x0077
            boolean r6 = r5.canRead()
            if (r6 == 0) goto L_0x0077
            r6 = 1024(0x400, float:1.435E-42)
            char[] r6 = new char[r6]
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r8 = 0
            java.io.BufferedReader r9 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            java.io.InputStreamReader r10 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            java.io.FileInputStream r11 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            r11.<init>(r5)     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            r10.<init>(r11)     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
            r9.<init>(r10)     // Catch:{ Exception -> 0x0070, all -> 0x0069 }
        L_0x0041:
            int r5 = r9.read(r6)     // Catch:{ Exception -> 0x0067, all -> 0x0064 }
            r8 = -1
            if (r5 == r8) goto L_0x004c
            r7.append(r6, r3, r5)     // Catch:{ Exception -> 0x0067, all -> 0x0064 }
            goto L_0x0041
        L_0x004c:
            r9.close()     // Catch:{ IOException -> 0x004f }
        L_0x004f:
            java.lang.String r5 = r7.toString()
            java.lang.String[] r6 = b
            r7 = 0
        L_0x0056:
            if (r7 > 0) goto L_0x0077
            r8 = r6[r7]
            boolean r8 = r5.contains(r8)
            if (r8 == 0) goto L_0x0061
            return r4
        L_0x0061:
            int r7 = r7 + 1
            goto L_0x0056
        L_0x0064:
            r0 = move-exception
            r8 = r9
            goto L_0x006a
        L_0x0067:
            r8 = r9
            goto L_0x0071
        L_0x0069:
            r0 = move-exception
        L_0x006a:
            if (r8 == 0) goto L_0x006f
            r8.close()     // Catch:{ IOException -> 0x006f }
        L_0x006f:
            throw r0
        L_0x0070:
        L_0x0071:
            if (r8 == 0) goto L_0x0076
            r8.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0076:
            return r3
        L_0x0077:
            int r2 = r2 + 1
            goto L_0x0018
        L_0x007a:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.a.a.a.b():boolean");
    }

    private boolean c() {
        b[] bVarArr = g;
        int i2 = 0;
        for (int i3 = 0; i3 < 24; i3++) {
            b bVar = bVarArr[i3];
            String a2 = a(this.h, bVar.f5902a);
            if (bVar.b == null && a2 != null) {
                i2++;
            }
            String str = bVar.b;
            if (!(str == null || a2 == null || !a2.contains(str))) {
                i2++;
            }
        }
        if (i2 >= 5) {
            return true;
        }
        return false;
    }

    private boolean d() {
        if (!b(this.h, "android.permission.INTERNET")) {
            return false;
        }
        String[] strArr = {"/system/bin/netcfg"};
        StringBuilder sb = new StringBuilder();
        try {
            ProcessBuilder processBuilder = new ProcessBuilder(strArr);
            processBuilder.directory(new File("/system/bin/"));
            processBuilder.redirectErrorStream(true);
            InputStream inputStream = processBuilder.start().getInputStream();
            byte[] bArr = new byte[ByteConstants.KB];
            while (inputStream.read(bArr) != -1) {
                sb.append(new String(bArr));
            }
            inputStream.close();
        } catch (Exception unused) {
        }
        String sb2 = sb.toString();
        if (TextUtils.isEmpty(sb2)) {
            return false;
        }
        for (String str : sb2.split(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE)) {
            if ((str.contains("wlan0") || str.contains("tunl0") || str.contains("eth0")) && str.contains("10.0.2.15")) {
                return true;
            }
        }
        return false;
    }

    private static boolean b(Context context, String str) {
        try {
            return Build.VERSION.SDK_INT >= 23 ? context.checkSelfPermission(str) == 0 : context.checkCallingOrSelfPermission(str) == 0;
        } catch (Throwable unused) {
        }
    }

    private boolean a() {
        if (this.i && !this.j.isEmpty()) {
            PackageManager packageManager = this.h.getPackageManager();
            for (String launchIntentForPackage : this.j) {
                Intent launchIntentForPackage2 = packageManager.getLaunchIntentForPackage(launchIntentForPackage);
                if (launchIntentForPackage2 != null && !packageManager.queryIntentActivities(launchIntentForPackage2, ImageMetadata.CONTROL_AE_ANTIBANDING_MODE).isEmpty()) {
                    return true;
                }
            }
        }
        return false;
    }

    private boolean a(String[] strArr, String str) {
        File file;
        for (String str2 : strArr) {
            if (!b(this.h, "android.permission.READ_EXTERNAL_STORAGE") || !str2.contains("/") || !str.equals("Nox")) {
                file = new File(str2);
            } else {
                file = new File(Environment.getExternalStorageDirectory() + str2);
            }
            if (file.exists()) {
                return true;
            }
        }
        return false;
    }

    private static String a(Context context, String str) {
        try {
            Class<?> loadClass = context.getClassLoader().loadClass("android.os.SystemProperties");
            return (String) loadClass.getMethod("get", new Class[]{String.class}).invoke(loadClass, new Object[]{str});
        } catch (Exception unused) {
            return null;
        }
    }
}
