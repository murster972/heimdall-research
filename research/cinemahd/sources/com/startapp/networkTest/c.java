package com.startapp.networkTest;

import android.content.Context;
import com.startapp.networkTest.e.b;
import java.security.PublicKey;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static c f5966a;
    private a b;
    private b c;
    private d d;
    private Context e;
    private PublicKey f;

    private c(Context context) {
        this.e = context;
    }

    public static boolean a() {
        return f5966a != null;
    }

    public static synchronized b b() {
        b bVar;
        synchronized (c.class) {
            bVar = f5966a.c;
        }
        return bVar;
    }

    public static d c() {
        return f5966a.d;
    }

    public static a d() {
        return f5966a.b;
    }

    public static void a(Context context, byte[] bArr) {
        if (context == null) {
            throw new IllegalArgumentException("context is null");
        } else if (bArr == null) {
            throw new IllegalArgumentException("config is null");
        } else if (f5966a == null) {
            try {
                b a2 = b.a(bArr);
                c cVar = new c(context);
                f5966a = cVar;
                cVar.f = null;
                c cVar2 = f5966a;
                cVar2.b = a2.f5964a;
                cVar2.c = new b();
                cVar2.d = new d(cVar2.e);
            } catch (Exception unused) {
                throw new IllegalArgumentException("configuration is invalid");
            }
        }
    }
}
