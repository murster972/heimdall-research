package com.startapp.networkTest.data;

import com.startapp.common.parser.d;
import com.startapp.networkTest.enums.Os;
import com.startapp.networkTest.enums.PhoneTypes;
import com.startapp.networkTest.enums.SimStates;
import com.startapp.networkTest.enums.ThreeState;
import com.startapp.sdk.adsbase.j.t;

public final class a implements Cloneable {
    @d(a = true)
    public t BluetoothInfo$3e5b9058 = new t();
    public String BuildFingerprint = "";
    public String DeviceManufacturer = "";
    public String DeviceName = "";
    public long DeviceUpTime;
    @d(a = true)
    public com.startapp.sdk.adsbase.k.a HostAppInfo$41202ccd = new com.startapp.sdk.adsbase.k.a();
    public boolean IsRooted;
    @d(a = true)
    public com.startapp.networkTest.data.a.a MultiSimInfo = new com.startapp.networkTest.data.a.a();
    public Os OS = Os.Android;
    public String OSVersion = "";
    public String OsSystemVersion = "";
    public int PhoneCount = -1;
    public PhoneTypes PhoneType = PhoneTypes.Unknown;
    public ThreeState PowerSaveMode = ThreeState.Unknown;
    public String SimOperator = "";
    public String SimOperatorName = "";
    public SimStates SimState = SimStates.Unknown;
    public String TAC = "";
    public String UserLocal = "";

    public final Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
