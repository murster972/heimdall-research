package com.startapp.networkTest.data;

import com.startapp.common.parser.d;
import com.startapp.networkTest.data.radio.NeighboringCell;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.DuplexMode;
import com.startapp.networkTest.enums.FlightModeStates;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.PreferredNetworkTypes;
import com.startapp.networkTest.enums.ServiceStates;
import com.startapp.networkTest.enums.ThreeState;
import com.startapp.networkTest.enums.ThreeStateShort;
import com.startapp.networkTest.enums.radio.DataConnectionStates;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class RadioInfo implements Serializable, Cloneable {
    public static final Integer INVALID = Integer.MAX_VALUE;
    private static final long serialVersionUID = 4817753379835440169L;
    public int ARFCN = -1;
    public ThreeStateShort CarrierAggregation;
    public String CdmaBaseStationId = "";
    public String CdmaBaseStationLatitude = "";
    public String CdmaBaseStationLongitude = "";
    public int CdmaEcIo = INVALID.intValue();
    public String CdmaNetworkId = "";
    public String CdmaSystemId = "";
    public ConnectionTypes ConnectionType = ConnectionTypes.Unknown;
    public DuplexMode DuplexMode;
    public int EcN0 = 0;
    public FlightModeStates FlightMode = FlightModeStates.Unknown;
    public String GsmCellId = "";
    public int GsmCellIdAge = -1;
    public String GsmLAC = "";
    public transient boolean IsDefaultDataSim = true;
    public transient boolean IsDefaultVoiceSim = true;
    public ThreeStateShort IsMetered;
    public boolean IsRoaming;
    public int IsVpn;
    public int LteCqi = INVALID.intValue();
    public int LteRsrp = INVALID.intValue();
    public int LteRsrq = INVALID.intValue();
    public int LteRssi = INVALID.intValue();
    public int LteRssnr = INVALID.intValue();
    public String MCC = "";
    public String MNC = "";
    public ThreeStateShort ManualSelection;
    public boolean MissingPermission;
    public DataConnectionStates MobileDataConnectionState;
    public ThreeState MobileDataEnabled;
    public int NativeDbm = INVALID.intValue();
    @d(b = ArrayList.class, c = NeighboringCell.class)
    public ArrayList<NeighboringCell> NeighboringCells;
    public NetworkTypes NetworkType = NetworkTypes.Unknown;
    public ThreeStateShort NrAvailable;
    public int NrCsiRsrp = INVALID.intValue();
    public int NrCsiRsrq = INVALID.intValue();
    public int NrCsiSinr = INVALID.intValue();
    public int NrSsRsrp = INVALID.intValue();
    public int NrSsRsrq = INVALID.intValue();
    public int NrSsSinr = INVALID.intValue();
    public String NrState = "Unknown";
    public String OperatorName = "";
    public PreferredNetworkTypes PreferredNetwork;
    public String PrimaryScramblingCode = "";
    public int RSCP = INVALID.intValue();
    public int RXLevel;
    public int RXLevelAge = -1;
    public ServiceStates ServiceState = ServiceStates.Unknown;
    public int ServiceStateAge;
    public int SubscriptionId;

    public RadioInfo() {
        ThreeStateShort threeStateShort = ThreeStateShort.Unknown;
        this.NrAvailable = threeStateShort;
        this.IsRoaming = false;
        this.IsMetered = threeStateShort;
        this.MobileDataEnabled = ThreeState.Unknown;
        this.MobileDataConnectionState = DataConnectionStates.Unknown;
        this.MissingPermission = false;
        this.SubscriptionId = -1;
        this.PreferredNetwork = PreferredNetworkTypes.Unknown;
        this.DuplexMode = DuplexMode.Unknown;
        ThreeStateShort threeStateShort2 = ThreeStateShort.Unknown;
        this.ManualSelection = threeStateShort2;
        this.CarrierAggregation = threeStateShort2;
        this.ServiceStateAge = -1;
        this.IsVpn = -1;
        this.NeighboringCells = new ArrayList<>();
    }

    public Object clone() throws CloneNotSupportedException {
        RadioInfo radioInfo = (RadioInfo) super.clone();
        radioInfo.NeighboringCells = new ArrayList<>();
        Iterator<NeighboringCell> it2 = this.NeighboringCells.iterator();
        while (it2.hasNext()) {
            radioInfo.NeighboringCells.add((NeighboringCell) it2.next().clone());
        }
        return radioInfo;
    }
}
