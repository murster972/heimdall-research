package com.startapp.networkTest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import com.startapp.networkTest.e.b;
import java.util.Set;
import java.util.UUID;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f6004a;

    public d(Context context) {
        this.f6004a = context.getSharedPreferences("p3inspreferences", 0);
    }

    @SuppressLint({"ApplySharedPref"})
    private String n() {
        String replace = UUID.randomUUID().toString().replace("-", "");
        SharedPreferences.Editor edit = this.f6004a.edit();
        edit.putString("p3ins_pfk_guid", replace);
        edit.putLong("P3INS_PFK_GUID_TIMESTAMP", b.b());
        edit.commit();
        return replace;
    }

    @SuppressLint({"ApplySharedPref"})
    public final void a(long j) {
        this.f6004a.edit().putLong("P3INS_PFK_CONNECTIVITY_TEST_TIMESTAMP", j).commit();
    }

    public final boolean b() {
        return this.f6004a.getBoolean("P3INS_PFK_CONNECTIVITY_TEST_ENABLED", c.d().b());
    }

    public final boolean c() {
        return this.f6004a.getBoolean("P3INS_PFK_CONNECTIVITY_KEEPALIVE_ENABLED", c.d().c());
    }

    public final long d() {
        return this.f6004a.getLong("P3INS_PFK_CONNECTIVITY_TEST_TRUSTSTORE_LAST_CHECK", 0);
    }

    public final long e() {
        return this.f6004a.getLong("P3INS_PFK_CONNECTIVITY_TEST_TRUSTSTORE_LAST_MODIFIED", 0);
    }

    public final Set<String> f() {
        return this.f6004a.getStringSet("P3INS_PFK_CT_CRITERIA_SERVER_LIST", (Set) null);
    }

    public final Set<String> g() {
        return this.f6004a.getStringSet("P3INS_PFK_LTR_CRITERIA_SERVER_LIST", (Set) null);
    }

    public final long h() {
        return this.f6004a.getLong("P3INS_PFK_CONNECTIVITY_TEST_CDNCONFIG_LAST_MODIFIED", 0);
    }

    public final long i() {
        return this.f6004a.getLong("P3INS_PFK_CONNECTIVITY_TEST_CDNCONFIG_LAST_CHECK", 0);
    }

    public final String[] j() {
        Set<String> stringSet = this.f6004a.getStringSet("P3INS_PFK_CDN_CT_SERVER_LIST", (Set) null);
        if (stringSet == null || stringSet.isEmpty()) {
            return c.d().C();
        }
        return (String[]) stringSet.toArray(new String[stringSet.size()]);
    }

    public final String k() {
        return this.f6004a.getString("P3INS_PFK_CDN_CT_CRITERIA", c.d().D().name());
    }

    public final String[] l() {
        Set<String> stringSet = this.f6004a.getStringSet("P3INS_PFK_CDN_LTR_SERVER_LIST", (Set) null);
        if (stringSet == null || stringSet.isEmpty()) {
            return c.d().E();
        }
        return (String[]) stringSet.toArray(new String[stringSet.size()]);
    }

    public final String m() {
        return this.f6004a.getString("P3INS_PFK_CDN_LTR_CRITERIA", c.d().F().name());
    }

    public final void a(Set<String> set) {
        this.f6004a.edit().putStringSet("P3INS_PFK_CT_CRITERIA_SERVER_LIST", set).commit();
    }

    @SuppressLint({"ApplySharedPref"})
    public final void b(long j) {
        this.f6004a.edit().putLong("P3INS_PFK_CONNECTIVITY_TEST_TRUSTSTORE_LAST_CHECK", j).commit();
    }

    @SuppressLint({"ApplySharedPref"})
    public final void c(long j) {
        this.f6004a.edit().putLong("P3INS_PFK_CONNECTIVITY_TEST_TRUSTSTORE_LAST_MODIFIED", j).commit();
    }

    @SuppressLint({"ApplySharedPref"})
    public final void d(long j) {
        this.f6004a.edit().putLong("P3INS_PFK_CONNECTIVITY_TEST_CDNCONFIG_LAST_MODIFIED", j).commit();
    }

    @SuppressLint({"ApplySharedPref"})
    public final void e(long j) {
        this.f6004a.edit().putLong("P3INS_PFK_CONNECTIVITY_TEST_CDNCONFIG_LAST_CHECK", j).commit();
    }

    public final void a(String str) {
        this.f6004a.edit().putString("P3INS_PFK_CDN_CT_CRITERIA", str).commit();
    }

    public final void b(Set<String> set) {
        this.f6004a.edit().putStringSet("P3INS_PFK_LTR_CRITERIA_SERVER_LIST", set).commit();
    }

    public final void c(Set<String> set) {
        this.f6004a.edit().putStringSet("P3INS_PFK_CDN_CT_SERVER_LIST", set).commit();
    }

    public final void d(Set<String> set) {
        this.f6004a.edit().putStringSet("P3INS_PFK_CDN_LTR_SERVER_LIST", set).commit();
    }

    public final String a() {
        final String string = this.f6004a.getString("p3ins_pfk_guid", "");
        boolean z = true;
        if (string == null || string.length() == 0) {
            string = n();
        } else {
            long b = b.b();
            long j = this.f6004a.getLong("P3INS_PFK_GUID_TIMESTAMP", 0);
            long p = c.d().p();
            if (p == -1 || b - j <= p) {
                z = false;
            } else {
                string = n();
            }
        }
        if (z) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                }
            });
            if (this.f6004a.getBoolean("P3INS_PFK_SEND_REGISTRATION_TIMESTAMP_ENABLED", c.d().q())) {
                this.f6004a.getBoolean("P3INS_PFK_IS_ALREADY_REGISTERED", false);
            }
        }
        return string;
    }

    public final void b(String str) {
        this.f6004a.edit().putString("P3INS_PFK_CDN_LTR_CRITERIA", str).commit();
    }
}
