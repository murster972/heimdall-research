package com.startapp.networkTest.a.a;

import android.util.Log;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5963a = "a";

    public static byte[] a(byte[] bArr) {
        try {
            MessageDigest instance = MessageDigest.getInstance("SHA-256");
            instance.update(bArr);
            return instance.digest();
        } catch (NoSuchAlgorithmException e) {
            String str = f5963a;
            Log.e(str, "hash: " + e.getMessage());
            e.printStackTrace();
            return null;
        }
    }
}
