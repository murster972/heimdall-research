package com.startapp.networkTest.a;

import android.content.Context;
import com.startapp.networkTest.enums.CtTestTypes;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import javax.net.ssl.X509TrustManager;

public class a implements X509TrustManager {
    private static String d = "";
    private static boolean f = false;
    private static X509TrustManager g;
    private static X509TrustManager h;
    private static final X509TrustManager i = new X509TrustManager() {
        public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public final X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private X509TrustManager[] f5962a;
    private CtTestTypes[] b;
    private String c = "";
    private CtTestTypes e = CtTestTypes.Unknown;

    static {
        Class<a> cls = a.class;
    }

    public a(Context context, boolean z) {
        a(context, z);
        this.f5962a = new X509TrustManager[3];
        this.b = new CtTestTypes[3];
        X509TrustManager[] x509TrustManagerArr = this.f5962a;
        x509TrustManagerArr[0] = g;
        CtTestTypes[] ctTestTypesArr = this.b;
        ctTestTypesArr[0] = CtTestTypes.SSLOwnTs;
        x509TrustManagerArr[1] = h;
        ctTestTypesArr[1] = CtTestTypes.SSLDeviceTs;
        x509TrustManagerArr[2] = i;
        ctTestTypesArr[2] = CtTestTypes.SSLTrustAll;
        this.c = d;
    }

    public final String a() {
        return this.c;
    }

    public final CtTestTypes b() {
        return this.e;
    }

    public void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
    }

    public void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) throws CertificateException {
        int i2 = 0;
        while (true) {
            X509TrustManager[] x509TrustManagerArr = this.f5962a;
            if (i2 < x509TrustManagerArr.length) {
                X509TrustManager x509TrustManager = x509TrustManagerArr[i2];
                if (x509TrustManager != null) {
                    try {
                        this.e = this.b[i2];
                        x509TrustManager.checkServerTrusted(x509CertificateArr, str);
                        return;
                    } catch (CertificateException e2) {
                        if (i2 == 0) {
                            this.c += e2.getMessage();
                        }
                        if (i2 + 1 == this.f5962a.length) {
                            throw e2;
                        }
                    }
                } else {
                    i2++;
                }
            } else {
                return;
            }
        }
    }

    public X509Certificate[] getAcceptedIssuers() {
        return h.getAcceptedIssuers();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(9:35|36|37|38|39|(1:(2:41|(2:60|43)(1:44))(1:61))|52|53|54) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x008f */
    /* JADX WARNING: Removed duplicated region for block: B:41:0x00a1 A[Catch:{ Exception -> 0x00bf }] */
    /* JADX WARNING: Removed duplicated region for block: B:61:0x00d7 A[EDGE_INSN: B:61:0x00d7->B:52:0x00d7 ?: BREAK  , SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r6, boolean r7) {
        /*
            boolean r0 = f
            if (r0 == 0) goto L_0x0007
            if (r7 != 0) goto L_0x0007
            return
        L_0x0007:
            java.lang.Class<com.startapp.networkTest.a.a> r0 = com.startapp.networkTest.a.a.class
            monitor-enter(r0)
            boolean r1 = f     // Catch:{ all -> 0x00db }
            if (r1 == 0) goto L_0x0012
            if (r7 != 0) goto L_0x0012
            monitor-exit(r0)     // Catch:{ all -> 0x00db }
            return
        L_0x0012:
            java.lang.String r7 = ""
            d = r7     // Catch:{ all -> 0x00db }
            r7 = 0
            java.lang.String r1 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()     // Catch:{ Exception -> 0x0039 }
            javax.net.ssl.TrustManagerFactory r1 = javax.net.ssl.TrustManagerFactory.getInstance(r1)     // Catch:{ Exception -> 0x0039 }
            r2 = 0
            r1.init(r2)     // Catch:{ Exception -> 0x0039 }
            javax.net.ssl.TrustManager[] r1 = r1.getTrustManagers()     // Catch:{ Exception -> 0x0039 }
            int r2 = r1.length     // Catch:{ Exception -> 0x0039 }
            r3 = 0
        L_0x0029:
            if (r3 >= r2) goto L_0x0051
            r4 = r1[r3]     // Catch:{ Exception -> 0x0039 }
            boolean r5 = r4 instanceof javax.net.ssl.X509TrustManager     // Catch:{ Exception -> 0x0039 }
            if (r5 == 0) goto L_0x0036
            javax.net.ssl.X509TrustManager r4 = (javax.net.ssl.X509TrustManager) r4     // Catch:{ Exception -> 0x0039 }
            h = r4     // Catch:{ Exception -> 0x0039 }
            goto L_0x0051
        L_0x0036:
            int r3 = r3 + 1
            goto L_0x0029
        L_0x0039:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00db }
            r2.<init>()     // Catch:{ all -> 0x00db }
            java.lang.String r3 = d     // Catch:{ all -> 0x00db }
            r2.append(r3)     // Catch:{ all -> 0x00db }
            java.lang.String r1 = r1.getMessage()     // Catch:{ all -> 0x00db }
            r2.append(r1)     // Catch:{ all -> 0x00db }
            java.lang.String r1 = r2.toString()     // Catch:{ all -> 0x00db }
            d = r1     // Catch:{ all -> 0x00db }
        L_0x0051:
            r1 = 1
            java.io.File r2 = com.startapp.networkTest.utils.j.b(r6)     // Catch:{ Exception -> 0x00bf }
            java.io.File r6 = com.startapp.networkTest.utils.j.c(r6)     // Catch:{ Exception -> 0x00bf }
            boolean r3 = r2.exists()     // Catch:{ Exception -> 0x00bf }
            if (r3 == 0) goto L_0x00b7
            boolean r3 = r6.exists()     // Catch:{ Exception -> 0x00bf }
            if (r3 == 0) goto L_0x00b7
            com.startapp.networkTest.a r3 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x00bf }
            boolean r3 = r3.j()     // Catch:{ Exception -> 0x00bf }
            if (r3 == 0) goto L_0x0075
            boolean r6 = com.startapp.networkTest.utils.j.a((java.io.File) r2, (java.io.File) r6)     // Catch:{ Exception -> 0x00bf }
            goto L_0x0076
        L_0x0075:
            r6 = 1
        L_0x0076:
            if (r6 == 0) goto L_0x00af
            java.io.FileInputStream r6 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00bf }
            r6.<init>(r2)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r2 = "BKS"
            java.security.KeyStore r2 = java.security.KeyStore.getInstance(r2)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r3 = "R_hqKukfFZxKn52"
            char[] r3 = r3.toCharArray()     // Catch:{ Exception -> 0x00bf }
            r2.load(r6, r3)     // Catch:{ Exception -> 0x00bf }
            r6.close()     // Catch:{ IOException -> 0x008f }
        L_0x008f:
            java.lang.String r6 = javax.net.ssl.TrustManagerFactory.getDefaultAlgorithm()     // Catch:{ Exception -> 0x00bf }
            javax.net.ssl.TrustManagerFactory r6 = javax.net.ssl.TrustManagerFactory.getInstance(r6)     // Catch:{ Exception -> 0x00bf }
            r6.init(r2)     // Catch:{ Exception -> 0x00bf }
            javax.net.ssl.TrustManager[] r6 = r6.getTrustManagers()     // Catch:{ Exception -> 0x00bf }
            int r2 = r6.length     // Catch:{ Exception -> 0x00bf }
        L_0x009f:
            if (r7 >= r2) goto L_0x00d7
            r3 = r6[r7]     // Catch:{ Exception -> 0x00bf }
            boolean r4 = r3 instanceof javax.net.ssl.X509TrustManager     // Catch:{ Exception -> 0x00bf }
            if (r4 == 0) goto L_0x00ac
            javax.net.ssl.X509TrustManager r3 = (javax.net.ssl.X509TrustManager) r3     // Catch:{ Exception -> 0x00bf }
            g = r3     // Catch:{ Exception -> 0x00bf }
            goto L_0x00d7
        L_0x00ac:
            int r7 = r7 + 1
            goto L_0x009f
        L_0x00af:
            java.security.KeyStoreException r6 = new java.security.KeyStoreException     // Catch:{ Exception -> 0x00bf }
            java.lang.String r7 = "Verification of downloaded truststore failed"
            r6.<init>(r7)     // Catch:{ Exception -> 0x00bf }
            throw r6     // Catch:{ Exception -> 0x00bf }
        L_0x00b7:
            java.security.KeyStoreException r6 = new java.security.KeyStoreException     // Catch:{ Exception -> 0x00bf }
            java.lang.String r7 = "Downloaded truststore not available"
            r6.<init>(r7)     // Catch:{ Exception -> 0x00bf }
            throw r6     // Catch:{ Exception -> 0x00bf }
        L_0x00bf:
            r6 = move-exception
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x00db }
            r7.<init>()     // Catch:{ all -> 0x00db }
            java.lang.String r2 = d     // Catch:{ all -> 0x00db }
            r7.append(r2)     // Catch:{ all -> 0x00db }
            java.lang.String r6 = r6.getMessage()     // Catch:{ all -> 0x00db }
            r7.append(r6)     // Catch:{ all -> 0x00db }
            java.lang.String r6 = r7.toString()     // Catch:{ all -> 0x00db }
            d = r6     // Catch:{ all -> 0x00db }
        L_0x00d7:
            f = r1     // Catch:{ all -> 0x00db }
            monitor-exit(r0)     // Catch:{ all -> 0x00db }
            return
        L_0x00db:
            r6 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00db }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.a.a.a(android.content.Context, boolean):void");
    }
}
