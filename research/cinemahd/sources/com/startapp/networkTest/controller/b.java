package com.startapp.networkTest.controller;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.PowerManager;
import android.os.Process;
import android.os.StatFs;
import android.os.SystemClock;
import android.provider.MediaStore;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.startapp.networkTest.data.a;
import com.startapp.networkTest.data.e;
import com.startapp.networkTest.data.f;
import com.startapp.networkTest.enums.AnonymizationLevel;
import com.startapp.networkTest.enums.AppCategoryTypes;
import com.startapp.networkTest.enums.IdleStates;
import com.startapp.networkTest.enums.MemoryStates;
import com.startapp.networkTest.enums.Os;
import com.startapp.networkTest.enums.PhoneTypes;
import com.startapp.networkTest.enums.ScreenStates;
import com.startapp.networkTest.enums.SimStates;
import com.startapp.networkTest.enums.ThreeState;
import com.startapp.networkTest.utils.g;
import com.startapp.networkTest.utils.h;
import com.startapp.networkTest.utils.i;
import com.startapp.sdk.adsbase.j.j;
import com.startapp.sdk.adsbase.j.t;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Locale;
import okhttp3.internal.cache.DiskLruCache;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5984a = "b";

    /* renamed from: com.startapp.networkTest.controller.b$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5985a = new int[AnonymizationLevel.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.startapp.networkTest.enums.AnonymizationLevel[] r0 = com.startapp.networkTest.enums.AnonymizationLevel.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5985a = r0
                int[] r0 = f5985a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.enums.AnonymizationLevel r1 = com.startapp.networkTest.enums.AnonymizationLevel.Full     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5985a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.enums.AnonymizationLevel r1 = com.startapp.networkTest.enums.AnonymizationLevel.Anonymized     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5985a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.enums.AnonymizationLevel r1 = com.startapp.networkTest.enums.AnonymizationLevel.None     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.b.AnonymousClass1.<clinit>():void");
        }
    }

    public static a a(Context context) {
        String[] strArr;
        SimStates simStates;
        PhoneTypes phoneTypes;
        a aVar = new a();
        aVar.DeviceManufacturer = Build.MANUFACTURER;
        aVar.DeviceName = Build.MODEL;
        aVar.OS = Os.Android;
        aVar.OSVersion = Build.VERSION.RELEASE;
        aVar.BuildFingerprint = Build.FINGERPRINT;
        aVar.DeviceUpTime = SystemClock.elapsedRealtime();
        aVar.UserLocal = Locale.getDefault().toString();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        if (telephonyManager != null) {
            aVar.SimOperator = h.a(telephonyManager.getSimOperator());
            aVar.SimOperatorName = h.a(telephonyManager.getSimOperatorName());
            if (Build.VERSION.SDK_INT >= 29) {
                String typeAllocationCode = telephonyManager.getTypeAllocationCode();
                if (typeAllocationCode == null || typeAllocationCode.isEmpty()) {
                    String manufacturerCode = telephonyManager.getManufacturerCode();
                    if (manufacturerCode != null && !manufacturerCode.isEmpty()) {
                        aVar.TAC = manufacturerCode;
                    }
                } else {
                    aVar.TAC = typeAllocationCode;
                }
            }
            SimStates simStates2 = SimStates.Unknown;
            int simState = telephonyManager.getSimState();
            if (simState == 1) {
                simStates = SimStates.Absent;
            } else if (simState == 2) {
                simStates = SimStates.PinRequired;
            } else if (simState == 3) {
                simStates = SimStates.PukRequired;
            } else if (simState == 4) {
                simStates = SimStates.NetworkLocked;
            } else if (simState != 5) {
                simStates = SimStates.Unknown;
            } else {
                simStates = SimStates.Ready;
            }
            aVar.SimState = simStates;
            if (Build.VERSION.SDK_INT >= 23) {
                try {
                    aVar.PhoneCount = ((Integer) telephonyManager.getClass().getDeclaredMethod("getPhoneCount", new Class[0]).invoke(telephonyManager, new Object[0])).intValue();
                } catch (Exception e) {
                    String str = f5984a;
                    Log.e(str, "getPhoneCount: " + e.getMessage());
                }
            }
            PhoneTypes phoneTypes2 = PhoneTypes.Unknown;
            int phoneType = telephonyManager.getPhoneType();
            if (phoneType == 0) {
                phoneTypes = PhoneTypes.None;
            } else if (phoneType == 1) {
                phoneTypes = PhoneTypes.GSM;
            } else if (phoneType == 2) {
                phoneTypes = PhoneTypes.CDMA;
            } else if (phoneType != 3) {
                phoneTypes = PhoneTypes.Unknown;
            } else {
                phoneTypes = PhoneTypes.SIP;
            }
            aVar.PhoneType = phoneTypes;
        }
        aVar.IsRooted = a();
        if (Build.VERSION.SDK_INT <= 24) {
            strArr = g.a("/proc/version");
        } else {
            strArr = g.b("uname -a");
        }
        if (strArr.length > 0) {
            aVar.OsSystemVersion = h.a(strArr[0]);
        }
        t tVar = new t();
        tVar.f6398a = true;
        aVar.BluetoothInfo$3e5b9058 = tVar;
        aVar.PowerSaveMode = i(context);
        aVar.MultiSimInfo = g(context);
        aVar.HostAppInfo$41202ccd = h(context);
        return aVar;
    }

    @SuppressLint({"NewApi"})
    public static com.startapp.networkTest.data.b b(Context context) {
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        ((ActivityManager) context.getSystemService("activity")).getMemoryInfo(memoryInfo);
        com.startapp.networkTest.data.b bVar = new com.startapp.networkTest.data.b();
        long j = memoryInfo.availMem;
        bVar.MemoryFree = j;
        if (Build.VERSION.SDK_INT >= 16) {
            long j2 = memoryInfo.totalMem;
            bVar.MemoryTotal = j2;
            bVar.MemoryUsed = j2 - j;
        }
        if (memoryInfo.lowMemory) {
            bVar.MemoryState = MemoryStates.Low;
        } else {
            bVar.MemoryState = MemoryStates.Normal;
        }
        return bVar;
    }

    public static e c(Context context) {
        e eVar = new e();
        StatFs statFs = new StatFs(Environment.getDataDirectory().getPath());
        long blockSize = (long) statFs.getBlockSize();
        eVar.StorageInternalSize = ((long) statFs.getBlockCount()) * blockSize;
        eVar.StorageInternalAvailable = blockSize * ((long) statFs.getAvailableBlocks());
        eVar.StorageInternalAudio = a(context, MediaStore.Audio.Media.INTERNAL_CONTENT_URI);
        eVar.StorageInternalImages = a(context, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        eVar.StorageInternalVideo = a(context, MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        if (b()) {
            try {
                StatFs statFs2 = new StatFs(Environment.getExternalStorageDirectory().getPath());
                long blockSize2 = (long) statFs2.getBlockSize();
                eVar.StorageExternalSize = ((long) statFs2.getBlockCount()) * blockSize2;
                eVar.StorageExternalAvailable = blockSize2 * ((long) statFs2.getAvailableBlocks());
            } catch (IllegalArgumentException unused) {
                eVar.StorageExternalSize = -1;
                eVar.StorageExternalAvailable = -1;
            }
            if (context.checkCallingOrSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == 0) {
                eVar.StorageExternalAudio = a(context, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
                eVar.StorageExternalImages = a(context, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                eVar.StorageExternalVideo = a(context, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            }
        }
        return eVar;
    }

    public static ScreenStates d(Context context) {
        ScreenStates screenStates = ScreenStates.Unknown;
        PowerManager powerManager = (PowerManager) context.getSystemService("power");
        if (powerManager == null) {
            return screenStates;
        }
        return powerManager.isScreenOn() ? ScreenStates.On : ScreenStates.Off;
    }

    public static IdleStates e(Context context) {
        PowerManager powerManager;
        IdleStates idleStates = IdleStates.Unknown;
        if (Build.VERSION.SDK_INT < 23 || (powerManager = (PowerManager) context.getSystemService("power")) == null) {
            return idleStates;
        }
        if (Build.VERSION.SDK_INT >= 24) {
            try {
                if (((Boolean) powerManager.getClass().getDeclaredMethod("isLightDeviceIdleMode", new Class[0]).invoke(powerManager, new Object[0])).booleanValue()) {
                    idleStates = IdleStates.LightIdle;
                }
            } catch (Exception e) {
                String str = f5984a;
                Log.e(str, "getIdleState: " + e.getMessage());
            }
        }
        if (idleStates == IdleStates.LightIdle) {
            return idleStates;
        }
        return powerManager.isDeviceIdleMode() ? IdleStates.DeepIdle : IdleStates.NonIdle;
    }

    public static com.startapp.networkTest.data.a.b f(Context context) {
        com.startapp.networkTest.data.a.a g = g(context);
        Iterator<com.startapp.networkTest.data.a.b> it2 = g.SimInfos.iterator();
        while (it2.hasNext()) {
            com.startapp.networkTest.data.a.b next = it2.next();
            if (next.SubscriptionId == g.DefaultDataSimId) {
                return next;
            }
        }
        return new com.startapp.networkTest.data.a.b();
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(14:180|(2:181|182)|(4:186|187|188|199)|200|201|(10:205|206|(2:211|(1:213)(3:214|215|(1:217)(1:218)))(1:208)|219|222|223|(7:227|228|229|230|231|242|240)|233|243|240)|220|222|223|(0)|233|243|240|178) */
    /* JADX WARNING: Can't wrap try/catch for region: R(15:6|(4:10|(27:13|14|15|(1:17)|20|21|(1:23)|26|27|(3:29|(1:31)(2:32|(1:(1:35)(2:36|(1:38)(1:39))))|40)|43|(1:45)(1:46)|47|(1:49)(1:50)|51|(1:53)(1:54)|55|(1:57)(1:58)|59|60|(3:62|63|(2:65|66))|(2:237|78)|69|70|239|78|11)|236|86)|(2:87|88)|(2:92|93)|(3:96|97|98)|(2:99|100)|(2:104|105)|(3:108|109|110)|(2:111|112)|(2:116|117)|(3:120|121|122)|123|124|(2:128|129)|(3:132|133|134)) */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x0280, code lost:
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:202:0x038c, code lost:
        r12 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:224:0x0408, code lost:
        r12 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:111:0x0241 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:123:0x0272 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:200:0x037a */
    /* JADX WARNING: Missing exception handler attribute for start block: B:222:0x03f6 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:99:0x0210 */
    /* JADX WARNING: Removed duplicated region for block: B:104:0x0221 A[SYNTHETIC, Splitter:B:104:0x0221] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x0232  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0252 A[SYNTHETIC, Splitter:B:116:0x0252] */
    /* JADX WARNING: Removed duplicated region for block: B:120:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0283 A[SYNTHETIC, Splitter:B:128:0x0283] */
    /* JADX WARNING: Removed duplicated region for block: B:132:0x0294  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x02c1 A[SYNTHETIC, Splitter:B:148:0x02c1] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0304 A[Catch:{ Exception -> 0x031c }] */
    /* JADX WARNING: Removed duplicated region for block: B:175:0x0318 A[Catch:{ Exception -> 0x031c }] */
    /* JADX WARNING: Removed duplicated region for block: B:180:0x0328  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x038f A[SYNTHETIC, Splitter:B:205:0x038f] */
    /* JADX WARNING: Removed duplicated region for block: B:227:0x040b A[SYNTHETIC, Splitter:B:227:0x040b] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x01c6 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:82:0x01d0 A[SYNTHETIC, Splitter:B:82:0x01d0] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.startapp.networkTest.data.a.a g(android.content.Context r25) {
        /*
            r1 = r25
            java.lang.String r2 = "type"
            java.lang.String r3 = "apn"
            java.lang.String r4 = "getMultiSimInfo: "
            com.startapp.networkTest.data.a.a r5 = new com.startapp.networkTest.data.a.a
            r5.<init>()
            int r0 = android.os.Build.VERSION.SDK_INT
            r6 = 22
            if (r0 < r6) goto L_0x042a
            java.lang.String r0 = "android.permission.READ_PHONE_STATE"
            int r0 = r1.checkCallingOrSelfPermission(r0)
            if (r0 != 0) goto L_0x042a
            java.lang.String r0 = "telephony_subscription_service"
            java.lang.Object r0 = r1.getSystemService(r0)
            r6 = r0
            android.telephony.SubscriptionManager r6 = (android.telephony.SubscriptionManager) r6
            java.lang.String r7 = ""
            java.lang.String r8 = "*"
            java.lang.String r9 = "[\\d\\w]"
            r10 = 2
            r13 = 1
            if (r6 == 0) goto L_0x02a5
            int r0 = r6.getActiveSubscriptionInfoCount()
            r5.ActiveSimCount = r0
            int r0 = r6.getActiveSubscriptionInfoCountMax()
            r5.ActiveSimCountMax = r0
            java.util.List r0 = r6.getActiveSubscriptionInfoList()
            if (r0 == 0) goto L_0x01df
            int r15 = r0.size()
            if (r15 <= 0) goto L_0x01df
            int r15 = r0.size()
            com.startapp.networkTest.data.a.b[] r15 = new com.startapp.networkTest.data.a.b[r15]
            java.util.Iterator r16 = r0.iterator()
            r17 = 0
        L_0x0052:
            boolean r0 = r16.hasNext()
            if (r0 == 0) goto L_0x01d4
            java.lang.Object r0 = r16.next()
            r18 = r0
            android.telephony.SubscriptionInfo r18 = (android.telephony.SubscriptionInfo) r18
            com.startapp.networkTest.data.a.b r12 = new com.startapp.networkTest.data.a.b
            r12.<init>()
            java.lang.CharSequence r0 = r18.getCarrierName()     // Catch:{ Exception -> 0x007a }
            if (r0 == 0) goto L_0x0090
            java.lang.CharSequence r0 = r18.getCarrierName()     // Catch:{ Exception -> 0x007a }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x007a }
            java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)     // Catch:{ Exception -> 0x007a }
            r12.CarrierName = r0     // Catch:{ Exception -> 0x007a }
            goto L_0x0090
        L_0x007a:
            r0 = move-exception
            java.lang.String r11 = f5984a
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>(r4)
            java.lang.String r0 = r0.getMessage()
            r14.append(r0)
            java.lang.String r0 = r14.toString()
            android.util.Log.e(r11, r0)
        L_0x0090:
            java.lang.String r0 = r18.getCountryIso()     // Catch:{ Exception -> 0x00a1 }
            if (r0 == 0) goto L_0x00b7
            java.lang.String r0 = r18.getCountryIso()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)     // Catch:{ Exception -> 0x00a1 }
            r12.CountryIso = r0     // Catch:{ Exception -> 0x00a1 }
            goto L_0x00b7
        L_0x00a1:
            r0 = move-exception
            java.lang.String r11 = f5984a
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>(r4)
            java.lang.String r0 = r0.getMessage()
            r14.append(r0)
            java.lang.String r0 = r14.toString()
            android.util.Log.e(r11, r0)
        L_0x00b7:
            java.lang.String r0 = r18.getIccId()     // Catch:{ Exception -> 0x0113 }
            if (r0 == 0) goto L_0x0129
            java.lang.String r0 = r18.getIccId()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)     // Catch:{ Exception -> 0x0113 }
            int r11 = r0.length()     // Catch:{ Exception -> 0x0113 }
            if (r11 != 0) goto L_0x00cc
            goto L_0x0110
        L_0x00cc:
            int[] r11 = com.startapp.networkTest.controller.b.AnonymousClass1.f5985a     // Catch:{ Exception -> 0x0113 }
            com.startapp.networkTest.a r14 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x0113 }
            com.startapp.networkTest.enums.AnonymizationLevel r14 = r14.u()     // Catch:{ Exception -> 0x0113 }
            int r14 = r14.ordinal()     // Catch:{ Exception -> 0x0113 }
            r11 = r11[r14]     // Catch:{ Exception -> 0x0113 }
            if (r11 == r13) goto L_0x0110
            if (r11 == r10) goto L_0x00e2
            r0 = r7
            goto L_0x0110
        L_0x00e2:
            int r11 = r0.length()     // Catch:{ Exception -> 0x0113 }
            r14 = 11
            if (r11 < r14) goto L_0x010c
            r11 = 7
            r14 = 0
            java.lang.String r10 = r0.substring(r14, r11)     // Catch:{ Exception -> 0x0113 }
            int r14 = r0.length()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r0.substring(r11, r14)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r0.replaceAll(r9, r8)     // Catch:{ Exception -> 0x0113 }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113 }
            r11.<init>()     // Catch:{ Exception -> 0x0113 }
            r11.append(r10)     // Catch:{ Exception -> 0x0113 }
            r11.append(r0)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r11.toString()     // Catch:{ Exception -> 0x0113 }
            goto L_0x0110
        L_0x010c:
            java.lang.String r0 = r0.replaceAll(r9, r8)     // Catch:{ Exception -> 0x0113 }
        L_0x0110:
            r12.IccId = r0     // Catch:{ Exception -> 0x0113 }
            goto L_0x0129
        L_0x0113:
            r0 = move-exception
            java.lang.String r10 = f5984a
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>(r4)
            java.lang.String r0 = r0.getMessage()
            r11.append(r0)
            java.lang.String r0 = r11.toString()
            android.util.Log.e(r10, r0)
        L_0x0129:
            int r0 = r18.getMcc()
            r10 = 2147483647(0x7fffffff, float:NaN)
            if (r0 != r10) goto L_0x0134
            r11 = -1
            goto L_0x0138
        L_0x0134:
            int r11 = r18.getMcc()
        L_0x0138:
            r12.Mcc = r11
            int r0 = r18.getMnc()
            if (r0 != r10) goto L_0x0142
            r11 = -1
            goto L_0x0146
        L_0x0142:
            int r11 = r18.getMnc()
        L_0x0146:
            r12.Mnc = r11
            int r0 = r18.getSimSlotIndex()
            r12.SimSlotIndex = r0
            int r0 = r18.getSubscriptionId()
            r12.SubscriptionId = r0
            int r0 = r18.getDataRoaming()
            if (r0 != r13) goto L_0x015c
            r0 = 1
            goto L_0x015d
        L_0x015c:
            r0 = 0
        L_0x015d:
            r12.DataRoaming = r0
            int r0 = r12.SubscriptionId
            r10 = -1
            if (r0 == r10) goto L_0x016f
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.String r11 = "content://telephony/carriers/preferapn/subId/"
            java.lang.String r0 = r11.concat(r0)
            goto L_0x0171
        L_0x016f:
            java.lang.String r0 = "content://telephony/carriers/preferapn"
        L_0x0171:
            android.net.Uri r20 = android.net.Uri.parse(r0)     // Catch:{ Exception -> 0x01b3, all -> 0x01b0 }
            android.content.ContentResolver r19 = r25.getContentResolver()     // Catch:{ Exception -> 0x01b3, all -> 0x01b0 }
            java.lang.String[] r21 = new java.lang.String[]{r3, r2}     // Catch:{ Exception -> 0x01b3, all -> 0x01b0 }
            r22 = 0
            r23 = 0
            r24 = 0
            android.database.Cursor r11 = r19.query(r20, r21, r22, r23, r24)     // Catch:{ Exception -> 0x01b3, all -> 0x01b0 }
            if (r11 == 0) goto L_0x01aa
            boolean r0 = r11.moveToFirst()     // Catch:{ Exception -> 0x01a8 }
            if (r0 == 0) goto L_0x01aa
            int r0 = r11.getColumnIndex(r3)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r0 = r11.getString(r0)     // Catch:{ Exception -> 0x01a8 }
            int r14 = r11.getColumnIndex(r2)     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r14 = r11.getString(r14)     // Catch:{ Exception -> 0x01a8 }
            r12.Apn = r0     // Catch:{ Exception -> 0x01a8 }
            r12.ApnTypes = r14     // Catch:{ Exception -> 0x01a8 }
            r11.close()     // Catch:{ Exception -> 0x01a8 }
            r11 = 0
            goto L_0x01aa
        L_0x01a8:
            r0 = move-exception
            goto L_0x01b5
        L_0x01aa:
            if (r11 == 0) goto L_0x01c6
        L_0x01ac:
            r11.close()     // Catch:{ Exception -> 0x01c6 }
            goto L_0x01c6
        L_0x01b0:
            r0 = move-exception
            r11 = 0
            goto L_0x01ce
        L_0x01b3:
            r0 = move-exception
            r11 = 0
        L_0x01b5:
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x01cd }
            java.lang.String r10 = "saveApnItemsToSimInfo: "
            r14.<init>(r10)     // Catch:{ all -> 0x01cd }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01cd }
            r14.append(r0)     // Catch:{ all -> 0x01cd }
            if (r11 == 0) goto L_0x01c6
            goto L_0x01ac
        L_0x01c6:
            r15[r17] = r12
            int r17 = r17 + 1
            r10 = 2
            goto L_0x0052
        L_0x01cd:
            r0 = move-exception
        L_0x01ce:
            if (r11 == 0) goto L_0x01d3
            r11.close()     // Catch:{ Exception -> 0x01d3 }
        L_0x01d3:
            throw r0
        L_0x01d4:
            java.util.ArrayList r0 = new java.util.ArrayList
            java.util.List r2 = java.util.Arrays.asList(r15)
            r0.<init>(r2)
            r5.SimInfos = r0
        L_0x01df:
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x01ed }
            java.lang.String r2 = "getDefaultDataSubscriptionId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x01ed }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x01ed }
            goto L_0x01ee
        L_0x01ed:
            r12 = 0
        L_0x01ee:
            if (r12 != 0) goto L_0x01ff
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x01fe }
            java.lang.String r2 = "getDefaultDataSubId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x01fe }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x01fe }
            goto L_0x01ff
        L_0x01fe:
        L_0x01ff:
            if (r12 == 0) goto L_0x0210
            r2 = 0
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0210 }
            java.lang.Object r0 = r12.invoke(r6, r0)     // Catch:{ Exception -> 0x0210 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0210 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0210 }
            r5.DefaultDataSimId = r0     // Catch:{ Exception -> 0x0210 }
        L_0x0210:
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x021e }
            java.lang.String r2 = "getDefaultSmsSubscriptionId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x021e }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x021e }
            goto L_0x021f
        L_0x021e:
            r12 = 0
        L_0x021f:
            if (r12 != 0) goto L_0x0230
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x022f }
            java.lang.String r2 = "getDefaultSmsSubId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x022f }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x022f }
            goto L_0x0230
        L_0x022f:
        L_0x0230:
            if (r12 == 0) goto L_0x0241
            r2 = 0
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0241 }
            java.lang.Object r0 = r12.invoke(r6, r0)     // Catch:{ Exception -> 0x0241 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0241 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0241 }
            r5.DefaultSmsSimId = r0     // Catch:{ Exception -> 0x0241 }
        L_0x0241:
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x024f }
            java.lang.String r2 = "getDefaultSubscriptionId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x024f }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x024f }
            goto L_0x0250
        L_0x024f:
            r12 = 0
        L_0x0250:
            if (r12 != 0) goto L_0x0261
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x0260 }
            java.lang.String r2 = "getDefaultSubId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x0260 }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x0260 }
            goto L_0x0261
        L_0x0260:
        L_0x0261:
            if (r12 == 0) goto L_0x0272
            r2 = 0
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x0272 }
            java.lang.Object r0 = r12.invoke(r6, r0)     // Catch:{ Exception -> 0x0272 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0272 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0272 }
            r5.DefaultSimId = r0     // Catch:{ Exception -> 0x0272 }
        L_0x0272:
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x0280 }
            java.lang.String r2 = "getDefaultVoiceSubscriptionId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x0280 }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x0280 }
            goto L_0x0281
        L_0x0280:
            r12 = 0
        L_0x0281:
            if (r12 != 0) goto L_0x0292
            java.lang.Class r0 = r6.getClass()     // Catch:{ NoSuchMethodException -> 0x0291 }
            java.lang.String r2 = "getDefaultVoiceSubId"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x0291 }
            java.lang.reflect.Method r12 = r0.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x0291 }
            goto L_0x0292
        L_0x0291:
        L_0x0292:
            if (r12 == 0) goto L_0x02a5
            r2 = 0
            java.lang.Object[] r0 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x02a4 }
            java.lang.Object r0 = r12.invoke(r6, r0)     // Catch:{ Exception -> 0x02a4 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x02a4 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x02a4 }
            r5.DefaultVoiceSimId = r0     // Catch:{ Exception -> 0x02a4 }
            goto L_0x02a5
        L_0x02a4:
        L_0x02a5:
            java.lang.String r0 = "phone"
            java.lang.Object r0 = r1.getSystemService(r0)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
            if (r0 == 0) goto L_0x042a
            java.lang.Class r1 = r0.getClass()     // Catch:{ NoSuchMethodException -> 0x02bd }
            java.lang.String r2 = "getMultiSimConfiguration"
            r3 = 0
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{ NoSuchMethodException -> 0x02be }
            java.lang.reflect.Method r12 = r1.getDeclaredMethod(r2, r4)     // Catch:{ NoSuchMethodException -> 0x02be }
            goto L_0x02bf
        L_0x02bd:
            r3 = 0
        L_0x02be:
            r12 = 0
        L_0x02bf:
            if (r12 == 0) goto L_0x031c
            java.lang.Object[] r1 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x031c }
            java.lang.Object r1 = r12.invoke(r0, r1)     // Catch:{ Exception -> 0x031c }
            boolean r2 = r1 instanceof java.lang.Enum     // Catch:{ Exception -> 0x031c }
            if (r2 == 0) goto L_0x031c
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x031c }
            int r2 = r1.hashCode()     // Catch:{ Exception -> 0x031c }
            r3 = 2107724(0x20294c, float:2.95355E-39)
            if (r2 == r3) goto L_0x02f7
            r3 = 2107742(0x20295e, float:2.953576E-39)
            if (r2 == r3) goto L_0x02ed
            r3 = 2584894(0x27713e, float:3.622208E-39)
            if (r2 == r3) goto L_0x02e3
            goto L_0x0301
        L_0x02e3:
            java.lang.String r2 = "TSTS"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x031c }
            if (r1 == 0) goto L_0x0301
            r1 = 2
            goto L_0x0302
        L_0x02ed:
            java.lang.String r2 = "DSDS"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x031c }
            if (r1 == 0) goto L_0x0301
            r1 = 1
            goto L_0x0302
        L_0x02f7:
            java.lang.String r2 = "DSDA"
            boolean r1 = r1.equals(r2)     // Catch:{ Exception -> 0x031c }
            if (r1 == 0) goto L_0x0301
            r1 = 0
            goto L_0x0302
        L_0x0301:
            r1 = -1
        L_0x0302:
            if (r1 == 0) goto L_0x0318
            if (r1 == r13) goto L_0x0313
            r2 = 2
            if (r1 == r2) goto L_0x030e
            com.startapp.networkTest.enums.MultiSimVariants r1 = com.startapp.networkTest.enums.MultiSimVariants.Unknown     // Catch:{ Exception -> 0x031c }
            r5.MultiSimVariant = r1     // Catch:{ Exception -> 0x031c }
            goto L_0x031c
        L_0x030e:
            com.startapp.networkTest.enums.MultiSimVariants r1 = com.startapp.networkTest.enums.MultiSimVariants.TSTS     // Catch:{ Exception -> 0x031c }
            r5.MultiSimVariant = r1     // Catch:{ Exception -> 0x031c }
            goto L_0x031c
        L_0x0313:
            com.startapp.networkTest.enums.MultiSimVariants r1 = com.startapp.networkTest.enums.MultiSimVariants.DSDS     // Catch:{ Exception -> 0x031c }
            r5.MultiSimVariant = r1     // Catch:{ Exception -> 0x031c }
            goto L_0x031c
        L_0x0318:
            com.startapp.networkTest.enums.MultiSimVariants r1 = com.startapp.networkTest.enums.MultiSimVariants.DSDA     // Catch:{ Exception -> 0x031c }
            r5.MultiSimVariant = r1     // Catch:{ Exception -> 0x031c }
        L_0x031c:
            java.util.ArrayList<com.startapp.networkTest.data.a.b> r1 = r5.SimInfos
            java.util.Iterator r1 = r1.iterator()
        L_0x0322:
            boolean r2 = r1.hasNext()
            if (r2 == 0) goto L_0x042a
            java.lang.Object r2 = r1.next()
            com.startapp.networkTest.data.a.b r2 = (com.startapp.networkTest.data.a.b) r2
            java.lang.Class r3 = r0.getClass()     // Catch:{ NoSuchMethodException -> 0x0340 }
            java.lang.String r4 = "getSimState"
            java.lang.Class[] r6 = new java.lang.Class[r13]     // Catch:{ NoSuchMethodException -> 0x0340 }
            java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0340 }
            r11 = 0
            r6[r11] = r10     // Catch:{ NoSuchMethodException -> 0x0340 }
            java.lang.reflect.Method r12 = r3.getDeclaredMethod(r4, r6)     // Catch:{ NoSuchMethodException -> 0x0340 }
            goto L_0x0341
        L_0x0340:
            r12 = 0
        L_0x0341:
            if (r12 == 0) goto L_0x037a
            java.lang.Object[] r3 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x037a }
            int r4 = r2.SimSlotIndex     // Catch:{ Exception -> 0x037a }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x037a }
            r6 = 0
            r3[r6] = r4     // Catch:{ Exception -> 0x037a }
            java.lang.Object r3 = r12.invoke(r0, r3)     // Catch:{ Exception -> 0x037a }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ Exception -> 0x037a }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x037a }
            switch(r3) {
                case 1: goto L_0x0376;
                case 2: goto L_0x0373;
                case 3: goto L_0x0370;
                case 4: goto L_0x036d;
                case 5: goto L_0x036a;
                case 6: goto L_0x0367;
                case 7: goto L_0x0364;
                case 8: goto L_0x0361;
                case 9: goto L_0x035e;
                default: goto L_0x035b;
            }     // Catch:{ Exception -> 0x037a }
        L_0x035b:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.Unknown     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x035e:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.CardRestricted     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x0361:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.CardIoError     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x0364:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.PermanentlyDisabled     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x0367:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.NotReady     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x036a:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.Ready     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x036d:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.NetworkLocked     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x0370:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.PukRequired     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x0373:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.PinRequired     // Catch:{ Exception -> 0x037a }
            goto L_0x0378
        L_0x0376:
            com.startapp.networkTest.enums.SimStates r3 = com.startapp.networkTest.enums.SimStates.Absent     // Catch:{ Exception -> 0x037a }
        L_0x0378:
            r2.SimState = r3     // Catch:{ Exception -> 0x037a }
        L_0x037a:
            java.lang.Class r3 = r0.getClass()     // Catch:{ NoSuchMethodException -> 0x038c }
            java.lang.String r4 = "getSubscriberId"
            java.lang.Class[] r6 = new java.lang.Class[r13]     // Catch:{ NoSuchMethodException -> 0x038c }
            java.lang.Class r10 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x038c }
            r11 = 0
            r6[r11] = r10     // Catch:{ NoSuchMethodException -> 0x038c }
            java.lang.reflect.Method r12 = r3.getDeclaredMethod(r4, r6)     // Catch:{ NoSuchMethodException -> 0x038c }
            goto L_0x038d
        L_0x038c:
            r12 = 0
        L_0x038d:
            if (r12 == 0) goto L_0x03f5
            java.lang.Object[] r3 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x03f5 }
            int r4 = r2.SubscriptionId     // Catch:{ Exception -> 0x03f5 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x03f5 }
            r6 = 0
            r3[r6] = r4     // Catch:{ Exception -> 0x03f5 }
            java.lang.Object r3 = r12.invoke(r0, r3)     // Catch:{ Exception -> 0x03f5 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x03f5 }
            java.lang.String r3 = com.startapp.networkTest.utils.h.a(r3)     // Catch:{ Exception -> 0x03f5 }
            int r4 = r3.length()     // Catch:{ Exception -> 0x03f5 }
            if (r4 != 0) goto L_0x03ac
        L_0x03aa:
            r6 = 2
            goto L_0x03f2
        L_0x03ac:
            int[] r4 = com.startapp.networkTest.controller.b.AnonymousClass1.f5985a     // Catch:{ Exception -> 0x03f5 }
            com.startapp.networkTest.a r6 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x03f5 }
            com.startapp.networkTest.enums.AnonymizationLevel r6 = r6.v()     // Catch:{ Exception -> 0x03f5 }
            int r6 = r6.ordinal()     // Catch:{ Exception -> 0x03f5 }
            r4 = r4[r6]     // Catch:{ Exception -> 0x03f5 }
            if (r4 == r13) goto L_0x03aa
            r6 = 2
            if (r4 == r6) goto L_0x03c3
            r3 = r7
            goto L_0x03f2
        L_0x03c3:
            int r4 = r3.length()     // Catch:{ Exception -> 0x03f6 }
            r10 = 14
            if (r4 < r10) goto L_0x03ee
            r4 = 10
            r10 = 0
            java.lang.String r11 = r3.substring(r10, r4)     // Catch:{ Exception -> 0x03f6 }
            int r10 = r3.length()     // Catch:{ Exception -> 0x03f6 }
            java.lang.String r3 = r3.substring(r4, r10)     // Catch:{ Exception -> 0x03f6 }
            java.lang.String r3 = r3.replaceAll(r9, r8)     // Catch:{ Exception -> 0x03f6 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03f6 }
            r4.<init>()     // Catch:{ Exception -> 0x03f6 }
            r4.append(r11)     // Catch:{ Exception -> 0x03f6 }
            r4.append(r3)     // Catch:{ Exception -> 0x03f6 }
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x03f6 }
            goto L_0x03f2
        L_0x03ee:
            java.lang.String r3 = r3.replaceAll(r9, r8)     // Catch:{ Exception -> 0x03f6 }
        L_0x03f2:
            r2.IMSI = r3     // Catch:{ Exception -> 0x03f6 }
            goto L_0x03f6
        L_0x03f5:
            r6 = 2
        L_0x03f6:
            java.lang.Class r3 = r0.getClass()     // Catch:{ NoSuchMethodException -> 0x0408 }
            java.lang.String r4 = "getGroupIdLevel1"
            java.lang.Class[] r10 = new java.lang.Class[r13]     // Catch:{ NoSuchMethodException -> 0x0408 }
            java.lang.Class r11 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0408 }
            r12 = 0
            r10[r12] = r11     // Catch:{ NoSuchMethodException -> 0x0408 }
            java.lang.reflect.Method r12 = r3.getDeclaredMethod(r4, r10)     // Catch:{ NoSuchMethodException -> 0x0408 }
            goto L_0x0409
        L_0x0408:
            r12 = 0
        L_0x0409:
            if (r12 == 0) goto L_0x0427
            java.lang.Object[] r3 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x0427 }
            int r4 = r2.SubscriptionId     // Catch:{ Exception -> 0x0427 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)     // Catch:{ Exception -> 0x0427 }
            r10 = 0
            r3[r10] = r4     // Catch:{ Exception -> 0x0424 }
            java.lang.Object r3 = r12.invoke(r0, r3)     // Catch:{ Exception -> 0x0424 }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x0424 }
            java.lang.String r3 = com.startapp.networkTest.utils.h.a(r3)     // Catch:{ Exception -> 0x0424 }
            r2.GroupIdentifierLevel1 = r3     // Catch:{ Exception -> 0x0424 }
            goto L_0x0322
        L_0x0424:
            goto L_0x0322
        L_0x0427:
            r10 = 0
            goto L_0x0322
        L_0x042a:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.b.g(android.content.Context):com.startapp.networkTest.data.a.a");
    }

    private static com.startapp.sdk.adsbase.k.a h(Context context) {
        com.startapp.sdk.adsbase.k.a aVar = new com.startapp.sdk.adsbase.k.a();
        aVar.f6409a = context.getPackageName();
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null) {
            try {
                applicationInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException unused) {
            }
        }
        if (applicationInfo != null) {
            aVar.d = applicationInfo.targetSdkVersion;
            aVar.c = (String) applicationInfo.loadLabel(context.getPackageManager());
            if (Build.VERSION.SDK_INT >= 26) {
                int i = applicationInfo.category;
                AppCategoryTypes appCategoryTypes = AppCategoryTypes.Unknown;
                switch (i) {
                    case -1:
                        appCategoryTypes = AppCategoryTypes.Undefined;
                        break;
                    case 0:
                        appCategoryTypes = AppCategoryTypes.Game;
                        break;
                    case 1:
                        appCategoryTypes = AppCategoryTypes.Audio;
                        break;
                    case 2:
                        appCategoryTypes = AppCategoryTypes.Video;
                        break;
                    case 3:
                        appCategoryTypes = AppCategoryTypes.Image;
                        break;
                    case 4:
                        appCategoryTypes = AppCategoryTypes.Social;
                        break;
                    case 5:
                        appCategoryTypes = AppCategoryTypes.News;
                        break;
                    case 6:
                        appCategoryTypes = AppCategoryTypes.Maps;
                        break;
                    case 7:
                        appCategoryTypes = AppCategoryTypes.Productivity;
                        break;
                }
                aVar.b = appCategoryTypes;
            }
        }
        ArrayList<j> arrayList = new ArrayList<>();
        try {
            for (String str : context.getPackageManager().getPackageInfo(context.getPackageName(), 4096).requestedPermissions) {
                j jVar = new j();
                jVar.f6387a = str.toLowerCase();
                int i2 = 1;
                if (str.equalsIgnoreCase("android.permission.PACKAGE_USAGE_STATS")) {
                    if (!(Build.VERSION.SDK_INT < 21 || ((AppOpsManager) context.getApplicationContext().getSystemService("appops")).checkOpNoThrow("android:get_usage_stats", Process.myUid(), context.getApplicationContext().getPackageName()) == 0)) {
                        i2 = 0;
                    }
                    jVar.b = i2;
                } else {
                    if (context.checkPermission(str, Process.myPid(), Process.myUid()) != 0) {
                        i2 = 0;
                    }
                    jVar.b = i2;
                }
                arrayList.add(jVar);
            }
        } catch (Exception unused2) {
        } catch (Throwable th) {
            aVar.e = arrayList;
            throw th;
        }
        aVar.e = arrayList;
        return aVar;
    }

    private static ThreeState i(Context context) {
        try {
            String string = Settings.System.getString(context.getContentResolver(), "user_powersaver_enable");
            if (string != null) {
                return string.equals(DiskLruCache.VERSION_1) ? ThreeState.Enabled : ThreeState.Disabled;
            }
            if (Build.VERSION.SDK_INT >= 21) {
                if (Build.MANUFACTURER.toLowerCase().startsWith("sony") && Build.VERSION.SDK_INT < 23) {
                    return ThreeState.Unknown;
                }
                PowerManager powerManager = (PowerManager) context.getSystemService("power");
                if (powerManager != null) {
                    return powerManager.isPowerSaveMode() ? ThreeState.Enabled : ThreeState.Disabled;
                }
            }
            return ThreeState.Unknown;
        } catch (Exception e) {
            String str = f5984a;
            Log.e(str, "getPowerSaveMode: " + e.getMessage());
        }
    }

    private static boolean b() {
        try {
            return Environment.getExternalStorageState().equals("mounted");
        } catch (Exception e) {
            new StringBuilder("isExternalMemoryAvailable: ").append(e.getMessage());
            return false;
        }
    }

    private static boolean a() {
        String[] strArr = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (int i = 0; i < 10; i++) {
            if (new File(strArr[i]).exists()) {
                return true;
            }
        }
        return false;
    }

    private static long a(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            cursor = context.getContentResolver().query(uri, new String[]{"_size"}, (String) null, (String[]) null, (String) null);
            long j = 0;
            if (cursor != null) {
                if (cursor.getCount() == 0) {
                    if (cursor != null) {
                        cursor.close();
                    }
                    return 0;
                }
                while (cursor.moveToNext()) {
                    j += cursor.getLong(cursor.getColumnIndexOrThrow("_size"));
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            return j;
        } catch (Exception e) {
            e.printStackTrace();
            if (cursor == null) {
                return -1;
            }
            cursor.close();
            return -1;
        } catch (Throwable th) {
            if (cursor != null) {
                cursor.close();
            }
            throw th;
        }
    }

    public static f a(d dVar) {
        f fVar = new f();
        fVar.MobileRxBytes = i.b();
        fVar.MobileTxBytes = i.a();
        fVar.TotalRxBytes = TrafficStats.getTotalRxBytes();
        fVar.TotalTxBytes = TrafficStats.getTotalTxBytes();
        if (dVar != null) {
            fVar.WifiRxBytes = dVar.b();
            fVar.WifiTxBytes = dVar.c();
        } else {
            fVar.WifiRxBytes = -1;
            fVar.WifiTxBytes = -1;
        }
        return fVar;
    }
}
