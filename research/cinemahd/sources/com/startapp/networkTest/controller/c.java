package com.startapp.networkTest.controller;

import android.annotation.TargetApi;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.provider.Settings;
import android.telephony.CellIdentity;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.CellSignalStrength;
import android.telephony.CellSignalStrengthCdma;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthNr;
import android.telephony.PhoneStateListener;
import android.telephony.ServiceState;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.radio.ApnInfo;
import com.startapp.networkTest.data.radio.NetworkRegistrationInfo;
import com.startapp.networkTest.enums.CellConnectionStatus;
import com.startapp.networkTest.enums.CellNetworkTypes;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.DuplexMode;
import com.startapp.networkTest.enums.NetworkGenerations;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.PreferredNetworkTypes;
import com.startapp.networkTest.enums.ServiceStates;
import com.startapp.networkTest.enums.ThreeStateShort;
import com.startapp.networkTest.enums.wifi.WifiDetailedStates;
import com.startapp.sdk.adsbase.cache.DiskAdCacheManager;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class c {
    /* access modifiers changed from: private */
    public static final String c = "c";
    /* access modifiers changed from: private */
    public Field A;
    /* access modifiers changed from: private */
    public Field B;
    /* access modifiers changed from: private */
    public Field C;
    /* access modifiers changed from: private */
    public Method D;
    /* access modifiers changed from: private */
    public Field E;
    private Field F;
    private Field G;
    private Field H;
    private Field I;
    private Method J;
    private Method K;
    private Method L;
    /* access modifiers changed from: private */
    public Method M;
    private Method N;
    private Method O;
    private Method P;
    private ContentResolver Q;
    /* access modifiers changed from: private */
    public int[] R;
    /* access modifiers changed from: private */
    public boolean S;

    /* renamed from: a  reason: collision with root package name */
    protected final Handler f5986a;
    protected final List<com.startapp.networkTest.controller.a.a> b;
    /* access modifiers changed from: private */
    public TelephonyManager d;
    /* access modifiers changed from: private */
    public SparseArray<TelephonyManager> e;
    /* access modifiers changed from: private */
    public Context f;
    private j g;
    private ArrayList<j> h;
    private ConnectivityManager i;
    /* access modifiers changed from: private */
    public d j;
    private SubscriptionManager.OnSubscriptionsChangedListener k;
    private com.startapp.networkTest.data.a.a l;
    private e m;
    /* access modifiers changed from: private */
    public List<CellInfo> n;
    /* access modifiers changed from: private */
    public Method o;
    /* access modifiers changed from: private */
    public Method p;
    /* access modifiers changed from: private */
    public Method q;
    /* access modifiers changed from: private */
    public Method r;
    /* access modifiers changed from: private */
    public Method s;
    /* access modifiers changed from: private */
    public Method t;
    /* access modifiers changed from: private */
    public Method u;
    /* access modifiers changed from: private */
    public Method v;
    private Method w;
    private Method x;
    /* access modifiers changed from: private */
    public Field y;
    private Field z;

    /* renamed from: com.startapp.networkTest.controller.c$4  reason: invalid class name */
    static /* synthetic */ class AnonymousClass4 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5990a = new int[NetworkTypes.values().length];
        private static /* synthetic */ int[] b = new int[NetworkGenerations.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(52:0|(2:1|2)|3|(2:5|6)|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(53:0|(2:1|2)|3|5|6|7|(2:9|10)|11|(2:13|14)|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(54:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Can't wrap try/catch for region: R(55:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|29|30|31|32|33|34|35|36|37|38|39|40|41|42|43|44|45|46|47|48|49|50|51|52|53|54|55|56|57|58|60) */
        /* JADX WARNING: Code restructure failed: missing block: B:61:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0048 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0052 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x005c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0066 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0071 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:29:0x007c */
        /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x0087 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:33:0x0093 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:35:0x009f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:37:0x00ab */
        /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x00b7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:41:0x00c3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x00cf */
        /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x00db */
        /* JADX WARNING: Missing exception handler attribute for start block: B:47:0x00e7 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:49:0x00f3 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:51:0x00ff */
        /* JADX WARNING: Missing exception handler attribute for start block: B:53:0x010b */
        /* JADX WARNING: Missing exception handler attribute for start block: B:55:0x0117 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:57:0x0123 */
        static {
            /*
                com.startapp.networkTest.enums.NetworkGenerations[] r0 = com.startapp.networkTest.enums.NetworkGenerations.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.enums.NetworkGenerations r2 = com.startapp.networkTest.enums.NetworkGenerations.Gen2     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.enums.NetworkGenerations r3 = com.startapp.networkTest.enums.NetworkGenerations.Gen3     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                r2 = 3
                int[] r3 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.enums.NetworkGenerations r4 = com.startapp.networkTest.enums.NetworkGenerations.Gen4     // Catch:{ NoSuchFieldError -> 0x002a }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r3[r4] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                r3 = 4
                int[] r4 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.networkTest.enums.NetworkGenerations r5 = com.startapp.networkTest.enums.NetworkGenerations.Gen5     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4[r5] = r3     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                com.startapp.networkTest.enums.NetworkTypes[] r4 = com.startapp.networkTest.enums.NetworkTypes.values()
                int r4 = r4.length
                int[] r4 = new int[r4]
                f5990a = r4
                int[] r4 = f5990a     // Catch:{ NoSuchFieldError -> 0x0048 }
                com.startapp.networkTest.enums.NetworkTypes r5 = com.startapp.networkTest.enums.NetworkTypes.GPRS     // Catch:{ NoSuchFieldError -> 0x0048 }
                int r5 = r5.ordinal()     // Catch:{ NoSuchFieldError -> 0x0048 }
                r4[r5] = r0     // Catch:{ NoSuchFieldError -> 0x0048 }
            L_0x0048:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0052 }
                com.startapp.networkTest.enums.NetworkTypes r4 = com.startapp.networkTest.enums.NetworkTypes.EDGE     // Catch:{ NoSuchFieldError -> 0x0052 }
                int r4 = r4.ordinal()     // Catch:{ NoSuchFieldError -> 0x0052 }
                r0[r4] = r1     // Catch:{ NoSuchFieldError -> 0x0052 }
            L_0x0052:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x005c }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.GSM     // Catch:{ NoSuchFieldError -> 0x005c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x005c }
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x005c }
            L_0x005c:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0066 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.Cdma1xRTT     // Catch:{ NoSuchFieldError -> 0x0066 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0066 }
                r0[r1] = r3     // Catch:{ NoSuchFieldError -> 0x0066 }
            L_0x0066:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0071 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.CDMA     // Catch:{ NoSuchFieldError -> 0x0071 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0071 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0071 }
            L_0x0071:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x007c }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.IDEN     // Catch:{ NoSuchFieldError -> 0x007c }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x007c }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x007c }
            L_0x007c:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0087 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.UMTS     // Catch:{ NoSuchFieldError -> 0x0087 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0087 }
                r2 = 7
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0087 }
            L_0x0087:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0093 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.EVDO_0     // Catch:{ NoSuchFieldError -> 0x0093 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0093 }
                r2 = 8
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0093 }
            L_0x0093:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x009f }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.EVDO_A     // Catch:{ NoSuchFieldError -> 0x009f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x009f }
                r2 = 9
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x009f }
            L_0x009f:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00ab }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.EVDO_B     // Catch:{ NoSuchFieldError -> 0x00ab }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ab }
                r2 = 10
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ab }
            L_0x00ab:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00b7 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.HSPA     // Catch:{ NoSuchFieldError -> 0x00b7 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00b7 }
                r2 = 11
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00b7 }
            L_0x00b7:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00c3 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.HSDPA     // Catch:{ NoSuchFieldError -> 0x00c3 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00c3 }
                r2 = 12
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00c3 }
            L_0x00c3:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00cf }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.HSPAP     // Catch:{ NoSuchFieldError -> 0x00cf }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00cf }
                r2 = 13
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00cf }
            L_0x00cf:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00db }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.HSUPA     // Catch:{ NoSuchFieldError -> 0x00db }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00db }
                r2 = 14
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00db }
            L_0x00db:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00e7 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.EHRPD     // Catch:{ NoSuchFieldError -> 0x00e7 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00e7 }
                r2 = 15
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00e7 }
            L_0x00e7:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00f3 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.TD_SCDMA     // Catch:{ NoSuchFieldError -> 0x00f3 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00f3 }
                r2 = 16
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00f3 }
            L_0x00f3:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x00ff }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.LTE     // Catch:{ NoSuchFieldError -> 0x00ff }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x00ff }
                r2 = 17
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x00ff }
            L_0x00ff:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x010b }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.LTE_CA     // Catch:{ NoSuchFieldError -> 0x010b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x010b }
                r2 = 18
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x010b }
            L_0x010b:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0117 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.NR     // Catch:{ NoSuchFieldError -> 0x0117 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0117 }
                r2 = 19
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0117 }
            L_0x0117:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x0123 }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.WiFi     // Catch:{ NoSuchFieldError -> 0x0123 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0123 }
                r2 = 20
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0123 }
            L_0x0123:
                int[] r0 = f5990a     // Catch:{ NoSuchFieldError -> 0x012f }
                com.startapp.networkTest.enums.NetworkTypes r1 = com.startapp.networkTest.enums.NetworkTypes.Unknown     // Catch:{ NoSuchFieldError -> 0x012f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x012f }
                r2 = 21
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x012f }
            L_0x012f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.AnonymousClass4.<clinit>():void");
        }
    }

    /* renamed from: com.startapp.networkTest.controller.c$c  reason: collision with other inner class name */
    class C0067c extends AsyncTask<Void, Void, Void> {
        C0067c() {
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            c.this.j();
            return null;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            c cVar = c.this;
            cVar.a(cVar.R);
            if (Build.VERSION.SDK_INT >= 29) {
                c.this.i();
            }
            c.this.a(false);
            boolean unused = c.this.S = false;
        }

        /* access modifiers changed from: protected */
        public final void onPreExecute() {
            boolean unused = c.this.S = true;
            c.this.b(false);
            int[] unused2 = c.this.R = new int[0];
        }
    }

    class d {

        /* renamed from: a  reason: collision with root package name */
        private SparseArray<i> f5994a = new SparseArray<>();
        private SparseArray<h> b = new SparseArray<>();
        private SparseArray<b> c = new SparseArray<>();
        private HashMap<String, f> d = new HashMap<>();
        private SparseArray<NetworkRegistrationInfo[]> e = new SparseArray<>();
        private SparseArray<g> f = new SparseArray<>();
        private Map<String, String> g = new HashMap();

        d() {
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, i iVar) {
            this.f5994a.put(i, iVar);
        }

        /* access modifiers changed from: package-private */
        public final h b(int i) {
            h hVar = this.b.get(i);
            return hVar == null ? new h(c.this, (byte) 0) : hVar;
        }

        /* access modifiers changed from: package-private */
        public final b c(int i) {
            return this.c.get(i);
        }

        /* access modifiers changed from: package-private */
        public final NetworkRegistrationInfo[] d(int i) {
            return this.e.get(i);
        }

        /* access modifiers changed from: package-private */
        public final g e(int i) {
            return this.f.get(i);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, h hVar) {
            this.b.put(i, hVar);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, b bVar) {
            this.c.put(i, bVar);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, NetworkRegistrationInfo[] networkRegistrationInfoArr) {
            this.e.put(i, networkRegistrationInfoArr);
        }

        /* access modifiers changed from: package-private */
        public final void a(String str, f fVar) {
            this.d.put(str, fVar);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, String str, String str2) {
            Map<String, String> map = this.g;
            map.put(i + str, str2);
        }

        /* access modifiers changed from: package-private */
        public final void a(int i, g gVar) {
            this.f.put(i, gVar);
        }

        /* access modifiers changed from: package-private */
        public final i a(int i) {
            i iVar = this.f5994a.get(i);
            return iVar == null ? new i(c.this, (byte) 0) : iVar;
        }

        /* access modifiers changed from: package-private */
        public final f a(String str) {
            return this.d.get(str);
        }

        /* access modifiers changed from: package-private */
        public final String a(int i, String str) {
            Map<String, String> map = this.g;
            StringBuilder sb = new StringBuilder();
            sb.append(i);
            sb.append(str != null ? str.split(",")[0] : "");
            String str2 = map.get(sb.toString());
            return str2 == null ? "" : str2;
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(38:0|(1:2)|3|(3:5|6|7)|9|(3:11|12|13)|15|(21:17|18|19|20|22|23|24|26|27|28|30|31|32|34|35|36|38|39|40|42|43)|44|(2:46|47)|48|(2:50|51)|52|(2:54|55)|57|(9:59|60|61|62|64|65|66|68|69)|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|(1:87)|(3:88|89|(1:91))|93|(12:95|96|97|98|100|101|102|104|105|106|108|109)|111|(2:113|114)(1:115)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(39:0|(1:2)|3|(3:5|6|7)|9|(3:11|12|13)|15|(21:17|18|19|20|22|23|24|26|27|28|30|31|32|34|35|36|38|39|40|42|43)|44|(2:46|47)|48|(2:50|51)|52|54|55|57|(9:59|60|61|62|64|65|66|68|69)|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|(1:87)|(3:88|89|(1:91))|93|(12:95|96|97|98|100|101|102|104|105|106|108|109)|111|(2:113|114)(1:115)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(40:0|(1:2)|3|(3:5|6|7)|9|(3:11|12|13)|15|(21:17|18|19|20|22|23|24|26|27|28|30|31|32|34|35|36|38|39|40|42|43)|44|(2:46|47)|48|50|51|52|54|55|57|(9:59|60|61|62|64|65|66|68|69)|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|(1:87)|(3:88|89|(1:91))|93|(12:95|96|97|98|100|101|102|104|105|106|108|109)|111|(2:113|114)(1:115)) */
    /* JADX WARNING: Can't wrap try/catch for region: R(43:0|(1:2)|3|(3:5|6|7)|9|(3:11|12|13)|15|(21:17|18|19|20|22|23|24|26|27|28|30|31|32|34|35|36|38|39|40|42|43)|44|46|47|48|50|51|52|54|55|57|(9:59|60|61|62|64|65|66|68|69)|70|71|72|73|74|75|76|77|78|79|80|81|82|83|84|85|(1:87)|88|89|(1:91)|93|(12:95|96|97|98|100|101|102|104|105|106|108|109)|111|(2:113|114)(1:115)) */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:70:0x0138 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:72:0x0146 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:74:0x0158 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:76:0x016c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:78:0x0180 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:80:0x0194 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:82:0x01a8 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:84:0x01bc */
    /* JADX WARNING: Missing exception handler attribute for start block: B:88:0x01d8 */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x0245  */
    /* JADX WARNING: Removed duplicated region for block: B:115:? A[RETURN, SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x01d1 A[Catch:{ Exception -> 0x01d8 }] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01f2 A[Catch:{ Exception -> 0x01fa }] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0201  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(android.content.Context r8) {
        /*
            r7 = this;
            java.lang.String r0 = "getVoiceNetworkType"
            java.lang.String r1 = "getDataEnabled"
            r7.<init>()
            r7.f = r8
            java.lang.String r2 = "phone"
            java.lang.Object r2 = r8.getSystemService(r2)
            android.telephony.TelephonyManager r2 = (android.telephony.TelephonyManager) r2
            r7.d = r2
            java.lang.String r2 = "connectivity"
            java.lang.Object r8 = r8.getSystemService(r2)
            android.net.ConnectivityManager r8 = (android.net.ConnectivityManager) r8
            r7.i = r8
            r7.j()
            int[] r8 = r7.R
            r7.a((int[]) r8)
            int r8 = android.os.Build.VERSION.SDK_INT
            r2 = 29
            if (r8 < r2) goto L_0x002e
            r7.i()
        L_0x002e:
            android.os.Handler r8 = new android.os.Handler
            android.os.Looper r3 = android.os.Looper.getMainLooper()
            r8.<init>(r3)
            r7.f5986a = r8
            java.util.concurrent.CopyOnWriteArrayList r8 = new java.util.concurrent.CopyOnWriteArrayList
            r8.<init>()
            r7.b = r8
            com.startapp.networkTest.data.a.a r8 = new com.startapp.networkTest.data.a.a
            r8.<init>()
            r7.l = r8
            com.startapp.networkTest.controller.c$d r8 = new com.startapp.networkTest.controller.c$d
            r8.<init>()
            r7.j = r8
            android.content.Context r8 = r7.f
            android.content.ContentResolver r8 = r8.getContentResolver()
            r7.Q = r8
            int r8 = android.os.Build.VERSION.SDK_INT
            r3 = 25
            r4 = 1
            if (r8 < r3) goto L_0x006e
            java.lang.Class<android.telephony.ServiceState> r8 = android.telephony.ServiceState.class
            java.lang.String r3 = "mIsUsingCarrierAggregation"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r3)     // Catch:{ Exception -> 0x006d }
            r7.E = r8     // Catch:{ Exception -> 0x006d }
            java.lang.reflect.Field r8 = r7.E     // Catch:{ Exception -> 0x006d }
            r8.setAccessible(r4)     // Catch:{ Exception -> 0x006d }
            goto L_0x006e
        L_0x006d:
        L_0x006e:
            int r8 = android.os.Build.VERSION.SDK_INT
            r3 = 0
            if (r8 < r2) goto L_0x0081
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r5 = "isUsingCarrierAggregation"
            java.lang.Class[] r6 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0080 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r5, r6)     // Catch:{ Exception -> 0x0080 }
            r7.D = r8     // Catch:{ Exception -> 0x0080 }
            goto L_0x0081
        L_0x0080:
        L_0x0081:
            int r8 = android.os.Build.VERSION.SDK_INT
            if (r8 >= r2) goto L_0x00d9
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getLteSignalStrength"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0091 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x0091 }
            r7.p = r8     // Catch:{ Exception -> 0x0091 }
        L_0x0091:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getLteCqi"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x009d }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x009d }
            r7.s = r8     // Catch:{ Exception -> 0x009d }
        L_0x009d:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getLteRsrp"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00a9 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x00a9 }
            r7.t = r8     // Catch:{ Exception -> 0x00a9 }
        L_0x00a9:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getLteRsrq"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00b5 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x00b5 }
            r7.u = r8     // Catch:{ Exception -> 0x00b5 }
        L_0x00b5:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getLteRssnr"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00c1 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x00c1 }
            r7.v = r8     // Catch:{ Exception -> 0x00c1 }
        L_0x00c1:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getLteDbm"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00cd }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x00cd }
            r7.q = r8     // Catch:{ Exception -> 0x00cd }
        L_0x00cd:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getDbm"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00d9 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x00d9 }
            r7.o = r8     // Catch:{ Exception -> 0x00d9 }
        L_0x00d9:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "getGsmEcno"
            java.lang.Class[] r5 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x00e5 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r2, r5)     // Catch:{ Exception -> 0x00e5 }
            r7.r = r8     // Catch:{ Exception -> 0x00e5 }
        L_0x00e5:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "mWcdmaRscp"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r2)     // Catch:{ NoSuchFieldException -> 0x00f4 }
            r7.y = r8     // Catch:{ NoSuchFieldException -> 0x00f4 }
            java.lang.reflect.Field r8 = r7.y     // Catch:{ NoSuchFieldException -> 0x00f4 }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x00f4 }
        L_0x00f4:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "mWcdmaEcio"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r2)     // Catch:{ NoSuchFieldException -> 0x0104 }
            r7.z = r8     // Catch:{ NoSuchFieldException -> 0x0104 }
            java.lang.reflect.Field r8 = r7.z     // Catch:{ NoSuchFieldException -> 0x0104 }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x0104 }
            goto L_0x0105
        L_0x0104:
        L_0x0105:
            int r8 = android.os.Build.VERSION.SDK_INT
            r2 = 28
            if (r8 < r2) goto L_0x0138
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "mNrRsrp"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r2)     // Catch:{ NoSuchFieldException -> 0x011a }
            r7.A = r8     // Catch:{ NoSuchFieldException -> 0x011a }
            java.lang.reflect.Field r8 = r7.A     // Catch:{ NoSuchFieldException -> 0x011a }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x011a }
        L_0x011a:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "mNrRsrq"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r2)     // Catch:{ NoSuchFieldException -> 0x0129 }
            r7.B = r8     // Catch:{ NoSuchFieldException -> 0x0129 }
            java.lang.reflect.Field r8 = r7.B     // Catch:{ NoSuchFieldException -> 0x0129 }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x0129 }
        L_0x0129:
            java.lang.Class<android.telephony.SignalStrength> r8 = android.telephony.SignalStrength.class
            java.lang.String r2 = "mNrRssnr"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r2)     // Catch:{ NoSuchFieldException -> 0x0138 }
            r7.C = r8     // Catch:{ NoSuchFieldException -> 0x0138 }
            java.lang.reflect.Field r8 = r7.C     // Catch:{ NoSuchFieldException -> 0x0138 }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x0138 }
        L_0x0138:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x0146 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x0146 }
            java.lang.Class[] r2 = new java.lang.Class[r3]     // Catch:{ Exception -> 0x0146 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0146 }
            r7.J = r8     // Catch:{ Exception -> 0x0146 }
        L_0x0146:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x0158 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x0158 }
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0158 }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0158 }
            r2[r3] = r5     // Catch:{ Exception -> 0x0158 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0158 }
            r7.K = r8     // Catch:{ Exception -> 0x0158 }
        L_0x0158:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x016c }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x016c }
            java.lang.String r1 = "isNetworkRoaming"
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x016c }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x016c }
            r2[r3] = r5     // Catch:{ Exception -> 0x016c }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x016c }
            r7.L = r8     // Catch:{ Exception -> 0x016c }
        L_0x016c:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x0180 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x0180 }
            java.lang.String r1 = "getNetworkType"
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0180 }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0180 }
            r2[r3] = r5     // Catch:{ Exception -> 0x0180 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0180 }
            r7.M = r8     // Catch:{ Exception -> 0x0180 }
        L_0x0180:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x0194 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "getNetworkOperatorName"
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x0194 }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0194 }
            r2[r3] = r5     // Catch:{ Exception -> 0x0194 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x0194 }
            r7.N = r8     // Catch:{ Exception -> 0x0194 }
        L_0x0194:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x01a8 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x01a8 }
            java.lang.String r1 = "getNetworkOperator"
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x01a8 }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x01a8 }
            r2[r3] = r5     // Catch:{ Exception -> 0x01a8 }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x01a8 }
            r7.O = r8     // Catch:{ Exception -> 0x01a8 }
        L_0x01a8:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x01bc }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x01bc }
            java.lang.String r1 = "getNetworkOperatorForSubscription"
            java.lang.Class[] r2 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x01bc }
            java.lang.Class r5 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x01bc }
            r2[r3] = r5     // Catch:{ Exception -> 0x01bc }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r1, r2)     // Catch:{ Exception -> 0x01bc }
            r7.P = r8     // Catch:{ Exception -> 0x01bc }
        L_0x01bc:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x01d8 }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x01d8 }
            r1 = 0
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r0, r1)     // Catch:{ Exception -> 0x01d8 }
            int r1 = r8.getModifiers()     // Catch:{ Exception -> 0x01d8 }
            boolean r1 = java.lang.reflect.Modifier.isAbstract(r1)     // Catch:{ Exception -> 0x01d8 }
            if (r1 != 0) goto L_0x01d8
            r7.w = r8     // Catch:{ Exception -> 0x01d8 }
            java.lang.reflect.Method r8 = r7.w     // Catch:{ Exception -> 0x01d8 }
            r8.setAccessible(r4)     // Catch:{ Exception -> 0x01d8 }
        L_0x01d8:
            android.telephony.TelephonyManager r8 = r7.d     // Catch:{ Exception -> 0x01fa }
            java.lang.Class r8 = r8.getClass()     // Catch:{ Exception -> 0x01fa }
            java.lang.Class[] r1 = new java.lang.Class[r4]     // Catch:{ Exception -> 0x01fa }
            java.lang.Class r2 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x01fa }
            r1[r3] = r2     // Catch:{ Exception -> 0x01fa }
            java.lang.reflect.Method r8 = r8.getDeclaredMethod(r0, r1)     // Catch:{ Exception -> 0x01fa }
            int r0 = r8.getModifiers()     // Catch:{ Exception -> 0x01fa }
            boolean r0 = java.lang.reflect.Modifier.isAbstract(r0)     // Catch:{ Exception -> 0x01fa }
            if (r0 != 0) goto L_0x01fb
            r7.x = r8     // Catch:{ Exception -> 0x01fa }
            java.lang.reflect.Method r8 = r7.x     // Catch:{ Exception -> 0x01fa }
            r8.setAccessible(r4)     // Catch:{ Exception -> 0x01fa }
            goto L_0x01fb
        L_0x01fa:
        L_0x01fb:
            int r8 = android.os.Build.VERSION.SDK_INT
            r0 = 17
            if (r8 < r0) goto L_0x023f
            java.lang.Class<android.telephony.CellSignalStrengthLte> r8 = android.telephony.CellSignalStrengthLte.class
            java.lang.String r0 = "mSignalStrength"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x0210 }
            r7.F = r8     // Catch:{ NoSuchFieldException -> 0x0210 }
            java.lang.reflect.Field r8 = r7.F     // Catch:{ NoSuchFieldException -> 0x0210 }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x0210 }
        L_0x0210:
            java.lang.Class<android.telephony.CellSignalStrengthLte> r8 = android.telephony.CellSignalStrengthLte.class
            java.lang.String r0 = "mRsrq"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x021f }
            r7.G = r8     // Catch:{ NoSuchFieldException -> 0x021f }
            java.lang.reflect.Field r8 = r7.G     // Catch:{ NoSuchFieldException -> 0x021f }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x021f }
        L_0x021f:
            java.lang.Class<android.telephony.CellSignalStrengthLte> r8 = android.telephony.CellSignalStrengthLte.class
            java.lang.String r0 = "mRssnr"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x022e }
            r7.H = r8     // Catch:{ NoSuchFieldException -> 0x022e }
            java.lang.reflect.Field r8 = r7.H     // Catch:{ NoSuchFieldException -> 0x022e }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x022e }
        L_0x022e:
            java.lang.Class<android.telephony.CellSignalStrengthLte> r8 = android.telephony.CellSignalStrengthLte.class
            java.lang.String r0 = "mCqi"
            java.lang.reflect.Field r8 = r8.getDeclaredField(r0)     // Catch:{ NoSuchFieldException -> 0x023e }
            r7.I = r8     // Catch:{ NoSuchFieldException -> 0x023e }
            java.lang.reflect.Field r8 = r7.I     // Catch:{ NoSuchFieldException -> 0x023e }
            r8.setAccessible(r4)     // Catch:{ NoSuchFieldException -> 0x023e }
            goto L_0x023f
        L_0x023e:
        L_0x023f:
            int r8 = android.os.Build.VERSION.SDK_INT
            r0 = 22
            if (r8 < r0) goto L_0x024c
            com.startapp.networkTest.controller.c$1 r8 = new com.startapp.networkTest.controller.c$1
            r8.<init>()
            r7.k = r8
        L_0x024c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.<init>(android.content.Context):void");
    }

    static /* synthetic */ int e(int i2) {
        if (i2 == 99 || i2 < 0 || i2 > 31) {
            return 0;
        }
        return (i2 * 2) - 113;
    }

    class b {

        /* renamed from: a  reason: collision with root package name */
        CellLocation f5992a;
        long b;

        private b() {
            this.b = 0;
        }

        /* synthetic */ b(c cVar, byte b2) {
            this();
        }
    }

    private boolean f(int i2) {
        return this.l.a(i2).SubscriptionId != -1;
    }

    /* JADX WARNING: Removed duplicated region for block: B:183:0x0490  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x04da  */
    @android.annotation.TargetApi(22)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.startapp.networkTest.data.RadioInfo g(int r17) {
        /*
            r16 = this;
            r1 = r16
            r2 = r17
            r0 = -1
            if (r2 == r0) goto L_0x04de
            boolean r0 = r16.f((int) r17)
            if (r0 == 0) goto L_0x04de
            int r0 = android.os.Build.VERSION.SDK_INT
            r3 = 22
            if (r0 < r3) goto L_0x04de
            int[] r0 = r1.R
            int r0 = r0.length
            if (r0 != 0) goto L_0x001a
            goto L_0x04de
        L_0x001a:
            com.startapp.networkTest.data.RadioInfo r3 = new com.startapp.networkTest.data.RadioInfo
            r3.<init>()
            r3.SubscriptionId = r2
            com.startapp.networkTest.data.a.a r0 = r1.l
            int r0 = r0.DefaultVoiceSimId
            r4 = 1
            r5 = 0
            if (r2 != r0) goto L_0x002b
            r0 = 1
            goto L_0x002c
        L_0x002b:
            r0 = 0
        L_0x002c:
            r3.IsDefaultVoiceSim = r0
            com.startapp.networkTest.data.a.a r0 = r1.l
            int r0 = r0.DefaultDataSimId
            if (r2 != r0) goto L_0x0036
            r0 = 1
            goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            r3.IsDefaultDataSim = r0
            android.content.Context r0 = r1.f
            com.startapp.networkTest.enums.PreferredNetworkTypes r0 = a((android.content.Context) r0, (int) r2)
            r3.PreferredNetwork = r0
            com.startapp.networkTest.enums.PreferredNetworkTypes r0 = r3.PreferredNetwork
            com.startapp.networkTest.enums.PreferredNetworkTypes r6 = com.startapp.networkTest.enums.PreferredNetworkTypes.Unknown
            if (r0 != r6) goto L_0x005f
            android.content.Context r0 = r1.f
            android.util.SparseArray r0 = a((android.content.Context) r0)
            com.startapp.networkTest.data.a.a r6 = r1.l
            com.startapp.networkTest.data.a.b r6 = r6.a(r2)
            int r6 = r6.SimSlotIndex
            java.lang.Object r0 = r0.get(r6)
            com.startapp.networkTest.enums.PreferredNetworkTypes r0 = (com.startapp.networkTest.enums.PreferredNetworkTypes) r0
            if (r0 == 0) goto L_0x005f
            r3.PreferredNetwork = r0
        L_0x005f:
            android.telephony.TelephonyManager r0 = r1.d
            if (r0 == 0) goto L_0x04dd
            r0 = 0
            android.util.SparseArray<android.telephony.TelephonyManager> r6 = r1.e
            if (r6 == 0) goto L_0x0076
            int r6 = r6.size()
            if (r6 <= 0) goto L_0x0076
            android.util.SparseArray<android.telephony.TelephonyManager> r0 = r1.e
            java.lang.Object r0 = r0.get(r2)
            android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
        L_0x0076:
            r6 = r0
            r7 = 3
            android.telephony.TelephonyManager r0 = r1.d     // Catch:{ SecurityException -> 0x00a0 }
            int r0 = r0.getDataState()     // Catch:{ SecurityException -> 0x00a0 }
            if (r0 == 0) goto L_0x009b
            if (r0 == r4) goto L_0x0096
            r8 = 2
            if (r0 == r8) goto L_0x0091
            if (r0 == r7) goto L_0x008c
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Unknown     // Catch:{ SecurityException -> 0x00a0 }
            r3.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x00a0 }
            goto L_0x00b8
        L_0x008c:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Suspended     // Catch:{ SecurityException -> 0x00a0 }
            r3.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x00a0 }
            goto L_0x00b8
        L_0x0091:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Connected     // Catch:{ SecurityException -> 0x00a0 }
            r3.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x00a0 }
            goto L_0x00b8
        L_0x0096:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Connecting     // Catch:{ SecurityException -> 0x00a0 }
            r3.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x00a0 }
            goto L_0x00b8
        L_0x009b:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Disconnected     // Catch:{ SecurityException -> 0x00a0 }
            r3.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x00a0 }
            goto L_0x00b8
        L_0x00a0:
            r0 = move-exception
            java.lang.String r8 = c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "getRadioInfo(subscriptionID): getDataState: "
            r9.<init>(r10)
            java.lang.String r0 = r0.toString()
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            android.util.Log.e(r8, r0)
        L_0x00b8:
            boolean r0 = r16.m()
            if (r0 == 0) goto L_0x00c1
            com.startapp.networkTest.enums.FlightModeStates r0 = com.startapp.networkTest.enums.FlightModeStates.Enabled
            goto L_0x00c3
        L_0x00c1:
            com.startapp.networkTest.enums.FlightModeStates r0 = com.startapp.networkTest.enums.FlightModeStates.Disabled
        L_0x00c3:
            r3.FlightMode = r0
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Unknown
            r3.MobileDataEnabled = r0
            if (r6 == 0) goto L_0x00df
            int r0 = android.os.Build.VERSION.SDK_INT
            r8 = 26
            if (r0 < r8) goto L_0x00df
            boolean r0 = r6.isDataEnabled()
            if (r0 == 0) goto L_0x00da
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Enabled
            goto L_0x00dc
        L_0x00da:
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Disabled
        L_0x00dc:
            r3.MobileDataEnabled = r0
            goto L_0x0119
        L_0x00df:
            java.lang.reflect.Method r0 = r1.K
            if (r0 == 0) goto L_0x0119
            android.telephony.TelephonyManager r8 = r1.d     // Catch:{ Exception -> 0x0101 }
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0101 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x0101 }
            r9[r5] = r10     // Catch:{ Exception -> 0x0101 }
            java.lang.Object r0 = r0.invoke(r8, r9)     // Catch:{ Exception -> 0x0101 }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x0101 }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x0101 }
            if (r0 == 0) goto L_0x00fc
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Enabled     // Catch:{ Exception -> 0x0101 }
            goto L_0x00fe
        L_0x00fc:
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Disabled     // Catch:{ Exception -> 0x0101 }
        L_0x00fe:
            r3.MobileDataEnabled = r0     // Catch:{ Exception -> 0x0101 }
            goto L_0x0119
        L_0x0101:
            r0 = move-exception
            java.lang.String r8 = c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "getRadioInfo(subscriptionID): MobileDataEnabled: "
            r9.<init>(r10)
            java.lang.String r0 = r0.toString()
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            android.util.Log.e(r8, r0)
        L_0x0119:
            if (r6 == 0) goto L_0x0122
            boolean r0 = r6.isNetworkRoaming()
            r3.IsRoaming = r0
            goto L_0x0155
        L_0x0122:
            java.lang.reflect.Method r0 = r1.L
            if (r0 == 0) goto L_0x0155
            android.telephony.TelephonyManager r8 = r1.d     // Catch:{ Exception -> 0x013d }
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x013d }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x013d }
            r9[r5] = r10     // Catch:{ Exception -> 0x013d }
            java.lang.Object r0 = r0.invoke(r8, r9)     // Catch:{ Exception -> 0x013d }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x013d }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x013d }
            r3.IsRoaming = r0     // Catch:{ Exception -> 0x013d }
            goto L_0x0155
        L_0x013d:
            r0 = move-exception
            java.lang.String r8 = c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "getRadioInfo(subscriptionID): IsRoaming: "
            r9.<init>(r10)
            java.lang.String r0 = r0.toString()
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            android.util.Log.e(r8, r0)
        L_0x0155:
            com.startapp.networkTest.enums.ThreeStateShort r0 = r16.l()
            r3.IsMetered = r0
            com.startapp.networkTest.enums.ThreeStateShort r0 = r16.p()
            int r0 = com.startapp.networkTest.utils.c.a((com.startapp.networkTest.enums.ThreeStateShort) r0)
            r3.IsVpn = r0
            com.startapp.networkTest.enums.ConnectionTypes r0 = r16.f()
            r3.ConnectionType = r0
            if (r6 == 0) goto L_0x0178
            int r0 = r6.getNetworkType()
            com.startapp.networkTest.enums.NetworkTypes r0 = b((int) r0)
            r3.NetworkType = r0
            goto L_0x01af
        L_0x0178:
            java.lang.reflect.Method r0 = r1.M
            if (r0 == 0) goto L_0x01af
            android.telephony.TelephonyManager r8 = r1.d     // Catch:{ Exception -> 0x0197 }
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0197 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x0197 }
            r9[r5] = r10     // Catch:{ Exception -> 0x0197 }
            java.lang.Object r0 = r0.invoke(r8, r9)     // Catch:{ Exception -> 0x0197 }
            java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0197 }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x0197 }
            com.startapp.networkTest.enums.NetworkTypes r0 = b((int) r0)     // Catch:{ Exception -> 0x0197 }
            r3.NetworkType = r0     // Catch:{ Exception -> 0x0197 }
            goto L_0x01af
        L_0x0197:
            r0 = move-exception
            java.lang.String r8 = c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "getRadioInfo(subscriptionID): NetworkType: "
            r9.<init>(r10)
            java.lang.String r0 = r0.toString()
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            android.util.Log.e(r8, r0)
        L_0x01af:
            java.lang.String r8 = "getRadioInfo(subscriptionID): OperatorName: "
            if (r6 == 0) goto L_0x01be
            java.lang.String r0 = r6.getNetworkOperatorName()
            java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)
            r3.OperatorName = r0
            goto L_0x01ef
        L_0x01be:
            java.lang.reflect.Method r0 = r1.N
            if (r0 == 0) goto L_0x01ef
            android.telephony.TelephonyManager r9 = r1.d     // Catch:{ Exception -> 0x01d9 }
            java.lang.Object[] r10 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x01d9 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x01d9 }
            r10[r5] = r11     // Catch:{ Exception -> 0x01d9 }
            java.lang.Object r0 = r0.invoke(r9, r10)     // Catch:{ Exception -> 0x01d9 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x01d9 }
            java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)     // Catch:{ Exception -> 0x01d9 }
            r3.OperatorName = r0     // Catch:{ Exception -> 0x01d9 }
            goto L_0x01ef
        L_0x01d9:
            r0 = move-exception
            java.lang.String r9 = c
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>(r8)
            java.lang.String r0 = r0.toString()
            r10.append(r0)
            java.lang.String r0 = r10.toString()
            android.util.Log.e(r9, r0)
        L_0x01ef:
            if (r6 == 0) goto L_0x01f6
            java.lang.String r0 = r6.getNetworkOperator()
            goto L_0x024f
        L_0x01f6:
            java.lang.reflect.Method r0 = r1.P
            if (r0 == 0) goto L_0x0222
            android.telephony.TelephonyManager r6 = r1.d     // Catch:{ Exception -> 0x020b }
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x020b }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x020b }
            r9[r5] = r10     // Catch:{ Exception -> 0x020b }
            java.lang.Object r0 = r0.invoke(r6, r9)     // Catch:{ Exception -> 0x020b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x020b }
            goto L_0x024f
        L_0x020b:
            r0 = move-exception
            java.lang.String r6 = c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>(r8)
            java.lang.String r0 = r0.toString()
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            android.util.Log.e(r6, r0)
            goto L_0x024d
        L_0x0222:
            java.lang.reflect.Method r0 = r1.O
            if (r0 == 0) goto L_0x024d
            android.telephony.TelephonyManager r6 = r1.d     // Catch:{ Exception -> 0x0237 }
            java.lang.Object[] r9 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x0237 }
            java.lang.Integer r10 = java.lang.Integer.valueOf(r17)     // Catch:{ Exception -> 0x0237 }
            r9[r5] = r10     // Catch:{ Exception -> 0x0237 }
            java.lang.Object r0 = r0.invoke(r6, r9)     // Catch:{ Exception -> 0x0237 }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x0237 }
            goto L_0x024f
        L_0x0237:
            r0 = move-exception
            java.lang.String r6 = c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>(r8)
            java.lang.String r0 = r0.toString()
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            android.util.Log.e(r6, r0)
        L_0x024d:
            java.lang.String r0 = ""
        L_0x024f:
            int r6 = r0.length()
            r8 = 4
            if (r6 <= r8) goto L_0x0262
            java.lang.String r6 = r0.substring(r5, r7)
            r3.MCC = r6
            java.lang.String r0 = r0.substring(r7)
            r3.MNC = r0
        L_0x0262:
            com.startapp.networkTest.controller.c$d r0 = r1.j
            com.startapp.networkTest.data.radio.NetworkRegistrationInfo[] r0 = r0.d(r2)
            com.startapp.networkTest.controller.c$d r6 = r1.j
            com.startapp.networkTest.controller.c$b r6 = r6.c(r2)
            if (r6 != 0) goto L_0x0275
            com.startapp.networkTest.controller.c$b r6 = new com.startapp.networkTest.controller.c$b
            r6.<init>(r1, r5)
        L_0x0275:
            android.content.Context r7 = r1.f
            java.lang.String r8 = "android.permission.ACCESS_COARSE_LOCATION"
            int r7 = r7.checkCallingOrSelfPermission(r8)
            if (r7 != 0) goto L_0x028d
            android.telephony.CellLocation r7 = r6.f5992a
            if (r7 != 0) goto L_0x028b
            android.telephony.TelephonyManager r7 = r1.d
            android.telephony.CellLocation r7 = r7.getCellLocation()
            r6.f5992a = r7
        L_0x028b:
            r7 = 0
            goto L_0x028e
        L_0x028d:
            r7 = 1
        L_0x028e:
            android.telephony.CellLocation r8 = r6.f5992a
            r9 = 0
            r11 = 2147483647(0x7fffffff, float:NaN)
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            if (r8 == 0) goto L_0x0366
            java.lang.Class r8 = r8.getClass()
            java.lang.Class<android.telephony.gsm.GsmCellLocation> r14 = android.telephony.gsm.GsmCellLocation.class
            boolean r8 = r8.equals(r14)
            if (r8 == 0) goto L_0x02e1
            android.telephony.CellLocation r8 = r6.f5992a
            android.telephony.gsm.GsmCellLocation r8 = (android.telephony.gsm.GsmCellLocation) r8
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getLac()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r3.GsmLAC = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getCid()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r3.GsmCellId = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r8 = r8.getPsc()
            r14.append(r8)
            java.lang.String r8 = r14.toString()
            r3.PrimaryScramblingCode = r8
            goto L_0x034d
        L_0x02e1:
            android.telephony.CellLocation r8 = r6.f5992a
            java.lang.Class r8 = r8.getClass()
            java.lang.Class<android.telephony.cdma.CdmaCellLocation> r14 = android.telephony.cdma.CdmaCellLocation.class
            boolean r8 = r8.equals(r14)
            if (r8 == 0) goto L_0x034d
            android.telephony.CellLocation r8 = r6.f5992a
            android.telephony.cdma.CdmaCellLocation r8 = (android.telephony.cdma.CdmaCellLocation) r8
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getBaseStationId()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r3.CdmaBaseStationId = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getBaseStationLatitude()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r3.CdmaBaseStationLatitude = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getBaseStationLongitude()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r3.CdmaBaseStationLongitude = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getNetworkId()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r3.CdmaNetworkId = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r8 = r8.getSystemId()
            r14.append(r8)
            java.lang.String r8 = r14.toString()
            r3.CdmaSystemId = r8
        L_0x034d:
            long r14 = r6.b
            int r8 = (r14 > r9 ? 1 : (r14 == r9 ? 0 : -1))
            if (r8 <= 0) goto L_0x038f
            long r14 = android.os.SystemClock.elapsedRealtime()
            long r5 = r6.b
            long r14 = r14 - r5
            int r5 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r5 <= 0) goto L_0x0362
            r5 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x0363
        L_0x0362:
            int r5 = (int) r14
        L_0x0363:
            r3.GsmCellIdAge = r5
            goto L_0x038f
        L_0x0366:
            if (r0 == 0) goto L_0x038f
            int r5 = r0.length
            r6 = 0
        L_0x036a:
            if (r6 >= r5) goto L_0x038f
            r14 = r0[r6]
            java.lang.String r15 = r14.Domain
            java.lang.String r8 = "CS"
            boolean r8 = r15.equals(r8)
            if (r8 == 0) goto L_0x038c
            java.lang.String r8 = r14.CellId
            r3.GsmCellId = r8
            java.lang.String r8 = r14.Tac
            r3.GsmLAC = r8
            java.lang.String r8 = r3.GsmCellId
            boolean r8 = r8.isEmpty()
            if (r8 != 0) goto L_0x038c
            int r8 = r14.Age
            r3.GsmCellIdAge = r8
        L_0x038c:
            int r6 = r6 + 1
            goto L_0x036a
        L_0x038f:
            java.lang.String r5 = r3.GsmCellId
            boolean r5 = r5.isEmpty()
            if (r5 == 0) goto L_0x039b
            if (r7 == 0) goto L_0x039b
            r3.MissingPermission = r4
        L_0x039b:
            com.startapp.networkTest.controller.c$d r4 = r1.j
            com.startapp.networkTest.controller.c$h r4 = r4.b(r2)
            com.startapp.networkTest.enums.ServiceStates r5 = r4.f5998a
            r3.ServiceState = r5
            com.startapp.networkTest.enums.DuplexMode r5 = r4.c
            r3.DuplexMode = r5
            com.startapp.networkTest.enums.ThreeStateShort r5 = r4.d
            r3.ManualSelection = r5
            com.startapp.networkTest.enums.ThreeStateShort r5 = r4.f
            r3.CarrierAggregation = r5
            int r5 = r4.e
            r3.ARFCN = r5
            long r5 = r4.b
            int r7 = (r5 > r9 ? 1 : (r5 == r9 ? 0 : -1))
            if (r7 <= 0) goto L_0x03cd
            long r5 = android.os.SystemClock.elapsedRealtime()
            long r7 = r4.b
            long r5 = r5 - r7
            int r4 = (r5 > r12 ? 1 : (r5 == r12 ? 0 : -1))
            if (r4 <= 0) goto L_0x03ca
            r4 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x03cb
        L_0x03ca:
            int r4 = (int) r5
        L_0x03cb:
            r3.ServiceStateAge = r4
        L_0x03cd:
            java.lang.String r4 = b((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r3.NrState = r4
            com.startapp.networkTest.enums.ThreeStateShort r4 = c((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r3.NrAvailable = r4
            com.startapp.networkTest.enums.NetworkTypes r4 = r3.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r5 = com.startapp.networkTest.enums.NetworkTypes.Unknown
            if (r4 != r5) goto L_0x03e5
            com.startapp.networkTest.enums.NetworkTypes r4 = a((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r3.NetworkType = r4
        L_0x03e5:
            com.startapp.networkTest.controller.c$d r4 = r1.j
            com.startapp.networkTest.controller.c$i r2 = r4.a((int) r2)
            int r4 = r2.j
            r3.RSCP = r4
            int r4 = r2.c
            r3.CdmaEcIo = r4
            int r4 = r2.f5999a
            r3.RXLevel = r4
            int r4 = r2.b
            r3.NativeDbm = r4
            int r4 = r2.i
            r3.EcN0 = r4
            int r4 = r2.d
            r3.LteCqi = r4
            int r4 = r2.e
            r3.LteRsrp = r4
            int r4 = r2.g
            r3.LteRsrq = r4
            int r4 = r2.f
            r3.LteRssnr = r4
            int r4 = r2.h
            r3.LteRssi = r4
            int r4 = r2.l
            r3.NrCsiRsrp = r4
            int r4 = r2.m
            r3.NrCsiRsrq = r4
            int r4 = r2.n
            r3.NrCsiSinr = r4
            int r4 = r2.o
            r3.NrSsRsrp = r4
            int r4 = r2.p
            r3.NrSsRsrq = r4
            int r4 = r2.q
            r3.NrSsSinr = r4
            com.startapp.networkTest.enums.NetworkTypes r4 = r3.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r5 = com.startapp.networkTest.enums.NetworkTypes.LTE
            if (r4 == r5) goto L_0x0435
            com.startapp.networkTest.enums.NetworkTypes r5 = com.startapp.networkTest.enums.NetworkTypes.LTE_CA
            if (r4 != r5) goto L_0x043d
        L_0x0435:
            int r4 = r3.RXLevel
            if (r4 < 0) goto L_0x043d
            int r4 = r3.LteRsrp
            r3.RXLevel = r4
        L_0x043d:
            com.startapp.networkTest.enums.NetworkTypes r4 = r3.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r5 = com.startapp.networkTest.enums.NetworkTypes.LTE_CA
            if (r4 != r5) goto L_0x0447
            com.startapp.networkTest.enums.ThreeStateShort r4 = com.startapp.networkTest.enums.ThreeStateShort.Yes
            r3.CarrierAggregation = r4
        L_0x0447:
            com.startapp.networkTest.enums.ThreeStateShort r4 = r3.CarrierAggregation
            com.startapp.networkTest.enums.ThreeStateShort r5 = com.startapp.networkTest.enums.ThreeStateShort.Unknown
            if (r4 != r5) goto L_0x0453
            com.startapp.networkTest.enums.ThreeStateShort r0 = d((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r3.CarrierAggregation = r0
        L_0x0453:
            com.startapp.networkTest.enums.NetworkTypes r0 = r3.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r4 = com.startapp.networkTest.enums.NetworkTypes.NR
            if (r0 != r4) goto L_0x04ce
            java.lang.String r0 = r3.MCC     // Catch:{ NumberFormatException -> 0x0468 }
            int r5 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0468 }
            java.lang.String r0 = r3.MNC     // Catch:{ NumberFormatException -> 0x0466 }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0466 }
            goto L_0x0479
        L_0x0466:
            r0 = move-exception
            goto L_0x046a
        L_0x0468:
            r0 = move-exception
            r5 = 0
        L_0x046a:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r6 = "radioInfo: "
            r4.<init>(r6)
            java.lang.String r0 = r0.getMessage()
            r4.append(r0)
            r0 = 0
        L_0x0479:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r5)
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            com.startapp.networkTest.controller.c$d r4 = r1.j
            com.startapp.networkTest.controller.c$f r0 = r4.a((java.lang.String) r0)
            if (r0 == 0) goto L_0x04ce
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            long r5 = r0.f5996a
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.GsmCellId = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            int r5 = r0.b
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.GsmLAC = r4
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            int r5 = r0.c
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.PrimaryScramblingCode = r4
            long r4 = android.os.SystemClock.elapsedRealtime()
            long r6 = r0.d
            r8 = 1000000(0xf4240, double:4.940656E-318)
            long r6 = r6 / r8
            long r4 = r4 - r6
            int r0 = (int) r4
            r3.GsmCellIdAge = r0
        L_0x04ce:
            long r4 = android.os.SystemClock.elapsedRealtime()
            long r6 = r2.k
            long r4 = r4 - r6
            int r0 = (r4 > r12 ? 1 : (r4 == r12 ? 0 : -1))
            if (r0 <= 0) goto L_0x04da
            goto L_0x04db
        L_0x04da:
            int r11 = (int) r4
        L_0x04db:
            r3.RXLevelAge = r11
        L_0x04dd:
            return r3
        L_0x04de:
            com.startapp.networkTest.data.RadioInfo r0 = r16.k()
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.g(int):com.startapp.networkTest.data.RadioInfo");
    }

    /* access modifiers changed from: private */
    @TargetApi(24)
    public void i() {
        this.e = new SparseArray<>();
        int i2 = 0;
        while (true) {
            int[] iArr = this.R;
            if (i2 < iArr.length) {
                this.e.put(iArr[i2], this.d.createForSubscriptionId(iArr[i2]));
                i2++;
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: private */
    public void j() {
        this.l = b.g(this.f);
        ArrayList<com.startapp.networkTest.data.a.b> arrayList = this.l.SimInfos;
        com.startapp.networkTest.data.a.b[] bVarArr = (com.startapp.networkTest.data.a.b[]) arrayList.toArray(new com.startapp.networkTest.data.a.b[arrayList.size()]);
        int[] iArr = new int[bVarArr.length];
        for (int i2 = 0; i2 < bVarArr.length; i2++) {
            iArr[i2] = bVarArr[i2].SubscriptionId;
        }
        this.R = iArr;
    }

    /* JADX WARNING: Removed duplicated region for block: B:130:0x037a  */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x03c4  */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.startapp.networkTest.data.RadioInfo k() {
        /*
            r16 = this;
            r1 = r16
            com.startapp.networkTest.data.RadioInfo r2 = new com.startapp.networkTest.data.RadioInfo
            r2.<init>()
            android.telephony.TelephonyManager r0 = r1.d
            if (r0 == 0) goto L_0x03c7
            android.content.Context r0 = r1.f
            android.util.SparseArray r0 = a((android.content.Context) r0)
            r3 = 0
            java.lang.Object r0 = r0.get(r3)
            com.startapp.networkTest.enums.PreferredNetworkTypes r0 = (com.startapp.networkTest.enums.PreferredNetworkTypes) r0
            if (r0 == 0) goto L_0x001c
            r2.PreferredNetwork = r0
        L_0x001c:
            r4 = 3
            r5 = 1
            android.telephony.TelephonyManager r0 = r1.d     // Catch:{ SecurityException -> 0x0046 }
            int r0 = r0.getDataState()     // Catch:{ SecurityException -> 0x0046 }
            if (r0 == 0) goto L_0x0041
            if (r0 == r5) goto L_0x003c
            r6 = 2
            if (r0 == r6) goto L_0x0037
            if (r0 == r4) goto L_0x0032
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Unknown     // Catch:{ SecurityException -> 0x0046 }
            r2.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x0046 }
            goto L_0x005e
        L_0x0032:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Suspended     // Catch:{ SecurityException -> 0x0046 }
            r2.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x0046 }
            goto L_0x005e
        L_0x0037:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Connected     // Catch:{ SecurityException -> 0x0046 }
            r2.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x0046 }
            goto L_0x005e
        L_0x003c:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Connecting     // Catch:{ SecurityException -> 0x0046 }
            r2.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x0046 }
            goto L_0x005e
        L_0x0041:
            com.startapp.networkTest.enums.radio.DataConnectionStates r0 = com.startapp.networkTest.enums.radio.DataConnectionStates.Disconnected     // Catch:{ SecurityException -> 0x0046 }
            r2.MobileDataConnectionState = r0     // Catch:{ SecurityException -> 0x0046 }
            goto L_0x005e
        L_0x0046:
            r0 = move-exception
            java.lang.String r6 = c
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            java.lang.String r8 = "getRadioInfo: getDataState: "
            r7.<init>(r8)
            java.lang.String r0 = r0.toString()
            r7.append(r0)
            java.lang.String r0 = r7.toString()
            android.util.Log.e(r6, r0)
        L_0x005e:
            boolean r0 = r16.m()
            if (r0 == 0) goto L_0x0067
            com.startapp.networkTest.enums.FlightModeStates r0 = com.startapp.networkTest.enums.FlightModeStates.Enabled
            goto L_0x0069
        L_0x0067:
            com.startapp.networkTest.enums.FlightModeStates r0 = com.startapp.networkTest.enums.FlightModeStates.Disabled
        L_0x0069:
            r2.FlightMode = r0
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Unknown
            r2.MobileDataEnabled = r0
            java.lang.reflect.Method r0 = r1.J
            java.lang.String r6 = "getRadioInfo: MobileDataEnabled: "
            if (r0 == 0) goto L_0x00a4
            android.telephony.TelephonyManager r7 = r1.d     // Catch:{ Exception -> 0x008d }
            java.lang.Object[] r8 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x008d }
            java.lang.Object r0 = r0.invoke(r7, r8)     // Catch:{ Exception -> 0x008d }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ Exception -> 0x008d }
            boolean r0 = r0.booleanValue()     // Catch:{ Exception -> 0x008d }
            if (r0 == 0) goto L_0x0088
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Enabled     // Catch:{ Exception -> 0x008d }
            goto L_0x008a
        L_0x0088:
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Disabled     // Catch:{ Exception -> 0x008d }
        L_0x008a:
            r2.MobileDataEnabled = r0     // Catch:{ Exception -> 0x008d }
            goto L_0x00f9
        L_0x008d:
            r0 = move-exception
            java.lang.String r7 = c
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>(r6)
            java.lang.String r0 = r0.toString()
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            android.util.Log.e(r7, r0)
            goto L_0x00f9
        L_0x00a4:
            int r0 = android.os.Build.VERSION.SDK_INT
            r7 = 17
            java.lang.String r8 = "mobile_data"
            if (r0 < r7) goto L_0x00d3
            android.content.ContentResolver r0 = r1.Q     // Catch:{ all -> 0x00bc }
            int r0 = android.provider.Settings.Global.getInt(r0, r8)     // Catch:{ all -> 0x00bc }
            if (r0 != r5) goto L_0x00b7
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Enabled     // Catch:{ all -> 0x00bc }
            goto L_0x00b9
        L_0x00b7:
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Disabled     // Catch:{ all -> 0x00bc }
        L_0x00b9:
            r2.MobileDataEnabled = r0     // Catch:{ all -> 0x00bc }
            goto L_0x00f9
        L_0x00bc:
            r0 = move-exception
            java.lang.String r7 = c
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>(r6)
            java.lang.String r0 = r0.toString()
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            android.util.Log.e(r7, r0)
            goto L_0x00f9
        L_0x00d3:
            android.content.ContentResolver r0 = r1.Q     // Catch:{ all -> 0x00e3 }
            int r0 = android.provider.Settings.Secure.getInt(r0, r8)     // Catch:{ all -> 0x00e3 }
            if (r0 != r5) goto L_0x00de
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Enabled     // Catch:{ all -> 0x00e3 }
            goto L_0x00e0
        L_0x00de:
            com.startapp.networkTest.enums.ThreeState r0 = com.startapp.networkTest.enums.ThreeState.Disabled     // Catch:{ all -> 0x00e3 }
        L_0x00e0:
            r2.MobileDataEnabled = r0     // Catch:{ all -> 0x00e3 }
            goto L_0x00f9
        L_0x00e3:
            r0 = move-exception
            java.lang.String r7 = c
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>(r6)
            java.lang.String r0 = r0.toString()
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            android.util.Log.e(r7, r0)
        L_0x00f9:
            android.telephony.TelephonyManager r0 = r1.d
            boolean r0 = r0.isNetworkRoaming()
            r2.IsRoaming = r0
            com.startapp.networkTest.enums.ThreeStateShort r0 = r16.l()
            r2.IsMetered = r0
            com.startapp.networkTest.enums.ThreeStateShort r0 = r16.p()
            int r0 = com.startapp.networkTest.utils.c.a((com.startapp.networkTest.enums.ThreeStateShort) r0)
            r2.IsVpn = r0
            com.startapp.networkTest.enums.ConnectionTypes r0 = r16.f()
            r2.ConnectionType = r0
            android.telephony.TelephonyManager r0 = r1.d
            int r0 = r0.getNetworkType()
            com.startapp.networkTest.enums.NetworkTypes r0 = b((int) r0)
            r2.NetworkType = r0
            android.telephony.TelephonyManager r0 = r1.d
            java.lang.String r0 = r0.getNetworkOperatorName()
            java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)
            r2.OperatorName = r0
            android.telephony.TelephonyManager r0 = r1.d
            java.lang.String r0 = r0.getNetworkOperator()
            if (r0 == 0) goto L_0x014a
            int r6 = r0.length()
            r7 = 4
            if (r6 <= r7) goto L_0x014a
            java.lang.String r6 = r0.substring(r3, r4)
            r2.MCC = r6
            java.lang.String r0 = r0.substring(r4)
            r2.MNC = r0
        L_0x014a:
            com.startapp.networkTest.controller.c$d r0 = r1.j
            r4 = -1
            com.startapp.networkTest.data.radio.NetworkRegistrationInfo[] r0 = r0.d(r4)
            com.startapp.networkTest.controller.c$d r6 = r1.j
            com.startapp.networkTest.controller.c$b r6 = r6.c(r4)
            if (r6 != 0) goto L_0x015e
            com.startapp.networkTest.controller.c$b r6 = new com.startapp.networkTest.controller.c$b
            r6.<init>(r1, r3)
        L_0x015e:
            android.content.Context r7 = r1.f
            java.lang.String r8 = "android.permission.ACCESS_COARSE_LOCATION"
            int r7 = r7.checkCallingOrSelfPermission(r8)
            if (r7 != 0) goto L_0x0176
            android.telephony.CellLocation r7 = r6.f5992a
            if (r7 != 0) goto L_0x0174
            android.telephony.TelephonyManager r7 = r1.d
            android.telephony.CellLocation r7 = r7.getCellLocation()
            r6.f5992a = r7
        L_0x0174:
            r7 = 0
            goto L_0x0177
        L_0x0176:
            r7 = 1
        L_0x0177:
            android.telephony.CellLocation r8 = r6.f5992a
            r9 = 0
            r11 = 2147483647(0x7fffffff, float:NaN)
            r12 = 2147483647(0x7fffffff, double:1.060997895E-314)
            if (r8 == 0) goto L_0x024f
            java.lang.Class r8 = r8.getClass()
            java.lang.Class<android.telephony.gsm.GsmCellLocation> r14 = android.telephony.gsm.GsmCellLocation.class
            boolean r8 = r8.equals(r14)
            if (r8 == 0) goto L_0x01ca
            android.telephony.CellLocation r8 = r6.f5992a
            android.telephony.gsm.GsmCellLocation r8 = (android.telephony.gsm.GsmCellLocation) r8
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getLac()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r2.GsmLAC = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getCid()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r2.GsmCellId = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r8 = r8.getPsc()
            r14.append(r8)
            java.lang.String r8 = r14.toString()
            r2.PrimaryScramblingCode = r8
            goto L_0x0236
        L_0x01ca:
            android.telephony.CellLocation r8 = r6.f5992a
            java.lang.Class r8 = r8.getClass()
            java.lang.Class<android.telephony.cdma.CdmaCellLocation> r14 = android.telephony.cdma.CdmaCellLocation.class
            boolean r8 = r8.equals(r14)
            if (r8 == 0) goto L_0x0236
            android.telephony.CellLocation r8 = r6.f5992a
            android.telephony.cdma.CdmaCellLocation r8 = (android.telephony.cdma.CdmaCellLocation) r8
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getBaseStationId()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r2.CdmaBaseStationId = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getBaseStationLatitude()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r2.CdmaBaseStationLatitude = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getBaseStationLongitude()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r2.CdmaBaseStationLongitude = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r15 = r8.getNetworkId()
            r14.append(r15)
            java.lang.String r14 = r14.toString()
            r2.CdmaNetworkId = r14
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            int r8 = r8.getSystemId()
            r14.append(r8)
            java.lang.String r8 = r14.toString()
            r2.CdmaSystemId = r8
        L_0x0236:
            long r14 = r6.b
            int r8 = (r14 > r9 ? 1 : (r14 == r9 ? 0 : -1))
            if (r8 <= 0) goto L_0x0278
            long r14 = android.os.SystemClock.elapsedRealtime()
            long r9 = r6.b
            long r14 = r14 - r9
            int r6 = (r14 > r12 ? 1 : (r14 == r12 ? 0 : -1))
            if (r6 <= 0) goto L_0x024b
            r6 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x024c
        L_0x024b:
            int r6 = (int) r14
        L_0x024c:
            r2.GsmCellIdAge = r6
            goto L_0x0278
        L_0x024f:
            if (r0 == 0) goto L_0x0278
            int r6 = r0.length
            r8 = 0
        L_0x0253:
            if (r8 >= r6) goto L_0x0278
            r9 = r0[r8]
            java.lang.String r10 = r9.Domain
            java.lang.String r14 = "CS"
            boolean r10 = r10.equals(r14)
            if (r10 == 0) goto L_0x0275
            java.lang.String r10 = r9.CellId
            r2.GsmCellId = r10
            java.lang.String r10 = r9.Tac
            r2.GsmLAC = r10
            java.lang.String r10 = r2.GsmCellId
            boolean r10 = r10.isEmpty()
            if (r10 != 0) goto L_0x0275
            int r9 = r9.Age
            r2.GsmCellIdAge = r9
        L_0x0275:
            int r8 = r8 + 1
            goto L_0x0253
        L_0x0278:
            java.lang.String r6 = r2.GsmCellId
            boolean r6 = r6.isEmpty()
            if (r6 == 0) goto L_0x0284
            if (r7 == 0) goto L_0x0284
            r2.MissingPermission = r5
        L_0x0284:
            com.startapp.networkTest.controller.c$d r5 = r1.j
            com.startapp.networkTest.controller.c$h r5 = r5.b(r4)
            com.startapp.networkTest.enums.ServiceStates r6 = r5.f5998a
            r2.ServiceState = r6
            com.startapp.networkTest.enums.DuplexMode r6 = r5.c
            r2.DuplexMode = r6
            com.startapp.networkTest.enums.ThreeStateShort r6 = r5.d
            r2.ManualSelection = r6
            com.startapp.networkTest.enums.ThreeStateShort r6 = r5.f
            r2.CarrierAggregation = r6
            int r6 = r5.e
            r2.ARFCN = r6
            long r6 = r5.b
            r8 = 0
            int r10 = (r6 > r8 ? 1 : (r6 == r8 ? 0 : -1))
            if (r10 <= 0) goto L_0x02b8
            long r6 = android.os.SystemClock.elapsedRealtime()
            long r8 = r5.b
            long r6 = r6 - r8
            int r5 = (r6 > r12 ? 1 : (r6 == r12 ? 0 : -1))
            if (r5 <= 0) goto L_0x02b5
            r5 = 2147483647(0x7fffffff, float:NaN)
            goto L_0x02b6
        L_0x02b5:
            int r5 = (int) r6
        L_0x02b6:
            r2.ServiceStateAge = r5
        L_0x02b8:
            java.lang.String r5 = b((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r2.NrState = r5
            com.startapp.networkTest.enums.ThreeStateShort r5 = c((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r2.NrAvailable = r5
            com.startapp.networkTest.enums.NetworkTypes r5 = r2.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.Unknown
            if (r5 != r6) goto L_0x02d0
            com.startapp.networkTest.enums.NetworkTypes r5 = a((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r2.NetworkType = r5
        L_0x02d0:
            com.startapp.networkTest.controller.c$d r5 = r1.j
            com.startapp.networkTest.controller.c$i r4 = r5.a((int) r4)
            int r5 = r4.j
            r2.RSCP = r5
            int r5 = r4.c
            r2.CdmaEcIo = r5
            int r5 = r4.f5999a
            r2.RXLevel = r5
            int r5 = r4.b
            r2.NativeDbm = r5
            int r5 = r4.i
            r2.EcN0 = r5
            int r5 = r4.d
            r2.LteCqi = r5
            int r5 = r4.e
            r2.LteRsrp = r5
            int r5 = r4.g
            r2.LteRsrq = r5
            int r5 = r4.f
            r2.LteRssnr = r5
            int r5 = r4.h
            r2.LteRssi = r5
            int r5 = r4.l
            r2.NrCsiRsrp = r5
            int r5 = r4.m
            r2.NrCsiRsrq = r5
            int r5 = r4.n
            r2.NrCsiSinr = r5
            int r5 = r4.o
            r2.NrSsRsrp = r5
            int r5 = r4.p
            r2.NrSsRsrq = r5
            int r5 = r4.q
            r2.NrSsSinr = r5
            com.startapp.networkTest.enums.NetworkTypes r5 = r2.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.LTE
            if (r5 == r6) goto L_0x0320
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.LTE_CA
            if (r5 != r6) goto L_0x0328
        L_0x0320:
            int r5 = r2.RXLevel
            if (r5 < 0) goto L_0x0328
            int r5 = r2.LteRsrp
            r2.RXLevel = r5
        L_0x0328:
            com.startapp.networkTest.enums.NetworkTypes r5 = r2.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.LTE_CA
            if (r5 != r6) goto L_0x0332
            com.startapp.networkTest.enums.ThreeStateShort r5 = com.startapp.networkTest.enums.ThreeStateShort.Yes
            r2.CarrierAggregation = r5
        L_0x0332:
            com.startapp.networkTest.enums.ThreeStateShort r5 = r2.CarrierAggregation
            com.startapp.networkTest.enums.ThreeStateShort r6 = com.startapp.networkTest.enums.ThreeStateShort.Unknown
            if (r5 != r6) goto L_0x033e
            com.startapp.networkTest.enums.ThreeStateShort r0 = d((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            r2.CarrierAggregation = r0
        L_0x033e:
            com.startapp.networkTest.enums.NetworkTypes r0 = r2.NetworkType
            com.startapp.networkTest.enums.NetworkTypes r5 = com.startapp.networkTest.enums.NetworkTypes.NR
            if (r0 != r5) goto L_0x03b8
            java.lang.String r0 = r2.MCC     // Catch:{ NumberFormatException -> 0x0353 }
            int r5 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0353 }
            java.lang.String r0 = r2.MNC     // Catch:{ NumberFormatException -> 0x0351 }
            int r3 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x0351 }
            goto L_0x0363
        L_0x0351:
            r0 = move-exception
            goto L_0x0355
        L_0x0353:
            r0 = move-exception
            r5 = 0
        L_0x0355:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "radioInfo: "
            r6.<init>(r7)
            java.lang.String r0 = r0.getMessage()
            r6.append(r0)
        L_0x0363:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r0.append(r3)
            java.lang.String r0 = r0.toString()
            com.startapp.networkTest.controller.c$d r3 = r1.j
            com.startapp.networkTest.controller.c$f r0 = r3.a((java.lang.String) r0)
            if (r0 == 0) goto L_0x03b8
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            long r5 = r0.f5996a
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r2.GsmCellId = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r5 = r0.b
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r2.GsmLAC = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            int r5 = r0.c
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            r2.PrimaryScramblingCode = r3
            long r5 = android.os.SystemClock.elapsedRealtime()
            long r7 = r0.d
            r9 = 1000000(0xf4240, double:4.940656E-318)
            long r7 = r7 / r9
            long r5 = r5 - r7
            int r0 = (int) r5
            r2.GsmCellIdAge = r0
        L_0x03b8:
            long r5 = android.os.SystemClock.elapsedRealtime()
            long r3 = r4.k
            long r5 = r5 - r3
            int r0 = (r5 > r12 ? 1 : (r5 == r12 ? 0 : -1))
            if (r0 <= 0) goto L_0x03c4
            goto L_0x03c5
        L_0x03c4:
            int r11 = (int) r5
        L_0x03c5:
            r2.RXLevelAge = r11
        L_0x03c7:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.k():com.startapp.networkTest.data.RadioInfo");
    }

    private ThreeStateShort l() {
        if (Build.VERSION.SDK_INT < 16 || this.f.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0) {
            return ThreeStateShort.Unknown;
        }
        return this.i.isActiveNetworkMetered() ? ThreeStateShort.Yes : ThreeStateShort.No;
    }

    @TargetApi(17)
    private boolean m() {
        return Build.VERSION.SDK_INT < 17 ? Settings.System.getInt(this.Q, "airplane_mode_on", 0) != 0 : Settings.Global.getInt(this.Q, "airplane_mode_on", 0) != 0;
    }

    private NetworkTypes n() {
        if (com.startapp.networkTest.utils.e.a(this.f)) {
            TelephonyManager telephonyManager = this.d;
            if (telephonyManager != null && Build.VERSION.SDK_INT >= 24) {
                return b(telephonyManager.getVoiceNetworkType());
            }
            Method method = this.w;
            if (method != null) {
                try {
                    return b(((Integer) method.invoke(this.d, new Object[0])).intValue());
                } catch (Exception e2) {
                    String str = c;
                    Log.e(str, "getVoiceNetworkType: " + e2.toString());
                }
            }
        }
        return NetworkTypes.Unknown;
    }

    private List<a> o() {
        Network[] allNetworks;
        ArrayList arrayList = new ArrayList();
        if (this.i != null && Build.VERSION.SDK_INT >= 21 && this.f.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") == 0 && (allNetworks = this.i.getAllNetworks()) != null && allNetworks.length > 0) {
            for (Network network : allNetworks) {
                NetworkCapabilities networkCapabilities = this.i.getNetworkCapabilities(network);
                if (networkCapabilities != null && networkCapabilities.hasTransport(0)) {
                    a aVar = new a(this, (byte) 0);
                    NetworkInfo networkInfo = this.i.getNetworkInfo(network);
                    LinkProperties linkProperties = this.i.getLinkProperties(network);
                    ArrayList arrayList2 = new ArrayList();
                    if (networkCapabilities.hasCapability(4)) {
                        arrayList2.add("ims");
                    }
                    if (networkCapabilities.hasCapability(1)) {
                        arrayList2.add("supl");
                    }
                    if (networkCapabilities.hasCapability(9)) {
                        arrayList2.add("xcap");
                    }
                    if (networkCapabilities.hasCapability(2)) {
                        arrayList2.add("dun");
                    }
                    if (networkCapabilities.hasCapability(5)) {
                        arrayList2.add("cbs");
                    }
                    if (networkCapabilities.hasCapability(3)) {
                        arrayList2.add("fota");
                    }
                    if (networkCapabilities.hasCapability(10)) {
                        arrayList2.add("emergency");
                    }
                    if (networkCapabilities.hasCapability(7)) {
                        arrayList2.add("ia");
                    }
                    if (networkCapabilities.hasCapability(0)) {
                        arrayList2.add("mms");
                    }
                    if (networkCapabilities.hasCapability(8)) {
                        arrayList2.add("rcs");
                    }
                    if (networkCapabilities.hasCapability(23)) {
                        arrayList2.add("mcx");
                    }
                    aVar.c = TextUtils.join(",", arrayList2);
                    if (networkInfo != null) {
                        aVar.b = networkInfo.getExtraInfo();
                        aVar.f5991a = networkInfo.getSubtype();
                        aVar.j = WifiDetailedStates.a(networkInfo.getDetailedState());
                    }
                    if (linkProperties != null) {
                        aVar.d = com.iab.omid.library.startapp.b.a(networkCapabilities);
                        aVar.f = com.iab.omid.library.startapp.b.b(networkCapabilities);
                        aVar.e = com.iab.omid.library.startapp.b.a(linkProperties);
                        String interfaceName = linkProperties.getInterfaceName();
                        if (interfaceName != null) {
                            try {
                                aVar.g = com.startapp.networkTest.utils.i.a(interfaceName);
                                aVar.h = com.startapp.networkTest.utils.i.b(interfaceName);
                            } catch (Exception unused) {
                            }
                            aVar.i = interfaceName;
                        }
                    }
                    arrayList.add(aVar);
                }
            }
        }
        return arrayList;
    }

    private ThreeStateShort p() {
        ConnectivityManager connectivityManager;
        NetworkCapabilities networkCapabilities;
        ThreeStateShort threeStateShort = ThreeStateShort.Unknown;
        if (Build.VERSION.SDK_INT < 23 || this.f.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || (connectivityManager = this.i) == null || (networkCapabilities = this.i.getNetworkCapabilities(connectivityManager.getActiveNetwork())) == null) {
            return threeStateShort;
        }
        return networkCapabilities.hasTransport(4) ? ThreeStateShort.Yes : ThreeStateShort.No;
    }

    public final RadioInfo c() {
        try {
            return g(this.l.DefaultDataSimId);
        } catch (Throwable th) {
            com.startapp.networkTest.startapp.a.a(th);
            return new RadioInfo();
        }
    }

    @TargetApi(18)
    public final com.startapp.networkTest.data.radio.CellInfo[] d() {
        CellConnectionStatus cellConnectionStatus;
        if (this.f.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") != 0) {
            return new com.startapp.networkTest.data.radio.CellInfo[0];
        }
        ArrayList arrayList = new ArrayList();
        TelephonyManager telephonyManager = this.d;
        if (telephonyManager != null && Build.VERSION.SDK_INT >= 18) {
            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
            if (this.n != null && (allCellInfo == null || allCellInfo.isEmpty())) {
                allCellInfo = this.n;
            }
            if (allCellInfo == null) {
                return new com.startapp.networkTest.data.radio.CellInfo[0];
            }
            long uptimeMillis = SystemClock.uptimeMillis();
            for (CellInfo next : allCellInfo) {
                com.startapp.networkTest.data.radio.CellInfo cellInfo = new com.startapp.networkTest.data.radio.CellInfo();
                if (Build.VERSION.SDK_INT >= 28) {
                    int cellConnectionStatus2 = next.getCellConnectionStatus();
                    if (cellConnectionStatus2 == 0) {
                        cellConnectionStatus = CellConnectionStatus.None;
                    } else if (cellConnectionStatus2 == 1) {
                        cellConnectionStatus = CellConnectionStatus.Primary;
                    } else if (cellConnectionStatus2 != 2) {
                        cellConnectionStatus = CellConnectionStatus.Unknown;
                    } else {
                        cellConnectionStatus = CellConnectionStatus.Secondary;
                    }
                    cellInfo.CellConnectionStatus = cellConnectionStatus;
                }
                if (next instanceof CellInfoGsm) {
                    CellInfoGsm cellInfoGsm = (CellInfoGsm) next;
                    cellInfo.IsRegistered = cellInfoGsm.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Gsm;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoGsm.getTimeStamp() / 1000000);
                    CellIdentityGsm cellIdentity = cellInfoGsm.getCellIdentity();
                    if (cellIdentity.getMcc() != Integer.MAX_VALUE) {
                        cellInfo.Mcc = cellIdentity.getMcc();
                    }
                    if (cellIdentity.getMnc() != Integer.MAX_VALUE) {
                        cellInfo.Mnc = cellIdentity.getMnc();
                    }
                    if (cellIdentity.getCid() != Integer.MAX_VALUE) {
                        cellInfo.Cid = cellIdentity.getCid();
                        cellInfo.CellId = (long) cellInfo.Cid;
                    }
                    if (cellIdentity.getLac() != Integer.MAX_VALUE) {
                        cellInfo.Lac = cellIdentity.getLac();
                    }
                    if (cellIdentity.getPsc() != Integer.MAX_VALUE) {
                        cellInfo.Psc = cellIdentity.getPsc();
                    }
                    if (Build.VERSION.SDK_INT >= 24) {
                        if (cellIdentity.getArfcn() != Integer.MAX_VALUE) {
                            cellInfo.Arfcn = cellIdentity.getArfcn();
                        }
                        if (cellIdentity.getBsic() != Integer.MAX_VALUE) {
                            cellInfo.GsmBsic = cellIdentity.getBsic();
                        }
                    }
                    cellInfo.Dbm = cellInfoGsm.getCellSignalStrength().getDbm();
                } else if (next instanceof CellInfoLte) {
                    CellInfoLte cellInfoLte = (CellInfoLte) next;
                    cellInfo.IsRegistered = cellInfoLte.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Lte;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoLte.getTimeStamp() / 1000000);
                    CellIdentityLte cellIdentity2 = cellInfoLte.getCellIdentity();
                    if (cellIdentity2.getMcc() != Integer.MAX_VALUE) {
                        cellInfo.Mcc = cellIdentity2.getMcc();
                    }
                    if (cellIdentity2.getMnc() != Integer.MAX_VALUE) {
                        cellInfo.Mnc = cellIdentity2.getMnc();
                    }
                    if (cellIdentity2.getCi() != Integer.MAX_VALUE) {
                        cellInfo.Cid = cellIdentity2.getCi();
                        cellInfo.CellId = (long) cellInfo.Cid;
                    }
                    if (cellIdentity2.getPci() != Integer.MAX_VALUE) {
                        cellInfo.LtePci = cellIdentity2.getPci();
                    }
                    if (cellIdentity2.getTac() != Integer.MAX_VALUE) {
                        cellInfo.LteTac = cellIdentity2.getTac();
                    }
                    if (Build.VERSION.SDK_INT >= 24 && cellIdentity2.getEarfcn() != Integer.MAX_VALUE) {
                        cellInfo.Arfcn = cellIdentity2.getEarfcn();
                        DiskAdCacheManager a2 = com.startapp.networkTest.utils.d.a(cellInfo.Arfcn);
                        if (a2 != null) {
                            cellInfo.LteBand = a2.f6309a;
                            cellInfo.LteUploadEarfcn = a2.c;
                            cellInfo.LteDownloadEarfcn = a2.b;
                            cellInfo.LteUploadFrequency = a2.e;
                            cellInfo.LteDonwloadFrequency = a2.d;
                        }
                    }
                    CellSignalStrengthLte cellSignalStrength = cellInfoLte.getCellSignalStrength();
                    cellInfo.Dbm = cellSignalStrength.getDbm();
                    if (cellSignalStrength.getTimingAdvance() != Integer.MAX_VALUE) {
                        cellInfo.LteTimingAdvance = cellSignalStrength.getTimingAdvance();
                    }
                    if (Build.VERSION.SDK_INT >= 29) {
                        int cqi = cellSignalStrength.getCqi();
                        if (cqi != Integer.MAX_VALUE) {
                            cellInfo.LteCqi = cqi;
                        }
                        cellInfo.LteRssnr = cellSignalStrength.getRssnr();
                        cellInfo.LteRsrq = cellSignalStrength.getRsrq();
                        cellInfo.LteRssi = cellSignalStrength.getRssi();
                    } else {
                        Field field = this.I;
                        if (field != null) {
                            try {
                                int i2 = field.getInt(cellSignalStrength);
                                if (i2 != Integer.MAX_VALUE) {
                                    cellInfo.LteCqi = i2;
                                }
                            } catch (IllegalAccessException unused) {
                            }
                        }
                        Field field2 = this.G;
                        if (field2 != null) {
                            try {
                                cellInfo.LteRsrq = field2.getInt(cellSignalStrength);
                            } catch (IllegalAccessException unused2) {
                            }
                        }
                        Field field3 = this.H;
                        if (field3 != null) {
                            try {
                                cellInfo.LteRssnr = field3.getInt(cellSignalStrength);
                            } catch (IllegalAccessException unused3) {
                            }
                        }
                        Field field4 = this.F;
                        if (field4 != null) {
                            try {
                                cellInfo.LteRssi = field4.getInt(cellSignalStrength);
                            } catch (IllegalAccessException unused4) {
                            }
                        }
                    }
                } else if (next instanceof CellInfoWcdma) {
                    CellInfoWcdma cellInfoWcdma = (CellInfoWcdma) next;
                    cellInfo.IsRegistered = cellInfoWcdma.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Wcdma;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoWcdma.getTimeStamp() / 1000000);
                    CellIdentityWcdma cellIdentity3 = cellInfoWcdma.getCellIdentity();
                    if (cellIdentity3.getMcc() != Integer.MAX_VALUE) {
                        cellInfo.Mcc = cellIdentity3.getMcc();
                    }
                    if (cellIdentity3.getMnc() != Integer.MAX_VALUE) {
                        cellInfo.Mnc = cellIdentity3.getMnc();
                    }
                    if (cellIdentity3.getCid() != Integer.MAX_VALUE) {
                        cellInfo.Cid = cellIdentity3.getCid();
                        cellInfo.CellId = (long) cellInfo.Cid;
                    }
                    if (cellIdentity3.getLac() != Integer.MAX_VALUE) {
                        cellInfo.Lac = cellIdentity3.getLac();
                    }
                    if (cellIdentity3.getPsc() != Integer.MAX_VALUE) {
                        cellInfo.Psc = cellIdentity3.getPsc();
                    }
                    if (Build.VERSION.SDK_INT >= 24 && cellIdentity3.getUarfcn() != Integer.MAX_VALUE) {
                        cellInfo.Arfcn = cellIdentity3.getUarfcn();
                    }
                    cellInfo.Dbm = cellInfoWcdma.getCellSignalStrength().getDbm();
                } else if (next instanceof CellInfoCdma) {
                    CellInfoCdma cellInfoCdma = (CellInfoCdma) next;
                    cellInfo.IsRegistered = cellInfoCdma.isRegistered();
                    cellInfo.CellNetworkType = CellNetworkTypes.Gsm;
                    cellInfo.CellInfoAge = uptimeMillis - (cellInfoCdma.getTimeStamp() / 1000000);
                    CellIdentityCdma cellIdentity4 = cellInfoCdma.getCellIdentity();
                    cellInfo.CdmaBaseStationLatitude = cellIdentity4.getLatitude();
                    cellInfo.CdmaBaseStationLongitude = cellIdentity4.getLongitude();
                    if (cellIdentity4.getSystemId() != Integer.MAX_VALUE) {
                        cellInfo.CdmaSystemId = cellIdentity4.getSystemId();
                    }
                    if (cellIdentity4.getNetworkId() != Integer.MAX_VALUE) {
                        cellInfo.CdmaNetworkId = cellIdentity4.getNetworkId();
                    }
                    if (cellIdentity4.getBasestationId() != Integer.MAX_VALUE) {
                        cellInfo.CdmaBaseStationId = cellIdentity4.getBasestationId();
                    }
                    CellSignalStrengthCdma cellSignalStrength2 = cellInfoCdma.getCellSignalStrength();
                    cellInfo.Dbm = cellSignalStrength2.getDbm();
                    cellInfo.CdmaDbm = cellSignalStrength2.getCdmaDbm();
                    cellInfo.CdmaEcio = cellSignalStrength2.getCdmaEcio();
                    cellInfo.EvdoDbm = cellSignalStrength2.getEvdoDbm();
                    cellInfo.EvdoEcio = cellSignalStrength2.getEvdoEcio();
                    cellInfo.EvdoSnr = cellSignalStrength2.getEvdoSnr();
                } else if (Build.VERSION.SDK_INT >= 29 && (next instanceof CellInfoNr)) {
                    try {
                        CellInfoNr cellInfoNr = (CellInfoNr) next;
                        cellInfo.IsRegistered = cellInfoNr.isRegistered();
                        cellInfo.CellNetworkType = CellNetworkTypes.Nr;
                        cellInfo.CellInfoAge = uptimeMillis - (cellInfoNr.getTimeStamp() / 1000000);
                        CellIdentity cellIdentity5 = cellInfoNr.getCellIdentity();
                        if (cellIdentity5 instanceof CellIdentityNr) {
                            CellIdentityNr cellIdentityNr = (CellIdentityNr) cellIdentity5;
                            cellInfo.Arfcn = cellIdentityNr.getNrarfcn();
                            cellInfo.LtePci = cellIdentityNr.getPci();
                            cellInfo.LteTac = cellIdentityNr.getTac();
                            cellInfo.CellId = cellIdentityNr.getNci();
                            if (cellIdentityNr.getMccString() != null) {
                                try {
                                    cellInfo.Mcc = Integer.parseInt(cellIdentityNr.getMccString());
                                } catch (NumberFormatException e2) {
                                    new StringBuilder("cellIdentityNr.getMccString: ").append(e2.getMessage());
                                }
                            }
                            if (cellIdentityNr.getMncString() != null) {
                                try {
                                    cellInfo.Mnc = Integer.parseInt(cellIdentityNr.getMncString());
                                } catch (NumberFormatException e3) {
                                    new StringBuilder("cellIdentityNr.getMncString: ").append(e3.getMessage());
                                }
                            }
                        }
                        CellSignalStrength cellSignalStrength3 = cellInfoNr.getCellSignalStrength();
                        if (cellSignalStrength3 instanceof CellSignalStrengthNr) {
                            CellSignalStrengthNr cellSignalStrengthNr = (CellSignalStrengthNr) cellSignalStrength3;
                            cellInfo.Dbm = cellSignalStrengthNr.getDbm();
                            cellInfo.NrCsiRsrp = cellSignalStrengthNr.getCsiRsrp();
                            cellInfo.NrCsiRsrq = cellSignalStrengthNr.getCsiRsrq();
                            cellInfo.NrCsiSinr = cellSignalStrengthNr.getCsiSinr();
                            cellInfo.NrSsRsrp = cellSignalStrengthNr.getSsRsrp();
                            cellInfo.NrSsRsrq = cellSignalStrengthNr.getSsRsrq();
                            cellInfo.NrSsSinr = cellSignalStrengthNr.getSsSinr();
                        }
                    } catch (Exception e4) {
                        e4.printStackTrace();
                    }
                }
                arrayList.add(cellInfo);
            }
        }
        return (com.startapp.networkTest.data.radio.CellInfo[]) arrayList.toArray(new com.startapp.networkTest.data.radio.CellInfo[arrayList.size()]);
    }

    @TargetApi(21)
    public final ApnInfo[] e() {
        g e2;
        ArrayList arrayList = new ArrayList();
        for (a next : o()) {
            ApnInfo apnInfo = new ApnInfo();
            apnInfo.Apn = next.b;
            apnInfo.TxBytes = next.g;
            apnInfo.RxBytes = next.h;
            apnInfo.ApnTypes = next.c;
            apnInfo.Capabilities = next.d;
            apnInfo.SubscriptionId = next.f;
            apnInfo.PcscfAddresses = next.e;
            apnInfo.MobileDataConnectionState = next.j;
            apnInfo.NetworkType = b(next.f5991a);
            apnInfo.Reason = this.j.a(next.f, next.c);
            if (apnInfo.ApnTypes.contains("ims") && (e2 = this.j.e(next.f)) != null) {
                apnInfo.SamsungSipError = e2.f5997a;
                apnInfo.SamsungImsServices = e2.b;
            }
            arrayList.add(apnInfo);
        }
        return (ApnInfo[]) arrayList.toArray(new ApnInfo[arrayList.size()]);
    }

    class e extends BroadcastReceiver {

        /* renamed from: a  reason: collision with root package name */
        private String f5995a;
        private String b;

        private e() {
            this.f5995a = "android.intent.action.ANY_DATA_STATE";
            this.b = "com.samsung.ims.action.IMS_REGISTRATION";
        }

        public final void onReceive(Context context, Intent intent) {
            if (intent != null && intent.getAction() != null) {
                try {
                    String action = intent.getAction();
                    Bundle extras = intent.getExtras();
                    int i = -1;
                    if (action.equalsIgnoreCase("android.intent.action.ANY_DATA_STATE") && extras != null) {
                        String string = extras.getString("reason", "");
                        String string2 = extras.getString("apnType", "");
                        if (extras.get("subscription") instanceof Integer) {
                            i = extras.getInt("subscription", -1);
                        } else if (extras.get("subscription") instanceof Long) {
                            i = (int) extras.getLong("subscription", -1);
                        }
                        if (string2.equalsIgnoreCase("default")) {
                            string2 = "supl";
                        }
                        c.this.j.a(i, string2, string);
                    } else if (action.equalsIgnoreCase("com.samsung.ims.action.IMS_REGISTRATION") && extras != null) {
                        String string3 = extras.getString("SERVICE");
                        int i2 = extras.getInt("PHONE_ID", -1);
                        int i3 = extras.getInt("SIP_ERROR", -1);
                        extras.getBoolean("VOWIFI", false);
                        extras.getBoolean("REGISTERED", false);
                        g gVar = new g(c.this, (byte) 0);
                        gVar.f5997a = i3;
                        if (string3 != null) {
                            gVar.b = string3.replaceAll("\\[", "").replaceAll("\\]", "").replace(", ", ",");
                        }
                        Iterator<com.startapp.networkTest.data.a.b> it2 = b.g(c.this.f).SimInfos.iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            com.startapp.networkTest.data.a.b next = it2.next();
                            if (next.SimSlotIndex == i2) {
                                i = next.SubscriptionId;
                                break;
                            }
                        }
                        c.this.j.a(i, gVar);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /* synthetic */ e(c cVar, byte b2) {
            this();
        }
    }

    class g {

        /* renamed from: a  reason: collision with root package name */
        int f5997a;
        String b;

        private g() {
            this.f5997a = -1;
            this.b = "";
        }

        /* synthetic */ g(c cVar, byte b2) {
            this();
        }
    }

    class j extends PhoneStateListener {
        private Field b;
        private int c = -1;

        public j() {
        }

        /* access modifiers changed from: package-private */
        /* JADX WARNING: Removed duplicated region for block: B:14:? A[RETURN, SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:9:0x0031  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final int a() {
            /*
                r5 = this;
                java.lang.reflect.Field r0 = r5.b
                r1 = -1
                if (r0 == 0) goto L_0x002a
                java.lang.Object r0 = r0.get(r5)     // Catch:{ IllegalAccessException -> 0x0010 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ IllegalAccessException -> 0x0010 }
                int r0 = r0.intValue()     // Catch:{ IllegalAccessException -> 0x0010 }
                goto L_0x002b
            L_0x0010:
                r0 = move-exception
                java.lang.String r2 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r3 = new java.lang.StringBuilder
                java.lang.String r4 = "getHiddenSubscriptionId: "
                r3.<init>(r4)
                java.lang.String r0 = r0.toString()
                r3.append(r0)
                java.lang.String r0 = r3.toString()
                android.util.Log.e(r2, r0)
            L_0x002a:
                r0 = -1
            L_0x002b:
                int r2 = android.os.Build.VERSION.SDK_INT
                r3 = 29
                if (r2 < r3) goto L_0x003a
                if (r0 == r1) goto L_0x0038
                r1 = 2147483647(0x7fffffff, float:NaN)
                if (r0 != r1) goto L_0x003a
            L_0x0038:
                int r0 = r5.c
            L_0x003a:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.j.a():int");
        }

        /* JADX WARNING: Removed duplicated region for block: B:23:0x007d  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0085  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x0088  */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onCellInfoChanged(java.util.List<android.telephony.CellInfo> r11) {
            /*
                r10 = this;
                if (r11 == 0) goto L_0x00a7
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                java.util.List unused = r0.n = r11
                int r0 = android.os.Build.VERSION.SDK_INT
                r1 = 29
                if (r0 < r1) goto L_0x00a7
                java.util.Iterator r11 = r11.iterator()
            L_0x0011:
                boolean r0 = r11.hasNext()
                if (r0 == 0) goto L_0x00a7
                java.lang.Object r0 = r11.next()
                android.telephony.CellInfo r0 = (android.telephony.CellInfo) r0
                boolean r1 = r0.isRegistered()
                if (r1 == 0) goto L_0x0011
                boolean r1 = r0 instanceof android.telephony.CellInfoNr
                if (r1 == 0) goto L_0x0011
                android.telephony.CellInfoNr r0 = (android.telephony.CellInfoNr) r0
                android.telephony.CellIdentity r1 = r0.getCellIdentity()
                boolean r2 = r1 instanceof android.telephony.CellIdentityNr
                if (r2 == 0) goto L_0x0011
                android.telephony.CellIdentityNr r1 = (android.telephony.CellIdentityNr) r1
                r2 = 0
                java.lang.String r3 = r1.getMccString()     // Catch:{ NumberFormatException -> 0x0047 }
                int r3 = java.lang.Integer.parseInt(r3)     // Catch:{ NumberFormatException -> 0x0047 }
                java.lang.String r4 = r1.getMncString()     // Catch:{ NumberFormatException -> 0x0045 }
                int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ NumberFormatException -> 0x0045 }
                goto L_0x005b
            L_0x0045:
                r4 = move-exception
                goto L_0x0049
            L_0x0047:
                r4 = move-exception
                r3 = 0
            L_0x0049:
                java.lang.String unused = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                java.lang.String r6 = "updateCellInfo: "
                r5.<init>(r6)
                java.lang.String r4 = r4.getMessage()
                r5.append(r4)
                r4 = 0
            L_0x005b:
                long r5 = r1.getNci()
                int r7 = r1.getTac()
                int r1 = r1.getPci()
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r8.<init>()
                r8.append(r3)
                r8.append(r4)
                java.lang.String r3 = r8.toString()
                r8 = 2147483647(0x7fffffff, double:1.060997895E-314)
                int r4 = (r5 > r8 ? 1 : (r5 == r8 ? 0 : -1))
                if (r4 != 0) goto L_0x007f
                r5 = -1
            L_0x007f:
                r4 = -1
                r8 = 2147483647(0x7fffffff, float:NaN)
                if (r7 != r8) goto L_0x0086
                r7 = -1
            L_0x0086:
                if (r1 != r8) goto L_0x0089
                r1 = -1
            L_0x0089:
                com.startapp.networkTest.controller.c$f r4 = new com.startapp.networkTest.controller.c$f
                com.startapp.networkTest.controller.c r8 = com.startapp.networkTest.controller.c.this
                r4.<init>(r8, r2)
                r4.f5996a = r5
                r4.b = r7
                r4.c = r1
                long r0 = r0.getTimeStamp()
                r4.d = r0
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                com.startapp.networkTest.controller.c$d r0 = r0.j
                r0.a((java.lang.String) r3, (com.startapp.networkTest.controller.c.f) r4)
                goto L_0x0011
            L_0x00a7:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.j.onCellInfoChanged(java.util.List):void");
        }

        public final void onCellLocationChanged(final CellLocation cellLocation) {
            final int a2 = a();
            long elapsedRealtime = SystemClock.elapsedRealtime();
            b bVar = new b(c.this, (byte) 0);
            bVar.f5992a = cellLocation;
            bVar.b = elapsedRealtime;
            c.this.j.a(a2, bVar);
            c.this.f5986a.post(new Runnable() {
                public final void run() {
                    for (com.startapp.networkTest.controller.a.a a2 : c.this.b) {
                        a2.a(cellLocation, a2);
                    }
                }
            });
        }

        public final void onServiceStateChanged(final ServiceState serviceState) {
            ServiceStates serviceStates;
            DuplexMode duplexMode;
            final int a2 = a();
            h hVar = new h(c.this, (byte) 0);
            if (Build.VERSION.SDK_INT >= 25) {
                if (c.this.E != null) {
                    try {
                        hVar.f = c.this.E.getBoolean(serviceState) ? ThreeStateShort.Yes : ThreeStateShort.No;
                    } catch (IllegalAccessException e) {
                        String h = c.c;
                        Log.e(h, "updateSignalStrengthData.mFieldIsUsingCarrierAggregation: " + e.toString());
                    }
                }
                if (hVar.f == ThreeStateShort.Unknown && c.this.D != null) {
                    try {
                        hVar.f = ((Boolean) c.this.D.invoke(serviceState, new Object[0])).booleanValue() ? ThreeStateShort.Yes : ThreeStateShort.No;
                    } catch (Exception e2) {
                        String h2 = c.c;
                        Log.e(h2, "updateSignalStrengthData.mMethodIsUsingCarrierAggregation: " + e2.toString());
                    }
                }
                if (Build.VERSION.SDK_INT >= 28) {
                    int duplexMode2 = serviceState.getDuplexMode();
                    if (duplexMode2 == 1) {
                        duplexMode = DuplexMode.FDD;
                    } else if (duplexMode2 != 2) {
                        duplexMode = DuplexMode.Unknown;
                    } else {
                        duplexMode = DuplexMode.TDD;
                    }
                    hVar.c = duplexMode;
                    hVar.e = serviceState.getChannelNumber();
                }
            }
            hVar.d = serviceState.getIsManualSelection() ? ThreeStateShort.Yes : ThreeStateShort.No;
            int state = serviceState.getState();
            if (state == 0) {
                serviceStates = ServiceStates.InService;
            } else if (state == 1) {
                serviceStates = ServiceStates.OutOfService;
            } else if (state == 2) {
                serviceStates = ServiceStates.EmergencyOnly;
            } else if (state != 3) {
                serviceStates = ServiceStates.Unknown;
            } else {
                serviceStates = ServiceStates.PowerOff;
            }
            hVar.f5998a = serviceStates;
            hVar.b = SystemClock.elapsedRealtime();
            NetworkRegistrationInfo[] a3 = com.iab.omid.library.startapp.b.a(serviceState.toString());
            c.this.j.a(a2, hVar);
            c.this.j.a(a2, a3);
            c.this.f5986a.post(new Runnable() {
                public final void run() {
                    for (com.startapp.networkTest.controller.a.a a2 : c.this.b) {
                        a2.a(serviceState, a2);
                    }
                }
            });
        }

        /* JADX WARNING: Code restructure failed: missing block: B:34:0x012b, code lost:
            r0 = r5;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:35:0x012c, code lost:
            r6 = r17;
            r7 = r18;
            r12 = r19;
            r20 = true;
         */
        /* JADX WARNING: Removed duplicated region for block: B:133:0x03d5  */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x03df  */
        /* JADX WARNING: Removed duplicated region for block: B:156:0x046d  */
        /* JADX WARNING: Removed duplicated region for block: B:160:0x0477 A[Catch:{ Exception -> 0x048c }] */
        /* JADX WARNING: Removed duplicated region for block: B:165:0x04bb  */
        /* JADX WARNING: Removed duplicated region for block: B:23:0x00f1  */
        /* JADX WARNING: Removed duplicated region for block: B:26:0x0109  */
        /* JADX WARNING: Removed duplicated region for block: B:62:0x01d0  */
        /* JADX WARNING: Removed duplicated region for block: B:85:0x0244 A[Catch:{ Exception -> 0x025e }] */
        /* JADX WARNING: Removed duplicated region for block: B:86:0x0258  */
        /* JADX WARNING: Removed duplicated region for block: B:92:0x0280 A[SYNTHETIC, Splitter:B:92:0x0280] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public final void onSignalStrengthsChanged(android.telephony.SignalStrength r29) {
            /*
                r28 = this;
                r1 = r28
                r2 = r29
                int r3 = r28.a()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r4 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r5 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r6 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r7 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r8 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r9 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r10 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r11 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r12 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r13 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r14 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r15 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r16 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r17 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r18 = r0.intValue()
                java.lang.Integer r0 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r19 = r0.intValue()
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                android.util.SparseArray r0 = r0.e
                if (r0 == 0) goto L_0x007d
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                android.util.SparseArray r0 = r0.e
                java.lang.Object r0 = r0.get(r3)
                android.telephony.TelephonyManager r0 = (android.telephony.TelephonyManager) r0
                goto L_0x007e
            L_0x007d:
                r0 = 0
            L_0x007e:
                if (r0 != 0) goto L_0x0086
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                android.telephony.TelephonyManager r0 = r0.d
            L_0x0086:
                com.startapp.networkTest.enums.NetworkTypes r20 = com.startapp.networkTest.enums.NetworkTypes.Unknown
                r21 = r4
                com.startapp.networkTest.controller.c r4 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x00d4 }
                android.util.SparseArray r4 = r4.e     // Catch:{ Exception -> 0x00d4 }
                if (r4 != 0) goto L_0x00c4
                com.startapp.networkTest.controller.c r4 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x00d4 }
                java.lang.reflect.Method r4 = r4.M     // Catch:{ Exception -> 0x00d4 }
                if (r4 == 0) goto L_0x00c4
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x00d4 }
                java.lang.reflect.Method r0 = r0.M     // Catch:{ Exception -> 0x00d4 }
                com.startapp.networkTest.controller.c r4 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x00d4 }
                android.telephony.TelephonyManager r4 = r4.d     // Catch:{ Exception -> 0x00d4 }
                r24 = r6
                r22 = r7
                r6 = 1
                java.lang.Object[] r7 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x00d2 }
                java.lang.Integer r25 = java.lang.Integer.valueOf(r3)     // Catch:{ Exception -> 0x00d2 }
                r23 = 0
                r7[r23] = r25     // Catch:{ Exception -> 0x00d2 }
                java.lang.Object r0 = r0.invoke(r4, r7)     // Catch:{ Exception -> 0x00d2 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x00d2 }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x00d2 }
                com.startapp.networkTest.enums.NetworkTypes r20 = com.startapp.networkTest.controller.c.b((int) r0)     // Catch:{ Exception -> 0x00d2 }
                goto L_0x00eb
            L_0x00c4:
                r24 = r6
                r22 = r7
                r6 = 1
                int r0 = r0.getNetworkType()     // Catch:{ Exception -> 0x00d2 }
                com.startapp.networkTest.enums.NetworkTypes r20 = com.startapp.networkTest.controller.c.b((int) r0)     // Catch:{ Exception -> 0x00d2 }
                goto L_0x00eb
            L_0x00d2:
                r0 = move-exception
                goto L_0x00da
            L_0x00d4:
                r0 = move-exception
                r24 = r6
                r22 = r7
                r6 = 1
            L_0x00da:
                java.lang.String unused = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                java.lang.String r7 = "updateSignalStrengthData: "
                r4.<init>(r7)
                java.lang.String r0 = r0.getMessage()
                r4.append(r0)
            L_0x00eb:
                r0 = r20
                com.startapp.networkTest.enums.NetworkTypes r4 = com.startapp.networkTest.enums.NetworkTypes.Unknown
                if (r0 != r4) goto L_0x00ff
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                com.startapp.networkTest.controller.c$d r0 = r0.j
                com.startapp.networkTest.data.radio.NetworkRegistrationInfo[] r0 = r0.d(r3)
                com.startapp.networkTest.enums.NetworkTypes r0 = com.startapp.networkTest.controller.c.a((com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]) r0)
            L_0x00ff:
                com.startapp.networkTest.enums.NetworkGenerations r4 = com.startapp.networkTest.controller.c.a((com.startapp.networkTest.enums.NetworkTypes) r0)
                int r0 = android.os.Build.VERSION.SDK_INT
                r7 = 29
                if (r0 < r7) goto L_0x01c4
                java.util.List r0 = r29.getCellSignalStrengths()
                java.util.Iterator r0 = r0.iterator()
            L_0x0111:
                boolean r7 = r0.hasNext()
                if (r7 == 0) goto L_0x01c4
                java.lang.Object r7 = r0.next()
                android.telephony.CellSignalStrength r7 = (android.telephony.CellSignalStrength) r7
                com.startapp.networkTest.enums.NetworkGenerations r6 = com.startapp.networkTest.enums.NetworkGenerations.Gen2
                if (r4 != r6) goto L_0x0136
                boolean r6 = r7 instanceof android.telephony.CellSignalStrengthGsm
                if (r6 == 0) goto L_0x0136
                android.telephony.CellSignalStrengthGsm r7 = (android.telephony.CellSignalStrengthGsm) r7
                int r5 = r7.getDbm()
            L_0x012b:
                r0 = r5
            L_0x012c:
                r6 = r17
                r7 = r18
                r12 = r19
                r20 = 1
                goto L_0x01ce
            L_0x0136:
                com.startapp.networkTest.enums.NetworkGenerations r6 = com.startapp.networkTest.enums.NetworkGenerations.Gen3
                if (r4 != r6) goto L_0x015c
                boolean r6 = r7 instanceof android.telephony.CellSignalStrengthWcdma
                if (r6 == 0) goto L_0x015c
                android.telephony.CellSignalStrengthWcdma r7 = (android.telephony.CellSignalStrengthWcdma) r7
                int r0 = r7.getDbm()
                java.lang.Integer r6 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r6 = r6.intValue()
                if (r5 != r6) goto L_0x014d
                r5 = r0
            L_0x014d:
                r6 = r17
                r7 = r18
                r12 = r19
                r20 = 1
                r27 = r5
                r5 = r0
                r0 = r27
                goto L_0x01ce
            L_0x015c:
                com.startapp.networkTest.enums.NetworkGenerations r6 = com.startapp.networkTest.enums.NetworkGenerations.Gen4
                if (r4 != r6) goto L_0x0184
                boolean r6 = r7 instanceof android.telephony.CellSignalStrengthLte
                if (r6 == 0) goto L_0x0184
                android.telephony.CellSignalStrengthLte r7 = (android.telephony.CellSignalStrengthLte) r7
                int r5 = r7.getDbm()
                int r6 = r7.getCqi()
                int r0 = r7.getRsrp()
                int r8 = r7.getRssnr()
                int r9 = r7.getRsrq()
                int r10 = r7.getRssi()
                r22 = r0
                r0 = r5
                r24 = r6
                goto L_0x012c
            L_0x0184:
                com.startapp.networkTest.enums.NetworkGenerations r6 = com.startapp.networkTest.enums.NetworkGenerations.Gen5
                if (r4 != r6) goto L_0x01ab
                boolean r6 = r7 instanceof android.telephony.CellSignalStrengthNr
                if (r6 == 0) goto L_0x01ab
                android.telephony.CellSignalStrengthNr r7 = (android.telephony.CellSignalStrengthNr) r7
                int r5 = r7.getDbm()
                int r14 = r7.getCsiRsrp()
                int r15 = r7.getCsiRsrq()
                int r16 = r7.getCsiSinr()
                int r17 = r7.getSsRsrp()
                int r18 = r7.getSsRsrq()
                int r19 = r7.getSsSinr()
                goto L_0x012b
            L_0x01ab:
                com.startapp.networkTest.enums.NetworkGenerations r6 = com.startapp.networkTest.enums.NetworkGenerations.Gen2
                if (r4 != r6) goto L_0x01c1
                boolean r6 = r7 instanceof android.telephony.CellSignalStrengthCdma
                if (r6 == 0) goto L_0x01c1
                android.telephony.CellSignalStrengthCdma r7 = (android.telephony.CellSignalStrengthCdma) r7
                int r0 = r7.getCdmaEcio()
                int r5 = r7.getDbm()
                r21 = r0
                goto L_0x012b
            L_0x01c1:
                r6 = 1
                goto L_0x0111
            L_0x01c4:
                r0 = r5
                r5 = r12
                r6 = r17
                r7 = r18
                r12 = r19
                r20 = 0
            L_0x01ce:
                if (r20 != 0) goto L_0x04bb
                boolean r0 = r29.isGsm()
                if (r0 == 0) goto L_0x022b
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                java.lang.reflect.Field r0 = r0.y
                if (r0 == 0) goto L_0x01ec
                com.startapp.networkTest.enums.NetworkGenerations r0 = com.startapp.networkTest.enums.NetworkGenerations.Gen3
                if (r4 != r0) goto L_0x01ec
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ IllegalAccessException -> 0x01f3 }
                java.lang.reflect.Field r0 = r0.y     // Catch:{ IllegalAccessException -> 0x01f3 }
                int r13 = r0.getInt(r2)     // Catch:{ IllegalAccessException -> 0x01f3 }
            L_0x01ec:
                r17 = r5
                r18 = r8
                r19 = r9
                goto L_0x0213
            L_0x01f3:
                r0 = move-exception
                r17 = r5
                java.lang.String r5 = com.startapp.networkTest.controller.c.c
                r18 = r8
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                r19 = r9
                java.lang.String r9 = "updateSignalStrengthData.WcdmaRscp: "
                r8.<init>(r9)
                java.lang.String r0 = r0.toString()
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                android.util.Log.e(r5, r0)
            L_0x0213:
                int r0 = r29.getGsmSignalStrength()
                if (r0 != 0) goto L_0x0223
                java.lang.Integer r5 = com.startapp.networkTest.data.RadioInfo.INVALID
                int r5 = r5.intValue()
                if (r13 == r5) goto L_0x0223
                r5 = r13
                goto L_0x023c
            L_0x0223:
                if (r0 >= 0) goto L_0x0226
                goto L_0x023b
            L_0x0226:
                int r0 = com.startapp.networkTest.controller.c.e((int) r0)
                goto L_0x023b
            L_0x022b:
                r17 = r5
                r18 = r8
                r19 = r9
                int r0 = r29.getCdmaDbm()
                int r5 = r29.getCdmaEcio()
                r21 = r5
            L_0x023b:
                r5 = r0
            L_0x023c:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x025e }
                java.lang.reflect.Method r0 = r0.o     // Catch:{ Exception -> 0x025e }
                if (r0 == 0) goto L_0x0258
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x025e }
                java.lang.reflect.Method r0 = r0.o     // Catch:{ Exception -> 0x025e }
                r8 = 0
                java.lang.Object[] r9 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x025e }
                java.lang.Object r0 = r0.invoke(r2, r9)     // Catch:{ Exception -> 0x025e }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x025e }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x025e }
                goto L_0x025a
            L_0x0258:
                r0 = r17
            L_0x025a:
                r20 = r5
                r5 = r0
                goto L_0x027c
            L_0x025e:
                r0 = move-exception
                java.lang.String r8 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r20 = r5
                java.lang.String r5 = "updateSignalStrengthData.GetDbm: "
                r9.<init>(r5)
                java.lang.String r0 = r0.toString()
                r9.append(r0)
                java.lang.String r0 = r9.toString()
                android.util.Log.e(r8, r0)
                r5 = r17
            L_0x027c:
                com.startapp.networkTest.enums.NetworkGenerations r0 = com.startapp.networkTest.enums.NetworkGenerations.Gen4
                if (r4 != r0) goto L_0x03d5
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x02a3 }
                java.lang.reflect.Method r0 = r0.q     // Catch:{ Exception -> 0x02a3 }
                if (r0 == 0) goto L_0x029c
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x02a3 }
                java.lang.reflect.Method r0 = r0.q     // Catch:{ Exception -> 0x02a3 }
                r8 = 0
                java.lang.Object[] r9 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x02a3 }
                java.lang.Object r0 = r0.invoke(r2, r9)     // Catch:{ Exception -> 0x02a3 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x02a3 }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x02a3 }
                goto L_0x029e
            L_0x029c:
                r0 = r20
            L_0x029e:
                r20 = r0
                r17 = r5
                goto L_0x02bf
            L_0x02a3:
                r0 = move-exception
                java.lang.String r8 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r9 = new java.lang.StringBuilder
                r17 = r5
                java.lang.String r5 = "updateSignalStrengthData.GetLTEDbm: "
                r9.<init>(r5)
                java.lang.String r0 = r0.toString()
                r9.append(r0)
                java.lang.String r0 = r9.toString()
                android.util.Log.e(r8, r0)
            L_0x02bf:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                java.lang.reflect.Method r0 = r0.p
                if (r0 == 0) goto L_0x02f5
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x02db }
                java.lang.reflect.Method r0 = r0.p     // Catch:{ Exception -> 0x02db }
                r5 = 0
                java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x02db }
                java.lang.Object r0 = r0.invoke(r2, r8)     // Catch:{ Exception -> 0x02db }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x02db }
                int r10 = r0.intValue()     // Catch:{ Exception -> 0x02db }
                goto L_0x02f5
            L_0x02db:
                r0 = move-exception
                java.lang.String r5 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "updateSignalStrengthData.GetLteSignalStrength: "
                r8.<init>(r9)
                java.lang.String r0 = r0.toString()
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                android.util.Log.e(r5, r0)
            L_0x02f5:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x0311 }
                java.lang.reflect.Method r0 = r0.s     // Catch:{ Exception -> 0x0311 }
                if (r0 == 0) goto L_0x032b
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x0311 }
                java.lang.reflect.Method r0 = r0.s     // Catch:{ Exception -> 0x0311 }
                r5 = 0
                java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0311 }
                java.lang.Object r0 = r0.invoke(r2, r8)     // Catch:{ Exception -> 0x0311 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0311 }
                int r24 = r0.intValue()     // Catch:{ Exception -> 0x0311 }
                goto L_0x032b
            L_0x0311:
                r0 = move-exception
                java.lang.String r5 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "updateSignalStrengthData.GetLteCqi: "
                r8.<init>(r9)
                java.lang.String r0 = r0.toString()
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                android.util.Log.e(r5, r0)
            L_0x032b:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x0347 }
                java.lang.reflect.Method r0 = r0.t     // Catch:{ Exception -> 0x0347 }
                if (r0 == 0) goto L_0x0361
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x0347 }
                java.lang.reflect.Method r0 = r0.t     // Catch:{ Exception -> 0x0347 }
                r5 = 0
                java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0347 }
                java.lang.Object r0 = r0.invoke(r2, r8)     // Catch:{ Exception -> 0x0347 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0347 }
                int r22 = r0.intValue()     // Catch:{ Exception -> 0x0347 }
                goto L_0x0361
            L_0x0347:
                r0 = move-exception
                java.lang.String r5 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "updateSignalStrengthData.GetLteRsrp: "
                r8.<init>(r9)
                java.lang.String r0 = r0.toString()
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                android.util.Log.e(r5, r0)
            L_0x0361:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x0382 }
                java.lang.reflect.Method r0 = r0.u     // Catch:{ Exception -> 0x0382 }
                if (r0 == 0) goto L_0x037d
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x0382 }
                java.lang.reflect.Method r0 = r0.u     // Catch:{ Exception -> 0x0382 }
                r5 = 0
                java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0382 }
                java.lang.Object r0 = r0.invoke(r2, r8)     // Catch:{ Exception -> 0x0382 }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x0382 }
                int r9 = r0.intValue()     // Catch:{ Exception -> 0x0382 }
                goto L_0x037f
            L_0x037d:
                r9 = r19
            L_0x037f:
                r19 = r9
                goto L_0x039c
            L_0x0382:
                r0 = move-exception
                java.lang.String r5 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "updateSignalStrengthData.GetLteRsrq: "
                r8.<init>(r9)
                java.lang.String r0 = r0.toString()
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                android.util.Log.e(r5, r0)
            L_0x039c:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x03ba }
                java.lang.reflect.Method r0 = r0.v     // Catch:{ Exception -> 0x03ba }
                if (r0 == 0) goto L_0x03d7
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x03ba }
                java.lang.reflect.Method r0 = r0.v     // Catch:{ Exception -> 0x03ba }
                r5 = 0
                java.lang.Object[] r8 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x03ba }
                java.lang.Object r0 = r0.invoke(r2, r8)     // Catch:{ Exception -> 0x03ba }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x03ba }
                int r8 = r0.intValue()     // Catch:{ Exception -> 0x03ba }
                r18 = r8
                goto L_0x03d7
            L_0x03ba:
                r0 = move-exception
                java.lang.String r5 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "updateSignalStrengthData.GetLteRssnr: "
                r8.<init>(r9)
                java.lang.String r0 = r0.toString()
                r8.append(r0)
                java.lang.String r0 = r8.toString()
                android.util.Log.e(r5, r0)
                goto L_0x03d7
            L_0x03d5:
                r17 = r5
            L_0x03d7:
                r8 = r18
                r9 = r19
                com.startapp.networkTest.enums.NetworkGenerations r0 = com.startapp.networkTest.enums.NetworkGenerations.Gen5
                if (r4 != r0) goto L_0x046d
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                java.lang.reflect.Field r0 = r0.A
                if (r0 == 0) goto L_0x03f1
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ IllegalAccessException -> 0x03f4 }
                java.lang.reflect.Field r0 = r0.A     // Catch:{ IllegalAccessException -> 0x03f4 }
                int r14 = r0.getInt(r2)     // Catch:{ IllegalAccessException -> 0x03f4 }
            L_0x03f1:
                r18 = r8
                goto L_0x0410
            L_0x03f4:
                r0 = move-exception
                java.lang.String r4 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                r18 = r8
                java.lang.String r8 = "updateSignalStrengthData.NrCsiRsrp: "
                r5.<init>(r8)
                java.lang.String r0 = r0.toString()
                r5.append(r0)
                java.lang.String r0 = r5.toString()
                android.util.Log.e(r4, r0)
            L_0x0410:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                java.lang.reflect.Field r0 = r0.B
                if (r0 == 0) goto L_0x043d
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ IllegalAccessException -> 0x0423 }
                java.lang.reflect.Field r0 = r0.B     // Catch:{ IllegalAccessException -> 0x0423 }
                int r15 = r0.getInt(r2)     // Catch:{ IllegalAccessException -> 0x0423 }
                goto L_0x043d
            L_0x0423:
                r0 = move-exception
                java.lang.String r4 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                java.lang.String r8 = "updateSignalStrengthData.NrCsiRsrq: "
                r5.<init>(r8)
                java.lang.String r0 = r0.toString()
                r5.append(r0)
                java.lang.String r0 = r5.toString()
                android.util.Log.e(r4, r0)
            L_0x043d:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                java.lang.reflect.Field r0 = r0.C
                if (r0 == 0) goto L_0x046f
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ IllegalAccessException -> 0x0452 }
                java.lang.reflect.Field r0 = r0.C     // Catch:{ IllegalAccessException -> 0x0452 }
                int r0 = r0.getInt(r2)     // Catch:{ IllegalAccessException -> 0x0452 }
                r16 = r0
                goto L_0x046f
            L_0x0452:
                r0 = move-exception
                java.lang.String r4 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r5 = new java.lang.StringBuilder
                java.lang.String r8 = "updateSignalStrengthData.NrCsiSinr: "
                r5.<init>(r8)
                java.lang.String r0 = r0.toString()
                r5.append(r0)
                java.lang.String r0 = r5.toString()
                android.util.Log.e(r4, r0)
                goto L_0x046f
            L_0x046d:
                r18 = r8
            L_0x046f:
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x048c }
                java.lang.reflect.Method r0 = r0.r     // Catch:{ Exception -> 0x048c }
                if (r0 == 0) goto L_0x04a6
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this     // Catch:{ Exception -> 0x048c }
                java.lang.reflect.Method r0 = r0.r     // Catch:{ Exception -> 0x048c }
                r4 = 0
                java.lang.Object[] r5 = new java.lang.Object[r4]     // Catch:{ Exception -> 0x048c }
                java.lang.Object r0 = r0.invoke(r2, r5)     // Catch:{ Exception -> 0x048c }
                java.lang.Integer r0 = (java.lang.Integer) r0     // Catch:{ Exception -> 0x048c }
                int r0 = r0.intValue()     // Catch:{ Exception -> 0x048c }
                r11 = r0
                goto L_0x04a6
            L_0x048c:
                r0 = move-exception
                java.lang.String r2 = com.startapp.networkTest.controller.c.c
                java.lang.StringBuilder r4 = new java.lang.StringBuilder
                java.lang.String r5 = "updateSignalStrengthData.GetEcno: "
                r4.<init>(r5)
                java.lang.String r0 = r0.toString()
                r4.append(r0)
                java.lang.String r0 = r4.toString()
                android.util.Log.e(r2, r0)
            L_0x04a6:
                r26 = r11
                r4 = r17
                r2 = r20
                r0 = r21
                r8 = r22
                r5 = r24
                r17 = r6
                r11 = r10
                r10 = r9
                r9 = r18
                r18 = r7
                goto L_0x04d5
            L_0x04bb:
                r17 = r5
                r18 = r8
                r19 = r9
                r2 = r0
                r26 = r11
                r4 = r17
                r9 = r18
                r0 = r21
                r8 = r22
                r5 = r24
                r17 = r6
                r18 = r7
                r11 = r10
                r10 = r19
            L_0x04d5:
                r27 = r16
                r16 = r3
                r3 = r13
                r13 = r27
                long r6 = android.os.SystemClock.elapsedRealtime()
                r19 = r6
                com.startapp.networkTest.controller.c$i r6 = new com.startapp.networkTest.controller.c$i
                com.startapp.networkTest.controller.c r7 = com.startapp.networkTest.controller.c.this
                r1 = 0
                r6.<init>(r7, r1)
                r6.c = r0
                r6.f5999a = r2
                r6.b = r4
                r6.d = r5
                r6.e = r8
                r6.f = r9
                r6.g = r10
                r6.h = r11
                r6.l = r14
                r6.m = r15
                r6.n = r13
                r1 = r17
                r6.o = r1
                r1 = r18
                r6.p = r1
                r6.q = r12
                r6.j = r3
                r11 = r26
                r6.i = r11
                r0 = r19
                r6.k = r0
                r1 = r28
                com.startapp.networkTest.controller.c r0 = com.startapp.networkTest.controller.c.this
                com.startapp.networkTest.controller.c$d r0 = r0.j
                r2 = r16
                r0.a((int) r2, (com.startapp.networkTest.controller.c.i) r6)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.j.onSignalStrengthsChanged(android.telephony.SignalStrength):void");
        }

        public j(int i) {
            this.c = i;
            try {
                this.b = getClass().getSuperclass().getDeclaredField("mSubId");
                this.b.setAccessible(true);
                this.b.set(this, Integer.valueOf(i));
            } catch (Exception unused) {
            }
        }
    }

    private static PreferredNetworkTypes h(int i2) {
        switch (i2) {
            case 0:
                return PreferredNetworkTypes.WCDMA_PREF;
            case 1:
                return PreferredNetworkTypes.GSM_ONLY;
            case 2:
                return PreferredNetworkTypes.WCDMA_ONLY;
            case 3:
                return PreferredNetworkTypes.GSM_UMTS;
            case 4:
                return PreferredNetworkTypes.CDMA;
            case 5:
                return PreferredNetworkTypes.CDMA_NO_EVDO;
            case 6:
                return PreferredNetworkTypes.EVDO_NO_CDMA;
            case 7:
                return PreferredNetworkTypes.GLOBAL;
            case 8:
                return PreferredNetworkTypes.LTE_CDMA_EVDO;
            case 9:
                return PreferredNetworkTypes.LTE_GSM_WCDMA;
            case 10:
                return PreferredNetworkTypes.LTE_CDMA_EVDO_GSM_WCDMA;
            case 11:
                return PreferredNetworkTypes.LTE_ONLY;
            case 12:
                return PreferredNetworkTypes.LTE_WCDMA;
            case 13:
                return PreferredNetworkTypes.TDSCDMA_ONLY;
            case 14:
                return PreferredNetworkTypes.TDSCDMA_WCDMA;
            case 15:
                return PreferredNetworkTypes.LTE_TDSCDMA;
            case 16:
                return PreferredNetworkTypes.TDSCDMA_GSM;
            case 17:
                return PreferredNetworkTypes.LTE_TDSCDMA_GSM;
            case 18:
                return PreferredNetworkTypes.TDSCDMA_GSM_WCDMA;
            case 19:
                return PreferredNetworkTypes.LTE_TDSCDMA_WCDMA;
            case 20:
                return PreferredNetworkTypes.LTE_TDSCDMA_GSM_WCDMA;
            case 21:
                return PreferredNetworkTypes.TDSCDMA_CDMA_EVDO_GSM_WCDMA;
            case 22:
                return PreferredNetworkTypes.LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA;
            case 23:
                return PreferredNetworkTypes.NR_ONLY;
            case 24:
                return PreferredNetworkTypes.NR_LTE;
            case 25:
                return PreferredNetworkTypes.NR_LTE_CDMA_EVDO;
            case 26:
                return PreferredNetworkTypes.NR_LTE_GSM_WCDMA;
            case 27:
                return PreferredNetworkTypes.NR_LTE_CDMA_EVDO_GSM_WCDMA;
            case 28:
                return PreferredNetworkTypes.NR_LTE_WCDMA;
            case 29:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA;
            case 30:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_GSM;
            case 31:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_WCDMA;
            case 32:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_GSM_WCDMA;
            case 33:
                return PreferredNetworkTypes.NR_LTE_TDSCDMA_CDMA_EVDO_GSM_WCDMA;
            default:
                return PreferredNetworkTypes.Unknown;
        }
    }

    public final void b() {
        try {
            b(true);
            Context context = this.f;
            if (context != null && this.m != null) {
                context.unregisterReceiver(this.m);
            }
        } catch (Throwable th) {
            com.startapp.networkTest.startapp.a.a(th);
        }
    }

    public final ConnectionTypes f() {
        NetworkInfo activeNetworkInfo;
        ConnectionTypes connectionTypes = ConnectionTypes.Unknown;
        if (this.i == null || this.f.checkCallingOrSelfPermission("android.permission.ACCESS_NETWORK_STATE") != 0 || (activeNetworkInfo = this.i.getActiveNetworkInfo()) == null) {
            return connectionTypes;
        }
        int type = activeNetworkInfo.getType();
        if (type == 0) {
            return ConnectionTypes.Mobile;
        }
        if (type == 1) {
            return ConnectionTypes.WiFi;
        }
        if (type == 6) {
            return ConnectionTypes.WiMAX;
        }
        if (type == 7) {
            return ConnectionTypes.Bluetooth;
        }
        if (type != 9) {
            return connectionTypes;
        }
        return ConnectionTypes.Ethernet;
    }

    class f {

        /* renamed from: a  reason: collision with root package name */
        long f5996a;
        int b;
        int c;
        long d;

        private f() {
            this.f5996a = 0;
            this.b = 0;
            this.c = 0;
            this.d = 0;
        }

        /* synthetic */ f(c cVar, byte b2) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public void a(int[] iArr) {
        this.h = new ArrayList<>();
        for (int jVar : iArr) {
            this.h.add(new j(jVar));
        }
    }

    public final boolean c(int i2) {
        Method method = this.L;
        if (method != null) {
            try {
                return ((Boolean) method.invoke(this.d, new Object[]{Integer.valueOf(i2)})).booleanValue();
            } catch (Exception e2) {
                String str = c;
                Log.e(str, "isRoaming: " + e2.toString());
            }
        }
        return this.d.isNetworkRoaming();
    }

    class h {

        /* renamed from: a  reason: collision with root package name */
        ServiceStates f5998a;
        long b;
        DuplexMode c;
        ThreeStateShort d;
        int e;
        ThreeStateShort f;

        private h() {
            this.f5998a = ServiceStates.Unknown;
            this.b = 0;
            this.c = DuplexMode.Unknown;
            ThreeStateShort threeStateShort = ThreeStateShort.Unknown;
            this.d = threeStateShort;
            this.e = -1;
            this.f = threeStateShort;
        }

        /* synthetic */ h(c cVar, byte b2) {
            this();
        }
    }

    public final void a() {
        try {
            a(true);
            Context context = this.f;
            if (this.m == null) {
                this.m = new e(this, (byte) 0);
            }
            this.m.getClass();
            IntentFilter intentFilter = new IntentFilter("android.intent.action.ANY_DATA_STATE");
            this.m.getClass();
            intentFilter.addAction("com.samsung.ims.action.IMS_REGISTRATION");
            context.registerReceiver(this.m, intentFilter);
        } catch (Throwable th) {
            com.startapp.networkTest.startapp.a.a(th);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        SubscriptionManager subscriptionManager;
        if (z2 && this.k != null && this.f.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0 && Build.VERSION.SDK_INT >= 22 && (subscriptionManager = (SubscriptionManager) this.f.getSystemService("telephony_subscription_service")) != null) {
            subscriptionManager.removeOnSubscriptionsChangedListener(this.k);
        }
        TelephonyManager telephonyManager = this.d;
        if (telephonyManager != null) {
            j jVar = this.g;
            if (jVar != null) {
                telephonyManager.listen(jVar, 0);
            }
            Iterator<j> it2 = this.h.iterator();
            while (it2.hasNext()) {
                j next = it2.next();
                TelephonyManager telephonyManager2 = null;
                SparseArray<TelephonyManager> sparseArray = this.e;
                if (sparseArray != null && sparseArray.size() > 0) {
                    telephonyManager2 = this.e.get(next.a());
                }
                if (telephonyManager2 == null) {
                    telephonyManager2 = this.d;
                }
                telephonyManager2.listen(next, 0);
            }
        }
    }

    private static ThreeStateShort c(NetworkRegistrationInfo[] networkRegistrationInfoArr) {
        if (networkRegistrationInfoArr != null) {
            for (NetworkRegistrationInfo networkRegistrationInfo : networkRegistrationInfoArr) {
                if (networkRegistrationInfo.Domain.equals("PS") && networkRegistrationInfo.TransportType.equals("WWAN")) {
                    return networkRegistrationInfo.NrAvailable;
                }
            }
        }
        return ThreeStateShort.Unknown;
    }

    class a {

        /* renamed from: a  reason: collision with root package name */
        int f5991a;
        String b;
        String c;
        String d;
        String e;
        int f;
        long g;
        long h;
        String i;
        WifiDetailedStates j;
        private String k;

        private a() {
            this.f5991a = -1;
            this.b = "";
            this.k = "";
            this.c = "";
            this.d = "";
            this.e = "";
            this.f = -1;
            this.g = -1;
            this.h = -1;
            this.i = "";
            this.j = WifiDetailedStates.Unknown;
        }

        /* synthetic */ a(c cVar, byte b2) {
            this();
        }
    }

    /* access modifiers changed from: private */
    public void a(boolean z2) {
        SubscriptionManager subscriptionManager;
        if (z2 && this.k != null && this.f.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0 && Build.VERSION.SDK_INT >= 22 && (subscriptionManager = (SubscriptionManager) this.f.getSystemService("telephony_subscription_service")) != null) {
            subscriptionManager.addOnSubscriptionsChangedListener(this.k);
        }
        if (this.d != null) {
            int i2 = 257;
            if (this.f.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0) {
                i2 = 273;
                if (Build.VERSION.SDK_INT >= 17) {
                    i2 = 1297;
                }
            }
            if (this.h.size() == 0) {
                if (this.g == null) {
                    this.g = new j();
                }
                this.d.listen(this.g, i2);
                return;
            }
            Iterator<j> it2 = this.h.iterator();
            while (it2.hasNext()) {
                j next = it2.next();
                TelephonyManager telephonyManager = null;
                SparseArray<TelephonyManager> sparseArray = this.e;
                if (sparseArray != null && sparseArray.size() > 0) {
                    telephonyManager = this.e.get(next.a());
                }
                if (telephonyManager == null) {
                    telephonyManager = this.d;
                }
                telephonyManager.listen(next, i2);
            }
        }
    }

    class i {

        /* renamed from: a  reason: collision with root package name */
        int f5999a;
        int b;
        int c;
        int d;
        int e;
        int f;
        int g;
        int h;
        int i;
        int j;
        long k;
        int l;
        int m;
        int n;
        int o;
        int p;
        int q;

        private i() {
            this.f5999a = RadioInfo.INVALID.intValue();
            this.b = RadioInfo.INVALID.intValue();
            this.c = RadioInfo.INVALID.intValue();
            this.d = RadioInfo.INVALID.intValue();
            this.e = RadioInfo.INVALID.intValue();
            this.f = RadioInfo.INVALID.intValue();
            this.g = RadioInfo.INVALID.intValue();
            this.h = RadioInfo.INVALID.intValue();
            this.i = RadioInfo.INVALID.intValue();
            this.j = RadioInfo.INVALID.intValue();
            this.l = RadioInfo.INVALID.intValue();
            this.m = RadioInfo.INVALID.intValue();
            this.n = RadioInfo.INVALID.intValue();
            this.o = RadioInfo.INVALID.intValue();
            this.p = RadioInfo.INVALID.intValue();
            this.q = RadioInfo.INVALID.intValue();
        }

        /* synthetic */ i(c cVar, byte b2) {
            this();
        }
    }

    public static NetworkTypes b(int i2) {
        switch (i2) {
            case 1:
                return NetworkTypes.GPRS;
            case 2:
                return NetworkTypes.EDGE;
            case 3:
                return NetworkTypes.UMTS;
            case 4:
                return NetworkTypes.CDMA;
            case 5:
                return NetworkTypes.EVDO_0;
            case 6:
                return NetworkTypes.EVDO_A;
            case 7:
                return NetworkTypes.Cdma1xRTT;
            case 8:
                return NetworkTypes.HSDPA;
            case 9:
                return NetworkTypes.HSUPA;
            case 10:
                return NetworkTypes.HSPA;
            case 11:
                return NetworkTypes.IDEN;
            case 12:
                return NetworkTypes.EVDO_B;
            case 13:
                return NetworkTypes.LTE;
            case 14:
                return NetworkTypes.EHRPD;
            case 15:
                return NetworkTypes.HSPAP;
            case 16:
                return NetworkTypes.GSM;
            case 17:
                return NetworkTypes.TD_SCDMA;
            case 18:
                return NetworkTypes.WiFi;
            case 19:
                return NetworkTypes.LTE_CA;
            case 20:
                return NetworkTypes.NR;
            default:
                return NetworkTypes.Unknown;
        }
    }

    public final NetworkRegistrationInfo[] a(int i2) {
        NetworkRegistrationInfo[] d2 = this.j.d(i2);
        if (d2 == null) {
            return new NetworkRegistrationInfo[0];
        }
        h b2 = this.j.b(i2);
        for (NetworkRegistrationInfo networkRegistrationInfo : d2) {
            if (b2 != null && b2.b > 0) {
                long elapsedRealtime = SystemClock.elapsedRealtime() - b2.b;
                networkRegistrationInfo.Age = elapsedRealtime > 2147483647L ? Integer.MAX_VALUE : (int) elapsedRealtime;
            }
        }
        return d2;
    }

    public static NetworkGenerations a(NetworkTypes networkTypes) {
        switch (AnonymousClass4.f5990a[networkTypes.ordinal()]) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return NetworkGenerations.Gen2;
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
                return NetworkGenerations.Gen3;
            case 17:
            case 18:
                return NetworkGenerations.Gen4;
            case 19:
                return NetworkGenerations.Gen5;
            default:
                return NetworkGenerations.Unknown;
        }
    }

    public final void b(final com.startapp.networkTest.controller.a.a aVar) {
        if (aVar == null) {
            return;
        }
        if (Thread.currentThread() == Looper.getMainLooper().getThread()) {
            this.b.remove(aVar);
        } else {
            this.f5986a.post(new Runnable() {
                public final void run() {
                    c.this.b(aVar);
                }
            });
        }
    }

    private static PreferredNetworkTypes a(Context context, int i2) {
        PreferredNetworkTypes preferredNetworkTypes = PreferredNetworkTypes.Unknown;
        if (Build.VERSION.SDK_INT < 17) {
            return preferredNetworkTypes;
        }
        try {
            return h(Settings.Global.getInt(context.getContentResolver(), "preferred_network_mode".concat(String.valueOf(i2))));
        } catch (Exception unused) {
            return preferredNetworkTypes;
        }
    }

    private static String b(NetworkRegistrationInfo[] networkRegistrationInfoArr) {
        if (networkRegistrationInfoArr == null) {
            return "Unknown";
        }
        for (NetworkRegistrationInfo networkRegistrationInfo : networkRegistrationInfoArr) {
            if (networkRegistrationInfo.Domain.equals("PS") && networkRegistrationInfo.TransportType.equals("WWAN")) {
                return networkRegistrationInfo.NrState;
            }
        }
        return "Unknown";
    }

    private static SparseArray<PreferredNetworkTypes> a(Context context) {
        SparseArray<PreferredNetworkTypes> sparseArray = new SparseArray<>();
        if (Build.VERSION.SDK_INT >= 17) {
            try {
                String[] split = Settings.Global.getString(context.getContentResolver(), "preferred_network_mode").split(",");
                for (int i2 = 0; i2 < split.length; i2++) {
                    sparseArray.put(i2, h(Integer.valueOf(split[i2]).intValue()));
                }
            } catch (Exception unused) {
            }
        }
        return sparseArray;
    }

    public final void a(final com.startapp.networkTest.controller.a.a aVar) {
        if (aVar == null) {
            return;
        }
        if (Thread.currentThread() != Looper.getMainLooper().getThread()) {
            this.f5986a.post(new Runnable() {
                public final void run() {
                    c.this.a(aVar);
                }
            });
        } else if (!this.b.contains(aVar)) {
            this.b.add(aVar);
        }
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0051, code lost:
        if (r6.equals("CDMA - 1xRTT") != false) goto L_0x00fd;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.startapp.networkTest.enums.NetworkTypes a(com.startapp.networkTest.data.radio.NetworkRegistrationInfo[] r6) {
        /*
            if (r6 == 0) goto L_0x0143
            int r0 = r6.length
            r1 = 0
            r2 = 0
        L_0x0005:
            if (r2 >= r0) goto L_0x0143
            r3 = r6[r2]
            java.lang.String r4 = r3.Domain
            java.lang.String r5 = "PS"
            boolean r4 = r4.equals(r5)
            if (r4 == 0) goto L_0x013f
            java.lang.String r6 = r3.NetworkTechnology
            r0 = -1
            int r2 = r6.hashCode()
            switch(r2) {
                case -2039427040: goto L_0x00f1;
                case -908593671: goto L_0x00e6;
                case 2500: goto L_0x00db;
                case 70881: goto L_0x00d0;
                case 75709: goto L_0x00c5;
                case 2063797: goto L_0x00bb;
                case 2123197: goto L_0x00b1;
                case 2194666: goto L_0x00a7;
                case 2227260: goto L_0x009c;
                case 2608919: goto L_0x0091;
                case 3195620: goto L_0x0085;
                case 69034058: goto L_0x0079;
                case 69045140: goto L_0x006d;
                case 69050395: goto L_0x0061;
                case 70083979: goto L_0x0055;
                case 836263277: goto L_0x004b;
                case 882856261: goto L_0x0040;
                case 893165057: goto L_0x0035;
                case 893165074: goto L_0x002a;
                case 893165075: goto L_0x001f;
                default: goto L_0x001d;
            }
        L_0x001d:
            goto L_0x00fc
        L_0x001f:
            java.lang.String r1 = "CDMA - EvDo rev. B"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 6
            goto L_0x00fd
        L_0x002a:
            java.lang.String r1 = "CDMA - EvDo rev. A"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 5
            goto L_0x00fd
        L_0x0035:
            java.lang.String r1 = "CDMA - EvDo rev. 0"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 4
            goto L_0x00fd
        L_0x0040:
            java.lang.String r1 = "CDMA - eHRPD"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 3
            goto L_0x00fd
        L_0x004b:
            java.lang.String r2 = "CDMA - 1xRTT"
            boolean r6 = r6.equals(r2)
            if (r6 == 0) goto L_0x00fc
            goto L_0x00fd
        L_0x0055:
            java.lang.String r1 = "IWLAN"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 17
            goto L_0x00fd
        L_0x0061:
            java.lang.String r1 = "HSUPA"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 11
            goto L_0x00fd
        L_0x006d:
            java.lang.String r1 = "HSPAP"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 10
            goto L_0x00fd
        L_0x0079:
            java.lang.String r1 = "HSDPA"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 9
            goto L_0x00fd
        L_0x0085:
            java.lang.String r1 = "iDEN"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 12
            goto L_0x00fd
        L_0x0091:
            java.lang.String r1 = "UMTS"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 14
            goto L_0x00fd
        L_0x009c:
            java.lang.String r1 = "HSPA"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 8
            goto L_0x00fd
        L_0x00a7:
            java.lang.String r1 = "GPRS"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 7
            goto L_0x00fd
        L_0x00b1:
            java.lang.String r1 = "EDGE"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 2
            goto L_0x00fd
        L_0x00bb:
            java.lang.String r1 = "CDMA"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 1
            goto L_0x00fd
        L_0x00c5:
            java.lang.String r1 = "LTE"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 13
            goto L_0x00fd
        L_0x00d0:
            java.lang.String r1 = "GSM"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 15
            goto L_0x00fd
        L_0x00db:
            java.lang.String r1 = "NR"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 19
            goto L_0x00fd
        L_0x00e6:
            java.lang.String r1 = "TD_SCDMA"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 16
            goto L_0x00fd
        L_0x00f1:
            java.lang.String r1 = "LTE_CA"
            boolean r6 = r6.equals(r1)
            if (r6 == 0) goto L_0x00fc
            r1 = 18
            goto L_0x00fd
        L_0x00fc:
            r1 = -1
        L_0x00fd:
            switch(r1) {
                case 0: goto L_0x013c;
                case 1: goto L_0x0139;
                case 2: goto L_0x0136;
                case 3: goto L_0x0133;
                case 4: goto L_0x0130;
                case 5: goto L_0x012d;
                case 6: goto L_0x012a;
                case 7: goto L_0x0127;
                case 8: goto L_0x0124;
                case 9: goto L_0x0121;
                case 10: goto L_0x011e;
                case 11: goto L_0x011b;
                case 12: goto L_0x0118;
                case 13: goto L_0x0115;
                case 14: goto L_0x0112;
                case 15: goto L_0x010f;
                case 16: goto L_0x010c;
                case 17: goto L_0x0109;
                case 18: goto L_0x0106;
                case 19: goto L_0x0103;
                default: goto L_0x0100;
            }
        L_0x0100:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.Unknown
            return r6
        L_0x0103:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.NR
            return r6
        L_0x0106:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.LTE_CA
            return r6
        L_0x0109:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.WiFi
            return r6
        L_0x010c:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.TD_SCDMA
            return r6
        L_0x010f:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.GSM
            return r6
        L_0x0112:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.UMTS
            return r6
        L_0x0115:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.LTE
            return r6
        L_0x0118:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.IDEN
            return r6
        L_0x011b:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.HSUPA
            return r6
        L_0x011e:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.HSPAP
            return r6
        L_0x0121:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.HSDPA
            return r6
        L_0x0124:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.HSPA
            return r6
        L_0x0127:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.GPRS
            return r6
        L_0x012a:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.EVDO_B
            return r6
        L_0x012d:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.EVDO_A
            return r6
        L_0x0130:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.EVDO_0
            return r6
        L_0x0133:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.EHRPD
            return r6
        L_0x0136:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.EDGE
            return r6
        L_0x0139:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.CDMA
            return r6
        L_0x013c:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.Cdma1xRTT
            return r6
        L_0x013f:
            int r2 = r2 + 1
            goto L_0x0005
        L_0x0143:
            com.startapp.networkTest.enums.NetworkTypes r6 = com.startapp.networkTest.enums.NetworkTypes.Unknown
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.c.a(com.startapp.networkTest.data.radio.NetworkRegistrationInfo[]):com.startapp.networkTest.enums.NetworkTypes");
    }

    public final com.startapp.networkTest.data.a.a g() {
        return this.l;
    }

    public final NetworkTypes d(int i2) {
        if (f(i2) && com.startapp.networkTest.utils.e.a(this.f)) {
            SparseArray<TelephonyManager> sparseArray = this.e;
            if (sparseArray != null && sparseArray.get(i2) != null && Build.VERSION.SDK_INT >= 24) {
                return b(this.e.get(i2).getVoiceNetworkType());
            }
            Method method = this.x;
            if (method != null) {
                try {
                    return b(((Integer) method.invoke(this.d, new Object[]{Integer.valueOf(i2)})).intValue());
                } catch (Exception e2) {
                    String str = c;
                    Log.e(str, "getVoiceNetworkType: " + e2.toString());
                }
            }
        }
        return n();
    }

    private static ThreeStateShort d(NetworkRegistrationInfo[] networkRegistrationInfoArr) {
        if (networkRegistrationInfoArr != null) {
            for (NetworkRegistrationInfo networkRegistrationInfo : networkRegistrationInfoArr) {
                if (networkRegistrationInfo.Domain.equals("PS")) {
                    return networkRegistrationInfo.CarrierAggregation;
                }
            }
        }
        return ThreeStateShort.Unknown;
    }
}
