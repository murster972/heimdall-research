package com.startapp.networkTest.controller;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.enums.LocationProviders;
import java.util.List;

public class LocationController {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5979a = "LocationController";
    private LocationManager b;
    /* access modifiers changed from: private */
    public long c;
    /* access modifiers changed from: private */
    public LocationInfo d;
    /* access modifiers changed from: private */
    public Location e;
    /* access modifiers changed from: private */
    public long f;
    private a g;
    private long h = 4000;
    private boolean i;
    /* access modifiers changed from: private */
    public b j;

    /* renamed from: com.startapp.networkTest.controller.LocationController$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5980a = new int[ProviderMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.startapp.networkTest.controller.LocationController$ProviderMode[] r0 = com.startapp.networkTest.controller.LocationController.ProviderMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5980a = r0
                int[] r0 = f5980a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.controller.LocationController$ProviderMode r1 = com.startapp.networkTest.controller.LocationController.ProviderMode.Gps     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5980a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.controller.LocationController$ProviderMode r1 = com.startapp.networkTest.controller.LocationController.ProviderMode.GpsAndNetwork     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5980a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.controller.LocationController$ProviderMode r1 = com.startapp.networkTest.controller.LocationController.ProviderMode.Network     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f5980a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.networkTest.controller.LocationController$ProviderMode r1 = com.startapp.networkTest.controller.LocationController.ProviderMode.Passive     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.controller.LocationController.AnonymousClass1.<clinit>():void");
        }
    }

    public enum ProviderMode {
        Passive,
        Network,
        Gps,
        GpsAndNetwork,
        RailNet
    }

    class a implements LocationListener {
        private a() {
        }

        public final void onLocationChanged(Location location) {
            if (location != null && location.getProvider() != null) {
                if (LocationController.this.e == null || location.getProvider().equals("gps") || LocationController.this.e.getProvider() == null || !LocationController.this.e.getProvider().equals("gps") || SystemClock.elapsedRealtime() - LocationController.this.c >= 5000) {
                    Location unused = LocationController.this.e = location;
                    long unused2 = LocationController.this.f = SystemClock.elapsedRealtime();
                    LocationInfo unused3 = LocationController.this.d = LocationController.b(location);
                    LocationController.this.d.LocationAge = 0;
                    long unused4 = LocationController.this.c = SystemClock.elapsedRealtime();
                    if (LocationController.this.j != null) {
                        LocationController.this.j.a(LocationController.this.d);
                    }
                    if (location.getProvider().equals("gps")) {
                        c.b().a(location);
                    }
                }
            }
        }

        public final void onProviderDisabled(String str) {
        }

        public final void onProviderEnabled(String str) {
        }

        public final void onStatusChanged(String str, int i, Bundle bundle) {
        }

        /* synthetic */ a(LocationController locationController, byte b) {
            this();
        }
    }

    public interface b {
        void a(LocationInfo locationInfo);
    }

    public LocationController(Context context) {
        this.b = (LocationManager) context.getSystemService("location");
        this.g = new a(this, (byte) 0);
    }

    public final LocationInfo b() {
        Location location;
        if (this.d == null) {
            List<String> allProviders = this.b.getAllProviders();
            Location location2 = null;
            if (allProviders != null && allProviders.size() > 0) {
                Location location3 = null;
                for (int i2 = 0; i2 < allProviders.size(); i2++) {
                    try {
                        location = this.b.getLastKnownLocation(allProviders.get(i2));
                    } catch (SecurityException e2) {
                        new StringBuilder("getNewestCachedLocationFromDevice: ").append(e2.toString());
                        location = null;
                    }
                    if (location != null && (location3 == null || location.getTime() > location3.getTime())) {
                        location3 = location;
                    }
                }
                location2 = location3;
            }
            if (location2 != null) {
                this.e = location2;
                if (Build.VERSION.SDK_INT >= 17) {
                    this.f = location2.getElapsedRealtimeNanos() / 1000000;
                } else {
                    this.f = SystemClock.elapsedRealtime() + (System.currentTimeMillis() - location2.getTime());
                }
                this.d = b(location2);
            }
        }
        if (this.d == null) {
            this.d = new LocationInfo();
            this.d.LocationProvider = LocationProviders.Unknown;
        }
        LocationInfo locationInfo = this.d;
        if (locationInfo.LocationProvider != LocationProviders.Unknown) {
            locationInfo.LocationAge = SystemClock.elapsedRealtime() - this.f;
        }
        try {
            return (LocationInfo) this.d.clone();
        } catch (CloneNotSupportedException e3) {
            Log.e(f5979a, "getLastLocationInfo", e3);
            return this.d;
        }
    }

    public final void a(ProviderMode providerMode) {
        LocationManager locationManager;
        boolean z;
        boolean z2;
        if (providerMode != null && (locationManager = this.b) != null) {
            this.i = true;
            List<String> allProviders = locationManager.getAllProviders();
            boolean z3 = false;
            if (allProviders != null) {
                boolean z4 = false;
                z2 = false;
                z = false;
                for (String next : allProviders) {
                    char c2 = 65535;
                    int hashCode = next.hashCode();
                    if (hashCode != -792039641) {
                        if (hashCode != 102570) {
                            if (hashCode == 1843485230 && next.equals("network")) {
                                c2 = 1;
                            }
                        } else if (next.equals("gps")) {
                            c2 = 0;
                        }
                    } else if (next.equals("passive")) {
                        c2 = 2;
                    }
                    if (c2 == 0) {
                        z4 = true;
                    } else if (c2 == 1) {
                        z2 = true;
                    } else if (c2 == 2) {
                        z = true;
                    }
                }
                z3 = z4;
            } else {
                z2 = false;
                z = false;
            }
            try {
                int i2 = AnonymousClass1.f5980a[providerMode.ordinal()];
                if (i2 != 1) {
                    if (i2 == 2) {
                        if (z3) {
                            this.b.requestLocationUpdates("gps", 500, 5.0f, this.g);
                        }
                        if (z2) {
                            this.b.requestLocationUpdates("network", 0, 0.0f, this.g);
                        }
                    } else if (i2 != 3) {
                        if (i2 == 4) {
                            if (z) {
                                this.b.requestLocationUpdates("passive", 0, 0.0f, this.g);
                            }
                        }
                    } else if (z2) {
                        this.b.requestLocationUpdates("network", 0, 0.0f, this.g);
                    }
                } else if (z3) {
                    this.b.requestLocationUpdates("gps", 500, 5.0f, this.g);
                }
            } catch (Exception e2) {
                new StringBuilder("startListening: ").append(e2.toString());
            }
        }
    }

    public final void a() {
        a aVar;
        LocationManager locationManager = this.b;
        if (!(locationManager == null || (aVar = this.g) == null)) {
            try {
                locationManager.removeUpdates(aVar);
            } catch (Exception e2) {
                new StringBuilder("stopListening: ").append(e2.toString());
            }
        }
        this.i = false;
    }

    public final void a(b bVar) {
        this.j = bVar;
    }

    /* access modifiers changed from: private */
    public static LocationInfo b(Location location) {
        LocationInfo locationInfo = new LocationInfo();
        locationInfo.LocationAccuracyHorizontal = (double) location.getAccuracy();
        if (Build.VERSION.SDK_INT < 26 || !location.hasVerticalAccuracy()) {
            locationInfo.LocationAccuracyVertical = (double) location.getAccuracy();
        } else {
            locationInfo.LocationAccuracyVertical = (double) location.getVerticalAccuracyMeters();
        }
        locationInfo.locationTimestampMillis = com.startapp.networkTest.e.b.b();
        locationInfo.LocationTimestamp = com.iab.omid.library.startapp.b.a(locationInfo.locationTimestampMillis);
        locationInfo.LocationAltitude = location.getAltitude();
        locationInfo.LocationBearing = (double) location.getBearing();
        locationInfo.LocationLatitude = location.getLatitude();
        locationInfo.LocationLongitude = location.getLongitude();
        if (Build.VERSION.SDK_INT >= 18) {
            locationInfo.IsMocked = location.isFromMockProvider() ? 1 : 0;
        }
        if (location.getProvider() != null) {
            if (location.getProvider().equals("gps")) {
                locationInfo.LocationProvider = LocationProviders.Gps;
            } else if (location.getProvider().equals("network")) {
                locationInfo.LocationProvider = LocationProviders.Network;
            } else if (location.getProvider().equals("fused")) {
                locationInfo.LocationProvider = LocationProviders.Fused;
            }
            locationInfo.LocationSpeed = (double) location.getSpeed();
            return locationInfo;
        }
        locationInfo.LocationProvider = LocationProviders.Unknown;
        locationInfo.LocationSpeed = (double) location.getSpeed();
        return locationInfo;
    }
}
