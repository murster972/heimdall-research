package com.startapp.networkTest.controller;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.os.Build;
import android.util.Log;
import com.applovin.sdk.AppLovinEventTypes;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.enums.BatteryChargePlugTypes;
import com.startapp.networkTest.enums.BatteryHealthStates;
import com.startapp.networkTest.enums.BatteryStatusStates;
import com.startapp.networkTest.utils.h;
import com.vungle.warren.model.ReportDBAdapter;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5983a = "a";
    private BatteryManager b;
    private Context c;

    public a(Context context) {
        if (Build.VERSION.SDK_INT >= 21) {
            this.b = (BatteryManager) context.getSystemService("batterymanager");
        }
        this.c = context;
    }

    public final BatteryInfo a() {
        BatteryChargePlugTypes batteryChargePlugTypes;
        BatteryHealthStates batteryHealthStates;
        Intent intent = null;
        try {
            intent = this.c.registerReceiver((BroadcastReceiver) null, new IntentFilter("android.intent.action.BATTERY_CHANGED"));
        } catch (Exception e) {
            Log.e(f5983a, e.getMessage(), e);
        }
        BatteryInfo batteryInfo = new BatteryInfo();
        if (intent == null) {
            batteryInfo.MissingPermission = true;
            return batteryInfo;
        }
        int intExtra = intent.getIntExtra(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, 1);
        BatteryStatusStates batteryStatusStates = BatteryStatusStates.Unknown;
        if (intExtra != 1) {
            if (intExtra == 2) {
                batteryStatusStates = BatteryStatusStates.Charging;
            } else if (intExtra == 3) {
                batteryStatusStates = BatteryStatusStates.Discharging;
            } else if (intExtra == 4) {
                batteryStatusStates = BatteryStatusStates.NotCharging;
            } else if (intExtra == 5) {
                batteryStatusStates = BatteryStatusStates.Full;
            }
        }
        batteryInfo.BatteryStatus = batteryStatusStates;
        int intExtra2 = intent.getIntExtra("plugged", -1);
        if (intExtra2 == 1) {
            batteryChargePlugTypes = BatteryChargePlugTypes.AC;
        } else if (intExtra2 == 2) {
            batteryChargePlugTypes = BatteryChargePlugTypes.USB;
        } else if (intExtra2 != 4) {
            batteryChargePlugTypes = BatteryChargePlugTypes.Unknown;
        } else {
            batteryChargePlugTypes = BatteryChargePlugTypes.Wireless;
        }
        batteryInfo.BatteryChargePlug = batteryChargePlugTypes;
        batteryInfo.BatteryLevel = (((float) intent.getIntExtra(AppLovinEventTypes.USER_COMPLETED_LEVEL, -1)) / ((float) intent.getIntExtra("scale", -1))) * 100.0f;
        int intExtra3 = intent.getIntExtra("health", -1);
        if (intExtra3 == 2) {
            batteryHealthStates = BatteryHealthStates.Good;
        } else if (intExtra3 == 3) {
            batteryHealthStates = BatteryHealthStates.Overheat;
        } else if (intExtra3 == 4) {
            batteryHealthStates = BatteryHealthStates.Dead;
        } else if (intExtra3 == 5) {
            batteryHealthStates = BatteryHealthStates.OverVoltage;
        } else if (intExtra3 != 7) {
            batteryHealthStates = BatteryHealthStates.Unknown;
        } else {
            batteryHealthStates = BatteryHealthStates.Cold;
        }
        batteryInfo.BatteryHealth = batteryHealthStates;
        int intExtra4 = intent.getIntExtra("temperature", -1);
        if (intExtra4 >= 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(((float) intExtra4) / 10.0f);
            batteryInfo.BatteryTemp = sb.toString();
        }
        int intExtra5 = intent.getIntExtra("voltage", -1);
        if (intExtra5 >= 0) {
            batteryInfo.BatteryVoltage = intExtra5;
        }
        batteryInfo.BatteryTechnology = h.a(intent.getStringExtra("technology"));
        if (Build.VERSION.SDK_INT >= 21) {
            try {
                if (this.b != null) {
                    int intProperty = this.b.getIntProperty(1);
                    if (intProperty != Integer.MIN_VALUE) {
                        batteryInfo.BatteryCapacity = intProperty;
                    }
                    int intProperty2 = this.b.getIntProperty(2);
                    if (intProperty2 != Integer.MIN_VALUE) {
                        batteryInfo.BatteryCurrent = intProperty2;
                    }
                    long longProperty = this.b.getLongProperty(5);
                    if (longProperty != Long.MIN_VALUE) {
                        batteryInfo.BatteryRemainingEnergy = longProperty;
                    }
                }
            } catch (Exception e2) {
                Log.e(f5983a, e2.toString());
            }
        }
        return batteryInfo;
    }
}
