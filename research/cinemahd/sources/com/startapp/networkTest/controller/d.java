package com.startapp.networkTest.controller;

import android.content.Context;
import android.net.ConnectivityManager;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.enums.WifiStates;
import com.startapp.networkTest.utils.i;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private WifiStates f6003a;
    private ConnectivityManager b;
    private boolean c = false;
    private Context d;
    private String e = "";

    public d(Context context) {
        this.d = context.getApplicationContext();
        this.b = (ConnectivityManager) context.getSystemService("connectivity");
        this.f6003a = WifiStates.Unknown;
    }

    public final WifiInfo a() {
        WifiInfo wifiInfo = new WifiInfo();
        wifiInfo.MissingPermission = true;
        try {
            if (this.d.checkCallingOrSelfPermission("android.permission.ACCESS_WIFI_STATE") == -1) {
                return wifiInfo;
            }
            return wifiInfo;
        } catch (Exception e2) {
            new StringBuilder("getWifiInfo: ").append(e2.getMessage());
        }
    }

    public final long b() {
        if (this.e.length() == 0) {
            a();
        }
        if (this.e.length() == 0) {
            return -1;
        }
        return i.b(this.e);
    }

    public final long c() {
        String str = this.e;
        if (str == null || str.length() == 0) {
            a();
        }
        String str2 = this.e;
        if (str2 == null || str2.length() == 0) {
            return -1;
        }
        return i.a(this.e);
    }
}
