package com.startapp.networkTest.threads;

import android.os.Build;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final a f6085a = new a();
    private final ScheduledThreadPoolExecutor b = new ScheduledThreadPoolExecutor(1);
    private final ExecutorService c;
    private final ExecutorService d;

    private a() {
        Class<a> cls = a.class;
        this.c = a(1, cls.getSimpleName() + "-Single");
        this.d = a(4, cls.getSimpleName() + "-Cached");
        this.b.setKeepAliveTime(60, TimeUnit.SECONDS);
        if (Build.VERSION.SDK_INT >= 9) {
            this.b.allowCoreThreadTimeOut(true);
        }
    }

    public static a a() {
        return f6085a;
    }

    public final ExecutorService b() {
        return this.d;
    }

    public final ScheduledExecutorService c() {
        return this.b;
    }

    public final ExecutorService d() {
        return this.c;
    }

    private static ExecutorService a(int i, final String str) {
        if (Build.VERSION.SDK_INT >= 21) {
            ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(0, i, 30, TimeUnit.SECONDS, new ThreadManager$1(), new ThreadFactory() {

                /* renamed from: a  reason: collision with root package name */
                private AtomicInteger f6086a = new AtomicInteger();

                public final Thread newThread(Runnable runnable) {
                    return new Thread(runnable, str + "-" + this.f6086a.incrementAndGet());
                }
            }, new RejectedExecutionHandler() {
                public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
                    try {
                        threadPoolExecutor.getQueue().put(runnable);
                    } catch (InterruptedException unused) {
                        Thread.currentThread().interrupt();
                    }
                }
            });
            threadPoolExecutor.allowCoreThreadTimeOut(true);
            return threadPoolExecutor;
        } else if (i < 2) {
            return Executors.newSingleThreadExecutor();
        } else {
            return Executors.newCachedThreadPool();
        }
    }
}
