package com.startapp.networkTest.utils;

import android.net.TrafficStats;
import android.util.Log;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6093a = "i";
    private static String[] b;
    private static Method c;
    private static Method d;
    private static Method e;

    static {
        try {
            Method declaredMethod = TrafficStats.class.getDeclaredMethod("getRxBytes", new Class[]{String.class});
            c = declaredMethod;
            declaredMethod.setAccessible(true);
        } catch (NoSuchMethodException unused) {
        }
        try {
            Method declaredMethod2 = TrafficStats.class.getDeclaredMethod("getTxBytes", new Class[]{String.class});
            d = declaredMethod2;
            declaredMethod2.setAccessible(true);
        } catch (NoSuchMethodException unused2) {
        }
        try {
            Method declaredMethod3 = TrafficStats.class.getDeclaredMethod("getMobileIfaces", new Class[0]);
            e = declaredMethod3;
            declaredMethod3.setAccessible(true);
        } catch (NoSuchMethodException unused3) {
        }
    }

    public static synchronized long a() {
        long j;
        synchronized (i.class) {
            try {
                j = TrafficStats.getMobileTxBytes();
            } catch (Exception e2) {
                Log.e(f6093a, "getMobileTxBytes: " + e2.getMessage());
                j = 0;
            }
            if (j > 0) {
                if (b == null) {
                    c();
                }
            } else if (b != null) {
                for (String str : b) {
                    long c2 = c("/sys/class/net/" + str + "/statistics/tx_bytes");
                    if (c2 > -1) {
                        j += c2;
                    }
                }
            }
        }
        return j;
    }

    public static synchronized long b() {
        long j;
        synchronized (i.class) {
            try {
                j = TrafficStats.getMobileRxBytes();
            } catch (Exception e2) {
                Log.e(f6093a, "getMobileRxBytes: " + e2.getMessage());
                j = 0;
            }
            if (j > 0) {
                if (b == null) {
                    c();
                }
            } else if (b != null) {
                for (String str : b) {
                    long c2 = c("/sys/class/net/" + str + "/statistics/rx_bytes");
                    if (c2 > -1) {
                        j += c2;
                    }
                }
            }
        }
        return j;
    }

    private static long c(String str) {
        String[] a2 = g.a(str);
        if (a2.length > 0) {
            return Long.parseLong(a2[0]);
        }
        return -1;
    }

    private static void c() {
        Method method = e;
        if (method != null) {
            try {
                String[] strArr = (String[]) method.invoke((Object) null, new Object[0]);
                if (strArr != null) {
                    b = strArr;
                }
            } catch (Exception e2) {
                String str = f6093a;
                Log.e(str, "getMobileInterfaces: " + e2.getMessage());
                e2.printStackTrace();
            }
        }
    }

    public static long a(String str) {
        Method method = d;
        if (method != null) {
            try {
                return ((Long) method.invoke((Object) null, new Object[]{str})).longValue();
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                e2.printStackTrace();
            }
        }
        return c("/sys/class/net/" + str + "/statistics/tx_bytes");
    }

    public static long b(String str) {
        Method method = c;
        if (method != null) {
            try {
                return ((Long) method.invoke((Object) null, new Object[]{str})).longValue();
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e2) {
                e2.printStackTrace();
            }
        }
        return c("/sys/class/net/" + str + "/statistics/rx_bytes");
    }
}
