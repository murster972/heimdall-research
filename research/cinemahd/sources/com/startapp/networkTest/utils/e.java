package com.startapp.networkTest.utils;

import android.content.Context;
import com.iab.omid.library.startapp.b;
import java.net.URL;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final String f6092a;
    private final URL b;
    private final String c;

    private e(String str, URL url, String str2) {
        this.f6092a = str;
        this.b = url;
        this.c = str2;
    }

    public static boolean a(Context context) {
        return context.checkCallingOrSelfPermission("android.permission.READ_PHONE_STATE") == 0;
    }

    public final URL b() {
        return this.b;
    }

    public final String c() {
        return this.c;
    }

    public static e a(String str, URL url, String str2) {
        b.b(str, "VendorKey is null or empty");
        b.a((Object) url, "ResourceURL is null");
        b.b(str2, "VerificationParameters is null or empty");
        return new e(str, url, str2);
    }

    public final String a() {
        return this.f6092a;
    }
}
