package com.startapp.networkTest.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class g {
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0048 A[SYNTHETIC, Splitter:B:19:0x0048] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x005c A[SYNTHETIC, Splitter:B:26:0x005c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String[] a(java.lang.String r6) {
        /*
            java.lang.String r0 = "cat: "
            java.io.ByteArrayOutputStream r1 = new java.io.ByteArrayOutputStream
            r1.<init>()
            r2 = 10240(0x2800, float:1.4349E-41)
            byte[] r2 = new byte[r2]
            r3 = 0
            r4 = 0
            java.io.FileInputStream r5 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005a, all -> 0x0044 }
            r5.<init>(r6)     // Catch:{ Exception -> 0x005a, all -> 0x0044 }
        L_0x0012:
            int r6 = r5.read(r2)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            r4 = -1
            if (r6 == r4) goto L_0x001d
            r1.write(r2, r3, r6)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            goto L_0x0012
        L_0x001d:
            java.lang.String r6 = new java.lang.String     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            byte[] r1 = r1.toByteArray()     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            java.lang.String r2 = "UTF-8"
            r6.<init>(r1, r2)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            java.lang.String r1 = "\n"
            java.lang.String[] r6 = r6.split(r1)     // Catch:{ Exception -> 0x0042, all -> 0x0040 }
            r5.close()     // Catch:{ IOException -> 0x0032 }
            goto L_0x003f
        L_0x0032:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            java.lang.String r0 = r1.toString()
            r2.append(r0)
        L_0x003f:
            return r6
        L_0x0040:
            r6 = move-exception
            goto L_0x0046
        L_0x0042:
            r4 = r5
            goto L_0x005a
        L_0x0044:
            r6 = move-exception
            r5 = r4
        L_0x0046:
            if (r5 == 0) goto L_0x0059
            r5.close()     // Catch:{ IOException -> 0x004c }
            goto L_0x0059
        L_0x004c:
            r1 = move-exception
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r0)
            java.lang.String r0 = r1.toString()
            r2.append(r0)
        L_0x0059:
            throw r6
        L_0x005a:
            if (r4 == 0) goto L_0x006d
            r4.close()     // Catch:{ IOException -> 0x0060 }
            goto L_0x006d
        L_0x0060:
            r6 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            java.lang.String r6 = r6.toString()
            r1.append(r6)
        L_0x006d:
            java.lang.String[] r6 = new java.lang.String[r3]
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.utils.g.a(java.lang.String):java.lang.String[]");
    }

    public static String[] b(String str) {
        StringBuffer stringBuffer = new StringBuffer();
        try {
            Process exec = Runtime.getRuntime().exec(str);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(exec.getInputStream()));
            boolean z = true;
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                if (!z) {
                    stringBuffer.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                }
                stringBuffer.append(readLine);
                z = false;
            }
            bufferedReader.close();
            exec.waitFor();
        } catch (Exception e) {
            new StringBuilder("shellResult: ").append(e.toString());
        }
        return stringBuffer.toString().split("\\n");
    }
}
