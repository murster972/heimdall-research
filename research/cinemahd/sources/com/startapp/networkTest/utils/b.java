package com.startapp.networkTest.utils;

import com.google.android.gms.ads.AdRequest;
import com.startapp.networkTest.c;
import com.startapp.networkTest.d;
import com.startapp.networkTest.d.a.a;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.PublicKey;
import java.security.Signature;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class b {
    /* JADX WARNING: Removed duplicated region for block: B:31:0x00ba  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00c1  */
    /* JADX WARNING: Removed duplicated region for block: B:38:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a() {
        /*
            r0 = 0
            com.startapp.networkTest.a r1 = com.startapp.networkTest.c.d()     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.lang.String r1 = r1.m()     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.lang.String r2 = "[PROJECTID]"
            com.startapp.networkTest.a r3 = com.startapp.networkTest.c.d()     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.lang.String r3 = r3.a()     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.lang.String r1 = r1.replace(r2, r3)     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.net.URL r2 = new java.net.URL     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            r2.<init>(r1)     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.net.URLConnection r1 = r2.openConnection()     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.net.HttpURLConnection r1 = (java.net.HttpURLConnection) r1     // Catch:{ IOException -> 0x00be, all -> 0x00b4 }
            java.lang.String r0 = "GET"
            r1.setRequestMethod(r0)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r0 = 10000(0x2710, float:1.4013E-41)
            r1.setConnectTimeout(r0)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r1.setReadTimeout(r0)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            com.startapp.networkTest.d r0 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            long r2 = r0.h()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.text.SimpleDateFormat r0 = new java.text.SimpleDateFormat     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.String r4 = "EEE, dd MMM yyyy HH:mm:ss z"
            java.util.Locale r5 = java.util.Locale.US     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r0.<init>(r4, r5)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.String r4 = "GMT"
            java.util.TimeZone r4 = java.util.TimeZone.getTimeZone(r4)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r0.setTimeZone(r4)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.String r0 = r0.format(r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.String r2 = "If-Modified-Since"
            r1.setRequestProperty(r2, r0)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.String r0 = "Connection"
            java.lang.String r2 = "close"
            r1.setRequestProperty(r0, r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            int r0 = r1.getResponseCode()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r2 = 304(0x130, float:4.26E-43)
            if (r0 != r2) goto L_0x0071
            com.startapp.networkTest.d r0 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            long r2 = com.startapp.networkTest.e.b.b()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r0.e(r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            goto L_0x00aa
        L_0x0071:
            int r0 = r1.getResponseCode()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r2 = 200(0xc8, float:2.8E-43)
            if (r0 != r2) goto L_0x00aa
            long r2 = r1.getLastModified()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.io.InputStream r0 = r1.getInputStream()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            boolean r4 = a(r0)     // Catch:{ all -> 0x00a5 }
            r0.close()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            if (r4 == 0) goto L_0x009d
            com.startapp.networkTest.d r0 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            long r4 = com.startapp.networkTest.e.b.b()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r0.e(r4)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            com.startapp.networkTest.d r0 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            r0.d((long) r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            goto L_0x00aa
        L_0x009d:
            java.io.IOException r0 = new java.io.IOException     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            java.lang.String r2 = "Verification of downloaded cdn config failed"
            r0.<init>(r2)     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            throw r0     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
        L_0x00a5:
            r2 = move-exception
            r0.close()     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
            throw r2     // Catch:{ IOException -> 0x00b2, all -> 0x00b0 }
        L_0x00aa:
            if (r1 == 0) goto L_0x00c4
            r1.disconnect()
            return
        L_0x00b0:
            r0 = move-exception
            goto L_0x00b8
        L_0x00b2:
            r0 = r1
            goto L_0x00bf
        L_0x00b4:
            r1 = move-exception
            r6 = r1
            r1 = r0
            r0 = r6
        L_0x00b8:
            if (r1 == 0) goto L_0x00bd
            r1.disconnect()
        L_0x00bd:
            throw r0
        L_0x00be:
        L_0x00bf:
            if (r0 == 0) goto L_0x00c4
            r0.disconnect()
        L_0x00c4:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.utils.b.a():void");
    }

    private static boolean a(InputStream inputStream) throws IOException {
        a aVar;
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream byteArrayOutputStream2 = new ByteArrayOutputStream();
        byte[] bArr = new byte[AdRequest.MAX_CONTENT_URL_LENGTH];
        while (true) {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    break;
                } else if (!nextEntry.isDirectory()) {
                    if (nextEntry.getName().equalsIgnoreCase("cdnconfig.txt")) {
                        for (int read = zipInputStream.read(bArr); read != -1; read = zipInputStream.read(bArr)) {
                            byteArrayOutputStream.write(bArr, 0, read);
                        }
                        byteArrayOutputStream.flush();
                        zipInputStream.closeEntry();
                    } else if (nextEntry.getName().equalsIgnoreCase("cdnconfig.txt.sig")) {
                        for (int read2 = zipInputStream.read(bArr); read2 != -1; read2 = zipInputStream.read(bArr)) {
                            byteArrayOutputStream2.write(bArr, 0, read2);
                        }
                        byteArrayOutputStream2.flush();
                        zipInputStream.closeEntry();
                    }
                }
            } finally {
                zipInputStream.close();
                try {
                    byteArrayOutputStream2.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    byteArrayOutputStream.close();
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
            }
        }
        boolean z = true;
        if (c.d().n()) {
            z = a(byteArrayOutputStream, byteArrayOutputStream2);
        }
        if (z && (aVar = (a) com.startapp.common.parser.b.a(new String(byteArrayOutputStream.toByteArray(), "UTF-8"), a.class)) != null) {
            d c = c.c();
            c.a((Set<String>) null);
            c.c((Set<String>) new HashSet(aVar.ct.cdn));
            c.a(aVar.ct.criteria);
            c.d((Set<String>) new HashSet(aVar.ltr.cdn));
            c.b(aVar.ltr.criteria);
        }
        return z;
    }

    private static boolean a(ByteArrayOutputStream byteArrayOutputStream, ByteArrayOutputStream byteArrayOutputStream2) {
        try {
            byte[] byteArray = byteArrayOutputStream2.toByteArray();
            byte[] byteArray2 = byteArrayOutputStream.toByteArray();
            Signature instance = Signature.getInstance("SHA256withRSA");
            instance.initVerify((PublicKey) null);
            instance.update(byteArray2);
            return instance.verify(byteArray);
        } catch (Exception unused) {
            return false;
        }
    }
}
