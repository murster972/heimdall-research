package com.startapp.networkTest.utils;

import com.iab.omid.library.startapp.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f6087a;
    private final String b;

    private a(String str, String str2) {
        this.f6087a = str;
        this.b = str2;
    }

    public static a a(String str, String str2) {
        b.b(str, "Name is null or empty");
        b.b(str2, "Version is null or empty");
        return new a(str, str2);
    }

    public final String b() {
        return this.b;
    }

    public final String a() {
        return this.f6087a;
    }
}
