package com.startapp.networkTest.utils;

import com.startapp.sdk.adsbase.cache.DiskAdCacheManager;
import java.util.List;

public final class d {
    private static final List<d> h = new LteFrequencyUtil$1();

    /* renamed from: a  reason: collision with root package name */
    private float f6091a;
    private float b;
    private float c;
    private float d;
    private float e;
    private float f;
    private DiskAdCacheManager g;

    /* synthetic */ d(int i, float f2, float f3, float f4, float f5, float f6, float f7, byte b2) {
        this(i, f2, f3, f4, f5, f6, f7);
    }

    public static DiskAdCacheManager a(int i) {
        DiskAdCacheManager diskAdCacheManager = null;
        for (d next : h) {
            float f2 = (float) i;
            boolean z = true;
            if (f2 >= next.f && f2 <= next.e) {
                DiskAdCacheManager diskAdCacheManager2 = next.g;
                diskAdCacheManager2.d = next.d + ((f2 - next.f) * 0.1f);
                diskAdCacheManager2.d = a(diskAdCacheManager2.d);
                next.g.b = i;
                if (!(next.f6091a == 0.0f && next.c == 0.0f)) {
                    DiskAdCacheManager diskAdCacheManager3 = next.g;
                    diskAdCacheManager3.e = next.f6091a + (diskAdCacheManager3.d - next.d);
                    diskAdCacheManager3.e = a(diskAdCacheManager3.e);
                    next.g.c = (int) (next.c + (f2 - next.f));
                }
                diskAdCacheManager = next.g;
            } else if (i > 0) {
                float f3 = (float) ((long) i);
                if (f3 < next.c || f3 > next.b) {
                    z = false;
                }
                if (z) {
                    DiskAdCacheManager diskAdCacheManager4 = next.g;
                    diskAdCacheManager4.e = next.f6091a + ((f2 - next.c) * 0.1f);
                    diskAdCacheManager4.e = a(diskAdCacheManager4.e);
                    DiskAdCacheManager diskAdCacheManager5 = next.g;
                    diskAdCacheManager5.c = i;
                    diskAdCacheManager5.d = next.d + (diskAdCacheManager5.e - next.f6091a);
                    diskAdCacheManager5.d = a(diskAdCacheManager5.d);
                    diskAdCacheManager = next.g;
                    diskAdCacheManager.b = (int) (next.f + (f2 - next.c));
                }
            }
        }
        return diskAdCacheManager;
    }

    private d(int i, float f2, float f3, float f4, float f5, float f6, float f7) {
        this.g = null;
        this.g = new DiskAdCacheManager();
        this.g.f6309a = i;
        this.f6091a = f5;
        this.b = f7;
        this.d = f2;
        this.e = f4;
        this.c = f6;
        this.f = f3;
    }

    private static float a(float f2) {
        float pow = (float) ((long) Math.pow(10.0d, 1.0d));
        return ((float) ((long) Math.round(f2 * pow))) / pow;
    }
}
