package com.startapp.networkTest.utils;

import android.content.Context;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class j {
    /* JADX WARNING: Can't wrap try/catch for region: R(8:39|38|42|43|(0)|46|(0)|(0)(0)) */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0112, code lost:
        r9 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0113, code lost:
        r3 = r2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0124, code lost:
        r3.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0136, code lost:
        r1.delete();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x013b, code lost:
        r2.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0142, code lost:
        r3.disconnect();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:?, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x0115 */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0124 A[Catch:{ all -> 0x0112 }] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0136 A[Catch:{ all -> 0x0112 }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x013b  */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x0142  */
    /* JADX WARNING: Removed duplicated region for block: B:57:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r9) {
        /*
            java.lang.String r0 = "truststore.bin.sig"
            java.lang.String r1 = "truststore.bin"
            r2 = 0
            com.startapp.networkTest.a r3 = com.startapp.networkTest.c.d()     // Catch:{ IOException -> 0x0115 }
            java.lang.String r3 = r3.i()     // Catch:{ IOException -> 0x0115 }
            java.lang.String r4 = "[PROJECTID]"
            com.startapp.networkTest.a r5 = com.startapp.networkTest.c.d()     // Catch:{ IOException -> 0x0115 }
            java.lang.String r5 = r5.a()     // Catch:{ IOException -> 0x0115 }
            java.lang.String r3 = r3.replace(r4, r5)     // Catch:{ IOException -> 0x0115 }
            java.net.URL r4 = new java.net.URL     // Catch:{ IOException -> 0x0115 }
            r4.<init>(r3)     // Catch:{ IOException -> 0x0115 }
            java.net.URLConnection r3 = r4.openConnection()     // Catch:{ IOException -> 0x0115 }
            java.net.HttpURLConnection r3 = (java.net.HttpURLConnection) r3     // Catch:{ IOException -> 0x0115 }
            java.lang.String r2 = "GET"
            r3.setRequestMethod(r2)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2 = 10000(0x2710, float:1.4013E-41)
            r3.setConnectTimeout(r2)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r3.setReadTimeout(r2)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            com.startapp.networkTest.d r2 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            long r4 = r2.e()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.text.SimpleDateFormat r2 = new java.text.SimpleDateFormat     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r6 = "EEE, dd MMM yyyy HH:mm:ss z"
            java.util.Locale r7 = java.util.Locale.US     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.<init>(r6, r7)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r6 = "GMT"
            java.util.TimeZone r6 = java.util.TimeZone.getTimeZone(r6)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.setTimeZone(r6)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.Long r4 = java.lang.Long.valueOf(r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r2 = r2.format(r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r4 = "If-Modified-Since"
            r3.setRequestProperty(r4, r2)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r2 = "Connection"
            java.lang.String r4 = "close"
            r3.setRequestProperty(r2, r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            int r2 = r3.getResponseCode()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r4 = 304(0x130, float:4.26E-43)
            if (r2 != r4) goto L_0x0076
            com.startapp.networkTest.d r2 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            long r4 = com.startapp.networkTest.e.b.b()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.b((long) r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            goto L_0x0108
        L_0x0076:
            int r2 = r3.getResponseCode()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 != r4) goto L_0x0108
            long r4 = r3.getLastModified()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.InputStream r2 = r3.getInputStream()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r6 = d(r9)     // Catch:{ all -> 0x0103 }
            a((java.io.InputStream) r2, (java.io.File) r6)     // Catch:{ all -> 0x0103 }
            r2.close()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            com.startapp.networkTest.a r2 = com.startapp.networkTest.c.d()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            boolean r2 = r2.j()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r6 = 1
            if (r2 == 0) goto L_0x00b2
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r7 = d(r9)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.<init>(r7, r1)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r7 = new java.io.File     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r8 = d(r9)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r7.<init>(r8, r0)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            boolean r2 = a((java.io.File) r2, (java.io.File) r7)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            goto L_0x00b3
        L_0x00b2:
            r2 = 1
        L_0x00b3:
            if (r2 == 0) goto L_0x00fb
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r7 = d(r9)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.<init>(r7, r1)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r7 = b(r9)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            boolean r2 = r2.renameTo(r7)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            if (r2 == 0) goto L_0x00f3
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r7 = d(r9)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.<init>(r7, r0)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.io.File r7 = c(r9)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            boolean r2 = r2.renameTo(r7)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            if (r2 == 0) goto L_0x00f3
            com.startapp.networkTest.d r2 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            long r7 = com.startapp.networkTest.e.b.b()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.b((long) r7)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            com.startapp.networkTest.d r2 = com.startapp.networkTest.c.c()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            r2.c((long) r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            if (r3 == 0) goto L_0x00f2
            r3.disconnect()
        L_0x00f2:
            return r6
        L_0x00f3:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r4 = "Moving of cached files failed."
            r2.<init>(r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            throw r2     // Catch:{ IOException -> 0x0110, all -> 0x010e }
        L_0x00fb:
            java.io.IOException r2 = new java.io.IOException     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            java.lang.String r4 = "Verification of downloaded truststore failed"
            r2.<init>(r4)     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            throw r2     // Catch:{ IOException -> 0x0110, all -> 0x010e }
        L_0x0103:
            r4 = move-exception
            r2.close()     // Catch:{ IOException -> 0x0110, all -> 0x010e }
            throw r4     // Catch:{ IOException -> 0x0110, all -> 0x010e }
        L_0x0108:
            if (r3 == 0) goto L_0x013e
            r3.disconnect()
            goto L_0x013e
        L_0x010e:
            r9 = move-exception
            goto L_0x0140
        L_0x0110:
            r2 = r3
            goto L_0x0115
        L_0x0112:
            r9 = move-exception
            r3 = r2
            goto L_0x0140
        L_0x0115:
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x0112 }
            java.io.File r4 = d(r9)     // Catch:{ all -> 0x0112 }
            r3.<init>(r4, r1)     // Catch:{ all -> 0x0112 }
            boolean r1 = r3.exists()     // Catch:{ all -> 0x0112 }
            if (r1 == 0) goto L_0x0127
            r3.delete()     // Catch:{ all -> 0x0112 }
        L_0x0127:
            java.io.File r1 = new java.io.File     // Catch:{ all -> 0x0112 }
            java.io.File r9 = d(r9)     // Catch:{ all -> 0x0112 }
            r1.<init>(r9, r0)     // Catch:{ all -> 0x0112 }
            boolean r9 = r1.exists()     // Catch:{ all -> 0x0112 }
            if (r9 == 0) goto L_0x0139
            r1.delete()     // Catch:{ all -> 0x0112 }
        L_0x0139:
            if (r2 == 0) goto L_0x013e
            r2.disconnect()
        L_0x013e:
            r9 = 0
            return r9
        L_0x0140:
            if (r3 == 0) goto L_0x0145
            r3.disconnect()
        L_0x0145:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.utils.j.a(android.content.Context):boolean");
    }

    public static File b(Context context) {
        File file = new File(context.getFilesDir() + "/insight/truststore/");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, "truststore.bin");
    }

    public static File c(Context context) {
        File file = new File(context.getFilesDir() + "/insight/truststore/");
        if (!file.exists()) {
            file.mkdirs();
        }
        return new File(file, "truststore.bin.sig");
    }

    private static File d(Context context) {
        File file = new File(context.getCacheDir() + "/insight/truststore/", "truststoreunzip");
        if (!file.exists()) {
            file.mkdirs();
        }
        return file;
    }

    private static void a(InputStream inputStream, File file) throws IOException {
        ZipInputStream zipInputStream = new ZipInputStream(inputStream);
        while (true) {
            try {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry == null) {
                    return;
                }
                if (nextEntry.isDirectory()) {
                    File file2 = new File(file + File.separator + nextEntry.getName());
                    if (!file2.isDirectory()) {
                        file2.mkdirs();
                    }
                } else {
                    FileOutputStream fileOutputStream = new FileOutputStream(file + File.separator + nextEntry.getName());
                    while (true) {
                        int read = zipInputStream.read();
                        if (read == -1) {
                            break;
                        }
                        fileOutputStream.write(read);
                    }
                    zipInputStream.closeEntry();
                    fileOutputStream.close();
                }
            } finally {
                zipInputStream.close();
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:32:0x0073 A[SYNTHETIC, Splitter:B:32:0x0073] */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x007d A[SYNTHETIC, Splitter:B:37:0x007d] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0089 A[SYNTHETIC, Splitter:B:45:0x0089] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x0093 A[SYNTHETIC, Splitter:B:50:0x0093] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(java.io.File r7, java.io.File r8) {
        /*
            r0 = 0
            r1 = 0
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0086, all -> 0x006f }
            r2.<init>(r8)     // Catch:{ Exception -> 0x0086, all -> 0x006f }
            java.io.ByteArrayOutputStream r8 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            r8.<init>()     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            r3 = 512(0x200, float:7.175E-43)
            byte[] r3 = new byte[r3]     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            int r4 = r2.read(r3)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
        L_0x0014:
            r5 = -1
            if (r4 == r5) goto L_0x001f
            r8.write(r3, r0, r4)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            int r4 = r2.read(r3)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            goto L_0x0014
        L_0x001f:
            r8.flush()     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            byte[] r8 = r8.toByteArray()     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            r4.<init>(r7)     // Catch:{ Exception -> 0x006d, all -> 0x006b }
            java.io.ByteArrayOutputStream r7 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r7.<init>()     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            int r6 = r4.read(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
        L_0x0034:
            if (r6 == r5) goto L_0x003e
            r7.write(r3, r0, r6)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            int r6 = r4.read(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            goto L_0x0034
        L_0x003e:
            r7.flush()     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            byte[] r7 = r7.toByteArray()     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            java.lang.String r3 = "SHA256withRSA"
            java.security.Signature r3 = java.security.Signature.getInstance(r3)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r3.initVerify(r1)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r3.update(r7)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            boolean r7 = r3.verify(r8)     // Catch:{ Exception -> 0x0069, all -> 0x0066 }
            r2.close()     // Catch:{ IOException -> 0x0059 }
            goto L_0x005d
        L_0x0059:
            r8 = move-exception
            r8.printStackTrace()
        L_0x005d:
            r4.close()     // Catch:{ IOException -> 0x0061 }
            goto L_0x0065
        L_0x0061:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0065:
            return r7
        L_0x0066:
            r7 = move-exception
            r1 = r4
            goto L_0x0071
        L_0x0069:
            r1 = r4
            goto L_0x0087
        L_0x006b:
            r7 = move-exception
            goto L_0x0071
        L_0x006d:
            goto L_0x0087
        L_0x006f:
            r7 = move-exception
            r2 = r1
        L_0x0071:
            if (r2 == 0) goto L_0x007b
            r2.close()     // Catch:{ IOException -> 0x0077 }
            goto L_0x007b
        L_0x0077:
            r8 = move-exception
            r8.printStackTrace()
        L_0x007b:
            if (r1 == 0) goto L_0x0085
            r1.close()     // Catch:{ IOException -> 0x0081 }
            goto L_0x0085
        L_0x0081:
            r8 = move-exception
            r8.printStackTrace()
        L_0x0085:
            throw r7
        L_0x0086:
            r2 = r1
        L_0x0087:
            if (r2 == 0) goto L_0x0091
            r2.close()     // Catch:{ IOException -> 0x008d }
            goto L_0x0091
        L_0x008d:
            r7 = move-exception
            r7.printStackTrace()
        L_0x0091:
            if (r1 == 0) goto L_0x009b
            r1.close()     // Catch:{ IOException -> 0x0097 }
            goto L_0x009b
        L_0x0097:
            r7 = move-exception
            r7.printStackTrace()
        L_0x009b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.utils.j.a(java.io.File, java.io.File):boolean");
    }
}
