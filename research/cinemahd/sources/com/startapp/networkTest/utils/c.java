package com.startapp.networkTest.utils;

import com.startapp.networkTest.enums.ThreeStateShort;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final char[] f6089a = "0123456789abcdef".toCharArray();

    /* renamed from: com.startapp.networkTest.utils.c$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6090a = new int[ThreeStateShort.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.startapp.networkTest.enums.ThreeStateShort[] r0 = com.startapp.networkTest.enums.ThreeStateShort.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6090a = r0
                int[] r0 = f6090a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.enums.ThreeStateShort r1 = com.startapp.networkTest.enums.ThreeStateShort.Yes     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6090a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.enums.ThreeStateShort r1 = com.startapp.networkTest.enums.ThreeStateShort.No     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6090a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.enums.ThreeStateShort r1 = com.startapp.networkTest.enums.ThreeStateShort.Unknown     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.utils.c.AnonymousClass1.<clinit>():void");
        }
    }

    public static String a(byte[] bArr) {
        char[] cArr = new char[(bArr.length << 1)];
        for (int i = 0; i < bArr.length; i++) {
            byte b = bArr[i] & 255;
            int i2 = i << 1;
            char[] cArr2 = f6089a;
            cArr[i2] = cArr2[b >>> 4];
            cArr[i2 + 1] = cArr2[b & 15];
        }
        return new String(cArr);
    }

    public static int a(ThreeStateShort threeStateShort) {
        int i = AnonymousClass1.f6090a[threeStateShort.ordinal()];
        if (i != 1) {
            return i != 2 ? -1 : 0;
        }
        return 1;
    }
}
