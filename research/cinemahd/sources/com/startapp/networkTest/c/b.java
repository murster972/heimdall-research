package com.startapp.networkTest.c;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.startapp.networkTest.controller.LocationController;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.d;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.TriggerEvents;
import com.startapp.networkTest.enums.voice.CallStates;
import com.startapp.networkTest.results.NetworkInformationResult;
import java.util.ArrayList;
import java.util.Arrays;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private SharedPreferences f5976a;
    private Context b;
    private d c;
    private String d;
    private c e;
    private com.startapp.networkTest.controller.d f;
    private LocationController g;
    private TelephonyManager h;
    private a i;
    private C0066b j;
    private int k = 0;
    private int l;
    private boolean m;

    class a {

        /* renamed from: a  reason: collision with root package name */
        String f5977a;
        double b;
        double c;

        a(String str, double d2, double d3) {
            this.f5977a = str;
            this.b = d2;
            this.c = d3;
        }
    }

    public b(Context context) {
        int i2;
        this.b = context;
        this.c = new d(context);
        this.d = com.startapp.networkTest.c.d().a();
        this.f5976a = context.getSharedPreferences("p3insnir", 0);
        this.h = (TelephonyManager) context.getSystemService("phone");
        if (!(Build.VERSION.SDK_INT < 24 || this.h == null || (i2 = com.startapp.networkTest.controller.b.f(context).SubscriptionId) == -1)) {
            this.h = this.h.createForSubscriptionId(i2);
        }
        this.e = new c(this.b);
        this.f = new com.startapp.networkTest.controller.d(this.b);
        this.g = new LocationController(this.b);
        this.j = new C0066b(this, (byte) 0);
        this.m = com.startapp.networkTest.c.d().x();
        this.l = com.startapp.networkTest.c.d().y();
        if (this.l <= 0) {
            this.l = 1;
        }
    }

    public final void a() {
        this.g.a(LocationController.ProviderMode.Passive);
        this.e.a();
    }

    public final void b() {
        this.g.a();
        this.e.b();
    }

    public final void c() {
        LocationController locationController = this.g;
        if (locationController != null) {
            locationController.a((LocationController.b) null);
        }
    }

    public final c d() {
        return this.e;
    }

    public final NetworkInformationResult a(TriggerEvents triggerEvents, boolean z) {
        return a(this.g.b(), triggerEvents, z);
    }

    public final void b(com.startapp.networkTest.controller.a.a aVar) {
        c cVar = this.e;
        if (cVar != null) {
            cVar.b(aVar);
        }
    }

    public final NetworkInformationResult a(LocationInfo locationInfo, TriggerEvents triggerEvents, boolean z) {
        CallStates callStates;
        String str;
        NetworkInformationResult networkInformationResult = new NetworkInformationResult(this.d, this.c.a());
        if (locationInfo != null) {
            networkInformationResult.LocationInfo = locationInfo;
        } else {
            networkInformationResult.LocationInfo = this.g.b();
        }
        networkInformationResult.TimeInfo = com.startapp.networkTest.e.b.a();
        TimeInfo timeInfo = networkInformationResult.TimeInfo;
        networkInformationResult.Timestamp = timeInfo.TimestampTableau;
        networkInformationResult.timestampMillis = timeInfo.TimestampMillis;
        networkInformationResult.NirId = com.iab.omid.library.startapp.b.a(timeInfo, networkInformationResult.GUID);
        networkInformationResult.WifiInfo = this.f.a();
        networkInformationResult.TriggerEvent = triggerEvents;
        networkInformationResult.ScreenState = com.startapp.networkTest.controller.b.d(this.b);
        TelephonyManager telephonyManager = this.h;
        if (telephonyManager != null) {
            int callState = telephonyManager.getCallState();
            if (callState == 0) {
                callStates = CallStates.Idle;
            } else if (callState == 1) {
                callStates = CallStates.Ringing;
            } else if (callState != 2) {
                callStates = CallStates.Unknown;
            } else {
                callStates = CallStates.Offhook;
            }
        } else {
            callStates = CallStates.Unknown;
        }
        networkInformationResult.CallState = callStates;
        if (this.m) {
            int i2 = this.k;
            this.k = i2 + 1;
            if (i2 % this.l == 0 || z) {
                networkInformationResult.CellInfo = new ArrayList<>(Arrays.asList(this.e.d()));
            }
        }
        networkInformationResult.RadioInfo = this.e.c();
        synchronized (this) {
            if (this.i == null) {
                String string = this.f5976a.getString("P3INS_PFK_NIR_FIRSTCELLID_GSMCELLID", "");
                if (!string.isEmpty()) {
                    this.i = new a(string, Double.longBitsToDouble(this.f5976a.getLong("P3INS_PFK_NIR_FIRSTCELLID_LATITUDE", 0)), Double.longBitsToDouble(this.f5976a.getLong("P3INS_PFK_NIR_FIRSTCELLID_LONGITUDE", 0)));
                }
            }
            if (!networkInformationResult.RadioInfo.GsmCellId.isEmpty()) {
                if (networkInformationResult.LocationInfo.LocationAge < 30000 && (this.i == null || !this.i.f5977a.equals(networkInformationResult.RadioInfo.GsmCellId))) {
                    this.i = new a(networkInformationResult.RadioInfo.GsmCellId, networkInformationResult.LocationInfo.LocationLatitude, networkInformationResult.LocationInfo.LocationLongitude);
                    networkInformationResult.CellIdDeltaDistance = 0.0d;
                    a(this.i);
                }
                str = networkInformationResult.RadioInfo.GsmCellId;
            } else if (!networkInformationResult.RadioInfo.CdmaBaseStationId.isEmpty()) {
                if (networkInformationResult.LocationInfo.LocationAge < 30000 && (this.i == null || !this.i.f5977a.equals(networkInformationResult.RadioInfo.CdmaBaseStationId))) {
                    this.i = new a(networkInformationResult.RadioInfo.CdmaBaseStationId, networkInformationResult.LocationInfo.LocationLatitude, networkInformationResult.LocationInfo.LocationLongitude);
                    networkInformationResult.CellIdDeltaDistance = 0.0d;
                    a(this.i);
                }
                str = networkInformationResult.RadioInfo.CdmaBaseStationId;
            } else {
                str = "";
            }
        }
        if ((!networkInformationResult.RadioInfo.GsmCellId.isEmpty() && networkInformationResult.CellIdDeltaDistance == -1.0d && this.i.f5977a.equals(networkInformationResult.RadioInfo.GsmCellId)) || (!networkInformationResult.RadioInfo.CdmaBaseStationId.isEmpty() && networkInformationResult.CellIdDeltaDistance == -1.0d && this.i.f5977a.equals(networkInformationResult.RadioInfo.CdmaBaseStationId))) {
            a aVar = this.i;
            double d2 = aVar.b;
            double d3 = aVar.c;
            LocationInfo locationInfo2 = networkInformationResult.LocationInfo;
            double d4 = locationInfo2.LocationLatitude;
            double radians = Math.toRadians(locationInfo2.LocationLongitude - d3) * Math.cos(Math.toRadians(d2 + d4) / 2.0d);
            double radians2 = Math.toRadians(d4 - d2);
            networkInformationResult.CellIdDeltaDistance = Math.sqrt((radians * radians) + (radians2 * radians2)) * 6371000.0d;
        }
        if (!str.isEmpty() && !str.equals(this.j.f5978a)) {
            C0066b bVar = this.j;
            networkInformationResult.PrevNirId = bVar.b;
            networkInformationResult.PrevCellId = bVar.f5978a;
            networkInformationResult.PrevLAC = bVar.c;
            networkInformationResult.PrevNetworkType = bVar.d;
            networkInformationResult.PrevMCC = bVar.e;
            networkInformationResult.PrevMNC = bVar.f;
            networkInformationResult.PrevRXLevel = bVar.g;
        }
        C0066b bVar2 = this.j;
        String str2 = networkInformationResult.NirId;
        RadioInfo radioInfo = networkInformationResult.RadioInfo;
        String str3 = radioInfo.GsmLAC;
        NetworkTypes networkTypes = radioInfo.NetworkType;
        String str4 = radioInfo.MCC;
        String str5 = radioInfo.MNC;
        int i3 = radioInfo.RXLevel;
        bVar2.b = str2;
        bVar2.f5978a = str;
        bVar2.c = str3;
        bVar2.d = networkTypes;
        bVar2.e = str4;
        bVar2.f = str5;
        bVar2.g = i3;
        return networkInformationResult;
    }

    /* renamed from: com.startapp.networkTest.c.b$b  reason: collision with other inner class name */
    class C0066b {

        /* renamed from: a  reason: collision with root package name */
        String f5978a;
        String b;
        String c;
        NetworkTypes d;
        String e;
        String f;
        int g;

        private C0066b() {
            this.f5978a = "";
            this.b = "";
            this.c = "";
            this.d = NetworkTypes.Unknown;
            this.e = "";
            this.f = "";
        }

        /* synthetic */ C0066b(b bVar, byte b2) {
            this();
        }
    }

    @SuppressLint({"ApplySharedPref"})
    private void a(a aVar) {
        this.f5976a.edit().putString("P3INS_PFK_NIR_FIRSTCELLID_GSMCELLID", aVar.f5977a).commit();
        this.f5976a.edit().putLong("P3INS_PFK_NIR_FIRSTCELLID_LATITUDE", Double.doubleToRawLongBits(aVar.b)).commit();
        this.f5976a.edit().putLong("P3INS_PFK_NIR_FIRSTCELLID_LONGITUDE", Double.doubleToRawLongBits(aVar.c)).commit();
    }

    public final void a(LocationController.b bVar) {
        LocationController locationController = this.g;
        if (locationController != null) {
            locationController.a(bVar);
        }
    }

    public final void a(com.startapp.networkTest.controller.a.a aVar) {
        c cVar = this.e;
        if (cVar != null) {
            cVar.a(aVar);
        }
    }
}
