package com.startapp.networkTest.c;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.original.tase.model.socket.UserResponces;
import com.startapp.common.parser.b;
import com.startapp.networkTest.controller.LocationController;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.controller.d;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.enums.LtrCriteriaTypes;
import com.startapp.networkTest.results.LatencyResult;
import com.startapp.networkTest.results.P3TestResult;
import com.startapp.networkTest.results.speedtest.MeasurementPointLatency;
import com.startapp.networkTest.speedtest.SpeedtestEngineError;
import com.startapp.networkTest.speedtest.SpeedtestEngineStatus;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class a {

    /* renamed from: a  reason: collision with root package name */
    public static final String f5967a = "a";
    /* access modifiers changed from: private */
    public com.startapp.networkTest.speedtest.a b;
    /* access modifiers changed from: private */
    public Context c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public d e;
    /* access modifiers changed from: private */
    public LocationController f;
    /* access modifiers changed from: private */
    public com.startapp.networkTest.controller.a g;
    /* access modifiers changed from: private */
    public P3TestResult h;
    private ArrayList<com.startapp.networkTest.data.d> i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public com.startapp.networkTest.d k;
    /* access modifiers changed from: private */
    public String l;
    /* access modifiers changed from: private */
    public String m = "";
    private String n = "";
    private String o = "";
    private String p = "";
    private String q = "";
    private String r = "";
    /* access modifiers changed from: private */
    public String s = "";

    /* renamed from: com.startapp.networkTest.c.a$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5968a = new int[LtrCriteriaTypes.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.networkTest.enums.LtrCriteriaTypes[] r0 = com.startapp.networkTest.enums.LtrCriteriaTypes.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5968a = r0
                int[] r0 = f5968a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.CTItem     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5968a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.NoChange     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5968a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.Random     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f5968a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.FullSuccessful     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f5968a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.TotalTests     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.c.a.AnonymousClass1.<clinit>():void");
        }
    }

    public a(com.startapp.networkTest.speedtest.a aVar, Context context) {
        this.b = aVar;
        this.c = context;
        com.startapp.networkTest.a d2 = com.startapp.networkTest.c.d();
        this.j = d2.a();
        this.k = new com.startapp.networkTest.d(this.c);
        this.d = new c(context);
        this.e = new d(context);
        this.f = new LocationController(this.c);
        this.g = new com.startapp.networkTest.controller.a(this.c);
        this.i = new ArrayList<>();
        if (d2.t()) {
            context.getSystemService("phone");
        }
    }

    /* renamed from: com.startapp.networkTest.c.a$a  reason: collision with other inner class name */
    class C0064a extends AsyncTask<Void, Void, LatencyResult> {
        private String b;
        /* access modifiers changed from: private */
        public int c = 10;
        private int d = UserResponces.USER_RESPONCE_SUCCSES;
        private int e = HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT;
        private int f = 56;
        private String[] g;
        private LtrCriteriaTypes h;
        private boolean i = true;

        public C0064a(String str) {
            this.b = str;
            if (this.d < 200) {
                this.d = UserResponces.USER_RESPONCE_SUCCSES;
            }
            if (a.this.b != null) {
                com.startapp.networkTest.speedtest.a a2 = a.this.b;
                SpeedtestEngineStatus speedtestEngineStatus = SpeedtestEngineStatus.CONNECT;
                SpeedtestEngineError speedtestEngineError = SpeedtestEngineError.OK;
                a2.a(speedtestEngineStatus);
            }
            com.startapp.networkTest.d c2 = com.startapp.networkTest.c.c();
            this.g = c2.l();
            this.h = LtrCriteriaTypes.valueOf(c2.m());
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            LatencyResult latencyResult = (LatencyResult) obj;
            super.onPostExecute(latencyResult);
            P3TestResult unused = a.this.h = latencyResult;
            if (latencyResult != null) {
                if (a.this.b != null) {
                    com.startapp.networkTest.speedtest.a a2 = a.this.b;
                    SpeedtestEngineStatus speedtestEngineStatus = SpeedtestEngineStatus.END;
                    SpeedtestEngineError speedtestEngineError = SpeedtestEngineError.OK;
                    a2.a(speedtestEngineStatus);
                }
            } else if (a.this.b != null) {
                com.startapp.networkTest.speedtest.a a3 = a.this.b;
                SpeedtestEngineStatus speedtestEngineStatus2 = SpeedtestEngineStatus.ABORTED;
                SpeedtestEngineError speedtestEngineError2 = SpeedtestEngineError.OK;
                a3.a(speedtestEngineStatus2);
            }
        }

        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v17, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v21, resolved type: java.util.List<com.startapp.networkTest.d.a.d>} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v18, resolved type: java.lang.String} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v32, resolved type: java.util.List<com.startapp.networkTest.d.a.d>} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v35, resolved type: java.util.List<com.startapp.networkTest.d.a.d>} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v37, resolved type: java.util.List<com.startapp.networkTest.d.a.d>} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v39, resolved type: java.util.List<com.startapp.networkTest.d.a.d>} */
        /* JADX WARNING: type inference failed for: r8v23, types: [java.lang.String[]] */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:136:0x02d8 A[Catch:{ Exception -> 0x0304 }] */
        /* JADX WARNING: Removed duplicated region for block: B:158:0x031d  */
        /* JADX WARNING: Removed duplicated region for block: B:180:0x0371 A[SYNTHETIC, Splitter:B:180:0x0371] */
        /* JADX WARNING: Removed duplicated region for block: B:185:0x037c  */
        /* JADX WARNING: Removed duplicated region for block: B:187:0x0381  */
        /* JADX WARNING: Removed duplicated region for block: B:188:0x03b3  */
        /* JADX WARNING: Removed duplicated region for block: B:191:0x03ce  */
        /* JADX WARNING: Removed duplicated region for block: B:194:0x0445  */
        /* JADX WARNING: Removed duplicated region for block: B:195:0x044c  */
        /* JADX WARNING: Removed duplicated region for block: B:199:0x0475 A[LOOP:0: B:8:0x0028->B:199:0x0475, LOOP_END] */
        /* JADX WARNING: Removed duplicated region for block: B:204:0x0484 A[SYNTHETIC, Splitter:B:204:0x0484] */
        /* JADX WARNING: Removed duplicated region for block: B:209:0x048f  */
        /* JADX WARNING: Removed duplicated region for block: B:221:0x046d A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:24:0x014d A[SYNTHETIC, Splitter:B:24:0x014d] */
        /* JADX WARNING: Removed duplicated region for block: B:61:0x01e5  */
        /* JADX WARNING: Removed duplicated region for block: B:75:0x0229  */
        /* JADX WARNING: Removed duplicated region for block: B:82:0x023d A[SYNTHETIC, Splitter:B:82:0x023d] */
        /* JADX WARNING: Removed duplicated region for block: B:89:0x0253  */
        /* JADX WARNING: Removed duplicated region for block: B:91:0x0257 A[SYNTHETIC, Splitter:B:91:0x0257] */
        /* JADX WARNING: Unknown variable types count: 1 */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private com.startapp.networkTest.results.LatencyResult a() {
            /*
                r31 = this;
                r7 = r31
                java.lang.String r8 = "ping6"
                boolean r0 = r31.isCancelled()
                r9 = 0
                if (r0 == 0) goto L_0x000c
                return r9
            L_0x000c:
                boolean r0 = r7.i
                if (r0 == 0) goto L_0x001b
                java.lang.String[] r0 = r7.g
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = r7.h
                java.lang.String r2 = r7.b
                java.util.List r0 = r7.a((java.lang.String[]) r0, (com.startapp.networkTest.enums.LtrCriteriaTypes) r1, (java.lang.String) r2)
                goto L_0x0025
            L_0x001b:
                java.lang.String[] r0 = r7.g
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.CTItem
                java.lang.String r2 = r7.b
                java.util.List r0 = r7.a((java.lang.String[]) r0, (com.startapp.networkTest.enums.LtrCriteriaTypes) r1, (java.lang.String) r2)
            L_0x0025:
                r10 = r0
                r0 = r9
                r12 = 0
            L_0x0028:
                int r1 = r10.size()
                if (r12 >= r1) goto L_0x0493
                long r13 = android.os.SystemClock.elapsedRealtime()
                long r15 = android.os.SystemClock.uptimeMillis()
                java.lang.Object r0 = r10.get(r12)
                r6 = r0
                com.startapp.networkTest.d.a.d r6 = (com.startapp.networkTest.d.a.d) r6
                int r0 = r6.totalTests
                r5 = 1
                int r0 = r0 + r5
                r6.totalTests = r0
                java.lang.String r0 = r6.address
                r7.b = r0
                java.util.ArrayList r4 = new java.util.ArrayList
                r4.<init>()
                com.startapp.networkTest.results.LatencyResult r3 = new com.startapp.networkTest.results.LatencyResult
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                java.lang.String r0 = r0.j
                com.startapp.networkTest.c.a r1 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.d r1 = r1.k
                java.lang.String r1 = r1.a()
                r3.<init>(r0, r1)
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.a r0 = r0.g
                com.startapp.networkTest.data.BatteryInfo r0 = r0.a()
                r3.BatteryInfoOnStart = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.LocationController r0 = r0.f
                com.startapp.networkTest.data.LocationInfo r0 = r0.b()
                r3.LocationInfoOnStart = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                android.content.Context r0 = r0.c
                com.startapp.networkTest.enums.ScreenStates r0 = com.startapp.networkTest.controller.b.d(r0)
                r3.ScreenStateOnStart = r0
                com.startapp.networkTest.enums.MeasurementTypes r0 = com.startapp.networkTest.enums.MeasurementTypes.IPING
                r3.MeasurementType = r0
                com.startapp.networkTest.data.TimeInfo r0 = com.startapp.networkTest.e.b.a()
                r3.TimeInfoOnStart = r0
                com.startapp.networkTest.data.TimeInfo r0 = r3.TimeInfoOnStart
                java.lang.String r1 = r3.GUID
                java.lang.String r0 = com.iab.omid.library.startapp.b.a((com.startapp.networkTest.data.TimeInfo) r0, (java.lang.String) r1)
                r3.LtrId = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                android.content.Context r0 = r0.c
                com.startapp.networkTest.data.b r0 = com.startapp.networkTest.controller.b.b(r0)
                r3.MemoryInfoOnStart = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.c r0 = r0.d
                com.startapp.networkTest.data.RadioInfo r0 = r0.c()
                r3.RadioInfoOnStart = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.d r0 = r0.e
                com.startapp.networkTest.data.WifiInfo r0 = r0.a()
                r3.WifiInfoOnStart = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.d r0 = r0.e
                com.startapp.networkTest.data.f r0 = com.startapp.networkTest.controller.b.a((com.startapp.networkTest.controller.d) r0)
                r3.TrafficInfoOnStart = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                android.content.Context r0 = r0.c
                com.startapp.networkTest.data.a r0 = com.startapp.networkTest.controller.b.a((android.content.Context) r0)
                r3.DeviceInfo = r0
                int r0 = r7.c
                r3.Pings = r0
                int r0 = r7.d
                r3.Pause = r0
                java.lang.String r1 = r7.b
                r3.Server = r1
                com.startapp.networkTest.enums.IpVersions r0 = com.startapp.networkTest.enums.IpVersions.IPv4
                r3.IpVersion = r0
                java.lang.String r2 = "ping"
                java.net.InetAddress r9 = java.net.InetAddress.getByName(r1)     // Catch:{ UnknownHostException -> 0x00fb }
                java.lang.String r1 = r9.getHostAddress()     // Catch:{ UnknownHostException -> 0x00f9 }
                boolean r0 = r9 instanceof java.net.Inet6Address     // Catch:{ UnknownHostException -> 0x00f9 }
                if (r0 == 0) goto L_0x0100
                com.startapp.networkTest.enums.IpVersions r0 = com.startapp.networkTest.enums.IpVersions.IPv6     // Catch:{ UnknownHostException -> 0x00f9 }
                r3.IpVersion = r0     // Catch:{ UnknownHostException -> 0x00f9 }
                r2 = r8
                goto L_0x0100
            L_0x00f9:
                r0 = move-exception
                goto L_0x00fd
            L_0x00fb:
                r0 = move-exception
                r9 = 0
            L_0x00fd:
                r0.printStackTrace()
            L_0x0100:
                r19 = r9
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                r0.append(r2)
                java.lang.String r9 = " -i "
                r0.append(r9)
                int r9 = r7.d
                r24 = r12
                double r11 = (double) r9
                r20 = 4652007308841189376(0x408f400000000000, double:1000.0)
                double r11 = r11 / r20
                r0.append(r11)
                java.lang.String r9 = " -W "
                r0.append(r9)
                int r9 = r7.e
                double r11 = (double) r9
                double r11 = r11 / r20
                r0.append(r11)
                java.lang.String r9 = " -c "
                r0.append(r9)
                int r9 = r7.c
                r0.append(r9)
                java.lang.String r9 = " -s "
                r0.append(r9)
                int r9 = r7.f
                r0.append(r9)
                java.lang.String r9 = r0.toString()
                boolean r0 = r2.equals(r8)
                r2 = 21
                java.lang.String r11 = ""
                if (r0 == 0) goto L_0x01e5
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this     // Catch:{ Exception -> 0x01d8 }
                android.content.Context r0 = r0.c     // Catch:{ Exception -> 0x01d8 }
                java.lang.String r12 = "connectivity"
                java.lang.Object r0 = r0.getSystemService(r12)     // Catch:{ Exception -> 0x01d8 }
                android.net.ConnectivityManager r0 = (android.net.ConnectivityManager) r0     // Catch:{ Exception -> 0x01d8 }
                if (r0 == 0) goto L_0x01b6
                com.startapp.networkTest.c.a r12 = com.startapp.networkTest.c.a.this     // Catch:{ Exception -> 0x01d8 }
                android.content.Context r12 = r12.c     // Catch:{ Exception -> 0x01d8 }
                java.lang.String r5 = "android.permission.ACCESS_NETWORK_STATE"
                int r5 = r12.checkCallingOrSelfPermission(r5)     // Catch:{ Exception -> 0x01d8 }
                if (r5 != 0) goto L_0x01b6
                int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01d8 }
                r12 = 23
                if (r5 < r12) goto L_0x0184
                android.net.Network r5 = r0.getActiveNetwork()     // Catch:{ Exception -> 0x01d8 }
                android.net.LinkProperties r0 = r0.getLinkProperties(r5)     // Catch:{ Exception -> 0x01d8 }
                if (r0 == 0) goto L_0x0180
                java.lang.String r0 = r0.getInterfaceName()     // Catch:{ Exception -> 0x01d8 }
                goto L_0x0181
            L_0x0180:
                r0 = r11
            L_0x0181:
                r25 = r6
                goto L_0x01b9
            L_0x0184:
                int r5 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x01d8 }
                if (r5 < r2) goto L_0x01b6
                android.net.Network[] r5 = r0.getAllNetworks()     // Catch:{ Exception -> 0x01d8 }
                int r12 = r5.length     // Catch:{ Exception -> 0x01d8 }
                r20 = r11
                r2 = 0
            L_0x0190:
                if (r2 >= r12) goto L_0x01b1
                r25 = r6
                r6 = r5[r2]     // Catch:{ Exception -> 0x01d6 }
                android.net.NetworkInfo r21 = r0.getNetworkInfo(r6)     // Catch:{ Exception -> 0x01d6 }
                if (r21 == 0) goto L_0x01ac
                boolean r21 = r21.isConnected()     // Catch:{ Exception -> 0x01d6 }
                if (r21 == 0) goto L_0x01ac
                android.net.LinkProperties r6 = r0.getLinkProperties(r6)     // Catch:{ Exception -> 0x01d6 }
                if (r6 == 0) goto L_0x01ac
                java.lang.String r20 = r6.getInterfaceName()     // Catch:{ Exception -> 0x01d6 }
            L_0x01ac:
                int r2 = r2 + 1
                r6 = r25
                goto L_0x0190
            L_0x01b1:
                r25 = r6
                r0 = r20
                goto L_0x01b9
            L_0x01b6:
                r25 = r6
                r0 = r11
            L_0x01b9:
                if (r0 == 0) goto L_0x01e7
                boolean r2 = r0.isEmpty()     // Catch:{ Exception -> 0x01d6 }
                if (r2 != 0) goto L_0x01e7
                java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x01d6 }
                r2.<init>()     // Catch:{ Exception -> 0x01d6 }
                r2.append(r9)     // Catch:{ Exception -> 0x01d6 }
                java.lang.String r5 = " -I "
                r2.append(r5)     // Catch:{ Exception -> 0x01d6 }
                r2.append(r0)     // Catch:{ Exception -> 0x01d6 }
                java.lang.String r9 = r2.toString()     // Catch:{ Exception -> 0x01d6 }
                goto L_0x01e7
            L_0x01d6:
                r0 = move-exception
                goto L_0x01db
            L_0x01d8:
                r0 = move-exception
                r25 = r6
            L_0x01db:
                java.lang.String r2 = com.startapp.networkTest.c.a.f5967a
                java.lang.String r0 = r0.toString()
                android.util.Log.e(r2, r0)
                goto L_0x01e7
            L_0x01e5:
                r25 = r6
            L_0x01e7:
                java.lang.StringBuilder r0 = new java.lang.StringBuilder
                r0.<init>()
                r0.append(r9)
                java.lang.String r2 = " "
                r0.append(r2)
                r0.append(r1)
                java.lang.String r0 = r0.toString()
                r1 = 1
                boolean[] r9 = new boolean[r1]
                r5 = 0
                r9[r5] = r5
                int[] r12 = new int[r1]
                r12[r5] = r5
                java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ Exception -> 0x034b, all -> 0x0344 }
                java.lang.Process r1 = r1.exec(r0)     // Catch:{ Exception -> 0x034b, all -> 0x0344 }
                java.io.BufferedReader r5 = new java.io.BufferedReader     // Catch:{ Exception -> 0x033c, all -> 0x0336 }
                java.io.InputStreamReader r0 = new java.io.InputStreamReader     // Catch:{ Exception -> 0x033c, all -> 0x0336 }
                java.io.InputStream r6 = r1.getInputStream()     // Catch:{ Exception -> 0x033c, all -> 0x0336 }
                r0.<init>(r6)     // Catch:{ Exception -> 0x033c, all -> 0x0336 }
                r5.<init>(r0)     // Catch:{ Exception -> 0x033c, all -> 0x0336 }
                java.lang.String r0 = r5.readLine()     // Catch:{ Exception -> 0x032e }
                if (r0 != 0) goto L_0x0234
                if (r19 == 0) goto L_0x0234
                int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x022b }
                r6 = 21
                if (r0 < r6) goto L_0x0234
                r6 = 1
                goto L_0x0235
            L_0x022b:
                r0 = move-exception
                r26 = r8
                r8 = r10
                r27 = r13
                r6 = 0
                goto L_0x032c
            L_0x0234:
                r6 = 0
            L_0x0235:
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this     // Catch:{ Exception -> 0x0324 }
                com.startapp.networkTest.speedtest.a r0 = r0.b     // Catch:{ Exception -> 0x0324 }
                if (r0 == 0) goto L_0x0253
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this     // Catch:{ Exception -> 0x0250 }
                com.startapp.networkTest.speedtest.a r0 = r0.b     // Catch:{ Exception -> 0x0250 }
                r26 = r8
                com.startapp.networkTest.speedtest.SpeedtestEngineStatus r8 = com.startapp.networkTest.speedtest.SpeedtestEngineStatus.PING     // Catch:{ Exception -> 0x024d }
                com.startapp.networkTest.speedtest.SpeedtestEngineError r18 = com.startapp.networkTest.speedtest.SpeedtestEngineError.OK     // Catch:{ Exception -> 0x024d }
                r0.a(r8)     // Catch:{ Exception -> 0x024d }
                goto L_0x0255
            L_0x024d:
                r0 = move-exception
                goto L_0x0329
            L_0x0250:
                r0 = move-exception
                goto L_0x0327
            L_0x0253:
                r26 = r8
            L_0x0255:
                if (r6 != 0) goto L_0x030c
                long r20 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0308 }
                r0 = 0
            L_0x025c:
                int r8 = r7.c     // Catch:{ Exception -> 0x0308 }
                if (r0 >= r8) goto L_0x030c
                boolean r8 = r31.isCancelled()     // Catch:{ Exception -> 0x0308 }
                if (r8 == 0) goto L_0x0279
                r5.close()     // Catch:{ Exception -> 0x024d }
                r5.close()     // Catch:{ IOException -> 0x026d }
                goto L_0x0272
            L_0x026d:
                r0 = move-exception
                r2 = r0
                r2.printStackTrace()
            L_0x0272:
                if (r1 == 0) goto L_0x0277
                r1.destroy()
            L_0x0277:
                r8 = 0
                return r8
            L_0x0279:
                java.lang.String r8 = r5.readLine()     // Catch:{ Exception -> 0x0308 }
                long r22 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0308 }
                r27 = r13
                long r13 = r22 - r20
                r18 = -1
                if (r8 == 0) goto L_0x02e9
                int r22 = r8.length()     // Catch:{ Exception -> 0x02e4 }
                if (r22 <= 0) goto L_0x02e9
                java.lang.String[] r8 = r8.split(r2)     // Catch:{ Exception -> 0x02e4 }
                r22 = r2
                int r2 = r8.length     // Catch:{ Exception -> 0x02e4 }
                r23 = r6
                r6 = 8
                if (r2 == r6) goto L_0x02aa
                int r2 = r8.length     // Catch:{ Exception -> 0x02a4 }
                r6 = 9
                if (r2 != r6) goto L_0x02a2
                goto L_0x02aa
            L_0x02a2:
                r2 = -1
                goto L_0x02ad
            L_0x02a4:
                r0 = move-exception
                r8 = r10
                r6 = r23
                goto L_0x032c
            L_0x02aa:
                int r2 = r8.length     // Catch:{ Exception -> 0x02e0 }
                int r2 = r2 + -2
            L_0x02ad:
                r6 = 6
                if (r2 == r6) goto L_0x02b3
                r6 = 7
                if (r2 != r6) goto L_0x02ed
            L_0x02b3:
                r2 = r8[r2]     // Catch:{ Exception -> 0x02e0 }
                java.lang.String r6 = "time="
                java.lang.String r2 = r2.replace(r6, r11)     // Catch:{ Exception -> 0x02e0 }
                double r29 = java.lang.Double.parseDouble(r2)     // Catch:{ Exception -> 0x02e0 }
                r8 = r10
                r2 = r11
                long r10 = java.lang.Math.round(r29)     // Catch:{ Exception -> 0x02de }
                int r6 = (int) r10
                r10 = 1
                r11 = 0
                r9[r11] = r10     // Catch:{ Exception -> 0x0304 }
                r18 = r12[r11]     // Catch:{ Exception -> 0x0304 }
                int r18 = r18 + 1
                r12[r11] = r18     // Catch:{ Exception -> 0x0304 }
                com.startapp.networkTest.c.a r11 = com.startapp.networkTest.c.a.this     // Catch:{ Exception -> 0x0304 }
                com.startapp.networkTest.speedtest.a r11 = r11.b     // Catch:{ Exception -> 0x0304 }
                if (r11 == 0) goto L_0x02f1
                com.startapp.networkTest.c.a r11 = com.startapp.networkTest.c.a.this     // Catch:{ Exception -> 0x0304 }
                com.startapp.networkTest.speedtest.a unused = r11.b     // Catch:{ Exception -> 0x0304 }
                goto L_0x02f1
            L_0x02de:
                r0 = move-exception
                goto L_0x02e2
            L_0x02e0:
                r0 = move-exception
                r8 = r10
            L_0x02e2:
                r10 = 1
                goto L_0x0305
            L_0x02e4:
                r0 = move-exception
                r23 = r6
                r8 = r10
                goto L_0x032c
            L_0x02e9:
                r22 = r2
                r23 = r6
            L_0x02ed:
                r8 = r10
                r2 = r11
                r10 = 1
                r6 = -1
            L_0x02f1:
                com.startapp.networkTest.results.speedtest.MeasurementPointLatency r6 = r7.a(r13, r6)     // Catch:{ Exception -> 0x0304 }
                r4.add(r6)     // Catch:{ Exception -> 0x0304 }
                int r0 = r0 + 1
                r11 = r2
                r10 = r8
                r2 = r22
                r6 = r23
                r13 = r27
                goto L_0x025c
            L_0x0304:
                r0 = move-exception
            L_0x0305:
                r6 = r23
                goto L_0x0355
            L_0x0308:
                r0 = move-exception
                r23 = r6
                goto L_0x0329
            L_0x030c:
                r23 = r6
                r8 = r10
                r27 = r13
                r10 = 1
                r5.close()     // Catch:{ IOException -> 0x0316 }
                goto L_0x031b
            L_0x0316:
                r0 = move-exception
                r2 = r0
                r2.printStackTrace()
            L_0x031b:
                if (r1 == 0) goto L_0x0320
                r1.destroy()
            L_0x0320:
                r6 = r23
                goto L_0x037f
            L_0x0324:
                r0 = move-exception
                r23 = r6
            L_0x0327:
                r26 = r8
            L_0x0329:
                r8 = r10
                r27 = r13
            L_0x032c:
                r10 = 1
                goto L_0x0355
            L_0x032e:
                r0 = move-exception
                r26 = r8
                r8 = r10
                r27 = r13
                r10 = 1
                goto L_0x0354
            L_0x0336:
                r0 = move-exception
                r17 = r1
                r5 = 0
                goto L_0x0481
            L_0x033c:
                r0 = move-exception
                r26 = r8
                r8 = r10
                r27 = r13
                r10 = 1
                goto L_0x0353
            L_0x0344:
                r0 = move-exception
                r1 = r0
                r5 = 0
                r17 = 0
                goto L_0x0482
            L_0x034b:
                r0 = move-exception
                r26 = r8
                r8 = r10
                r27 = r13
                r10 = 1
                r1 = 0
            L_0x0353:
                r5 = 0
            L_0x0354:
                r6 = 0
            L_0x0355:
                java.lang.String r2 = com.startapp.networkTest.c.a.f5967a     // Catch:{ all -> 0x047e }
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ all -> 0x047e }
                java.lang.String r13 = "IcmpTestAsyncTask: "
                r11.<init>(r13)     // Catch:{ all -> 0x047e }
                java.lang.String r13 = r0.getMessage()     // Catch:{ all -> 0x047e }
                r11.append(r13)     // Catch:{ all -> 0x047e }
                java.lang.String r11 = r11.toString()     // Catch:{ all -> 0x047e }
                android.util.Log.e(r2, r11)     // Catch:{ all -> 0x047e }
                r0.printStackTrace()     // Catch:{ all -> 0x047e }
                if (r5 == 0) goto L_0x037a
                r5.close()     // Catch:{ IOException -> 0x0375 }
                goto L_0x037a
            L_0x0375:
                r0 = move-exception
                r2 = r0
                r2.printStackTrace()
            L_0x037a:
                if (r1 == 0) goto L_0x037f
                r1.destroy()
            L_0x037f:
                if (r6 == 0) goto L_0x03b3
                com.startapp.networkTest.enums.MeasurementTypes r0 = com.startapp.networkTest.enums.MeasurementTypes.APIIPING
                r3.MeasurementType = r0
                com.startapp.networkTest.c.a.a r0 = new com.startapp.networkTest.c.a.a
                int r1 = r7.c
                int r2 = r7.d
                int r5 = r7.e
                int r6 = r7.f
                r18 = r0
                r20 = r1
                r21 = r2
                r22 = r5
                r23 = r6
                r18.<init>(r19, r20, r21, r22, r23)
                com.startapp.networkTest.c.a$a$1 r11 = new com.startapp.networkTest.c.a$a$1
                r1 = r11
                r2 = r31
                r13 = r3
                r3 = r9
                r14 = r4
                r4 = r12
                r5 = r14
                r10 = r25
                r6 = r0
                r1.<init>(r3, r4, r5, r6)
                r0.a((com.startapp.networkTest.c.a.c) r11)
                r0.a()
                goto L_0x03b7
            L_0x03b3:
                r13 = r3
                r14 = r4
                r10 = r25
            L_0x03b7:
                com.startapp.networkTest.enums.SpeedtestEndStates r0 = com.startapp.networkTest.enums.SpeedtestEndStates.Finish
                r13.TestEndState = r0
                com.startapp.networkTest.speedtest.SpeedtestEngineError r0 = com.startapp.networkTest.speedtest.SpeedtestEngineError.OK
                r13.TestErrorReason = r0
                r1 = 0
                boolean r0 = r9[r1]
                r13.Success = r0
                r0 = r12[r1]
                r13.SuccessfulPings = r0
                int r0 = r14.size()
                if (r0 <= 0) goto L_0x03d6
                r13.a(r14)
                java.util.ArrayList<com.startapp.networkTest.results.speedtest.MeasurementPointLatency> r0 = r13.MeasurementPoints
                r13.b(r0)
            L_0x03d6:
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.a r0 = r0.g
                com.startapp.networkTest.data.BatteryInfo r0 = r0.a()
                r13.BatteryInfoOnEnd = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.LocationController r0 = r0.f
                com.startapp.networkTest.data.LocationInfo r0 = r0.b()
                r13.LocationInfoOnEnd = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                android.content.Context r0 = r0.c
                com.startapp.networkTest.enums.ScreenStates r0 = com.startapp.networkTest.controller.b.d(r0)
                r13.ScreenStateOnEnd = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                android.content.Context r0 = r0.c
                com.startapp.networkTest.data.b r0 = com.startapp.networkTest.controller.b.b(r0)
                r13.MemoryInfoOnEnd = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.c r0 = r0.d
                com.startapp.networkTest.data.RadioInfo r0 = r0.c()
                r13.RadioInfoOnEnd = r0
                com.startapp.networkTest.data.TimeInfo r0 = com.startapp.networkTest.e.b.a()
                r13.TimeInfoOnEnd = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.d r0 = r0.e
                com.startapp.networkTest.data.WifiInfo r0 = r0.a()
                r13.WifiInfoOnEnd = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                com.startapp.networkTest.controller.d r0 = r0.e
                com.startapp.networkTest.data.f r0 = com.startapp.networkTest.controller.b.a((com.startapp.networkTest.controller.d) r0)
                r13.TrafficInfoOnEnd = r0
                long r0 = android.os.SystemClock.uptimeMillis()
                long r0 = r0 - r15
                r13.DurationOverallNoSleep = r0
                long r0 = android.os.SystemClock.elapsedRealtime()
                long r0 = r0 - r27
                r13.DurationOverall = r0
                com.startapp.networkTest.enums.LtrCriteriaTypes r0 = r7.h
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = com.startapp.networkTest.enums.LtrCriteriaTypes.CTItem
                if (r0 != r1) goto L_0x044c
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                java.lang.String r0 = r0.s
                goto L_0x0452
            L_0x044c:
                java.lang.String r0 = r7.b
                java.lang.String r0 = com.startapp.networkTest.utils.f.a(r0)
            L_0x0452:
                r13.AirportCode = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                java.lang.String r0 = r0.m
                r13.Meta = r0
                com.startapp.networkTest.c.a r0 = com.startapp.networkTest.c.a.this
                java.lang.String r0 = r0.l
                java.lang.String r0 = com.startapp.networkTest.utils.h.a(r0)
                r13.QuestionnaireName = r0
                r2 = 0
                boolean r0 = r9[r2]
                if (r0 == 0) goto L_0x0475
                int r0 = r10.successfulTests
                r1 = 1
                int r0 = r0 + r1
                r10.successfulTests = r0
                r0 = r13
                goto L_0x0494
            L_0x0475:
                int r12 = r24 + 1
                r10 = r8
                r0 = r13
                r8 = r26
                r9 = 0
                goto L_0x0028
            L_0x047e:
                r0 = move-exception
                r17 = r1
            L_0x0481:
                r1 = r0
            L_0x0482:
                if (r5 == 0) goto L_0x048d
                r5.close()     // Catch:{ IOException -> 0x0488 }
                goto L_0x048d
            L_0x0488:
                r0 = move-exception
                r2 = r0
                r2.printStackTrace()
            L_0x048d:
                if (r17 == 0) goto L_0x0492
                r17.destroy()
            L_0x0492:
                throw r1
            L_0x0493:
                r8 = r10
            L_0x0494:
                com.startapp.networkTest.enums.LtrCriteriaTypes r1 = r7.h
                com.startapp.networkTest.enums.LtrCriteriaTypes r2 = com.startapp.networkTest.enums.LtrCriteriaTypes.CTItem
                if (r1 == r2) goto L_0x049d
                a((java.util.List<com.startapp.networkTest.d.a.d>) r8)
            L_0x049d:
                com.startapp.networkTest.a r1 = com.startapp.networkTest.c.d()
                boolean r1 = r1.B()
                if (r1 == 0) goto L_0x04b7
                if (r0 == 0) goto L_0x04b7
                com.startapp.networkTest.data.LocationInfo r1 = new com.startapp.networkTest.data.LocationInfo
                r1.<init>()
                r0.LocationInfoOnStart = r1
                com.startapp.networkTest.data.LocationInfo r1 = new com.startapp.networkTest.data.LocationInfo
                r1.<init>()
                r0.LocationInfoOnEnd = r1
            L_0x04b7:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.c.a.C0064a.a():com.startapp.networkTest.results.LatencyResult");
        }

        /* access modifiers changed from: private */
        public MeasurementPointLatency a(long j, int i2) {
            MeasurementPointLatency measurementPointLatency = new MeasurementPointLatency();
            measurementPointLatency.Delta = j;
            RadioInfo c2 = a.this.d.c();
            measurementPointLatency.ConnectionType = c2.ConnectionType;
            measurementPointLatency.NetworkType = c2.NetworkType;
            measurementPointLatency.NrAvailable = c2.NrAvailable;
            measurementPointLatency.NrState = c2.NrState;
            measurementPointLatency.RxLev = c2.RXLevel;
            measurementPointLatency.Rtt = i2;
            return measurementPointLatency;
        }

        private static void a(List<com.startapp.networkTest.d.a.d> list) {
            HashSet hashSet = new HashSet();
            for (com.startapp.networkTest.d.a.d dVar : list) {
                hashSet.add(dVar.toString());
            }
            com.startapp.networkTest.c.c().b((Set<String>) hashSet);
        }

        private List<com.startapp.networkTest.d.a.d> a(String[] strArr, LtrCriteriaTypes ltrCriteriaTypes, String str) {
            LinkedList linkedList = new LinkedList();
            LinkedList linkedList2 = new LinkedList();
            Set<String> g2 = com.startapp.networkTest.c.c().g();
            LinkedList<com.startapp.networkTest.d.a.d> linkedList3 = new LinkedList<>();
            if (g2 != null) {
                for (String a2 : g2) {
                    com.startapp.networkTest.d.a.d dVar = (com.startapp.networkTest.d.a.d) b.a(a2, com.startapp.networkTest.d.a.d.class);
                    if (dVar != null) {
                        linkedList3.add(dVar);
                    }
                }
            }
            for (String str2 : strArr) {
                com.startapp.networkTest.d.a.d dVar2 = new com.startapp.networkTest.d.a.d();
                dVar2.address = str2;
                linkedList2.add(dVar2);
            }
            for (com.startapp.networkTest.d.a.d dVar3 : linkedList3) {
                for (int i2 = 0; i2 < strArr.length; i2++) {
                    if (strArr[i2].equals(dVar3.address)) {
                        linkedList2.set(i2, dVar3);
                    }
                }
            }
            int i3 = AnonymousClass1.f5968a[ltrCriteriaTypes.ordinal()];
            if (i3 == 1) {
                com.startapp.networkTest.d.a.d dVar4 = new com.startapp.networkTest.d.a.d();
                dVar4.address = str;
                linkedList.add(dVar4);
                return linkedList;
            } else if (i3 == 2) {
                return linkedList2;
            } else {
                if (i3 == 3) {
                    Collections.shuffle(linkedList2, new Random(System.nanoTime()));
                    return new LinkedList(linkedList2);
                } else if (i3 == 4) {
                    Collections.sort(linkedList2, new Comparator<com.startapp.networkTest.d.a.d>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((com.startapp.networkTest.d.a.d) obj).successfulTests - ((com.startapp.networkTest.d.a.d) obj2).successfulTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                } else if (i3 != 5) {
                    return linkedList;
                } else {
                    Collections.sort(linkedList2, new Comparator<com.startapp.networkTest.d.a.d>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((com.startapp.networkTest.d.a.d) obj).totalTests - ((com.startapp.networkTest.d.a.d) obj2).totalTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                }
            }
        }
    }

    public final void b() {
        LocationController locationController = this.f;
        if (locationController != null) {
            locationController.a();
        }
        c cVar = this.d;
        if (cVar != null) {
            cVar.b();
        }
    }

    public final void c(String str) {
        this.l = str;
    }

    public final void d(String str) {
        this.i = new ArrayList<>();
        if (Build.VERSION.SDK_INT < 11) {
            new C0064a(str).execute(new Void[0]);
        } else {
            new C0064a(str).executeOnExecutor(com.startapp.networkTest.threads.a.a().b(), new Void[0]);
        }
    }

    public final P3TestResult a() {
        return this.h;
    }

    public final void a(LocationController.ProviderMode providerMode) {
        LocationController locationController = this.f;
        if (locationController != null) {
            locationController.a(providerMode);
        }
        c cVar = this.d;
        if (cVar != null) {
            cVar.a();
        }
    }

    public final void b(String str) {
        this.s = str;
    }

    public final void a(String str) {
        this.m = str;
    }
}
