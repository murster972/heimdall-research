package com.startapp.networkTest.c.a;

import java.nio.ByteBuffer;
import java.util.Random;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private final byte f5975a;

    b(byte b) {
        this.f5975a = b;
    }

    /* access modifiers changed from: package-private */
    public final ByteBuffer a(short s, short s2, byte[] bArr) {
        if (bArr == null) {
            bArr = new byte[0];
        } else if (bArr.length > 65507) {
            bArr = a(65507);
        }
        byte[] bArr2 = new byte[(bArr.length + 8)];
        ByteBuffer wrap = ByteBuffer.wrap(bArr2);
        wrap.put(this.f5975a);
        wrap.put((byte) 0);
        int position = wrap.position();
        wrap.position(position + 2);
        wrap.putShort(s2);
        wrap.putShort(s);
        wrap.put(bArr);
        int length = bArr2.length;
        int i = 0;
        for (int i2 = 0; i2 < length; i2 += 2) {
            int i3 = i + ((bArr2[i2] & 255) << 8);
            i = (i3 >> 16) + (65535 & i3);
        }
        for (int i4 = 1; i4 < length; i4 += 2) {
            int i5 = i + (bArr2[i4] & 255);
            i = (i5 >> 16) + (i5 & 65535);
        }
        wrap.putShort(position, (short) (((i & 65535) + (i >> 16)) ^ 65535));
        wrap.flip();
        return wrap;
    }

    static byte[] a(int i) {
        byte[] bArr = new byte[i];
        new Random().nextBytes(bArr);
        return bArr;
    }
}
