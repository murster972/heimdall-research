package com.startapp.networkTest.c.a;

import android.annotation.TargetApi;
import android.os.SystemClock;
import android.system.Os;
import android.system.OsConstants;
import android.system.StructPollfd;
import android.util.SparseArray;
import java.io.FileDescriptor;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Arrays;

@TargetApi(21)
public class a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5970a = "a";
    /* access modifiers changed from: private */
    public static final short b;
    private InetAddress c;
    /* access modifiers changed from: private */
    public c d;
    /* access modifiers changed from: private */
    public int e;
    private int f;
    /* access modifiers changed from: private */
    public int g;
    private b h;
    private short i = 1;
    private short j = 30583;
    /* access modifiers changed from: private */
    public boolean k = false;
    /* access modifiers changed from: private */
    public boolean l = false;
    private int m;
    /* access modifiers changed from: private */
    public int n;
    /* access modifiers changed from: private */
    public int o;
    /* access modifiers changed from: private */
    public long p;
    /* access modifiers changed from: private */
    public SparseArray<Long> q;

    /* renamed from: com.startapp.networkTest.c.a.a$a  reason: collision with other inner class name */
    class C0065a extends Thread {

        /* renamed from: a  reason: collision with root package name */
        private StructPollfd[] f5974a;

        C0065a(StructPollfd[] structPollfdArr) {
            this.f5974a = structPollfdArr;
        }

        public final void run() {
            StructPollfd structPollfd = this.f5974a[0];
            FileDescriptor fileDescriptor = structPollfd.fd;
            byte[] bArr = new byte[a.this.n];
            while (a.this.l && !a.this.k && a.this.o < a.this.g) {
                try {
                    int poll = Os.poll(this.f5974a, a.this.e);
                    if (!a.this.k) {
                        if (poll >= 0 && structPollfd.revents == a.b) {
                            structPollfd.revents = 0;
                            Os.recvfrom(fileDescriptor, bArr, 0, bArr.length, 64, (InetSocketAddress) null);
                            int hashCode = Arrays.hashCode(Arrays.copyOfRange(bArr, 8, bArr.length));
                            Long l = (Long) a.this.q.get(hashCode);
                            if (l != null) {
                                a.this.q.remove(hashCode);
                                long elapsedRealtime = SystemClock.elapsedRealtime() - l.longValue();
                                a.this.d.a(SystemClock.elapsedRealtime() - a.this.p, elapsedRealtime);
                                a.j(a.this);
                            }
                        }
                    } else {
                        return;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static {
        int i2 = OsConstants.POLLIN;
        if (i2 == 0) {
            i2 = 1;
        }
        b = (short) i2;
    }

    public a(InetAddress inetAddress, int i2, int i3, int i4, int i5) {
        this.c = inetAddress;
        this.e = i4;
        this.g = i2;
        this.f = i3;
        this.h = new b(inetAddress instanceof Inet6Address ? Byte.MIN_VALUE : 8);
        this.m = i5;
        this.n = i5 + 8;
        this.q = new SparseArray<>();
    }

    static /* synthetic */ int j(a aVar) {
        int i2 = aVar.o;
        aVar.o = i2 + 1;
        return i2;
    }

    public final void a(c cVar) {
        this.d = cVar;
    }

    public final void b() {
        this.k = true;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(2:31|32) */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        r12.d.a(android.os.SystemClock.elapsedRealtime() - r12.p, -1);
        r12.o++;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:31:0x00d3 */
    @android.annotation.TargetApi(21)
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r12 = this;
            r0 = 0
            r12.k = r0
            java.net.InetAddress r1 = r12.c
            boolean r1 = r1 instanceof java.net.Inet6Address
            if (r1 == 0) goto L_0x000e
            int r1 = android.system.OsConstants.AF_INET6
            int r2 = android.system.OsConstants.IPPROTO_ICMPV6
            goto L_0x0012
        L_0x000e:
            int r1 = android.system.OsConstants.AF_INET
            int r2 = android.system.OsConstants.IPPROTO_ICMP
        L_0x0012:
            long r3 = android.os.SystemClock.elapsedRealtime()
            r12.p = r3
            int r3 = android.system.OsConstants.SOCK_DGRAM     // Catch:{ Exception -> 0x0134 }
            java.io.FileDescriptor r1 = android.system.Os.socket(r1, r3, r2)     // Catch:{ Exception -> 0x0134 }
            boolean r2 = r1.valid()     // Catch:{ Exception -> 0x0134 }
            r3 = -1
            if (r2 == 0) goto L_0x011a
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0113 }
            r5 = 26
            r6 = 16
            r7 = 1
            if (r2 < r5) goto L_0x0037
            int r2 = android.system.OsConstants.IPPROTO_IP     // Catch:{ all -> 0x0113 }
            int r5 = android.system.OsConstants.IP_TOS     // Catch:{ all -> 0x0113 }
            android.system.Os.setsockoptInt(r1, r2, r5, r6)     // Catch:{ all -> 0x0113 }
            goto L_0x007b
        L_0x0037:
            java.lang.Class<android.system.Os> r2 = android.system.Os.class
            r5 = 4
            java.lang.Class[] r8 = new java.lang.Class[r5]     // Catch:{ Exception -> 0x0073 }
            java.lang.Class<java.io.FileDescriptor> r9 = java.io.FileDescriptor.class
            r8[r0] = r9     // Catch:{ Exception -> 0x0073 }
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0073 }
            r8[r7] = r9     // Catch:{ Exception -> 0x0073 }
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0073 }
            r10 = 2
            r8[r10] = r9     // Catch:{ Exception -> 0x0073 }
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x0073 }
            r11 = 3
            r8[r11] = r9     // Catch:{ Exception -> 0x0073 }
            java.lang.String r9 = "setsockoptInt"
            java.lang.reflect.Method r2 = r2.getMethod(r9, r8)     // Catch:{ Exception -> 0x0073 }
            r8 = 0
            java.lang.Object[] r5 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0073 }
            r5[r0] = r1     // Catch:{ Exception -> 0x0073 }
            int r9 = android.system.OsConstants.IPPROTO_IP     // Catch:{ Exception -> 0x0073 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0073 }
            r5[r7] = r9     // Catch:{ Exception -> 0x0073 }
            int r9 = android.system.OsConstants.IP_TOS     // Catch:{ Exception -> 0x0073 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0073 }
            r5[r10] = r9     // Catch:{ Exception -> 0x0073 }
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)     // Catch:{ Exception -> 0x0073 }
            r5[r11] = r6     // Catch:{ Exception -> 0x0073 }
            r2.invoke(r8, r5)     // Catch:{ Exception -> 0x0073 }
            goto L_0x007b
        L_0x0073:
            r2 = move-exception
            java.lang.String r5 = f5970a     // Catch:{ all -> 0x0113 }
            java.lang.String r6 = "setLowDelay: setsockoptInt"
            android.util.Log.e(r5, r6, r2)     // Catch:{ all -> 0x0113 }
        L_0x007b:
            android.system.StructPollfd r2 = new android.system.StructPollfd     // Catch:{ all -> 0x0113 }
            r2.<init>()     // Catch:{ all -> 0x0113 }
            r2.fd = r1     // Catch:{ all -> 0x0113 }
            short r5 = b     // Catch:{ all -> 0x0113 }
            r2.events = r5     // Catch:{ all -> 0x0113 }
            android.system.StructPollfd[] r5 = new android.system.StructPollfd[r7]     // Catch:{ all -> 0x0113 }
            r5[r0] = r2     // Catch:{ all -> 0x0113 }
            com.startapp.networkTest.c.a.a$a r2 = new com.startapp.networkTest.c.a.a$a     // Catch:{ all -> 0x0113 }
            r2.<init>(r5)     // Catch:{ all -> 0x0113 }
            r12.l = r7     // Catch:{ all -> 0x0113 }
            long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0113 }
            r12.p = r5     // Catch:{ all -> 0x0113 }
            r2.start()     // Catch:{ all -> 0x0113 }
            r5 = 0
        L_0x009b:
            int r6 = r12.g     // Catch:{ all -> 0x0113 }
            if (r5 >= r6) goto L_0x0102
            boolean r6 = r12.k     // Catch:{ all -> 0x0113 }
            if (r6 != 0) goto L_0x0102
            int r6 = r12.m     // Catch:{ all -> 0x0113 }
            byte[] r6 = com.startapp.networkTest.c.a.b.a(r6)     // Catch:{ all -> 0x0113 }
            com.startapp.networkTest.c.a.b r8 = r12.h     // Catch:{ all -> 0x0113 }
            short r9 = r12.i     // Catch:{ all -> 0x0113 }
            int r10 = r9 + 1
            short r10 = (short) r10     // Catch:{ all -> 0x0113 }
            r12.i = r10     // Catch:{ all -> 0x0113 }
            short r10 = r12.j     // Catch:{ all -> 0x0113 }
            java.nio.ByteBuffer r8 = r8.a(r9, r10, r6)     // Catch:{ all -> 0x0113 }
            int r6 = java.util.Arrays.hashCode(r6)     // Catch:{ Exception -> 0x00d3 }
            long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x00d3 }
            android.util.SparseArray<java.lang.Long> r11 = r12.q     // Catch:{ Exception -> 0x00d3 }
            java.lang.Long r9 = java.lang.Long.valueOf(r9)     // Catch:{ Exception -> 0x00d3 }
            r11.put(r6, r9)     // Catch:{ Exception -> 0x00d3 }
            java.net.InetAddress r6 = r12.c     // Catch:{ Exception -> 0x00d3 }
            r9 = 7
            int r6 = android.system.Os.sendto(r1, r8, r0, r6, r9)     // Catch:{ Exception -> 0x00d3 }
            if (r6 >= 0) goto L_0x00e4
            goto L_0x0102
        L_0x00d3:
            long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ all -> 0x0113 }
            long r10 = r12.p     // Catch:{ all -> 0x0113 }
            long r8 = r8 - r10
            com.startapp.networkTest.c.a.c r6 = r12.d     // Catch:{ all -> 0x0113 }
            r6.a(r8, r3)     // Catch:{ all -> 0x0113 }
            int r6 = r12.o     // Catch:{ all -> 0x0113 }
            int r6 = r6 + r7
            r12.o = r6     // Catch:{ all -> 0x0113 }
        L_0x00e4:
            int r6 = r12.g     // Catch:{ all -> 0x0113 }
            int r6 = r6 - r7
            if (r5 >= r6) goto L_0x00ff
            int r6 = r12.f     // Catch:{ Exception -> 0x00f0 }
            long r8 = (long) r6     // Catch:{ Exception -> 0x00f0 }
            java.lang.Thread.sleep(r8)     // Catch:{ Exception -> 0x00f0 }
            goto L_0x00ff
        L_0x00f0:
            r6 = move-exception
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x0113 }
            java.lang.String r9 = "Pause: "
            r8.<init>(r9)     // Catch:{ all -> 0x0113 }
            java.lang.String r6 = r6.toString()     // Catch:{ all -> 0x0113 }
            r8.append(r6)     // Catch:{ all -> 0x0113 }
        L_0x00ff:
            int r5 = r5 + 1
            goto L_0x009b
        L_0x0102:
            r12.l = r0     // Catch:{ all -> 0x0113 }
            boolean r5 = r2.isAlive()     // Catch:{ all -> 0x0113 }
            if (r5 == 0) goto L_0x010d
            r2.join()     // Catch:{ all -> 0x0113 }
        L_0x010d:
            android.system.Os.close(r1)     // Catch:{ Exception -> 0x0134 }
            r12.l = r0     // Catch:{ Exception -> 0x0134 }
            goto L_0x011a
        L_0x0113:
            r2 = move-exception
            android.system.Os.close(r1)     // Catch:{ Exception -> 0x0134 }
            r12.l = r0     // Catch:{ Exception -> 0x0134 }
            throw r2     // Catch:{ Exception -> 0x0134 }
        L_0x011a:
            boolean r0 = r12.k     // Catch:{ Exception -> 0x0134 }
            if (r0 != 0) goto L_0x0133
            int r0 = r12.o     // Catch:{ Exception -> 0x0134 }
        L_0x0120:
            int r1 = r12.g     // Catch:{ Exception -> 0x0134 }
            if (r0 >= r1) goto L_0x0133
            long r1 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0134 }
            long r5 = r12.p     // Catch:{ Exception -> 0x0134 }
            long r1 = r1 - r5
            com.startapp.networkTest.c.a.c r5 = r12.d     // Catch:{ Exception -> 0x0134 }
            r5.a(r1, r3)     // Catch:{ Exception -> 0x0134 }
            int r0 = r0 + 1
            goto L_0x0120
        L_0x0133:
            return
        L_0x0134:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "start: Os.socket: "
            r1.<init>(r2)
            java.lang.String r0 = r0.toString()
            r1.append(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.c.a.a.a():void");
    }
}
