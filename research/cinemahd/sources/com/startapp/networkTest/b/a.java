package com.startapp.networkTest.b;

import com.startapp.common.parser.b;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.IspInfo;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.net.WebApiClient;
import com.startapp.networkTest.utils.h;
import com.startapp.sdk.g.b.g;
import java.io.IOException;
import java.util.HashMap;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static a f5965a;
    private boolean b = false;
    private boolean c = false;
    private HashMap<String, IspInfo> d = new HashMap<>();
    private IspInfo e;

    private a() {
    }

    public static a a() {
        if (f5965a == null) {
            f5965a = new a();
        }
        return f5965a;
    }

    public final IspInfo a(WifiInfo wifiInfo) {
        b bVar;
        IspInfo ispInfo = new IspInfo();
        try {
            g a2 = WebApiClient.a(WebApiClient.RequestMethod.GET, c.d().r() + "ispinfo");
            if (a2.b.length() > 0 && (bVar = (b) b.a(a2.b, b.class)) != null) {
                ispInfo.AutonomousSystemNumber = h.a(bVar.AutonomousSystemNumber);
                ispInfo.AutonomousSystemOrganization = h.a(bVar.AutonomousSystemOrganization);
                ispInfo.IpAddress = h.a(bVar.IpAddress);
                ispInfo.IspName = h.a(bVar.IspName);
                ispInfo.IspOrganizationalName = h.a(bVar.IspOrganizationalName);
                ispInfo.SuccessfulIspLookup = true;
                if (wifiInfo != null) {
                    synchronized (this.d) {
                        this.d.put(wifiInfo.WifiBSSID_Full, ispInfo);
                    }
                } else {
                    this.e = ispInfo;
                }
            }
        } catch (IOException e2) {
            new StringBuilder("getIspInfo: ").append(e2.getMessage());
        }
        return ispInfo;
    }
}
