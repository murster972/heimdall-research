package com.startapp.networkTest.enums;

public enum ServiceStates {
    Unknown,
    EmergencyOnly,
    InService,
    OutOfService,
    PowerOff
}
