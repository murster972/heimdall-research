package com.startapp.networkTest.enums;

public enum Os {
    Android,
    iOS,
    WindowsPhone
}
