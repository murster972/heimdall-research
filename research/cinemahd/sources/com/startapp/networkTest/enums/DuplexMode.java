package com.startapp.networkTest.enums;

public enum DuplexMode {
    Unknown,
    FDD,
    TDD
}
