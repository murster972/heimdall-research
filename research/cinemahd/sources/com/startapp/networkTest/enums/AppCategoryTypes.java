package com.startapp.networkTest.enums;

public enum AppCategoryTypes {
    Audio,
    Game,
    Image,
    Maps,
    News,
    Productivity,
    Social,
    Video,
    Undefined,
    Unknown
}
