package com.startapp.networkTest.enums.bluetooth;

public enum BluetoothTypes {
    Classic,
    LowEnergy,
    DualMode,
    Unknown
}
