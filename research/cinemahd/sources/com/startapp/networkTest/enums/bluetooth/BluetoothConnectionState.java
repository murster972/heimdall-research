package com.startapp.networkTest.enums.bluetooth;

public enum BluetoothConnectionState {
    Disconnected,
    Connecting,
    Connected,
    Disconnecting,
    Unknown
}
