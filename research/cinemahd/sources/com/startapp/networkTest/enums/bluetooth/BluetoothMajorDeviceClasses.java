package com.startapp.networkTest.enums.bluetooth;

public enum BluetoothMajorDeviceClasses {
    AudioVideo,
    Computer,
    Health,
    Imaging,
    Misc,
    g,
    Peripheral,
    Phone,
    Toy,
    Uncategorized,
    Wearable,
    Unknown
}
