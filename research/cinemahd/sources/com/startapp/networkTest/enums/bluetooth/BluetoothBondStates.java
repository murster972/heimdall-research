package com.startapp.networkTest.enums.bluetooth;

public enum BluetoothBondStates {
    None,
    Bonding,
    Bonded,
    Unknown
}
