package com.startapp.networkTest.enums;

public enum PhoneTypes {
    GSM,
    CDMA,
    SIP,
    None,
    Unknown
}
