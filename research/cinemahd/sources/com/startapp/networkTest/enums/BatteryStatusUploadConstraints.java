package com.startapp.networkTest.enums;

public enum BatteryStatusUploadConstraints {
    Charging,
    FullOrCharging,
    Always
}
