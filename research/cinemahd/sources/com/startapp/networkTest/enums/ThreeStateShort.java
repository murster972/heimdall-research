package com.startapp.networkTest.enums;

public enum ThreeStateShort {
    Yes,
    No,
    Unknown
}
