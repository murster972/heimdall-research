package com.startapp.networkTest.enums;

public enum ConnectionTypes {
    Unknown,
    Bluetooth,
    Ethernet,
    Mobile,
    WiFi,
    WiMAX
}
