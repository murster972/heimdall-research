package com.startapp.networkTest.enums;

public enum ScreenStates {
    Unknown,
    On,
    Off
}
