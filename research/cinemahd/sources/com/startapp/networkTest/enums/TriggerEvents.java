package com.startapp.networkTest.enums;

public enum TriggerEvents {
    Unknown,
    PeriodicExternal,
    PeriodicPushNotification,
    PeriodicNetworkFeedback,
    PeriodicBackgroundService,
    PeriodicVoiceCall,
    LocationUpdateGps,
    LocationUpdateNetwork,
    Manual,
    Automatic,
    OutOfService,
    CellIdChange
}
