package com.startapp.networkTest.enums;

public enum HotspotStates {
    Unknown,
    Failed,
    Disabled,
    Disabling,
    Enabled,
    Enabling
}
