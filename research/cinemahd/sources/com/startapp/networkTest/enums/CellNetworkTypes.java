package com.startapp.networkTest.enums;

public enum CellNetworkTypes {
    Gsm,
    Wcdma,
    Lte,
    Nr,
    Cdma,
    Unknown
}
