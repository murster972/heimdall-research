package com.startapp.networkTest.enums;

public enum BatteryHealthStates {
    Cold,
    Dead,
    Good,
    OverVoltage,
    Overheat,
    Unknown
}
