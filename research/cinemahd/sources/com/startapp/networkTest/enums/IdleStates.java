package com.startapp.networkTest.enums;

public enum IdleStates {
    Unknown,
    DeepIdle,
    LightIdle,
    NonIdle
}
