package com.startapp.networkTest.enums.wifi;

public enum WifiPairwiseCiphers {
    Unknown,
    CCMP,
    NONE,
    TKIP
}
