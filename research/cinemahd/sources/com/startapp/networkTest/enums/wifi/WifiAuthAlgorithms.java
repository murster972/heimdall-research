package com.startapp.networkTest.enums.wifi;

public enum WifiAuthAlgorithms {
    Unknown,
    LEAP,
    OPEN,
    SHARED
}
