package com.startapp.networkTest.enums.wifi;

public enum WifiProtocols {
    Unknown,
    RSN,
    WPA
}
