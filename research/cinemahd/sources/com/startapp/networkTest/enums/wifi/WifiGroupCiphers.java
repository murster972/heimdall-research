package com.startapp.networkTest.enums.wifi;

public enum WifiGroupCiphers {
    Unknown,
    CCMP,
    TKIP,
    WEP104,
    WEP40
}
