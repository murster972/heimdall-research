package com.startapp.networkTest.enums.wifi;

public enum WifiKeyManagements {
    Unknown,
    IEEE8021X,
    NONE,
    WPA_EAP,
    WPA_PSK
}
