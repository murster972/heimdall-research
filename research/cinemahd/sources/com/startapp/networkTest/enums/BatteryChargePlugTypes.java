package com.startapp.networkTest.enums;

public enum BatteryChargePlugTypes {
    AC,
    USB,
    Unknown,
    Wireless
}
