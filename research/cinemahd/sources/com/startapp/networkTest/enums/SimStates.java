package com.startapp.networkTest.enums;

public enum SimStates {
    Unknown,
    Absent,
    NetworkLocked,
    PinRequired,
    PukRequired,
    Ready,
    NotReady,
    PermanentlyDisabled,
    CardIoError,
    CardRestricted
}
