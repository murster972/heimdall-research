package com.startapp.networkTest.enums;

public enum TrafficDetectionMode {
    Auto,
    Legacy
}
