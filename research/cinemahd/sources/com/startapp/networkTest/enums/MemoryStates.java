package com.startapp.networkTest.enums;

public enum MemoryStates {
    Unknown,
    Normal,
    Low
}
