package com.startapp.networkTest.enums;

public enum MultiSimVariants {
    DSDS,
    DSDA,
    TSTS,
    Unknown
}
