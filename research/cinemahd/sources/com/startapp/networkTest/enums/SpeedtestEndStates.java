package com.startapp.networkTest.enums;

public enum SpeedtestEndStates {
    Unknown,
    ConnectingToControlServer,
    ConnectedToControlServer,
    ConnectingToTestServer,
    ConnectedToTestServer,
    LatencyTestStart,
    LatencyTestEnd,
    DownloadTestStart,
    DownloadTestEnd,
    UploadTestStart,
    UploadTestEnd,
    TracerouteTestStart,
    TracerouteTestEnd,
    TestStart,
    TestEnd,
    Finish
}
