package com.startapp.networkTest.enums;

public enum CtTestTypes {
    SSLOwnTs,
    SSLDeviceTs,
    SSLTrustAll,
    Unknown
}
