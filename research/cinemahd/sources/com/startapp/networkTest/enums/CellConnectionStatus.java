package com.startapp.networkTest.enums;

public enum CellConnectionStatus {
    None,
    Primary,
    Secondary,
    Unknown
}
