package com.startapp.networkTest.enums;

public enum LtrCriteriaTypes {
    TotalTests,
    FullSuccessful,
    Random,
    NoChange,
    CTItem
}
