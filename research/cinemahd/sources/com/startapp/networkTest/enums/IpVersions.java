package com.startapp.networkTest.enums;

public enum IpVersions {
    Unknown,
    IPv4,
    IPv6
}
