package com.startapp.networkTest.enums;

public enum TimeSources {
    NTP,
    GPS,
    Device,
    Unknown
}
