package com.startapp.networkTest.enums;

public enum NetworkGenerations {
    Gen2,
    Gen3,
    Gen4,
    Gen5,
    Unknown
}
