package com.startapp.networkTest.enums;

public enum NetworkTypes {
    Unknown,
    Cdma1xRTT,
    CDMA,
    EDGE,
    EVDO_0,
    EVDO_A,
    EVDO_B,
    EHRPD,
    GPRS,
    HSPA,
    HSDPA,
    HSPAP,
    HSUPA,
    IDEN,
    LTE,
    LTE_CA,
    NR,
    UMTS,
    GSM,
    TD_SCDMA,
    WiFi
}
