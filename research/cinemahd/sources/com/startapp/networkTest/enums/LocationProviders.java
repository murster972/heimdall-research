package com.startapp.networkTest.enums;

public enum LocationProviders {
    Unknown,
    Gps,
    Network,
    Fused,
    RailNet
}
