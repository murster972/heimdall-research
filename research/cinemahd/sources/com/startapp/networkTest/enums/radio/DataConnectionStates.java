package com.startapp.networkTest.enums.radio;

public enum DataConnectionStates {
    Disconnected,
    Suspended,
    Connecting,
    Connected,
    Unknown,
    Disabled
}
