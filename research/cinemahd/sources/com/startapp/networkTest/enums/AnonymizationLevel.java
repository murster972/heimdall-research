package com.startapp.networkTest.enums;

public enum AnonymizationLevel {
    Full,
    Anonymized,
    None
}
