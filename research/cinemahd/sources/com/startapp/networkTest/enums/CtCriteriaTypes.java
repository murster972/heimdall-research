package com.startapp.networkTest.enums;

public enum CtCriteriaTypes {
    TotalTests,
    DNSSuccessful,
    TCPSuccessful,
    FullSuccessful,
    Random,
    NoChange
}
