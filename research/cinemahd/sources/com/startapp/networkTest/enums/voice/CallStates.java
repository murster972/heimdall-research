package com.startapp.networkTest.enums.voice;

public enum CallStates {
    Offhook,
    Ringing,
    Idle,
    Unknown
}
