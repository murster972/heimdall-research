package com.startapp.networkTest.enums;

public enum ThreeState {
    Unknown,
    Enabled,
    Disabled
}
