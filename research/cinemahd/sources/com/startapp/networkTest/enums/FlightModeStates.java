package com.startapp.networkTest.enums;

public enum FlightModeStates {
    Unknown,
    Enabled,
    Disabled
}
