package com.startapp.networkTest.enums;

public enum WifiStates {
    Unknown,
    Disabled,
    Disabling,
    Enabled,
    Enabling
}
