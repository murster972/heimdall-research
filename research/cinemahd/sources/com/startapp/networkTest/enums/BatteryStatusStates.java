package com.startapp.networkTest.enums;

public enum BatteryStatusStates {
    Charging,
    Full,
    Unknown,
    Discharging,
    NotCharging
}
