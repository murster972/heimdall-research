package com.startapp.networkTest.d.a;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import com.facebook.common.util.ByteConstants;
import com.startapp.networkTest.controller.LocationController;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.controller.d;
import com.startapp.networkTest.enums.CtCriteriaTypes;
import com.startapp.networkTest.results.ConnectivityTestResult;
import com.startapp.networkTest.results.LatencyResult;
import com.startapp.networkTest.speedtest.SpeedtestEngineStatus;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f6006a;
    /* access modifiers changed from: private */
    public c b;
    /* access modifiers changed from: private */
    public d c;
    /* access modifiers changed from: private */
    public LocationController d;
    /* access modifiers changed from: private */
    public com.startapp.networkTest.d e;
    /* access modifiers changed from: private */
    public f f;
    /* access modifiers changed from: private */
    public String g;
    /* access modifiers changed from: private */
    public String h;
    /* access modifiers changed from: private */
    public String i;
    /* access modifiers changed from: private */
    public String j;
    /* access modifiers changed from: private */
    public Random k = new Random();
    /* access modifiers changed from: private */
    public float l;
    /* access modifiers changed from: private */
    public boolean m;

    /* renamed from: com.startapp.networkTest.d.a.b$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6007a = new int[CtCriteriaTypes.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.networkTest.enums.CtCriteriaTypes[] r0 = com.startapp.networkTest.enums.CtCriteriaTypes.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6007a = r0
                int[] r0 = f6007a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.enums.CtCriteriaTypes r1 = com.startapp.networkTest.enums.CtCriteriaTypes.NoChange     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6007a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.enums.CtCriteriaTypes r1 = com.startapp.networkTest.enums.CtCriteriaTypes.Random     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6007a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.enums.CtCriteriaTypes r1 = com.startapp.networkTest.enums.CtCriteriaTypes.DNSSuccessful     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6007a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.networkTest.enums.CtCriteriaTypes r1 = com.startapp.networkTest.enums.CtCriteriaTypes.TCPSuccessful     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6007a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.networkTest.enums.CtCriteriaTypes r1 = com.startapp.networkTest.enums.CtCriteriaTypes.FullSuccessful     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f6007a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.startapp.networkTest.enums.CtCriteriaTypes r1 = com.startapp.networkTest.enums.CtCriteriaTypes.TotalTests     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.d.a.b.AnonymousClass1.<clinit>():void");
        }
    }

    public b(Context context) {
        this.f6006a = context;
        this.e = new com.startapp.networkTest.d(context);
        com.startapp.networkTest.a d2 = com.startapp.networkTest.c.d();
        this.g = d2.a();
        this.h = d2.d();
        this.i = d2.e();
        this.j = d2.f();
        this.l = d2.h();
        this.m = d2.g();
        this.d = new LocationController(context);
        this.b = new c(context);
        this.c = new d(context);
    }

    public final void a() {
        this.d.a(LocationController.ProviderMode.Passive);
        this.b.a();
    }

    public final void b() {
        this.d.a();
        this.b.b();
    }

    public final void a(f fVar) {
        this.f = fVar;
        if (Build.VERSION.SDK_INT < 11) {
            new a().execute(new Void[0]);
        } else {
            new a().executeOnExecutor(com.startapp.networkTest.threads.a.a().b(), new Void[0]);
        }
    }

    class a extends AsyncTask<Void, String, ConnectivityTestResult> implements com.startapp.networkTest.speedtest.a {

        /* renamed from: a  reason: collision with root package name */
        private ConnectivityTestResult f6008a;
        private com.startapp.networkTest.c.a b;

        /* renamed from: com.startapp.networkTest.d.a.b$a$a  reason: collision with other inner class name */
        class C0068a {

            /* renamed from: a  reason: collision with root package name */
            final int f6013a;
            final String b;
            final boolean c;

            public C0068a(int i, String str, boolean z) {
                this.f6013a = i;
                this.b = str;
                this.c = z;
            }
        }

        a() {
        }

        private C0068a a(InputStream inputStream) throws IOException {
            boolean z;
            byte[] bArr = new byte[ByteConstants.KB];
            int i = 0;
            int i2 = 0;
            while (true) {
                int read = inputStream.read();
                z = true;
                i++;
                if (read == 10) {
                    z = false;
                    break;
                } else if (read < 0) {
                    break;
                } else {
                    int i3 = i2 + 1;
                    bArr[i2] = (byte) read;
                    if (i3 == bArr.length) {
                        bArr = Arrays.copyOf(bArr, i3 + ByteConstants.KB);
                    }
                    i2 = i3;
                }
            }
            if (i2 > 0 && bArr[i2 - 1] == 13) {
                i2--;
            }
            return new C0068a(i, new String(bArr, 0, i2, "UTF-8"), z);
        }

        /* JADX WARNING: Removed duplicated region for block: B:26:0x004f A[SYNTHETIC, Splitter:B:26:0x004f] */
        /* JADX WARNING: Removed duplicated region for block: B:31:0x005b A[SYNTHETIC, Splitter:B:31:0x005b] */
        /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private static boolean b() {
            /*
                java.lang.String r0 = "ping -W 3 -c 1 -s 56 127.0.0.1"
                r1 = 0
                java.lang.Runtime r2 = java.lang.Runtime.getRuntime()     // Catch:{ IOException -> 0x0049 }
                java.lang.Process r0 = r2.exec(r0)     // Catch:{ IOException -> 0x0049 }
                java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0049 }
                java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0049 }
                java.io.InputStream r0 = r0.getInputStream()     // Catch:{ IOException -> 0x0049 }
                r3.<init>(r0)     // Catch:{ IOException -> 0x0049 }
                r2.<init>(r3)     // Catch:{ IOException -> 0x0049 }
                r2.readLine()     // Catch:{ IOException -> 0x0044, all -> 0x0041 }
                java.lang.String r0 = r2.readLine()     // Catch:{ IOException -> 0x0044, all -> 0x0041 }
                if (r0 == 0) goto L_0x003d
                int r1 = r0.length()     // Catch:{ IOException -> 0x0044, all -> 0x0041 }
                if (r1 <= 0) goto L_0x003d
                java.lang.String r1 = " "
                java.lang.String[] r0 = r0.split(r1)     // Catch:{ IOException -> 0x0044, all -> 0x0041 }
                int r0 = r0.length     // Catch:{ IOException -> 0x0044, all -> 0x0041 }
                r1 = 8
                if (r0 != r1) goto L_0x003d
                r2.close()     // Catch:{ IOException -> 0x0037 }
                goto L_0x003b
            L_0x0037:
                r0 = move-exception
                r0.printStackTrace()
            L_0x003b:
                r0 = 1
                return r0
            L_0x003d:
                r2.close()     // Catch:{ IOException -> 0x0053 }
                goto L_0x0057
            L_0x0041:
                r0 = move-exception
                r1 = r2
                goto L_0x0059
            L_0x0044:
                r0 = move-exception
                r1 = r2
                goto L_0x004a
            L_0x0047:
                r0 = move-exception
                goto L_0x0059
            L_0x0049:
                r0 = move-exception
            L_0x004a:
                r0.printStackTrace()     // Catch:{ all -> 0x0047 }
                if (r1 == 0) goto L_0x0057
                r1.close()     // Catch:{ IOException -> 0x0053 }
                goto L_0x0057
            L_0x0053:
                r0 = move-exception
                r0.printStackTrace()
            L_0x0057:
                r0 = 0
                return r0
            L_0x0059:
                if (r1 == 0) goto L_0x0063
                r1.close()     // Catch:{ IOException -> 0x005f }
                goto L_0x0063
            L_0x005f:
                r1 = move-exception
                r1.printStackTrace()
            L_0x0063:
                throw r0
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.d.a.b.a.b():boolean");
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            ConnectivityTestResult connectivityTestResult = (ConnectivityTestResult) obj;
            b.this.e.a(SystemClock.elapsedRealtime());
            if (b.this.f != null) {
                b.this.f.a(connectivityTestResult);
            }
            if (connectivityTestResult != null) {
                boolean z = false;
                if (b.this.e.b() && connectivityTestResult.ServerIp.length() > 0) {
                    this.b = new com.startapp.networkTest.c.a(this, b.this.f6006a);
                    this.b.c(connectivityTestResult.CtId);
                    this.b.b(connectivityTestResult.AirportCode);
                    this.b.a(String.valueOf(connectivityTestResult.TimeInfo.TimestampMillis + connectivityTestResult.DurationDNS + connectivityTestResult.DurationTcpConnect + connectivityTestResult.DurationHttpReceive));
                    this.b.a(com.startapp.networkTest.c.d().o());
                    this.b.d(connectivityTestResult.ServerIp);
                    z = true;
                }
                if (!z && b.this.f != null) {
                    b.this.f.a();
                }
            } else if (b.this.f != null) {
                b.this.f.a();
            }
        }

        /* JADX WARNING: type inference failed for: r25v1, types: [javax.net.SocketFactory] */
        /* JADX WARNING: Code restructure failed: missing block: B:101:0x03ae, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:102:0x03af, code lost:
            r1 = r0;
            r8 = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:104:0x03b5, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:105:0x03b6, code lost:
            r28 = r13;
            r27 = r3;
            r26 = r8;
            r8 = r9;
            r4 = r17;
            r11 = r23;
            r17 = r24;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:165:0x04e7, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:166:0x04e9, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:167:0x04eb, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:168:0x04ec, code lost:
            r2 = r0;
            r31 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:169:0x04f1, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:170:0x04f2, code lost:
            r24 = r9;
            r27 = r12;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:171:0x04f6, code lost:
            r22 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:172:0x04f8, code lost:
            r3 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:174:?, code lost:
            r9 = new java.lang.StringBuilder();
            r10 = r1.f6008a;
            r9.append(r10.SslException);
            r9.append("SNI not available:");
            r9.append(r3.getMessage());
            r9.append(r7);
            r10.SslException = r9.toString();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:179:0x055c, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:180:0x055d, code lost:
            r3 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:182:?, code lost:
            r9 = new java.lang.StringBuilder();
            r10 = r1.f6008a;
            r9.append(r10.SslException);
            r9.append("Cannot validate hostname: ");
            r9.append(r3.getMessage());
            r9.append(r7);
            r10.SslException = r9.toString();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:194:0x05c7, code lost:
            r0 = move-exception;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:195:0x05c8, code lost:
            r2 = r0;
            r6 = r3;
            r31 = r4;
            r34 = r7;
            r32 = r8;
            r33 = r13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:294:0x07b8, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:295:0x07b9, code lost:
            r30 = r3;
            r31 = r4;
            r34 = r7;
            r32 = r8;
            r33 = r13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:299:0x07d8, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:300:0x07d9, code lost:
            r31 = r4;
            r34 = r7;
            r32 = r8;
            r33 = r13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:301:0x07e2, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:302:0x07e3, code lost:
            r31 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:304:0x07e7, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:305:0x07e8, code lost:
            r31 = r4;
            r34 = r7;
            r32 = r8;
            r24 = r9;
            r26 = r11;
            r27 = r12;
            r33 = r13;
            r22 = false;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:306:0x07f8, code lost:
            r2 = r0;
            r6 = 0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:311:?, code lost:
            r31.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:316:?, code lost:
            r31.close();
         */
        /* JADX WARNING: Code restructure failed: missing block: B:340:0x0918, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:341:0x091a, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:342:0x091b, code lost:
            r10 = r8;
            r16 = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:344:0x0933, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:345:0x0934, code lost:
            r10 = r8;
            r26 = r11;
            r8 = r13;
            r11 = r23;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:346:0x093d, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:347:0x093e, code lost:
            r29 = r10;
            r26 = r11;
            r11 = r23;
            r10 = r8;
            r8 = r13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:348:0x0948, code lost:
            r16 = r10;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:356:0x0970, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:367:0x0995, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:368:0x0996, code lost:
            r26 = r11;
            r8 = r13;
            r11 = r23;
            r27 = r3;
            r10 = "";
         */
        /* JADX WARNING: Code restructure failed: missing block: B:369:0x09a1, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:370:0x09a2, code lost:
            r8 = r13;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:376:0x09c4, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:377:0x09c5, code lost:
            r17 = r4;
            r26 = r8;
            r8 = r9;
            r28 = r13;
            r11 = r23;
            r27 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:379:0x09d5, code lost:
            r0 = th;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:380:0x09d6, code lost:
            r8 = r9;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:381:0x09d7, code lost:
            r1 = r0;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:383:0x09dd, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:384:0x09de, code lost:
            r26 = r8;
            r8 = r9;
            r28 = r13;
            r11 = r23;
            r27 = r3;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:390:0x0a0f, code lost:
            r8.add(r5);
         */
        /* JADX WARNING: Code restructure failed: missing block: B:93:0x038c, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:94:0x038e, code lost:
            r0 = e;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:95:0x038f, code lost:
            r29 = r4;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:96:0x0391, code lost:
            r27 = r3;
            r26 = r8;
            r8 = r9;
         */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [B:152:0x04a2, B:157:0x04c4] */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [B:152:0x04a2, B:160:0x04cf] */
        /* JADX WARNING: Exception block dominator not found, dom blocks: [B:152:0x04a2, B:163:0x04d5] */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Multi-variable type inference failed */
        /* JADX WARNING: Removed duplicated region for block: B:101:0x03ae A[ExcHandler: all (r0v65 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:75:0x032b] */
        /* JADX WARNING: Removed duplicated region for block: B:167:0x04eb A[ExcHandler: all (r0v23 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:152:0x04a2] */
        /* JADX WARNING: Removed duplicated region for block: B:178:0x052a A[Catch:{ Exception -> 0x055c, all -> 0x04eb }] */
        /* JADX WARNING: Removed duplicated region for block: B:186:0x058c  */
        /* JADX WARNING: Removed duplicated region for block: B:187:0x058e  */
        /* JADX WARNING: Removed duplicated region for block: B:191:0x05a3 A[SYNTHETIC, Splitter:B:191:0x05a3] */
        /* JADX WARNING: Removed duplicated region for block: B:214:0x066a  */
        /* JADX WARNING: Removed duplicated region for block: B:223:0x06b6  */
        /* JADX WARNING: Removed duplicated region for block: B:247:0x0725  */
        /* JADX WARNING: Removed duplicated region for block: B:248:0x072d  */
        /* JADX WARNING: Removed duplicated region for block: B:257:0x0747  */
        /* JADX WARNING: Removed duplicated region for block: B:262:0x0754 A[SYNTHETIC, Splitter:B:262:0x0754] */
        /* JADX WARNING: Removed duplicated region for block: B:268:0x076b A[SYNTHETIC, Splitter:B:268:0x076b] */
        /* JADX WARNING: Removed duplicated region for block: B:271:0x077a A[SYNTHETIC, Splitter:B:271:0x077a] */
        /* JADX WARNING: Removed duplicated region for block: B:290:0x07a8 A[SYNTHETIC, Splitter:B:290:0x07a8] */
        /* JADX WARNING: Removed duplicated region for block: B:301:0x07e2 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:148:0x0494] */
        /* JADX WARNING: Removed duplicated region for block: B:310:0x081e A[SYNTHETIC, Splitter:B:310:0x081e] */
        /* JADX WARNING: Removed duplicated region for block: B:315:0x0828 A[SYNTHETIC, Splitter:B:315:0x0828] */
        /* JADX WARNING: Removed duplicated region for block: B:321:0x083b  */
        /* JADX WARNING: Removed duplicated region for block: B:331:0x08ef  */
        /* JADX WARNING: Removed duplicated region for block: B:334:0x08ff  */
        /* JADX WARNING: Removed duplicated region for block: B:337:0x090c  */
        /* JADX WARNING: Removed duplicated region for block: B:34:0x00d0  */
        /* JADX WARNING: Removed duplicated region for block: B:356:0x0970 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:131:0x042c] */
        /* JADX WARNING: Removed duplicated region for block: B:369:0x09a1 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:109:0x03cc] */
        /* JADX WARNING: Removed duplicated region for block: B:379:0x09d5 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:66:0x0308] */
        /* JADX WARNING: Removed duplicated region for block: B:390:0x0a0f  */
        /* JADX WARNING: Removed duplicated region for block: B:395:0x0a26  */
        /* JADX WARNING: Removed duplicated region for block: B:397:0x0a2a  */
        /* JADX WARNING: Removed duplicated region for block: B:403:0x0a12 A[SYNTHETIC] */
        /* JADX WARNING: Removed duplicated region for block: B:409:0x073a A[SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private com.startapp.networkTest.results.ConnectivityTestResult a() {
            /*
                r35 = this;
                r1 = r35
                java.lang.String r2 = "\r\n"
                com.startapp.networkTest.controller.a r3 = new com.startapp.networkTest.controller.a
                com.startapp.networkTest.d.a.b r4 = com.startapp.networkTest.d.a.b.this
                android.content.Context r4 = r4.f6006a
                r3.<init>(r4)
                com.startapp.networkTest.data.BatteryInfo r3 = r3.a()
                com.startapp.networkTest.d.a.b r4 = com.startapp.networkTest.d.a.b.this
                float r4 = r4.l
                r5 = 0
                r6 = -1082130432(0xffffffffbf800000, float:-1.0)
                int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r4 == 0) goto L_0x002d
                float r4 = r3.BatteryLevel
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                float r6 = r6.l
                int r4 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
                if (r4 >= 0) goto L_0x002d
                return r5
            L_0x002d:
                com.startapp.networkTest.d.a.b r4 = com.startapp.networkTest.d.a.b.this
                android.content.Context r4 = r4.f6006a
                com.startapp.networkTest.data.a.b r4 = com.startapp.networkTest.controller.b.f(r4)
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                boolean r6 = r6.m
                if (r6 != 0) goto L_0x005c
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r6 = r6.b
                com.startapp.networkTest.enums.ConnectionTypes r6 = r6.f()
                com.startapp.networkTest.enums.ConnectionTypes r7 = com.startapp.networkTest.enums.ConnectionTypes.Mobile
                if (r6 != r7) goto L_0x005c
                com.startapp.networkTest.d.a.b r6 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r6 = r6.b
                int r7 = r4.SubscriptionId
                boolean r6 = r6.c((int) r7)
                if (r6 == 0) goto L_0x005c
                return r5
            L_0x005c:
                com.startapp.networkTest.d r7 = com.startapp.networkTest.c.c()     // Catch:{ Exception -> 0x0089 }
                long r7 = r7.d()     // Catch:{ Exception -> 0x0089 }
                long r9 = com.startapp.networkTest.e.b.b()     // Catch:{ Exception -> 0x0089 }
                com.startapp.networkTest.a r11 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x0089 }
                long r11 = r11.k()     // Catch:{ Exception -> 0x0089 }
                long r11 = r11 + r7
                int r13 = (r11 > r9 ? 1 : (r11 == r9 ? 0 : -1))
                if (r13 < 0) goto L_0x007c
                int r11 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
                if (r11 <= 0) goto L_0x007a
                goto L_0x007c
            L_0x007a:
                r7 = 0
                goto L_0x0086
            L_0x007c:
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this     // Catch:{ Exception -> 0x0089 }
                android.content.Context r7 = r7.f6006a     // Catch:{ Exception -> 0x0089 }
                boolean r7 = com.startapp.networkTest.utils.j.a(r7)     // Catch:{ Exception -> 0x0089 }
            L_0x0086:
                r8 = r7
                r7 = r5
                goto L_0x009e
            L_0x0089:
                r0 = move-exception
                r7 = r0
                java.lang.StringBuilder r8 = new java.lang.StringBuilder
                java.lang.String r9 = "checkAndLoadTruststore: "
                r8.<init>(r9)
                java.lang.String r7 = r7.toString()
                r8.append(r7)
                java.lang.String r7 = r8.toString()
                r8 = 0
            L_0x009e:
                com.startapp.networkTest.d r9 = com.startapp.networkTest.c.c()     // Catch:{ Exception -> 0x00bf }
                long r9 = r9.i()     // Catch:{ Exception -> 0x00bf }
                long r11 = com.startapp.networkTest.e.b.b()     // Catch:{ Exception -> 0x00bf }
                com.startapp.networkTest.a r13 = com.startapp.networkTest.c.d()     // Catch:{ Exception -> 0x00bf }
                long r13 = r13.l()     // Catch:{ Exception -> 0x00bf }
                long r13 = r13 + r9
                int r15 = (r13 > r11 ? 1 : (r13 == r11 ? 0 : -1))
                if (r15 < 0) goto L_0x00bb
                int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
                if (r13 <= 0) goto L_0x00c0
            L_0x00bb:
                com.startapp.networkTest.utils.b.a()     // Catch:{ Exception -> 0x00bf }
                goto L_0x00c0
            L_0x00bf:
            L_0x00c0:
                boolean r9 = com.startapp.networkTest.c.a()
                if (r9 == 0) goto L_0x0a2a
                com.startapp.networkTest.d.a.b r9 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.d r9 = r9.c
                if (r9 != 0) goto L_0x00d0
                goto L_0x0a2a
            L_0x00d0:
                com.startapp.networkTest.results.ConnectivityTestResult r9 = new com.startapp.networkTest.results.ConnectivityTestResult
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                java.lang.String r10 = r10.g
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.d r11 = r11.e
                java.lang.String r11 = r11.a()
                r9.<init>(r10, r11)
                r1.f6008a = r9
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.LocationController r10 = r10.d
                com.startapp.networkTest.data.LocationInfo r10 = r10.b()
                r9.LocationInfo = r10
                com.startapp.networkTest.d r9 = com.startapp.networkTest.c.c()
                java.lang.String[] r9 = r9.j()
                com.startapp.networkTest.d r10 = com.startapp.networkTest.c.c()
                java.lang.String r10 = r10.k()
                com.startapp.networkTest.enums.CtCriteriaTypes r10 = com.startapp.networkTest.enums.CtCriteriaTypes.valueOf(r10)
                if (r7 == 0) goto L_0x0120
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                com.startapp.networkTest.results.ConnectivityTestResult r12 = r1.f6008a
                java.lang.String r13 = r12.ErrorReason
                r11.append(r13)
                r11.append(r7)
                java.lang.String r7 = r11.toString()
                r12.ErrorReason = r7
            L_0x0120:
                com.startapp.networkTest.results.ConnectivityTestResult r7 = r1.f6008a
                java.lang.String r11 = "20200514123200"
                r7.Version = r11
                java.lang.StringBuilder r11 = new java.lang.StringBuilder
                r11.<init>()
                com.startapp.networkTest.d.a.b r12 = com.startapp.networkTest.d.a.b.this
                java.lang.String r12 = r12.i
                r11.append(r12)
                java.lang.String r12 = "?id="
                r11.append(r12)
                com.startapp.networkTest.d.a.b r12 = com.startapp.networkTest.d.a.b.this
                java.util.Random r12 = r12.k
                long r12 = r12.nextLong()
                r11.append(r12)
                java.lang.String r11 = r11.toString()
                r7.ServerFilename = r11
                com.startapp.networkTest.results.ConnectivityTestResult r7 = r1.f6008a
                r7.BatteryInfo = r3
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                android.content.Context r3 = r3.f6006a
                com.startapp.networkTest.data.a r3 = com.startapp.networkTest.controller.b.a((android.content.Context) r3)
                r7.DeviceInfo = r3
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                android.content.Context r7 = r7.f6006a
                com.startapp.networkTest.data.b r7 = com.startapp.networkTest.controller.b.b(r7)
                r3.MemoryInfo = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r7 = r7.b
                com.startapp.networkTest.data.RadioInfo r7 = r7.c()
                r3.RadioInfo = r7
                com.startapp.networkTest.a r3 = com.startapp.networkTest.c.d()
                boolean r3 = r3.z()
                if (r3 == 0) goto L_0x0199
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                java.util.ArrayList r7 = new java.util.ArrayList
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r11 = r11.b
                com.startapp.networkTest.data.radio.CellInfo[] r11 = r11.d()
                java.util.List r11 = java.util.Arrays.asList(r11)
                r7.<init>(r11)
                r3.CellInfo = r7
            L_0x0199:
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                java.util.ArrayList r7 = new java.util.ArrayList
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r11 = r11.b
                com.startapp.networkTest.data.radio.ApnInfo[] r11 = r11.e()
                java.util.List r11 = java.util.Arrays.asList(r11)
                r7.<init>(r11)
                r3.ApnInfo = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                java.util.ArrayList r7 = new java.util.ArrayList
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r11 = r11.b
                com.startapp.networkTest.d.a.b r12 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r12 = r12.b
                com.startapp.networkTest.data.a.a r12 = r12.g()
                int r12 = r12.DefaultDataSimId
                com.startapp.networkTest.data.radio.NetworkRegistrationInfo[] r11 = r11.a((int) r12)
                java.util.List r11 = java.util.Arrays.asList(r11)
                r7.<init>(r11)
                r3.NetworkRegistrationInfo = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r7 = r7.b
                com.startapp.networkTest.d.a.b r11 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r11 = r11.b
                com.startapp.networkTest.data.a.a r11 = r11.g()
                int r11 = r11.DefaultDataSimId
                com.startapp.networkTest.enums.NetworkTypes r7 = r7.d((int) r11)
                r3.VoiceNetworkType = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                android.content.Context r7 = r7.f6006a
                java.lang.String r11 = "phone"
                java.lang.Object r7 = r7.getSystemService(r11)
                android.telephony.TelephonyManager r7 = (android.telephony.TelephonyManager) r7
                r11 = 1
                if (r7 == 0) goto L_0x0217
                int r7 = r7.getCallState()
                if (r7 == 0) goto L_0x0214
                if (r7 == r11) goto L_0x0211
                r12 = 2
                if (r7 == r12) goto L_0x020e
                com.startapp.networkTest.enums.voice.CallStates r7 = com.startapp.networkTest.enums.voice.CallStates.Unknown
                goto L_0x0219
            L_0x020e:
                com.startapp.networkTest.enums.voice.CallStates r7 = com.startapp.networkTest.enums.voice.CallStates.Offhook
                goto L_0x0219
            L_0x0211:
                com.startapp.networkTest.enums.voice.CallStates r7 = com.startapp.networkTest.enums.voice.CallStates.Ringing
                goto L_0x0219
            L_0x0214:
                com.startapp.networkTest.enums.voice.CallStates r7 = com.startapp.networkTest.enums.voice.CallStates.Idle
                goto L_0x0219
            L_0x0217:
                com.startapp.networkTest.enums.voice.CallStates r7 = com.startapp.networkTest.enums.voice.CallStates.Unknown
            L_0x0219:
                r3.CallState = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                android.content.Context r7 = r7.f6006a
                com.startapp.networkTest.data.e r7 = com.startapp.networkTest.controller.b.c(r7)
                r3.StorageInfo = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.d r7 = r7.c
                com.startapp.networkTest.data.WifiInfo r7 = r7.a()
                r3.WifiInfo = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.d r7 = r7.c
                com.startapp.networkTest.data.f r7 = com.startapp.networkTest.controller.b.a((com.startapp.networkTest.controller.d) r7)
                r3.TrafficInfo = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                android.content.Context r7 = r7.f6006a
                com.startapp.networkTest.enums.ScreenStates r7 = com.startapp.networkTest.controller.b.d(r7)
                r3.ScreenState = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.d.a.b r7 = com.startapp.networkTest.d.a.b.this
                android.content.Context r7 = r7.f6006a
                com.startapp.networkTest.enums.IdleStates r7 = com.startapp.networkTest.controller.b.e(r7)
                r3.IdleStateOnStart = r7
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                r3.SimInfo = r4
                com.startapp.networkTest.data.TimeInfo r4 = com.startapp.networkTest.e.b.a()
                r3.TimeInfo = r4
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.data.TimeInfo r4 = r3.TimeInfo
                java.lang.String r4 = r4.TimestampTableau
                r3.TestTimestamp = r4
                com.startapp.networkTest.d r4 = com.startapp.networkTest.c.c()
                long r12 = r4.e()
                r3.TruststoreTimestamp = r12
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                com.startapp.networkTest.data.TimeInfo r4 = r3.TimeInfo
                java.lang.String r7 = r3.GUID
                java.lang.String r4 = com.iab.omid.library.startapp.b.a((com.startapp.networkTest.data.TimeInfo) r4, (java.lang.String) r7)
                r3.CtId = r4
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.d r3 = r3.e
                boolean r3 = r3.c()
                if (r3 == 0) goto L_0x02a5
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.d r3 = r3.e
                boolean r3 = r3.b()
                if (r3 != 0) goto L_0x02a5
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a
                r3.IsKeepAlive = r11
            L_0x02a5:
                long r3 = android.os.SystemClock.elapsedRealtime()
                long r12 = android.os.SystemClock.uptimeMillis()
                javax.net.ssl.HostnameVerifier r7 = javax.net.ssl.HttpsURLConnection.getDefaultHostnameVerifier()
                com.startapp.networkTest.a.a r14 = new com.startapp.networkTest.a.a
                com.startapp.networkTest.d.a.b r15 = com.startapp.networkTest.d.a.b.this
                android.content.Context r15 = r15.f6006a
                r14.<init>(r15, r8)
                java.util.List r8 = r1.a(r9, r10)
                java.util.LinkedList r9 = new java.util.LinkedList
                r9.<init>()
                com.startapp.networkTest.d.a.b r10 = com.startapp.networkTest.d.a.b.this
                java.lang.String r10 = r10.h
                com.startapp.networkTest.d.a.b r15 = com.startapp.networkTest.d.a.b.this
                java.lang.String r15 = r15.j
                r18 = r3
                r16 = r5
                r17 = r16
                r20 = r12
                r3 = 0
                r4 = 0
                r12 = 0
                r13 = r17
            L_0x02de:
                int r5 = r8.size()
                r23 = r7
                java.lang.String r6 = ""
                java.lang.String r7 = "; "
                if (r3 < r5) goto L_0x0303
                boolean r5 = r8.isEmpty()
                if (r5 == 0) goto L_0x02f7
                int r5 = r10.length()
                if (r5 <= 0) goto L_0x02f7
                goto L_0x0303
            L_0x02f7:
                r11 = r8
                r10 = r16
                r5 = r17
                r3 = 0
                r8 = r4
                r4 = r13
                r13 = r9
                r9 = 0
                goto L_0x0492
            L_0x0303:
                com.startapp.networkTest.data.c r5 = new com.startapp.networkTest.data.c
                r5.<init>()
                long r18 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x09dd, all -> 0x09d5 }
                long r20 = android.os.SystemClock.uptimeMillis()     // Catch:{ Exception -> 0x09dd, all -> 0x09d5 }
                com.startapp.networkTest.d.a.c r24 = new com.startapp.networkTest.d.a.c     // Catch:{ Exception -> 0x09dd, all -> 0x09d5 }
                r24.<init>()     // Catch:{ Exception -> 0x09dd, all -> 0x09d5 }
                int r4 = r4 + 1
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.f6008a     // Catch:{ Exception -> 0x09c4, all -> 0x09d5 }
                r17 = r4
                boolean r4 = b()     // Catch:{ Exception -> 0x09b5, all -> 0x09d5 }
                r11.LocalhostPingSuccess = r4     // Catch:{ Exception -> 0x09b5, all -> 0x09d5 }
                long r26 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x09b5, all -> 0x09d5 }
                boolean r4 = r8.isEmpty()     // Catch:{ Exception -> 0x09b5, all -> 0x09d5 }
                if (r4 != 0) goto L_0x03c8
                java.lang.Object r4 = r8.get(r3)     // Catch:{ Exception -> 0x03b5, all -> 0x03ae }
                com.startapp.networkTest.d.a.c r4 = (com.startapp.networkTest.d.a.c) r4     // Catch:{ Exception -> 0x03b5, all -> 0x03ae }
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.f6008a     // Catch:{ Exception -> 0x039d, all -> 0x03ae }
                r28 = r13
                java.lang.String r13 = r4.address     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r11.ServerHostname = r13     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                int r11 = r4.totalTests     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r13 = 1
                int r11 = r11 + r13
                r4.totalTests = r11     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                int r11 = r3 + 1
                r5.Try = r11     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r11.<init>()     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.f6008a     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                java.lang.String r13 = r13.ServerHostname     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r11.append(r13)     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.f6008a     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                java.lang.String r13 = r13.ServerFilename     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r11.append(r13)     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                java.lang.String r11 = r11.toString()     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r5.HostFile = r11     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.f6008a     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                com.startapp.networkTest.net.a r13 = new com.startapp.networkTest.net.a     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r13.<init>()     // Catch:{ Exception -> 0x038e, all -> 0x03ae }
                r29 = r4
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x038c, all -> 0x03ae }
                java.lang.String r4 = r4.ServerHostname     // Catch:{ Exception -> 0x038c, all -> 0x03ae }
                java.lang.String r4 = r13.a((java.lang.String) r4)     // Catch:{ Exception -> 0x038c, all -> 0x03ae }
                r11.ServerIp = r4     // Catch:{ Exception -> 0x038c, all -> 0x03ae }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x038c, all -> 0x03ae }
                long r30 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x038c, all -> 0x03ae }
                r11 = r8
                r13 = r9
                long r8 = r30 - r26
                r4.DurationDNS = r8     // Catch:{ Exception -> 0x0385, all -> 0x0381 }
                r4 = r3
                r3 = r29
                r8 = 1
                goto L_0x0428
            L_0x0381:
                r0 = move-exception
                r1 = r0
                r8 = r13
                goto L_0x03b1
            L_0x0385:
                r0 = move-exception
                r27 = r3
                r26 = r11
                r8 = r13
                goto L_0x0396
            L_0x038c:
                r0 = move-exception
                goto L_0x0391
            L_0x038e:
                r0 = move-exception
                r29 = r4
            L_0x0391:
                r27 = r3
                r26 = r8
                r8 = r9
            L_0x0396:
                r4 = r17
                r11 = r23
                r13 = r28
                goto L_0x03ab
            L_0x039d:
                r0 = move-exception
                r29 = r4
                r28 = r13
                r27 = r3
                r26 = r8
                r8 = r9
                r4 = r17
                r11 = r23
            L_0x03ab:
                r17 = r29
                goto L_0x03c3
            L_0x03ae:
                r0 = move-exception
                r1 = r0
                r8 = r9
            L_0x03b1:
                r24 = 1
                goto L_0x0a24
            L_0x03b5:
                r0 = move-exception
                r28 = r13
                r27 = r3
                r26 = r8
                r8 = r9
                r4 = r17
                r11 = r23
                r17 = r24
            L_0x03c3:
                r6 = 1
            L_0x03c4:
                r22 = 0
                goto L_0x09ea
            L_0x03c8:
                r11 = r8
                r28 = r13
                r13 = r9
                int r4 = r15.length()     // Catch:{ Exception -> 0x09a4, all -> 0x09a1 }
                if (r4 <= 0) goto L_0x0404
                int r4 = r10.length()     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                if (r4 <= 0) goto L_0x0404
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                r4.ServerIp = r15     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                r4.ServerHostname = r10     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                r8 = 0
                r4.DurationDNS = r8     // Catch:{ Exception -> 0x03f4, all -> 0x03ef }
                int r3 = r3 + -1
                r4 = r3
                r10 = r6
                r15 = r10
            L_0x03eb:
                r3 = r24
                r8 = 0
                goto L_0x0428
            L_0x03ef:
                r0 = move-exception
                r1 = r0
                r8 = r13
                goto L_0x09da
            L_0x03f4:
                r0 = move-exception
                r27 = r3
                r26 = r11
                r8 = r13
                r4 = r17
                r11 = r23
                r17 = r24
                r13 = r28
                r6 = 0
                goto L_0x03c4
            L_0x0404:
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x09a4, all -> 0x09a1 }
                r4.ServerHostname = r10     // Catch:{ Exception -> 0x09a4, all -> 0x09a1 }
                int r3 = r3 + -1
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                com.startapp.networkTest.net.a r8 = new com.startapp.networkTest.net.a     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                r8.<init>()     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                java.lang.String r9 = r9.ServerHostname     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                java.lang.String r8 = r8.a((java.lang.String) r9)     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                r4.ServerIp = r8     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                long r8 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                long r8 = r8 - r26
                r4.DurationDNS = r8     // Catch:{ Exception -> 0x0995, all -> 0x09a1 }
                r4 = r3
                r10 = r6
                goto L_0x03eb
            L_0x0428:
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x097d, all -> 0x0976 }
                r24 = r8
                long r8 = r9.DurationDNS     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                r26 = 30000(0x7530, double:1.4822E-319)
                int r29 = (r8 > r26 ? 1 : (r8 == r26 ? 0 : -1))
                if (r29 > 0) goto L_0x094b
                com.startapp.networkTest.results.ConnectivityTestResult r8 = r1.f6008a     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                java.lang.String r8 = r8.ServerIp     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                r5.ServerIp = r8     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                com.startapp.networkTest.results.ConnectivityTestResult r8 = r1.f6008a     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                long r8 = r8.DurationDNS     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                r5.DurationDNS = r8     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                int r8 = r3.DNSSuccess     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                r9 = 1
                int r8 = r8 + r9
                r3.DNSSuccess = r8     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                int r12 = r12 + 1
                r8 = 30000(0x7530, float:4.2039E-41)
                javax.net.SocketFactory r25 = android.net.SSLCertificateSocketFactory.getDefault(r8)     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                r8 = r25
                android.net.SSLCertificateSocketFactory r8 = (android.net.SSLCertificateSocketFactory) r8     // Catch:{ Exception -> 0x0972, all -> 0x0970 }
                r27 = r4
                javax.net.ssl.TrustManager[] r4 = new javax.net.ssl.TrustManager[r9]     // Catch:{ Exception -> 0x093d, all -> 0x0970 }
                r9 = 0
                r4[r9] = r14     // Catch:{ Exception -> 0x093d, all -> 0x0970 }
                r8.setTrustManagers(r4)     // Catch:{ Exception -> 0x093d, all -> 0x0970 }
                java.net.InetSocketAddress r4 = new java.net.InetSocketAddress     // Catch:{ Exception -> 0x093d, all -> 0x0970 }
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x093d, all -> 0x0970 }
                java.lang.String r9 = r9.ServerIp     // Catch:{ Exception -> 0x093d, all -> 0x0970 }
                r29 = r10
                r10 = 443(0x1bb, float:6.21E-43)
                r4.<init>(r9, r10)     // Catch:{ Exception -> 0x0933, all -> 0x0970 }
                java.net.Socket r9 = r8.createSocket()     // Catch:{ Exception -> 0x0933, all -> 0x0970 }
                javax.net.ssl.SSLSocket r9 = (javax.net.ssl.SSLSocket) r9     // Catch:{ Exception -> 0x0933, all -> 0x0970 }
                long r30 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x091a, all -> 0x0970 }
                r10 = 30000(0x7530, float:4.2039E-41)
                r9.connect(r4, r10)     // Catch:{ Exception -> 0x091a, all -> 0x0970 }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x091a, all -> 0x0970 }
                long r32 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x091a, all -> 0x0970 }
                r10 = r8
                r16 = r9
                long r8 = r32 - r30
                r4.DurationTcpConnect = r8     // Catch:{ Exception -> 0x0918, all -> 0x0970 }
                int r4 = r3.TCPSuccess     // Catch:{ Exception -> 0x0918, all -> 0x0970 }
                r8 = 1
                int r4 = r4 + r8
                r3.TCPSuccess = r4     // Catch:{ Exception -> 0x0918, all -> 0x0970 }
                r5 = r3
                r4 = r16
                r8 = r17
                r3 = 1
                r9 = 1
            L_0x0492:
                if (r3 == 0) goto L_0x082c
                long r15 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07e7, all -> 0x07e2 }
                r17 = r3
                int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x07e7, all -> 0x07e2 }
                r26 = r11
                r11 = 17
                if (r3 < r11) goto L_0x04c4
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a     // Catch:{ Exception -> 0x04b1, all -> 0x04eb }
                java.lang.String r3 = r3.ServerHostname     // Catch:{ Exception -> 0x04b1, all -> 0x04eb }
                r10.setHostname(r4, r3)     // Catch:{ Exception -> 0x04b1, all -> 0x04eb }
                r24 = r9
                r27 = r12
                r22 = 0
                goto L_0x051a
            L_0x04b1:
                r0 = move-exception
                r2 = r0
                r31 = r4
                r34 = r7
                r32 = r8
                r24 = r9
                r27 = r12
                r33 = r13
                r6 = 0
                r22 = 0
                goto L_0x07fa
            L_0x04c4:
                java.lang.Class r3 = r10.getClass()     // Catch:{ Exception -> 0x04f1, all -> 0x04eb }
                java.lang.String r11 = "setHostname"
                r24 = r9
                r27 = r12
                r9 = 1
                java.lang.Class[] r12 = new java.lang.Class[r9]     // Catch:{ Exception -> 0x04e9, all -> 0x04eb }
                java.lang.Class<java.lang.String> r25 = java.lang.String.class
                r22 = 0
                r12[r22] = r25     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                java.lang.reflect.Method r3 = r3.getMethod(r11, r12)     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                java.lang.Object[] r11 = new java.lang.Object[r9]     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                java.lang.String r9 = r9.ServerHostname     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                r11[r22] = r9     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                r3.invoke(r10, r11)     // Catch:{ Exception -> 0x04e7, all -> 0x04eb }
                goto L_0x051a
            L_0x04e7:
                r0 = move-exception
                goto L_0x04f8
            L_0x04e9:
                r0 = move-exception
                goto L_0x04f6
            L_0x04eb:
                r0 = move-exception
                r2 = r0
                r31 = r4
                goto L_0x0826
            L_0x04f1:
                r0 = move-exception
                r24 = r9
                r27 = r12
            L_0x04f6:
                r22 = 0
            L_0x04f8:
                r3 = r0
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.<init>()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.f6008a     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r11 = r10.SslException     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.append(r11)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r11 = "SNI not available:"
                r9.append(r11)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r3 = r3.getMessage()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.append(r3)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.append(r7)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r3 = r9.toString()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r10.SslException = r3     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
            L_0x051a:
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.lang.String r3 = r3.ServerHostname     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                javax.net.ssl.SSLSession r9 = r4.getSession()     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r11 = r23
                boolean r3 = r11.verify(r3, r9)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                if (r3 != 0) goto L_0x0581
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r3.<init>()     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.lang.String r10 = r9.SslException     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r3.append(r10)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.lang.String r10 = "Expected "
                r3.append(r10)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.f6008a     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.lang.String r10 = r10.ServerHostname     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r3.append(r10)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.lang.String r10 = " found "
                r3.append(r10)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                javax.net.ssl.SSLSession r10 = r4.getSession()     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.security.Principal r10 = r10.getPeerPrincipal()     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r3.append(r10)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r3.append(r7)     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                r9.SslException = r3     // Catch:{ Exception -> 0x055c, all -> 0x04eb }
                goto L_0x057f
            L_0x055c:
                r0 = move-exception
                r3 = r0
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.<init>()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.f6008a     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r11 = r10.SslException     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.append(r11)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r11 = "Cannot validate hostname: "
                r9.append(r11)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r3 = r3.getMessage()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.append(r3)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r9.append(r7)     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                java.lang.String r3 = r9.toString()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                r10.SslException = r3     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
            L_0x057f:
                r17 = 0
            L_0x0581:
                com.startapp.networkTest.results.ConnectivityTestResult r3 = r1.f6008a     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                long r9 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                long r9 = r9 - r15
                r3.DurationSSL = r9     // Catch:{ Exception -> 0x07d8, all -> 0x07e2 }
                if (r17 == 0) goto L_0x058e
                r3 = 1
                goto L_0x058f
            L_0x058e:
                r3 = 0
            L_0x058f:
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                com.startapp.networkTest.enums.CtTestTypes r10 = r14.b()     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r9.TestType = r10     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r9 = r1.f6008a     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                com.startapp.networkTest.enums.CtTestTypes r9 = r9.TestType     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                com.startapp.networkTest.enums.CtTestTypes r10 = com.startapp.networkTest.enums.CtTestTypes.SSLOwnTs     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                boolean r9 = r9.equals(r10)     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                if (r9 != 0) goto L_0x05d4
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                r9.<init>()     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.f6008a     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                java.lang.String r11 = r10.SslException     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                r9.append(r11)     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                java.lang.String r11 = "We couldn't use our own truststore, used: "
                r9.append(r11)     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                com.startapp.networkTest.results.ConnectivityTestResult r11 = r1.f6008a     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                com.startapp.networkTest.enums.CtTestTypes r11 = r11.TestType     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                r9.append(r11)     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                r9.append(r7)     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                r10.SslException = r9     // Catch:{ Exception -> 0x05c7, all -> 0x04eb }
                r17 = 0
                goto L_0x05d4
            L_0x05c7:
                r0 = move-exception
                r2 = r0
                r6 = r3
                r31 = r4
                r34 = r7
                r32 = r8
                r33 = r13
                goto L_0x07fa
            L_0x05d4:
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r9.<init>()     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r10 = r1.f6008a     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                java.lang.String r11 = r10.SslException     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r9.append(r11)     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                java.lang.String r11 = r14.a()     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r9.append(r11)     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r9.append(r7)     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                java.lang.String r9 = r9.toString()     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r10.SslException = r9     // Catch:{ Exception -> 0x07c9, all -> 0x07e2 }
                r9 = 2048(0x800, float:2.87E-42)
                byte[] r9 = new byte[r9]     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.io.PrintWriter r10 = new java.io.PrintWriter     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.io.OutputStream r11 = r4.getOutputStream()     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.<init>(r11)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                long r11 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.lang.String r14 = "GET "
                r10.print(r14)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r14 = r1.f6008a     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.lang.String r14 = r14.ServerFilename     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r14)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.lang.String r14 = " HTTP/1.1"
                r10.print(r14)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r2)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.lang.String r14 = "HOST: "
                r10.print(r14)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r14 = r1.f6008a     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.lang.String r14 = r14.ServerHostname     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r14)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r2)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.lang.String r14 = "Connection: close"
                r10.print(r14)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r2)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r2)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.print(r2)     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r10.flush()     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                long r14 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                long r14 = r14 - r11
                r2.DurationHttpGetCommand = r14     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                long r10 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                java.io.InputStream r2 = r4.getInputStream()     // Catch:{ Exception -> 0x07b8, all -> 0x07e2 }
                r15 = r17
                r14 = -1
                r16 = 0
            L_0x064b:
                com.startapp.networkTest.d.a.b$a$a r12 = r1.a((java.io.InputStream) r2)     // Catch:{ all -> 0x0794 }
                r30 = r3
                int r3 = r12.f6013a     // Catch:{ all -> 0x0792 }
                r31 = r4
                long r3 = (long) r3
                long r3 = r16 + r3
                r32 = r8
                java.lang.String r8 = r12.b     // Catch:{ all -> 0x078b }
                java.lang.String r8 = r8.toUpperCase()     // Catch:{ all -> 0x078b }
                r33 = r13
                java.lang.String r13 = "HTTP"
                boolean r13 = r8.startsWith(r13)     // Catch:{ all -> 0x0787 }
                if (r13 == 0) goto L_0x06b6
                java.lang.String r12 = " "
                java.lang.String[] r8 = r8.split(r12)     // Catch:{ all -> 0x06b0 }
                com.startapp.networkTest.results.ConnectivityTestResult r12 = r1.f6008a     // Catch:{ all -> 0x06b0 }
                r13 = 1
                r8 = r8[r13]     // Catch:{ all -> 0x06b0 }
                int r8 = java.lang.Integer.parseInt(r8)     // Catch:{ all -> 0x06b0 }
                r12.HTTPStatus = r8     // Catch:{ all -> 0x06b0 }
                com.startapp.networkTest.results.ConnectivityTestResult r8 = r1.f6008a     // Catch:{ all -> 0x06b0 }
                int r8 = r8.HTTPStatus     // Catch:{ all -> 0x06b0 }
                r12 = 200(0xc8, float:2.8E-43)
                if (r8 == r12) goto L_0x06a5
                java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ all -> 0x06b0 }
                r8.<init>()     // Catch:{ all -> 0x06b0 }
                com.startapp.networkTest.results.ConnectivityTestResult r12 = r1.f6008a     // Catch:{ all -> 0x06b0 }
                java.lang.String r13 = r12.ErrorReason     // Catch:{ all -> 0x06b0 }
                r8.append(r13)     // Catch:{ all -> 0x06b0 }
                java.lang.String r13 = "Request failed! Unexcepted HTTP code: "
                r8.append(r13)     // Catch:{ all -> 0x06b0 }
                com.startapp.networkTest.results.ConnectivityTestResult r13 = r1.f6008a     // Catch:{ all -> 0x06b0 }
                int r13 = r13.HTTPStatus     // Catch:{ all -> 0x06b0 }
                r8.append(r13)     // Catch:{ all -> 0x06b0 }
                r8.append(r7)     // Catch:{ all -> 0x06b0 }
                java.lang.String r8 = r8.toString()     // Catch:{ all -> 0x06b0 }
                r12.ErrorReason = r8     // Catch:{ all -> 0x06b0 }
                r15 = 0
            L_0x06a5:
                r16 = r3
                r3 = r30
                r4 = r31
                r8 = r32
                r13 = r33
                goto L_0x064b
            L_0x06b0:
                r0 = move-exception
                r2 = r0
                r34 = r7
                goto L_0x07a2
            L_0x06b6:
                java.lang.String r13 = "CONTENT-LENGTH:"
                boolean r13 = r8.startsWith(r13)     // Catch:{ all -> 0x0787 }
                r34 = r7
                r7 = 15
                if (r13 == 0) goto L_0x06cf
                java.lang.String r7 = r8.substring(r7)     // Catch:{ NumberFormatException -> 0x0710 }
                java.lang.String r7 = r7.trim()     // Catch:{ NumberFormatException -> 0x0710 }
                int r14 = java.lang.Integer.parseInt(r7)     // Catch:{ NumberFormatException -> 0x0710 }
                goto L_0x0710
            L_0x06cf:
                java.lang.String r13 = "X-AMZ-CF-ID:"
                boolean r13 = r8.startsWith(r13)     // Catch:{ all -> 0x0785 }
                if (r13 == 0) goto L_0x06e6
                com.startapp.networkTest.results.ConnectivityTestResult r8 = r1.f6008a     // Catch:{ all -> 0x0785 }
                java.lang.String r12 = r12.b     // Catch:{ all -> 0x0785 }
                java.lang.String r7 = r12.substring(r7)     // Catch:{ all -> 0x0785 }
                java.lang.String r7 = r7.trim()     // Catch:{ all -> 0x0785 }
                r8.AmazonId = r7     // Catch:{ all -> 0x0785 }
                goto L_0x0710
            L_0x06e6:
                boolean r7 = r12.c     // Catch:{ all -> 0x0785 }
                if (r7 != 0) goto L_0x071e
                boolean r7 = r8.equals(r6)     // Catch:{ all -> 0x0785 }
                if (r7 != 0) goto L_0x071e
                java.lang.String r7 = "X-AMZ-CF-POP:"
                boolean r7 = r8.startsWith(r7)     // Catch:{ all -> 0x0785 }
                if (r7 == 0) goto L_0x0710
                com.startapp.networkTest.results.ConnectivityTestResult r7 = r1.f6008a     // Catch:{ all -> 0x0785 }
                java.lang.String r8 = r12.b     // Catch:{ all -> 0x0785 }
                java.lang.String r8 = r8.toLowerCase()     // Catch:{ all -> 0x0785 }
                r12 = 13
                java.lang.String r8 = r8.substring(r12)     // Catch:{ all -> 0x0785 }
                java.lang.String r8 = r8.trim()     // Catch:{ all -> 0x0785 }
                java.lang.String r8 = com.startapp.networkTest.utils.f.b(r8)     // Catch:{ all -> 0x0785 }
                r7.AirportCode = r8     // Catch:{ all -> 0x0785 }
            L_0x0710:
                r16 = r3
                r3 = r30
                r4 = r31
                r8 = r32
                r13 = r33
                r7 = r34
                goto L_0x064b
            L_0x071e:
                com.startapp.networkTest.results.ConnectivityTestResult r6 = r1.f6008a     // Catch:{ all -> 0x0785 }
                r6.HeaderBytesRead = r3     // Catch:{ all -> 0x0785 }
                r6 = -1
                if (r14 == r6) goto L_0x072d
                int r7 = (int) r3
                int r7 = r7 + r14
                long r7 = (long) r7
                r16 = r3
                r28 = r7
                goto L_0x0734
            L_0x072d:
                r16 = r3
                r28 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            L_0x0734:
                int r3 = r2.read(r9)     // Catch:{ all -> 0x0783 }
                if (r3 != r6) goto L_0x0747
                int r2 = (r16 > r28 ? 1 : (r16 == r28 ? 0 : -1))
                if (r2 < 0) goto L_0x073f
                goto L_0x0750
            L_0x073f:
                java.io.IOException r2 = new java.io.IOException     // Catch:{ all -> 0x0783 }
                java.lang.String r3 = "Could not read all bytes"
                r2.<init>(r3)     // Catch:{ all -> 0x0783 }
                throw r2     // Catch:{ all -> 0x0783 }
            L_0x0747:
                long r7 = (long) r3
                long r16 = r16 + r7
                int r4 = (r16 > r28 ? 1 : (r16 == r28 ? 0 : -1))
                if (r4 >= 0) goto L_0x0750
                if (r3 > 0) goto L_0x0734
            L_0x0750:
                r2 = r16
                if (r15 == 0) goto L_0x0765
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ all -> 0x075f }
                r6 = 1
                r4.Success = r6     // Catch:{ all -> 0x075f }
                int r4 = r5.successfulTests     // Catch:{ all -> 0x075f }
                int r4 = r4 + r6
                r5.successfulTests = r4     // Catch:{ all -> 0x075f }
                goto L_0x0765
            L_0x075f:
                r0 = move-exception
                r3 = r2
                r5 = 0
                r2 = r0
                goto L_0x07a4
            L_0x0765:
                r4 = 0
                int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
                if (r6 <= 0) goto L_0x0778
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x07b6 }
                long r5 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07b6 }
                long r5 = r5 - r10
                r4.DurationHttpReceive = r5     // Catch:{ Exception -> 0x07b6 }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x07b6 }
                r4.BytesRead = r2     // Catch:{ Exception -> 0x07b6 }
            L_0x0778:
                if (r31 == 0) goto L_0x077d
                r31.close()     // Catch:{ Exception -> 0x077d }
            L_0x077d:
                r6 = r30
                r22 = 1
                goto L_0x0839
            L_0x0783:
                r0 = move-exception
                goto L_0x079f
            L_0x0785:
                r0 = move-exception
                goto L_0x0790
            L_0x0787:
                r0 = move-exception
                r34 = r7
                goto L_0x0790
            L_0x078b:
                r0 = move-exception
                r34 = r7
                r33 = r13
            L_0x0790:
                r2 = r0
                goto L_0x07a2
            L_0x0792:
                r0 = move-exception
                goto L_0x0797
            L_0x0794:
                r0 = move-exception
                r30 = r3
            L_0x0797:
                r31 = r4
                r34 = r7
                r32 = r8
                r33 = r13
            L_0x079f:
                r2 = r0
                r3 = r16
            L_0x07a2:
                r5 = 0
            L_0x07a4:
                int r7 = (r3 > r5 ? 1 : (r3 == r5 ? 0 : -1))
                if (r7 <= 0) goto L_0x07b5
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.f6008a     // Catch:{ Exception -> 0x07b6 }
                long r6 = android.os.SystemClock.elapsedRealtime()     // Catch:{ Exception -> 0x07b6 }
                long r6 = r6 - r10
                r5.DurationHttpReceive = r6     // Catch:{ Exception -> 0x07b6 }
                com.startapp.networkTest.results.ConnectivityTestResult r5 = r1.f6008a     // Catch:{ Exception -> 0x07b6 }
                r5.BytesRead = r3     // Catch:{ Exception -> 0x07b6 }
            L_0x07b5:
                throw r2     // Catch:{ Exception -> 0x07b6 }
            L_0x07b6:
                r0 = move-exception
                goto L_0x07c3
            L_0x07b8:
                r0 = move-exception
                r30 = r3
                r31 = r4
                r34 = r7
                r32 = r8
                r33 = r13
            L_0x07c3:
                r2 = r0
                r6 = r30
                r22 = 1
                goto L_0x07fa
            L_0x07c9:
                r0 = move-exception
                r30 = r3
                r31 = r4
                r34 = r7
                r32 = r8
                r33 = r13
                r2 = r0
                r6 = r30
                goto L_0x07fa
            L_0x07d8:
                r0 = move-exception
                r31 = r4
                r34 = r7
                r32 = r8
                r33 = r13
                goto L_0x07f8
            L_0x07e2:
                r0 = move-exception
                r31 = r4
            L_0x07e5:
                r2 = r0
                goto L_0x0826
            L_0x07e7:
                r0 = move-exception
                r31 = r4
                r34 = r7
                r32 = r8
                r24 = r9
                r26 = r11
                r27 = r12
                r33 = r13
                r22 = 0
            L_0x07f8:
                r2 = r0
                r6 = 0
            L_0x07fa:
                java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0824 }
                java.lang.String r2 = com.startapp.networkTest.d.a.b.a((java.lang.String) r2)     // Catch:{ all -> 0x0824 }
                java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x0824 }
                r3.<init>()     // Catch:{ all -> 0x0824 }
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ all -> 0x0824 }
                java.lang.String r5 = r4.ErrorReason     // Catch:{ all -> 0x0824 }
                r3.append(r5)     // Catch:{ all -> 0x0824 }
                r3.append(r2)     // Catch:{ all -> 0x0824 }
                r7 = r34
                r3.append(r7)     // Catch:{ all -> 0x0824 }
                java.lang.String r2 = r3.toString()     // Catch:{ all -> 0x0824 }
                r4.ErrorReason = r2     // Catch:{ all -> 0x0824 }
                if (r31 == 0) goto L_0x0839
                r31.close()     // Catch:{ Exception -> 0x0822 }
                goto L_0x0839
            L_0x0822:
                goto L_0x0839
            L_0x0824:
                r0 = move-exception
                goto L_0x07e5
            L_0x0826:
                if (r31 == 0) goto L_0x082b
                r31.close()     // Catch:{ Exception -> 0x082b }
            L_0x082b:
                throw r2
            L_0x082c:
                r32 = r8
                r24 = r9
                r26 = r11
                r27 = r12
                r33 = r13
                r22 = 0
                r6 = 0
            L_0x0839:
                if (r22 == 0) goto L_0x0873
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                com.startapp.networkTest.data.RadioInfo r3 = r2.RadioInfo
                com.startapp.networkTest.enums.ConnectionTypes r3 = r3.ConnectionType
                com.startapp.networkTest.enums.ConnectionTypes r4 = com.startapp.networkTest.enums.ConnectionTypes.WiFi
                if (r3 != r4) goto L_0x0854
                com.startapp.networkTest.b.a r3 = com.startapp.networkTest.b.a.a()
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a
                com.startapp.networkTest.data.WifiInfo r4 = r4.WifiInfo
                com.startapp.networkTest.data.IspInfo r3 = r3.a(r4)
                r2.IspInfo = r3
                goto L_0x0873
            L_0x0854:
                com.startapp.networkTest.a r2 = com.startapp.networkTest.c.d()
                boolean r2 = r2.s()
                if (r2 == 0) goto L_0x0873
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                com.startapp.networkTest.data.RadioInfo r3 = r2.RadioInfo
                com.startapp.networkTest.enums.ConnectionTypes r3 = r3.ConnectionType
                com.startapp.networkTest.enums.ConnectionTypes r4 = com.startapp.networkTest.enums.ConnectionTypes.Mobile
                if (r3 != r4) goto L_0x0873
                com.startapp.networkTest.b.a r3 = com.startapp.networkTest.b.a.a()
                r4 = 0
                com.startapp.networkTest.data.IspInfo r3 = r3.a(r4)
                r2.IspInfo = r3
            L_0x0873:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                com.startapp.networkTest.controller.c r3 = r3.b
                com.startapp.networkTest.data.RadioInfo r3 = r3.c()
                r2.RadioInfoOnEnd = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                long r3 = android.os.SystemClock.uptimeMillis()
                long r3 = r3 - r20
                r2.DurationOverallNoSleep = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                long r3 = android.os.SystemClock.elapsedRealtime()
                long r3 = r3 - r18
                r2.DurationOverall = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                com.startapp.networkTest.d.a.b r3 = com.startapp.networkTest.d.a.b.this
                android.content.Context r3 = r3.f6006a
                com.startapp.networkTest.enums.IdleStates r3 = com.startapp.networkTest.controller.b.e(r3)
                r2.IdleStateOnEnd = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                java.util.ArrayList r3 = new java.util.ArrayList
                r8 = r33
                r3.<init>(r8)
                r2.MultiCdnInfo = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                r4 = r32
                long r3 = (long) r4
                r12 = r27
                long r7 = (long) r12
                r9 = 4611686018427387904(0x4000000000000000, double:2.0)
                r11 = 4621819117588971520(0x4024000000000000, double:10.0)
                double r9 = java.lang.Math.pow(r11, r9)
                long r9 = java.lang.Math.round(r9)
                long r7 = r7 * r9
                long r3 = r3 + r7
                r5 = r24
                long r7 = (long) r5
                r9 = 4616189618054758400(0x4010000000000000, double:4.0)
                double r9 = java.lang.Math.pow(r11, r9)
                long r9 = java.lang.Math.round(r9)
                long r7 = r7 * r9
                long r3 = r3 + r7
                long r5 = (long) r6
                r7 = 4618441417868443648(0x4018000000000000, double:6.0)
                double r7 = java.lang.Math.pow(r11, r7)
                long r7 = java.lang.Math.round(r7)
                long r5 = r5 * r7
                long r3 = r3 + r5
                r2.ServerMultiSuccess = r3
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                java.lang.String r2 = r2.AirportCode
                boolean r2 = r2.isEmpty()
                if (r2 == 0) goto L_0x08f9
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                java.lang.String r3 = r2.ServerIp
                java.lang.String r3 = com.startapp.networkTest.utils.f.a(r3)
                r2.AirportCode = r3
            L_0x08f9:
                int r2 = r26.size()
                if (r2 <= 0) goto L_0x0902
                a((java.util.List<com.startapp.networkTest.d.a.c>) r26)
            L_0x0902:
                com.startapp.networkTest.a r2 = com.startapp.networkTest.c.d()
                boolean r2 = r2.A()
                if (r2 == 0) goto L_0x0915
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                com.startapp.networkTest.data.LocationInfo r3 = new com.startapp.networkTest.data.LocationInfo
                r3.<init>()
                r2.LocationInfo = r3
            L_0x0915:
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a
                return r2
            L_0x0918:
                r0 = move-exception
                goto L_0x091e
            L_0x091a:
                r0 = move-exception
                r10 = r8
                r16 = r9
            L_0x091e:
                r26 = r11
                r8 = r13
                r11 = r23
                r22 = 0
                r13 = r16
                r4 = r17
                r6 = r24
                r17 = r3
                r16 = r10
                r10 = r29
                goto L_0x09ea
            L_0x0933:
                r0 = move-exception
                r10 = r8
                r26 = r11
                r8 = r13
                r11 = r23
                r22 = 0
                goto L_0x0948
            L_0x093d:
                r0 = move-exception
                r29 = r10
                r26 = r11
                r11 = r23
                r22 = 0
                r10 = r8
                r8 = r13
            L_0x0948:
                r16 = r10
                goto L_0x0967
            L_0x094b:
                r27 = r4
                r29 = r10
                r26 = r11
                r8 = r13
                r11 = r23
                r22 = 0
                com.startapp.networkTest.results.ConnectivityTestResult r4 = r1.f6008a     // Catch:{ Exception -> 0x0966, all -> 0x0964 }
                r9 = -1
                r4.DurationDNS = r9     // Catch:{ Exception -> 0x0966, all -> 0x0964 }
                java.util.concurrent.TimeoutException r4 = new java.util.concurrent.TimeoutException     // Catch:{ Exception -> 0x0966, all -> 0x0964 }
                java.lang.String r6 = "DNS Timeout"
                r4.<init>(r6)     // Catch:{ Exception -> 0x0966, all -> 0x0964 }
                throw r4     // Catch:{ Exception -> 0x0966, all -> 0x0964 }
            L_0x0964:
                r0 = move-exception
                goto L_0x097a
            L_0x0966:
                r0 = move-exception
            L_0x0967:
                r4 = r17
                r6 = r24
                r13 = r28
                r10 = r29
                goto L_0x0991
            L_0x0970:
                r0 = move-exception
                goto L_0x0979
            L_0x0972:
                r0 = move-exception
                r27 = r4
                goto L_0x0982
            L_0x0976:
                r0 = move-exception
                r24 = r8
            L_0x0979:
                r8 = r13
            L_0x097a:
                r1 = r0
                goto L_0x0a24
            L_0x097d:
                r0 = move-exception
                r27 = r4
                r24 = r8
            L_0x0982:
                r29 = r10
                r26 = r11
                r8 = r13
                r11 = r23
                r22 = 0
                r4 = r17
                r6 = r24
                r13 = r28
            L_0x0991:
                r17 = r3
                goto L_0x09ea
            L_0x0995:
                r0 = move-exception
                r26 = r11
                r8 = r13
                r11 = r23
                r22 = 0
                r27 = r3
                r10 = r6
                goto L_0x09ae
            L_0x09a1:
                r0 = move-exception
                r8 = r13
                goto L_0x09d7
            L_0x09a4:
                r0 = move-exception
                r26 = r11
                r8 = r13
                r11 = r23
                r22 = 0
                r27 = r3
            L_0x09ae:
                r4 = r17
                r17 = r24
                r13 = r28
                goto L_0x09e9
            L_0x09b5:
                r0 = move-exception
                r26 = r8
                r8 = r9
                r28 = r13
                r11 = r23
                r22 = 0
                r27 = r3
                r4 = r17
                goto L_0x09d2
            L_0x09c4:
                r0 = move-exception
                r17 = r4
                r26 = r8
                r8 = r9
                r28 = r13
                r11 = r23
                r22 = 0
                r27 = r3
            L_0x09d2:
                r17 = r24
                goto L_0x09e9
            L_0x09d5:
                r0 = move-exception
                r8 = r9
            L_0x09d7:
                r22 = 0
                r1 = r0
            L_0x09da:
                r24 = 0
                goto L_0x0a24
            L_0x09dd:
                r0 = move-exception
                r26 = r8
                r8 = r9
                r28 = r13
                r11 = r23
                r22 = 0
                r27 = r3
            L_0x09e9:
                r6 = 0
            L_0x09ea:
                r3 = r0
                java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x0a20 }
                java.lang.String r3 = com.startapp.networkTest.d.a.b.a((java.lang.String) r3)     // Catch:{ all -> 0x0a20 }
                java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0a20 }
                r9.<init>()     // Catch:{ all -> 0x0a20 }
                r23 = r2
                com.startapp.networkTest.results.ConnectivityTestResult r2 = r1.f6008a     // Catch:{ all -> 0x0a20 }
                java.lang.String r1 = r2.ErrorReason     // Catch:{ all -> 0x0a20 }
                r9.append(r1)     // Catch:{ all -> 0x0a20 }
                r9.append(r3)     // Catch:{ all -> 0x0a20 }
                r9.append(r7)     // Catch:{ all -> 0x0a20 }
                java.lang.String r1 = r9.toString()     // Catch:{ all -> 0x0a20 }
                r2.ErrorReason = r1     // Catch:{ all -> 0x0a20 }
                if (r6 == 0) goto L_0x0a12
                r8.add(r5)
            L_0x0a12:
                r1 = 1
                int r3 = r27 + 1
                r1 = r35
                r9 = r8
                r7 = r11
                r2 = r23
                r8 = r26
                r11 = 1
                goto L_0x02de
            L_0x0a20:
                r0 = move-exception
                r1 = r0
                r24 = r6
            L_0x0a24:
                if (r24 == 0) goto L_0x0a29
                r8.add(r5)
            L_0x0a29:
                throw r1
            L_0x0a2a:
                r1 = r5
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.d.a.b.a.a():com.startapp.networkTest.results.ConnectivityTestResult");
        }

        public final void a(SpeedtestEngineStatus speedtestEngineStatus) {
            if (speedtestEngineStatus == SpeedtestEngineStatus.END || speedtestEngineStatus == SpeedtestEngineStatus.ABORTED) {
                this.b.b();
                if (b.this.f != null) {
                    b.this.f.a((LatencyResult) this.b.a());
                    b.this.f.a();
                }
            }
        }

        private List<c> a(String[] strArr, CtCriteriaTypes ctCriteriaTypes) {
            LinkedList linkedList = new LinkedList();
            LinkedList linkedList2 = new LinkedList();
            Set<String> f = com.startapp.networkTest.c.c().f();
            LinkedList<c> linkedList3 = new LinkedList<>();
            if (f != null) {
                for (String a2 : f) {
                    c cVar = (c) com.startapp.common.parser.b.a(a2, c.class);
                    if (cVar != null) {
                        linkedList3.add(cVar);
                    }
                }
            }
            for (String str : strArr) {
                c cVar2 = new c();
                cVar2.address = str;
                linkedList2.add(cVar2);
            }
            for (c cVar3 : linkedList3) {
                for (int i = 0; i < linkedList2.size(); i++) {
                    if (((c) linkedList2.get(i)).address.equals(cVar3.address)) {
                        linkedList2.set(i, cVar3);
                    }
                }
            }
            switch (AnonymousClass1.f6007a[ctCriteriaTypes.ordinal()]) {
                case 1:
                    return linkedList2;
                case 2:
                    Collections.shuffle(linkedList2, new Random(System.nanoTime()));
                    return new LinkedList(linkedList2);
                case 3:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).DNSSuccess - ((c) obj2).DNSSuccess;
                        }
                    });
                    return new LinkedList(linkedList2);
                case 4:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).TCPSuccess - ((c) obj2).TCPSuccess;
                        }
                    });
                    return new LinkedList(linkedList2);
                case 5:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).successfulTests - ((c) obj2).successfulTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                case 6:
                    Collections.sort(linkedList2, new Comparator<c>() {
                        public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                            return ((c) obj).totalTests - ((c) obj2).totalTests;
                        }
                    });
                    return new LinkedList(linkedList2);
                default:
                    return linkedList;
            }
        }

        private static void a(List<c> list) {
            HashSet hashSet = new HashSet();
            for (c cVar : list) {
                hashSet.add(cVar.toString());
            }
            com.startapp.networkTest.c.c().a((Set<String>) hashSet);
        }
    }

    static /* synthetic */ String a(String str) {
        return (str == null || str.isEmpty()) ? "" : str.replaceAll("(?:[0-9]{1,3}\\.){3}[0-9]{1,3}", "XXX").replaceAll("([A-Fa-f0-9]{1,4}::?){1,7}[A-Fa-f0-9]{1,4}", "XXX");
    }
}
