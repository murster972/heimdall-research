package com.startapp.networkTest.net;

import android.util.Log;
import com.startapp.sdk.b.b;
import com.startapp.sdk.g.b.g;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class WebApiClient {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6068a = "WebApiClient";

    public enum RequestMethod {
        POST,
        GET,
        PUT,
        DELETE
    }

    private static g a(RequestMethod requestMethod, String str, b[] bVarArr) throws IOException {
        g gVar = new g();
        URL url = new URL(str);
        new URL(str);
        HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
        httpURLConnection.setRequestMethod(requestMethod.toString());
        for (int i = 0; i < 2; i++) {
            b bVar = bVarArr[i];
            httpURLConnection.setRequestProperty(bVar.f6435a, bVar.b);
        }
        httpURLConnection.setConnectTimeout(10000);
        httpURLConnection.setReadTimeout(10000);
        gVar.f6480a = httpURLConnection.getResponseCode();
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpURLConnection.getInputStream()));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                sb.append(readLine);
            }
            bufferedReader.close();
        } catch (Exception e) {
            String str2 = f6068a;
            Log.e(str2, "read content: " + e.getMessage());
        }
        httpURLConnection.disconnect();
        gVar.b = sb.toString();
        return gVar;
    }

    public static g a(RequestMethod requestMethod, String str) throws IOException {
        return a(requestMethod, str, new b[]{new b(TraktV2.HEADER_CONTENT_TYPE, "application/json; charset=UTF-8"), new b(TheTvdb.HEADER_ACCEPT, TraktV2.CONTENT_TYPE_JSON)});
    }
}
