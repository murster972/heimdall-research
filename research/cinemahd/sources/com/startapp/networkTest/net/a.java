package com.startapp.networkTest.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f6070a = "a";
    /* access modifiers changed from: private */
    public Object b;
    /* access modifiers changed from: private */
    public InetAddress c;

    public final String a(final String str) throws UnknownHostException {
        String hostAddress;
        this.b = new Object();
        Thread thread = new Thread(new Runnable() {
            public final void run() {
                try {
                    InetAddress byName = InetAddress.getByName(str);
                    synchronized (a.this.b) {
                        InetAddress unused = a.this.c = byName;
                    }
                } catch (Exception e) {
                    String unused2 = a.f6070a;
                    new StringBuilder("resolveHostname: ").append(e.toString());
                }
            }
        });
        thread.start();
        try {
            thread.join(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (this.b) {
            if (this.c != null) {
                hostAddress = this.c.getHostAddress();
            } else {
                throw new UnknownHostException();
            }
        }
        return hostAddress;
    }
}
