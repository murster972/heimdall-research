package com.startapp.networkTest.results;

import com.startapp.common.parser.d;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.data.IspInfo;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.data.a;
import com.startapp.networkTest.data.b;
import com.startapp.networkTest.data.c;
import com.startapp.networkTest.data.e;
import com.startapp.networkTest.data.f;
import com.startapp.networkTest.data.radio.ApnInfo;
import com.startapp.networkTest.data.radio.CellInfo;
import com.startapp.networkTest.data.radio.NetworkRegistrationInfo;
import com.startapp.networkTest.enums.CtTestTypes;
import com.startapp.networkTest.enums.IdleStates;
import com.startapp.networkTest.enums.NetworkTypes;
import com.startapp.networkTest.enums.ScreenStates;
import com.startapp.networkTest.enums.voice.CallStates;
import java.util.ArrayList;

public class ConnectivityTestResult extends BaseResult {
    public String AirportCode;
    public String AmazonId = "";
    @d(b = ArrayList.class, c = ApnInfo.class)
    public ArrayList<ApnInfo> ApnInfo;
    @d(a = true)
    public BatteryInfo BatteryInfo;
    public long BytesRead = -1;
    public CallStates CallState;
    @d(b = ArrayList.class, c = CellInfo.class)
    public ArrayList<CellInfo> CellInfo;
    public String CtId = "";
    @d(a = true)
    public a DeviceInfo;
    public long DurationDNS = -1;
    public long DurationHttpGetCommand = -1;
    public long DurationHttpReceive = -1;
    public long DurationOverall = -1;
    public long DurationOverallNoSleep = -1;
    public long DurationSSL = -1;
    public long DurationTcpConnect = -1;
    public String ErrorReason;
    public int HTTPStatus = -1;
    public long HeaderBytesRead = -1;
    public IdleStates IdleStateOnEnd;
    public IdleStates IdleStateOnStart;
    public boolean IsKeepAlive = false;
    @d(a = true)
    public IspInfo IspInfo;
    public boolean LocalhostPingSuccess = false;
    @d(a = true)
    public LocationInfo LocationInfo;
    @d(a = true)
    public b MemoryInfo;
    @d(b = ArrayList.class, c = c.class)
    public ArrayList<c> MultiCdnInfo;
    @d(b = ArrayList.class, c = NetworkRegistrationInfo.class)
    public ArrayList<NetworkRegistrationInfo> NetworkRegistrationInfo;
    @d(a = true)
    public RadioInfo RadioInfo;
    @d(a = true)
    public RadioInfo RadioInfoOnEnd;
    public ScreenStates ScreenState = ScreenStates.Unknown;
    public String ServerFilename = "";
    public String ServerHostname = "";
    public String ServerIp = "";
    public long ServerMultiSuccess;
    @d(a = true)
    public com.startapp.networkTest.data.a.b SimInfo;
    public String SslException;
    @d(a = true)
    public e StorageInfo;
    public boolean Success = false;
    public String TestTimestamp = "";
    public CtTestTypes TestType = CtTestTypes.Unknown;
    @d(a = true)
    public TimeInfo TimeInfo;
    @d(a = true)
    public f TrafficInfo;
    public long TruststoreTimestamp;
    public NetworkTypes VoiceNetworkType;
    @d(a = true)
    public WifiInfo WifiInfo;

    public ConnectivityTestResult(String str, String str2) {
        super(str, str2);
        IdleStates idleStates = IdleStates.Unknown;
        this.IdleStateOnStart = idleStates;
        this.IdleStateOnEnd = idleStates;
        this.ErrorReason = "";
        this.SslException = "";
        this.CallState = CallStates.Unknown;
        this.VoiceNetworkType = NetworkTypes.Unknown;
        this.ServerMultiSuccess = -1;
        this.AirportCode = "";
        this.BatteryInfo = new BatteryInfo();
        this.DeviceInfo = new a();
        this.LocationInfo = new LocationInfo();
        this.MemoryInfo = new b();
        this.RadioInfo = new RadioInfo();
        this.RadioInfoOnEnd = new RadioInfo();
        this.StorageInfo = new e();
        this.TrafficInfo = new f();
        this.WifiInfo = new WifiInfo();
        this.TimeInfo = new TimeInfo();
        this.IspInfo = new IspInfo();
        this.SimInfo = new com.startapp.networkTest.data.a.b();
        this.MultiCdnInfo = new ArrayList<>();
        this.CellInfo = new ArrayList<>();
        this.ApnInfo = new ArrayList<>();
        this.NetworkRegistrationInfo = new ArrayList<>();
    }
}
