package com.startapp.networkTest.results;

import com.startapp.common.parser.d;
import com.startapp.networkTest.controller.c;
import com.startapp.networkTest.data.BatteryInfo;
import com.startapp.networkTest.data.LocationInfo;
import com.startapp.networkTest.data.RadioInfo;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.data.WifiInfo;
import com.startapp.networkTest.data.a;
import com.startapp.networkTest.data.b;
import com.startapp.networkTest.data.f;
import com.startapp.networkTest.enums.ConnectionTypes;
import com.startapp.networkTest.enums.IpVersions;
import com.startapp.networkTest.enums.MeasurementTypes;
import com.startapp.networkTest.enums.NetworkGenerations;
import com.startapp.networkTest.enums.SpeedtestEndStates;
import com.startapp.networkTest.results.speedtest.MeasurementPointBase;
import com.startapp.networkTest.speedtest.SpeedtestEngineError;
import java.util.ArrayList;
import java.util.Iterator;

public class P3TestResult extends BaseResult {
    public int AvgValue;
    @d(a = true)
    public BatteryInfo BatteryInfoOnEnd = new BatteryInfo();
    @d(a = true)
    public BatteryInfo BatteryInfoOnStart = new BatteryInfo();
    public String CampaignId = "";
    public long ConnectingTimeControlServer = -1;
    public long ConnectingTimeTestServerControl = -1;
    public long ConnectingTimeTestServerSockets = -1;
    public String CustomerID = "";
    @d(a = true)
    public a DeviceInfo = new a();
    public String IMEI = "";
    public String IMSI = "";
    public IpVersions IpVersion = IpVersions.Unknown;
    @d(a = true)
    public LocationInfo LocationInfoOnEnd = new LocationInfo();
    @d(a = true)
    public LocationInfo LocationInfoOnStart = new LocationInfo();
    public int MaxValue;
    public MeasurementTypes MeasurementType = MeasurementTypes.Unknown;
    public int MedValue;
    @d(a = true)
    public b MemoryInfoOnEnd = new b();
    @d(a = true)
    public b MemoryInfoOnStart = new b();
    public String Meta = "";
    public int MinValue;
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    public ArrayList<com.startapp.networkTest.data.d> QuestionAnswerList = new ArrayList<>();
    public String QuestionnaireName = "";
    @d(a = true)
    public RadioInfo RadioInfoOnEnd = new RadioInfo();
    @d(a = true)
    public RadioInfo RadioInfoOnStart = new RadioInfo();
    public double RatShare2G;
    public double RatShare3G;
    public double RatShare4G;
    public double RatShare5G;
    public double RatShareUnknown;
    public double RatShareWiFi;
    public String SequenceID = "";
    public String Server = "";
    public boolean Success;
    public SpeedtestEndStates TestEndState = SpeedtestEndStates.Unknown;
    public SpeedtestEngineError TestErrorReason = SpeedtestEngineError.OK;
    @d(a = true)
    public TimeInfo TimeInfoOnEnd = new TimeInfo();
    @d(a = true)
    public TimeInfo TimeInfoOnStart = new TimeInfo();
    @d(a = true)
    public f TrafficInfoOnEnd = new f();
    @d(a = true)
    public f TrafficInfoOnStart = new f();
    @d(a = true)
    public WifiInfo WifiInfoOnEnd = new WifiInfo();
    @d(a = true)
    public WifiInfo WifiInfoOnStart = new WifiInfo();

    /* renamed from: com.startapp.networkTest.results.P3TestResult$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6072a = new int[NetworkGenerations.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.networkTest.enums.NetworkGenerations[] r0 = com.startapp.networkTest.enums.NetworkGenerations.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6072a = r0
                int[] r0 = f6072a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.networkTest.enums.NetworkGenerations r1 = com.startapp.networkTest.enums.NetworkGenerations.Gen2     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6072a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.networkTest.enums.NetworkGenerations r1 = com.startapp.networkTest.enums.NetworkGenerations.Gen3     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6072a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.networkTest.enums.NetworkGenerations r1 = com.startapp.networkTest.enums.NetworkGenerations.Gen4     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6072a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.networkTest.enums.NetworkGenerations r1 = com.startapp.networkTest.enums.NetworkGenerations.Gen5     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6072a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.networkTest.enums.NetworkGenerations r1 = com.startapp.networkTest.enums.NetworkGenerations.Unknown     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.results.P3TestResult.AnonymousClass1.<clinit>():void");
        }
    }

    public P3TestResult(String str, String str2) {
        super(str, str2);
    }

    public final void b(ArrayList<? extends MeasurementPointBase> arrayList) {
        Iterator<? extends MeasurementPointBase> it2 = arrayList.iterator();
        int i = 0;
        int i2 = 0;
        int i3 = 0;
        int i4 = 0;
        int i5 = 0;
        int i6 = 0;
        int i7 = 0;
        while (it2.hasNext()) {
            MeasurementPointBase measurementPointBase = (MeasurementPointBase) it2.next();
            ConnectionTypes connectionTypes = measurementPointBase.ConnectionType;
            if (connectionTypes != ConnectionTypes.Unknown) {
                if (connectionTypes == ConnectionTypes.Mobile) {
                    NetworkGenerations a2 = c.a(measurementPointBase.NetworkType);
                    if (measurementPointBase.NrState.equals("CONNECTED")) {
                        a2 = NetworkGenerations.Gen5;
                    }
                    int i8 = AnonymousClass1.f6072a[a2.ordinal()];
                    if (i8 == 1) {
                        i2++;
                    } else if (i8 == 2) {
                        i3++;
                    } else if (i8 == 3) {
                        i4++;
                    } else if (i8 == 4) {
                        i5++;
                    }
                } else {
                    i6++;
                }
                i++;
            }
            i7++;
            i++;
        }
        if (i > 0) {
            double d = (double) i;
            this.RatShare2G = ((double) i2) / d;
            this.RatShare3G = ((double) i3) / d;
            this.RatShare4G = ((double) i4) / d;
            this.RatShare5G = ((double) i5) / d;
            this.RatShareWiFi = ((double) i6) / d;
            this.RatShareUnknown = ((double) i7) / d;
        }
    }
}
