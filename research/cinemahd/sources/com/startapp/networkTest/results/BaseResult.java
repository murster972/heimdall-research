package com.startapp.networkTest.results;

public class BaseResult implements Cloneable {
    public String GUID = "";
    public String ProjectId = "";
    public String Version = "20200514123200";

    public BaseResult(String str, String str2) {
        this.ProjectId = str;
        this.GUID = str2;
    }

    /* access modifiers changed from: protected */
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
