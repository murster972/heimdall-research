package com.startapp.networkTest.e;

class a {

    /* renamed from: a  reason: collision with root package name */
    private long f6014a;

    a() {
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x0071  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.lang.String r11) {
        /*
            r10 = this;
            r0 = 0
            r1 = 0
            java.net.DatagramSocket r2 = new java.net.DatagramSocket     // Catch:{ Exception -> 0x005e }
            r2.<init>()     // Catch:{ Exception -> 0x005e }
            r1 = 10000(0x2710, float:1.4013E-41)
            r2.setSoTimeout(r1)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            java.net.InetAddress r11 = java.net.InetAddress.getByName(r11)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r1 = 48
            byte[] r3 = new byte[r1]     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            java.net.DatagramPacket r4 = new java.net.DatagramPacket     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r5 = 123(0x7b, float:1.72E-43)
            r4.<init>(r3, r1, r11, r5)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r11 = 27
            r3[r0] = r11     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            a((byte[]) r3)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r2.send(r4)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            java.net.DatagramPacket r11 = new java.net.DatagramPacket     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r11.<init>(r3, r1)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r2.receive(r11)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r2.close()     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r11 = 32
            long r4 = a(r3, r11)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r11 = 36
            long r6 = a(r3, r11)     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r8 = 2208988800(0x83aa7e80, double:1.091385478E-314)
            long r4 = r4 - r8
            r8 = 1000(0x3e8, double:4.94E-321)
            long r4 = r4 * r8
            long r6 = r6 * r8
            r8 = 4294967296(0x100000000, double:2.121995791E-314)
            long r6 = r6 / r8
            long r4 = r4 + r6
            r10.f6014a = r4     // Catch:{ Exception -> 0x0058, all -> 0x0056 }
            r2.close()
            r11 = 1
            return r11
        L_0x0056:
            r11 = move-exception
            goto L_0x006f
        L_0x0058:
            r11 = move-exception
            r1 = r2
            goto L_0x005f
        L_0x005b:
            r11 = move-exception
            r2 = r1
            goto L_0x006f
        L_0x005e:
            r11 = move-exception
        L_0x005f:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x005b }
            java.lang.String r3 = "request time failed: "
            r2.<init>(r3)     // Catch:{ all -> 0x005b }
            r2.append(r11)     // Catch:{ all -> 0x005b }
            if (r1 == 0) goto L_0x006e
            r1.close()
        L_0x006e:
            return r0
        L_0x006f:
            if (r2 == 0) goto L_0x0074
            r2.close()
        L_0x0074:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.networkTest.e.a.a(java.lang.String):boolean");
    }

    public final long a() {
        return this.f6014a;
    }

    private static long a(byte[] bArr, int i) {
        byte b = bArr[i];
        byte b2 = bArr[i + 1];
        byte b3 = bArr[i + 2];
        byte b4 = bArr[i + 3];
        byte b5 = b & true;
        int i2 = b;
        if (b5 == true) {
            i2 = (b & Byte.MAX_VALUE) + 128;
        }
        byte b6 = b2 & true;
        int i3 = b2;
        if (b6 == true) {
            i3 = (b2 & Byte.MAX_VALUE) + 128;
        }
        byte b7 = b3 & true;
        int i4 = b3;
        if (b7 == true) {
            i4 = (b3 & Byte.MAX_VALUE) + 128;
        }
        byte b8 = b4 & true;
        int i5 = b4;
        if (b8 == true) {
            i5 = (b4 & Byte.MAX_VALUE) + 128;
        }
        return (((long) i2) << 24) + (((long) i3) << 16) + (((long) i4) << 8) + ((long) i5);
    }

    private static void a(byte[] bArr) {
        for (int i = 40; i < 48; i++) {
            bArr[i] = 0;
        }
    }
}
