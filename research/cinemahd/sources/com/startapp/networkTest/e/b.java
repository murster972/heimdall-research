package com.startapp.networkTest.e;

import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import com.startapp.networkTest.c;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.enums.TimeSources;
import java.util.Date;
import java.util.TimeZone;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f6015a = "b";
    /* access modifiers changed from: private */
    public boolean b = false;
    /* access modifiers changed from: private */
    public boolean c = false;
    private boolean d = false;
    /* access modifiers changed from: private */
    public long e;
    /* access modifiers changed from: private */
    public long f = -1;
    /* access modifiers changed from: private */
    public long g = -1;
    private long h = -1;
    private long i = -1;
    /* access modifiers changed from: private */
    public a j = new a();

    class a extends AsyncTask<Void, Void, Void> {
        a() {
        }

        private Void a() {
            try {
                String unused = b.f6015a;
                if (b.this.j.a("0.de.pool.ntp.org")) {
                    long a2 = b.this.j.a();
                    if (a2 <= 1458564533202L || a2 >= 3468524400000L) {
                        return null;
                    }
                    long unused2 = b.this.f = SystemClock.elapsedRealtime();
                    long unused3 = b.this.g = a2;
                    String unused4 = b.f6015a;
                    new StringBuilder("Time: ").append(new Date(b.this.g).toString());
                    boolean unused5 = b.this.c = true;
                    return null;
                }
                String unused6 = b.f6015a;
                long unused7 = b.this.e = SystemClock.elapsedRealtime();
                return null;
            } catch (Exception unused8) {
                return null;
            }
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return a();
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            boolean unused = b.this.b = false;
        }

        /* access modifiers changed from: protected */
        public final void onPreExecute() {
            boolean unused = b.this.b = true;
        }
    }

    public b() {
        if (c.d().w()) {
            d();
        }
    }

    private void d() {
        if (Build.VERSION.SDK_INT < 11) {
            new a().execute(new Void[0]);
        } else {
            new a().executeOnExecutor(com.startapp.networkTest.threads.a.a().b(), new Void[0]);
        }
    }

    private void e() {
        if (c.d().w() && !this.b && SystemClock.elapsedRealtime() - this.e > 30000) {
            d();
        }
    }

    public static long b() {
        long j2;
        long elapsedRealtime;
        long j3;
        b b2 = c.b();
        if (b2.d && b2.h > b2.f) {
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
            j2 = b2.i;
            elapsedRealtime = SystemClock.elapsedRealtime();
            j3 = b2.h;
        } else if (b2.c) {
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
            j2 = b2.g;
            elapsedRealtime = SystemClock.elapsedRealtime();
            j3 = b2.f;
        } else {
            b2.e();
            return System.currentTimeMillis();
        }
        return j2 + (elapsedRealtime - j3);
    }

    public static TimeInfo a() {
        long j2;
        b b2 = c.b();
        TimeInfo timeInfo = new TimeInfo();
        timeInfo.IsSynced = b2.c || b2.d;
        if (b2.d && b2.h > b2.f) {
            j2 = b2.i + (SystemClock.elapsedRealtime() - b2.h);
            timeInfo.DeviceDriftMillis = System.currentTimeMillis() - j2;
            timeInfo.MillisSinceLastSync = j2 - b2.i;
            timeInfo.TimeSource = TimeSources.GPS;
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
        } else if (b2.c) {
            if (SystemClock.elapsedRealtime() - b2.f > 28800000) {
                b2.e();
            }
            j2 = b2.g + (SystemClock.elapsedRealtime() - b2.f);
            timeInfo.DeviceDriftMillis = System.currentTimeMillis() - j2;
            timeInfo.MillisSinceLastSync = j2 - b2.g;
            timeInfo.TimeSource = TimeSources.NTP;
        } else {
            b2.e();
            j2 = System.currentTimeMillis();
            timeInfo.TimeSource = TimeSources.Device;
        }
        timeInfo.TimestampTableau = com.iab.omid.library.startapp.b.a(j2);
        timeInfo.TimestampDateTime = com.iab.omid.library.startapp.b.b(j2);
        timeInfo.TimestampOffset = (double) (((((float) TimeZone.getDefault().getOffset(j2)) / 1000.0f) / 60.0f) / 60.0f);
        timeInfo.TimestampMillis = j2;
        com.startapp.networkTest.utils.a.a c2 = com.iab.omid.library.startapp.b.c(j2);
        timeInfo.year = c2.f6088a;
        timeInfo.month = c2.b;
        timeInfo.day = c2.c;
        timeInfo.hour = c2.d;
        timeInfo.minute = c2.e;
        timeInfo.second = c2.f;
        timeInfo.millisecond = c2.g;
        return timeInfo;
    }

    public final void a(Location location) {
        this.i = location.getTime();
        this.h = SystemClock.elapsedRealtime();
        this.d = true;
    }
}
