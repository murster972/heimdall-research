package com.startapp.sdk.b;

import android.content.Context;
import com.startapp.sdk.adsbase.j.d;
import com.startapp.sdk.adsbase.j.h;
import com.startapp.sdk.adsbase.j.u;
import java.lang.ref.SoftReference;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f6432a;
    private final String b;
    private final String[] c;
    private final Class[] d;
    private final Object[] e;
    private final String[] f;
    private transient SoftReference<c> g;
    private final transient Map<String, SoftReference<Map<String, Object>>> h = new ConcurrentHashMap();

    /* renamed from: com.startapp.sdk.b.a$a  reason: collision with other inner class name */
    static class C0079a implements Iterator<Object> {

        /* renamed from: a  reason: collision with root package name */
        private Object f6433a;
        private int b;
        private int c;

        public C0079a(Object obj, int i) {
            this.f6433a = obj;
            this.b = i;
        }

        public final boolean hasNext() {
            return this.c < this.b;
        }

        public final Object next() {
            Object obj = this.f6433a;
            int i = this.c;
            this.c = i + 1;
            return Array.get(obj, i);
        }
    }

    static class b implements Iterator<Object> {

        /* renamed from: a  reason: collision with root package name */
        static final b f6434a = new b();

        b() {
        }

        public final boolean hasNext() {
            return false;
        }

        public final Object next() {
            return null;
        }
    }

    public a(String str, String str2, String[] strArr, Class[] clsArr, Object[] objArr, String[] strArr2) {
        this.f6432a = str;
        this.b = str2;
        this.c = strArr;
        this.d = clsArr;
        this.e = objArr;
        this.f = strArr2;
    }

    public final String a() {
        return this.f6432a;
    }

    public final String b() {
        return this.b;
    }

    public final String[] c() {
        return this.c;
    }

    public final Object[] d() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && a.class == obj.getClass()) {
            a aVar = (a) obj;
            return u.b(this.f6432a, aVar.f6432a) && u.b(this.b, aVar.b) && Arrays.equals(this.c, aVar.c) && Arrays.equals(this.e, aVar.e) && Arrays.equals(this.f, aVar.f);
        }
    }

    public int hashCode() {
        return u.a(this.f6432a, this.b, this.c, this.e, this.f);
    }

    public String toString() {
        return super.toString();
    }

    public final JSONArray a(Context context, int[] iArr, Integer num) {
        Iterator it2;
        try {
            Object a2 = a(context).a(this.e);
            if (a2 == null) {
                it2 = b.f6434a;
            } else if (a2 instanceof Collection) {
                it2 = ((Collection) a2).iterator();
            } else if (a2.getClass().isArray()) {
                it2 = new C0079a(a2, Array.getLength(a2));
            } else {
                it2 = Collections.singleton(a2).iterator();
            }
            List<JSONObject> arrayList = new ArrayList<>();
            while (true) {
                Map<String, Object> map = null;
                if (!it2.hasNext()) {
                    break;
                }
                Object next = it2.next();
                if (next != null) {
                    JSONObject jSONObject = new JSONObject();
                    Class<?> cls = next.getClass();
                    SoftReference softReference = this.h.get(cls.getName());
                    if (softReference != null) {
                        map = (Map) softReference.get();
                    }
                    if (map == null) {
                        map = a(cls, this.f);
                        this.h.put(cls.getName(), new SoftReference(map));
                    }
                    for (Map.Entry next2 : map.entrySet()) {
                        String str = (String) next2.getKey();
                        Object value = next2.getValue();
                        try {
                            if (value instanceof Field) {
                                jSONObject.put(str, a(((Field) value).get(next)));
                            } else if (value instanceof Method) {
                                jSONObject.put(str, a(((Method) value).invoke(next, new Object[0])));
                            }
                        } catch (Throwable unused) {
                        }
                    }
                    arrayList.add(jSONObject);
                }
            }
            if (iArr != null && iArr.length > 0) {
                int length = this.f.length;
                d dVar = null;
                for (int i : iArr) {
                    if (i != 0 && Math.abs(i) <= length) {
                        Comparator hVar = new h(this.f[Math.abs(i) - 1]);
                        if (i < 0) {
                            hVar = Collections.reverseOrder(hVar);
                        }
                        if (dVar == null) {
                            dVar = hVar;
                        } else {
                            dVar = new d(dVar, hVar);
                        }
                    }
                }
                if (dVar != null) {
                    Collections.sort(arrayList, dVar);
                }
            }
            if (num != null && num.intValue() > 0) {
                arrayList = arrayList.subList(0, Math.min(num.intValue(), arrayList.size()));
            }
            JSONArray jSONArray = new JSONArray();
            for (JSONObject put : arrayList) {
                jSONArray.put(put);
            }
            return jSONArray;
        } catch (InvocationTargetException e2) {
            throw new RuntimeException("5", e2);
        } catch (IllegalAccessException e3) {
            throw new RuntimeException("5", e3);
        }
    }

    private static Object a(Object obj) {
        if (obj instanceof Short) {
            return Integer.valueOf(((Short) obj).intValue());
        }
        if ((obj instanceof Integer) || (obj instanceof Long)) {
            return obj;
        }
        if (obj instanceof Float) {
            return Double.valueOf(((Float) obj).doubleValue());
        }
        if ((obj instanceof Double) || (obj instanceof Boolean) || (obj instanceof String)) {
            return obj;
        }
        if (obj != null) {
            return obj.toString();
        }
        return null;
    }

    private c a(Context context) {
        SoftReference<c> softReference = this.g;
        c cVar = softReference != null ? softReference.get() : null;
        if (cVar != null) {
            return cVar;
        }
        Object systemService = context.getApplicationContext().getSystemService(this.f6432a);
        if (systemService == null) {
            try {
                Object obj = a((Class<?>) Context.class, new String[]{this.f6432a}).get(this.f6432a);
                if (obj instanceof Method) {
                    systemService = ((Method) obj).invoke(context.getApplicationContext(), new Object[0]);
                } else if (obj instanceof Field) {
                    systemService = ((Field) obj).get(context.getApplicationContext());
                }
            } catch (Throwable unused) {
            }
        }
        if (systemService != null) {
            try {
                Method a2 = a(systemService.getClass(), this.b, this.d);
                if (!a2.isAccessible()) {
                    try {
                        a2.setAccessible(true);
                    } catch (SecurityException e2) {
                        throw new RuntimeException("4", e2);
                    }
                }
                c cVar2 = new c(systemService, a2);
                this.g = new SoftReference<>(cVar2);
                return cVar2;
            } catch (NoSuchMethodException e3) {
                throw new RuntimeException("3", e3);
            }
        } else {
            throw new RuntimeException(DiskLruCache.VERSION_1);
        }
    }

    private static Method a(Class<?> cls, String str, Class[] clsArr) throws NoSuchMethodException {
        NoSuchMethodException noSuchMethodException = null;
        Class<? super Object> cls2 = cls;
        while (cls2 != null) {
            try {
                return cls2.getDeclaredMethod(str, clsArr);
            } catch (NoSuchMethodException e2) {
                if (noSuchMethodException == null) {
                    noSuchMethodException = e2;
                }
                cls2 = cls2.getSuperclass();
            }
        }
        throw noSuchMethodException;
    }

    private static Field a(Class<?> cls, String str) throws NoSuchFieldException {
        NoSuchFieldException noSuchFieldException = null;
        Class<? super Object> cls2 = cls;
        while (cls2 != null) {
            try {
                return cls2.getDeclaredField(str);
            } catch (NoSuchFieldException e2) {
                if (noSuchFieldException == null) {
                    noSuchFieldException = e2;
                }
                cls2 = cls2.getSuperclass();
            }
        }
        throw noSuchFieldException;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0076, code lost:
        r0.put(r4, r6);
     */
    /* JADX WARNING: Removed duplicated region for block: B:23:? A[ExcHandler: SecurityException (unused java.lang.SecurityException), SYNTHETIC, Splitter:B:17:0x005b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.util.Map<java.lang.String, java.lang.Object> a(java.lang.Class<?> r10, java.lang.String[] r11) {
        /*
            java.util.LinkedHashMap r0 = new java.util.LinkedHashMap
            r0.<init>()
            int r1 = r11.length
            r2 = 0
            r3 = 0
        L_0x0008:
            if (r3 >= r1) goto L_0x007c
            r4 = r11[r3]
            r5 = 1
            java.lang.reflect.Field r6 = a((java.lang.Class<?>) r10, (java.lang.String) r4)     // Catch:{ NoSuchFieldException -> 0x0020, SecurityException -> 0x001e }
            boolean r7 = r6.isAccessible()     // Catch:{ NoSuchFieldException -> 0x0020, SecurityException -> 0x001e }
            if (r7 != 0) goto L_0x001a
            r6.setAccessible(r5)     // Catch:{ NoSuchFieldException -> 0x0020, SecurityException -> 0x001e }
        L_0x001a:
            r0.put(r4, r6)     // Catch:{ NoSuchFieldException -> 0x0020, SecurityException -> 0x001e }
            goto L_0x0079
        L_0x001e:
            r6 = move-exception
            goto L_0x0021
        L_0x0020:
            r6 = move-exception
        L_0x0021:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            char r8 = r4.charAt(r2)
            char r8 = java.lang.Character.toUpperCase(r8)
            r7.append(r8)
            java.lang.String r8 = r4.substring(r5)
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            java.lang.String r8 = "get"
            java.lang.String r9 = java.lang.String.valueOf(r7)     // Catch:{ NoSuchMethodException -> 0x0059 }
            java.lang.String r8 = r8.concat(r9)     // Catch:{ NoSuchMethodException -> 0x0059 }
            java.lang.Class[] r9 = new java.lang.Class[r2]     // Catch:{ NoSuchMethodException -> 0x0059 }
            java.lang.reflect.Method r8 = a((java.lang.Class<?>) r10, (java.lang.String) r8, (java.lang.Class[]) r9)     // Catch:{ NoSuchMethodException -> 0x0059 }
            boolean r9 = r8.isAccessible()     // Catch:{ NoSuchMethodException -> 0x0059 }
            if (r9 != 0) goto L_0x0055
            r8.setAccessible(r5)     // Catch:{ NoSuchMethodException -> 0x0059 }
        L_0x0055:
            r0.put(r4, r8)     // Catch:{ NoSuchMethodException -> 0x0059 }
            goto L_0x0079
        L_0x0059:
            java.lang.String r8 = "is"
            java.lang.String r7 = java.lang.String.valueOf(r7)     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
            java.lang.String r7 = r8.concat(r7)     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
            java.lang.Class[] r8 = new java.lang.Class[r2]     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
            java.lang.reflect.Method r7 = a((java.lang.Class<?>) r10, (java.lang.String) r7, (java.lang.Class[]) r8)     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
            boolean r8 = r7.isAccessible()     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
            if (r8 != 0) goto L_0x0072
            r7.setAccessible(r5)     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
        L_0x0072:
            r0.put(r4, r7)     // Catch:{ SecurityException -> 0x0076, SecurityException -> 0x0076 }
            goto L_0x0079
        L_0x0076:
            r0.put(r4, r6)
        L_0x0079:
            int r3 = r3 + 1
            goto L_0x0008
        L_0x007c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.b.a.a(java.lang.Class, java.lang.String[]):java.util.Map");
    }
}
