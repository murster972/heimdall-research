package com.startapp.sdk.b;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    public String f6435a;
    public String b;

    public b(String str, String str2) {
        this.f6435a = str;
        this.b = str2;
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* JADX WARNING: Code restructure failed: missing block: B:133:0x01f1, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x01f7, code lost:
        throw new java.lang.IllegalArgumentException(r1, r0);
     */
    /* JADX WARNING: Removed duplicated region for block: B:133:0x01f1 A[ExcHandler: IOException (r0v1 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:1:0x0002] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.startapp.sdk.b.a a(java.lang.String r18) {
        /*
            r1 = r18
            android.util.JsonReader r0 = new android.util.JsonReader     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.io.StringReader r2 = new java.io.StringReader     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r2.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r2)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.util.List r0 = com.startapp.sdk.adsbase.j.j.b(r0)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r2 = 0
            java.lang.Object r3 = r0.get(r2)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r5 = r3
            java.lang.String r5 = (java.lang.String) r5     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r5 == 0) goto L_0x01eb
            r3 = 1
            java.lang.Object r4 = r0.get(r3)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r6 = r4
            java.lang.String r6 = (java.lang.String) r6     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r6 == 0) goto L_0x01e5
            r4 = 2
            java.lang.Object r7 = r0.get(r4)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.util.List r7 = (java.util.List) r7     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r7 == 0) goto L_0x01df
            int r8 = r7.size()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.String[] r9 = new java.lang.String[r8]     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.Class[] r10 = new java.lang.Class[r8]     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.Object[] r11 = new java.lang.Object[r8]     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r12 = 0
        L_0x0038:
            r13 = 3
            if (r12 >= r8) goto L_0x01a6
            java.lang.Object r14 = r7.get(r12)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.util.Map r14 = (java.util.Map) r14     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x01a0
            int r15 = r14.size()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r15 != r3) goto L_0x019a
            java.util.Set r14 = r14.entrySet()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.util.Iterator r14 = r14.iterator()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.Object r14 = r14.next()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.util.Map$Entry r14 = (java.util.Map.Entry) r14     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.Object r15 = r14.getKey()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.String r15 = (java.lang.String) r15     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r15 == 0) goto L_0x0194
            r16 = -1
            int r17 = r15.hashCode()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            switch(r17) {
                case -1808118735: goto L_0x00c7;
                case -1325958191: goto L_0x00bd;
                case -891985903: goto L_0x00b2;
                case 104431: goto L_0x00a8;
                case 3039496: goto L_0x009e;
                case 3052374: goto L_0x0094;
                case 3327612: goto L_0x008a;
                case 64711720: goto L_0x007f;
                case 97526364: goto L_0x0074;
                case 109413500: goto L_0x006a;
                default: goto L_0x0068;
            }     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
        L_0x0068:
            goto L_0x00d2
        L_0x006a:
            java.lang.String r13 = "short"
            boolean r13 = r15.equals(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r13 == 0) goto L_0x00d2
            r2 = 1
            goto L_0x00d3
        L_0x0074:
            java.lang.String r13 = "float"
            boolean r13 = r15.equals(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r13 == 0) goto L_0x00d2
            r13 = 4
            r2 = 4
            goto L_0x00d3
        L_0x007f:
            java.lang.String r13 = "boolean"
            boolean r13 = r15.equals(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r13 == 0) goto L_0x00d2
            r13 = 6
            r2 = 6
            goto L_0x00d3
        L_0x008a:
            java.lang.String r2 = "long"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 3
            goto L_0x00d3
        L_0x0094:
            java.lang.String r2 = "char"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 7
            goto L_0x00d3
        L_0x009e:
            java.lang.String r2 = "byte"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 0
            goto L_0x00d3
        L_0x00a8:
            java.lang.String r2 = "int"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 2
            goto L_0x00d3
        L_0x00b2:
            java.lang.String r2 = "string"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 8
            goto L_0x00d3
        L_0x00bd:
            java.lang.String r2 = "double"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 5
            goto L_0x00d3
        L_0x00c7:
            java.lang.String r2 = "String"
            boolean r2 = r15.equals(r2)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 == 0) goto L_0x00d2
            r2 = 9
            goto L_0x00d3
        L_0x00d2:
            r2 = -1
        L_0x00d3:
            switch(r2) {
                case 0: goto L_0x00f3;
                case 1: goto L_0x00f0;
                case 2: goto L_0x00ed;
                case 3: goto L_0x00ea;
                case 4: goto L_0x00e7;
                case 5: goto L_0x00e4;
                case 6: goto L_0x00e1;
                case 7: goto L_0x00de;
                case 8: goto L_0x00db;
                case 9: goto L_0x00db;
                default: goto L_0x00d6;
            }     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
        L_0x00d6:
            java.lang.Class r2 = java.lang.Class.forName(r15)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00db:
            java.lang.Class<java.lang.String> r2 = java.lang.String.class
            goto L_0x00f5
        L_0x00de:
            java.lang.Class r2 = java.lang.Character.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00e1:
            java.lang.Class r2 = java.lang.Boolean.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00e4:
            java.lang.Class r2 = java.lang.Double.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00e7:
            java.lang.Class r2 = java.lang.Float.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00ea:
            java.lang.Class r2 = java.lang.Long.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00ed:
            java.lang.Class r2 = java.lang.Integer.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00f0:
            java.lang.Class r2 = java.lang.Short.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x00f5
        L_0x00f3:
            java.lang.Class r2 = java.lang.Byte.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
        L_0x00f5:
            java.lang.Object r13 = r14.getValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Class r14 = java.lang.Byte.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 != r14) goto L_0x010d
            boolean r14 = r13 instanceof java.lang.Number     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x0177
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            byte r13 = r13.byteValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Byte r13 = java.lang.Byte.valueOf(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x010d:
            java.lang.Class r14 = java.lang.Short.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 != r14) goto L_0x0120
            boolean r14 = r13 instanceof java.lang.Number     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x0177
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            short r13 = r13.shortValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Short r13 = java.lang.Short.valueOf(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x0120:
            java.lang.Class r14 = java.lang.Integer.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 != r14) goto L_0x0133
            boolean r14 = r13 instanceof java.lang.Number     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x0177
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            int r13 = r13.intValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x0133:
            java.lang.Class r14 = java.lang.Long.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 != r14) goto L_0x0146
            boolean r14 = r13 instanceof java.lang.Number     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x0177
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            long r13 = r13.longValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Long r13 = java.lang.Long.valueOf(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x0146:
            java.lang.Class r14 = java.lang.Float.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 != r14) goto L_0x0159
            boolean r14 = r13 instanceof java.lang.Number     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x0177
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            float r13 = r13.floatValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Float r13 = java.lang.Float.valueOf(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x0159:
            java.lang.Class r14 = java.lang.Double.TYPE     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r2 != r14) goto L_0x016c
            boolean r14 = r13 instanceof java.lang.Number     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            if (r14 == 0) goto L_0x0177
            java.lang.Number r13 = (java.lang.Number) r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            double r13 = r13.doubleValue()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            java.lang.Double r13 = java.lang.Double.valueOf(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x016c:
            java.lang.Class<java.lang.String> r14 = java.lang.String.class
            if (r2 != r14) goto L_0x0177
            if (r13 == 0) goto L_0x0177
            java.lang.String r13 = r13.toString()     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            goto L_0x017b
        L_0x0177:
            java.lang.Object r13 = r2.cast(r13)     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
        L_0x017b:
            r9[r12] = r15     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            r10[r12] = r2     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            r11[r12] = r13     // Catch:{ ClassCastException -> 0x018d, ClassNotFoundException -> 0x0186, IOException -> 0x01f1 }
            int r12 = r12 + 1
            r2 = 0
            goto L_0x0038
        L_0x0186:
            r0 = move-exception
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r2.<init>(r1, r0)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r2     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x018d:
            r0 = move-exception
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r2.<init>(r1, r0)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r2     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x0194:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x019a:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01a0:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01a6:
            java.lang.Object r0 = r0.get(r13)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.util.List r0 = (java.util.List) r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r0 == 0) goto L_0x01d9
            int r2 = r0.size()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.String[] r2 = new java.lang.String[r2]     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            int r3 = r0.size()     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r4 = 0
        L_0x01b9:
            if (r4 >= r3) goto L_0x01ce
            java.lang.Object r7 = r0.get(r4)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            java.lang.String r7 = (java.lang.String) r7     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            if (r7 == 0) goto L_0x01c8
            r2[r4] = r7     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            int r4 = r4 + 1
            goto L_0x01b9
        L_0x01c8:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01ce:
            com.startapp.sdk.b.a r0 = new com.startapp.sdk.b.a     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r4 = r0
            r7 = r9
            r8 = r10
            r9 = r11
            r10 = r2
            r4.<init>(r5, r6, r7, r8, r9, r10)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            return r0
        L_0x01d9:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01df:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01e5:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01eb:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            r0.<init>(r1)     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
            throw r0     // Catch:{ ClassCastException -> 0x01f8, IOException -> 0x01f1 }
        L_0x01f1:
            r0 = move-exception
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            r2.<init>(r1, r0)
            throw r2
        L_0x01f8:
            r0 = move-exception
            java.lang.IllegalArgumentException r2 = new java.lang.IllegalArgumentException
            r2.<init>(r1, r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.b.b.a(java.lang.String):com.startapp.sdk.b.a");
    }
}
