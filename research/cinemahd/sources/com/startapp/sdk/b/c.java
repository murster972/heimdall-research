package com.startapp.sdk.b;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Object f6436a;
    private final Method b;

    public c(Object obj, Method method) {
        this.f6436a = obj;
        this.b = method;
    }

    public final Object a(Object[] objArr) throws InvocationTargetException, IllegalAccessException {
        return this.b.invoke(this.f6436a, objArr);
    }
}
