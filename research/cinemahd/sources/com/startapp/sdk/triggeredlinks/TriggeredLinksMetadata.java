package com.startapp.sdk.triggeredlinks;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TriggeredLinksMetadata implements Serializable {
    private static final long serialVersionUID = 1082253451843108611L;
    @d(a = true, f = "AppEvents")
    private AppEventsMetadata appEvents;
    private boolean enabled = true;
    private int ief;
    private int smartRedirectTimeoutInSec = 10;
    @d(b = ArrayList.class, c = Integer.class, f = "ThrottlingPublisherIDs")
    private List<Integer> throttlingPublisherIDs;
    private boolean triggeredLinkInfoEvent = true;

    public final boolean a() {
        return this.enabled;
    }

    public final int b() {
        return this.smartRedirectTimeoutInSec;
    }

    public final boolean c() {
        return this.triggeredLinkInfoEvent;
    }

    public final AppEventsMetadata d() {
        return this.appEvents;
    }

    public final List<Integer> e() {
        return this.throttlingPublisherIDs;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && TriggeredLinksMetadata.class == obj.getClass()) {
            TriggeredLinksMetadata triggeredLinksMetadata = (TriggeredLinksMetadata) obj;
            return this.enabled == triggeredLinksMetadata.enabled && this.smartRedirectTimeoutInSec == triggeredLinksMetadata.smartRedirectTimeoutInSec && this.triggeredLinkInfoEvent == triggeredLinksMetadata.triggeredLinkInfoEvent && this.ief == triggeredLinksMetadata.ief && u.b(this.appEvents, triggeredLinksMetadata.appEvents) && u.b(this.throttlingPublisherIDs, triggeredLinksMetadata.throttlingPublisherIDs);
        }
    }

    public final int f() {
        return this.ief;
    }

    public int hashCode() {
        return u.a(Boolean.valueOf(this.enabled), Integer.valueOf(this.smartRedirectTimeoutInSec), Boolean.valueOf(this.triggeredLinkInfoEvent), this.appEvents, this.throttlingPublisherIDs, Integer.valueOf(this.ief));
    }
}
