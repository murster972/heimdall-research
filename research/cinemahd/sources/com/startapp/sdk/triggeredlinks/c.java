package com.startapp.sdk.triggeredlinks;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.JsonReader;
import android.util.MalformedJsonException;
import android.webkit.WebSettings;
import com.facebook.common.time.Clock;
import com.startapp.common.a.d;
import com.startapp.common.jobrunner.RunnerRequest;
import com.startapp.common.jobrunner.a;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.j.j;
import com.startapp.sdk.adsbase.j.o;
import com.startapp.sdk.adsbase.j.u;
import com.vungle.warren.model.ReportDBAdapter;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import okhttp3.internal.cache.DiskLruCache;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f6499a;
    protected final Runnable b = new Runnable() {
        public final void run() {
            c.this.e();
        }
    };
    private final SharedPreferences c;
    private Executor d;
    private final Handler e;
    private final Map<String, Long> f;
    private final d g;
    private final g<TriggeredLinksMetadata> h;

    public c(Context context, SharedPreferences sharedPreferences, Executor executor, d dVar, g<TriggeredLinksMetadata> gVar) {
        this.f6499a = context;
        this.c = sharedPreferences;
        this.d = new o(executor);
        this.e = new Handler(Looper.getMainLooper());
        this.f = new HashMap();
        this.g = dVar;
        this.h = gVar;
    }

    private String f() {
        String a2 = this.g.b().a();
        return a2.equals("0") ? "00000000-0000-0000-0000-000000000000" : a2;
    }

    private String g() {
        return this.g.b().c() ? DiskLruCache.VERSION_1 : "0";
    }

    private TriggeredLinksMetadata h() {
        TriggeredLinksMetadata a2 = this.h.a();
        if (a2 == null || !a2.a()) {
            return null;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i) {
        TriggeredLinksMetadata h2 = h();
        return h2 != null && (h2.f() & i) == i;
    }

    public final void b() {
        TriggeredLinksMetadata h2 = h();
        Map<String, String> map = null;
        AppEventsMetadata d2 = h2 != null ? h2.d() : null;
        if (d2 != null) {
            map = d2.b();
        }
        if (map != null) {
            b(h2, map, "Active");
        }
    }

    public final void c() {
        TriggeredLinksMetadata h2 = h();
        Map<String, String> map = null;
        AppEventsMetadata d2 = h2 != null ? h2.d() : null;
        if (d2 != null) {
            map = d2.c();
        }
        if (map != null) {
            b(h2, map, "Inactive");
        }
    }

    public final void d() {
        a.a(this.f6499a);
        a.a((com.startapp.common.jobrunner.interfaces.a) new b());
        e();
    }

    /* access modifiers changed from: protected */
    public final void e() {
        String a2;
        this.e.removeCallbacks(this.b);
        a.a(1347213260, false);
        TriggeredLinksMetadata h2 = h();
        Map<String, PeriodicAppEventMetadata> map = null;
        AppEventsMetadata d2 = h2 != null ? h2.d() : null;
        if (d2 != null) {
            map = d2.d();
        }
        if (map != null && map.size() > 0) {
            SharedPreferences.Editor edit = this.c.edit();
            long currentTimeMillis = System.currentTimeMillis();
            long j = Clock.MAX_TIME;
            for (Map.Entry next : map.entrySet()) {
                String str = (String) next.getKey();
                PeriodicAppEventMetadata periodicAppEventMetadata = (PeriodicAppEventMetadata) next.getValue();
                if (!(str == null || str.length() <= 0 || periodicAppEventMetadata == null || (a2 = periodicAppEventMetadata.a()) == null || a2.length() <= 0)) {
                    int b2 = periodicAppEventMetadata.b();
                    if (b2 < 5) {
                        b2 = 5;
                    }
                    long j2 = this.c.getLong(str, 0);
                    if (j2 > currentTimeMillis) {
                        edit.putLong(str, j2);
                        if (j > j2) {
                            j = j2;
                        }
                    } else {
                        edit.putLong(str, ((long) (b2 * 1000)) + currentTimeMillis);
                        a(h2, str, a2, b2);
                    }
                }
            }
            if (Build.VERSION.SDK_INT >= 9) {
                edit.apply();
            } else {
                edit.commit();
            }
            if (j != Clock.MAX_TIME) {
                long j3 = j - currentTimeMillis;
                if (j3 < 5000) {
                    a(j3);
                } else {
                    a.a(new RunnerRequest.a(1347213260).a(j3).a(RunnerRequest.NetworkType.b).b());
                }
            }
        }
    }

    public final void a() {
        TriggeredLinksMetadata h2 = h();
        Map<String, String> map = null;
        AppEventsMetadata d2 = h2 != null ? h2.d() : null;
        if (d2 != null) {
            map = d2.a();
        }
        if (map != null) {
            b(h2, map, "Launch");
        }
    }

    private void b(final TriggeredLinksMetadata triggeredLinksMetadata, final Map<String, String> map, final String str) {
        this.d.execute(new Runnable() {
            public final void run() {
                try {
                    c.this.a(triggeredLinksMetadata, (Map<String, String>) map, str);
                } catch (Throwable th) {
                    if (c.this.a(2)) {
                        new e(th).a(c.this.f6499a);
                    }
                }
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(long j) {
        if (j > 0) {
            this.e.postDelayed(this.b, j);
        } else {
            this.e.post(this.b);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(final RunnerJob.a aVar) {
        if (aVar != null) {
            this.e.post(new Runnable() {
                public final void run() {
                    try {
                        c.this.b.run();
                    } finally {
                        aVar.a(RunnerJob.Result.SUCCESS);
                    }
                }
            });
        } else {
            this.e.post(this.b);
        }
    }

    private void a(TriggeredLinksMetadata triggeredLinksMetadata, String str, String str2, int i) {
        final TriggeredLinksMetadata triggeredLinksMetadata2 = triggeredLinksMetadata;
        final String str3 = str;
        final String str4 = str2;
        final int i2 = i;
        this.d.execute(new Runnable() {
            public final void run() {
                try {
                    c.this.a(triggeredLinksMetadata2, "Periodic", str3, str4);
                } catch (Throwable th) {
                    c.this.a(str3, i2);
                    c.this.a(0);
                    throw th;
                }
                c.this.a(str3, i2);
                c.this.a(0);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final void a(String str, int i) {
        SharedPreferences.Editor edit = this.c.edit();
        edit.putLong(str, System.currentTimeMillis() + ((long) (i * 1000)));
        if (Build.VERSION.SDK_INT >= 9) {
            edit.apply();
        } else {
            edit.commit();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(TriggeredLinksMetadata triggeredLinksMetadata, Map<String, String> map, String str) {
        for (Map.Entry next : map.entrySet()) {
            String str2 = (String) next.getKey();
            String str3 = (String) next.getValue();
            if (str2 != null && str2.length() > 0 && str3 != null && str3.length() > 0) {
                List<Integer> e2 = triggeredLinksMetadata.e();
                boolean z = false;
                if (e2 != null) {
                    Iterator<Integer> it2 = e2.iterator();
                    while (true) {
                        if (!it2.hasNext()) {
                            break;
                        } else if (str2.equals(String.valueOf(it2.next()))) {
                            Long l = this.f.get(a(str2, str));
                            if (l != null && l.longValue() > SystemClock.elapsedRealtime()) {
                                z = true;
                            }
                        }
                    }
                }
                if (!z) {
                    try {
                        a(triggeredLinksMetadata, str, str2, str3);
                    } catch (Throwable th) {
                        if (a(4)) {
                            new e(th).a(this.f6499a);
                        }
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(TriggeredLinksMetadata triggeredLinksMetadata, String str, String str2, String str3) throws IOException, JSONException {
        URLConnection uRLConnection;
        int i;
        String uri = a(Uri.parse(str3)).toString();
        InputStream inputStream = null;
        try {
            uRLConnection = new URL(uri).openConnection();
            try {
                if (uRLConnection instanceof HttpURLConnection) {
                    HttpURLConnection httpURLConnection = (HttpURLConnection) uRLConnection;
                    httpURLConnection.setInstanceFollowRedirects(true);
                    httpURLConnection.setReadTimeout(triggeredLinksMetadata.b() * 1000);
                    httpURLConnection.setConnectTimeout(triggeredLinksMetadata.b() * 1000);
                    if (Build.VERSION.SDK_INT >= 17) {
                        httpURLConnection.setRequestProperty("User-Agent", WebSettings.getDefaultUserAgent(this.f6499a));
                    }
                    httpURLConnection.connect();
                    i = httpURLConnection.getResponseCode();
                    inputStream = httpURLConnection.getInputStream();
                    if (Build.VERSION.SDK_INT >= 11) {
                        a(str, str2, inputStream);
                    }
                } else {
                    i = 0;
                }
                u.a((Closeable) inputStream);
                a(uRLConnection);
                if (triggeredLinksMetadata.c() && i / 100 == 2) {
                    new e(InfoEventCategory.TRIGGERED_LINK).f(str2).i().g(new JSONObject().put("eventType", str).put(ReportDBAdapter.ReportColumns.COLUMN_URL, uri).toString()).a(this.f6499a);
                }
            } catch (Throwable th) {
                th = th;
                u.a((Closeable) null);
                a(uRLConnection);
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            uRLConnection = null;
            u.a((Closeable) null);
            a(uRLConnection);
            throw th;
        }
    }

    private void a(String str, String str2, InputStream inputStream) throws IOException {
        try {
            Object a2 = j.a(new JsonReader(new InputStreamReader(inputStream)));
            if (a2 instanceof Map) {
                Object obj = ((Map) a2).get("throttleSec");
                if (obj instanceof Number) {
                    a(str2, str, ((Number) obj).intValue());
                }
            }
        } catch (IOException e2) {
            if (!(e2 instanceof MalformedJsonException)) {
                throw e2;
            }
        }
    }

    private static String a(String str, String str2) {
        return str + "-" + str2;
    }

    private void a(String str, String str2, int i) {
        this.f.put(a(str, str2), Long.valueOf(SystemClock.elapsedRealtime() + ((long) (i * 1000))));
    }

    private Uri a(Uri uri) {
        String queryParameter;
        Uri.Builder buildUpon = uri.buildUpon();
        if (Build.VERSION.SDK_INT >= 11) {
            buildUpon.query((String) null);
            for (String next : uri.getQueryParameterNames()) {
                if (!(next == null || (queryParameter = uri.getQueryParameter(next)) == null)) {
                    char c2 = 65535;
                    int hashCode = queryParameter.hashCode();
                    if (hashCode != -1089147532) {
                        if (hashCode != 613582261) {
                            if (hashCode == 1311708630 && queryParameter.equals("startapp_advertising_id")) {
                                c2 = 1;
                            }
                        } else if (queryParameter.equals("startapp_no_tracking")) {
                            c2 = 0;
                        }
                    } else if (queryParameter.equals("startapp_package_id")) {
                        c2 = 2;
                    }
                    if (c2 == 0) {
                        buildUpon.appendQueryParameter(next, g());
                    } else if (c2 == 1) {
                        buildUpon.appendQueryParameter(next, f());
                    } else if (c2 != 2) {
                        buildUpon.appendQueryParameter(next, queryParameter);
                    } else {
                        buildUpon.appendQueryParameter(next, this.f6499a.getPackageName());
                    }
                }
            }
        }
        return buildUpon.build();
    }

    private static void a(URLConnection uRLConnection) {
        try {
            if (uRLConnection instanceof HttpURLConnection) {
                ((HttpURLConnection) uRLConnection).disconnect();
            }
        } catch (Throwable unused) {
        }
    }
}
