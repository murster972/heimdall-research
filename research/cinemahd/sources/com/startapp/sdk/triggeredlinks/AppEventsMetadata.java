package com.startapp.sdk.triggeredlinks;

import com.startapp.common.parser.d;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class AppEventsMetadata implements Serializable {
    private static final long serialVersionUID = -5670027899854165615L;
    @d(b = HashMap.class)
    private Map<String, String> active;
    @d(b = HashMap.class)
    private Map<String, String> inactive;
    @d(b = HashMap.class)
    private Map<String, String> launch;
    @d(b = HashMap.class, c = PeriodicAppEventMetadata.class)
    private Map<String, PeriodicAppEventMetadata> periodic;

    public final Map<String, String> a() {
        return this.launch;
    }

    public final Map<String, String> b() {
        return this.active;
    }

    public final Map<String, String> c() {
        return this.inactive;
    }

    public final Map<String, PeriodicAppEventMetadata> d() {
        return this.periodic;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AppEventsMetadata.class == obj.getClass()) {
            AppEventsMetadata appEventsMetadata = (AppEventsMetadata) obj;
            Map<String, String> map = this.launch;
            if (map == null ? appEventsMetadata.launch != null : !map.equals(appEventsMetadata.launch)) {
                return false;
            }
            Map<String, String> map2 = this.active;
            if (map2 == null ? appEventsMetadata.active != null : !map2.equals(appEventsMetadata.active)) {
                return false;
            }
            Map<String, String> map3 = this.inactive;
            if (map3 == null ? appEventsMetadata.inactive != null : !map3.equals(appEventsMetadata.inactive)) {
                return false;
            }
            Map<String, PeriodicAppEventMetadata> map4 = this.periodic;
            Map<String, PeriodicAppEventMetadata> map5 = appEventsMetadata.periodic;
            if (map4 != null) {
                return map4.equals(map5);
            }
            if (map5 == null) {
                return true;
            }
        }
        return false;
    }

    public int hashCode() {
        Map<String, String> map = this.launch;
        int i = 0;
        int hashCode = (map != null ? map.hashCode() : 0) * 31;
        Map<String, String> map2 = this.active;
        int hashCode2 = (hashCode + (map2 != null ? map2.hashCode() : 0)) * 31;
        Map<String, String> map3 = this.inactive;
        int hashCode3 = (hashCode2 + (map3 != null ? map3.hashCode() : 0)) * 31;
        Map<String, PeriodicAppEventMetadata> map4 = this.periodic;
        if (map4 != null) {
            i = map4.hashCode();
        }
        return hashCode3 + i;
    }
}
