package com.startapp.sdk.g.b;

import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    public int f6480a;
    public String b;

    public static f a(Object obj) {
        InfoEventCategory a2;
        InfoEventCategory a3;
        if (!(obj instanceof Map)) {
            return new f();
        }
        Map map = (Map) obj;
        Object obj2 = map.get("type");
        Object obj3 = map.get("params");
        if (obj2 instanceof Number) {
            int intValue = ((Number) obj2).intValue();
            if (intValue != 1) {
                if (intValue != 2) {
                    if (intValue != 3) {
                        if (intValue == 4 && (obj3 instanceof List)) {
                            List list = (List) obj3;
                            if (list.size() > 0 && (a3 = InfoEventCategory.a(String.valueOf(list.get(0)))) != null) {
                                return new c(a3, list.size() > 1 ? String.valueOf(list.get(1)) : null);
                            }
                        }
                    } else if (obj3 instanceof Map) {
                        Map map2 = (Map) obj3;
                        Object obj4 = map2.get("action");
                        if (obj4 instanceof String) {
                            Object obj5 = map2.get("extras");
                            HashMap hashMap = new HashMap();
                            if (obj5 instanceof Map) {
                                for (Map.Entry entry : ((Map) obj5).entrySet()) {
                                    Object key = entry.getKey();
                                    if (key instanceof String) {
                                        hashMap.put((String) key, String.valueOf(entry.getValue()));
                                    }
                                }
                            }
                            return new b((String) obj4, hashMap);
                        }
                    }
                } else if (obj3 instanceof List) {
                    LinkedList linkedList = new LinkedList();
                    for (Object next : (List) obj3) {
                        if ((next instanceof String) && (a2 = InfoEventCategory.a((String) next)) != null) {
                            linkedList.add(a2);
                        }
                    }
                    if (linkedList.size() > 0) {
                        return new d(linkedList);
                    }
                }
            } else if (obj3 instanceof Number) {
                return new a(((Number) obj3).intValue());
            }
        }
        return new f();
    }
}
