package com.startapp.sdk.g.b;

import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;

final class c extends f {

    /* renamed from: a  reason: collision with root package name */
    private final InfoEventCategory f6477a;
    private final String b;

    public c(InfoEventCategory infoEventCategory, String str) {
        this.f6477a = infoEventCategory;
        this.b = str;
    }

    public final boolean a(Object obj) {
        if (obj instanceof e) {
            e eVar = (e) obj;
            if (this.f6477a == eVar.g()) {
                String str = this.b;
                if (str == null || str.equals(eVar.h())) {
                    return true;
                }
                return false;
            }
        }
        return false;
    }
}
