package com.startapp.sdk.g.b;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Pair;
import com.startapp.sdk.g.a;
import java.util.Map;

class b extends e {
    private BroadcastReceiver b;

    public b(String str, Map<String, String> map) {
        super(str, map);
    }

    public final void a(Context context, final a aVar) throws Exception {
        super.a(context, aVar);
        if (this.b == null) {
            AnonymousClass1 r0 = new BroadcastReceiver() {
                public final void onReceive(Context context, Intent intent) {
                    aVar.a((Object) new Pair(b.this, intent));
                }
            };
            this.b = r0;
            context.registerReceiver(r0, new IntentFilter(this.f6479a));
            return;
        }
        throw new IllegalStateException();
    }

    public final void a(Context context) throws Exception {
        super.a(context);
        BroadcastReceiver broadcastReceiver = this.b;
        if (broadcastReceiver != null) {
            context.unregisterReceiver(broadcastReceiver);
            this.b = null;
            return;
        }
        throw new IllegalStateException();
    }
}
