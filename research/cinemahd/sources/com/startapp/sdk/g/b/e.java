package com.startapp.sdk.g.b;

import android.content.Intent;
import android.os.Bundle;
import android.util.Pair;
import java.util.Map;

class e extends f {

    /* renamed from: a  reason: collision with root package name */
    protected final String f6479a;
    private final Map<String, String> b;

    public e(String str, Map<String, String> map) {
        this.f6479a = str;
        this.b = map;
    }

    public final boolean a(Object obj) {
        if (!(obj instanceof Pair)) {
            return false;
        }
        Pair pair = (Pair) obj;
        if (pair.first != this) {
            return false;
        }
        Object obj2 = pair.second;
        if (!(obj2 instanceof Intent)) {
            return false;
        }
        Intent intent = (Intent) obj2;
        if (!this.f6479a.equals(intent.getAction())) {
            return false;
        }
        Bundle extras = intent.getExtras();
        if (extras == null) {
            extras = Bundle.EMPTY;
        }
        for (Map.Entry next : this.b.entrySet()) {
            if (!((String) next.getValue()).equals(String.valueOf(extras.get((String) next.getKey())))) {
                return false;
            }
        }
        return true;
    }
}
