package com.startapp.sdk.g.b;

import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.util.List;

final class d extends f {

    /* renamed from: a  reason: collision with root package name */
    private final List<InfoEventCategory> f6478a;

    public d(List<InfoEventCategory> list) {
        this.f6478a = list;
    }

    public final boolean a(Object obj) {
        if (obj instanceof e) {
            return this.f6478a.contains(((e) obj).g());
        }
        return false;
    }
}
