package com.startapp.sdk.g.a;

import com.startapp.sdk.adsbase.j.u;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

final class b extends a {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6473a = u.a(101, -15, 2, 8, -10, 6, -2, -23, 19, 12, -8);

    b() {
    }

    public final JSONArray a(JSONArray jSONArray) throws JSONException {
        JSONArray jSONArray2 = new JSONArray();
        ArrayList arrayList = new ArrayList(jSONArray.length());
        int length = jSONArray.length();
        for (int i = 0; i < length; i++) {
            JSONObject jSONObject = jSONArray.getJSONObject(i);
            if (jSONObject != null) {
                arrayList.add(jSONObject.getString(f6473a));
            }
        }
        jSONArray2.put(com.iab.omid.library.startapp.b.a((List<String>) arrayList));
        return jSONArray2;
    }
}
