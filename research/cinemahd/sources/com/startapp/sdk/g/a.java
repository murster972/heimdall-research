package com.startapp.sdk.g;

import android.content.Context;
import android.os.SystemClock;
import android.util.Base64;
import android.util.JsonReader;
import android.util.Pair;
import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.j.j;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.remoteconfig.RscMetadata;
import com.startapp.sdk.adsbase.remoteconfig.RscMetadataItem;
import com.startapp.sdk.b.b;
import com.startapp.sdk.g.b.f;
import java.io.IOException;
import java.io.StringReader;
import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6472a;
    private final g<RscMetadata> b;
    private RscMetadata c;
    private List<f> d;
    private List<b> e;
    private final Map<com.startapp.sdk.b.a, Pair<Long, SoftReference<JSONObject>>> f = new WeakHashMap();

    public a(Context context, g<RscMetadata> gVar) {
        this.f6472a = context;
        this.b = gVar;
    }

    private RscMetadata b() {
        RscMetadata a2 = this.b.a();
        if (a2 == null || !a2.a()) {
            return null;
        }
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0021, code lost:
        r2 = a(r15.f6472a, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0027, code lost:
        if (r2 == null) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x002d, code lost:
        if (r2.size() > 0) goto L_0x0031;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0031, code lost:
        r3 = r0.c();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x0035, code lost:
        if (r3 == null) goto L_0x00b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x003b, code lost:
        if (r3.size() > 0) goto L_0x003f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x003f, code lost:
        r1 = new java.util.LinkedList();
        r3 = r3.iterator();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x004c, code lost:
        if (r3.hasNext() == false) goto L_0x00b0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x004e, code lost:
        r4 = r3.next();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0054, code lost:
        if (r4 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0056, code lost:
        r7 = a(r15.f6472a, r0, r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x005c, code lost:
        if (r7 == null) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x005e, code lost:
        r8 = a(r2, r4.b(), r4.c());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x006e, code lost:
        if (r8.size() <= 0) goto L_0x0048;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0076, code lost:
        if (r4.d() == null) goto L_0x0082;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0078, code lost:
        r9 = r4.d().intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0082, code lost:
        r9 = 300;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0086, code lost:
        r10 = r4.e();
        r11 = r4.f();
        r12 = r4.g();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0096, code lost:
        if (r4.h() == null) goto L_0x00a2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0098, code lost:
        r13 = r4.h().intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x00a2, code lost:
        r13 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00a4, code lost:
        r1.add(new com.startapp.sdk.g.b(r7, r8, r9, r10, r11, r12, r13, r0.a(r4)));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00b4, code lost:
        return a(r0, r2, (java.util.List<com.startapp.sdk.g.b>) r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x00b9, code lost:
        return a(r0, (java.util.List<com.startapp.sdk.g.b.f>) null, (java.util.List<com.startapp.sdk.g.b>) null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x00be, code lost:
        return a(r0, (java.util.List<com.startapp.sdk.g.b.f>) null, (java.util.List<com.startapp.sdk.g.b>) null);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.List<com.startapp.sdk.g.b> c() {
        /*
            r15 = this;
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 0
            r2 = 11
            if (r0 >= r2) goto L_0x0008
            return r1
        L_0x0008:
            com.startapp.sdk.adsbase.remoteconfig.RscMetadata r0 = r15.b()
            if (r0 != 0) goto L_0x0013
            java.util.List r0 = r15.a((com.startapp.sdk.adsbase.remoteconfig.RscMetadata) r1, (java.util.List<com.startapp.sdk.g.b.f>) r1, (java.util.List<com.startapp.sdk.g.b>) r1)
            return r0
        L_0x0013:
            monitor-enter(r15)
            com.startapp.sdk.adsbase.remoteconfig.RscMetadata r2 = r15.c     // Catch:{ all -> 0x00bf }
            boolean r2 = r0.equals(r2)     // Catch:{ all -> 0x00bf }
            if (r2 == 0) goto L_0x0020
            java.util.List<com.startapp.sdk.g.b> r0 = r15.e     // Catch:{ all -> 0x00bf }
            monitor-exit(r15)     // Catch:{ all -> 0x00bf }
            return r0
        L_0x0020:
            monitor-exit(r15)     // Catch:{ all -> 0x00bf }
            android.content.Context r2 = r15.f6472a
            java.util.List r2 = a((android.content.Context) r2, (com.startapp.sdk.adsbase.remoteconfig.RscMetadata) r0)
            if (r2 == 0) goto L_0x00ba
            int r3 = r2.size()
            if (r3 > 0) goto L_0x0031
            goto L_0x00ba
        L_0x0031:
            java.util.List r3 = r0.c()
            if (r3 == 0) goto L_0x00b5
            int r4 = r3.size()
            if (r4 > 0) goto L_0x003f
            goto L_0x00b5
        L_0x003f:
            java.util.LinkedList r1 = new java.util.LinkedList
            r1.<init>()
            java.util.Iterator r3 = r3.iterator()
        L_0x0048:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x00b0
            java.lang.Object r4 = r3.next()
            com.startapp.sdk.adsbase.remoteconfig.RscMetadataItem r4 = (com.startapp.sdk.adsbase.remoteconfig.RscMetadataItem) r4
            if (r4 == 0) goto L_0x0048
            android.content.Context r5 = r15.f6472a
            com.startapp.sdk.b.a r7 = a((android.content.Context) r5, (com.startapp.sdk.adsbase.remoteconfig.RscMetadata) r0, (com.startapp.sdk.adsbase.remoteconfig.RscMetadataItem) r4)
            if (r7 == 0) goto L_0x0048
            int r5 = r4.b()
            int r6 = r4.c()
            java.util.List r8 = a((java.util.List<com.startapp.sdk.g.b.f>) r2, (int) r5, (int) r6)
            int r5 = r8.size()
            if (r5 <= 0) goto L_0x0048
            com.startapp.sdk.g.b r5 = new com.startapp.sdk.g.b
            java.lang.Integer r6 = r4.d()
            if (r6 == 0) goto L_0x0082
            java.lang.Integer r6 = r4.d()
            int r6 = r6.intValue()
            r9 = r6
            goto L_0x0086
        L_0x0082:
            r6 = 300(0x12c, float:4.2E-43)
            r9 = 300(0x12c, float:4.2E-43)
        L_0x0086:
            int[] r10 = r4.e()
            java.lang.Integer r11 = r4.f()
            java.lang.Integer r12 = r4.g()
            java.lang.Integer r6 = r4.h()
            if (r6 == 0) goto L_0x00a2
            java.lang.Integer r6 = r4.h()
            int r6 = r6.intValue()
            r13 = r6
            goto L_0x00a4
        L_0x00a2:
            r6 = 0
            r13 = 0
        L_0x00a4:
            int r14 = r0.a(r4)
            r6 = r5
            r6.<init>(r7, r8, r9, r10, r11, r12, r13, r14)
            r1.add(r5)
            goto L_0x0048
        L_0x00b0:
            java.util.List r0 = r15.a((com.startapp.sdk.adsbase.remoteconfig.RscMetadata) r0, (java.util.List<com.startapp.sdk.g.b.f>) r2, (java.util.List<com.startapp.sdk.g.b>) r1)
            return r0
        L_0x00b5:
            java.util.List r0 = r15.a((com.startapp.sdk.adsbase.remoteconfig.RscMetadata) r0, (java.util.List<com.startapp.sdk.g.b.f>) r1, (java.util.List<com.startapp.sdk.g.b>) r1)
            return r0
        L_0x00ba:
            java.util.List r0 = r15.a((com.startapp.sdk.adsbase.remoteconfig.RscMetadata) r0, (java.util.List<com.startapp.sdk.g.b.f>) r1, (java.util.List<com.startapp.sdk.g.b>) r1)
            return r0
        L_0x00bf:
            r0 = move-exception
            monitor-exit(r15)     // Catch:{ all -> 0x00bf }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.g.a.c():java.util.List");
    }

    public final void a() {
        List<b> c2 = c();
        if (a((int) ByteConstants.KB)) {
            e f2 = new e(InfoEventCategory.GENERAL).f("RSC init");
            StringBuilder sb = new StringBuilder("targets: ");
            sb.append(c2 != null ? Integer.valueOf(c2.size()) : null);
            f2.g(sb.toString()).a(this.f6472a);
        }
    }

    private boolean a(int i) {
        RscMetadata b2 = b();
        return (b2 == null || (i & b2.d()) == 0) ? false : true;
    }

    private static boolean a(RscMetadata rscMetadata, int i) {
        return (rscMetadata == null || (rscMetadata.d() & i) == 0) ? false : true;
    }

    private static boolean a(RscMetadata rscMetadata, RscMetadataItem rscMetadataItem, int i) {
        return (rscMetadata.a(rscMetadataItem) & i) != 0;
    }

    private synchronized List<b> a(RscMetadata rscMetadata, List<f> list, List<b> list2) {
        if (this.d != null) {
            for (f a2 : this.d) {
                try {
                    a2.a(this.f6472a);
                } catch (Throwable th) {
                    if (a(this.c, 64)) {
                        new e(th).a(this.f6472a);
                    }
                }
            }
        }
        this.c = rscMetadata;
        this.d = list;
        this.e = list2;
        if (list != null) {
            for (f a3 : this.d) {
                try {
                    a3.a(this.f6472a, this);
                } catch (Throwable th2) {
                    if (a(rscMetadata, 128)) {
                        new e(th2).a(this.f6472a);
                    }
                }
            }
        }
        return list2;
    }

    private static List<f> a(Context context, RscMetadata rscMetadata) {
        String b2 = rscMetadata.b();
        if (b2 != null && b2.length() > 0) {
            try {
                try {
                    List<Object> b3 = j.b(new JsonReader(new StringReader(a(b2))));
                    ArrayList arrayList = new ArrayList();
                    for (Object a2 : b3) {
                        arrayList.add(com.startapp.sdk.g.b.g.a(a2));
                    }
                    return arrayList;
                } catch (Throwable th) {
                    if (a(rscMetadata, 1)) {
                        new e(th).a(context);
                    }
                    return null;
                }
            } catch (Throwable th2) {
                if (a(rscMetadata, 1)) {
                    new e(th2).a(context);
                }
            }
        }
        return null;
    }

    private static com.startapp.sdk.b.a a(Context context, RscMetadata rscMetadata, RscMetadataItem rscMetadataItem) {
        String a2 = rscMetadataItem.a();
        if (a2 != null && a2.length() > 0) {
            try {
                try {
                    return b.a(a(a2));
                } catch (Throwable th) {
                    if (!a(rscMetadata, rscMetadataItem, 4)) {
                        return null;
                    }
                    new e(th).a(context);
                    return null;
                }
            } catch (Throwable th2) {
                if (a(rscMetadata, rscMetadataItem, 2)) {
                    new e(th2).a(context);
                }
            }
        }
        return null;
    }

    public final String a(Object obj) {
        JSONArray jSONArray;
        List<b> c2 = c();
        if (c2 == null) {
            return null;
        }
        JSONObject jSONObject = null;
        for (b next : c2) {
            int i = 0;
            try {
                i = next.a(obj);
            } catch (Throwable th) {
                if (next.b(256)) {
                    new e(th).a(this.f6472a);
                }
            }
            if ((i & 1) != 0) {
                com.startapp.sdk.b.a a2 = next.a();
                JSONObject a3 = (i & 2) == 0 ? a(a2, next.b()) : null;
                if (a3 == null) {
                    try {
                        jSONArray = a2.a(this.f6472a, next.c(), next.d());
                    } catch (Throwable th2) {
                        if (next.b(8)) {
                            new e(th2).a(this.f6472a);
                        }
                        jSONArray = null;
                    }
                    if (!(jSONArray == null || next.e() == null)) {
                        try {
                            com.startapp.sdk.g.a.a a4 = com.startapp.sdk.g.a.a.a(next.e().intValue());
                            if (a4 != null) {
                                jSONArray = a4.a(jSONArray);
                            }
                        } catch (Throwable th3) {
                            if (next.b(2048)) {
                                new e(th3).a(this.f6472a);
                            }
                        }
                    }
                    if (jSONArray != null && jSONArray.length() > 0) {
                        a3 = new JSONObject();
                        try {
                            if (next.a(1)) {
                                a3.put("currentTimeMillis", System.currentTimeMillis());
                            }
                            if (next.a(2)) {
                                a3.put("bootTimeMillis", SystemClock.elapsedRealtime());
                            }
                            JSONArray a5 = a(this.f6472a, next);
                            if (a5 != null) {
                                a3.put("params", a5);
                            }
                            a3.put("items", jSONArray);
                        } catch (JSONException e2) {
                            if (next.b(32)) {
                                new e((Throwable) e2).a(this.f6472a);
                            }
                        }
                        a(a2, a3);
                    }
                }
                if (a3 != null) {
                    if (jSONObject == null) {
                        jSONObject = new JSONObject();
                    }
                    try {
                        JSONObject optJSONObject = jSONObject.optJSONObject(a2.a());
                        if (optJSONObject == null) {
                            optJSONObject = new JSONObject();
                            jSONObject.put(a2.a(), optJSONObject);
                        }
                        JSONArray optJSONArray = optJSONObject.optJSONArray(a2.b());
                        if (optJSONArray == null) {
                            optJSONArray = new JSONArray();
                            optJSONObject.put(a2.b(), optJSONArray);
                        }
                        optJSONArray.put(a3);
                    } catch (JSONException e3) {
                        if (next.b(32)) {
                            new e((Throwable) e3).a(this.f6472a);
                        }
                    }
                }
            }
        }
        if (jSONObject == null) {
            return null;
        }
        try {
            return Base64.encodeToString(com.startapp.common.b.a.a(u.a(jSONObject.toString())), 10);
        } catch (Throwable th4) {
            if (!a(16)) {
                return null;
            }
            new e(th4).a(this.f6472a);
            return null;
        }
    }

    private static JSONArray a(Context context, b bVar) {
        String[] c2 = bVar.a().c();
        Object[] d2 = bVar.a().d();
        if (c2.length == d2.length) {
            int length = c2.length;
            if (length == 0) {
                return null;
            }
            try {
                JSONArray jSONArray = new JSONArray();
                for (int i = 0; i < length; i++) {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put(c2[i], d2[i]);
                    jSONArray.put(jSONObject);
                }
                return jSONArray;
            } catch (JSONException e2) {
                if (bVar.b(32)) {
                    new e((Throwable) e2).a(context);
                }
            }
        } else {
            if (bVar.b(AdRequest.MAX_CONTENT_URL_LENGTH)) {
                e f2 = new e(InfoEventCategory.ERROR).f("c690e4ef5365d88b");
                f2.g(Arrays.toString(c2) + ", " + Arrays.toString(d2)).a(context);
            }
            return null;
        }
    }

    private JSONObject a(com.startapp.sdk.b.a aVar, int i) {
        Pair pair;
        JSONObject jSONObject;
        synchronized (this) {
            pair = this.f.get(aVar);
        }
        if (pair == null || (jSONObject = (JSONObject) ((SoftReference) pair.second).get()) == null) {
            return null;
        }
        if (((Long) pair.first).longValue() + ((long) (i * 1000)) < SystemClock.elapsedRealtime()) {
            return null;
        }
        return jSONObject;
    }

    private synchronized void a(com.startapp.sdk.b.a aVar, JSONObject jSONObject) {
        this.f.put(aVar, new Pair(Long.valueOf(SystemClock.elapsedRealtime()), new SoftReference(jSONObject)));
    }

    private static List<Pair<f, Boolean>> a(List<f> list, int i, int i2) {
        ArrayList arrayList = new ArrayList(Math.min(list.size(), Integer.bitCount(i)));
        int i3 = 0;
        for (f next : list) {
            boolean z = true;
            int i4 = 1 << i3;
            if ((i & i4) != 0) {
                if ((i4 & i2) == 0) {
                    z = false;
                }
                arrayList.add(new Pair(next, Boolean.valueOf(z)));
            }
            i3++;
        }
        return arrayList;
    }

    private static String a(String str) throws IOException {
        return new String(u.a(com.startapp.common.b.a.a(Base64.decode(str, 8))));
    }
}
