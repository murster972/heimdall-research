package com.startapp.sdk.g;

import android.util.Pair;
import com.startapp.sdk.b.a;
import com.startapp.sdk.g.b.f;
import java.util.Iterator;
import java.util.List;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final a f6474a;
    private final List<Pair<f, Boolean>> b;
    private final int c;
    private final int[] d;
    private final Integer e;
    private final Integer f;
    private final int g;
    private final int h;

    public b(a aVar, List<Pair<f, Boolean>> list, int i, int[] iArr, Integer num, Integer num2, int i2, int i3) {
        this.f6474a = aVar;
        this.b = list;
        this.c = i;
        this.d = iArr;
        this.e = num;
        this.f = num2;
        this.g = i2;
        this.h = i3;
    }

    public final a a() {
        return this.f6474a;
    }

    public final int b() {
        return this.c;
    }

    public final int[] c() {
        return this.d;
    }

    public final Integer d() {
        return this.e;
    }

    public final Integer e() {
        return this.f;
    }

    public final int a(Object obj) {
        int i;
        Pair next;
        Iterator<Pair<f, Boolean>> it2 = this.b.iterator();
        do {
            i = 0;
            if (!it2.hasNext()) {
                return 0;
            }
            next = it2.next();
        } while (!((f) next.first).a(obj));
        if (((Boolean) next.second).booleanValue()) {
            i = 2;
        }
        return i | 1;
    }

    public final boolean b(int i) {
        return (i & this.h) != 0;
    }

    public final boolean a(int i) {
        return (i & this.g) != 0;
    }
}
