package com.startapp.sdk.omsdk;

import com.startapp.common.parser.d;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class AdVerification implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = VerificationDetails.class, f = "adVerifications")
    private VerificationDetails[] adVerification;

    public AdVerification() {
    }

    public final List<VerificationDetails> a() {
        VerificationDetails[] verificationDetailsArr = this.adVerification;
        if (verificationDetailsArr == null) {
            return null;
        }
        return Arrays.asList(verificationDetailsArr);
    }

    public AdVerification(VerificationDetails[] verificationDetailsArr) {
        this.adVerification = verificationDetailsArr;
    }
}
