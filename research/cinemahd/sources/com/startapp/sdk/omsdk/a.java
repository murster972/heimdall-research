package com.startapp.sdk.omsdk;

import android.content.Context;
import android.webkit.WebView;
import com.iab.omid.library.startapp.adsession.Owner;
import com.iab.omid.library.startapp.adsession.b;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class a {
    public static b a(WebView webView) {
        if (!a(webView.getContext())) {
            return null;
        }
        return a(com.startapp.common.c.a.a(com.startapp.networkTest.utils.a.a("StartApp", "4.6.3"), webView, ""), false);
    }

    public static b a(Context context, AdVerification adVerification) {
        if (!a(context)) {
            return null;
        }
        if (adVerification == null) {
            new e(InfoEventCategory.ERROR).f("OMSDK: Verification details can't be null!").a(context);
            return null;
        }
        String a2 = b.a();
        List<VerificationDetails> a3 = adVerification.a();
        ArrayList arrayList = new ArrayList(a3.size());
        for (VerificationDetails next : a3) {
            URL a4 = a(context, next.b());
            if (a4 != null) {
                arrayList.add(com.startapp.networkTest.utils.e.a(next.a(), a4, next.c()));
            }
        }
        return a(com.startapp.common.c.a.a(com.startapp.networkTest.utils.a.a("StartApp", "4.6.3"), a2, arrayList, ""), true);
    }

    private static b a(com.startapp.common.c.a aVar, boolean z) {
        Owner owner = Owner.NATIVE;
        return b.a(com.startapp.common.b.e.a(owner, z ? owner : Owner.NONE), aVar);
    }

    private static URL a(Context context, String str) {
        try {
            return new URL(str);
        } catch (Throwable th) {
            new e(th).a(context);
            return null;
        }
    }

    private static boolean a(Context context) {
        try {
            if (com.iab.omid.library.startapp.a.b() || com.iab.omid.library.startapp.a.a(com.iab.omid.library.startapp.a.a(), context)) {
                return true;
            }
            new e(InfoEventCategory.ERROR).f("OMSDK: Failed to activate sdk.").a(context);
            return false;
        } catch (Throwable th) {
            new e(th).a(context);
            return false;
        }
    }
}
