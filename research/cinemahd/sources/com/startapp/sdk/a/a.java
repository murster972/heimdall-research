package com.startapp.sdk.a;

import android.content.Context;
import android.content.SharedPreferences;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.c;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.remoteconfig.AnalyticsReporterConfig;
import java.util.Locale;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicInteger;
import org.joda.time.DateTimeConstants;

public final class a {
    private static final String b = u.a(71, 13, -10, 14, -3, -6, -5, -54, 66, -11, 13, -5, -4, 10, 0, -10, 6, -1, -64, 19, 2, 0, 2, 14, 0, 12);
    private static final String c = u.a(66, 3, 5, -9);
    private static final String d = u.a(61, 12, -14, 17, 1, -14);
    private static final String e = u.a(56, -1, 2, 8, -4, 11, -3, 6, -7, -10);
    private static final String f = u.a(86, -19, 3, -12, -2, 19, -11, 6, -1);

    /* renamed from: a  reason: collision with root package name */
    protected final SharedPreferences f6094a;
    private final Context g;
    private final Executor h;
    private final g<AnalyticsReporterConfig> i;
    private final boolean j;
    private final AtomicInteger k = new AtomicInteger(0);

    public a(Context context, SharedPreferences sharedPreferences, Executor executor, g<AnalyticsReporterConfig> gVar) {
        boolean z = false;
        this.g = context;
        this.f6094a = sharedPreferences;
        this.h = executor;
        this.i = gVar;
        AnalyticsReporterConfig a2 = this.i.a();
        if (a2 != null && Math.random() < Math.min(Math.max(0.0d, a2.b()), 1.0d)) {
            z = true;
        }
        this.j = z;
    }

    /* access modifiers changed from: protected */
    public final void a() {
        Context context = this.g;
        int[] a2 = u.a(context, b + c + f, b + d + f, b + e + f);
        new e(InfoEventCategory.GENERAL).f("AR.lp").g(c.toLowerCase(Locale.ENGLISH) + ": " + a2[0] + ", " + d.toLowerCase(Locale.ENGLISH) + ": " + a2[1] + ", " + e.toLowerCase(Locale.ENGLISH) + ": " + a2[2] + ", appCode: " + b.c(this.g) + ", appVersion: " + b.d(this.g)).a(this.g);
    }

    public final void a(final int i2) {
        AnalyticsReporterConfig a2;
        AnalyticsReporterConfig a3 = this.i.a();
        int c2 = a3 != null ? a3.c() : 480;
        boolean z = false;
        if (c2 > 0 && System.currentTimeMillis() >= this.f6094a.getLong("5bd03adca4174bfb", 0) + ((long) (c2 * DateTimeConstants.MILLIS_PER_MINUTE))) {
            new e(InfoEventCategory.GENERAL).f("initialize").g(String.format(Locale.ENGLISH, "cnt=%d,aar=%d,mds=%d", new Object[]{Integer.valueOf(this.k.incrementAndGet()), Integer.valueOf(u.m(this.g)), Integer.valueOf(i2)})).a(this.g, new c() {
                public final void a() {
                }

                public final void a(e eVar, boolean z) {
                    if (z && i2 != 0) {
                        a.this.f6094a.edit().putLong("5bd03adca4174bfb", System.currentTimeMillis()).commit();
                    }
                }
            });
        }
        if (this.j && (a2 = this.i.a()) != null && (a2.a() & 1) == 1) {
            z = true;
        }
        if (z) {
            this.h.execute(new Runnable() {
                public final void run() {
                    a.this.a();
                }
            });
        }
    }
}
