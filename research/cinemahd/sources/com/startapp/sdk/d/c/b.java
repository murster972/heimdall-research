package com.startapp.sdk.d.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.remoteconfig.StaleDcConfig;
import com.startapp.sdk.d.a;

public final class b extends a<a> {
    private final SharedPreferences b;
    private final g<StaleDcConfig> c;

    public b(Context context, SharedPreferences sharedPreferences, g<StaleDcConfig> gVar) {
        super(context, 86400000);
        this.b = sharedPreferences;
        this.c = gVar;
    }

    private boolean a(int i) {
        StaleDcConfig a2 = this.c.a();
        return a2 != null && (a2.a() & i) == i;
    }

    private String b(boolean z) {
        String str = null;
        if (!z) {
            str = this.b.getString("a83b59c2138cbf65", (String) null);
        }
        if (str != null) {
            return str;
        }
        PackageManager packageManager = this.f6463a.getPackageManager();
        if (Build.VERSION.SDK_INT > 29 || "R".equals(Build.VERSION.CODENAME)) {
            try {
                Object invoke = PackageManager.class.getMethod("getInstallSourceInfo", new Class[]{String.class}).invoke(packageManager, new Object[]{this.f6463a.getPackageName()});
                str = (String) invoke.getClass().getMethod("getInstallingPackageName", new Class[0]).invoke(invoke, new Object[0]);
            } catch (Throwable th) {
                if (a(1)) {
                    new e(th).a(this.f6463a);
                }
            }
        }
        if (str == null) {
            try {
                str = packageManager.getInstallerPackageName(this.f6463a.getPackageName());
            } catch (Throwable th2) {
                if (a(2)) {
                    new e(th2).a(this.f6463a);
                }
            }
        }
        this.b.edit().putString("a83b59c2138cbf65", str).commit();
        return str;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a(boolean z) {
        a aVar = new a();
        aVar.a(b(z));
        return aVar;
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object b() {
        return new a();
    }
}
