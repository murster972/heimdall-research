package com.startapp.sdk.d.d;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.telephony.CellIdentity;
import android.telephony.CellIdentityCdma;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityTdscdma;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoCdma;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoTdscdma;
import android.telephony.CellInfoWcdma;
import android.telephony.CellLocation;
import android.telephony.TelephonyManager;
import android.telephony.cdma.CdmaCellLocation;
import android.telephony.gsm.GsmCellLocation;
import com.startapp.sdk.d.a;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public final class c extends a<a> {
    private static final Comparator<CellInfo> b = new Comparator<CellInfo>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            CellInfo cellInfo = (CellInfo) obj;
            CellInfo cellInfo2 = (CellInfo) obj2;
            if (cellInfo == null && cellInfo2 == null) {
                return 0;
            }
            if (cellInfo == null) {
                return -1;
            }
            if (cellInfo2 == null) {
                return 1;
            }
            int i = (cellInfo.getTimeStamp() > cellInfo2.getTimeStamp() ? 1 : (cellInfo.getTimeStamp() == cellInfo2.getTimeStamp() ? 0 : -1));
            if (i < 0) {
                return -1;
            }
            if (i > 0) {
                return 1;
            }
            return 0;
        }
    };

    public c(Context context) {
        super(context, 120000);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"MissingPermission"})
    public final /* synthetic */ Object a() {
        CellIdentityWcdma cellIdentity;
        boolean z = false;
        if (Build.VERSION.SDK_INT < 23 ? this.f6463a.checkCallingOrSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 || this.f6463a.checkCallingOrSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0 : this.f6463a.checkSelfPermission("android.permission.ACCESS_COARSE_LOCATION") == 0 || this.f6463a.checkSelfPermission("android.permission.ACCESS_FINE_LOCATION") == 0) {
            z = true;
        }
        if (!z) {
            return null;
        }
        Object systemService = this.f6463a.getSystemService("phone");
        if (!(systemService instanceof TelephonyManager)) {
            return null;
        }
        TelephonyManager telephonyManager = (TelephonyManager) systemService;
        b bVar = new b();
        if (Build.VERSION.SDK_INT >= 17) {
            List<CellInfo> allCellInfo = telephonyManager.getAllCellInfo();
            if (allCellInfo != null) {
                Collections.sort(allCellInfo, b);
                for (CellInfo next : allCellInfo) {
                    if (next instanceof CellInfoCdma) {
                        CellIdentityCdma cellIdentity2 = ((CellInfoCdma) next).getCellIdentity();
                        if (cellIdentity2 != null) {
                            if (cellIdentity2.getLatitude() != Integer.MAX_VALUE) {
                                bVar.a(com.startapp.common.b.a.c(String.valueOf(cellIdentity2.getLatitude())));
                            }
                            if (cellIdentity2.getLongitude() != Integer.MAX_VALUE) {
                                bVar.b(com.startapp.common.b.a.c(String.valueOf(cellIdentity2.getLongitude())));
                            }
                        }
                    } else if (next instanceof CellInfoGsm) {
                        CellIdentityGsm cellIdentity3 = ((CellInfoGsm) next).getCellIdentity();
                        if (cellIdentity3 != null) {
                            if (cellIdentity3.getLac() != Integer.MAX_VALUE) {
                                bVar.c(com.startapp.common.b.a.c(String.valueOf(cellIdentity3.getLac())));
                            }
                            if (cellIdentity3.getCid() != Integer.MAX_VALUE) {
                                bVar.d(com.startapp.common.b.a.c(String.valueOf(cellIdentity3.getCid())));
                            }
                        }
                    } else if (next instanceof CellInfoLte) {
                        CellIdentityLte cellIdentity4 = ((CellInfoLte) next).getCellIdentity();
                        if (!(cellIdentity4 == null || cellIdentity4.getTac() == Integer.MAX_VALUE)) {
                            bVar.e(com.startapp.common.b.a.c(String.valueOf(cellIdentity4.getTac())));
                        }
                    } else if (Build.VERSION.SDK_INT >= 29 && (next instanceof CellInfoNr)) {
                        CellIdentity cellIdentity5 = ((CellInfoNr) next).getCellIdentity();
                        CellIdentityNr cellIdentityNr = cellIdentity5 instanceof CellIdentityNr ? (CellIdentityNr) cellIdentity5 : null;
                        if (!(cellIdentityNr == null || cellIdentityNr.getTac() == Integer.MAX_VALUE)) {
                            bVar.e(com.startapp.common.b.a.c(String.valueOf(cellIdentityNr.getTac())));
                        }
                    } else if (Build.VERSION.SDK_INT >= 29 && (next instanceof CellInfoTdscdma)) {
                        CellIdentityTdscdma cellIdentity6 = ((CellInfoTdscdma) next).getCellIdentity();
                        if (cellIdentity6 != null) {
                            if (cellIdentity6.getLac() != Integer.MAX_VALUE) {
                                bVar.c(com.startapp.common.b.a.c(String.valueOf(cellIdentity6.getLac())));
                            }
                            if (cellIdentity6.getCid() != Integer.MAX_VALUE) {
                                bVar.d(com.startapp.common.b.a.c(String.valueOf(cellIdentity6.getCid())));
                            }
                        }
                    } else if (Build.VERSION.SDK_INT >= 18 && (next instanceof CellInfoWcdma) && (cellIdentity = ((CellInfoWcdma) next).getCellIdentity()) != null) {
                        if (cellIdentity.getLac() != Integer.MAX_VALUE) {
                            bVar.c(com.startapp.common.b.a.c(String.valueOf(cellIdentity.getLac())));
                        }
                        if (cellIdentity.getCid() != Integer.MAX_VALUE) {
                            bVar.d(com.startapp.common.b.a.c(String.valueOf(cellIdentity.getCid())));
                        }
                    }
                }
            }
        } else {
            CellLocation cellLocation = telephonyManager.getCellLocation();
            if (cellLocation instanceof GsmCellLocation) {
                GsmCellLocation gsmCellLocation = (GsmCellLocation) cellLocation;
                bVar.d(com.startapp.common.b.a.c(String.valueOf(gsmCellLocation.getCid())));
                bVar.c(com.startapp.common.b.a.c(String.valueOf(gsmCellLocation.getLac())));
            } else if (cellLocation instanceof CdmaCellLocation) {
                CdmaCellLocation cdmaCellLocation = (CdmaCellLocation) cellLocation;
                bVar.a(com.startapp.common.b.a.c(String.valueOf(cdmaCellLocation.getBaseStationLatitude())));
                bVar.b(com.startapp.common.b.a.c(String.valueOf(cdmaCellLocation.getBaseStationLongitude())));
            }
        }
        return bVar;
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object b() {
        return b.f6467a;
    }
}
