package com.startapp.sdk.d.b;

import java.util.Collection;
import java.util.Locale;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    protected static final a f6465a = new a();
    private final String b;
    private final String c;
    private final String d;

    public a(Locale locale, Collection<Locale> collection) {
        this.b = locale.toString();
        this.c = a((Locale) null, collection, ';');
        this.d = a(locale, collection, ',');
    }

    public final String a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    private static String a(Locale locale, Iterable<Locale> iterable, char c2) {
        boolean z;
        StringBuilder sb;
        if (locale != null) {
            sb = new StringBuilder();
            sb.append(locale);
            z = true;
        } else {
            z = false;
            sb = null;
        }
        if (iterable != null) {
            for (Locale next : iterable) {
                if (next != null) {
                    if (sb == null) {
                        sb = new StringBuilder();
                    }
                    if (z) {
                        sb.append(c2);
                    }
                    sb.append(next);
                    z = true;
                }
            }
        }
        if (sb != null) {
            return sb.toString();
        }
        return null;
    }

    private a() {
        this.b = null;
        this.c = null;
        this.d = null;
    }
}
