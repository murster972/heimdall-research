package com.startapp.sdk.d.b;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.LocaleList;
import com.startapp.sdk.d.a;
import java.util.LinkedHashSet;
import java.util.Locale;

public final class b extends a<a> {
    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object a() {
        Locale locale;
        Locale locale2;
        Configuration configuration;
        LocaleList locales;
        LinkedHashSet linkedHashSet = new LinkedHashSet();
        Resources resources = this.f6463a.getResources();
        if (resources == null || (configuration = resources.getConfiguration()) == null) {
            locale = null;
        } else {
            locale = configuration.locale;
            if (Build.VERSION.SDK_INT >= 24 && (locales = configuration.getLocales()) != null && locales.size() > 0) {
                int size = locales.size();
                Locale locale3 = locale;
                boolean z = true;
                for (int i = 0; i < size; i++) {
                    Locale locale4 = locales.get(i);
                    if (locale4 != null) {
                        if (linkedHashSet.size() < 11) {
                            linkedHashSet.add(locale4);
                        }
                        if (z) {
                            locale3 = locale4;
                            z = false;
                        }
                    }
                }
                locale = locale3;
            }
        }
        if (Build.VERSION.SDK_INT >= 24 && (locale2 = Locale.getDefault(Locale.Category.DISPLAY)) != null) {
            if (locale == null) {
                locale = locale2;
            }
            if (linkedHashSet.size() < 11) {
                linkedHashSet.add(locale2);
            }
        }
        Locale locale5 = Locale.getDefault();
        if (locale5 != null) {
            if (locale == null) {
                locale = locale5;
            }
            if (linkedHashSet.size() < 11) {
                linkedHashSet.add(locale5);
            }
        }
        if (locale == null) {
            locale = new Locale("en");
        }
        linkedHashSet.remove(locale);
        return new a(locale, linkedHashSet);
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object b() {
        return a.f6465a;
    }
}
