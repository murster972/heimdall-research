package com.startapp.sdk.d.a;

import com.startapp.sdk.adsbase.j.u;
import java.util.Collection;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    protected static final a f6464a = new a();
    private final String b;

    public a(Collection<String> collection) {
        this.b = u.a(collection, ";");
    }

    public final String a() {
        return this.b;
    }

    private a() {
        this.b = null;
    }
}
