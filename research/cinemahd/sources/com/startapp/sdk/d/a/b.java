package com.startapp.sdk.d.a;

import android.content.Context;
import com.startapp.sdk.d.a;

public final class b extends a<a> {
    public b(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0049  */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x009b  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final /* synthetic */ java.lang.Object a() {
        /*
            r9 = this;
            int r0 = android.os.Build.VERSION.SDK_INT
            r1 = 0
            r2 = 11
            if (r0 < r2) goto L_0x00a1
            android.content.Context r0 = r9.f6463a
            java.lang.String r2 = "input_method"
            java.lang.Object r0 = r0.getSystemService(r2)
            boolean r2 = r0 instanceof android.view.inputmethod.InputMethodManager
            if (r2 == 0) goto L_0x00a1
            android.view.inputmethod.InputMethodManager r0 = (android.view.inputmethod.InputMethodManager) r0
            android.view.inputmethod.InputMethodSubtype r2 = r0.getCurrentInputMethodSubtype()
            r3 = 10
            java.lang.String r4 = "keyboard"
            if (r2 == 0) goto L_0x0042
            java.lang.String r5 = r2.getMode()
            boolean r5 = r4.equals(r5)
            if (r5 == 0) goto L_0x0042
            java.lang.String r2 = r2.getLocale()
            boolean r5 = android.text.TextUtils.isEmpty(r2)
            if (r5 != 0) goto L_0x0042
            java.util.LinkedHashSet r5 = new java.util.LinkedHashSet
            r5.<init>()
            int r6 = r5.size()
            if (r6 >= r3) goto L_0x0043
            r5.add(r2)
            goto L_0x0043
        L_0x0042:
            r5 = r1
        L_0x0043:
            java.util.List r2 = r0.getInputMethodList()
            if (r2 == 0) goto L_0x0099
            java.util.Iterator r2 = r2.iterator()
        L_0x004d:
            boolean r6 = r2.hasNext()
            if (r6 == 0) goto L_0x0099
            java.lang.Object r6 = r2.next()
            android.view.inputmethod.InputMethodInfo r6 = (android.view.inputmethod.InputMethodInfo) r6
            if (r6 == 0) goto L_0x004d
            r7 = 1
            java.util.List r6 = r0.getEnabledInputMethodSubtypeList(r6, r7)
            if (r6 == 0) goto L_0x004d
            java.util.Iterator r6 = r6.iterator()
        L_0x0066:
            boolean r7 = r6.hasNext()
            if (r7 == 0) goto L_0x004d
            java.lang.Object r7 = r6.next()
            android.view.inputmethod.InputMethodSubtype r7 = (android.view.inputmethod.InputMethodSubtype) r7
            if (r7 == 0) goto L_0x0066
            java.lang.String r8 = r7.getMode()
            boolean r8 = r4.equals(r8)
            if (r8 == 0) goto L_0x0066
            java.lang.String r7 = r7.getLocale()
            boolean r8 = android.text.TextUtils.isEmpty(r7)
            if (r8 != 0) goto L_0x0066
            if (r5 != 0) goto L_0x008f
            java.util.LinkedHashSet r5 = new java.util.LinkedHashSet
            r5.<init>()
        L_0x008f:
            int r8 = r5.size()
            if (r8 >= r3) goto L_0x0066
            r5.add(r7)
            goto L_0x0066
        L_0x0099:
            if (r5 == 0) goto L_0x00a1
            com.startapp.sdk.d.a.a r0 = new com.startapp.sdk.d.a.a
            r0.<init>(r5)
            return r0
        L_0x00a1:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.d.a.b.a():java.lang.Object");
    }

    /* access modifiers changed from: protected */
    public final /* bridge */ /* synthetic */ Object b() {
        return a.f6464a;
    }
}
