package com.startapp.sdk.d;

import android.content.Context;
import android.os.SystemClock;

public abstract class a<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f6463a;
    private volatile T b;
    private volatile long c;
    private final long d;

    public a(Context context) {
        this(context, 900000);
    }

    private boolean d() {
        return this.c + this.d < SystemClock.uptimeMillis();
    }

    /* access modifiers changed from: protected */
    public T a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public T a(boolean z) {
        return a();
    }

    /* access modifiers changed from: protected */
    public abstract T b();

    public final T c() {
        T t = this.b;
        if (t == null || d()) {
            synchronized (this) {
                t = this.b;
                boolean d2 = d();
                if (t == null || d2) {
                    try {
                        t = a(d2);
                    } catch (Throwable unused) {
                    }
                    if (t != null) {
                        this.b = t;
                        this.c = SystemClock.uptimeMillis();
                    }
                }
            }
        }
        if (t != null) {
            return t;
        }
        return b();
    }

    public a(Context context, long j) {
        this.f6463a = context;
        this.d = j;
    }
}
