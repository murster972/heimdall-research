package com.startapp.sdk.json;

import android.content.Context;
import android.content.Intent;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.d;
import com.startapp.sdk.adsbase.j.q;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.adsbase.model.GetAdResponse;
import com.startapp.sdk.c.c;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class a extends d {
    private int g = 0;
    private Set<String> h = new HashSet();

    public a(Context context, Ad ad, AdPreferences adPreferences, b bVar, AdPreferences.Placement placement) {
        super(context, ad, adPreferences, bVar, placement);
    }

    /* access modifiers changed from: protected */
    public abstract void a(Ad ad);

    /* access modifiers changed from: protected */
    public final Object e() {
        GetAdRequest a2 = a();
        if (a2 == null) {
            return null;
        }
        if (this.h.size() == 0) {
            this.h.add(this.f6335a.getPackageName());
        }
        boolean z = false;
        if (this.g > 0) {
            a2.c(false);
        }
        a2.a(this.h);
        if (this.g == 0) {
            z = true;
        }
        a2.c(z);
        return c.a(this.f6335a).l().a(AdsConstants.a(AdsConstants.AdApiType.JSON, f())).a((com.startapp.sdk.adsbase.c) a2).a((q<String>) new q<String>() {
            public final /* bridge */ /* synthetic */ void a(Object obj) {
                String unused = a.this.f = (String) obj;
            }
        }).a(GetAdResponse.class);
    }

    /* access modifiers changed from: protected */
    public final boolean a(Object obj) {
        int i;
        GetAdResponse getAdResponse = (GetAdResponse) obj;
        boolean z = false;
        if (obj == null) {
            this.f = "Empty Response";
            return false;
        } else if (!getAdResponse.a()) {
            this.f = getAdResponse.b();
            return false;
        } else {
            JsonAd jsonAd = (JsonAd) this.b;
            List<AdDetails> a2 = com.iab.omid.library.startapp.b.a(this.f6335a, getAdResponse.c(), this.g, this.h);
            jsonAd.a(a2);
            jsonAd.setAdInfoOverride(getAdResponse.d());
            if (getAdResponse.c() != null && getAdResponse.c().size() > 0) {
                z = true;
            }
            if (!z) {
                this.f = "Empty Response";
            } else if (a2.size() == 0 && (i = this.g) == 0) {
                this.g = i + 1;
                return d().booleanValue();
            }
            return z;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        Intent intent = new Intent("com.startapp.android.OnReceiveResponseBroadcastListener");
        intent.putExtra("adHashcode", this.b.hashCode());
        intent.putExtra("adResult", bool);
        com.startapp.common.b.a(this.f6335a).a(intent);
        if (bool.booleanValue()) {
            a((Ad) (JsonAd) this.b);
            this.d.a(this.b);
        }
    }
}
