package com.startapp.sdk.ads.nativead;

import android.content.Context;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;

public final class a extends com.startapp.sdk.json.a {
    private NativeAdPreferences g;

    public a(Context context, Ad ad, AdPreferences adPreferences, b bVar, NativeAdPreferences nativeAdPreferences) {
        super(context, ad, adPreferences, bVar, AdPreferences.Placement.INAPP_NATIVE);
        this.g = nativeAdPreferences;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest a2 = super.a();
        if (a2 == null) {
            return null;
        }
        a2.e(this.g.getAdsNumber());
        if (this.g.getImageSize() != null) {
            a2.c(this.g.getImageSize().getWidth());
            a2.d(this.g.getImageSize().getHeight());
        } else {
            int primaryImageSize = this.g.getPrimaryImageSize();
            if (primaryImageSize == -1) {
                primaryImageSize = 2;
            }
            a2.c(Integer.toString(primaryImageSize));
            int secondaryImageSize = this.g.getSecondaryImageSize();
            if (secondaryImageSize == -1) {
                secondaryImageSize = 2;
            }
            a2.d(Integer.toString(secondaryImageSize));
        }
        if (this.g.isContentAd()) {
            a2.b(this.g.isContentAd());
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a(Ad ad) {
    }
}
