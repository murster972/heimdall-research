package com.startapp.sdk.ads.nativead;

import android.os.Handler;
import android.os.Looper;

final class b implements NativeAdDisplayListener {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public NativeAdDisplayListener f6164a;

    b(NativeAdDisplayListener nativeAdDisplayListener) {
        this.f6164a = nativeAdDisplayListener;
    }

    public final void adClicked(final NativeAdInterface nativeAdInterface) {
        if (this.f6164a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.f6164a.adClicked(nativeAdInterface);
                }
            });
        }
    }

    public final void adDisplayed(final NativeAdInterface nativeAdInterface) {
        if (this.f6164a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.f6164a.adDisplayed(nativeAdInterface);
                }
            });
        }
    }

    public final void adHidden(final NativeAdInterface nativeAdInterface) {
        if (this.f6164a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.f6164a.adHidden(nativeAdInterface);
                }
            });
        }
    }

    public final void adNotDisplayed(final NativeAdInterface nativeAdInterface) {
        if (this.f6164a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    b.this.f6164a.adNotDisplayed(nativeAdInterface);
                }
            });
        }
    }
}
