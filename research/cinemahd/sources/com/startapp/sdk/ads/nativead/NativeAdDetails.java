package com.startapp.sdk.ads.nativead;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import com.startapp.common.a;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.nativead.StartAppNativeAd;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.k.b;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.lang.ref.WeakReference;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class NativeAdDetails implements NativeAdInterface {

    /* renamed from: a  reason: collision with root package name */
    private AdDetails f6152a;
    /* access modifiers changed from: private */
    public int b;
    /* access modifiers changed from: private */
    public Bitmap c;
    /* access modifiers changed from: private */
    public Bitmap d;
    /* access modifiers changed from: private */
    public boolean e = false;
    /* access modifiers changed from: private */
    public boolean f = false;
    /* access modifiers changed from: private */
    public a g;
    private String h;
    private b i;
    private WeakReference<View> j = new WeakReference<>((Object) null);
    /* access modifiers changed from: private */
    public View.OnAttachStateChangeListener k;
    /* access modifiers changed from: private */
    public b l;

    /* renamed from: com.startapp.sdk.ads.nativead.NativeAdDetails$8  reason: invalid class name */
    static /* synthetic */ class AnonymousClass8 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6161a = new int[StartAppNativeAd.CampaignAction.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.startapp.sdk.ads.nativead.StartAppNativeAd$CampaignAction[] r0 = com.startapp.sdk.ads.nativead.StartAppNativeAd.CampaignAction.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6161a = r0
                int[] r0 = f6161a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.ads.nativead.StartAppNativeAd$CampaignAction r1 = com.startapp.sdk.ads.nativead.StartAppNativeAd.CampaignAction.OPEN_MARKET     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6161a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.ads.nativead.StartAppNativeAd$CampaignAction r1 = com.startapp.sdk.ads.nativead.StartAppNativeAd.CampaignAction.LAUNCH_APP     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.nativead.NativeAdDetails.AnonymousClass8.<clinit>():void");
        }
    }

    public interface a {
        void onNativeAdDetailsLoaded(int i);
    }

    public NativeAdDetails(AdDetails adDetails, NativeAdPreferences nativeAdPreferences, int i2, a aVar) {
        this.f6152a = adDetails;
        this.b = i2;
        this.g = aVar;
        if (nativeAdPreferences.isAutoBitmapDownload()) {
            new com.startapp.common.a(getImageUrl(), new a.C0062a() {
                public final void a(Bitmap bitmap, int i) {
                    NativeAdDetails.this.c = bitmap;
                    new com.startapp.common.a(NativeAdDetails.this.getSecondaryImageUrl(), new a.C0062a() {
                        public final void a(Bitmap bitmap, int i) {
                            NativeAdDetails.this.d = bitmap;
                            NativeAdDetails.this.b();
                        }
                    }, i).a();
                }
            }, i2).a();
        } else {
            b();
        }
    }

    /* access modifiers changed from: protected */
    public void finalize() throws Throwable {
        super.finalize();
        unregisterView();
    }

    public StartAppNativeAd.CampaignAction getCampaignAction() {
        StartAppNativeAd.CampaignAction campaignAction = StartAppNativeAd.CampaignAction.OPEN_MARKET;
        AdDetails adDetails = this.f6152a;
        return (adDetails == null || !adDetails.r()) ? campaignAction : StartAppNativeAd.CampaignAction.LAUNCH_APP;
    }

    public String getCategory() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.t() : "";
    }

    public String getDescription() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.g() : "";
    }

    public int getIdentifier() {
        return this.b;
    }

    public Bitmap getImageBitmap() {
        return this.c;
    }

    public String getImageUrl() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.h() : "";
    }

    public String getInstalls() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.s() : "";
    }

    public String getPackacgeName() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.n() : "";
    }

    public float getRating() {
        AdDetails adDetails = this.f6152a;
        if (adDetails != null) {
            return adDetails.k();
        }
        return 5.0f;
    }

    public Bitmap getSecondaryImageBitmap() {
        return this.d;
    }

    public String getSecondaryImageUrl() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.j() : "";
    }

    public String getTitle() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null ? adDetails.f() : "";
    }

    public boolean isApp() {
        AdDetails adDetails = this.f6152a;
        if (adDetails != null) {
            return adDetails.u();
        }
        return true;
    }

    public boolean isBelowMinCPM() {
        AdDetails adDetails = this.f6152a;
        return adDetails != null && adDetails.i();
    }

    public void registerViewForInteraction(View view) {
        a(view);
        ((View) this.j.get()).setOnClickListener(new View.OnClickListener() {
            public final void onClick(View view) {
                NativeAdDetails.a(NativeAdDetails.this, view);
            }
        });
    }

    public String toString() {
        String description = getDescription();
        if (description != null) {
            description = description.substring(0, Math.min(30, description.length()));
        }
        return "         Title: [" + getTitle() + "]\n         Description: [" + description + "]...\n         Rating: [" + getRating() + "]\n         Installs: [" + getInstalls() + "]\n         Category: [" + getCategory() + "]\n         PackageName: [" + getPackacgeName() + "]\n         CampaginAction: [" + getCampaignAction() + "]\n";
    }

    public void unregisterView() {
        View.OnAttachStateChangeListener onAttachStateChangeListener;
        e();
        View view = (View) this.j.get();
        this.j.clear();
        if (!(view == null || Build.VERSION.SDK_INT < 12 || (onAttachStateChangeListener = this.k) == null)) {
            view.removeOnAttachStateChangeListener(onAttachStateChangeListener);
        }
        Bitmap bitmap = this.c;
        if (bitmap != null) {
            bitmap.recycle();
            this.c = null;
        }
        Bitmap bitmap2 = this.d;
        if (bitmap2 != null) {
            bitmap2.recycle();
            this.d = null;
        }
    }

    /* access modifiers changed from: private */
    public void b() {
        new Handler().post(new Runnable() {
            public final void run() {
                if (NativeAdDetails.this.g != null) {
                    NativeAdDetails.this.g.onNativeAdDetailsLoaded(NativeAdDetails.this.b);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void c() {
        if (this.i == null && !this.e) {
            View view = (View) this.j.get();
            if (view == null) {
                b bVar = this.l;
                if (bVar != null) {
                    bVar.adNotDisplayed(this);
                    return;
                }
                return;
            }
            h hVar = new h(view.getContext(), new String[]{this.f6152a.d()}, new TrackingParams(this.h), f());
            hVar.a((h.a) new h.a() {
                public final void onSent() {
                    boolean unused = NativeAdDetails.this.e = true;
                    if (NativeAdDetails.this.l != null) {
                        NativeAdDetails.this.l.adDisplayed(NativeAdDetails.this);
                    }
                }
            });
            this.i = new b(this.j, hVar, d());
            this.i.a(new b.a() {
                public final void a() {
                    if (NativeAdDetails.this.l != null && !NativeAdDetails.this.f) {
                        NativeAdDetails.this.l.adHidden(NativeAdDetails.this);
                        boolean unused = NativeAdDetails.this.f = true;
                    }
                }
            });
            this.i.a();
        }
    }

    private static int d() {
        return BannerMetaData.a().b().q();
    }

    /* access modifiers changed from: private */
    public void e() {
        b bVar = this.i;
        if (bVar != null) {
            bVar.b();
            this.i = null;
        }
    }

    private long f() {
        if (this.f6152a.y() != null) {
            return TimeUnit.SECONDS.toMillis(this.f6152a.y().longValue());
        }
        return TimeUnit.SECONDS.toMillis(MetaData.G().H());
    }

    private View.OnAttachStateChangeListener g() {
        if (this.k == null) {
            this.k = new View.OnAttachStateChangeListener() {
                public final void onViewAttachedToWindow(View view) {
                    NativeAdDetails.this.c();
                }

                public final void onViewDetachedFromWindow(View view) {
                    NativeAdDetails.this.e();
                    view.removeOnAttachStateChangeListener(NativeAdDetails.this.k);
                }
            };
        }
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.h = str;
    }

    /* access modifiers changed from: protected */
    public final AdDetails a() {
        return this.f6152a;
    }

    public void registerViewForInteraction(View view, List<View> list) {
        registerViewForInteraction(view, list, (NativeAdDisplayListener) null);
    }

    private void a(View view) {
        this.j = new WeakReference<>(view);
        if (view.hasWindowFocus() || Build.VERSION.SDK_INT < 12) {
            c();
        } else {
            view.addOnAttachStateChangeListener(g());
        }
    }

    public void registerViewForInteraction(View view, List<View> list, NativeAdDisplayListener nativeAdDisplayListener) {
        if (list == null || list.isEmpty() || this.j.get() != null) {
            registerViewForInteraction(view);
        } else {
            AnonymousClass4 r0 = new View.OnClickListener() {
                public final void onClick(View view) {
                    NativeAdDetails.a(NativeAdDetails.this, view);
                }
            };
            for (View onClickListener : list) {
                onClickListener.setOnClickListener(r0);
            }
            a(view);
        }
        if (nativeAdDisplayListener != null) {
            this.l = new b(nativeAdDisplayListener);
        }
    }

    static /* synthetic */ void a(NativeAdDetails nativeAdDetails, View view) {
        Context context = view.getContext();
        int i2 = AnonymousClass8.f6161a[nativeAdDetails.getCampaignAction().ordinal()];
        if (i2 == 1) {
            boolean a2 = com.startapp.sdk.adsbase.a.a(context, AdPreferences.Placement.INAPP_NATIVE);
            if (!nativeAdDetails.f6152a.l() || a2) {
                com.startapp.sdk.adsbase.a.a(context, nativeAdDetails.f6152a.c(), nativeAdDetails.f6152a.e(), new TrackingParams(nativeAdDetails.h), nativeAdDetails.f6152a.w() && !a2, false);
            } else {
                com.startapp.sdk.adsbase.a.a(context, nativeAdDetails.f6152a.c(), nativeAdDetails.f6152a.e(), nativeAdDetails.f6152a.n(), new TrackingParams(nativeAdDetails.h), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), nativeAdDetails.f6152a.w(), nativeAdDetails.f6152a.z());
            }
        } else if (i2 == 2) {
            com.startapp.sdk.adsbase.a.a(nativeAdDetails.getPackacgeName(), nativeAdDetails.f6152a.p(), nativeAdDetails.f6152a.c(), context, new TrackingParams(nativeAdDetails.h));
        }
        b bVar = nativeAdDetails.l;
        if (bVar != null) {
            bVar.adClicked(nativeAdDetails);
        }
    }
}
