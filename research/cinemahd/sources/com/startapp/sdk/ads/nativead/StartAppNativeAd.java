package com.startapp.sdk.ads.nativead;

import android.content.Context;
import com.startapp.sdk.ads.nativead.NativeAdDetails;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.adinformation.AdInformationMetaData;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.adrules.AdaptMetaData;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.util.ArrayList;
import java.util.List;

public class StartAppNativeAd extends Ad implements NativeAdDetails.a {
    private static final long serialVersionUID = 1;
    private a adEventDelegate;
    boolean isLoading = false;
    private List<NativeAdDetails> listNativeAds = new ArrayList();
    private NativeAd nativeAd;
    private NativeAdPreferences preferences;
    private int totalObjectsLoaded = 0;

    public enum CampaignAction {
        LAUNCH_APP,
        OPEN_MARKET
    }

    class a extends b {

        /* renamed from: a  reason: collision with root package name */
        private b f6163a;

        public a(b bVar) {
            this.f6163a = bVar;
        }

        public final void a(Ad ad) {
            StartAppNativeAd.this.a();
        }

        public final void b(Ad ad) {
            StartAppNativeAd.this.setErrorMessage(ad.getErrorMessage());
            b bVar = this.f6163a;
            if (bVar != null) {
                bVar.b(StartAppNativeAd.this);
                this.f6163a = null;
            }
            StartAppNativeAd startAppNativeAd = StartAppNativeAd.this;
            startAppNativeAd.isLoading = false;
            startAppNativeAd.a();
        }

        public final b a() {
            return this.f6163a;
        }
    }

    public StartAppNativeAd(Context context) {
        super(context, AdPreferences.Placement.INAPP_NATIVE);
    }

    private List<AdDetails> g() {
        ArrayList arrayList = new ArrayList();
        List<NativeAdDetails> list = this.listNativeAds;
        if (list != null) {
            for (NativeAdDetails a2 : list) {
                arrayList.add(a2.a());
            }
        }
        return arrayList;
    }

    public static String getPrivacyImageUrl() {
        return AdInformationMetaData.b().d();
    }

    public static String getPrivacyURL() {
        if (AdInformationMetaData.b().c() == null) {
            return "";
        }
        String c = AdInformationMetaData.b().c();
        if (c.contains("http://") || c.contains("https://")) {
            return AdInformationMetaData.b().c();
        }
        return "https://" + AdInformationMetaData.b().c();
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        this.totalObjectsLoaded = 0;
        if (this.listNativeAds == null) {
            this.listNativeAds = new ArrayList();
        }
        this.listNativeAds.clear();
        NativeAd nativeAd2 = this.nativeAd;
        if (nativeAd2 != null && nativeAd2.g() != null) {
            for (int i = 0; i < this.nativeAd.g().size(); i++) {
                this.listNativeAds.add(new NativeAdDetails(this.nativeAd.g().get(i), this.preferences, i, this));
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
    }

    public String getAdId() {
        NativeAdDetails nativeAdDetails;
        List<NativeAdDetails> list = this.listNativeAds;
        if (list == null || list.size() <= 0 || (nativeAdDetails = this.listNativeAds.get(0)) == null || nativeAdDetails.a() == null) {
            return null;
        }
        return nativeAdDetails.a().a();
    }

    public ArrayList<NativeAdDetails> getNativeAds() {
        return getNativeAds((String) null);
    }

    public int getNumberOfAds() {
        List<NativeAdDetails> list = this.listNativeAds;
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public boolean isBelowMinCPM() {
        return this.nativeAd.isBelowMinCPM();
    }

    public boolean loadAd() {
        return loadAd(new NativeAdPreferences(), (AdEventListener) null);
    }

    public void onNativeAdDetailsLoaded(int i) {
        b a2;
        this.totalObjectsLoaded++;
        if (this.nativeAd.g() != null && this.totalObjectsLoaded == this.nativeAd.g().size()) {
            this.isLoading = false;
            setErrorMessage((String) null);
            a aVar = this.adEventDelegate;
            if (aVar != null && (a2 = aVar.a()) != null) {
                a2.a(this);
            }
        }
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n===== StartAppNativeAd =====\n");
        for (int i = 0; i < getNumberOfAds(); i++) {
            stringBuffer.append(this.listNativeAds.get(i));
        }
        stringBuffer.append("===== End StartAppNativeAd =====");
        return stringBuffer.toString();
    }

    public ArrayList<NativeAdDetails> getNativeAds(String str) {
        ArrayList<NativeAdDetails> arrayList = new ArrayList<>();
        AdRulesResult a2 = AdaptMetaData.a().b().a(AdPreferences.Placement.INAPP_NATIVE, str);
        if (a2.a()) {
            List<NativeAdDetails> list = this.listNativeAds;
            if (list != null) {
                for (NativeAdDetails next : list) {
                    next.a(str);
                    arrayList.add(next);
                }
                com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(AdPreferences.Placement.INAPP_NATIVE, str));
            }
        } else {
            com.startapp.sdk.adsbase.a.a(this.f6243a, com.startapp.sdk.adsbase.a.a(g()), str, a2.b());
        }
        return arrayList;
    }

    public boolean loadAd(AdEventListener adEventListener) {
        return loadAd(new NativeAdPreferences(), adEventListener);
    }

    public boolean loadAd(NativeAdPreferences nativeAdPreferences) {
        return loadAd(nativeAdPreferences, (AdEventListener) null);
    }

    public boolean loadAd(NativeAdPreferences nativeAdPreferences, AdEventListener adEventListener) {
        this.adEventDelegate = new a(b.a(this.f6243a, adEventListener));
        this.preferences = nativeAdPreferences;
        if (this.isLoading) {
            setErrorMessage("Ad is currently being loaded");
            return false;
        }
        this.isLoading = true;
        this.nativeAd = new NativeAd(this.f6243a, this.preferences);
        return this.nativeAd.load(nativeAdPreferences, this.adEventDelegate, true);
    }
}
