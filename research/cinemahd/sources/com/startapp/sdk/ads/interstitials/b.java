package com.startapp.sdk.ads.interstitials;

import android.content.Context;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.e.a;

public final class b extends a {
    public b(Context context, HtmlAd htmlAd, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        super(context, htmlAd, adPreferences, bVar, AdPreferences.Placement.INAPP_RETURN, true);
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        a(bool.booleanValue());
    }
}
