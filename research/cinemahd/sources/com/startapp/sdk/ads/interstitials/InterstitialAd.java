package com.startapp.sdk.ads.interstitials;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import com.facebook.react.uimanager.ViewProps;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.adsbase.ActivityExtra;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.VideoConfig;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.activities.AppWallActivity;
import com.startapp.sdk.adsbase.activities.FullScreenActivity;
import com.startapp.sdk.adsbase.activities.OverlayActivity;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.c.c;
import com.unity3d.services.ads.adunit.AdUnitActivity;

public abstract class InterstitialAd extends HtmlAd implements f {
    private static final long serialVersionUID = 1;

    public InterstitialAd(Context context, AdPreferences.Placement placement) {
        super(context, placement);
    }

    /* access modifiers changed from: protected */
    public boolean a() {
        return false;
    }

    /* JADX WARNING: type inference failed for: r11v7, types: [java.lang.Boolean[], java.io.Serializable] */
    public final boolean a(String str) {
        Class cls;
        String b = a.b();
        if (!a() || !AdsCommonMetaData.a().I().a().equals(VideoConfig.BackMode.DISABLED) || !b.equals("back")) {
            if (!AdsConstants.b.booleanValue()) {
                setState(Ad.AdState.UN_INITIALIZED);
            }
            if (j() == null) {
                a(AdDisplayListener.NotDisplayedReason.INTERNAL_ERROR);
                return false;
            } else if (super.e_()) {
                a(AdDisplayListener.NotDisplayedReason.AD_EXPIRED);
                return false;
            } else {
                ActivityExtra activityExtra = this.activityExtra;
                boolean z = activityExtra != null && activityExtra.a();
                Context context = this.f6243a;
                if (((r() != 0 && r() != this.f6243a.getResources().getConfiguration().orientation) || a() || v() || b.equals("back")) && u.a(getContext(), (Class<? extends Activity>) FullScreenActivity.class)) {
                    cls = FullScreenActivity.class;
                } else {
                    cls = u.a(getContext(), (Class<? extends Activity>) OverlayActivity.class, (Class<? extends Activity>) AppWallActivity.class);
                }
                Intent intent = new Intent(context, cls);
                intent.putExtra("fileUrl", "exit.html");
                String[] strArr = this.trackingUrls;
                String a2 = a.a();
                for (int i = 0; i < strArr.length; i++) {
                    if (strArr[i] != null && !"".equals(strArr[i])) {
                        strArr[i] = strArr[i] + a2;
                    }
                }
                intent.putExtra("tracking", strArr);
                intent.putExtra("trackingClickUrl", q());
                intent.putExtra("packageNames", s());
                intent.putExtra("htmlUuid", k());
                intent.putExtra("smartRedirect", this.smartRedirect);
                intent.putExtra("browserEnabled", this.inAppBrowserEnabled);
                intent.putExtra("placement", this.placement.a());
                intent.putExtra("adInfoOverride", getAdInfoOverride());
                intent.putExtra("ad", this);
                intent.putExtra("videoAd", a());
                intent.putExtra("fullscreen", z);
                intent.putExtra(AdUnitActivity.EXTRA_ORIENTATION, r() == 0 ? this.f6243a.getResources().getConfiguration().orientation : r());
                intent.putExtra("adTag", str);
                intent.putExtra("lastLoadTime", super.b());
                intent.putExtra("adCacheTtl", super.c());
                intent.putExtra("closingUrl", m());
                intent.putExtra("rewardDuration", o());
                intent.putExtra("rewardedHideTimer", p());
                if (t() != null) {
                    intent.putExtra("delayImpressionSeconds", t());
                }
                intent.putExtra("sendRedirectHops", u());
                intent.putExtra("mraidAd", v());
                if (v()) {
                    intent.putExtra("activityShouldLockOrientation", false);
                }
                u.b();
                if (this instanceof SplashAd) {
                    intent.putExtra("isSplash", true);
                }
                intent.putExtra(ViewProps.POSITION, b);
                intent.addFlags(343932928);
                com.startapp.sdk.adsbase.consent.a f = c.a(this.f6243a).f();
                if (f.b()) {
                    f.a(intent);
                } else {
                    this.f6243a.startActivity(intent);
                }
                return true;
            }
        } else {
            a(AdDisplayListener.NotDisplayedReason.VIDEO_BACK);
            return false;
        }
    }

    public final Long b() {
        return super.b();
    }

    public final Long c() {
        return super.c();
    }

    public final boolean e() {
        return super.e();
    }

    public final boolean e_() {
        return super.e_();
    }

    public final void a(boolean z) {
        super.a(z);
    }
}
