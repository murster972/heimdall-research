package com.startapp.sdk.ads.interstitials;

import android.content.Context;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.model.AdPreferences;

public class ReturnAd extends InterstitialAd {
    private static final long serialVersionUID = 1;

    public ReturnAd(Context context) {
        super(context, AdPreferences.Placement.INAPP_RETURN);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new b(this.f6243a, this, adPreferences, bVar).c();
    }

    /* access modifiers changed from: protected */
    public final long f() {
        return CacheMetaData.a().b().b();
    }
}
