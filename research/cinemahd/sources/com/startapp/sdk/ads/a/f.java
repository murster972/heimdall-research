package com.startapp.sdk.ads.a;

import android.os.Handler;
import android.webkit.WebView;

public final class f extends c {
    public final void a(WebView webView) {
        super.a(webView);
        if (h().equals("interstitial")) {
            webView.setBackgroundColor(0);
        }
    }

    /* access modifiers changed from: protected */
    public final void b(final WebView webView) {
        new Handler().postDelayed(new Runnable() {
            public final void run() {
                try {
                    webView.setBackgroundColor(0);
                } catch (Exception unused) {
                }
            }
        }, 1000);
    }
}
