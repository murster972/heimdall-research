package com.startapp.sdk.ads.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.iab.omid.library.startapp.adsession.b;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.commontracking.CloseTrackingParams;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;

public class c extends b {
    protected WebView c;
    protected b d;
    protected RelativeLayout e;
    protected long f = 0;
    protected boolean g = true;
    protected boolean h = false;
    /* access modifiers changed from: protected */
    public int i = 0;
    protected boolean j = false;
    protected Runnable k = new Runnable() {
        public final void run() {
            c.this.B();
            c.this.p();
        }
    };
    private Long l;
    private Long m;
    private h n;
    private Runnable o = new Runnable() {
        public final void run() {
            c cVar = c.this;
            cVar.g = true;
            WebView webView = cVar.c;
            if (webView != null) {
                webView.setOnTouchListener((View.OnTouchListener) null);
            }
        }
    };

    class a extends WebViewClient {
        a() {
        }

        public final void onPageFinished(WebView webView, String str) {
            c.this.b(webView);
            c cVar = c.this;
            cVar.a("gClientInterface.setMode", cVar.h());
            c.this.a("enableScheme", "externalLinks");
            c.this.a((View) null);
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            if (webView == null || str == null || u.a(webView.getContext(), str)) {
                return true;
            }
            if (!c.this.g) {
                e h = new e(InfoEventCategory.ERROR).f("fake_click").h(c.this.n());
                h.g("jsTag=" + c.this.j).a((Context) c.this.c());
            }
            c cVar = c.this;
            if (!cVar.j || cVar.g) {
                return c.this.a(str, false);
            }
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void A() {
        this.n.a();
    }

    /* access modifiers changed from: protected */
    public void B() {
        String[] l2 = l();
        if (l2 != null && l2.length > 0 && l()[0] != null) {
            com.startapp.sdk.adsbase.a.b((Context) c(), l()[0], a());
        }
    }

    /* access modifiers changed from: protected */
    public TrackingParams C() {
        return new TrackingParams(m());
    }

    /* access modifiers changed from: protected */
    public long D() {
        return (SystemClock.uptimeMillis() - this.f) / 1000;
    }

    /* access modifiers changed from: protected */
    public long E() {
        if (o() != null) {
            return TimeUnit.SECONDS.toMillis(o().longValue());
        }
        return TimeUnit.SECONDS.toMillis(MetaData.G().H());
    }

    /* access modifiers changed from: protected */
    public final void F() {
        if (H() && !this.h && this.i == 0) {
            this.h = true;
            com.startapp.common.b.a((Context) c()).a(new Intent("com.startapp.android.OnVideoCompleted"));
            G();
        }
    }

    /* access modifiers changed from: protected */
    public void G() {
    }

    /* access modifiers changed from: protected */
    public boolean H() {
        return false;
    }

    public void a(Bundle bundle) {
        super.a(bundle);
        if (bundle == null) {
            if (b().hasExtra("lastLoadTime")) {
                this.l = (Long) b().getSerializableExtra("lastLoadTime");
            }
            if (b().hasExtra("adCacheTtl")) {
                this.m = (Long) b().getSerializableExtra("adCacheTtl");
                return;
            }
            return;
        }
        if (bundle.containsKey("postrollHtml")) {
            a(bundle.getString("postrollHtml"));
        }
        if (bundle.containsKey("lastLoadTime")) {
            this.l = (Long) bundle.getSerializable("lastLoadTime");
        }
        if (bundle.containsKey("adCacheTtl")) {
            this.m = (Long) bundle.getSerializable("adCacheTtl");
        }
        this.h = bundle.getBoolean("videoCompletedBroadcastSent", false);
        this.i = bundle.getInt("replayNum");
    }

    public void b(Bundle bundle) {
        super.b(bundle);
        if (g() != null) {
            bundle.putString("postrollHtml", g());
        }
        Long l2 = this.l;
        if (l2 != null) {
            bundle.putLong("lastLoadTime", l2.longValue());
        }
        Long l3 = this.m;
        if (l3 != null) {
            bundle.putLong("adCacheTtl", l3.longValue());
        }
        bundle.putBoolean("videoCompletedBroadcastSent", this.h);
        bundle.putInt("replayNum", this.i);
    }

    /* access modifiers changed from: protected */
    public void b(WebView webView) {
    }

    public void p() {
        super.p();
        k.a().a(false);
        h hVar = this.n;
        if (hVar != null) {
            hVar.a(false);
        }
        c().runOnUiThread(new Runnable() {
            public final void run() {
                WebView webView = c.this.c;
                if (webView != null) {
                    com.startapp.common.b.b.b(webView);
                }
            }
        });
    }

    public boolean r() {
        B();
        k.a().a(false);
        this.n.a(false);
        return false;
    }

    public void s() {
        h hVar = this.n;
        if (hVar != null) {
            hVar.b();
        }
        AdInformationObject adInformationObject = this.f6097a;
        if (adInformationObject != null && adInformationObject.b()) {
            this.f6097a.e();
        }
        WebView webView = this.c;
        if (webView != null) {
            com.startapp.common.b.b.b(webView);
        }
        if (h().equals("back")) {
            p();
        }
    }

    public void u() {
        if (x() instanceof InterstitialAd ? ((InterstitialAd) x()).e_() : false) {
            p();
            return;
        }
        k.a().a(true);
        if (this.n == null) {
            this.n = new h(c(), i(), C(), E());
        }
        WebView webView = this.c;
        if (webView == null) {
            this.e = new RelativeLayout(c());
            this.e.setContentDescription("StartApp Ad");
            this.e.setId(1475346432);
            c().setContentView(this.e);
            try {
                this.c = new WebView(c().getApplicationContext());
                this.c.setBackgroundColor(-16777216);
                c().getWindow().getDecorView().findViewById(16908290).setBackgroundColor(7829367);
                this.c.setVerticalScrollBarEnabled(false);
                this.c.setHorizontalScrollBarEnabled(false);
                this.c.getSettings().setJavaScriptEnabled(true);
                com.startapp.common.b.b.a(this.c);
                this.c.setOnLongClickListener(new View.OnLongClickListener() {
                    public final boolean onLongClick(View view) {
                        return true;
                    }
                });
                this.c.setLongClickable(false);
                this.c.addJavascriptInterface(z(), "startappwall");
                A();
                a(this.c);
                u.a((Context) c(), this.c, g());
                this.j = "true".equals(u.a(g(), "@jsTag@", "@jsTag@"));
                y();
                this.e.addView(this.c, new RelativeLayout.LayoutParams(-1, -1));
                a(this.e);
            } catch (Throwable th) {
                new e(th).a((Context) c());
                p();
            }
        } else {
            com.startapp.common.b.b.c(webView);
            this.n.a();
        }
        this.f = SystemClock.uptimeMillis();
    }

    public final void w() {
        super.w();
        b bVar = this.d;
        if (bVar != null) {
            bVar.b();
            this.d = null;
        }
        u.a((Object) this.c);
    }

    /* access modifiers changed from: protected */
    public void y() {
        this.c.setWebViewClient(new a());
        this.c.setWebChromeClient(new WebChromeClient());
    }

    /* access modifiers changed from: protected */
    public com.startapp.sdk.e.b z() {
        Activity c2 = c();
        Runnable runnable = this.k;
        return new com.startapp.sdk.e.b(c2, runnable, runnable, this.o, a(), a(0));
    }

    /* access modifiers changed from: protected */
    public boolean b(String str) {
        return !this.j && str.contains("index=");
    }

    private void b(String str, int i2, boolean z) {
        com.startapp.common.b.a((Context) c()).a(new Intent("com.startapp.android.OnClickCallback"));
        com.startapp.sdk.adsbase.a.a(c(), str, i2 < j().length ? j()[i2] : null, a(), a(i2) && !com.startapp.sdk.adsbase.a.a(c().getApplicationContext(), this.b), z);
        p();
    }

    public void a(WebView webView) {
        this.g = false;
        webView.setOnTouchListener(new View.OnTouchListener() {
            public final boolean onTouch(View view, MotionEvent motionEvent) {
                c.this.g = true;
                if (motionEvent.getAction() == 2) {
                    return true;
                }
                return false;
            }
        });
    }

    /* access modifiers changed from: protected */
    public void a(View view) {
        View a2;
        if (MetaData.G().S() && this.d == null) {
            this.d = com.startapp.sdk.omsdk.a.a(this.c);
            if (this.d != null && this.c != null) {
                AdInformationObject adInformationObject = this.f6097a;
                if (!(adInformationObject == null || (a2 = adInformationObject.a()) == null)) {
                    this.d.b(a2);
                }
                if (view != null) {
                    this.d.b(view);
                }
                this.d.a(this.c);
                this.d.a();
                com.iab.omid.library.startapp.adsession.a.a(this.d).a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, Object... objArr) {
        u.a(this.c, str, objArr);
    }

    /* access modifiers changed from: protected */
    public static long a(long j2) {
        long j3 = j2 % 1000;
        if (j3 == 0) {
            return 1000;
        }
        return j3;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x004f  */
    /* JADX WARNING: Removed duplicated region for block: B:8:0x002b A[SYNTHETIC, Splitter:B:8:0x002b] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(java.lang.String r6, boolean r7) {
        /*
            r5 = this;
            com.startapp.sdk.adsbase.h r0 = r5.n
            r1 = 1
            r0.a((boolean) r1)
            com.startapp.sdk.adsbase.Ad r0 = r5.x()
            android.app.Activity r2 = r5.c()
            android.content.Context r2 = r2.getApplicationContext()
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r3 = r5.b
            boolean r2 = com.startapp.sdk.adsbase.a.a((android.content.Context) r2, (com.startapp.sdk.adsbase.model.AdPreferences.Placement) r3)
            r3 = 0
            if (r2 == 0) goto L_0x0024
            com.startapp.sdk.adsbase.j.u.b()
            boolean r0 = r0 instanceof com.startapp.sdk.ads.splash.SplashAd
            if (r0 != 0) goto L_0x0024
            r0 = 1
            goto L_0x0025
        L_0x0024:
            r0 = 0
        L_0x0025:
            boolean r2 = r5.b((java.lang.String) r6)
            if (r2 == 0) goto L_0x004f
            int r2 = com.startapp.sdk.adsbase.a.a((java.lang.String) r6)     // Catch:{ Exception -> 0x0041 }
            boolean[] r4 = r5.d()     // Catch:{ Exception -> 0x0041 }
            boolean r4 = r4[r2]     // Catch:{ Exception -> 0x0041 }
            if (r4 == 0) goto L_0x003d
            if (r0 != 0) goto L_0x003d
            r5.a(r6, r2, r7)     // Catch:{ Exception -> 0x0041 }
            goto L_0x0060
        L_0x003d:
            r5.b(r6, r2, r7)     // Catch:{ Exception -> 0x0041 }
            goto L_0x0060
        L_0x0041:
            r6 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r7 = new com.startapp.sdk.adsbase.infoevents.e
            r7.<init>((java.lang.Throwable) r6)
            android.app.Activity r6 = r5.c()
            r7.a((android.content.Context) r6)
            return r3
        L_0x004f:
            boolean[] r2 = r5.d()
            boolean r2 = r2[r3]
            if (r2 == 0) goto L_0x005d
            if (r0 != 0) goto L_0x005d
            r5.a(r6, r3, r7)
            goto L_0x0060
        L_0x005d:
            r5.b(r6, r3, r7)
        L_0x0060:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.a.c.a(java.lang.String, boolean):boolean");
    }

    private void a(String str, int i2, boolean z) {
        int i3 = i2;
        Activity c2 = c();
        String str2 = null;
        String str3 = i3 < j().length ? j()[i3] : null;
        if (i3 < k().length) {
            str2 = k()[i3];
        }
        com.startapp.sdk.adsbase.a.a(c2, str, str3, str2, a(), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), a(i3), b(i3), z, new Runnable() {
            public final void run() {
                c.this.p();
            }
        });
    }

    private TrackingParams a() {
        return new CloseTrackingParams(D(), m());
    }
}
