package com.startapp.sdk.ads.a;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.RelativeLayout;
import com.facebook.react.uimanager.ViewProps;
import com.startapp.sdk.ads.splash.d;
import com.startapp.sdk.ads.video.VideoMode;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.inappbrowser.a;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    protected AdInformationObject f6097a = null;
    protected AdPreferences.Placement b;
    private Intent c;
    private Activity d;
    private BroadcastReceiver e = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            b.this.p();
        }
    };
    private String[] f;
    private boolean[] g;
    private boolean[] h = {true};
    private String i;
    private String[] j;
    private String[] k;
    private String[] l;
    private Ad m;
    private String n;
    private boolean o;
    private AdInformationOverrides p;
    private String q;
    private Long r;
    private Boolean[] s = null;
    private int t = 0;
    private boolean u = false;
    private boolean v = false;

    /* renamed from: com.startapp.sdk.ads.a.b$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6100a = new int[AdPreferences.Placement.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|(3:11|12|14)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.sdk.adsbase.model.AdPreferences$Placement[] r0 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6100a = r0
                int[] r0 = f6100a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OFFER_WALL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6100a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_RETURN     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6100a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OVERLAY     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6100a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_SPLASH     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6100a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_FULL_SCREEN     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r0 = f6100a     // Catch:{ NoSuchFieldError -> 0x004b }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_BROWSER     // Catch:{ NoSuchFieldError -> 0x004b }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r2 = 6
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.a.b.AnonymousClass3.<clinit>():void");
        }
    }

    public static b a(Activity activity, Intent intent, AdPreferences.Placement placement) {
        b bVar;
        switch (AnonymousClass3.f6100a[placement.ordinal()]) {
            case 1:
                u.b();
                bVar = new e();
                break;
            case 2:
            case 3:
                u.b();
                if (!intent.getBooleanExtra("videoAd", false)) {
                    if (!intent.getBooleanExtra("mraidAd", false)) {
                        bVar = new f();
                        break;
                    } else {
                        bVar = new d();
                        break;
                    }
                } else {
                    bVar = new VideoMode();
                    break;
                }
            case 4:
                u.b();
                bVar = new d();
                break;
            case 5:
            case 6:
                u.b();
                Uri data = intent.getData();
                if (data != null) {
                    bVar = new a(data.toString());
                    break;
                } else {
                    return null;
                }
            default:
                bVar = new a();
                break;
        }
        bVar.c = intent;
        bVar.d = activity;
        bVar.i = intent.getStringExtra(ViewProps.POSITION);
        bVar.j = intent.getStringArrayExtra("tracking");
        bVar.k = intent.getStringArrayExtra("trackingClickUrl");
        bVar.l = intent.getStringArrayExtra("packageNames");
        bVar.f = intent.getStringArrayExtra("closingUrl");
        bVar.g = intent.getBooleanArrayExtra("smartRedirect");
        bVar.h = intent.getBooleanArrayExtra("browserEnabled");
        bVar.q = intent.getStringExtra("adTag");
        String stringExtra = intent.getStringExtra("htmlUuid");
        if (stringExtra != null) {
            if (AdsConstants.b.booleanValue()) {
                bVar.a(com.startapp.sdk.adsbase.cache.a.a().a(stringExtra));
            } else {
                bVar.a(com.startapp.sdk.adsbase.cache.a.a().b(stringExtra));
            }
        }
        bVar.o = intent.getBooleanExtra("isSplash", false);
        bVar.p = (AdInformationOverrides) intent.getSerializableExtra("adInfoOverride");
        bVar.b = placement;
        bVar.f = intent.getStringArrayExtra("closingUrl");
        bVar.t = intent.getIntExtra("rewardDuration", 0);
        bVar.u = intent.getBooleanExtra("rewardedHideTimer", false);
        if (bVar.g == null) {
            bVar.g = new boolean[]{true};
        }
        if (bVar.h == null) {
            bVar.h = new boolean[]{true};
        }
        bVar.m = (Ad) intent.getSerializableExtra("ad");
        long longExtra = intent.getLongExtra("delayImpressionSeconds", -1);
        if (longExtra != -1) {
            bVar.r = Long.valueOf(longExtra);
        }
        bVar.s = (Boolean[]) intent.getSerializableExtra("sendRedirectHops");
        return bVar;
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        return false;
    }

    public final Intent b() {
        return this.c;
    }

    public void b(Bundle bundle) {
    }

    public final Activity c() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public final boolean[] d() {
        return this.g;
    }

    public final int e() {
        return this.t;
    }

    public final boolean f() {
        return this.u;
    }

    /* access modifiers changed from: protected */
    public final String g() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public final String h() {
        return this.i;
    }

    /* access modifiers changed from: protected */
    public final String[] i() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final String[] j() {
        return this.k;
    }

    /* access modifiers changed from: protected */
    public final String[] k() {
        return this.l;
    }

    /* access modifiers changed from: protected */
    public final String[] l() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final String m() {
        return this.q;
    }

    /* access modifiers changed from: protected */
    public final String n() {
        try {
            String[] strArr = this.j;
            if (strArr == null || strArr.length <= 0) {
                return "";
            }
            return com.startapp.sdk.adsbase.a.e(strArr[0]);
        } catch (Exception e2) {
            new e((Throwable) e2).a((Context) this.d);
            return "";
        }
    }

    public final Long o() {
        return this.r;
    }

    public void p() {
        this.d.runOnUiThread(new Runnable() {
            public final void run() {
                b.this.c().finish();
            }
        });
    }

    public void q() {
        com.startapp.common.b.a((Context) this.d).a(new Intent("com.startapp.android.HideDisplayBroadcastListener"));
    }

    public boolean r() {
        return false;
    }

    public void s() {
        p();
    }

    public void t() {
    }

    public abstract void u();

    public void v() {
    }

    public void w() {
        if (this.e != null) {
            com.startapp.common.b.a((Context) this.d).a(this.e);
        }
        this.e = null;
    }

    public final Ad x() {
        return this.m;
    }

    public final Boolean b(int i2) {
        Boolean[] boolArr = this.s;
        if (boolArr == null || i2 < 0 || i2 >= boolArr.length) {
            return null;
        }
        return boolArr[i2];
    }

    /* access modifiers changed from: protected */
    public final boolean a(int i2) {
        boolean[] zArr = this.h;
        if (zArr == null || i2 < 0 || i2 >= zArr.length) {
            return true;
        }
        return zArr[i2];
    }

    /* access modifiers changed from: protected */
    public final void a(RelativeLayout relativeLayout) {
        this.f6097a = new AdInformationObject(this.d, AdInformationObject.Size.LARGE, this.b, this.p);
        this.f6097a.a(relativeLayout);
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        String str2;
        if (str == null || (str2 = this.q) == null || str2.length() <= 0) {
            this.n = str;
        } else {
            this.n = str.replaceAll("startapp_adtag_placeholder", this.q);
        }
    }

    public void a(Bundle bundle) {
        com.startapp.common.b.a((Context) this.d).a(this.e, new IntentFilter("com.startapp.android.CloseAdActivity"));
    }
}
