package com.startapp.sdk.ads.offerWall.offerWallJson;

import android.content.Context;
import android.content.Intent;
import com.facebook.react.uimanager.ViewProps;
import com.startapp.sdk.ads.list3d.List3DActivity;
import com.startapp.sdk.ads.list3d.g;
import com.startapp.sdk.adsbase.ActivityExtra;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.util.UUID;

public class OfferWall3DAd extends JsonAd implements f {
    private static String b = null;
    private static final long serialVersionUID = 1;
    private final String uuid = UUID.randomUUID().toString();

    public OfferWall3DAd(Context context) {
        super(context, AdPreferences.Placement.INAPP_OFFER_WALL);
        if (b == null) {
            b = u.f(context);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new a(this.f6243a, this, adPreferences, bVar).c();
    }

    public final Long b() {
        return super.b();
    }

    public final Long c() {
        return super.c();
    }

    public final boolean e() {
        return super.e();
    }

    public final boolean e_() {
        return super.e_();
    }

    public final boolean a(String str) {
        g.a().a(this.uuid).b(a.a());
        ActivityExtra activityExtra = this.activityExtra;
        boolean a2 = activityExtra != null ? activityExtra.a() : false;
        if (super.e_()) {
            a(AdDisplayListener.NotDisplayedReason.AD_EXPIRED);
            return false;
        }
        Intent intent = new Intent(this.f6243a, List3DActivity.class);
        intent.putExtra("adInfoOverride", getAdInfoOverride());
        intent.putExtra("fullscreen", a2);
        intent.putExtra("adTag", str);
        intent.putExtra("lastLoadTime", super.b());
        intent.putExtra("adCacheTtl", super.c());
        intent.putExtra(ViewProps.POSITION, a.b());
        intent.putExtra("listModelUuid", this.uuid);
        intent.addFlags(343932928);
        this.f6243a.startActivity(intent);
        if (AdsConstants.b.booleanValue()) {
            return true;
        }
        setState(Ad.AdState.UN_INITIALIZED);
        return true;
    }

    public final String a() {
        return this.uuid;
    }

    public final void a(boolean z) {
        super.a(z);
    }
}
