package com.startapp.sdk.ads.offerWall.offerWallHtml;

import android.content.Context;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;

public final class a extends com.startapp.sdk.e.a {
    public a(Context context, OfferWallAd offerWallAd, AdPreferences adPreferences, b bVar) {
        super(context, offerWallAd, adPreferences, bVar, AdPreferences.Placement.INAPP_OFFER_WALL, true);
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest a2 = super.a();
        if (a2 == null) {
            return null;
        }
        a2.e(AdsCommonMetaData.a().g());
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void a(Boolean bool) {
        super.a(bool);
        a(bool.booleanValue());
    }
}
