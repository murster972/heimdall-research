package com.startapp.sdk.ads.list3d;

import android.os.Parcel;
import android.os.Parcelable;

final class i extends a implements Parcelable {
    public static final Parcelable.Creator<i> CREATOR = new Parcelable.Creator<i>() {
        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new i(parcel);
        }

        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new i[i];
        }
    };
    private float c;
    private float d;

    public i() {
        this.c = 0.9f;
        this.d = 0.6f;
    }

    /* access modifiers changed from: protected */
    public final void a(int i) {
        this.b += d() * this.d;
        float f = this.f6142a;
        float f2 = this.b;
        this.f6142a = f + ((((float) i) * f2) / 1000.0f);
        this.b = f2 * this.c;
    }

    public final int describeContents() {
        return 0;
    }

    public final String toString() {
        return super.toString() + ", Friction: [" + this.c + "], Snap:[" + this.d + "]";
    }

    public final void writeToParcel(Parcel parcel, int i) {
        super.writeToParcel(parcel, i);
        parcel.writeFloat(this.c);
        parcel.writeFloat(this.d);
    }

    public i(Parcel parcel) {
        super(parcel);
        this.c = parcel.readFloat();
        this.d = parcel.readFloat();
    }

    public final void a(double d2) {
        super.a(d2);
    }
}
