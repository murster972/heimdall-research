package com.startapp.sdk.ads.list3d;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    private static g f6151a = new g();
    private Map<String, f> b = new ConcurrentHashMap();

    private g() {
    }

    public static g a() {
        return f6151a;
    }

    public final void b(String str) {
        this.b.remove(str);
    }

    public final f a(String str) {
        if (this.b.containsKey(str)) {
            return this.b.get(str);
        }
        f fVar = new f();
        this.b.put(str, fVar);
        return fVar;
    }
}
