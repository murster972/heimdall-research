package com.startapp.sdk.ads.list3d;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.view.View;
import android.webkit.WebView;
import com.startapp.common.b;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.commontracking.CloseTrackingParams;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.k;
import java.util.List;

public class List3DActivity extends Activity implements h {

    /* renamed from: a  reason: collision with root package name */
    String f6131a;
    String b;
    List<d> c;
    private List3DView d;
    private ProgressDialog e = null;
    private WebView f = null;
    private int g;
    private AdInformationObject h;
    private Long i;
    private Long j;
    private String k;
    private long l = 0;
    private long m = 0;
    private BroadcastReceiver n = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            List3DActivity.this.finish();
        }
    };

    /* access modifiers changed from: protected */
    public final TrackingParams a() {
        this.l = SystemClock.uptimeMillis();
        return new CloseTrackingParams((this.l - this.m) / 1000, this.b);
    }

    /* access modifiers changed from: protected */
    public final String b() {
        List<d> list = this.c;
        if (list == null || list.isEmpty() || this.c.get(0).d() == null) {
            return "";
        }
        return this.c.get(0).d();
    }

    public void finish() {
        try {
            this.l = SystemClock.uptimeMillis();
            a.b((Context) this, b(), a());
            k.a().a(false);
            if (this.g == getResources().getConfiguration().orientation) {
                b.a((Context) this).a(new Intent("com.startapp.android.HideDisplayBroadcastListener"));
            }
            synchronized (this) {
                if (this.n != null) {
                    b.a((Context) this).a(this.n);
                    this.n = null;
                }
            }
            if (this.f6131a != null) {
                g.a().a(this.f6131a).d();
                if (!AdsConstants.b.booleanValue()) {
                    g.a().b(this.f6131a);
                }
            }
        } catch (Throwable th) {
            new e(th).a((Context) this);
        }
        super.finish();
    }

    public void onBackPressed() {
        g.a().a(this.f6131a).d();
        super.onBackPressed();
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: android.widget.TextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v6, resolved type: android.widget.ImageButton} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v7, resolved type: android.widget.TextView} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v8, resolved type: android.widget.TextView} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r14) {
        /*
            r13 = this;
            r0 = 0
            r13.overridePendingTransition(r0, r0)     // Catch:{ all -> 0x033d }
            super.onCreate(r14)     // Catch:{ all -> 0x033d }
            android.content.Intent r1 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.lang.String r2 = "fullscreen"
            boolean r1 = r1.getBooleanExtra(r2, r0)     // Catch:{ all -> 0x033d }
            r2 = 1
            if (r1 == 0) goto L_0x0020
            r13.requestWindowFeature(r2)     // Catch:{ all -> 0x033d }
            android.view.Window r1 = r13.getWindow()     // Catch:{ all -> 0x033d }
            r3 = 1024(0x400, float:1.435E-42)
            r1.setFlags(r3, r3)     // Catch:{ all -> 0x033d }
        L_0x0020:
            java.lang.String r1 = "adCacheTtl"
            java.lang.String r3 = "lastLoadTime"
            if (r14 != 0) goto L_0x004d
            com.startapp.common.b r14 = com.startapp.common.b.a((android.content.Context) r13)     // Catch:{ all -> 0x033d }
            android.content.Intent r4 = new android.content.Intent     // Catch:{ all -> 0x033d }
            java.lang.String r5 = "com.startapp.android.ShowDisplayBroadcastListener"
            r4.<init>(r5)     // Catch:{ all -> 0x033d }
            r14.a((android.content.Intent) r4)     // Catch:{ all -> 0x033d }
            android.content.Intent r14 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.io.Serializable r14 = r14.getSerializableExtra(r3)     // Catch:{ all -> 0x033d }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x033d }
            r13.i = r14     // Catch:{ all -> 0x033d }
            android.content.Intent r14 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.io.Serializable r14 = r14.getSerializableExtra(r1)     // Catch:{ all -> 0x033d }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x033d }
            r13.j = r14     // Catch:{ all -> 0x033d }
            goto L_0x0069
        L_0x004d:
            boolean r4 = r14.containsKey(r3)     // Catch:{ all -> 0x033d }
            if (r4 == 0) goto L_0x005b
            java.io.Serializable r3 = r14.getSerializable(r3)     // Catch:{ all -> 0x033d }
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ all -> 0x033d }
            r13.i = r3     // Catch:{ all -> 0x033d }
        L_0x005b:
            boolean r3 = r14.containsKey(r1)     // Catch:{ all -> 0x033d }
            if (r3 == 0) goto L_0x0069
            java.io.Serializable r14 = r14.getSerializable(r1)     // Catch:{ all -> 0x033d }
            java.lang.Long r14 = (java.lang.Long) r14     // Catch:{ all -> 0x033d }
            r13.j = r14     // Catch:{ all -> 0x033d }
        L_0x0069:
            android.content.Intent r14 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.lang.String r1 = "position"
            java.lang.String r14 = r14.getStringExtra(r1)     // Catch:{ all -> 0x033d }
            r13.k = r14     // Catch:{ all -> 0x033d }
            android.content.Intent r14 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.lang.String r1 = "listModelUuid"
            java.lang.String r14 = r14.getStringExtra(r1)     // Catch:{ all -> 0x033d }
            r13.f6131a = r14     // Catch:{ all -> 0x033d }
            com.startapp.common.b r14 = com.startapp.common.b.a((android.content.Context) r13)     // Catch:{ all -> 0x033d }
            android.content.BroadcastReceiver r1 = r13.n     // Catch:{ all -> 0x033d }
            android.content.IntentFilter r3 = new android.content.IntentFilter     // Catch:{ all -> 0x033d }
            java.lang.String r4 = "com.startapp.android.CloseAdActivity"
            r3.<init>(r4)     // Catch:{ all -> 0x033d }
            r14.a(r1, r3)     // Catch:{ all -> 0x033d }
            android.content.res.Resources r14 = r13.getResources()     // Catch:{ all -> 0x033d }
            android.content.res.Configuration r14 = r14.getConfiguration()     // Catch:{ all -> 0x033d }
            int r14 = r14.orientation     // Catch:{ all -> 0x033d }
            r13.g = r14     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.j.u.a((android.app.Activity) r13, (boolean) r2)     // Catch:{ all -> 0x033d }
            android.content.Intent r14 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.lang.String r1 = "overlay"
            boolean r14 = r14.getBooleanExtra(r1, r0)     // Catch:{ all -> 0x033d }
            r13.requestWindowFeature(r2)     // Catch:{ all -> 0x033d }
            android.content.Intent r1 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.lang.String r3 = "adTag"
            java.lang.String r1 = r1.getStringExtra(r3)     // Catch:{ all -> 0x033d }
            r13.b = r1     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r1 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            int r1 = r1.e()     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r3 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            int r3 = r3.f()     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r4 = new com.startapp.sdk.ads.list3d.List3DView     // Catch:{ all -> 0x033d }
            java.lang.String r5 = r13.b     // Catch:{ all -> 0x033d }
            java.lang.String r6 = r13.f6131a     // Catch:{ all -> 0x033d }
            r4.<init>(r13, r5, r6)     // Catch:{ all -> 0x033d }
            r13.d = r4     // Catch:{ all -> 0x033d }
            android.graphics.drawable.GradientDrawable r4 = new android.graphics.drawable.GradientDrawable     // Catch:{ all -> 0x033d }
            android.graphics.drawable.GradientDrawable$Orientation r5 = android.graphics.drawable.GradientDrawable.Orientation.TOP_BOTTOM     // Catch:{ all -> 0x033d }
            r6 = 2
            int[] r7 = new int[r6]     // Catch:{ all -> 0x033d }
            r7[r0] = r1     // Catch:{ all -> 0x033d }
            r7[r2] = r3     // Catch:{ all -> 0x033d }
            r4.<init>(r5, r7)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r1 = r13.d     // Catch:{ all -> 0x033d }
            r1.setBackgroundDrawable(r4)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.g r1 = com.startapp.sdk.ads.list3d.g.a()     // Catch:{ all -> 0x033d }
            java.lang.String r3 = r13.f6131a     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.f r1 = r1.a(r3)     // Catch:{ all -> 0x033d }
            java.util.List r1 = r1.e()     // Catch:{ all -> 0x033d }
            r13.c = r1     // Catch:{ all -> 0x033d }
            java.util.List<com.startapp.sdk.ads.list3d.d> r1 = r13.c     // Catch:{ all -> 0x033d }
            if (r1 != 0) goto L_0x00ff
            r13.finish()     // Catch:{ all -> 0x033d }
            return
        L_0x00ff:
            if (r14 == 0) goto L_0x0114
            com.startapp.common.b r1 = com.startapp.common.b.a((android.content.Context) r13)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r3 = r13.d     // Catch:{ all -> 0x033d }
            android.content.BroadcastReceiver r3 = r3.p     // Catch:{ all -> 0x033d }
            android.content.IntentFilter r4 = new android.content.IntentFilter     // Catch:{ all -> 0x033d }
            java.lang.String r5 = "com.startapp.android.Activity3DGetValues"
            r4.<init>(r5)     // Catch:{ all -> 0x033d }
            r1.a(r3, r4)     // Catch:{ all -> 0x033d }
            goto L_0x0123
        L_0x0114:
            com.startapp.sdk.ads.list3d.List3DView r1 = r13.d     // Catch:{ all -> 0x033d }
            r1.setStarted()     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r1 = r13.d     // Catch:{ all -> 0x033d }
            r1.setHint(r2)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r1 = r13.d     // Catch:{ all -> 0x033d }
            r1.setFade(r2)     // Catch:{ all -> 0x033d }
        L_0x0123:
            com.startapp.sdk.ads.list3d.c r1 = new com.startapp.sdk.ads.list3d.c     // Catch:{ all -> 0x033d }
            java.util.List<com.startapp.sdk.ads.list3d.d> r3 = r13.c     // Catch:{ all -> 0x033d }
            java.lang.String r4 = r13.b     // Catch:{ all -> 0x033d }
            java.lang.String r5 = r13.f6131a     // Catch:{ all -> 0x033d }
            r1.<init>(r13, r3, r4, r5)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.g r3 = com.startapp.sdk.ads.list3d.g.a()     // Catch:{ all -> 0x033d }
            java.lang.String r4 = r13.f6131a     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.f r3 = r3.a(r4)     // Catch:{ all -> 0x033d }
            if (r14 != 0) goto L_0x013c
            r14 = 1
            goto L_0x013d
        L_0x013c:
            r14 = 0
        L_0x013d:
            r3.a(r13, r14)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r14 = r13.d     // Catch:{ all -> 0x033d }
            r14.setAdapter(r1)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r14 = r13.d     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.i r1 = new com.startapp.sdk.ads.list3d.i     // Catch:{ all -> 0x033d }
            r1.<init>()     // Catch:{ all -> 0x033d }
            r14.setDynamics(r1)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r14 = r13.d     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DActivity$2 r1 = new com.startapp.sdk.ads.list3d.List3DActivity$2     // Catch:{ all -> 0x033d }
            r1.<init>()     // Catch:{ all -> 0x033d }
            r14.setOnItemClickListener(r1)     // Catch:{ all -> 0x033d }
            android.widget.RelativeLayout r14 = new android.widget.RelativeLayout     // Catch:{ all -> 0x033d }
            r14.<init>(r13)     // Catch:{ all -> 0x033d }
            java.lang.String r1 = "StartApp Ad"
            r14.setContentDescription(r1)     // Catch:{ all -> 0x033d }
            r1 = 1475346432(0x57f00000, float:5.27765581E14)
            r14.setId(r1)     // Catch:{ all -> 0x033d }
            android.widget.FrameLayout$LayoutParams r1 = new android.widget.FrameLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r3 = -1
            r1.<init>(r3, r3)     // Catch:{ all -> 0x033d }
            android.widget.RelativeLayout$LayoutParams r4 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r4.<init>(r3, r3)     // Catch:{ all -> 0x033d }
            android.widget.LinearLayout r5 = new android.widget.LinearLayout     // Catch:{ all -> 0x033d }
            r5.<init>(r13)     // Catch:{ all -> 0x033d }
            r5.setOrientation(r2)     // Catch:{ all -> 0x033d }
            r14.addView(r5, r4)     // Catch:{ all -> 0x033d }
            android.widget.RelativeLayout r4 = new android.widget.RelativeLayout     // Catch:{ all -> 0x033d }
            r4.<init>(r13)     // Catch:{ all -> 0x033d }
            android.widget.RelativeLayout$LayoutParams r7 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r8 = -2
            r7.<init>(r3, r8)     // Catch:{ all -> 0x033d }
            r4.setLayoutParams(r7)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r7 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.Integer r7 = r7.i()     // Catch:{ all -> 0x033d }
            int r7 = r7.intValue()     // Catch:{ all -> 0x033d }
            r4.setBackgroundColor(r7)     // Catch:{ all -> 0x033d }
            r5.addView(r4)     // Catch:{ all -> 0x033d }
            android.widget.TextView r7 = new android.widget.TextView     // Catch:{ all -> 0x033d }
            r7.<init>(r13)     // Catch:{ all -> 0x033d }
            android.widget.RelativeLayout$LayoutParams r9 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r9.<init>(r8, r8)     // Catch:{ all -> 0x033d }
            r10 = 13
            r9.addRule(r10)     // Catch:{ all -> 0x033d }
            r7.setLayoutParams(r9)     // Catch:{ all -> 0x033d }
            int r9 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r6)     // Catch:{ all -> 0x033d }
            r10 = 5
            int r10 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r10)     // Catch:{ all -> 0x033d }
            r7.setPadding(r0, r9, r0, r10)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r9 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.Integer r9 = r9.l()     // Catch:{ all -> 0x033d }
            int r9 = r9.intValue()     // Catch:{ all -> 0x033d }
            r7.setTextColor(r9)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r9 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.Integer r9 = r9.k()     // Catch:{ all -> 0x033d }
            int r9 = r9.intValue()     // Catch:{ all -> 0x033d }
            float r9 = (float) r9     // Catch:{ all -> 0x033d }
            r7.setTextSize(r9)     // Catch:{ all -> 0x033d }
            r7.setSingleLine(r2)     // Catch:{ all -> 0x033d }
            android.text.TextUtils$TruncateAt r9 = android.text.TextUtils.TruncateAt.END     // Catch:{ all -> 0x033d }
            r7.setEllipsize(r9)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r9 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.String r9 = r9.j()     // Catch:{ all -> 0x033d }
            r7.setText(r9)     // Catch:{ all -> 0x033d }
            r9 = 1075838976(0x40200000, float:2.5)
            r10 = -1073741824(0xffffffffc0000000, float:-2.0)
            r11 = 1073741824(0x40000000, float:2.0)
            r12 = -11513776(0xffffffffff505050, float:-2.7689643E38)
            r7.setShadowLayer(r9, r10, r11, r12)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r9 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.util.Set r9 = r9.m()     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.j.t.a((android.widget.TextView) r7, (java.util.Set<java.lang.String>) r9)     // Catch:{ all -> 0x033d }
            r4.addView(r7)     // Catch:{ all -> 0x033d }
            android.widget.RelativeLayout$LayoutParams r7 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r7.<init>(r8, r8)     // Catch:{ all -> 0x033d }
            r9 = 11
            r7.addRule(r9)     // Catch:{ all -> 0x033d }
            r9 = 15
            r7.addRule(r9)     // Catch:{ all -> 0x033d }
            java.lang.String r9 = "close_button.png"
            android.graphics.Bitmap r9 = com.startapp.sdk.adsbase.j.a.a(r13, r9)     // Catch:{ all -> 0x033d }
            if (r9 == 0) goto L_0x023a
            android.widget.ImageButton r10 = new android.widget.ImageButton     // Catch:{ all -> 0x033d }
            r11 = 0
            r12 = 16973839(0x103000f, float:2.4060942E-38)
            r10.<init>(r13, r11, r12)     // Catch:{ all -> 0x033d }
            r11 = 36
            int r12 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r11)     // Catch:{ all -> 0x033d }
            int r11 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r11)     // Catch:{ all -> 0x033d }
            android.graphics.Bitmap r9 = android.graphics.Bitmap.createScaledBitmap(r9, r12, r11, r2)     // Catch:{ all -> 0x033d }
            r10.setImageBitmap(r9)     // Catch:{ all -> 0x033d }
            goto L_0x0249
        L_0x023a:
            android.widget.TextView r10 = new android.widget.TextView     // Catch:{ all -> 0x033d }
            r10.<init>(r13)     // Catch:{ all -> 0x033d }
            java.lang.String r9 = "   x   "
            r10.setText(r9)     // Catch:{ all -> 0x033d }
            r9 = 1101004800(0x41a00000, float:20.0)
            r10.setTextSize(r9)     // Catch:{ all -> 0x033d }
        L_0x0249:
            r10.setLayoutParams(r7)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DActivity$3 r7 = new com.startapp.sdk.ads.list3d.List3DActivity$3     // Catch:{ all -> 0x033d }
            r7.<init>()     // Catch:{ all -> 0x033d }
            r10.setOnClickListener(r7)     // Catch:{ all -> 0x033d }
            java.lang.String r7 = "x"
            r10.setContentDescription(r7)     // Catch:{ all -> 0x033d }
            r7 = 1475346435(0x57f00003, float:5.27765682E14)
            r10.setId(r7)     // Catch:{ all -> 0x033d }
            r4.addView(r10)     // Catch:{ all -> 0x033d }
            android.view.View r4 = new android.view.View     // Catch:{ all -> 0x033d }
            r4.<init>(r13)     // Catch:{ all -> 0x033d }
            android.widget.LinearLayout$LayoutParams r7 = new android.widget.LinearLayout$LayoutParams     // Catch:{ all -> 0x033d }
            int r9 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r6)     // Catch:{ all -> 0x033d }
            r7.<init>(r3, r9)     // Catch:{ all -> 0x033d }
            r4.setLayoutParams(r7)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r7 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.Integer r7 = r7.n()     // Catch:{ all -> 0x033d }
            int r7 = r7.intValue()     // Catch:{ all -> 0x033d }
            r4.setBackgroundColor(r7)     // Catch:{ all -> 0x033d }
            r5.addView(r4)     // Catch:{ all -> 0x033d }
            android.widget.LinearLayout$LayoutParams r4 = new android.widget.LinearLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r4.<init>(r3, r0)     // Catch:{ all -> 0x033d }
            r7 = 1065353216(0x3f800000, float:1.0)
            r4.weight = r7     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r7 = r13.d     // Catch:{ all -> 0x033d }
            r7.setLayoutParams(r4)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DView r4 = r13.d     // Catch:{ all -> 0x033d }
            r5.addView(r4)     // Catch:{ all -> 0x033d }
            android.widget.LinearLayout r4 = new android.widget.LinearLayout     // Catch:{ all -> 0x033d }
            r4.<init>(r13)     // Catch:{ all -> 0x033d }
            android.widget.LinearLayout$LayoutParams r7 = new android.widget.LinearLayout$LayoutParams     // Catch:{ all -> 0x033d }
            r7.<init>(r3, r8)     // Catch:{ all -> 0x033d }
            r3 = 80
            r7.gravity = r3     // Catch:{ all -> 0x033d }
            r4.setLayoutParams(r7)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r3 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.Integer r3 = r3.w()     // Catch:{ all -> 0x033d }
            int r3 = r3.intValue()     // Catch:{ all -> 0x033d }
            r4.setBackgroundColor(r3)     // Catch:{ all -> 0x033d }
            r3 = 17
            r4.setGravity(r3)     // Catch:{ all -> 0x033d }
            r5.addView(r4)     // Catch:{ all -> 0x033d }
            android.widget.TextView r3 = new android.widget.TextView     // Catch:{ all -> 0x033d }
            r3.<init>(r13)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.AdsCommonMetaData r5 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x033d }
            java.lang.Integer r5 = r5.x()     // Catch:{ all -> 0x033d }
            int r5 = r5.intValue()     // Catch:{ all -> 0x033d }
            r3.setTextColor(r5)     // Catch:{ all -> 0x033d }
            int r5 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r6)     // Catch:{ all -> 0x033d }
            r6 = 3
            int r6 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r6)     // Catch:{ all -> 0x033d }
            r3.setPadding(r0, r5, r0, r6)     // Catch:{ all -> 0x033d }
            java.lang.String r0 = "Powered By "
            r3.setText(r0)     // Catch:{ all -> 0x033d }
            r0 = 1098907648(0x41800000, float:16.0)
            r3.setTextSize(r0)     // Catch:{ all -> 0x033d }
            r4.addView(r3)     // Catch:{ all -> 0x033d }
            android.widget.ImageView r0 = new android.widget.ImageView     // Catch:{ all -> 0x033d }
            r0.<init>(r13)     // Catch:{ all -> 0x033d }
            java.lang.String r3 = "logo.png"
            android.graphics.Bitmap r3 = com.startapp.sdk.adsbase.j.a.a(r13, r3)     // Catch:{ all -> 0x033d }
            r5 = 56
            int r5 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r5)     // Catch:{ all -> 0x033d }
            r6 = 12
            int r6 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r13, (int) r6)     // Catch:{ all -> 0x033d }
            android.graphics.Bitmap r2 = android.graphics.Bitmap.createScaledBitmap(r3, r5, r6, r2)     // Catch:{ all -> 0x033d }
            r0.setImageBitmap(r2)     // Catch:{ all -> 0x033d }
            r4.addView(r0)     // Catch:{ all -> 0x033d }
            android.content.Intent r0 = r13.getIntent()     // Catch:{ all -> 0x033d }
            java.lang.String r2 = "adInfoOverride"
            java.io.Serializable r0 = r0.getSerializableExtra(r2)     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.adinformation.AdInformationOverrides r0 = (com.startapp.sdk.adsbase.adinformation.AdInformationOverrides) r0     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject r2 = new com.startapp.sdk.adsbase.adinformation.AdInformationObject     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject$Size r3 = com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size.LARGE     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r4 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OFFER_WALL     // Catch:{ all -> 0x033d }
            r2.<init>(r13, r3, r4, r0)     // Catch:{ all -> 0x033d }
            r13.h = r2     // Catch:{ all -> 0x033d }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject r0 = r13.h     // Catch:{ all -> 0x033d }
            r0.a((android.widget.RelativeLayout) r14)     // Catch:{ all -> 0x033d }
            r13.setContentView(r14, r1)     // Catch:{ all -> 0x033d }
            android.os.Handler r14 = new android.os.Handler     // Catch:{ all -> 0x033d }
            r14.<init>()     // Catch:{ all -> 0x033d }
            com.startapp.sdk.ads.list3d.List3DActivity$4 r0 = new com.startapp.sdk.ads.list3d.List3DActivity$4     // Catch:{ all -> 0x033d }
            r0.<init>()     // Catch:{ all -> 0x033d }
            r1 = 500(0x1f4, double:2.47E-321)
            r14.postDelayed(r0, r1)     // Catch:{ all -> 0x033d }
            return
        L_0x033d:
            r14 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r14)
            r0.a((android.content.Context) r13)
            r13.finish()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.list3d.List3DActivity.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        u.a((Activity) this, false);
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        g.a().a(this.f6131a).b();
        AdInformationObject adInformationObject = this.h;
        if (adInformationObject != null && adInformationObject.b()) {
            this.h.e();
        }
        overridePendingTransition(0, 0);
        String str = this.k;
        if (str != null && str.equals("back")) {
            finish();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        boolean z = false;
        if (!(this.i == null || this.j == null || System.currentTimeMillis() - this.i.longValue() <= this.j.longValue())) {
            z = true;
        }
        if (z) {
            finish();
            return;
        }
        k.a().a(true);
        this.m = SystemClock.uptimeMillis();
        g.a().a(this.f6131a).c();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Long l2 = this.i;
        if (l2 != null) {
            bundle.putSerializable("lastLoadTime", l2);
        }
        Long l3 = this.j;
        if (l3 != null) {
            bundle.putSerializable("adCacheTtl", l3);
        }
    }

    public final void a(int i2) {
        View childAt = this.d.getChildAt(i2 - this.d.a());
        if (childAt != null) {
            e eVar = (e) childAt.getTag();
            f a2 = g.a().a(this.f6131a);
            if (a2 != null && a2.e() != null && i2 < a2.e().size()) {
                d dVar = a2.e().get(i2);
                eVar.b().setImageBitmap(a2.a(i2, dVar.a(), dVar.i()));
                eVar.b().requestLayout();
                eVar.a(dVar.p());
            }
        }
    }
}
