package com.startapp.sdk.ads.list3d;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import com.startapp.common.b.c;
import com.startapp.sdk.adsbase.h;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    HashMap<String, h> f6143a = new HashMap<>();
    Hashtable<String, Bitmap> b = new Hashtable<>();
    Set<String> c = new HashSet();
    h d;
    int e = 0;
    ConcurrentLinkedQueue<C0070b> f = new ConcurrentLinkedQueue<>();

    class a extends AsyncTask<Void, Void, Bitmap> {

        /* renamed from: a  reason: collision with root package name */
        private int f6144a = -1;
        private String b;
        private String c;

        public a(int i, String str, String str2) {
            this.f6144a = i;
            this.b = str;
            this.c = str2;
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ Object doInBackground(Object[] objArr) {
            return c.b(this.c);
        }

        /* access modifiers changed from: protected */
        public final /* synthetic */ void onPostExecute(Object obj) {
            Bitmap bitmap = (Bitmap) obj;
            b bVar = b.this;
            bVar.e--;
            if (bitmap != null) {
                bVar.b.put(this.b, bitmap);
                h hVar = b.this.d;
                if (hVar != null) {
                    hVar.a(this.f6144a);
                }
                b bVar2 = b.this;
                if (!bVar2.f.isEmpty()) {
                    C0070b poll = bVar2.f.poll();
                    new a(poll.f6145a, poll.b, poll.c).execute(new Void[0]);
                }
            }
        }
    }

    /* renamed from: com.startapp.sdk.ads.list3d.b$b  reason: collision with other inner class name */
    class C0070b {

        /* renamed from: a  reason: collision with root package name */
        int f6145a;
        String b;
        String c;

        public C0070b(int i, String str, String str2) {
            this.f6145a = i;
            this.b = str;
            this.c = str2;
        }
    }

    public final Bitmap a(int i, String str, String str2) {
        Bitmap bitmap = this.b.get(str);
        if (bitmap != null) {
            return bitmap;
        }
        if (this.c.contains(str)) {
            return null;
        }
        this.c.add(str);
        int i2 = this.e;
        if (i2 >= 15) {
            this.f.add(new C0070b(i, str, str2));
            return null;
        }
        this.e = i2 + 1;
        new a(i, str, str2).execute(new Void[0]);
        return null;
    }
}
