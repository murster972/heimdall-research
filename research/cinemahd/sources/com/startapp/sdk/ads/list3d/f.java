package com.startapp.sdk.ads.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.model.AdDetails;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class f {

    /* renamed from: a  reason: collision with root package name */
    private b f6150a = new b();
    private List<d> b;
    private String c = "";

    public final void a() {
        this.b = new ArrayList();
        this.c = "";
    }

    public final void b() {
        b bVar = this.f6150a;
        for (String next : bVar.f6143a.keySet()) {
            if (bVar.f6143a.get(next) != null) {
                bVar.f6143a.get(next).b();
            }
        }
    }

    public final void c() {
        b bVar = this.f6150a;
        for (String next : bVar.f6143a.keySet()) {
            if (bVar.f6143a.get(next) != null) {
                bVar.f6143a.get(next).a();
            }
        }
    }

    public final void d() {
        b bVar = this.f6150a;
        for (String next : bVar.f6143a.keySet()) {
            if (bVar.f6143a.get(next) != null) {
                bVar.f6143a.get(next).a(false);
            }
        }
    }

    public final List<d> e() {
        return this.b;
    }

    public final void a(AdDetails adDetails) {
        d dVar = new d(adDetails);
        this.b.add(dVar);
        this.f6150a.a(this.b.size() - 1, dVar.a(), dVar.i());
    }

    public final void b(String str) {
        this.c = str;
    }

    public final Bitmap a(int i, String str, String str2) {
        return this.f6150a.a(i, str, str2);
    }

    public final void a(Context context, String str, TrackingParams trackingParams, long j) {
        b bVar = this.f6150a;
        String str2 = str + this.c;
        if (!bVar.f6143a.containsKey(str2)) {
            h hVar = new h(context, new String[]{str2}, trackingParams, j);
            bVar.f6143a.put(str2, hVar);
            hVar.a();
        }
    }

    public final void a(String str) {
        b bVar = this.f6150a;
        String str2 = str + this.c;
        HashMap<String, h> hashMap = bVar.f6143a;
        if (hashMap != null && hashMap.containsKey(str2) && bVar.f6143a.get(str2) != null) {
            bVar.f6143a.get(str2).a(true);
        }
    }

    public final void a(h hVar, boolean z) {
        b bVar = this.f6150a;
        bVar.d = hVar;
        if (z) {
            bVar.c.clear();
            bVar.e = 0;
            bVar.f.clear();
            HashMap<String, h> hashMap = bVar.f6143a;
            if (hashMap != null) {
                for (String str : hashMap.keySet()) {
                    bVar.f6143a.get(str).a(false);
                }
                bVar.f6143a.clear();
            }
        }
    }
}
