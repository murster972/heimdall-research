package com.startapp.sdk.ads.list3d;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.text.TextUtils;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataStyle;
import com.startapp.sdk.json.RatingBar;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private RelativeLayout f6148a;
    private ImageView b;
    private TextView c;
    private TextView d;
    private TextView e;
    private RatingBar f;
    private MetaDataStyle g = null;

    public e(Context context) {
        Context context2 = context;
        context2.setTheme(16973829);
        ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(-2, -2);
        this.f6148a = new RelativeLayout(context2);
        this.f6148a.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{AdsCommonMetaData.a().o(), AdsCommonMetaData.a().p()}));
        this.f6148a.setLayoutParams(layoutParams);
        int a2 = t.a(context2, 3);
        int a3 = t.a(context2, 4);
        int a4 = t.a(context2, 5);
        int a5 = t.a(context2, 6);
        int a6 = t.a(context2, 10);
        int a7 = t.a(context2, 84);
        this.f6148a.setPadding(a6, a2, a6, a2);
        this.f6148a.setTag(this);
        this.b = new ImageView(context2);
        this.b.setId(1);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(a7, a7);
        layoutParams2.addRule(15);
        this.b.setLayoutParams(layoutParams2);
        this.b.setPadding(0, 0, a5, 0);
        this.c = new TextView(context2);
        this.c.setId(2);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(b.a(17), 1);
        layoutParams3.addRule(6, 1);
        this.c.setLayoutParams(layoutParams3);
        this.c.setPadding(0, 0, 0, a4);
        this.c.setTextColor(AdsCommonMetaData.a().r().intValue());
        this.c.setTextSize((float) AdsCommonMetaData.a().q().intValue());
        this.c.setSingleLine(true);
        this.c.setEllipsize(TextUtils.TruncateAt.END);
        t.a(this.c, AdsCommonMetaData.a().s());
        this.d = new TextView(context2);
        this.d.setId(3);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams4.addRule(b.a(17), 1);
        layoutParams4.addRule(3, 2);
        layoutParams4.setMargins(0, 0, 0, a4);
        this.d.setLayoutParams(layoutParams4);
        this.d.setTextColor(AdsCommonMetaData.a().u().intValue());
        this.d.setTextSize((float) AdsCommonMetaData.a().t().intValue());
        this.d.setSingleLine(true);
        this.d.setEllipsize(TextUtils.TruncateAt.END);
        t.a(this.d, AdsCommonMetaData.a().v());
        this.f = new RatingBar(context2);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams5.addRule(b.a(17), 1);
        layoutParams5.addRule(8, 1);
        layoutParams5.setMargins(0, 0, 0, -a4);
        this.f.setLayoutParams(layoutParams5);
        this.f.setPadding(0, 0, 0, a3);
        this.f.setId(5);
        this.e = new TextView(context2);
        RelativeLayout.LayoutParams layoutParams6 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams6.addRule(b.a(21));
        layoutParams6.addRule(8, 1);
        this.e.setLayoutParams(layoutParams6);
        a(false);
        this.e.setTextColor(-1);
        this.e.setTextSize(12.0f);
        this.e.setTypeface((Typeface) null, 1);
        this.e.setPadding(a6, a5, a6, a5);
        this.e.setId(4);
        this.e.setShadowLayer(2.5f, -3.0f, 3.0f, -9013642);
        this.e.setBackgroundDrawable(new ShapeDrawable(new RoundRectShape(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f}, (RectF) null, (float[]) null)) {
            /* access modifiers changed from: protected */
            public final void onDraw(Shape shape, Canvas canvas, Paint paint) {
                paint.setColor(-11363070);
                paint.setMaskFilter(new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 5.0f, 3.0f));
                super.onDraw(shape, canvas, paint);
            }
        });
        this.f6148a.addView(this.b);
        this.f6148a.addView(this.c);
        this.f6148a.addView(this.d);
        this.f6148a.addView(this.f);
        this.f6148a.addView(this.e);
    }

    public final RelativeLayout a() {
        return this.f6148a;
    }

    public final ImageView b() {
        return this.b;
    }

    public final TextView c() {
        return this.c;
    }

    public final TextView d() {
        return this.d;
    }

    public final RatingBar e() {
        return this.f;
    }

    public final void a(boolean z) {
        if (z) {
            this.e.setText("Open");
        } else {
            this.e.setText("Download");
        }
    }

    public final void a(MetaDataStyle metaDataStyle) {
        if (this.g != metaDataStyle) {
            this.g = metaDataStyle;
            this.f6148a.setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{metaDataStyle.a().intValue(), metaDataStyle.b().intValue()}));
            this.c.setTextSize((float) metaDataStyle.c().intValue());
            this.c.setTextColor(metaDataStyle.d().intValue());
            t.a(this.c, metaDataStyle.e());
            this.d.setTextSize((float) metaDataStyle.f().intValue());
            this.d.setTextColor(metaDataStyle.g().intValue());
            t.a(this.d, metaDataStyle.h());
        }
    }
}
