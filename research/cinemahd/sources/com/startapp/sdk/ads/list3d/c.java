package com.startapp.sdk.ads.list3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.List;
import java.util.concurrent.TimeUnit;

public final class c extends ArrayAdapter<d> {

    /* renamed from: a  reason: collision with root package name */
    private String f6146a;
    private String b;

    public c(Context context, List<d> list, String str, String str2) {
        super(context, 0, list);
        this.f6146a = str;
        this.b = str2;
    }

    public final View getView(int i, View view, ViewGroup viewGroup) {
        View view2;
        e eVar;
        long j;
        if (view == null) {
            eVar = new e(getContext());
            view2 = eVar.a();
        } else {
            view2 = view;
            eVar = (e) view.getTag();
        }
        d dVar = (d) getItem(i);
        eVar.a(AdsCommonMetaData.a().a(dVar.m()));
        eVar.c().setText(dVar.g());
        eVar.d().setText(dVar.h());
        Bitmap a2 = g.a().a(this.b).a(i, dVar.a(), dVar.i());
        if (a2 == null) {
            eVar.b().setImageResource(17301651);
            eVar.b().setTag("tag_error");
        } else {
            eVar.b().setImageBitmap(a2);
            eVar.b().setTag("tag_ok");
        }
        eVar.e().setRating(dVar.j());
        eVar.a(dVar.p());
        f a3 = g.a().a(this.b);
        Context context = getContext();
        String c = dVar.c();
        TrackingParams trackingParams = new TrackingParams(this.f6146a);
        if (dVar.q() != null) {
            j = TimeUnit.SECONDS.toMillis(dVar.q().longValue());
        } else {
            j = TimeUnit.SECONDS.toMillis(MetaData.G().H());
        }
        a3.a(context, c, trackingParams, j);
        return view2;
    }
}
