package com.startapp.sdk.ads.list3d;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Adapter;
import android.widget.AdapterView;
import com.facebook.imagepipeline.common.RotationOptions;
import com.facebook.imageutils.JfifUtil;
import com.startapp.common.b;
import java.util.ArrayList;
import java.util.LinkedList;

public class List3DView extends AdapterView<Adapter> {
    private int A = Integer.MIN_VALUE;
    private boolean B = false;
    private boolean C = false;
    private boolean D = false;

    /* renamed from: a  reason: collision with root package name */
    protected int f6137a = 0;
    protected int b;
    protected int c;
    protected int d;
    protected int e;
    protected int f;
    protected int g;
    protected int h;
    protected int i;
    protected a j;
    protected float k = 0.0f;
    protected boolean l = false;
    protected boolean m = false;
    protected String n;
    protected String o;
    public BroadcastReceiver p = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            double height = ((double) List3DView.this.getHeight()) / ((double) intent.getIntExtra("getHeight", List3DView.this.getHeight()));
            List3DView list3DView = List3DView.this;
            list3DView.f6137a = intent.getIntExtra("mTouchState", list3DView.f6137a);
            List3DView list3DView2 = List3DView.this;
            list3DView2.b = intent.getIntExtra("mTouchStartX", list3DView2.b);
            List3DView list3DView3 = List3DView.this;
            list3DView3.c = intent.getIntExtra("mTouchStartY", list3DView3.c);
            List3DView list3DView4 = List3DView.this;
            list3DView4.g = intent.getIntExtra("mListRotation", list3DView4.g);
            List3DView list3DView5 = List3DView.this;
            list3DView5.h = (int) (((double) intent.getIntExtra("mFirstItemPosition", list3DView5.h)) * height);
            List3DView list3DView6 = List3DView.this;
            list3DView6.h--;
            list3DView6.i = (int) (((double) intent.getIntExtra("mLastItemPosition", list3DView6.i)) * height);
            List3DView list3DView7 = List3DView.this;
            list3DView7.i--;
            list3DView7.e = (int) (((double) intent.getIntExtra("mListTop", list3DView7.e)) * height);
            List3DView list3DView8 = List3DView.this;
            list3DView8.d = (int) (((double) intent.getIntExtra("mListTopStart", list3DView8.d)) * height);
            List3DView list3DView9 = List3DView.this;
            list3DView9.f = (int) (((double) intent.getIntExtra("mListTopOffset", list3DView9.f)) * height);
            List3DView.this.j = (a) intent.getParcelableExtra("mDynamics");
            List3DView list3DView10 = List3DView.this;
            list3DView10.k = intent.getFloatExtra("mLastVelocity", list3DView10.k);
            List3DView.this.j.a(height);
            ArrayList parcelableArrayListExtra = intent.getParcelableArrayListExtra("list");
            Context context2 = List3DView.this.getContext();
            List3DView list3DView11 = List3DView.this;
            List3DView.this.setAdapter(new c(context2, parcelableArrayListExtra, list3DView11.n, list3DView11.o));
            List3DView list3DView12 = List3DView.this;
            list3DView12.l = true;
            list3DView12.m = true;
            list3DView12.a(list3DView12.k, true);
            b.a(context).a((BroadcastReceiver) this);
        }
    };
    private String q = "List3DView";
    private Adapter r;
    private VelocityTracker s;
    private Runnable t;
    private final LinkedList<View> u = new LinkedList<>();
    private Runnable v;
    private Rect w;
    private Camera x;
    private Matrix y;
    private Paint z;

    public List3DView(Context context, String str, String str2) {
        super(context, (AttributeSet) null);
        this.n = str;
        this.o = str2;
    }

    private void a(Canvas canvas, Bitmap bitmap, int i2, int i3, int i4, int i5, float f2, float f3) {
        if (this.x == null) {
            this.x = new Camera();
        }
        this.x.save();
        this.x.translate(0.0f, 0.0f, (float) i5);
        this.x.rotateX(f3);
        float f4 = (float) (-i5);
        this.x.translate(0.0f, 0.0f, f4);
        if (this.y == null) {
            this.y = new Matrix();
        }
        this.x.getMatrix(this.y);
        this.x.restore();
        this.y.preTranslate((float) (-i4), f4);
        this.y.postScale(f2, f2);
        this.y.postTranslate((float) (i3 + i4), (float) (i2 + i5));
        if (this.z == null) {
            this.z = new Paint();
            this.z.setAntiAlias(true);
            this.z.setFilterBitmap(true);
        }
        this.z.setColorFilter(a(f3));
        canvas.drawBitmap(bitmap, this.y, this.z);
    }

    private void b() {
        int i2;
        int i3 = this.g;
        int i4 = i3 % 90;
        if (i4 < 45) {
            i2 = ((-(i3 - i4)) * getHeight()) / RotationOptions.ROTATE_270;
        } else {
            i2 = ((-((i3 + 90) - i4)) * getHeight()) / RotationOptions.ROTATE_270;
        }
        if (this.A == Integer.MIN_VALUE && this.i == this.r.getCount() - 1 && c(getChildAt(getChildCount() - 1)) < getHeight()) {
            this.A = i2;
        }
        if (i2 > 0) {
            i2 = 0;
        } else {
            int i5 = this.A;
            if (i2 < i5) {
                i2 = i5;
            }
        }
        float f2 = (float) i2;
        this.j.a(f2);
        this.j.b(f2);
    }

    private View c() {
        if (this.u.size() != 0) {
            return this.u.removeFirst();
        }
        return null;
    }

    private static int d(View view) {
        return view.getMeasuredHeight() + (b(view) * 2);
    }

    public boolean dispatchKeyShortcutEvent(KeyEvent keyEvent) {
        return super.dispatchKeyShortcutEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public boolean drawChild(Canvas canvas, View view, long j2) {
        Bitmap drawingCache = view.getDrawingCache();
        if (drawingCache == null) {
            return super.drawChild(canvas, view, j2);
        }
        int top = view.getTop();
        int left = view.getLeft();
        int width = view.getWidth() / 2;
        int height = view.getHeight() / 2;
        float height2 = (float) (getHeight() / 2);
        float f2 = (((float) (top + height)) - height2) / height2;
        float cos = (float) (1.0d - ((1.0d - Math.cos((double) f2)) * 0.15000000596046448d));
        float f3 = (((float) this.g) - (f2 * 20.0f)) % 90.0f;
        if (f3 < 0.0f) {
            f3 += 90.0f;
        }
        float f4 = f3;
        if (f4 < 45.0f) {
            Canvas canvas2 = canvas;
            Bitmap bitmap = drawingCache;
            int i2 = top;
            int i3 = left;
            int i4 = width;
            int i5 = height;
            float f5 = cos;
            a(canvas2, bitmap, i2, i3, i4, i5, f5, f4 - 90.0f);
            a(canvas2, bitmap, i2, i3, i4, i5, f5, f4);
            return false;
        }
        Canvas canvas3 = canvas;
        Bitmap bitmap2 = drawingCache;
        int i6 = top;
        int i7 = left;
        int i8 = width;
        int i9 = height;
        float f6 = cos;
        a(canvas3, bitmap2, i6, i7, i8, i9, f6, f4);
        a(canvas3, bitmap2, i6, i7, i8, i9, f6, f4 - 90.0f);
        return false;
    }

    public Adapter getAdapter() {
        return this.r;
    }

    public View getSelectedView() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this.t);
    }

    /* access modifiers changed from: protected */
    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        super.onLayout(z2, i2, i3, i4, i5);
        if (this.l && this.r != null) {
            if (getChildCount() == 0) {
                if (this.C) {
                    this.e = getHeight() / 3;
                }
                if (!this.m) {
                    this.i = -1;
                } else {
                    int i7 = this.h;
                    this.i = i7;
                    this.h = i7 + 1;
                }
                b(this.e, 0);
            } else {
                int a2 = (this.e + this.f) - a(getChildAt(0));
                int childCount = getChildCount();
                if (this.i != this.r.getCount() - 1 && childCount > 1) {
                    View childAt = getChildAt(0);
                    while (childAt != null && c(childAt) + a2 < 0) {
                        removeViewInLayout(childAt);
                        childCount--;
                        this.u.addLast(childAt);
                        this.h++;
                        this.f += d(childAt);
                        childAt = childCount > 1 ? getChildAt(0) : null;
                    }
                }
                if (this.h != 0 && childCount > 1) {
                    View childAt2 = getChildAt(childCount - 1);
                    while (childAt2 != null && a(childAt2) + a2 > getHeight()) {
                        removeViewInLayout(childAt2);
                        childCount--;
                        this.u.addLast(childAt2);
                        this.i--;
                        childAt2 = childCount > 1 ? getChildAt(childCount - 1) : null;
                    }
                }
                b(c(getChildAt(getChildCount() - 1)), a2);
                int a3 = a(getChildAt(0));
                while (a3 + a2 > 0 && (i6 = this.h) > 0) {
                    this.h = i6 - 1;
                    View view = this.r.getView(this.h, c(), this);
                    a(view, 1);
                    int d2 = d(view);
                    a3 -= d2;
                    this.f -= d2;
                }
            }
            int i8 = this.e + this.f;
            float width = ((float) getWidth()) * 0.0f;
            float height = 1.0f / (((float) getHeight()) * 0.9f);
            for (int i9 = 0; i9 < getChildCount(); i9++) {
                View childAt3 = getChildAt(i9);
                int measuredWidth = childAt3.getMeasuredWidth();
                int measuredHeight = childAt3.getMeasuredHeight();
                int sin = ((int) (((double) width) * Math.sin(((double) height) * 6.283185307179586d * ((double) i8)))) + ((getWidth() - measuredWidth) / 2);
                int b2 = b(childAt3);
                int i10 = i8 + b2;
                childAt3.layout(sin, i10, measuredWidth + sin, i10 + measuredHeight);
                i8 += measuredHeight + (b2 * 2);
            }
            if (this.C && !this.D) {
                this.D = true;
                dispatchTouchEvent(MotionEvent.obtain(System.currentTimeMillis(), System.currentTimeMillis(), 0, 0.0f, 0.0f, 0));
                postDelayed(new Runnable() {
                    public final void run() {
                        List3DView.this.dispatchTouchEvent(MotionEvent.obtain(System.currentTimeMillis(), System.currentTimeMillis(), 2, 0.0f, -20.0f, 0));
                        List3DView.this.dispatchTouchEvent(MotionEvent.obtain(System.currentTimeMillis(), System.currentTimeMillis(), 1, 0.0f, -20.0f, 0));
                    }
                }, 5);
            }
            invalidate();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:18:0x003a, code lost:
        if (r1 <= (r0 + 10)) goto L_0x0043;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r8) {
        /*
            r7 = this;
            int r0 = r7.getChildCount()
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            int r0 = r8.getAction()
            r2 = 1
            if (r0 == 0) goto L_0x0099
            r3 = 0
            r4 = 2
            if (r0 == r2) goto L_0x0059
            if (r0 == r4) goto L_0x001a
            r7.a((float) r3, (boolean) r1)
            goto L_0x00e4
        L_0x001a:
            int r0 = r7.f6137a
            if (r0 != r2) goto L_0x0043
            float r0 = r8.getX()
            int r0 = (int) r0
            float r1 = r8.getY()
            int r1 = (int) r1
            int r3 = r7.b
            int r5 = r3 + -10
            if (r0 < r5) goto L_0x003c
            int r3 = r3 + 10
            if (r0 > r3) goto L_0x003c
            int r0 = r7.c
            int r3 = r0 + -10
            if (r1 < r3) goto L_0x003c
            int r0 = r0 + 10
            if (r1 <= r0) goto L_0x0043
        L_0x003c:
            java.lang.Runnable r0 = r7.v
            r7.removeCallbacks(r0)
            r7.f6137a = r4
        L_0x0043:
            int r0 = r7.f6137a
            if (r0 != r4) goto L_0x00e4
            android.view.VelocityTracker r0 = r7.s
            r0.addMovement(r8)
            float r8 = r8.getY()
            int r8 = (int) r8
            int r0 = r7.c
            int r8 = r8 - r0
            r7.a((int) r8)
            goto L_0x00e4
        L_0x0059:
            int r0 = r7.f6137a
            if (r0 != r2) goto L_0x007f
            float r0 = r8.getX()
            int r0 = (int) r0
            float r8 = r8.getY()
            int r8 = (int) r8
            int r8 = r7.a((int) r0, (int) r8)
            r0 = -1
            if (r8 == r0) goto L_0x0095
            android.view.View r0 = r7.getChildAt(r8)
            int r4 = r7.h
            int r4 = r4 + r8
            android.widget.Adapter r8 = r7.r
            long r5 = r8.getItemId(r4)
            r7.performItemClick(r0, r4, r5)
            goto L_0x0095
        L_0x007f:
            if (r0 != r4) goto L_0x0095
            android.view.VelocityTracker r0 = r7.s
            r0.addMovement(r8)
            android.view.VelocityTracker r8 = r7.s
            r0 = 1000(0x3e8, float:1.401E-42)
            r8.computeCurrentVelocity(r0)
            android.view.VelocityTracker r8 = r7.s
            float r3 = r8.getYVelocity()
            r7.k = r3
        L_0x0095:
            r7.a((float) r3, (boolean) r1)
            goto L_0x00e4
        L_0x0099:
            boolean r0 = com.startapp.common.b.b.a()
            if (r0 == 0) goto L_0x00a2
            com.startapp.common.b.b.a((android.view.View) r7)
        L_0x00a2:
            java.lang.Runnable r0 = r7.t
            r7.removeCallbacks(r0)
            float r0 = r8.getX()
            int r0 = (int) r0
            r7.b = r0
            float r0 = r8.getY()
            int r0 = (int) r0
            r7.c = r0
            android.view.View r0 = r7.getChildAt(r1)
            int r0 = a((android.view.View) r0)
            int r1 = r7.f
            int r0 = r0 - r1
            r7.d = r0
            java.lang.Runnable r0 = r7.v
            if (r0 != 0) goto L_0x00cd
            com.startapp.sdk.ads.list3d.List3DView$4 r0 = new com.startapp.sdk.ads.list3d.List3DView$4
            r0.<init>()
            r7.v = r0
        L_0x00cd:
            java.lang.Runnable r0 = r7.v
            int r1 = android.view.ViewConfiguration.getLongPressTimeout()
            long r3 = (long) r1
            r7.postDelayed(r0, r3)
            android.view.VelocityTracker r0 = android.view.VelocityTracker.obtain()
            r7.s = r0
            android.view.VelocityTracker r0 = r7.s
            r0.addMovement(r8)
            r7.f6137a = r2
        L_0x00e4:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.list3d.List3DView.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setAdapter(Adapter adapter) {
        if (com.startapp.common.b.b.a() && this.B) {
            com.startapp.common.b.b.a((View) this, 0.0f);
        }
        this.r = adapter;
        removeAllViewsInLayout();
        requestLayout();
    }

    public void setDynamics(a aVar) {
        a aVar2 = this.j;
        if (aVar2 != null) {
            aVar.a(aVar2.a(), this.j.b(), AnimationUtils.currentAnimationTimeMillis());
        }
        this.j = aVar;
    }

    public void setFade(boolean z2) {
        this.B = z2;
    }

    public void setHint(boolean z2) {
        this.C = z2;
    }

    public void setSelection(int i2) {
        throw new UnsupportedOperationException("Not supported");
    }

    public void setStarted() {
        this.l = true;
    }

    public void setTag(String str) {
        this.q = str;
    }

    private static int c(View view) {
        return view.getBottom() + b(view);
    }

    /* access modifiers changed from: protected */
    public final void b(int i2) {
        View childAt = getChildAt(i2);
        int i3 = this.h + i2;
        long itemId = this.r.getItemId(i3);
        AdapterView.OnItemLongClickListener onItemLongClickListener = getOnItemLongClickListener();
        if (onItemLongClickListener != null) {
            onItemLongClickListener.onItemLongClick(this, childAt, i3, itemId);
        }
    }

    private void b(int i2, int i3) {
        while (i2 + i3 < getHeight() && this.i < this.r.getCount() - 1) {
            this.i++;
            View view = this.r.getView(this.i, c(), this);
            a(view, 0);
            i2 += d(view);
        }
    }

    private static LightingColorFilter a(float f2) {
        double cos = Math.cos((((double) f2) * 3.141592653589793d) / 180.0d);
        int i2 = ((int) (cos * 200.0d)) + 55;
        int pow = (int) (Math.pow(cos, 200.0d) * 70.0d);
        if (i2 > 255) {
            i2 = JfifUtil.MARKER_FIRST_BYTE;
        }
        if (pow > 255) {
            pow = JfifUtil.MARKER_FIRST_BYTE;
        }
        return new LightingColorFilter(Color.rgb(i2, i2, i2), Color.rgb(pow, pow, pow));
    }

    private static int b(View view) {
        return (int) ((((float) view.getMeasuredHeight()) * 0.35000002f) / 2.0f);
    }

    /* access modifiers changed from: protected */
    public final void a(float f2, boolean z2) {
        if (this.s != null || z2) {
            VelocityTracker velocityTracker = this.s;
            if (velocityTracker != null) {
                velocityTracker.recycle();
            }
            this.s = null;
            removeCallbacks(this.v);
            if (this.t == null) {
                this.t = new Runnable() {
                    public final void run() {
                        List3DView list3DView = List3DView.this;
                        if (list3DView.j != null) {
                            View childAt = list3DView.getChildAt(0);
                            if (childAt != null) {
                                List3DView list3DView2 = List3DView.this;
                                int a2 = List3DView.a(childAt);
                                List3DView list3DView3 = List3DView.this;
                                list3DView2.d = a2 - list3DView3.f;
                                list3DView3.j.a(AnimationUtils.currentAnimationTimeMillis());
                                List3DView list3DView4 = List3DView.this;
                                list3DView4.a(((int) list3DView4.j.a()) - List3DView.this.d);
                            }
                            if (!List3DView.this.j.c()) {
                                List3DView.this.postDelayed(this, 16);
                            }
                        }
                    }
                };
            }
            a aVar = this.j;
            if (aVar != null) {
                if (!z2) {
                    aVar.a((float) this.e, f2, AnimationUtils.currentAnimationTimeMillis());
                }
                post(this.t);
            }
            this.f6137a = 0;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.e = this.d + i2;
        this.g = (-(this.e * RotationOptions.ROTATE_270)) / getHeight();
        b();
        requestLayout();
    }

    /* access modifiers changed from: protected */
    public final int a(int i2, int i3) {
        if (this.w == null) {
            this.w = new Rect();
        }
        for (int i4 = 0; i4 < getChildCount(); i4++) {
            getChildAt(i4).getHitRect(this.w);
            if (this.w.contains(i2, i3)) {
                return i4;
            }
        }
        return -1;
    }

    private void a(View view, int i2) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (layoutParams == null) {
            layoutParams = new ViewGroup.LayoutParams(-2, -2);
        }
        int i3 = i2 == 1 ? 0 : -1;
        view.setDrawingCacheEnabled(true);
        addViewInLayout(view, i3, layoutParams, true);
        view.measure(((int) (((float) getWidth()) * 0.85f)) | 1073741824, 0);
    }

    protected static int a(View view) {
        return view.getTop() - b(view);
    }

    public final int a() {
        return this.h;
    }
}
