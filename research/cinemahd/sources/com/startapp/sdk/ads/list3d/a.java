package com.startapp.sdk.ads.list3d;

import android.os.Parcel;
import android.os.Parcelable;
import android.view.animation.AnimationUtils;

public abstract class a implements Parcelable {

    /* renamed from: a  reason: collision with root package name */
    protected float f6142a;
    protected float b;
    private float c = Float.MAX_VALUE;
    private float d = -3.4028235E38f;
    private long e = 0;

    public a() {
    }

    public final void a(float f, float f2, long j) {
        this.b = f2;
        this.f6142a = f;
        this.e = j;
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i);

    public final float b() {
        return this.b;
    }

    public final boolean c() {
        boolean z = Math.abs(this.b) < 0.5f;
        float f = this.f6142a;
        return z && (((f - 0.4f) > this.c ? 1 : ((f - 0.4f) == this.c ? 0 : -1)) < 0 && ((f + 0.4f) > this.d ? 1 : ((f + 0.4f) == this.d ? 0 : -1)) > 0);
    }

    /* access modifiers changed from: protected */
    public final float d() {
        float f = this.f6142a;
        float f2 = this.c;
        if (f <= f2) {
            f2 = this.d;
            if (f >= f2) {
                return 0.0f;
            }
        }
        return f2 - f;
    }

    public int describeContents() {
        return 0;
    }

    public String toString() {
        return "Position: [" + this.f6142a + "], Velocity:[" + this.b + "], MaxPos: [" + this.c + "], mMinPos: [" + this.d + "] LastTime:[" + this.e + "]";
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeFloat(this.f6142a);
        parcel.writeFloat(this.b);
        parcel.writeFloat(this.c);
        parcel.writeFloat(this.d);
    }

    public final void b(float f) {
        this.d = f;
    }

    public final float a() {
        return this.f6142a;
    }

    public a(Parcel parcel) {
        this.f6142a = parcel.readFloat();
        this.b = parcel.readFloat();
        this.c = parcel.readFloat();
        this.d = parcel.readFloat();
        this.e = AnimationUtils.currentAnimationTimeMillis();
    }

    public final void a(float f) {
        this.c = f;
    }

    public final void a(long j) {
        long j2 = this.e;
        if (j2 != 0) {
            int i = (int) (j - j2);
            int i2 = 50;
            if (i <= 50) {
                i2 = i;
            }
            a(i2);
        }
        this.e = j;
    }

    public void a(double d2) {
        this.f6142a = (float) (((double) this.f6142a) * d2);
    }
}
