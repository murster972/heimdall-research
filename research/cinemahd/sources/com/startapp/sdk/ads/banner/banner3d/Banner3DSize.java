package com.startapp.sdk.ads.banner.banner3d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.view.View;
import android.view.ViewParent;
import android.view.WindowManager;
import com.facebook.common.util.ByteConstants;
import com.facebook.react.uimanager.ViewProps;
import com.iab.omid.library.startapp.adsession.b;
import com.startapp.sdk.ads.banner.Banner;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.ads.banner.c;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.t;
import org.json.JSONObject;

public final class Banner3DSize {

    /* renamed from: a  reason: collision with root package name */
    private final b f6120a;

    public enum Size {
        XXSMALL(new c(280, 50)),
        XSMALL(new c(300, 50)),
        SMALL(new c(320, 50)),
        MEDIUM(new c(468, 60)),
        LARGE(new c(728, 90)),
        XLARGE(new c(ByteConstants.KB, 90));
        
        private c size;

        private Size(c cVar) {
            this.size = cVar;
        }

        public final c getSize() {
            return this.size;
        }
    }

    private Banner3DSize(b bVar) {
        this.f6120a = bVar;
    }

    public static boolean a(Context context, ViewParent viewParent, BannerOptions bannerOptions, Banner3D banner3D, c cVar) {
        c a2 = a(context, viewParent, bannerOptions, banner3D);
        cVar.a(a2.a(), a2.b());
        boolean z = false;
        for (Size size : Size.values()) {
            if (size.getSize().a() <= a2.a() && size.getSize().b() <= a2.b()) {
                bannerOptions.a(size.getSize().a(), size.getSize().b());
                z = true;
            }
        }
        if (!z) {
            bannerOptions.a(0, 0);
        }
        return z;
    }

    private static void b(Context context, Point point, View view) {
        point.x = t.b(context, (view.getMeasuredWidth() - view.getPaddingLeft()) - view.getPaddingRight());
    }

    private static void c(Context context, Point point, View view) {
        point.x = t.b(context, view.getMeasuredWidth());
        point.y = t.b(context, view.getMeasuredHeight());
    }

    public final void d() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("complete");
    }

    public final void e() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("pause");
    }

    public final void f() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("bufferStart");
    }

    public final void g() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("bufferFinish");
    }

    public final void h() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("skipped");
    }

    public final void b() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("midpoint");
    }

    private static void b(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Video volume");
        }
    }

    public final void c() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("thirdQuartile");
    }

    private static c a(Context context, ViewParent viewParent, BannerOptions bannerOptions, Banner3D banner3D) {
        Point point = new Point();
        point.x = bannerOptions.d();
        point.y = bannerOptions.e();
        if (banner3D.getLayoutParams() != null && banner3D.getLayoutParams().width > 0) {
            point.x = t.b(context, banner3D.getLayoutParams().width + 1);
        }
        if (banner3D.getLayoutParams() != null && banner3D.getLayoutParams().height > 0) {
            point.y = t.b(context, banner3D.getLayoutParams().height + 1);
        }
        if (banner3D.getLayoutParams() == null || banner3D.getLayoutParams().width <= 0 || banner3D.getLayoutParams().height <= 0) {
            if (context instanceof Activity) {
                View decorView = ((Activity) context).getWindow().getDecorView();
                try {
                    View view = (View) viewParent;
                    if (view instanceof Banner) {
                        view = (View) view.getParent();
                    }
                    boolean z = false;
                    boolean z2 = false;
                    while (view != null && (view.getMeasuredWidth() <= 0 || view.getMeasuredHeight() <= 0)) {
                        if (view.getMeasuredWidth() > 0 && !z) {
                            b(context, point, view);
                            z = true;
                        }
                        if (view.getMeasuredHeight() > 0 && !z2) {
                            a(context, point, view);
                            z2 = true;
                        }
                        view = (View) view.getParent();
                    }
                    if (view == null) {
                        c(context, point, decorView);
                    } else {
                        if (!z) {
                            b(context, point, view);
                        }
                        if (!z2) {
                            a(context, point, view);
                        }
                    }
                } catch (Exception e) {
                    c(context, point, decorView);
                    new e((Throwable) e).a(context);
                }
            } else {
                try {
                    WindowManager windowManager = (WindowManager) context.getSystemService("window");
                    if (windowManager != null) {
                        if (Build.VERSION.SDK_INT >= 13) {
                            windowManager.getDefaultDisplay().getSize(point);
                        } else {
                            point.x = windowManager.getDefaultDisplay().getWidth();
                            point.y = windowManager.getDefaultDisplay().getHeight();
                        }
                        point.x = t.b(context, point.x);
                        point.y = t.b(context, point.y);
                    }
                } catch (Throwable th) {
                    new e(th).a(context);
                }
            }
        }
        return new c(point.x, point.y);
    }

    private static void a(Context context, Point point, View view) {
        point.y = t.b(context, (view.getMeasuredHeight() - view.getPaddingBottom()) - view.getPaddingTop());
    }

    public static Banner3DSize a(b bVar) {
        com.iab.omid.library.startapp.b.a((Object) bVar, "AdSession is null");
        if (!bVar.l()) {
            throw new IllegalStateException("Cannot create VideoEvents for JavaScript AdSession");
        } else if (!bVar.i()) {
            com.iab.omid.library.startapp.b.a(bVar);
            if (bVar.e().e() == null) {
                Banner3DSize banner3DSize = new Banner3DSize(bVar);
                bVar.e().a(banner3DSize);
                return banner3DSize;
            }
            throw new IllegalStateException("VideoEvents already exists for AdSession");
        } else {
            throw new IllegalStateException("AdSession is started");
        }
    }

    public final void a(float f, float f2) {
        if (f > 0.0f) {
            b(f2);
            com.iab.omid.library.startapp.b.b(this.f6120a);
            JSONObject jSONObject = new JSONObject();
            com.iab.omid.library.startapp.d.b.a(jSONObject, "duration", Float.valueOf(f));
            com.iab.omid.library.startapp.d.b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f2));
            com.iab.omid.library.startapp.d.b.a(jSONObject, "deviceVolume", Float.valueOf(com.iab.omid.library.startapp.b.e.d().c()));
            this.f6120a.e().a(ViewProps.START, jSONObject);
            return;
        }
        throw new IllegalArgumentException("Invalid Video duration");
    }

    public final void a() {
        com.iab.omid.library.startapp.b.b(this.f6120a);
        this.f6120a.e().a("firstQuartile");
    }

    public final void a(float f) {
        b(f);
        com.iab.omid.library.startapp.b.b(this.f6120a);
        JSONObject jSONObject = new JSONObject();
        com.iab.omid.library.startapp.d.b.a(jSONObject, "videoPlayerVolume", Float.valueOf(f));
        com.iab.omid.library.startapp.d.b.a(jSONObject, "deviceVolume", Float.valueOf(com.iab.omid.library.startapp.b.e.d().c()));
        this.f6120a.e().a("volumeChange", jSONObject);
    }
}
