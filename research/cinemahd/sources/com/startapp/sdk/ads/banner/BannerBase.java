package com.startapp.sdk.ads.banner;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.adrules.AdaptMetaData;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.k.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledFuture;

public abstract class BannerBase extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    protected AdRulesResult f6113a;
    protected Point b;
    protected boolean c;
    protected int d;
    protected int e;
    protected String f;
    private boolean g;
    private AdPreferences h;
    private int i;
    private boolean j;
    private b k;
    private boolean l;
    private boolean m;
    private String n;
    private ScheduledFuture<?> o;
    private Timer p;
    private a q;
    private final Handler r;
    private final Object s;

    class a extends TimerTask {
        a() {
        }

        public final void run() {
            BannerBase.this.post(new Runnable() {
                public final void run() {
                    AdRulesResult adRulesResult;
                    if (BannerBase.this.isShown() || ((adRulesResult = BannerBase.this.f6113a) != null && !adRulesResult.a())) {
                        BannerBase.this.m();
                    }
                }
            });
        }
    }

    public BannerBase(Context context) {
        super(context);
        this.g = false;
        this.i = 0;
        this.j = true;
        this.c = false;
        this.d = new Random().nextInt(100000) + 159868227;
        this.e = this.d + 1;
        this.f = null;
        this.l = false;
        this.m = false;
        this.o = null;
        this.p = new Timer();
        this.q = new a();
        this.r = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            public final boolean handleMessage(Message message) {
                if (message.what == 1) {
                    BannerBase.this.l();
                }
                return true;
            }
        });
        this.s = new Object();
    }

    protected static int n() {
        return BannerMetaData.a().b().q();
    }

    private void s() {
        b bVar = this.k;
        if (bVar != null) {
            bVar.b();
            this.k = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void a() {
        if (!isInEditMode()) {
            b();
            return;
        }
        setMinimumWidth(t.a(getContext(), 300));
        setMinimumHeight(t.a(getContext(), c()));
        setBackgroundColor(Color.rgb(169, 169, 169));
        TextView textView = new TextView(getContext());
        textView.setText(d());
        textView.setTextColor(-16777216);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams.addRule(13);
        addView(textView, layoutParams);
    }

    /* access modifiers changed from: protected */
    public abstract void a(int i2);

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public abstract int c();

    /* access modifiers changed from: protected */
    public abstract String d();

    /* access modifiers changed from: protected */
    public abstract void e();

    /* access modifiers changed from: protected */
    public abstract int f();

    /* access modifiers changed from: protected */
    public abstract int g();

    public String getErrorMessage() {
        return this.n;
    }

    /* access modifiers changed from: protected */
    public abstract int h();

    /* access modifiers changed from: protected */
    public abstract void hideBanner();

    /* access modifiers changed from: protected */
    public int i() {
        return f();
    }

    public boolean isClicked() {
        return this.l;
    }

    public boolean isFirstLoad() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final String j() {
        return this.f;
    }

    /* access modifiers changed from: protected */
    public final void k() {
        synchronized (this.s) {
            if (!this.r.hasMessages(1)) {
                this.r.sendEmptyMessage(1);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void l() {
        p();
        m();
    }

    public void loadAd(int i2, int i3) {
        if (getParent() == null) {
            this.b = new Point(i2, i3);
            k();
        }
    }

    /* access modifiers changed from: protected */
    public final void m() {
        s();
        if (this.f6113a == null || AdaptMetaData.a().b().a()) {
            this.f6113a = AdaptMetaData.a().b().a(AdPreferences.Placement.INAPP_BANNER, this.f);
            if (this.f6113a.a()) {
                e();
            } else {
                hideBanner();
            }
        } else if (this.f6113a.a()) {
            e();
        }
    }

    /* access modifiers changed from: protected */
    public View o() {
        return this;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.g = true;
        p();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.g = false;
        q();
        s();
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof Bundle)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        Bundle bundle = (Bundle) parcelable;
        a(bundle.getInt("bannerId"));
        this.f6113a = (AdRulesResult) bundle.getSerializable("adRulesResult");
        this.h = (AdPreferences) bundle.getSerializable("adPreferences");
        this.i = bundle.getInt("offset");
        this.j = bundle.getBoolean("firstLoad");
        this.m = bundle.getBoolean("shouldReloadBanner");
        super.onRestoreInstanceState(bundle.getParcelable("upperState"));
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        if (isClicked()) {
            setClicked(false);
            this.m = true;
        }
        Parcelable onSaveInstanceState = super.onSaveInstanceState();
        Bundle bundle = new Bundle();
        bundle.putInt("bannerId", h());
        bundle.putParcelable("upperState", onSaveInstanceState);
        bundle.putSerializable("adRulesResult", this.f6113a);
        bundle.putSerializable("adPreferences", this.h);
        bundle.putInt("offset", this.i);
        bundle.putBoolean("firstLoad", this.j);
        bundle.putBoolean("shouldReloadBanner", this.m);
        return bundle;
    }

    public void onWindowFocusChanged(boolean z) {
        super.onWindowFocusChanged(z);
        if (z) {
            if (this.m) {
                this.m = false;
                m();
            }
            this.g = true;
            p();
            return;
        }
        this.g = false;
        q();
    }

    /* access modifiers changed from: protected */
    public void p() {
        if (this.g && !isInEditMode()) {
            a aVar = this.q;
            if (aVar != null) {
                aVar.cancel();
            }
            Timer timer = this.p;
            if (timer != null) {
                timer.cancel();
            }
            this.q = new a();
            this.p = new Timer();
            this.p.schedule(this.q, (long) i());
            ScheduledFuture<?> scheduledFuture = this.o;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
            }
            this.o = ThreadManager.a((Runnable) new Runnable() {
                public final void run() {
                    BannerBase.this.k();
                }
            }, (long) (MetaData.G().F() * 1000));
        }
    }

    /* access modifiers changed from: protected */
    public final void q() {
        if (!isInEditMode()) {
            a aVar = this.q;
            if (aVar != null) {
                aVar.cancel();
            }
            Timer timer = this.p;
            if (timer != null) {
                timer.cancel();
            }
            ScheduledFuture<?> scheduledFuture = this.o;
            if (scheduledFuture != null) {
                scheduledFuture.cancel(false);
            }
            this.q = null;
            this.p = null;
            this.o = null;
        }
    }

    /* access modifiers changed from: protected */
    public final void r() {
        if (isFirstLoad() || AdaptMetaData.a().b().a()) {
            setFirstLoad(false);
            com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(AdPreferences.Placement.INAPP_BANNER, this.f));
        }
    }

    public abstract void setAdTag(String str);

    public void setClicked(boolean z) {
        this.l = z;
    }

    public void setErrorMessage(String str) {
        this.n = str;
    }

    public void setFirstLoad(boolean z) {
        this.j = z;
    }

    public void loadAd() {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        loadAd(t.b(getContext(), displayMetrics.widthPixels), t.b(getContext(), displayMetrics.heightPixels));
    }

    /* access modifiers changed from: protected */
    public final void a(h hVar) {
        if (this.k == null) {
            this.k = new b(o(), hVar, n());
            this.k.a();
        }
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences) {
        adPreferences.setHardwareAccelerated(com.startapp.common.b.b.a((View) this, this.g));
    }

    public BannerBase(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public BannerBase(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.g = false;
        this.i = 0;
        this.j = true;
        this.c = false;
        this.d = new Random().nextInt(100000) + 159868227;
        this.e = this.d + 1;
        this.f = null;
        this.l = false;
        this.m = false;
        this.o = null;
        this.p = new Timer();
        this.q = new a();
        this.r = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            public final boolean handleMessage(Message message) {
                if (message.what == 1) {
                    BannerBase.this.l();
                }
                return true;
            }
        });
        this.s = new Object();
        setAdTag(new b(context, attributeSet).a());
    }
}
