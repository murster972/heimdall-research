package com.startapp.sdk.ads.banner.bannerstandard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import com.iab.omid.library.startapp.adsession.b;
import com.startapp.sdk.ads.banner.BannerBase;
import com.startapp.sdk.ads.banner.BannerInterface;
import com.startapp.sdk.ads.banner.BannerListener;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.ads.banner.bannerstandard.CloseableLayout;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.k.c;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.mraid.bridge.MraidState;
import com.startapp.sdk.adsbase.mraid.bridge.a;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.Map;

public class BannerStandard extends BannerBase implements BannerInterface, AdEventListener {
    private c A;
    /* access modifiers changed from: private */
    public MraidBannerController B;
    /* access modifiers changed from: private */
    public MraidBannerController C;
    private ViewGroup D;
    protected BannerStandardAd g;
    protected boolean h;
    protected boolean i;
    protected BannerListener j;
    /* access modifiers changed from: private */
    public boolean k;
    private boolean l;
    private boolean m;
    private final Handler n;
    /* access modifiers changed from: private */
    public long o;
    private BannerOptions p;
    private AdPreferences q;
    /* access modifiers changed from: private */
    public final com.startapp.sdk.ads.banner.c r;
    private boolean s;
    private AdInformationObject t;
    public WebView twoPartWebView;
    private RelativeLayout u;
    private RelativeLayout v;
    private CloseableLayout w;
    public WebView webView;
    private h x;
    private b y;
    private c z;

    private class MraidBannerController extends a {
        private WebView activeWebView;
        /* access modifiers changed from: private */
        public MraidState mraidState = MraidState.LOADING;
        private boolean mraidVisibility = false;
        /* access modifiers changed from: private */
        public com.startapp.sdk.adsbase.mraid.a.a nativeFeatureManager;
        private com.startapp.sdk.adsbase.mraid.b.a orientationProperties;
        private com.startapp.sdk.adsbase.mraid.b.b resizeProperties;

        class BannerWebViewClient extends com.startapp.sdk.adsbase.mraid.bridge.c {
            BannerWebViewClient(com.startapp.sdk.adsbase.mraid.bridge.b bVar) {
                super(bVar);
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                if (MraidBannerController.this.mraidState == MraidState.LOADING) {
                    com.iab.omid.library.startapp.b.a("inline", webView);
                    com.iab.omid.library.startapp.b.a(BannerStandard.this.getContext(), webView, MraidBannerController.this.nativeFeatureManager);
                    MraidBannerController.this.updateDisplayMetrics(webView);
                    MraidState unused = MraidBannerController.this.mraidState = MraidState.DEFAULT;
                    com.iab.omid.library.startapp.b.a(MraidBannerController.this.mraidState, webView);
                    com.iab.omid.library.startapp.b.a(webView);
                }
                BannerStandard.this.a(webView);
            }
        }

        MraidBannerController(WebView webView, a.C0078a aVar) {
            super(aVar);
            this.activeWebView = webView;
            this.activeWebView.setWebViewClient(new BannerWebViewClient(this));
            this.nativeFeatureManager = new com.startapp.sdk.adsbase.mraid.a.a(BannerStandard.this.getContext());
            this.orientationProperties = new com.startapp.sdk.adsbase.mraid.b.a();
        }

        /* access modifiers changed from: private */
        public void fireViewableChangeEvent(boolean z) {
            if (this.mraidVisibility != z) {
                this.mraidVisibility = z;
                com.iab.omid.library.startapp.b.a(this.activeWebView, this.mraidVisibility);
            }
        }

        /* access modifiers changed from: private */
        public void updateDisplayMetrics(WebView webView) {
            Context context = BannerStandard.this.getContext();
            try {
                DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
                int i = displayMetrics.widthPixels;
                int i2 = displayMetrics.heightPixels;
                int[] iArr = new int[2];
                BannerStandard.this.getLocationOnScreen(iArr);
                int i3 = iArr[0];
                int i4 = iArr[1];
                com.iab.omid.library.startapp.b.a(context, i, i2, webView);
                com.iab.omid.library.startapp.b.b(context, i3, i4, BannerStandard.this.r.a(), BannerStandard.this.r.b(), webView);
                com.iab.omid.library.startapp.b.b(context, i, i2, webView);
                com.iab.omid.library.startapp.b.a(context, i3, i4, BannerStandard.this.r.a(), BannerStandard.this.r.b(), webView);
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }

        public void close() {
            BannerStandard.a(BannerStandard.this);
        }

        public void expand(String str) {
            BannerStandard.b(BannerStandard.this, str);
        }

        /* access modifiers changed from: package-private */
        public com.startapp.sdk.adsbase.mraid.b.b getResizeProperties() {
            return this.resizeProperties;
        }

        /* access modifiers changed from: package-private */
        public MraidState getState() {
            return this.mraidState;
        }

        public boolean isFeatureSupported(String str) {
            return this.nativeFeatureManager.a(str);
        }

        public void resize() {
            BannerStandard.h(BannerStandard.this);
        }

        public void setExpandProperties(Map<String, String> map) {
            String str = map.get("useCustomClose");
            if (str != null) {
                BannerStandard.a(BannerStandard.this, Boolean.parseBoolean(str));
            }
        }

        public void setOrientationProperties(Map<String, String> map) {
            boolean parseBoolean = Boolean.parseBoolean(map.get("allowOrientationChange"));
            String str = map.get("forceOrientation");
            com.startapp.sdk.adsbase.mraid.b.a aVar = this.orientationProperties;
            if (aVar.f6418a != parseBoolean || aVar.b != com.startapp.sdk.adsbase.mraid.b.a.a(str)) {
                com.startapp.sdk.adsbase.mraid.b.a aVar2 = this.orientationProperties;
                aVar2.f6418a = parseBoolean;
                aVar2.b = com.startapp.sdk.adsbase.mraid.b.a.a(str);
                applyOrientationProperties((Activity) BannerStandard.this.getContext(), this.orientationProperties);
            }
        }

        public void setResizeProperties(Map<String, String> map) {
            boolean z;
            try {
                int parseInt = Integer.parseInt(map.get("width"));
                int parseInt2 = Integer.parseInt(map.get("height"));
                int parseInt3 = Integer.parseInt(map.get("offsetX"));
                int parseInt4 = Integer.parseInt(map.get("offsetY"));
                String str = map.get("allowOffscreen");
                String str2 = map.get("customClosePosition");
                if (str != null) {
                    if (!Boolean.parseBoolean(str)) {
                        z = false;
                        this.resizeProperties = new com.startapp.sdk.adsbase.mraid.b.b(parseInt, parseInt2, parseInt3, parseInt4, str2, z);
                    }
                }
                z = true;
                this.resizeProperties = new com.startapp.sdk.adsbase.mraid.b.b(parseInt, parseInt2, parseInt3, parseInt4, str2, z);
            } catch (Exception unused) {
                com.iab.omid.library.startapp.b.a(this.activeWebView, "wrong format", "setResizeProperties");
            }
        }

        /* access modifiers changed from: package-private */
        public void setState(MraidState mraidState2) {
            this.mraidState = mraidState2;
            com.iab.omid.library.startapp.b.a(this.mraidState, this.activeWebView);
        }

        public void useCustomClose(String str) {
            BannerStandard.a(BannerStandard.this, Boolean.parseBoolean(str));
        }
    }

    public BannerStandard(Activity activity) {
        this((Context) activity);
    }

    private ViewGroup A() {
        View view;
        View rootView;
        ViewGroup viewGroup = this.D;
        if (viewGroup != null) {
            return viewGroup;
        }
        Context context = getContext();
        RelativeLayout relativeLayout = this.v;
        View view2 = null;
        if (!(context instanceof Activity)) {
            view = null;
        } else {
            view = ((Activity) context).getWindow().getDecorView().findViewById(16908290);
        }
        if (!(relativeLayout == null || (rootView = relativeLayout.getRootView()) == null || (view2 = rootView.findViewById(16908290)) != null)) {
            view2 = rootView;
        }
        if (view == null) {
            view = view2;
        }
        return view instanceof ViewGroup ? (ViewGroup) view : this.v;
    }

    private ViewGroup B() {
        if (this.D == null) {
            this.D = A();
        }
        return this.D;
    }

    private void t() {
        RelativeLayout relativeLayout = this.v;
        if (relativeLayout != null) {
            relativeLayout.setVisibility(4);
        }
    }

    private void u() {
        RelativeLayout relativeLayout = this.v;
        if (relativeLayout != null) {
            relativeLayout.setVisibility(0);
        }
        if (this.g != null) {
            com.startapp.sdk.c.c.a(getContext()).h().a(AdPreferences.Placement.INAPP_BANNER, s(), this.g.getAdId());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:34:0x00a1 A[Catch:{ all -> 0x0127 }] */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00ee A[Catch:{ all -> 0x0127 }, EDGE_INSN: B:50:0x00ee->B:43:0x00ee ?: BREAK  
    EDGE_INSN: B:51:0x00ee->B:43:0x00ee ?: BREAK  ] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x00f0 A[Catch:{ all -> 0x0127 }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x00f4 A[Catch:{ all -> 0x0127 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.graphics.Point v() {
        /*
            r7 = this;
            android.graphics.Point r0 = new android.graphics.Point
            r0.<init>()
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            if (r1 == 0) goto L_0x0025
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.width
            if (r1 <= 0) goto L_0x0025
            android.content.Context r1 = r7.getContext()
            android.view.ViewGroup$LayoutParams r2 = r7.getLayoutParams()
            int r2 = r2.width
            int r2 = r2 + 1
            int r1 = com.startapp.sdk.adsbase.j.t.b(r1, r2)
            r0.x = r1
        L_0x0025:
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            if (r1 == 0) goto L_0x0045
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.height
            if (r1 <= 0) goto L_0x0045
            android.content.Context r1 = r7.getContext()
            android.view.ViewGroup$LayoutParams r2 = r7.getLayoutParams()
            int r2 = r2.height
            int r2 = r2 + 1
            int r1 = com.startapp.sdk.adsbase.j.t.b(r1, r2)
            r0.y = r1
        L_0x0045:
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            if (r1 == 0) goto L_0x0060
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.width
            if (r1 <= 0) goto L_0x0060
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.height
            if (r1 <= 0) goto L_0x0060
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r1 = r7.g
            r1.c_()
        L_0x0060:
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            if (r1 == 0) goto L_0x0076
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.width
            if (r1 <= 0) goto L_0x0076
            android.view.ViewGroup$LayoutParams r1 = r7.getLayoutParams()
            int r1 = r1.height
            if (r1 > 0) goto L_0x0137
        L_0x0076:
            android.content.Context r1 = r7.getContext()
            android.content.res.Resources r1 = r1.getResources()
            android.util.DisplayMetrics r1 = r1.getDisplayMetrics()
            android.view.ViewParent r2 = r7.getParent()     // Catch:{ all -> 0x0127 }
            boolean r2 = r2 instanceof android.view.View     // Catch:{ all -> 0x0127 }
            r3 = 0
            if (r2 == 0) goto L_0x0092
            android.view.ViewParent r2 = r7.getParent()     // Catch:{ all -> 0x0127 }
            android.view.View r2 = (android.view.View) r2     // Catch:{ all -> 0x0127 }
            goto L_0x0093
        L_0x0092:
            r2 = r3
        L_0x0093:
            if (r2 == 0) goto L_0x00ee
            int r4 = r2.getMeasuredWidth()     // Catch:{ all -> 0x0127 }
            if (r4 <= 0) goto L_0x00a1
            int r4 = r2.getMeasuredHeight()     // Catch:{ all -> 0x0127 }
            if (r4 > 0) goto L_0x00ee
        L_0x00a1:
            int r4 = r2.getMeasuredWidth()     // Catch:{ all -> 0x0127 }
            if (r4 <= 0) goto L_0x00c0
            android.content.Context r4 = r7.getContext()     // Catch:{ all -> 0x0127 }
            int r5 = r2.getMeasuredWidth()     // Catch:{ all -> 0x0127 }
            int r6 = r2.getPaddingLeft()     // Catch:{ all -> 0x0127 }
            int r5 = r5 - r6
            int r6 = r2.getPaddingRight()     // Catch:{ all -> 0x0127 }
            int r5 = r5 - r6
            int r4 = com.startapp.sdk.adsbase.j.t.b(r4, r5)     // Catch:{ all -> 0x0127 }
            a((android.graphics.Point) r0, (int) r4)     // Catch:{ all -> 0x0127 }
        L_0x00c0:
            int r4 = r2.getMeasuredHeight()     // Catch:{ all -> 0x0127 }
            if (r4 <= 0) goto L_0x00df
            android.content.Context r4 = r7.getContext()     // Catch:{ all -> 0x0127 }
            int r5 = r2.getMeasuredHeight()     // Catch:{ all -> 0x0127 }
            int r6 = r2.getPaddingBottom()     // Catch:{ all -> 0x0127 }
            int r5 = r5 - r6
            int r6 = r2.getPaddingTop()     // Catch:{ all -> 0x0127 }
            int r5 = r5 - r6
            int r4 = com.startapp.sdk.adsbase.j.t.b(r4, r5)     // Catch:{ all -> 0x0127 }
            b((android.graphics.Point) r0, (int) r4)     // Catch:{ all -> 0x0127 }
        L_0x00df:
            android.view.ViewParent r4 = r2.getParent()     // Catch:{ all -> 0x0127 }
            boolean r4 = r4 instanceof android.view.View     // Catch:{ all -> 0x0127 }
            if (r4 == 0) goto L_0x0092
            android.view.ViewParent r2 = r2.getParent()     // Catch:{ all -> 0x0127 }
            android.view.View r2 = (android.view.View) r2     // Catch:{ all -> 0x0127 }
            goto L_0x0093
        L_0x00ee:
            if (r2 != 0) goto L_0x00f4
            r7.a((android.graphics.Point) r0, (android.util.DisplayMetrics) r1)     // Catch:{ all -> 0x0127 }
            goto L_0x0137
        L_0x00f4:
            android.content.Context r3 = r7.getContext()     // Catch:{ all -> 0x0127 }
            int r4 = r2.getMeasuredWidth()     // Catch:{ all -> 0x0127 }
            int r5 = r2.getPaddingLeft()     // Catch:{ all -> 0x0127 }
            int r4 = r4 - r5
            int r5 = r2.getPaddingRight()     // Catch:{ all -> 0x0127 }
            int r4 = r4 - r5
            int r3 = com.startapp.sdk.adsbase.j.t.b(r3, r4)     // Catch:{ all -> 0x0127 }
            a((android.graphics.Point) r0, (int) r3)     // Catch:{ all -> 0x0127 }
            android.content.Context r3 = r7.getContext()     // Catch:{ all -> 0x0127 }
            int r4 = r2.getMeasuredHeight()     // Catch:{ all -> 0x0127 }
            int r5 = r2.getPaddingBottom()     // Catch:{ all -> 0x0127 }
            int r4 = r4 - r5
            int r2 = r2.getPaddingTop()     // Catch:{ all -> 0x0127 }
            int r4 = r4 - r2
            int r2 = com.startapp.sdk.adsbase.j.t.b(r3, r4)     // Catch:{ all -> 0x0127 }
            b((android.graphics.Point) r0, (int) r2)     // Catch:{ all -> 0x0127 }
            goto L_0x0137
        L_0x0127:
            r2 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e
            r3.<init>((java.lang.Throwable) r2)
            android.content.Context r2 = r7.getContext()
            r3.a((android.content.Context) r2)
            r7.a((android.graphics.Point) r0, (android.util.DisplayMetrics) r1)
        L_0x0137:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.banner.bannerstandard.BannerStandard.v():android.graphics.Point");
    }

    private void w() {
        WebView webView2 = this.webView;
        if (webView2 != null) {
            com.startapp.common.b.b.c(webView2);
        }
        WebView webView3 = this.twoPartWebView;
        if (webView3 != null) {
            com.startapp.common.b.b.c(webView3);
        }
    }

    private void x() {
        WebView webView2 = this.webView;
        if (webView2 != null) {
            com.startapp.common.b.b.b(webView2);
        }
        WebView webView3 = this.twoPartWebView;
        if (webView3 != null) {
            com.startapp.common.b.b.b(webView3);
        }
    }

    private void y() {
        c cVar = this.z;
        if (cVar != null) {
            cVar.a();
        }
        c cVar2 = this.A;
        if (cVar2 != null) {
            cVar2.a();
        }
    }

    private void z() {
        this.n.removeCallbacksAndMessages((Object) null);
    }

    /* access modifiers changed from: protected */
    public int c() {
        return 50;
    }

    /* access modifiers changed from: protected */
    public String d() {
        return "StartApp Banner";
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return this.e;
    }

    public void hideBanner() {
        this.m = false;
        t();
    }

    public void loadHtml() {
        String j2;
        BannerStandardAd bannerStandardAd = this.g;
        if (bannerStandardAd != null && (j2 = bannerStandardAd.j()) != null) {
            if (j() != null && j().length() > 0) {
                j2 = j2.replaceAll("startapp_adtag_placeholder", j());
            }
            this.n.postDelayed(new Runnable() {
                public void run() {
                    BannerStandard.this.m();
                }
            }, (long) this.p.i());
            this.o = System.currentTimeMillis();
            u.a(getContext(), this.webView, j2);
        }
    }

    /* access modifiers changed from: protected */
    public final View o() {
        RelativeLayout relativeLayout = this.v;
        if (relativeLayout != null) {
            return relativeLayout;
        }
        return super.o();
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        w();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        x();
        a(false);
        y();
        z();
        b bVar = this.y;
        if (bVar != null) {
            bVar.b();
            this.y = null;
            u.a((Object) this.webView);
        }
    }

    public void onFailedToReceiveAd(Ad ad) {
        a(ad.getErrorMessage());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x01af, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x01b0, code lost:
        new com.startapp.sdk.adsbase.infoevents.e(r12).a(getContext());
        a(r12.getMessage());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x01c3, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:32:0x010e */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x013b A[Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x014c A[Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0177 A[Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }] */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x0196 A[Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }] */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x01a1 A[Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x01af A[ExcHandler: all (r12v4 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:28:0x00ff] */
    /* JADX WARNING: Removed duplicated region for block: B:63:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onReceiveAd(com.startapp.sdk.adsbase.Ad r12) {
        /*
            r11 = this;
            r12 = 0
            r11.h = r12
            android.widget.RelativeLayout r0 = r11.u
            r11.removeView(r0)
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r0 = r11.g
            if (r0 == 0) goto L_0x01ca
            java.lang.String r0 = r0.j()
            if (r0 == 0) goto L_0x01ca
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r0 = r11.g
            java.lang.String r0 = r0.j()
            java.lang.String r1 = ""
            int r0 = r0.compareTo(r1)
            if (r0 == 0) goto L_0x01ca
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r0 = r11.g
            java.lang.String r0 = r0.j()
            java.lang.String r1 = "@jsTag@"
            java.lang.String r0 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, (java.lang.String) r1, (java.lang.String) r1)
            java.lang.String r1 = "true"
            boolean r0 = r1.equals(r0)
            r11.i = r0
            r11.loadHtml()
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r0 = r11.g
            java.lang.String r0 = r0.j()
            java.lang.String r1 = "@width@"
            java.lang.String r0 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, (java.lang.String) r1, (java.lang.String) r1)
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r1 = r11.g
            java.lang.String r1 = r1.j()
            java.lang.String r2 = "@height@"
            java.lang.String r1 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r1, (java.lang.String) r2, (java.lang.String) r2)
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r1 = java.lang.Integer.parseInt(r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.graphics.Point r2 = r11.v()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r3 = r2.x     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r4 = 1
            if (r3 < r0) goto L_0x00a8
            int r2 = r2.y     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r2 < r1) goto L_0x00a8
            com.startapp.sdk.ads.banner.c r12 = r11.r     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.a(r0, r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.content.Context r12 = r11.getContext()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.c r0 = r11.r     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r0 = r0.a()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r12 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r12, (int) r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.content.Context r0 = r11.getContext()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.c r1 = r11.r     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r1 = r1.b()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r0 = com.startapp.sdk.adsbase.j.t.a((android.content.Context) r0, (int) r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.widget.RelativeLayout r1 = r11.v     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.setMinimumWidth(r12)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.widget.RelativeLayout r1 = r11.v     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.setMinimumHeight(r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.webkit.WebView r1 = r11.webView     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r1 != 0) goto L_0x009d
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.<init>(r12, r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            goto L_0x00a1
        L_0x009d:
            r1.width = r12     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.height = r0     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x00a1:
            android.webkit.WebView r12 = r11.webView     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.setLayoutParams(r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12 = 1
            goto L_0x00cc
        L_0x00a8:
            android.graphics.Point r0 = new android.graphics.Point     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r0.<init>(r12, r12)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.webkit.WebView r1 = r11.webView     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.view.ViewGroup$LayoutParams r1 = r1.getLayoutParams()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r1 != 0) goto L_0x00bf
            android.widget.RelativeLayout$LayoutParams r1 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r2 = r0.x     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r0 = r0.y     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.<init>(r2, r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            goto L_0x00c7
        L_0x00bf:
            int r2 = r0.x     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.width = r2     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r0 = r0.y     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r1.height = r0     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x00c7:
            android.webkit.WebView r0 = r11.webView     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r0.setLayoutParams(r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x00cc:
            if (r12 == 0) goto L_0x01a9
            r11.k = r4     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject r12 = r11.t     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 != 0) goto L_0x00ff
            android.widget.RelativeLayout r12 = r11.u     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 != 0) goto L_0x00ff
            android.widget.RelativeLayout r12 = new android.widget.RelativeLayout     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.content.Context r0 = r11.getContext()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.<init>(r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r11.u = r12     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject r12 = new com.startapp.sdk.adsbase.adinformation.AdInformationObject     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.content.Context r0 = r11.getContext()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject$Size r1 = com.startapp.sdk.adsbase.adinformation.AdInformationObject.Size.SMALL     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r2 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_BANNER     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r3 = r11.g     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.adinformation.AdInformationOverrides r3 = r3.getAdInfoOverride()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.<init>(r0, r1, r2, r3)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r11.t = r12     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.adinformation.AdInformationObject r12 = r11.t     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.widget.RelativeLayout r0 = r11.u     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.a((android.widget.RelativeLayout) r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x00ff:
            android.widget.RelativeLayout r12 = r11.u     // Catch:{ Exception -> 0x010e, all -> 0x01af }
            android.view.ViewParent r12 = r12.getParent()     // Catch:{ Exception -> 0x010e, all -> 0x01af }
            android.view.ViewGroup r12 = (android.view.ViewGroup) r12     // Catch:{ Exception -> 0x010e, all -> 0x01af }
            if (r12 == 0) goto L_0x010e
            android.widget.RelativeLayout r0 = r11.u     // Catch:{ Exception -> 0x010e, all -> 0x01af }
            r12.removeView(r0)     // Catch:{ Exception -> 0x010e, all -> 0x01af }
        L_0x010e:
            android.widget.RelativeLayout$LayoutParams r12 = new android.widget.RelativeLayout$LayoutParams     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r0 = -2
            r12.<init>(r0, r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r0 = 13
            r12.addRule(r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.webkit.WebView r0 = r11.webView     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.widget.RelativeLayout r1 = r11.u     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r0.addView(r1, r12)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.h r12 = new com.startapp.sdk.adsbase.h     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.content.Context r6 = r11.getContext()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r0 = r11.g     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            java.lang.String[] r7 = r0.trackingUrls     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.commontracking.TrackingParams r8 = new com.startapp.sdk.adsbase.commontracking.TrackingParams     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            java.lang.String r0 = r11.j()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r8.<init>(r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r0 = r11.g     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            java.lang.Long r0 = r0.t()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r0 == 0) goto L_0x014c
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r1 = r11.g     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            java.lang.Long r1 = r1.t()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            long r1 = r1.longValue()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            long r0 = r0.toMillis(r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            goto L_0x015a
        L_0x014c:
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.remoteconfig.MetaData r1 = com.startapp.sdk.adsbase.remoteconfig.MetaData.G()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            long r1 = r1.H()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            long r0 = r0.toMillis(r1)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x015a:
            r9 = r0
            r5 = r12
            r5.<init>(r6, r7, r8, r9)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r11.x = r12     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.h r12 = r11.x     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandard$9 r0 = new com.startapp.sdk.ads.banner.bannerstandard.BannerStandard$9     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r0.<init>()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.a((com.startapp.sdk.adsbase.h.a) r0)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.adsbase.h r12 = r11.x     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r11.a((com.startapp.sdk.adsbase.h) r12)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r11.r()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r12 = r11.g     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 == 0) goto L_0x0192
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd r12 = r11.g     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            boolean r12 = r12.v()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 != 0) goto L_0x0180
            goto L_0x0192
        L_0x0180:
            com.startapp.sdk.adsbase.k.c r12 = new com.startapp.sdk.adsbase.k.c     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            android.webkit.WebView r0 = r11.webView     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            int r1 = com.startapp.sdk.ads.banner.BannerBase.n()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.bannerstandard.BannerStandard$8 r2 = new com.startapp.sdk.ads.banner.bannerstandard.BannerStandard$8     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r2.<init>()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.<init>(r0, r1, r2)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r11.z = r12     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x0192:
            boolean r12 = r11.m     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 == 0) goto L_0x0199
            r11.u()     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x0199:
            com.startapp.sdk.ads.banner.BannerListener r12 = r11.j     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 == 0) goto L_0x01ae
            boolean r12 = r11.s     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            if (r12 != 0) goto L_0x01ae
            r11.s = r4     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            com.startapp.sdk.ads.banner.BannerListener r12 = r11.j     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            r12.onReceiveAd(r11)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
            return
        L_0x01a9:
            java.lang.String r12 = "Banner cannot be displayed (not enough room)"
            r11.a((java.lang.String) r12)     // Catch:{ NumberFormatException -> 0x01c4, all -> 0x01af }
        L_0x01ae:
            return
        L_0x01af:
            r12 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r12)
            android.content.Context r1 = r11.getContext()
            r0.a((android.content.Context) r1)
            java.lang.String r12 = r12.getMessage()
            r11.a((java.lang.String) r12)
            return
        L_0x01c4:
            java.lang.String r12 = "Error Casting width & height from HTML"
            r11.a((java.lang.String) r12)
            return
        L_0x01ca:
            java.lang.String r12 = "No Banner received"
            r11.a((java.lang.String) r12)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.banner.bannerstandard.BannerStandard.onReceiveAd(com.startapp.sdk.adsbase.Ad):void");
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int i2, int i3, int i4, int i5) {
        super.onSizeChanged(i2, i3, i4, i5);
        int a2 = t.a(getContext(), this.r.a());
        int a3 = t.a(getContext(), this.r.b());
        if (i2 < a2 || i3 < a3) {
            t();
        } else if (this.m && this.k) {
            u();
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            w();
        } else {
            x();
        }
    }

    /* access modifiers changed from: protected */
    public final void p() {
        h hVar = this.x;
        if (hVar != null && hVar.c()) {
            super.p();
        }
    }

    /* access modifiers changed from: protected */
    public int s() {
        return 0;
    }

    public void setAdTag(String str) {
        this.f = str;
    }

    public void setBannerListener(BannerListener bannerListener) {
        this.j = bannerListener;
    }

    public void showBanner() {
        this.m = true;
        u();
    }

    public BannerStandard(Activity activity, AdPreferences adPreferences) {
        this((Context) activity, adPreferences);
    }

    private void b(WebView webView2) {
        webView2.setBackgroundColor(0);
        webView2.setHorizontalScrollBarEnabled(false);
        webView2.getSettings().setJavaScriptEnabled(true);
        webView2.setVerticalScrollBarEnabled(false);
        webView2.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View view, MotionEvent motionEvent) {
                BannerStandard.this.h = true;
                if (motionEvent.getAction() == 2) {
                    return true;
                }
                return false;
            }
        });
        webView2.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                return true;
            }
        });
        webView2.setLongClickable(false);
    }

    static /* synthetic */ void h(BannerStandard bannerStandard) {
        com.startapp.sdk.adsbase.mraid.b.b resizeProperties = bannerStandard.B.getResizeProperties();
        if (resizeProperties == null) {
            com.iab.omid.library.startapp.b.a(bannerStandard.webView, "requires: setResizeProperties first", "resize");
            return;
        }
        bannerStandard.q();
        if (bannerStandard.B.getState() != MraidState.LOADING && bannerStandard.B.getState() != MraidState.HIDDEN) {
            if (bannerStandard.B.getState() == MraidState.EXPANDED) {
                com.iab.omid.library.startapp.b.a(bannerStandard.webView, "Not allowed to resize from an already expanded ad", "resize");
                return;
            }
            int i2 = resizeProperties.f6419a;
            int i3 = resizeProperties.b;
            int i4 = resizeProperties.c;
            int i5 = resizeProperties.d;
            int[] iArr = new int[2];
            bannerStandard.webView.getLocationOnScreen(iArr);
            Context context = bannerStandard.getContext();
            int b = t.b(context, iArr[0]) + i4;
            int b2 = t.b(context, iArr[1]) + i5;
            Rect rect = new Rect(b, b2, i2 + b, i3 + b2);
            ViewGroup A2 = bannerStandard.A();
            int b3 = t.b(context, A2.getWidth());
            int b4 = t.b(context, A2.getHeight());
            int[] iArr2 = new int[2];
            A2.getLocationOnScreen(iArr2);
            int b5 = t.b(context, iArr2[0]);
            int b6 = t.b(context, iArr2[1]);
            if (!resizeProperties.f) {
                if (rect.width() > b3 || rect.height() > b4) {
                    com.iab.omid.library.startapp.b.a(bannerStandard.webView, "Not enough room for the ad", "resize");
                    return;
                }
                rect.offsetTo(a(b5, rect.left, (b5 + b3) - rect.width()), a(b6, rect.top, (b6 + b4) - rect.height()));
            }
            Rect rect2 = new Rect();
            try {
                CloseableLayout.ClosePosition a2 = CloseableLayout.ClosePosition.a(resizeProperties.e, CloseableLayout.ClosePosition.TOP_RIGHT);
                bannerStandard.w.a(a2, rect, rect2);
                if (!new Rect(b5, b6, b3 + b5, b4 + b6).contains(rect2)) {
                    com.iab.omid.library.startapp.b.a(bannerStandard.webView, "The close region to appear within the max allowed size", "resize");
                } else if (!rect.contains(rect2)) {
                    com.iab.omid.library.startapp.b.a(bannerStandard.webView, "The close region to appear within the max allowed size", "resize");
                } else {
                    bannerStandard.w.setCloseVisible(false);
                    bannerStandard.w.setClosePosition(a2);
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(rect.width(), rect.height());
                    layoutParams.leftMargin = rect.left - b5;
                    layoutParams.topMargin = rect.top - b6;
                    if (bannerStandard.B.getState() == MraidState.DEFAULT) {
                        RelativeLayout relativeLayout = bannerStandard.v;
                        if (relativeLayout != null) {
                            relativeLayout.removeView(bannerStandard.webView);
                            bannerStandard.v.setVisibility(4);
                        }
                        bannerStandard.w.addView(bannerStandard.webView, new FrameLayout.LayoutParams(-1, -1));
                        bannerStandard.B().addView(bannerStandard.w, layoutParams);
                    } else if (bannerStandard.B.getState() == MraidState.RESIZED) {
                        bannerStandard.w.setLayoutParams(layoutParams);
                    }
                    bannerStandard.w.setClosePosition(a2);
                    bannerStandard.B.setState(MraidState.RESIZED);
                }
            } catch (Exception e) {
                com.iab.omid.library.startapp.b.a(bannerStandard.webView, e.getMessage(), "resize");
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void e() {
        b bVar = this.y;
        if (bVar != null) {
            bVar.b();
            this.y = null;
        }
        if (this.q == null) {
            this.q = new AdPreferences();
        }
        if (this.g != null) {
            Point point = this.b;
            if (point == null) {
                point = v();
            }
            BannerStandardAd bannerStandardAd = this.g;
            int i2 = point.x;
            int i3 = point.y;
            bannerStandardAd.b(i2);
            bannerStandardAd.c(i3);
            this.g.setState(Ad.AdState.UN_INITIALIZED);
            this.g.a(s());
            this.g.load(this.q, this);
        }
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return this.p.i();
    }

    /* access modifiers changed from: protected */
    public final int g() {
        BannerStandardAd bannerStandardAd = this.g;
        if (bannerStandardAd == null) {
            return 0;
        }
        return bannerStandardAd.a();
    }

    /* access modifiers changed from: protected */
    public final int i() {
        return Math.max(this.p.i() - ((int) this.o), 0);
    }

    public BannerStandard(Activity activity, BannerListener bannerListener) {
        this((Context) activity, bannerListener);
    }

    private void a(View view) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(t.a(getContext(), this.r.a()), t.a(getContext(), this.r.b()));
        layoutParams.addRule(13);
        this.v.addView(view, layoutParams);
    }

    public BannerStandard(Activity activity, AdPreferences adPreferences, BannerListener bannerListener) {
        this((Context) activity, adPreferences, bannerListener);
    }

    public BannerStandard(Activity activity, boolean z2) {
        this((Context) activity, z2);
    }

    public BannerStandard(Activity activity, boolean z2, AdPreferences adPreferences) {
        this((Context) activity, z2, adPreferences);
    }

    public BannerStandard(Activity activity, AttributeSet attributeSet) {
        this((Context) activity, attributeSet);
    }

    public BannerStandard(Activity activity, AttributeSet attributeSet, int i2) {
        this((Context) activity, attributeSet, i2);
    }

    private void a(Point point, DisplayMetrics displayMetrics) {
        a(point, t.b(getContext(), displayMetrics.widthPixels));
        b(point, t.b(getContext(), displayMetrics.heightPixels));
    }

    @Deprecated
    public BannerStandard(Context context) {
        this(context, true, (AdPreferences) null);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            Context context = getContext();
            this.w = new CloseableLayout(context);
            this.w.setOnCloseListener(new CloseableLayout.a() {
                public void onClose() {
                    BannerStandard.a(BannerStandard.this);
                }
            });
            this.webView = new WebView(context);
            this.B = new MraidBannerController(this.webView, new a.C0078a() {
                public boolean onClickEvent(String str) {
                    if (!BannerStandard.this.h) {
                        e h = new e(InfoEventCategory.ERROR).f("fake_click").h(com.startapp.sdk.adsbase.a.e(str));
                        h.g("jsTag=" + BannerStandard.this.i).a(BannerStandard.this.getContext());
                    }
                    BannerStandard bannerStandard = BannerStandard.this;
                    if ((!bannerStandard.i || bannerStandard.h) && str != null) {
                        return BannerStandard.this.b(str);
                    }
                    return false;
                }
            });
            this.p = new BannerOptions();
            this.g = new BannerStandardAd(context, g());
            if (this.q == null) {
                this.q = new AdPreferences();
            }
            if (getId() == -1) {
                setId(this.e);
            }
            this.webView.setId(159868225);
            b(this.webView);
            this.p = BannerMetaData.a().c();
            a(this.q);
            setMinimumWidth(t.a(getContext(), this.r.a()));
            setMinimumHeight(t.a(getContext(), this.r.b()));
            this.webView.addJavascriptInterface(new com.startapp.sdk.e.b(getContext(), (Runnable) new Runnable() {
                public void run() {
                }
            }, new TrackingParams(j()), this.g.e(0)), "startappwall");
            this.v = new RelativeLayout(getContext());
            a((View) this.webView);
            t();
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            layoutParams.addRule(13);
            addView(this.v, layoutParams);
            if (this.l) {
                getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        com.startapp.common.b.b.a(BannerStandard.this.getViewTreeObserver(), (ViewTreeObserver.OnGlobalLayoutListener) this);
                        if (!BannerStandard.this.k) {
                            BannerStandard.this.k();
                        }
                    }
                });
            }
        } catch (Throwable th) {
            new e(th).a(getContext());
            hideBanner();
            a("BannerStandard.init - webview failed");
        }
    }

    @Deprecated
    public BannerStandard(Context context, AdPreferences adPreferences) {
        this(context, true, adPreferences);
    }

    private static void a(Point point, int i2) {
        if (point.x <= 0) {
            point.x = i2;
        }
    }

    @Deprecated
    public BannerStandard(Context context, BannerListener bannerListener) {
        this(context, true, (AdPreferences) null);
        setBannerListener(bannerListener);
    }

    private void a(String str) {
        setErrorMessage(str);
        BannerListener bannerListener = this.j;
        if (bannerListener != null && !this.s) {
            this.s = true;
            bannerListener.onFailedToReceiveAd(this);
        }
    }

    @Deprecated
    public BannerStandard(Context context, AdPreferences adPreferences, BannerListener bannerListener) {
        this(context, true, adPreferences);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public BannerStandard(Context context, boolean z2) {
        this(context, z2, (AdPreferences) null);
    }

    @Deprecated
    public BannerStandard(Context context, boolean z2, AdPreferences adPreferences) {
        super(context);
        this.k = false;
        this.h = true;
        this.i = false;
        this.l = true;
        this.m = true;
        this.n = new Handler(Looper.getMainLooper());
        this.r = new com.startapp.sdk.ads.banner.c(300, c());
        this.s = false;
        this.t = null;
        this.u = null;
        try {
            this.l = z2;
            this.q = adPreferences;
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(WebView webView2) {
        z();
        if (MetaData.G().S()) {
            this.y = com.startapp.sdk.omsdk.a.a(webView2);
            b bVar = this.y;
            if (bVar != null) {
                RelativeLayout relativeLayout = this.u;
                if (relativeLayout != null) {
                    bVar.b(relativeLayout);
                }
                CloseableLayout closeableLayout = this.w;
                if (closeableLayout != null) {
                    this.y.b(closeableLayout);
                }
                this.y.a(webView2);
                this.y.a();
                com.iab.omid.library.startapp.adsession.a.a(this.y).a();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.e = i2;
    }

    private void a(boolean z2) {
        h hVar = this.x;
        if (hVar != null) {
            hVar.a(z2);
        }
    }

    private static int a(int i2, int i3, int i4) {
        return Math.max(i2, Math.min(i3, i4));
    }

    static /* synthetic */ void a(BannerStandard bannerStandard) {
        if (bannerStandard.B.getState() != MraidState.LOADING && bannerStandard.B.getState() != MraidState.HIDDEN) {
            if (bannerStandard.B.getState() == MraidState.RESIZED || bannerStandard.B.getState() == MraidState.EXPANDED) {
                if (bannerStandard.C != null) {
                    bannerStandard.w.removeView(bannerStandard.twoPartWebView);
                    bannerStandard.A.a();
                    bannerStandard.A = null;
                    bannerStandard.C = null;
                    bannerStandard.twoPartWebView.stopLoading();
                    bannerStandard.twoPartWebView = null;
                } else {
                    bannerStandard.w.removeView(bannerStandard.webView);
                    bannerStandard.a((View) bannerStandard.webView);
                    bannerStandard.u();
                }
                CloseableLayout closeableLayout = bannerStandard.w;
                if (!(closeableLayout == null || closeableLayout.getParent() == null || !(closeableLayout.getParent() instanceof ViewGroup))) {
                    ((ViewGroup) closeableLayout.getParent()).removeView(closeableLayout);
                }
                bannerStandard.B.setState(MraidState.DEFAULT);
            } else if (bannerStandard.B.getState() == MraidState.DEFAULT) {
                bannerStandard.t();
                bannerStandard.B.setState(MraidState.HIDDEN);
            }
            bannerStandard.p();
        }
    }

    @Deprecated
    public BannerStandard(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Deprecated
    public BannerStandard(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.k = false;
        this.h = true;
        this.i = false;
        this.l = true;
        this.m = true;
        this.n = new Handler(Looper.getMainLooper());
        this.r = new com.startapp.sdk.ads.banner.c(300, c());
        this.s = false;
        this.t = null;
        this.u = null;
        try {
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    private static void b(Point point, int i2) {
        if (point.y <= 0) {
            point.y = i2;
        }
    }

    /* access modifiers changed from: private */
    public boolean b(String str) {
        BannerListener bannerListener = this.j;
        if (bannerListener != null) {
            bannerListener.onClick(this);
        }
        a(true);
        y();
        z();
        boolean a2 = com.startapp.sdk.adsbase.a.a(getContext(), AdPreferences.Placement.INAPP_BANNER);
        String[] q2 = this.g.q();
        String[] s2 = this.g.s();
        if (this.i) {
            String str2 = str;
        } else if (str.contains("index=")) {
            try {
                int a3 = com.startapp.sdk.adsbase.a.a(str);
                if (a3 < 0) {
                    e f = new e(InfoEventCategory.ERROR).f("Wrong index extracted from URL");
                    f.g("adId: " + this.g.getAdId()).a(getContext());
                    return false;
                } else if (!this.g.d(a3) || a2) {
                    com.startapp.sdk.adsbase.a.a(getContext(), str, a3 < q2.length ? q2[a3] : null, new TrackingParams(j()), this.g.e(a3) && !a2, false);
                    this.webView.stopLoading();
                    setClicked(true);
                    return true;
                } else {
                    com.startapp.sdk.adsbase.a.a(getContext(), str, a3 < q2.length ? q2[a3] : null, a3 < s2.length ? s2[a3] : null, new TrackingParams(j()), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), this.g.e(a3), this.g.f(a3));
                    this.webView.stopLoading();
                    setClicked(true);
                    return true;
                }
            } catch (Throwable th) {
                new e(th).a(getContext());
                return false;
            }
        }
        if (q2.length <= 0) {
            e f2 = new e(InfoEventCategory.ERROR).f("No tracking URLs");
            f2.g("adId: " + this.g.getAdId()).a(getContext());
            return false;
        } else if (!this.g.d(0) || a2) {
            com.startapp.sdk.adsbase.a.a(getContext(), str, q2[0], new TrackingParams(j()), this.g.e(0) && !a2, false);
            this.webView.stopLoading();
            setClicked(true);
            return true;
        } else if (s2.length <= 0) {
            e f3 = new e(InfoEventCategory.ERROR).f("No package names");
            f3.g("adId: " + this.g.getAdId()).a(getContext());
            return false;
        } else {
            com.startapp.sdk.adsbase.a.a(getContext(), str, q2[0], s2[0], new TrackingParams(j()), AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), this.g.e(0), this.g.f(0));
            this.webView.stopLoading();
            setClicked(true);
            return true;
        }
    }

    static /* synthetic */ void a(BannerStandard bannerStandard, boolean z2) {
        if (z2 != (!bannerStandard.w.a())) {
            bannerStandard.w.setCloseVisible(!z2);
        }
    }

    static /* synthetic */ void b(BannerStandard bannerStandard, String str) {
        bannerStandard.q();
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(-1, -1);
        boolean z2 = str != null && !TextUtils.isEmpty(str);
        if (z2) {
            bannerStandard.h = false;
            if (bannerStandard.twoPartWebView == null) {
                bannerStandard.twoPartWebView = new WebView(bannerStandard.getContext());
            }
            bannerStandard.C = new MraidBannerController(bannerStandard.twoPartWebView, new a.C0078a() {
                public boolean onClickEvent(String str) {
                    if (!BannerStandard.this.h) {
                        e h = new e(InfoEventCategory.ERROR).f("fake_click").h(com.startapp.sdk.adsbase.a.e(str));
                        h.g("jsTag=" + BannerStandard.this.i).a(BannerStandard.this.getContext());
                    }
                    BannerStandard bannerStandard = BannerStandard.this;
                    if ((!bannerStandard.i || bannerStandard.h) && str != null) {
                        return BannerStandard.this.b(str);
                    }
                    return false;
                }
            });
            bannerStandard.A = new c(bannerStandard.twoPartWebView, BannerBase.n(), new c.a() {
                public boolean onUpdate(boolean z) {
                    BannerStandard.this.B.fireViewableChangeEvent(z);
                    BannerStandard.this.C.fireViewableChangeEvent(z);
                    return BannerStandard.this.g.v();
                }
            });
            bannerStandard.twoPartWebView.setId(159868226);
            bannerStandard.b(bannerStandard.twoPartWebView);
            bannerStandard.twoPartWebView.loadUrl(str);
        }
        if (bannerStandard.B.getState() == MraidState.DEFAULT) {
            if (z2) {
                bannerStandard.w.addView(bannerStandard.twoPartWebView, layoutParams);
            } else {
                RelativeLayout relativeLayout = bannerStandard.v;
                if (relativeLayout != null) {
                    relativeLayout.removeView(bannerStandard.webView);
                    bannerStandard.v.setVisibility(4);
                }
                bannerStandard.w.addView(bannerStandard.webView, layoutParams);
            }
            bannerStandard.B().addView(bannerStandard.w, new FrameLayout.LayoutParams(-1, -1));
        } else if (bannerStandard.B.getState() == MraidState.RESIZED && z2) {
            bannerStandard.w.removeView(bannerStandard.webView);
            RelativeLayout relativeLayout2 = bannerStandard.v;
            if (relativeLayout2 != null) {
                relativeLayout2.addView(bannerStandard.webView, layoutParams);
                bannerStandard.v.setVisibility(4);
            }
            bannerStandard.w.addView(bannerStandard.twoPartWebView, layoutParams);
        }
        bannerStandard.w.setLayoutParams(layoutParams);
        bannerStandard.B.setState(MraidState.EXPANDED);
    }
}
