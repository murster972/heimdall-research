package com.startapp.sdk.ads.banner;

import android.content.Context;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.j.m;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.c.c;

public final class a extends GetAdRequest {
    private boolean c;
    private int d;

    public final void a(boolean z) {
        this.c = z;
    }

    public final void a(int i) {
        this.d = i;
    }

    /* access modifiers changed from: protected */
    public final void a(m mVar) throws SDKException {
        super.a(mVar);
        mVar.a("fixedSize", Boolean.valueOf(this.c), false);
        mVar.a("bnrt", Integer.valueOf(this.d), false);
    }

    public final void a(Context context) {
        this.b = c.a(context).h().a(g(), this.d);
    }
}
