package com.startapp.sdk.ads.banner.banner3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import com.startapp.common.a;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class a implements Parcelable, a.C0062a {
    public static final Parcelable.Creator<a> CREATOR = new Parcelable.Creator<a>() {
        public final /* synthetic */ Object createFromParcel(Parcel parcel) {
            return new a(parcel);
        }

        public final /* bridge */ /* synthetic */ Object[] newArray(int i) {
            return new a[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private AdDetails f6125a;
    private Point b;
    private Bitmap c = null;
    private Bitmap d = null;
    private AtomicBoolean e = new AtomicBoolean(false);
    private TrackingParams f;
    private h g = null;
    private Banner3DView h = null;

    public a(Context context, ViewGroup viewGroup, AdDetails adDetails, BannerOptions bannerOptions, TrackingParams trackingParams) {
        this.f6125a = adDetails;
        this.f = trackingParams;
        a(context, bannerOptions, viewGroup);
    }

    private void d() {
        int i;
        this.d = a((View) this.h);
        Point point = this.b;
        int i2 = point.x;
        if (i2 > 0 && (i = point.y) > 0) {
            this.d = Bitmap.createScaledBitmap(this.d, i2, i, false);
        }
    }

    public final Bitmap a() {
        return this.d;
    }

    public final void b() {
        h hVar = this.g;
        if (hVar != null) {
            hVar.a(false);
        }
    }

    public final void c() {
        a(this.c);
        a(this.d);
        this.c = null;
        this.d = null;
        h hVar = this.g;
        if (hVar != null) {
            hVar.a(false);
        }
        Banner3DView banner3DView = this.h;
        if (banner3DView != null) {
            banner3DView.removeAllViews();
            this.h = null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeParcelable(this.f6125a, i);
        parcel.writeInt(this.b.x);
        parcel.writeInt(this.b.y);
        parcel.writeParcelable(this.c, i);
        parcel.writeBooleanArray(new boolean[]{this.e.get()});
        parcel.writeSerializable(this.f);
    }

    public final void a(Context context, BannerOptions bannerOptions, ViewGroup viewGroup) {
        int a2 = t.a(context, bannerOptions.e() - 5);
        this.b = new Point((int) (((float) t.a(context, bannerOptions.d())) * bannerOptions.j()), (int) (((float) t.a(context, bannerOptions.e())) * bannerOptions.k()));
        this.h = new Banner3DView(context, new Point(bannerOptions.d(), bannerOptions.e()));
        this.h.setText(this.f6125a.f());
        this.h.setRating(this.f6125a.k());
        this.h.setDescription(this.f6125a.g());
        this.h.setButtonText(this.f6125a.r());
        Bitmap bitmap = this.c;
        if (bitmap != null) {
            this.h.setImage(bitmap, a2, a2);
        } else {
            this.h.setImage(17301651, a2, a2);
            new com.startapp.common.a(this.f6125a.h(), this, 0).a();
        }
        Point point = this.b;
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(point.x, point.y);
        layoutParams.addRule(13);
        viewGroup.addView(this.h, layoutParams);
        this.h.setVisibility(8);
        d();
    }

    public final void b(Context context) {
        String q = this.f6125a.q();
        boolean a2 = com.startapp.sdk.adsbase.a.a(context, AdPreferences.Placement.INAPP_BANNER);
        h hVar = this.g;
        if (hVar != null) {
            hVar.a(true);
        }
        if (q != null && !"null".equals(q) && !TextUtils.isEmpty(q)) {
            com.startapp.sdk.adsbase.a.a(q, this.f6125a.p(), this.f6125a.c(), context, this.f);
        } else if (!this.f6125a.l() || a2) {
            com.startapp.sdk.adsbase.a.a(context, this.f6125a.c(), this.f6125a.e(), this.f, this.f6125a.w() && !a2, false);
        } else {
            com.startapp.sdk.adsbase.a.a(context, this.f6125a.c(), this.f6125a.e(), this.f6125a.n(), this.f, AdsCommonMetaData.a().B(), AdsCommonMetaData.a().C(), this.f6125a.w(), this.f6125a.z());
        }
    }

    public a(Parcel parcel) {
        this.f6125a = (AdDetails) parcel.readParcelable(AdDetails.class.getClassLoader());
        this.b = new Point(1, 1);
        this.b.x = parcel.readInt();
        this.b.y = parcel.readInt();
        this.c = (Bitmap) parcel.readParcelable(Bitmap.class.getClassLoader());
        boolean[] zArr = new boolean[1];
        parcel.readBooleanArray(zArr);
        this.e.set(zArr[0]);
        this.f = (TrackingParams) parcel.readSerializable();
    }

    private static Bitmap a(View view) {
        view.measure(view.getMeasuredWidth(), view.getMeasuredHeight());
        Bitmap createBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(createBitmap);
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
        view.draw(canvas);
        return createBitmap;
    }

    public final void a(Bitmap bitmap, int i) {
        Banner3DView banner3DView;
        if (bitmap != null && (banner3DView = this.h) != null) {
            this.c = bitmap;
            banner3DView.setImage(bitmap);
            d();
        }
    }

    private static void a(Bitmap bitmap) {
        if (bitmap != null) {
            bitmap.recycle();
        }
    }

    public final h a(Context context) {
        long j;
        if (this.f6125a.d() == null || !this.e.compareAndSet(false, true)) {
            return null;
        }
        String[] strArr = {this.f6125a.d()};
        TrackingParams trackingParams = this.f;
        if (this.f6125a.y() != null) {
            j = TimeUnit.SECONDS.toMillis(this.f6125a.y().longValue());
        } else {
            j = TimeUnit.SECONDS.toMillis(MetaData.G().H());
        }
        this.g = new h(context, strArr, trackingParams, j);
        return this.g;
    }
}
