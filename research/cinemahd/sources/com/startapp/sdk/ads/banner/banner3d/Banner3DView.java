package com.startapp.sdk.ads.banner.banner3d;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.EmbossMaskFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.graphics.drawable.shapes.Shape;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.banner.banner3d.Banner3DSize;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.json.RatingBar;

public class Banner3DView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private TextView f6121a;
    private TextView b;
    private ImageView c;
    private RatingBar d;
    private TextView e;
    private Point f;

    /* renamed from: com.startapp.sdk.ads.banner.banner3d.Banner3DView$2  reason: invalid class name */
    static /* synthetic */ class AnonymousClass2 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6123a = new int[Template.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.sdk.ads.banner.banner3d.Banner3DView$Template[] r0 = com.startapp.sdk.ads.banner.banner3d.Banner3DView.Template.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6123a = r0
                int[] r0 = f6123a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.ads.banner.banner3d.Banner3DView$Template r1 = com.startapp.sdk.ads.banner.banner3d.Banner3DView.Template.XS     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6123a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.ads.banner.banner3d.Banner3DView$Template r1 = com.startapp.sdk.ads.banner.banner3d.Banner3DView.Template.S     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6123a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.ads.banner.banner3d.Banner3DView$Template r1 = com.startapp.sdk.ads.banner.banner3d.Banner3DView.Template.M     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6123a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.ads.banner.banner3d.Banner3DView$Template r1 = com.startapp.sdk.ads.banner.banner3d.Banner3DView.Template.L     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6123a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.sdk.ads.banner.banner3d.Banner3DView$Template r1 = com.startapp.sdk.ads.banner.banner3d.Banner3DView.Template.XL     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.banner.banner3d.Banner3DView.AnonymousClass2.<clinit>():void");
        }
    }

    private enum Template {
        XS,
        S,
        M,
        L,
        XL
    }

    public Banner3DView(Context context) {
        super(context);
        a();
    }

    private void a() {
        Context context = getContext();
        Template b2 = b();
        setBackgroundDrawable(new GradientDrawable(GradientDrawable.Orientation.TOP_BOTTOM, new int[]{AdsCommonMetaData.a().o(), AdsCommonMetaData.a().p()}));
        setLayoutParams(new ViewGroup.LayoutParams(-2, -2));
        int a2 = t.a(context, 2);
        int a3 = t.a(context, 3);
        t.a(context, 4);
        int a4 = t.a(context, 5);
        int a5 = t.a(context, 6);
        int a6 = t.a(context, 8);
        t.a(context, 10);
        int a7 = t.a(context, 20);
        t.a(context, 84);
        int a8 = t.a(context, 90);
        setPadding(a4, 0, a4, 0);
        setTag(this);
        this.c = new ImageView(context);
        this.c.setId(1);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a8, a8);
        layoutParams.addRule(15);
        this.c.setLayoutParams(layoutParams);
        this.f6121a = new TextView(context);
        this.f6121a.setId(2);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-2, -2);
        layoutParams2.addRule(b.a(17), 1);
        layoutParams2.addRule(14);
        this.f6121a.setLayoutParams(layoutParams2);
        this.f6121a.setTextColor(AdsCommonMetaData.a().r().intValue());
        this.f6121a.setGravity(b.a(8388611));
        this.f6121a.setBackgroundColor(0);
        int i = AnonymousClass2.f6123a[b2.ordinal()];
        if (i == 1 || i == 2) {
            this.f6121a.setTextSize(17.0f);
            this.f6121a.setPadding(a3, 0, 0, a2);
            layoutParams2.width = t.a(getContext(), (int) (((double) this.f.x) * 0.55d));
        } else if (i == 3) {
            this.f6121a.setTextSize(17.0f);
            this.f6121a.setPadding(a3, 0, 0, a2);
            layoutParams2.width = t.a(getContext(), (int) (((double) this.f.x) * 0.65d));
        } else if (i == 4 || i == 5) {
            this.f6121a.setTextSize(22.0f);
            this.f6121a.setPadding(a3, 0, 0, a4);
        }
        this.f6121a.setSingleLine(true);
        this.f6121a.setEllipsize(TextUtils.TruncateAt.END);
        t.a(this.f6121a, AdsCommonMetaData.a().s());
        this.b = new TextView(context);
        this.b.setId(3);
        RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-1, -2);
        layoutParams3.addRule(b.a(17), 1);
        layoutParams3.addRule(3, 2);
        layoutParams3.setMargins(0, 0, 0, a4);
        this.b.setLayoutParams(layoutParams3);
        this.b.setTextColor(AdsCommonMetaData.a().u().intValue());
        this.b.setTextSize(18.0f);
        this.b.setMaxLines(2);
        this.b.setLines(2);
        this.b.setSingleLine(false);
        this.b.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        this.b.setHorizontallyScrolling(true);
        this.b.setPadding(a3, 0, 0, 0);
        this.d = new RatingBar(getContext());
        this.d.setId(5);
        RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
        int i2 = AnonymousClass2.f6123a[b2.ordinal()];
        if (i2 == 1 || i2 == 2 || i2 == 3) {
            layoutParams4.addRule(b.a(17), 1);
            layoutParams4.addRule(8, 1);
        } else if (i2 == 4 || i2 == 5) {
            layoutParams4.addRule(b.a(17), 2);
            layoutParams3.width = t.a(getContext(), (int) (((double) this.f.x) * 0.6d));
        }
        layoutParams4.setMargins(a3, a6, a3, 0);
        this.d.setLayoutParams(layoutParams4);
        this.e = new TextView(context);
        RelativeLayout.LayoutParams layoutParams5 = new RelativeLayout.LayoutParams(-2, -2);
        int i3 = AnonymousClass2.f6123a[b2.ordinal()];
        if (i3 == 1 || i3 == 2 || i3 == 3) {
            this.e.setTextSize(13.0f);
            layoutParams5.addRule(b.a(17), 2);
            layoutParams5.addRule(15);
        } else if (i3 == 4) {
            layoutParams5.addRule(b.a(17), 3);
            layoutParams5.addRule(15);
            layoutParams5.setMargins(a7, 0, 0, 0);
            this.e.setTextSize(26.0f);
        } else if (i3 == 5) {
            layoutParams5.addRule(b.a(17), 3);
            layoutParams5.addRule(15);
            layoutParams5.setMargins(a7 * 7, 0, 0, 0);
            this.e.setTextSize(26.0f);
        }
        this.e.setPadding(a5, a5, a5, a5);
        this.e.setLayoutParams(layoutParams5);
        setButtonText(false);
        this.e.setTextColor(-1);
        this.e.setTypeface((Typeface) null, 1);
        this.e.setId(4);
        this.e.setShadowLayer(2.5f, -3.0f, 3.0f, -9013642);
        this.e.setBackgroundDrawable(new ShapeDrawable(new RoundRectShape(new float[]{10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f, 10.0f}, (RectF) null, (float[]) null)) {
            /* access modifiers changed from: protected */
            public final void onDraw(Shape shape, Canvas canvas, Paint paint) {
                paint.setColor(-11363070);
                paint.setMaskFilter(new EmbossMaskFilter(new float[]{1.0f, 1.0f, 1.0f}, 0.4f, 5.0f, 3.0f));
                super.onDraw(shape, canvas, paint);
            }
        });
        addView(this.c);
        addView(this.f6121a);
        int i4 = AnonymousClass2.f6123a[b2.ordinal()];
        if (i4 == 1 || i4 == 2 || i4 == 3) {
            addView(this.e);
        } else if (i4 == 4 || i4 == 5) {
            addView(this.e);
            addView(this.b);
        }
        addView(this.d);
    }

    private Template b() {
        Template template = Template.S;
        if (this.f.x > Banner3DSize.Size.SMALL.getSize().a() || this.f.y > Banner3DSize.Size.SMALL.getSize().b()) {
            template = Template.M;
        }
        if (this.f.x > Banner3DSize.Size.MEDIUM.getSize().a() || this.f.y > Banner3DSize.Size.MEDIUM.getSize().b()) {
            template = Template.L;
        }
        return (this.f.x > Banner3DSize.Size.LARGE.getSize().a() || this.f.y > Banner3DSize.Size.LARGE.getSize().b()) ? Template.XL : template;
    }

    public void setButtonText(boolean z) {
        if (z) {
            this.e.setText("OPEN");
        } else {
            this.e.setText("DOWNLOAD");
        }
    }

    public void setDescription(String str) {
        if (str != null) {
            String str2 = "";
            if (str.compareTo(str2) != 0) {
                String[] a2 = a(str);
                String str3 = a2[0];
                if (a2[1] != null) {
                    str2 = a(a2[1])[0];
                }
                if (str.length() >= 110) {
                    str2 = str2 + "...";
                }
                this.b.setText(str3 + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE + str2);
            }
        }
    }

    public void setImage(Bitmap bitmap) {
        this.c.setImageBitmap(bitmap);
    }

    public void setRating(float f2) {
        try {
            this.d.setRating(f2);
        } catch (NullPointerException unused) {
        }
    }

    public void setText(String str) {
        this.f6121a.setText(str);
    }

    public void setImage(int i, int i2, int i3) {
        this.c.setImageResource(i);
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = i2;
        layoutParams.height = i3;
        this.c.setLayoutParams(layoutParams);
    }

    public Banner3DView(Context context, Point point) {
        super(context);
        this.f = point;
        a();
    }

    public Banner3DView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        a();
    }

    public void setImage(Bitmap bitmap, int i, int i2) {
        this.c.setImageBitmap(bitmap);
        ViewGroup.LayoutParams layoutParams = this.c.getLayoutParams();
        layoutParams.width = i;
        layoutParams.height = i2;
        this.c.setLayoutParams(layoutParams);
    }

    public Banner3DView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a();
    }

    private static String[] a(String str) {
        boolean z;
        String[] strArr = new String[2];
        int i = 55;
        if (str.length() > 55) {
            char[] charArray = str.substring(0, 55).toCharArray();
            int length = charArray.length - 1;
            int i2 = length - 1;
            while (true) {
                if (i2 <= 0) {
                    z = false;
                    break;
                } else if (charArray[i2] == ' ') {
                    length = i2;
                    z = true;
                    break;
                } else {
                    i2--;
                }
            }
            if (z) {
                i = length;
            }
            strArr[0] = str.substring(0, i);
            strArr[1] = str.substring(i + 1, str.length());
        } else {
            strArr[0] = str;
            strArr[1] = null;
        }
        return strArr;
    }
}
