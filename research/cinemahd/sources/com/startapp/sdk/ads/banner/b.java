package com.startapp.sdk.ads.banner;

import android.content.Context;
import android.util.AttributeSet;

final class b {

    /* renamed from: a  reason: collision with root package name */
    private Context f6119a;
    private String b;

    b(Context context, AttributeSet attributeSet) {
        this.f6119a = context;
        this.b = a(attributeSet, "adTag");
    }

    private String a(AttributeSet attributeSet, String str) {
        try {
            int attributeResourceValue = attributeSet.getAttributeResourceValue((String) null, str, -1);
            if (attributeResourceValue != -1) {
                return this.f6119a.getResources().getString(attributeResourceValue);
            }
            return attributeSet.getAttributeValue((String) null, str);
        } catch (Exception unused) {
            return null;
        }
    }

    public final String a() {
        return this.b;
    }
}
