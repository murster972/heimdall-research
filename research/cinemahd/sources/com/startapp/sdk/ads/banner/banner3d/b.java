package com.startapp.sdk.ads.banner.banner3d;

import android.content.Context;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.json.a;

public final class b extends a {
    private int g = 0;

    public b(Context context, Banner3DAd banner3DAd, int i, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        super(context, banner3DAd, adPreferences, bVar, AdPreferences.Placement.INAPP_BANNER);
        this.g = i;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        com.startapp.sdk.ads.banner.a aVar = new com.startapp.sdk.ads.banner.a();
        b((GetAdRequest) aVar);
        aVar.e(BannerMetaData.a().b().f());
        aVar.f(this.g);
        aVar.a(((Banner3DAd) this.b).b_());
        aVar.a(this.f6335a);
        return aVar;
    }

    /* access modifiers changed from: protected */
    public final void a(Ad ad) {
    }
}
