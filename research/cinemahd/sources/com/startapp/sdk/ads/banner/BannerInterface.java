package com.startapp.sdk.ads.banner;

public interface BannerInterface {
    void hideBanner();

    void setBannerListener(BannerListener bannerListener);

    void showBanner();
}
