package com.startapp.sdk.ads.banner.banner3d;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Camera;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.banner.BannerBase;
import com.startapp.sdk.ads.banner.BannerInterface;
import com.startapp.sdk.ads.banner.BannerListener;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.banner.BannerOptions;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.JsonAd;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.h;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.c.c;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Banner3D extends BannerBase implements BannerInterface, AdEventListener {
    private boolean A;
    private boolean B;
    private boolean C;
    private AdInformationOverrides D;
    private int E;
    private BannerListener F;
    private Runnable G;
    protected BannerOptions g;
    protected List<AdDetails> h;
    protected AdPreferences i;
    protected float j;
    protected boolean k;
    protected boolean l;
    protected boolean m;
    protected boolean n;
    protected boolean o;
    protected boolean p;
    protected boolean q;
    protected boolean r;
    protected List<a> s;
    private Banner3DAd t;
    private Camera u;
    private Matrix v;
    private Paint w;
    private float x;
    private boolean y;
    private boolean z;

    private static class SavedState extends View.BaseSavedState {
        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            public final SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            public final SavedState[] newArray(int i) {
                return new SavedState[i];
            }
        };
        public AdRulesResult adRulesResult;
        public boolean bDefaultLoad;
        public boolean bIsVisible;
        private int currentImage;
        private AdDetails[] details;
        public a[] faces;
        private int firstRotation;
        private int firstRotationFinished;
        public boolean loaded;
        public boolean loading;
        public BannerOptions options;
        public AdInformationOverrides overrides;
        private float rotation;

        public int describeContents() {
            return 0;
        }

        public int getCurrentImage() {
            return this.currentImage;
        }

        public List<AdDetails> getDetails() {
            return Arrays.asList(this.details);
        }

        public float getRotation() {
            return this.rotation;
        }

        public boolean isFirstRotation() {
            return this.firstRotation == 1;
        }

        public boolean isFirstRotationFinished() {
            return this.firstRotationFinished == 1;
        }

        public void setCurrentImage(int i) {
            this.currentImage = i;
        }

        public void setDetails(List<AdDetails> list) {
            this.details = new AdDetails[list.size()];
            for (int i = 0; i < list.size(); i++) {
                this.details[i] = list.get(i);
            }
        }

        public void setFirstRotation(boolean z) {
            this.firstRotation = z ? 1 : 0;
        }

        public void setFirstRotationFinished(boolean z) {
            this.firstRotationFinished = z ? 1 : 0;
        }

        public void setRotation(float f) {
            this.rotation = f;
        }

        public void writeToParcel(Parcel parcel, int i) {
            super.writeToParcel(parcel, i);
            int i2 = 0;
            if (!this.bIsVisible) {
                parcel.writeInt(0);
                return;
            }
            parcel.writeInt(1);
            parcel.writeInt(this.currentImage);
            parcel.writeFloat(this.rotation);
            parcel.writeInt(this.firstRotation);
            parcel.writeInt(this.firstRotationFinished);
            parcel.writeParcelableArray(this.details, i);
            parcel.writeInt(this.loaded ? 1 : 0);
            parcel.writeInt(this.loading ? 1 : 0);
            parcel.writeInt(this.bDefaultLoad ? 1 : 0);
            parcel.writeInt(this.faces.length);
            while (true) {
                a[] aVarArr = this.faces;
                if (i2 < aVarArr.length) {
                    parcel.writeParcelable(aVarArr[i2], i);
                    i2++;
                } else {
                    parcel.writeSerializable(this.overrides);
                    parcel.writeSerializable(this.options);
                    parcel.writeSerializable(this.adRulesResult);
                    return;
                }
            }
        }

        SavedState(Parcelable parcelable) {
            super(parcelable);
        }

        private SavedState(Parcel parcel) {
            super(parcel);
            if (parcel.readInt() == 1) {
                this.bIsVisible = true;
                this.currentImage = parcel.readInt();
                this.rotation = parcel.readFloat();
                this.firstRotation = parcel.readInt();
                this.firstRotationFinished = parcel.readInt();
                Parcelable[] readParcelableArray = parcel.readParcelableArray(AdDetails.class.getClassLoader());
                if (readParcelableArray != null) {
                    this.details = new AdDetails[readParcelableArray.length];
                    System.arraycopy(readParcelableArray, 0, this.details, 0, readParcelableArray.length);
                }
                int readInt = parcel.readInt();
                this.loaded = false;
                if (readInt == 1) {
                    this.loaded = true;
                }
                int readInt2 = parcel.readInt();
                this.loading = false;
                if (readInt2 == 1) {
                    this.loading = true;
                }
                int readInt3 = parcel.readInt();
                this.bDefaultLoad = false;
                if (readInt3 == 1) {
                    this.bDefaultLoad = true;
                }
                int readInt4 = parcel.readInt();
                if (readInt4 > 0) {
                    this.faces = new a[readInt4];
                    for (int i = 0; i < readInt4; i++) {
                        this.faces[i] = (a) parcel.readParcelable(a.class.getClassLoader());
                    }
                }
                this.overrides = (AdInformationOverrides) parcel.readSerializable();
                this.options = (BannerOptions) parcel.readSerializable();
                this.adRulesResult = (AdRulesResult) parcel.readSerializable();
                return;
            }
            this.bIsVisible = false;
        }
    }

    public Banner3D(Activity activity) {
        this((Context) activity);
    }

    private int A() {
        return (getHeight() - C()) / 2;
    }

    private int B() {
        return (getWidth() - D()) / 2;
    }

    private int C() {
        return (int) (((float) t.a(getContext(), this.g.e())) * this.g.k());
    }

    private int D() {
        return (int) (((float) t.a(getContext(), this.g.d())) * this.g.j());
    }

    private void E() {
        List<a> list = this.s;
        if (list != null && !list.isEmpty()) {
            for (a next : this.s) {
                if (next != null) {
                    next.c();
                }
            }
        }
    }

    private void F() {
        if (this.r && this.c) {
            removeCallbacks(this.G);
            post(this.G);
        }
    }

    private void v() {
        setVisibility(0);
        if (this.t != null) {
            c.a(getContext()).h().a(AdPreferences.Placement.INAPP_BANNER, this.t.getAdId());
        }
    }

    private void w() {
        for (a a2 : this.s) {
            a2.a(getContext(), this.g, this);
        }
    }

    private boolean x() {
        List<a> list = this.s;
        return list == null || list.size() == 0;
    }

    private int y() {
        return this.s.size();
    }

    private void z() {
        this.E = ((this.E - 1) + y()) % y();
    }

    /* access modifiers changed from: protected */
    public final int c() {
        return 50;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        return "StartApp Banner3D";
    }

    /* access modifiers changed from: protected */
    public final void e() {
        this.q = false;
        this.B = true;
        this.z = false;
        this.k = true;
        this.m = true;
        this.n = false;
        this.o = false;
        this.c = false;
        this.f6113a = null;
        E();
        this.s = new ArrayList();
        this.t = new Banner3DAd(getContext(), g());
        if (this.i == null) {
            this.i = new AdPreferences();
        }
        this.t.load(this.i, this);
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return BannerMetaData.a().b().h();
    }

    /* access modifiers changed from: protected */
    public final int g() {
        Banner3DAd banner3DAd = this.t;
        if (banner3DAd == null) {
            return 0;
        }
        return banner3DAd.a();
    }

    /* access modifiers changed from: protected */
    public final int h() {
        return this.d;
    }

    public void hideBanner() {
        this.p = false;
        setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.r = true;
        BannerOptions bannerOptions = this.g;
        if (bannerOptions == null || !bannerOptions.o()) {
            this.m = false;
            this.n = true;
        }
        F();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.r = false;
        removeCallbacks(this.G);
        List<a> list = this.s;
        if (list != null) {
            for (a b : list) {
                b.b();
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!this.c && !this.B) {
            this.c = true;
            F();
        }
        if (!isInEditMode() && this.p && !x()) {
            try {
                int D2 = D();
                int C2 = C();
                int B2 = B();
                int A2 = A();
                float l2 = this.g.l() + (((float) Math.pow((double) (Math.abs(this.j - 45.0f) / 45.0f), (double) this.g.m())) * (1.0f - this.g.l()));
                if (!this.n) {
                    l2 = this.g.l();
                }
                float f = l2;
                Bitmap a2 = this.s.get(((this.E - 1) + this.s.size()) % this.s.size()).a();
                Bitmap a3 = this.s.get(this.E).a();
                if (a3 != null && a2 != null) {
                    if (this.j < 45.0f) {
                        if (this.j > 3.0f) {
                            Canvas canvas2 = canvas;
                            Bitmap bitmap = a3;
                            int i2 = A2;
                            int i3 = B2;
                            a(canvas2, bitmap, i2, i3, D2 / 2, C2 / 2, f, (this.j - 90.0f) * ((float) this.g.n().getRotationMultiplier()));
                        }
                        Canvas canvas3 = canvas;
                        Bitmap bitmap2 = a2;
                        int i4 = A2;
                        int i5 = B2;
                        a(canvas3, bitmap2, i4, i5, D2 / 2, C2 / 2, f, this.j * ((float) this.g.n().getRotationMultiplier()));
                        return;
                    }
                    if (this.j < 87.0f) {
                        Canvas canvas4 = canvas;
                        Bitmap bitmap3 = a2;
                        int i6 = A2;
                        int i7 = B2;
                        a(canvas4, bitmap3, i6, i7, D2 / 2, C2 / 2, f, this.j * ((float) this.g.n().getRotationMultiplier()));
                    }
                    Canvas canvas5 = canvas;
                    Bitmap bitmap4 = a3;
                    int i8 = A2;
                    int i9 = B2;
                    a(canvas5, bitmap4, i8, i9, D2 / 2, C2 / 2, f, (this.j - 90.0f) * ((float) this.g.n().getRotationMultiplier()));
                    if (!this.m) {
                        this.n = true;
                    }
                }
            } catch (Exception e) {
                new e((Throwable) e).a(getContext());
            }
        }
    }

    public void onFailedToReceiveAd(Ad ad) {
        setErrorMessage(ad.getErrorMessage());
        BannerListener bannerListener = this.F;
        if (bannerListener != null) {
            bannerListener.onFailedToReceiveAd(this);
        }
    }

    public void onReceiveAd(Ad ad) {
        this.q = true;
        this.B = false;
        this.D = this.t.getAdInfoOverride();
        this.h = ((JsonAd) ad).g();
        a(this.h, this.C);
        this.C = false;
    }

    /* access modifiers changed from: protected */
    public void onRestoreInstanceState(Parcelable parcelable) {
        if (!(parcelable instanceof SavedState)) {
            super.onRestoreInstanceState(parcelable);
            return;
        }
        SavedState savedState = (SavedState) parcelable;
        super.onRestoreInstanceState(savedState.getSuperState());
        this.p = savedState.bIsVisible;
        if (this.p) {
            this.h = savedState.getDetails();
            this.j = savedState.getRotation();
            this.m = savedState.isFirstRotation();
            this.n = savedState.isFirstRotationFinished();
            this.E = savedState.getCurrentImage();
            a[] aVarArr = savedState.faces;
            E();
            this.s = new ArrayList();
            if (aVarArr != null) {
                for (a add : aVarArr) {
                    this.s.add(add);
                }
            }
            this.q = savedState.loaded;
            this.B = savedState.loading;
            this.A = savedState.bDefaultLoad;
            this.D = savedState.overrides;
            this.g = savedState.options;
            if (this.h.size() == 0) {
                this.A = true;
                a();
                return;
            }
            post(new Runnable() {
                public void run() {
                    Banner3D banner3D = Banner3D.this;
                    banner3D.a(banner3D.h, false);
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public Parcelable onSaveInstanceState() {
        SavedState savedState = new SavedState(super.onSaveInstanceState());
        savedState.bIsVisible = this.p;
        savedState.setDetails(this.h);
        savedState.setRotation(this.j);
        savedState.setFirstRotation(this.m);
        savedState.setFirstRotationFinished(this.n);
        savedState.setCurrentImage(this.E);
        savedState.options = this.g;
        savedState.faces = new a[this.s.size()];
        savedState.loaded = this.q;
        savedState.loading = this.B;
        savedState.overrides = this.D;
        for (int i2 = 0; i2 < this.s.size(); i2++) {
            savedState.faces[i2] = this.s.get(i2);
        }
        return savedState;
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        List<a> list;
        int D2 = D();
        int C2 = C();
        int B2 = B();
        int A2 = A();
        if (!(motionEvent.getX() >= ((float) B2) && motionEvent.getY() >= ((float) A2) && motionEvent.getX() <= ((float) (B2 + D2)) && motionEvent.getY() <= ((float) (A2 + C2))) || (list = this.s) == null || list.size() == 0) {
            return false;
        }
        int action = motionEvent.getAction();
        if (action == 0) {
            this.y = true;
            this.x = motionEvent.getY();
        } else if (action != 1) {
            if (action == 2 && this.x - motionEvent.getY() >= 10.0f) {
                this.y = false;
                this.x = motionEvent.getY();
            }
        } else if (this.y) {
            if (this.j < 45.0f) {
                z();
            }
            this.y = false;
            this.k = false;
            setClicked(true);
            postDelayed(new Runnable() {
                public void run() {
                    Banner3D.this.k = true;
                }
            }, AdsCommonMetaData.a().B());
            this.s.get(this.E).b(getContext());
            BannerListener bannerListener = this.F;
            if (bannerListener != null) {
                bannerListener.onClick(this);
            }
        }
        return true;
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
        if (z2) {
            this.r = true;
            BannerOptions bannerOptions = this.g;
            if (bannerOptions == null || !bannerOptions.o()) {
                this.m = false;
                this.n = true;
            }
            F();
            return;
        }
        this.r = false;
        if (!this.l) {
            removeCallbacks(this.G);
        }
    }

    /* access modifiers changed from: protected */
    public final BannerOptions s() {
        return this.g;
    }

    public void setAdTag(String str) {
        this.f = str;
    }

    public void setBannerListener(BannerListener bannerListener) {
        this.F = bannerListener;
    }

    public void showBanner() {
        this.p = true;
        v();
    }

    /* access modifiers changed from: protected */
    public final int t() {
        return this.E;
    }

    /* access modifiers changed from: protected */
    public final int u() {
        return (this.E + 1) % y();
    }

    public Banner3D(Activity activity, AdPreferences adPreferences) {
        this((Context) activity, adPreferences);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (!this.B) {
            this.g = BannerMetaData.a().c();
            this.h = new ArrayList();
            if (this.i == null) {
                this.i = new AdPreferences();
            }
            this.D = AdInformationOverrides.a();
            E();
            this.s = new ArrayList();
            this.B = true;
            setBackgroundColor(0);
            if (getId() == -1) {
                setId(this.d);
            }
            if (this.A) {
                getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    public void onGlobalLayout() {
                        b.a(Banner3D.this.getViewTreeObserver(), (ViewTreeObserver.OnGlobalLayoutListener) this);
                        Banner3D banner3D = Banner3D.this;
                        banner3D.a(banner3D.i);
                        Banner3D banner3D2 = Banner3D.this;
                        if (!banner3D2.q) {
                            banner3D2.k();
                        }
                    }
                });
            }
        }
    }

    public Banner3D(Activity activity, BannerListener bannerListener) {
        this((Context) activity, bannerListener);
    }

    /* access modifiers changed from: protected */
    public final void a(List<AdDetails> list, boolean z2) {
        this.h = list;
        if (list != null) {
            com.startapp.sdk.ads.banner.c cVar = new com.startapp.sdk.ads.banner.c();
            if (Banner3DSize.a(getContext(), getParent(), this.g, this, cVar)) {
                setMinimumWidth(t.a(getContext(), this.g.d()));
                setMinimumHeight(t.a(getContext(), this.g.e()));
                if (getLayoutParams() != null && getLayoutParams().width == -1) {
                    setMinimumWidth(t.a(getContext(), cVar.a()));
                }
                if (getLayoutParams() != null && getLayoutParams().height == -1) {
                    setMinimumHeight(t.a(getContext(), cVar.b()));
                }
                if (getLayoutParams() != null) {
                    if (getLayoutParams().width > 0) {
                        setMinimumWidth(getLayoutParams().width);
                    }
                    if (getLayoutParams().height > 0) {
                        setMinimumHeight(getLayoutParams().height);
                    }
                    if (getLayoutParams().width > 0 && getLayoutParams().height > 0) {
                        this.t.a_();
                    }
                }
                if (x()) {
                    E();
                    removeAllViews();
                    this.s = new ArrayList();
                    for (AdDetails aVar : list) {
                        this.s.add(new a(getContext(), this, aVar, this.g, new TrackingParams(j())));
                    }
                    this.E = 0;
                } else {
                    w();
                }
                RelativeLayout relativeLayout = new RelativeLayout(getContext());
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(D(), C());
                layoutParams.addRule(13);
                int B2 = B();
                layoutParams.rightMargin = B2;
                layoutParams.leftMargin = B2;
                int A2 = A();
                layoutParams.topMargin = A2;
                layoutParams.bottomMargin = A2;
                addView(relativeLayout, layoutParams);
                new AdInformationObject(getContext(), AdInformationObject.Size.SMALL, AdPreferences.Placement.INAPP_BANNER, this.D).a(relativeLayout);
                if (this.w == null) {
                    this.w = new Paint();
                    this.w.setAntiAlias(true);
                    this.w.setFilterBitmap(true);
                }
                if (!this.z) {
                    this.z = true;
                    F();
                }
                if (this.p) {
                    v();
                }
                BannerListener bannerListener = this.F;
                if (bannerListener != null && z2) {
                    bannerListener.onReceiveAd(this);
                    return;
                }
                return;
            }
            setErrorMessage("Error in banner screen size");
            setVisibility(8);
            BannerListener bannerListener2 = this.F;
            if (bannerListener2 != null && z2) {
                bannerListener2.onFailedToReceiveAd(this);
                return;
            }
            return;
        }
        setErrorMessage("No ads to load");
        BannerListener bannerListener3 = this.F;
        if (bannerListener3 != null && z2) {
            bannerListener3.onFailedToReceiveAd(this);
        }
    }

    public Banner3D(Activity activity, AdPreferences adPreferences, BannerListener bannerListener) {
        this((Context) activity, adPreferences, bannerListener);
    }

    public Banner3D(Activity activity, boolean z2) {
        this((Context) activity, z2);
    }

    public Banner3D(Activity activity, boolean z2, AdPreferences adPreferences) {
        this((Context) activity, z2, adPreferences);
    }

    public Banner3D(Activity activity, AttributeSet attributeSet) {
        this((Context) activity, attributeSet);
    }

    public Banner3D(Activity activity, AttributeSet attributeSet, int i2) {
        this((Context) activity, attributeSet, i2);
    }

    @Deprecated
    public Banner3D(Context context) {
        this(context, true, (AdPreferences) null);
    }

    @Deprecated
    public Banner3D(Context context, AdPreferences adPreferences) {
        this(context, true, adPreferences);
    }

    @Deprecated
    public Banner3D(Context context, BannerListener bannerListener) {
        this(context, true, (AdPreferences) null);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public Banner3D(Context context, AdPreferences adPreferences, BannerListener bannerListener) {
        this(context, true, adPreferences);
        setBannerListener(bannerListener);
    }

    @Deprecated
    public Banner3D(Context context, boolean z2) {
        this(context, z2, (AdPreferences) null);
    }

    @Deprecated
    public Banner3D(Context context, boolean z2, AdPreferences adPreferences) {
        super(context);
        this.u = null;
        this.v = null;
        this.w = null;
        this.j = 45.0f;
        this.x = 0.0f;
        this.k = true;
        this.l = false;
        this.m = true;
        this.n = false;
        this.o = false;
        this.y = false;
        this.z = false;
        this.p = true;
        this.A = true;
        this.q = false;
        this.B = false;
        this.r = false;
        this.C = true;
        this.s = new ArrayList();
        this.E = 0;
        this.G = new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:33:0x00c1  */
            /* JADX WARNING: Removed duplicated region for block: B:36:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.q
                    if (r1 == 0) goto L_0x00c5
                    java.util.List<com.startapp.sdk.ads.banner.banner3d.a> r0 = r0.s
                    int r0 = r0.size()
                    if (r0 != 0) goto L_0x0010
                    goto L_0x00c5
                L_0x0010:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.p
                    r2 = 1
                    if (r1 == 0) goto L_0x0043
                    boolean r0 = r0.isShown()
                    if (r0 == 0) goto L_0x0043
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r0 = r0.c
                    if (r0 == 0) goto L_0x0043
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    java.util.List<com.startapp.sdk.ads.banner.banner3d.a> r1 = r0.s
                    int r0 = r0.t()
                    java.lang.Object r0 = r1.get(r0)
                    com.startapp.sdk.ads.banner.banner3d.a r0 = (com.startapp.sdk.ads.banner.banner3d.a) r0
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r1 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    com.startapp.sdk.ads.banner.banner3d.Banner3D.a((com.startapp.sdk.ads.banner.banner3d.Banner3D) r1, (com.startapp.sdk.ads.banner.banner3d.a) r0)
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.o
                    if (r1 != 0) goto L_0x0043
                    r0.o = r2
                    r0.r()
                L_0x0043:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.k
                    if (r1 == 0) goto L_0x0065
                    com.startapp.sdk.ads.banner.BannerOptions r1 = r0.s()
                    int r1 = r1.b()
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r3 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r4 = r3.n
                    if (r4 != 0) goto L_0x005e
                    com.startapp.sdk.ads.banner.BannerOptions r3 = r3.g
                    int r3 = r3.p()
                    goto L_0x005f
                L_0x005e:
                    r3 = 1
                L_0x005f:
                    int r1 = r1 * r3
                    float r1 = (float) r1
                    r0.a((float) r1)
                L_0x0065:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    float r1 = r0.j
                    com.startapp.sdk.ads.banner.BannerOptions r0 = r0.s()
                    int r0 = r0.b()
                    int r0 = 90 - r0
                    float r0 = (float) r0
                    r3 = 0
                    int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
                    if (r0 <= 0) goto L_0x00a7
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    float r1 = r0.j
                    com.startapp.sdk.ads.banner.BannerOptions r0 = r0.s()
                    int r0 = r0.b()
                    int r0 = r0 + 90
                    float r0 = (float) r0
                    int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
                    if (r0 >= 0) goto L_0x00a7
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.m
                    if (r1 != 0) goto L_0x00a7
                    boolean r1 = r0.r
                    if (r1 == 0) goto L_0x00a2
                    com.startapp.sdk.ads.banner.BannerOptions r1 = r0.s()
                    int r1 = r1.c()
                    long r1 = (long) r1
                    r0.postDelayed(r6, r1)
                L_0x00a2:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    r0.l = r3
                    goto L_0x00b9
                L_0x00a7:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    com.startapp.sdk.ads.banner.BannerOptions r1 = r0.s()
                    int r1 = r1.a()
                    long r4 = (long) r1
                    r0.postDelayed(r6, r4)
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    r0.l = r2
                L_0x00b9:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    int r0 = r0.u()
                    if (r0 != 0) goto L_0x00c5
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    r0.m = r3
                L_0x00c5:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.banner.banner3d.Banner3D.AnonymousClass1.run():void");
            }
        };
        try {
            this.A = z2;
            if (adPreferences == null) {
                this.i = new AdPreferences();
            } else {
                this.i = adPreferences;
            }
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    @Deprecated
    public Banner3D(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    @Deprecated
    public Banner3D(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        this.u = null;
        this.v = null;
        this.w = null;
        this.j = 45.0f;
        this.x = 0.0f;
        this.k = true;
        this.l = false;
        this.m = true;
        this.n = false;
        this.o = false;
        this.y = false;
        this.z = false;
        this.p = true;
        this.A = true;
        this.q = false;
        this.B = false;
        this.r = false;
        this.C = true;
        this.s = new ArrayList();
        this.E = 0;
        this.G = new Runnable() {
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r6 = this;
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.q
                    if (r1 == 0) goto L_0x00c5
                    java.util.List<com.startapp.sdk.ads.banner.banner3d.a> r0 = r0.s
                    int r0 = r0.size()
                    if (r0 != 0) goto L_0x0010
                    goto L_0x00c5
                L_0x0010:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.p
                    r2 = 1
                    if (r1 == 0) goto L_0x0043
                    boolean r0 = r0.isShown()
                    if (r0 == 0) goto L_0x0043
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r0 = r0.c
                    if (r0 == 0) goto L_0x0043
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    java.util.List<com.startapp.sdk.ads.banner.banner3d.a> r1 = r0.s
                    int r0 = r0.t()
                    java.lang.Object r0 = r1.get(r0)
                    com.startapp.sdk.ads.banner.banner3d.a r0 = (com.startapp.sdk.ads.banner.banner3d.a) r0
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r1 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    com.startapp.sdk.ads.banner.banner3d.Banner3D.a((com.startapp.sdk.ads.banner.banner3d.Banner3D) r1, (com.startapp.sdk.ads.banner.banner3d.a) r0)
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.o
                    if (r1 != 0) goto L_0x0043
                    r0.o = r2
                    r0.r()
                L_0x0043:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.k
                    if (r1 == 0) goto L_0x0065
                    com.startapp.sdk.ads.banner.BannerOptions r1 = r0.s()
                    int r1 = r1.b()
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r3 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r4 = r3.n
                    if (r4 != 0) goto L_0x005e
                    com.startapp.sdk.ads.banner.BannerOptions r3 = r3.g
                    int r3 = r3.p()
                    goto L_0x005f
                L_0x005e:
                    r3 = 1
                L_0x005f:
                    int r1 = r1 * r3
                    float r1 = (float) r1
                    r0.a((float) r1)
                L_0x0065:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    float r1 = r0.j
                    com.startapp.sdk.ads.banner.BannerOptions r0 = r0.s()
                    int r0 = r0.b()
                    int r0 = 90 - r0
                    float r0 = (float) r0
                    r3 = 0
                    int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
                    if (r0 <= 0) goto L_0x00a7
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    float r1 = r0.j
                    com.startapp.sdk.ads.banner.BannerOptions r0 = r0.s()
                    int r0 = r0.b()
                    int r0 = r0 + 90
                    float r0 = (float) r0
                    int r0 = (r1 > r0 ? 1 : (r1 == r0 ? 0 : -1))
                    if (r0 >= 0) goto L_0x00a7
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    boolean r1 = r0.m
                    if (r1 != 0) goto L_0x00a7
                    boolean r1 = r0.r
                    if (r1 == 0) goto L_0x00a2
                    com.startapp.sdk.ads.banner.BannerOptions r1 = r0.s()
                    int r1 = r1.c()
                    long r1 = (long) r1
                    r0.postDelayed(r6, r1)
                L_0x00a2:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    r0.l = r3
                    goto L_0x00b9
                L_0x00a7:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    com.startapp.sdk.ads.banner.BannerOptions r1 = r0.s()
                    int r1 = r1.a()
                    long r4 = (long) r1
                    r0.postDelayed(r6, r4)
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    r0.l = r2
                L_0x00b9:
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    int r0 = r0.u()
                    if (r0 != 0) goto L_0x00c5
                    com.startapp.sdk.ads.banner.banner3d.Banner3D r0 = com.startapp.sdk.ads.banner.banner3d.Banner3D.this
                    r0.m = r3
                L_0x00c5:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.banner.banner3d.Banner3D.AnonymousClass1.run():void");
            }
        };
        try {
            a();
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    private void a(Canvas canvas, Bitmap bitmap, int i2, int i3, int i4, int i5, float f, float f2) {
        if (this.u == null) {
            this.u = new Camera();
        }
        this.u.save();
        this.u.translate(0.0f, 0.0f, (float) i5);
        this.u.rotateX(f2);
        float f3 = (float) (-i5);
        this.u.translate(0.0f, 0.0f, f3);
        if (this.v == null) {
            this.v = new Matrix();
        }
        this.u.getMatrix(this.v);
        this.u.restore();
        this.v.preTranslate((float) (-i4), f3);
        this.v.postScale(f, f);
        this.v.postTranslate((float) (i3 + i4), (float) (i2 + i5));
        canvas.drawBitmap(bitmap, this.v, this.w);
    }

    /* access modifiers changed from: protected */
    public final void a(float f) {
        this.j += f;
        if (this.j >= 90.0f) {
            this.E = (this.E + 1) % y();
            this.j -= 90.0f;
        }
        if (this.j <= 0.0f) {
            z();
            this.j += 90.0f;
        }
        invalidate();
    }

    /* access modifiers changed from: protected */
    public final void a(int i2) {
        this.d = i2;
    }

    static /* synthetic */ void a(Banner3D banner3D, a aVar) {
        h a2 = aVar.a(banner3D.getContext());
        if (a2 != null) {
            banner3D.a(a2);
        }
    }
}
