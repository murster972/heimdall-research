package com.startapp.sdk.ads.banner;

import android.graphics.Point;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private Point f6130a = new Point();

    public c() {
    }

    private void a(int i) {
        this.f6130a.x = i;
    }

    private void b(int i) {
        this.f6130a.y = i;
    }

    public final void a(int i, int i2) {
        a(i);
        b(i2);
    }

    public final int b() {
        return this.f6130a.y;
    }

    public c(int i, int i2) {
        a(i, i2);
    }

    public final int a() {
        return this.f6130a.x;
    }
}
