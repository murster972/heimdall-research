package com.startapp.sdk.ads.splash;

import android.content.Context;
import android.webkit.JavascriptInterface;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f6188a = false;
    private Runnable b = null;
    private Context c;

    public b(Context context, Runnable runnable) {
        this.b = runnable;
        this.c = context;
    }

    @JavascriptInterface
    public final void closeSplash() {
        if (!this.f6188a) {
            this.f6188a = true;
            this.b.run();
        }
    }
}
