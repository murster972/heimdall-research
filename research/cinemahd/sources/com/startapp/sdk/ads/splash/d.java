package com.startapp.sdk.ads.splash;

import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.Toast;
import com.startapp.sdk.ads.a.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.io.Serializable;

public final class d extends b {
    private SplashConfig c = null;
    private SplashScreen d;
    private boolean e = false;
    private boolean f = false;

    public final void a(Bundle bundle) {
        this.c = (SplashConfig) b().getSerializableExtra("SplashConfig");
    }

    public final void q() {
    }

    public final void s() {
    }

    public final void t() {
        SplashScreen splashScreen = this.d;
        if (splashScreen != null) {
            splashScreen.b();
        }
    }

    public final void u() {
        AdPreferences adPreferences;
        if (this.c != null) {
            Serializable serializableExtra = b().getSerializableExtra("AdPreference");
            if (serializableExtra != null) {
                adPreferences = (AdPreferences) serializableExtra;
            } else {
                adPreferences = new AdPreferences();
            }
            this.e = b().getBooleanExtra("testMode", false);
            this.d = new SplashScreen(c(), this.c, adPreferences);
            this.d.a();
        }
    }

    public final void w() {
    }

    public final boolean a(int i, KeyEvent keyEvent) {
        if (this.e) {
            if (i == 25) {
                if (!this.f) {
                    this.f = true;
                    SplashScreen splashScreen = this.d;
                    splashScreen.e = true;
                    splashScreen.b.e();
                    Toast.makeText(c(), "Test Mode", 0).show();
                    return true;
                }
            } else if (i == 24 && this.f) {
                c().finish();
                return true;
            }
        }
        return i == 4;
    }
}
