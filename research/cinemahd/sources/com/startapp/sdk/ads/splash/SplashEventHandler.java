package com.startapp.sdk.ads.splash;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.startapp.common.b;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.adrules.AdaptMetaData;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;

public final class SplashEventHandler implements c {

    /* renamed from: a  reason: collision with root package name */
    Activity f6170a;
    boolean b;
    SplashState c;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;
    private SplashHtml i;
    private BroadcastReceiver j;

    enum SplashState {
        LOADING,
        RECEIVED,
        DISPLAYED,
        HIDDEN,
        DO_NOT_DISPLAY
    }

    public SplashEventHandler(Activity activity) {
        this.d = false;
        this.e = true;
        this.f = false;
        this.g = false;
        this.h = false;
        this.b = false;
        this.c = SplashState.LOADING;
        this.i = null;
        this.j = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                SplashEventHandler.this.f();
            }
        };
        this.f6170a = activity;
    }

    private void i() {
        a(this.i, (c) this);
    }

    private void j() {
        if (!this.f) {
            this.f = true;
            b.a((Context) this.f6170a).a(new Intent("com.startapp.android.splashHidden"));
        }
        if (this.j != null) {
            try {
                b.a((Context) this.f6170a).a(this.j);
            } catch (IllegalArgumentException unused) {
            }
        }
    }

    public final void a(final Runnable runnable, final CacheKey cacheKey) {
        this.d = true;
        AnonymousClass1 r0 = new com.startapp.sdk.adsbase.remoteconfig.b() {
            private Runnable d = new Runnable() {
                public final void run() {
                    AnonymousClass1 r0 = AnonymousClass1.this;
                    SplashEventHandler splashEventHandler = SplashEventHandler.this;
                    splashEventHandler.b = true;
                    if (splashEventHandler.c != SplashState.DO_NOT_DISPLAY) {
                        splashEventHandler.c(runnable, cacheKey);
                    }
                }
            };

            public final void a(MetaDataRequest.RequestReason requestReason, boolean z) {
                SplashEventHandler.this.f6170a.runOnUiThread(this.d);
            }

            public final void a() {
                SplashEventHandler.this.f6170a.runOnUiThread(this.d);
            }
        };
        if (this.c != SplashState.DO_NOT_DISPLAY) {
            synchronized (MetaData.i()) {
                if (MetaData.G().j()) {
                    r0.a((MetaDataRequest.RequestReason) null, false);
                } else {
                    MetaData.G().a((com.startapp.sdk.adsbase.remoteconfig.b) r0);
                }
            }
            return;
        }
        i();
    }

    public final void b() {
        this.c = SplashState.DO_NOT_DISPLAY;
        b((Runnable) null);
    }

    /* access modifiers changed from: package-private */
    public final void c(Runnable runnable, CacheKey cacheKey) {
        AdRulesResult a2 = AdaptMetaData.a().b().a(AdPreferences.Placement.INAPP_SPLASH, (String) null);
        if (a2.a()) {
            b(runnable);
            return;
        }
        this.c = SplashState.DO_NOT_DISPLAY;
        if (cacheKey != null) {
            a.a((Context) this.f6170a, a.a(com.startapp.sdk.adsbase.cache.a.a().b(cacheKey)), (String) null, a2.b());
        }
        i();
    }

    public final void d() {
        SplashState splashState;
        SplashState splashState2 = this.c;
        if (splashState2 != SplashState.DISPLAYED && splashState2 != (splashState = SplashState.DO_NOT_DISPLAY)) {
            this.c = splashState;
            if (this.e) {
                j();
            }
        }
    }

    public final void e() {
        this.h = true;
    }

    public final void f() {
        this.g = true;
    }

    public final void g() {
        b.a((Context) this.f6170a).a(this.j, new IntentFilter("com.startapp.android.adInfoWasClickedBroadcastListener"));
    }

    public final void h() {
        j();
        if (!this.f6170a.isFinishing()) {
            this.f6170a.finish();
        }
    }

    public final boolean b(Runnable runnable, CacheKey cacheKey) {
        if (!this.h) {
            SplashState splashState = this.c;
            if (splashState == SplashState.LOADING) {
                this.e = false;
                this.c = SplashState.DO_NOT_DISPLAY;
                i();
                return true;
            } else if (splashState == SplashState.RECEIVED) {
                this.b = true;
                c(runnable, cacheKey);
            }
        }
        return false;
    }

    public final void c() {
        this.c = SplashState.HIDDEN;
        j();
        if (!this.f6170a.isFinishing()) {
            this.f6170a.finish();
        }
    }

    public final void a() {
        this.d = true;
    }

    private void b(Runnable runnable) {
        if (!this.d) {
            return;
        }
        if (!this.b && runnable != null) {
            return;
        }
        if (this.c == SplashState.RECEIVED && runnable != null) {
            this.e = false;
            runnable.run();
        } else if (this.c != SplashState.LOADING) {
            i();
        }
    }

    public final void a(Runnable runnable) {
        if (this.c == SplashState.LOADING) {
            this.c = SplashState.RECEIVED;
        }
        b(runnable);
    }

    public SplashEventHandler(Activity activity, SplashHtml splashHtml) {
        this(activity);
        this.i = splashHtml;
    }

    public final void a(StartAppAd startAppAd) {
        if (this.c == SplashState.DISPLAYED && !this.g) {
            startAppAd.close();
            c();
        }
    }

    protected static void a(SplashHtml splashHtml, c cVar) {
        if (splashHtml == null) {
            cVar.h();
            return;
        }
        splashHtml.callback = cVar;
        splashHtml.b();
    }
}
