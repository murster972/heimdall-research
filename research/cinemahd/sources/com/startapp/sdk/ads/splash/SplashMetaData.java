package com.startapp.sdk.ads.splash;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class SplashMetaData implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static volatile SplashMetaData f6177a = new SplashMetaData();
    private static Object b = new Object();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private SplashConfig SplashConfig = new SplashConfig();
    private String splashMetadataUpdateVersion = "4.6.3";

    public static SplashMetaData b() {
        return f6177a;
    }

    public final SplashConfig a() {
        return this.SplashConfig;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && SplashMetaData.class == obj.getClass()) {
            SplashMetaData splashMetaData = (SplashMetaData) obj;
            return u.b(this.SplashConfig, splashMetaData.SplashConfig) && u.b(this.splashMetadataUpdateVersion, splashMetaData.splashMetadataUpdateVersion);
        }
    }

    public int hashCode() {
        return u.a(this.SplashConfig, this.splashMetadataUpdateVersion);
    }

    public static void a(Context context, SplashMetaData splashMetaData) {
        synchronized (b) {
            splashMetaData.splashMetadataUpdateVersion = "4.6.3";
            f6177a = splashMetaData;
            com.startapp.common.b.d.a(context, "StartappSplashMetadata", (Serializable) splashMetaData);
        }
    }

    public static void a(Context context) {
        SplashMetaData splashMetaData = (SplashMetaData) com.startapp.common.b.d.a(context, "StartappSplashMetadata");
        SplashMetaData splashMetaData2 = new SplashMetaData();
        if (splashMetaData != null) {
            boolean a2 = u.a(splashMetaData, splashMetaData2);
            if (!(!"4.6.3".equals(splashMetaData.splashMetadataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).f("metadata_null").a(context);
            }
            f6177a = splashMetaData;
            return;
        }
        f6177a = splashMetaData2;
    }
}
