package com.startapp.sdk.ads.splash;

public interface SplashHideListener {
    void splashHidden();
}
