package com.startapp.sdk.ads.splash;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.ads.splash.SplashEventHandler;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;

public final class SplashScreen {

    /* renamed from: a  reason: collision with root package name */
    Activity f6178a;
    SplashEventHandler b;
    CacheKey c;
    SplashHtml d = null;
    boolean e = false;
    SplashStartAppAd f;
    Runnable g = new Runnable() {
        public final void run() {
            SplashEventHandler.a(SplashScreen.this.d, (c) new c() {
                public final void h() {
                    SplashStartAppAd splashStartAppAd;
                    SplashScreen splashScreen = SplashScreen.this;
                    if (!splashScreen.e && (splashStartAppAd = splashScreen.f) != null) {
                        splashStartAppAd.showAd((AdDisplayListener) new AdDisplayListener() {
                            public final void adClicked(Ad ad) {
                                SplashScreen.this.b.f();
                            }

                            public final void adDisplayed(Ad ad) {
                                SplashScreen.this.b.c = SplashEventHandler.SplashState.DISPLAYED;
                            }

                            public final void adHidden(Ad ad) {
                                SplashScreen.this.b.c();
                            }

                            public final void adNotDisplayed(Ad ad) {
                            }
                        });
                        SplashScreen.this.f();
                        SplashScreen.this.f6178a.finish();
                    }
                }
            });
        }
    };
    private SplashConfig h;
    private Handler i = new Handler();
    private AdPreferences j;
    private Runnable k = new Runnable() {
        public final void run() {
            if (SplashScreen.this.c()) {
                SplashScreen.this.d();
                SplashScreen.this.e();
                return;
            }
            SplashScreen.this.f6178a.finish();
        }
    };
    private AdEventListener l = new AdEventListener() {
        public final void onFailedToReceiveAd(Ad ad) {
            SplashScreen splashScreen = SplashScreen.this;
            if (splashScreen.f != null) {
                splashScreen.b.b();
            }
        }

        public final void onReceiveAd(Ad ad) {
            SplashScreen splashScreen = SplashScreen.this;
            splashScreen.b.a(splashScreen.g);
        }
    };

    /* renamed from: com.startapp.sdk.ads.splash.SplashScreen$7  reason: invalid class name */
    static /* synthetic */ class AnonymousClass7 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6187a = new int[SplashConfig.Orientation.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.startapp.sdk.ads.splash.SplashConfig$Orientation[] r0 = com.startapp.sdk.ads.splash.SplashConfig.Orientation.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6187a = r0
                int[] r0 = f6187a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.ads.splash.SplashConfig$Orientation r1 = com.startapp.sdk.ads.splash.SplashConfig.Orientation.PORTRAIT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6187a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.ads.splash.SplashConfig$Orientation r1 = com.startapp.sdk.ads.splash.SplashConfig.Orientation.LANDSCAPE     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.splash.SplashScreen.AnonymousClass7.<clinit>():void");
        }
    }

    private static class SplashStartAppAd extends StartAppAd {
        private static final long serialVersionUID = 1;

        public SplashStartAppAd(Context context) {
            super(context);
            this.placement = AdPreferences.Placement.INAPP_SPLASH;
        }

        /* access modifiers changed from: protected */
        public final AdRulesResult a(String str, AdPreferences.Placement placement) {
            return new AdRulesResult();
        }
    }

    public SplashScreen(Activity activity, SplashConfig splashConfig, AdPreferences adPreferences) {
        this.f6178a = activity;
        this.h = splashConfig;
        this.j = adPreferences;
        try {
            this.h.a(this.f6178a);
            if (!g()) {
                this.d = this.h.b(this.f6178a);
            }
            this.b = new SplashEventHandler(activity, this.d);
        } catch (Throwable th) {
            this.b = new SplashEventHandler(activity);
            this.b.a();
            this.b.b();
            new e(th).a((Context) activity);
        }
    }

    private boolean g() {
        return !this.h.isHtmlSplash() || this.h.c();
    }

    public final void a() {
        this.b.g();
        int i2 = this.f6178a.getResources().getConfiguration().orientation;
        if (this.h.getOrientation() == SplashConfig.Orientation.AUTO) {
            if (i2 == 2) {
                this.h.setOrientation(SplashConfig.Orientation.LANDSCAPE);
            } else {
                this.h.setOrientation(SplashConfig.Orientation.PORTRAIT);
            }
        }
        int i3 = AnonymousClass7.f6187a[this.h.getOrientation().ordinal()];
        boolean z = false;
        if (i3 == 1) {
            if (i2 == 2) {
                z = true;
            }
            b.a(this.f6178a);
        } else if (i3 == 2) {
            if (i2 == 1) {
                z = true;
            }
            b.b(this.f6178a);
        }
        if (!z) {
            this.i.post(this.k);
        } else {
            this.i.postDelayed(this.k, 100);
        }
    }

    public final void b() {
        this.i.removeCallbacks(this.k);
        this.b.d();
    }

    /* access modifiers changed from: package-private */
    public final boolean c() {
        if (this.h.a((Context) this.f6178a)) {
            View view = null;
            if (g()) {
                view = this.h.b((Context) this.f6178a);
            } else {
                SplashHtml splashHtml = this.d;
                if (splashHtml != null) {
                    view = splashHtml.c();
                }
            }
            if (view == null) {
                return false;
            }
            this.f6178a.setContentView(view, new ViewGroup.LayoutParams(-1, -1));
            return true;
        }
        throw new IllegalArgumentException(this.h.getErrorMessage());
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        this.f = new SplashStartAppAd(this.f6178a.getApplicationContext());
        this.c = this.f.loadSplash(this.j, this.l);
    }

    /* access modifiers changed from: package-private */
    public final void e() {
        this.i.postDelayed(new Runnable() {
            public final void run() {
                SplashScreen splashScreen = SplashScreen.this;
                if (splashScreen.b.b(splashScreen.g, splashScreen.c)) {
                    SplashScreen splashScreen2 = SplashScreen.this;
                    splashScreen2.f = null;
                    splashScreen2.c = null;
                }
            }
        }, this.h.a().longValue());
        this.i.postDelayed(new Runnable() {
            public final void run() {
                SplashScreen splashScreen = SplashScreen.this;
                splashScreen.b.a(splashScreen.g, splashScreen.c);
            }
        }, this.h.getMinSplashTime().getIndex());
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        if (this.h.getMaxAdDisplayTime() != SplashConfig.MaxAdDisplayTime.FOR_EVER) {
            this.i.postDelayed(new Runnable() {
                public final void run() {
                    SplashScreen splashScreen = SplashScreen.this;
                    splashScreen.b.a((StartAppAd) splashScreen.f);
                }
            }, this.h.getMaxAdDisplayTime().getIndex());
        }
    }
}
