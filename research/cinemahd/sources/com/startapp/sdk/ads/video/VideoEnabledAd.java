package com.startapp.sdk.ads.video;

import android.content.Context;
import com.startapp.sdk.ads.interstitials.InterstitialAd;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;

public class VideoEnabledAd extends InterstitialAd {
    private static final long serialVersionUID = 1;
    private VideoAdDetails videoAdDetails = null;

    public VideoEnabledAd(Context context) {
        super(context, AdPreferences.Placement.INAPP_OVERLAY);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences, b bVar) {
        new b(this.f6243a, this, adPreferences, bVar).c();
    }

    public final void b(String str) {
        super.b(str);
        String a2 = u.a(str, "@videoJson@", "@videoJson@");
        if (a2 != null) {
            this.videoAdDetails = (VideoAdDetails) com.startapp.common.parser.b.a(a2, VideoAdDetails.class);
        }
    }

    public final VideoAdDetails g() {
        return this.videoAdDetails;
    }

    public final void h() {
        this.videoAdDetails = null;
    }

    /* access modifiers changed from: protected */
    public final boolean a() {
        return this.videoAdDetails != null;
    }

    public final void a(c cVar, boolean z) {
        if (cVar != null) {
            this.videoAdDetails = new VideoAdDetails(cVar, z);
            com.startapp.sdk.ads.video.vast.model.a.b f = cVar.f();
            if (f == null) {
                return;
            }
            if (f.d().intValue() > f.e().intValue()) {
                a(SplashConfig.Orientation.LANDSCAPE);
            } else {
                a(SplashConfig.Orientation.PORTRAIT);
            }
        }
    }
}
