package com.startapp.sdk.ads.video;

import android.content.Context;
import android.util.Base64;
import com.startapp.common.b.d;
import com.startapp.sdk.ads.video.c;
import com.startapp.sdk.ads.video.g;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.cache.CachedVideoAd;
import com.startapp.sdk.adsbase.cache.a;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Iterator;
import java.util.LinkedList;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static e f6218a = new e();
    private LinkedList<CachedVideoAd> b = new LinkedList<>();

    private e() {
    }

    private boolean a(int i) {
        boolean z;
        Iterator it2 = this.b.iterator();
        boolean z2 = false;
        while (it2.hasNext() && this.b.size() > i) {
            CachedVideoAd cachedVideoAd = (CachedVideoAd) it2.next();
            String a2 = cachedVideoAd.a();
            Iterator<com.startapp.sdk.adsbase.cache.e> it3 = a.a().c().iterator();
            while (true) {
                if (!it3.hasNext()) {
                    z = false;
                    break;
                }
                com.startapp.sdk.adsbase.cache.e next = it3.next();
                if (next.b() instanceof VideoEnabledAd) {
                    VideoEnabledAd videoEnabledAd = (VideoEnabledAd) next.b();
                    if (!(videoEnabledAd.g() == null || videoEnabledAd.g().b() == null || !videoEnabledAd.g().b().equals(a2))) {
                        z = true;
                        break;
                    }
                }
            }
            if (!z) {
                it2.remove();
                if (cachedVideoAd.a() != null) {
                    new File(cachedVideoAd.a()).delete();
                }
                z2 = true;
            }
        }
        return z2;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, CachedVideoAd cachedVideoAd) {
        if (this.b.contains(cachedVideoAd)) {
            this.b.remove(cachedVideoAd);
        }
        a(AdsCommonMetaData.a().I().b() - 1);
        this.b.add(cachedVideoAd);
        a(context);
    }

    private void a(Context context) {
        d.b(context, "CachedAds", this.b);
    }

    public static e a() {
        return f6218a;
    }

    static /* synthetic */ void a(e eVar, final Context context, String str, final g.a aVar, final c.a aVar2) {
        if (eVar.b == null) {
            Class<LinkedList> cls = LinkedList.class;
            eVar.b = (LinkedList) d.a(context, "CachedAds");
            if (eVar.b == null) {
                eVar.b = new LinkedList<>();
            }
            if (eVar.a(AdsCommonMetaData.a().I().b())) {
                eVar.a(context);
            }
        }
        try {
            URL url = new URL(str);
            String str2 = url.getHost() + url.getPath().replace("/", "_");
            try {
                String substring = str2.substring(0, str2.lastIndexOf(46));
                str2 = new String(Base64.encodeToString(MessageDigest.getInstance("MD5").digest(substring.getBytes()), 0)).replaceAll("[^a-zA-Z0-9]+", "_") + str2.substring(str2.lastIndexOf(46));
            } catch (NoSuchAlgorithmException e) {
                new com.startapp.sdk.adsbase.infoevents.e((Throwable) e).a(context);
            }
            String str3 = str2;
            final CachedVideoAd cachedVideoAd = new CachedVideoAd(str3);
            new g(context, url, str3, new g.a() {
                public final void a(String str) {
                    g.a aVar = aVar;
                    if (aVar != null) {
                        aVar.a(str);
                    }
                    if (str != null) {
                        cachedVideoAd.a(System.currentTimeMillis());
                        cachedVideoAd.a(str);
                        e.this.a(context, cachedVideoAd);
                    }
                }
            }, new c.a() {
                public final void a(String str) {
                    c.a aVar = aVar2;
                    if (aVar != null) {
                        aVar.a(str);
                    }
                }
            }).a();
        } catch (MalformedURLException e2) {
            if (aVar != null) {
                aVar.a((String) null);
            }
            new com.startapp.sdk.adsbase.infoevents.e((Throwable) e2).a(context);
        }
    }
}
