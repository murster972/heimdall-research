package com.startapp.sdk.ads.video.tracking;

import com.startapp.sdk.adsbase.commontracking.TrackingParams;

public class VideoTrackingParams extends TrackingParams {
    private static final long serialVersionUID = 1;
    private int completed;
    protected boolean internalParamsIndicator;
    private String replayParameter;
    private boolean shouldAppendOffset;
    private String videoPlayingMode;

    public VideoTrackingParams(String str, int i, int i2, String str2) {
        super(str);
        a(i2);
        this.completed = i;
        this.videoPlayingMode = str2;
    }

    public final VideoTrackingParams a(boolean z) {
        this.shouldAppendOffset = z;
        return this;
    }

    public final VideoTrackingParams b(boolean z) {
        this.internalParamsIndicator = z;
        return this;
    }

    public final boolean c() {
        return this.internalParamsIndicator;
    }

    /* access modifiers changed from: protected */
    public final String d() {
        if (!this.shouldAppendOffset) {
            return "";
        }
        String str = this.replayParameter;
        if (str != null) {
            return str.replace("%startapp_replay_count%", new Integer(h()).toString());
        }
        return super.d();
    }

    /* access modifiers changed from: protected */
    public final String e() {
        return "&vpm=" + this.videoPlayingMode;
    }

    public final VideoTrackingParams a(String str) {
        this.replayParameter = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public String b() {
        return "&cp=" + this.completed;
    }

    public String a() {
        return b(b() + e());
    }

    /* access modifiers changed from: protected */
    public final String b(String str) {
        if (!this.internalParamsIndicator) {
            return d();
        }
        return super.a() + str;
    }
}
