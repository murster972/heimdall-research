package com.startapp.sdk.ads.video;

import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.j.m;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.vungle.warren.model.Advertisement;

public final class a extends GetAdRequest {
    private GetAdRequest.VideoRequestType c;
    private GetAdRequest.VideoRequestMode d = GetAdRequest.VideoRequestMode.INTERSTITIAL;

    /* JADX WARNING: Removed duplicated region for block: B:16:0x0045  */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:21:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r1, com.startapp.sdk.adsbase.model.AdPreferences r2, com.startapp.sdk.adsbase.model.AdPreferences.Placement r3, android.util.Pair<java.lang.String, java.lang.String> r4) {
        /*
            r0 = this;
            super.a(r1, r2, r3, r4)
            com.startapp.sdk.adsbase.Ad$AdType r2 = r0.k()
            if (r2 == 0) goto L_0x0029
            com.startapp.sdk.adsbase.Ad$AdType r1 = r0.k()
            com.startapp.sdk.adsbase.Ad$AdType r2 = com.startapp.sdk.adsbase.Ad.AdType.NON_VIDEO
            if (r1 == r2) goto L_0x0039
            com.startapp.sdk.adsbase.Ad$AdType r1 = r0.k()
            com.startapp.sdk.adsbase.Ad$AdType r2 = com.startapp.sdk.adsbase.Ad.AdType.VIDEO_NO_VAST
            if (r1 != r2) goto L_0x001e
            com.startapp.sdk.adsbase.model.GetAdRequest$VideoRequestType r1 = com.startapp.sdk.adsbase.model.GetAdRequest.VideoRequestType.FORCED_NONVAST
            r0.c = r1
            goto L_0x003d
        L_0x001e:
            boolean r1 = r0.j()
            if (r1 == 0) goto L_0x003d
            com.startapp.sdk.adsbase.model.GetAdRequest$VideoRequestType r1 = com.startapp.sdk.adsbase.model.GetAdRequest.VideoRequestType.FORCED
            r0.c = r1
            goto L_0x003d
        L_0x0029:
            com.startapp.sdk.ads.video.VideoUtil$VideoEligibility r1 = com.startapp.sdk.ads.video.VideoUtil.a((android.content.Context) r1)
            com.startapp.sdk.ads.video.VideoUtil$VideoEligibility r2 = com.startapp.sdk.ads.video.VideoUtil.VideoEligibility.ELIGIBLE
            if (r1 != r2) goto L_0x0039
            com.startapp.sdk.adsbase.j.u.b()
            com.startapp.sdk.adsbase.model.GetAdRequest$VideoRequestType r1 = com.startapp.sdk.adsbase.model.GetAdRequest.VideoRequestType.ENABLED
            r0.c = r1
            goto L_0x003d
        L_0x0039:
            com.startapp.sdk.adsbase.model.GetAdRequest$VideoRequestType r1 = com.startapp.sdk.adsbase.model.GetAdRequest.VideoRequestType.DISABLED
            r0.c = r1
        L_0x003d:
            com.startapp.sdk.adsbase.Ad$AdType r1 = r0.k()
            com.startapp.sdk.adsbase.Ad$AdType r2 = com.startapp.sdk.adsbase.Ad.AdType.REWARDED_VIDEO
            if (r1 != r2) goto L_0x0049
            com.startapp.sdk.adsbase.model.GetAdRequest$VideoRequestMode r1 = com.startapp.sdk.adsbase.model.GetAdRequest.VideoRequestMode.REWARDED
            r0.d = r1
        L_0x0049:
            com.startapp.sdk.adsbase.Ad$AdType r1 = r0.k()
            com.startapp.sdk.adsbase.Ad$AdType r2 = com.startapp.sdk.adsbase.Ad.AdType.VIDEO
            if (r1 != r2) goto L_0x0055
            com.startapp.sdk.adsbase.model.GetAdRequest$VideoRequestMode r1 = com.startapp.sdk.adsbase.model.GetAdRequest.VideoRequestMode.INTERSTITIAL
            r0.d = r1
        L_0x0055:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.a.a(android.content.Context, com.startapp.sdk.adsbase.model.AdPreferences, com.startapp.sdk.adsbase.model.AdPreferences$Placement, android.util.Pair):void");
    }

    /* access modifiers changed from: protected */
    public final void a(m mVar) throws SDKException {
        super.a(mVar);
        mVar.a(Advertisement.KEY_VIDEO, this.c, false);
        mVar.a("videoMode", this.d, false);
    }
}
