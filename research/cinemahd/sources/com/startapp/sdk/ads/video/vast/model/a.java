package com.startapp.sdk.ads.video.vast.model;

import android.content.Context;
import android.util.DisplayMetrics;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.video.vast.model.a.b;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    protected int f6234a;
    protected int b;
    /* access modifiers changed from: private */
    public int c = (this.f6234a * this.b);

    /* renamed from: com.startapp.sdk.ads.video.vast.model.a$a  reason: collision with other inner class name */
    class C0071a implements Comparator<b> {
        private C0071a() {
        }

        public final /* synthetic */ int compare(Object obj, Object obj2) {
            b bVar = (b) obj;
            b bVar2 = (b) obj2;
            int intValue = bVar.d().intValue() * bVar.e().intValue();
            int intValue2 = bVar2.d().intValue() * bVar2.e().intValue();
            int abs = Math.abs(intValue - a.this.c);
            int abs2 = Math.abs(intValue2 - a.this.c);
            if (abs < abs2) {
                return -1;
            }
            return abs > abs2 ? 1 : 0;
        }

        /* synthetic */ C0071a(a aVar, byte b) {
            this();
        }
    }

    public a(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        this.f6234a = displayMetrics.widthPixels;
        this.b = displayMetrics.heightPixels;
        if (!e.a(context).equals("WIFI")) {
            this.c = (int) (((float) this.c) * 0.75f);
        }
    }

    /* access modifiers changed from: protected */
    public Comparator<b> a() {
        return new C0071a(this, (byte) 0);
    }

    public final b a(List<b> list) {
        if (list != null) {
            Iterator<b> it2 = list.iterator();
            while (it2.hasNext()) {
                b next = it2.next();
                if (!next.f() || !next.b().matches("video/.*(?i)(mp4|3gpp|mp2t|webm|matroska)")) {
                    it2.remove();
                }
            }
            if (list.size() != 0) {
                Collections.sort(list, a());
                if (list != null && list.size() > 0) {
                    return list.get(0);
                }
            }
        }
        return null;
    }
}
