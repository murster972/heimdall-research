package com.startapp.sdk.ads.video.player;

public interface VideoPlayerInterface {

    public enum VideoPlayerErrorType {
        UNKNOWN,
        SERVER_DIED,
        BUFFERING_TIMEOUT,
        PLAYER_CREATION
    }

    public interface a {
    }

    public interface b {
    }

    public interface c {
        void g(int i);
    }

    public interface d {
        void U();
    }

    public interface e {
        boolean b(g gVar);
    }

    public interface f {
        void T();
    }

    public static class g {

        /* renamed from: a  reason: collision with root package name */
        private VideoPlayerErrorType f6228a;
        private String b;
        private int c;

        public g(VideoPlayerErrorType videoPlayerErrorType, String str, int i) {
            this.f6228a = videoPlayerErrorType;
            this.b = str;
            this.c = i;
        }

        public final VideoPlayerErrorType a() {
            return this.f6228a;
        }

        public final String b() {
            return this.b;
        }

        public final int c() {
            return this.c;
        }
    }

    void a();

    void a(int i);

    void a(a aVar);

    void a(b bVar);

    void a(c cVar);

    void a(d dVar);

    void a(e eVar);

    void a(f fVar);

    void a(String str);

    void a(boolean z);

    void b();

    void c();

    int d();

    int e();

    boolean f();

    void g();
}
