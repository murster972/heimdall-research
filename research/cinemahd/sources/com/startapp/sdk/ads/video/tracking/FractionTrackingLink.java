package com.startapp.sdk.ads.video.tracking;

import com.startapp.common.parser.c;
import java.io.Serializable;

@c(c = true)
public class FractionTrackingLink extends VideoTrackingLink implements Serializable {
    private static final long serialVersionUID = 1;
    private int fraction;

    public final int a() {
        return this.fraction;
    }

    public String toString() {
        return super.toString() + ", fraction=" + this.fraction;
    }

    public final void a(int i) {
        this.fraction = i;
    }
}
