package com.startapp.sdk.ads.video.vast.model.a;

import java.util.ArrayList;
import java.util.List;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private String f6240a;
    private List<String> b;
    private List<String> c;

    public final String a() {
        return this.f6240a;
    }

    public final List<String> b() {
        if (this.b == null) {
            this.b = new ArrayList();
        }
        return this.b;
    }

    public final List<String> c() {
        if (this.c == null) {
            this.c = new ArrayList();
        }
        return this.c;
    }

    public final String toString() {
        return "VASTVideoClicks [clickThrough=" + this.f6240a + ", clickTracking=[" + this.b + "], customClick=[" + this.c + "] ]";
    }

    public final void a(String str) {
        this.f6240a = str;
    }
}
