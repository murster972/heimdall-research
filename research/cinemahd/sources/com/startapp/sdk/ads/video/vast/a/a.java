package com.startapp.sdk.ads.video.vast.a;

import android.content.Context;
import android.text.TextUtils;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.adsbase.j.u;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6230a;
    private final String b;
    private final JSONArray c = new JSONArray();
    private final String d;
    private final String e;
    private boolean f;

    public a(Context context, String str, String str2, String str3, boolean z) {
        this.f6230a = context;
        this.b = str;
        this.d = str2;
        this.e = str3;
        this.f = z;
    }

    public final void a(String str) {
        if (str != null) {
            this.c.put(str);
        }
    }

    public final void a(VASTErrorCodes vASTErrorCodes) {
        if (this.c.length() != 0) {
            if (!this.f || vASTErrorCodes == VASTErrorCodes.ErrorNone) {
                try {
                    JSONObject jSONObject = new JSONObject();
                    jSONObject.put("vastDocs", this.c);
                    String str = "";
                    jSONObject.put("partnerResponse", this.d != null ? this.d : str);
                    if (this.e != null) {
                        str = this.e;
                    }
                    jSONObject.put("partnerName", str);
                    jSONObject.put("error", vASTErrorCodes.a());
                    String jSONObject2 = jSONObject.toString();
                    if (!TextUtils.isEmpty(jSONObject2)) {
                        e.a(this.b, u.b(jSONObject2.getBytes()), (Map<String, String>) null, (String) null, true);
                    }
                } catch (Throwable th) {
                    new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6230a);
                }
            }
        }
    }
}
