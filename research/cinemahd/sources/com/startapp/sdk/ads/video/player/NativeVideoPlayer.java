package com.startapp.sdk.ads.video.player;

import android.media.MediaPlayer;
import android.widget.VideoView;
import com.startapp.sdk.ads.video.c;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface;
import com.startapp.sdk.adsbase.a;

public final class NativeVideoPlayer extends a implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnPreparedListener {
    private MediaPlayer f;
    private VideoView g;

    public enum MediaErrorExtra {
        MEDIA_ERROR_IO,
        MEDIA_ERROR_MALFORMED,
        MEDIA_ERROR_UNSUPPORTED,
        MEDIA_ERROR_TIMED_OUT;

        public static MediaErrorExtra a(int i) {
            if (i == -1010) {
                return MEDIA_ERROR_UNSUPPORTED;
            }
            if (i == -1007) {
                return MEDIA_ERROR_MALFORMED;
            }
            if (i == -1004) {
                return MEDIA_ERROR_IO;
            }
            if (i != -110) {
                return MEDIA_ERROR_IO;
            }
            return MEDIA_ERROR_TIMED_OUT;
        }
    }

    public enum MediaErrorType {
        MEDIA_ERROR_UNKNOWN,
        MEDIA_ERROR_SERVER_DIED;

        public static MediaErrorType a(int i) {
            if (i == 100) {
                return MEDIA_ERROR_SERVER_DIED;
            }
            return MEDIA_ERROR_UNKNOWN;
        }
    }

    public NativeVideoPlayer(VideoView videoView) {
        this.g = videoView;
        this.g.setOnPreparedListener(this);
        this.g.setOnCompletionListener(this);
        this.g.setOnErrorListener(this);
    }

    public final void a() {
        this.g.start();
    }

    public final void b() {
        this.g.pause();
    }

    public final void c() {
        this.g.stopPlayback();
    }

    public final int d() {
        return this.g.getCurrentPosition();
    }

    public final int e() {
        return this.g.getDuration();
    }

    public final boolean f() {
        return this.f != null;
    }

    public final void g() {
        if (this.f != null) {
            this.f = null;
        }
        c.a().a((VideoPlayerInterface.c) null);
    }

    public final void onCompletion(MediaPlayer mediaPlayer) {
        VideoPlayerInterface.d dVar = this.d;
        if (dVar != null) {
            dVar.U();
        }
    }

    public final boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
        if (this.c == null) {
            return false;
        }
        return this.c.b(new VideoPlayerInterface.g(MediaErrorType.a(i) == MediaErrorType.MEDIA_ERROR_SERVER_DIED ? VideoPlayerInterface.VideoPlayerErrorType.SERVER_DIED : VideoPlayerInterface.VideoPlayerErrorType.UNKNOWN, MediaErrorExtra.a(i2).toString(), mediaPlayer != null ? mediaPlayer.getCurrentPosition() : -1));
    }

    public final void onPrepared(MediaPlayer mediaPlayer) {
        MediaPlayer mediaPlayer2;
        this.f = mediaPlayer;
        VideoPlayerInterface.f fVar = this.b;
        if (fVar != null) {
            fVar.T();
        }
        if (a.d(this.f6229a) && (mediaPlayer2 = this.f) != null) {
            mediaPlayer2.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                public final void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
                    VideoPlayerInterface.c cVar = NativeVideoPlayer.this.e;
                    if (cVar != null) {
                        cVar.g(i);
                    }
                }
            });
        } else if (!a.d(this.f6229a)) {
            c.a().a(this.e);
        }
    }

    public final void a(int i) {
        this.g.seekTo(i);
    }

    public final void a(boolean z) {
        MediaPlayer mediaPlayer = this.f;
        if (mediaPlayer == null) {
            return;
        }
        if (z) {
            mediaPlayer.setVolume(0.0f, 0.0f);
        } else {
            mediaPlayer.setVolume(1.0f, 1.0f);
        }
    }

    public final void a(String str) {
        super.a(str);
        this.g.setVideoPath(this.f6229a);
    }
}
