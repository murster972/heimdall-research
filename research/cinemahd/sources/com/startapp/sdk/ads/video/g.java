package com.startapp.sdk.ads.video;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.sdk.ads.video.c;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import java.net.URL;

public final class g {

    /* renamed from: a  reason: collision with root package name */
    protected a f6222a;
    private Context b;
    private URL c;
    private String d;
    private c.a e;

    public interface a {
        void a(String str);
    }

    public g(Context context, URL url, String str, a aVar, c.a aVar2) {
        this.b = context;
        this.c = url;
        this.d = str;
        this.f6222a = aVar;
        this.e = aVar2;
    }

    public final void a() {
        final String str;
        try {
            str = AdsCommonMetaData.a().I().i() ? c.b.f6217a.a(this.b, this.c, this.d, this.e) : VideoUtil.a(this.b, this.c, this.d);
        } catch (Exception unused) {
            str = null;
        }
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            public final void run() {
                a aVar = g.this.f6222a;
                if (aVar != null) {
                    aVar.a(str);
                }
            }
        });
    }
}
