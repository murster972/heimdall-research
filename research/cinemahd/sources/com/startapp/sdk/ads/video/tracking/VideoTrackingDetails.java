package com.startapp.sdk.ads.video.tracking;

import com.startapp.common.parser.d;
import com.startapp.sdk.ads.video.vast.model.VASTEventType;
import com.startapp.sdk.ads.video.vast.model.c;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class VideoTrackingDetails implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = AbsoluteTrackingLink.class)
    private AbsoluteTrackingLink[] absoluteTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] creativeViewUrls;
    @d(b = FractionTrackingLink.class)
    private FractionTrackingLink[] fractionTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] impressionUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] inlineErrorTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] soundMuteUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] soundUnmuteUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoClickTrackingUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoClosedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPausedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPostRollClosedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoPostRollImpressionUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoResumedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoRewardedUrls;
    @d(b = ActionTrackingLink.class)
    private ActionTrackingLink[] videoSkippedUrls;

    public VideoTrackingDetails() {
    }

    public final FractionTrackingLink[] a() {
        return this.fractionTrackingUrls;
    }

    public final AbsoluteTrackingLink[] b() {
        return this.absoluteTrackingUrls;
    }

    public final ActionTrackingLink[] c() {
        return this.impressionUrls;
    }

    public final ActionTrackingLink[] d() {
        return this.soundUnmuteUrls;
    }

    public final ActionTrackingLink[] e() {
        return this.creativeViewUrls;
    }

    public final ActionTrackingLink[] f() {
        return this.soundMuteUrls;
    }

    public final ActionTrackingLink[] g() {
        return this.videoPausedUrls;
    }

    public final ActionTrackingLink[] h() {
        return this.videoResumedUrls;
    }

    public final ActionTrackingLink[] i() {
        return this.videoSkippedUrls;
    }

    public final ActionTrackingLink[] j() {
        return this.videoClosedUrls;
    }

    public final ActionTrackingLink[] k() {
        return this.videoPostRollImpressionUrls;
    }

    public final ActionTrackingLink[] l() {
        return this.videoPostRollClosedUrls;
    }

    public final ActionTrackingLink[] m() {
        return this.videoRewardedUrls;
    }

    public final ActionTrackingLink[] n() {
        return this.videoClickTrackingUrls;
    }

    public final ActionTrackingLink[] o() {
        return this.inlineErrorTrackingUrls;
    }

    public String toString() {
        return "VideoTrackingDetails [fractionTrackingUrls=" + a((VideoTrackingLink[]) this.fractionTrackingUrls) + ", absoluteTrackingUrls=" + a((VideoTrackingLink[]) this.absoluteTrackingUrls) + ", impressionUrls=" + a((VideoTrackingLink[]) this.impressionUrls) + ", creativeViewUrls=" + a((VideoTrackingLink[]) this.creativeViewUrls) + ", soundMuteUrls=" + a((VideoTrackingLink[]) this.soundMuteUrls) + ", soundUnmuteUrls=" + a((VideoTrackingLink[]) this.soundUnmuteUrls) + ", videoPausedUrls=" + a((VideoTrackingLink[]) this.videoPausedUrls) + ", videoResumedUrls=" + a((VideoTrackingLink[]) this.videoResumedUrls) + ", videoSkippedUrls=" + a((VideoTrackingLink[]) this.videoSkippedUrls) + ", videoClosedUrls=" + a((VideoTrackingLink[]) this.videoClosedUrls) + ", videoPostRollImpressionUrls=" + a((VideoTrackingLink[]) this.videoPostRollImpressionUrls) + ", videoPostRollClosedUrls=" + a((VideoTrackingLink[]) this.videoPostRollClosedUrls) + ", videoRewardedUrls=" + a((VideoTrackingLink[]) this.videoRewardedUrls) + ", videoClickTrackingUrls=" + a((VideoTrackingLink[]) this.videoClickTrackingUrls) + ", inlineErrorTrackingUrls=" + a((VideoTrackingLink[]) this.inlineErrorTrackingUrls) + "]";
    }

    public VideoTrackingDetails(c cVar) {
        if (cVar != null) {
            HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> a2 = cVar.a();
            ArrayList arrayList = new ArrayList();
            a(a2.get(VASTEventType.f), 0, arrayList);
            a(a2.get(VASTEventType.firstQuartile), 25, arrayList);
            a(a2.get(VASTEventType.midpoint), 50, arrayList);
            a(a2.get(VASTEventType.thirdQuartile), 75, arrayList);
            a(a2.get(VASTEventType.complete), 100, arrayList);
            this.fractionTrackingUrls = (FractionTrackingLink[]) arrayList.toArray(new FractionTrackingLink[arrayList.size()]);
            this.impressionUrls = b(cVar.c());
            this.soundMuteUrls = a(a2.get(VASTEventType.mute));
            this.soundUnmuteUrls = a(a2.get(VASTEventType.unmute));
            this.videoPausedUrls = a(a2.get(VASTEventType.pause));
            this.videoResumedUrls = a(a2.get(VASTEventType.resume));
            this.videoSkippedUrls = a(a2.get(VASTEventType.skip));
            this.videoPausedUrls = a(a2.get(VASTEventType.pause));
            this.videoClosedUrls = a(a2.get(VASTEventType.close));
            this.inlineErrorTrackingUrls = b(cVar.d());
            this.videoClickTrackingUrls = b(cVar.b().b());
            this.absoluteTrackingUrls = c(a2.get(VASTEventType.progress));
        }
    }

    private static String a(VideoTrackingLink[] videoTrackingLinkArr) {
        return videoTrackingLinkArr != null ? Arrays.toString(videoTrackingLinkArr) : "";
    }

    private static ActionTrackingLink[] b(List<String> list) {
        if (list == null) {
            return new ActionTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String a2 : list) {
            ActionTrackingLink actionTrackingLink = new ActionTrackingLink();
            actionTrackingLink.a(a2);
            actionTrackingLink.d();
            actionTrackingLink.b("");
            arrayList.add(actionTrackingLink);
        }
        return (ActionTrackingLink[]) arrayList.toArray(new ActionTrackingLink[arrayList.size()]);
    }

    private static AbsoluteTrackingLink[] c(List<com.startapp.sdk.ads.video.vast.model.a.c> list) {
        if (list == null) {
            return new AbsoluteTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (com.startapp.sdk.ads.video.vast.model.a.c next : list) {
            AbsoluteTrackingLink absoluteTrackingLink = new AbsoluteTrackingLink();
            absoluteTrackingLink.a(next.a());
            if (next.b() != -1) {
                absoluteTrackingLink.a(next.b());
            }
            absoluteTrackingLink.d();
            absoluteTrackingLink.b("");
            arrayList.add(absoluteTrackingLink);
        }
        return (AbsoluteTrackingLink[]) arrayList.toArray(new AbsoluteTrackingLink[arrayList.size()]);
    }

    private static void a(List<com.startapp.sdk.ads.video.vast.model.a.c> list, int i, List<FractionTrackingLink> list2) {
        if (list != null) {
            for (com.startapp.sdk.ads.video.vast.model.a.c a2 : list) {
                FractionTrackingLink fractionTrackingLink = new FractionTrackingLink();
                fractionTrackingLink.a(a2.a());
                fractionTrackingLink.a(i);
                fractionTrackingLink.d();
                fractionTrackingLink.b("");
                list2.add(fractionTrackingLink);
            }
        }
    }

    private static ActionTrackingLink[] a(List<com.startapp.sdk.ads.video.vast.model.a.c> list) {
        if (list == null) {
            return new ActionTrackingLink[0];
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (com.startapp.sdk.ads.video.vast.model.a.c a2 : list) {
            ActionTrackingLink actionTrackingLink = new ActionTrackingLink();
            actionTrackingLink.a(a2.a());
            actionTrackingLink.d();
            actionTrackingLink.b("");
            arrayList.add(actionTrackingLink);
        }
        return (ActionTrackingLink[]) arrayList.toArray(new ActionTrackingLink[arrayList.size()]);
    }
}
