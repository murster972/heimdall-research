package com.startapp.sdk.ads.video;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;
import com.startapp.common.b.b;
import com.startapp.sdk.ads.a.c;
import com.startapp.sdk.ads.banner.banner3d.Banner3DSize;
import com.startapp.sdk.ads.video.VideoAdDetails;
import com.startapp.sdk.ads.video.c;
import com.startapp.sdk.ads.video.player.NativeVideoPlayer;
import com.startapp.sdk.ads.video.player.VideoPlayerInterface;
import com.startapp.sdk.ads.video.tracking.AbsoluteTrackingLink;
import com.startapp.sdk.ads.video.tracking.ActionTrackingLink;
import com.startapp.sdk.ads.video.tracking.FractionTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoClickedTrackingParams;
import com.startapp.sdk.ads.video.tracking.VideoPausedTrackingParams;
import com.startapp.sdk.ads.video.tracking.VideoProgressTrackingParams;
import com.startapp.sdk.ads.video.tracking.VideoTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoTrackingParams;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.VideoConfig;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.omsdk.a;
import com.uwetrottmann.trakt5.TraktV2;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import okhttp3.internal.cache.DiskLruCache;

public final class VideoMode extends c implements b.a, VideoPlayerInterface.a, VideoPlayerInterface.b, VideoPlayerInterface.c, VideoPlayerInterface.d, VideoPlayerInterface.e, VideoPlayerInterface.f {
    private int A = 0;
    private int B = 0;
    private boolean C = false;
    private boolean D = false;
    private HashMap<Integer, Boolean> E = new HashMap<>();
    private HashMap<Integer, Boolean> F = new HashMap<>();
    private int G = 1;
    private boolean H = false;
    private boolean I = false;
    private int J = 0;
    private boolean K = false;
    private boolean L = false;
    private boolean M = false;
    private int N = 0;
    private int O;
    private String P = null;
    private Handler Q = new Handler();
    private Map<Integer, List<FractionTrackingLink>> R = new HashMap();
    private Map<Integer, List<AbsoluteTrackingLink>> S = new HashMap();
    private long T;
    private VideoClickedTrackingParams.ClickOrigin U;
    private long V;
    /* access modifiers changed from: private */
    public Banner3DSize W;
    private boolean X = false;
    /* access modifiers changed from: private */
    public BroadcastReceiver Y = new BroadcastReceiver() {
        public final void onReceive(Context context, Intent intent) {
            if (!VideoMode.this.Y.isInitialStickyBroadcast()) {
                VideoMode videoMode = VideoMode.this;
                if (videoMode.o != videoMode.Y()) {
                    VideoMode videoMode2 = VideoMode.this;
                    videoMode2.o = !videoMode2.o;
                    videoMode2.R();
                    VideoMode videoMode3 = VideoMode.this;
                    videoMode3.a(videoMode3.o);
                }
            }
        }
    };
    protected VideoPlayerInterface l;
    protected VideoView m;
    protected ProgressBar n;
    protected boolean o = false;
    protected int p = 0;
    protected boolean q;
    protected boolean r = false;
    protected boolean s = false;
    protected boolean t = false;
    protected boolean u = false;
    protected Handler v = new Handler();
    protected Handler w = new Handler();
    protected Handler x = new Handler();
    private RelativeLayout y;
    private RelativeLayout z;

    /* renamed from: com.startapp.sdk.ads.video.VideoMode$12  reason: invalid class name */
    static /* synthetic */ class AnonymousClass12 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6193a = new int[VideoPlayerInterface.VideoPlayerErrorType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.startapp.sdk.ads.video.player.VideoPlayerInterface$VideoPlayerErrorType[] r0 = com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6193a = r0
                int[] r0 = f6193a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.ads.video.player.VideoPlayerInterface$VideoPlayerErrorType r1 = com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType.SERVER_DIED     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6193a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.ads.video.player.VideoPlayerInterface$VideoPlayerErrorType r1 = com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType.BUFFERING_TIMEOUT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6193a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.ads.video.player.VideoPlayerInterface$VideoPlayerErrorType r1 = com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType.PLAYER_CREATION     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6193a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.ads.video.player.VideoPlayerInterface$VideoPlayerErrorType r1 = com.startapp.sdk.ads.video.player.VideoPlayerInterface.VideoPlayerErrorType.UNKNOWN     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.VideoMode.AnonymousClass12.<clinit>():void");
        }
    }

    private enum HtmlMode {
        PLAYER,
        POST_ROLL
    }

    private enum Sound {
        ON,
        OFF
    }

    private enum VideoFinishedReason {
        COMPLETE,
        CLICKED,
        SKIPPED
    }

    private void V() {
        this.L = true;
        ac();
        if (ah()) {
            this.l.b();
            return;
        }
        new Handler().postDelayed(new Runnable() {
            public final void run() {
                VideoPlayerInterface videoPlayerInterface = VideoMode.this.l;
                if (videoPlayerInterface != null) {
                    videoPlayerInterface.a();
                    if (VideoMode.this.W != null) {
                        VideoMode.this.W.a((float) VideoMode.this.l.e(), VideoMode.this.o ? 0.0f : 1.0f);
                    }
                    VideoMode videoMode = VideoMode.this;
                    videoMode.r = true;
                    videoMode.K();
                    new Handler().post(new Runnable() {
                        public final void run() {
                            VideoMode.this.I();
                        }
                    });
                }
            }
        }, Z());
        if (this.p == 0) {
            this.v.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (VideoMode.this.l == null) {
                            return;
                        }
                        if (VideoMode.this.l.d() > 0) {
                            VideoMode.this.e(0);
                            VideoMode.this.f(0);
                            if (VideoMode.this.i == 0) {
                                VideoMode.this.S();
                                com.startapp.common.b.a((Context) VideoMode.this.c()).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
                            }
                        } else if (!VideoMode.this.s) {
                            VideoMode.this.v.postDelayed(this, 100);
                        }
                    } catch (Throwable th) {
                        new e(th).a((Context) VideoMode.this.c());
                        VideoMode.this.p();
                    }
                }
            }, 100);
        }
        ai();
        al();
        ad();
        ae();
        this.f6097a.a().setVisibility(4);
        R();
    }

    private void W() {
        if (!X()) {
            this.t = false;
            this.x.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.n.setVisibility(0);
                        if (VideoMode.this.W != null) {
                            VideoMode.this.W.f();
                        }
                        VideoMode.this.x.postDelayed(new Runnable() {
                            public final void run() {
                                try {
                                    VideoMode.this.K();
                                    VideoMode.this.t = true;
                                    VideoMode.this.a(new VideoPlayerInterface.g(VideoPlayerInterface.VideoPlayerErrorType.BUFFERING_TIMEOUT, "Buffering timeout reached", VideoMode.this.p));
                                } catch (Throwable th) {
                                    new e(th).a((Context) VideoMode.this.c());
                                }
                            }
                        }, AdsCommonMetaData.a().I().g());
                    } catch (Throwable th) {
                        VideoMode.this.K();
                        new e(th).a((Context) VideoMode.this.c());
                    }
                }
            }, AdsCommonMetaData.a().I().f());
        }
    }

    private boolean X() {
        ProgressBar progressBar = this.n;
        return progressBar != null && progressBar.isShown();
    }

    /* access modifiers changed from: private */
    public boolean Y() {
        AudioManager audioManager = (AudioManager) c().getSystemService("audio");
        if (audioManager == null) {
            return false;
        }
        if (audioManager.getRingerMode() == 0 || audioManager.getRingerMode() == 1) {
            return true;
        }
        return false;
    }

    private long Z() {
        long currentTimeMillis = System.currentTimeMillis() - this.T;
        if (this.p == 0 && this.i == 0 && currentTimeMillis < 500) {
            return Math.max(200, 500 - currentTimeMillis);
        }
        return 0;
    }

    private void aa() {
        Object[] objArr = new Object[1];
        objArr[0] = Boolean.valueOf(this.l != null);
        a("videoApi.setReplayEnabled", objArr);
        a("videoApi.setMode", HtmlMode.POST_ROLL + "_" + P().c());
        a("videoApi.setCloseable", Boolean.TRUE);
    }

    private void ab() {
        a("videoApi.setClickableVideo", Boolean.valueOf(P().g()));
        a("videoApi.setMode", HtmlMode.PLAYER.toString());
        Object[] objArr = new Object[1];
        objArr[0] = Boolean.valueOf(P().d() || this.I);
        a("videoApi.setCloseable", objArr);
        a("videoApi.setSkippable", Boolean.valueOf(ap()));
    }

    private void ac() {
        a("videoApi.setVideoDuration", Integer.valueOf(this.l.e() / 1000));
        M();
        af();
        a("videoApi.setVideoCurrentPosition", Integer.valueOf(this.p / 1000));
    }

    private void ad() {
        this.w.post(new Runnable() {
            public final void run() {
                int M = VideoMode.this.M();
                if (M >= 1000) {
                    VideoMode.this.w.postDelayed(this, c.a((long) M) + 50);
                }
            }
        });
    }

    private void ae() {
        af();
        this.w.post(new Runnable() {

            /* renamed from: a  reason: collision with root package name */
            private boolean f6200a;
            private final int b = VideoMode.this.d(AdsCommonMetaData.a().I().d());

            public final void run() {
                try {
                    int c2 = VideoMode.this.c(VideoMode.this.l.d() + 50);
                    if (c2 >= 0 && !this.f6200a) {
                        if (c2 != 0) {
                            if (VideoMode.this.p < VideoMode.this.P().f() * 1000) {
                                VideoMode.this.a("videoApi.setSkipTimer", Integer.valueOf(c2));
                            }
                        }
                        this.f6200a = true;
                        VideoMode.this.a("videoApi.setSkipTimer", 0);
                    }
                    if (VideoMode.this.u && VideoMode.this.l.d() >= this.b) {
                        VideoMode.this.F();
                    }
                    int d = (VideoMode.this.l.d() + 50) / 1000;
                    VideoMode.this.a("videoApi.setVideoCurrentPosition", Integer.valueOf(d));
                    if (d < VideoMode.this.l.e() / 1000) {
                        VideoMode.this.w.postDelayed(this, VideoMode.this.N());
                    }
                } catch (Exception unused) {
                }
            }
        });
    }

    private void af() {
        a("videoApi.setSkipTimer", Integer.valueOf(c(this.p + 50)));
    }

    private int ag() {
        if (this.l.d() != this.l.e() || ah()) {
            return this.l.e() - this.l.d();
        }
        return this.l.e();
    }

    private boolean ah() {
        return this.p == -1;
    }

    private void ai() {
        this.B = this.l.e();
        aj();
        ak();
    }

    private void aj() {
        for (Integer intValue : this.R.keySet()) {
            final int intValue2 = intValue.intValue();
            a(d(intValue2), this.v, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.e(intValue2);
                    } catch (Throwable th) {
                        new e(th).a((Context) VideoMode.this.c());
                    }
                }
            });
        }
    }

    private void ak() {
        for (Integer intValue : this.S.keySet()) {
            final int intValue2 = intValue.intValue();
            a(intValue2, this.v, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.f(intValue2);
                    } catch (Throwable th) {
                        new e(th).a((Context) VideoMode.this.c());
                    }
                }
            });
        }
    }

    private void al() {
        if (!this.u) {
            a(d(AdsCommonMetaData.a().I().d()), this.Q, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        VideoMode.this.F();
                    } catch (Throwable th) {
                        new e(th).a((Context) VideoMode.this.c());
                    }
                }
            });
        }
    }

    private void am() {
        Intent intent = new Intent("com.startapp.android.ShowFailedDisplayBroadcastListener");
        intent.putExtra("showFailedReason", AdDisplayListener.NotDisplayedReason.VIDEO_ERROR);
        com.startapp.common.b.a((Context) c()).a(intent);
        this.s = true;
    }

    private boolean an() {
        VideoPlayerInterface videoPlayerInterface = this.l;
        return videoPlayerInterface != null && videoPlayerInterface.f();
    }

    private boolean ao() {
        return !this.u ? an() && this.C : this.J >= AdsCommonMetaData.a().I().k() && an() && this.C;
    }

    private boolean ap() {
        return this.i > 0 || P().e() || this.H;
    }

    private void aq() {
        a(P().h().k(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), this.A, "postrollImression");
    }

    private void ar() {
        a(P().h().l(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), this.A, "postrollClosed");
    }

    private void as() {
        a(P().h().j(), new VideoTrackingParams(m(), h(this.l.d()), this.i, this.P), this.l.d(), "closed");
    }

    static /* synthetic */ int g(VideoMode videoMode) {
        int i = videoMode.i;
        videoMode.i = i + 1;
        return i;
    }

    private int h(int i) {
        int i2 = this.B;
        if (i2 > 0) {
            return (i * 100) / i2;
        }
        return 0;
    }

    /* access modifiers changed from: protected */
    public final void A() {
    }

    /* access modifiers changed from: protected */
    public final void B() {
        if (this.s) {
            return;
        }
        if (ah() || this.m == null) {
            ar();
            super.B();
            return;
        }
        as();
    }

    /* access modifiers changed from: protected */
    public final TrackingParams C() {
        return new VideoTrackingParams(m(), 0, this.i, this.P);
    }

    /* access modifiers changed from: protected */
    public final long D() {
        return (SystemClock.uptimeMillis() - this.V) / 1000;
    }

    /* access modifiers changed from: protected */
    public final long E() {
        if (o() != null) {
            return TimeUnit.SECONDS.toMillis(o().longValue());
        }
        return TimeUnit.SECONDS.toMillis(MetaData.G().I());
    }

    /* access modifiers changed from: protected */
    public final void G() {
        a(P().h().m(), new VideoTrackingParams(m(), AdsCommonMetaData.a().I().d(), this.i, this.P), d(AdsCommonMetaData.a().I().d()), "rewarded");
    }

    /* access modifiers changed from: protected */
    public final boolean H() {
        return x().getType() == Ad.AdType.REWARDED_VIDEO;
    }

    /* access modifiers changed from: protected */
    public final void I() {
        if (this.r) {
            b((View) this.m);
            if (!ah()) {
                ab();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void J() {
        boolean i = AdsCommonMetaData.a().I().i();
        String b = P().b();
        if (b != null) {
            this.l.a(b);
            if (i) {
                c unused = c.b.f6217a;
                if (c.b(b)) {
                    this.u = true;
                    this.M = true;
                    this.J = AdsCommonMetaData.a().I().k();
                }
            }
        } else if (i) {
            String a2 = P().a();
            c.b.f6217a.a(a2);
            this.l.a(a2);
            this.u = true;
            W();
        } else {
            a(VideoFinishedReason.SKIPPED);
        }
        if (this.P == null) {
            this.P = this.u ? TraktV2.API_VERSION : DiskLruCache.VERSION_1;
        }
    }

    /* access modifiers changed from: protected */
    public final void K() {
        this.x.removeCallbacksAndMessages((Object) null);
        if (X()) {
            this.n.setVisibility(8);
            Banner3DSize banner3DSize = this.W;
            if (banner3DSize != null) {
                banner3DSize.g();
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void L() {
        a("videoApi.setVideoCurrentPosition", 0);
        a("videoApi.setSkipTimer", 0);
    }

    /* access modifiers changed from: protected */
    public final int M() {
        int ag = ag();
        int i = ag / 1000;
        if (i > 0 && ag % 1000 < 100) {
            i--;
        }
        a("videoApi.setVideoRemainingTimer", Integer.valueOf(i));
        return ag;
    }

    /* access modifiers changed from: protected */
    public final long N() {
        return (long) (1000 - (this.l.d() % 1000));
    }

    /* access modifiers changed from: protected */
    public final void O() {
        this.p = 0;
    }

    /* access modifiers changed from: protected */
    public final VideoAdDetails P() {
        return ((VideoEnabledAd) x()).g();
    }

    /* access modifiers changed from: protected */
    public final void Q() {
        if (X()) {
            K();
        }
        a(VideoFinishedReason.SKIPPED);
        a(P().h().i(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), this.A, "skipped");
    }

    /* access modifiers changed from: protected */
    public final void R() {
        String str;
        VideoPlayerInterface videoPlayerInterface = this.l;
        if (videoPlayerInterface != null) {
            try {
                if (this.o) {
                    videoPlayerInterface.a(true);
                } else {
                    videoPlayerInterface.a(false);
                }
            } catch (IllegalStateException unused) {
            }
        }
        Object[] objArr = new Object[1];
        if (this.o) {
            str = Sound.OFF.toString();
        } else {
            str = Sound.ON.toString();
        }
        objArr[0] = str;
        a("videoApi.setSound", objArr);
    }

    /* access modifiers changed from: protected */
    public final void S() {
        super.A();
        a(P().h().c(), new VideoTrackingParams(m(), 0, this.i, this.P), 0, "impression");
        a(P().h().e(), new VideoTrackingParams(m(), 0, this.i, this.P), 0, "creativeView");
    }

    public final void T() {
        this.K = true;
        if (this.C && this.D) {
            I();
        }
        if (ao()) {
            V();
        }
    }

    public final void U() {
        if (!ah()) {
            a(VideoFinishedReason.COMPLETE);
        }
        VideoPlayerInterface videoPlayerInterface = this.l;
        if (videoPlayerInterface != null) {
            videoPlayerInterface.c();
        }
    }

    public final void p() {
        super.p();
        if (this.M) {
            c unused = c.b.f6217a;
            c.c(P().b());
        }
    }

    public final void q() {
        if (!this.s) {
            super.q();
        }
    }

    public final boolean r() {
        if (ah()) {
            B();
            return false;
        }
        VideoPlayerInterface videoPlayerInterface = this.l;
        if (videoPlayerInterface == null) {
            return false;
        }
        int c = c(videoPlayerInterface.d() + 50);
        if (ap() && c == 0) {
            Q();
            return true;
        } else if (!P().d() && !this.I) {
            return true;
        } else {
            B();
            return false;
        }
    }

    public final void s() {
        if (!ah() && !c().isFinishing() && !this.I && !this.H) {
            VideoPausedTrackingParams.PauseOrigin pauseOrigin = VideoPausedTrackingParams.PauseOrigin.EXTERNAL;
            VideoPlayerInterface videoPlayerInterface = this.l;
            if (videoPlayerInterface != null) {
                int d = videoPlayerInterface.d();
                this.p = d;
                this.A = d;
                this.l.b();
                Banner3DSize banner3DSize = this.W;
                if (banner3DSize != null) {
                    banner3DSize.e();
                }
            }
            a(P().h().g(), new VideoPausedTrackingParams(m(), h(this.A), this.i, this.G, pauseOrigin, this.P), this.A, "paused");
        }
        VideoPlayerInterface videoPlayerInterface2 = this.l;
        if (videoPlayerInterface2 != null) {
            videoPlayerInterface2.g();
            this.l = null;
        }
        this.v.removeCallbacksAndMessages((Object) null);
        this.w.removeCallbacksAndMessages((Object) null);
        this.Q.removeCallbacksAndMessages((Object) null);
        K();
        this.q = true;
        if (this.X) {
            c().unregisterReceiver(this.Y);
            this.X = false;
        }
        super.s();
    }

    public final void u() {
        super.u();
        c().registerReceiver(this.Y, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        this.X = true;
        if (!c().isFinishing()) {
            if (this.m == null) {
                Context applicationContext = c().getApplicationContext();
                this.V = SystemClock.uptimeMillis();
                this.z = (RelativeLayout) c().findViewById(1475346432);
                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                this.m = new VideoView(applicationContext);
                this.m.setId(100);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams2.addRule(13);
                this.n = new ProgressBar(applicationContext, (AttributeSet) null, 16843399);
                this.n.setVisibility(4);
                RelativeLayout.LayoutParams layoutParams3 = new RelativeLayout.LayoutParams(-2, -2);
                layoutParams3.addRule(14);
                layoutParams3.addRule(15);
                this.y = new RelativeLayout(applicationContext);
                this.y.setId(1475346436);
                c().setContentView(this.y);
                this.y.addView(this.m, layoutParams2);
                this.y.addView(this.z, layoutParams);
                this.y.addView(this.n, layoutParams3);
                if (AdsConstants.a().booleanValue()) {
                    RelativeLayout.LayoutParams layoutParams4 = new RelativeLayout.LayoutParams(-2, -2);
                    layoutParams4.addRule(12);
                    layoutParams4.addRule(14);
                    RelativeLayout relativeLayout = this.y;
                    StringBuilder sb = new StringBuilder();
                    sb.append("url=" + P().a());
                    TextView textView = new TextView(applicationContext);
                    textView.setBackgroundColor(-16777216);
                    b.a((View) textView, 0.5f);
                    textView.setTextColor(-7829368);
                    textView.setSingleLine(false);
                    textView.setText(sb.toString());
                    relativeLayout.addView(textView, layoutParams4);
                }
                this.f6097a.a().setVisibility(4);
            }
            if (this.l == null) {
                this.l = new NativeVideoPlayer(this.m);
            }
            this.D = false;
            this.y.setBackgroundColor(-16777216);
            J();
            if (ah()) {
                this.f6097a.a().setVisibility(0);
                this.m.setVisibility(4);
            } else {
                int i = this.p;
                if (i != 0) {
                    this.l.a(i);
                    a(P().h().h(), new VideoPausedTrackingParams(m(), h(this.A), this.i, this.G, VideoPausedTrackingParams.PauseOrigin.EXTERNAL, this.P), this.A, "resumed");
                    this.G++;
                }
            }
            this.l.a((VideoPlayerInterface.f) this);
            this.l.a((VideoPlayerInterface.d) this);
            this.l.a((VideoPlayerInterface.e) this);
            this.l.a((VideoPlayerInterface.b) this);
            this.l.a((VideoPlayerInterface.c) this);
            this.l.a((VideoPlayerInterface.a) this);
            b.a((View) this.m, (b.a) this);
        }
    }

    /* access modifiers changed from: protected */
    public final com.startapp.sdk.e.b z() {
        Activity c = c();
        Runnable runnable = this.k;
        return new f(c, runnable, runnable, new Runnable() {
            public final void run() {
                VideoMode videoMode = VideoMode.this;
                if (videoMode.l != null) {
                    VideoMode.g(videoMode);
                    VideoMode.this.m.setVisibility(0);
                    VideoMode videoMode2 = VideoMode.this;
                    videoMode2.q = false;
                    videoMode2.O();
                    VideoMode.this.L();
                    VideoMode.this.J();
                }
            }
        }, new Runnable() {
            public final void run() {
                VideoMode videoMode = VideoMode.this;
                if (videoMode.l != null) {
                    videoMode.Q();
                }
            }
        }, new Runnable() {
            public final void run() {
                VideoMode videoMode = VideoMode.this;
                if (videoMode.l != null) {
                    videoMode.o = !videoMode.o;
                    videoMode.R();
                    VideoMode videoMode2 = VideoMode.this;
                    videoMode2.a(videoMode2.o);
                }
            }
        }, new TrackingParams(m()), a(0));
    }

    /* access modifiers changed from: protected */
    public final int d(int i) {
        return (this.B * i) / 100;
    }

    /* access modifiers changed from: protected */
    public final void e(int i) {
        if (this.E.get(Integer.valueOf(i)) == null) {
            if (this.R.containsKey(Integer.valueOf(i))) {
                List list = this.R.get(Integer.valueOf(i));
                a((VideoTrackingLink[]) list.toArray(new FractionTrackingLink[list.size()]), new VideoProgressTrackingParams(m(), i, this.i, this.P), d(i), "fraction");
                Banner3DSize banner3DSize = this.W;
                if (banner3DSize != null) {
                    if (i == 25) {
                        banner3DSize.a();
                    } else if (i == 50) {
                        banner3DSize.b();
                    } else if (i == 75) {
                        banner3DSize.c();
                    }
                }
            }
            this.E.put(Integer.valueOf(i), Boolean.TRUE);
        }
    }

    /* access modifiers changed from: protected */
    public final void f(int i) {
        if (this.F.get(Integer.valueOf(i)) == null) {
            if (this.S.containsKey(Integer.valueOf(i))) {
                List list = this.S.get(Integer.valueOf(i));
                a((VideoTrackingLink[]) list.toArray(new AbsoluteTrackingLink[list.size()]), new VideoProgressTrackingParams(m(), i, this.i, this.P), i, "absolute");
            }
            this.F.put(Integer.valueOf(i), Boolean.TRUE);
        }
    }

    public final void g(int i) {
        VideoPlayerInterface videoPlayerInterface;
        if (this.u && this.K && (videoPlayerInterface = this.l) != null && videoPlayerInterface.e() != 0) {
            this.J = i;
            int d = (this.l.d() * 100) / this.l.e();
            if (!X()) {
                int i2 = this.J;
                if (i2 < 100 && i2 - d <= AdsCommonMetaData.a().I().k()) {
                    this.l.b();
                    W();
                }
            } else if (this.L || !ao()) {
                int i3 = this.J;
                if (i3 == 100 || i3 - d > AdsCommonMetaData.a().I().j()) {
                    this.l.a();
                    K();
                }
            } else {
                V();
            }
        }
    }

    public final void a(Bundle bundle) {
        super.a(bundle);
        try {
            this.T = System.currentTimeMillis();
            this.O = 100 / AdsCommonMetaData.a().I().j();
            boolean z2 = true;
            if (h().equals("back")) {
                if (AdsCommonMetaData.a().I().a().equals(VideoConfig.BackMode.BOTH)) {
                    this.H = true;
                    this.I = true;
                } else if (AdsCommonMetaData.a().I().a().equals(VideoConfig.BackMode.SKIP)) {
                    this.H = true;
                    this.I = false;
                } else if (AdsCommonMetaData.a().I().a().equals(VideoConfig.BackMode.CLOSE)) {
                    this.H = false;
                    this.I = true;
                } else {
                    AdsCommonMetaData.a().I().a().equals(VideoConfig.BackMode.DISABLED);
                    this.H = false;
                    this.I = false;
                }
            }
            FractionTrackingLink[] a2 = P().h().a();
            if (a2 != null) {
                for (FractionTrackingLink fractionTrackingLink : a2) {
                    List list = this.R.get(Integer.valueOf(fractionTrackingLink.a()));
                    if (list == null) {
                        list = new ArrayList();
                        this.R.put(Integer.valueOf(fractionTrackingLink.a()), list);
                    }
                    list.add(fractionTrackingLink);
                }
            }
            AbsoluteTrackingLink[] b = P().h().b();
            if (b != null) {
                for (AbsoluteTrackingLink absoluteTrackingLink : b) {
                    List list2 = this.S.get(Integer.valueOf(absoluteTrackingLink.a()));
                    if (list2 == null) {
                        list2 = new ArrayList();
                        this.S.put(Integer.valueOf(absoluteTrackingLink.a()), list2);
                    }
                    list2.add(absoluteTrackingLink);
                }
            }
            if (!Y() && !P().i()) {
                if (!AdsCommonMetaData.a().I().m().equals("muted")) {
                    z2 = false;
                }
            }
            this.o = z2;
            if (bundle != null && bundle.containsKey("currentPosition")) {
                this.p = bundle.getInt("currentPosition");
                this.A = bundle.getInt("latestPosition");
                this.E = (HashMap) bundle.getSerializable("fractionProgressImpressionsSent");
                this.F = (HashMap) bundle.getSerializable("absoluteProgressImpressionsSent");
                this.o = bundle.getBoolean("isMuted");
                this.q = bundle.getBoolean("shouldSetBg");
                this.G = bundle.getInt("pauseNum");
            }
        } catch (Throwable th) {
            new e(th).a((Context) c());
            am();
            p();
        }
    }

    /* access modifiers changed from: protected */
    public final int c(int i) {
        int f;
        if (!this.H && this.i <= 0 && (f = (P().f() * 1000) - i) > 0) {
            return (f / 1000) + 1;
        }
        return 0;
    }

    private void b(View view) {
        a("videoApi.setVideoFrame", Integer.valueOf(t.b(c(), view.getLeft())), Integer.valueOf(t.b(c(), view.getTop())), Integer.valueOf(t.b(c(), view.getWidth())), Integer.valueOf(t.b(c(), view.getHeight())));
    }

    public final void b(Bundle bundle) {
        super.b(bundle);
        bundle.putInt("currentPosition", this.p);
        bundle.putInt("latestPosition", this.A);
        bundle.putSerializable("fractionProgressImpressionsSent", this.E);
        bundle.putSerializable("absoluteProgressImpressionsSent", this.F);
        bundle.putBoolean("isMuted", this.o);
        bundle.putBoolean("shouldSetBg", this.q);
        bundle.putInt("pauseNum", this.G);
    }

    public final boolean b(VideoPlayerInterface.g gVar) {
        this.K = false;
        if (!this.u || this.N > this.O || gVar.c() <= 0 || !gVar.b().equals(NativeVideoPlayer.MediaErrorExtra.MEDIA_ERROR_IO.toString())) {
            a(gVar);
        } else {
            this.N++;
            W();
            this.l.a(P().b());
            this.l.a(gVar.c());
        }
        return true;
    }

    public final void a(WebView webView) {
        super.a(webView);
        webView.setBackgroundColor(33554431);
        b.d(webView);
    }

    /* access modifiers changed from: protected */
    public final void a(View view) {
        this.C = true;
        if (this.D && an()) {
            I();
        } else if (ah()) {
            b((View) this.c);
        }
        if (ao()) {
            V();
        }
        if (ah()) {
            aa();
        }
        VideoAdDetails P2 = P();
        if (MetaData.G().S() && this.d == null && P2 != null) {
            P2.k();
            if (P2.k().a() != null) {
                this.d = a.a(this.c.getContext(), P().k());
                com.iab.omid.library.startapp.adsession.b bVar = this.d;
                if (bVar != null) {
                    this.W = Banner3DSize.a(bVar);
                    View a2 = this.f6097a.a();
                    if (a2 != null) {
                        this.d.b(a2);
                    }
                    this.d.b(this.c);
                    this.d.b(this.z);
                    this.d.a(this.m);
                    this.d.a();
                    com.iab.omid.library.startapp.adsession.a.a(this.d).a();
                }
            }
        }
    }

    private void a(VideoFinishedReason videoFinishedReason) {
        Banner3DSize banner3DSize;
        Banner3DSize banner3DSize2;
        if (videoFinishedReason == VideoFinishedReason.COMPLETE && (banner3DSize2 = this.W) != null) {
            banner3DSize2.d();
        }
        if (videoFinishedReason == VideoFinishedReason.SKIPPED && (banner3DSize = this.W) != null) {
            banner3DSize.h();
        }
        if (videoFinishedReason == VideoFinishedReason.SKIPPED || videoFinishedReason == VideoFinishedReason.CLICKED) {
            this.v.removeCallbacksAndMessages((Object) null);
            this.Q.removeCallbacksAndMessages((Object) null);
            VideoPlayerInterface videoPlayerInterface = this.l;
            if (videoPlayerInterface != null) {
                this.A = videoPlayerInterface.d();
                this.l.b();
            }
        } else {
            this.A = this.B;
            F();
        }
        this.w.removeCallbacksAndMessages((Object) null);
        this.E.clear();
        this.F.clear();
        if (videoFinishedReason == VideoFinishedReason.CLICKED) {
            this.p = -1;
            return;
        }
        if (P().c() != VideoAdDetails.PostRollType.NONE) {
            aa();
            this.f6097a.a().setVisibility(0);
        } else if (P().c() == VideoAdDetails.PostRollType.NONE) {
            p();
        }
        this.p = -1;
        if (P().c() != VideoAdDetails.PostRollType.NONE) {
            aq();
        }
    }

    private void a(int i, Handler handler, Runnable runnable) {
        int i2 = this.p;
        if (i2 < i) {
            handler.postDelayed(runnable, (long) (i - i2));
        }
    }

    /* access modifiers changed from: protected */
    public final void a(VideoPlayerInterface.g gVar) {
        VASTErrorCodes vASTErrorCodes;
        e eVar = new e(InfoEventCategory.ERROR);
        eVar.f("Video player error: " + gVar.a()).g(gVar.b()).h(n()).a((Context) c());
        int i = AnonymousClass12.f6193a[gVar.a().ordinal()];
        if (i == 1) {
            vASTErrorCodes = VASTErrorCodes.GeneralLinearError;
        } else if (i == 2) {
            vASTErrorCodes = VASTErrorCodes.TimeoutMediaFileURI;
        } else if (i != 3) {
            vASTErrorCodes = VASTErrorCodes.UndefinedError;
        } else {
            vASTErrorCodes = VASTErrorCodes.MediaFileDisplayError;
        }
        VideoUtil.a((Context) c(), new com.startapp.sdk.ads.video.a.b(P().h().o(), new VideoTrackingParams(m(), h(this.A), this.i, this.P), P().a(), this.A).a(vASTErrorCodes).a("error").a());
        if ((this.u ? this.l.d() : this.p) == 0) {
            com.startapp.sdk.adsbase.a.a((Context) c(), i(), m(), this.i, AdDisplayListener.NotDisplayedReason.VIDEO_ERROR.toString());
            if (!this.u) {
                VideoUtil.b((Context) c());
            } else if (!gVar.a().equals(VideoPlayerInterface.VideoPlayerErrorType.BUFFERING_TIMEOUT)) {
                VideoUtil.b((Context) c());
            }
        }
        if ((!H() || this.h) && P().c() != VideoAdDetails.PostRollType.NONE) {
            a(VideoFinishedReason.SKIPPED);
            return;
        }
        am();
        p();
    }

    /* access modifiers changed from: protected */
    public final boolean a(String str, boolean z2) {
        if (!TextUtils.isEmpty(P().j())) {
            str = P().j();
            z2 = true;
        }
        this.U = ah() ? VideoClickedTrackingParams.ClickOrigin.POSTROLL : VideoClickedTrackingParams.ClickOrigin.VIDEO;
        if (this.U == VideoClickedTrackingParams.ClickOrigin.VIDEO) {
            a(VideoFinishedReason.CLICKED);
        }
        a(P().h().n(), new VideoClickedTrackingParams(m(), h(this.A), this.i, this.U, this.P), this.A, "clicked");
        return super.a(str, z2);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z2) {
        ActionTrackingLink[] actionTrackingLinkArr;
        if (this.l != null) {
            if (z2) {
                actionTrackingLinkArr = P().h().f();
            } else {
                actionTrackingLinkArr = P().h().d();
            }
            a(actionTrackingLinkArr, new VideoTrackingParams(m(), h(this.l.d()), this.i, this.P), this.l.d(), "sound");
            Banner3DSize banner3DSize = this.W;
            if (banner3DSize != null) {
                banner3DSize.a(z2 ? 0.0f : 1.0f);
            }
        }
    }

    private void a(VideoTrackingLink[] videoTrackingLinkArr, VideoTrackingParams videoTrackingParams, int i, String str) {
        VideoUtil.a((Context) c(), new com.startapp.sdk.ads.video.a.b(videoTrackingLinkArr, videoTrackingParams, P().a(), i).a(str).a());
    }

    public final void a() {
        this.D = true;
        if (this.C && an()) {
            I();
        }
    }
}
