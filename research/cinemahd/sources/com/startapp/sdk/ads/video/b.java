package com.startapp.sdk.ads.video;

import android.content.Context;
import android.text.TextUtils;
import com.facebook.ads.AudienceNetworkActivity;
import com.startapp.common.ThreadManager;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.video.VideoUtil;
import com.startapp.sdk.ads.video.c;
import com.startapp.sdk.ads.video.g;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.e.a;

public final class b extends a {
    private long i = System.currentTimeMillis();
    private volatile CacheKey j;
    private com.startapp.sdk.ads.video.vast.model.a k;
    private int l = 0;

    public b(Context context, Ad ad, AdPreferences adPreferences, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        super(context, ad, adPreferences, bVar, AdPreferences.Placement.INAPP_OVERLAY, true);
        com.startapp.sdk.ads.video.vast.model.a aVar;
        if (AdsCommonMetaData.a().I().r() == 0) {
            aVar = new com.startapp.sdk.ads.video.vast.model.a(context);
        } else {
            aVar = new com.startapp.sdk.ads.video.vast.model.b(context, AdsCommonMetaData.a().I().s());
        }
        this.k = aVar;
    }

    private boolean g() {
        return b() != null;
    }

    /* access modifiers changed from: protected */
    public final void b(Boolean bool) {
        if (!g()) {
            super.b(bool);
        }
    }

    /* access modifiers changed from: package-private */
    public final VideoAdDetails b() {
        return ((VideoEnabledAd) this.b).g();
    }

    private void b(boolean z) {
        AdPreferences adPreferences;
        if ((this.b.getType() != Ad.AdType.REWARDED_VIDEO && this.b.getType() != Ad.AdType.VIDEO) || z) {
            AdPreferences adPreferences2 = this.c;
            if (adPreferences2 == null) {
                adPreferences = new AdPreferences();
            } else {
                adPreferences = new AdPreferences(adPreferences2);
            }
            adPreferences.setType((this.b.getType() == Ad.AdType.REWARDED_VIDEO || this.b.getType() == Ad.AdType.VIDEO) ? Ad.AdType.VIDEO_NO_VAST : Ad.AdType.NON_VIDEO);
            CacheKey a2 = com.startapp.sdk.adsbase.cache.a.a().a(this.f6335a, (StartAppAd) null, this.e, adPreferences, (com.startapp.sdk.adsbase.adlisteners.b) null);
            if (z) {
                this.j = a2;
            }
        }
    }

    /* access modifiers changed from: protected */
    public final boolean a(Object obj) {
        e.a aVar = (e.a) obj;
        String str = null;
        String b = aVar != null ? aVar.b() : null;
        boolean z = false;
        boolean z2 = true;
        if (b == null || !b.toLowerCase().contains("json")) {
            if (aVar != null) {
                str = aVar.a();
            }
            if (AdsCommonMetaData.a().I().h()) {
                if (u.a(str, "@videoJson@", "@videoJson@") == null) {
                    z2 = false;
                }
                if (z2) {
                    b(false);
                }
            }
            return super.a(obj);
        }
        if (AdsCommonMetaData.a().I().h() && !this.h.i()) {
            b(true);
        }
        try {
            d dVar = (d) com.startapp.common.parser.b.a(aVar.a(), d.class);
            if (!(dVar == null || dVar.getVastTag() == null)) {
                String t = MetaData.G().t();
                com.startapp.sdk.ads.video.vast.a.b bVar = new com.startapp.sdk.ads.video.vast.a.b(AdsCommonMetaData.a().I().n(), AdsCommonMetaData.a().I().o(), (!dVar.isRecordHops() || TextUtils.isEmpty(t)) ? null : new com.startapp.sdk.ads.video.vast.a.a(this.f6335a, t, dVar.getPartnerResponse(), dVar.getPartnerName(), dVar.isSkipFailed()));
                VASTErrorCodes a2 = bVar.a(this.f6335a, dVar.getVastTag(), this.k);
                c a3 = bVar.a();
                Ad ad = this.b;
                VideoEnabledAd videoEnabledAd = (VideoEnabledAd) ad;
                if (ad.getType() != Ad.AdType.REWARDED_VIDEO) {
                    z = true;
                }
                videoEnabledAd.a(a3, z);
                if (dVar.getTtlSec() != null) {
                    ((VideoEnabledAd) this.b).c(dVar.getTtlSec());
                }
                if (a2 == VASTErrorCodes.ErrorNone) {
                    a(VASTErrorCodes.SAProcessSuccess);
                    aVar.a(dVar.getAdmTag());
                    aVar.b(AudienceNetworkActivity.WEBVIEW_MIME_TYPE);
                    return super.a((Object) aVar);
                }
                a(a2);
                if (dVar.getCampaignId() != null) {
                    this.g.add(dVar.getCampaignId());
                }
                this.l++;
                ((VideoEnabledAd) this.b).h();
                if (System.currentTimeMillis() - this.i < ((long) AdsCommonMetaData.a().I().p()) && this.l <= AdsCommonMetaData.a().I().q()) {
                    return d().booleanValue();
                }
            }
            return a((Throwable) null);
        } catch (Exception e) {
            return a((Throwable) e);
        }
    }

    /* access modifiers changed from: protected */
    public final void a(final Boolean bool) {
        super.a(bool);
        if (!bool.booleanValue() || !g()) {
            a(bool.booleanValue());
            return;
        }
        if (AdsCommonMetaData.a().I().i()) {
            super.b(bool);
        }
        b().a(this.c.isVideoMuted());
        AnonymousClass1 r6 = new g.a() {
            public final void a(String str) {
                if (str != null) {
                    if (!str.equals("downloadInterrupted")) {
                        b.super.b(bool);
                        b.this.b().a(str);
                    }
                    b.this.a(bool.booleanValue());
                    return;
                }
                b.this.a(false);
                b.this.d.b(b.this.b);
                b.this.a(VASTErrorCodes.FileNotFound);
            }
        };
        AnonymousClass2 r7 = new c.a() {
            public final void a(String str) {
                b.this.b().a(str);
            }
        };
        ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable(this.f6335a.getApplicationContext(), b().a(), r6, r7) {

            /* renamed from: a  reason: collision with root package name */
            private /* synthetic */ Context f6219a;
            private /* synthetic */ String b;
            private /* synthetic */ g.a c;
            private /* synthetic */ c.a d;

            {
                this.f6219a = r2;
                this.b = r3;
                this.c = r4;
                this.d = r5;
            }

            public final void run() {
                e.a(e.this, this.f6219a, this.b, this.c, this.d);
            }
        });
    }

    /* access modifiers changed from: protected */
    public final boolean a(GetAdRequest getAdRequest) {
        VideoUtil.VideoEligibility a2;
        if (!super.a(getAdRequest)) {
            return false;
        }
        if (!getAdRequest.j() || (a2 = VideoUtil.a(this.f6335a)) == VideoUtil.VideoEligibility.ELIGIBLE) {
            return true;
        }
        this.f = a2.a();
        return false;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest a() {
        GetAdRequest b = b((GetAdRequest) new a());
        if (b != null) {
            b.a(this.f6335a);
        }
        return b;
    }

    /* JADX WARNING: type inference failed for: r1v8, types: [com.startapp.sdk.ads.video.tracking.VideoTrackingLink[]] */
    /* access modifiers changed from: private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r8) {
        /*
            r7 = this;
            r0 = 0
            com.startapp.sdk.ads.video.VideoAdDetails r1 = r7.b()     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x001d
            com.startapp.sdk.ads.video.VideoAdDetails r1 = r7.b()     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.tracking.VideoTrackingDetails r1 = r1.h()     // Catch:{ all -> 0x0090 }
            if (r1 == 0) goto L_0x001d
            com.startapp.sdk.ads.video.VideoAdDetails r0 = r7.b()     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.tracking.VideoTrackingDetails r0 = r0.h()     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.tracking.ActionTrackingLink[] r0 = r0.o()     // Catch:{ all -> 0x0090 }
        L_0x001d:
            if (r0 == 0) goto L_0x008f
            int r1 = r0.length     // Catch:{ all -> 0x0090 }
            if (r1 <= 0) goto L_0x008f
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r1 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.SAShowBeforeVast     // Catch:{ all -> 0x0090 }
            r2 = 0
            if (r8 == r1) goto L_0x002b
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r1 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.SAProcessSuccess     // Catch:{ all -> 0x0090 }
            if (r8 != r1) goto L_0x0066
        L_0x002b:
            java.util.ArrayList r1 = new java.util.ArrayList     // Catch:{ all -> 0x005b }
            r1.<init>()     // Catch:{ all -> 0x005b }
            int r3 = r0.length     // Catch:{ all -> 0x005b }
            r4 = 0
        L_0x0032:
            if (r4 >= r3) goto L_0x0046
            r5 = r0[r4]     // Catch:{ all -> 0x005b }
            java.lang.String r6 = r5.b()     // Catch:{ all -> 0x005b }
            boolean r6 = com.startapp.sdk.adsbase.j.u.b((java.lang.String) r6)     // Catch:{ all -> 0x005b }
            if (r6 == 0) goto L_0x0043
            r1.add(r5)     // Catch:{ all -> 0x005b }
        L_0x0043:
            int r4 = r4 + 1
            goto L_0x0032
        L_0x0046:
            int r3 = r1.size()     // Catch:{ all -> 0x005b }
            if (r3 <= 0) goto L_0x005a
            int r3 = r1.size()     // Catch:{ all -> 0x005b }
            com.startapp.sdk.ads.video.tracking.VideoTrackingLink[] r3 = new com.startapp.sdk.ads.video.tracking.VideoTrackingLink[r3]     // Catch:{ all -> 0x005b }
            java.lang.Object[] r1 = r1.toArray(r3)     // Catch:{ all -> 0x005b }
            com.startapp.sdk.ads.video.tracking.VideoTrackingLink[] r1 = (com.startapp.sdk.ads.video.tracking.VideoTrackingLink[]) r1     // Catch:{ all -> 0x005b }
            r0 = r1
            goto L_0x0066
        L_0x005a:
            return
        L_0x005b:
            r1 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0090 }
            r3.<init>((java.lang.Throwable) r1)     // Catch:{ all -> 0x0090 }
            android.content.Context r1 = r7.f6335a     // Catch:{ all -> 0x0090 }
            r3.a((android.content.Context) r1)     // Catch:{ all -> 0x0090 }
        L_0x0066:
            com.startapp.sdk.ads.video.tracking.VideoTrackingParams r1 = new com.startapp.sdk.ads.video.tracking.VideoTrackingParams     // Catch:{ all -> 0x0090 }
            java.lang.String r3 = ""
            java.lang.String r4 = "1"
            r1.<init>(r3, r2, r2, r4)     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.a.b r3 = new com.startapp.sdk.ads.video.a.b     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.VideoAdDetails r4 = r7.b()     // Catch:{ all -> 0x0090 }
            java.lang.String r4 = r4.a()     // Catch:{ all -> 0x0090 }
            r3.<init>(r0, r1, r4, r2)     // Catch:{ all -> 0x0090 }
            java.lang.String r0 = "error"
            com.startapp.sdk.ads.video.a.b r0 = r3.a((java.lang.String) r0)     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.a.b r8 = r0.a((com.startapp.sdk.ads.video.vast.model.VASTErrorCodes) r8)     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.a.a r8 = r8.a()     // Catch:{ all -> 0x0090 }
            android.content.Context r0 = r7.f6335a     // Catch:{ all -> 0x0090 }
            com.startapp.sdk.ads.video.VideoUtil.a((android.content.Context) r0, (com.startapp.sdk.ads.video.a.a) r8)     // Catch:{ all -> 0x0090 }
        L_0x008f:
            return
        L_0x0090:
            r8 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r8)
            android.content.Context r8 = r7.f6335a
            r0.a((android.content.Context) r8)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.b.a(com.startapp.sdk.ads.video.vast.model.VASTErrorCodes):void");
    }

    private boolean a(Throwable th) {
        if (th != null) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6335a);
        }
        f a2 = com.startapp.sdk.adsbase.cache.a.a().a(this.j);
        if (a2 instanceof HtmlAd) {
            e.a aVar = new e.a();
            aVar.b(AudienceNetworkActivity.WEBVIEW_MIME_TYPE);
            aVar.a(((HtmlAd) a2).j());
            return super.a((Object) aVar);
        }
        this.b.setErrorMessage(this.f);
        return false;
    }
}
