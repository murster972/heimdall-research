package com.startapp.sdk.ads.video.a;

import java.util.List;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private List<String> f6210a;
    private String b;

    public a(List<String> list, String str) {
        this.f6210a = list;
        this.b = str;
    }

    public final List<String> a() {
        return this.f6210a;
    }

    public final String toString() {
        return "[VideoEvent: tag=" + this.b + ", fullUrls=" + this.f6210a.toString() + "]";
    }
}
