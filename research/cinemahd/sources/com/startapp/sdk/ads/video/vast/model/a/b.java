package com.startapp.sdk.ads.video.vast.model.a;

import android.text.TextUtils;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private String f6237a;
    private String b;
    private String c;
    private String d;
    private Integer e;
    private Integer f;
    private Integer g;
    private Boolean h;
    private Boolean i;
    private String j;

    private static boolean a(int i2) {
        return i2 > 0 && i2 < 5000;
    }

    public final String a() {
        return this.f6237a;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void c(String str) {
        this.c = str;
    }

    public final void d(String str) {
        this.d = str;
    }

    public final Integer e() {
        return this.g;
    }

    public final boolean f() {
        if (TextUtils.isEmpty(this.d)) {
            return false;
        }
        Integer num = this.g;
        Integer num2 = this.f;
        if (num == null || num2 == null || !a(num.intValue()) || !a(num2.intValue()) || TextUtils.isEmpty(this.f6237a)) {
            return false;
        }
        return true;
    }

    public final String toString() {
        return "MediaFile [url=" + this.f6237a + ", id=" + this.b + ", delivery=" + this.c + ", type=" + this.d + ", bitrate=" + this.e + ", width=" + this.f + ", height=" + this.g + ", scalable=" + this.h + ", maintainAspectRatio=" + this.i + ", apiFramework=" + this.j + "]";
    }

    public final void a(String str) {
        this.f6237a = str;
    }

    public final String b() {
        return this.d;
    }

    public final Integer c() {
        return this.e;
    }

    public final Integer d() {
        return this.f;
    }

    public final void e(String str) {
        this.j = str;
    }

    public final void a(Integer num) {
        this.e = num;
    }

    public final void b(Integer num) {
        this.f = num;
    }

    public final void c(Integer num) {
        this.g = num;
    }

    public final void a(Boolean bool) {
        this.h = bool;
    }

    public final void b(Boolean bool) {
        this.i = bool;
    }
}
