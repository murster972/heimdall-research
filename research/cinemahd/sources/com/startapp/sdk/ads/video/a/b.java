package com.startapp.sdk.ads.video.a;

import android.text.TextUtils;
import com.startapp.common.b.a;
import com.startapp.sdk.ads.video.tracking.VideoTrackingLink;
import com.startapp.sdk.ads.video.tracking.VideoTrackingParams;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.adsbase.j.u;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private VideoTrackingLink[] f6211a;
    private VideoTrackingParams b;
    private String c;
    private int d;
    private String e = "";
    private VASTErrorCodes f;

    public b(VideoTrackingLink[] videoTrackingLinkArr, VideoTrackingParams videoTrackingParams, String str, int i) {
        this.f6211a = videoTrackingLinkArr;
        this.b = videoTrackingParams;
        this.c = str;
        this.d = i;
    }

    public final b a(VASTErrorCodes vASTErrorCodes) {
        this.f = vASTErrorCodes;
        return this;
    }

    public final b a(String str) {
        this.e = str;
        return this;
    }

    public final a a() {
        if (!((this.f6211a == null || this.b == null) ? false : true)) {
            return null;
        }
        ArrayList arrayList = new ArrayList();
        for (VideoTrackingLink videoTrackingLink : this.f6211a) {
            if (videoTrackingLink.b() != null && (this.b.h() <= 0 || videoTrackingLink.c())) {
                StringBuilder sb = new StringBuilder();
                VideoTrackingLink.TrackingSource f2 = videoTrackingLink.f();
                if (f2 == null) {
                    f2 = u.b(videoTrackingLink.b()) ? VideoTrackingLink.TrackingSource.STARTAPP : VideoTrackingLink.TrackingSource.EXTERNAL;
                }
                VideoTrackingParams a2 = this.b.b(f2 == VideoTrackingLink.TrackingSource.STARTAPP).a(videoTrackingLink.c()).a(videoTrackingLink.e());
                String b2 = videoTrackingLink.b();
                String str = this.c;
                String replace = b2.replace("[ASSETURI]", str != null ? TextUtils.htmlEncode(str) : "");
                int i = this.d;
                long convert = TimeUnit.SECONDS.convert((long) i, TimeUnit.MILLISECONDS);
                String replace2 = replace.replace("[CONTENTPLAYHEAD]", TextUtils.htmlEncode(String.format(Locale.US, "%02d:%02d:%02d.%03d", new Object[]{Long.valueOf(convert / 3600), Long.valueOf((convert % 3600) / 60), Long.valueOf(convert % 60), Long.valueOf((long) (i % 1000))}))).replace("[CACHEBUSTING]", TextUtils.htmlEncode(String.valueOf(new SecureRandom().nextInt(90000000) + 10000000)));
                String format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.US).format(new Date());
                int length = format.length() - 2;
                String replace3 = replace2.replace("[TIMESTAMP]", TextUtils.htmlEncode(format.substring(0, length) + ":" + format.substring(length)));
                VASTErrorCodes vASTErrorCodes = this.f;
                if (vASTErrorCodes != null) {
                    replace3 = replace3.replace("[ERRORCODE]", String.valueOf(vASTErrorCodes.a()));
                }
                sb.append(replace3);
                sb.append(a2.a());
                if (a2.c()) {
                    sb.append(a.a(com.startapp.sdk.adsbase.a.e(b2)));
                }
                arrayList.add(sb.toString());
            }
        }
        return new a(arrayList, this.e);
    }
}
