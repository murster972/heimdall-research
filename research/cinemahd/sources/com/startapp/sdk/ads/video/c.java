package com.startapp.sdk.ads.video;

import com.startapp.sdk.ads.video.player.VideoPlayerInterface;
import java.io.File;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private boolean f6214a;
    /* access modifiers changed from: private */
    public VideoPlayerInterface.c b;
    private String c;

    public interface a {
        void a(String str);
    }

    static class b {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final c f6217a = new c((byte) 0);
    }

    /* synthetic */ c(byte b2) {
        this();
    }

    public static boolean b(String str) {
        return str != null && str.endsWith(".temp");
    }

    public static void c(String str) {
        if (b(str)) {
            new File(str).delete();
        }
    }

    private c() {
        this.f6214a = true;
        this.b = null;
        this.c = null;
    }

    public static c a() {
        return b.f6217a;
    }

    public final void a(VideoPlayerInterface.c cVar) {
        this.b = cVar;
    }

    /* JADX WARNING: type inference failed for: r7v0, types: [java.io.DataInputStream, java.io.FileOutputStream, java.lang.String, java.io.InputStream] */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0116, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:55:0x0118, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x0119, code lost:
        r7 = r10;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x0116 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:13:0x0051] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.lang.String a(android.content.Context r22, java.net.URL r23, java.lang.String r24, com.startapp.sdk.ads.video.c.a r25) {
        /*
            r21 = this;
            r1 = r21
            r2 = r22
            r0 = r24
            r3 = r25
            java.lang.String r4 = ".temp"
            java.lang.String r5 = r23.toString()
            r1.c = r5
            r5 = 1
            r1.f6214a = r5
            com.startapp.sdk.adsbase.AdsCommonMetaData r6 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            com.startapp.sdk.adsbase.VideoConfig r6 = r6.I()
            int r6 = r6.l()
            r7 = 0
            java.lang.String r8 = com.startapp.sdk.ads.video.VideoUtil.a((android.content.Context) r2, (java.lang.String) r0)     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            java.io.File r9 = new java.io.File     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            r9.<init>(r8)     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            boolean r10 = r9.exists()     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            if (r10 == 0) goto L_0x003b
            r1.c = r7     // Catch:{ Exception -> 0x003a }
            r7.close()     // Catch:{ Exception -> 0x003a }
            r7.close()     // Catch:{ Exception -> 0x003a }
            r7.close()     // Catch:{ Exception -> 0x003a }
        L_0x003a:
            return r8
        L_0x003b:
            java.net.URLConnection r10 = r23.openConnection()     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            r10.connect()     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            int r11 = r10.getContentLength()     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            java.io.InputStream r10 = r10.getInputStream()     // Catch:{ Exception -> 0x0125, all -> 0x0120 }
            java.io.DataInputStream r12 = new java.io.DataInputStream     // Catch:{ Exception -> 0x011d, all -> 0x011b }
            r12.<init>(r10)     // Catch:{ Exception -> 0x011d, all -> 0x011b }
            r13 = 4096(0x1000, float:5.74E-42)
            byte[] r13 = new byte[r13]     // Catch:{ Exception -> 0x0118, all -> 0x0116 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0118, all -> 0x0116 }
            r14.<init>()     // Catch:{ Exception -> 0x0118, all -> 0x0116 }
            r14.append(r0)     // Catch:{ Exception -> 0x0118, all -> 0x0116 }
            r14.append(r4)     // Catch:{ Exception -> 0x0118, all -> 0x0116 }
            java.lang.String r14 = r14.toString()     // Catch:{ Exception -> 0x0118, all -> 0x0116 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0113, all -> 0x0116 }
            r0.<init>()     // Catch:{ Exception -> 0x0113, all -> 0x0116 }
            r0.append(r8)     // Catch:{ Exception -> 0x0113, all -> 0x0116 }
            r0.append(r4)     // Catch:{ Exception -> 0x0113, all -> 0x0116 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0113, all -> 0x0116 }
            r4 = 0
            java.io.FileOutputStream r15 = r2.openFileOutput(r14, r4)     // Catch:{ Exception -> 0x0113, all -> 0x0116 }
            r16 = 0
            r17 = 0
            r18 = 0
        L_0x007c:
            int r5 = r12.read(r13)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            if (r5 <= 0) goto L_0x00d2
            boolean r7 = r1.f6214a     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            if (r7 == 0) goto L_0x00d2
            r15.write(r13, r4, r5)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            int r5 = r16 + r5
            r16 = r8
            double r7 = (double) r5     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r19 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r7 = r7 * r19
            r24 = r5
            double r4 = (double) r11     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            double r7 = r7 / r4
            int r4 = (int) r7     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            if (r4 < r6) goto L_0x00cb
            if (r17 != 0) goto L_0x00b0
            if (r3 == 0) goto L_0x00b0
            android.os.Handler r5 = new android.os.Handler     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            android.os.Looper r7 = android.os.Looper.getMainLooper()     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r5.<init>(r7)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            com.startapp.sdk.ads.video.c$1 r7 = new com.startapp.sdk.ads.video.c$1     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r7.<init>(r3, r0)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r5.post(r7)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r17 = 1
        L_0x00b0:
            int r5 = r18 + 1
            if (r4 < r5) goto L_0x00cb
            com.startapp.sdk.ads.video.player.VideoPlayerInterface$c r5 = r1.b     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            if (r5 == 0) goto L_0x00c9
            android.os.Handler r5 = new android.os.Handler     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            android.os.Looper r7 = android.os.Looper.getMainLooper()     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r5.<init>(r7)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            com.startapp.sdk.ads.video.c$2 r7 = new com.startapp.sdk.ads.video.c$2     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r7.<init>(r4)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r5.post(r7)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
        L_0x00c9:
            r18 = r4
        L_0x00cb:
            r8 = r16
            r4 = 0
            r7 = 0
            r16 = r24
            goto L_0x007c
        L_0x00d2:
            r16 = r8
            boolean r0 = r1.f6214a     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            if (r0 != 0) goto L_0x00f5
            if (r5 <= 0) goto L_0x00f5
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            java.lang.String r3 = com.startapp.sdk.ads.video.VideoUtil.a((android.content.Context) r2, (java.lang.String) r14)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r0.delete()     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            java.lang.String r0 = "downloadInterrupted"
            r2 = 0
            r1.c = r2     // Catch:{ Exception -> 0x00f4 }
            r10.close()     // Catch:{ Exception -> 0x00f4 }
            r12.close()     // Catch:{ Exception -> 0x00f4 }
            r15.close()     // Catch:{ Exception -> 0x00f4 }
        L_0x00f4:
            return r0
        L_0x00f5:
            java.io.File r0 = new java.io.File     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            java.lang.String r3 = com.startapp.sdk.ads.video.VideoUtil.a((android.content.Context) r2, (java.lang.String) r14)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r0.<init>(r3)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            a(r0, r9)     // Catch:{ Exception -> 0x0110, all -> 0x010e }
            r2 = 0
            r1.c = r2     // Catch:{ Exception -> 0x014c }
            r10.close()     // Catch:{ Exception -> 0x014c }
            r12.close()     // Catch:{ Exception -> 0x014c }
            r15.close()     // Catch:{ Exception -> 0x014c }
            goto L_0x014c
        L_0x010e:
            r0 = move-exception
            goto L_0x014f
        L_0x0110:
            r0 = move-exception
            r7 = r10
            goto L_0x012a
        L_0x0113:
            r0 = move-exception
            r7 = r10
            goto L_0x0129
        L_0x0116:
            r0 = move-exception
            goto L_0x0123
        L_0x0118:
            r0 = move-exception
            r7 = r10
            goto L_0x0128
        L_0x011b:
            r0 = move-exception
            goto L_0x0122
        L_0x011d:
            r0 = move-exception
            r7 = r10
            goto L_0x0127
        L_0x0120:
            r0 = move-exception
            r10 = 0
        L_0x0122:
            r12 = 0
        L_0x0123:
            r15 = 0
            goto L_0x014f
        L_0x0125:
            r0 = move-exception
            r7 = 0
        L_0x0127:
            r12 = 0
        L_0x0128:
            r14 = 0
        L_0x0129:
            r15 = 0
        L_0x012a:
            java.io.File r3 = new java.io.File     // Catch:{ all -> 0x014d }
            java.lang.String r4 = com.startapp.sdk.ads.video.VideoUtil.a((android.content.Context) r2, (java.lang.String) r14)     // Catch:{ all -> 0x014d }
            r3.<init>(r4)     // Catch:{ all -> 0x014d }
            r3.delete()     // Catch:{ all -> 0x014d }
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x014d }
            r3.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x014d }
            r3.a((android.content.Context) r2)     // Catch:{ all -> 0x014d }
            r2 = 0
            r1.c = r2     // Catch:{ Exception -> 0x014a }
            r7.close()     // Catch:{ Exception -> 0x014a }
            r12.close()     // Catch:{ Exception -> 0x014a }
            r15.close()     // Catch:{ Exception -> 0x014a }
        L_0x014a:
            r16 = 0
        L_0x014c:
            return r16
        L_0x014d:
            r0 = move-exception
            r10 = r7
        L_0x014f:
            r2 = 0
            r1.c = r2     // Catch:{ Exception -> 0x015b }
            r10.close()     // Catch:{ Exception -> 0x015b }
            r12.close()     // Catch:{ Exception -> 0x015b }
            r15.close()     // Catch:{ Exception -> 0x015b }
        L_0x015b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.c.a(android.content.Context, java.net.URL, java.lang.String, com.startapp.sdk.ads.video.c$a):java.lang.String");
    }

    public final void a(String str) {
        if (str != null && str.equals(this.c)) {
            this.f6214a = false;
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0032 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(java.io.File r3, java.io.File r4) {
        /*
            r0 = 0
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0031, all -> 0x0028 }
            r1.<init>(r3)     // Catch:{ Exception -> 0x0031, all -> 0x0028 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0032, all -> 0x0026 }
            r3.<init>(r4)     // Catch:{ Exception -> 0x0032, all -> 0x0026 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
        L_0x000f:
            int r0 = r1.read(r4)     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            if (r0 <= 0) goto L_0x001a
            r2 = 0
            r3.write(r4, r2, r0)     // Catch:{ Exception -> 0x0024, all -> 0x0021 }
            goto L_0x000f
        L_0x001a:
            r1.close()     // Catch:{ Exception -> 0x0020 }
            r3.close()     // Catch:{ Exception -> 0x0020 }
        L_0x0020:
            return
        L_0x0021:
            r4 = move-exception
            r0 = r3
            goto L_0x002a
        L_0x0024:
            r0 = r3
            goto L_0x0032
        L_0x0026:
            r4 = move-exception
            goto L_0x002a
        L_0x0028:
            r4 = move-exception
            r1 = r0
        L_0x002a:
            r1.close()     // Catch:{ Exception -> 0x0030 }
            r0.close()     // Catch:{ Exception -> 0x0030 }
        L_0x0030:
            throw r4
        L_0x0031:
            r1 = r0
        L_0x0032:
            r1.close()     // Catch:{ Exception -> 0x0038 }
            r0.close()     // Catch:{ Exception -> 0x0038 }
        L_0x0038:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.c.a(java.io.File, java.io.File):void");
    }
}
