package com.startapp.sdk.ads.video;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.ViewParent;
import com.iab.omid.library.startapp.adsession.b;
import com.iab.omid.library.startapp.b.a;
import com.iab.omid.library.startapp.d.c;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.activities.FullScreenActivity;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.u;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public final class VideoUtil {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<View, String> f6208a = new HashMap<>();
    private final HashMap<View, ArrayList<String>> b = new HashMap<>();
    private final HashSet<View> c = new HashSet<>();
    private final HashSet<String> d = new HashSet<>();
    private final HashSet<String> e = new HashSet<>();
    private boolean f;

    public enum VideoEligibility {
        ELIGIBLE(""),
        INELIGIBLE_NO_STORAGE("Not enough storage for video"),
        INELIGIBLE_MISSING_ACTIVITY("FullScreenActivity not declared in AndroidManifest.xml"),
        INELIGIBLE_ERRORS_THRESHOLD_REACHED("Video errors threshold reached.");
        
        private String desctiption;

        private VideoEligibility(String str) {
            this.desctiption = str;
        }

        public final String a() {
            return this.desctiption;
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v0, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v0, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v1, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v3, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v4, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v4, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v7, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v7, resolved type: java.io.FileOutputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v9, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v10, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v11, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v12, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v13, resolved type: java.io.DataInputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v14, resolved type: java.io.DataInputStream} */
    /* JADX WARNING: type inference failed for: r1v0, types: [java.io.DataInputStream, java.io.FileOutputStream, java.lang.String, java.io.InputStream] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r10, java.net.URL r11, java.lang.String r12) {
        /*
            java.lang.String r0 = ".temp"
            r1 = 0
            java.lang.String r2 = a((android.content.Context) r10, (java.lang.String) r12)     // Catch:{ Exception -> 0x0080, all -> 0x007b }
            java.io.File r3 = new java.io.File     // Catch:{ Exception -> 0x0080, all -> 0x007b }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0080, all -> 0x007b }
            boolean r4 = r3.exists()     // Catch:{ Exception -> 0x0080, all -> 0x007b }
            if (r4 == 0) goto L_0x001c
            r1.close()     // Catch:{ Exception -> 0x001b }
            r1.close()     // Catch:{ Exception -> 0x001b }
            r1.close()     // Catch:{ Exception -> 0x001b }
        L_0x001b:
            return r2
        L_0x001c:
            java.io.InputStream r4 = r11.openStream()     // Catch:{ Exception -> 0x0080, all -> 0x007b }
            java.io.DataInputStream r5 = new java.io.DataInputStream     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            r5.<init>(r4)     // Catch:{ Exception -> 0x0078, all -> 0x0075 }
            r6 = 4096(0x1000, float:5.74E-42)
            byte[] r6 = new byte[r6]     // Catch:{ Exception -> 0x0072, all -> 0x006f }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0072, all -> 0x006f }
            r7.<init>()     // Catch:{ Exception -> 0x0072, all -> 0x006f }
            r7.append(r12)     // Catch:{ Exception -> 0x0072, all -> 0x006f }
            r7.append(r0)     // Catch:{ Exception -> 0x0072, all -> 0x006f }
            java.lang.String r7 = r7.toString()     // Catch:{ Exception -> 0x0072, all -> 0x006f }
            r8 = 0
            java.io.FileOutputStream r7 = r10.openFileOutput(r7, r8)     // Catch:{ Exception -> 0x0072, all -> 0x006f }
        L_0x003d:
            int r9 = r5.read(r6)     // Catch:{ Exception -> 0x006d }
            if (r9 <= 0) goto L_0x0047
            r7.write(r6, r8, r9)     // Catch:{ Exception -> 0x006d }
            goto L_0x003d
        L_0x0047:
            java.io.File r6 = new java.io.File     // Catch:{ Exception -> 0x006d }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x006d }
            r8.<init>()     // Catch:{ Exception -> 0x006d }
            r8.append(r12)     // Catch:{ Exception -> 0x006d }
            r8.append(r0)     // Catch:{ Exception -> 0x006d }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x006d }
            java.lang.String r8 = a((android.content.Context) r10, (java.lang.String) r8)     // Catch:{ Exception -> 0x006d }
            r6.<init>(r8)     // Catch:{ Exception -> 0x006d }
            r6.renameTo(r3)     // Catch:{ Exception -> 0x006d }
            r4.close()     // Catch:{ Exception -> 0x006b }
            r5.close()     // Catch:{ Exception -> 0x006b }
            r7.close()     // Catch:{ Exception -> 0x006b }
        L_0x006b:
            r1 = r2
            goto L_0x00b7
        L_0x006d:
            r2 = move-exception
            goto L_0x0084
        L_0x006f:
            r10 = move-exception
            r7 = r1
            goto L_0x00b9
        L_0x0072:
            r2 = move-exception
            r7 = r1
            goto L_0x0084
        L_0x0075:
            r10 = move-exception
            r5 = r1
            goto L_0x007e
        L_0x0078:
            r2 = move-exception
            r5 = r1
            goto L_0x0083
        L_0x007b:
            r10 = move-exception
            r4 = r1
            r5 = r4
        L_0x007e:
            r7 = r5
            goto L_0x00b9
        L_0x0080:
            r2 = move-exception
            r4 = r1
            r5 = r4
        L_0x0083:
            r7 = r5
        L_0x0084:
            java.lang.String r3 = "StartAppWall.VideoUtil"
            java.lang.String r6 = "Error downloading video from "
            java.lang.String r11 = java.lang.String.valueOf(r11)     // Catch:{ all -> 0x00b8 }
            java.lang.String r11 = r6.concat(r11)     // Catch:{ all -> 0x00b8 }
            android.util.Log.e(r3, r11, r2)     // Catch:{ all -> 0x00b8 }
            java.io.File r11 = new java.io.File     // Catch:{ all -> 0x00b8 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b8 }
            r2.<init>()     // Catch:{ all -> 0x00b8 }
            r2.append(r12)     // Catch:{ all -> 0x00b8 }
            r2.append(r0)     // Catch:{ all -> 0x00b8 }
            java.lang.String r12 = r2.toString()     // Catch:{ all -> 0x00b8 }
            java.lang.String r10 = a((android.content.Context) r10, (java.lang.String) r12)     // Catch:{ all -> 0x00b8 }
            r11.<init>(r10)     // Catch:{ all -> 0x00b8 }
            r11.delete()     // Catch:{ all -> 0x00b8 }
            r4.close()     // Catch:{ Exception -> 0x00b7 }
            r5.close()     // Catch:{ Exception -> 0x00b7 }
            r7.close()     // Catch:{ Exception -> 0x00b7 }
        L_0x00b7:
            return r1
        L_0x00b8:
            r10 = move-exception
        L_0x00b9:
            r4.close()     // Catch:{ Exception -> 0x00c2 }
            r5.close()     // Catch:{ Exception -> 0x00c2 }
            r7.close()     // Catch:{ Exception -> 0x00c2 }
        L_0x00c2:
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.VideoUtil.a(android.content.Context, java.net.URL, java.lang.String):java.lang.String");
    }

    public static void b(Context context) {
        j.b(context, "videoErrorsCount", Integer.valueOf(j.a(context, "videoErrorsCount", (Integer) 0).intValue() + 1));
    }

    public final void c() {
        a c2 = a.c();
        if (c2 != null) {
            for (b next : c2.b()) {
                View g = next.g();
                if (next.h()) {
                    if (g != null) {
                        boolean z = false;
                        if (g.hasWindowFocus()) {
                            HashSet hashSet = new HashSet();
                            View view = g;
                            while (true) {
                                if (view != null) {
                                    if (!c.c(view)) {
                                        break;
                                    }
                                    hashSet.add(view);
                                    ViewParent parent = view.getParent();
                                    view = parent instanceof View ? (View) parent : null;
                                } else {
                                    this.c.addAll(hashSet);
                                    z = true;
                                    break;
                                }
                            }
                        }
                        if (z) {
                            this.d.add(next.f());
                            this.f6208a.put(g, next.f());
                            a(next);
                        }
                    }
                    this.e.add(next.f());
                }
            }
        }
    }

    public final void d() {
        this.f6208a.clear();
        this.b.clear();
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f = false;
    }

    public final void e() {
        this.f = true;
    }

    public final HashSet<String> b() {
        return this.e;
    }

    public final ArrayList<String> b(View view) {
        if (this.b.size() == 0) {
            return null;
        }
        ArrayList<String> arrayList = this.b.get(view);
        if (arrayList != null) {
            this.b.remove(view);
            Collections.sort(arrayList);
        }
        return arrayList;
    }

    public final com.iab.omid.library.startapp.walking.c c(View view) {
        if (this.c.contains(view)) {
            return com.iab.omid.library.startapp.walking.c.PARENT_VIEW;
        }
        return this.f ? com.iab.omid.library.startapp.walking.c.OBSTRUCTION_VIEW : com.iab.omid.library.startapp.walking.c.UNDERLYING_VIEW;
    }

    public static VideoEligibility a(Context context) {
        VideoEligibility videoEligibility = VideoEligibility.ELIGIBLE;
        boolean z = true;
        if (AdsCommonMetaData.a().I().e() >= 0 && j.a(context, "videoErrorsCount", (Integer) 0).intValue() >= AdsCommonMetaData.a().I().e()) {
            videoEligibility = VideoEligibility.INELIGIBLE_ERRORS_THRESHOLD_REACHED;
        }
        if (!u.a(context, (Class<? extends Activity>) FullScreenActivity.class)) {
            videoEligibility = VideoEligibility.INELIGIBLE_MISSING_ACTIVITY;
        }
        long a2 = u.a(context.getFilesDir());
        if (a2 < 0 || a2 / 1024 <= (AdsCommonMetaData.a().I().c() << 10)) {
            z = false;
        }
        return !z ? VideoEligibility.INELIGIBLE_NO_STORAGE : videoEligibility;
    }

    public static void a(Context context, com.startapp.sdk.ads.video.a.a aVar) {
        if (aVar != null) {
            for (String b2 : aVar.a()) {
                com.startapp.sdk.adsbase.a.b(context, b2);
            }
        }
    }

    static String a(Context context, String str) {
        return context.getFilesDir() + "/" + str;
    }

    public final HashSet<String> a() {
        return this.d;
    }

    private void a(b bVar) {
        for (com.iab.omid.library.startapp.e.a aVar : bVar.c()) {
            View view = (View) aVar.get();
            if (view != null) {
                ArrayList arrayList = this.b.get(view);
                if (arrayList == null) {
                    arrayList = new ArrayList();
                    this.b.put(view, arrayList);
                }
                arrayList.add(bVar.f());
            }
        }
    }

    public final String a(View view) {
        if (this.f6208a.size() == 0) {
            return null;
        }
        String str = this.f6208a.get(view);
        if (str != null) {
            this.f6208a.remove(view);
        }
        return str;
    }
}
