package com.startapp.sdk.ads.video.tracking;

public class VideoClickedTrackingParams extends VideoTrackingParams {
    private static final long serialVersionUID = 1;
    private ClickOrigin clickOrigin;

    public enum ClickOrigin {
        POSTROLL,
        VIDEO
    }

    public VideoClickedTrackingParams(String str, int i, int i2, ClickOrigin clickOrigin2, String str2) {
        super(str, i, i2, str2);
        this.clickOrigin = clickOrigin2;
    }

    public final String a() {
        StringBuilder sb = new StringBuilder();
        sb.append(b());
        sb.append("&co=" + this.clickOrigin.toString());
        sb.append(e());
        return b(sb.toString());
    }
}
