package com.startapp.sdk.ads.video.vast.a;

import android.content.Context;
import com.startapp.sdk.ads.video.vast.model.VASTErrorCodes;
import com.startapp.sdk.ads.video.vast.model.a;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.adsbase.m;
import org.w3c.dom.Document;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final int f6231a;
    private final int b;
    private c c;
    private StringBuilder d = new StringBuilder(500);
    private long e = -1;
    private a f;

    public b(int i, int i2, a aVar) {
        this.f6231a = i;
        this.b = i2;
        this.f = aVar;
    }

    public final c a() {
        return this.c;
    }

    public final VASTErrorCodes a(Context context, String str, a aVar) {
        Document document = null;
        this.c = null;
        this.e = System.currentTimeMillis();
        VASTErrorCodes a2 = a(context, str, 0);
        if (a2 == VASTErrorCodes.XMLParsingError) {
            a aVar2 = this.f;
            if (aVar2 != null) {
                aVar2.a(a2);
            }
            return a2;
        }
        String sb = this.d.toString();
        if (sb != null && sb.length() > 0) {
            document = m.a("<VASTS>" + sb + "</VASTS>");
        }
        if (document == null) {
            a aVar3 = this.f;
            if (aVar3 != null) {
                aVar3.a(VASTErrorCodes.XMLParsingError);
            }
            return VASTErrorCodes.XMLParsingError;
        }
        this.c = new c(document);
        if (!this.c.a(aVar) && a2 == VASTErrorCodes.ErrorNone) {
            a2 = VASTErrorCodes.MediaNotSupported;
        }
        a aVar4 = this.f;
        if (aVar4 != null) {
            aVar4.a(a2);
        }
        return a2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:21:0x003b, code lost:
        r2 = r13.getElementsByTagName("VAST");
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.startapp.sdk.ads.video.vast.model.VASTErrorCodes a(android.content.Context r12, java.lang.String r13, int r14) {
        /*
            r11 = this;
            com.startapp.sdk.ads.video.vast.a.a r0 = r11.f
            if (r0 == 0) goto L_0x0007
            r0.a((java.lang.String) r13)
        L_0x0007:
            int r0 = r11.f6231a
            if (r14 < r0) goto L_0x000e
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.WrapperLimitReached
            return r12
        L_0x000e:
            long r0 = java.lang.System.currentTimeMillis()
            long r2 = r11.e
            long r0 = r0 - r2
            int r4 = r11.b
            long r4 = (long) r4
            int r6 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r6 <= 0) goto L_0x0025
            r0 = 0
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 <= 0) goto L_0x0025
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.WrapperTimeout
            return r12
        L_0x0025:
            org.w3c.dom.Document r13 = com.startapp.sdk.adsbase.m.a((java.lang.String) r13)
            if (r13 == 0) goto L_0x0032
            org.w3c.dom.Element r0 = r13.getDocumentElement()
            r0.normalize()
        L_0x0032:
            if (r13 != 0) goto L_0x0037
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.XMLParsingError
            return r12
        L_0x0037:
            r0 = 0
            r1 = 0
            if (r13 == 0) goto L_0x0052
            java.lang.String r2 = "VAST"
            org.w3c.dom.NodeList r2 = r13.getElementsByTagName(r2)
            if (r2 == 0) goto L_0x0052
            int r3 = r2.getLength()
            if (r3 <= 0) goto L_0x0052
            org.w3c.dom.Node r2 = r2.item(r1)
            java.lang.String r2 = com.startapp.sdk.adsbase.m.a((org.w3c.dom.Node) r2)
            goto L_0x0053
        L_0x0052:
            r2 = r0
        L_0x0053:
            org.w3c.dom.NodeList r3 = r13.getChildNodes()
            int r3 = r3.getLength()
            if (r3 == 0) goto L_0x00df
            org.w3c.dom.NodeList r3 = r13.getChildNodes()
            org.w3c.dom.Node r3 = r3.item(r1)
            org.w3c.dom.NodeList r3 = r3.getChildNodes()
            int r3 = r3.getLength()
            if (r3 == 0) goto L_0x00df
            if (r2 != 0) goto L_0x0072
            goto L_0x00df
        L_0x0072:
            java.lang.StringBuilder r3 = r11.d
            r3.append(r2)
            java.lang.String r2 = "VASTAdTagURI"
            org.w3c.dom.NodeList r13 = r13.getElementsByTagName(r2)
            if (r13 == 0) goto L_0x00dc
            int r2 = r13.getLength()
            if (r2 != 0) goto L_0x0086
            goto L_0x00dc
        L_0x0086:
            org.w3c.dom.Node r13 = r13.item(r1)
            java.lang.String r13 = com.startapp.sdk.adsbase.m.b(r13)
            java.net.URL r2 = new java.net.URL     // Catch:{ Exception -> 0x00d9 }
            r2.<init>(r13)     // Catch:{ Exception -> 0x00d9 }
            java.net.URI r13 = new java.net.URI     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r4 = r2.getProtocol()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r5 = r2.getUserInfo()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r6 = r2.getHost()     // Catch:{ Exception -> 0x00d9 }
            int r7 = r2.getPort()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r8 = r2.getPath()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r9 = r2.getQuery()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r10 = r2.getRef()     // Catch:{ Exception -> 0x00d9 }
            r3 = r13
            r3.<init>(r4, r5, r6, r7, r8, r9, r10)     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r2 = "User-Agent"
            java.lang.String r3 = "-1"
            java.lang.String r2 = com.startapp.sdk.adsbase.j.a((android.content.Context) r12, (java.lang.String) r2, (java.lang.String) r3)     // Catch:{ Exception -> 0x00d9 }
            com.startapp.common.b.e$a r13 = com.startapp.common.b.e.a(r13, r0, r2, r1)     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r0 = r13.a()     // Catch:{ Exception -> 0x00d9 }
            if (r0 == 0) goto L_0x00d6
            java.lang.String r13 = r13.a()     // Catch:{ Exception -> 0x00d9 }
            int r14 = r14 + 1
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = r11.a((android.content.Context) r12, (java.lang.String) r13, (int) r14)     // Catch:{ Exception -> 0x00d9 }
            return r12
        L_0x00d6:
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.WrapperNoReponse     // Catch:{ Exception -> 0x00d9 }
            return r12
        L_0x00d9:
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.GeneralWrapperError
            return r12
        L_0x00dc:
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.ErrorNone
            return r12
        L_0x00df:
            com.startapp.sdk.ads.video.vast.model.VASTErrorCodes r12 = com.startapp.sdk.ads.video.vast.model.VASTErrorCodes.WrapperNoReponse
            return r12
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.vast.a.b.a(android.content.Context, java.lang.String, int):com.startapp.sdk.ads.video.vast.model.VASTErrorCodes");
    }
}
