package com.startapp.sdk.ads.video;

import com.startapp.common.parser.d;
import com.startapp.sdk.ads.video.tracking.VideoTrackingDetails;
import com.startapp.sdk.ads.video.vast.model.c;
import com.startapp.sdk.omsdk.AdVerification;
import com.startapp.sdk.omsdk.VerificationDetails;
import java.io.Serializable;

public class VideoAdDetails implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = VerificationDetails.class, f = "adVerifications")
    private VerificationDetails[] adVerifications;
    private String clickUrl;
    private boolean clickable;
    private boolean closeable;
    private boolean isVideoMuted;
    private String localVideoPath;
    @d(b = PostRollType.class)
    private PostRollType postRoll;
    private boolean skippable;
    private int skippableAfter;
    @d(a = true)
    private VideoTrackingDetails videoTrackingDetails;
    private String videoUrl;

    public enum PostRollType {
        IMAGE,
        LAST_FRAME,
        NONE
    }

    public VideoAdDetails() {
    }

    public final String a() {
        return this.videoUrl;
    }

    public final String b() {
        return this.localVideoPath;
    }

    public final PostRollType c() {
        return this.postRoll;
    }

    public final boolean d() {
        return this.closeable;
    }

    public final boolean e() {
        return this.skippable;
    }

    public final int f() {
        return this.skippableAfter;
    }

    public final boolean g() {
        return this.clickable;
    }

    public final VideoTrackingDetails h() {
        return this.videoTrackingDetails;
    }

    public final boolean i() {
        return this.isVideoMuted;
    }

    public final String j() {
        return this.clickUrl;
    }

    public final AdVerification k() {
        return new AdVerification(this.adVerifications);
    }

    public String toString() {
        return "VideoAdDetails [videoUrl=" + this.videoUrl + ", localVideoPath=" + this.localVideoPath + ", postRoll=" + this.postRoll + ", closeable=" + this.closeable + ", skippable=" + this.skippable + ", skippableAfter=" + this.skippableAfter + ", videoTrackingDetails= " + this.videoTrackingDetails + ", isVideoMuted= " + this.isVideoMuted + "]";
    }

    public VideoAdDetails(c cVar, boolean z) {
        if (cVar != null) {
            this.videoTrackingDetails = new VideoTrackingDetails(cVar);
            if (cVar.f() != null) {
                this.videoUrl = cVar.f().a();
            }
            boolean z2 = true;
            if (z) {
                this.skippableAfter = cVar.e();
                this.skippable = this.skippableAfter != Integer.MAX_VALUE;
            } else {
                this.skippable = false;
            }
            this.clickUrl = cVar.b().a();
            this.clickable = this.clickUrl == null ? false : z2;
            this.postRoll = PostRollType.NONE;
            AdVerification g = cVar.g();
            if (g != null && g.a() != null) {
                this.adVerifications = (VerificationDetails[]) g.a().toArray(new VerificationDetails[g.a().size()]);
            }
        }
    }

    public final void a(String str) {
        this.localVideoPath = str;
    }

    public final void a(boolean z) {
        this.isVideoMuted = z;
    }
}
