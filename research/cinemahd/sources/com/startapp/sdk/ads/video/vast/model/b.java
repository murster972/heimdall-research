package com.startapp.sdk.ads.video.vast.model;

import android.content.Context;
import java.util.Comparator;

public final class b extends a {
    /* access modifiers changed from: private */
    public final double c;
    /* access modifiers changed from: private */
    public final int d;
    /* access modifiers changed from: private */
    public final int e;

    public b(Context context, int i) {
        super(context);
        this.e = i;
        int i2 = this.f6234a;
        int i3 = this.b;
        this.c = ((double) i2) / ((double) i3);
        this.d = i2 * i3;
    }

    /* access modifiers changed from: protected */
    public final Comparator<com.startapp.sdk.ads.video.vast.model.a.b> a() {
        return new Comparator<com.startapp.sdk.ads.video.vast.model.a.b>() {
            /* JADX WARNING: Code restructure failed: missing block: B:14:0x006c, code lost:
                r8 = java.lang.Integer.valueOf(java.lang.Math.abs(com.startapp.sdk.ads.video.vast.model.b.c(r7.f6241a) - r8.intValue()));
                r9 = java.lang.Integer.valueOf(java.lang.Math.abs(com.startapp.sdk.ads.video.vast.model.b.c(r7.f6241a) - r9.intValue()));
             */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final /* synthetic */ int compare(java.lang.Object r8, java.lang.Object r9) {
                /*
                    r7 = this;
                    com.startapp.sdk.ads.video.vast.model.a.b r8 = (com.startapp.sdk.ads.video.vast.model.a.b) r8
                    com.startapp.sdk.ads.video.vast.model.a.b r9 = (com.startapp.sdk.ads.video.vast.model.a.b) r9
                    java.lang.Integer r0 = r8.d()
                    int r0 = r0.intValue()
                    java.lang.Integer r1 = r8.e()
                    int r1 = r1.intValue()
                    com.startapp.sdk.ads.video.vast.model.b r2 = com.startapp.sdk.ads.video.vast.model.b.this
                    double r2 = r2.c
                    com.startapp.sdk.ads.video.vast.model.b r4 = com.startapp.sdk.ads.video.vast.model.b.this
                    int r4 = r4.d
                    java.lang.Double r0 = java.lang.Double.valueOf((java.lang.Math.abs(java.lang.Math.log((((double) r0) / ((double) r1)) / r2)) * 40.0d) + (java.lang.Math.abs(java.lang.Math.log(((double) (r0 * r1)) / ((double) r4))) * 60.0d))
                    double r0 = r0.doubleValue()
                    java.lang.Integer r2 = r9.d()
                    int r2 = r2.intValue()
                    java.lang.Integer r3 = r9.e()
                    int r3 = r3.intValue()
                    com.startapp.sdk.ads.video.vast.model.b r4 = com.startapp.sdk.ads.video.vast.model.b.this
                    double r4 = r4.c
                    com.startapp.sdk.ads.video.vast.model.b r6 = com.startapp.sdk.ads.video.vast.model.b.this
                    int r6 = r6.d
                    java.lang.Double r2 = java.lang.Double.valueOf((java.lang.Math.abs(java.lang.Math.log((((double) r2) / ((double) r3)) / r4)) * 40.0d) + (java.lang.Math.abs(java.lang.Math.log(((double) (r2 * r3)) / ((double) r6))) * 60.0d))
                    double r2 = r2.doubleValue()
                    r4 = -1
                    int r5 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                    if (r5 >= 0) goto L_0x0052
                    return r4
                L_0x0052:
                    r5 = 1
                    int r6 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                    if (r6 <= 0) goto L_0x0058
                    return r5
                L_0x0058:
                    java.lang.Integer r8 = r8.c()
                    java.lang.Integer r9 = r9.c()
                    r0 = 0
                    if (r8 != 0) goto L_0x0066
                    if (r9 != 0) goto L_0x0066
                    return r0
                L_0x0066:
                    if (r8 != 0) goto L_0x0069
                    return r5
                L_0x0069:
                    if (r9 != 0) goto L_0x006c
                    return r4
                L_0x006c:
                    com.startapp.sdk.ads.video.vast.model.b r1 = com.startapp.sdk.ads.video.vast.model.b.this
                    int r1 = r1.e
                    int r8 = r8.intValue()
                    int r1 = r1 - r8
                    int r8 = java.lang.Math.abs(r1)
                    java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
                    com.startapp.sdk.ads.video.vast.model.b r1 = com.startapp.sdk.ads.video.vast.model.b.this
                    int r1 = r1.e
                    int r9 = r9.intValue()
                    int r1 = r1 - r9
                    int r9 = java.lang.Math.abs(r1)
                    java.lang.Integer r9 = java.lang.Integer.valueOf(r9)
                    int r8 = r8.intValue()
                    int r9 = r9.intValue()
                    if (r8 >= r9) goto L_0x009d
                    return r4
                L_0x009d:
                    if (r8 != r9) goto L_0x00a0
                    return r0
                L_0x00a0:
                    return r5
                */
                throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.vast.model.b.AnonymousClass1.compare(java.lang.Object, java.lang.Object):int");
            }
        };
    }
}
