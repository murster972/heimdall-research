package com.startapp.sdk.ads.video.vast.model;

import android.text.TextUtils;
import com.startapp.sdk.ads.video.vast.model.a.a;
import com.startapp.sdk.ads.video.vast.model.a.b;
import com.startapp.sdk.ads.video.vast.model.a.d;
import com.startapp.sdk.ads.video.vast.model.a.e;
import com.startapp.sdk.adsbase.m;
import com.startapp.sdk.omsdk.AdVerification;
import com.startapp.sdk.omsdk.VerificationDetails;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import org.joda.time.DateTimeConstants;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> f6242a;
    private List<b> b;
    private int c;
    private e d;
    private List<String> e;
    private List<String> f;
    private int g;
    private b h = null;
    private List<a> i;
    private AdVerification j;

    public c(Document document) {
        this.c = c(document);
        this.f6242a = a(document);
        this.b = b(document);
        this.d = d(document);
        this.e = a(document, "//Impression");
        this.f = a(document, "//Error");
        this.g = e(document);
        this.i = f(document);
        this.j = g(document);
    }

    public final HashMap<VASTEventType, List<com.startapp.sdk.ads.video.vast.model.a.c>> a() {
        return this.f6242a;
    }

    public final e b() {
        return this.d;
    }

    public final List<String> c() {
        return this.e;
    }

    public final List<String> d() {
        return this.f;
    }

    public final int e() {
        return this.g;
    }

    public final b f() {
        return this.h;
    }

    public final AdVerification g() {
        return this.j;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(10:8|9|10|11|(3:15|16|(1:18)(2:19|20))|21|22|(1:24)(1:25)|26|33) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0068 */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x006e A[Catch:{ Exception -> 0x0089 }] */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0075 A[Catch:{ Exception -> 0x0089 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.HashMap<com.startapp.sdk.ads.video.vast.model.VASTEventType, java.util.List<com.startapp.sdk.ads.video.vast.model.a.c>> a(org.w3c.dom.Document r9) {
        /*
            r8 = this;
            java.lang.String r0 = "%"
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            javax.xml.xpath.XPathFactory r2 = javax.xml.xpath.XPathFactory.newInstance()
            javax.xml.xpath.XPath r2 = r2.newXPath()
            java.lang.String r3 = "/VASTS/VAST/Ad/InLine/Creatives/Creative/Linear/TrackingEvents/Tracking|/VASTS/VAST/Ad/InLine/Creatives/Creative/NonLinearAds/TrackingEvents/Tracking|/VASTS/VAST/Ad/Wrapper/Creatives/Creative/Linear/TrackingEvents/Tracking|/VASTS/VAST/Ad/Wrapper/Creatives/Creative/NonLinearAds/TrackingEvents/Tracking"
            javax.xml.namespace.QName r4 = javax.xml.xpath.XPathConstants.NODESET     // Catch:{ Exception -> 0x0089 }
            java.lang.Object r9 = r2.evaluate(r3, r9, r4)     // Catch:{ Exception -> 0x0089 }
            org.w3c.dom.NodeList r9 = (org.w3c.dom.NodeList) r9     // Catch:{ Exception -> 0x0089 }
            if (r9 == 0) goto L_0x0088
            r2 = 0
        L_0x001c:
            int r3 = r9.getLength()     // Catch:{ Exception -> 0x0089 }
            if (r2 >= r3) goto L_0x0088
            org.w3c.dom.Node r3 = r9.item(r2)     // Catch:{ Exception -> 0x0089 }
            org.w3c.dom.NamedNodeMap r4 = r3.getAttributes()     // Catch:{ Exception -> 0x0089 }
            java.lang.String r5 = "event"
            org.w3c.dom.Node r5 = r4.getNamedItem(r5)     // Catch:{ Exception -> 0x0089 }
            java.lang.String r5 = r5.getNodeValue()     // Catch:{ Exception -> 0x0089 }
            com.startapp.sdk.ads.video.vast.model.VASTEventType r5 = com.startapp.sdk.ads.video.vast.model.VASTEventType.valueOf(r5)     // Catch:{ IllegalArgumentException -> 0x0085 }
            java.lang.String r3 = com.startapp.sdk.adsbase.m.b(r3)     // Catch:{ Exception -> 0x0089 }
            r6 = -1
            java.lang.String r7 = "offset"
            org.w3c.dom.Node r4 = r4.getNamedItem(r7)     // Catch:{ Exception -> 0x0089 }
            if (r4 == 0) goto L_0x0068
            java.lang.String r4 = r4.getNodeValue()     // Catch:{ Exception -> 0x0089 }
            if (r4 == 0) goto L_0x0068
            boolean r7 = r4.contains(r0)     // Catch:{ Exception -> 0x0068 }
            if (r7 == 0) goto L_0x0062
            java.lang.String r7 = ""
            java.lang.String r4 = r4.replace(r0, r7)     // Catch:{ Exception -> 0x0068 }
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x0068 }
            int r7 = r8.c     // Catch:{ Exception -> 0x0068 }
            int r7 = r7 / 100
            int r6 = r7 * r4
            goto L_0x0068
        L_0x0062:
            int r4 = a((java.lang.String) r4)     // Catch:{ Exception -> 0x0068 }
            int r6 = r4 * 1000
        L_0x0068:
            boolean r4 = r1.containsKey(r5)     // Catch:{ Exception -> 0x0089 }
            if (r4 == 0) goto L_0x0075
            java.lang.Object r4 = r1.get(r5)     // Catch:{ Exception -> 0x0089 }
            java.util.List r4 = (java.util.List) r4     // Catch:{ Exception -> 0x0089 }
            goto L_0x007d
        L_0x0075:
            java.util.ArrayList r4 = new java.util.ArrayList     // Catch:{ Exception -> 0x0089 }
            r4.<init>()     // Catch:{ Exception -> 0x0089 }
            r1.put(r5, r4)     // Catch:{ Exception -> 0x0089 }
        L_0x007d:
            com.startapp.sdk.ads.video.vast.model.a.c r5 = new com.startapp.sdk.ads.video.vast.model.a.c     // Catch:{ Exception -> 0x0089 }
            r5.<init>(r3, r6)     // Catch:{ Exception -> 0x0089 }
            r4.add(r5)     // Catch:{ Exception -> 0x0089 }
        L_0x0085:
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0088:
            return r1
        L_0x0089:
            r9 = 0
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.ads.video.vast.model.c.a(org.w3c.dom.Document):java.util.HashMap");
    }

    private static List<b> b(Document document) {
        String str;
        Integer num;
        String str2;
        Integer num2;
        Integer num3;
        String str3;
        Boolean bool;
        Boolean bool2;
        String str4;
        ArrayList arrayList = new ArrayList();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//MediaFile", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    b bVar = new b();
                    Node item = nodeList.item(i2);
                    NamedNodeMap attributes = item.getAttributes();
                    Node namedItem = attributes.getNamedItem("apiFramework");
                    if (namedItem == null) {
                        str = null;
                    } else {
                        str = namedItem.getNodeValue();
                    }
                    bVar.e(str);
                    Node namedItem2 = attributes.getNamedItem("bitrate");
                    if (namedItem2 == null) {
                        num = null;
                    } else {
                        num = Integer.valueOf(namedItem2.getNodeValue());
                    }
                    bVar.a(num);
                    Node namedItem3 = attributes.getNamedItem("delivery");
                    if (namedItem3 == null) {
                        str2 = null;
                    } else {
                        str2 = namedItem3.getNodeValue();
                    }
                    bVar.c(str2);
                    Node namedItem4 = attributes.getNamedItem("height");
                    if (namedItem4 == null) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(namedItem4.getNodeValue());
                    }
                    bVar.c(num2);
                    Node namedItem5 = attributes.getNamedItem("width");
                    if (namedItem5 == null) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(namedItem5.getNodeValue());
                    }
                    bVar.b(num3);
                    Node namedItem6 = attributes.getNamedItem("id");
                    if (namedItem6 == null) {
                        str3 = null;
                    } else {
                        str3 = namedItem6.getNodeValue();
                    }
                    bVar.b(str3);
                    Node namedItem7 = attributes.getNamedItem("maintainAspectRatio");
                    if (namedItem7 == null) {
                        bool = null;
                    } else {
                        bool = Boolean.valueOf(namedItem7.getNodeValue());
                    }
                    bVar.b(bool);
                    Node namedItem8 = attributes.getNamedItem("scalable");
                    if (namedItem8 == null) {
                        bool2 = null;
                    } else {
                        bool2 = Boolean.valueOf(namedItem8.getNodeValue());
                    }
                    bVar.a(bool2);
                    Node namedItem9 = attributes.getNamedItem("type");
                    if (namedItem9 == null) {
                        str4 = null;
                    } else {
                        str4 = namedItem9.getNodeValue();
                    }
                    bVar.d(str4);
                    bVar.a(m.b(item));
                    if (bVar.f()) {
                        arrayList.add(bVar);
                    }
                }
            }
            return arrayList;
        } catch (Exception unused) {
            return null;
        }
    }

    private static int c(Document document) {
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//Duration", document, XPathConstants.NODESET);
            if (nodeList == null || nodeList.getLength() <= 0) {
                return Integer.MAX_VALUE;
            }
            return a(m.b(nodeList.item(0)));
        } catch (Exception unused) {
            return Integer.MAX_VALUE;
        }
    }

    private static e d(Document document) {
        e eVar = new e();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//VideoClicks", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    NodeList childNodes = nodeList.item(i2).getChildNodes();
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item = childNodes.item(i3);
                        String nodeName = item.getNodeName();
                        String b2 = m.b(item);
                        if (nodeName.equalsIgnoreCase("ClickTracking")) {
                            eVar.b().add(b2);
                        } else if (nodeName.equalsIgnoreCase("ClickThrough")) {
                            eVar.a(b2);
                        } else if (nodeName.equalsIgnoreCase("CustomClick")) {
                            eVar.c().add(b2);
                        }
                    }
                }
            }
            return eVar;
        } catch (Exception unused) {
            return null;
        }
    }

    private static int e(Document document) {
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//Linear", document, XPathConstants.NODESET);
            if (nodeList == null) {
                return Integer.MAX_VALUE;
            }
            int i2 = 0;
            while (i2 < nodeList.getLength()) {
                try {
                    if (nodeList.item(i2).getAttributes().getNamedItem("skipoffset") != null) {
                        return a(nodeList.item(i2).getAttributes().getNamedItem("skipoffset").getNodeValue());
                    }
                    i2++;
                } catch (Exception unused) {
                }
            }
            return Integer.MAX_VALUE;
        } catch (Exception unused2) {
            return Integer.MAX_VALUE;
        }
    }

    private static List<a> f(Document document) {
        String str;
        Integer num;
        Integer num2;
        Integer num3;
        Integer num4;
        Integer num5;
        Integer num6;
        String str2;
        Integer num7;
        String str3;
        ArrayList arrayList = new ArrayList();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//Icon", document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    a aVar = new a();
                    Node item = nodeList.item(i2);
                    NamedNodeMap attributes = item.getAttributes();
                    Node namedItem = attributes.getNamedItem("program");
                    if (namedItem == null) {
                        str = null;
                    } else {
                        str = namedItem.getNodeValue();
                    }
                    aVar.a(str);
                    Node namedItem2 = attributes.getNamedItem("width");
                    if (namedItem2 == null) {
                        num = null;
                    } else {
                        num = Integer.valueOf(namedItem2.getNodeValue());
                    }
                    aVar.a(num);
                    Node namedItem3 = attributes.getNamedItem("height");
                    if (namedItem3 == null) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(namedItem3.getNodeValue());
                    }
                    aVar.b(num2);
                    Node namedItem4 = attributes.getNamedItem("xPosition");
                    if (namedItem4 == null) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(namedItem4.getNodeValue());
                    }
                    aVar.c(num3);
                    Node namedItem5 = attributes.getNamedItem("yPosition");
                    if (namedItem5 == null) {
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(namedItem5.getNodeValue());
                    }
                    aVar.d(num4);
                    Node namedItem6 = attributes.getNamedItem("duration");
                    if (namedItem6 == null) {
                        num5 = null;
                    } else {
                        num5 = Integer.valueOf(namedItem6.getNodeValue());
                    }
                    aVar.e(num5);
                    Node namedItem7 = attributes.getNamedItem("offset");
                    if (namedItem7 == null) {
                        num6 = null;
                    } else {
                        num6 = Integer.valueOf(namedItem7.getNodeValue());
                    }
                    aVar.f(num6);
                    Node namedItem8 = attributes.getNamedItem("apiFramework");
                    if (namedItem8 == null) {
                        str2 = null;
                    } else {
                        str2 = namedItem8.getNodeValue();
                    }
                    aVar.b(str2);
                    Node namedItem9 = attributes.getNamedItem("pxratio");
                    if (namedItem9 == null) {
                        num7 = null;
                    } else {
                        num7 = Integer.valueOf(namedItem9.getNodeValue());
                    }
                    aVar.g(num7);
                    NodeList childNodes = item.getChildNodes();
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item2 = childNodes.item(i3);
                        String nodeName = item2.getNodeName();
                        String b2 = m.b(item2);
                        if (nodeName.equalsIgnoreCase("IconClicks")) {
                            NodeList childNodes2 = item.getChildNodes();
                            for (int i4 = 0; i4 < childNodes2.getLength(); i4++) {
                                Node item3 = childNodes.item(i3);
                                String nodeName2 = item3.getNodeName();
                                String b3 = m.b(item3);
                                if (nodeName2.equalsIgnoreCase("ClickThrough")) {
                                    aVar.c(b3);
                                } else if (nodeName2.equalsIgnoreCase("IconViewTracking")) {
                                    aVar.c().add(b3);
                                }
                            }
                        } else if (nodeName.equalsIgnoreCase("ClickTracking")) {
                            aVar.b().add(b2);
                        } else if (nodeName.equalsIgnoreCase("StaticResource")) {
                            d dVar = new d();
                            dVar.b(b2);
                            Node namedItem10 = item.getAttributes().getNamedItem("creativeType");
                            if (namedItem10 == null) {
                                str3 = null;
                            } else {
                                str3 = namedItem10.getNodeValue();
                            }
                            dVar.a(str3);
                            if (dVar.a()) {
                                aVar.a().add(dVar);
                            }
                        }
                    }
                    if (aVar.d()) {
                        arrayList.add(aVar);
                    }
                }
            }
            return arrayList;
        } catch (Exception unused) {
            return null;
        }
    }

    private static AdVerification g(Document document) {
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate("//AdVerifications", document, XPathConstants.NODESET);
            if (nodeList != null) {
                ArrayList arrayList = new ArrayList();
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    NodeList childNodes = nodeList.item(i2).getChildNodes();
                    for (int i3 = 0; i3 < childNodes.getLength(); i3++) {
                        Node item = childNodes.item(i3);
                        if (item.getNodeName().equalsIgnoreCase("Verification")) {
                            NamedNodeMap attributes = item.getAttributes();
                            String nodeValue = (attributes == null || attributes.getNamedItem("vendor") == null) ? null : attributes.getNamedItem("vendor").getNodeValue();
                            NodeList childNodes2 = item.getChildNodes();
                            String str = null;
                            String str2 = null;
                            String str3 = "";
                            for (int i4 = 0; i4 < childNodes2.getLength(); i4++) {
                                Node item2 = childNodes2.item(i4);
                                if (item2.getNodeName().equalsIgnoreCase("JavaScriptResource")) {
                                    Node namedItem = item2.getAttributes().getNamedItem("apiFramework");
                                    if (namedItem != null) {
                                        str3 = namedItem.getNodeValue();
                                    }
                                    str = m.b(item2);
                                } else if (item2.getNodeName().equalsIgnoreCase("VerificationParameters")) {
                                    str2 = m.b(item2);
                                }
                            }
                            if (!TextUtils.isEmpty(nodeValue) && !TextUtils.isEmpty(str) && !TextUtils.isEmpty(str2) && "omid".equalsIgnoreCase(str3)) {
                                arrayList.add(new VerificationDetails(nodeValue, str, str2));
                            }
                        }
                    }
                }
                if (!arrayList.isEmpty()) {
                    return new AdVerification((VerificationDetails[]) arrayList.toArray(new VerificationDetails[arrayList.size()]));
                }
            }
            return null;
        } catch (Exception unused) {
            return null;
        }
    }

    private static List<String> a(Document document, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            NodeList nodeList = (NodeList) XPathFactory.newInstance().newXPath().evaluate(str, document, XPathConstants.NODESET);
            if (nodeList != null) {
                for (int i2 = 0; i2 < nodeList.getLength(); i2++) {
                    arrayList.add(m.b(nodeList.item(i2)));
                }
            }
            return arrayList;
        } catch (Exception unused) {
            return null;
        }
    }

    private static int a(String str) {
        String[] split = str.split(":");
        return (Integer.parseInt(split[0]) * DateTimeConstants.SECONDS_PER_HOUR) + (Integer.parseInt(split[1]) * 60) + Integer.parseInt(split[2]);
    }

    public final boolean a(a aVar) {
        b a2;
        List<String> list = this.e;
        boolean z = (list == null || list.size() == 0) ? false : true;
        List<b> list2 = this.b;
        if (list2 == null || list2.size() == 0) {
            z = false;
        }
        b bVar = null;
        if (z && aVar != null && (a2 = aVar.a(this.b)) != null && !TextUtils.isEmpty(a2.a())) {
            bVar = a2;
        }
        this.h = bVar;
        return this.h != null;
    }
}
