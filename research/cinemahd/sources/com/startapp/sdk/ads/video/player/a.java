package com.startapp.sdk.ads.video.player;

import com.startapp.sdk.ads.video.player.VideoPlayerInterface;

public abstract class a implements VideoPlayerInterface {

    /* renamed from: a  reason: collision with root package name */
    protected String f6229a;
    protected VideoPlayerInterface.f b;
    protected VideoPlayerInterface.e c;
    protected VideoPlayerInterface.d d;
    protected VideoPlayerInterface.c e;
    private VideoPlayerInterface.b f;
    private VideoPlayerInterface.a g;

    public void a(String str) {
        this.f6229a = str;
    }

    public final void a(VideoPlayerInterface.f fVar) {
        this.b = fVar;
    }

    public final void a(VideoPlayerInterface.e eVar) {
        this.c = eVar;
    }

    public final void a(VideoPlayerInterface.d dVar) {
        this.d = dVar;
    }

    public final void a(VideoPlayerInterface.b bVar) {
        this.f = bVar;
    }

    public final void a(VideoPlayerInterface.c cVar) {
        this.e = cVar;
    }

    public final void a(VideoPlayerInterface.a aVar) {
        this.g = aVar;
    }
}
