package com.startapp.sdk.ads.video.vast.model;

import com.facebook.common.statfs.StatFsHelper;
import com.original.tase.model.socket.UserResponces;

public enum VASTErrorCodes {
    ErrorNone(0),
    XMLParsingError(100),
    SchemaValidationError(101),
    VersionOfResponseNotSupported(102),
    TraffickingError(UserResponces.USER_RESPONCE_SUCCSES),
    VideoPlayerExpectingDifferentLinearity(201),
    VideoPlayerExpectingDifferentDuration(202),
    VideoPlayerExpectingDifferentSize(203),
    AdCategoryRequired(204),
    GeneralWrapperError(300),
    WrapperTimeout(301),
    WrapperLimitReached(302),
    WrapperNoReponse(303),
    InlineResponseTimeout(304),
    GeneralLinearError(StatFsHelper.DEFAULT_DISK_YELLOW_LEVEL_IN_MB),
    FileNotFound(401),
    TimeoutMediaFileURI(402),
    MediaNotSupported(403),
    MediaFileDisplayError(405),
    MezzanineNotPovided(406),
    MezzanineDownloadInProgrees(407),
    ConditionalAdRejected(408),
    InteractiveCreativeFileNotExecuted(409),
    VerificationNotExecuted(410),
    MezzanineNotAsExpected(411),
    GeneralNonLinearAdsError(500),
    CreativeTooLarge(501),
    ResourceDownloadFailed(502),
    NonLinearResourceNotSupported(503),
    GeneralCompanionAdsError(600),
    CompanionTooLarge(601),
    CompanionNotDisplay(602),
    CompanionFetchFailed(603),
    CompanionNotSupported(604),
    UndefinedError(900),
    GeneralVPAIDerror(901),
    SAShowBeforeVast(10000),
    SAProcessSuccess(20000);
    
    private int value;

    private VASTErrorCodes(int i) {
        this.value = i;
    }

    public final int a() {
        return this.value;
    }
}
