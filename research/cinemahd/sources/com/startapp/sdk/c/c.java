package com.startapp.sdk.c;

import android.content.Context;
import com.startapp.common.ThreadManager;
import com.startapp.common.a.d;
import com.startapp.sdk.adsbase.infoevents.AnalyticsConfig;
import com.startapp.sdk.adsbase.infoevents.b;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.j.r;
import com.startapp.sdk.adsbase.remoteconfig.AnalyticsReporterConfig;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.NetworkDiagnosticConfig;
import com.startapp.sdk.adsbase.remoteconfig.RscMetadata;
import com.startapp.sdk.adsbase.remoteconfig.StaleDcConfig;
import com.startapp.sdk.d.b.b;
import com.startapp.sdk.g.a;
import com.startapp.sdk.triggeredlinks.TriggeredLinksMetadata;
import java.util.concurrent.Executors;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static final b<c, Context> f6439a = new b<c, Context>() {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object a(Object obj) {
            return new c((Context) obj);
        }
    };
    private final a<b> b;
    private final a<com.startapp.sdk.d.a.b> c;
    private final a<com.startapp.sdk.d.d.c> d;
    private final a<d> e;
    private final a<a> f;
    private final a<com.startapp.sdk.adsbase.consent.a> g;
    private final a<com.startapp.sdk.f.a> h;
    private final a<com.startapp.sdk.adsbase.i.a> i = new a<com.startapp.sdk.adsbase.i.a>() {
        /* access modifiers changed from: protected */
        public final /* synthetic */ Object a() {
            return new com.startapp.sdk.adsbase.i.a();
        }
    };
    private final a<com.startapp.sdk.triggeredlinks.c> j;
    private final a<com.startapp.sdk.adsbase.b> k;
    private final a<com.startapp.sdk.adsbase.f.d> l;
    private final a<com.startapp.sdk.adsbase.d.b> m;
    private final a<com.startapp.sdk.d.c.b> n;
    private final a<com.startapp.sdk.a.a> o;
    private final a<com.startapp.sdk.adsbase.infoevents.d> p;

    protected c(final Context context) {
        this.b = new a<b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new b(context);
            }
        };
        this.c = new a<com.startapp.sdk.d.a.b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.d.a.b(context);
            }
        };
        this.d = new a<com.startapp.sdk.d.d.c>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.d.d.c(context);
            }
        };
        this.e = new a<d>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new d(context);
            }
        };
        this.f = new a<a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new a(context, new g<RscMetadata>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.G().b();
                    }
                });
            }
        };
        this.g = new a<com.startapp.sdk.adsbase.consent.a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.adsbase.consent.a(context);
            }
        };
        this.h = new a<com.startapp.sdk.f.a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.f.a(context.getSharedPreferences("StartApp-54ff24db2aee60b9", 0));
            }
        };
        this.j = new a<com.startapp.sdk.triggeredlinks.c>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                Context context = context;
                return new com.startapp.sdk.triggeredlinks.c(context, context.getSharedPreferences("StartApp-fba1a5307d96ef31", 0), new r(ThreadManager.Priority.DEFAULT), c.a(context).d(), new g<TriggeredLinksMetadata>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.G().d();
                    }
                });
            }
        };
        this.k = new a<com.startapp.sdk.adsbase.b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.adsbase.b(context.getSharedPreferences("StartApp-790ba54ab8e69f2f", 0));
            }
        };
        this.l = new a<com.startapp.sdk.adsbase.f.d>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                Context context = context;
                return new com.startapp.sdk.adsbase.f.d(context, context.getSharedPreferences("StartApp-3286f6eaf694fa40", 0), new g<com.startapp.sdk.adsbase.remoteconfig.c>() {
                    public final /* synthetic */ Object a() {
                        return new com.startapp.sdk.adsbase.remoteconfig.c(MetaData.G().p());
                    }
                }, new b.a());
            }
        };
        this.m = new a<com.startapp.sdk.adsbase.d.b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                Context context = context;
                d d = c.this.d();
                com.startapp.sdk.d.b.b a2 = c.this.a();
                Context context2 = context;
                return new com.startapp.sdk.adsbase.d.b(context, d, a2, new com.startapp.sdk.adsbase.e.a(context2, context2.getSharedPreferences("StartApp-770c613f81fb5b52", 0), new com.startapp.sdk.adsbase.e.b(context, "StartApp-ac51a09f00e0f80c"), Executors.newSingleThreadExecutor(), new g<NetworkDiagnosticConfig>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.G().a();
                    }
                }), new g<com.startapp.sdk.adsbase.d.c>() {
                    public final /* synthetic */ Object a() {
                        MetaData G = MetaData.G();
                        return new com.startapp.sdk.adsbase.d.c(G.A(), G.R(), G.q());
                    }
                });
            }
        };
        this.n = new a<com.startapp.sdk.d.c.b>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                Context context = context;
                return new com.startapp.sdk.d.c.b(context, context.getSharedPreferences("StartApp-9b9bfdb86df82dad", 0), new g<StaleDcConfig>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.G().e();
                    }
                });
            }
        };
        this.o = new a<com.startapp.sdk.a.a>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                Context context = context;
                return new com.startapp.sdk.a.a(context, context.getSharedPreferences("StartApp-c378d30bf27db04d", 0), new r(ThreadManager.Priority.DEFAULT), new g<AnalyticsReporterConfig>() {
                    public final /* synthetic */ Object a() {
                        AnalyticsConfig analyticsConfig = MetaData.G().analytics;
                        if (analyticsConfig != null) {
                            return analyticsConfig.f();
                        }
                        return null;
                    }
                });
            }
        };
        this.p = new a<com.startapp.sdk.adsbase.infoevents.d>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object a() {
                return new com.startapp.sdk.adsbase.infoevents.d(context, new ThreadPoolExecutor(2, 2, 10, TimeUnit.SECONDS, new PriorityBlockingQueue()), new g<AnalyticsConfig>() {
                    public final /* synthetic */ Object a() {
                        return MetaData.G().analytics;
                    }
                });
            }
        };
    }

    public static c a(Context context) {
        return f6439a.b(context.getApplicationContext());
    }

    public final com.startapp.sdk.d.a.b b() {
        return this.c.b();
    }

    public final com.startapp.sdk.d.d.c c() {
        return this.d.b();
    }

    public final d d() {
        return this.e.b();
    }

    public final a e() {
        return this.f.b();
    }

    public final com.startapp.sdk.adsbase.consent.a f() {
        return this.g.b();
    }

    public final com.startapp.sdk.f.a g() {
        return this.h.b();
    }

    public final com.startapp.sdk.adsbase.i.a h() {
        return this.i.b();
    }

    public final com.startapp.sdk.triggeredlinks.c i() {
        return this.j.b();
    }

    public final com.startapp.sdk.adsbase.b j() {
        return this.k.b();
    }

    public final com.startapp.sdk.adsbase.f.d k() {
        return this.l.b();
    }

    public final com.startapp.sdk.adsbase.d.b l() {
        return this.m.b();
    }

    public final com.startapp.sdk.d.c.b m() {
        return this.n.b();
    }

    public final com.startapp.sdk.a.a n() {
        return this.o.b();
    }

    public final com.startapp.sdk.adsbase.infoevents.d o() {
        return this.p.b();
    }

    public final com.startapp.sdk.d.b.b a() {
        return this.b.b();
    }
}
