package com.startapp.sdk.c;

public abstract class a<T> {

    /* renamed from: a  reason: collision with root package name */
    private volatile T f6437a;

    /* access modifiers changed from: protected */
    public abstract T a();

    public final T b() {
        T t = this.f6437a;
        if (t == null) {
            synchronized (this) {
                t = this.f6437a;
                if (t == null) {
                    t = a();
                    this.f6437a = t;
                }
            }
        }
        return t;
    }
}
