package com.startapp.sdk.c;

public abstract class b<T, A> {

    /* renamed from: a  reason: collision with root package name */
    private volatile T f6438a;

    /* access modifiers changed from: protected */
    public abstract T a(A a2);

    public final T b(A a2) {
        T t = this.f6438a;
        if (t == null) {
            synchronized (this) {
                t = this.f6438a;
                if (t == null) {
                    T a3 = a(a2);
                    this.f6438a = a3;
                    t = a3;
                }
            }
        }
        return t;
    }
}
