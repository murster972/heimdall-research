package com.startapp.sdk.inappbrowser;

import android.graphics.Bitmap;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private Bitmap f6486a = null;
    private int b;
    private int c;
    private String d;

    public b(int i, int i2, String str) {
        this.b = i;
        this.c = i2;
        this.d = str;
    }

    public final int a() {
        return this.b;
    }

    public final int b() {
        return this.c;
    }

    public final String c() {
        return this.d;
    }

    public final Bitmap d() {
        return this.f6486a;
    }

    public final void a(Bitmap bitmap) {
        this.f6486a = bitmap;
    }
}
