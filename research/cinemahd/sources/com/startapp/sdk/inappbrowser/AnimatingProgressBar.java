package com.startapp.sdk.inappbrowser;

import android.animation.ValueAnimator;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ProgressBar;

public class AnimatingProgressBar extends ProgressBar {

    /* renamed from: a  reason: collision with root package name */
    private static final Interpolator f6481a = new AccelerateDecelerateInterpolator();
    private ValueAnimator b;
    private boolean c = false;

    public AnimatingProgressBar(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        boolean z = false;
        this.c = Build.VERSION.SDK_INT >= 11 ? true : z;
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ValueAnimator valueAnimator = this.b;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
    }

    public void setProgress(int i) {
        if (!this.c) {
            super.setProgress(i);
            return;
        }
        ValueAnimator valueAnimator = this.b;
        if (valueAnimator != null) {
            valueAnimator.cancel();
            if (getProgress() >= i) {
                return;
            }
        } else {
            this.b = ValueAnimator.ofInt(new int[]{getProgress(), i});
            this.b.setInterpolator(f6481a);
            this.b.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

                /* renamed from: a  reason: collision with root package name */
                private Integer f6482a;

                public final void onAnimationUpdate(ValueAnimator valueAnimator) {
                    this.f6482a = (Integer) valueAnimator.getAnimatedValue();
                    AnimatingProgressBar.super.setProgress(this.f6482a.intValue());
                }
            });
        }
        this.b.setIntValues(new int[]{getProgress(), i});
        this.b.start();
    }

    public final void a() {
        super.setProgress(0);
        ValueAnimator valueAnimator = this.b;
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
    }
}
