package com.startapp.sdk.e;

import android.content.Context;
import android.webkit.JavascriptInterface;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.j.u;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f6470a;
    private boolean b;
    private Runnable c;
    private Runnable d;
    private Runnable e;
    private Context f;
    private TrackingParams g;

    public b(Context context, Runnable runnable, TrackingParams trackingParams, boolean z) {
        this(context, runnable, trackingParams);
        this.f6470a = z;
    }

    @JavascriptInterface
    public void closeAd() {
        if (!this.b) {
            this.b = true;
            this.c.run();
        }
    }

    @JavascriptInterface
    public void enableScroll(String str) {
        Runnable runnable = this.e;
        if (runnable != null) {
            runnable.run();
        }
    }

    @JavascriptInterface
    public void externalLinks(String str) {
        if (this.f6470a) {
            u.b();
            a.a(this.f, str, (String) null);
            return;
        }
        a.c(this.f, str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:(1:3)|4|(3:6|7|(2:10|8))|11|12|15|(2:17|18)(1:19)) */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0044, code lost:
        r3 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0045, code lost:
        new com.startapp.sdk.adsbase.infoevents.e(r3).a(r2.f);
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003e */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x0053  */
    /* JADX WARNING: Removed duplicated region for block: B:19:? A[RETURN, SYNTHETIC] */
    @android.webkit.JavascriptInterface
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void openApp(java.lang.String r3, java.lang.String r4, java.lang.String r5) {
        /*
            r2 = this;
            if (r3 == 0) goto L_0x000f
            boolean r0 = android.text.TextUtils.isEmpty(r3)
            if (r0 != 0) goto L_0x000f
            android.content.Context r0 = r2.f
            com.startapp.sdk.adsbase.commontracking.TrackingParams r1 = r2.g
            com.startapp.sdk.adsbase.a.b((android.content.Context) r0, (java.lang.String) r3, (com.startapp.sdk.adsbase.commontracking.TrackingParams) r1)
        L_0x000f:
            android.content.Context r3 = r2.f
            android.content.pm.PackageManager r3 = r3.getPackageManager()
            android.content.Intent r3 = r3.getLaunchIntentForPackage(r4)
            if (r5 == 0) goto L_0x003e
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ JSONException -> 0x003e }
            r4.<init>(r5)     // Catch:{ JSONException -> 0x003e }
            java.util.Iterator r5 = r4.keys()     // Catch:{ JSONException -> 0x003e }
        L_0x0024:
            boolean r0 = r5.hasNext()     // Catch:{ JSONException -> 0x003e }
            if (r0 == 0) goto L_0x003e
            java.lang.Object r0 = r5.next()     // Catch:{ JSONException -> 0x003e }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ JSONException -> 0x003e }
            java.lang.Object r1 = r4.get(r0)     // Catch:{ JSONException -> 0x003e }
            java.lang.String r1 = java.lang.String.valueOf(r1)     // Catch:{ JSONException -> 0x003e }
            r3.putExtra(r0, r1)     // Catch:{ JSONException -> 0x003e }
            goto L_0x0024
        L_0x003e:
            android.content.Context r4 = r2.f     // Catch:{ all -> 0x0044 }
            r4.startActivity(r3)     // Catch:{ all -> 0x0044 }
            goto L_0x004f
        L_0x0044:
            r3 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r4 = new com.startapp.sdk.adsbase.infoevents.e
            r4.<init>((java.lang.Throwable) r3)
            android.content.Context r3 = r2.f
            r4.a((android.content.Context) r3)
        L_0x004f:
            java.lang.Runnable r3 = r2.d
            if (r3 == 0) goto L_0x0056
            r3.run()
        L_0x0056:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.e.b.openApp(java.lang.String, java.lang.String, java.lang.String):void");
    }

    private b(Context context, Runnable runnable, TrackingParams trackingParams) {
        this.b = false;
        this.f6470a = true;
        this.c = null;
        this.d = null;
        this.e = null;
        this.c = runnable;
        this.f = context;
        this.g = trackingParams;
    }

    public b(Context context, Runnable runnable, Runnable runnable2, Runnable runnable3, TrackingParams trackingParams, boolean z) {
        this(context, runnable, trackingParams, z);
        this.d = runnable2;
        this.e = runnable3;
    }

    public b(Context context, Runnable runnable, Runnable runnable2, TrackingParams trackingParams) {
        this(context, runnable, trackingParams);
        this.d = runnable2;
    }
}
