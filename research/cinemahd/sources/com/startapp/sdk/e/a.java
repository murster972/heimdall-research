package com.startapp.sdk.e;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import com.startapp.common.b.e;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.d;
import com.startapp.sdk.adsbase.j.q;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.c.c;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public abstract class a extends d {
    protected Set<String> g = new HashSet();
    protected GetAdRequest h;
    private Set<String> i = new HashSet();
    private int j = 0;
    private boolean k;

    public a(Context context, Ad ad, AdPreferences adPreferences, b bVar, AdPreferences.Placement placement, boolean z) {
        super(context, ad, adPreferences, bVar, placement);
        this.k = z;
    }

    /* access modifiers changed from: protected */
    public boolean a(GetAdRequest getAdRequest) {
        return getAdRequest != null;
    }

    /* access modifiers changed from: protected */
    public void b(Boolean bool) {
        super.b(bool);
    }

    /* access modifiers changed from: protected */
    public final Object e() {
        this.h = a();
        if (!a(this.h)) {
            return null;
        }
        if (this.i.size() == 0) {
            this.i.add(this.f6335a.getPackageName());
        }
        this.h.a(this.i);
        this.h.b(this.g);
        if (this.j > 0) {
            this.h.c(false);
            if (MetaData.G().f().a(this.f6335a)) {
                SimpleTokenUtils.b(this.f6335a);
            }
        }
        return c.a(this.f6335a).l().a(AdsConstants.a(AdsConstants.AdApiType.HTML, f())).a((com.startapp.sdk.adsbase.c) this.h).a((q<String>) new q<String>() {
            public final /* bridge */ /* synthetic */ void a(Object obj) {
                String unused = a.this.f = (String) obj;
            }
        }).a();
    }

    /* access modifiers changed from: protected */
    public boolean a(Object obj) {
        if (obj == null) {
            if (this.f == null) {
                this.f = "No response";
            }
            return false;
        }
        try {
            ArrayList arrayList = new ArrayList();
            String a2 = ((e.a) obj).a();
            if (TextUtils.isEmpty(a2)) {
                if (this.f == null) {
                    if (this.h == null || !this.h.j()) {
                        this.f = "Empty Ad";
                    } else {
                        this.f = "Video isn't available";
                    }
                }
                return false;
            }
            List<AppPresenceDetails> a3 = com.iab.omid.library.startapp.b.a(a2, this.j);
            if (!(AdsCommonMetaData.a().F() ? com.iab.omid.library.startapp.b.a(this.f6335a, a3, this.j, this.i, (List<AppPresenceDetails>) arrayList).booleanValue() : false)) {
                ((HtmlAd) this.b).a(a3);
                ((HtmlAd) this.b).b(a2);
                return true;
            }
            this.j++;
            new com.startapp.sdk.adsbase.apppresence.a(this.f6335a, arrayList).a();
            return d().booleanValue();
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6335a);
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        super.a(bool);
    }

    /* access modifiers changed from: protected */
    public final void a(boolean z) {
        Ad ad;
        Intent intent = new Intent("com.startapp.android.OnReceiveResponseBroadcastListener");
        intent.putExtra("adHashcode", this.b.hashCode());
        intent.putExtra("adResult", z);
        com.startapp.common.b.a(this.f6335a).a(intent);
        if (z && (ad = this.b) != null) {
            if (this.k) {
                u.a(this.f6335a, ((HtmlAd) ad).j(), (u.a) new u.a() {
                    public final void a() {
                        a.this.d.a(a.this.b);
                    }

                    public final void a(String str) {
                        a.this.b.setErrorMessage(str);
                        a.this.d.b(a.this.b);
                    }
                });
            } else if (z) {
                this.d.a(ad);
            } else {
                this.d.b(ad);
            }
        }
    }
}
