package com.startapp.sdk.insight;

import android.content.Context;
import android.os.Build;
import com.startapp.networkTest.startapp.NetworkTester;
import com.startapp.networkTest.startapp.a;
import com.startapp.sdk.adsbase.infoevents.e;

public class a {
    public static void a(final Context context, NetworkTestsMetaData networkTestsMetaData) {
        if (Build.VERSION.SDK_INT >= 14) {
            com.startapp.networkTest.startapp.a.a((a.C0069a) new a.C0069a() {
                public final void a(Throwable th) {
                    if (th != null) {
                        new e(th).a(context);
                    }
                }
            });
            if (networkTestsMetaData == null || !networkTestsMetaData.a()) {
                NetworkTester.stopListening();
                return;
            }
            try {
                NetworkTester.Config config = new NetworkTester.Config();
                config.PROJECT_ID = networkTestsMetaData.b();
                config.CONNECTIVITY_TEST_HOSTNAME = networkTestsMetaData.c();
                config.CONNECTIVITY_TEST_FILENAME = networkTestsMetaData.d();
                config.CONNECTIVITY_TEST_ENABLED = networkTestsMetaData.e();
                config.NIR_COLLECT_CELLINFO = networkTestsMetaData.f();
                config.CT_COLLECT_CELLINFO = networkTestsMetaData.g();
                config.CONNECTIVITY_TEST_CDNCONFIG_URL = networkTestsMetaData.h();
                config.GEOIP_URL = networkTestsMetaData.i();
                b a2 = b.a(context);
                NetworkTester.init(context, config);
                NetworkTester.setOnConnectivityLatencyListener(a2);
                NetworkTester.setOnNetworkInfoListener(a2);
                NetworkTester.startListening(networkTestsMetaData.j(), networkTestsMetaData.l());
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
    }
}
