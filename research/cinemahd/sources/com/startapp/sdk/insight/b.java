package com.startapp.sdk.insight;

import android.content.Context;
import com.startapp.common.ThreadManager;
import com.startapp.networkTest.results.BaseResult;
import com.startapp.networkTest.results.ConnectivityTestResult;
import com.startapp.networkTest.results.LatencyResult;
import com.startapp.networkTest.results.NetworkInformationResult;
import com.startapp.networkTest.startapp.ConnectivityTestListener;
import com.startapp.networkTest.startapp.CoverageMapperManager;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.c;
import com.startapp.sdk.adsbase.j.r;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.Executor;

public class b implements ConnectivityTestListener, CoverageMapperManager.OnNetworkInfoResultListener {
    private static final Comparator<File> d = new Comparator<File>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return ((File) obj2).getName().compareTo(((File) obj).getName());
        }
    };

    /* renamed from: a  reason: collision with root package name */
    final Context f6488a;
    final c b;
    final File c;
    private File e;

    /* renamed from: com.startapp.sdk.insight.b$7  reason: invalid class name */
    static /* synthetic */ class AnonymousClass7 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6495a = new int[InfoEventCategory.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.startapp.sdk.adsbase.infoevents.InfoEventCategory[] r0 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6495a = r0
                int[] r0 = f6495a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.infoevents.InfoEventCategory r1 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.INSIGHT_CORE_CT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6495a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.infoevents.InfoEventCategory r1 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.INSIGHT_CORE_LT     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6495a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.adsbase.infoevents.InfoEventCategory r1 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.INSIGHT_CORE_NIR     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.insight.b.AnonymousClass7.<clinit>():void");
        }
    }

    private b(Context context, Executor executor, File file) {
        this.f6488a = context;
        this.b = new c(executor);
        this.c = new File(file, "saved");
        this.e = new File(file, "sending");
    }

    public static b a(Context context) {
        j.b(context, "SuccessfulSentTimeKey", Long.valueOf(System.currentTimeMillis()));
        return new b(context.getApplicationContext(), new r(ThreadManager.Priority.DEFAULT), new File(context.getFilesDir(), "StartApp-Events"));
    }

    public void onConnectivityTestFinished(final Runnable runnable) {
        this.b.a(new com.startapp.sdk.adsbase.j.b() {
            /* JADX WARNING: Removed duplicated region for block: B:10:0x004b A[DONT_GENERATE] */
            /* JADX WARNING: Removed duplicated region for block: B:15:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void a(java.lang.Runnable r7) {
                /*
                    r6 = this;
                    java.lang.String r0 = "SuccessfulSentTimeKey"
                    long r1 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x004f }
                    com.startapp.sdk.insight.b r3 = com.startapp.sdk.insight.b.this     // Catch:{ all -> 0x004f }
                    android.content.Context r3 = r3.f6488a     // Catch:{ all -> 0x004f }
                    java.lang.Long r4 = java.lang.Long.valueOf(r1)     // Catch:{ all -> 0x004f }
                    java.lang.Long r3 = com.startapp.sdk.adsbase.j.a((android.content.Context) r3, (java.lang.String) r0, (java.lang.Long) r4)     // Catch:{ all -> 0x004f }
                    long r3 = r3.longValue()     // Catch:{ all -> 0x004f }
                    long r1 = r1 - r3
                    com.startapp.sdk.insight.b r3 = com.startapp.sdk.insight.b.this     // Catch:{ all -> 0x004f }
                    android.content.Context r3 = r3.f6488a     // Catch:{ all -> 0x004f }
                    boolean r3 = com.startapp.sdk.adsbase.j.u.d((android.content.Context) r3)     // Catch:{ all -> 0x004f }
                    if (r3 != 0) goto L_0x0034
                    com.startapp.sdk.adsbase.remoteconfig.MetaData r3 = com.startapp.sdk.adsbase.remoteconfig.MetaData.G()     // Catch:{ all -> 0x004f }
                    com.startapp.sdk.insight.NetworkTestsMetaData r3 = r3.c()     // Catch:{ all -> 0x004f }
                    long r3 = r3.k()     // Catch:{ all -> 0x004f }
                    int r5 = (r1 > r3 ? 1 : (r1 == r3 ? 0 : -1))
                    if (r5 <= 0) goto L_0x0032
                    goto L_0x0034
                L_0x0032:
                    r0 = 1
                    goto L_0x0049
                L_0x0034:
                    com.startapp.sdk.insight.b r1 = com.startapp.sdk.insight.b.this     // Catch:{ all -> 0x004f }
                    android.content.Context r1 = r1.f6488a     // Catch:{ all -> 0x004f }
                    long r2 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x004f }
                    java.lang.Long r2 = java.lang.Long.valueOf(r2)     // Catch:{ all -> 0x004f }
                    com.startapp.sdk.adsbase.j.b((android.content.Context) r1, (java.lang.String) r0, (java.lang.Long) r2)     // Catch:{ all -> 0x004f }
                    com.startapp.sdk.insight.b r0 = com.startapp.sdk.insight.b.this     // Catch:{ all -> 0x004f }
                    boolean r0 = r0.a((java.lang.Runnable) r7)     // Catch:{ all -> 0x004f }
                L_0x0049:
                    if (r0 == 0) goto L_0x004e
                    r7.run()
                L_0x004e:
                    return
                L_0x004f:
                    r0 = move-exception
                    r7.run()
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.insight.b.AnonymousClass3.a(java.lang.Runnable):void");
            }
        });
        if (runnable != null) {
            this.b.a(new com.startapp.sdk.adsbase.j.b() {
                public final void a(Runnable runnable) {
                    try {
                        runnable = runnable;
                        runnable.run();
                    } finally {
                        runnable.run();
                    }
                }
            });
        }
    }

    public void onConnectivityTestResult(ConnectivityTestResult connectivityTestResult) {
        if (connectivityTestResult != null) {
            a(InfoEventCategory.INSIGHT_CORE_CT, (BaseResult) connectivityTestResult, System.currentTimeMillis());
        }
    }

    public void onLatencyTestResult(LatencyResult latencyResult) {
        if (latencyResult != null) {
            a(InfoEventCategory.INSIGHT_CORE_LT, (BaseResult) latencyResult, System.currentTimeMillis());
        }
    }

    public void onNetworkInfoResult(NetworkInformationResult networkInformationResult) {
        if (networkInformationResult != null) {
            a(InfoEventCategory.INSIGHT_CORE_NIR, (BaseResult) networkInformationResult, System.currentTimeMillis());
        }
    }

    private static NetworkTestsMetaData a() {
        NetworkTestsMetaData c2 = MetaData.G().c();
        return c2 == null ? new NetworkTestsMetaData() : c2;
    }

    /* access modifiers changed from: package-private */
    public final boolean a(InfoEventCategory infoEventCategory, BaseResult baseResult, long j, Runnable runnable) {
        String str;
        try {
            str = u.b((Object) baseResult);
        } catch (Throwable th) {
            new e(th).a(this.f6488a);
            str = null;
        }
        final String str2 = str;
        if (str2 == null) {
            return true;
        }
        if (u.d(this.f6488a)) {
            e eVar = new e(infoEventCategory);
            eVar.a(j);
            eVar.g(str2);
            eVar.n(a(infoEventCategory));
            final InfoEventCategory infoEventCategory2 = infoEventCategory;
            final long j2 = j;
            final Runnable runnable2 = runnable;
            eVar.a(this.f6488a, new Object() {
                public final void a(e eVar, boolean z) {
                    if (!z) {
                        b.this.b.a(this);
                    }
                }

                public final void a() {
                    runnable2.run();
                }

                public final void a(Runnable runnable) {
                    try {
                        b.this.a(infoEventCategory2, str2, j2);
                    } catch (Throwable th) {
                        new e(th).a(b.this.f6488a);
                    } finally {
                        runnable.run();
                    }
                }
            });
            return false;
        }
        try {
            a(infoEventCategory, str2, j);
            return true;
        } catch (Throwable th2) {
            new e(th2).a(this.f6488a);
            return true;
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(InfoEventCategory infoEventCategory, String str, long j) throws IOException {
        int r;
        if (this.c.exists() || this.c.mkdirs()) {
            File file = this.c;
            PrintStream printStream = new PrintStream(new File(file, j + "-" + infoEventCategory.a()));
            printStream.print(str);
            printStream.close();
            File[] listFiles = this.c.listFiles();
            if (listFiles != null && listFiles.length > (r = a().r()) && r > 10) {
                Arrays.sort(listFiles, d);
                int length = listFiles.length;
                for (int min = Math.min(Math.max(10, a().q()), r); min < length; min++) {
                    a(listFiles[min]);
                }
                return;
            }
            return;
        }
        throw new IOException();
    }

    /* access modifiers changed from: package-private */
    public final boolean a(final Runnable runnable) {
        File[] listFiles = this.c.listFiles();
        e eVar = null;
        if (listFiles != null) {
            Arrays.sort(listFiles, d);
            long currentTimeMillis = System.currentTimeMillis() - a().m();
            e eVar2 = null;
            e eVar3 = null;
            for (File file : listFiles) {
                int indexOf = file.getName().indexOf("-");
                if (indexOf < 0) {
                    a(file);
                } else {
                    InfoEventCategory a2 = InfoEventCategory.a(file.getName().substring(indexOf + 1));
                    if (a2 == null) {
                        a(file);
                    } else {
                        try {
                            long parseLong = Long.parseLong(file.getName().substring(0, indexOf));
                            if (parseLong < currentTimeMillis) {
                                a(file);
                            } else {
                                File a3 = a(file, this.e);
                                if (a3 == null) {
                                    a(file);
                                } else {
                                    e eVar4 = new e(a2);
                                    eVar4.a(parseLong);
                                    eVar4.a(a3);
                                    eVar4.n(a(a2));
                                    if (eVar2 == null) {
                                        eVar2 = eVar4;
                                    }
                                    if (eVar3 != null) {
                                        eVar3.a(eVar4);
                                    }
                                    eVar3 = eVar4;
                                }
                            }
                        } catch (NumberFormatException unused) {
                            a(file);
                        }
                    }
                }
            }
            eVar = eVar2;
        }
        if (eVar == null) {
            return true;
        }
        eVar.a(this.f6488a, new com.startapp.sdk.adsbase.infoevents.c() {
            public final void a(e eVar, final boolean z) {
                final File j = eVar.j();
                if (j != null) {
                    b.this.b.a(new com.startapp.sdk.adsbase.j.b() {
                        public final void a(Runnable runnable) {
                            try {
                                if (z) {
                                    b.a(j);
                                } else if (b.a(j, b.this.c) == null) {
                                    b.a(j);
                                }
                            } finally {
                                runnable.run();
                            }
                        }
                    });
                }
            }

            public final void a() {
                runnable.run();
            }
        });
        return false;
    }

    private static String a(InfoEventCategory infoEventCategory) {
        int i = AnonymousClass7.f6495a[infoEventCategory.ordinal()];
        if (i == 1) {
            return a().n();
        }
        if (i == 2) {
            return a().o();
        }
        if (i != 3) {
            return null;
        }
        return a().p();
    }

    static void a(File file) {
        if (!file.delete()) {
            file.deleteOnExit();
        }
    }

    private void a(InfoEventCategory infoEventCategory, BaseResult baseResult, long j) {
        final InfoEventCategory infoEventCategory2 = infoEventCategory;
        final BaseResult baseResult2 = baseResult;
        final long j2 = j;
        this.b.a(new com.startapp.sdk.adsbase.j.b() {
            public final void a(Runnable runnable) {
                try {
                    if (!b.this.a(infoEventCategory2, baseResult2, j2, runnable)) {
                    }
                } finally {
                    runnable.run();
                }
            }
        });
    }

    static File a(File file, File file2) {
        if (!file2.exists() && !file2.mkdirs()) {
            return null;
        }
        File file3 = new File(file2, file.getName());
        if (file.renameTo(file3)) {
            return file3;
        }
        return null;
    }
}
