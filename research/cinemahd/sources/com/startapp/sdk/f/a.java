package com.startapp.sdk.f;

import android.content.SharedPreferences;
import java.util.UUID;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f6471a;
    private volatile String b;

    public a(SharedPreferences sharedPreferences) {
        this.f6471a = sharedPreferences;
    }

    public final String a() {
        String str = this.b;
        if (str == null) {
            synchronized (this) {
                str = this.b;
                if (str == null) {
                    str = this.f6471a.getString("e695c6d894060903", (String) null);
                    if (str == null) {
                        str = UUID.randomUUID().toString();
                        if (!this.f6471a.edit().putString("e695c6d894060903", str).commit()) {
                            str = "00000000-0000-0000-0000-000000000000";
                        }
                    }
                    this.b = str;
                }
            }
        }
        return str;
    }
}
