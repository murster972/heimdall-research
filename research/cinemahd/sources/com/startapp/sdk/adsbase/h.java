package com.startapp.sdk.adsbase;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.atomic.AtomicBoolean;

public final class h {

    /* renamed from: a  reason: collision with root package name */
    private static final boolean f6362a = MetaData.G().L();
    private Handler b = new Handler(Looper.getMainLooper());
    private long c;
    private Context d;
    private long e = -1;
    private long f;
    private boolean g;
    private boolean h;
    private String[] i;
    private TrackingParams j;
    private AtomicBoolean k = new AtomicBoolean(false);
    private a l;

    public interface a {
        void onSent();
    }

    public h(Context context, String[] strArr, TrackingParams trackingParams, long j2) {
        this.d = context.getApplicationContext();
        this.i = strArr;
        this.j = trackingParams;
        this.c = j2;
    }

    public final void a(a aVar) {
        this.l = aVar;
    }

    public final void b() {
        if (this.g && this.h) {
            this.b.removeCallbacksAndMessages((Object) null);
            this.e = System.currentTimeMillis();
            this.c -= this.e - this.f;
            this.h = false;
        }
    }

    public final boolean c() {
        return this.k.get();
    }

    public final void a(boolean z) {
        b(z);
        this.g = false;
        this.b.removeCallbacksAndMessages((Object) null);
        this.h = false;
        this.e = -1;
        this.f = 0;
    }

    /* access modifiers changed from: protected */
    public final void b(boolean z) {
        if (!this.k.compareAndSet(false, true)) {
            return;
        }
        if (z) {
            a.a(this.d, this.i, this.j);
            a aVar = this.l;
            if (aVar != null) {
                aVar.onSent();
                return;
            }
            return;
        }
        a.a(this.d, this.i, this.j.f(), AdDisplayListener.NotDisplayedReason.AD_CLOSED_TOO_QUICKLY.toString());
    }

    public final void a() {
        if (this.k.get()) {
            return;
        }
        if (f6362a) {
            long j2 = this.c;
            if (!this.h) {
                this.h = true;
                if (!this.g) {
                    this.g = true;
                }
                this.f = System.currentTimeMillis();
                this.b.postDelayed(new Runnable() {
                    public final void run() {
                        h.this.b(true);
                    }
                }, j2);
                return;
            }
            return;
        }
        b(true);
    }
}
