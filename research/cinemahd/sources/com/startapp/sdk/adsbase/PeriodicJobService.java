package com.startapp.sdk.adsbase;

import android.annotation.TargetApi;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import com.startapp.common.jobrunner.a;
import com.startapp.common.jobrunner.interfaces.RunnerJob;

@TargetApi(21)
public class PeriodicJobService extends JobService {
    static {
        Class<PeriodicJobService> cls = PeriodicJobService.class;
    }

    public boolean onStartJob(final JobParameters jobParameters) {
        int jobId = jobParameters.getJobId();
        if (jobId == Integer.MAX_VALUE || jobId == 2147483646) {
            a.a(getApplicationContext(), jobId);
            return false;
        }
        a.a((Context) this);
        boolean a2 = a.a(jobParameters, (RunnerJob.a) new RunnerJob.a() {
            public final void a(RunnerJob.Result result) {
                PeriodicJobService.this.jobFinished(jobParameters, false);
            }
        });
        "onStartJob: RunnerManager.runJob".concat(String.valueOf(a2));
        a.c();
        return a2;
    }

    public boolean onStopJob(JobParameters jobParameters) {
        return true;
    }
}
