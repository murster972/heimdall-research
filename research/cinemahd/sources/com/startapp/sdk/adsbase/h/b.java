package com.startapp.sdk.adsbase.h;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import com.startapp.common.d;
import com.startapp.sdk.adsbase.remoteconfig.BaseSensorConfig;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.SensorsConfig;
import java.util.HashMap;
import org.json.JSONArray;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    protected a f6365a = new a();
    protected d b;
    private HashMap<Integer, a> c = null;
    private SensorManager d;
    /* access modifiers changed from: private */
    public int e;
    private SensorEventListener f = new SensorEventListener() {
        public final void onAccuracyChanged(Sensor sensor, int i) {
        }

        public final void onSensorChanged(SensorEvent sensorEvent) {
            if (b.this.f6365a.a(sensorEvent) == b.this.e) {
                b.this.b();
                b bVar = b.this;
                d dVar = bVar.b;
                if (dVar != null) {
                    dVar.a(bVar.c());
                }
            }
        }
    };

    class a {

        /* renamed from: a  reason: collision with root package name */
        private int f6367a;
        private int b;

        public a(int i, int i2) {
            this.f6367a = i;
            this.b = i2;
        }

        public final int a() {
            return this.f6367a;
        }

        public final int b() {
            return this.b;
        }
    }

    public b(Context context, d dVar) {
        this.d = (SensorManager) context.getSystemService("sensor");
        this.b = dVar;
        this.e = 0;
        this.c = new HashMap<>();
        SensorsConfig D = MetaData.G().D();
        a(13, D.b());
        a(9, D.c());
        a(5, D.d());
        a(10, D.e());
        a(2, D.f());
        a(6, D.g());
        a(12, D.h());
        a(11, D.i());
        a(16, D.j());
    }

    public final void b() {
        this.d.unregisterListener(this.f);
    }

    public final JSONArray c() {
        try {
            return this.f6365a.a();
        } catch (Exception unused) {
            return null;
        }
    }

    public final void a() {
        Sensor defaultSensor;
        for (Integer intValue : this.c.keySet()) {
            int intValue2 = intValue.intValue();
            a aVar = this.c.get(Integer.valueOf(intValue2));
            if (Build.VERSION.SDK_INT >= aVar.a() && (defaultSensor = this.d.getDefaultSensor(intValue2)) != null) {
                this.d.registerListener(this.f, defaultSensor, aVar.b());
                this.e++;
            }
        }
    }

    private void a(int i, BaseSensorConfig baseSensorConfig) {
        if (baseSensorConfig.c()) {
            this.c.put(Integer.valueOf(i), new a(baseSensorConfig.b(), baseSensorConfig.a()));
        }
    }
}
