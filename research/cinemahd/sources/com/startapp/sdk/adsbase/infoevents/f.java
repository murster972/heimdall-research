package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import android.os.Build;

public class f implements Comparable<f>, Runnable {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6376a;
    private final e b;
    private final c c;
    private final Exception d = new Exception();

    static {
        Class<f> cls = f.class;
    }

    public f(Context context, e eVar, c cVar) {
        this.f6376a = context;
        this.b = eVar;
        this.c = cVar;
    }

    private Throwable a(Throwable th) {
        if (Build.VERSION.SDK_INT >= 19) {
            th.addSuppressed(this.d);
        }
        return th;
    }

    public /* synthetic */ int compareTo(Object obj) {
        return this.b.g().b() - ((f) obj).b.g().b();
    }

    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x006b */
    /* JADX WARNING: Removed duplicated region for block: B:103:0x0165 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0071 A[SYNTHETIC, Splitter:B:26:0x0071] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008c A[SYNTHETIC, Splitter:B:33:0x008c] */
    /* JADX WARNING: Removed duplicated region for block: B:40:0x00a7 A[SYNTHETIC, Splitter:B:40:0x00a7] */
    /* JADX WARNING: Removed duplicated region for block: B:52:0x00e3 A[SYNTHETIC, Splitter:B:52:0x00e3] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x0106 A[Catch:{ all -> 0x0149, all -> 0x016b }] */
    /* JADX WARNING: Removed duplicated region for block: B:66:0x013e  */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x0143 A[Catch:{ all -> 0x0077, all -> 0x017e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            com.startapp.sdk.adsbase.infoevents.e r0 = r12.b     // Catch:{ all -> 0x017e }
            r1 = 0
            r2 = r1
            r3 = r2
            r4 = r3
        L_0x0006:
            if (r0 == 0) goto L_0x0176
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r5 = r0.g()     // Catch:{ all -> 0x017e }
            boolean r5 = r5.f()     // Catch:{ all -> 0x017e }
            r6 = 1
            r7 = 0
            if (r5 == 0) goto L_0x0025
            if (r2 != 0) goto L_0x0020
            java.lang.String[] r2 = new java.lang.String[r6]     // Catch:{ all -> 0x017e }
            android.content.Context r5 = r12.f6376a     // Catch:{ all -> 0x017e }
            java.lang.String r5 = com.startapp.sdk.adsbase.j.u.i(r5)     // Catch:{ all -> 0x017e }
            r2[r7] = r5     // Catch:{ all -> 0x017e }
        L_0x0020:
            r5 = r2[r7]     // Catch:{ all -> 0x017e }
            r0.m(r5)     // Catch:{ all -> 0x017e }
        L_0x0025:
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r5 = r0.g()     // Catch:{ all -> 0x017e }
            boolean r5 = r5.c()     // Catch:{ all -> 0x017e }
            if (r5 == 0) goto L_0x004c
            if (r3 != 0) goto L_0x0037
            android.content.Context r3 = r12.f6376a     // Catch:{ all -> 0x017e }
            java.lang.String r3 = com.startapp.sdk.adsbase.j.u.e(r3)     // Catch:{ all -> 0x017e }
        L_0x0037:
            r0.i(r3)     // Catch:{ all -> 0x017e }
            if (r4 != 0) goto L_0x0042
            android.content.Context r4 = r12.f6376a     // Catch:{ all -> 0x017e }
            java.lang.String[] r4 = com.startapp.sdk.adsbase.j.u.j(r4)     // Catch:{ all -> 0x017e }
        L_0x0042:
            r5 = r4[r7]     // Catch:{ all -> 0x017e }
            r0.k(r5)     // Catch:{ all -> 0x017e }
            r5 = r4[r6]     // Catch:{ all -> 0x017e }
            r0.j(r5)     // Catch:{ all -> 0x017e }
        L_0x004c:
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r5 = r0.g()     // Catch:{ all -> 0x017e }
            boolean r8 = r5.h()     // Catch:{ all -> 0x017e }
            if (r8 == 0) goto L_0x005b
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            com.startapp.sdk.adsbase.SimpleTokenUtils.b((android.content.Context) r8)     // Catch:{ all -> 0x017e }
        L_0x005b:
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            r0.d((android.content.Context) r8)     // Catch:{ all -> 0x017e }
            boolean r8 = r5.c()     // Catch:{ all -> 0x017e }
            if (r8 == 0) goto L_0x006b
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x006b }
            r0.b(r8, r1)     // Catch:{ all -> 0x006b }
        L_0x006b:
            boolean r8 = r5.d()     // Catch:{ all -> 0x017e }
            if (r8 == 0) goto L_0x0086
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x0077 }
            r0.c((android.content.Context) r8)     // Catch:{ all -> 0x0077 }
            goto L_0x0086
        L_0x0077:
            r8 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r9 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x017e }
            java.lang.Throwable r8 = r12.a(r8)     // Catch:{ all -> 0x017e }
            r9.<init>((java.lang.Throwable) r8)     // Catch:{ all -> 0x017e }
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            r9.a((android.content.Context) r8)     // Catch:{ all -> 0x017e }
        L_0x0086:
            boolean r8 = r5.e()     // Catch:{ all -> 0x017e }
            if (r8 == 0) goto L_0x00a1
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x0092 }
            r0.e(r8)     // Catch:{ all -> 0x0092 }
            goto L_0x00a1
        L_0x0092:
            r8 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r9 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x017e }
            java.lang.Throwable r8 = r12.a(r8)     // Catch:{ all -> 0x017e }
            r9.<init>((java.lang.Throwable) r8)     // Catch:{ all -> 0x017e }
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            r9.a((android.content.Context) r8)     // Catch:{ all -> 0x017e }
        L_0x00a1:
            boolean r8 = r5.g()     // Catch:{ all -> 0x017e }
            if (r8 == 0) goto L_0x00bc
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x00ad }
            r0.b((android.content.Context) r8)     // Catch:{ all -> 0x00ad }
            goto L_0x00bc
        L_0x00ad:
            r8 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r9 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x017e }
            java.lang.Throwable r8 = r12.a(r8)     // Catch:{ all -> 0x017e }
            r9.<init>((java.lang.Throwable) r8)     // Catch:{ all -> 0x017e }
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            r9.a((android.content.Context) r8)     // Catch:{ all -> 0x017e }
        L_0x00bc:
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x00ce }
            com.startapp.sdk.c.c r8 = com.startapp.sdk.c.c.a(r8)     // Catch:{ all -> 0x00ce }
            com.startapp.sdk.g.a r8 = r8.e()     // Catch:{ all -> 0x00ce }
            java.lang.String r8 = r8.a((java.lang.Object) r0)     // Catch:{ all -> 0x00ce }
            r0.b((java.lang.String) r8)     // Catch:{ all -> 0x00ce }
            goto L_0x00dd
        L_0x00ce:
            r8 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r9 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x017e }
            java.lang.Throwable r8 = r12.a(r8)     // Catch:{ all -> 0x017e }
            r9.<init>((java.lang.Throwable) r8)     // Catch:{ all -> 0x017e }
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            r9.a((android.content.Context) r8)     // Catch:{ all -> 0x017e }
        L_0x00dd:
            java.io.File r8 = r0.j()     // Catch:{ all -> 0x017e }
            if (r8 == 0) goto L_0x00fa
            java.lang.String r8 = com.startapp.sdk.adsbase.j.u.b((java.io.File) r8)     // Catch:{ all -> 0x00eb }
            r0.g(r8)     // Catch:{ all -> 0x00eb }
            goto L_0x00fa
        L_0x00eb:
            r8 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r9 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x017e }
            java.lang.Throwable r8 = r12.a(r8)     // Catch:{ all -> 0x017e }
            r9.<init>((java.lang.Throwable) r8)     // Catch:{ all -> 0x017e }
            android.content.Context r8 = r12.f6376a     // Catch:{ all -> 0x017e }
            r9.a((android.content.Context) r8)     // Catch:{ all -> 0x017e }
        L_0x00fa:
            com.startapp.sdk.adsbase.remoteconfig.MetaData r8 = com.startapp.sdk.adsbase.remoteconfig.MetaData.G()     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.adsbase.infoevents.AnalyticsConfig r8 = r8.analytics     // Catch:{ all -> 0x0149 }
            java.lang.String r9 = r0.k()     // Catch:{ all -> 0x0149 }
            if (r9 != 0) goto L_0x0115
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r9 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.PERIODIC     // Catch:{ all -> 0x0149 }
            boolean r9 = r9.equals(r5)     // Catch:{ all -> 0x0149 }
            if (r9 == 0) goto L_0x0113
            java.lang.String r9 = r8.a()     // Catch:{ all -> 0x0149 }
            goto L_0x0115
        L_0x0113:
            java.lang.String r9 = r8.hostSecured     // Catch:{ all -> 0x0149 }
        L_0x0115:
            android.content.Context r10 = r12.f6376a     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.c.c r10 = com.startapp.sdk.c.c.a(r10)     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.adsbase.d.b r10 = r10.l()     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.adsbase.d.a r9 = r10.a((java.lang.String) r9)     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.adsbase.d.a r9 = r9.a((com.startapp.sdk.adsbase.c) r0)     // Catch:{ all -> 0x0149 }
            int r10 = r8.b()     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.adsbase.d.a r9 = r9.a((int) r10)     // Catch:{ all -> 0x0149 }
            long r10 = r8.c()     // Catch:{ all -> 0x0149 }
            com.startapp.sdk.adsbase.d.a r8 = r9.a((long) r10)     // Catch:{ all -> 0x0149 }
            java.lang.String r5 = r8.b()     // Catch:{ all -> 0x0149 }
            if (r5 == 0) goto L_0x013e
            goto L_0x013f
        L_0x013e:
            r6 = 0
        L_0x013f:
            com.startapp.sdk.adsbase.infoevents.c r5 = r12.c     // Catch:{ all -> 0x017e }
            if (r5 == 0) goto L_0x0165
            com.startapp.sdk.adsbase.infoevents.c r5 = r12.c     // Catch:{ all -> 0x017e }
            r5.a(r0, r6)     // Catch:{ all -> 0x017e }
            goto L_0x0165
        L_0x0149:
            r6 = move-exception
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r8 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.EXCEPTION     // Catch:{ all -> 0x016b }
            if (r5 == r8) goto L_0x015c
            com.startapp.sdk.adsbase.infoevents.e r5 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x016b }
            java.lang.Throwable r6 = r12.a(r6)     // Catch:{ all -> 0x016b }
            r5.<init>((java.lang.Throwable) r6)     // Catch:{ all -> 0x016b }
            android.content.Context r6 = r12.f6376a     // Catch:{ all -> 0x016b }
            r5.a((android.content.Context) r6)     // Catch:{ all -> 0x016b }
        L_0x015c:
            com.startapp.sdk.adsbase.infoevents.c r5 = r12.c     // Catch:{ all -> 0x017e }
            if (r5 == 0) goto L_0x0165
            com.startapp.sdk.adsbase.infoevents.c r5 = r12.c     // Catch:{ all -> 0x017e }
            r5.a(r0, r7)     // Catch:{ all -> 0x017e }
        L_0x0165:
            com.startapp.sdk.adsbase.infoevents.e r0 = r0.l()     // Catch:{ all -> 0x017e }
            goto L_0x0006
        L_0x016b:
            r1 = move-exception
            com.startapp.sdk.adsbase.infoevents.c r2 = r12.c     // Catch:{ all -> 0x017e }
            if (r2 == 0) goto L_0x0175
            com.startapp.sdk.adsbase.infoevents.c r2 = r12.c     // Catch:{ all -> 0x017e }
            r2.a(r0, r7)     // Catch:{ all -> 0x017e }
        L_0x0175:
            throw r1     // Catch:{ all -> 0x017e }
        L_0x0176:
            com.startapp.sdk.adsbase.infoevents.c r0 = r12.c
            if (r0 == 0) goto L_0x0194
            r0.a()
            return
        L_0x017e:
            r0 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r1 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0195 }
            java.lang.Throwable r0 = r12.a(r0)     // Catch:{ all -> 0x0195 }
            r1.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x0195 }
            android.content.Context r0 = r12.f6376a     // Catch:{ all -> 0x0195 }
            r1.a((android.content.Context) r0)     // Catch:{ all -> 0x0195 }
            com.startapp.sdk.adsbase.infoevents.c r0 = r12.c
            if (r0 == 0) goto L_0x0194
            r0.a()
        L_0x0194:
            return
        L_0x0195:
            r0 = move-exception
            com.startapp.sdk.adsbase.infoevents.c r1 = r12.c
            if (r1 == 0) goto L_0x019d
            r1.a()
        L_0x019d:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.infoevents.f.run():void");
    }
}
