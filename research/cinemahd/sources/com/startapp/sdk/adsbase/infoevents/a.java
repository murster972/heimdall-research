package com.startapp.sdk.adsbase.infoevents;

import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.j.m;

public final class a extends e {
    private String b;
    private String c;
    private boolean d;
    private String e;

    public a(InfoEventCategory infoEventCategory) {
        super(infoEventCategory);
    }

    /* access modifiers changed from: protected */
    public final void a(m mVar) throws SDKException {
        super.a(mVar);
        mVar.a("sens", this.b, false);
        mVar.a("bt", this.c, false);
        mVar.a("isService", Boolean.valueOf(this.d), false);
        mVar.a("packagingType", this.e, false);
    }

    public final void c(String str) {
        if (str != null) {
            this.b = com.startapp.common.b.a.c(str);
        }
    }

    public final void d(String str) {
        if (str != null) {
            this.c = com.startapp.common.b.a.c(str);
        }
    }

    public final void e(String str) {
        this.e = str;
    }

    public final String toString() {
        return super.toString() + " DataEventRequest [sensors=" + this.b + ", bluetooth=" + this.c + ", isService=" + this.d + ", packagingType=" + this.e + "]";
    }

    public final void a(boolean z) {
        this.d = z;
    }
}
