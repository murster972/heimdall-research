package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import android.util.Pair;
import com.startapp.common.SDKException;
import com.startapp.common.b.a;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.c.b;
import com.startapp.sdk.adsbase.j.m;
import com.startapp.sdk.adsbase.j.u;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import java.io.File;
import org.json.JSONArray;

public class e extends c {
    private final InfoEventCategory b;
    private String c;
    private String d;
    private boolean e;
    private String f;
    private String g;
    private String h;
    private String i;
    private Long j;
    private String k;
    private String l;
    private Object m;
    private File n;
    private String o;
    private e p;
    private String q;
    private Throwable r;

    public e(InfoEventCategory infoEventCategory) {
        super(8);
        this.e = true;
        if (infoEventCategory != InfoEventCategory.EXCEPTION) {
            this.b = infoEventCategory;
        } else {
            this.b = InfoEventCategory.ERROR;
        }
        InfoEventCategory infoEventCategory2 = this.b;
        if (infoEventCategory2 == InfoEventCategory.ERROR || infoEventCategory2 == InfoEventCategory.GENERAL) {
            this.k = u.a(u.d());
        }
    }

    public final void a(long j2) {
        this.j = Long.valueOf(j2);
    }

    public final e f(String str) {
        if (this.b != InfoEventCategory.EXCEPTION) {
            this.c = str;
        }
        return this;
    }

    public final InfoEventCategory g() {
        return this.b;
    }

    public final String h() {
        return this.c;
    }

    public final e i() {
        this.e = false;
        return this;
    }

    public final e j(String str) {
        this.h = str;
        return this;
    }

    public final e k(String str) {
        this.i = str;
        return this;
    }

    public final void l(String str) {
        this.k = str;
    }

    public final e m(String str) {
        this.l = str;
        return this;
    }

    public final e n(String str) {
        this.o = str;
        return this;
    }

    public final void o(String str) {
        this.q = str;
    }

    public String toString() {
        return super.toString();
    }

    public final e a(JSONArray jSONArray) {
        this.m = jSONArray;
        return this;
    }

    public final e g(String str) {
        this.d = str;
        return this;
    }

    public final e h(String str) {
        this.f = str;
        return this;
    }

    public final e i(String str) {
        this.g = str;
        return this;
    }

    public final File j() {
        return this.n;
    }

    public final String k() {
        return this.o;
    }

    public final e l() {
        return this.p;
    }

    public final String m() {
        return this.q;
    }

    public final e a(File file) {
        this.n = file;
        return this;
    }

    public final e a(e eVar) {
        this.p = eVar;
        return this;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x001d, code lost:
        return r3.toString();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0022, code lost:
        return r3.getMessage();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0019 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.Throwable r3) {
        /*
            java.io.ByteArrayOutputStream r0 = new java.io.ByteArrayOutputStream     // Catch:{ all -> 0x0019 }
            r0.<init>()     // Catch:{ all -> 0x0019 }
            java.io.PrintWriter r1 = new java.io.PrintWriter     // Catch:{ all -> 0x0019 }
            java.io.OutputStream r2 = com.startapp.sdk.adsbase.j.u.a((java.io.OutputStream) r0)     // Catch:{ all -> 0x0019 }
            r1.<init>(r2)     // Catch:{ all -> 0x0019 }
            com.startapp.sdk.adsbase.c.b.a(r3, r1)     // Catch:{ all -> 0x0019 }
            r1.close()     // Catch:{ all -> 0x0019 }
            java.lang.String r3 = r0.toString()     // Catch:{ all -> 0x0019 }
            return r3
        L_0x0019:
            java.lang.String r3 = r3.toString()     // Catch:{ all -> 0x001e }
            return r3
        L_0x001e:
            java.lang.String r3 = r3.getMessage()
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.infoevents.e.a(java.lang.Throwable):java.lang.String");
    }

    public e(Throwable th) {
        super(8);
        this.e = true;
        this.b = InfoEventCategory.EXCEPTION;
        this.d = a(th);
        this.c = u.a(b.a(th));
        this.k = u.a(u.d());
        this.r = th;
    }

    public final void a(Context context) {
        Context k2 = u.k(context);
        if (k2 != null) {
            com.startapp.sdk.c.c.a(k2).o().a(this, (c) null);
        }
    }

    public final void a(Context context, c cVar) {
        Context k2 = u.k(context);
        if (k2 == null) {
            cVar.a();
        } else {
            com.startapp.sdk.c.c.a(k2).o().a(this, cVar);
        }
    }

    /* access modifiers changed from: protected */
    public void a(m mVar) throws SDKException {
        super.a(mVar);
        Long l2 = this.j;
        String l3 = l2 != null ? l2.toString() : a.d();
        mVar.a(a.a(), l3, true);
        mVar.a(a.b(), a.b(l3), true);
        mVar.a("category", this.b.a(), true);
        mVar.a("value", this.c, false);
        mVar.a("d", this.f, false);
        mVar.a(AdUnitActivity.EXTRA_ORIENTATION, this.g, false);
        mVar.a("usedRam", this.h, false);
        mVar.a("freeRam", this.i, false);
        mVar.a("sessionTime", (Object) null, false);
        mVar.a("appActivity", this.k, false);
        mVar.a("details", this.d, false, this.e);
        mVar.a("details_json", this.m, false);
        mVar.a("cellScanRes", this.l, false);
        Pair<String, String> c2 = SimpleTokenUtils.c();
        Pair<String, String> d2 = SimpleTokenUtils.d();
        mVar.a((String) c2.first, c2.second, false);
        mVar.a((String) d2.first, d2.second, false);
    }
}
