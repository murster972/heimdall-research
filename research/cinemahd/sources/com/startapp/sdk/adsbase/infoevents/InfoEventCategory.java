package com.startapp.sdk.adsbase.infoevents;

public enum InfoEventCategory {
    GENERAL("general", 23, 50),
    ERROR("error", 17, 70),
    EXCEPTION("exception", 17, 70),
    EXCEPTION_FATAL("exception_fatal", 17, 60),
    NETWORK_DIAGNOSTIC("netdiag", 17, 70),
    PERIODIC("periodic", -1, 10),
    SUCCESS_SMART_REDIRECT_HOP_INFO("success_smart_redirect_hop_info", 17, 40),
    TRIGGERED_LINK("triggeredLink", 17, 30),
    INSIGHT_CORE_CT("ct", 23, 20),
    INSIGHT_CORE_LT("lt", 23, 20),
    INSIGHT_CORE_NIR("nir", 23, 20);
    
    private final int flags;
    private final int priority;
    private final String value;

    private InfoEventCategory(String str, int i, int i2) {
        this.value = str;
        this.flags = i;
        this.priority = i2;
    }

    public final String a() {
        return this.value;
    }

    public final int b() {
        return this.priority;
    }

    public final boolean c() {
        return (this.flags & 1) != 0;
    }

    public final boolean d() {
        return (this.flags & 2) != 0;
    }

    public final boolean e() {
        return (this.flags & 4) != 0;
    }

    public final boolean f() {
        return (this.flags & 8) != 0;
    }

    public final boolean g() {
        return (this.flags & 16) != 0;
    }

    public final boolean h() {
        return (this.flags & 32) != 0;
    }

    public static InfoEventCategory a(String str) {
        for (InfoEventCategory infoEventCategory : values()) {
            if (infoEventCategory.value.equals(str)) {
                return infoEventCategory;
            }
        }
        return null;
    }
}
