package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import com.startapp.sdk.adsbase.j.g;
import java.util.concurrent.Executor;

public final class d {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6375a;
    private final Executor b;
    private final g<AnalyticsConfig> c;

    public d(Context context, Executor executor, g<AnalyticsConfig> gVar) {
        this.f6375a = context;
        this.b = executor;
        this.c = gVar;
    }

    /* access modifiers changed from: package-private */
    public final void a(e eVar, c cVar) {
        AnalyticsConfig a2 = this.c.a();
        if (a2 != null && !a2.dns) {
            this.b.execute(new f(this.f6375a, eVar, cVar));
        } else if (cVar != null) {
            cVar.a();
        }
    }
}
