package com.startapp.sdk.adsbase.infoevents;

import android.content.Context;
import com.startapp.common.d;
import com.startapp.sdk.adsbase.f.c;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected d f6372a;
    private Context b;
    private ArrayList<com.startapp.sdk.adsbase.f.a> c;
    private final a d = new a(InfoEventCategory.PERIODIC);
    private final AtomicBoolean e;
    private int f;

    public static class a {
    }

    public b(Context context, boolean z, d dVar) {
        int i = 0;
        this.e = new AtomicBoolean(false);
        this.b = context;
        this.f6372a = dVar;
        this.d.a(z);
        this.d.e(u.n(context));
        AnonymousClass1 r5 = new Runnable() {
            public final void run() {
                synchronized (b.this) {
                    if (b.a(b.this) == 0) {
                        b.this.b();
                    }
                }
            }
        };
        if (MetaData.G().D().a(context)) {
            if (this.c == null) {
                this.c = new ArrayList<>();
            }
            this.c.add(new c(context, r5, this.d));
        }
        if (MetaData.G().E().a(context)) {
            if (this.c == null) {
                this.c = new ArrayList<>();
            }
            this.c.add(new com.startapp.sdk.adsbase.f.b(context, r5, this.d));
        }
        ArrayList<com.startapp.sdk.adsbase.f.a> arrayList = this.c;
        this.f = arrayList != null ? arrayList.size() : i;
    }

    static /* synthetic */ int a(b bVar) {
        int i = bVar.f - 1;
        bVar.f = i;
        return i;
    }

    /* access modifiers changed from: private */
    public void b() {
        this.d.a(this.b, new c() {

            /* renamed from: a  reason: collision with root package name */
            private Boolean f6374a;

            public final void a(e eVar, boolean z) {
                if (this.f6374a == null) {
                    this.f6374a = Boolean.valueOf(z);
                    return;
                }
                throw new IllegalStateException();
            }

            public final void a() {
                d dVar = b.this.f6372a;
                if (dVar != null) {
                    dVar.a(this.f6374a);
                }
            }
        });
        this.e.set(false);
    }

    public final synchronized void a() {
        if (this.f > 0) {
            if (this.e.compareAndSet(false, true)) {
                for (int i = 0; i < this.f; i++) {
                    if (this.c != null) {
                        this.c.get(i).a();
                    }
                }
                return;
            }
        }
        b();
    }
}
