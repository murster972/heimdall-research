package com.startapp.sdk.adsbase.infoevents;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.remoteconfig.AnalyticsReporterConfig;
import java.io.Serializable;
import java.util.concurrent.TimeUnit;

public class AnalyticsConfig implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6370a = "https://infoevent.startappservice.com/tracking/infoEvent";
    private static final long serialVersionUID = -5497097103874215198L;
    public boolean dns = false;
    public String hostPeriodic;
    public String hostSecured;
    @d(a = true)
    private AnalyticsReporterConfig reporter = new AnalyticsReporterConfig();
    private int retryNum = 3;
    private int retryTime = 10;
    private boolean sendHopsOnFirstSucceededSmartRedirect = false;
    private float succeededSmartRedirectInfoProbability = 0.01f;

    public AnalyticsConfig() {
        String str = f6370a;
        this.hostSecured = str;
        this.hostPeriodic = str;
    }

    public final String a() {
        String str = this.hostPeriodic;
        return str != null ? str : f6370a;
    }

    public final int b() {
        return this.retryNum;
    }

    public final long c() {
        return TimeUnit.SECONDS.toMillis((long) this.retryTime);
    }

    public final float d() {
        return this.succeededSmartRedirectInfoProbability;
    }

    public final boolean e() {
        return this.sendHopsOnFirstSucceededSmartRedirect;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AnalyticsConfig.class == obj.getClass()) {
            AnalyticsConfig analyticsConfig = (AnalyticsConfig) obj;
            return this.dns == analyticsConfig.dns && this.retryNum == analyticsConfig.retryNum && this.retryTime == analyticsConfig.retryTime && Float.compare(this.succeededSmartRedirectInfoProbability, analyticsConfig.succeededSmartRedirectInfoProbability) == 0 && this.sendHopsOnFirstSucceededSmartRedirect == analyticsConfig.sendHopsOnFirstSucceededSmartRedirect && u.b(this.hostSecured, analyticsConfig.hostSecured) && u.b(this.hostPeriodic, analyticsConfig.hostPeriodic) && u.b(this.reporter, analyticsConfig.reporter);
        }
    }

    public final AnalyticsReporterConfig f() {
        return this.reporter;
    }

    public int hashCode() {
        return u.a(this.hostSecured, this.hostPeriodic, Boolean.valueOf(this.dns), Integer.valueOf(this.retryNum), Integer.valueOf(this.retryTime), Float.valueOf(this.succeededSmartRedirectInfoProbability), Boolean.valueOf(this.sendHopsOnFirstSucceededSmartRedirect), this.reporter);
    }
}
