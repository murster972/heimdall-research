package com.startapp.sdk.adsbase;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Pair;
import com.startapp.common.Constants;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import com.startapp.sdk.adsbase.remoteconfig.b;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

public class SimpleTokenUtils {

    /* renamed from: a  reason: collision with root package name */
    private static List<PackageInfo> f6253a = null;
    private static List<PackageInfo> b = null;
    private static long c = 0;
    private static volatile Pair<TokenType, String> d = null;
    private static volatile Pair<TokenType, String> e = null;
    private static boolean f = true;
    private static boolean g = false;
    private static TokenType h = TokenType.UNDEFINED;

    private enum TokenType {
        T1("token"),
        T2("token2"),
        UNDEFINED("");
        
        private final String text;

        private TokenType(String str) {
            this.text = str;
        }

        public final String toString() {
            return this.text;
        }
    }

    public static long a() {
        return c;
    }

    public static void b(Context context) {
        a(context, MetaData.G().f().a(context));
    }

    public static void c(final Context context) {
        try {
            if ((d == null || e == null) && MetaData.G().f().a(context)) {
                ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable() {
                    public final void run() {
                        SimpleTokenUtils.b(context);
                    }
                });
            }
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    static Pair<String, String> d(Context context) {
        return a(context, MetaData.G().f().a(context), MetaData.G().y(), MetaData.G().z());
    }

    private static Pair<TokenType, String> e(Context context) {
        if (d == null) {
            b(context);
        }
        j.b(context, "shared_prefs_simple_token", (String) d.second);
        f = false;
        h = TokenType.UNDEFINED;
        return new Pair<>(TokenType.T1, d.second);
    }

    private static Pair<TokenType, String> f(Context context) {
        if (e == null) {
            b(context);
        }
        j.b(context, "shared_prefs_simple_token2", (String) e.second);
        f = false;
        h = TokenType.UNDEFINED;
        return new Pair<>(TokenType.T2, e.second);
    }

    public static void a(final Context context) {
        c(context);
        f = true;
        g = false;
        h = TokenType.UNDEFINED;
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.PACKAGE_ADDED");
        intentFilter.addAction("android.intent.action.PACKAGE_REMOVED");
        context.getApplicationContext().registerReceiver(new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                SimpleTokenUtils.b();
                SimpleTokenUtils.c(context);
            }
        }, intentFilter);
        MetaData.G().a((b) new b() {
            public final void a(MetaDataRequest.RequestReason requestReason, boolean z) {
                if (z) {
                    SimpleTokenUtils.b();
                    SimpleTokenUtils.c(context);
                }
                MetaData.G().a((b) this);
            }

            public final void a() {
                MetaData.G().a((b) this);
            }
        });
    }

    public static void b() {
        d = null;
        e = null;
    }

    private static List<PackageInfo> b(List<PackageInfo> list) {
        if (list.size() <= 100) {
            return list;
        }
        ArrayList arrayList = new ArrayList(list);
        c((List<PackageInfo>) arrayList);
        return arrayList.subList(0, 100);
    }

    public static Pair<String, String> c() {
        if (d != null) {
            return new Pair<>(((TokenType) d.first).toString(), d.second);
        }
        return new Pair<>(TokenType.T1.toString(), "");
    }

    public static Pair<String, String> d() {
        if (e != null) {
            return new Pair<>(((TokenType) e.first).toString(), e.second);
        }
        return new Pair<>(TokenType.T2.toString(), "");
    }

    private static void c(List<PackageInfo> list) {
        if (Build.VERSION.SDK_INT >= 9) {
            Collections.sort(list, new Comparator<PackageInfo>() {
                public final /* bridge */ /* synthetic */ int compare(Object obj, Object obj2) {
                    int i = (((PackageInfo) obj).firstInstallTime > ((PackageInfo) obj2).firstInstallTime ? 1 : (((PackageInfo) obj).firstInstallTime == ((PackageInfo) obj2).firstInstallTime ? 0 : -1));
                    if (i > 0) {
                        return -1;
                    }
                    return i == 0 ? 0 : 1;
                }
            });
        }
    }

    private static synchronized void a(Context context, boolean z) {
        synchronized (SimpleTokenUtils.class) {
            if ((d == null || e == null) && z) {
                try {
                    PackageManager packageManager = context.getPackageManager();
                    Set<String> w = MetaData.G().w();
                    Set<String> x = MetaData.G().x();
                    f6253a = new CopyOnWriteArrayList();
                    b = new CopyOnWriteArrayList();
                    try {
                        List<PackageInfo> a2 = com.startapp.common.b.b.a(packageManager);
                        c = System.currentTimeMillis();
                        PackageInfo packageInfo = null;
                        for (PackageInfo next : a2) {
                            if (!com.startapp.common.b.b.a(next)) {
                                if (Build.VERSION.SDK_INT >= 9) {
                                    long j = next.firstInstallTime;
                                    if (j < c && j >= 1291593600000L) {
                                        c = j;
                                    }
                                }
                                f6253a.add(next);
                                String installerPackageName = packageManager.getInstallerPackageName(next.packageName);
                                if (w != null && w.contains(installerPackageName)) {
                                    b.add(next);
                                }
                            } else if (x.contains(next.packageName)) {
                                f6253a.add(next);
                            } else if (next.packageName.equals(Constants.f5922a)) {
                                packageInfo = next;
                            }
                        }
                        f6253a = b(f6253a);
                        b = b(b);
                        if (packageInfo != null) {
                            f6253a.add(0, packageInfo);
                        }
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                    d = new Pair<>(TokenType.T1, com.iab.omid.library.startapp.b.a(a(f6253a)));
                    e = new Pair<>(TokenType.T2, com.iab.omid.library.startapp.b.a(a(b)));
                } catch (Throwable th2) {
                    new e(th2).a(context);
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:16:0x002a A[Catch:{ all -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002b A[Catch:{ all -> 0x005a }] */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0038 A[Catch:{ all -> 0x005a }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static synchronized android.util.Pair<java.lang.String, java.lang.String> a(android.content.Context r4, boolean r5, boolean r6, boolean r7) {
        /*
            java.lang.Class<com.startapp.sdk.adsbase.SimpleTokenUtils> r0 = com.startapp.sdk.adsbase.SimpleTokenUtils.class
            monitor-enter(r0)
            android.util.Pair r1 = new android.util.Pair     // Catch:{ all -> 0x0074 }
            com.startapp.sdk.adsbase.SimpleTokenUtils$TokenType r2 = com.startapp.sdk.adsbase.SimpleTokenUtils.TokenType.T1     // Catch:{ all -> 0x0074 }
            java.lang.String r3 = ""
            r1.<init>(r2, r3)     // Catch:{ all -> 0x0074 }
            if (r5 == 0) goto L_0x0063
            com.startapp.sdk.adsbase.SimpleTokenUtils$TokenType r5 = h     // Catch:{ all -> 0x005a }
            com.startapp.sdk.adsbase.SimpleTokenUtils$TokenType r2 = com.startapp.sdk.adsbase.SimpleTokenUtils.TokenType.UNDEFINED     // Catch:{ all -> 0x005a }
            if (r5 != r2) goto L_0x0049
            boolean r5 = f     // Catch:{ all -> 0x005a }
            boolean r2 = g     // Catch:{ all -> 0x005a }
            if (r2 == 0) goto L_0x0024
            boolean r2 = f     // Catch:{ all -> 0x005a }
            if (r2 == 0) goto L_0x001f
            goto L_0x0024
        L_0x001f:
            android.util.Pair r2 = f(r4)     // Catch:{ all -> 0x005a }
            goto L_0x0028
        L_0x0024:
            android.util.Pair r2 = e(r4)     // Catch:{ all -> 0x005a }
        L_0x0028:
            if (r7 == 0) goto L_0x002b
            goto L_0x0032
        L_0x002b:
            boolean r5 = g     // Catch:{ all -> 0x005a }
            if (r5 != 0) goto L_0x0031
            r5 = 1
            goto L_0x0032
        L_0x0031:
            r5 = 0
        L_0x0032:
            g = r5     // Catch:{ all -> 0x005a }
            if (r6 == 0) goto L_0x0038
        L_0x0036:
            r1 = r2
            goto L_0x0063
        L_0x0038:
            java.lang.String r5 = "shared_prefs_simple_token"
            java.lang.String r6 = ""
            java.lang.String r5 = com.startapp.sdk.adsbase.j.a((android.content.Context) r4, (java.lang.String) r5, (java.lang.String) r6)     // Catch:{ all -> 0x005a }
            java.lang.Object r6 = r2.second     // Catch:{ all -> 0x005a }
            boolean r4 = r5.equals(r6)     // Catch:{ all -> 0x005a }
            if (r4 != 0) goto L_0x0063
            goto L_0x0036
        L_0x0049:
            com.startapp.sdk.adsbase.SimpleTokenUtils$TokenType r5 = h     // Catch:{ all -> 0x005a }
            com.startapp.sdk.adsbase.SimpleTokenUtils$TokenType r6 = com.startapp.sdk.adsbase.SimpleTokenUtils.TokenType.T1     // Catch:{ all -> 0x005a }
            if (r5 != r6) goto L_0x0054
            android.util.Pair r4 = e(r4)     // Catch:{ all -> 0x005a }
            goto L_0x0058
        L_0x0054:
            android.util.Pair r4 = f(r4)     // Catch:{ all -> 0x005a }
        L_0x0058:
            r1 = r4
            goto L_0x0063
        L_0x005a:
            r5 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r6 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0074 }
            r6.<init>((java.lang.Throwable) r5)     // Catch:{ all -> 0x0074 }
            r6.a((android.content.Context) r4)     // Catch:{ all -> 0x0074 }
        L_0x0063:
            android.util.Pair r4 = new android.util.Pair     // Catch:{ all -> 0x0074 }
            java.lang.Object r5 = r1.first     // Catch:{ all -> 0x0074 }
            com.startapp.sdk.adsbase.SimpleTokenUtils$TokenType r5 = (com.startapp.sdk.adsbase.SimpleTokenUtils.TokenType) r5     // Catch:{ all -> 0x0074 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x0074 }
            java.lang.Object r6 = r1.second     // Catch:{ all -> 0x0074 }
            r4.<init>(r5, r6)     // Catch:{ all -> 0x0074 }
            monitor-exit(r0)
            return r4
        L_0x0074:
            r4 = move-exception
            monitor-exit(r0)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.SimpleTokenUtils.a(android.content.Context, boolean, boolean, boolean):android.util.Pair");
    }

    static void a(Pair<String, String> pair) {
        h = TokenType.valueOf((String) pair.first);
    }

    private static List<String> a(List<PackageInfo> list) {
        ArrayList arrayList = new ArrayList();
        for (PackageInfo packageInfo : list) {
            arrayList.add(packageInfo.packageName);
        }
        return arrayList;
    }
}
