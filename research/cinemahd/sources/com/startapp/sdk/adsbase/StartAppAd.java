package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.b;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.ads.splash.SplashHideListener;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.activities.AppWallActivity;
import com.startapp.sdk.adsbase.activities.OverlayActivity;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.cache.a;
import com.startapp.sdk.adsbase.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.c.c;
import java.io.Serializable;

public class StartAppAd extends Ad {
    private static final long serialVersionUID = 1;
    public f ad = null;
    private CacheKey adKey = null;
    private AdMode adMode = AdMode.AUTOMATIC;
    private AdPreferences adPreferences = null;
    AdDisplayListener callback = null;
    private BroadcastReceiver callbackBroadcastReceiver = new BroadcastReceiver() {
        private void a(Context context) {
            b.a(context).a((BroadcastReceiver) this);
        }

        public final void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (action == null) {
                action = "";
            }
            if (action.equals("com.startapp.android.ShowFailedDisplayBroadcastListener")) {
                Bundle extras = intent.getExtras();
                if (extras == null) {
                    extras = Bundle.EMPTY;
                }
                if (extras.containsKey("showFailedReason")) {
                    StartAppAd.this.a((AdDisplayListener.NotDisplayedReason) extras.getSerializable("showFailedReason"));
                }
                StartAppAd startAppAd = StartAppAd.this;
                AdDisplayListener adDisplayListener = startAppAd.callback;
                if (adDisplayListener != null) {
                    adDisplayListener.adNotDisplayed(startAppAd);
                }
                a(context);
            } else if (action.equals("com.startapp.android.ShowDisplayBroadcastListener")) {
                StartAppAd startAppAd2 = StartAppAd.this;
                AdDisplayListener adDisplayListener2 = startAppAd2.callback;
                if (adDisplayListener2 != null) {
                    adDisplayListener2.adDisplayed(startAppAd2);
                }
            } else if (action.equals("com.startapp.android.OnClickCallback")) {
                StartAppAd startAppAd3 = StartAppAd.this;
                AdDisplayListener adDisplayListener3 = startAppAd3.callback;
                if (adDisplayListener3 != null) {
                    adDisplayListener3.adClicked(startAppAd3);
                }
            } else if (!action.equals("com.startapp.android.OnVideoCompleted")) {
                StartAppAd startAppAd4 = StartAppAd.this;
                AdDisplayListener adDisplayListener4 = startAppAd4.callback;
                if (adDisplayListener4 != null) {
                    adDisplayListener4.adHidden(startAppAd4);
                }
                a(context);
            } else if (StartAppAd.this.videoListener != null) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        StartAppAd.this.videoListener.onVideoCompleted();
                    }
                });
            }
            StartAppAd.this.ad = null;
        }
    };
    VideoListener videoListener = null;

    /* renamed from: com.startapp.sdk.adsbase.StartAppAd$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6260a = new int[AdMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(10:0|1|2|3|4|5|6|7|8|10) */
        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|(3:7|8|10)) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        static {
            /*
                com.startapp.sdk.adsbase.StartAppAd$AdMode[] r0 = com.startapp.sdk.adsbase.StartAppAd.AdMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6260a = r0
                int[] r0 = f6260a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r1 = com.startapp.sdk.adsbase.StartAppAd.AdMode.FULLPAGE     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6260a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r1 = com.startapp.sdk.adsbase.StartAppAd.AdMode.OFFERWALL     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6260a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r1 = com.startapp.sdk.adsbase.StartAppAd.AdMode.OVERLAY     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6260a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r1 = com.startapp.sdk.adsbase.StartAppAd.AdMode.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.StartAppAd.AnonymousClass3.<clinit>():void");
        }
    }

    public enum AdMode {
        AUTOMATIC,
        FULLPAGE,
        OFFERWALL,
        REWARDED_VIDEO,
        VIDEO,
        OVERLAY
    }

    public StartAppAd(Context context) {
        super(context, (AdPreferences.Placement) null);
    }

    public static void disableAutoInterstitial() {
        e.a.f6343a.b();
    }

    public static void disableSplash() {
        k.a().c(false);
    }

    public static void enableAutoInterstitial() {
        e.a.f6343a.a();
    }

    public static void enableConsent(Context context, boolean z) {
        c.a(context).f().a(z);
    }

    public static void init(Context context, String str, String str2) {
        StartAppSDK.init(context, str, str2);
    }

    public static void setAutoInterstitialPreferences(AutoInterstitialPreferences autoInterstitialPreferences) {
        e.a.f6343a.a(autoInterstitialPreferences);
    }

    public static void setCommonAdsPreferences(Context context, SDKAdPreferences sDKAdPreferences) {
        Context k = u.k(context);
        if (k != null) {
            k.a().a(k, sDKAdPreferences);
        }
    }

    public static void setReturnAdsPreferences(AdPreferences adPreferences2) {
        k.a().a(adPreferences2);
    }

    public static void showSplash(Activity activity, Bundle bundle) {
        showSplash(activity, bundle, new SplashConfig());
    }

    /* access modifiers changed from: protected */
    public AdRulesResult a(String str, AdPreferences.Placement placement) {
        return AdsCommonMetaData.a().G().a(placement, str);
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences2, com.startapp.sdk.adsbase.adlisteners.b bVar) {
    }

    public void close() {
        if (this.callbackBroadcastReceiver != null) {
            b.a(this.f6243a).a(this.callbackBroadcastReceiver);
        }
        b.a(this.f6243a).a(new Intent("com.startapp.android.CloseAdActivity"));
    }

    public String getAdId() {
        f b = a.a().b(this.adKey);
        if (b instanceof HtmlAd) {
            return ((HtmlAd) b).getAdId();
        }
        return null;
    }

    public Ad.AdState getState() {
        f b = a.a().b(this.adKey);
        if (b != null) {
            return b.getState();
        }
        return Ad.AdState.UN_INITIALIZED;
    }

    /* access modifiers changed from: protected */
    public final AdPreferences.Placement i() {
        AdPreferences.Placement i = super.i();
        return (i != null || this.adKey == null || a.a().b(this.adKey) == null) ? i : ((Ad) a.a().b(this.adKey)).i();
    }

    public boolean isBelowMinCPM() {
        f b = a.a().b(this.adKey);
        if (b != null) {
            return b.isBelowMinCPM();
        }
        return false;
    }

    public boolean isNetworkAvailable() {
        return u.c(this.f6243a);
    }

    public boolean isReady() {
        f b = a.a().b(this.adKey);
        if (b != null) {
            return b.isReady();
        }
        return false;
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences2, AdEventListener adEventListener) {
        if (!MetaData.G().M()) {
            if (adEventListener != null) {
                setErrorMessage("serving ads disabled");
                adEventListener.onFailedToReceiveAd(this);
            }
            return false;
        }
        a a2 = a.a();
        Context context = this.f6243a;
        this.adKey = a2.a(context, this, this.adMode, adPreferences2, com.startapp.sdk.adsbase.adlisteners.b.a(context, adEventListener));
        if (this.adKey != null) {
            return true;
        }
        return false;
    }

    public void loadAd() {
        loadAd(AdMode.AUTOMATIC, new AdPreferences(), (AdEventListener) null);
    }

    public CacheKey loadSplash(AdPreferences adPreferences2, AdEventListener adEventListener) {
        a a2 = a.a();
        Context context = this.f6243a;
        this.adKey = a2.a(context, this, adPreferences2, com.startapp.sdk.adsbase.adlisteners.b.a(context, adEventListener));
        return this.adKey;
    }

    public void onBackPressed() {
        showAd("exit_ad");
        k.a().m();
    }

    public void onPause() {
    }

    public void onRestoreInstanceState(Bundle bundle) {
        int i = bundle.getInt("AdMode");
        this.adMode = AdMode.AUTOMATIC;
        if (i == 1) {
            this.adMode = AdMode.FULLPAGE;
        } else if (i == 2) {
            this.adMode = AdMode.OFFERWALL;
        } else if (i == 3) {
            this.adMode = AdMode.OVERLAY;
        } else if (i == 4) {
            this.adMode = AdMode.REWARDED_VIDEO;
        } else if (i == 5) {
            this.adMode = AdMode.VIDEO;
        }
        Serializable serializable = bundle.getSerializable("AdPrefs");
        if (serializable != null) {
            this.adPreferences = (AdPreferences) serializable;
        }
    }

    public void onResume() {
        if (!isReady()) {
            loadAd();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        int i = AnonymousClass3.f6260a[this.adMode.ordinal()];
        int i2 = 4;
        if (i == 1) {
            i2 = 1;
        } else if (i == 2) {
            i2 = 2;
        } else if (i == 3) {
            i2 = 3;
        } else if (i != 4) {
            i2 = 0;
        }
        AdPreferences adPreferences2 = this.adPreferences;
        if (adPreferences2 != null) {
            bundle.putSerializable("AdPrefs", adPreferences2);
        }
        bundle.putInt("AdMode", i2);
    }

    public void setVideoListener(VideoListener videoListener2) {
        this.videoListener = videoListener2;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:65:0x015c, code lost:
        if (r10 == false) goto L_0x015f;
     */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x0196  */
    @java.lang.Deprecated
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean show(java.lang.String r9, com.startapp.sdk.adsbase.adlisteners.AdDisplayListener r10) {
        /*
            r8 = this;
            r0 = 0
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r0)
            com.startapp.sdk.adsbase.adlisteners.a r1 = new com.startapp.sdk.adsbase.adlisteners.a
            r1.<init>(r10)
            r8.callback = r1
            com.startapp.sdk.adsbase.remoteconfig.MetaData r10 = com.startapp.sdk.adsbase.remoteconfig.MetaData.G()
            boolean r10 = r10.M()
            r1 = 0
            java.lang.Integer r2 = java.lang.Integer.valueOf(r1)
            if (r10 != 0) goto L_0x0025
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r9 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.SERVING_ADS_DISABLED
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r9)
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener r9 = r8.callback
            r9.adNotDisplayed(r8)
            return r1
        L_0x0025:
            com.startapp.sdk.adsbase.cache.CacheKey r10 = r8.adKey
            if (r10 != 0) goto L_0x0030
            com.startapp.sdk.adsbase.StartAppAd$AdMode r10 = r8.adMode
            com.startapp.sdk.adsbase.model.AdPreferences r3 = r8.adPreferences
            r8.loadAd((com.startapp.sdk.adsbase.StartAppAd.AdMode) r10, (com.startapp.sdk.adsbase.model.AdPreferences) r3)
        L_0x0030:
            com.startapp.sdk.adsbase.AdsCommonMetaData r10 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            boolean r10 = r10.O()
            r3 = 1
            if (r10 == 0) goto L_0x0046
            android.content.Context r10 = r8.f6243a
            boolean r10 = com.startapp.sdk.adsbase.j.u.g(r10)
            if (r10 == 0) goto L_0x0044
            goto L_0x0046
        L_0x0044:
            r10 = 0
            goto L_0x0047
        L_0x0046:
            r10 = 1
        L_0x0047:
            if (r10 == 0) goto L_0x0170
            boolean r10 = r8.isNetworkAvailable()
            if (r10 == 0) goto L_0x0169
            boolean r10 = r8.isReady()
            if (r10 == 0) goto L_0x00ff
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r10 = r8.i()
            com.startapp.sdk.adsbase.adrules.AdRulesResult r4 = r8.a((java.lang.String) r9, (com.startapp.sdk.adsbase.model.AdPreferences.Placement) r10)
            boolean r5 = r4.a()
            if (r5 == 0) goto L_0x00f8
            com.startapp.sdk.adsbase.cache.a r5 = com.startapp.sdk.adsbase.cache.a.a()
            com.startapp.sdk.adsbase.cache.CacheKey r6 = r8.adKey
            com.startapp.sdk.adsbase.f r5 = r5.a((com.startapp.sdk.adsbase.cache.CacheKey) r6)
            r8.ad = r5
            com.startapp.sdk.adsbase.f r5 = r8.ad
            if (r5 == 0) goto L_0x0176
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r5 = r8.placement
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r6 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_SPLASH
            if (r5 != r6) goto L_0x0083
            com.startapp.sdk.adsbase.k r5 = com.startapp.sdk.adsbase.k.a()
            boolean r5 = r5.n()
            if (r5 != 0) goto L_0x0171
        L_0x0083:
            com.startapp.sdk.adsbase.f r5 = r8.ad
            boolean r5 = r5.a((java.lang.String) r9)
            if (r5 == 0) goto L_0x00df
            com.startapp.sdk.adsbase.adrules.b r6 = com.startapp.sdk.adsbase.adrules.b.a()
            com.startapp.sdk.adsbase.adrules.a r7 = new com.startapp.sdk.adsbase.adrules.a
            r7.<init>(r10, r9)
            r6.a((com.startapp.sdk.adsbase.adrules.a) r7)
            com.startapp.sdk.adsbase.StartAppAd$AdMode r10 = r8.adMode
            if (r10 == 0) goto L_0x00b1
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r10 = r8.placement
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r6 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_SPLASH
            if (r10 == r6) goto L_0x00b1
            com.startapp.sdk.adsbase.model.AdPreferences r10 = r8.adPreferences
            if (r10 == 0) goto L_0x00b2
            com.startapp.sdk.adsbase.model.AdPreferences r6 = new com.startapp.sdk.adsbase.model.AdPreferences
            r6.<init>()
            boolean r10 = r10.equals(r6)
            if (r10 == 0) goto L_0x00b1
            goto L_0x00b2
        L_0x00b1:
            r3 = 0
        L_0x00b2:
            if (r3 == 0) goto L_0x00ee
            com.startapp.sdk.adsbase.cache.a.a()
            com.startapp.sdk.adsbase.StartAppAd$AdMode r10 = r8.adMode
            java.lang.String r10 = com.startapp.sdk.adsbase.cache.a.a((com.startapp.sdk.adsbase.StartAppAd.AdMode) r10)
            android.content.Context r3 = r8.f6243a
            com.startapp.sdk.adsbase.j.b((android.content.Context) r3, (java.lang.String) r10, (java.lang.Integer) r2)
            com.startapp.sdk.adsbase.StartAppAd$AdMode r10 = r8.adMode
            com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.AUTOMATIC
            if (r10 != r3) goto L_0x00ee
            android.content.Context r10 = r8.f6243a
            com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.FULLPAGE
            java.lang.String r3 = com.startapp.sdk.adsbase.cache.a.a((com.startapp.sdk.adsbase.StartAppAd.AdMode) r3)
            com.startapp.sdk.adsbase.j.b((android.content.Context) r10, (java.lang.String) r3, (java.lang.Integer) r2)
            android.content.Context r10 = r8.f6243a
            com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.OFFERWALL
            java.lang.String r3 = com.startapp.sdk.adsbase.cache.a.a((com.startapp.sdk.adsbase.StartAppAd.AdMode) r3)
            com.startapp.sdk.adsbase.j.b((android.content.Context) r10, (java.lang.String) r3, (java.lang.Integer) r2)
            goto L_0x00ee
        L_0x00df:
            com.startapp.sdk.adsbase.f r10 = r8.ad
            boolean r2 = r10 instanceof com.startapp.sdk.adsbase.Ad
            if (r2 == 0) goto L_0x00ee
            com.startapp.sdk.adsbase.Ad r10 = (com.startapp.sdk.adsbase.Ad) r10
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = r10.getNotDisplayedReason()
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r10)
        L_0x00ee:
            com.startapp.sdk.adsbase.StartAppAd$AdMode r10 = r8.adMode
            com.startapp.sdk.adsbase.model.AdPreferences r2 = r8.adPreferences
            r8.loadAd(r10, r2, r0)
            r1 = r5
            goto L_0x0176
        L_0x00f8:
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.AD_RULES
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r10)
            goto L_0x0176
        L_0x00ff:
            com.startapp.sdk.adsbase.StartAppAd$AdMode r10 = r8.adMode
            com.startapp.sdk.adsbase.StartAppAd$AdMode r2 = com.startapp.sdk.adsbase.StartAppAd.AdMode.REWARDED_VIDEO
            if (r10 == r2) goto L_0x015f
            com.startapp.sdk.adsbase.StartAppAd$AdMode r2 = com.startapp.sdk.adsbase.StartAppAd.AdMode.VIDEO
            if (r10 == r2) goto L_0x015f
            com.startapp.sdk.adsbase.remoteconfig.MetaData r10 = com.startapp.sdk.adsbase.remoteconfig.MetaData.G()
            boolean r10 = r10.M()
            if (r10 == 0) goto L_0x015b
            com.startapp.sdk.adsbase.AdsCommonMetaData r10 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            com.startapp.sdk.adsbase.VideoConfig r10 = r10.I()
            boolean r10 = r10.h()
            if (r10 != 0) goto L_0x0122
            goto L_0x015b
        L_0x0122:
            com.startapp.sdk.adsbase.model.AdPreferences r10 = r8.adPreferences
            if (r10 != 0) goto L_0x012b
            com.startapp.sdk.adsbase.model.AdPreferences r10 = new com.startapp.sdk.adsbase.model.AdPreferences
            r10.<init>()
        L_0x012b:
            com.startapp.sdk.adsbase.Ad$AdType r2 = com.startapp.sdk.adsbase.Ad.AdType.NON_VIDEO
            r10.setType(r2)
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r2 = r8.i()
            com.startapp.sdk.adsbase.cache.a r4 = com.startapp.sdk.adsbase.cache.a.a()
            com.startapp.sdk.adsbase.cache.CacheKey r5 = new com.startapp.sdk.adsbase.cache.CacheKey
            r5.<init>(r2, r10)
            com.startapp.sdk.adsbase.f r10 = r4.b((com.startapp.sdk.adsbase.cache.CacheKey) r5)
            if (r10 == 0) goto L_0x015b
            boolean r4 = r10.isReady()
            if (r4 == 0) goto L_0x015b
            com.startapp.sdk.adsbase.adrules.AdRulesResult r2 = r8.a((java.lang.String) r9, (com.startapp.sdk.adsbase.model.AdPreferences.Placement) r2)
            boolean r2 = r2.a()
            if (r2 == 0) goto L_0x015b
            r10.a((boolean) r3)
            boolean r10 = r10.a((java.lang.String) r9)
            goto L_0x015c
        L_0x015b:
            r10 = 0
        L_0x015c:
            if (r10 == 0) goto L_0x015f
            goto L_0x0160
        L_0x015f:
            r3 = 0
        L_0x0160:
            if (r3 != 0) goto L_0x0167
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.AD_NOT_READY
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r10)
        L_0x0167:
            r4 = r0
            goto L_0x0177
        L_0x0169:
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.NETWORK_PROBLEM
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r10)
            r4 = r0
            goto L_0x0176
        L_0x0170:
            r4 = r0
        L_0x0171:
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.APP_IN_BACKGROUND
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r10)
        L_0x0176:
            r3 = 0
        L_0x0177:
            if (r1 != 0) goto L_0x017b
            if (r3 == 0) goto L_0x0194
        L_0x017b:
            java.lang.String r10 = "com.startapp.android.HideDisplayBroadcastListener"
            r8.a(r10)
            java.lang.String r10 = "com.startapp.android.ShowDisplayBroadcastListener"
            r8.a(r10)
            java.lang.String r10 = "com.startapp.android.ShowFailedDisplayBroadcastListener"
            r8.a(r10)
            java.lang.String r10 = "com.startapp.android.OnClickCallback"
            r8.a(r10)
            java.lang.String r10 = "com.startapp.android.OnVideoCompleted"
            r8.a(r10)
        L_0x0194:
            if (r1 != 0) goto L_0x0217
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = r8.getNotDisplayedReason()
            if (r10 != 0) goto L_0x01a1
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.INTERNAL_ERROR
            r8.a((com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason) r10)
        L_0x01a1:
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = r8.getNotDisplayedReason()
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r2 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.NETWORK_PROBLEM
            if (r10 == r2) goto L_0x020c
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r10 = r8.getNotDisplayedReason()
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r2 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.AD_RULES
            if (r10 == r2) goto L_0x01f3
            if (r3 == 0) goto L_0x01d2
            android.content.Context r10 = r8.f6243a
            com.startapp.sdk.adsbase.f r2 = r8.ad
            if (r2 == 0) goto L_0x01ba
            goto L_0x01c4
        L_0x01ba:
            com.startapp.sdk.adsbase.cache.a r2 = com.startapp.sdk.adsbase.cache.a.a()
            com.startapp.sdk.adsbase.cache.CacheKey r4 = r8.adKey
            com.startapp.sdk.adsbase.f r2 = r2.b((com.startapp.sdk.adsbase.cache.CacheKey) r4)
        L_0x01c4:
            java.lang.String[] r2 = com.startapp.sdk.adsbase.a.a((com.startapp.sdk.adsbase.f) r2)
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r4 = com.startapp.sdk.adsbase.adlisteners.AdDisplayListener.NotDisplayedReason.AD_NOT_READY_VIDEO_FALLBACK
            java.lang.String r4 = r4.toString()
            com.startapp.sdk.adsbase.a.a((android.content.Context) r10, (java.lang.String[]) r2, (java.lang.String) r9, (java.lang.String) r4)
            goto L_0x020c
        L_0x01d2:
            android.content.Context r10 = r8.f6243a
            com.startapp.sdk.adsbase.f r2 = r8.ad
            if (r2 == 0) goto L_0x01d9
            goto L_0x01e3
        L_0x01d9:
            com.startapp.sdk.adsbase.cache.a r2 = com.startapp.sdk.adsbase.cache.a.a()
            com.startapp.sdk.adsbase.cache.CacheKey r4 = r8.adKey
            com.startapp.sdk.adsbase.f r2 = r2.b((com.startapp.sdk.adsbase.cache.CacheKey) r4)
        L_0x01e3:
            java.lang.String[] r2 = com.startapp.sdk.adsbase.a.a((com.startapp.sdk.adsbase.f) r2)
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener$NotDisplayedReason r4 = r8.getNotDisplayedReason()
            java.lang.String r4 = r4.toString()
            com.startapp.sdk.adsbase.a.a((android.content.Context) r10, (java.lang.String[]) r2, (java.lang.String) r9, (java.lang.String) r4)
            goto L_0x020c
        L_0x01f3:
            if (r4 == 0) goto L_0x020c
            android.content.Context r10 = r8.f6243a
            com.startapp.sdk.adsbase.cache.a r2 = com.startapp.sdk.adsbase.cache.a.a()
            com.startapp.sdk.adsbase.cache.CacheKey r5 = r8.adKey
            com.startapp.sdk.adsbase.f r2 = r2.b((com.startapp.sdk.adsbase.cache.CacheKey) r5)
            java.lang.String[] r2 = com.startapp.sdk.adsbase.a.a((com.startapp.sdk.adsbase.f) r2)
            java.lang.String r4 = r4.b()
            com.startapp.sdk.adsbase.a.a((android.content.Context) r10, (java.lang.String[]) r2, (java.lang.String) r9, (java.lang.String) r4)
        L_0x020c:
            r8.ad = r0
            if (r3 != 0) goto L_0x0217
            com.startapp.sdk.adsbase.adlisteners.AdDisplayListener r9 = r8.callback
            if (r9 == 0) goto L_0x0217
            r9.adNotDisplayed(r8)
        L_0x0217:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.StartAppAd.show(java.lang.String, com.startapp.sdk.adsbase.adlisteners.AdDisplayListener):boolean");
    }

    public boolean showAd() {
        return showAd((String) null, (AdDisplayListener) null);
    }

    private void a(String str) {
        b.a(this.f6243a).a(this.callbackBroadcastReceiver, new IntentFilter(str));
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig) {
        showSplash(activity, bundle, splashConfig, new AdPreferences());
    }

    public void loadAd(AdPreferences adPreferences2) {
        loadAd(AdMode.AUTOMATIC, adPreferences2, (AdEventListener) null);
    }

    public boolean showAd(String str) {
        return showAd(str, (AdDisplayListener) null);
    }

    static void a(final Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2, final SplashHideListener splashHideListener, boolean z) {
        if (bundle == null && MetaData.G().M() && u.g(activity)) {
            try {
                k a2 = k.a();
                if (!a2.k() && z) {
                    a2.c(true);
                }
                a2.b(z);
                if (!z) {
                    if (adPreferences2 == null) {
                        adPreferences2 = new AdPreferences();
                    }
                    adPreferences2.setAs(Boolean.TRUE);
                }
                splashConfig.setDefaults(activity);
                u.a(activity, true);
                Intent intent = new Intent(activity, u.a((Context) activity, (Class<? extends Activity>) OverlayActivity.class, (Class<? extends Activity>) AppWallActivity.class));
                intent.putExtra("SplashConfig", splashConfig);
                intent.putExtra("AdPreference", adPreferences2);
                int i = 0;
                intent.putExtra("testMode", false);
                intent.putExtra("fullscreen", a.a(activity));
                intent.putExtra("placement", AdPreferences.Placement.INAPP_SPLASH.a());
                if (Build.VERSION.SDK_INT >= 11) {
                    i = 32768;
                }
                intent.addFlags(67108864 | i | 1073741824);
                activity.startActivity(intent);
                b.a((Context) activity).a(new BroadcastReceiver() {
                    public final void onReceive(Context context, Intent intent) {
                        u.a(activity, false);
                        SplashHideListener splashHideListener = splashHideListener;
                        if (splashHideListener != null) {
                            splashHideListener.splashHidden();
                        }
                        b.a((Context) activity).a((BroadcastReceiver) this);
                    }
                }, new IntentFilter("com.startapp.android.splashHidden"));
            } catch (Throwable th) {
                new com.startapp.sdk.adsbase.infoevents.e(th).a((Context) activity);
                if (splashHideListener != null) {
                    splashHideListener.splashHidden();
                }
            }
        }
    }

    public static void onBackPressed(Context context) {
        new StartAppAd(context).onBackPressed();
    }

    public static void showSplash(Activity activity, Bundle bundle, AdPreferences adPreferences2) {
        showSplash(activity, bundle, new SplashConfig(), adPreferences2);
    }

    public void loadAd(AdEventListener adEventListener) {
        loadAd(AdMode.AUTOMATIC, new AdPreferences(), adEventListener);
    }

    public boolean showAd(AdDisplayListener adDisplayListener) {
        return showAd((String) null, adDisplayListener);
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2) {
        showSplash(activity, bundle, splashConfig, adPreferences2, (SplashHideListener) null);
    }

    public void loadAd(AdPreferences adPreferences2, AdEventListener adEventListener) {
        loadAd(AdMode.AUTOMATIC, adPreferences2, adEventListener);
    }

    public boolean showAd(String str, AdDisplayListener adDisplayListener) {
        try {
            return show(str, adDisplayListener);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6243a);
            a(AdDisplayListener.NotDisplayedReason.INTERNAL_ERROR);
            if (adDisplayListener == null) {
                return false;
            }
            adDisplayListener.adNotDisplayed((Ad) null);
            return false;
        }
    }

    public static void showSplash(Activity activity, Bundle bundle, SplashConfig splashConfig, AdPreferences adPreferences2, SplashHideListener splashHideListener) {
        a(activity, bundle, splashConfig, adPreferences2, splashHideListener, true);
    }

    public void loadAd(AdMode adMode2) {
        loadAd(adMode2, new AdPreferences(), (AdEventListener) null);
    }

    public void loadAd(AdMode adMode2, AdPreferences adPreferences2) {
        loadAd(adMode2, adPreferences2, (AdEventListener) null);
    }

    public void loadAd(AdMode adMode2, AdEventListener adEventListener) {
        loadAd(adMode2, new AdPreferences(), adEventListener);
    }

    public static boolean showAd(Context context) {
        if (context == null) {
            return false;
        }
        try {
            return new StartAppAd(context).showAd();
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
            return false;
        }
    }

    public void loadAd(AdMode adMode2, AdPreferences adPreferences2, AdEventListener adEventListener) {
        this.adMode = adMode2;
        this.adPreferences = adPreferences2;
        try {
            load(adPreferences2, adEventListener);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6243a);
            if (adEventListener != null) {
                adEventListener.onFailedToReceiveAd(this);
            }
        }
    }

    @Deprecated
    public boolean show() {
        return show((String) null, (AdDisplayListener) null);
    }
}
