package com.startapp.sdk.adsbase.i;

import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Map<C0077a, String> f6368a = new ConcurrentHashMap();

    /* renamed from: com.startapp.sdk.adsbase.i.a$a  reason: collision with other inner class name */
    static class C0077a {

        /* renamed from: a  reason: collision with root package name */
        private AdPreferences.Placement f6369a;
        private int b;

        C0077a(AdPreferences.Placement placement) {
            this(placement, -1);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj != null && C0077a.class == obj.getClass()) {
                C0077a aVar = (C0077a) obj;
                return this.b == aVar.b && this.f6369a == aVar.f6369a;
            }
        }

        public final int hashCode() {
            return u.a(this.f6369a, Integer.valueOf(this.b));
        }

        C0077a(AdPreferences.Placement placement, int i) {
            this.f6369a = placement;
            this.b = i;
        }
    }

    public final String a(AdPreferences.Placement placement) {
        if (placement == null) {
            return null;
        }
        return this.f6368a.get(new C0077a(placement));
    }

    public final String a(AdPreferences.Placement placement, int i) {
        if (placement == null) {
            return null;
        }
        return this.f6368a.get(new C0077a(placement, i));
    }

    public final void a(AdPreferences.Placement placement, String str) {
        if (str != null) {
            this.f6368a.put(new C0077a(placement), str);
        }
    }

    public final void a(AdPreferences.Placement placement, int i, String str) {
        if (str != null) {
            this.f6368a.put(new C0077a(placement, i), str);
        }
    }
}
