package com.startapp.sdk.adsbase.cache;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.b.d;
import com.startapp.sdk.ads.list3d.g;
import com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.HtmlAd;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.io.File;
import java.io.Serializable;
import java.util.List;

public final class DiskAdCacheManager {

    /* renamed from: a  reason: collision with root package name */
    public int f6309a;
    public int b;
    public int c;
    public float d;
    public float e;

    protected static class DiskCacheKey implements Serializable {
        private static final long serialVersionUID = 1;
        protected AdPreferences adPreferences;
        private int numberOfLoadedAd;
        protected AdPreferences.Placement placement;

        protected DiskCacheKey(AdPreferences.Placement placement2, AdPreferences adPreferences2) {
            this.placement = placement2;
            this.adPreferences = adPreferences2;
        }

        /* access modifiers changed from: protected */
        public final AdPreferences.Placement a() {
            return this.placement;
        }

        /* access modifiers changed from: protected */
        public final AdPreferences b() {
            return this.adPreferences;
        }

        /* access modifiers changed from: protected */
        public final int c() {
            return this.numberOfLoadedAd;
        }

        /* access modifiers changed from: protected */
        public final void a(int i) {
            this.numberOfLoadedAd = i;
        }
    }

    protected static class DiskCachedAd implements Serializable {
        private static final long serialVersionUID = 1;
        private f ad;
        private String html;

        protected DiskCachedAd(f fVar) {
            this.ad = fVar;
            f fVar2 = this.ad;
            if (fVar2 != null && (fVar2 instanceof HtmlAd)) {
                this.html = ((HtmlAd) fVar2).j();
            }
        }

        /* access modifiers changed from: protected */
        public final f a() {
            return this.ad;
        }

        /* access modifiers changed from: protected */
        public final String b() {
            return this.html;
        }
    }

    public interface a {
        void a(f fVar);
    }

    public interface b {
        void a(List<DiskCacheKey> list);
    }

    public interface c {
        void a();
    }

    protected static void a(Context context, AdPreferences.Placement placement, AdPreferences adPreferences, String str, int i) {
        DiskCacheKey diskCacheKey = new DiskCacheKey(placement, adPreferences);
        diskCacheKey.a(i);
        d.a(context, a(), str, diskCacheKey);
    }

    protected static String b() {
        return "startapp_ads".concat(File.separator).concat("interstitials");
    }

    protected static void a(Context context, e eVar, String str) {
        d.a(context, b(), str, new DiskCachedAd(eVar.b()));
    }

    protected static void a(final Context context, final String str, final a aVar, final com.startapp.sdk.adsbase.adlisteners.b bVar) {
        ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                try {
                    Class<DiskCachedAd> cls = DiskCachedAd.class;
                    final DiskCachedAd diskCachedAd = (DiskCachedAd) d.a(context, DiskAdCacheManager.b(), str);
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public final void run() {
                            try {
                                if (diskCachedAd == null) {
                                    bVar.b((Ad) null);
                                    return;
                                }
                                if (diskCachedAd.a() != null) {
                                    if (diskCachedAd.a().isReady()) {
                                        if (diskCachedAd.a().e_()) {
                                            bVar.b((Ad) null);
                                            return;
                                        } else {
                                            DiskAdCacheManager.a(context, diskCachedAd, aVar, bVar);
                                            return;
                                        }
                                    }
                                }
                                bVar.b((Ad) null);
                            } catch (Throwable th) {
                                new e(th).a(context);
                                bVar.b((Ad) null);
                            }
                        }
                    });
                } catch (Throwable th) {
                    new e(th).a(context);
                    bVar.b((Ad) null);
                }
            }
        });
    }

    protected static String a() {
        return "startapp_ads".concat(File.separator).concat("keys");
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x004e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    protected static void a(android.content.Context r6, com.startapp.sdk.adsbase.cache.DiskAdCacheManager.DiskCachedAd r7, com.startapp.sdk.adsbase.cache.DiskAdCacheManager.a r8, final com.startapp.sdk.adsbase.adlisteners.b r9) {
        /*
            com.startapp.sdk.adsbase.f r0 = r7.a()
            r0.setContext(r6)
            com.startapp.sdk.adsbase.j.u.b()
            boolean r1 = r0 instanceof com.startapp.sdk.ads.interstitials.InterstitialAd
            r2 = 0
            r3 = 0
            if (r1 == 0) goto L_0x0069
            com.startapp.sdk.ads.interstitials.InterstitialAd r0 = (com.startapp.sdk.ads.interstitials.InterstitialAd) r0
            java.lang.String r7 = r7.b()
            if (r7 == 0) goto L_0x0065
            java.lang.String r1 = ""
            boolean r1 = r7.equals(r1)
            if (r1 != 0) goto L_0x0065
            com.startapp.sdk.adsbase.AdsCommonMetaData r1 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            boolean r1 = r1.F()
            if (r1 == 0) goto L_0x004b
            java.util.List r1 = com.iab.omid.library.startapp.b.a((java.lang.String) r7, (int) r3)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            java.util.HashSet r5 = new java.util.HashSet
            r5.<init>()
            java.lang.Boolean r1 = com.iab.omid.library.startapp.b.a((android.content.Context) r6, (java.util.List<com.startapp.sdk.adsbase.apppresence.AppPresenceDetails>) r1, (int) r3, (java.util.Set<java.lang.String>) r5, (java.util.List<com.startapp.sdk.adsbase.apppresence.AppPresenceDetails>) r4)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x004b
            com.startapp.sdk.adsbase.apppresence.a r1 = new com.startapp.sdk.adsbase.apppresence.a
            r1.<init>(r6, r4)
            r1.a()
            goto L_0x004c
        L_0x004b:
            r3 = 1
        L_0x004c:
            if (r3 == 0) goto L_0x0065
            com.startapp.sdk.adsbase.cache.a r1 = com.startapp.sdk.adsbase.cache.a.a()
            java.lang.String r2 = r0.k()
            r1.a((java.lang.String) r7, (java.lang.String) r2)
            r8.a(r0)
            com.startapp.sdk.adsbase.cache.DiskAdCacheManager$4 r8 = new com.startapp.sdk.adsbase.cache.DiskAdCacheManager$4
            r8.<init>(r9, r0)
            com.startapp.sdk.adsbase.j.u.a((android.content.Context) r6, (java.lang.String) r7, (com.startapp.sdk.adsbase.j.u.a) r8)
            return
        L_0x0065:
            r9.b(r2)
            return
        L_0x0069:
            com.startapp.sdk.adsbase.j.u.b()
            boolean r7 = r0 instanceof com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd
            if (r7 == 0) goto L_0x009e
            com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd r0 = (com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd) r0
            java.util.List r7 = r0.g()
            if (r7 == 0) goto L_0x009a
            com.startapp.sdk.adsbase.AdsCommonMetaData r1 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            boolean r1 = r1.F()
            if (r1 == 0) goto L_0x008b
            java.util.HashSet r1 = new java.util.HashSet
            r1.<init>()
            java.util.List r7 = com.iab.omid.library.startapp.b.a((android.content.Context) r6, (java.util.List<com.startapp.sdk.adsbase.model.AdDetails>) r7, (int) r3, (java.util.Set<java.lang.String>) r1)
        L_0x008b:
            if (r7 == 0) goto L_0x009a
            int r6 = r7.size()
            if (r6 <= 0) goto L_0x009a
            r8.a(r0)
            a((com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd) r0, (com.startapp.sdk.adsbase.adlisteners.b) r9, (java.util.List<com.startapp.sdk.adsbase.model.AdDetails>) r7)
            return
        L_0x009a:
            r9.b(r2)
            return
        L_0x009e:
            r9.b(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.cache.DiskAdCacheManager.a(android.content.Context, com.startapp.sdk.adsbase.cache.DiskAdCacheManager$DiskCachedAd, com.startapp.sdk.adsbase.cache.DiskAdCacheManager$a, com.startapp.sdk.adsbase.adlisteners.b):void");
    }

    private static void a(OfferWall3DAd offerWall3DAd, com.startapp.sdk.adsbase.adlisteners.b bVar, List<AdDetails> list) {
        com.startapp.sdk.ads.list3d.f a2 = g.a().a(offerWall3DAd.a());
        a2.a();
        for (AdDetails a3 : list) {
            a2.a(a3);
        }
        bVar.a(offerWall3DAd);
    }
}
