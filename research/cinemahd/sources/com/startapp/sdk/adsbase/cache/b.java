package com.startapp.sdk.adsbase.cache;

import com.startapp.sdk.adsbase.k;
import java.util.concurrent.TimeUnit;

public final class b extends c {
    private final FailuresHandler b = CacheMetaData.a().b().f();
    private int c = 0;
    private boolean d = false;

    public b(e eVar) {
        super(eVar);
    }

    public final void a() {
        super.a();
        this.c = 0;
        this.d = false;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        if (this.c == this.b.a().size() - 1) {
            this.d = true;
        } else {
            this.c++;
        }
        super.b();
    }

    /* access modifiers changed from: protected */
    public final boolean c() {
        if (!k.a().l()) {
            return false;
        }
        FailuresHandler failuresHandler = this.b;
        if (!((failuresHandler == null || failuresHandler.a() == null) ? false : true)) {
            return false;
        }
        if (this.d) {
            return this.b.b();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public final long d() {
        Long h;
        if (this.c >= this.b.a().size() || (h = h()) == null) {
            return -1;
        }
        long millis = TimeUnit.SECONDS.toMillis((long) this.b.a().get(this.c).intValue()) - (System.currentTimeMillis() - h.longValue());
        if (millis >= 0) {
            return millis;
        }
        return 0;
    }
}
