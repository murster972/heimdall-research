package com.startapp.sdk.adsbase.cache;

import android.app.Activity;
import android.content.Context;
import android.os.SystemClock;
import com.startapp.sdk.ads.interstitials.OverlayAd;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.ads.offerWall.offerWallHtml.OfferWallAd;
import com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.ads.video.VideoEnabledAd;
import com.startapp.sdk.adsbase.ActivityExtra;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.cache.DiskAdCacheManager;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class e {

    /* renamed from: a  reason: collision with root package name */
    protected f f6327a;
    protected AtomicBoolean b;
    protected long c;
    protected d d;
    protected b e;
    protected final Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>> f;
    private final AdPreferences.Placement g;
    private Context h;
    private ActivityExtra i;
    private AdPreferences j;
    private String k;
    private boolean l;
    private int m;
    private boolean n;
    private Long o;
    private b p;

    /* renamed from: com.startapp.sdk.adsbase.cache.e$3  reason: invalid class name */
    static /* synthetic */ class AnonymousClass3 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6330a = new int[AdPreferences.Placement.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.sdk.adsbase.model.AdPreferences$Placement[] r0 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6330a = r0
                int[] r0 = f6330a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_FULL_SCREEN     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6330a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OVERLAY     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6330a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OFFER_WALL     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6330a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_RETURN     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6330a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_SPLASH     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.cache.e.AnonymousClass3.<clinit>():void");
        }
    }

    class a implements AdEventListener {

        /* renamed from: a  reason: collision with root package name */
        private boolean f6331a = false;
        private boolean b = false;

        a() {
        }

        public final void onFailedToReceiveAd(Ad ad) {
            List<StartAppAd> a2;
            ConcurrentHashMap concurrentHashMap;
            ConcurrentHashMap concurrentHashMap2 = null;
            if (!this.b) {
                synchronized (e.this.f) {
                    concurrentHashMap = new ConcurrentHashMap(e.this.f);
                    e.this.f6327a = null;
                    e.this.f.clear();
                }
                concurrentHashMap2 = concurrentHashMap;
            }
            if (concurrentHashMap2 != null) {
                for (com.startapp.sdk.adsbase.adlisteners.b bVar : concurrentHashMap2.keySet()) {
                    if (!(bVar == null || (a2 = e.this.a((Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>>) concurrentHashMap2, bVar)) == null)) {
                        for (StartAppAd next : a2) {
                            next.setErrorMessage(ad.getErrorMessage());
                            bVar.b(next);
                        }
                    }
                }
            }
            this.b = true;
            e.this.e.e();
            e.this.d.a();
            e.this.b.set(false);
        }

        public final void onReceiveAd(Ad ad) {
            List<StartAppAd> a2;
            f fVar = e.this.f6327a;
            boolean z = fVar != null && fVar.e();
            if (!this.f6331a && !z) {
                this.f6331a = true;
                synchronized (e.this.f) {
                    for (com.startapp.sdk.adsbase.adlisteners.b next : e.this.f.keySet()) {
                        if (!(next == null || (a2 = e.this.a(e.this.f, next)) == null)) {
                            for (StartAppAd next2 : a2) {
                                next2.setErrorMessage(ad.getErrorMessage());
                                next.a(next2);
                            }
                        }
                    }
                    e.this.f.clear();
                }
            }
            e.this.d.e();
            e.this.e.a();
            e.this.b.set(false);
        }
    }

    public interface b {
        void a(e eVar);
    }

    public e(Context context, AdPreferences.Placement placement, AdPreferences adPreferences) {
        this.f6327a = null;
        this.b = new AtomicBoolean(false);
        this.k = null;
        this.l = false;
        this.d = null;
        this.e = null;
        this.f = new ConcurrentHashMap();
        this.n = true;
        this.g = placement;
        this.j = adPreferences;
        if (context instanceof Activity) {
            this.h = context.getApplicationContext();
            this.i = new ActivityExtra((Activity) context);
        } else {
            this.h = context;
            this.i = null;
        }
        this.d = new d(this);
        this.e = new b(this);
    }

    private boolean k() {
        f fVar = this.f6327a;
        return fVar != null && fVar.isReady();
    }

    private f l() {
        int i2 = AnonymousClass3.f6330a[this.g.ordinal()];
        if (i2 == 1) {
            return new OverlayAd(this.h);
        }
        if (i2 == 2) {
            u.b();
            return new VideoEnabledAd(this.h);
        } else if (i2 == 3) {
            boolean z = new Random().nextInt(100) < AdsCommonMetaData.a().d();
            boolean isForceOfferWall3D = this.j.isForceOfferWall3D();
            boolean isForceOfferWall2D = true ^ this.j.isForceOfferWall2D();
            u.b();
            u.b();
            u.b();
            if ((z || isForceOfferWall3D) && isForceOfferWall2D) {
                return new OfferWall3DAd(this.h);
            }
            return new OfferWallAd(this.h);
        } else if (i2 == 4) {
            return new ReturnAd(this.h);
        } else {
            if (i2 != 5) {
                return new OverlayAd(this.h);
            }
            return new SplashAd(this.h);
        }
    }

    private boolean m() {
        return this.l && this.k != null;
    }

    private boolean n() {
        Long h2 = AdsCommonMetaData.a().h();
        if (h2 == null || this.o == null || SystemClock.elapsedRealtime() - this.o.longValue() >= h2.longValue()) {
            this.o = Long.valueOf(SystemClock.elapsedRealtime());
            return false;
        }
        new a().onFailedToReceiveAd(new CachedAd$3(this, this.h, this.g));
        Context context = this.h;
        u.a(context, true, "Failed to load " + this.g.name() + " ad: NO FILL");
        return true;
    }

    private boolean o() {
        f fVar = this.f6327a;
        if (fVar == null) {
            return false;
        }
        return fVar.e_();
    }

    public final f b() {
        return this.f6327a;
    }

    /* access modifiers changed from: protected */
    public final AdPreferences.Placement c() {
        return this.g;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        this.l = true;
    }

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:20:0x005f  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void e() {
        /*
            r6 = this;
            boolean r0 = r6.k()
            if (r0 == 0) goto L_0x0064
            android.content.Context r0 = r6.h
            com.startapp.sdk.adsbase.f r1 = r6.f6327a
            com.startapp.sdk.adsbase.Ad r1 = (com.startapp.sdk.adsbase.Ad) r1
            r2 = 1
            r3 = 0
            if (r1 == 0) goto L_0x0047
            java.util.HashSet r4 = new java.util.HashSet
            r4.<init>()
            boolean r5 = r1 instanceof com.startapp.sdk.adsbase.HtmlAd
            if (r5 == 0) goto L_0x0031
            com.startapp.sdk.adsbase.HtmlAd r1 = (com.startapp.sdk.adsbase.HtmlAd) r1
            java.lang.String r1 = r1.j()
            java.util.List r1 = com.iab.omid.library.startapp.b.a((java.lang.String) r1, (int) r3)
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.lang.Boolean r0 = com.iab.omid.library.startapp.b.a((android.content.Context) r0, (java.util.List<com.startapp.sdk.adsbase.apppresence.AppPresenceDetails>) r1, (int) r3, (java.util.Set<java.lang.String>) r4, (java.util.List<com.startapp.sdk.adsbase.apppresence.AppPresenceDetails>) r5)
            boolean r0 = r0.booleanValue()
            goto L_0x0048
        L_0x0031:
            boolean r5 = r1 instanceof com.startapp.sdk.adsbase.JsonAd
            if (r5 == 0) goto L_0x0047
            com.startapp.sdk.adsbase.JsonAd r1 = (com.startapp.sdk.adsbase.JsonAd) r1
            java.util.List r1 = r1.g()
            java.util.List r0 = com.iab.omid.library.startapp.b.a((android.content.Context) r0, (java.util.List<com.startapp.sdk.adsbase.model.AdDetails>) r1, (int) r3, (java.util.Set<java.lang.String>) r4, (boolean) r3)
            int r0 = r0.size()
            if (r0 != 0) goto L_0x0047
            r0 = 1
            goto L_0x0048
        L_0x0047:
            r0 = 0
        L_0x0048:
            if (r0 != 0) goto L_0x005f
            boolean r0 = r6.o()
            if (r0 == 0) goto L_0x0051
            goto L_0x005f
        L_0x0051:
            java.util.concurrent.atomic.AtomicBoolean r0 = r6.b
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0071
            com.startapp.sdk.adsbase.cache.d r0 = r6.d
            r0.e()
            return
        L_0x005f:
            r0 = 0
            r6.a(r0, r0, r2, r3)
            return
        L_0x0064:
            java.util.concurrent.atomic.AtomicBoolean r0 = r6.b
            boolean r0 = r0.get()
            if (r0 != 0) goto L_0x0071
            com.startapp.sdk.adsbase.cache.b r0 = r6.e
            r0.e()
        L_0x0071:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.cache.e.e():void");
    }

    /* access modifiers changed from: package-private */
    public final void f() {
        this.e.g();
    }

    /* access modifiers changed from: package-private */
    public final void g() {
        this.d.g();
    }

    public final f h() {
        if (!k()) {
            return null;
        }
        f fVar = this.f6327a;
        this.m = 0;
        this.o = null;
        if (!AdsConstants.b.booleanValue() && this.n) {
            a((StartAppAd) null, (com.startapp.sdk.adsbase.adlisteners.b) null, true, true);
            return fVar;
        } else if (this.n) {
            return fVar;
        } else {
            b bVar = this.p;
            if (bVar != null) {
                bVar.a(this);
            }
            d dVar = this.d;
            if (dVar == null) {
                return fVar;
            }
            dVar.a();
            return fVar;
        }
    }

    public final boolean i() {
        if (this.m < MetaData.G().N()) {
            this.m++;
            a((StartAppAd) null, (com.startapp.sdk.adsbase.adlisteners.b) null, true, false);
            return true;
        }
        b bVar = this.p;
        if (bVar != null) {
            bVar.a(this);
        }
        return false;
    }

    public final int j() {
        return this.m;
    }

    private void b(final boolean z) {
        final a aVar = new a();
        DiskAdCacheManager.a(this.h, this.k, (DiskAdCacheManager.a) new DiskAdCacheManager.a() {
            public final void a(f fVar) {
                e.this.f6327a = fVar;
            }
        }, (com.startapp.sdk.adsbase.adlisteners.b) new com.startapp.sdk.adsbase.adlisteners.b() {
            public final void a(Ad ad) {
                aVar.onReceiveAd(ad);
            }

            public final void b(Ad ad) {
                e eVar = e.this;
                eVar.f6327a = null;
                eVar.c(z);
            }
        });
    }

    /* access modifiers changed from: private */
    public void c(boolean z) {
        if (!z || !n()) {
            this.f6327a = l();
            this.f6327a.setActivityExtra(this.i);
            this.j.setAutoLoadAmount(this.m);
            this.f6327a.load(this.j, new a());
            this.c = System.currentTimeMillis();
        }
    }

    public final AdPreferences a() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final void a(AdPreferences adPreferences) {
        this.j = adPreferences;
    }

    /* access modifiers changed from: protected */
    public final void a(String str) {
        this.k = str;
    }

    /* access modifiers changed from: protected */
    public final List<StartAppAd> a(Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>> map, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        try {
            return map.get(bVar);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.h);
            return null;
        }
    }

    private void a(Map<com.startapp.sdk.adsbase.adlisteners.b, List<StartAppAd>> map, com.startapp.sdk.adsbase.adlisteners.b bVar, List<StartAppAd> list) {
        try {
            map.put(bVar, list);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.h);
        }
    }

    public final void a(StartAppAd startAppAd, com.startapp.sdk.adsbase.adlisteners.b bVar) {
        a(startAppAd, bVar, false, true);
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0019 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:21:0x0048 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.startapp.sdk.adsbase.StartAppAd r5, com.startapp.sdk.adsbase.adlisteners.b r6, boolean r7, boolean r8) {
        /*
            r4 = this;
            java.util.Map<com.startapp.sdk.adsbase.adlisteners.b, java.util.List<com.startapp.sdk.adsbase.StartAppAd>> r0 = r4.f
            monitor-enter(r0)
            boolean r1 = r4.k()     // Catch:{ all -> 0x0051 }
            r2 = 0
            r3 = 1
            if (r1 == 0) goto L_0x0016
            boolean r1 = r4.o()     // Catch:{ all -> 0x0051 }
            if (r1 != 0) goto L_0x0016
            if (r7 == 0) goto L_0x0014
            goto L_0x0016
        L_0x0014:
            r7 = 0
            goto L_0x0017
        L_0x0016:
            r7 = 1
        L_0x0017:
            if (r7 == 0) goto L_0x0048
            if (r5 == 0) goto L_0x0032
            if (r6 == 0) goto L_0x0032
            java.util.Map<com.startapp.sdk.adsbase.adlisteners.b, java.util.List<com.startapp.sdk.adsbase.StartAppAd>> r7 = r4.f     // Catch:{ all -> 0x0051 }
            java.util.List r7 = r4.a((java.util.Map<com.startapp.sdk.adsbase.adlisteners.b, java.util.List<com.startapp.sdk.adsbase.StartAppAd>>) r7, (com.startapp.sdk.adsbase.adlisteners.b) r6)     // Catch:{ all -> 0x0051 }
            if (r7 != 0) goto L_0x002f
            java.util.ArrayList r7 = new java.util.ArrayList     // Catch:{ all -> 0x0051 }
            r7.<init>()     // Catch:{ all -> 0x0051 }
            java.util.Map<com.startapp.sdk.adsbase.adlisteners.b, java.util.List<com.startapp.sdk.adsbase.StartAppAd>> r1 = r4.f     // Catch:{ all -> 0x0051 }
            r4.a(r1, r6, r7)     // Catch:{ all -> 0x0051 }
        L_0x002f:
            r7.add(r5)     // Catch:{ all -> 0x0051 }
        L_0x0032:
            java.util.concurrent.atomic.AtomicBoolean r5 = r4.b     // Catch:{ all -> 0x0051 }
            boolean r5 = r5.compareAndSet(r2, r3)     // Catch:{ all -> 0x0051 }
            if (r5 == 0) goto L_0x004f
            com.startapp.sdk.adsbase.cache.d r5 = r4.d     // Catch:{ all -> 0x0051 }
            r5.f()     // Catch:{ all -> 0x0051 }
            com.startapp.sdk.adsbase.cache.b r5 = r4.e     // Catch:{ all -> 0x0051 }
            r5.f()     // Catch:{ all -> 0x0051 }
            r4.a((boolean) r8)     // Catch:{ all -> 0x0051 }
            goto L_0x004f
        L_0x0048:
            if (r5 == 0) goto L_0x004f
            if (r6 == 0) goto L_0x004f
            r6.a(r5)     // Catch:{ all -> 0x0051 }
        L_0x004f:
            monitor-exit(r0)     // Catch:{ all -> 0x0051 }
            return
        L_0x0051:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0051 }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.cache.e.a(com.startapp.sdk.adsbase.StartAppAd, com.startapp.sdk.adsbase.adlisteners.b, boolean, boolean):void");
    }

    public e(Context context, AdPreferences.Placement placement, AdPreferences adPreferences, byte b2) {
        this(context, placement, adPreferences);
        this.n = false;
    }

    private void a(boolean z) {
        f fVar = this.f6327a;
        if (fVar != null) {
            fVar.a(false);
        }
        if (m()) {
            this.l = false;
            b(z);
            return;
        }
        c(z);
    }

    public final void a(b bVar) {
        this.p = bVar;
    }

    public final void a(int i2) {
        this.m = i2;
    }
}
