package com.startapp.sdk.adsbase.cache;

import java.io.Serializable;

public class CachedVideoAd implements Serializable {
    private static final long serialVersionUID = 1;
    private String filename;
    private long lastUseTimestamp;
    private String videoLocation;

    public CachedVideoAd(String str) {
        this.filename = str;
    }

    public final String a() {
        return this.videoLocation;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || CachedVideoAd.class != obj.getClass()) {
            return false;
        }
        CachedVideoAd cachedVideoAd = (CachedVideoAd) obj;
        String str = this.filename;
        if (str == null) {
            if (cachedVideoAd.filename != null) {
                return false;
            }
        } else if (!str.equals(cachedVideoAd.filename)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int i;
        String str = this.filename;
        if (str == null) {
            i = 0;
        } else {
            i = str.hashCode();
        }
        return i + 31;
    }

    public final void a(String str) {
        this.videoLocation = str;
    }

    public final void a(long j) {
        this.lastUseTimestamp = j;
    }
}
