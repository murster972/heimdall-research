package com.startapp.sdk.adsbase.cache;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class CacheMetaData implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static volatile CacheMetaData f6308a = new CacheMetaData();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private ACMConfig ACM = new ACMConfig();
    private String cacheMetaDataUpdateVersion = "4.6.3";
    private float sendCacheSizeProb = 20.0f;

    public static CacheMetaData a() {
        return f6308a;
    }

    public final ACMConfig b() {
        return this.ACM;
    }

    public final float c() {
        return this.sendCacheSizeProb;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && CacheMetaData.class == obj.getClass()) {
            CacheMetaData cacheMetaData = (CacheMetaData) obj;
            return Float.compare(cacheMetaData.sendCacheSizeProb, this.sendCacheSizeProb) == 0 && u.b(this.ACM, cacheMetaData.ACM) && u.b(this.cacheMetaDataUpdateVersion, cacheMetaData.cacheMetaDataUpdateVersion);
        }
    }

    public int hashCode() {
        return u.a(this.ACM, Float.valueOf(this.sendCacheSizeProb), this.cacheMetaDataUpdateVersion);
    }

    public static void a(Context context, CacheMetaData cacheMetaData) {
        cacheMetaData.cacheMetaDataUpdateVersion = "4.6.3";
        f6308a = cacheMetaData;
        com.startapp.common.b.d.a(context, "StartappCacheMetadata", (Serializable) cacheMetaData);
    }

    public static void a(Context context) {
        CacheMetaData cacheMetaData = (CacheMetaData) com.startapp.common.b.d.a(context, "StartappCacheMetadata");
        CacheMetaData cacheMetaData2 = new CacheMetaData();
        if (cacheMetaData != null) {
            boolean a2 = u.a(cacheMetaData, cacheMetaData2);
            if (!(!"4.6.3".equals(cacheMetaData.cacheMetaDataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).f("metadata_null").a(context);
            }
            f6308a = cacheMetaData;
            return;
        }
        f6308a = cacheMetaData2;
    }
}
