package com.startapp.sdk.adsbase.cache;

import android.content.Context;
import com.facebook.common.time.Clock;
import com.startapp.common.ThreadManager;
import com.startapp.common.b.d;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.cache.DiskAdCacheManager;
import com.startapp.sdk.adsbase.cache.e;
import com.startapp.sdk.adsbase.f;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;
import java.util.WeakHashMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

public class a {
    private static a d = new a();

    /* renamed from: a  reason: collision with root package name */
    final Map<CacheKey, e> f6317a = new ConcurrentHashMap();
    protected boolean b = false;
    protected Context c;
    private Map<String, String> e = new WeakHashMap();
    private boolean f = false;
    private Queue<C0074a> g = new ConcurrentLinkedQueue();
    private e.b h;

    /* renamed from: com.startapp.sdk.adsbase.cache.a$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6323a = new int[AdPreferences.Placement.values().length];
        static final /* synthetic */ int[] b = new int[StartAppAd.AdMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(16:0|(2:1|2)|3|5|6|7|8|9|10|11|12|(2:13|14)|15|17|18|(3:19|20|22)) */
        /* JADX WARNING: Can't wrap try/catch for region: R(18:0|(2:1|2)|3|5|6|7|8|9|10|11|12|(2:13|14)|15|17|18|19|20|22) */
        /* JADX WARNING: Can't wrap try/catch for region: R(19:0|1|2|3|5|6|7|8|9|10|11|12|(2:13|14)|15|17|18|19|20|22) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0035 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0040 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x005e */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x002a */
        static {
            /*
                com.startapp.sdk.adsbase.StartAppAd$AdMode[] r0 = com.startapp.sdk.adsbase.StartAppAd.AdMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r2 = com.startapp.sdk.adsbase.StartAppAd.AdMode.OFFERWALL     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.OVERLAY     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.FULLPAGE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r4 = 3
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.VIDEO     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r4 = 4
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.REWARDED_VIDEO     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r4 = 5
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x004b }
                com.startapp.sdk.adsbase.StartAppAd$AdMode r3 = com.startapp.sdk.adsbase.StartAppAd.AdMode.AUTOMATIC     // Catch:{ NoSuchFieldError -> 0x004b }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x004b }
                r4 = 6
                r2[r3] = r4     // Catch:{ NoSuchFieldError -> 0x004b }
            L_0x004b:
                com.startapp.sdk.adsbase.model.AdPreferences$Placement[] r2 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                f6323a = r2
                int[] r2 = f6323a     // Catch:{ NoSuchFieldError -> 0x005e }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r3 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_SPLASH     // Catch:{ NoSuchFieldError -> 0x005e }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x005e }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x005e }
            L_0x005e:
                int[] r0 = f6323a     // Catch:{ NoSuchFieldError -> 0x0068 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r2 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_RETURN     // Catch:{ NoSuchFieldError -> 0x0068 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0068 }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x0068 }
            L_0x0068:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.cache.a.AnonymousClass6.<clinit>():void");
        }
    }

    /* renamed from: com.startapp.sdk.adsbase.cache.a$a  reason: collision with other inner class name */
    class C0074a {

        /* renamed from: a  reason: collision with root package name */
        StartAppAd f6324a;
        AdPreferences.Placement b;
        AdPreferences c;
        b d;

        C0074a(StartAppAd startAppAd, AdPreferences.Placement placement, AdPreferences adPreferences, b bVar) {
            this.f6324a = startAppAd;
            this.b = placement;
            this.c = adPreferences;
            this.d = bVar;
        }
    }

    private a() {
    }

    public static a a() {
        return d;
    }

    private int d() {
        return this.f6317a.size();
    }

    private boolean e() {
        return !this.b && CacheMetaData.a().b().d();
    }

    public final void b() {
        if (!this.f) {
            synchronized (this.f6317a) {
                for (e e2 : this.f6317a.values()) {
                    e2.e();
                }
            }
        }
    }

    public final void c(final Context context) {
        AnonymousClass3 r0 = new com.startapp.sdk.adsbase.remoteconfig.b() {
            public final void a() {
            }

            public final void a(MetaDataRequest.RequestReason requestReason, boolean z) {
                Set<StartAppAd.AdMode> c;
                if (z && (c = CacheMetaData.a().b().c()) != null) {
                    for (StartAppAd.AdMode next : a.this.a(c)) {
                        int b2 = AdsCommonMetaData.a().b();
                        StartAppAd.AdMode adMode = StartAppAd.AdMode.FULLPAGE;
                        if (next != adMode) {
                            StartAppAd.AdMode adMode2 = StartAppAd.AdMode.OFFERWALL;
                            if (next != adMode2) {
                                a.this.a(context, (StartAppAd) null, next, new AdPreferences(), (b) null);
                            } else if (b2 < 100) {
                                a.this.a(context, (StartAppAd) null, adMode2, new AdPreferences(), (b) null);
                            }
                        } else if (b2 > 0) {
                            a.this.a(context, (StartAppAd) null, adMode, new AdPreferences(), (b) null);
                        }
                        String a2 = a.a(next);
                        if (a2 != null) {
                            j.b(context, a2, Integer.valueOf(j.a(context, a2, (Integer) 0).intValue() + 1));
                        }
                    }
                }
            }
        };
        synchronized (MetaData.i()) {
            MetaData.G().a((com.startapp.sdk.adsbase.remoteconfig.b) r0);
        }
    }

    public final CacheKey a(Context context, StartAppAd startAppAd, AdPreferences adPreferences, b bVar) {
        if (!c(AdPreferences.Placement.INAPP_SPLASH)) {
            return null;
        }
        return a(context, startAppAd, AdPreferences.Placement.INAPP_SPLASH, adPreferences, bVar);
    }

    /* access modifiers changed from: protected */
    public final void d(Context context) {
        this.f = false;
        for (C0074a aVar : this.g) {
            if (c(aVar.b)) {
                a(context, aVar.f6324a, aVar.b, aVar.c, aVar.d);
            }
        }
        this.g.clear();
    }

    public final CacheKey a(Context context, AdPreferences adPreferences) {
        if (!c(AdPreferences.Placement.INAPP_RETURN)) {
            return null;
        }
        return a(context, (StartAppAd) null, AdPreferences.Placement.INAPP_RETURN, adPreferences, (b) null);
    }

    public final synchronized List<e> c() {
        return new ArrayList(this.f6317a.values());
    }

    protected static String c(CacheKey cacheKey) {
        return String.valueOf(cacheKey.hashCode()).replace('-', '_');
    }

    /* JADX WARNING: Can't fix incorrect switch cases order */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final com.startapp.sdk.adsbase.cache.CacheKey a(android.content.Context r9, com.startapp.sdk.adsbase.StartAppAd r10, com.startapp.sdk.adsbase.StartAppAd.AdMode r11, com.startapp.sdk.adsbase.model.AdPreferences r12, com.startapp.sdk.adsbase.adlisteners.b r13) {
        /*
            r8 = this;
            if (r12 != 0) goto L_0x0007
            com.startapp.sdk.adsbase.model.AdPreferences r12 = new com.startapp.sdk.adsbase.model.AdPreferences
            r12.<init>()
        L_0x0007:
            r4 = r12
            int[] r12 = com.startapp.sdk.adsbase.cache.a.AnonymousClass6.b
            int r0 = r11.ordinal()
            r12 = r12[r0]
            switch(r12) {
                case 1: goto L_0x005a;
                case 2: goto L_0x0057;
                case 3: goto L_0x0057;
                case 4: goto L_0x0057;
                case 5: goto L_0x0057;
                case 6: goto L_0x0014;
                default: goto L_0x0013;
            }
        L_0x0013:
            goto L_0x0060
        L_0x0014:
            com.startapp.sdk.adsbase.j.u.b()
            com.startapp.sdk.adsbase.j.u.b()
            com.startapp.sdk.adsbase.j.u.b()
            com.startapp.sdk.adsbase.AdsCommonMetaData r12 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            int r12 = r12.b()
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            r1 = 100
            int r0 = r0.nextInt(r1)
            if (r0 >= r12) goto L_0x0054
            com.startapp.sdk.adsbase.AdsCommonMetaData r12 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            int r12 = r12.c()
            java.util.Random r0 = new java.util.Random
            r0.<init>()
            int r0 = r0.nextInt(r1)
            if (r0 < r12) goto L_0x004b
            boolean r12 = r4.isForceFullpage()
            if (r12 == 0) goto L_0x0051
        L_0x004b:
            boolean r12 = r4.isForceOverlay()
            if (r12 == 0) goto L_0x0060
        L_0x0051:
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r12 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OVERLAY
            goto L_0x0062
        L_0x0054:
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r12 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_FULL_SCREEN
            goto L_0x0062
        L_0x0057:
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r12 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OVERLAY
            goto L_0x0062
        L_0x005a:
            com.startapp.sdk.adsbase.j.u.b()
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r12 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OFFER_WALL
            goto L_0x0062
        L_0x0060:
            com.startapp.sdk.adsbase.model.AdPreferences$Placement r12 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_FULL_SCREEN
        L_0x0062:
            r3 = r12
            com.startapp.sdk.adsbase.StartAppAd$AdMode r12 = com.startapp.sdk.adsbase.StartAppAd.AdMode.REWARDED_VIDEO
            boolean r12 = r11.equals(r12)
            if (r12 == 0) goto L_0x0071
            com.startapp.sdk.adsbase.Ad$AdType r11 = com.startapp.sdk.adsbase.Ad.AdType.REWARDED_VIDEO
            r4.setType(r11)
            goto L_0x007e
        L_0x0071:
            com.startapp.sdk.adsbase.StartAppAd$AdMode r12 = com.startapp.sdk.adsbase.StartAppAd.AdMode.VIDEO
            boolean r11 = r11.equals(r12)
            if (r11 == 0) goto L_0x007e
            com.startapp.sdk.adsbase.Ad$AdType r11 = com.startapp.sdk.adsbase.Ad.AdType.VIDEO
            r4.setType(r11)
        L_0x007e:
            r6 = 0
            r7 = 0
            r0 = r8
            r1 = r9
            r2 = r10
            r5 = r13
            com.startapp.sdk.adsbase.cache.CacheKey r9 = r0.a(r1, r2, r3, r4, r5, r6, r7)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.cache.a.a(android.content.Context, com.startapp.sdk.adsbase.StartAppAd, com.startapp.sdk.adsbase.StartAppAd$AdMode, com.startapp.sdk.adsbase.model.AdPreferences, com.startapp.sdk.adsbase.adlisteners.b):com.startapp.sdk.adsbase.cache.CacheKey");
    }

    public final void b(Context context) {
        this.b = true;
        ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable(context, new DiskAdCacheManager.c() {
            public final void a() {
                a.this.b = false;
            }
        }) {

            /* renamed from: a */
            final /* synthetic */ c f6310a;
            private /* synthetic */ Context b;

            public final void run(
/*
Method generation error in method: com.startapp.sdk.adsbase.cache.DiskAdCacheManager.1.run():void, dex: classes2.dex
            jadx.core.utils.exceptions.JadxRuntimeException: Method args not loaded: com.startapp.sdk.adsbase.cache.DiskAdCacheManager.1.run():void, class status: UNLOADED
            	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:278)
            	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:116)
            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:313)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:271)
            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:240)
            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
            	at java.util.ArrayList.forEach(ArrayList.java:1259)
            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
            	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
            	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
            	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
            	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
            	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
            	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
            	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:236)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:227)
            	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:676)
            	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:607)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:364)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:231)
            	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:123)
            	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:107)
            	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:787)
            	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:728)
            	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:368)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:250)
            	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:221)
            	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
            	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
            	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
            	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:211)
            	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:204)
            	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:318)
            	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:271)
            	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:240)
            	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
            	at java.util.ArrayList.forEach(ArrayList.java:1259)
            	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
            	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
            	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
            	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
            	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
            	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
            	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
            	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
            	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:236)
            	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:227)
            	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:112)
            	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:78)
            	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
            	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:33)
            	at jadx.core.codegen.CodeGen.generate(CodeGen.java:21)
            	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
            	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:273)
            
*/
        });
    }

    protected static boolean c(AdPreferences.Placement placement) {
        int i = AnonymousClass6.f6323a[placement.ordinal()];
        if (i == 1) {
            return k.a().k() && !AdsCommonMetaData.a().A();
        }
        if (i != 2) {
            return true;
        }
        return k.a().j() && !AdsCommonMetaData.a().z();
    }

    public final f b(CacheKey cacheKey) {
        e eVar = cacheKey != null ? this.f6317a.get(cacheKey) : null;
        if (eVar != null) {
            return eVar.b();
        }
        return null;
    }

    public final String b(String str) {
        return this.e.remove(str);
    }

    public final void b(AdPreferences.Placement placement) {
        synchronized (this.f6317a) {
            Iterator<Map.Entry<CacheKey, e>> it2 = this.f6317a.entrySet().iterator();
            while (it2.hasNext()) {
                if (((CacheKey) it2.next().getKey()).a() == placement) {
                    it2.remove();
                }
            }
        }
    }

    public final void a(final Context context) {
        this.c = context.getApplicationContext();
        if (e()) {
            this.f = true;
            ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable(context, new DiskAdCacheManager.b() {
                public final void a(List<DiskAdCacheManager.DiskCacheKey> list) {
                    if (list != null) {
                        try {
                            for (DiskAdCacheManager.DiskCacheKey next : list) {
                                if (a.c(next.placement)) {
                                    a.this.a(context, (StartAppAd) null, next.a(), next.b(), (b) null, true, next.c());
                                }
                            }
                        } catch (Throwable th) {
                            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
                        }
                    }
                    a.this.d(context);
                }
            }) {

                /* renamed from: a */
                final /* synthetic */ b f6312a;
                private /* synthetic */ Context b;

                public final void run(
/*
Method generation error in method: com.startapp.sdk.adsbase.cache.DiskAdCacheManager.2.run():void, dex: classes2.dex
                jadx.core.utils.exceptions.JadxRuntimeException: Method args not loaded: com.startapp.sdk.adsbase.cache.DiskAdCacheManager.2.run():void, class status: UNLOADED
                	at jadx.core.dex.nodes.MethodNode.getArgRegs(MethodNode.java:278)
                	at jadx.core.codegen.MethodGen.addDefinition(MethodGen.java:116)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:313)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:271)
                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:240)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
                	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
                	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:236)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:227)
                	at jadx.core.codegen.InsnGen.inlineAnonymousConstructor(InsnGen.java:676)
                	at jadx.core.codegen.InsnGen.makeConstructor(InsnGen.java:607)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:364)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:231)
                	at jadx.core.codegen.InsnGen.addWrappedArg(InsnGen.java:123)
                	at jadx.core.codegen.InsnGen.addArg(InsnGen.java:107)
                	at jadx.core.codegen.InsnGen.generateMethodArguments(InsnGen.java:787)
                	at jadx.core.codegen.InsnGen.makeInvoke(InsnGen.java:728)
                	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:368)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:250)
                	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:221)
                	at jadx.core.codegen.RegionGen.makeSimpleBlock(RegionGen.java:109)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:55)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:98)
                	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:142)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:62)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:92)
                	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:58)
                	at jadx.core.codegen.MethodGen.addRegionInsns(MethodGen.java:211)
                	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:204)
                	at jadx.core.codegen.ClassGen.addMethodCode(ClassGen.java:318)
                	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:271)
                	at jadx.core.codegen.ClassGen.lambda$addInnerClsAndMethods$2(ClassGen.java:240)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.accept(ForEachOps.java:183)
                	at java.util.ArrayList.forEach(ArrayList.java:1259)
                	at java.util.stream.SortedOps$RefSortingSink.end(SortedOps.java:395)
                	at java.util.stream.Sink$ChainedReference.end(Sink.java:258)
                	at java.util.stream.AbstractPipeline.copyInto(AbstractPipeline.java:483)
                	at java.util.stream.AbstractPipeline.wrapAndCopyInto(AbstractPipeline.java:472)
                	at java.util.stream.ForEachOps$ForEachOp.evaluateSequential(ForEachOps.java:150)
                	at java.util.stream.ForEachOps$ForEachOp$OfRef.evaluateSequential(ForEachOps.java:173)
                	at java.util.stream.AbstractPipeline.evaluate(AbstractPipeline.java:234)
                	at java.util.stream.ReferencePipeline.forEach(ReferencePipeline.java:485)
                	at jadx.core.codegen.ClassGen.addInnerClsAndMethods(ClassGen.java:236)
                	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:227)
                	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:112)
                	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:78)
                	at jadx.core.codegen.CodeGen.wrapCodeGen(CodeGen.java:44)
                	at jadx.core.codegen.CodeGen.generateJavaCode(CodeGen.java:33)
                	at jadx.core.codegen.CodeGen.generate(CodeGen.java:21)
                	at jadx.core.ProcessClass.generateCode(ProcessClass.java:61)
                	at jadx.core.dex.nodes.ClassNode.decompile(ClassNode.java:273)
                
*/
            });
        }
    }

    public final void a(AdPreferences.Placement placement) {
        if (!this.f) {
            synchronized (this.f6317a) {
                for (e next : this.f6317a.values()) {
                    if (next.c() == placement) {
                        next.e();
                    }
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public final Set<StartAppAd.AdMode> a(Set<StartAppAd.AdMode> set) {
        Iterator<StartAppAd.AdMode> it2 = set.iterator();
        while (it2.hasNext()) {
            boolean z = false;
            if (j.a(this.c, a(it2.next()), (Integer) 0).intValue() >= MetaData.G().O()) {
                z = true;
            }
            if (z) {
                it2.remove();
            }
        }
        u.b();
        u.b();
        u.b();
        return set;
    }

    public final f a(CacheKey cacheKey) {
        e eVar;
        if (cacheKey == null || (eVar = this.f6317a.get(cacheKey)) == null) {
            return null;
        }
        return eVar.h();
    }

    public final String a(String str, String str2) {
        this.e.put(str2, str);
        return str2;
    }

    public final String a(String str) {
        return this.e.get(str);
    }

    public final CacheKey a(Context context, StartAppAd startAppAd, AdPreferences.Placement placement, AdPreferences adPreferences, b bVar) {
        return a(context, startAppAd, placement, adPreferences, bVar, false, 0);
    }

    /* access modifiers changed from: protected */
    public final CacheKey a(Context context, StartAppAd startAppAd, AdPreferences.Placement placement, AdPreferences adPreferences, b bVar, boolean z, int i) {
        e eVar;
        Context context2 = context;
        AdPreferences.Placement placement2 = placement;
        this.c = context.getApplicationContext();
        AdPreferences adPreferences2 = adPreferences == null ? new AdPreferences() : adPreferences;
        CacheKey cacheKey = new CacheKey(placement2, adPreferences2);
        if (!this.f || z) {
            AdPreferences adPreferences3 = new AdPreferences(adPreferences2);
            synchronized (this.f6317a) {
                eVar = this.f6317a.get(cacheKey);
                if (eVar == null) {
                    if (AnonymousClass6.f6323a[placement.ordinal()] != 1) {
                        eVar = new e(context2, placement2, adPreferences3);
                    } else {
                        eVar = new e(context2, placement2, adPreferences3, (byte) 0);
                    }
                    if (this.h == null) {
                        this.h = new e.b() {
                            public final void a(e eVar) {
                                synchronized (a.this.f6317a) {
                                    CacheKey cacheKey = null;
                                    Iterator<CacheKey> it2 = a.this.f6317a.keySet().iterator();
                                    while (true) {
                                        if (!it2.hasNext()) {
                                            break;
                                        }
                                        CacheKey next = it2.next();
                                        if (a.this.f6317a.get(next) == eVar) {
                                            cacheKey = next;
                                            break;
                                        }
                                    }
                                    if (cacheKey != null) {
                                        a.this.f6317a.remove(cacheKey);
                                        if (eVar.c() != AdPreferences.Placement.INAPP_SPLASH) {
                                            new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.ERROR).f("Stop reload in cache").g(cacheKey.toString()).a(a.this.c);
                                        }
                                    }
                                }
                            }
                        };
                    }
                    eVar.a(this.h);
                    if (z) {
                        eVar.a(c(cacheKey));
                        eVar.d();
                        eVar.a(i);
                    }
                    synchronized (this.f6317a) {
                        int g2 = CacheMetaData.a().b().g();
                        if (g2 != 0 && d() >= g2) {
                            long j = Clock.MAX_TIME;
                            CacheKey cacheKey2 = null;
                            for (CacheKey next : this.f6317a.keySet()) {
                                e eVar2 = this.f6317a.get(next);
                                if (eVar2.c() == eVar.c() && eVar2.c < j) {
                                    j = eVar2.c;
                                    cacheKey2 = next;
                                }
                            }
                            if (cacheKey2 != null) {
                                this.f6317a.remove(cacheKey2);
                            }
                        }
                        this.f6317a.put(cacheKey, eVar);
                        if (Math.random() * 100.0d < ((double) CacheMetaData.a().c())) {
                            new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.GENERAL).f("Cache Size").g(String.valueOf(d())).a(context2);
                        }
                    }
                } else {
                    eVar.a(adPreferences3);
                }
            }
            eVar.a(startAppAd, bVar);
            return cacheKey;
        }
        this.g.add(new C0074a(startAppAd, placement, adPreferences2, bVar));
        return cacheKey;
    }

    public static String a(StartAppAd.AdMode adMode) {
        if (adMode == null) {
            return null;
        }
        return "autoLoadNotShownAdPrefix" + adMode.name();
    }

    public final void a(final Context context, boolean z) {
        if (e()) {
            ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable() {
                public final void run() {
                    try {
                        d.c(context, DiskAdCacheManager.a());
                        d.c(context, DiskAdCacheManager.b());
                        CacheKey cacheKey = null;
                        for (Map.Entry next : a.this.f6317a.entrySet()) {
                            CacheKey cacheKey2 = (CacheKey) next.getKey();
                            if (cacheKey2.a() == AdPreferences.Placement.INAPP_SPLASH) {
                                cacheKey = cacheKey2;
                            } else {
                                e eVar = (e) next.getValue();
                                DiskAdCacheManager.a(context, cacheKey2.a(), eVar.a(), a.c(cacheKey2), eVar.j());
                                DiskAdCacheManager.a(context, eVar, a.c(cacheKey2));
                            }
                        }
                        if (cacheKey != null) {
                            a.this.f6317a.remove(cacheKey);
                        }
                    } catch (Throwable th) {
                        new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
                    }
                }
            });
        }
        for (e next : this.f6317a.values()) {
            if (next.b() != null) {
                u.b();
                if ((next.b() instanceof ReturnAd) && !z && CacheMetaData.a().b().e()) {
                    next.f();
                }
            }
            next.g();
            next.f();
        }
    }
}
