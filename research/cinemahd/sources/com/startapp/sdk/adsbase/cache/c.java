package com.startapp.sdk.adsbase.cache;

import android.os.Handler;
import android.os.Looper;

public abstract class c {

    /* renamed from: a  reason: collision with root package name */
    protected e f6325a;
    private Handler b = null;
    private Long c = null;
    private boolean d = false;

    public c(e eVar) {
        this.f6325a = eVar;
    }

    private void i() {
        Handler handler = this.b;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
        }
    }

    private void j() {
        this.c = null;
        this.d = false;
    }

    public void a() {
        f();
    }

    /* access modifiers changed from: protected */
    public void b() {
        j();
        this.f6325a.i();
    }

    /* access modifiers changed from: protected */
    public abstract boolean c();

    /* access modifiers changed from: protected */
    public abstract long d();

    public final void e() {
        if (!this.d) {
            if (this.c == null) {
                this.c = Long.valueOf(System.currentTimeMillis());
            }
            if (c()) {
                if (this.b == null) {
                    Looper myLooper = Looper.myLooper();
                    if (myLooper == null) {
                        myLooper = Looper.getMainLooper();
                    }
                    this.b = new Handler(myLooper);
                }
                long d2 = d();
                if (d2 >= 0) {
                    this.d = true;
                    this.b.postDelayed(new Runnable() {
                        public final void run() {
                            c.this.b();
                        }
                    }, d2);
                }
            }
        }
    }

    public final void f() {
        i();
        j();
    }

    public final void g() {
        i();
        this.d = false;
    }

    /* access modifiers changed from: protected */
    public final Long h() {
        return this.c;
    }
}
