package com.startapp.sdk.adsbase.cache;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FailuresHandler implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean infiniteLastRetry = true;
    @d(b = ArrayList.class, c = Integer.class)
    private List<Integer> intervals = Arrays.asList(new Integer[]{10, 30, 60, 300});

    public final List<Integer> a() {
        return this.intervals;
    }

    public final boolean b() {
        return this.infiniteLastRetry;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && FailuresHandler.class == obj.getClass()) {
            FailuresHandler failuresHandler = (FailuresHandler) obj;
            return this.infiniteLastRetry == failuresHandler.infiniteLastRetry && u.b(this.intervals, failuresHandler.intervals);
        }
    }

    public int hashCode() {
        return u.a(this.intervals, Boolean.valueOf(this.infiniteLastRetry));
    }
}
