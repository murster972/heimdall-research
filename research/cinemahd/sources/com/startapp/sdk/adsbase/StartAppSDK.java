package com.startapp.sdk.adsbase;

import android.content.Context;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;

public class StartAppSDK {
    public static void addWrapper(Context context, String str, String str2) {
        k.a().a(context, str, str2);
    }

    public static void enableReturnAds(boolean z) {
        k.a().d(z);
    }

    public static String getVersion() {
        return "4.6.3";
    }

    public static void inAppPurchaseMade(Context context) {
        inAppPurchaseMade(context, 0.0d);
    }

    public static void init(Context context, String str) {
        init(context, str, new SDKAdPreferences());
    }

    private static void pauseServices(Context context) {
        k.a();
        k.c(context);
        k.a();
        k.d(context);
    }

    private static void resumeServices(Context context) {
        k.a();
        k.e(context);
        k.a();
        k.f(context);
    }

    public static void setTestAdsEnabled(boolean z) {
        k.a().e(z);
    }

    public static void setUserConsent(Context context, String str, long j, boolean z) {
        k.a();
        k.a(context, str, z);
    }

    public static void startNewSession(Context context) {
        k.a();
        k.a(context, MetaDataRequest.RequestReason.CUSTOM);
    }

    public static void inAppPurchaseMade(Context context, double d) {
        j.b(context, "payingUser", Boolean.TRUE);
        j.b(context, "inAppPurchaseAmount", Float.valueOf((float) (((double) j.a(context, "inAppPurchaseAmount", Float.valueOf(0.0f)).floatValue()) + d)));
        k.a();
        k.a(context, MetaDataRequest.RequestReason.IN_APP_PURCHASE);
    }

    public static void init(Context context, String str, SDKAdPreferences sDKAdPreferences) {
        init(context, (String) null, str, sDKAdPreferences);
    }

    public static void init(Context context, String str, String str2) {
        init(context, str, str2, new SDKAdPreferences());
    }

    public static void init(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences) {
        init(context, str, str2, sDKAdPreferences, true);
    }

    public static void init(Context context, String str, boolean z) {
        init(context, (String) null, str, z);
    }

    public static void init(Context context, String str, String str2, boolean z) {
        init(context, str, str2, new SDKAdPreferences(), z);
    }

    public static void init(Context context, String str, SDKAdPreferences sDKAdPreferences, boolean z) {
        init(context, (String) null, str, sDKAdPreferences, z);
    }

    public static void init(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z) {
        k.a().a(context, str, str2, sDKAdPreferences, z);
    }
}
