package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import android.graphics.Bitmap;
import com.startapp.common.a;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class ImageResourceConfig implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private transient Bitmap f6284a;
    private transient Bitmap b;
    private transient Bitmap c = null;
    private int height = 1;
    private String imageFallbackUrl = "";
    private String imageUrlSecured = "";
    private String name;
    private int width = 1;

    private ImageResourceConfig() {
    }

    public final String a() {
        return this.name;
    }

    public final int b() {
        return this.width;
    }

    public final int c() {
        return this.height;
    }

    public final String d() {
        String str = this.imageUrlSecured;
        return str != null ? str : "";
    }

    /* access modifiers changed from: protected */
    public final void e() {
        a((Bitmap) null);
        new a(d(), new a.C0062a() {
            public final void a(Bitmap bitmap, int i) {
                ImageResourceConfig.this.a(bitmap);
            }
        }, 0).a();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && ImageResourceConfig.class == obj.getClass()) {
            ImageResourceConfig imageResourceConfig = (ImageResourceConfig) obj;
            return this.width == imageResourceConfig.width && this.height == imageResourceConfig.height && u.b(this.imageUrlSecured, imageResourceConfig.imageUrlSecured) && u.b(this.imageFallbackUrl, imageResourceConfig.imageFallbackUrl) && u.b(this.name, imageResourceConfig.name);
        }
    }

    public int hashCode() {
        return u.a(this.imageUrlSecured, this.imageFallbackUrl, Integer.valueOf(this.width), Integer.valueOf(this.height), this.name);
    }

    public final Bitmap a(Context context) {
        if (this.c == null) {
            this.c = this.f6284a;
            if (this.c == null) {
                if (this.b == null) {
                    this.b = com.startapp.sdk.adsbase.j.a.a(context, this.imageFallbackUrl);
                }
                this.c = this.b;
            }
        }
        return this.c;
    }

    public final void b(int i) {
        this.height = i;
    }

    public static ImageResourceConfig b(String str) {
        ImageResourceConfig imageResourceConfig = new ImageResourceConfig();
        imageResourceConfig.name = str;
        return imageResourceConfig;
    }

    public final void a(int i) {
        this.width = i;
    }

    public final void a(String str) {
        this.imageFallbackUrl = str;
    }

    /* access modifiers changed from: protected */
    public final void a(Bitmap bitmap) {
        this.f6284a = bitmap;
        if (bitmap != null) {
            this.c = bitmap;
        }
    }
}
