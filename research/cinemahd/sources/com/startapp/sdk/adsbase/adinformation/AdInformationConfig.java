package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class AdInformationConfig implements Serializable {
    private static final long serialVersionUID = 1;
    @d(b = ArrayList.class, c = ImageResourceConfig.class)
    private List<ImageResourceConfig> ImageResources = new ArrayList();
    @d(b = HashMap.class, c = AdInformationPositions.Position.class, d = AdPreferences.Placement.class)
    protected HashMap<AdPreferences.Placement, AdInformationPositions.Position> Positions = new HashMap<>();

    /* renamed from: a  reason: collision with root package name */
    private transient EnumMap<ImageResourceType, ImageResourceConfig> f6270a = new EnumMap<>(ImageResourceType.class);
    private String dialogUrlSecured = "https://d1byvlfiet2h9q.cloudfront.net/InApp/resources/adInformationDialog3.html";
    private boolean enabled = true;
    private String eulaUrlSecured = "https://www.com.startapp.com/policy/sdk-policy/";
    private float fatFingersFactor = 200.0f;

    public enum ImageResourceType {
        INFO_S(17, 14),
        INFO_EX_S(88, 14),
        INFO_L(25, 21),
        INFO_EX_L(130, 21);
        
        private int height;
        private int width;

        private ImageResourceType(int i, int i2) {
            this.width = i;
            this.height = i2;
        }

        public static ImageResourceType getByName(String str) {
            ImageResourceType imageResourceType = INFO_S;
            ImageResourceType[] values = values();
            for (int i = 0; i < values.length; i++) {
                if (values[i].name().toLowerCase().compareTo(str.toLowerCase()) == 0) {
                    imageResourceType = values[i];
                }
            }
            return imageResourceType;
        }

        public final int getDefaultHeight() {
            return this.height;
        }

        public final int getDefaultWidth() {
            return this.width;
        }
    }

    private AdInformationConfig() {
    }

    public static AdInformationConfig a() {
        AdInformationConfig adInformationConfig = new AdInformationConfig();
        a(adInformationConfig);
        return adInformationConfig;
    }

    private void h() {
        ImageResourceType[] values = ImageResourceType.values();
        int length = values.length;
        int i = 0;
        while (i < length) {
            ImageResourceType imageResourceType = values[i];
            if (this.f6270a.get(imageResourceType) != null) {
                i++;
            } else {
                throw new IllegalArgumentException("AdInformation error in ImageResource [" + imageResourceType + "] cannot be found in MetaData");
            }
        }
    }

    private void i() {
        for (ImageResourceType imageResourceType : ImageResourceType.values()) {
            ImageResourceConfig imageResourceConfig = this.f6270a.get(imageResourceType);
            Boolean bool = Boolean.TRUE;
            if (imageResourceConfig == null) {
                imageResourceConfig = ImageResourceConfig.b(imageResourceType.name());
                Iterator<ImageResourceConfig> it2 = this.ImageResources.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (ImageResourceType.getByName(it2.next().a()).equals(imageResourceType)) {
                            bool = Boolean.FALSE;
                            break;
                        }
                    } else {
                        break;
                    }
                }
                this.f6270a.put(imageResourceType, imageResourceConfig);
                if (bool.booleanValue()) {
                    this.ImageResources.add(imageResourceConfig);
                }
            }
            imageResourceConfig.a(imageResourceType.getDefaultWidth());
            imageResourceConfig.b(imageResourceType.getDefaultHeight());
            imageResourceConfig.a(imageResourceType.name().toLowerCase() + ".png");
        }
    }

    public final String b() {
        String str = this.eulaUrlSecured;
        return (str == null || str.equals("")) ? "https://www.com.startapp.com/policy/sdk-policy/" : this.eulaUrlSecured;
    }

    public final String c() {
        return (!this.f6270a.containsKey(ImageResourceType.INFO_L) || this.f6270a.get(ImageResourceType.INFO_L).d().equals("")) ? "https://info.startappservice.com/InApp/resources/info_l.png" : this.f6270a.get(ImageResourceType.INFO_L).d();
    }

    public final float d() {
        return this.fatFingersFactor / 100.0f;
    }

    public final String e() {
        String str = this.dialogUrlSecured;
        return str != null ? str : "https://d1byvlfiet2h9q.cloudfront.net/InApp/resources/adInformationDialog3.html";
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AdInformationConfig.class == obj.getClass()) {
            AdInformationConfig adInformationConfig = (AdInformationConfig) obj;
            return this.enabled == adInformationConfig.enabled && Float.compare(adInformationConfig.fatFingersFactor, this.fatFingersFactor) == 0 && u.b(this.dialogUrlSecured, adInformationConfig.dialogUrlSecured) && u.b(this.eulaUrlSecured, adInformationConfig.eulaUrlSecured) && u.b(this.Positions, adInformationConfig.Positions) && u.b(this.ImageResources, adInformationConfig.ImageResources);
        }
    }

    public final void f() {
        for (ImageResourceConfig next : this.ImageResources) {
            this.f6270a.put(ImageResourceType.getByName(next.a()), next);
            next.e();
        }
    }

    public final void g() {
        this.f6270a = new EnumMap<>(ImageResourceType.class);
    }

    public int hashCode() {
        return u.a(Boolean.valueOf(this.enabled), Float.valueOf(this.fatFingersFactor), this.dialogUrlSecured, this.eulaUrlSecured, this.Positions, this.ImageResources);
    }

    public static void a(AdInformationConfig adInformationConfig) {
        adInformationConfig.i();
        adInformationConfig.h();
    }

    public static void b(Context context) {
        j.b(context, "userDisabledAdInformation", Boolean.FALSE);
    }

    public final boolean a(Context context) {
        return !j.a(context, "userDisabledAdInformation", Boolean.FALSE).booleanValue() && this.enabled;
    }

    public final AdInformationPositions.Position a(AdPreferences.Placement placement) {
        AdInformationPositions.Position position = this.Positions.get(placement);
        if (position != null) {
            return position;
        }
        AdInformationPositions.Position position2 = AdInformationPositions.Position.BOTTOM_LEFT;
        this.Positions.put(placement, position2);
        return position2;
    }

    public final ImageResourceConfig a(ImageResourceType imageResourceType) {
        return this.f6270a.get(imageResourceType);
    }
}
