package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import android.webkit.JavascriptInterface;
import com.startapp.sdk.c.c;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f6286a = null;
    private boolean b = false;
    private Runnable c = null;
    private Runnable d = null;
    private Runnable e = null;

    public a(Context context, Runnable runnable, Runnable runnable2, Runnable runnable3) {
        this.f6286a = context;
        this.d = runnable;
        this.c = runnable2;
        this.e = runnable3;
    }

    @JavascriptInterface
    public final void accept() {
        if (!this.b) {
            this.b = true;
            this.d.run();
        }
    }

    @JavascriptInterface
    public final void decline() {
        if (!this.b) {
            this.b = true;
            this.c.run();
        }
    }

    @JavascriptInterface
    public final void fullPrivacyPolicy() {
        if (!this.b) {
            this.b = true;
            this.e.run();
        }
    }

    @JavascriptInterface
    public final String getAppId() {
        Context context = this.f6286a;
        if (context != null) {
            try {
                String b2 = c.a(context).j().b();
                if (b2 == null) {
                    return null;
                }
                return String.valueOf(Long.parseLong(b2) ^ 121212121);
            } catch (Exception unused) {
            }
        }
        return null;
    }
}
