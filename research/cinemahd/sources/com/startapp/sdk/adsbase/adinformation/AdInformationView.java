package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.startapp.sdk.adsbase.adinformation.AdInformationObject;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.model.AdPreferences;

public class AdInformationView extends RelativeLayout {

    /* renamed from: a  reason: collision with root package name */
    private ImageView f6282a;
    private RelativeLayout b;
    private View.OnClickListener c = null;
    private AdInformationConfig d;
    private ImageResourceConfig e;
    private AdPreferences.Placement f;
    private AdInformationPositions.Position g;

    public AdInformationView(Context context, AdInformationObject.Size size, AdPreferences.Placement placement, AdInformationOverrides adInformationOverrides, final View.OnClickListener onClickListener) {
        super(context);
        this.f = placement;
        this.c = new View.OnClickListener() {
            public final void onClick(View view) {
                onClickListener.onClick(view);
            }
        };
        getContext();
        this.d = AdInformationObject.c();
        if (this.d == null) {
            this.d = AdInformationConfig.a();
        }
        this.e = this.d.a(size.a());
        if (adInformationOverrides == null || !adInformationOverrides.d()) {
            this.g = this.d.a(this.f);
        } else {
            this.g = adInformationOverrides.c();
        }
        this.f6282a = new ImageView(getContext());
        this.f6282a.setContentDescription("info");
        this.f6282a.setId(1475346433);
        this.f6282a.setImageBitmap(this.e.a(getContext()));
        this.b = new RelativeLayout(getContext());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(t.a(getContext(), (int) (((float) this.e.b()) * this.d.d())), t.a(getContext(), (int) (((float) this.e.c()) * this.d.d())));
        this.b.setBackgroundColor(0);
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(t.a(getContext(), this.e.b()), t.a(getContext(), this.e.c()));
        layoutParams2.setMargins(0, 0, 0, 0);
        this.f6282a.setPadding(0, 0, 0, 0);
        this.g.addRules(layoutParams2);
        this.b.addView(this.f6282a, layoutParams2);
        this.b.setOnClickListener(this.c);
        addView(this.b, layoutParams);
    }
}
