package com.startapp.sdk.adsbase.adinformation;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.adinformation.AdInformationConfig;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.SimpleTokenConfig;

public class AdInformationObject implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    Context f6272a;
    RelativeLayout b;
    RelativeLayout c;
    private AdInformationView d;
    private WebView e;
    private Dialog f = null;
    private AdPreferences.Placement g;
    private final Handler h = new Handler(Looper.getMainLooper());
    private DisplayMode i = DisplayMode.REGULAR;
    private boolean j = false;
    private AdInformationOverrides k;
    private AdInformationConfig l;
    private SimpleTokenConfig m;
    private Runnable n = new Runnable() {
        public final void run() {
            try {
                AdInformationObject.this.e();
                SimpleTokenConfig.a(AdInformationObject.this.f6272a, true);
                AdInformationConfig.b(AdInformationObject.this.f6272a);
                AdInformationObject.this.a(false);
            } catch (Throwable th) {
                new e(th).a(AdInformationObject.this.f6272a);
            }
        }
    };
    private Runnable o = new Runnable() {
        public final void run() {
            try {
                AdInformationObject.this.e();
                SimpleTokenUtils.b();
                SimpleTokenConfig.a(AdInformationObject.this.f6272a, false);
                AdInformationConfig.b(AdInformationObject.this.f6272a);
                AdInformationObject.this.a(false);
            } catch (Throwable th) {
                new e(th).a(AdInformationObject.this.f6272a);
            }
        }
    };
    private Runnable p = new Runnable() {
        public final void run() {
            try {
                AdInformationObject.this.e();
                AdInformationObject.this.d();
                AdInformationObject.this.a(false);
            } catch (Throwable th) {
                new e(th).a(AdInformationObject.this.f6272a);
            }
        }
    };

    /* renamed from: com.startapp.sdk.adsbase.adinformation.AdInformationObject$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6278a = new int[DisplayMode.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.startapp.sdk.adsbase.adinformation.AdInformationObject$DisplayMode[] r0 = com.startapp.sdk.adsbase.adinformation.AdInformationObject.DisplayMode.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6278a = r0
                int[] r0 = f6278a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.adinformation.AdInformationObject$DisplayMode r1 = com.startapp.sdk.adsbase.adinformation.AdInformationObject.DisplayMode.LAYOUT     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6278a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.adinformation.AdInformationObject$DisplayMode r1 = com.startapp.sdk.adsbase.adinformation.AdInformationObject.DisplayMode.REGULAR     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.adinformation.AdInformationObject.AnonymousClass6.<clinit>():void");
        }
    }

    public enum DisplayMode {
        REGULAR,
        LAYOUT
    }

    public enum Size {
        SMALL(AdInformationConfig.ImageResourceType.INFO_S, AdInformationConfig.ImageResourceType.INFO_EX_S),
        LARGE(AdInformationConfig.ImageResourceType.INFO_L, AdInformationConfig.ImageResourceType.INFO_EX_L);
        
        private AdInformationConfig.ImageResourceType infoExtendedType;
        private AdInformationConfig.ImageResourceType infoType;

        private Size(AdInformationConfig.ImageResourceType imageResourceType, AdInformationConfig.ImageResourceType imageResourceType2) {
            this.infoType = imageResourceType;
            this.infoExtendedType = imageResourceType2;
        }

        public final AdInformationConfig.ImageResourceType a() {
            return this.infoType;
        }
    }

    static {
        Class<AdInformationObject> cls = AdInformationObject.class;
    }

    public AdInformationObject(Context context, Size size, AdPreferences.Placement placement, AdInformationOverrides adInformationOverrides) {
        this.f6272a = context;
        this.g = placement;
        this.k = adInformationOverrides;
        this.l = AdInformationMetaData.b().a();
        this.m = MetaData.G().f();
        this.d = new AdInformationView(context, size, placement, adInformationOverrides, this);
    }

    public static AdInformationConfig c() {
        return AdInformationMetaData.b().a();
    }

    public final View a() {
        return this.d;
    }

    public final boolean b() {
        return this.j;
    }

    /* access modifiers changed from: protected */
    public final void d() {
        u.b();
        if (MetaData.G().B()) {
            a.a(this.f6272a, this.l.b(), "");
        } else {
            a.c(this.f6272a, this.l.b());
        }
    }

    public final void e() {
        this.j = false;
        int i2 = AnonymousClass6.f6278a[this.i.ordinal()];
        if (i2 == 1) {
            this.h.post(new Runnable() {
                public final void run() {
                    AdInformationObject adInformationObject = AdInformationObject.this;
                    adInformationObject.c.removeView(adInformationObject.b);
                }
            });
        } else if (i2 == 2) {
            this.f.dismiss();
        }
    }

    public void onClick(View view) {
        if (this.m.a(this.f6272a)) {
            Context context = this.f6272a;
            if ((context instanceof Activity) && !((Activity) context).isFinishing()) {
                a(true);
                this.b = new RelativeLayout(this.f6272a);
                try {
                    this.e = new WebView(this.f6272a);
                    this.e.setWebViewClient(new WebViewClient());
                    this.e.setWebChromeClient(new WebChromeClient());
                    this.e.getSettings().setJavaScriptEnabled(true);
                    this.e.setHorizontalScrollBarEnabled(false);
                    this.e.setVerticalScrollBarEnabled(false);
                    this.e.loadUrl(this.l.e());
                    this.e.addJavascriptInterface(new a(this.f6272a, this.n, this.o, this.p), "startappwall");
                    Point point = new Point(1, 1);
                    try {
                        WindowManager windowManager = (WindowManager) this.f6272a.getSystemService("window");
                        if (Build.VERSION.SDK_INT >= 13) {
                            windowManager.getDefaultDisplay().getSize(point);
                        } else {
                            point.x = windowManager.getDefaultDisplay().getWidth();
                            point.y = windowManager.getDefaultDisplay().getHeight();
                        }
                        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
                        layoutParams.addRule(13);
                        this.e.setPadding(0, 0, 0, 0);
                        layoutParams.setMargins(0, 0, 0, 0);
                        this.b.addView(this.e, layoutParams);
                        String a2 = a.a(this.f6272a, (String) null);
                        if (a2 != null) {
                            WebView webView = this.e;
                            webView.loadUrl("javascript:window.onload=function(){document.getElementById('titlePlacement').innerText='" + a2 + "';}");
                        }
                        int i2 = AnonymousClass6.f6278a[this.i.ordinal()];
                        if (i2 == 1) {
                            final RelativeLayout relativeLayout = this.b;
                            this.j = true;
                            final RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams((int) (((float) point.x) * 0.9f), (int) (((float) point.y) * 0.85f));
                            layoutParams2.addRule(13);
                            this.h.post(new Runnable() {
                                public final void run() {
                                    AdInformationObject.this.c.addView(relativeLayout, layoutParams2);
                                }
                            });
                            return;
                        } else if (i2 == 2) {
                            RelativeLayout relativeLayout2 = this.b;
                            this.j = true;
                            this.f = new Dialog(this.f6272a);
                            this.f.requestWindowFeature(1);
                            this.f.setContentView(relativeLayout2);
                            WindowManager.LayoutParams layoutParams3 = new WindowManager.LayoutParams();
                            layoutParams3.copyFrom(this.f.getWindow().getAttributes());
                            layoutParams3.width = (int) (((float) point.x) * 0.9f);
                            layoutParams3.height = (int) (((float) point.y) * 0.85f);
                            this.f.show();
                            this.f.getWindow().setAttributes(layoutParams3);
                            return;
                        } else {
                            return;
                        }
                    } catch (Throwable th) {
                        new e(th).a(this.f6272a);
                        a(false);
                        return;
                    }
                } catch (Throwable th2) {
                    new e(th2).a(this.f6272a);
                    a(false);
                    return;
                }
            }
        }
        d();
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        AdPreferences.Placement placement = this.g;
        if (!(placement == AdPreferences.Placement.INAPP_FULL_SCREEN || placement == AdPreferences.Placement.INAPP_OFFER_WALL || placement == AdPreferences.Placement.INAPP_SPLASH || placement == AdPreferences.Placement.INAPP_OVERLAY)) {
            Context context = this.f6272a;
            if (context instanceof Activity) {
                u.a((Activity) context, z);
            }
        }
    }

    public final void a(RelativeLayout relativeLayout) {
        boolean z;
        AdInformationOverrides adInformationOverrides = this.k;
        if (adInformationOverrides == null || !adInformationOverrides.e()) {
            z = AdInformationMetaData.b().a().a(this.f6272a);
        } else {
            z = this.k.b();
        }
        if (z) {
            this.c = relativeLayout;
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            AdInformationOverrides adInformationOverrides2 = this.k;
            if (adInformationOverrides2 == null || !adInformationOverrides2.d()) {
                AdInformationMetaData.b().a().a(this.g).addRules(layoutParams);
            } else {
                this.k.c().addRules(layoutParams);
            }
            this.c.addView(this.d, layoutParams);
        }
    }
}
