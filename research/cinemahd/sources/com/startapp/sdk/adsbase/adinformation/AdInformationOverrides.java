package com.startapp.sdk.adsbase.adinformation;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions;
import java.io.Serializable;

public class AdInformationOverrides implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean enable = true;
    private boolean enableOverride = false;
    @d(b = AdInformationPositions.Position.class)
    private AdInformationPositions.Position position = AdInformationPositions.Position.getByName(AdInformationPositions.f6281a);
    private boolean positionOverride = false;

    private AdInformationOverrides() {
    }

    public static AdInformationOverrides a() {
        return new AdInformationOverrides();
    }

    public final boolean b() {
        return this.enable;
    }

    public final AdInformationPositions.Position c() {
        return this.position;
    }

    public final boolean d() {
        return this.positionOverride;
    }

    public final boolean e() {
        return this.enableOverride;
    }

    public final void a(boolean z) {
        this.enable = z;
        this.enableOverride = true;
    }

    public final void a(AdInformationPositions.Position position2) {
        this.position = position2;
        if (position2 != null) {
            this.positionOverride = true;
        } else {
            this.positionOverride = false;
        }
    }
}
