package com.startapp.sdk.adsbase.adinformation;

import android.content.Context;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class AdInformationMetaData implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static volatile AdInformationMetaData f6271a = new AdInformationMetaData();
    private static Object b = new Object();
    private static final long serialVersionUID = 1;
    @d(a = true)
    private AdInformationConfig AdInformation = AdInformationConfig.a();
    private String adInformationMetadataUpdateVersion = "4.6.3";

    public AdInformationMetaData() {
        this.AdInformation.f();
    }

    public static AdInformationMetaData b() {
        return f6271a;
    }

    public final AdInformationConfig a() {
        return this.AdInformation;
    }

    public final String c() {
        return this.AdInformation.b();
    }

    public final String d() {
        return this.AdInformation.c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AdInformationMetaData.class == obj.getClass()) {
            AdInformationMetaData adInformationMetaData = (AdInformationMetaData) obj;
            return u.b(this.AdInformation, adInformationMetaData.AdInformation) && u.b(this.adInformationMetadataUpdateVersion, adInformationMetaData.adInformationMetadataUpdateVersion);
        }
    }

    public int hashCode() {
        return u.a(this.AdInformation, this.adInformationMetadataUpdateVersion);
    }

    public static void a(Context context) {
        AdInformationMetaData adInformationMetaData = (AdInformationMetaData) com.startapp.common.b.d.a(context, "StartappAdInfoMetadata");
        AdInformationMetaData adInformationMetaData2 = new AdInformationMetaData();
        if (adInformationMetaData != null) {
            boolean a2 = u.a(adInformationMetaData, adInformationMetaData2);
            if (!(!"4.6.3".equals(adInformationMetaData.adInformationMetadataUpdateVersion)) && a2) {
                new e(InfoEventCategory.ERROR).f("metadata_null").a(context);
            }
            adInformationMetaData.AdInformation.g();
            f6271a = adInformationMetaData;
        } else {
            f6271a = adInformationMetaData2;
        }
        f6271a.AdInformation.f();
    }

    public static void a(Context context, AdInformationMetaData adInformationMetaData) {
        synchronized (b) {
            adInformationMetaData.adInformationMetadataUpdateVersion = "4.6.3";
            f6271a = adInformationMetaData;
            AdInformationConfig.a(adInformationMetaData.AdInformation);
            f6271a.AdInformation.f();
            com.startapp.common.b.d.a(context, "StartappAdInfoMetadata", (Serializable) adInformationMetaData);
        }
    }
}
