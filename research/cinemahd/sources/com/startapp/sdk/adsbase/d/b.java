package com.startapp.sdk.adsbase.d;

import android.content.Context;
import com.startapp.common.SDKException;
import com.startapp.common.a.d;
import com.startapp.common.b.e;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.e.a;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.j.q;
import com.startapp.sdk.adsbase.j.u;
import com.uwetrottmann.thetvdb.TheTvdb;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class b {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f6339a;
    private final d b;
    private final com.startapp.sdk.d.b.b c;
    private final a d;
    private g<c> e;

    public b(Context context, d dVar, com.startapp.sdk.d.b.b bVar, a aVar, g<c> gVar) {
        this.f6339a = context;
        this.b = dVar;
        this.c = bVar;
        this.d = aVar;
        this.e = gVar;
    }

    private c a() {
        c a2 = this.e.a();
        if (a2 != null) {
            return a2;
        }
        return c.b;
    }

    /* access modifiers changed from: package-private */
    public final String b(a aVar) {
        try {
            return b(aVar.f6338a, aVar.b, aVar.c, aVar.d, aVar.e);
        } catch (Throwable th) {
            new e(th).a(this.f6339a);
            return null;
        }
    }

    private String b(String str, c cVar, q<String> qVar, int i, long j) {
        byte[] bArr;
        int i2 = 1;
        boolean z = false;
        if (cVar != null) {
            try {
                bArr = cVar.e().toString().getBytes();
                if (a().a()) {
                    try {
                        bArr = u.b(bArr);
                        z = true;
                    } catch (IOException e2) {
                        new e((Throwable) e2).a(this.f6339a);
                    }
                }
            } catch (SDKException e3) {
                new e((Throwable) e3).a(this.f6339a);
                return null;
            }
        } else {
            bArr = null;
        }
        Map<String, String> b2 = b();
        while (true) {
            com.startapp.sdk.adsbase.e.c a2 = this.d.a();
            try {
                String a3 = com.startapp.common.b.e.a(str, bArr, b2, j.a(this.f6339a, "User-Agent", "-1"), z);
                a2.a("POST", str, (SDKException) null);
                return a3 != null ? a3 : "";
            } catch (SDKException e4) {
                a2.a("POST", str, e4);
                if (!e4.c() || i2 >= i) {
                    a(qVar, e4);
                    return null;
                }
                i2++;
                if (j > 0) {
                    try {
                        Thread.sleep(j);
                    } catch (InterruptedException e5) {
                        new e((Throwable) e5).a(this.f6339a);
                        return null;
                    }
                }
            }
        }
        a(qVar, e4);
        return null;
    }

    public final a a(String str) {
        return new a(this, str);
    }

    /* access modifiers changed from: package-private */
    public final e.a a(a aVar) {
        try {
            return a(aVar.f6338a, aVar.b, aVar.c, aVar.d, aVar.e);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6339a);
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0093, code lost:
        r12 = a().c();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.startapp.common.b.e.a a(java.lang.String r15, com.startapp.sdk.adsbase.c r16, com.startapp.sdk.adsbase.j.q<java.lang.String> r17, int r18, long r19) {
        /*
            r14 = this;
            r1 = r14
            r2 = r15
            r3 = r16
            java.lang.String r4 = "GET"
            java.lang.String r5 = "?"
            java.util.Map r6 = r14.b()
            r7 = 1
            r0 = r2
            r8 = 1
        L_0x000f:
            r9 = 0
            if (r3 == 0) goto L_0x005e
            if (r8 <= r7) goto L_0x0019
            int r0 = r8 + -1
            r3.b((int) r0)
        L_0x0019:
            java.lang.String r0 = r16.f()     // Catch:{ SDKException -> 0x0052 }
            boolean r10 = r15.contains(r5)     // Catch:{ SDKException -> 0x0052 }
            if (r10 == 0) goto L_0x0042
            boolean r10 = r0.startsWith(r5)     // Catch:{ SDKException -> 0x0052 }
            if (r10 == 0) goto L_0x0042
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ SDKException -> 0x0052 }
            r10.<init>()     // Catch:{ SDKException -> 0x0052 }
            r10.append(r15)     // Catch:{ SDKException -> 0x0052 }
            java.lang.String r11 = "&"
            r10.append(r11)     // Catch:{ SDKException -> 0x0052 }
            java.lang.String r0 = r0.substring(r7)     // Catch:{ SDKException -> 0x0052 }
            r10.append(r0)     // Catch:{ SDKException -> 0x0052 }
            java.lang.String r0 = r10.toString()     // Catch:{ SDKException -> 0x0052 }
            goto L_0x005e
        L_0x0042:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ SDKException -> 0x0052 }
            r10.<init>()     // Catch:{ SDKException -> 0x0052 }
            r10.append(r15)     // Catch:{ SDKException -> 0x0052 }
            r10.append(r0)     // Catch:{ SDKException -> 0x0052 }
            java.lang.String r0 = r10.toString()     // Catch:{ SDKException -> 0x0052 }
            goto L_0x005e
        L_0x0052:
            r0 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r2 = new com.startapp.sdk.adsbase.infoevents.e
            r2.<init>((java.lang.Throwable) r0)
            android.content.Context r0 = r1.f6339a
            r2.a((android.content.Context) r0)
            return r9
        L_0x005e:
            r10 = r0
            com.startapp.sdk.adsbase.e.a r0 = r1.d
            com.startapp.sdk.adsbase.e.c r11 = r0.a()
            android.content.Context r0 = r1.f6339a     // Catch:{ SDKException -> 0x007f }
            java.lang.String r12 = "User-Agent"
            java.lang.String r13 = "-1"
            java.lang.String r0 = com.startapp.sdk.adsbase.j.a((android.content.Context) r0, (java.lang.String) r12, (java.lang.String) r13)     // Catch:{ SDKException -> 0x007f }
            com.startapp.sdk.adsbase.d.c r12 = r14.a()     // Catch:{ SDKException -> 0x007f }
            boolean r12 = r12.a()     // Catch:{ SDKException -> 0x007f }
            com.startapp.common.b.e$a r0 = com.startapp.common.b.e.a(r10, r6, r0, r12)     // Catch:{ SDKException -> 0x007f }
            r11.a(r4, r10, r9)     // Catch:{ SDKException -> 0x007f }
            return r0
        L_0x007f:
            r0 = move-exception
            r11.a(r4, r10, r0)
            boolean r11 = r0.c()
            if (r11 == 0) goto L_0x00cd
            r11 = r18
            if (r8 >= r11) goto L_0x00cd
            int r12 = r0.b()
            if (r12 == 0) goto L_0x00ae
            com.startapp.sdk.adsbase.d.c r12 = r14.a()
            java.util.Set r12 = r12.c()
            if (r12 == 0) goto L_0x00ae
            int r13 = r0.b()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            boolean r12 = r12.contains(r13)
            if (r12 != 0) goto L_0x00ac
            goto L_0x00ae
        L_0x00ac:
            r12 = 0
            goto L_0x00af
        L_0x00ae:
            r12 = 1
        L_0x00af:
            if (r12 == 0) goto L_0x00cd
            int r8 = r8 + 1
            r12 = 0
            int r0 = (r19 > r12 ? 1 : (r19 == r12 ? 0 : -1))
            if (r0 <= 0) goto L_0x00ca
            java.lang.Thread.sleep(r19)     // Catch:{ InterruptedException -> 0x00bd }
            goto L_0x00ca
        L_0x00bd:
            r0 = move-exception
            r2 = r0
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r2)
            android.content.Context r2 = r1.f6339a
            r0.a((android.content.Context) r2)
            return r9
        L_0x00ca:
            r0 = r10
            goto L_0x000f
        L_0x00cd:
            r2 = r17
            r14.a(r2, r0)
            return r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.d.b.a(java.lang.String, com.startapp.sdk.adsbase.c, com.startapp.sdk.adsbase.j.q, int, long):com.startapp.common.b.e$a");
    }

    private Map<String, String> b() {
        HashMap hashMap = new HashMap();
        if (!a().b()) {
            String str = null;
            try {
                str = URLEncoder.encode(this.b.b().a(), "UTF-8");
            } catch (Throwable th) {
                new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6339a);
            }
            hashMap.put("device-id", str);
        }
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, ((com.startapp.sdk.d.b.a) this.c.c()).c());
        return hashMap;
    }

    private void a(q<String> qVar, Throwable th) {
        if (qVar != null) {
            try {
                qVar.a(th.getMessage());
            } catch (Throwable th2) {
                new com.startapp.sdk.adsbase.infoevents.e(th2).a(this.f6339a);
            }
        }
    }
}
