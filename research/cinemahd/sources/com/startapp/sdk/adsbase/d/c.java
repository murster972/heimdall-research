package com.startapp.sdk.adsbase.d;

import java.util.Set;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Set<Integer> f6340a = null;
    public static final c b = new c(false, false, (Set<Integer>) null);
    private final boolean c;
    private final boolean d;
    private final Set<Integer> e;

    public c(boolean z, boolean z2, Set<Integer> set) {
        this.c = z;
        this.d = z2;
        this.e = set;
    }

    public final boolean a() {
        return this.c;
    }

    public final boolean b() {
        return this.d;
    }

    public final Set<Integer> c() {
        return this.e;
    }
}
