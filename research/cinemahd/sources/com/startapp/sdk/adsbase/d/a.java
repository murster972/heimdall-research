package com.startapp.sdk.adsbase.d;

import com.startapp.common.b.e;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.j.q;
import com.startapp.sdk.adsbase.j.u;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    final String f6338a;
    c b;
    q<String> c;
    int d = 3;
    long e = 0;
    private final b f;

    a(b bVar, String str) {
        this.f = bVar;
        this.f6338a = str;
    }

    public final a a(c cVar) {
        this.b = cVar;
        return this;
    }

    public final String b() {
        return this.f.b(this);
    }

    public final a a(q<String> qVar) {
        this.c = qVar;
        return this;
    }

    public final a a(int i) {
        this.d = i;
        return this;
    }

    public final a a(long j) {
        this.e = j;
        return this;
    }

    public final <T> T a(Class<T> cls) {
        e.a a2 = a();
        if (a2 == null) {
            return null;
        }
        try {
            return u.a(a2.a(), cls);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f.f6339a);
            return null;
        }
    }

    public final e.a a() {
        return this.f.a(this);
    }
}
