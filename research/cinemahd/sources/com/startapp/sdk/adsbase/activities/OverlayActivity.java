package com.startapp.sdk.adsbase.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import com.facebook.common.util.ByteConstants;
import com.startapp.sdk.ads.a.b;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.a;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.c.c;
import com.unity3d.services.ads.adunit.AdUnitActivity;
import java.io.Serializable;

public class OverlayActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private b f6269a;
    private boolean b;
    private int c;
    private boolean d;
    private Bundle e;
    private boolean f = false;
    private int g = -1;

    static {
        Class<OverlayActivity> cls = OverlayActivity.class;
    }

    private void a() {
        this.f6269a = b.a(this, getIntent(), AdPreferences.Placement.a(getIntent().getIntExtra("placement", 0)));
        if (this.f6269a == null) {
            finish();
        }
    }

    public void finish() {
        b bVar = this.f6269a;
        if (bVar != null) {
            bVar.q();
        }
        super.finish();
    }

    public void onBackPressed() {
        if (!this.f6269a.r()) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.b) {
            a();
            this.f6269a.a(this.e);
            this.f6269a.u();
            this.b = false;
        }
        this.f6269a.v();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        boolean z = false;
        overridePendingTransition(0, 0);
        super.onCreate(bundle);
        int intExtra = getIntent().getIntExtra("placement", -1);
        Serializable serializableExtra = getIntent().getSerializableExtra("ad");
        if (intExtra >= 0 && (serializableExtra instanceof Ad)) {
            c.a(getApplicationContext()).h().a(AdPreferences.Placement.a(intExtra), ((Ad) serializableExtra).getAdId());
        }
        boolean booleanExtra = getIntent().getBooleanExtra("videoAd", false);
        requestWindowFeature(1);
        if (getIntent().getBooleanExtra("fullscreen", false) || booleanExtra) {
            getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
        }
        this.d = getIntent().getBooleanExtra("activityShouldLockOrientation", true);
        if (bundle == null && !booleanExtra) {
            com.startapp.common.b.a((Context) this).a(new Intent("com.startapp.android.ShowDisplayBroadcastListener"));
        }
        if (bundle != null) {
            this.g = bundle.getInt("activityLockedOrientation", -1);
            this.d = bundle.getBoolean("activityShouldLockOrientation", true);
        }
        this.c = getIntent().getIntExtra(AdUnitActivity.EXTRA_ORIENTATION, getResources().getConfiguration().orientation);
        if (getResources().getConfiguration().orientation != this.c) {
            z = true;
        }
        this.b = z;
        if (!this.b) {
            a();
            this.f6269a.a(bundle);
            return;
        }
        this.e = bundle;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        if (!this.b) {
            this.f6269a.w();
            this.f6269a = null;
            u.a((Activity) this, false);
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        b bVar = this.f6269a;
        if (bVar == null || bVar.a(i, keyEvent)) {
            return true;
        }
        return super.onKeyDown(i, keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        if (!this.b) {
            this.f6269a.s();
            a.a((Context) this);
        }
        overridePendingTransition(0, 0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        int i = this.g;
        if (i == -1) {
            this.g = u.a((Activity) this, this.c, this.d);
        } else {
            com.startapp.common.b.b.a((Activity) this, i);
        }
        if (!this.b) {
            this.f6269a.u();
        }
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (!this.b) {
            this.f6269a.b(bundle);
            bundle.putInt("activityLockedOrientation", this.g);
            bundle.putBoolean("activityShouldLockOrientation", this.d);
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        if (!this.b) {
            this.f6269a.t();
        }
    }
}
