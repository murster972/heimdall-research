package com.startapp.sdk.adsbase.adrules;

import com.startapp.sdk.adsbase.model.AdPreferences;

public final class a implements Comparable<a> {

    /* renamed from: a  reason: collision with root package name */
    private long f6297a = System.currentTimeMillis();
    private AdPreferences.Placement b;
    private String c;

    public a(AdPreferences.Placement placement, String str) {
        this.b = placement;
        this.c = str == null ? "" : str;
    }

    public final AdPreferences.Placement a() {
        return this.b;
    }

    public final String b() {
        return this.c;
    }

    public final /* bridge */ /* synthetic */ int compareTo(Object obj) {
        int i = ((this.f6297a - ((a) obj).f6297a) > 0 ? 1 : ((this.f6297a - ((a) obj).f6297a) == 0 ? 0 : -1));
        if (i > 0) {
            return 1;
        }
        return i == 0 ? 0 : -1;
    }

    public final String toString() {
        return "AdDisplayEvent [displayTime=" + this.f6297a + ", placement=" + this.b + ", adTag=" + this.c + "]";
    }
}
