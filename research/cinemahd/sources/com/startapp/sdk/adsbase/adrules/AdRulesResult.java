package com.startapp.sdk.adsbase.adrules;

import java.io.Serializable;

public class AdRulesResult implements Serializable {
    private static final long serialVersionUID = 1;
    private String reason;
    private boolean shouldDisplayAd;

    public AdRulesResult(boolean z, String str) {
        this.shouldDisplayAd = z;
        this.reason = str;
    }

    public final boolean a() {
        return this.shouldDisplayAd;
    }

    public final String b() {
        String str = this.reason;
        return str != null ? str.split(" ")[0] : "";
    }

    public AdRulesResult() {
        this(true, "");
    }
}
