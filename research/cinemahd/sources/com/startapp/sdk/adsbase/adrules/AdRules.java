package com.startapp.sdk.adsbase.adrules;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AdRules implements Serializable {
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    private transient Set<Class<? extends AdRule>> f6295a = new HashSet();
    private boolean applyOnBannerRefresh = true;
    @d(b = HashMap.class, c = ArrayList.class, d = AdPreferences.Placement.class, e = AdRule.class)
    private Map<AdPreferences.Placement, List<AdRule>> placements = new HashMap();
    @d(b = ArrayList.class, c = AdRule.class)
    private List<AdRule> session = new ArrayList();
    @d(b = HashMap.class, c = ArrayList.class, e = AdRule.class)
    private Map<String, List<AdRule>> tags = new HashMap();

    public final boolean a() {
        return this.applyOnBannerRefresh;
    }

    public final void b() {
        this.f6295a = new HashSet();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AdRules.class == obj.getClass()) {
            AdRules adRules = (AdRules) obj;
            return this.applyOnBannerRefresh == adRules.applyOnBannerRefresh && u.b(this.session, adRules.session) && u.b(this.placements, adRules.placements) && u.b(this.tags, adRules.tags);
        }
    }

    public int hashCode() {
        return u.a(this.session, this.placements, this.tags, Boolean.valueOf(this.applyOnBannerRefresh));
    }

    public final synchronized AdRulesResult a(AdPreferences.Placement placement, String str) {
        AdRulesResult a2;
        this.f6295a.clear();
        b.a().a(str);
        a2 = a(this.tags.get(str), AdRuleLevel.TAG);
        if (a2.a()) {
            b.a().a(placement);
            a2 = a(this.placements.get(placement), AdRuleLevel.PLACEMENT);
            if (a2.a()) {
                List<AdRule> list = this.session;
                b.a();
                a2 = a(list, AdRuleLevel.SESSION);
            }
        }
        return a2;
    }

    private AdRulesResult a(List<AdRule> list, AdRuleLevel adRuleLevel) {
        if (list == null) {
            return new AdRulesResult();
        }
        for (AdRule next : list) {
            if (!this.f6295a.contains(next.getClass())) {
                if (!next.a()) {
                    return new AdRulesResult(false, next.getClass().getSimpleName() + "_" + adRuleLevel);
                }
                this.f6295a.add(next.getClass());
            }
        }
        return new AdRulesResult();
    }
}
