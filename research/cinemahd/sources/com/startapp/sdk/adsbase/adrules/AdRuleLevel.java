package com.startapp.sdk.adsbase.adrules;

public enum AdRuleLevel {
    TAG,
    PLACEMENT,
    SESSION
}
