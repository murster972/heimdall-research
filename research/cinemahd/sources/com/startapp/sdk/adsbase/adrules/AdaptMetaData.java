package com.startapp.sdk.adsbase.adrules;

import com.startapp.common.parser.d;
import java.io.Serializable;

public class AdaptMetaData implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static transient AdaptMetaData f6296a = new AdaptMetaData();
    @d(a = true)
    private AdRules adRules = new AdRules();
    private String adaptMetaDataUpdateVersion = "4.6.3";

    private AdaptMetaData() {
    }

    public static AdaptMetaData a() {
        return f6296a;
    }

    public final AdRules b() {
        return this.adRules;
    }
}
