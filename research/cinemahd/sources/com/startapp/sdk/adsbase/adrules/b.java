package com.startapp.sdk.adsbase.adrules;

import com.startapp.sdk.adsbase.model.AdPreferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private static b f6298a = new b();
    private List<a> b = new ArrayList();
    private Map<AdPreferences.Placement, List<a>> c = new HashMap();
    private Map<String, List<a>> d = new HashMap();

    public static b a() {
        return f6298a;
    }

    public final void b() {
        this.b.clear();
        this.c.clear();
        this.d.clear();
    }

    public final int c() {
        return this.b.size();
    }

    public final List<a> a(AdPreferences.Placement placement) {
        return this.c.get(placement);
    }

    public final List<a> a(String str) {
        return this.d.get(str);
    }

    public final synchronized void a(a aVar) {
        this.b.add(0, aVar);
        List list = this.c.get(aVar.a());
        if (list == null) {
            list = new ArrayList();
            this.c.put(aVar.a(), list);
        }
        list.add(0, aVar);
        List list2 = this.d.get(aVar.b());
        if (list2 == null) {
            list2 = new ArrayList();
            this.d.put(aVar.b(), list2);
        }
        list2.add(0, aVar);
    }
}
