package com.startapp.sdk.adsbase;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import com.startapp.sdk.adsbase.StartAppInitProvider;
import com.startapp.sdk.adsbase.infoevents.e;

public class g implements StartAppInitProvider.a {

    /* renamed from: a  reason: collision with root package name */
    private static String f6358a = "g";
    private static String b = "com.startapp.sdk.APPLICATION_ID";
    private static String c = "com.startapp.sdk.RETURN_ADS_ENABLED";
    private String d;
    private boolean e = true;

    g(Context context) {
        PackageManager packageManager = context.getPackageManager();
        if (packageManager != null) {
            try {
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(context.getPackageName(), 128);
                if (applicationInfo.metaData != null) {
                    if (applicationInfo.metaData.containsKey(b)) {
                        int i = applicationInfo.metaData.getInt(b);
                        this.d = i != 0 ? String.valueOf(i) : applicationInfo.metaData.getString(b);
                        String str = f6358a;
                        Log.i(str, "appId is " + this.d);
                        if (applicationInfo.metaData.containsKey(c)) {
                            this.e = applicationInfo.metaData.getBoolean(c);
                            String str2 = f6358a;
                            Log.i(str2, "returnAds enabled: " + this.e);
                            return;
                        }
                        return;
                    }
                    Log.i(f6358a, "appId hasn't been provided in the Manifest");
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
    }

    public final String a() {
        return this.d;
    }

    public final boolean b() {
        return this.e;
    }
}
