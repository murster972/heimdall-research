package com.startapp.sdk.adsbase.c;

import android.content.Context;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

class c implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private static final Comparator<File> f6307a = new Comparator<File>() {
        public final /* synthetic */ int compare(Object obj, Object obj2) {
            return ((File) obj2).getName().compareTo(((File) obj).getName());
        }
    };
    private final Context b;
    private final File[] c;

    static {
        Class<c> cls = c.class;
    }

    public c(Context context, File[] fileArr) {
        this.b = context;
        this.c = fileArr;
    }

    public void run() {
        String str;
        String str2;
        String str3;
        Arrays.sort(this.c, f6307a);
        e eVar = null;
        e eVar2 = null;
        int i = 0;
        for (File file : this.c) {
            if (file != null) {
                if (i >= 5) {
                    u.d(file);
                } else {
                    List<String> c2 = u.c(file);
                    if (c2 == null || c2.size() < 2) {
                        u.d(file);
                    } else {
                        if (c2.size() < 3) {
                            str3 = c2.get(1);
                            str = c2.get(0);
                            str2 = "4.6.3.0";
                        } else {
                            str2 = c2.get(0);
                            str = c2.get(1);
                            str3 = c2.get(2);
                        }
                        if (!(str2 == null || str == null || str3 == null)) {
                            e eVar3 = new e(InfoEventCategory.EXCEPTION_FATAL);
                            eVar3.a(str2);
                            eVar3.f(str);
                            eVar3.g(str3);
                            eVar3.o(file.getAbsolutePath());
                            if (eVar == null) {
                                eVar = eVar3;
                            }
                            if (eVar2 != null) {
                                eVar2.a(eVar3);
                            }
                            i++;
                            eVar2 = eVar3;
                        }
                    }
                }
            }
        }
        if (eVar != null) {
            eVar.a(this.b, new a());
        }
    }
}
