package com.startapp.sdk.adsbase.c;

import android.content.Context;
import android.os.SystemClock;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.adsbase.j.s;
import com.startapp.sdk.adsbase.j.u;
import java.io.Closeable;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.lang.Thread;

public class b implements Thread.UncaughtExceptionHandler {

    /* renamed from: a  reason: collision with root package name */
    private final File f6306a;
    private final Thread.UncaughtExceptionHandler b;

    static {
        Class<b> cls = b.class;
    }

    public b(Context context, Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
        this.b = uncaughtExceptionHandler;
        this.f6306a = new File(context.getCacheDir(), "StartApp-767b8b9bfc82ce39");
        this.f6306a.mkdirs();
        File[] listFiles = this.f6306a.listFiles();
        if (listFiles != null && listFiles.length > 0) {
            ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new c(context, listFiles));
        }
    }

    public static void a(Throwable th, PrintWriter printWriter) {
        String className;
        s sVar = new s(th);
        while (sVar.hasNext()) {
            Throwable b2 = sVar.next();
            if (sVar.a()) {
                printWriter.println('-');
            }
            printWriter.println(b2.toString().trim());
            StackTraceElement[] stackTrace = b2.getStackTrace();
            if (stackTrace != null) {
                int length = stackTrace.length;
                StackTraceElement stackTraceElement = null;
                int i = 0;
                int i2 = 0;
                boolean z = false;
                while (i < length) {
                    StackTraceElement stackTraceElement2 = stackTrace[i];
                    if (!(stackTraceElement2 == null || (className = stackTraceElement2.getClassName()) == null)) {
                        boolean z2 = i < 3;
                        boolean startsWith = className.startsWith("com.startapp.");
                        if (z2 || startsWith || z) {
                            if (i2 > 0) {
                                printWriter.print(' ');
                                printWriter.println(i2);
                                i2 = 0;
                            }
                            if (stackTraceElement != null) {
                                printWriter.print(' ');
                                printWriter.print(stackTraceElement.getClassName());
                                printWriter.print('.');
                                printWriter.print(stackTraceElement.getMethodName());
                                printWriter.println("()");
                                stackTraceElement = null;
                            }
                            printWriter.print(' ');
                            printWriter.print(stackTraceElement2.getClassName());
                            printWriter.print('.');
                            printWriter.print(stackTraceElement2.getMethodName());
                            printWriter.println("()");
                        } else {
                            if (stackTraceElement != null) {
                                i2++;
                            }
                            stackTraceElement = stackTraceElement2;
                        }
                        z = startsWith;
                    }
                    i++;
                }
                if (stackTraceElement != null) {
                    i2++;
                }
                if (i2 > 0) {
                    printWriter.print(' ');
                    printWriter.println(i2);
                }
            }
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        StackTraceElement a2 = a(th);
        if (a2 != null) {
            OutputStream outputStream = null;
            try {
                File file = this.f6306a;
                FileOutputStream fileOutputStream = new FileOutputStream(new File(file, System.currentTimeMillis() + "-" + SystemClock.uptimeMillis()));
                try {
                    PrintStream printStream = new PrintStream(fileOutputStream, true);
                    printStream.println("4.6.3");
                    printStream.println(u.a(a2));
                    outputStream = u.a((OutputStream) fileOutputStream);
                    PrintWriter printWriter = new PrintWriter(outputStream);
                    a(th, printWriter);
                    printWriter.close();
                } catch (Throwable unused) {
                    outputStream = fileOutputStream;
                }
            } catch (Throwable unused2) {
            }
            u.a((Closeable) outputStream);
        }
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.b;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
        }
    }

    public static StackTraceElement a(Throwable th) {
        String className;
        for (Throwable th2 = th; th2 != null; th2 = th2.getCause()) {
            StackTraceElement[] stackTrace = th.getStackTrace();
            if (stackTrace != null) {
                for (StackTraceElement stackTraceElement : stackTrace) {
                    if (stackTraceElement != null && (className = stackTraceElement.getClassName()) != null && className.startsWith("com.startapp.")) {
                        return stackTraceElement;
                    }
                }
                continue;
            }
        }
        return null;
    }
}
