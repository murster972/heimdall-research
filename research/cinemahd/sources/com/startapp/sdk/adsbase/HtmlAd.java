package com.startapp.sdk.adsbase;

import android.content.Context;
import android.text.TextUtils;
import com.iab.omid.library.startapp.b;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.adsbase.adinformation.AdInformationPositions;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.cache.a;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public abstract class HtmlAd extends Ad {
    private static String b = null;
    private static final long serialVersionUID = 1;
    private List<AppPresenceDetails> apps;
    private String[] closingUrl = {""};
    private Boolean consentApc;
    private Long consentTimeStamp;
    private Integer consentType;
    private Long delayImpressionInSeconds;
    private int height;
    private String htmlUuid = "";
    public boolean[] inAppBrowserEnabled = {true};
    private boolean isMraidAd = false;
    private int orientation = 0;
    private String[] packageNames = {""};
    private int rewardDuration = 0;
    private boolean rewardedHideTimer = false;
    private Boolean[] sendRedirectHops = null;
    public boolean[] smartRedirect = {false};
    private String[] trackingClickUrls = {""};
    public String[] trackingUrls = {""};
    private int width;

    static {
        Class<HtmlAd> cls = HtmlAd.class;
    }

    public HtmlAd(Context context, AdPreferences.Placement placement) {
        super(context, placement);
        if (b == null) {
            b = u.f(getContext());
        }
    }

    private String d(String str) {
        try {
            return b.a(com.startapp.sdk.omsdk.b.a(), str);
        } catch (Throwable th) {
            new e(th).a(this.f6243a);
            return str;
        }
    }

    /* access modifiers changed from: protected */
    public final void a(SplashConfig.Orientation orientation2) {
        this.orientation = 0;
        u.b();
        if (orientation2 == null) {
            return;
        }
        if (orientation2.equals(SplashConfig.Orientation.PORTRAIT)) {
            this.orientation = 1;
        } else if (orientation2.equals(SplashConfig.Orientation.LANDSCAPE)) {
            this.orientation = 2;
        }
    }

    public final void b(int i) {
        this.width = i;
    }

    public final void c(int i) {
        this.height = i;
    }

    public final boolean e(int i) {
        boolean[] zArr = this.inAppBrowserEnabled;
        if (zArr == null || i < 0 || i >= zArr.length) {
            return true;
        }
        return zArr[i];
    }

    public final Boolean f(int i) {
        Boolean[] boolArr = this.sendRedirectHops;
        if (boolArr == null || i < 0 || i >= boolArr.length) {
            return null;
        }
        return boolArr[i];
    }

    public String getAdId() {
        return u.a(j(), "@adId@", "@adId@");
    }

    public Boolean getConsentApc() {
        return this.consentApc;
    }

    public Long getConsentTimestamp() {
        return this.consentTimeStamp;
    }

    public Integer getConsentType() {
        return this.consentType;
    }

    public final String j() {
        return a.a().a(this.htmlUuid);
    }

    public final String k() {
        return this.htmlUuid;
    }

    public final int l() {
        return this.width;
    }

    public final String[] m() {
        return this.closingUrl;
    }

    public final int n() {
        return this.height;
    }

    public final int o() {
        return this.rewardDuration;
    }

    public final boolean p() {
        return this.rewardedHideTimer;
    }

    public final String[] q() {
        return this.trackingClickUrls;
    }

    public final int r() {
        return this.orientation;
    }

    public final String[] s() {
        return this.packageNames;
    }

    public void setConsentApc(Boolean bool) {
        this.consentApc = bool;
    }

    public void setConsentTimestamp(Long l) {
        this.consentTimeStamp = l;
    }

    public void setConsentType(Integer num) {
        this.consentType = num;
    }

    public final Long t() {
        return this.delayImpressionInSeconds;
    }

    public final Boolean[] u() {
        return this.sendRedirectHops;
    }

    public final boolean v() {
        return this.isMraidAd;
    }

    public void b(String str) {
        if (com.startapp.sdk.adsbase.mraid.c.a.b(str)) {
            str = com.startapp.sdk.adsbase.mraid.c.a.a(str);
            this.isMraidAd = true;
        }
        if (MetaData.G().S()) {
            str = d(str);
        }
        u.b();
        this.htmlUuid = a.a().a(str, UUID.randomUUID().toString());
        String a2 = u.a(str, "@smartRedirect@", "@smartRedirect@");
        if (a2 != null) {
            String[] split = a2.split(",");
            this.smartRedirect = new boolean[split.length];
            for (int i = 0; i < split.length; i++) {
                if (split[i].compareTo("true") == 0) {
                    this.smartRedirect[i] = true;
                } else {
                    this.smartRedirect[i] = false;
                }
            }
        }
        String a3 = u.a(str, "@trackingClickUrl@", "@trackingClickUrl@");
        if (a3 != null) {
            this.trackingClickUrls = a3.split(",");
        }
        String a4 = u.a(str, "@closeUrl@", "@closeUrl@");
        if (a4 != null) {
            this.closingUrl = a4.split(",");
        }
        String a5 = u.a(str, "@tracking@", "@tracking@");
        if (a5 != null) {
            this.trackingUrls = a5.split(",");
        }
        String a6 = u.a(str, "@packageName@", "@packageName@");
        if (a6 != null) {
            this.packageNames = a6.split(",");
        }
        String a7 = u.a(str, "@startappBrowserEnabled@", "@startappBrowserEnabled@");
        if (a7 != null) {
            String[] split2 = a7.split(",");
            this.inAppBrowserEnabled = new boolean[split2.length];
            for (int i2 = 0; i2 < split2.length; i2++) {
                if (split2[i2].compareTo("false") == 0) {
                    this.inAppBrowserEnabled[i2] = false;
                } else {
                    this.inAppBrowserEnabled[i2] = true;
                }
            }
        }
        String a8 = u.a(str, "@orientation@", "@orientation@");
        if (a8 != null) {
            u.b();
            a(SplashConfig.Orientation.getByName(a8));
        }
        String a9 = u.a(str, "@adInfoEnable@", "@adInfoEnable@");
        if (a9 != null) {
            getAdInfoOverride().a(Boolean.parseBoolean(a9));
        }
        String a10 = u.a(str, "@adInfoPosition@", "@adInfoPosition@");
        if (a10 != null) {
            getAdInfoOverride().a(AdInformationPositions.Position.getByName(a10));
        }
        String a11 = u.a(str, "@ttl@", "@ttl@");
        if (a11 != null) {
            c(a11);
        }
        String a12 = u.a(str, "@belowMinCPM@", "@belowMinCPM@");
        if (a12 != null) {
            if (Arrays.asList(a12.split(",")).contains("false")) {
                this.belowMinCPM = false;
            } else {
                this.belowMinCPM = true;
            }
        }
        String a13 = u.a(str, "@delayImpressionInSeconds@", "@delayImpressionInSeconds@");
        if (!(a13 == null || a13 == null || a13.equals(""))) {
            try {
                this.delayImpressionInSeconds = Long.valueOf(Long.parseLong(a13));
            } catch (NumberFormatException unused) {
            }
        }
        String a14 = u.a(str, "@rewardDuration@", "@rewardDuration@");
        if (a14 != null) {
            try {
                this.rewardDuration = Integer.parseInt(a14);
            } catch (Throwable th) {
                new e(th).a(this.f6243a);
            }
        }
        String a15 = u.a(str, "@rewardedHideTimer@", "@rewardedHideTimer@");
        if (a15 != null) {
            try {
                this.rewardedHideTimer = Boolean.parseBoolean(a15);
            } catch (Throwable th2) {
                new e(th2).a(this.f6243a);
            }
        }
        String a16 = u.a(str, "@sendRedirectHops@", "@sendRedirectHops@");
        if (!(a16 == null || a16 == null || a16.equals(""))) {
            String[] split3 = a16.split(",");
            this.sendRedirectHops = new Boolean[split3.length];
            for (int i3 = 0; i3 < split3.length; i3++) {
                if (split3[i3].compareTo("true") == 0) {
                    this.sendRedirectHops[i3] = Boolean.TRUE;
                } else if (split3[i3].compareTo("false") == 0) {
                    this.sendRedirectHops[i3] = Boolean.FALSE;
                } else {
                    this.sendRedirectHops[i3] = null;
                }
            }
        }
        String a17 = u.a(str, "@ct@", "@ct@");
        String a18 = u.a(str, "@tsc@", "@tsc@");
        String a19 = u.a(str, "@apc@", "@apc@");
        try {
            if (!TextUtils.isEmpty(a17)) {
                this.consentType = Integer.valueOf(Integer.parseInt(a17));
            }
            if (!TextUtils.isEmpty(a18)) {
                this.consentTimeStamp = Long.valueOf(Long.parseLong(a18));
            }
            if (!TextUtils.isEmpty(a19)) {
                this.consentApc = Boolean.valueOf(Boolean.parseBoolean(a19));
            }
        } catch (Throwable th3) {
            new e(th3).a(this.f6243a);
        }
        int length = this.smartRedirect.length;
        String[] strArr = this.trackingUrls;
        if (length < strArr.length) {
            boolean[] zArr = new boolean[strArr.length];
            int i4 = 0;
            while (true) {
                boolean[] zArr2 = this.smartRedirect;
                if (i4 >= zArr2.length) {
                    break;
                }
                zArr[i4] = zArr2[i4];
                i4++;
            }
            while (i4 < this.trackingUrls.length) {
                zArr[i4] = false;
                i4++;
            }
            this.smartRedirect = zArr;
        }
    }

    public final void c(String str) {
        Long l = null;
        for (String str2 : str.split(",")) {
            if (!str2.equals("")) {
                try {
                    long parseLong = Long.parseLong(str2);
                    if (parseLong > 0 && (l == null || parseLong < l.longValue())) {
                        l = Long.valueOf(parseLong);
                    }
                } catch (NumberFormatException unused) {
                }
            }
        }
        if (l != null) {
            this.adCacheTtl = Long.valueOf(TimeUnit.SECONDS.toMillis(l.longValue()));
        }
    }

    public final boolean d(int i) {
        if (i < 0) {
            return false;
        }
        boolean[] zArr = this.smartRedirect;
        if (i >= zArr.length) {
            return false;
        }
        return zArr[i];
    }

    public final void a(List<AppPresenceDetails> list) {
        this.apps = list;
    }
}
