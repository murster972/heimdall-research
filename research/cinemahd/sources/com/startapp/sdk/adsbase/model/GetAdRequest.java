package com.startapp.sdk.adsbase.model;

import android.content.Context;
import android.util.Pair;
import com.startapp.common.SDKException;
import com.startapp.common.b.a;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.SDKAdPreferences;
import com.startapp.sdk.adsbase.adrules.b;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.j.m;
import com.startapp.sdk.adsbase.j.p;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.k;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.vungle.warren.model.Advertisement;
import java.util.Set;

public class GetAdRequest extends c {
    private long A = (System.currentTimeMillis() - p.d().b());
    private int B;
    private String C;
    private String D;
    private String E;
    private boolean F;
    private Boolean G;
    private Boolean H;
    private String I = null;
    private String J = null;
    private Ad.AdType K = null;
    protected String b;
    private AdPreferences.Placement c;
    private boolean d;
    private Integer e;
    private Long f;
    private Boolean g;
    private SDKAdPreferences.Gender h;
    private String i;
    private String j;
    private int k = 1;
    private boolean l = true;
    private Boolean m;
    private boolean n = AdsCommonMetaData.a().E();
    private Double o;
    private String p;
    private Integer q;
    private boolean r = true;
    private int s = 0;
    private Set<String> t = null;
    private Set<String> u = null;
    private Set<String> v = null;
    private Set<String> w = null;
    private Set<String> x = null;
    private Pair<String, String> y;
    private boolean z = true;

    protected enum VideoRequestMode {
        INTERSTITIAL,
        REWARDED
    }

    protected enum VideoRequestType {
        ENABLED,
        DISABLED,
        FORCED,
        FORCED_NONVAST
    }

    public GetAdRequest() {
        super(4);
        u.a();
        this.B = b.a().c();
        this.C = MetaData.G().C();
    }

    public void a(Context context) {
        this.b = com.startapp.sdk.c.c.a(context).h().a(this.c);
    }

    public final void b(boolean z2) {
        this.F = z2;
    }

    public final void c(String str) {
        this.D = str;
    }

    public final void d(String str) {
        this.E = str;
    }

    public final void e(int i2) {
        this.k = i2;
    }

    public final void f(int i2) {
        this.s = i2;
    }

    public final AdPreferences.Placement g() {
        return this.c;
    }

    public final void h() {
        this.n = true;
    }

    public final boolean i() {
        Set<String> set = this.x;
        return set != null && set.size() > 0;
    }

    public final boolean j() {
        Ad.AdType adType = this.K;
        return adType == Ad.AdType.VIDEO || adType == Ad.AdType.REWARDED_VIDEO;
    }

    public final Ad.AdType k() {
        return this.K;
    }

    public String toString() {
        return super.toString();
    }

    public final void b(Set<String> set) {
        this.x = set;
    }

    public final void c(boolean z2) {
        this.z = z2;
    }

    public final void b(int i2) {
        this.f6305a = Integer.valueOf(i2);
    }

    public final void a(Set<String> set) {
        this.v = set;
    }

    public final void a(Integer num, Long l2, Boolean bool) {
        this.e = num;
        this.f = l2;
        this.g = bool;
    }

    public void a(Context context, AdPreferences adPreferences, AdPreferences.Placement placement, Pair<String, String> pair) {
        this.c = placement;
        this.y = pair;
        this.G = adPreferences.getAi();
        this.H = adPreferences.getAs();
        this.h = adPreferences.getGender(context);
        this.i = adPreferences.getKeywords();
        this.d = adPreferences.isTestMode();
        this.t = adPreferences.getCategories();
        this.u = adPreferences.getCategoriesExclude();
        this.l = adPreferences.b();
        this.q = adPreferences.a();
        this.m = Boolean.valueOf(com.startapp.common.b.b.b(context));
        this.o = adPreferences.getMinCpm();
        this.p = adPreferences.getAdTag();
        this.r = !MetaData.b(context);
        this.I = adPreferences.country;
        this.J = adPreferences.advertiserId;
        this.j = adPreferences.template;
        this.K = adPreferences.type;
        this.w = adPreferences.packageInclude;
    }

    /* access modifiers changed from: protected */
    public void a(m mVar) throws SDKException {
        super.a(mVar);
        mVar.a("placement", this.c.name(), true);
        mVar.a("testMode", Boolean.toString(this.d), false);
        mVar.a("gender", this.h, false);
        mVar.a("keywords", this.i, false);
        mVar.a(Advertisement.KEY_TEMPLATE, this.j, false);
        mVar.a("adsNumber", Integer.toString(this.k), false);
        mVar.a("category", this.t);
        mVar.a("categoryExclude", this.u);
        mVar.a("packageExclude", this.v);
        mVar.a("campaignExclude", this.x);
        mVar.a("offset", Integer.toString(this.s), false);
        mVar.a("ai", this.G, false);
        mVar.a("as", this.H, false);
        mVar.a("minCPM", u.a(this.o), false);
        mVar.a("adTag", this.p, false);
        mVar.a("previousAdId", this.b, false);
        mVar.a("twoClicks", Boolean.valueOf(!this.n), false);
        mVar.a("engInclude", Boolean.toString(this.z), false);
        Ad.AdType adType = this.K;
        if (adType == Ad.AdType.INTERSTITIAL || adType == Ad.AdType.RICH_TEXT) {
            mVar.a("type", this.K, false);
        }
        mVar.a("timeSinceSessionStart", Long.valueOf(this.A), true);
        mVar.a("adsDisplayed", Integer.valueOf(this.B), true);
        mVar.a("profileId", this.C, false);
        mVar.a("hardwareAccelerated", Boolean.valueOf(this.l), false);
        mVar.a("autoLoadAmount", this.q, false);
        mVar.a("dts", this.m, false);
        mVar.a("downloadingMode", "CACHE", false);
        mVar.a("primaryImg", this.D, false);
        mVar.a("moreImg", this.E, false);
        mVar.a("contentAd", Boolean.toString(this.F), false);
        mVar.a("ct", this.e, false);
        mVar.a("tsc", this.f, false);
        mVar.a("apc", this.g, false);
        mVar.a("testAdsEnabled", k.a().o() ? Boolean.TRUE : null, false);
        String d2 = a.d();
        mVar.a(a.a(), d2, true);
        String c2 = a.c();
        mVar.a(c2, a.b(b() + this.c.name() + d() + c() + d2), true, false);
        String str = this.I;
        if (str != null) {
            mVar.a("country", str, false);
        }
        String str2 = this.J;
        if (str2 != null) {
            mVar.a("advertiserId", str2, false);
        }
        Set<String> set = this.w;
        if (set != null) {
            mVar.a("packageInclude", set);
        }
        mVar.a("defaultMetaData", Boolean.valueOf(this.r), true);
        Pair<String, String> pair = this.y;
        mVar.a((String) pair.first, pair.second, false);
    }
}
