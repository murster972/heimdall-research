package com.startapp.sdk.adsbase.j;

import com.startapp.common.SDKException;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public final class i extends m {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f6386a;

    public i() {
        this.f6386a = null;
        this.f6386a = new JSONObject();
    }

    public final void a(String str, Object obj, boolean z, boolean z2) throws SDKException {
        if (z && obj == null) {
            throw new SDKException("Required key: [" + str + "] is missing", (Throwable) null);
        } else if (obj != null && !obj.toString().equals("")) {
            try {
                this.f6386a.put(str, obj);
            } catch (JSONException e) {
                if (z) {
                    throw new SDKException("failed converting to json object value: [" + obj + "]", e);
                }
            }
        }
    }

    public final String toString() {
        return this.f6386a.toString();
    }

    public final void a(String str, Set<String> set) throws SDKException {
        if (set != null && set.size() > 0) {
            try {
                this.f6386a.put(str, new JSONArray(set));
            } catch (JSONException unused) {
            }
        }
    }
}
