package com.startapp.sdk.adsbase.j;

import android.content.Context;
import android.os.Environment;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

public final class n {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f6391a = true;

    public static void a(Context context, boolean z) {
        if (z) {
            f6391a = true;
            j.b(context, "copyDrawables", Boolean.TRUE);
        }
        if (f6391a) {
            boolean booleanValue = j.a(context, "copyDrawables", Boolean.TRUE).booleanValue();
            f6391a = booleanValue;
            if (booleanValue) {
                try {
                    String str = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0).sourceDir;
                    String a2 = a(context);
                    if (!a(context, str, "", "drawable-hdpi.zip") && !a(context, str, "assets/", "drawable-hdpi.zip") && !a(context, a2, "", "drawable-hdpi.zip")) {
                        a(context, a2, "assets/", "drawable-hdpi.zip");
                    }
                } catch (Exception e) {
                    new e((Throwable) e).a(context);
                }
            }
        }
    }

    private static String a(Context context) {
        String str = Environment.getExternalStorageDirectory() + "/Android/obb/" + context.getPackageName() + "/";
        String str2 = "main.1." + context.getPackageName() + ".obb";
        File file = new File(str);
        if (file.exists() && file.isDirectory()) {
            final Pattern compile = Pattern.compile("main[.][1-9][0-9]*[.]" + context.getPackageName() + "[.]obb");
            File[] listFiles = file.listFiles(new FileFilter() {
                public final boolean accept(File file) {
                    return compile.matcher(file.getName()).matches();
                }
            });
            if (listFiles.length > 0) {
                int i = 0;
                int i2 = 0;
                for (int i3 = 0; i3 < listFiles.length; i3++) {
                    try {
                        int parseInt = Integer.parseInt(listFiles[i3].getName().split("[.]")[1]);
                        if (parseInt > i2) {
                            i = i3;
                            i2 = parseInt;
                        }
                    } catch (Exception unused) {
                    }
                }
                str2 = listFiles[i].getName();
            }
        }
        return str + str2;
    }

    private static boolean a(Context context, String str, String str2, String str3) {
        if (!a(str, str2 + str3, context.getFilesDir().getPath() + "/" + str3)) {
            return false;
        }
        a(context, context.getFilesDir().getPath() + "/" + str3);
        StringBuilder sb = new StringBuilder();
        sb.append(str2);
        sb.append("drawable.zip");
        a(str, sb.toString(), context.getFilesDir().getPath() + "/drawable.zip");
        a(context, context.getFilesDir().getPath() + "/drawable.zip");
        j.b(context, "copyDrawables", Boolean.FALSE);
        return true;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0050, code lost:
        r1 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0056, code lost:
        r5 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0058, code lost:
        r5 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:?, code lost:
        r5.close();
        r1.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:?, code lost:
        r2.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x006d, code lost:
        return false;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:26:0x004b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:42:0x005f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:48:0x006a */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0056 A[ExcHandler: all (th java.lang.Throwable), PHI: r2 
      PHI: (r2v4 java.util.zip.ZipFile) = (r2v1 java.util.zip.ZipFile), (r2v1 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile), (r2v5 java.util.zip.ZipFile) binds: [B:42:0x005f, B:43:?, B:3:0x0007, B:15:0x002f, B:16:?, B:18:0x0036, B:24:0x0045, B:25:?] A[DONT_GENERATE, DONT_INLINE], Splitter:B:3:0x0007] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(java.lang.String r5, java.lang.String r6, java.lang.String r7) {
        /*
            r0 = 0
            r1 = 0
            java.util.zip.ZipFile r2 = new java.util.zip.ZipFile     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            r2.<init>(r5)     // Catch:{ IOException -> 0x005d, all -> 0x005a }
            java.util.Enumeration r5 = r2.entries()     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
        L_0x000b:
            boolean r3 = r5.hasMoreElements()     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            if (r3 == 0) goto L_0x0028
            java.lang.Object r3 = r5.nextElement()     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            java.util.zip.ZipEntry r3 = (java.util.zip.ZipEntry) r3     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            boolean r4 = r3.isDirectory()     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            if (r4 != 0) goto L_0x000b
            java.lang.String r4 = r3.getName()     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            boolean r4 = r4.equals(r6)     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            if (r4 == 0) goto L_0x000b
            goto L_0x0029
        L_0x0028:
            r3 = r1
        L_0x0029:
            if (r3 == 0) goto L_0x0052
            java.io.InputStream r5 = r2.getInputStream(r3)     // Catch:{ IOException -> 0x0058, all -> 0x0056 }
            java.io.FileOutputStream r6 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x005f, all -> 0x0056 }
            r6.<init>(r7)     // Catch:{ IOException -> 0x005f, all -> 0x0056 }
            r7 = 256(0x100, float:3.59E-43)
            byte[] r7 = new byte[r7]     // Catch:{ IOException -> 0x0050, all -> 0x0056 }
        L_0x0038:
            int r1 = r5.read(r7)     // Catch:{ IOException -> 0x0050, all -> 0x0056 }
            if (r1 <= 0) goto L_0x0042
            r6.write(r7, r0, r1)     // Catch:{ IOException -> 0x0050, all -> 0x0056 }
            goto L_0x0038
        L_0x0042:
            r6.flush()     // Catch:{ IOException -> 0x0050, all -> 0x0056 }
            r5.close()     // Catch:{ IOException -> 0x004b, all -> 0x0056 }
            r6.close()     // Catch:{ IOException -> 0x004b, all -> 0x0056 }
        L_0x004b:
            r2.close()     // Catch:{ Exception -> 0x004e }
        L_0x004e:
            r5 = 1
            return r5
        L_0x0050:
            r1 = r6
            goto L_0x005f
        L_0x0052:
            r2.close()     // Catch:{ Exception -> 0x0055 }
        L_0x0055:
            return r0
        L_0x0056:
            r5 = move-exception
            goto L_0x0066
        L_0x0058:
            r5 = r1
            goto L_0x005f
        L_0x005a:
            r5 = move-exception
            r2 = r1
            goto L_0x0066
        L_0x005d:
            r5 = r1
            r2 = r5
        L_0x005f:
            r5.close()     // Catch:{ Exception -> 0x006a, all -> 0x0056 }
            r1.close()     // Catch:{ Exception -> 0x006a, all -> 0x0056 }
            goto L_0x006a
        L_0x0066:
            r2.close()     // Catch:{ Exception -> 0x0069 }
        L_0x0069:
            throw r5
        L_0x006a:
            r2.close()     // Catch:{ Exception -> 0x006d }
        L_0x006d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.j.n.a(java.lang.String, java.lang.String, java.lang.String):boolean");
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(7:14|15|28|29|30|31|32) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:17|16|36|37|38|39|51) */
    /* JADX WARNING: Can't wrap try/catch for region: R(7:19|18|43|44|45|46|53) */
    /* JADX WARNING: Code restructure failed: missing block: B:52:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:22:0x005b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x0066 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:36:0x006b */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x006e */
    /* JADX WARNING: Missing exception handler attribute for start block: B:43:0x0073 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:45:0x0076 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void a(android.content.Context r7, java.lang.String r8) {
        /*
            r0 = 1024(0x400, float:1.435E-42)
            byte[] r1 = new byte[r0]
            r2 = 0
            java.util.zip.ZipInputStream r3 = new java.util.zip.ZipInputStream     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x006a, all -> 0x0061 }
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x006a, all -> 0x0061 }
            r4.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x006a, all -> 0x0061 }
            r3.<init>(r4)     // Catch:{ FileNotFoundException -> 0x0072, IOException -> 0x006a, all -> 0x0061 }
            java.util.zip.ZipEntry r8 = r3.getNextEntry()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
        L_0x0013:
            if (r8 == 0) goto L_0x0058
            java.lang.String r8 = r8.getName()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            java.io.FileOutputStream r4 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            r5.<init>()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            java.io.File r6 = r7.getFilesDir()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            java.lang.String r6 = r6.getPath()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            java.lang.String r6 = "/"
            r5.append(r6)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            r5.append(r8)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            java.lang.String r8 = r5.toString()     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
            r4.<init>(r8)     // Catch:{ FileNotFoundException -> 0x0073, IOException -> 0x006b, all -> 0x005f }
        L_0x003a:
            r8 = 0
            int r2 = r3.read(r1, r8, r0)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0054, all -> 0x0051 }
            if (r2 < 0) goto L_0x0045
            r4.write(r1, r8, r2)     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0054, all -> 0x0051 }
            goto L_0x003a
        L_0x0045:
            r4.close()     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0054, all -> 0x0051 }
            r3.closeEntry()     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0054, all -> 0x0051 }
            java.util.zip.ZipEntry r8 = r3.getNextEntry()     // Catch:{ FileNotFoundException -> 0x0056, IOException -> 0x0054, all -> 0x0051 }
            r2 = r4
            goto L_0x0013
        L_0x0051:
            r7 = move-exception
            r2 = r4
            goto L_0x0063
        L_0x0054:
            r2 = r4
            goto L_0x006b
        L_0x0056:
            r2 = r4
            goto L_0x0073
        L_0x0058:
            r2.close()     // Catch:{ IOException -> 0x005b }
        L_0x005b:
            r3.close()     // Catch:{ IOException -> 0x005e }
        L_0x005e:
            return
        L_0x005f:
            r7 = move-exception
            goto L_0x0063
        L_0x0061:
            r7 = move-exception
            r3 = r2
        L_0x0063:
            r2.close()     // Catch:{ IOException -> 0x0066 }
        L_0x0066:
            r3.close()     // Catch:{ IOException -> 0x0069 }
        L_0x0069:
            throw r7
        L_0x006a:
            r3 = r2
        L_0x006b:
            r2.close()     // Catch:{ IOException -> 0x006e }
        L_0x006e:
            r3.close()     // Catch:{ IOException -> 0x0071 }
        L_0x0071:
            return
        L_0x0072:
            r3 = r2
        L_0x0073:
            r2.close()     // Catch:{ IOException -> 0x0076 }
        L_0x0076:
            r3.close()     // Catch:{ IOException -> 0x0079 }
        L_0x0079:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.j.n.a(android.content.Context, java.lang.String):void");
    }
}
