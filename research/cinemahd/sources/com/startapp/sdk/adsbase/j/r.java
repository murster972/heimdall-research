package com.startapp.sdk.adsbase.j;

import com.startapp.common.ThreadManager;
import java.util.concurrent.Executor;

public final class r implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final ThreadManager.Priority f6396a;

    public r(ThreadManager.Priority priority) {
        this.f6396a = priority;
    }

    public final void execute(Runnable runnable) {
        ThreadManager.a(this.f6396a, runnable);
    }
}
