package com.startapp.sdk.adsbase.j;

import java.util.Comparator;
import org.json.JSONObject;

public final class h implements Comparator<JSONObject> {

    /* renamed from: a  reason: collision with root package name */
    private final String f6385a;

    public h(String str) {
        this.f6385a = str;
    }

    public final /* synthetic */ int compare(Object obj, Object obj2) {
        Object opt = ((JSONObject) obj).opt(this.f6385a);
        Object opt2 = ((JSONObject) obj2).opt(this.f6385a);
        if ((opt instanceof Comparable) && (opt2 instanceof Comparable)) {
            if (opt.getClass() == opt2.getClass()) {
                return ((Comparable) opt).compareTo(opt2);
            }
            if ((opt instanceof Number) && (opt2 instanceof Number)) {
                return Double.compare(((Number) opt).doubleValue(), ((Number) opt2).doubleValue());
            }
        }
        if (opt == JSONObject.NULL) {
            opt = null;
        }
        if (opt2 == JSONObject.NULL) {
            opt2 = null;
        }
        if (opt != null && opt2 != null) {
            return opt.toString().compareTo(opt2.toString());
        }
        if (opt != null) {
            return 1;
        }
        return opt2 != null ? -1 : 0;
    }
}
