package com.startapp.sdk.adsbase.j;

import java.util.Comparator;

public final class d<T> implements Comparator<T> {

    /* renamed from: a  reason: collision with root package name */
    private final Comparator<T> f6383a;
    private final Comparator<T> b;

    public d(Comparator<T> comparator, Comparator<T> comparator2) {
        this.f6383a = comparator;
        this.b = comparator2;
    }

    public final int compare(T t, T t2) {
        int compare = this.f6383a.compare(t, t2);
        return compare == 0 ? this.b.compare(t, t2) : compare;
    }
}
