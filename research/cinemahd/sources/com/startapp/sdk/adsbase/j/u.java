package com.startapp.sdk.adsbase.j;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.support.v4.media.session.PlaybackStateCompat;
import android.telephony.CellInfo;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.ads.AudienceNetworkActivity;
import com.google.android.gms.ads.AdRequest;
import com.original.tase.helper.js.JJDecoder;
import com.startapp.common.SDKException;
import com.startapp.sdk.ads.banner.banner3d.Banner3DAd;
import com.startapp.sdk.ads.banner.bannerstandard.BannerStandardAd;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.ads.nativead.NativeAd;
import com.startapp.sdk.ads.offerWall.offerWallHtml.OfferWallAd;
import com.startapp.sdk.ads.offerWall.offerWallJson.OfferWall3DAd;
import com.startapp.sdk.ads.splash.SplashAd;
import com.startapp.sdk.ads.video.VideoEnabledAd;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.GZIPOutputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterOutputStream;

public class u {

    /* renamed from: a  reason: collision with root package name */
    protected static int f6399a = 0;
    private static final String b = "u";
    private static Map<Activity, Integer> c = new WeakHashMap();

    public interface a {
        void a();

        void a(String str);
    }

    static class b {
        b() {
        }

        static StackTraceElement[] a() {
            return Thread.currentThread().getStackTrace();
        }
    }

    public static String a(Context context) {
        Drawable loadIcon;
        try {
            PackageManager packageManager = context.getPackageManager();
            if (packageManager == null || (loadIcon = packageManager.getApplicationInfo(context.getPackageName(), 128).loadIcon(packageManager)) == null) {
                return null;
            }
            float f = context.getResources().getDisplayMetrics().density;
            int i = (int) (48.0f * f);
            try {
                return a(loadIcon, i, i, Bitmap.Config.ARGB_8888);
            } catch (OutOfMemoryError unused) {
                int i2 = (int) (f * 24.0f);
                try {
                    return a(loadIcon, i2, i2, Bitmap.Config.ARGB_4444);
                } catch (OutOfMemoryError unused2) {
                    return null;
                }
            }
        } catch (Throwable th) {
            new e(th).a(context);
            return null;
        }
    }

    public static boolean a() {
        return false;
    }

    public static String b(Context context) {
        ComponentName component;
        Intent launchIntentForPackage = context.getPackageManager().getLaunchIntentForPackage(context.getPackageName());
        if (launchIntentForPackage == null || (component = launchIntentForPackage.getComponent()) == null) {
            return null;
        }
        return component.getClassName();
    }

    public static boolean b() {
        return true;
    }

    public static boolean c() {
        return true;
    }

    public static boolean c(Context context) {
        if (AdsConstants.b.booleanValue()) {
            return true;
        }
        if (com.startapp.common.b.b.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
            try {
                NetworkInfo activeNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity")).getActiveNetworkInfo();
                if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
                    return false;
                }
                return true;
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        return false;
    }

    public static void d(File file) {
        if (file != null) {
            try {
                if (!file.delete()) {
                    file.deleteOnExit();
                }
            } catch (Exception unused) {
            }
        }
    }

    public static String e(Context context) {
        Configuration configuration;
        Resources resources = context.getResources();
        if (resources == null || (configuration = resources.getConfiguration()) == null) {
            return JJDecoder.UNDEFINED;
        }
        int i = configuration.orientation;
        if (i == 2) {
            return "landscape";
        }
        return i == 1 ? "portrait" : JJDecoder.UNDEFINED;
    }

    public static String f(Context context) {
        try {
            Intent intent = new Intent();
            intent.setAction("android.intent.action.MAIN");
            intent.addCategory("android.intent.category.HOME");
            ResolveInfo resolveActivity = context.getPackageManager().resolveActivity(intent, 0);
            if (resolveActivity == null || resolveActivity.activityInfo == null) {
                return "";
            }
            String str = resolveActivity.activityInfo.packageName;
            return str != null ? str.toLowerCase() : str;
        } catch (Exception unused) {
            return "";
        }
    }

    public static boolean g(Context context) {
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses;
        try {
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager == null || (runningAppProcesses = activityManager.getRunningAppProcesses()) == null) {
                return false;
            }
            String packageName = context.getPackageName();
            for (ActivityManager.RunningAppProcessInfo next : runningAppProcesses) {
                if (next != null && next.importance == 100 && packageName.equals(next.processName)) {
                    return true;
                }
            }
            return false;
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static boolean h(Context context) {
        try {
            ActivityInfo[] activityInfoArr = context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities;
            boolean z = false;
            int i = 0;
            while (!z) {
                try {
                    if (i >= activityInfoArr.length) {
                        return z;
                    }
                    int i2 = i + 1;
                    ActivityInfo activityInfo = activityInfoArr[i];
                    if (activityInfo.name.equals("com.startapp.sdk.adsbase.activities.AppWallActivity") || activityInfo.name.equals("com.startapp.sdk.adsbase.activities.OverlayActivity") || activityInfo.name.equals("com.startapp.sdk.adsbase.activities.FullScreenActivity")) {
                        z = (activityInfo.flags & AdRequest.MAX_CONTENT_URL_LENGTH) == 0;
                    }
                    i = i2;
                } catch (PackageManager.NameNotFoundException | Exception unused) {
                    return z;
                }
            }
            return z;
        } catch (PackageManager.NameNotFoundException | Exception unused2) {
            return false;
        }
    }

    public static String i(Context context) {
        List<CellInfo> a2;
        try {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
            if (telephonyManager == null || (a2 = com.startapp.common.b.b.a(context, telephonyManager)) == null || a2.size() <= 0) {
                return null;
            }
            return com.startapp.common.b.a.c(a2.toString());
        } catch (SecurityException unused) {
            return null;
        } catch (Throwable th) {
            new e(th).a(context);
            return null;
        }
    }

    public static String[] j(Context context) {
        String[] strArr = {null, null};
        try {
            ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
            ActivityManager activityManager = (ActivityManager) context.getSystemService("activity");
            if (activityManager != null) {
                activityManager.getMemoryInfo(memoryInfo);
                strArr[0] = Long.toString(memoryInfo.availMem / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
                Long a2 = com.startapp.common.b.b.a(memoryInfo);
                if (a2 != null) {
                    strArr[1] = Long.toString((a2.longValue() - memoryInfo.availMem) / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED);
                }
            }
        } catch (SecurityException unused) {
        } catch (Throwable th) {
            new e(th).a(context);
        }
        return strArr;
    }

    public static Context k(Context context) {
        if (context instanceof Application) {
            return context;
        }
        try {
            if (context instanceof ContextWrapper) {
                return k(((ContextWrapper) context).getBaseContext());
            }
            if (context != null) {
                return context.getApplicationContext();
            }
            return null;
        } catch (Throwable unused) {
            return null;
        }
    }

    public static boolean l(Context context) {
        ApplicationInfo applicationInfo = context.getApplicationInfo();
        if (applicationInfo == null || (applicationInfo.flags & 2) == 0) {
            return false;
        }
        return true;
    }

    public static int m(Context context) {
        try {
            Resources resources = context.getResources();
            int identifier = resources.getIdentifier("com_startapp_sdk_aar", "integer", context.getPackageName());
            if (identifier != 0) {
                return resources.getInteger(identifier);
            }
            return 0;
        } catch (Throwable unused) {
            return 0;
        }
    }

    public static String n(Context context) {
        return m(context) == 1 ? "aar" : "jar";
    }

    public static StackTraceElement d() {
        StackTraceElement[] a2 = b.a();
        if (a2 == null) {
            return null;
        }
        String name = b.class.getName();
        int i = 0;
        int length = a2.length;
        while (i < length) {
            StackTraceElement stackTraceElement = a2[i];
            if (stackTraceElement == null || !name.equals(stackTraceElement.getClassName())) {
                i++;
            } else {
                int i2 = i + 3;
                if (i2 < length) {
                    return a2[i2];
                }
                return null;
            }
        }
        return null;
    }

    public static <T> boolean b(T t, T t2) {
        if (t == null) {
            return t2 == null;
        }
        return t.equals(t2);
    }

    public static String b(Object obj) throws IOException {
        String a2 = com.startapp.common.parser.b.a(obj);
        if (a2 != null) {
            return Base64.encodeToString(c(a2.getBytes()), 11);
        }
        return null;
    }

    private static byte[] c(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(byteArrayOutputStream, new Deflater(9, true));
        deflaterOutputStream.write(bArr);
        deflaterOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    private static String a(Drawable drawable, int i, int i2, Bitmap.Config config) {
        Bitmap createBitmap = Bitmap.createBitmap(i, i2, config);
        Drawable mutate = drawable.mutate();
        mutate.setBounds(0, 0, i, i2);
        mutate.draw(new Canvas(createBitmap));
        f fVar = new f(i * i2);
        createBitmap.compress(Bitmap.CompressFormat.PNG, 100, fVar);
        return new String(Base64.encode(fVar.a(), 0, fVar.b(), 2));
    }

    public static byte[] b(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        gZIPOutputStream.write(bArr);
        gZIPOutputStream.flush();
        gZIPOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static boolean d(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        NetworkInfo activeNetworkInfo = connectivityManager != null ? connectivityManager.getActiveNetworkInfo() : null;
        return activeNetworkInfo != null && activeNetworkInfo.getType() == 1;
    }

    /* JADX WARNING: type inference failed for: r0v0 */
    /* JADX WARNING: type inference failed for: r0v1, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: type inference failed for: r0v2, types: [java.io.Closeable] */
    /* JADX WARNING: type inference failed for: r0v3, types: [java.util.List<java.lang.String>] */
    /* JADX WARNING: type inference failed for: r0v4 */
    /* JADX WARNING: type inference failed for: r0v7 */
    /* JADX WARNING: type inference failed for: r0v9 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.util.List<java.lang.String> c(java.io.File r3) {
        /*
            r0 = 0
            java.io.BufferedReader r1 = new java.io.BufferedReader     // Catch:{ Exception -> 0x0029, all -> 0x0024 }
            java.io.FileReader r2 = new java.io.FileReader     // Catch:{ Exception -> 0x0029, all -> 0x0024 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0029, all -> 0x0024 }
            r1.<init>(r2)     // Catch:{ Exception -> 0x0029, all -> 0x0024 }
        L_0x000b:
            java.lang.String r3 = r1.readLine()     // Catch:{ Exception -> 0x002a, all -> 0x0021 }
            if (r3 == 0) goto L_0x001d
            if (r0 != 0) goto L_0x0019
            java.util.ArrayList r2 = new java.util.ArrayList     // Catch:{ Exception -> 0x002a, all -> 0x0021 }
            r2.<init>()     // Catch:{ Exception -> 0x002a, all -> 0x0021 }
            r0 = r2
        L_0x0019:
            r0.add(r3)     // Catch:{ Exception -> 0x002a, all -> 0x0021 }
            goto L_0x000b
        L_0x001d:
            a((java.io.Closeable) r1)
            return r0
        L_0x0021:
            r3 = move-exception
            r0 = r1
            goto L_0x0025
        L_0x0024:
            r3 = move-exception
        L_0x0025:
            a((java.io.Closeable) r0)
            throw r3
        L_0x0029:
            r1 = r0
        L_0x002a:
            a((java.io.Closeable) r1)
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.j.u.c(java.io.File):java.util.List");
    }

    public static String a(Double d) {
        if (d == null) {
            return null;
        }
        return String.format(Locale.US, "%.2f", new Object[]{d});
    }

    public static String b(File file) {
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(new FileReader(file));
            try {
                StringBuilder sb = new StringBuilder();
                while (true) {
                    String readLine = bufferedReader.readLine();
                    if (readLine != null) {
                        sb.append(readLine);
                    } else {
                        String sb2 = sb.toString();
                        a((Closeable) bufferedReader);
                        return sb2;
                    }
                }
            } catch (Exception unused) {
                a((Closeable) bufferedReader);
                return null;
            } catch (Throwable th) {
                th = th;
                a((Closeable) bufferedReader);
                throw th;
            }
        } catch (Exception unused2) {
            bufferedReader = null;
            a((Closeable) bufferedReader);
            return null;
        } catch (Throwable th2) {
            th = th2;
            bufferedReader = null;
            a((Closeable) bufferedReader);
            throw th;
        }
    }

    public static void a(SharedPreferences.Editor editor) {
        com.startapp.common.b.b.a(editor);
    }

    public static String a(String str, String str2, String str3) {
        int indexOf;
        int indexOf2;
        if (str == null || (indexOf = str.indexOf(str2)) == -1 || (indexOf2 = str.indexOf(str3, str2.length() + indexOf)) == -1) {
            return null;
        }
        return str.substring(indexOf + str2.length(), indexOf2);
    }

    public static int a(Activity activity, int i, boolean z) {
        if (z) {
            if (!c.containsKey(activity)) {
                c.put(activity, Integer.valueOf(activity.getRequestedOrientation()));
            }
            if (i == activity.getResources().getConfiguration().orientation) {
                return com.startapp.common.b.b.a(activity, i, false);
            }
            return com.startapp.common.b.b.a(activity, i, true);
        } else if (!c.containsKey(activity)) {
            return -1;
        } else {
            int intValue = c.get(activity).intValue();
            com.startapp.common.b.b.a(activity, intValue);
            c.remove(activity);
            return intValue;
        }
    }

    public static boolean b(String str) {
        if (str == null) {
            return false;
        }
        try {
            String[] split = new URL(MetaData.G().r()).getHost().split("\\.");
            if (split.length > 1) {
                return str.toLowerCase(Locale.ENGLISH).contains(split[1].toLowerCase(Locale.ENGLISH));
            }
        } catch (MalformedURLException unused) {
        }
        return false;
    }

    public static void a(Activity activity, boolean z) {
        a(activity, activity.getResources().getConfiguration().orientation, z);
    }

    private static List<Field> a(List<Field> list, Class<?> cls) {
        list.addAll(Arrays.asList(cls.getDeclaredFields()));
        if (cls.getSuperclass() != null) {
            a(list, (Class<?>) cls.getSuperclass());
        }
        return list;
    }

    public static <T> boolean a(T t, T t2) {
        Object obj;
        boolean z = false;
        try {
            for (Field next : a((List<Field>) new LinkedList(), t2.getClass())) {
                int modifiers = next.getModifiers();
                if (!Modifier.isTransient(modifiers) && !Modifier.isStatic(modifiers)) {
                    next.setAccessible(true);
                    if (next.get(t) == null && (obj = next.get(t2)) != null) {
                        next.set(t, obj);
                        z = true;
                    }
                }
            }
        } catch (Exception unused) {
        }
        return z;
    }

    public static void a(Context context, String str, final a aVar) {
        if ("true".equals(a(str, "@doNotRender@", "@doNotRender@"))) {
            aVar.a();
            return;
        }
        try {
            final WebView webView = new WebView(context);
            final Handler handler = new Handler(Looper.getMainLooper());
            if (AdsConstants.b.booleanValue()) {
                f6399a = 25000;
                webView.getSettings().setBlockNetworkImage(false);
                webView.getSettings().setLoadsImagesAutomatically(true);
                webView.getSettings().setJavaScriptEnabled(true);
            } else {
                f6399a = 0;
            }
            webView.setWebChromeClient(new WebChromeClient());
            webView.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    super.onPageFinished(webView, str);
                    handler.removeCallbacksAndMessages((Object) null);
                    handler.postDelayed(new Runnable() {
                        public final void run() {
                            webView.destroy();
                            aVar.a();
                        }
                    }, (long) u.f6399a);
                }

                public final void onReceivedError(final WebView webView, int i, final String str, String str2) {
                    super.onReceivedError(webView, i, str, str2);
                    handler.removeCallbacksAndMessages((Object) null);
                    handler.post(new Runnable() {
                        public final void run() {
                            webView.destroy();
                            aVar.a(str);
                        }
                    });
                }

                public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
                    if (webView == null || str == null || u.a(webView.getContext(), str)) {
                        return true;
                    }
                    return super.shouldOverrideUrlLoading(webView, str);
                }
            });
            a(context, webView, str);
            handler.postDelayed(new Runnable() {
                public final void run() {
                    webView.destroy();
                    aVar.a();
                }
            }, 25000);
        } catch (Throwable th) {
            new e(th).a(context);
            aVar.a("WebView instantiation Error");
        }
    }

    public static void a(Context context, WebView webView, String str) {
        try {
            webView.loadDataWithBaseURL(MetaData.G().u(), str, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, AudienceNetworkActivity.WEBVIEW_ENCODING, (String) null);
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static void a(WebView webView, String str, Object... objArr) {
        a(webView, true, str, objArr);
    }

    public static void a(WebView webView, boolean z, String str, Object... objArr) {
        if (webView != null) {
            try {
                StringBuilder sb = new StringBuilder();
                sb.append(str);
                sb.append("(");
                if (objArr != null) {
                    for (int i = 0; i < objArr.length; i++) {
                        if (!z || !(objArr[i] instanceof String)) {
                            sb.append(objArr[i]);
                        } else {
                            sb.append("\"");
                            sb.append(objArr[i]);
                            sb.append("\"");
                        }
                        if (i < objArr.length - 1) {
                            sb.append(",");
                        }
                    }
                }
                sb.append(")");
                webView.loadUrl("javascript:" + sb.toString());
            } catch (Exception unused) {
            }
        }
    }

    public static Class<?> a(Context context, Class<? extends Activity> cls, Class<? extends Activity> cls2) {
        if (a(context, cls) || !a(context, cls2)) {
            return cls;
        }
        String str = b;
        Log.w(str, "Expected activity " + cls.getName() + " is missing from AndroidManifest.xml");
        return cls2;
    }

    public static boolean a(Context context, Class<? extends Activity> cls) {
        try {
            for (ActivityInfo activityInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 1).activities) {
                if (activityInfo.name.equals(cls.getName())) {
                    return true;
                }
            }
        } catch (Exception unused) {
        }
        return false;
    }

    public static String a(String str, Context context) {
        try {
            new com.startapp.common.b.b();
            return com.startapp.common.b.b.a(str, context);
        } catch (Throwable th) {
            new e(th).a(context);
            return null;
        }
    }

    public static long a(File file) {
        return com.startapp.common.b.b.a(file);
    }

    public static String a(Context context, int i) {
        try {
            Bitmap decodeResource = BitmapFactory.decodeResource(context.getResources(), i);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            decodeResource.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
            return Base64.encodeToString(byteArrayOutputStream.toByteArray(), 2);
        } catch (Exception unused) {
            return "";
        }
    }

    public static <T> T a(String str, Class<T> cls) throws SDKException {
        T a2 = com.startapp.common.parser.b.a(str, cls);
        if (a2 != null) {
            return a2;
        }
        throw new SDKException();
    }

    public static void a(Object obj) {
        new Handler(Looper.getMainLooper()).postAtTime((Runnable) null, obj, SystemClock.uptimeMillis() + 1000);
    }

    public static int a(Object... objArr) {
        return Arrays.deepHashCode(objArr);
    }

    public static byte[] a(String str) throws IOException {
        return c(str.getBytes());
    }

    public static byte[] a(byte[] bArr) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        InflaterOutputStream inflaterOutputStream = new InflaterOutputStream(byteArrayOutputStream, new Inflater(true));
        inflaterOutputStream.write(bArr);
        inflaterOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static String a(int... iArr) {
        int length = iArr.length;
        char[] cArr = new char[length];
        char c2 = (char) length;
        for (int i = 0; i < length; i++) {
            c2 = (char) (c2 + iArr[i]);
            cArr[i] = c2;
        }
        return new String(cArr);
    }

    public static OutputStream a(OutputStream outputStream) {
        return new DeflaterOutputStream(new Base64OutputStream(outputStream, 10), new Deflater(9, true));
    }

    public static void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Exception unused) {
            }
        }
    }

    public static <T> String a(Iterable<T> iterable, String str) {
        StringBuilder sb = new StringBuilder();
        boolean z = false;
        for (T next : iterable) {
            if (z) {
                sb.append(str);
            }
            sb.append(next);
            z = true;
        }
        return sb.toString();
    }

    public static String a(StackTraceElement stackTraceElement) {
        if (stackTraceElement == null) {
            return "null";
        }
        return stackTraceElement.getClassName() + '.' + stackTraceElement.getMethodName() + "()";
    }

    public static void a(Context context, boolean z, String str) {
        if (z) {
            Log.e("StartAppSDK", str);
        } else {
            Log.i("StartAppSDK", str);
        }
        if (l(context) || com.startapp.common.b.b.i(context)) {
            new e(InfoEventCategory.GENERAL).f("Log for a publisher").g(str).a(context);
        }
    }

    public static String a(Ad ad) {
        if (ad instanceof VideoEnabledAd) {
            VideoEnabledAd videoEnabledAd = (VideoEnabledAd) ad;
            if (videoEnabledAd.getType() == Ad.AdType.VIDEO) {
                return "VIDEO";
            }
            return videoEnabledAd.getType() == Ad.AdType.REWARDED_VIDEO ? "REWARDED_VIDEO" : "INTERSTITIAL";
        } else if (ad instanceof ReturnAd) {
            return "RETURN";
        } else {
            if (ad instanceof OfferWallAd) {
                return "OFFER_WALL";
            }
            if (ad instanceof OfferWall3DAd) {
                return "OFFER_WALL_3D";
            }
            if (ad instanceof BannerStandardAd) {
                BannerStandardAd bannerStandardAd = (BannerStandardAd) ad;
                if (bannerStandardAd.d() == 0) {
                    return AdPreferences.TYPE_BANNER;
                }
                if (bannerStandardAd.d() == 1) {
                    return "MREC";
                }
                return bannerStandardAd.d() == 2 ? "COVER" : "BANNER_UNDEFINED";
            } else if (ad instanceof Banner3DAd) {
                return "BANNER_3D";
            } else {
                if (ad instanceof NativeAd) {
                    return "NATIVE";
                }
                return ad instanceof SplashAd ? "SPLASH" : "UNDEFINED";
            }
        }
    }

    public static boolean a(Context context, String str) {
        if (Build.VERSION.SDK_INT < 15) {
            return false;
        }
        if (str.startsWith("sms:") || str.startsWith("smsto:")) {
            Intent intent = new Intent("android.intent.action.SENDTO");
            intent.setData(Uri.parse(str));
            intent.addFlags(268435456);
            try {
                context.startActivity(intent);
                return true;
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        return false;
    }

    public static int[] a(Context context, String... strArr) {
        int[] iArr = new int[3];
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 4096);
            if (packageInfo.requestedPermissions != null) {
                int length = packageInfo.requestedPermissions.length;
                for (int i = 0; i < length; i++) {
                    for (int i2 = 0; i2 < 3; i2++) {
                        if (strArr[i2].equals(packageInfo.requestedPermissions[i])) {
                            if (Build.VERSION.SDK_INT < 16) {
                                iArr[i2] = 1;
                            } else if ((packageInfo.requestedPermissionsFlags[i] & 2) == 2) {
                                iArr[i2] = 1;
                            } else {
                                iArr[i2] = 2;
                            }
                        }
                    }
                }
            }
        } catch (Throwable unused) {
        }
        return iArr;
    }
}
