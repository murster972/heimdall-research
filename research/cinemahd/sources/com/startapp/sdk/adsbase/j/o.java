package com.startapp.sdk.adsbase.j;

import android.os.Build;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;

public final class o implements Executor {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<Runnable> f6393a;
    private final Executor b;
    private Runnable c;

    public o(Executor executor) {
        if (Build.VERSION.SDK_INT >= 9) {
            this.f6393a = new ArrayDeque();
        } else {
            this.f6393a = new LinkedList();
        }
        this.b = executor;
    }

    /* access modifiers changed from: protected */
    public final synchronized void a() {
        Runnable poll = this.f6393a.poll();
        this.c = poll;
        if (poll != null) {
            this.b.execute(this.c);
        }
    }

    public final synchronized void execute(final Runnable runnable) {
        this.f6393a.offer(new Runnable() {
            public final void run() {
                try {
                    runnable.run();
                } finally {
                    o.this.a();
                }
            }
        });
        if (this.c == null) {
            a();
        }
    }
}
