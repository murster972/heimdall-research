package com.startapp.sdk.adsbase.j;

import android.content.Context;
import com.startapp.sdk.adsbase.adrules.b;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import java.util.UUID;

public final class p {

    /* renamed from: a  reason: collision with root package name */
    private static p f6395a = new p();
    private String b = "";
    private long c = 0;
    private MetaDataRequest.RequestReason d = MetaDataRequest.RequestReason.LAUNCH;

    public static p d() {
        return f6395a;
    }

    public final String a() {
        return this.b;
    }

    public final long b() {
        return this.c;
    }

    public final MetaDataRequest.RequestReason c() {
        return this.d;
    }

    public final synchronized void a(Context context, MetaDataRequest.RequestReason requestReason) {
        this.b = UUID.randomUUID().toString();
        this.c = System.currentTimeMillis();
        this.d = requestReason;
        u.a();
        b.a().b();
        MetaData.G().a(context, new AdPreferences(), requestReason, false, (com.startapp.sdk.adsbase.remoteconfig.b) null, true);
    }
}
