package com.startapp.sdk.adsbase.j;

import com.startapp.common.SDKException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class k extends m {

    /* renamed from: a  reason: collision with root package name */
    private List<l> f6389a;

    public k() {
        this.f6389a = null;
        this.f6389a = new ArrayList();
    }

    public final void a(String str, Object obj, boolean z, boolean z2) throws SDKException {
        if (z && obj == null) {
            throw new SDKException("Required key: [" + str + "] is missing", (Throwable) null);
        } else if (obj != null && !obj.toString().equals("")) {
            try {
                l lVar = new l();
                lVar.a(str);
                String obj2 = obj.toString();
                if (z2) {
                    obj2 = URLEncoder.encode(obj2, "UTF-8");
                }
                lVar.b(obj2);
                this.f6389a.add(lVar);
            } catch (UnsupportedEncodingException e) {
                if (z) {
                    throw new SDKException("failed encoding value: [" + obj + "]", e);
                }
            }
        }
    }

    public final String toString() {
        Set<String> c;
        StringBuilder sb = new StringBuilder();
        if (this.f6389a == null) {
            return sb.toString();
        }
        sb.append('?');
        for (l next : this.f6389a) {
            if (next.b() != null) {
                sb.append(next.a());
                sb.append('=');
                sb.append(next.b());
                sb.append('&');
            } else if (!(next.c() == null || (c = next.c()) == null)) {
                for (String append : c) {
                    sb.append(next.a());
                    sb.append('=');
                    sb.append(append);
                    sb.append('&');
                }
            }
        }
        if (sb.length() != 0) {
            sb.deleteCharAt(sb.length() - 1);
        }
        return sb.toString().replace("+", "%20");
    }

    public final void a(String str, Set<String> set) throws SDKException {
        if (set != null) {
            l lVar = new l();
            lVar.a(str);
            HashSet hashSet = new HashSet();
            for (String encode : set) {
                try {
                    hashSet.add(URLEncoder.encode(encode, "UTF-8"));
                } catch (UnsupportedEncodingException unused) {
                }
            }
            lVar.a((Set<String>) hashSet);
            this.f6389a.add(lVar);
        }
    }
}
