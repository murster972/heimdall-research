package com.startapp.sdk.adsbase.j;

import android.os.Build;
import java.util.Iterator;
import java.util.NoSuchElementException;

public final class s implements Iterator<Throwable> {

    /* renamed from: a  reason: collision with root package name */
    private Throwable f6397a;
    private Throwable[] b;
    private int c;
    private boolean d;

    public s(Throwable th) {
        this.f6397a = th;
        if (Build.VERSION.SDK_INT >= 19) {
            this.b = th.getSuppressed();
        }
    }

    public final boolean a() {
        return this.d;
    }

    /* renamed from: b */
    public final Throwable next() {
        int i;
        Throwable th = this.f6397a;
        boolean z = false;
        this.d = false;
        if (th != null) {
            this.f6397a = th.getCause();
        } else {
            Throwable[] thArr = this.b;
            if (thArr != null && (i = this.c) < thArr.length) {
                if (i == 0) {
                    z = true;
                }
                this.d = z;
                Throwable[] thArr2 = this.b;
                int i2 = this.c;
                this.c = i2 + 1;
                th = thArr2[i2];
            }
        }
        if (th != null) {
            return th;
        }
        throw new NoSuchElementException();
    }

    public final boolean hasNext() {
        if (this.f6397a != null) {
            return true;
        }
        Throwable[] thArr = this.b;
        return thArr != null && this.c < thArr.length;
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
