package com.startapp.sdk.adsbase.j;

import android.os.Build;
import java.util.ArrayDeque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.Executor;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final Queue<Runnable> f6380a;
    private final Executor b;
    private Runnable c;

    public c(Executor executor) {
        if (Build.VERSION.SDK_INT >= 9) {
            this.f6380a = new ArrayDeque();
        } else {
            this.f6380a = new LinkedList();
        }
        this.b = executor;
    }

    public final synchronized void a(final b bVar) {
        this.f6380a.offer(new Runnable() {
            public final void run() {
                bVar.a(new Runnable() {

                    /* renamed from: a  reason: collision with root package name */
                    private boolean f6382a;

                    public final void run() {
                        synchronized (this) {
                            if (!this.f6382a) {
                                this.f6382a = true;
                                c.this.a();
                            }
                        }
                    }
                });
            }
        });
        if (this.c == null) {
            a();
        }
    }

    /* access modifiers changed from: protected */
    public final synchronized void a() {
        Runnable poll = this.f6380a.poll();
        this.c = poll;
        if (poll != null) {
            this.b.execute(this.c);
        }
    }
}
