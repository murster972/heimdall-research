package com.startapp.sdk.adsbase.j;

import com.startapp.common.SDKException;
import java.util.Set;

public abstract class m {
    public final void a(String str, Object obj, boolean z) throws SDKException {
        a(str, obj, z, true);
    }

    public abstract void a(String str, Object obj, boolean z, boolean z2) throws SDKException;

    public abstract void a(String str, Set<String> set) throws SDKException;
}
