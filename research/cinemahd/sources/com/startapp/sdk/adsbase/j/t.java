package com.startapp.sdk.adsbase.j;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.TypedValue;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.startapp.common.parser.d;
import com.startapp.networkTest.enums.bluetooth.BluetoothConnectionState;
import java.util.ArrayList;
import java.util.Set;

public final class t {

    /* renamed from: a  reason: collision with root package name */
    public boolean f6398a = false;
    private BluetoothConnectionState b;
    private BluetoothConnectionState c;
    private BluetoothConnectionState d;
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> e = new ArrayList<>();
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> f = new ArrayList<>();
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> g = new ArrayList<>();
    @d(b = ArrayList.class, c = com.startapp.networkTest.data.d.class)
    private ArrayList<com.startapp.networkTest.data.d> h = new ArrayList<>();

    public t() {
        BluetoothConnectionState bluetoothConnectionState = BluetoothConnectionState.Unknown;
        this.b = bluetoothConnectionState;
        this.c = bluetoothConnectionState;
        this.d = bluetoothConnectionState;
    }

    public static int a(Context context, int i) {
        return Math.round(TypedValue.applyDimension(1, (float) i, context.getResources().getDisplayMetrics()));
    }

    public static int b(Context context, int i) {
        return Math.round(((float) i) / context.getResources().getDisplayMetrics().density);
    }

    public static void a(TextView textView, Set<String> set) {
        if (set.contains("UNDERLINE")) {
            textView.setPaintFlags(textView.getPaintFlags() | 8);
        }
        int i = 0;
        if (set.contains("BOLD") && set.contains("ITALIC")) {
            i = 3;
        } else if (set.contains("BOLD")) {
            i = 1;
        } else if (set.contains("ITALIC")) {
            i = 2;
        }
        textView.setTypeface((Typeface) null, i);
    }

    public static TextView a(Context context, Typeface typeface, float f2, int i, int i2) {
        TextView textView = new TextView(context);
        textView.setTypeface(typeface, 1);
        textView.setTextSize(1, f2);
        textView.setSingleLine(true);
        textView.setEllipsize(TextUtils.TruncateAt.END);
        textView.setTextColor(i);
        textView.setId(i2);
        return textView;
    }

    public static RelativeLayout.LayoutParams a(Context context, int[] iArr, int[] iArr2) {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-2, -2);
        for (int addRule : iArr2) {
            layoutParams.addRule(addRule);
        }
        for (int i = 0; i < iArr.length; i++) {
            iArr[i] = iArr[i] == 0 ? 0 : a(context, iArr[i]);
        }
        layoutParams.setMargins(iArr[0], iArr[1], iArr[2], iArr[3]);
        return layoutParams;
    }

    public static RelativeLayout.LayoutParams a(Context context, int[] iArr, int[] iArr2, int i, int i2) {
        RelativeLayout.LayoutParams a2 = a(context, iArr, iArr2);
        a2.addRule(i, i2);
        return a2;
    }

    public static ImageView a(Context context, Bitmap bitmap, int i) {
        ImageView imageView = new ImageView(context);
        imageView.setImageBitmap(bitmap);
        imageView.setId(i);
        return imageView;
    }
}
