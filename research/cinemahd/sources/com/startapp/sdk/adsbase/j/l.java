package com.startapp.sdk.adsbase.j;

import java.util.Set;

public final class l {

    /* renamed from: a  reason: collision with root package name */
    private String f6390a;
    private String b;
    private Set<String> c;

    public final String a() {
        return this.f6390a;
    }

    public final String b() {
        return this.b;
    }

    public final Set<String> c() {
        return this.c;
    }

    public final String toString() {
        return "NameValueObject [name=" + this.f6390a + ", value=" + this.b + ", valueSet=" + this.c + "]";
    }

    public final void a(String str) {
        this.f6390a = str;
    }

    public final void b(String str) {
        this.b = str;
    }

    public final void a(Set<String> set) {
        this.c = set;
    }
}
