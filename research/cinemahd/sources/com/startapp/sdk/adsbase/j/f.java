package com.startapp.sdk.adsbase.j;

import java.io.ByteArrayOutputStream;

public final class f extends ByteArrayOutputStream {
    public f() {
    }

    public final byte[] a() {
        return this.buf;
    }

    public final int b() {
        return this.count;
    }

    public f(int i) {
        super(i);
    }
}
