package com.startapp.sdk.adsbase.j;

import android.content.Context;
import android.graphics.Bitmap;
import com.startapp.common.ThreadManager;
import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public final class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final Map<String, Bitmap> f6378a = new ConcurrentHashMap();

    /* JADX WARNING: Removed duplicated region for block: B:20:0x0059 A[SYNTHETIC, Splitter:B:20:0x0059] */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0060 A[SYNTHETIC, Splitter:B:28:0x0060] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static android.graphics.Bitmap a(android.content.Context r3, java.lang.String r4, boolean r5) {
        /*
            java.util.Map<java.lang.String, android.graphics.Bitmap> r0 = f6378a
            java.lang.Object r0 = r0.get(r4)
            android.graphics.Bitmap r0 = (android.graphics.Bitmap) r0
            if (r0 == 0) goto L_0x000b
            return r0
        L_0x000b:
            com.startapp.sdk.adsbase.j.n.a((android.content.Context) r3, (boolean) r5)
            r5 = 0
            java.io.FileInputStream r0 = new java.io.FileInputStream     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            r1.<init>()     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.io.File r2 = r3.getFilesDir()     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.lang.String r2 = r2.getPath()     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            r1.append(r2)     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.lang.String r2 = "/"
            r1.append(r2)     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            r1.append(r4)     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            r0.<init>(r1)     // Catch:{ Exception -> 0x005d, all -> 0x0056 }
            android.graphics.Bitmap r1 = android.graphics.BitmapFactory.decodeStream(r0)     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            android.content.res.Resources r2 = r3.getResources()     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            android.content.res.Resources r3 = r3.getResources()     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            if (r3 == 0) goto L_0x0045
            android.util.DisplayMetrics r3 = r2.getDisplayMetrics()     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            int r3 = r3.densityDpi     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            goto L_0x0047
        L_0x0045:
            r3 = 160(0xa0, float:2.24E-43)
        L_0x0047:
            r1.setDensity(r3)     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            java.util.Map<java.lang.String, android.graphics.Bitmap> r3 = f6378a     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            r3.put(r4, r1)     // Catch:{ Exception -> 0x005e, all -> 0x0053 }
            r0.close()     // Catch:{ IOException -> 0x0052 }
        L_0x0052:
            return r1
        L_0x0053:
            r3 = move-exception
            r5 = r0
            goto L_0x0057
        L_0x0056:
            r3 = move-exception
        L_0x0057:
            if (r5 == 0) goto L_0x005c
            r5.close()     // Catch:{ IOException -> 0x005c }
        L_0x005c:
            throw r3
        L_0x005d:
            r0 = r5
        L_0x005e:
            if (r0 == 0) goto L_0x0063
            r0.close()     // Catch:{ IOException -> 0x0063 }
        L_0x0063:
            return r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.j.a.a(android.content.Context, java.lang.String, boolean):android.graphics.Bitmap");
    }

    public static Bitmap a(Context context, String str) {
        Bitmap a2 = a(context, str, false);
        return a2 == null ? a(context, str, true) : a2;
    }

    public static void a(final Context context, final Bitmap bitmap, final String str, final String str2) {
        ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable() {
            /* JADX WARNING: Removed duplicated region for block: B:20:0x006b A[SYNTHETIC, Splitter:B:20:0x006b] */
            /* JADX WARNING: Removed duplicated region for block: B:25:0x0076 A[SYNTHETIC, Splitter:B:25:0x0076] */
            /* JADX WARNING: Removed duplicated region for block: B:31:? A[RETURN, SYNTHETIC] */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void run() {
                /*
                    r5 = this;
                    java.util.Map r0 = com.startapp.sdk.adsbase.j.a.f6378a
                    java.lang.StringBuilder r1 = new java.lang.StringBuilder
                    r1.<init>()
                    java.lang.String r2 = r4
                    r1.append(r2)
                    java.lang.String r2 = r5
                    r1.append(r2)
                    java.lang.String r1 = r1.toString()
                    android.graphics.Bitmap r2 = r3
                    r0.put(r1, r2)
                    r0 = 0
                    java.io.FileOutputStream r1 = new java.io.FileOutputStream     // Catch:{ Exception -> 0x0065 }
                    java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0065 }
                    r2.<init>()     // Catch:{ Exception -> 0x0065 }
                    android.content.Context r3 = r2     // Catch:{ Exception -> 0x0065 }
                    java.io.File r3 = r3.getFilesDir()     // Catch:{ Exception -> 0x0065 }
                    java.lang.String r3 = r3.getPath()     // Catch:{ Exception -> 0x0065 }
                    r2.append(r3)     // Catch:{ Exception -> 0x0065 }
                    java.lang.String r3 = "/"
                    r2.append(r3)     // Catch:{ Exception -> 0x0065 }
                    java.lang.String r3 = r4     // Catch:{ Exception -> 0x0065 }
                    r2.append(r3)     // Catch:{ Exception -> 0x0065 }
                    java.lang.String r3 = r5     // Catch:{ Exception -> 0x0065 }
                    r2.append(r3)     // Catch:{ Exception -> 0x0065 }
                    java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0065 }
                    r1.<init>(r2)     // Catch:{ Exception -> 0x0065 }
                    android.graphics.Bitmap r0 = r3     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
                    android.graphics.Bitmap$CompressFormat r2 = android.graphics.Bitmap.CompressFormat.PNG     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
                    r3 = 100
                    r0.compress(r2, r3, r1)     // Catch:{ Exception -> 0x005e, all -> 0x0059 }
                    r1.close()     // Catch:{ IOException -> 0x0054 }
                    return
                L_0x0054:
                    r0 = move-exception
                    r0.printStackTrace()
                    return
                L_0x0059:
                    r0 = move-exception
                    r4 = r1
                    r1 = r0
                    r0 = r4
                    goto L_0x0074
                L_0x005e:
                    r0 = move-exception
                    r4 = r1
                    r1 = r0
                    r0 = r4
                    goto L_0x0066
                L_0x0063:
                    r1 = move-exception
                    goto L_0x0074
                L_0x0065:
                    r1 = move-exception
                L_0x0066:
                    r1.printStackTrace()     // Catch:{ all -> 0x0063 }
                    if (r0 == 0) goto L_0x0073
                    r0.close()     // Catch:{ IOException -> 0x006f }
                    goto L_0x0073
                L_0x006f:
                    r0 = move-exception
                    r0.printStackTrace()
                L_0x0073:
                    return
                L_0x0074:
                    if (r0 == 0) goto L_0x007e
                    r0.close()     // Catch:{ IOException -> 0x007a }
                    goto L_0x007e
                L_0x007a:
                    r0 = move-exception
                    r0.printStackTrace()
                L_0x007e:
                    throw r1
                */
                throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.j.a.AnonymousClass1.run():void");
            }
        });
    }

    public static boolean a(Context context, String str, String str2) {
        if (!str.endsWith(str2)) {
            str = str + str2;
        }
        if (!f6378a.containsKey(str)) {
            if (!new File(context.getFilesDir().getPath() + "/" + str).exists()) {
                return false;
            }
        }
        return true;
    }
}
