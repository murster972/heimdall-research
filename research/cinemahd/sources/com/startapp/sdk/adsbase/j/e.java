package com.startapp.sdk.adsbase.j;

import android.content.Context;
import android.os.SystemClock;
import com.startapp.common.jobrunner.RunnerRequest;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.d;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private static volatile boolean f6384a = false;

    public static final class a implements com.startapp.common.jobrunner.interfaces.a {
        public final RunnerJob a(int i) {
            if (i == 586482792) {
                return new com.startapp.sdk.adsbase.remoteconfig.e();
            }
            if (i != 786564404) {
                return null;
            }
            return new d();
        }
    }

    public static void a(Context context) {
        if (!f6384a) {
            f6384a = true;
            com.startapp.common.jobrunner.a.a(context);
            com.startapp.common.jobrunner.a.a((com.startapp.common.jobrunner.interfaces.a) new a());
        }
    }

    public static long b(Context context) {
        return SystemClock.elapsedRealtime() + (((long) MetaData.G().c(context)) * 60000);
    }

    public static void c(Context context) {
        a(context, Long.valueOf(a()));
    }

    public static void d(Context context) {
        a(context, b(context));
    }

    public static long a() {
        return SystemClock.elapsedRealtime() + (((long) MetaData.G().n()) * 60000);
    }

    public static void a(Context context, Long l) {
        if (!j.a(context, "periodicMetadataPaused", Boolean.FALSE).booleanValue() && MetaData.G().m()) {
            a(context, 586482792, l.longValue() - SystemClock.elapsedRealtime(), "periodicMetadataTriggerTime");
        }
    }

    public static void a(Context context, long j) {
        if (!j.a(context, "periodicInfoEventPaused", Boolean.FALSE).booleanValue() && MetaData.G().o()) {
            a(context, 786564404, j - SystemClock.elapsedRealtime(), "periodicInfoEventTriggerTime");
        }
    }

    public static void a(int i) {
        com.startapp.common.jobrunner.a.a(i, false);
    }

    private static void a(Context context, int i, long j, String str) {
        if (com.startapp.common.jobrunner.a.a(new RunnerRequest.a(i).a(j).b())) {
            j.b(context, str, Long.valueOf(j + SystemClock.elapsedRealtime()));
        } else {
            new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.ERROR).f("Util.setPeriodicAlarm - failed setting alarm ".concat(String.valueOf(i))).a(context);
        }
    }
}
