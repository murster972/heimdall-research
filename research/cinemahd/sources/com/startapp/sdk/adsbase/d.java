package com.startapp.sdk.adsbase;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Process;
import android.util.Pair;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.model.GetAdRequest;

public abstract class d {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f6335a;
    /* access modifiers changed from: protected */
    public final Ad b;
    protected final AdPreferences c;
    /* access modifiers changed from: protected */
    public final b d;
    protected AdPreferences.Placement e;
    /* access modifiers changed from: protected */
    public String f = null;

    public d(Context context, Ad ad, AdPreferences adPreferences, b bVar, AdPreferences.Placement placement) {
        this.f6335a = context;
        this.b = ad;
        this.c = adPreferences;
        this.d = bVar;
        this.e = placement;
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        b(bool);
        if (!bool.booleanValue()) {
            this.b.setErrorMessage(this.f);
            this.d.b(this.b);
        }
    }

    /* access modifiers changed from: protected */
    public abstract boolean a(Object obj);

    /* access modifiers changed from: protected */
    public void b(Boolean bool) {
        this.b.setState(bool.booleanValue() ? Ad.AdState.READY : Ad.AdState.UN_INITIALIZED);
    }

    public final void c() {
        ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                Process.setThreadPriority(10);
                final Boolean d = d.this.d();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        d.this.a(d);
                    }
                });
            }
        });
    }

    /* access modifiers changed from: protected */
    public final Boolean d() {
        return Boolean.valueOf(a(e()));
    }

    /* access modifiers changed from: protected */
    public abstract Object e();

    /* access modifiers changed from: protected */
    public final AdPreferences.Placement f() {
        return this.e;
    }

    /* access modifiers changed from: protected */
    public final GetAdRequest b(GetAdRequest getAdRequest) {
        Pair<String, String> d2 = SimpleTokenUtils.d(this.f6335a);
        try {
            getAdRequest.a(this.f6335a, this.c, this.e, d2);
            getAdRequest.a(this.b.getConsentType(), this.b.getConsentTimestamp(), this.b.getConsentApc());
            if (!AdsCommonMetaData.a().E() && a.a(this.f6335a, this.e)) {
                getAdRequest.h();
            }
            try {
                getAdRequest.a(this.f6335a, this.c);
            } catch (Throwable th) {
                new e(th).a(this.f6335a);
            }
            return getAdRequest;
        } catch (Throwable th2) {
            new e(th2).a(this.f6335a);
            SimpleTokenUtils.a(d2);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    public GetAdRequest a() {
        GetAdRequest b2 = b(new GetAdRequest());
        if (b2 != null) {
            b2.a(this.f6335a);
        }
        return b2;
    }
}
