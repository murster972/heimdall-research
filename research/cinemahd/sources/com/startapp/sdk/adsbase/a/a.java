package com.startapp.sdk.adsbase.a;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

public class a implements Application.ActivityLifecycleCallbacks {

    /* renamed from: a  reason: collision with root package name */
    private final b f6266a;
    private int b;
    private boolean c;
    private boolean d;

    static {
        Class<a> cls = a.class;
    }

    public a(b bVar) {
        this.f6266a = bVar;
    }

    public void onActivityCreated(Activity activity, Bundle bundle) {
    }

    public void onActivityDestroyed(Activity activity) {
    }

    public void onActivityPaused(Activity activity) {
    }

    public void onActivityResumed(Activity activity) {
    }

    public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
    }

    public void onActivityStarted(Activity activity) {
        if (activity != null) {
            this.b++;
            if (this.b == 1 && !this.c) {
                if (!this.d) {
                    this.d = true;
                    this.f6266a.b();
                }
                this.f6266a.c();
            }
        }
    }

    public void onActivityStopped(Activity activity) {
        if (activity != null) {
            this.b--;
            this.c = activity.isChangingConfigurations();
            if (this.b == 0 && !this.c) {
                this.f6266a.d();
            }
        }
    }
}
