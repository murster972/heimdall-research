package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.splash.SplashMetaData;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.adinformation.AdInformationMetaData;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;

public class a {

    /* renamed from: a  reason: collision with root package name */
    protected boolean f6427a = false;
    private final Context b;
    private final AdPreferences c;
    private MetaDataRequest.RequestReason d;
    private MetaData e = null;
    private BannerMetaData f = null;
    private SplashMetaData g = null;
    private CacheMetaData h = null;
    private AdInformationMetaData i = null;
    private AdsCommonMetaData j = null;
    private boolean k = false;

    public a(Context context, AdPreferences adPreferences, MetaDataRequest.RequestReason requestReason) {
        this.b = context;
        this.c = adPreferences;
        this.d = requestReason;
    }

    public final void a() {
        ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                final Boolean c = a.this.c();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        a.this.a(c);
                    }
                });
            }
        });
    }

    public final void b() {
        this.k = true;
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:13|14|(23:20|21|22|23|(1:25)|29|30|31|(1:33)|37|38|39|(1:41)|45|46|47|(1:49)|53|54|55|(1:57)|61|62)|63|64) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:63:0x0161 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Boolean c() {
        /*
            r4 = this;
            com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest r0 = new com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest
            android.content.Context r1 = r4.b
            com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest$RequestReason r2 = r4.d
            r0.<init>(r1, r2)
            android.content.Context r1 = r4.b     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.model.AdPreferences r2 = r4.c     // Catch:{ all -> 0x0168 }
            r0.a(r1, r2)     // Catch:{ all -> 0x0168 }
            android.content.Context r1 = r4.b     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.c.c r1 = com.startapp.sdk.c.c.a(r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.d.b r1 = r1.l()     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.AdsConstants$ServiceApiType r2 = com.startapp.sdk.adsbase.AdsConstants.ServiceApiType.METADATA     // Catch:{ all -> 0x0168 }
            java.lang.String r2 = com.startapp.sdk.adsbase.AdsConstants.a(r2)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.d.a r1 = r1.a((java.lang.String) r2)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.d.a r0 = r1.a((com.startapp.sdk.adsbase.c) r0)     // Catch:{ all -> 0x0168 }
            com.startapp.common.b.e$a r0 = r0.a()     // Catch:{ all -> 0x0168 }
            if (r0 != 0) goto L_0x0031
            java.lang.Boolean r0 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0168 }
            return r0
        L_0x0031:
            java.lang.String r0 = r0.a()     // Catch:{ all -> 0x0168 }
            if (r0 != 0) goto L_0x003a
            java.lang.Boolean r0 = java.lang.Boolean.FALSE     // Catch:{ all -> 0x0168 }
            return r0
        L_0x003a:
            java.lang.Class<com.startapp.sdk.adsbase.remoteconfig.MetaData> r1 = com.startapp.sdk.adsbase.remoteconfig.MetaData.class
            java.lang.Object r1 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.remoteconfig.MetaData r1 = (com.startapp.sdk.adsbase.remoteconfig.MetaData) r1     // Catch:{ all -> 0x0168 }
            r4.e = r1     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.j.u.a()     // Catch:{ all -> 0x0168 }
            java.lang.Class<com.startapp.sdk.adsbase.AdsCommonMetaData> r1 = com.startapp.sdk.adsbase.AdsCommonMetaData.class
            java.lang.Object r1 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.AdsCommonMetaData r1 = (com.startapp.sdk.adsbase.AdsCommonMetaData) r1     // Catch:{ all -> 0x0168 }
            r4.j = r1     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.j.u.b()     // Catch:{ all -> 0x0168 }
            java.lang.Class<com.startapp.sdk.ads.banner.BannerMetaData> r1 = com.startapp.sdk.ads.banner.BannerMetaData.class
            java.lang.Object r1 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.ads.banner.BannerMetaData r1 = (com.startapp.sdk.ads.banner.BannerMetaData) r1     // Catch:{ all -> 0x0168 }
            r4.f = r1     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.j.u.b()     // Catch:{ all -> 0x0168 }
            java.lang.Class<com.startapp.sdk.ads.splash.SplashMetaData> r1 = com.startapp.sdk.ads.splash.SplashMetaData.class
            java.lang.Object r1 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.ads.splash.SplashMetaData r1 = (com.startapp.sdk.ads.splash.SplashMetaData) r1     // Catch:{ all -> 0x0168 }
            r4.g = r1     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.j.u.b()     // Catch:{ all -> 0x0168 }
            java.lang.Class<com.startapp.sdk.adsbase.cache.CacheMetaData> r1 = com.startapp.sdk.adsbase.cache.CacheMetaData.class
            java.lang.Object r1 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.cache.CacheMetaData r1 = (com.startapp.sdk.adsbase.cache.CacheMetaData) r1     // Catch:{ all -> 0x0168 }
            r4.h = r1     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.j.u.c()     // Catch:{ all -> 0x0168 }
            java.lang.Class<com.startapp.sdk.adsbase.adinformation.AdInformationMetaData> r1 = com.startapp.sdk.adsbase.adinformation.AdInformationMetaData.class
            java.lang.Object r0 = com.startapp.sdk.adsbase.j.u.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0168 }
            com.startapp.sdk.adsbase.adinformation.AdInformationMetaData r0 = (com.startapp.sdk.adsbase.adinformation.AdInformationMetaData) r0     // Catch:{ all -> 0x0168 }
            r4.i = r0     // Catch:{ all -> 0x0168 }
            java.lang.Object r0 = com.startapp.sdk.adsbase.remoteconfig.MetaData.i()
            monitor-enter(r0)
            boolean r1 = r4.k     // Catch:{ all -> 0x0165 }
            if (r1 != 0) goto L_0x0161
            com.startapp.sdk.adsbase.remoteconfig.MetaData r1 = r4.e     // Catch:{ all -> 0x0165 }
            if (r1 == 0) goto L_0x0161
            android.content.Context r1 = r4.b     // Catch:{ all -> 0x0165 }
            if (r1 == 0) goto L_0x0161
            com.startapp.sdk.adsbase.j.u.a()     // Catch:{ all -> 0x0165 }
            r1 = 1
            com.startapp.sdk.adsbase.AdsCommonMetaData r2 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()     // Catch:{ all -> 0x00b0 }
            com.startapp.sdk.adsbase.AdsCommonMetaData r3 = r4.j     // Catch:{ all -> 0x00b0 }
            boolean r2 = com.startapp.sdk.adsbase.j.u.b(r2, r3)     // Catch:{ all -> 0x00b0 }
            if (r2 != 0) goto L_0x00bb
            r4.f6427a = r1     // Catch:{ all -> 0x00b0 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x00b0 }
            com.startapp.sdk.adsbase.AdsCommonMetaData r3 = r4.j     // Catch:{ all -> 0x00b0 }
            com.startapp.sdk.adsbase.AdsCommonMetaData.a(r2, r3)     // Catch:{ all -> 0x00b0 }
            goto L_0x00bb
        L_0x00b0:
            r2 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0165 }
            r3.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0165 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x0165 }
            r3.a((android.content.Context) r2)     // Catch:{ all -> 0x0165 }
        L_0x00bb:
            com.startapp.sdk.adsbase.j.u.b()     // Catch:{ all -> 0x0165 }
            com.startapp.sdk.ads.banner.BannerMetaData r2 = com.startapp.sdk.ads.banner.BannerMetaData.a()     // Catch:{ all -> 0x00d4 }
            com.startapp.sdk.ads.banner.BannerMetaData r3 = r4.f     // Catch:{ all -> 0x00d4 }
            boolean r2 = com.startapp.sdk.adsbase.j.u.b(r2, r3)     // Catch:{ all -> 0x00d4 }
            if (r2 != 0) goto L_0x00df
            r4.f6427a = r1     // Catch:{ all -> 0x00d4 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x00d4 }
            com.startapp.sdk.ads.banner.BannerMetaData r3 = r4.f     // Catch:{ all -> 0x00d4 }
            com.startapp.sdk.ads.banner.BannerMetaData.a(r2, r3)     // Catch:{ all -> 0x00d4 }
            goto L_0x00df
        L_0x00d4:
            r2 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0165 }
            r3.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0165 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x0165 }
            r3.a((android.content.Context) r2)     // Catch:{ all -> 0x0165 }
        L_0x00df:
            com.startapp.sdk.adsbase.j.u.b()     // Catch:{ all -> 0x0165 }
            com.startapp.sdk.ads.splash.SplashMetaData r2 = r4.g     // Catch:{ all -> 0x0165 }
            com.startapp.sdk.ads.splash.SplashConfig r2 = r2.a()     // Catch:{ all -> 0x0165 }
            android.content.Context r3 = r4.b     // Catch:{ all -> 0x0165 }
            r2.setDefaults(r3)     // Catch:{ all -> 0x0165 }
            com.startapp.sdk.ads.splash.SplashMetaData r2 = com.startapp.sdk.ads.splash.SplashMetaData.b()     // Catch:{ all -> 0x0103 }
            com.startapp.sdk.ads.splash.SplashMetaData r3 = r4.g     // Catch:{ all -> 0x0103 }
            boolean r2 = com.startapp.sdk.adsbase.j.u.b(r2, r3)     // Catch:{ all -> 0x0103 }
            if (r2 != 0) goto L_0x010e
            r4.f6427a = r1     // Catch:{ all -> 0x0103 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x0103 }
            com.startapp.sdk.ads.splash.SplashMetaData r3 = r4.g     // Catch:{ all -> 0x0103 }
            com.startapp.sdk.ads.splash.SplashMetaData.a(r2, r3)     // Catch:{ all -> 0x0103 }
            goto L_0x010e
        L_0x0103:
            r2 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0165 }
            r3.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0165 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x0165 }
            r3.a((android.content.Context) r2)     // Catch:{ all -> 0x0165 }
        L_0x010e:
            com.startapp.sdk.adsbase.j.u.b()     // Catch:{ all -> 0x0165 }
            com.startapp.sdk.adsbase.cache.CacheMetaData r2 = com.startapp.sdk.adsbase.cache.CacheMetaData.a()     // Catch:{ all -> 0x0127 }
            com.startapp.sdk.adsbase.cache.CacheMetaData r3 = r4.h     // Catch:{ all -> 0x0127 }
            boolean r2 = com.startapp.sdk.adsbase.j.u.b(r2, r3)     // Catch:{ all -> 0x0127 }
            if (r2 != 0) goto L_0x0132
            r4.f6427a = r1     // Catch:{ all -> 0x0127 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x0127 }
            com.startapp.sdk.adsbase.cache.CacheMetaData r3 = r4.h     // Catch:{ all -> 0x0127 }
            com.startapp.sdk.adsbase.cache.CacheMetaData.a(r2, r3)     // Catch:{ all -> 0x0127 }
            goto L_0x0132
        L_0x0127:
            r2 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0165 }
            r3.<init>((java.lang.Throwable) r2)     // Catch:{ all -> 0x0165 }
            android.content.Context r2 = r4.b     // Catch:{ all -> 0x0165 }
            r3.a((android.content.Context) r2)     // Catch:{ all -> 0x0165 }
        L_0x0132:
            com.startapp.sdk.adsbase.j.u.c()     // Catch:{ all -> 0x0165 }
            com.startapp.sdk.adsbase.adinformation.AdInformationMetaData r2 = com.startapp.sdk.adsbase.adinformation.AdInformationMetaData.b()     // Catch:{ all -> 0x014b }
            com.startapp.sdk.adsbase.adinformation.AdInformationMetaData r3 = r4.i     // Catch:{ all -> 0x014b }
            boolean r2 = com.startapp.sdk.adsbase.j.u.b(r2, r3)     // Catch:{ all -> 0x014b }
            if (r2 != 0) goto L_0x0156
            r4.f6427a = r1     // Catch:{ all -> 0x014b }
            android.content.Context r1 = r4.b     // Catch:{ all -> 0x014b }
            com.startapp.sdk.adsbase.adinformation.AdInformationMetaData r2 = r4.i     // Catch:{ all -> 0x014b }
            com.startapp.sdk.adsbase.adinformation.AdInformationMetaData.a(r1, r2)     // Catch:{ all -> 0x014b }
            goto L_0x0156
        L_0x014b:
            r1 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r2 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0165 }
            r2.<init>((java.lang.Throwable) r1)     // Catch:{ all -> 0x0165 }
            android.content.Context r1 = r4.b     // Catch:{ all -> 0x0165 }
            r2.a((android.content.Context) r1)     // Catch:{ all -> 0x0165 }
        L_0x0156:
            android.content.Context r1 = r4.b     // Catch:{ Exception -> 0x0161 }
            com.startapp.sdk.adsbase.remoteconfig.MetaData r2 = r4.e     // Catch:{ Exception -> 0x0161 }
            java.lang.String r2 = r2.l()     // Catch:{ Exception -> 0x0161 }
            com.startapp.sdk.adsbase.remoteconfig.MetaData.a((android.content.Context) r1, (java.lang.String) r2)     // Catch:{ Exception -> 0x0161 }
        L_0x0161:
            monitor-exit(r0)     // Catch:{ all -> 0x0165 }
            java.lang.Boolean r0 = java.lang.Boolean.TRUE
            return r0
        L_0x0165:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0165 }
            throw r1
        L_0x0168:
            r0 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r1 = new com.startapp.sdk.adsbase.infoevents.e
            r1.<init>((java.lang.Throwable) r0)
            android.content.Context r0 = r4.b
            r1.a((android.content.Context) r0)
            java.lang.Boolean r0 = java.lang.Boolean.FALSE
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.remoteconfig.a.c():java.lang.Boolean");
    }

    /* access modifiers changed from: protected */
    public void a(Boolean bool) {
        synchronized (MetaData.i()) {
            if (!this.k) {
                if (!bool.booleanValue() || this.e == null || this.b == null) {
                    MetaData.h();
                } else {
                    try {
                        MetaData.a(this.b, this.e, this.d, this.f6427a);
                    } catch (Throwable th) {
                        new e(th).a(this.b);
                    }
                }
            }
        }
    }
}
