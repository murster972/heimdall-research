package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RscMetadata implements Serializable {
    private static final long serialVersionUID = 1389056033245684328L;
    private boolean enabled;
    private int ief;
    @d(b = ArrayList.class, c = RscMetadataItem.class)
    private List<RscMetadataItem> items;
    private String triggers;

    public final boolean a() {
        return this.enabled;
    }

    public final String b() {
        return this.triggers;
    }

    public final List<RscMetadataItem> c() {
        return this.items;
    }

    public final int d() {
        return this.ief;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && RscMetadata.class == obj.getClass()) {
            RscMetadata rscMetadata = (RscMetadata) obj;
            return this.enabled == rscMetadata.enabled && this.ief == rscMetadata.ief && u.b(this.triggers, rscMetadata.triggers) && u.b(this.items, rscMetadata.items);
        }
    }

    public int hashCode() {
        return u.a(Boolean.valueOf(this.enabled), this.triggers, this.items, Integer.valueOf(this.ief));
    }

    public final int a(RscMetadataItem rscMetadataItem) {
        return rscMetadataItem.i() != null ? rscMetadataItem.i().intValue() : this.ief;
    }
}
