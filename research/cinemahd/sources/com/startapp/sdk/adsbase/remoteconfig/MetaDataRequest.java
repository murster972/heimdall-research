package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Pair;
import com.startapp.common.SDKException;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.c;
import com.startapp.sdk.adsbase.consent.a;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.m;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.k;

public final class MetaDataRequest extends c {
    private int b;
    private int c;
    private boolean d;
    private float e;
    private RequestReason f;
    private String g = MetaData.G().C();
    private String h;
    private Integer i;
    private Pair<String, String> j;
    private Integer k;
    private Boolean l;
    private long m;

    public enum RequestReason {
        LAUNCH(1),
        APP_IDLE(2),
        IN_APP_PURCHASE(3),
        CUSTOM(4),
        PERIODIC(5),
        PAS(6),
        CONSENT(7);
        
        private int index;

        private RequestReason(int i) {
            this.index = i;
        }
    }

    public MetaDataRequest(Context context, RequestReason requestReason) {
        super(2);
        int f2;
        this.b = j.a(context, "totalSessions", (Integer) 0).intValue();
        this.c = (int) ((System.currentTimeMillis() - j.a(context, "firstSessionTime", Long.valueOf(System.currentTimeMillis())).longValue()) / 86400000);
        this.e = j.a(context, "inAppPurchaseAmount", Float.valueOf(0.0f)).floatValue();
        this.d = j.a(context, "payingUser", Boolean.FALSE).booleanValue();
        this.f = requestReason;
        SharedPreferences a2 = j.a(context);
        boolean g2 = k.a().g();
        new u();
        String string = a2.getString("shared_prefs_app_apk_hash", (String) null);
        if (TextUtils.isEmpty(string) || g2) {
            string = u.a("SHA-256", context);
            a2.edit().putString("shared_prefs_app_apk_hash", string).commit();
        }
        this.h = string;
        SimpleTokenConfig f3 = MetaData.G().f();
        if (f3 != null && f3.a(context) && (f2 = b.f(context)) > 0) {
            this.i = Integer.valueOf(f2);
        }
        this.j = SimpleTokenUtils.c();
        this.m = SimpleTokenUtils.a();
        a f4 = com.startapp.sdk.c.c.a(context).f();
        this.k = f4.d();
        this.l = f4.f();
    }

    /* access modifiers changed from: protected */
    public final void a(m mVar) throws SDKException {
        super.a(mVar);
        mVar.a(com.startapp.common.b.a.a(), com.startapp.common.b.a.d(), true);
        mVar.a("totalSessions", Integer.valueOf(this.b), true);
        mVar.a("daysSinceFirstSession", Integer.valueOf(this.c), true);
        mVar.a("payingUser", Boolean.valueOf(this.d), true);
        mVar.a("profileId", this.g, false);
        mVar.a("paidAmount", Float.valueOf(this.e), true);
        mVar.a("reason", this.f, true);
        mVar.a("ct", this.k, false);
        mVar.a("apc", this.l, false);
        mVar.a("testAdsEnabled", k.a().o() ? Boolean.TRUE : null, false);
        mVar.a("apkHash", this.h, false);
        mVar.a("ian", this.i, false);
        Pair<String, String> pair = this.j;
        mVar.a((String) pair.first, pair.second, false);
        if (Build.VERSION.SDK_INT >= 9) {
            long j2 = this.m;
            if (j2 != 0) {
                mVar.a("firstInstalledAppTS", Long.valueOf(j2), false);
            }
        }
    }

    public final String toString() {
        return "MetaDataRequest [totalSessions=" + this.b + ", daysSinceFirstSession=" + this.c + ", payingUser=" + this.d + ", paidAmount=" + this.e + ", reason=" + this.f + ", ct=" + this.k + ", apc=" + this.l + ", profileId=" + this.g + "]";
    }
}
