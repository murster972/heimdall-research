package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;
import java.util.Arrays;

public class RscMetadataItem implements Serializable {
    private static final long serialVersionUID = 1691586261519008915L;
    private String config;
    private Integer ief;
    private Integer limit;
    private int noCache;
    private Integer output;
    private Integer ppid;
    private int[] sortBy;
    private int triggers;
    private Integer ttl;

    public final String a() {
        return this.config;
    }

    public final int b() {
        return this.triggers;
    }

    public final int c() {
        return this.noCache;
    }

    public final Integer d() {
        return this.ttl;
    }

    public final int[] e() {
        return this.sortBy;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && RscMetadataItem.class == obj.getClass()) {
            RscMetadataItem rscMetadataItem = (RscMetadataItem) obj;
            return this.triggers == rscMetadataItem.triggers && this.noCache == rscMetadataItem.noCache && u.b(this.config, rscMetadataItem.config) && u.b(this.ttl, rscMetadataItem.ttl) && Arrays.equals(this.sortBy, rscMetadataItem.sortBy) && u.b(this.limit, rscMetadataItem.limit) && u.b(this.ppid, rscMetadataItem.ppid) && u.b(this.output, rscMetadataItem.output) && u.b(this.ief, rscMetadataItem.ief);
        }
    }

    public final Integer f() {
        return this.limit;
    }

    public final Integer g() {
        return this.ppid;
    }

    public final Integer h() {
        return this.output;
    }

    public int hashCode() {
        return u.a(this.config, Integer.valueOf(this.triggers), Integer.valueOf(this.noCache), this.ttl, this.sortBy, this.limit, this.ppid, this.output, this.ief);
    }

    public final Integer i() {
        return this.ief;
    }
}
