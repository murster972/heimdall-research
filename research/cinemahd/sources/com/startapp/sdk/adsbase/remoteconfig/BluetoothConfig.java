package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.c.c;
import java.io.Serializable;
import org.joda.time.DateTimeConstants;

public class BluetoothConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private int discoveryIntervalInMinutes = DateTimeConstants.MINUTES_PER_DAY;
    private boolean enabled = false;
    private int timeoutInSec = 20;

    public final int a() {
        return this.timeoutInSec;
    }

    public final int b() {
        return this.discoveryIntervalInMinutes;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && BluetoothConfig.class == obj.getClass()) {
            BluetoothConfig bluetoothConfig = (BluetoothConfig) obj;
            return this.timeoutInSec == bluetoothConfig.timeoutInSec && this.enabled == bluetoothConfig.enabled && this.discoveryIntervalInMinutes == bluetoothConfig.discoveryIntervalInMinutes;
        }
    }

    public int hashCode() {
        return u.a(Integer.valueOf(this.timeoutInSec), Boolean.valueOf(this.enabled), Integer.valueOf(this.discoveryIntervalInMinutes));
    }

    public final boolean a(Context context) {
        return this.enabled && c.a(context).f().g();
    }
}
