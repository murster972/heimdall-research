package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class LocationConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean coEnabled = false;
    private boolean fiEnabled = false;
    private int ief = 0;
    private double iep = 0.0d;

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && LocationConfig.class == obj.getClass()) {
            LocationConfig locationConfig = (LocationConfig) obj;
            return this.fiEnabled == locationConfig.fiEnabled && this.coEnabled == locationConfig.coEnabled && this.ief == locationConfig.ief && Double.compare(this.iep, locationConfig.iep) == 0;
        }
    }

    public int hashCode() {
        return u.a(Boolean.valueOf(this.fiEnabled), Boolean.valueOf(this.coEnabled), Integer.valueOf(this.ief), Double.valueOf(this.iep));
    }
}
