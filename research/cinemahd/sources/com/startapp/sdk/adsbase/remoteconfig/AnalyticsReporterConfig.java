package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class AnalyticsReporterConfig implements Serializable {
    private static final long serialVersionUID = 2491016904331040762L;
    private int ief = 0;
    private double iep = 0.0d;
    private int initInterval = 480;

    public final int a() {
        return this.ief;
    }

    public final double b() {
        return this.iep;
    }

    public final int c() {
        return this.initInterval;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && AnalyticsReporterConfig.class == obj.getClass()) {
            AnalyticsReporterConfig analyticsReporterConfig = (AnalyticsReporterConfig) obj;
            return this.initInterval == analyticsReporterConfig.initInterval && this.ief == analyticsReporterConfig.ief && Double.compare(this.iep, analyticsReporterConfig.iep) == 0;
        }
    }

    public int hashCode() {
        return u.a(Integer.valueOf(this.initInterval), Integer.valueOf(this.ief), Double.valueOf(this.iep));
    }
}
