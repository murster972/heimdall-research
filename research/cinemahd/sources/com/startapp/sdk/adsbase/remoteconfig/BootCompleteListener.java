package com.startapp.sdk.adsbase.remoteconfig;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import com.startapp.sdk.adsbase.j.e;
import com.startapp.sdk.adsbase.k;

public class BootCompleteListener extends BroadcastReceiver {
    static {
        Class<BootCompleteListener> cls = BootCompleteListener.class;
    }

    public void onReceive(Context context, Intent intent) {
        try {
            long elapsedRealtime = SystemClock.elapsedRealtime() + 60000;
            e.a(context);
            e.a(context, Long.valueOf(elapsedRealtime));
            e.a(context, elapsedRealtime);
            k.b(context.getApplicationContext());
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
        }
    }
}
