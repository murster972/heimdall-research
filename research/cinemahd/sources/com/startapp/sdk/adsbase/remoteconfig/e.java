package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import com.startapp.common.c;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;

public class e implements RunnerJob {
    public final void a(Context context, RunnerJob.a aVar) {
        try {
            c.b(context);
            MetaData.a(context);
            if (MetaData.G().m()) {
                final AdPreferences adPreferences = new AdPreferences();
                final Context context2 = context;
                final RunnerJob.a aVar2 = aVar;
                new a(context, adPreferences, MetaDataRequest.RequestReason.PERIODIC) {
                    private MetaData b;

                    /* access modifiers changed from: protected */
                    public final void a(Boolean bool) {
                        try {
                            if (!(!bool.booleanValue() || this.b == null || context2 == null)) {
                                MetaData.a(context2, this.b, MetaDataRequest.RequestReason.PERIODIC, this.f6427a);
                            }
                            com.startapp.sdk.adsbase.j.e.c(context2);
                            if (aVar2 != null) {
                                aVar2.a(RunnerJob.Result.SUCCESS);
                            }
                        } catch (Throwable th) {
                            new com.startapp.sdk.adsbase.infoevents.e(th).a(context2);
                        }
                    }

                    /* access modifiers changed from: protected */
                    public final Boolean c() {
                        try {
                            SimpleTokenUtils.b(context2);
                            MetaDataRequest metaDataRequest = new MetaDataRequest(context2, MetaDataRequest.RequestReason.PERIODIC);
                            metaDataRequest.a(context2, adPreferences);
                            this.b = (MetaData) com.startapp.sdk.c.c.a(context2).l().a(AdsConstants.a(AdsConstants.ServiceApiType.METADATA)).a((com.startapp.sdk.adsbase.c) metaDataRequest).a(MetaData.class);
                            return Boolean.TRUE;
                        } catch (Throwable th) {
                            new com.startapp.sdk.adsbase.infoevents.e(th).a(context2);
                            return Boolean.FALSE;
                        }
                    }
                }.a();
            }
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
        }
    }
}
