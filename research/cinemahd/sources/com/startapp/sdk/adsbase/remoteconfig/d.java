package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import com.startapp.common.c;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.sdk.adsbase.j.e;

public final class d implements RunnerJob {
    public final void a(final Context context, final RunnerJob.a aVar) {
        try {
            c.b(context);
            MetaData.a(context);
            MetaData.G().k();
            if (MetaData.G().o()) {
                com.startapp.sdk.c.c.a(context).k().a(true, new com.startapp.common.d() {
                    public final void a(Object obj) {
                        if (aVar != null) {
                            e.d(context);
                            aVar.a(RunnerJob.Result.SUCCESS);
                        }
                    }
                });
                return;
            }
            e.d(context);
            aVar.a(RunnerJob.Result.SUCCESS);
        } catch (Throwable th) {
            new com.startapp.sdk.adsbase.infoevents.e(th).a(context);
        }
    }
}
