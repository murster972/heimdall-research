package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import com.facebook.common.util.UriUtil;
import com.startapp.common.Constants;
import com.startapp.common.a;
import com.startapp.common.b.b;
import com.startapp.common.parser.d;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.consent.ConsentConfig;
import com.startapp.sdk.adsbase.d.c;
import com.startapp.sdk.adsbase.infoevents.AnalyticsConfig;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import com.startapp.sdk.insight.NetworkTestsMetaData;
import com.startapp.sdk.triggeredlinks.TriggeredLinksMetadata;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.joda.time.DateTimeConstants;

public class MetaData implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f6422a = new Object();
    private static final AtomicBoolean b = new AtomicBoolean();
    private static Set<String> c = new HashSet(Arrays.asList(new String[]{Constants.f5922a}));
    private static String d = "https://adsmetadata.startappservice.com/1.5/";
    private static String e = "https://req.startappservice.com/1.5/";
    private static int[] f = {60, 60, 240};
    private static Set<String> g = new HashSet(Arrays.asList(new String[]{"com.facebook.katana", "com.yandex.browser"}));
    private static volatile MetaData h = new MetaData();
    private static a k = null;
    private static final long serialVersionUID = 1;
    private long IABDisplayImpressionDelayInSeconds;
    private long IABVideoImpressionDelayInSeconds;
    @d(a = true)
    private SimpleTokenConfig SimpleToken = new SimpleTokenConfig();
    private boolean SupportIABViewability;
    private String adPlatformBannerHostSecured;
    public String adPlatformHostSecured;
    private String adPlatformNativeHostSecured;
    private String adPlatformOverlayHostSecured;
    private String adPlatformReturnHostSecured;
    private String adPlatformSplashHostSecured;
    private boolean alwaysSendToken;
    @d(a = true)
    public AnalyticsConfig analytics;
    private String assetsBaseUrlSecured;
    @d(a = true)
    private BluetoothConfig btConfig;
    private boolean chromeCustomeTabsExternal;
    private boolean chromeCustomeTabsInternal;
    private boolean compressionEnabled;
    @d(a = true)
    private ConsentConfig consentDetails;
    private boolean disableSendAdvertisingId;
    private boolean dns;
    private transient boolean i;
    private boolean inAppBrowser;
    @d(b = HashSet.class)
    private Set<String> installersList;
    @d(b = HashSet.class)
    private Set<Integer> invalidForRetry;
    private boolean isToken1Mandatory;
    private transient boolean j;
    private transient List<b> l;
    @d(a = true)
    private LocationConfig location;
    public String metaDataHostSecured = d;
    private String metadataUpdateVersion;
    @d(a = true)
    private NetworkDiagnosticConfig netDiag;
    @d(a = true)
    private NetworkTestsMetaData networkTests;
    private int notVisibleBannerReloadInterval;
    private boolean omSdkEnabled;
    private int[] periodicEventIntMin;
    private boolean periodicInfoEventEnabled;
    private boolean periodicMetaDataEnabled;
    private int periodicMetaDataIntervalInMinutes;
    private int periodicThresholdMin;
    @d(b = HashSet.class)
    private Set<String> preInstalledPackages;
    private String profileId;
    @d(a = true)
    private RscMetadata rsc;
    @d(a = true)
    private SensorsConfig sensorsConfig;
    private int sessionMaxBackgroundTime;
    private boolean simpleToken2;
    @d(a = true)
    private StaleDcConfig staleDc;
    private int stopAutoLoadAmount;
    private int stopAutoLoadPreCacheAmount;
    private String trackDownloadHost;
    @d(a = true)
    private TriggeredLinksMetadata triggeredLinks;
    private boolean trueNetEnabled;
    private long userAgentDelayInSeconds;
    private boolean userAgentEnabled;
    private String vastRecorderHost;
    private boolean webViewSecured;

    /* renamed from: com.startapp.sdk.adsbase.remoteconfig.MetaData$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6423a = new int[AdPreferences.Placement.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.startapp.sdk.adsbase.model.AdPreferences$Placement[] r0 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f6423a = r0
                int[] r0 = f6423a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_BANNER     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f6423a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_OVERLAY     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f6423a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_NATIVE     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f6423a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_RETURN     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f6423a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.startapp.sdk.adsbase.model.AdPreferences$Placement r1 = com.startapp.sdk.adsbase.model.AdPreferences.Placement.INAPP_SPLASH     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.remoteconfig.MetaData.AnonymousClass1.<clinit>():void");
        }
    }

    public static class a implements a.C0062a {

        /* renamed from: a  reason: collision with root package name */
        private Context f6424a;
        private String b;

        public a(Context context, String str) {
            this.f6424a = context;
            this.b = str;
        }

        public final void a(Bitmap bitmap, int i) {
            if (bitmap != null) {
                com.startapp.sdk.adsbase.j.a.a(this.f6424a, bitmap, this.b, ".png");
            }
        }
    }

    static {
        Class<MetaData> cls = MetaData.class;
    }

    public MetaData() {
        String str = e;
        this.adPlatformHostSecured = str;
        this.trackDownloadHost = str;
        this.sessionMaxBackgroundTime = 1800;
        this.profileId = null;
        this.installersList = c;
        this.preInstalledPackages = g;
        this.simpleToken2 = true;
        this.alwaysSendToken = true;
        this.isToken1Mandatory = true;
        this.compressionEnabled = false;
        this.periodicMetaDataEnabled = false;
        this.periodicMetaDataIntervalInMinutes = 360;
        this.periodicInfoEventEnabled = false;
        this.periodicEventIntMin = f;
        this.periodicThresholdMin = 5;
        this.inAppBrowser = true;
        this.SupportIABViewability = true;
        this.IABDisplayImpressionDelayInSeconds = 1;
        this.IABVideoImpressionDelayInSeconds = 2;
        this.userAgentDelayInSeconds = 5;
        this.userAgentEnabled = true;
        this.sensorsConfig = new SensorsConfig();
        this.btConfig = new BluetoothConfig();
        this.assetsBaseUrlSecured = "";
        this.invalidForRetry = c.f6340a;
        this.notVisibleBannerReloadInterval = DateTimeConstants.SECONDS_PER_HOUR;
        this.analytics = new AnalyticsConfig();
        this.location = new LocationConfig();
        this.i = false;
        this.j = false;
        this.l = new ArrayList();
        this.metadataUpdateVersion = "4.6.3";
        this.dns = false;
        this.stopAutoLoadAmount = 3;
        this.stopAutoLoadPreCacheAmount = 3;
        this.trueNetEnabled = false;
        this.webViewSecured = true;
        this.omSdkEnabled = false;
        this.chromeCustomeTabsInternal = true;
        this.chromeCustomeTabsExternal = true;
        this.disableSendAdvertisingId = false;
        this.networkTests = new NetworkTestsMetaData();
        this.staleDc = new StaleDcConfig();
    }

    public static MetaData G() {
        return h;
    }

    private void T() {
        this.adPlatformHostSecured = a(this.adPlatformHostSecured, e);
        this.metaDataHostSecured = a(this.metaDataHostSecured, d);
        this.adPlatformBannerHostSecured = a(this.adPlatformBannerHostSecured, (String) null);
        this.adPlatformSplashHostSecured = a(this.adPlatformSplashHostSecured, (String) null);
        this.adPlatformReturnHostSecured = a(this.adPlatformReturnHostSecured, (String) null);
        this.adPlatformOverlayHostSecured = a(this.adPlatformOverlayHostSecured, (String) null);
        this.adPlatformNativeHostSecured = a(this.adPlatformNativeHostSecured, (String) null);
    }

    public static void h() {
        ArrayList<b> arrayList;
        synchronized (f6422a) {
            if (h.l != null) {
                arrayList = new ArrayList<>(h.l);
                h.l.clear();
            } else {
                arrayList = null;
            }
            h.i = false;
        }
        if (arrayList != null) {
            for (b a2 : arrayList) {
                a2.a();
            }
        }
    }

    public static Object i() {
        return f6422a;
    }

    public final boolean A() {
        return this.compressionEnabled;
    }

    public final boolean B() {
        u.b();
        return this.inAppBrowser;
    }

    public final String C() {
        return this.profileId;
    }

    public final SensorsConfig D() {
        return this.sensorsConfig;
    }

    public final BluetoothConfig E() {
        return this.btConfig;
    }

    public final int F() {
        return this.notVisibleBannerReloadInterval;
    }

    public final long H() {
        return this.IABDisplayImpressionDelayInSeconds;
    }

    public final long I() {
        return this.IABVideoImpressionDelayInSeconds;
    }

    public final long J() {
        return this.userAgentDelayInSeconds;
    }

    public final boolean K() {
        return this.userAgentEnabled;
    }

    public final boolean L() {
        return this.SupportIABViewability;
    }

    public final boolean M() {
        return !this.dns;
    }

    public final int N() {
        return this.stopAutoLoadAmount;
    }

    public final int O() {
        return this.stopAutoLoadPreCacheAmount;
    }

    public final boolean P() {
        return this.chromeCustomeTabsInternal;
    }

    public final boolean Q() {
        return this.chromeCustomeTabsExternal;
    }

    public final boolean R() {
        return this.disableSendAdvertisingId;
    }

    public final boolean S() {
        return this.omSdkEnabled;
    }

    public final NetworkDiagnosticConfig a() {
        return this.netDiag;
    }

    public final RscMetadata b() {
        return this.rsc;
    }

    public final NetworkTestsMetaData c() {
        return this.networkTests;
    }

    public final TriggeredLinksMetadata d() {
        return this.triggeredLinks;
    }

    public final StaleDcConfig e() {
        return this.staleDc;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && MetaData.class == obj.getClass()) {
            MetaData metaData = (MetaData) obj;
            return this.sessionMaxBackgroundTime == metaData.sessionMaxBackgroundTime && this.simpleToken2 == metaData.simpleToken2 && this.alwaysSendToken == metaData.alwaysSendToken && this.isToken1Mandatory == metaData.isToken1Mandatory && this.compressionEnabled == metaData.compressionEnabled && this.periodicMetaDataEnabled == metaData.periodicMetaDataEnabled && this.periodicMetaDataIntervalInMinutes == metaData.periodicMetaDataIntervalInMinutes && this.periodicInfoEventEnabled == metaData.periodicInfoEventEnabled && this.periodicThresholdMin == metaData.periodicThresholdMin && this.inAppBrowser == metaData.inAppBrowser && this.SupportIABViewability == metaData.SupportIABViewability && this.IABDisplayImpressionDelayInSeconds == metaData.IABDisplayImpressionDelayInSeconds && this.IABVideoImpressionDelayInSeconds == metaData.IABVideoImpressionDelayInSeconds && this.userAgentDelayInSeconds == metaData.userAgentDelayInSeconds && this.userAgentEnabled == metaData.userAgentEnabled && this.notVisibleBannerReloadInterval == metaData.notVisibleBannerReloadInterval && this.dns == metaData.dns && this.stopAutoLoadAmount == metaData.stopAutoLoadAmount && this.stopAutoLoadPreCacheAmount == metaData.stopAutoLoadPreCacheAmount && this.trueNetEnabled == metaData.trueNetEnabled && this.webViewSecured == metaData.webViewSecured && this.omSdkEnabled == metaData.omSdkEnabled && this.chromeCustomeTabsInternal == metaData.chromeCustomeTabsInternal && this.chromeCustomeTabsExternal == metaData.chromeCustomeTabsExternal && this.disableSendAdvertisingId == metaData.disableSendAdvertisingId && u.b(this.SimpleToken, metaData.SimpleToken) && u.b(this.consentDetails, metaData.consentDetails) && u.b(this.metaDataHostSecured, metaData.metaDataHostSecured) && u.b(this.adPlatformHostSecured, metaData.adPlatformHostSecured) && u.b(this.trackDownloadHost, metaData.trackDownloadHost) && u.b(this.adPlatformBannerHostSecured, metaData.adPlatformBannerHostSecured) && u.b(this.adPlatformSplashHostSecured, metaData.adPlatformSplashHostSecured) && u.b(this.adPlatformReturnHostSecured, metaData.adPlatformReturnHostSecured) && u.b(this.adPlatformOverlayHostSecured, metaData.adPlatformOverlayHostSecured) && u.b(this.adPlatformNativeHostSecured, metaData.adPlatformNativeHostSecured) && u.b(this.vastRecorderHost, metaData.vastRecorderHost) && u.b(this.profileId, metaData.profileId) && u.b(this.installersList, metaData.installersList) && u.b(this.preInstalledPackages, metaData.preInstalledPackages) && Arrays.equals(this.periodicEventIntMin, metaData.periodicEventIntMin) && u.b(this.sensorsConfig, metaData.sensorsConfig) && u.b(this.btConfig, metaData.btConfig) && u.b(this.assetsBaseUrlSecured, metaData.assetsBaseUrlSecured) && u.b(this.invalidForRetry, metaData.invalidForRetry) && u.b(this.analytics, metaData.analytics) && u.b(this.location, metaData.location) && u.b(this.metadataUpdateVersion, metaData.metadataUpdateVersion) && u.b(this.networkTests, metaData.networkTests) && u.b(this.triggeredLinks, metaData.triggeredLinks) && u.b(this.rsc, metaData.rsc) && u.b(this.netDiag, metaData.netDiag) && u.b(this.staleDc, metaData.staleDc);
        }
    }

    public final SimpleTokenConfig f() {
        return this.SimpleToken;
    }

    public final ConsentConfig g() {
        return this.consentDetails;
    }

    public int hashCode() {
        return u.a(this.SimpleToken, this.consentDetails, this.metaDataHostSecured, this.adPlatformHostSecured, this.trackDownloadHost, this.adPlatformBannerHostSecured, this.adPlatformSplashHostSecured, this.adPlatformReturnHostSecured, this.adPlatformOverlayHostSecured, this.adPlatformNativeHostSecured, this.vastRecorderHost, Integer.valueOf(this.sessionMaxBackgroundTime), this.profileId, this.installersList, this.preInstalledPackages, Boolean.valueOf(this.simpleToken2), Boolean.valueOf(this.alwaysSendToken), Boolean.valueOf(this.isToken1Mandatory), Boolean.valueOf(this.compressionEnabled), Boolean.valueOf(this.periodicMetaDataEnabled), Integer.valueOf(this.periodicMetaDataIntervalInMinutes), Boolean.valueOf(this.periodicInfoEventEnabled), this.periodicEventIntMin, Integer.valueOf(this.periodicThresholdMin), Boolean.valueOf(this.inAppBrowser), Boolean.valueOf(this.SupportIABViewability), Long.valueOf(this.IABDisplayImpressionDelayInSeconds), Long.valueOf(this.IABVideoImpressionDelayInSeconds), Long.valueOf(this.userAgentDelayInSeconds), Boolean.valueOf(this.userAgentEnabled), this.sensorsConfig, this.btConfig, this.assetsBaseUrlSecured, this.invalidForRetry, Integer.valueOf(this.notVisibleBannerReloadInterval), this.analytics, this.location, this.metadataUpdateVersion, Boolean.valueOf(this.dns), Integer.valueOf(this.stopAutoLoadAmount), Integer.valueOf(this.stopAutoLoadPreCacheAmount), Boolean.valueOf(this.trueNetEnabled), Boolean.valueOf(this.webViewSecured), Boolean.valueOf(this.omSdkEnabled), Boolean.valueOf(this.chromeCustomeTabsInternal), Boolean.valueOf(this.chromeCustomeTabsExternal), Boolean.valueOf(this.disableSendAdvertisingId), this.networkTests, this.triggeredLinks, this.rsc, this.netDiag, this.staleDc);
    }

    public final boolean j() {
        return this.j;
    }

    public final void k() {
        this.j = true;
    }

    public final String l() {
        String str = this.assetsBaseUrlSecured;
        return str != null ? str : "";
    }

    public final boolean m() {
        return this.periodicMetaDataEnabled;
    }

    public final int n() {
        return this.periodicMetaDataIntervalInMinutes;
    }

    public final boolean o() {
        return this.periodicInfoEventEnabled;
    }

    public final int p() {
        return this.periodicThresholdMin;
    }

    public final Set<Integer> q() {
        return this.invalidForRetry;
    }

    public final String r() {
        String str = this.adPlatformHostSecured;
        return str != null ? str : e;
    }

    public final String s() {
        String str = this.trackDownloadHost;
        return str != null ? str : r();
    }

    public final String t() {
        return this.vastRecorderHost;
    }

    public final String u() {
        int indexOf;
        String r = h.r();
        String str = (Build.VERSION.SDK_INT > 26 || this.webViewSecured) ? UriUtil.HTTPS_SCHEME : UriUtil.HTTP_SCHEME;
        if (r.startsWith(str + "://") || (indexOf = r.indexOf(58)) == -1) {
            return r;
        }
        return str + r.substring(indexOf);
    }

    public final long v() {
        return TimeUnit.SECONDS.toMillis((long) this.sessionMaxBackgroundTime);
    }

    public final Set<String> w() {
        return this.installersList;
    }

    public final Set<String> x() {
        Set<String> set = this.preInstalledPackages;
        if (set == null) {
            set = g;
        }
        return Collections.unmodifiableSet(set);
    }

    public final boolean y() {
        return this.alwaysSendToken;
    }

    public final boolean z() {
        return this.isToken1Mandatory;
    }

    public static void a(Context context) {
        if (!b.getAndSet(true)) {
            MetaData metaData = (MetaData) com.startapp.common.b.d.a(context, "StartappMetadata");
            MetaData metaData2 = new MetaData();
            if (metaData != null) {
                boolean a2 = u.a(metaData, metaData2);
                if (!(true ^ "4.6.3".equals(metaData.metadataUpdateVersion)) && a2) {
                    new e(InfoEventCategory.ERROR).f("metadata_null").a(context);
                }
                metaData.i = false;
                metaData.j = false;
                metaData.l = new ArrayList();
                h = metaData;
            } else {
                h = metaData2;
            }
            h.T();
        }
    }

    public static boolean b(Context context) {
        return context.getFileStreamPath("StartappMetadata").exists();
    }

    public final int c(Context context) {
        int[] iArr = this.periodicEventIntMin;
        if (iArr == null || iArr.length < 3) {
            this.periodicEventIntMin = f;
        }
        if (b.a(context, "android.permission.ACCESS_FINE_LOCATION")) {
            int i2 = this.periodicEventIntMin[0];
            if (i2 <= 0) {
                return f[0];
            }
            return i2;
        } else if (!b.a(context, "android.permission.ACCESS_COARSE_LOCATION")) {
            return this.periodicEventIntMin[2];
        } else {
            int i3 = this.periodicEventIntMin[1];
            if (i3 <= 0) {
                return f[1];
            }
            return i3;
        }
    }

    public static void a(Context context, MetaData metaData, MetaDataRequest.RequestReason requestReason, boolean z) {
        ArrayList<b> arrayList;
        synchronized (f6422a) {
            if (h.l != null) {
                arrayList = new ArrayList<>(h.l);
                h.l.clear();
            } else {
                arrayList = null;
            }
            metaData.l = h.l;
            metaData.T();
            metaData.metadataUpdateVersion = "4.6.3";
            com.startapp.common.b.d.b(context, "StartappMetadata", metaData);
            metaData.i = false;
            metaData.j = true;
            if (!u.b(h, metaData)) {
                z = true;
            }
            h = metaData;
            if (u.g(context)) {
                try {
                    j.b(context, "totalSessions", Integer.valueOf(j.a(context, "totalSessions", (Integer) 0).intValue() + 1));
                } catch (Throwable th) {
                    new e(th).a(context);
                }
            }
            k = null;
        }
        if (arrayList != null) {
            for (b a2 : arrayList) {
                a2.a(requestReason, z);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x0015, code lost:
        if (r7 == false) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x0017, code lost:
        if (r8 == null) goto L_?;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0019, code lost:
        r8.a(r6, false);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:27:0x0047, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r4, com.startapp.sdk.adsbase.model.AdPreferences r5, com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest.RequestReason r6, boolean r7, com.startapp.sdk.adsbase.remoteconfig.b r8, boolean r9) {
        /*
            r3 = this;
            r0 = 0
            if (r7 != 0) goto L_0x0008
            if (r8 == 0) goto L_0x0008
            r8.a(r6, r0)
        L_0x0008:
            java.lang.Object r1 = f6422a
            monitor-enter(r1)
            com.startapp.sdk.adsbase.remoteconfig.MetaData r2 = h     // Catch:{ all -> 0x0048 }
            boolean r2 = r2.j     // Catch:{ all -> 0x0048 }
            if (r2 == 0) goto L_0x001d
            if (r9 == 0) goto L_0x0014
            goto L_0x001d
        L_0x0014:
            monitor-exit(r1)     // Catch:{ all -> 0x0048 }
            if (r7 == 0) goto L_0x001c
            if (r8 == 0) goto L_0x001c
            r8.a(r6, r0)
        L_0x001c:
            return
        L_0x001d:
            com.startapp.sdk.adsbase.remoteconfig.MetaData r2 = h     // Catch:{ all -> 0x0048 }
            boolean r2 = r2.i     // Catch:{ all -> 0x0048 }
            if (r2 == 0) goto L_0x0025
            if (r9 == 0) goto L_0x003d
        L_0x0025:
            r9 = 1
            r3.i = r9     // Catch:{ all -> 0x0048 }
            r3.j = r0     // Catch:{ all -> 0x0048 }
            com.startapp.sdk.adsbase.remoteconfig.a r9 = k     // Catch:{ all -> 0x0048 }
            if (r9 == 0) goto L_0x0033
            com.startapp.sdk.adsbase.remoteconfig.a r9 = k     // Catch:{ all -> 0x0048 }
            r9.b()     // Catch:{ all -> 0x0048 }
        L_0x0033:
            com.startapp.sdk.adsbase.remoteconfig.a r9 = new com.startapp.sdk.adsbase.remoteconfig.a     // Catch:{ all -> 0x0048 }
            r9.<init>(r4, r5, r6)     // Catch:{ all -> 0x0048 }
            k = r9     // Catch:{ all -> 0x0048 }
            r9.a()     // Catch:{ all -> 0x0048 }
        L_0x003d:
            if (r7 == 0) goto L_0x0046
            if (r8 == 0) goto L_0x0046
            com.startapp.sdk.adsbase.remoteconfig.MetaData r4 = h     // Catch:{ all -> 0x0048 }
            r4.a((com.startapp.sdk.adsbase.remoteconfig.b) r8)     // Catch:{ all -> 0x0048 }
        L_0x0046:
            monitor-exit(r1)     // Catch:{ all -> 0x0048 }
            return
        L_0x0048:
            r4 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0048 }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.remoteconfig.MetaData.a(android.content.Context, com.startapp.sdk.adsbase.model.AdPreferences, com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest$RequestReason, boolean, com.startapp.sdk.adsbase.remoteconfig.b, boolean):void");
    }

    public final void a(b bVar) {
        synchronized (f6422a) {
            this.l.add(bVar);
        }
    }

    public final String a(AdPreferences.Placement placement) {
        int i2 = AnonymousClass1.f6423a[placement.ordinal()];
        if (i2 == 1) {
            String str = this.adPlatformBannerHostSecured;
            return str != null ? str : r();
        } else if (i2 == 2) {
            String str2 = this.adPlatformOverlayHostSecured;
            return str2 != null ? str2 : r();
        } else if (i2 == 3) {
            String str3 = this.adPlatformNativeHostSecured;
            return str3 != null ? str3 : r();
        } else if (i2 == 4) {
            String str4 = this.adPlatformReturnHostSecured;
            return str4 != null ? str4 : r();
        } else if (i2 != 5) {
            return r();
        } else {
            String str5 = this.adPlatformSplashHostSecured;
            return str5 != null ? str5 : r();
        }
    }

    private static String a(String str, String str2) {
        return str != null ? str.replace("%AdPlatformProtocol%", "1.5") : str2;
    }

    public static void a(Context context, String str) {
        if (str != null && !str.equals("")) {
            if (!com.startapp.sdk.adsbase.j.a.a(context, "close_button", ".png")) {
                u.a();
                new com.startapp.common.a(str + "close_button.png", new a(context, "close_button"), 0).a();
            }
            u.b();
            String[] strArr = AdsConstants.c;
            for (int i2 = 0; i2 < 6; i2++) {
                String str2 = strArr[i2];
                if (!com.startapp.sdk.adsbase.j.a.a(context, str2, ".png")) {
                    new com.startapp.common.a(str + str2 + ".png", new a(context, str2), 0).a();
                }
            }
            u.b();
            String[] strArr2 = AdsConstants.d;
            for (int i3 = 0; i3 < 3; i3++) {
                String str3 = strArr2[i3];
                if (!com.startapp.sdk.adsbase.j.a.a(context, str3, ".png")) {
                    new com.startapp.common.a(str + str3 + ".png", new a(context, str3), 0).a();
                }
            }
            if (!com.startapp.sdk.adsbase.j.a.a(context, "logo", ".png")) {
                new com.startapp.common.a(str + "logo.png", new a(context, "logo"), 0).a();
            }
        }
    }
}
