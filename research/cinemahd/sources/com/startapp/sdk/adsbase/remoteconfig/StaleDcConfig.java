package com.startapp.sdk.adsbase.remoteconfig;

import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public class StaleDcConfig implements Serializable {
    private int ief = 0;

    public final int a() {
        return this.ief;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && StaleDcConfig.class == obj.getClass() && this.ief == ((StaleDcConfig) obj).ief;
    }

    public int hashCode() {
        return u.a(Integer.valueOf(this.ief));
    }
}
