package com.startapp.sdk.adsbase.remoteconfig;

import android.content.Context;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.c.c;
import java.io.Serializable;

public class SimpleTokenConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private boolean enabled = false;

    public static void a(Context context, boolean z) {
        j.b(context, "userDisabledSimpleToken", Boolean.valueOf(!z));
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        return obj != null && SimpleTokenConfig.class == obj.getClass() && this.enabled == ((SimpleTokenConfig) obj).enabled;
    }

    public int hashCode() {
        return Boolean.valueOf(this.enabled).hashCode();
    }

    public final boolean a(Context context) {
        return !j.a(context, "userDisabledSimpleToken", Boolean.FALSE).booleanValue() && this.enabled && c.a(context).f().g();
    }
}
