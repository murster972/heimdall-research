package com.startapp.sdk.adsbase;

import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;

public final class AdsConstants {

    /* renamed from: a  reason: collision with root package name */
    public static final String f6247a = "https://imp.startappservice.com/tracking/adImpression";
    public static final Boolean b;
    public static final String[] c = {"back_", "back_dark", "browser_icon_dark", "forward_", "forward_dark", "x_dark"};
    public static final String[] d = {"empty_star", "filled_star", "half_star"};
    private static String e = "get";
    private static String f;
    private static String g;
    private static String h = "trackdownload";
    private static String i;
    private static Boolean j;

    /* renamed from: com.startapp.sdk.adsbase.AdsConstants$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f6248a = new int[ServiceApiType.values().length];
        static final /* synthetic */ int[] b = new int[AdApiType.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(11:0|1|2|3|(2:5|6)|7|9|10|11|12|14) */
        /* JADX WARNING: Code restructure failed: missing block: B:15:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x0032 */
        static {
            /*
                com.startapp.sdk.adsbase.AdsConstants$AdApiType[] r0 = com.startapp.sdk.adsbase.AdsConstants.AdApiType.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                b = r0
                r0 = 1
                int[] r1 = b     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.startapp.sdk.adsbase.AdsConstants$AdApiType r2 = com.startapp.sdk.adsbase.AdsConstants.AdApiType.HTML     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r1[r2] = r0     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                r1 = 2
                int[] r2 = b     // Catch:{ NoSuchFieldError -> 0x001f }
                com.startapp.sdk.adsbase.AdsConstants$AdApiType r3 = com.startapp.sdk.adsbase.AdsConstants.AdApiType.JSON     // Catch:{ NoSuchFieldError -> 0x001f }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2[r3] = r1     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                com.startapp.sdk.adsbase.AdsConstants$ServiceApiType[] r2 = com.startapp.sdk.adsbase.AdsConstants.ServiceApiType.values()
                int r2 = r2.length
                int[] r2 = new int[r2]
                f6248a = r2
                int[] r2 = f6248a     // Catch:{ NoSuchFieldError -> 0x0032 }
                com.startapp.sdk.adsbase.AdsConstants$ServiceApiType r3 = com.startapp.sdk.adsbase.AdsConstants.ServiceApiType.METADATA     // Catch:{ NoSuchFieldError -> 0x0032 }
                int r3 = r3.ordinal()     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r3] = r0     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                int[] r0 = f6248a     // Catch:{ NoSuchFieldError -> 0x003c }
                com.startapp.sdk.adsbase.AdsConstants$ServiceApiType r2 = com.startapp.sdk.adsbase.AdsConstants.ServiceApiType.DOWNLOAD     // Catch:{ NoSuchFieldError -> 0x003c }
                int r2 = r2.ordinal()     // Catch:{ NoSuchFieldError -> 0x003c }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x003c }
            L_0x003c:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.AdsConstants.AnonymousClass1.<clinit>():void");
        }
    }

    public enum AdApiType {
        HTML,
        JSON
    }

    public enum ServiceApiType {
        METADATA,
        DOWNLOAD
    }

    static {
        StringBuilder sb = new StringBuilder();
        sb.append(e);
        sb.append("ads");
        f = sb.toString();
        StringBuilder sb2 = new StringBuilder();
        sb2.append(e);
        sb2.append("htmlad");
        g = sb2.toString();
        StringBuilder sb3 = new StringBuilder();
        sb3.append(e);
        sb3.append("adsmetadata");
        i = sb3.toString();
        Boolean bool = Boolean.FALSE;
        b = bool;
        j = bool;
    }

    public static String a(ServiceApiType serviceApiType) {
        String str;
        int i2 = AnonymousClass1.f6248a[serviceApiType.ordinal()];
        String str2 = null;
        if (i2 == 1) {
            str2 = i;
            str = MetaData.G().metaDataHostSecured;
        } else if (i2 != 2) {
            str = null;
        } else {
            str2 = h;
            str = MetaData.G().s();
        }
        return str + str2;
    }

    public static String a(AdApiType adApiType, AdPreferences.Placement placement) {
        String str;
        int i2 = AnonymousClass1.b[adApiType.ordinal()];
        String str2 = null;
        if (i2 == 1) {
            str2 = g;
            str = MetaData.G().a(placement);
        } else if (i2 != 2) {
            str = null;
        } else {
            str2 = f;
            str = MetaData.G().a(placement);
        }
        return str + str2;
    }

    public static Boolean a() {
        return j;
    }
}
