package com.startapp.sdk.adsbase.consent;

import com.startapp.sdk.adsbase.j.u;
import java.io.Serializable;

public final class ConsentTypeInfoConfig implements Serializable {
    private static final long serialVersionUID = 1;
    private Integer falseClick;
    private Integer impression;
    private Integer trueClick;

    public final Integer a() {
        return this.impression;
    }

    public final Integer b() {
        return this.trueClick;
    }

    public final Integer c() {
        return this.falseClick;
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj != null && ConsentTypeInfoConfig.class == obj.getClass()) {
            ConsentTypeInfoConfig consentTypeInfoConfig = (ConsentTypeInfoConfig) obj;
            return u.b(this.impression, consentTypeInfoConfig.impression) && u.b(this.trueClick, consentTypeInfoConfig.trueClick) && u.b(this.falseClick, consentTypeInfoConfig.falseClick);
        }
    }

    public final int hashCode() {
        return u.a(this.impression, this.trueClick, this.falseClick);
    }
}
