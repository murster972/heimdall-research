package com.startapp.sdk.adsbase.consent;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import com.facebook.common.util.ByteConstants;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.c.c;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.model.VisionDataDBAdapter;
import com.vungle.warren.ui.JavascriptBridge;
import java.net.URI;

public class ConsentActivity extends Activity {

    /* renamed from: a  reason: collision with root package name */
    private WebView f6332a;
    private String b;
    /* access modifiers changed from: private */
    public boolean c;

    public void onBackPressed() {
        WebView webView = this.f6332a;
        if (webView == null) {
            this.c = true;
            super.onBackPressed();
            return;
        }
        String url = webView.getUrl();
        String str = this.b;
        if (str != null && url != null && url.contains(str)) {
            this.f6332a.loadUrl("javascript:startappBackPressed();");
        } else if (this.f6332a.canGoBack()) {
            this.f6332a.goBack();
        } else {
            this.c = true;
            super.onBackPressed();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
        RelativeLayout relativeLayout = new RelativeLayout(this);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        String dataString = getIntent().getDataString();
        if (!TextUtils.isEmpty(dataString)) {
            try {
                URI uri = new URI(dataString);
                this.b = new URI(uri.getScheme(), uri.getAuthority(), uri.getPath(), (String) null, (String) null).toString();
                this.f6332a = new WebView(this);
                this.f6332a.setWebViewClient(new a());
                this.f6332a.getSettings().setJavaScriptEnabled(true);
                this.f6332a.setHorizontalScrollBarEnabled(false);
                this.f6332a.setVerticalScrollBarEnabled(false);
                if (Build.VERSION.SDK_INT >= 15) {
                    this.f6332a.getSettings().setTextZoom(100);
                } else {
                    this.f6332a.getSettings().setTextSize(WebSettings.TextSize.NORMAL);
                }
                this.f6332a.loadUrl(dataString);
                this.f6332a.setBackgroundColor(0);
                b.d(this.f6332a);
                RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(-1, -1);
                layoutParams2.addRule(13);
                relativeLayout.addView(this.f6332a, layoutParams2);
            } catch (Throwable th) {
                new e(th).a((Context) this);
            }
        }
        setContentView(relativeLayout, layoutParams);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        ConsentConfig g = MetaData.G().g();
        if (!this.c && g != null && g.b() && u.c((Context) this) && u.g(this)) {
            new e(InfoEventCategory.GENERAL).f("ConsentActivityHasBeenCovered").a((Context) this);
            finish();
            startActivity(getIntent());
        }
        c.a(this).f().c();
    }

    class a extends WebViewClient {
        a() {
        }

        private boolean a(Uri uri) {
            String scheme = uri.getScheme();
            String host = uri.getHost();
            ConsentConfig g = MetaData.G().g();
            if (scheme != null && scheme.equalsIgnoreCase("startappad") && !TextUtils.isEmpty(host) && g != null) {
                if (host.equalsIgnoreCase("setconsent")) {
                    String queryParameter = uri.getQueryParameter(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
                    String queryParameter2 = uri.getQueryParameter("apc");
                    try {
                        Boolean bool = null;
                        Integer valueOf = !TextUtils.isEmpty(queryParameter) ? Integer.valueOf(Integer.parseInt(queryParameter)) : null;
                        if (!TextUtils.isEmpty(queryParameter2)) {
                            bool = Boolean.valueOf(Boolean.parseBoolean(queryParameter2));
                        }
                        c.a(ConsentActivity.this).f().a(valueOf, Long.valueOf(g.e()), bool, true, true);
                    } catch (Throwable th) {
                        new e(th).a((Context) ConsentActivity.this);
                    }
                    return true;
                } else if (host.equalsIgnoreCase(JavascriptBridge.MraidHandler.CLOSE_ACTION)) {
                    boolean unused = ConsentActivity.this.c = true;
                    ConsentActivity.this.finish();
                    return true;
                }
            }
            return false;
        }

        public final void onPageFinished(WebView webView, String str) {
            Bundle extras = ConsentActivity.this.getIntent().getExtras();
            if (extras != null) {
                StringBuilder sb = new StringBuilder("javascript:var obj = {};");
                if (!TextUtils.isEmpty(str)) {
                    sb.append("obj.template = '");
                    sb.append(str);
                    sb.append("';");
                }
                if (extras.containsKey("allowCT")) {
                    boolean z = extras.getBoolean("allowCT");
                    sb.append("obj.allowCT = ");
                    sb.append(z);
                    sb.append(";");
                }
                String a2 = u.a((Context) ConsentActivity.this);
                if (!TextUtils.isEmpty(a2)) {
                    sb.append("obj.imageBase64 = '");
                    sb.append(a2);
                    sb.append("';");
                }
                if (extras.containsKey("dParam")) {
                    String string = extras.getString("dParam");
                    if (!TextUtils.isEmpty(string)) {
                        sb.append("obj.dParam = '");
                        sb.append(string);
                        sb.append("';");
                    }
                }
                if (extras.containsKey("clickUrl")) {
                    String string2 = extras.getString("clickUrl");
                    if (!TextUtils.isEmpty(string2)) {
                        sb.append("obj.clickUrl = '");
                        sb.append(string2);
                        sb.append("';");
                    }
                }
                if (extras.containsKey("impressionUrl")) {
                    String string3 = extras.getString("impressionUrl");
                    if (!TextUtils.isEmpty(string3)) {
                        sb.append("obj.impressionUrl = '");
                        sb.append(string3);
                        sb.append("';");
                    }
                }
                String c = ((com.startapp.sdk.d.b.a) c.a(ConsentActivity.this).a().c()).c();
                if (!TextUtils.isEmpty(c)) {
                    sb.append("obj.locales = '");
                    sb.append(c);
                    sb.append("';");
                }
                if (extras.containsKey(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP)) {
                    long j = extras.getLong(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP);
                    sb.append("obj.timeStamp = ");
                    sb.append(j);
                    sb.append(";");
                }
                if (extras.containsKey("templateName")) {
                    int i = extras.getInt("templateName");
                    sb.append("obj.templateName = ");
                    sb.append(i);
                    sb.append(";");
                }
                if (extras.containsKey("templateId")) {
                    int i2 = extras.getInt("templateId");
                    sb.append("obj.templateId = ");
                    sb.append(i2);
                    sb.append(";");
                }
                sb.append("obj.os = 'android';");
                sb.append("obj.consentTypeInfo = {};");
                if (extras.containsKey("impression")) {
                    sb.append("obj.consentTypeInfo.impression = ");
                    sb.append((long) extras.getInt("impression"));
                    sb.append(";");
                }
                if (extras.containsKey("trueClick")) {
                    sb.append("obj.consentTypeInfo.trueClick = ");
                    sb.append((long) extras.getInt("trueClick"));
                    sb.append(";");
                }
                if (extras.containsKey("falseClick")) {
                    sb.append("obj.consentTypeInfo.falseClick = ");
                    sb.append((long) extras.getInt("falseClick"));
                    sb.append(";");
                }
                sb.append("startappInit(obj);");
                webView.loadUrl(sb.toString());
            }
        }

        @TargetApi(24)
        public final boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
            return a(webResourceRequest.getUrl());
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            return a(Uri.parse(str));
        }
    }
}
