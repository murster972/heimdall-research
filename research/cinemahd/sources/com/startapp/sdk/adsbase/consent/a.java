package com.startapp.sdk.adsbase.consent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.text.TextUtils;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import com.startapp.sdk.adsbase.remoteconfig.b;
import com.startapp.sdk.c.c;
import com.vungle.warren.model.VisionDataDBAdapter;

public final class a implements b {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6334a;
    private final SharedPreferences b;
    private Intent c;
    private boolean d = false;
    private boolean e = true;

    public a(Context context) {
        this.f6334a = context;
        this.b = context.getSharedPreferences("com.startapp.sdk", 0);
    }

    private boolean i() {
        ConsentConfig g = MetaData.G().g();
        return this.e && g != null && g.a();
    }

    public final void a(Intent intent) {
        this.c = intent;
    }

    public final boolean b() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public final void c() {
        this.d = false;
        Intent intent = this.c;
        if (intent != null) {
            this.f6334a.startActivity(intent);
        }
    }

    public final Integer d() {
        if (i()) {
            int hashCode = c.a(this.f6334a).d().b().a().hashCode();
            if (!this.b.contains("advIdHash") || this.b.getInt("advIdHash", 0) != hashCode) {
                this.b.edit().remove("consentType").remove("consentTimestamp").putInt("advIdHash", hashCode).commit();
            }
        }
        if (!i() || !this.b.contains("consentType")) {
            return null;
        }
        return Integer.valueOf(this.b.getInt("consentType", -1));
    }

    public final Long e() {
        if (!i() || !this.b.contains("consentTimestamp")) {
            return null;
        }
        return Long.valueOf(this.b.getLong("consentTimestamp", 0));
    }

    public final Boolean f() {
        if (!i() || !this.b.contains("consentApc")) {
            return null;
        }
        return Boolean.valueOf(this.b.getBoolean("consentApc", false));
    }

    public final boolean g() {
        Boolean f = f();
        return f != null && f.booleanValue();
    }

    public final void h() {
        ConsentConfig g = MetaData.G().g();
        if (g != null && i() && !this.d && u.c(this.f6334a) && u.g(this.f6334a) && g.d() != null && g.h() != null && !this.b.contains("consentApc")) {
            Intent intent = new Intent(this.f6334a, ConsentActivity.class);
            intent.setFlags(805306368);
            intent.putExtra("allowCT", g.a());
            intent.putExtra(VisionDataDBAdapter.VisionDataColumns.COLUMN_TIMESTAMP, g.e());
            intent.putExtra("templateName", g.h());
            intent.putExtra("templateId", g.i());
            if (!TextUtils.isEmpty(g.d())) {
                intent.setData(Uri.parse(g.d()));
            }
            if (!TextUtils.isEmpty(g.j())) {
                intent.putExtra("dParam", g.j());
            }
            if (!TextUtils.isEmpty(g.g())) {
                intent.putExtra("clickUrl", g.g());
            }
            if (!TextUtils.isEmpty(g.f())) {
                intent.putExtra("impressionUrl", g.f());
            }
            ConsentTypeInfoConfig k = g.k();
            if (k != null) {
                intent.putExtra("impression", k.a());
                intent.putExtra("trueClick", k.b());
                intent.putExtra("falseClick", k.c());
            }
            new StringBuilder("Dialog becomes visible with ts=").append(g.e());
            this.d = true;
            this.f6334a.startActivity(intent);
        }
    }

    public final void a(Integer num, Long l, Boolean bool, boolean z, boolean z2) {
        Integer num2 = num;
        Long l2 = l;
        Boolean bool2 = bool;
        StringBuilder sb = new StringBuilder("set ct=");
        sb.append(num2);
        sb.append(", timestamp=");
        sb.append(l2);
        sb.append(", apc=");
        sb.append(bool2);
        if (i()) {
            long j = this.b.getLong("consentTimestamp", 0);
            int i = this.b.getInt("consentType", -1);
            boolean contains = this.b.contains("consentApc");
            boolean z3 = true;
            boolean z4 = (num2 == null || i == num.intValue()) ? false : true;
            boolean z5 = bool2 != null && (!contains || this.b.getBoolean("consentApc", false) != bool.booleanValue());
            if (l2 == null || l.longValue() <= j) {
                z3 = false;
            }
            if (!z && !z3) {
                return;
            }
            if (z4 || z5) {
                SharedPreferences.Editor edit = this.b.edit();
                if (z4) {
                    edit.putInt("consentType", num.intValue());
                }
                if (z5) {
                    edit.putBoolean("consentApc", bool.booleanValue());
                }
                if (z3) {
                    edit.putLong("consentTimestamp", l.longValue());
                }
                edit.commit();
                if (z2) {
                    MetaData.G().a(this.f6334a, new AdPreferences(), MetaDataRequest.RequestReason.CONSENT, false, (b) null, true);
                }
            }
        }
    }

    public final void a(boolean z) {
        this.e = z;
    }

    public final void a(MetaDataRequest.RequestReason requestReason, boolean z) {
        MetaData.G().a((b) this);
        ConsentConfig g = MetaData.G().g();
        if (g != null && i()) {
            Integer c2 = g.c();
            if (c2 != null) {
                a(c2, Long.valueOf(g.e()), (Boolean) null, false, false);
            }
            if (requestReason == MetaDataRequest.RequestReason.CONSENT) {
                this.b.edit().putLong("consentTimestamp", g.e()).commit();
            } else if (requestReason == MetaDataRequest.RequestReason.LAUNCH) {
                h();
            }
        }
    }

    public final void a() {
        MetaData.G().a((b) this);
    }
}
