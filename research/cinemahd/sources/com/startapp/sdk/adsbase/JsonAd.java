package com.startapp.sdk.adsbase;

import android.content.Context;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import java.util.List;
import java.util.concurrent.TimeUnit;

public abstract class JsonAd extends Ad {
    private static final long serialVersionUID = 1;
    private List<AdDetails> adsDetails = null;
    private Boolean consentApc;
    private Long consentTimeStamp;
    private Integer consentType;

    public JsonAd(Context context, AdPreferences.Placement placement) {
        super(context, placement);
    }

    public final void a(List<AdDetails> list) {
        this.adsDetails = list;
        List<AdDetails> list2 = this.adsDetails;
        Long l = null;
        if (list2 != null) {
            for (AdDetails next : list2) {
                if (!(next == null || next.x() == null)) {
                    if (l == null || next.x().longValue() < l.longValue()) {
                        l = next.x();
                    }
                }
            }
        }
        if (l != null) {
            this.adCacheTtl = Long.valueOf(TimeUnit.SECONDS.toMillis(l.longValue()));
        }
        boolean z = true;
        for (AdDetails i : this.adsDetails) {
            if (!i.i()) {
                z = false;
            }
        }
        this.belowMinCPM = z;
    }

    public final List<AdDetails> g() {
        return this.adsDetails;
    }

    public String getAdId() {
        List<AdDetails> list = this.adsDetails;
        if (list == null || list.size() <= 0) {
            return null;
        }
        return this.adsDetails.get(0).a();
    }

    public Boolean getConsentApc() {
        return this.consentApc;
    }

    public Long getConsentTimestamp() {
        return this.consentTimeStamp;
    }

    public Integer getConsentType() {
        return this.consentType;
    }

    public void setConsentApc(Boolean bool) {
        this.consentApc = bool;
    }

    public void setConsentTimestamp(Long l) {
        this.consentTimeStamp = l;
    }

    public void setConsentType(Integer num) {
        this.consentType = num;
    }
}
