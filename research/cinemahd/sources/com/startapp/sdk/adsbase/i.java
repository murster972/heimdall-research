package com.startapp.sdk.adsbase;

import android.content.Context;
import android.os.Build;
import com.startapp.common.SDKException;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.g.a;
import com.startapp.sdk.adsbase.j.m;

public final class i extends c {
    private a.AnonymousClass1 b;
    private String c;

    public i(Context context) {
        super(1);
        this.b = a.a(context);
        this.c = b.j(context);
    }

    /* access modifiers changed from: protected */
    public final void a(m mVar) throws SDKException {
        super.a(mVar);
        mVar.a("placement", "INAPP_DOWNLOAD", true);
        a.AnonymousClass1 r1 = this.b;
        if (r1 != null) {
            mVar.a("install_referrer", r1.a(), true);
            mVar.a("referrer_click_timestamp_seconds", Long.valueOf(this.b.b()), true);
            mVar.a("install_begin_timestamp_seconds", Long.valueOf(this.b.c()), true);
        }
        mVar.a("apkSig", this.c, true);
        if (Build.VERSION.SDK_INT >= 9) {
            long a2 = SimpleTokenUtils.a();
            if (a2 != 0) {
                mVar.a("firstInstalledAppTS", Long.valueOf(a2), false);
            }
        }
    }
}
