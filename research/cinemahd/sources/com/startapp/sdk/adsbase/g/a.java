package com.startapp.sdk.adsbase.g;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import com.google.android.finsky.externalreferrer.a;
import java.util.concurrent.CountDownLatch;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static CountDownLatch f6359a;
    /* access modifiers changed from: private */
    public static volatile AnonymousClass1 b;

    /* renamed from: com.startapp.sdk.adsbase.g.a$a  reason: collision with other inner class name */
    static final class C0076a implements ServiceConnection {

        /* renamed from: a  reason: collision with root package name */
        private String f6361a;

        /* synthetic */ C0076a(String str, byte b) {
            this(str);
        }

        public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            com.google.android.finsky.externalreferrer.a a2 = a.C0031a.a(iBinder);
            Bundle bundle = new Bundle();
            bundle.putString("package_name", this.f6361a);
            try {
                AnonymousClass1 unused = a.b = new Object(a2.a(bundle)) {

                    /* renamed from: a  reason: collision with root package name */
                    private final Bundle f6360a;

                    {
                        this.f6360a = r1;
                    }

                    public final String a() {
                        return this.f6360a.getString("install_referrer");
                    }

                    public final long b() {
                        return this.f6360a.getLong("referrer_click_timestamp_seconds");
                    }

                    public final long c() {
                        return this.f6360a.getLong("install_begin_timestamp_seconds");
                    }
                };
            } catch (RemoteException unused2) {
            }
            a.f6359a.countDown();
        }

        public final void onServiceDisconnected(ComponentName componentName) {
            a.f6359a.countDown();
        }

        private C0076a(String str) {
            this.f6361a = str;
        }
    }

    private static boolean b(Context context) {
        try {
            if (context.getPackageManager().getPackageInfo("com.android.vending", 128).versionCode >= 80837300) {
                return true;
            }
            return false;
        } catch (PackageManager.NameNotFoundException unused) {
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(4:16|17|18|19) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x006d */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.startapp.sdk.adsbase.g.a.AnonymousClass1 a(android.content.Context r7) {
        /*
            java.lang.String r0 = "com.android.vending"
            com.startapp.sdk.adsbase.g.a$1 r1 = b
            if (r1 != 0) goto L_0x0082
            java.util.concurrent.CountDownLatch r1 = new java.util.concurrent.CountDownLatch     // Catch:{ all -> 0x0079 }
            r2 = 1
            r1.<init>(r2)     // Catch:{ all -> 0x0079 }
            f6359a = r1     // Catch:{ all -> 0x0079 }
            com.startapp.sdk.adsbase.g.a$a r1 = new com.startapp.sdk.adsbase.g.a$a     // Catch:{ all -> 0x0079 }
            java.lang.String r3 = r7.getPackageName()     // Catch:{ all -> 0x0079 }
            r4 = 0
            r1.<init>(r3, r4)     // Catch:{ all -> 0x0079 }
            android.content.Intent r3 = new android.content.Intent     // Catch:{ all -> 0x0079 }
            java.lang.String r5 = "com.google.android.finsky.BIND_GET_INSTALL_REFERRER_SERVICE"
            r3.<init>(r5)     // Catch:{ all -> 0x0079 }
            android.content.ComponentName r5 = new android.content.ComponentName     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = "com.google.android.finsky.externalreferrer.GetInstallReferrerService"
            r5.<init>(r0, r6)     // Catch:{ all -> 0x0079 }
            r3.setComponent(r5)     // Catch:{ all -> 0x0079 }
            android.content.pm.PackageManager r5 = r7.getPackageManager()     // Catch:{ all -> 0x0079 }
            java.util.List r5 = r5.queryIntentServices(r3, r4)     // Catch:{ all -> 0x0079 }
            if (r5 == 0) goto L_0x0082
            boolean r6 = r5.isEmpty()     // Catch:{ all -> 0x0079 }
            if (r6 != 0) goto L_0x0082
            java.lang.Object r4 = r5.get(r4)     // Catch:{ all -> 0x0079 }
            android.content.pm.ResolveInfo r4 = (android.content.pm.ResolveInfo) r4     // Catch:{ all -> 0x0079 }
            android.content.pm.ServiceInfo r5 = r4.serviceInfo     // Catch:{ all -> 0x0079 }
            if (r5 == 0) goto L_0x0082
            android.content.pm.ServiceInfo r5 = r4.serviceInfo     // Catch:{ all -> 0x0079 }
            java.lang.String r5 = r5.packageName     // Catch:{ all -> 0x0079 }
            android.content.pm.ServiceInfo r4 = r4.serviceInfo     // Catch:{ all -> 0x0079 }
            java.lang.String r4 = r4.name     // Catch:{ all -> 0x0079 }
            boolean r0 = r0.equals(r5)     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0082
            if (r4 == 0) goto L_0x0082
            boolean r0 = b(r7)     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0082
            android.content.Intent r0 = new android.content.Intent     // Catch:{ all -> 0x0079 }
            r0.<init>(r3)     // Catch:{ all -> 0x0079 }
            boolean r0 = r7.bindService(r0, r1, r2)     // Catch:{ all -> 0x0079 }
            if (r0 == 0) goto L_0x0071
            java.util.concurrent.CountDownLatch r0 = f6359a     // Catch:{ InterruptedException -> 0x006d }
            r2 = 1
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS     // Catch:{ InterruptedException -> 0x006d }
            r0.await(r2, r4)     // Catch:{ InterruptedException -> 0x006d }
        L_0x006d:
            r7.unbindService(r1)     // Catch:{ all -> 0x0079 }
            goto L_0x0082
        L_0x0071:
            java.lang.Exception r0 = new java.lang.Exception     // Catch:{ all -> 0x0079 }
            java.lang.String r1 = "failed to connect to referrer service"
            r0.<init>(r1)     // Catch:{ all -> 0x0079 }
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x0079:
            r0 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r1 = new com.startapp.sdk.adsbase.infoevents.e
            r1.<init>((java.lang.Throwable) r0)
            r1.a((android.content.Context) r7)
        L_0x0082:
            com.startapp.sdk.adsbase.g.a$1 r7 = b
            return r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.g.a.a(android.content.Context):com.startapp.sdk.adsbase.g.a$1");
    }
}
