package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.common.util.ByteConstants;
import com.google.ar.core.ImageMetadata;
import com.startapp.common.Constants;
import com.startapp.common.ThreadManager;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.activities.OverlayActivity;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.c.c;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private static ProgressDialog f6262a;

    /* renamed from: com.startapp.sdk.adsbase.a$a  reason: collision with other inner class name */
    static class C0072a extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        protected String f6265a = "";
        protected String b;
        protected boolean c = false;
        protected boolean d = true;
        protected Runnable e;
        protected boolean f = false;
        protected boolean g = false;
        private boolean h = false;
        private long i;
        private long j;
        private Boolean k = null;
        private String l;
        private ProgressDialog m;
        private LinkedHashMap<String, Float> n = new LinkedHashMap<>();
        private long o;
        private Timer p;

        public C0072a(long j2, long j3, boolean z, Boolean bool, ProgressDialog progressDialog, String str, String str2, String str3, Runnable runnable) {
            this.i = j2;
            this.j = j3;
            this.d = z;
            this.k = bool;
            this.m = progressDialog;
            this.f6265a = str;
            this.l = str2;
            this.b = str3;
            this.e = runnable;
        }

        /* access modifiers changed from: private */
        public void b() {
            Timer timer = this.p;
            if (timer != null) {
                timer.cancel();
                this.p = null;
            }
        }

        public final void onPageFinished(WebView webView, String str) {
            if (!this.c && !this.f && this.f6265a.equals(str) && str != null && !a.b(str) && (str.startsWith("http://") || str.startsWith("https://"))) {
                this.g = true;
                try {
                    a(str);
                } catch (Exception unused) {
                }
                final Context context = webView.getContext();
                b();
                try {
                    this.p = new Timer();
                    this.p.schedule(new TimerTask() {
                        public final void run() {
                            C0072a aVar = C0072a.this;
                            if (!aVar.f && !aVar.c) {
                                try {
                                    aVar.c = true;
                                    a.a(context);
                                    if (!C0072a.this.d || !MetaData.G().B()) {
                                        a.d(context, C0072a.this.f6265a);
                                    } else {
                                        a.a(context, C0072a.this.f6265a, C0072a.this.b);
                                    }
                                    if (C0072a.this.e != null) {
                                        C0072a.this.e.run();
                                    }
                                } catch (Throwable th) {
                                    new e(th).a(context);
                                }
                            }
                        }
                    }, this.j);
                } catch (Exception e2) {
                    this.p = null;
                    new e((Throwable) e2).a(context);
                }
            }
            super.onPageFinished(webView, str);
        }

        public final void onPageStarted(WebView webView, String str, Bitmap bitmap) {
            super.onPageStarted(webView, str, bitmap);
            if (!this.h) {
                this.o = System.currentTimeMillis();
                this.n.put(str, Float.valueOf(-1.0f));
                final Context context = webView.getContext();
                ThreadManager.a((Runnable) new Runnable() {
                    public final void run() {
                        if (!C0072a.this.c) {
                            try {
                                e eVar = new e(InfoEventCategory.ERROR);
                                StringBuilder sb = new StringBuilder("Failed smart redirect hop info: ");
                                sb.append(C0072a.this.g ? "Page Finished" : "Timeout");
                                eVar.f(sb.toString()).a(C0072a.this.a()).h(C0072a.this.b).a(context);
                            } catch (Throwable th) {
                                new e(th).a(context);
                            }
                            try {
                                C0072a.this.f = true;
                                a.a(context);
                                C0072a.this.b();
                                if (!C0072a.this.d || !MetaData.G().B()) {
                                    a.d(context, C0072a.this.f6265a);
                                } else {
                                    a.a(context, C0072a.this.f6265a, C0072a.this.b);
                                }
                                if (C0072a.this.e != null) {
                                    C0072a.this.e.run();
                                }
                            } catch (Throwable th2) {
                                new e(th2).a(context);
                            }
                        }
                    }
                }, this.i);
                this.h = true;
            }
            this.g = false;
            b();
        }

        public final void onReceivedError(WebView webView, int i2, String str, String str2) {
            b();
            if (str2 != null && !a.b(str2) && a.d(str2)) {
                new e(InfoEventCategory.ERROR).f("Failed smart redirect: ".concat(String.valueOf(i2))).g(str2).h(this.b).a(webView.getContext());
            }
            super.onReceivedError(webView, i2, str, str2);
        }

        public final boolean shouldOverrideUrlLoading(WebView webView, String str) {
            float f2;
            if (webView == null || str == null || u.a(webView.getContext(), str)) {
                return true;
            }
            try {
                long currentTimeMillis = System.currentTimeMillis();
                Float valueOf = Float.valueOf(((float) (currentTimeMillis - this.o)) / 1000.0f);
                this.o = currentTimeMillis;
                this.n.put(this.f6265a, valueOf);
                this.n.put(str, Float.valueOf(-1.0f));
                this.f6265a = str;
                String lowerCase = str.toLowerCase();
                boolean z = false;
                if (!a.b(lowerCase) && !a.c(lowerCase)) {
                    return false;
                }
                if (!this.f) {
                    this.c = true;
                    a.a(webView.getContext());
                    b();
                    Context context = webView.getContext();
                    if (a.c(lowerCase)) {
                        str = webView.getUrl();
                    }
                    a.c(context, str);
                    if (this.l == null || this.l.equals("") || this.f6265a.toLowerCase().contains(this.l.toLowerCase())) {
                        if (MetaData.G().analytics.e()) {
                            if (j.a(webView.getContext(), "firstSucceededSmartRedirect", Boolean.TRUE).booleanValue()) {
                                z = true;
                            }
                        }
                        if (this.k == null) {
                            f2 = MetaData.G().analytics.d();
                        } else {
                            f2 = this.k.booleanValue() ? 100.0f : 0.0f;
                        }
                        if (z || Math.random() * 100.0d < ((double) f2)) {
                            new e(InfoEventCategory.SUCCESS_SMART_REDIRECT_HOP_INFO).a(a()).h(this.b).a(webView.getContext());
                            j.b(webView.getContext(), "firstSucceededSmartRedirect", Boolean.FALSE);
                        }
                    } else {
                        e eVar = new e(InfoEventCategory.ERROR);
                        e f3 = eVar.f("Wrong package reached, expected: " + this.l);
                        f3.g("Link: " + this.f6265a).h(this.b).a(webView.getContext());
                    }
                    if (this.e != null) {
                        this.e.run();
                    }
                }
                return true;
            } catch (Throwable th) {
                new e(th).a(webView.getContext());
            }
        }

        private void a(String str) {
            if (this.n.get(str).floatValue() < 0.0f) {
                this.n.put(str, Float.valueOf(((float) (System.currentTimeMillis() - this.o)) / 1000.0f));
            }
        }

        /* access modifiers changed from: protected */
        public final JSONArray a() {
            JSONArray jSONArray = new JSONArray();
            for (String next : this.n.keySet()) {
                JSONObject jSONObject = new JSONObject();
                try {
                    a(next);
                    jSONObject.put("time", this.n.get(next).toString());
                    jSONObject.put(ReportDBAdapter.ReportColumns.COLUMN_URL, next);
                    jSONArray.put(jSONObject);
                } catch (JSONException unused) {
                }
            }
            return jSONArray;
        }
    }

    /* JADX WARNING: type inference failed for: r4v3, types: [java.lang.CharSequence] */
    /* JADX WARNING: Failed to insert additional move for type inference */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.Context r3, java.lang.String r4) {
        /*
            android.content.res.Resources r0 = r3.getResources()     // Catch:{ NotFoundException -> 0x000f }
            android.content.pm.ApplicationInfo r1 = r3.getApplicationInfo()     // Catch:{ NotFoundException -> 0x000f }
            int r1 = r1.labelRes     // Catch:{ NotFoundException -> 0x000f }
            java.lang.String r3 = r0.getString(r1)     // Catch:{ NotFoundException -> 0x000f }
            return r3
        L_0x000f:
            android.content.pm.PackageManager r0 = r3.getPackageManager()
            r1 = 0
            android.content.pm.ApplicationInfo r3 = r3.getApplicationInfo()     // Catch:{ NameNotFoundException -> 0x0020 }
            java.lang.String r3 = r3.packageName     // Catch:{ NameNotFoundException -> 0x0020 }
            r2 = 0
            android.content.pm.ApplicationInfo r1 = r0.getApplicationInfo(r3, r2)     // Catch:{ NameNotFoundException -> 0x0020 }
            goto L_0x0021
        L_0x0020:
        L_0x0021:
            if (r1 == 0) goto L_0x0027
            java.lang.CharSequence r4 = r0.getApplicationLabel(r1)
        L_0x0027:
            java.lang.String r4 = (java.lang.String) r4
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.a.a(android.content.Context, java.lang.String):java.lang.String");
    }

    public static void b(Context context, String str, TrackingParams trackingParams) {
        a(context, str, trackingParams, true);
        u.a(context, false, TextUtils.isEmpty(str) ? "Closed Ad" : "Clicked Ad");
    }

    public static boolean c(String str) {
        return str.startsWith("intent://");
    }

    public static boolean d(String str) {
        if (str != null) {
            return str.startsWith("http://") || str.startsWith("https://");
        }
        return false;
    }

    public static String e(String str) {
        return a(str, (String) null);
    }

    private static boolean f(String str) {
        return AdsCommonMetaData.a().E() || TextUtils.isEmpty(str);
    }

    public static void c(Context context, String str) {
        d(context, str);
    }

    public static void d(Context context, String str) {
        a(context, str, d(str));
    }

    public static void b(final Context context, final String str) {
        if (context != null && !TextUtils.isEmpty(str)) {
            ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable() {
                public final void run() {
                    l lVar = new l();
                    try {
                        lVar.b(c.a(context).e().a((Object) lVar));
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                    try {
                        c.a(context).l().a(str).a((c) lVar).a();
                    } catch (Throwable th2) {
                        new e(th2).a(context);
                    }
                }
            });
        }
    }

    private static boolean c(Context context) {
        return j.a(context, "chromeTabs", Boolean.FALSE).booleanValue();
    }

    private static boolean d(Context context) {
        return !c.a(context).d().b().c() && a(j.a(context, "shared_prefs_CookieFeatureTS", (Long) 0L).longValue(), System.currentTimeMillis());
    }

    public static boolean a(Activity activity) {
        boolean z = activity.getTheme().obtainStyledAttributes(new int[]{16843277}).getBoolean(0, false);
        if ((activity.getWindow().getAttributes().flags & ByteConstants.KB) != 0) {
            return true;
        }
        return z;
    }

    private static boolean b(Context context) {
        if (Build.VERSION.SDK_INT >= 23) {
            return Settings.canDrawOverlays(context);
        }
        return b.a(context, "android.permission.SYSTEM_ALERT_WINDOW");
    }

    public static boolean b(String str) {
        return str.startsWith("market") || str.startsWith("http://play.google.com") || str.startsWith("https://play.google.com");
    }

    public static int a(String str) {
        String[] split = str.split("&");
        return Integer.parseInt(split[split.length - 1].split("=")[1]);
    }

    private static void b(Context context, String str, boolean z) {
        Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
        Bundle bundle = new Bundle();
        bundle.putBinder("android.support.customtabs.extra.SESSION", (IBinder) null);
        intent.putExtras(bundle);
        if (z) {
            try {
                List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
                if (queryIntentActivities != null && queryIntentActivities.size() > 1) {
                    intent.setPackage(queryIntentActivities.get(0).activityInfo.packageName);
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        if (!(context instanceof Activity)) {
            intent.addFlags(268435456);
        }
        context.startActivity(intent);
    }

    public static void a(Context context, String[] strArr, String str, String str2) {
        a(context, strArr, str, 0, str2);
    }

    public static void a(Context context, String[] strArr, String str, int i, String str2) {
        u.a(context, true, "Dropped impression because ".concat(String.valueOf(str2)));
        TrackingParams c = new TrackingParams(str).a(i).c(str2);
        if (strArr == null || strArr.length == 0) {
            new e(InfoEventCategory.ERROR).f("Non-impression without trackingUrls: ".concat(String.valueOf(str2))).g(c.g()).a(context);
            return;
        }
        for (String str3 : strArr) {
            if (str3 != null && !str3.equalsIgnoreCase("")) {
                a(context, str3, c, false);
            }
        }
    }

    public static String b() {
        StackTraceElement[] stackTrace = new Throwable().getStackTrace();
        int i = 0;
        while (i < 8) {
            if (stackTrace[i].getMethodName().compareTo("doHome") == 0) {
                return "home";
            }
            if (stackTrace[i].getMethodName().compareTo("onBackPressed") != 0) {
                i++;
            } else if (!k.a().i() && !k.p()) {
                return "interstitial";
            } else {
                k.a().m();
                return "back";
            }
        }
        return "interstitial";
    }

    public static void a(Context context, String str, TrackingParams trackingParams) {
        if (str != null && !str.equalsIgnoreCase("")) {
            u.a(context, false, "Sending impression");
            a(context, str, trackingParams, true);
        }
    }

    public static void a(Context context, String[] strArr, TrackingParams trackingParams) {
        if (strArr != null) {
            for (String a2 : strArr) {
                a(context, a2, trackingParams);
            }
        }
    }

    public static List<String> a(List<String> list, String str, String str2) {
        ArrayList arrayList = new ArrayList();
        int i = 0;
        while (i < list.size()) {
            int i2 = i + 5;
            List<String> subList = list.subList(i, Math.min(i2, list.size()));
            arrayList.add(AdsConstants.f6247a + "?" + TextUtils.join("&", subList) + "&isShown=" + str + "&appPresence=".concat(String.valueOf(str2)));
            i = i2;
        }
        return arrayList;
    }

    public static final void a(Context context, String str, String str2, TrackingParams trackingParams, boolean z, boolean z2) {
        if (!TextUtils.isEmpty(str2)) {
            b(context, str2, trackingParams);
        }
        k.a().e();
        String str3 = null;
        if (!z2) {
            try {
                str3 = a(str, str2);
            } catch (Throwable th) {
                new e(th).a(context);
            }
        }
        try {
            StringBuilder sb = new StringBuilder();
            sb.append(str);
            sb.append(f(str2) ? com.startapp.common.b.a.a(str3) : "");
            String sb2 = sb.toString();
            if (MetaData.G().B() && z) {
                a(context, sb2, str3);
            } else if (!TextUtils.isEmpty(str2) || !d(context)) {
                d(context, sb2);
            } else {
                j.b(context, "shared_prefs_CookieFeatureTS", Long.valueOf(System.currentTimeMillis()));
                d(context, sb2 + "&cki=1");
            }
        } catch (Throwable th2) {
            new e(th2).a(context);
        }
    }

    public static final void a(Context context, String str, String str2, String str3, TrackingParams trackingParams, long j, long j2, boolean z, Boolean bool) {
        a(context, str, str2, str3, trackingParams, j, j2, z, bool, false, (Runnable) null);
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0045  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final void a(android.content.Context r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, com.startapp.sdk.adsbase.commontracking.TrackingParams r16, long r17, long r19, boolean r21, java.lang.Boolean r22, boolean r23, java.lang.Runnable r24) {
        /*
            r1 = r12
            r3 = r14
            com.startapp.sdk.adsbase.AdsCommonMetaData r0 = com.startapp.sdk.adsbase.AdsCommonMetaData.a()
            boolean r0 = r0.D()
            if (r0 == 0) goto L_0x0060
            com.startapp.sdk.adsbase.k r0 = com.startapp.sdk.adsbase.k.a()
            r0.e()
            r2 = 0
            if (r23 != 0) goto L_0x0026
            java.lang.String r0 = a((java.lang.String) r13, (java.lang.String) r14)     // Catch:{ all -> 0x001c }
            r4 = r0
            goto L_0x0027
        L_0x001c:
            r0 = move-exception
            r4 = r0
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r4)
            r0.a((android.content.Context) r12)
        L_0x0026:
            r4 = r2
        L_0x0027:
            java.lang.String r0 = ""
            if (r3 == 0) goto L_0x0036
            boolean r2 = r14.equals(r0)
            if (r2 != 0) goto L_0x0036
            r5 = r16
            b((android.content.Context) r12, (java.lang.String) r14, (com.startapp.sdk.adsbase.commontracking.TrackingParams) r5)
        L_0x0036:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r6 = r13
            r2.append(r13)
            boolean r3 = f(r14)
            if (r3 == 0) goto L_0x0049
            java.lang.String r0 = com.startapp.common.b.a.a((java.lang.String) r4)
        L_0x0049:
            r2.append(r0)
            java.lang.String r2 = r2.toString()
            r1 = r12
            r3 = r15
            r5 = r17
            r7 = r19
            r9 = r21
            r10 = r22
            r11 = r24
            a((android.content.Context) r1, (java.lang.String) r2, (java.lang.String) r3, (java.lang.String) r4, (long) r5, (long) r7, (boolean) r9, (java.lang.Boolean) r10, (java.lang.Runnable) r11)
            return
        L_0x0060:
            r6 = r13
            r5 = r16
            r1 = r12
            r2 = r13
            r3 = r14
            r4 = r16
            r5 = r21
            r6 = r23
            a(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.a.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, com.startapp.sdk.adsbase.commontracking.TrackingParams, long, long, boolean, java.lang.Boolean, boolean, java.lang.Runnable):void");
    }

    private static void a(Context context, String str, TrackingParams trackingParams, boolean z) {
        if (context != null && !TextUtils.isEmpty(str)) {
            StringBuilder sb = new StringBuilder(str);
            if (z) {
                sb.append(com.startapp.common.b.a.a(a(str, (String) null)));
            }
            if (trackingParams != null) {
                sb.append(trackingParams.a());
            }
            b(context, sb.toString());
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:46:0x0138  */
    /* JADX WARNING: Removed duplicated region for block: B:50:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static final void a(android.content.Context r16, java.lang.String r17, java.lang.String r18, java.lang.String r19, long r20, long r22, boolean r24, java.lang.Boolean r25, java.lang.Runnable r26) {
        /*
            r1 = r16
            r0 = r18
            android.content.Intent r2 = new android.content.Intent
            java.lang.String r3 = "com.startapp.android.OnClickCallback"
            r2.<init>(r3)
            com.startapp.common.b r3 = com.startapp.common.b.a((android.content.Context) r16)
            r3.a((android.content.Intent) r2)
            boolean r2 = b((java.lang.String) r17)
            if (r2 == 0) goto L_0x0065
            if (r0 == 0) goto L_0x005c
            java.lang.String r2 = ""
            boolean r2 = r0.equals(r2)
            if (r2 != 0) goto L_0x005c
            java.lang.String r2 = r17.toLowerCase()
            java.lang.String r3 = r18.toLowerCase()
            boolean r2 = r2.contains(r3)
            if (r2 != 0) goto L_0x005c
            com.startapp.sdk.adsbase.infoevents.e r2 = new com.startapp.sdk.adsbase.infoevents.e
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r3 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.ERROR
            r2.<init>((com.startapp.sdk.adsbase.infoevents.InfoEventCategory) r3)
            java.lang.String r0 = java.lang.String.valueOf(r18)
            java.lang.String r3 = "Wrong package reached, expected: "
            java.lang.String r0 = r3.concat(r0)
            com.startapp.sdk.adsbase.infoevents.e r0 = r2.f(r0)
            java.lang.String r2 = java.lang.String.valueOf(r17)
            java.lang.String r3 = "Link: "
            java.lang.String r2 = r3.concat(r2)
            com.startapp.sdk.adsbase.infoevents.e r0 = r0.g(r2)
            r12 = r19
            com.startapp.sdk.adsbase.infoevents.e r0 = r0.h(r12)
            r0.a((android.content.Context) r1)
        L_0x005c:
            d(r16, r17)
            if (r26 == 0) goto L_0x0064
            r26.run()
        L_0x0064:
            return
        L_0x0065:
            r12 = r19
            boolean r2 = r1 instanceof android.app.Activity
            r3 = 1
            if (r2 == 0) goto L_0x0072
            r2 = r1
            android.app.Activity r2 = (android.app.Activity) r2
            com.startapp.sdk.adsbase.j.u.a((android.app.Activity) r2, (boolean) r3)
        L_0x0072:
            android.webkit.WebView r14 = new android.webkit.WebView     // Catch:{ all -> 0x0128 }
            r14.<init>(r1)     // Catch:{ all -> 0x0128 }
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            if (r2 != 0) goto L_0x00f6
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0128 }
            r4 = 22
            if (r2 < r4) goto L_0x008c
            android.app.ProgressDialog r2 = new android.app.ProgressDialog     // Catch:{ all -> 0x0128 }
            r4 = 16974545(0x10302d1, float:2.406292E-38)
            r2.<init>(r1, r4)     // Catch:{ all -> 0x0128 }
            f6262a = r2     // Catch:{ all -> 0x0128 }
            goto L_0x0093
        L_0x008c:
            android.app.ProgressDialog r2 = new android.app.ProgressDialog     // Catch:{ all -> 0x0128 }
            r2.<init>(r1)     // Catch:{ all -> 0x0128 }
            f6262a = r2     // Catch:{ all -> 0x0128 }
        L_0x0093:
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            r4 = 0
            r2.setTitle(r4)     // Catch:{ all -> 0x0128 }
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            java.lang.String r4 = "Loading...."
            r2.setMessage(r4)     // Catch:{ all -> 0x0128 }
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            r4 = 0
            r2.setIndeterminate(r4)     // Catch:{ all -> 0x0128 }
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            r2.setCancelable(r4)     // Catch:{ all -> 0x0128 }
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            com.startapp.sdk.adsbase.a$2 r4 = new com.startapp.sdk.adsbase.a$2     // Catch:{ all -> 0x0128 }
            r4.<init>(r14)     // Catch:{ all -> 0x0128 }
            r2.setOnCancelListener(r4)     // Catch:{ all -> 0x0128 }
            boolean r2 = r1 instanceof android.app.Activity     // Catch:{ all -> 0x0128 }
            if (r2 == 0) goto L_0x00c2
            r2 = r1
            android.app.Activity r2 = (android.app.Activity) r2     // Catch:{ all -> 0x0128 }
            boolean r2 = r2.isFinishing()     // Catch:{ all -> 0x0128 }
            if (r2 == 0) goto L_0x00f1
        L_0x00c2:
            boolean r2 = r1 instanceof android.app.Activity     // Catch:{ all -> 0x0128 }
            if (r2 != 0) goto L_0x00f6
            boolean r2 = b((android.content.Context) r16)     // Catch:{ all -> 0x0128 }
            if (r2 == 0) goto L_0x00f6
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            android.view.Window r2 = r2.getWindow()     // Catch:{ all -> 0x0128 }
            if (r2 == 0) goto L_0x00f6
            int r2 = android.os.Build.VERSION.SDK_INT     // Catch:{ all -> 0x0128 }
            r4 = 26
            if (r2 < r4) goto L_0x00e6
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            android.view.Window r2 = r2.getWindow()     // Catch:{ all -> 0x0128 }
            r4 = 2038(0x7f6, float:2.856E-42)
            r2.setType(r4)     // Catch:{ all -> 0x0128 }
            goto L_0x00f1
        L_0x00e6:
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            android.view.Window r2 = r2.getWindow()     // Catch:{ all -> 0x0128 }
            r4 = 2003(0x7d3, float:2.807E-42)
            r2.setType(r4)     // Catch:{ all -> 0x0128 }
        L_0x00f1:
            android.app.ProgressDialog r2 = f6262a     // Catch:{ all -> 0x0128 }
            r2.show()     // Catch:{ all -> 0x0128 }
        L_0x00f6:
            android.webkit.WebSettings r2 = r14.getSettings()     // Catch:{ all -> 0x0128 }
            r2.setJavaScriptEnabled(r3)     // Catch:{ all -> 0x0128 }
            android.webkit.WebChromeClient r2 = new android.webkit.WebChromeClient     // Catch:{ all -> 0x0128 }
            r2.<init>()     // Catch:{ all -> 0x0128 }
            r14.setWebChromeClient(r2)     // Catch:{ all -> 0x0128 }
            com.startapp.sdk.adsbase.a$a r15 = new com.startapp.sdk.adsbase.a$a     // Catch:{ all -> 0x0128 }
            android.app.ProgressDialog r9 = f6262a     // Catch:{ all -> 0x0128 }
            r2 = r15
            r3 = r20
            r5 = r22
            r7 = r24
            r8 = r25
            r10 = r17
            r11 = r18
            r12 = r19
            r13 = r26
            r2.<init>(r3, r5, r7, r8, r9, r10, r11, r12, r13)     // Catch:{ all -> 0x0128 }
            r14.setWebViewClient(r15)     // Catch:{ all -> 0x0128 }
            r2 = r17
            r14.loadUrl(r2)     // Catch:{ all -> 0x0126 }
            return
        L_0x0126:
            r0 = move-exception
            goto L_0x012b
        L_0x0128:
            r0 = move-exception
            r2 = r17
        L_0x012b:
            com.startapp.sdk.adsbase.infoevents.e r3 = new com.startapp.sdk.adsbase.infoevents.e
            r3.<init>((java.lang.Throwable) r0)
            r3.a((android.content.Context) r1)
            d(r16, r17)
            if (r26 == 0) goto L_0x013b
            r26.run()
        L_0x013b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.a.a(android.content.Context, java.lang.String, java.lang.String, java.lang.String, long, long, boolean, java.lang.Boolean, java.lang.Runnable):void");
    }

    public static final void a(Context context) {
        if (context != null && (context instanceof Activity)) {
            u.a((Activity) context, false);
        }
        ProgressDialog progressDialog = f6262a;
        if (progressDialog != null) {
            synchronized (progressDialog) {
                if (f6262a != null && f6262a.isShowing()) {
                    try {
                        f6262a.cancel();
                    } catch (Throwable th) {
                        new e(th).a(context);
                    }
                    f6262a = null;
                }
            }
        }
    }

    private static void a(Context context, String str, boolean z) {
        if (context != null) {
            int i = 76021760;
            if (AdsCommonMetaData.a().H() || !(context instanceof Activity)) {
                i = 344457216;
            }
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(str));
            intent.addFlags(i);
            boolean a2 = a(context, intent);
            if (!a2) {
                try {
                    if (Build.VERSION.SDK_INT >= 18 && MetaData.G().Q() && c(context)) {
                        b(context, str, z);
                        return;
                    }
                } catch (Throwable th) {
                    new e(th).a(context);
                    a(context, str, i);
                    return;
                }
            }
            if (z && !a2) {
                a(context, intent, i);
            }
            context.startActivity(intent);
        }
    }

    private static void a(Context context, Intent intent, int i) {
        String[] strArr = {"com.android.chrome", "com.android.browser", "com.opera.mini.native", "org.mozilla.firefox", "com.opera.browser"};
        try {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, i);
            if (queryIntentActivities != null && queryIntentActivities.size() > 1) {
                for (int i2 = 0; i2 < 5; i2++) {
                    String str = strArr[i2];
                    if (b.a(context, str, 0)) {
                        intent.setPackage(str);
                        return;
                    }
                }
            }
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    private static void a(Context context, String str, int i) {
        try {
            Intent parseUri = Intent.parseUri(str, i);
            a(context, parseUri);
            if (!(context instanceof Activity)) {
                parseUri.addFlags(268435456);
            }
            context.startActivity(parseUri);
        } catch (Throwable th) {
            new e(th).a(context);
        }
    }

    public static void a(Context context, String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            new e(InfoEventCategory.ERROR).f("Can not open in app browser, clickUrl is empty").h(str2).a(context);
        } else if (!b(str)) {
            u.b();
            try {
                if (Build.VERSION.SDK_INT >= 18 && MetaData.G().P() && c(context)) {
                    b(context, str, true);
                    return;
                }
            } catch (Throwable th) {
                new e(th).a(context);
            }
            Intent intent = new Intent(context, OverlayActivity.class);
            if (Build.VERSION.SDK_INT >= 21) {
                intent.addFlags(ImageMetadata.LENS_APERTURE);
            }
            if (Build.VERSION.SDK_INT >= 11) {
                intent.addFlags(32768);
            }
            if (!(context instanceof Activity)) {
                intent.addFlags(268435456);
            }
            intent.setData(Uri.parse(str));
            intent.putExtra("placement", AdPreferences.Placement.INAPP_BROWSER.a());
            intent.putExtra("activityShouldLockOrientation", false);
            try {
                context.startActivity(intent);
            } catch (Throwable th2) {
                new e(th2).a(context);
            }
        } else {
            d(context, str);
        }
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:0|(3:2|3|(2:6|4))|7|8|9) */
    /* JADX WARNING: Code restructure failed: missing block: B:10:0x0035, code lost:
        r1 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x0036, code lost:
        new com.startapp.sdk.adsbase.infoevents.e(r1).a(r4);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x003e, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0031 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(java.lang.String r1, java.lang.String r2, java.lang.String r3, android.content.Context r4, com.startapp.sdk.adsbase.commontracking.TrackingParams r5) {
        /*
            r0 = 1
            a((android.content.Context) r4, (java.lang.String) r3, (com.startapp.sdk.adsbase.commontracking.TrackingParams) r5, (boolean) r0)
            android.content.pm.PackageManager r3 = r4.getPackageManager()
            android.content.Intent r1 = r3.getLaunchIntentForPackage(r1)
            if (r2 == 0) goto L_0x0031
            org.json.JSONObject r3 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0031 }
            r3.<init>(r2)     // Catch:{ JSONException -> 0x0031 }
            java.util.Iterator r2 = r3.keys()     // Catch:{ JSONException -> 0x0031 }
        L_0x0017:
            boolean r5 = r2.hasNext()     // Catch:{ JSONException -> 0x0031 }
            if (r5 == 0) goto L_0x0031
            java.lang.Object r5 = r2.next()     // Catch:{ JSONException -> 0x0031 }
            java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ JSONException -> 0x0031 }
            java.lang.Object r0 = r3.get(r5)     // Catch:{ JSONException -> 0x0031 }
            java.lang.String r0 = java.lang.String.valueOf(r0)     // Catch:{ JSONException -> 0x0031 }
            r1.putExtra(r5, r0)     // Catch:{ JSONException -> 0x0031 }
            goto L_0x0017
        L_0x0031:
            r4.startActivity(r1)     // Catch:{ all -> 0x0035 }
            return
        L_0x0035:
            r1 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r2 = new com.startapp.sdk.adsbase.infoevents.e
            r2.<init>((java.lang.Throwable) r1)
            r2.a((android.content.Context) r4)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.a.a(java.lang.String, java.lang.String, java.lang.String, android.content.Context, com.startapp.sdk.adsbase.commontracking.TrackingParams):void");
    }

    private static String a(String str, String str2) {
        if (str2 != null) {
            try {
                if (!str2.equals("")) {
                    str = str2;
                }
            } catch (Exception unused) {
                return "";
            }
        }
        String[] split = str.split("[?&]d=");
        if (split.length >= 2) {
            return split[1].split("[?&]")[0];
        }
        return "";
    }

    private static boolean a(Context context, Intent intent) {
        for (ResolveInfo next : context.getPackageManager().queryIntentActivities(intent, 0)) {
            if (next.activityInfo.packageName.equalsIgnoreCase(Constants.f5922a)) {
                intent.setComponent(new ComponentName(next.activityInfo.packageName, next.activityInfo.name));
                return true;
            }
        }
        return false;
    }

    public static String a() {
        return "&position=" + b();
    }

    public static String[] a(f fVar) {
        if (fVar instanceof HtmlAd) {
            return ((HtmlAd) fVar).trackingUrls;
        }
        return fVar instanceof JsonAd ? a(((JsonAd) fVar).g()) : new String[0];
    }

    public static String[] a(List<AdDetails> list) {
        ArrayList arrayList = new ArrayList();
        if (list != null) {
            for (AdDetails d : list) {
                arrayList.add(d.d());
            }
        }
        return (String[]) arrayList.toArray(new String[0]);
    }

    public static boolean a(Context context, AdPreferences.Placement placement) {
        if (placement.equals(AdPreferences.Placement.INAPP_SPLASH) || !AdsCommonMetaData.a().N()) {
            return false;
        }
        return d(context);
    }

    private static boolean a(long j, long j2) {
        return j == 0 || j + (((long) AdsCommonMetaData.a().M()) * 86400000) <= j2;
    }
}
