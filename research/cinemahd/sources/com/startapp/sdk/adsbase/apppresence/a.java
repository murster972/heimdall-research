package com.startapp.sdk.adsbase.apppresence;

import android.content.Context;
import android.net.Uri;
import com.startapp.common.ThreadManager;
import com.startapp.sdk.adsbase.commontracking.TrackingParams;
import com.startapp.sdk.adsbase.infoevents.e;
import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final Context f6299a;
    private final List<AppPresenceDetails> b;

    public a(Context context, List<AppPresenceDetails> list) {
        this.b = list;
        this.f6299a = context;
    }

    public final void a() {
        ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                a.this.b();
            }
        });
    }

    /* access modifiers changed from: protected */
    public final Boolean b() {
        boolean z;
        String a2;
        String a3;
        try {
            List<AppPresenceDetails> list = this.b;
            ArrayList<String> arrayList = new ArrayList<>();
            ArrayList arrayList2 = new ArrayList();
            ArrayList arrayList3 = new ArrayList();
            for (AppPresenceDetails next : list) {
                if (!(next.c() || (a2 = next.a()) == null || (a3 = a(a2)) == null)) {
                    if (next.d()) {
                        arrayList2.add("d=".concat(String.valueOf(a3)));
                    } else {
                        arrayList3.add("d=".concat(String.valueOf(a3)));
                    }
                }
            }
            if (!arrayList2.isEmpty()) {
                arrayList.addAll(com.startapp.sdk.adsbase.a.a((List<String>) arrayList2, "false", "true"));
            }
            if (!arrayList3.isEmpty()) {
                arrayList.addAll(com.startapp.sdk.adsbase.a.a((List<String>) arrayList3, "false", "false"));
            }
            for (String str : arrayList) {
                if (str.length() != 0) {
                    com.startapp.sdk.adsbase.a.a(this.f6299a, str, new TrackingParams().c("APP_PRESENCE"));
                }
            }
            z = true;
        } catch (Throwable th) {
            new e(th).a(this.f6299a);
            z = false;
        }
        return Boolean.valueOf(z);
    }

    private String a(String str) {
        try {
            return Uri.parse(str).getQueryParameter("d");
        } catch (Throwable th) {
            new e(th).a(this.f6299a);
            return null;
        }
    }
}
