package com.startapp.sdk.adsbase.mraid.a;

import android.content.Context;
import android.os.Build;
import com.startapp.common.b.b;
import java.util.ArrayList;
import java.util.List;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private Context f6417a;
    private List<String> b = new ArrayList();

    public a(Context context) {
        this.f6417a = context.getApplicationContext();
    }

    public final boolean a() {
        return this.b.contains("calendar") && Build.VERSION.SDK_INT >= 14 && b.a(this.f6417a, "android.permission.WRITE_CALENDAR");
    }

    public final boolean b() {
        return this.b.contains("inlineVideo");
    }

    public final boolean c() {
        return this.b.contains("sms") && b.a(this.f6417a, "android.permission.SEND_SMS");
    }

    public final boolean d() {
        return this.b.contains("storePicture");
    }

    public final boolean e() {
        return this.b.contains("tel") && b.a(this.f6417a, "android.permission.CALL_PHONE");
    }

    public final boolean a(String str) {
        return this.b.contains(str);
    }
}
