package com.startapp.sdk.adsbase.mraid.bridge;

import android.app.Activity;
import android.content.Context;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.infoevents.e;
import java.net.URLDecoder;
import java.util.Map;

public abstract class a implements b {
    private static final String LOG_TAG = "a";
    protected C0078a openListener;

    /* renamed from: com.startapp.sdk.adsbase.mraid.bridge.a$a  reason: collision with other inner class name */
    public interface C0078a {
        boolean onClickEvent(String str);
    }

    public a(C0078a aVar) {
        this.openListener = aVar;
    }

    /* access modifiers changed from: protected */
    public void applyOrientationProperties(Activity activity, com.startapp.sdk.adsbase.mraid.b.a aVar) {
        try {
            int i = 0;
            boolean z = activity.getResources().getConfiguration().orientation == 1;
            if (aVar.b != 0) {
                if (aVar.b != 1) {
                    if (aVar.f6418a) {
                        i = -1;
                    } else if (z) {
                    }
                }
                b.a(activity, i);
            }
            i = 1;
            b.a(activity, i);
        } catch (Throwable th) {
            new e(th).a((Context) activity);
        }
    }

    public abstract void close();

    public void createCalendarEvent(String str) {
        isFeatureSupported("calendar");
    }

    public void expand(String str) {
    }

    public abstract boolean isFeatureSupported(String str);

    public boolean open(String str) {
        try {
            String trim = URLDecoder.decode(str, "UTF-8").trim();
            if (trim.startsWith("sms")) {
                return openSMS(trim);
            }
            if (trim.startsWith("tel")) {
                return openTel(trim);
            }
            return this.openListener.onClickEvent(trim);
        } catch (Exception unused) {
            return this.openListener.onClickEvent(str);
        }
    }

    public boolean openSMS(String str) {
        isFeatureSupported("sms");
        return true;
    }

    public boolean openTel(String str) {
        isFeatureSupported("tel");
        return true;
    }

    public void playVideo(String str) {
        isFeatureSupported("inlineVideo");
    }

    public void resize() {
    }

    public void setExpandProperties(Map<String, String> map) {
    }

    public abstract void setOrientationProperties(Map<String, String> map);

    public void setResizeProperties(Map<String, String> map) {
    }

    public void storePicture(String str) {
        isFeatureSupported("storePicture");
    }

    public abstract void useCustomClose(String str);
}
