package com.startapp.sdk.adsbase.mraid.b;

import com.facebook.react.uimanager.ViewProps;
import java.util.Arrays;
import java.util.List;

public final class a {
    private static final List<String> c = Arrays.asList(new String[]{"portrait", "landscape", ViewProps.NONE});

    /* renamed from: a  reason: collision with root package name */
    public boolean f6418a;
    public int b;

    public a() {
        this((byte) 0);
    }

    public static int a(String str) {
        int indexOf = c.indexOf(str);
        if (indexOf != -1) {
            return indexOf;
        }
        return 2;
    }

    private a(byte b2) {
        this.f6418a = true;
        this.b = 2;
    }
}
