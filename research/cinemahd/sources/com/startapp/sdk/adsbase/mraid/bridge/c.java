package com.startapp.sdk.adsbase.mraid.bridge;

import android.annotation.TargetApi;
import android.net.Uri;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.mraid.c.a;
import com.startapp.sdk.adsbase.mraid.c.b;
import com.vungle.warren.model.ReportDBAdapter;
import com.vungle.warren.ui.JavascriptBridge;
import com.vungle.warren.ui.contract.AdContract;
import java.io.ByteArrayInputStream;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Locale;
import java.util.Map;

public class c extends WebViewClient {
    private static final String LOG_TAG = c.class.getSimpleName();
    private static final String MRAID_JS = "mraid.js";
    private static final String MRAID_PREFIX = "mraid://";
    private b controller;
    private boolean isMraidInjected = false;

    public c(b bVar) {
        this.controller = bVar;
    }

    @TargetApi(11)
    private WebResourceResponse createMraidInjectionResponse() {
        return new WebResourceResponse("text/javascript", "UTF-8", new ByteArrayInputStream(("javascript:" + a.a()).getBytes()));
    }

    public boolean invokeMraidMethod(String str) {
        Class<b> cls = b.class;
        String[] strArr = {JavascriptBridge.MraidHandler.CLOSE_ACTION, "resize"};
        String[] strArr2 = {"createCalendarEvent", "expand", "open", "playVideo", "storePicture", "useCustomClose"};
        String[] strArr3 = {"setOrientationProperties", "setResizeProperties"};
        try {
            Map<String, String> a2 = b.a(str);
            String str2 = a2.get(AdContract.AdvertisementBus.COMMAND);
            if (Arrays.asList(strArr).contains(str2)) {
                cls.getDeclaredMethod(str2, new Class[0]).invoke(this.controller, new Object[0]);
            } else if (Arrays.asList(strArr2).contains(str2)) {
                Method declaredMethod = cls.getDeclaredMethod(str2, new Class[]{String.class});
                char c = 65535;
                int hashCode = str2.hashCode();
                String str3 = "useCustomClose";
                if (hashCode != -733616544) {
                    if (hashCode == 1614272768) {
                        if (str2.equals(str3)) {
                            c = 1;
                        }
                    }
                } else if (str2.equals("createCalendarEvent")) {
                    c = 0;
                }
                if (c == 0) {
                    str3 = "eventJSON";
                } else if (c != 1) {
                    str3 = ReportDBAdapter.ReportColumns.COLUMN_URL;
                }
                declaredMethod.invoke(this.controller, new Object[]{a2.get(str3)});
            } else if (Arrays.asList(strArr3).contains(str2)) {
                cls.getDeclaredMethod(str2, new Class[]{Map.class}).invoke(this.controller, new Object[]{a2});
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    /* access modifiers changed from: protected */
    public boolean isMraidUrl(String str) {
        return str != null && str.startsWith(MRAID_PREFIX);
    }

    public boolean matchesInjectionUrl(String str) {
        try {
            return MRAID_JS.equals(Uri.parse(str.toLowerCase(Locale.US)).getLastPathSegment());
        } catch (Exception unused) {
            return false;
        }
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
    }

    public WebResourceResponse shouldInterceptRequest(WebView webView, String str) {
        if (this.isMraidInjected || !matchesInjectionUrl(str)) {
            return super.shouldInterceptRequest(webView, str);
        }
        this.isMraidInjected = true;
        return createMraidInjectionResponse();
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        if (webView == null || str == null || u.a(webView.getContext(), str)) {
            return true;
        }
        if (isMraidUrl(str)) {
            return invokeMraidMethod(str);
        }
        return this.controller.open(str);
    }
}
