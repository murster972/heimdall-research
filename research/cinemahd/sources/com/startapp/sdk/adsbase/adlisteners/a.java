package com.startapp.sdk.adsbase.adlisteners;

import android.os.Handler;
import android.os.Looper;
import com.startapp.sdk.adsbase.Ad;

public final class a implements AdDisplayListener {

    /* renamed from: a  reason: collision with root package name */
    AdDisplayListener f6287a;

    public a(AdDisplayListener adDisplayListener) {
        this.f6287a = adDisplayListener;
    }

    public final void adClicked(final Ad ad) {
        if (this.f6287a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f6287a.adClicked(ad);
                }
            });
        }
    }

    public final void adDisplayed(final Ad ad) {
        if (this.f6287a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f6287a.adDisplayed(ad);
                }
            });
        }
    }

    public final void adHidden(final Ad ad) {
        if (this.f6287a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f6287a.adHidden(ad);
                }
            });
        }
    }

    public final void adNotDisplayed(final Ad ad) {
        if (this.f6287a != null) {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                public final void run() {
                    a.this.f6287a.adNotDisplayed(ad);
                }
            });
        }
    }
}
