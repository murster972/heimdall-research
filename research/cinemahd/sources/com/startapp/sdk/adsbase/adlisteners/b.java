package com.startapp.sdk.adsbase.adlisteners;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import com.startapp.sdk.adsbase.Ad;
import com.startapp.sdk.adsbase.infoevents.e;

public abstract class b {

    static class a extends b {

        /* renamed from: a  reason: collision with root package name */
        public static final a f6292a = new a();

        a() {
        }

        public final void a(Ad ad) {
        }

        public final void b(Ad ad) {
        }
    }

    /* renamed from: com.startapp.sdk.adsbase.adlisteners.b$b  reason: collision with other inner class name */
    static class C0073b extends b implements Handler.Callback {

        /* renamed from: a  reason: collision with root package name */
        private Context f6293a;
        private Handler b = new Handler(Looper.getMainLooper(), this);
        private AdEventListener c;

        public C0073b(Context context, AdEventListener adEventListener) {
            this.f6293a = context;
            this.c = adEventListener;
        }

        public final void a(Ad ad) {
            Message.obtain(this.b, 1, ad).sendToTarget();
        }

        public final void b(Ad ad) {
            Message.obtain(this.b, 2, ad).sendToTarget();
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            return obj != null && C0073b.class == obj.getClass() && this.c == ((C0073b) obj).c;
        }

        public final boolean handleMessage(Message message) {
            try {
                int i = message.what;
                if (i == 1) {
                    this.c.onReceiveAd((Ad) message.obj);
                    return false;
                } else if (i != 2) {
                    return false;
                } else {
                    this.c.onFailedToReceiveAd((Ad) message.obj);
                    return false;
                }
            } catch (Throwable th) {
                new e(th).a(this.f6293a);
                return false;
            }
        }

        public final int hashCode() {
            try {
                return System.identityHashCode(this.c);
            } catch (Throwable th) {
                new e(th).a(this.f6293a);
                return 0;
            }
        }
    }

    public static b a(Context context, AdEventListener adEventListener) {
        if (adEventListener instanceof b) {
            return (b) adEventListener;
        }
        if (adEventListener != null) {
            return new C0073b(context, adEventListener);
        }
        return a.f6292a;
    }

    public abstract void a(Ad ad);

    public abstract void b(Ad ad);
}
