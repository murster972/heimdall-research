package com.startapp.sdk.adsbase;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import com.startapp.common.b.b;
import com.startapp.common.jobrunner.a;
import com.startapp.common.jobrunner.interfaces.RunnerJob;

public class InfoEventService extends Service {
    static {
        b.a((Class<?>) InfoEventService.class);
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public int onStartCommand(Intent intent, int i, final int i2) {
        if (intent != null && intent.getIntExtra("__RUNNER_TASK_ID__", -1) == Integer.MAX_VALUE) {
            return 3;
        }
        a.a((Context) this);
        "onHandleIntent: RunnerManager.runJob".concat(String.valueOf(a.a(intent, (RunnerJob.a) new RunnerJob.a() {
            public final void a(RunnerJob.Result result) {
                InfoEventService.this.stopSelf(i2);
            }
        })));
        a.c();
        return 3;
    }
}
