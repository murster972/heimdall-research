package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import com.startapp.common.ThreadManager;
import com.startapp.common.b.d;
import com.startapp.common.b.e;
import com.startapp.sdk.ads.banner.BannerMetaData;
import com.startapp.sdk.ads.interstitials.ReturnAd;
import com.startapp.sdk.ads.splash.SplashConfig;
import com.startapp.sdk.ads.splash.SplashHideListener;
import com.startapp.sdk.ads.splash.SplashMetaData;
import com.startapp.sdk.adsbase.AdsConstants;
import com.startapp.sdk.adsbase.a.b;
import com.startapp.sdk.adsbase.adinformation.AdInformationMetaData;
import com.startapp.sdk.adsbase.adrules.AdRulesResult;
import com.startapp.sdk.adsbase.cache.CacheKey;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.e;
import com.startapp.sdk.adsbase.infoevents.InfoEventCategory;
import com.startapp.sdk.adsbase.j.e;
import com.startapp.sdk.adsbase.j.p;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import com.startapp.sdk.triggeredlinks.c;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.cache.DiskLruCache;

public class k implements b {

    /* renamed from: a  reason: collision with root package name */
    private static final String f6404a = "k";
    private static final AtomicBoolean b = new AtomicBoolean();
    private f A;
    private com.startapp.sdk.adsbase.a.a B;
    private c C;
    private com.startapp.sdk.adsbase.c.b D;
    private boolean E;
    private SDKAdPreferences c;
    private boolean d;
    private boolean e;
    private boolean f;
    private boolean g;
    private boolean h;
    private long i;
    private Application j;
    private HashMap<Integer, Integer> k;
    private Object l;
    private Activity m;
    private boolean n;
    private boolean o;
    private boolean p;
    private boolean q;
    private Map<String, String> r;
    private Bundle s;
    private AdPreferences t;
    private CacheKey u;
    private boolean v;
    private boolean w;
    private boolean x;
    private boolean y;
    private boolean z;

    static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final k f6408a = new k((byte) 0);
    }

    /* synthetic */ k(byte b2) {
        this();
    }

    public static k a() {
        return a.f6408a;
    }

    static void e(Context context) {
        j.b(context, "periodicInfoEventPaused", Boolean.FALSE);
        e.a(context, j.a(context, "periodicInfoEventTriggerTime", Long.valueOf(e.b(context))).longValue());
    }

    static void f(Context context) {
        j.b(context, "periodicMetadataPaused", Boolean.FALSE);
        e.a(context, Long.valueOf(j.a(context, "periodicMetadataTriggerTime", Long.valueOf(e.a())).longValue()));
    }

    public static boolean p() {
        return a.f6408a.a("Unity") != null;
    }

    static /* synthetic */ void q() {
    }

    private AdPreferences r() {
        AdPreferences adPreferences = this.t;
        return adPreferences != null ? new AdPreferences(adPreferences) : new AdPreferences();
    }

    public final void b() {
        a((Context) this.j);
        c cVar = this.C;
        if (cVar != null) {
            cVar.a();
        }
    }

    public final void c() {
        a((Context) this.j);
        c cVar = this.C;
        if (cVar != null) {
            cVar.b();
        }
    }

    public final void d() {
        a((Context) this.j);
        c cVar = this.C;
        if (cVar != null) {
            cVar.c();
        }
    }

    public final boolean g() {
        return this.q;
    }

    public final void h() {
        this.i = System.currentTimeMillis();
        this.m = null;
    }

    public final boolean i() {
        Activity activity = this.m;
        if (activity != null) {
            return activity.isTaskRoot();
        }
        return true;
    }

    public final boolean j() {
        return this.v;
    }

    public final boolean k() {
        return !this.x;
    }

    public final boolean l() {
        return this.e && !this.f && !this.h;
    }

    public final void m() {
        this.f = false;
        this.h = true;
    }

    public final boolean n() {
        return this.e && this.f;
    }

    public final boolean o() {
        return this.E;
    }

    private k() {
        this.d = u.b();
        this.f = false;
        this.g = false;
        this.h = false;
        this.j = null;
        this.k = new HashMap<>();
        this.n = false;
        this.o = true;
        this.p = false;
        this.q = false;
        this.s = null;
        this.w = false;
        this.x = false;
        this.y = false;
        this.z = false;
        this.A = null;
    }

    /* access modifiers changed from: protected */
    public final void a(Context context, String str, String str2, SDKAdPreferences sDKAdPreferences, boolean z2) {
        com.startapp.sdk.c.c a2;
        Application application;
        boolean z3 = true;
        if (!b.getAndSet(true)) {
            if (Math.random() < 0.0d) {
                Log.i(f6404a, "!SDK-VERSION-STRING!:com.startapp.startappsdk:inapp-sdk:4.6.3");
            }
            final Context applicationContext = context.getApplicationContext();
            if (!TextUtils.isEmpty(str2)) {
                try {
                    a2 = com.startapp.sdk.c.c.a(applicationContext);
                    a2.j().a(str, str2);
                    if (!com.startapp.common.b.b.a(applicationContext, "android.permission.INTERNET") || !com.startapp.common.b.b.a(applicationContext, "android.permission.ACCESS_NETWORK_STATE")) {
                        u.a(applicationContext, true, "Please grant the mandatory permissions : INTERNET & ACCESS_NETWORK_STATE, SDK could not be initialized.");
                    }
                    this.o = !u.h(applicationContext);
                    TreeMap treeMap = new TreeMap();
                    if (c("org.apache.cordova.CordovaPlugin")) {
                        treeMap.put("Cordova", "4.6.3");
                    }
                    if (c("com.startapp.android.mediation.admob.StartAppCustomEvent")) {
                        treeMap.put("AdMob", d("com.startapp.android.mediation.admob"));
                    }
                    if (c("com.mopub.mobileads.StartAppCustomEventInterstitial")) {
                        treeMap.put("MoPub", d("com.mopub.mobileads"));
                    }
                    if (c("anywheresoftware.b4a.BA") && !a.f6408a.r.containsKey("B4A")) {
                        treeMap.put("MoPub", "0");
                    }
                    if (!treeMap.isEmpty()) {
                        j.a(applicationContext, "sharedPrefsWrappers", (Map<String, String>) treeMap);
                    }
                    if (this.D == null) {
                        com.startapp.sdk.adsbase.c.b bVar = new com.startapp.sdk.adsbase.c.b(applicationContext, Thread.getDefaultUncaughtExceptionHandler());
                        this.D = bVar;
                        Thread.setDefaultUncaughtExceptionHandler(bVar);
                    }
                } catch (Throwable th) {
                    new com.startapp.sdk.adsbase.infoevents.e(th).a(applicationContext);
                    return;
                }
                a2.d().a();
                if (Build.VERSION.SDK_INT >= 14) {
                    if (applicationContext instanceof Application) {
                        application = (Application) applicationContext;
                    } else if (applicationContext instanceof Activity) {
                        application = ((Activity) applicationContext).getApplication();
                    } else if (applicationContext instanceof Service) {
                        application = ((Service) applicationContext).getApplication();
                    } else {
                        Context applicationContext2 = applicationContext.getApplicationContext();
                        application = applicationContext2 instanceof Application ? (Application) applicationContext2 : null;
                    }
                    if (application != null && this.B == null) {
                        com.startapp.sdk.adsbase.a.a aVar = new com.startapp.sdk.adsbase.a.a(this);
                        this.B = aVar;
                        application.registerActivityLifecycleCallbacks(aVar);
                    }
                }
                com.startapp.common.c.b(applicationContext);
                e.a(applicationContext);
                MetaData.a(applicationContext);
                u.a();
                AdsCommonMetaData.a(applicationContext);
                u.b();
                BannerMetaData.a(applicationContext);
                u.b();
                SplashMetaData.a(applicationContext);
                if (this.d) {
                    CacheMetaData.a(applicationContext);
                }
                u.c();
                AdInformationMetaData.a(applicationContext);
                SimpleTokenUtils.a(applicationContext);
                MetaData.G().a((com.startapp.sdk.adsbase.remoteconfig.b) a2.f());
                a(applicationContext, sDKAdPreferences);
                com.startapp.common.c.a.a(applicationContext);
                Integer a3 = j.a(applicationContext, "shared_prefs_app_version_id", (Integer) -1);
                int c2 = com.startapp.common.b.b.c(applicationContext);
                if (a3.intValue() > 0 && c2 > a3.intValue()) {
                    this.q = true;
                }
                j.b(applicationContext, "shared_prefs_app_version_id", Integer.valueOf(c2));
                this.v = false;
                this.e = false;
                if (com.startapp.common.b.b.b()) {
                    u.b();
                    this.v = z2;
                    this.e = true;
                }
                if (this.d) {
                    if (!this.q) {
                        if (CacheMetaData.a().b().d()) {
                            if (this.e) {
                                com.startapp.sdk.adsbase.cache.a.a().a(applicationContext);
                            }
                            h(applicationContext);
                            com.startapp.sdk.adsbase.cache.a.a().c(applicationContext);
                        }
                    }
                    com.startapp.sdk.adsbase.cache.a.a().b(applicationContext);
                    h(applicationContext);
                    com.startapp.sdk.adsbase.cache.a.a().c(applicationContext);
                }
                a(applicationContext, MetaDataRequest.RequestReason.LAUNCH);
                String i2 = i(applicationContext);
                if (Build.VERSION.SDK_INT < 18 || i2 == null) {
                    j.b(applicationContext, "chromeTabs", Boolean.FALSE);
                } else {
                    Intent intent = new Intent("android.support.customtabs.action.CustomTabsService");
                    intent.setPackage(i2);
                    List<ResolveInfo> queryIntentServices = applicationContext.getPackageManager().queryIntentServices(intent, 0);
                    if (queryIntentServices == null || queryIntentServices.isEmpty()) {
                        z3 = false;
                    }
                    j.b(applicationContext, "chromeTabs", Boolean.valueOf(z3));
                }
                j.b(applicationContext, "periodicInfoEventPaused", Boolean.FALSE);
                j.b(applicationContext, "periodicMetadataPaused", Boolean.FALSE);
                AnonymousClass1 r9 = new com.startapp.sdk.adsbase.remoteconfig.b() {
                    public final void a(MetaDataRequest.RequestReason requestReason, boolean z) {
                        if (MetaData.G().K()) {
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable(applicationContext) {
                                public final void run() {
                                    try {
                                        j.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                    } catch (Throwable th) {
                                        new com.startapp.sdk.adsbase.infoevents.e(th).a(r3);
                                    }
                                }
                            }, TimeUnit.SECONDS.toMillis(MetaData.G().J()));
                        }
                        e.c(applicationContext);
                        e.d(applicationContext);
                        k kVar = k.this;
                        Context context = applicationContext;
                        if (j.a(context, "shared_prefs_first_init", Boolean.TRUE).booleanValue()) {
                            j.b(context, "totalSessions", (Integer) 0);
                            j.b(context, "firstSessionTime", Long.valueOf(System.currentTimeMillis()));
                            ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable(context) {

                                /* renamed from: a  reason: collision with root package name */
                                private /* synthetic */ Context f6406a;

                                {
                                    this.f6406a = r2;
                                }

                                public final void run() {
                                    try {
                                        i iVar = new i(this.f6406a);
                                        iVar.a(this.f6406a, new AdPreferences());
                                        e.a a2 = com.startapp.sdk.c.c.a(this.f6406a).l().a(AdsConstants.a(AdsConstants.ServiceApiType.DOWNLOAD)).a((c) iVar).a();
                                        if (a2 != null) {
                                            String a3 = a2.a();
                                            if (!TextUtils.isEmpty(a3)) {
                                                String a4 = u.a(a3, "@ct@", "@ct@");
                                                String a5 = u.a(a3, "@tsc@", "@tsc@");
                                                String a6 = u.a(a3, "@apc@", "@apc@");
                                                Boolean bool = null;
                                                Integer valueOf = !TextUtils.isEmpty(a4) ? Integer.valueOf(Integer.parseInt(a4)) : null;
                                                Long valueOf2 = !TextUtils.isEmpty(a5) ? Long.valueOf(Long.parseLong(a5)) : null;
                                                if (!TextUtils.isEmpty(a6)) {
                                                    bool = Boolean.valueOf(Boolean.parseBoolean(a6));
                                                }
                                                if (!(valueOf == null && valueOf2 == null && bool == null)) {
                                                    com.startapp.sdk.c.c.a(this.f6406a).f().a(valueOf, valueOf2, bool, false, true);
                                                }
                                            }
                                        }
                                    } catch (Throwable th) {
                                        new com.startapp.sdk.adsbase.infoevents.e(th).a(this.f6406a);
                                        return;
                                    }
                                    j.b(this.f6406a, "shared_prefs_first_init", Boolean.FALSE);
                                }
                            });
                        }
                        k.a(applicationContext);
                        com.startapp.sdk.c.c a2 = com.startapp.sdk.c.c.a(applicationContext);
                        k.q();
                        k.b(applicationContext);
                        k.a(k.this, applicationContext);
                        a2.e().a();
                        a2.n().a(z ? 1 : 2);
                    }

                    public final void a() {
                        if (MetaData.G().K()) {
                            new Handler(Looper.getMainLooper()).postDelayed(new Runnable(applicationContext) {
                                public final void run() {
                                    try {
                                        j.b(r3, "User-Agent", new WebView(r3).getSettings().getUserAgentString());
                                    } catch (Throwable th) {
                                        new com.startapp.sdk.adsbase.infoevents.e(th).a(r3);
                                    }
                                }
                            }, TimeUnit.SECONDS.toMillis(10));
                        }
                        com.startapp.sdk.c.c.a(applicationContext).n().a(0);
                    }
                };
                if (MetaData.G().j()) {
                    r9.a((MetaDataRequest.RequestReason) null, false);
                } else {
                    MetaData.G().a((com.startapp.sdk.adsbase.remoteconfig.b) r9);
                }
                if (com.startapp.common.b.b.b()) {
                    if (applicationContext instanceof Activity) {
                        this.m = (Activity) applicationContext;
                        this.j = this.m.getApplication();
                    } else if (applicationContext.getApplicationContext() instanceof Application) {
                        this.j = (Application) applicationContext.getApplicationContext();
                    }
                    try {
                        if (!(this.l == null || this.j == null)) {
                            this.j.unregisterActivityLifecycleCallbacks((Application.ActivityLifecycleCallbacks) this.l);
                        }
                    } catch (Exception e2) {
                        new com.startapp.sdk.adsbase.infoevents.e((Throwable) e2).a(applicationContext);
                    }
                    Application application2 = this.j;
                    AnonymousClass3 r11 = new Application.ActivityLifecycleCallbacks() {
                        public final void onActivityCreated(Activity activity, Bundle bundle) {
                            k.a().a(activity, bundle);
                            u.b();
                            e.a.f6343a.a(activity, bundle);
                        }

                        public final void onActivityDestroyed(Activity activity) {
                            k.a().d(activity);
                        }

                        public final void onActivityPaused(Activity activity) {
                            k.a().h();
                        }

                        public final void onActivityResumed(Activity activity) {
                            k.a().b(activity);
                        }

                        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                            k.a();
                        }

                        public final void onActivityStarted(Activity activity) {
                            k.a().a(activity);
                        }

                        public final void onActivityStopped(Activity activity) {
                            k.a().c(activity);
                        }
                    };
                    application2.registerActivityLifecycleCallbacks(r11);
                    this.l = r11;
                }
                u.a(applicationContext, false, "StartApp SDK initialized with App ID: ".concat(String.valueOf(str2)));
                return;
            }
            throw new IllegalArgumentException("\n+-------------------------------------------------------------+\n|                S   T   A   R   T   A   P   P                |\n| - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |\n| Invalid App ID passed to init, please provide valid App ID  |\n|                                                             |\n| https://support.startapp.com/hc/en-us/articles/360002411114 |\n+-------------------------------------------------------------+\n");
        }
    }

    public final SDKAdPreferences g(Context context) {
        if (this.c == null) {
            Class<SDKAdPreferences> cls = SDKAdPreferences.class;
            SDKAdPreferences sDKAdPreferences = (SDKAdPreferences) d.a(context, "shared_prefs_sdk_ad_prefs");
            if (sDKAdPreferences == null) {
                this.c = new SDKAdPreferences();
            } else {
                this.c = sDKAdPreferences;
            }
        }
        return this.c;
    }

    private void h(Context context) {
        if (this.v && !AdsCommonMetaData.a().z()) {
            this.u = com.startapp.sdk.adsbase.cache.a.a().a(context, r());
        }
    }

    private static String i(Context context) {
        String str = "com.android.chrome";
        try {
            PackageManager packageManager = context.getPackageManager();
            Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.example.com"));
            ResolveInfo resolveActivity = packageManager.resolveActivity(intent, 0);
            String str2 = resolveActivity != null ? resolveActivity.activityInfo.packageName : null;
            List<ResolveInfo> queryIntentActivities = packageManager.queryIntentActivities(intent, 0);
            ArrayList arrayList = new ArrayList();
            for (ResolveInfo next : queryIntentActivities) {
                Intent intent2 = new Intent();
                intent2.setAction("android.support.customtabs.action.CustomTabsService");
                intent2.setPackage(next.activityInfo.packageName);
                if (packageManager.resolveService(intent2, 0) != null) {
                    arrayList.add(next.activityInfo.packageName);
                }
            }
            if (arrayList.isEmpty()) {
                return null;
            }
            if (arrayList.size() == 1) {
                str = (String) arrayList.get(0);
            } else if (!TextUtils.isEmpty(str2) && !a(context, intent) && arrayList.contains(str2)) {
                return str2;
            } else {
                if (!arrayList.contains(str)) {
                    return null;
                }
            }
            return str;
        } catch (Exception e2) {
            new com.startapp.sdk.adsbase.infoevents.e((Throwable) e2).a(context);
            return null;
        }
    }

    public static void b(Context context) {
        com.startapp.sdk.insight.a.a(context, MetaData.G().c());
    }

    static void c(Context context) {
        j.b(context, "periodicInfoEventPaused", Boolean.TRUE);
        com.startapp.sdk.adsbase.j.e.a(786564404);
    }

    static void d(Context context) {
        j.b(context, "periodicMetadataPaused", Boolean.TRUE);
        com.startapp.sdk.adsbase.j.e.a(586482792);
    }

    private static boolean e(Activity activity) {
        return activity.getClass().getName().equals(u.b((Context) activity));
    }

    private boolean f(Activity activity) {
        return this.z || e(activity);
    }

    public final void b(Activity activity) {
        if (this.d && this.g) {
            this.g = false;
            com.startapp.sdk.adsbase.cache.a.a().b();
        }
        if (this.n) {
            this.n = false;
            SimpleTokenUtils.c(activity.getApplicationContext());
        }
        this.m = activity;
    }

    public final void e() {
        this.n = true;
        this.g = true;
    }

    public final boolean f() {
        return this.o;
    }

    public final void c(Activity activity) {
        Integer num = this.k.get(Integer.valueOf(activity.hashCode()));
        if (num != null) {
            Integer valueOf = Integer.valueOf(num.intValue() - 1);
            if (valueOf.intValue() == 0) {
                this.k.remove(Integer.valueOf(activity.hashCode()));
            } else {
                this.k.put(Integer.valueOf(activity.hashCode()), valueOf);
            }
            if (this.k.size() == 0) {
                if (!this.h) {
                    this.f = true;
                    h(activity);
                    if (com.startapp.common.c.a() != null) {
                        com.startapp.common.c.a().a(activity);
                    }
                }
                if (this.d) {
                    com.startapp.sdk.adsbase.cache.a.a().a(activity.getApplicationContext(), this.h);
                    this.g = true;
                }
            }
        }
    }

    public final void d(Activity activity) {
        if (f(activity)) {
            this.y = false;
        }
        if (this.k.size() == 0) {
            this.f = false;
        }
    }

    /* access modifiers changed from: package-private */
    public final void e(boolean z2) {
        this.E = z2;
    }

    /* access modifiers changed from: protected */
    public final void d(boolean z2) {
        boolean z3 = z2 && com.startapp.common.b.b.b();
        this.v = z3;
        if (!z3) {
            com.startapp.sdk.adsbase.cache.a.a().b(AdPreferences.Placement.INAPP_RETURN);
        }
    }

    public final void b(boolean z2) {
        this.w = z2;
    }

    private boolean b(String str) {
        return a(str) != null;
    }

    private static String d(String str) {
        try {
            return (String) Class.forName(str + ".StartAppConstants").getField("WRAPPER_VERSION").get((Object) null);
        } catch (Exception unused) {
            return "0";
        }
    }

    public final void c(boolean z2) {
        this.x = !z2;
        if (!z2) {
            com.startapp.sdk.adsbase.cache.a.a().b(AdPreferences.Placement.INAPP_SPLASH);
        }
    }

    private static boolean c(String str) {
        try {
            Class.forName(str);
            return true;
        } catch (ClassNotFoundException | Exception unused) {
            return false;
        }
    }

    protected static void a(Context context) {
        if (context != null) {
            com.startapp.sdk.c.c.a(context).k().a(false, (com.startapp.common.d) null);
        }
    }

    public final void a(Activity activity, Bundle bundle) {
        if (e(activity)) {
            this.z = true;
        }
        this.s = bundle;
    }

    public final void a(Activity activity) {
        StringBuilder sb = new StringBuilder("onActivityStarted [");
        sb.append(activity.getClass().getName());
        sb.append("]");
        boolean f2 = f(activity);
        boolean z2 = !this.y && f2 && this.s == null && this.k.size() == 0;
        if (z2) {
            com.startapp.sdk.c.c.a(activity).f().h();
        }
        u.b();
        if (!com.startapp.sdk.c.c.a(activity).f().b() && !AdsCommonMetaData.a().A() && !this.x && !b("MoPub") && !b("AdMob") && !this.w && z2) {
            StartAppAd.a(activity, this.s, new SplashConfig(), new AdPreferences(), (SplashHideListener) null, false);
        }
        if (f2) {
            this.z = false;
            this.y = true;
        }
        if (this.f) {
            if (MetaData.G().M() && this.v && !AdsCommonMetaData.a().z()) {
                u.a();
                if (!this.p) {
                    if (System.currentTimeMillis() - this.i > AdsCommonMetaData.a().y()) {
                        this.A = com.startapp.sdk.adsbase.cache.a.a().a(this.u);
                        f fVar = this.A;
                        if (fVar != null && fVar.isReady()) {
                            AdRulesResult a2 = AdsCommonMetaData.a().G().a(AdPreferences.Placement.INAPP_RETURN, (String) null);
                            if (!a2.a()) {
                                a.a((Context) activity, ((ReturnAd) this.A).trackingUrls, (String) null, a2.b());
                            } else if (this.A.a((String) null)) {
                                com.startapp.sdk.adsbase.adrules.b.a().a(new com.startapp.sdk.adsbase.adrules.a(AdPreferences.Placement.INAPP_RETURN, (String) null));
                            }
                        }
                    }
                }
            }
            if (System.currentTimeMillis() - this.i > MetaData.G().v()) {
                a((Context) activity, MetaDataRequest.RequestReason.APP_IDLE);
            }
        }
        this.h = false;
        this.f = false;
        if (this.k.get(Integer.valueOf(activity.hashCode())) == null) {
            this.k.put(Integer.valueOf(activity.hashCode()), 1);
        }
    }

    public final void a(boolean z2) {
        this.p = z2;
    }

    public final boolean a(AdPreferences.Placement placement) {
        if (!this.e || this.h) {
            return false;
        }
        if (!this.f) {
            return true;
        }
        if (placement != AdPreferences.Placement.INAPP_RETURN || !CacheMetaData.a().b().e()) {
            return false;
        }
        return true;
    }

    public final void a(Context context, String str, String str2) {
        if (this.r == null) {
            this.r = new TreeMap();
        }
        this.r.put(str, str2);
        j.a(context, "sharedPrefsWrappers", this.r);
    }

    private String a(String str) {
        Map<String, String> map = this.r;
        if (map == null) {
            return null;
        }
        return map.get(str);
    }

    public final void a(Context context, SDKAdPreferences sDKAdPreferences) {
        this.c = sDKAdPreferences;
        d.b(context, "shared_prefs_sdk_ad_prefs", this.c);
    }

    protected static void a(Context context, MetaDataRequest.RequestReason requestReason) {
        p.d().a(context, requestReason);
    }

    protected static void a(Context context, String str, boolean z2) {
        String str2;
        if ("pas".equalsIgnoreCase(str)) {
            String string = context.getSharedPreferences("com.startapp.sdk", 0).getString("USER_CONSENT_PERSONALIZED_ADS_SERVING", (String) null);
            boolean isEmpty = TextUtils.isEmpty(string);
            String str3 = DiskLruCache.VERSION_1;
            if (!isEmpty) {
                if (string.equalsIgnoreCase(z2 ? str3 : "0")) {
                    return;
                }
            }
            StringBuilder sb = new StringBuilder();
            if (z2) {
                str2 = str3;
            } else {
                str2 = "0";
            }
            sb.append(str2);
            sb.append("M");
            new com.startapp.sdk.adsbase.infoevents.e(InfoEventCategory.GENERAL).f("User consent: ".concat(String.valueOf(str))).g(sb.toString()).a(context);
            if (!z2) {
                str3 = "0";
            }
            j.b(context, "USER_CONSENT_PERSONALIZED_ADS_SERVING", str3);
            p.d().a(context, MetaDataRequest.RequestReason.PAS);
        }
    }

    private static boolean a(Context context, Intent intent) {
        try {
            List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 64);
            if (queryIntentActivities != null) {
                if (queryIntentActivities.size() != 0) {
                    for (ResolveInfo next : queryIntentActivities) {
                        IntentFilter intentFilter = next.filter;
                        if (intentFilter != null && intentFilter.countDataAuthorities() != 0 && intentFilter.countDataPaths() != 0 && next.activityInfo != null) {
                            return true;
                        }
                    }
                    return false;
                }
            }
            return false;
        } catch (RuntimeException e2) {
            new com.startapp.sdk.adsbase.infoevents.e((Throwable) e2).a(context);
        }
    }

    public final void a(AdPreferences adPreferences) {
        boolean z2 = !u.b(this.t, adPreferences);
        this.t = adPreferences != null ? new AdPreferences(adPreferences) : null;
        if (z2) {
            com.startapp.sdk.adsbase.cache.a.a().a(AdPreferences.Placement.INAPP_RETURN);
        }
    }

    static /* synthetic */ void a(k kVar, Context context) {
        if (kVar.C == null) {
            kVar.C = com.startapp.sdk.c.c.a(context).i();
            kVar.C.d();
        }
    }
}
