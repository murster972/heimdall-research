package com.startapp.sdk.adsbase.f;

import android.content.Context;
import android.content.SharedPreferences;
import com.startapp.sdk.adsbase.infoevents.b;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.remoteconfig.c;
import org.joda.time.DateTimeConstants;

public class d {

    /* renamed from: a  reason: collision with root package name */
    protected final SharedPreferences f6356a;
    protected volatile boolean b;
    private final Context c;
    private final g<c> d;
    private final b.a e;

    public d(Context context, SharedPreferences sharedPreferences, g<c> gVar, b.a aVar) {
        this.c = context;
        this.f6356a = sharedPreferences;
        this.d = gVar;
        this.e = aVar;
    }

    public final void a(boolean z, final com.startapp.common.d dVar) {
        c a2 = this.d.a();
        if (a2 != null) {
            long a3 = (long) (a2.a() * DateTimeConstants.MILLIS_PER_MINUTE);
            if (a3 < 10000) {
                a3 = 10000;
            }
            if ((this.f6356a.getLong("6955cb8b653a5b89", 0) + a3 <= System.currentTimeMillis() || z) && !this.b) {
                new b(this.c, z, new com.startapp.common.d() {
                    public final void a(Object obj) {
                        d.this.b = false;
                        if (Boolean.TRUE.equals(obj)) {
                            d.this.f6356a.edit().putLong("6955cb8b653a5b89", System.currentTimeMillis()).commit();
                        }
                        com.startapp.common.d dVar = dVar;
                        if (dVar != null) {
                            dVar.a(obj);
                        }
                    }
                }).a();
                this.b = true;
            }
        }
    }
}
