package com.startapp.sdk.adsbase.f;

import android.content.Context;
import com.startapp.sdk.adsbase.infoevents.a;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;

public class b extends a {
    public b(Context context, Runnable runnable, a aVar) {
        super(context, runnable, aVar);
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.d(obj.toString());
        }
        super.a(obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            long millis = TimeUnit.SECONDS.toMillis((long) MetaData.G().E().a());
            final com.startapp.sdk.adsbase.b.b bVar = new com.startapp.sdk.adsbase.b.b(this.f6352a, this);
            a(new Runnable() {
                public final void run() {
                    bVar.a();
                    b.this.a(bVar.b());
                }
            }, millis);
            long currentTimeMillis = System.currentTimeMillis();
            boolean z = currentTimeMillis - j.a(this.f6352a, "lastBtDiscoveringTime", (Long) 0L).longValue() >= ((long) MetaData.G().E().b()) * 60000;
            if (z) {
                j.b(this.f6352a, "lastBtDiscoveringTime", Long.valueOf(currentTimeMillis));
            }
            bVar.a(z);
        } catch (Throwable th) {
            new e(th).a(this.f6352a);
            a((Object) null);
        }
    }
}
