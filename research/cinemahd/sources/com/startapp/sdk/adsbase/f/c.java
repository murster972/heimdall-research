package com.startapp.sdk.adsbase.f;

import android.content.Context;
import com.startapp.sdk.adsbase.h.b;
import com.startapp.sdk.adsbase.infoevents.a;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.concurrent.TimeUnit;

public final class c extends a {
    public c(Context context, Runnable runnable, a aVar) {
        super(context, runnable, aVar);
    }

    public final void a(Object obj) {
        if (obj != null) {
            this.b.c(obj.toString());
        }
        super.a(obj);
    }

    /* access modifiers changed from: protected */
    public final void b() {
        try {
            long millis = TimeUnit.SECONDS.toMillis((long) MetaData.G().D().a());
            final b bVar = new b(this.f6352a, this);
            a(new Runnable() {
                public final void run() {
                    bVar.b();
                    c.this.a(bVar.c());
                }
            }, millis);
            bVar.a();
        } catch (Exception unused) {
            a((Object) null);
        }
    }
}
