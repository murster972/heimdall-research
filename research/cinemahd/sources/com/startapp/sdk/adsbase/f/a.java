package com.startapp.sdk.adsbase.f;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.d;

public abstract class a implements d {

    /* renamed from: a  reason: collision with root package name */
    protected final Context f6352a;
    protected com.startapp.sdk.adsbase.infoevents.a b;
    private Runnable c;
    private Handler d = new Handler(Looper.getMainLooper());

    public a(Context context, Runnable runnable, com.startapp.sdk.adsbase.infoevents.a aVar) {
        this.f6352a = context;
        this.c = runnable;
        this.b = aVar;
    }

    public final void a() {
        ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                a.this.b();
            }
        });
    }

    /* access modifiers changed from: protected */
    public abstract void b();

    /* access modifiers changed from: protected */
    public final void a(Runnable runnable, long j) {
        this.d.postDelayed(runnable, j);
    }

    public void a(Object obj) {
        Handler handler = this.d;
        if (handler != null) {
            handler.removeCallbacksAndMessages((Object) null);
        }
        this.c.run();
    }
}
