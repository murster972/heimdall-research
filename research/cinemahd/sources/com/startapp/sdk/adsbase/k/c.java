package com.startapp.sdk.adsbase.k;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import java.lang.ref.WeakReference;

public final class c {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Handler f6411a = new Handler(Looper.getMainLooper());
    private final WeakReference<View> b;
    private final int c;

    public interface a {
        boolean onUpdate(boolean z);
    }

    public c(View view, int i, final a aVar) {
        this.b = new WeakReference<>(view);
        this.c = i;
        this.f6411a.postDelayed(new Runnable() {
            public final void run() {
                if (aVar.onUpdate(a.a((View) c.this.b.get(), c.this.c))) {
                    c.this.f6411a.postDelayed(this, 100);
                }
            }
        }, 100);
    }

    public final void a() {
        this.f6411a.removeCallbacksAndMessages((Object) null);
    }
}
