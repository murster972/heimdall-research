package com.startapp.sdk.adsbase.k;

import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.RegionIterator;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import com.startapp.common.parser.d;
import com.startapp.networkTest.enums.AppCategoryTypes;
import com.startapp.sdk.adsbase.j.j;
import java.util.ArrayList;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    public String f6409a = "";
    public AppCategoryTypes b = AppCategoryTypes.Unknown;
    public String c = "";
    public int d = -1;
    @d(b = ArrayList.class, c = j.class)
    public ArrayList<j> e = new ArrayList<>();

    public static boolean a(View view, int i) {
        if (view != null && view.getParent() != null && view.getRootView() != null && view.getRootView().getParent() != null && view.hasWindowFocus() && view.isShown() && view.getWidth() > 0 && view.getHeight() > 0) {
            Rect rect = new Rect();
            if (view.getGlobalVisibleRect(rect) && !rect.isEmpty() && a(rect, view) >= ((view.getWidth() * view.getHeight()) * Math.min(Math.max(1, i), 100)) / 100) {
                return true;
            }
        }
        return false;
    }

    private static int a(Rect rect, View view) {
        Region region = new Region(rect);
        Rect rect2 = new Rect();
        while (true) {
            int i = 0;
            if (!(view.getParent() instanceof ViewGroup)) {
                RegionIterator regionIterator = new RegionIterator(region);
                while (regionIterator.next(rect2)) {
                    i += rect2.width() * rect2.height();
                }
                return i;
            } else if (Build.VERSION.SDK_INT >= 11 && view.getAlpha() < 1.0f) {
                return 0;
            } else {
                ViewGroup viewGroup = (ViewGroup) view.getParent();
                int childCount = viewGroup.getChildCount();
                for (int indexOfChild = viewGroup.indexOfChild(view) + 1; indexOfChild < childCount; indexOfChild++) {
                    View childAt = viewGroup.getChildAt(indexOfChild);
                    if (childAt != null && childAt.getVisibility() == 0 && childAt.getGlobalVisibleRect(rect2)) {
                        region.op(rect2, Region.Op.DIFFERENCE);
                    }
                }
                view = viewGroup;
            }
        }
    }
}
