package com.startapp.sdk.adsbase.k;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.startapp.sdk.adsbase.h;
import java.lang.ref.WeakReference;

public class b implements Runnable {

    /* renamed from: a  reason: collision with root package name */
    private a f6410a;
    private Handler b = new Handler(Looper.getMainLooper());
    private WeakReference<View> c;
    private final h d;
    private final int e;
    private boolean f = true;

    public interface a {
        void a();
    }

    static {
        Class<b> cls = b.class;
    }

    public b(View view, h hVar, int i) {
        this.c = new WeakReference<>(view);
        this.d = hVar;
        this.e = i;
    }

    private boolean c() {
        h hVar = this.d;
        return (hVar == null || hVar.c() || this.c.get() == null) ? false : true;
    }

    public final void a(a aVar) {
        this.f6410a = aVar;
    }

    public final void b() {
        try {
            if (this.d != null) {
                this.d.a(false);
            }
            if (this.b != null) {
                this.b.removeCallbacksAndMessages((Object) null);
            }
        } catch (Exception unused) {
        }
    }

    public void run() {
        try {
            if (c()) {
                boolean a2 = a.a((View) this.c.get(), this.e);
                if (a2 && this.f) {
                    this.f = false;
                    this.d.a();
                    a aVar = this.f6410a;
                } else if (!a2 && !this.f) {
                    this.f = true;
                    this.d.b();
                    if (this.f6410a != null) {
                        this.f6410a.a();
                    }
                }
                this.b.postDelayed(this, 100);
                return;
            }
            b();
        } catch (Exception unused) {
            b();
        }
    }

    public final void a() {
        if (c()) {
            run();
        }
    }

    public b(WeakReference<View> weakReference, h hVar, int i) {
        this.c = weakReference;
        this.d = hVar;
        this.e = i;
    }
}
