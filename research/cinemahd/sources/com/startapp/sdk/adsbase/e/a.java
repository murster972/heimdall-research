package com.startapp.sdk.adsbase.e;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import com.startapp.common.SDKException;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.g;
import com.startapp.sdk.adsbase.remoteconfig.NetworkDiagnosticConfig;
import java.util.List;
import java.util.concurrent.Executor;
import org.joda.time.DateTimeConstants;

public class a {

    /* renamed from: a  reason: collision with root package name */
    protected final Executor f6344a;
    private Context b;
    private final SharedPreferences c;
    private b d;
    private final g<NetworkDiagnosticConfig> e;
    private boolean f;
    private final Runnable g = new Runnable() {
        public final void run() {
            a.this.b();
        }
    };

    /* renamed from: com.startapp.sdk.adsbase.e.a$a  reason: collision with other inner class name */
    static class C0075a {

        /* renamed from: a  reason: collision with root package name */
        final long f6349a;
        final long b;

        private C0075a(long j, long j2) {
            this.f6349a = j;
            this.b = j2;
        }

        static C0075a a(String str) {
            if (str == null) {
                return null;
            }
            String[] split = str.split(",");
            if (split.length != 2 || split[0] == null || split[1] == null) {
                return null;
            }
            try {
                return new C0075a(Long.parseLong(split[0]), Long.parseLong(split[1]));
            } catch (NumberFormatException unused) {
                return null;
            }
        }
    }

    public a(Context context, SharedPreferences sharedPreferences, b bVar, Executor executor, g<NetworkDiagnosticConfig> gVar) {
        this.b = context;
        this.c = sharedPreferences;
        this.d = bVar;
        this.f6344a = executor;
        this.e = gVar;
    }

    private boolean a(int i) {
        NetworkDiagnosticConfig c2 = c();
        return c2 != null && (c2.e() & i) == i;
    }

    private NetworkDiagnosticConfig c() {
        NetworkDiagnosticConfig a2 = this.e.a();
        if (a2 == null || !a2.a()) {
            return null;
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public final void b() {
        NetworkDiagnosticConfig c2 = c();
        if (c2 != null && !this.f) {
            long j = this.c.getLong("181bb7005f9db75a", 0) + ((long) (c2.b() * DateTimeConstants.MILLIS_PER_MINUTE));
            long currentTimeMillis = System.currentTimeMillis();
            if (j <= currentTimeMillis) {
                this.f = a(c2, currentTimeMillis);
            }
        }
    }

    public final c a() {
        return new c(this);
    }

    public final void a(String str, String str2, SDKException sDKException, long j) {
        final String str3;
        int i;
        String str4;
        NetworkDiagnosticConfig c2 = c();
        if (c2 != null) {
            if (sDKException != null) {
                if (sDKException.getCause() != null) {
                    i = 2;
                    str4 = "Failure: " + sDKException.getCause().getClass().getName();
                } else {
                    i = 1;
                    str4 = "Error: " + sDKException.b();
                }
                str3 = str4;
            } else {
                str3 = "Success";
                i = 4;
            }
            if ((c2.d() & i) != 0) {
                Uri a2 = sDKException != null ? sDKException.a() : null;
                if (a2 == null) {
                    a2 = Uri.parse(str2).buildUpon().query((String) null).build();
                }
                final String str5 = str + ' ' + a2;
                final long j2 = j;
                this.f6344a.execute(new Runnable() {
                    public final void run() {
                        a.this.a(str5, str3, j2);
                    }
                });
            }
            if (i == 4) {
                this.f6344a.execute(this.g);
            }
        }
    }

    /* access modifiers changed from: protected */
    public final void a(String str, String str2, long j) {
        try {
            this.d.a(str, str2, System.currentTimeMillis(), j);
        } catch (Throwable th) {
            if (a(1)) {
                new e(th).a(this.b);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x00f4 A[Catch:{ all -> 0x0105 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(com.startapp.sdk.adsbase.remoteconfig.NetworkDiagnosticConfig r25, long r26) {
        /*
            r24 = this;
            r1 = r24
            r2 = r26
            java.lang.String r0 = ";"
            r4 = 0
            r5 = 2
            r6 = 0
            com.startapp.sdk.adsbase.e.b r7 = r1.d     // Catch:{ all -> 0x00eb }
            android.database.Cursor r7 = r7.a(r2)     // Catch:{ all -> 0x00eb }
            r8 = 1
            if (r7 == 0) goto L_0x00d2
            boolean r9 = r7.moveToFirst()     // Catch:{ all -> 0x00d0 }
            if (r9 == 0) goto L_0x00d2
            r9 = r6
        L_0x0019:
            long r10 = r7.getLong(r4)     // Catch:{ all -> 0x00d0 }
            long r12 = r7.getLong(r8)     // Catch:{ all -> 0x00d0 }
            java.lang.String r14 = r7.getString(r5)     // Catch:{ all -> 0x00d0 }
            r15 = 3
            java.lang.String r15 = r7.getString(r15)     // Catch:{ all -> 0x00d0 }
            r4 = 4
            r16 = r9
            long r8 = r7.getLong(r4)     // Catch:{ all -> 0x00d0 }
            int r4 = r25.c()     // Catch:{ all -> 0x00d0 }
            r17 = r6
            long r5 = (long) r4     // Catch:{ all -> 0x00d0 }
            int r4 = (r8 > r5 ? 1 : (r8 == r5 ? 0 : -1))
            if (r4 < 0) goto L_0x00bf
            r4 = 5
            long r4 = r7.getLong(r4)     // Catch:{ all -> 0x00d0 }
            r6 = 6
            r18 = r14
            r19 = r15
            long r14 = r7.getLong(r6)     // Catch:{ all -> 0x00d0 }
            r6 = 7
            r20 = r12
            long r12 = r7.getLong(r6)     // Catch:{ all -> 0x00d0 }
            r6 = 8
            r22 = r10
            long r10 = r7.getLong(r6)     // Catch:{ all -> 0x00d0 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d0 }
            r6.<init>()     // Catch:{ all -> 0x00d0 }
            r6.append(r8)     // Catch:{ all -> 0x00d0 }
            r6.append(r0)     // Catch:{ all -> 0x00d0 }
            r6.append(r4)     // Catch:{ all -> 0x00d0 }
            r6.append(r0)     // Catch:{ all -> 0x00d0 }
            r6.append(r14)     // Catch:{ all -> 0x00d0 }
            r6.append(r0)     // Catch:{ all -> 0x00d0 }
            r6.append(r12)     // Catch:{ all -> 0x00d0 }
            r6.append(r0)     // Catch:{ all -> 0x00d0 }
            r6.append(r10)     // Catch:{ all -> 0x00d0 }
            r6.append(r0)     // Catch:{ all -> 0x00d0 }
            r6.append(r2)     // Catch:{ all -> 0x00d0 }
            java.lang.String r4 = r6.toString()     // Catch:{ all -> 0x00d0 }
            com.startapp.sdk.adsbase.infoevents.e r6 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x00d0 }
            com.startapp.sdk.adsbase.infoevents.InfoEventCategory r5 = com.startapp.sdk.adsbase.infoevents.InfoEventCategory.NETWORK_DIAGNOSTIC     // Catch:{ all -> 0x00d0 }
            r6.<init>((com.startapp.sdk.adsbase.infoevents.InfoEventCategory) r5)     // Catch:{ all -> 0x00d0 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00d0 }
            r5.<init>()     // Catch:{ all -> 0x00d0 }
            r8 = r22
            r5.append(r8)     // Catch:{ all -> 0x00d0 }
            java.lang.String r8 = ","
            r5.append(r8)     // Catch:{ all -> 0x00d0 }
            r8 = r20
            r5.append(r8)     // Catch:{ all -> 0x00d0 }
            java.lang.String r5 = r5.toString()     // Catch:{ all -> 0x00d0 }
            r6.o(r5)     // Catch:{ all -> 0x00d0 }
            r5 = r18
            r6.l(r5)     // Catch:{ all -> 0x00d0 }
            r5 = r19
            r6.f(r5)     // Catch:{ all -> 0x00d0 }
            r6.g(r4)     // Catch:{ all -> 0x00d0 }
            if (r17 != 0) goto L_0x00b6
            r17 = r6
        L_0x00b6:
            if (r16 == 0) goto L_0x00bd
            r4 = r16
            r4.a((com.startapp.sdk.adsbase.infoevents.e) r6)     // Catch:{ all -> 0x00d0 }
        L_0x00bd:
            r9 = r6
            goto L_0x00c2
        L_0x00bf:
            r4 = r16
            r9 = r4
        L_0x00c2:
            r6 = r17
            boolean r4 = r7.moveToNext()     // Catch:{ all -> 0x00d0 }
            if (r4 != 0) goto L_0x00cb
            goto L_0x00d2
        L_0x00cb:
            r4 = 0
            r5 = 2
            r8 = 1
            goto L_0x0019
        L_0x00d0:
            r0 = move-exception
            goto L_0x00ed
        L_0x00d2:
            if (r6 == 0) goto L_0x00e5
            android.content.Context r0 = r1.b     // Catch:{ all -> 0x00d0 }
            com.startapp.sdk.adsbase.e.a$3 r4 = new com.startapp.sdk.adsbase.e.a$3     // Catch:{ all -> 0x00d0 }
            r4.<init>(r2)     // Catch:{ all -> 0x00d0 }
            r6.a(r0, r4)     // Catch:{ all -> 0x00d0 }
            if (r7 == 0) goto L_0x00e3
            r7.close()
        L_0x00e3:
            r0 = 1
            return r0
        L_0x00e5:
            r1.a((long) r2)     // Catch:{ all -> 0x00d0 }
            if (r7 == 0) goto L_0x0103
            goto L_0x0100
        L_0x00eb:
            r0 = move-exception
            r7 = r6
        L_0x00ed:
            r2 = 2
            boolean r2 = r1.a((int) r2)     // Catch:{ all -> 0x0105 }
            if (r2 == 0) goto L_0x00fe
            com.startapp.sdk.adsbase.infoevents.e r2 = new com.startapp.sdk.adsbase.infoevents.e     // Catch:{ all -> 0x0105 }
            r2.<init>((java.lang.Throwable) r0)     // Catch:{ all -> 0x0105 }
            android.content.Context r0 = r1.b     // Catch:{ all -> 0x0105 }
            r2.a((android.content.Context) r0)     // Catch:{ all -> 0x0105 }
        L_0x00fe:
            if (r7 == 0) goto L_0x0103
        L_0x0100:
            r7.close()
        L_0x0103:
            r2 = 0
            return r2
        L_0x0105:
            r0 = move-exception
            if (r7 == 0) goto L_0x010b
            r7.close()
        L_0x010b:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.e.a.a(com.startapp.sdk.adsbase.remoteconfig.NetworkDiagnosticConfig, long):boolean");
    }

    /* access modifiers changed from: protected */
    public final void a(long j, List<C0075a> list) {
        this.f = false;
        try {
            for (C0075a next : list) {
                this.d.a(next.f6349a, next.b, j);
            }
        } catch (Throwable th) {
            if (a(4)) {
                new e(th).a(this.b);
            }
        }
        try {
            a(j);
        } catch (Throwable th2) {
            if (a(8)) {
                new e(th2).a(this.b);
            }
        }
    }

    private void a(long j) {
        this.c.edit().putLong("181bb7005f9db75a", j).commit();
    }
}
