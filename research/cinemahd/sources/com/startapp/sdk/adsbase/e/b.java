package com.startapp.sdk.adsbase.e;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public final class b extends SQLiteOpenHelper {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f6350a = {"id"};
    private volatile SQLiteDatabase b;
    private final Object c = new Object();

    public b(Context context, String str) {
        super(context, str, (SQLiteDatabase.CursorFactory) null, 1);
    }

    private SQLiteDatabase a() {
        SQLiteDatabase sQLiteDatabase = this.b;
        if (sQLiteDatabase == null) {
            synchronized (this.c) {
                sQLiteDatabase = this.b;
                if (sQLiteDatabase == null) {
                    sQLiteDatabase = getWritableDatabase();
                    this.b = sQLiteDatabase;
                }
            }
        }
        return sQLiteDatabase;
    }

    public final void onCreate(SQLiteDatabase sQLiteDatabase) {
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS requests (id INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL,value TEXT NOT NULL UNIQUE,CHECK (value = TRIM(value) AND LENGTH(value) > 0));");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS statuses (id INTEGER PRIMARY KEY ASC AUTOINCREMENT NOT NULL,value TEXT NOT NULL UNIQUE,CHECK (value = TRIM(value) AND LENGTH(value) > 0));");
        sQLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS traces (requestId INTEGER NOT NULL,statusId INTEGER NOT NULL,timeMillis INTEGER NOT NULL,durationNanos INTEGER NOT NULL,FOREIGN KEY (requestId)REFERENCES requests (requestId)ON UPDATE CASCADE ON DELETE CASCADE,FOREIGN KEY (statusId)REFERENCES statuses (statusId)ON UPDATE CASCADE ON DELETE CASCADE,CHECK (timeMillis > 0),CHECK (durationNanos > 0));");
    }

    public final void onUpgrade(SQLiteDatabase sQLiteDatabase, int i, int i2) {
    }

    public final void a(String str, String str2, long j, long j2) {
        String str3 = str;
        String str4 = str2;
        SQLiteDatabase a2 = a();
        a2.beginTransaction();
        try {
            ContentValues contentValues = new ContentValues();
            long a3 = a(a2, "requests", str);
            if (a3 < 0) {
                contentValues.clear();
                contentValues.put("value", str);
                a3 = a2.insert("requests", (String) null, contentValues);
            }
            long a4 = a(a2, "statuses", str4);
            if (a4 < 0) {
                contentValues.clear();
                contentValues.put("value", str4);
                a4 = a2.insert("statuses", (String) null, contentValues);
            }
            contentValues.clear();
            contentValues.put("requestId", Long.valueOf(a3));
            contentValues.put("statusId", Long.valueOf(a4));
            contentValues.put("timeMillis", Long.valueOf(j));
            contentValues.put("durationNanos", Long.valueOf(j2));
            a2.insert("traces", (String) null, contentValues);
            a2.setTransactionSuccessful();
        } finally {
            a2.endTransaction();
        }
    }

    public final Cursor a(long j) {
        return a().rawQuery("SELECT requestId, statusId, requests.value AS request, statuses.value AS status, COUNT(*), MIN(durationNanos), MAX(durationNanos), AVG(durationNanos), MIN(timeMillis) FROM traces JOIN requests ON requests.id = traces.requestId JOIN statuses ON statuses.id = traces.statusId WHERE timeMillis < ? GROUP BY requestId, statusId", new String[]{String.valueOf(j)});
    }

    public final void a(long j, long j2, long j3) {
        a().delete("traces", "requestId = ? AND statusId = ? AND timeMillis < ?", new String[]{String.valueOf(j), String.valueOf(j2), String.valueOf(j3)});
    }

    private static long a(SQLiteDatabase sQLiteDatabase, String str, String str2) {
        Cursor query = sQLiteDatabase.query(str, f6350a, "value=?", new String[]{str2}, (String) null, (String) null, (String) null);
        if (query != null) {
            try {
                if (query.moveToFirst()) {
                    return query.getLong(0);
                }
            } finally {
                if (query != null) {
                    query.close();
                }
            }
        }
        if (query == null) {
            return -1;
        }
        query.close();
        return -1;
    }
}
