package com.startapp.sdk.adsbase.e;

import android.os.Build;
import android.os.SystemClock;
import com.startapp.common.SDKException;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final a f6351a;
    private final long b = a();

    public c(a aVar) {
        this.f6351a = aVar;
    }

    public final void a(String str, String str2, SDKException sDKException) {
        long a2 = a();
        this.f6351a.a(str, str2, sDKException, a2 - this.b);
    }

    private static long a() {
        if (Build.VERSION.SDK_INT < 17) {
            return SystemClock.elapsedRealtime() * 1000000;
        }
        return SystemClock.elapsedRealtimeNanos();
    }
}
