package com.startapp.sdk.adsbase;

import android.content.Context;
import com.startapp.sdk.adsbase.adinformation.AdInformationOverrides;
import com.startapp.sdk.adsbase.adlisteners.AdDisplayListener;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.adlisteners.b;
import com.startapp.sdk.adsbase.cache.CacheMetaData;
import com.startapp.sdk.adsbase.consent.a;
import com.startapp.sdk.adsbase.j.p;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import com.startapp.sdk.adsbase.remoteconfig.MetaDataRequest;
import com.startapp.sdk.c.c;
import java.io.Serializable;

public abstract class Ad implements Serializable {
    private static boolean b = false;
    private static final long serialVersionUID = 1;

    /* renamed from: a  reason: collision with root package name */
    protected transient Context f6243a;
    protected ActivityExtra activityExtra;
    protected Long adCacheTtl = null;
    private AdInformationOverrides adInfoOverride;
    protected boolean belowMinCPM = false;
    protected String errorMessage;
    protected Serializable extraData = null;
    /* access modifiers changed from: private */
    public Long lastLoadTime = null;
    private AdDisplayListener.NotDisplayedReason notDisplayedReason;
    protected AdPreferences.Placement placement;
    private AdState state = AdState.UN_INITIALIZED;
    private AdType type;
    private boolean videoCancelCallBack;

    public enum AdState {
        UN_INITIALIZED,
        PROCESSING,
        READY
    }

    public enum AdType {
        INTERSTITIAL,
        RICH_TEXT,
        VIDEO,
        REWARDED_VIDEO,
        NON_VIDEO,
        VIDEO_NO_VAST
    }

    public Ad(Context context, AdPreferences.Placement placement2) {
        this.f6243a = context;
        this.placement = placement2;
        u.c();
        this.adInfoOverride = AdInformationOverrides.a();
    }

    /* access modifiers changed from: protected */
    public final void a(AdDisplayListener.NotDisplayedReason notDisplayedReason2) {
        this.notDisplayedReason = notDisplayedReason2;
    }

    /* access modifiers changed from: protected */
    public abstract void a(AdPreferences adPreferences, b bVar);

    /* access modifiers changed from: protected */
    public Long b() {
        return this.lastLoadTime;
    }

    /* access modifiers changed from: protected */
    public Long c() {
        long f = f();
        Long l = this.adCacheTtl;
        if (l != null) {
            f = Math.min(l.longValue(), f);
        }
        return Long.valueOf(f);
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        return this.videoCancelCallBack;
    }

    /* access modifiers changed from: protected */
    public boolean e_() {
        if (this.lastLoadTime != null && System.currentTimeMillis() - this.lastLoadTime.longValue() > c().longValue()) {
            return true;
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public long f() {
        return CacheMetaData.a().b().a();
    }

    public abstract String getAdId();

    public AdInformationOverrides getAdInfoOverride() {
        return this.adInfoOverride;
    }

    public Boolean getConsentApc() {
        return null;
    }

    public Long getConsentTimestamp() {
        return null;
    }

    public Integer getConsentType() {
        return null;
    }

    public Context getContext() {
        return this.f6243a;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public Serializable getExtraData() {
        return this.extraData;
    }

    public AdDisplayListener.NotDisplayedReason getNotDisplayedReason() {
        return this.notDisplayedReason;
    }

    public AdState getState() {
        return this.state;
    }

    public AdType getType() {
        return this.type;
    }

    /* access modifiers changed from: protected */
    public AdPreferences.Placement i() {
        return this.placement;
    }

    public boolean isBelowMinCPM() {
        return this.belowMinCPM;
    }

    public boolean isReady() {
        return this.state == AdState.READY && !e_();
    }

    @Deprecated
    public boolean load() {
        return load(new AdPreferences(), (AdEventListener) null);
    }

    public void setActivityExtra(ActivityExtra activityExtra2) {
        this.activityExtra = activityExtra2;
    }

    public void setAdInfoOverride(AdInformationOverrides adInformationOverrides) {
        this.adInfoOverride = adInformationOverrides;
    }

    public void setConsentApc(Boolean bool) {
    }

    public void setConsentTimestamp(Long l) {
    }

    public void setConsentType(Integer num) {
    }

    public void setContext(Context context) {
        this.f6243a = context;
    }

    public void setErrorMessage(String str) {
        this.errorMessage = str;
    }

    public void setExtraData(Serializable serializable) {
        this.extraData = serializable;
    }

    public void setState(AdState adState) {
        this.state = adState;
    }

    @Deprecated
    public boolean show() {
        return false;
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        this.videoCancelCallBack = z;
    }

    @Deprecated
    public boolean load(AdEventListener adEventListener) {
        return load(new AdPreferences(), adEventListener);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences) {
        return load(adPreferences, (AdEventListener) null);
    }

    @Deprecated
    public boolean load(AdPreferences adPreferences, AdEventListener adEventListener) {
        return load(adPreferences, b.a(this.f6243a, adEventListener), true);
    }

    public boolean load(final AdPreferences adPreferences, final b bVar, boolean z) {
        boolean z2;
        String str;
        final a f = c.a(this.f6243a).f();
        final AnonymousClass1 r1 = new b() {
            public final void a(Ad ad) {
                Ad.this.lastLoadTime = Long.valueOf(System.currentTimeMillis());
                bVar.a(ad);
                f.a(ad.getConsentType(), ad.getConsentTimestamp(), ad.getConsentApc(), false, true);
                Context context = Ad.this.f6243a;
                u.a(context, false, "Loaded " + u.a(ad) + " ad with creative ID - " + ad.getAdId());
            }

            public final void b(Ad ad) {
                bVar.b(ad);
                String errorMessage = ad.getErrorMessage();
                if (errorMessage == null) {
                    errorMessage = "";
                } else if (errorMessage.contains("204")) {
                    errorMessage = "NO FILL";
                }
                Context context = Ad.this.f6243a;
                u.a(context, true, "Failed to load " + u.a(ad) + " ad: " + errorMessage);
            }
        };
        if (!b) {
            SimpleTokenUtils.c(this.f6243a);
            b = true;
        }
        if (this.state != AdState.UN_INITIALIZED) {
            str = "load() was already called.";
            z2 = true;
        } else {
            str = "";
            z2 = false;
        }
        if (!u.c(this.f6243a)) {
            str = "network not available.";
            z2 = true;
        }
        if (!MetaData.G().M()) {
            str = "serving ads disabled";
            z2 = true;
        }
        if (z2) {
            setErrorMessage("Ad wasn't loaded: ".concat(str));
            r1.b(this);
            return false;
        }
        setConsentType(f.d());
        setConsentTimestamp(f.e());
        setConsentApc(f.f());
        setState(AdState.PROCESSING);
        AnonymousClass2 r8 = new com.startapp.sdk.adsbase.remoteconfig.b() {
            public final void a(MetaDataRequest.RequestReason requestReason, boolean z) {
                Ad.this.a(adPreferences, r1);
            }

            public final void a() {
                Ad.this.a(adPreferences, r1);
            }
        };
        if (adPreferences.getType() != null) {
            this.type = adPreferences.getType();
        }
        MetaData.G().a(this.f6243a, adPreferences, p.d().c(), z, r8, false);
        return true;
    }
}
