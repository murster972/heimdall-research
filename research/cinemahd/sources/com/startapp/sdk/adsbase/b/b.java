package com.startapp.sdk.adsbase.b;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.startapp.common.d;
import com.startapp.sdk.adsbase.infoevents.e;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    protected d f6303a;
    protected a b = new a();
    private Context c;
    private BluetoothAdapter d;
    private BroadcastReceiver e;

    public b(Context context, d dVar) {
        this.c = context;
        this.f6303a = dVar;
        this.d = com.startapp.common.b.b.a(this.c, "android.permission.BLUETOOTH") ? BluetoothAdapter.getDefaultAdapter() : null;
    }

    @SuppressLint({"MissingPermission"})
    private Set<BluetoothDevice> c() {
        HashSet hashSet = new HashSet();
        try {
            if (com.startapp.common.b.b.a(this.c, "android.permission.BLUETOOTH") && this.d.isEnabled()) {
                return this.d.getBondedDevices();
            }
        } catch (Exception e2) {
            new e((Throwable) e2).a(this.c);
        }
        return hashSet;
    }

    @SuppressLint({"MissingPermission"})
    public final void a(boolean z) {
        BluetoothAdapter bluetoothAdapter = this.d;
        if (bluetoothAdapter == null || !bluetoothAdapter.isEnabled()) {
            this.f6303a.a((Object) null);
            return;
        }
        this.b.a(c());
        if (!z || !com.startapp.common.b.b.a(this.c, "android.permission.BLUETOOTH_ADMIN")) {
            this.f6303a.a(b());
            return;
        }
        IntentFilter intentFilter = new IntentFilter("android.bluetooth.device.action.FOUND");
        this.e = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if ("android.bluetooth.device.action.FOUND".equals(action)) {
                    b.this.b.a((BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE"));
                } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                    b.this.a();
                    b bVar = b.this;
                    bVar.f6303a.a(bVar.b());
                }
            }
        };
        try {
            this.c.registerReceiver(this.e, intentFilter);
            this.d.startDiscovery();
        } catch (Exception e2) {
            this.d.cancelDiscovery();
            this.f6303a.a(b());
            new e((Throwable) e2).a(this.c);
        }
    }

    public final JSONObject b() {
        try {
            return this.b.a();
        } catch (Exception unused) {
            return null;
        }
    }

    @SuppressLint({"MissingPermission"})
    public final void a() {
        BluetoothAdapter bluetoothAdapter;
        if (com.startapp.common.b.b.a(this.c, "android.permission.BLUETOOTH_ADMIN") && this.e != null && (bluetoothAdapter = this.d) != null) {
            bluetoothAdapter.cancelDiscovery();
            try {
                this.c.unregisterReceiver(this.e);
            } catch (Exception e2) {
                new e((Throwable) e2).a(this.c);
            }
            this.e = null;
        }
    }
}
