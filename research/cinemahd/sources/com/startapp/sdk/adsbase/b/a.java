package com.startapp.sdk.adsbase.b;

import android.bluetooth.BluetoothDevice;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.HashSet;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONObject;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private Set<BluetoothDevice> f6302a;
    private Set<BluetoothDevice> b;

    private static JSONArray b(Set<BluetoothDevice> set) {
        try {
            JSONArray jSONArray = new JSONArray();
            for (BluetoothDevice next : set) {
                JSONObject jSONObject = new JSONObject();
                jSONObject.put("bluetoothClass", next.getBluetoothClass().getDeviceClass());
                jSONObject.put(MediationMetaData.KEY_NAME, next.getName());
                jSONObject.put("mac", next.getAddress());
                jSONObject.put("bondState", next.getBondState());
                jSONArray.put(jSONObject);
            }
            return jSONArray;
        } catch (Exception unused) {
            return null;
        }
    }

    public final void a(BluetoothDevice bluetoothDevice) {
        if (this.b == null) {
            this.b = new HashSet();
        }
        this.b.add(bluetoothDevice);
    }

    public final void a(Set<BluetoothDevice> set) {
        this.f6302a = set;
    }

    public final JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            if (this.f6302a != null && this.f6302a.size() > 0) {
                jSONObject.put("paired", b(this.f6302a));
            }
            if (this.b != null && this.b.size() > 0) {
                jSONObject.put("available", b(this.b));
            }
        } catch (Exception unused) {
        }
        if (jSONObject.length() > 0) {
            return jSONObject;
        }
        return null;
    }
}
