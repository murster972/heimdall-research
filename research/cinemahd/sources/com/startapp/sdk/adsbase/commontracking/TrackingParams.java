package com.startapp.sdk.adsbase.commontracking;

import com.original.tase.model.socket.UserResponces;
import com.startapp.sdk.adsbase.j.p;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class TrackingParams implements Serializable {
    private static final long serialVersionUID = 1;
    private String adTag;
    private String clientSessionId;
    private String nonImpressionReason;
    private int offset;
    private String profileId;

    public TrackingParams() {
        this((String) null);
    }

    public final TrackingParams a(int i) {
        this.offset = i;
        return this;
    }

    public final TrackingParams c(String str) {
        this.nonImpressionReason = str;
        return this;
    }

    /* access modifiers changed from: protected */
    public String d() {
        if (this.offset <= 0) {
            return "";
        }
        return "&offset=" + this.offset;
    }

    public final String f() {
        return this.adTag;
    }

    public final String g() {
        return this.profileId;
    }

    public final int h() {
        return this.offset;
    }

    public TrackingParams(String str) {
        this.adTag = str;
        this.clientSessionId = p.d().a();
        this.profileId = MetaData.G().C();
        this.offset = 0;
    }

    private static String a(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (UnsupportedEncodingException unused) {
            return "";
        }
    }

    public String a() {
        String str;
        String str2;
        String str3;
        StringBuilder sb = new StringBuilder();
        String str4 = this.adTag;
        String str5 = "";
        if (str4 == null || str4.equals(str5)) {
            str = str5;
        } else {
            int length = this.adTag.length();
            int i = UserResponces.USER_RESPONCE_SUCCSES;
            if (length < 200) {
                i = this.adTag.length();
            }
            str = "&adTag=" + a(this.adTag.substring(0, i));
        }
        sb.append(str);
        if (this.clientSessionId != null) {
            str2 = "&clientSessionId=" + a(this.clientSessionId);
        } else {
            str2 = str5;
        }
        sb.append(str2);
        if (this.profileId != null) {
            str3 = "&profileId=" + a(this.profileId);
        } else {
            str3 = str5;
        }
        sb.append(str3);
        sb.append(d());
        String str6 = this.nonImpressionReason;
        if (str6 != null && !str6.equals(str5)) {
            str5 = "&isShown=false&reason=" + a(this.nonImpressionReason);
        }
        sb.append(str5);
        return sb.toString();
    }
}
