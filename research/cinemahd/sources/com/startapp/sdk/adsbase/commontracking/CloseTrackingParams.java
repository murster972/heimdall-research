package com.startapp.sdk.adsbase.commontracking;

public class CloseTrackingParams extends TrackingParams {
    private static final long serialVersionUID = 1;
    private final long duration;

    public CloseTrackingParams(long j, String str) {
        super(str);
        this.duration = j;
    }

    public final String a() {
        return super.a() + "&displayDuration=" + Math.max(0, this.duration);
    }
}
