package com.startapp.sdk.adsbase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Build;
import android.os.SystemClock;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import com.startapp.common.SDKException;
import com.startapp.common.a.a;
import com.startapp.common.b.b;
import com.startapp.sdk.adsbase.infoevents.e;
import com.startapp.sdk.adsbase.j.i;
import com.startapp.sdk.adsbase.j.k;
import com.startapp.sdk.adsbase.j.m;
import com.startapp.sdk.adsbase.j.p;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.startapp.sdk.adsbase.remoteconfig.MetaData;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public abstract class c {
    private String A;
    private String B;
    private String C;
    private String D;
    private String E;
    private String F;
    private String G = "android";
    private int H;
    private int I;
    private float J;
    private Boolean K;
    private int L = 3;
    private String M;
    private String N;
    private int O;
    private boolean P;
    private boolean Q;
    private boolean R;
    private boolean S;
    private String T;
    private final int U;

    /* renamed from: a  reason: collision with root package name */
    protected Integer f6305a;
    private Map<String, String> b = new HashMap();
    private String c;
    private String d;
    private String e = "4.6.3";
    private Map<String, String> f = new TreeMap();
    private String g;
    private a h;
    private String i;
    private String j;
    private String k;
    private String l;
    private String m;
    private String n;
    private String o;
    private String p;
    private String q;
    private Boolean r;
    private Boolean s;
    private String t;
    private String u;
    private String v;
    private String w;
    private String x;
    private String y;
    private String z;

    public c(int i2) {
        this.U = i2;
    }

    @SuppressLint({"MissingPermission"})
    private void f(Context context) {
        com.startapp.sdk.d.d.a aVar = (com.startapp.sdk.d.d.a) com.startapp.sdk.c.c.a(context).c().c();
        this.z = aVar.d();
        this.A = aVar.c();
        this.B = aVar.a();
        this.C = aVar.b();
    }

    public final int a() {
        return this.U;
    }

    public final String b() {
        return this.d;
    }

    public final void c(int i2) {
        this.H = i2;
    }

    public final void d(int i2) {
        this.I = i2;
    }

    public final void e(Context context) {
        Object systemService = context.getSystemService("phone");
        if (systemService instanceof TelephonyManager) {
            TelephonyManager telephonyManager = (TelephonyManager) systemService;
            a(telephonyManager);
            b(telephonyManager);
            f(context);
            this.E = b.b(context, telephonyManager);
        }
    }

    public String toString() {
        return "BaseRequest [parameters=" + this.b + "]";
    }

    public final void a(String str) {
        this.e = str;
    }

    public void b(int i2) {
        this.f6305a = null;
    }

    public final String c() {
        return this.e;
    }

    public final String d() {
        String str = this.M;
        return str == null ? "" : str;
    }

    private void a(Context context) {
        this.K = Boolean.valueOf(u.g(context));
    }

    public final void b(Context context) {
        if (!MetaData.G().R()) {
            a b2 = com.startapp.sdk.c.c.a(context).d().b();
            this.h = b2;
            if (TextUtils.isEmpty(b2.a()) || "0".equals(b2.a())) {
                if (!j.a(context, "advertising_id_retrieving_failed", Boolean.FALSE).booleanValue()) {
                    j.b(context, "advertising_id_retrieving_failed", Boolean.TRUE);
                    Throwable[] d2 = b2.d();
                    if (d2 != null) {
                        e eVar = null;
                        for (Throwable th : d2) {
                            if (th != null) {
                                if (eVar == null) {
                                    eVar = new e(th);
                                } else {
                                    eVar.a(new e(th));
                                }
                            }
                        }
                        if (eVar != null) {
                            eVar.a(context);
                        }
                    }
                }
                this.k = com.startapp.sdk.c.c.a(context).g().a();
            }
        }
    }

    public final void c(Context context) {
        this.t = com.startapp.common.b.e.a(context);
        this.u = "e106";
        this.D = "e106";
        com.startapp.common.c a2 = com.startapp.common.c.a();
        if (a2 != null) {
            String b2 = a2.b();
            this.D = b2;
            this.u = b2;
        }
    }

    public final void d(Context context) {
        b j2 = com.startapp.sdk.c.c.a(context).j();
        this.c = j2.a();
        this.d = j2.b();
    }

    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0006 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(android.content.Context r2, com.startapp.sdk.adsbase.model.AdPreferences r3) {
        /*
            r1 = this;
            r1.d((android.content.Context) r2)
            r1.b(r2, r3)     // Catch:{ all -> 0x0006 }
        L_0x0006:
            r1.c((android.content.Context) r2)     // Catch:{ all -> 0x000a }
            goto L_0x0013
        L_0x000a:
            r3 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r3)
            r0.a((android.content.Context) r2)
        L_0x0013:
            r1.e(r2)     // Catch:{ all -> 0x0017 }
            goto L_0x0020
        L_0x0017:
            r3 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r3)
            r0.a((android.content.Context) r2)
        L_0x0020:
            com.startapp.sdk.c.c r3 = com.startapp.sdk.c.c.a(r2)     // Catch:{ all -> 0x002f }
            com.startapp.sdk.g.a r3 = r3.e()     // Catch:{ all -> 0x002f }
            java.lang.String r3 = r3.a((java.lang.Object) r1)     // Catch:{ all -> 0x002f }
            r1.F = r3     // Catch:{ all -> 0x002f }
            goto L_0x0038
        L_0x002f:
            r3 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r3)
            r0.a((android.content.Context) r2)
        L_0x0038:
            r1.b((android.content.Context) r2)     // Catch:{ all -> 0x003c }
            return
        L_0x003c:
            r3 = move-exception
            com.startapp.sdk.adsbase.infoevents.e r0 = new com.startapp.sdk.adsbase.infoevents.e
            r0.<init>((java.lang.Throwable) r3)
            r0.a((android.content.Context) r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.sdk.adsbase.c.a(android.content.Context, com.startapp.sdk.adsbase.model.AdPreferences):void");
    }

    public final m e() throws SDKException {
        i iVar = new i();
        a((m) iVar);
        return iVar;
    }

    public final String f() throws SDKException {
        k kVar = new k();
        a((m) kVar);
        return kVar.toString();
    }

    /* access modifiers changed from: protected */
    public void a(m mVar) throws SDKException {
        mVar.a("publisherId", this.c, false);
        mVar.a("productId", this.d, true);
        mVar.a("os", this.G, true);
        mVar.a("sdkVersion", this.e, false);
        mVar.a("flavor", 1023, false);
        Map<String, String> map = this.f;
        if (map != null && !map.isEmpty()) {
            String str = "";
            for (String next : this.f.keySet()) {
                str = str + next + ":" + this.f.get(next) + ";";
            }
            mVar.a("frameworksData", str.substring(0, str.length() - 1), false, false);
        }
        mVar.a("packageId", this.i, false);
        mVar.a("installerPkg", this.j, false);
        mVar.a("age", this.g, false);
        a aVar = this.h;
        if (aVar != null) {
            mVar.a("userAdvertisingId", aVar.a(), false);
            if (this.h.c()) {
                mVar.a("limat", Boolean.valueOf(this.h.c()), false);
            }
            mVar.a("advertisingIdSource", this.h.b(), false);
        } else {
            String str2 = this.k;
            if (str2 != null) {
                mVar.a("userId", str2, false);
            }
        }
        mVar.a("model", this.l, false);
        mVar.a("manufacturer", this.m, false);
        mVar.a("deviceVersion", this.n, false);
        mVar.a("locale", this.o, false);
        mVar.a("localeList", this.p, false);
        mVar.a("inputLangs", this.q, false);
        mVar.a("isp", this.v, false);
        mVar.a("ispName", this.w, false);
        mVar.a("netOper", this.x, false);
        mVar.a("networkOperName", this.y, false);
        mVar.a("cid", this.z, false);
        mVar.a("lac", this.A, false);
        mVar.a("blat", this.B, false);
        mVar.a("blon", this.C, false);
        mVar.a("subPublisherId", (Object) null, false);
        mVar.a("subProductId", (Object) null, false);
        mVar.a("retryCount", this.f6305a, false);
        mVar.a("roaming", this.s, false);
        mVar.a("grid", this.t, false);
        mVar.a("silev", this.u, false);
        mVar.a("cellSignalLevel", this.D, false);
        mVar.a("cellTimingAdv", this.E, false);
        mVar.a("outsource", this.r, false);
        mVar.a("width", String.valueOf(this.H), false);
        mVar.a("height", String.valueOf(this.I), false);
        mVar.a("density", String.valueOf(this.J), false);
        mVar.a("fgApp", this.K, false);
        mVar.a("sdkId", String.valueOf(this.L), true);
        mVar.a("clientSessionId", this.M, false);
        mVar.a("appVersion", this.N, false);
        mVar.a("appCode", Integer.valueOf(this.O), false);
        mVar.a("timeSinceBoot", Long.valueOf(SystemClock.elapsedRealtime()), false);
        mVar.a("udbg", Boolean.valueOf(this.P), false);
        mVar.a("root", Boolean.valueOf(this.Q), false);
        mVar.a("smltr", Boolean.valueOf(this.R), false);
        mVar.a("isddbg", Boolean.valueOf(this.S), false);
        mVar.a("pas", this.T, false);
        b(mVar);
    }

    public final void b(Context context, AdPreferences adPreferences) {
        DisplayMetrics displayMetrics;
        this.m = Build.MANUFACTURER;
        this.l = Build.MODEL;
        this.n = Integer.toString(Build.VERSION.SDK_INT);
        if (adPreferences != null) {
            this.g = adPreferences.getAge(context);
        }
        this.i = context.getPackageName();
        this.j = ((com.startapp.sdk.d.c.a) com.startapp.sdk.c.c.a(context).m().c()).a();
        this.N = b.d(context);
        this.O = b.c(context);
        this.r = Boolean.valueOf(b.a(context));
        this.P = b.g(context);
        this.Q = b.h(context);
        this.R = b.i(context);
        this.s = com.startapp.common.b.e.b(context);
        this.S = u.l(context);
        Resources resources = context.getResources();
        if (!(resources == null || (displayMetrics = resources.getDisplayMetrics()) == null)) {
            this.H = displayMetrics.widthPixels;
            this.I = displayMetrics.heightPixels;
            this.J = displayMetrics.density;
        }
        com.startapp.sdk.c.c a2 = com.startapp.sdk.c.c.a(context);
        com.startapp.sdk.d.b.a aVar = (com.startapp.sdk.d.b.a) a2.a().c();
        this.o = aVar.a();
        this.p = aVar.b();
        this.q = ((com.startapp.sdk.d.a.a) a2.b().c()).a();
        this.T = j.a(context, "USER_CONSENT_PERSONALIZED_ADS_SERVING", (String) null);
        j.b(context, "sharedPrefsWrappers", this.f);
        a(context);
        this.M = p.d().a();
    }

    /* access modifiers changed from: protected */
    public final void b(m mVar) throws SDKException {
        mVar.a("rsc", this.F, false, true);
    }

    private void b(TelephonyManager telephonyManager) {
        int phoneType = telephonyManager.getPhoneType();
        if (phoneType != 0 && phoneType != 2) {
            String networkOperator = telephonyManager.getNetworkOperator();
            if (networkOperator != null) {
                this.x = com.startapp.common.b.a.c(networkOperator);
            }
            String networkOperatorName = telephonyManager.getNetworkOperatorName();
            if (networkOperatorName != null) {
                this.y = com.startapp.common.b.a.c(networkOperatorName);
            }
        }
    }

    public final void b(String str) {
        this.F = str;
    }

    private void a(TelephonyManager telephonyManager) {
        if (telephonyManager.getSimState() == 5) {
            this.v = telephonyManager.getSimOperator();
            this.w = telephonyManager.getSimOperatorName();
        }
    }
}
