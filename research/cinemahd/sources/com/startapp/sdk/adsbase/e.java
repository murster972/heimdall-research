package com.startapp.sdk.adsbase;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import com.startapp.sdk.adsbase.StartAppAd;
import com.startapp.sdk.adsbase.adlisteners.AdEventListener;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdPreferences;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    protected StartAppAd f6341a;
    private boolean b;
    private AutoInterstitialPreferences c;
    private long d;
    private int e;

    static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public static final e f6343a = new e((byte) 0);
    }

    /* synthetic */ e(byte b2) {
        this();
    }

    public final void a() {
        this.b = true;
    }

    public final void b() {
        this.b = false;
    }

    /* access modifiers changed from: protected */
    public final void c() {
        this.d = System.currentTimeMillis();
        this.e = 0;
    }

    private e() {
        this.b = false;
        this.c = null;
        this.d = -1;
        this.e = -1;
        this.f6341a = null;
    }

    public final void a(AutoInterstitialPreferences autoInterstitialPreferences) {
        this.c = autoInterstitialPreferences;
        this.d = -1;
        this.e = -1;
    }

    public final void a(Activity activity, Bundle bundle) {
        boolean z;
        boolean equals = activity.getClass().getName().equals(u.b((Context) activity));
        if (bundle == null) {
            String[] split = e.class.getName().split("\\.");
            boolean z2 = true;
            if (split.length < 3) {
                z = false;
            } else {
                z = activity.getClass().getName().startsWith(split[0] + "." + split[1] + "." + split[2]);
            }
            if (!z && !equals) {
                this.e++;
                if (this.b && AdsCommonMetaData.a().J()) {
                    if (this.c == null) {
                        this.c = new AutoInterstitialPreferences();
                    }
                    boolean z3 = this.d <= 0 || System.currentTimeMillis() >= this.d + ((long) (this.c.getSecondsBetweenAds() * 1000));
                    int i = this.e;
                    boolean z4 = i <= 0 || i >= this.c.getActivitiesBetweenAds();
                    if (!z3 || !z4) {
                        z2 = false;
                    }
                    if (z2) {
                        if (this.f6341a == null) {
                            this.f6341a = new StartAppAd(activity);
                        }
                        this.f6341a.loadAd(StartAppAd.AdMode.AUTOMATIC, new AdPreferences().setAi(Boolean.TRUE), new AdEventListener() {
                            public final void onFailedToReceiveAd(Ad ad) {
                            }

                            public final void onReceiveAd(Ad ad) {
                                if (e.this.f6341a.showAd()) {
                                    e.this.c();
                                }
                            }
                        });
                    }
                }
            }
        }
    }
}
