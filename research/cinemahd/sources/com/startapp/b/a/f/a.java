package com.startapp.b.a.f;

import com.startapp.b.a.a.f;
import com.startapp.b.a.e.b;
import com.startapp.b.a.h.c;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final b f5918a;
    private final c b;

    public a(b bVar, c cVar) {
        this.b = cVar;
        this.f5918a = bVar;
    }

    public final String a(com.startapp.b.a.h.a aVar, f fVar, long j) {
        try {
            String a2 = b.a(fVar);
            com.startapp.b.a.d.c b2 = this.b.b(aVar);
            return j + "-" + aVar.a() + "-" + b2.a(a2);
        } catch (Throwable unused) {
            return null;
        }
    }
}
