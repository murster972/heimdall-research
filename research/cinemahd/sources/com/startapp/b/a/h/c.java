package com.startapp.b.a.h;

import com.startapp.b.a.a.a;
import java.util.HashMap;
import java.util.Map;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private final Map<a, b> f5921a = new HashMap();

    public c() {
        this.f5921a.put(a.ZERO, new g());
        this.f5921a.put(a.THREE, new f());
        this.f5921a.put(a.FOUR, new e());
        this.f5921a.put(a.FIVE, new d());
    }

    public final a a(a aVar) {
        return this.f5921a.get(aVar).b();
    }

    public final com.startapp.b.a.d.c b(a aVar) {
        return this.f5921a.get(aVar).a();
    }
}
