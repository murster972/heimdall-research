package com.startapp.b.a.h;

import com.startapp.b.a.a.a;
import com.startapp.b.a.d.c;
import com.startapp.b.a.e.d;

abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private final a f5920a;
    private final c b;
    private final d c;
    private final a d;

    protected b(a aVar, c cVar, d dVar, a aVar2) {
        this.f5920a = aVar;
        this.b = cVar;
        this.c = dVar;
        this.d = aVar2;
    }

    /* access modifiers changed from: package-private */
    public final c a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public final a b() {
        return this.d;
    }
}
