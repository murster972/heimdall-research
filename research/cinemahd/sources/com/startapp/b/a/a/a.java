package com.startapp.b.a.a;

import com.startapp.b.a.c.c;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final int f5905a;
    private final int b;

    public a(int i, int i2) {
        this.f5905a = i;
        this.b = i2;
    }

    public final f a(List<String> list) {
        f fVar = new f((long) (this.f5905a * this.b));
        for (String bytes : list) {
            ByteBuffer wrap = ByteBuffer.wrap(bytes.getBytes());
            long a2 = fVar.a();
            int i = this.f5905a;
            long[] jArr = new long[i];
            long j = a2 / ((long) i);
            long a3 = a(wrap, wrap.position(), wrap.remaining(), 0);
            long a4 = a(wrap, wrap.position(), wrap.remaining(), a3);
            for (int i2 = 0; i2 < this.f5905a; i2++) {
                long j2 = (long) i2;
                jArr[i2] = (j2 * j) + Math.abs(((j2 * a4) + a3) % j);
            }
            for (long a5 : jArr) {
                fVar.a(a5);
            }
        }
        return fVar;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:10:0x00c7, code lost:
        r2 = r2 ^ (((long) r0.get(((r18 + r1) - r4) + 3)) << 24);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x00d3, code lost:
        r2 = r2 ^ (((long) r0.get(((r18 + r1) - r4) + 2)) << 16);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x00df, code lost:
        r2 = r2 ^ (((long) r0.get(((r18 + r1) - r4) + 1)) << 8);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x00eb, code lost:
        r4 = -4132994306676758123L;
        r2 = (((long) r0.get((r18 + r1) - r4)) ^ r2) * -4132994306676758123L;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x00fb, code lost:
        r1 = (r2 ^ (r2 >>> 47)) * r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0107, code lost:
        return r1 ^ (r1 >>> 47);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x00ab, code lost:
        r2 = r2 ^ (((long) r0.get(((r18 + r1) - r4) + 5)) << 40);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x00b9, code lost:
        r2 = r2 ^ (((long) r0.get(((r18 + r1) - r4) + 4)) << 32);
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static long a(java.nio.ByteBuffer r17, int r18, int r19, long r20) {
        /*
            r0 = r17
            r1 = r19
            r2 = 4294967295(0xffffffff, double:2.1219957905E-314)
            long r2 = r20 & r2
            long r4 = (long) r1
            r6 = -4132994306676758123(0xc6a4a7935bd1e995, double:-2.0946245025644615E32)
            long r4 = r4 * r6
            long r2 = r2 ^ r4
            int r4 = r1 >> 3
            r5 = 0
        L_0x0017:
            r10 = 24
            r11 = 16
            r12 = 8
            if (r5 >= r4) goto L_0x0092
            int r14 = r5 << 3
            int r14 = r18 + r14
            byte r15 = r0.get(r14)
            long r6 = (long) r15
            r15 = 255(0xff, double:1.26E-321)
            long r6 = r6 & r15
            int r13 = r14 + 1
            byte r13 = r0.get(r13)
            long r8 = (long) r13
            long r8 = r8 & r15
            long r8 = r8 << r12
            long r6 = r6 + r8
            int r8 = r14 + 2
            byte r8 = r0.get(r8)
            long r8 = (long) r8
            long r8 = r8 & r15
            long r8 = r8 << r11
            long r6 = r6 + r8
            int r8 = r14 + 3
            byte r8 = r0.get(r8)
            long r8 = (long) r8
            long r8 = r8 & r15
            long r8 = r8 << r10
            long r6 = r6 + r8
            int r8 = r14 + 4
            byte r8 = r0.get(r8)
            long r8 = (long) r8
            long r8 = r8 & r15
            r10 = 32
            long r8 = r8 << r10
            long r6 = r6 + r8
            int r8 = r14 + 5
            byte r8 = r0.get(r8)
            long r8 = (long) r8
            long r8 = r8 & r15
            r10 = 40
            long r8 = r8 << r10
            long r6 = r6 + r8
            int r8 = r14 + 6
            byte r8 = r0.get(r8)
            long r8 = (long) r8
            long r8 = r8 & r15
            r10 = 48
            long r8 = r8 << r10
            long r6 = r6 + r8
            int r14 = r14 + 7
            byte r8 = r0.get(r14)
            long r8 = (long) r8
            long r8 = r8 & r15
            r10 = 56
            long r8 = r8 << r10
            long r6 = r6 + r8
            r8 = -4132994306676758123(0xc6a4a7935bd1e995, double:-2.0946245025644615E32)
            long r6 = r6 * r8
            r10 = 47
            long r10 = r6 >>> r10
            long r6 = r6 ^ r10
            long r6 = r6 * r8
            long r2 = r2 ^ r6
            long r2 = r2 * r8
            int r5 = r5 + 1
            r6 = -4132994306676758123(0xc6a4a7935bd1e995, double:-2.0946245025644615E32)
            goto L_0x0017
        L_0x0092:
            r4 = r1 & 7
            switch(r4) {
                case 0: goto L_0x0097;
                case 1: goto L_0x00eb;
                case 2: goto L_0x00df;
                case 3: goto L_0x00d3;
                case 4: goto L_0x00c7;
                case 5: goto L_0x00b9;
                case 6: goto L_0x00ab;
                case 7: goto L_0x009d;
                default: goto L_0x0097;
            }
        L_0x0097:
            r4 = -4132994306676758123(0xc6a4a7935bd1e995, double:-2.0946245025644615E32)
            goto L_0x00fb
        L_0x009d:
            int r5 = r18 + r1
            int r5 = r5 - r4
            int r5 = r5 + 6
            byte r5 = r0.get(r5)
            long r5 = (long) r5
            r7 = 48
            long r5 = r5 << r7
            long r2 = r2 ^ r5
        L_0x00ab:
            int r5 = r18 + r1
            int r5 = r5 - r4
            int r5 = r5 + 5
            byte r5 = r0.get(r5)
            long r5 = (long) r5
            r7 = 40
            long r5 = r5 << r7
            long r2 = r2 ^ r5
        L_0x00b9:
            int r5 = r18 + r1
            int r5 = r5 - r4
            int r5 = r5 + 4
            byte r5 = r0.get(r5)
            long r5 = (long) r5
            r7 = 32
            long r5 = r5 << r7
            long r2 = r2 ^ r5
        L_0x00c7:
            int r5 = r18 + r1
            int r5 = r5 - r4
            int r5 = r5 + 3
            byte r5 = r0.get(r5)
            long r5 = (long) r5
            long r5 = r5 << r10
            long r2 = r2 ^ r5
        L_0x00d3:
            int r5 = r18 + r1
            int r5 = r5 - r4
            int r5 = r5 + 2
            byte r5 = r0.get(r5)
            long r5 = (long) r5
            long r5 = r5 << r11
            long r2 = r2 ^ r5
        L_0x00df:
            int r5 = r18 + r1
            int r5 = r5 - r4
            int r5 = r5 + 1
            byte r5 = r0.get(r5)
            long r5 = (long) r5
            long r5 = r5 << r12
            long r2 = r2 ^ r5
        L_0x00eb:
            int r1 = r18 + r1
            int r1 = r1 - r4
            byte r0 = r0.get(r1)
            long r0 = (long) r0
            long r0 = r0 ^ r2
            r4 = -4132994306676758123(0xc6a4a7935bd1e995, double:-2.0946245025644615E32)
            long r2 = r0 * r4
        L_0x00fb:
            r0 = 47
            long r6 = r2 >>> r0
            long r1 = r2 ^ r6
            long r1 = r1 * r4
            long r3 = r1 >>> r0
            long r0 = r1 ^ r3
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.b.a.a.a.a(java.nio.ByteBuffer, int, int, long):long");
    }

    public static String a(byte[] bArr) {
        Charset charset = c.f5911a;
        if (bArr == null) {
            return null;
        }
        return new String(bArr, charset);
    }
}
