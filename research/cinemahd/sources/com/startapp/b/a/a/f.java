package com.startapp.b.a.a;

import java.io.Serializable;

public class f implements Serializable {

    /* renamed from: a  reason: collision with root package name */
    private static /* synthetic */ boolean f5906a = (!f.class.desiredAssertionStatus());
    private final long[][] d;
    private int e;
    private final int f;

    public f(long j) {
        this.e = b(j);
        int i = this.e;
        int i2 = i % 4096;
        int i3 = i / 4096;
        this.f = (i2 == 0 ? 0 : 1) + i3;
        int i4 = this.f;
        if (i4 <= 100) {
            this.d = new long[i4][];
            for (int i5 = 0; i5 < i3; i5++) {
                this.d[i5] = new long[4096];
            }
            if (i2 != 0) {
                long[][] jArr = this.d;
                jArr[jArr.length - 1] = new long[i2];
                return;
            }
            return;
        }
        throw new RuntimeException("HighPageCountException pageCount = " + this.f);
    }

    private static int b(long j) {
        return (int) (((j - 1) >>> 6) + 1);
    }

    /* access modifiers changed from: package-private */
    public final long a() {
        return ((long) this.e) << 6;
    }

    public final int b() {
        return this.e;
    }

    public final int c() {
        return this.f;
    }

    public final long[] a(int i) {
        return this.d[i];
    }

    /* access modifiers changed from: package-private */
    public final void a(long j) {
        int i = (int) (j >> 6);
        if (i >= this.e) {
            int b = b(j + 1);
            if (f5906a || b <= this.e) {
                this.e = i + 1;
            } else {
                throw new AssertionError("Growing of paged bitset is not supported");
            }
        }
        long[] jArr = this.d[i / 4096];
        int i2 = i % 4096;
        jArr[i2] = (1 << (((int) j) & 63)) | jArr[i2];
    }
}
