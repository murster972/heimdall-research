package com.startapp.b.a.c;

import com.startapp.b.a.c.b;

public final class a extends b {

    /* renamed from: a  reason: collision with root package name */
    private static byte[] f5908a = {13, 10};
    private static final byte[] b = {65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 43, 47};
    private static final byte[] c = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 62, -1, 62, -1, 63, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, -1, -1, -1, -1, 63, -1, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51};
    private final byte[] d;
    private final byte[] e;
    private final byte[] f;
    private final int g;
    private final int h;

    public a() {
        this((byte) 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v3, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v16, resolved type: byte} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r9v17, resolved type: byte} */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(byte[] r8, int r9, int r10, com.startapp.b.a.c.b.a r11) {
        /*
            r7 = this;
            boolean r0 = r11.e
            if (r0 == 0) goto L_0x0005
            return
        L_0x0005:
            r0 = 1
            if (r10 >= 0) goto L_0x00aa
            r11.e = r0
            int r8 = r11.g
            if (r8 != 0) goto L_0x000f
            return
        L_0x000f:
            byte[] r8 = com.startapp.b.a.c.b.a((com.startapp.b.a.c.b.a) r11)
            int r9 = r11.c
            int r10 = r11.g
            if (r10 == 0) goto L_0x00a1
            r1 = 61
            if (r10 == r0) goto L_0x006f
            r0 = 2
            if (r10 != r0) goto L_0x0059
            int r10 = r9 + 1
            r11.c = r10
            byte[] r10 = r7.d
            int r2 = r11.f5910a
            int r3 = r2 >> 10
            r3 = r3 & 63
            byte r3 = r10[r3]
            r8[r9] = r3
            int r3 = r11.c
            int r4 = r3 + 1
            r11.c = r4
            int r4 = r2 >> 4
            r4 = r4 & 63
            byte r4 = r10[r4]
            r8[r3] = r4
            int r3 = r11.c
            int r4 = r3 + 1
            r11.c = r4
            int r0 = r2 << 2
            r0 = r0 & 63
            byte r0 = r10[r0]
            r8[r3] = r0
            byte[] r0 = b
            if (r10 != r0) goto L_0x00a1
            int r10 = r11.c
            int r0 = r10 + 1
            r11.c = r0
            r8[r10] = r1
            goto L_0x00a1
        L_0x0059:
            java.lang.IllegalStateException r8 = new java.lang.IllegalStateException
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = "Impossible modulus "
            r9.<init>(r10)
            int r10 = r11.g
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            r8.<init>(r9)
            throw r8
        L_0x006f:
            int r10 = r9 + 1
            r11.c = r10
            byte[] r10 = r7.d
            int r0 = r11.f5910a
            int r2 = r0 >> 2
            r2 = r2 & 63
            byte r2 = r10[r2]
            r8[r9] = r2
            int r2 = r11.c
            int r3 = r2 + 1
            r11.c = r3
            int r0 = r0 << 4
            r0 = r0 & 63
            byte r0 = r10[r0]
            r8[r2] = r0
            byte[] r0 = b
            if (r10 != r0) goto L_0x00a1
            int r10 = r11.c
            int r0 = r10 + 1
            r11.c = r0
            r8[r10] = r1
            int r10 = r11.c
            int r0 = r10 + 1
            r11.c = r0
            r8[r10] = r1
        L_0x00a1:
            int r8 = r11.f
            int r10 = r11.c
            int r10 = r10 - r9
            int r8 = r8 + r10
            r11.f = r8
            return
        L_0x00aa:
            r1 = 0
        L_0x00ab:
            if (r1 >= r10) goto L_0x010f
            byte[] r2 = com.startapp.b.a.c.b.a((com.startapp.b.a.c.b.a) r11)
            int r3 = r11.g
            int r3 = r3 + r0
            int r3 = r3 % 3
            r11.g = r3
            int r3 = r9 + 1
            byte r9 = r8[r9]
            if (r9 >= 0) goto L_0x00c0
            int r9 = r9 + 256
        L_0x00c0:
            int r4 = r11.f5910a
            int r4 = r4 << 8
            int r4 = r4 + r9
            r11.f5910a = r4
            int r9 = r11.g
            if (r9 != 0) goto L_0x010b
            int r9 = r11.c
            int r4 = r9 + 1
            r11.c = r4
            byte[] r4 = r7.d
            int r5 = r11.f5910a
            int r6 = r5 >> 18
            r6 = r6 & 63
            byte r6 = r4[r6]
            r2[r9] = r6
            int r9 = r11.c
            int r6 = r9 + 1
            r11.c = r6
            int r6 = r5 >> 12
            r6 = r6 & 63
            byte r6 = r4[r6]
            r2[r9] = r6
            int r9 = r11.c
            int r6 = r9 + 1
            r11.c = r6
            int r6 = r5 >> 6
            r6 = r6 & 63
            byte r6 = r4[r6]
            r2[r9] = r6
            int r9 = r11.c
            int r6 = r9 + 1
            r11.c = r6
            r5 = r5 & 63
            byte r4 = r4[r5]
            r2[r9] = r4
            int r9 = r11.f
            int r9 = r9 + 4
            r11.f = r9
        L_0x010b:
            int r1 = r1 + 1
            r9 = r3
            goto L_0x00ab
        L_0x010f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.b.a.c.a.a(byte[], int, int, com.startapp.b.a.c.b$a):void");
    }

    private a(byte b2) {
        this(f5908a);
    }

    private a(byte[] bArr) {
        this(bArr, (byte) 0);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    private a(byte[] bArr, byte b2) {
        super(bArr == null ? 0 : bArr.length);
        this.e = c;
        if (bArr == null) {
            this.h = 4;
            this.f = null;
        } else if (!b(bArr)) {
            this.h = 4;
            this.f = null;
        } else {
            String a2 = com.startapp.b.a.a.a.a(bArr);
            throw new IllegalArgumentException("lineSeparator must not contain base64 characters: [" + a2 + "]");
        }
        this.g = 3;
        this.d = b;
    }

    /* access modifiers changed from: protected */
    public final boolean a(byte b2) {
        if (b2 < 0) {
            return false;
        }
        byte[] bArr = this.e;
        return b2 < bArr.length && bArr[b2] != -1;
    }

    public static String a(byte[] bArr) {
        if (!(bArr == null || bArr.length == 0)) {
            a aVar = new a(f5908a, (byte) 0);
            long length = ((long) (((bArr.length + 3) - 1) / 3)) << 2;
            if (length > 2147483647L) {
                throw new IllegalArgumentException("Input array too big, the output array would be bigger (" + length + ") than the specified maximum size of 2147483647");
            } else if (!(bArr == null || bArr.length == 0)) {
                b.a aVar2 = new b.a();
                aVar.a(bArr, 0, bArr.length, aVar2);
                aVar.a(bArr, 0, -1, aVar2);
                int i = aVar2.c;
                int i2 = aVar2.d;
                byte[] bArr2 = new byte[(i - i2)];
                int length2 = bArr2.length;
                byte[] bArr3 = aVar2.b;
                if (bArr3 != null) {
                    int min = Math.min(bArr3 != null ? i - i2 : 0, length2);
                    System.arraycopy(aVar2.b, aVar2.d, bArr2, 0, min);
                    aVar2.d += min;
                    if (aVar2.d >= aVar2.c) {
                        aVar2.b = null;
                    }
                }
                bArr = bArr2;
            }
        }
        return com.startapp.b.a.a.a.a(bArr);
    }
}
