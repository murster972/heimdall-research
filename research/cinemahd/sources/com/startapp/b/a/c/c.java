package com.startapp.b.a.c;

import java.nio.charset.Charset;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    public static final Charset f5911a = Charset.forName("UTF-8");

    static {
        Charset.forName("ISO-8859-1");
        Charset.forName("US-ASCII");
        Charset.forName("UTF-16");
        Charset.forName("UTF-16BE");
        Charset.forName("UTF-16LE");
    }
}
