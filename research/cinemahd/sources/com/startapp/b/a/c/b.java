package com.startapp.b.a.c;

import java.util.Arrays;

public abstract class b {

    /* renamed from: a  reason: collision with root package name */
    private byte f5909a = 61;
    private final int b = 3;
    private final int c = 4;
    private int d = 0;
    private final int e;

    static class a {

        /* renamed from: a  reason: collision with root package name */
        int f5910a;
        byte[] b;
        int c;
        int d;
        boolean e;
        int f;
        int g;

        a() {
        }

        public final String toString() {
            return String.format("%s[buffer=%s, currentLinePos=%s, eof=%s, ibitWorkArea=%s, lbitWorkArea=%s, modulus=%s, pos=%s, readPos=%s]", new Object[]{a.class.getSimpleName(), Arrays.toString(this.b), Integer.valueOf(this.f), Boolean.valueOf(this.e), Integer.valueOf(this.f5910a), 0L, Integer.valueOf(this.g), Integer.valueOf(this.c), Integer.valueOf(this.d)});
        }
    }

    protected b(int i) {
        this.e = i;
    }

    protected static byte[] a(a aVar) {
        byte[] bArr = aVar.b;
        if (bArr != null && bArr.length >= aVar.c + 4) {
            return bArr;
        }
        byte[] bArr2 = aVar.b;
        if (bArr2 == null) {
            aVar.b = new byte[8192];
            aVar.c = 0;
            aVar.d = 0;
        } else {
            byte[] bArr3 = new byte[(bArr2.length << 1)];
            System.arraycopy(bArr2, 0, bArr3, 0, bArr2.length);
            aVar.b = bArr3;
        }
        return aVar.b;
    }

    /* access modifiers changed from: package-private */
    public abstract void a(byte[] bArr, int i, int i2, a aVar);

    /* access modifiers changed from: protected */
    public abstract boolean a(byte b2);

    /* access modifiers changed from: protected */
    public final boolean b(byte[] bArr) {
        if (bArr == null) {
            return false;
        }
        for (byte b2 : bArr) {
            if (61 == b2 || a(b2)) {
                return true;
            }
        }
        return false;
    }
}
