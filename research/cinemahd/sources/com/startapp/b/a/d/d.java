package com.startapp.b.a.d;

import java.util.regex.Pattern;

public final class d implements c {

    /* renamed from: a  reason: collision with root package name */
    private final Pattern f5913a = Pattern.compile("\\+");
    private final Pattern b = Pattern.compile("/");
    private final Pattern c = Pattern.compile("=");
    private final Pattern d = Pattern.compile("_");
    private final Pattern e = Pattern.compile("\\*");
    private final Pattern f = Pattern.compile("#");

    public final String a(String str) {
        return this.c.matcher(this.b.matcher(this.f5913a.matcher(str).replaceAll("_")).replaceAll("*")).replaceAll("#");
    }
}
