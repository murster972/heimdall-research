package com.startapp.b.a.e;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final c f5915a = new c();

    /* JADX WARNING: Removed duplicated region for block: B:31:0x0054 A[SYNTHETIC, Splitter:B:31:0x0054] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(com.startapp.b.a.a.f r11) {
        /*
            int r0 = r11.b()
            int r1 = r11.c()
            r2 = 0
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x0049 }
            r3.<init>()     // Catch:{ Exception -> 0x0049 }
            java.io.DataOutputStream r2 = new java.io.DataOutputStream     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.<init>(r3)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r4 = 0
            r5 = r0
            r0 = 0
        L_0x0016:
            if (r0 >= r1) goto L_0x0035
            long[] r6 = r11.a((int) r0)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r7 = r5
            r5 = 0
        L_0x001e:
            r8 = 4096(0x1000, float:5.74E-42)
            if (r5 >= r8) goto L_0x0031
            int r8 = r7 + -1
            if (r7 <= 0) goto L_0x002f
            r9 = r6[r5]     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            r2.writeLong(r9)     // Catch:{ Exception -> 0x0044, all -> 0x0041 }
            int r5 = r5 + 1
            r7 = r8
            goto L_0x001e
        L_0x002f:
            r5 = r8
            goto L_0x0032
        L_0x0031:
            r5 = r7
        L_0x0032:
            int r0 = r0 + 1
            goto L_0x0016
        L_0x0035:
            r3.close()     // Catch:{ IOException -> 0x0038 }
        L_0x0038:
            byte[] r11 = r3.toByteArray()
            java.lang.String r11 = com.startapp.b.a.e.c.a((byte[]) r11)
            return r11
        L_0x0041:
            r11 = move-exception
            r2 = r3
            goto L_0x0052
        L_0x0044:
            r11 = move-exception
            r2 = r3
            goto L_0x004a
        L_0x0047:
            r11 = move-exception
            goto L_0x0052
        L_0x0049:
            r11 = move-exception
        L_0x004a:
            java.lang.RuntimeException r0 = new java.lang.RuntimeException     // Catch:{ all -> 0x0047 }
            java.lang.String r1 = "problem serializing bitSet"
            r0.<init>(r1, r11)     // Catch:{ all -> 0x0047 }
            throw r0     // Catch:{ all -> 0x0047 }
        L_0x0052:
            if (r2 == 0) goto L_0x0057
            r2.close()     // Catch:{ IOException -> 0x0057 }
        L_0x0057:
            throw r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.b.a.e.b.a(com.startapp.b.a.a.f):java.lang.String");
    }
}
