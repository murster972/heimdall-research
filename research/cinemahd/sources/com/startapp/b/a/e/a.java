package com.startapp.b.a.e;

import com.startapp.b.a.a.f;
import java.io.DataInput;
import java.io.IOException;

public final class a extends d {

    /* renamed from: a  reason: collision with root package name */
    private final int f5914a;
    private final int b;

    public a(int i, int i2) {
        this.f5914a = i;
        this.b = i2;
    }

    /* access modifiers changed from: protected */
    public final f a(DataInput dataInput) throws IOException {
        f fVar = new f((long) (this.f5914a * this.b));
        d.a(dataInput, fVar, (long) fVar.b());
        return fVar;
    }
}
