package com.startapp.common;

import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Looper;
import com.startapp.common.ThreadManager;
import com.startapp.common.b.c;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    String f5927a;
    C0062a b;
    int c;

    /* renamed from: com.startapp.common.a$a  reason: collision with other inner class name */
    public interface C0062a {
        void a(Bitmap bitmap, int i);
    }

    public a(String str, C0062a aVar, int i) {
        this.f5927a = str;
        this.b = aVar;
        this.c = i;
    }

    public final void a() {
        ThreadManager.a(ThreadManager.Priority.HIGH, (Runnable) new Runnable() {
            public final void run() {
                final Bitmap b = c.b(a.this.f5927a);
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        a aVar = a.this;
                        C0062a aVar2 = aVar.b;
                        if (aVar2 != null) {
                            aVar2.a(b, aVar.c);
                        }
                    }
                });
            }
        });
    }
}
