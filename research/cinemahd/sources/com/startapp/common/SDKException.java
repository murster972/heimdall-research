package com.startapp.common;

import android.net.Uri;

public class SDKException extends Exception {
    private String b;
    private Uri c;
    private int d;
    private boolean e;

    public SDKException(String str, Uri uri, int i, boolean z) {
        this(str, uri, i, z, (Throwable) null);
    }

    public final Uri a() {
        return this.c;
    }

    public final int b() {
        return this.d;
    }

    public final boolean c() {
        return this.e;
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public SDKException(java.lang.String r5, android.net.Uri r6, int r7, boolean r8, java.lang.Throwable r9) {
        /*
            r4 = this;
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r5)
            r1 = 32
            r0.append(r1)
            r0.append(r6)
            java.lang.String r1 = ""
            if (r7 == 0) goto L_0x001f
            java.lang.String r2 = java.lang.String.valueOf(r7)
            java.lang.String r3 = ", status "
            java.lang.String r2 = r3.concat(r2)
            goto L_0x0020
        L_0x001f:
            r2 = r1
        L_0x0020:
            r0.append(r2)
            if (r8 == 0) goto L_0x0027
            java.lang.String r1 = ", retry"
        L_0x0027:
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r4.<init>(r0, r9)
            r4.b = r5
            r4.c = r6
            r4.d = r7
            r4.e = r8
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.SDKException.<init>(java.lang.String, android.net.Uri, int, boolean, java.lang.Throwable):void");
    }

    public SDKException() {
    }

    public SDKException(String str, Throwable th) {
        super(str, th);
    }
}
