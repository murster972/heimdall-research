package com.startapp.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Message;
import java.util.ArrayList;
import java.util.HashMap;

public class b {
    private static final Object f = new Object();
    private static b g;

    /* renamed from: a  reason: collision with root package name */
    private final Context f5935a;
    private final HashMap<BroadcastReceiver, ArrayList<IntentFilter>> b = new HashMap<>();
    private final HashMap<String, ArrayList<C0063b>> c = new HashMap<>();
    private final ArrayList<a> d = new ArrayList<>();
    private final Handler e;

    static class a {

        /* renamed from: a  reason: collision with root package name */
        final Intent f5937a;
        final ArrayList<C0063b> b;

        a(Intent intent, ArrayList<C0063b> arrayList) {
            this.f5937a = intent;
            this.b = arrayList;
        }
    }

    /* renamed from: com.startapp.common.b$b  reason: collision with other inner class name */
    static class C0063b {

        /* renamed from: a  reason: collision with root package name */
        final IntentFilter f5939a;
        final BroadcastReceiver b;
        boolean c;

        C0063b(IntentFilter intentFilter, BroadcastReceiver broadcastReceiver) {
            this.f5939a = intentFilter;
            this.b = broadcastReceiver;
        }

        public final String toString() {
            StringBuilder sb = new StringBuilder(128);
            sb.append("Receiver{");
            sb.append(this.b);
            sb.append(" filter=");
            sb.append(this.f5939a);
            sb.append("}");
            return sb.toString();
        }
    }

    static {
        com.startapp.common.b.b.a((Class<?>) b.class);
    }

    private b(Context context) {
        this.f5935a = context;
        this.e = new Handler(context.getMainLooper()) {
            public final void handleMessage(Message message) {
                if (message.what != 1) {
                    super.handleMessage(message);
                } else {
                    b.this.a();
                }
            }
        };
    }

    public static b a(Context context) {
        b bVar;
        synchronized (f) {
            if (g == null) {
                g = new b(context.getApplicationContext());
            }
            bVar = g;
        }
        return bVar;
    }

    public final void a(BroadcastReceiver broadcastReceiver, IntentFilter intentFilter) {
        synchronized (this.b) {
            C0063b bVar = new C0063b(intentFilter, broadcastReceiver);
            ArrayList arrayList = this.b.get(broadcastReceiver);
            if (arrayList == null) {
                arrayList = new ArrayList(1);
                this.b.put(broadcastReceiver, arrayList);
            }
            arrayList.add(intentFilter);
            for (int i = 0; i < intentFilter.countActions(); i++) {
                String action = intentFilter.getAction(i);
                ArrayList arrayList2 = this.c.get(action);
                if (arrayList2 == null) {
                    arrayList2 = new ArrayList(1);
                    this.c.put(action, arrayList2);
                }
                arrayList2.add(bVar);
            }
        }
    }

    public final void a(BroadcastReceiver broadcastReceiver) {
        synchronized (this.b) {
            ArrayList remove = this.b.remove(broadcastReceiver);
            if (remove != null) {
                for (int i = 0; i < remove.size(); i++) {
                    IntentFilter intentFilter = (IntentFilter) remove.get(i);
                    for (int i2 = 0; i2 < intentFilter.countActions(); i2++) {
                        String action = intentFilter.getAction(i2);
                        ArrayList arrayList = this.c.get(action);
                        if (arrayList != null) {
                            int i3 = 0;
                            while (i3 < arrayList.size()) {
                                if (((C0063b) arrayList.get(i3)).b == broadcastReceiver) {
                                    arrayList.remove(i3);
                                    i3--;
                                }
                                i3++;
                            }
                            if (arrayList.size() <= 0) {
                                this.c.remove(action);
                            }
                        }
                    }
                }
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0108, code lost:
        return true;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x010a, code lost:
        return false;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(android.content.Intent r23) {
        /*
            r22 = this;
            r1 = r22
            r0 = r23
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> r2 = r1.b
            monitor-enter(r2)
            java.lang.String r10 = r23.getAction()     // Catch:{ all -> 0x010c }
            android.content.Context r3 = r1.f5935a     // Catch:{ all -> 0x010c }
            android.content.ContentResolver r3 = r3.getContentResolver()     // Catch:{ all -> 0x010c }
            java.lang.String r11 = r0.resolveTypeIfNeeded(r3)     // Catch:{ all -> 0x010c }
            android.net.Uri r12 = r23.getData()     // Catch:{ all -> 0x010c }
            java.lang.String r13 = r23.getScheme()     // Catch:{ all -> 0x010c }
            java.util.Set r14 = r23.getCategories()     // Catch:{ all -> 0x010c }
            int r3 = r23.getFlags()     // Catch:{ all -> 0x010c }
            r3 = r3 & 8
            r9 = 1
            if (r3 == 0) goto L_0x002d
            r16 = 1
            goto L_0x002f
        L_0x002d:
            r16 = 0
        L_0x002f:
            if (r16 == 0) goto L_0x004b
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r4 = "Resolving type "
            r3.<init>(r4)     // Catch:{ all -> 0x010c }
            r3.append(r11)     // Catch:{ all -> 0x010c }
            java.lang.String r4 = " scheme "
            r3.append(r4)     // Catch:{ all -> 0x010c }
            r3.append(r13)     // Catch:{ all -> 0x010c }
            java.lang.String r4 = " of intent "
            r3.append(r4)     // Catch:{ all -> 0x010c }
            r3.append(r0)     // Catch:{ all -> 0x010c }
        L_0x004b:
            java.util.HashMap<java.lang.String, java.util.ArrayList<com.startapp.common.b$b>> r3 = r1.c     // Catch:{ all -> 0x010c }
            java.lang.String r4 = r23.getAction()     // Catch:{ all -> 0x010c }
            java.lang.Object r3 = r3.get(r4)     // Catch:{ all -> 0x010c }
            r8 = r3
            java.util.ArrayList r8 = (java.util.ArrayList) r8     // Catch:{ all -> 0x010c }
            if (r8 == 0) goto L_0x0109
            if (r16 == 0) goto L_0x0066
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r4 = "Action list: "
            r3.<init>(r4)     // Catch:{ all -> 0x010c }
            r3.append(r8)     // Catch:{ all -> 0x010c }
        L_0x0066:
            r3 = 0
            r6 = r3
            r7 = 0
        L_0x0069:
            int r3 = r8.size()     // Catch:{ all -> 0x010c }
            if (r7 >= r3) goto L_0x00d9
            java.lang.Object r3 = r8.get(r7)     // Catch:{ all -> 0x010c }
            r5 = r3
            com.startapp.common.b$b r5 = (com.startapp.common.b.C0063b) r5     // Catch:{ all -> 0x010c }
            if (r16 == 0) goto L_0x0084
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r4 = "Matching against filter "
            r3.<init>(r4)     // Catch:{ all -> 0x010c }
            android.content.IntentFilter r4 = r5.f5939a     // Catch:{ all -> 0x010c }
            r3.append(r4)     // Catch:{ all -> 0x010c }
        L_0x0084:
            boolean r3 = r5.c     // Catch:{ all -> 0x010c }
            if (r3 == 0) goto L_0x0093
            r19 = r7
            r20 = r8
            r18 = r10
            r21 = r11
            r11 = 1
            r10 = r6
            goto L_0x00ce
        L_0x0093:
            android.content.IntentFilter r3 = r5.f5939a     // Catch:{ all -> 0x010c }
            java.lang.String r17 = "LocalBroadcastManager"
            r4 = r10
            r15 = r5
            r5 = r11
            r18 = r10
            r10 = r6
            r6 = r13
            r19 = r7
            r7 = r12
            r20 = r8
            r8 = r14
            r21 = r11
            r11 = 1
            r9 = r17
            int r3 = r3.match(r4, r5, r6, r7, r8, r9)     // Catch:{ all -> 0x010c }
            if (r3 < 0) goto L_0x00ce
            if (r16 == 0) goto L_0x00bf
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x010c }
            java.lang.String r5 = "  Filter matched!  match=0x"
            r4.<init>(r5)     // Catch:{ all -> 0x010c }
            java.lang.String r3 = java.lang.Integer.toHexString(r3)     // Catch:{ all -> 0x010c }
            r4.append(r3)     // Catch:{ all -> 0x010c }
        L_0x00bf:
            if (r10 != 0) goto L_0x00c7
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ all -> 0x010c }
            r6.<init>()     // Catch:{ all -> 0x010c }
            goto L_0x00c8
        L_0x00c7:
            r6 = r10
        L_0x00c8:
            r6.add(r15)     // Catch:{ all -> 0x010c }
            r15.c = r11     // Catch:{ all -> 0x010c }
            goto L_0x00cf
        L_0x00ce:
            r6 = r10
        L_0x00cf:
            int r7 = r19 + 1
            r10 = r18
            r8 = r20
            r11 = r21
            r9 = 1
            goto L_0x0069
        L_0x00d9:
            r10 = r6
            r11 = 1
            if (r10 == 0) goto L_0x0109
            r3 = 0
        L_0x00de:
            int r4 = r10.size()     // Catch:{ all -> 0x010c }
            if (r3 >= r4) goto L_0x00f0
            java.lang.Object r4 = r10.get(r3)     // Catch:{ all -> 0x010c }
            com.startapp.common.b$b r4 = (com.startapp.common.b.C0063b) r4     // Catch:{ all -> 0x010c }
            r5 = 0
            r4.c = r5     // Catch:{ all -> 0x010c }
            int r3 = r3 + 1
            goto L_0x00de
        L_0x00f0:
            java.util.ArrayList<com.startapp.common.b$a> r3 = r1.d     // Catch:{ all -> 0x010c }
            com.startapp.common.b$a r4 = new com.startapp.common.b$a     // Catch:{ all -> 0x010c }
            r4.<init>(r0, r10)     // Catch:{ all -> 0x010c }
            r3.add(r4)     // Catch:{ all -> 0x010c }
            android.os.Handler r0 = r1.e     // Catch:{ all -> 0x010c }
            boolean r0 = r0.hasMessages(r11)     // Catch:{ all -> 0x010c }
            if (r0 != 0) goto L_0x0107
            android.os.Handler r0 = r1.e     // Catch:{ all -> 0x010c }
            r0.sendEmptyMessage(r11)     // Catch:{ all -> 0x010c }
        L_0x0107:
            monitor-exit(r2)     // Catch:{ all -> 0x010c }
            return r11
        L_0x0109:
            monitor-exit(r2)     // Catch:{ all -> 0x010c }
            r0 = 0
            return r0
        L_0x010c:
            r0 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x010c }
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.b.a(android.content.Intent):boolean");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001d, code lost:
        if (r2 >= r1.length) goto L_0x0000;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001f, code lost:
        r3 = r1[r2];
        r4 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0028, code lost:
        if (r4 >= r3.b.size()) goto L_0x003e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x002a, code lost:
        r3.b.get(r4).b.onReceive(r8.f5935a, r3.f5937a);
        r4 = r4 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003e, code lost:
        r2 = r2 + 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001a, code lost:
        r2 = 0;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a() {
        /*
            r8 = this;
        L_0x0000:
            java.util.HashMap<android.content.BroadcastReceiver, java.util.ArrayList<android.content.IntentFilter>> r0 = r8.b
            monitor-enter(r0)
            java.util.ArrayList<com.startapp.common.b$a> r1 = r8.d     // Catch:{ all -> 0x0041 }
            int r1 = r1.size()     // Catch:{ all -> 0x0041 }
            if (r1 > 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x0041 }
            return
        L_0x000d:
            com.startapp.common.b$a[] r1 = new com.startapp.common.b.a[r1]     // Catch:{ all -> 0x0041 }
            java.util.ArrayList<com.startapp.common.b$a> r2 = r8.d     // Catch:{ all -> 0x0041 }
            r2.toArray(r1)     // Catch:{ all -> 0x0041 }
            java.util.ArrayList<com.startapp.common.b$a> r2 = r8.d     // Catch:{ all -> 0x0041 }
            r2.clear()     // Catch:{ all -> 0x0041 }
            monitor-exit(r0)     // Catch:{ all -> 0x0041 }
            r0 = 0
            r2 = 0
        L_0x001c:
            int r3 = r1.length
            if (r2 >= r3) goto L_0x0000
            r3 = r1[r2]
            r4 = 0
        L_0x0022:
            java.util.ArrayList<com.startapp.common.b$b> r5 = r3.b
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x003e
            java.util.ArrayList<com.startapp.common.b$b> r5 = r3.b
            java.lang.Object r5 = r5.get(r4)
            com.startapp.common.b$b r5 = (com.startapp.common.b.C0063b) r5
            android.content.BroadcastReceiver r5 = r5.b
            android.content.Context r6 = r8.f5935a
            android.content.Intent r7 = r3.f5937a
            r5.onReceive(r6, r7)
            int r4 = r4 + 1
            goto L_0x0022
        L_0x003e:
            int r2 = r2 + 1
            goto L_0x001c
        L_0x0041:
            r1 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0041 }
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.b.a():void");
    }
}
