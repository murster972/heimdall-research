package com.startapp.common.b;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.telephony.TelephonyManager;
import com.iab.omid.library.startapp.adsession.Owner;
import com.iab.omid.library.startapp.d.b;
import com.startapp.common.SDKException;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import org.json.JSONObject;

public final class e {

    /* renamed from: a  reason: collision with root package name */
    private final Owner f5944a;
    private final Owner b;
    private final boolean c;

    public static class a {

        /* renamed from: a  reason: collision with root package name */
        private String f5945a;
        private String b;

        public final String a() {
            return this.f5945a;
        }

        public final String b() {
            return this.b;
        }

        public final String toString() {
            return "HttpResult: " + this.b + " " + this.f5945a;
        }

        public final void a(String str) {
            this.f5945a = str;
        }

        public final void b(String str) {
            this.b = str;
        }
    }

    private e(Owner owner, Owner owner2) {
        this.f5944a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = false;
    }

    public static String a(String str, byte[] bArr, Map<String, String> map, String str2, boolean z) throws SDKException {
        return a(str, bArr, map, str2, z, TraktV2.CONTENT_TYPE_JSON);
    }

    private static HttpURLConnection b(String str, byte[] bArr, Map<String, String> map, String str2, boolean z, String str3) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.addRequestProperty("Cache-Control", "no-cache");
        com.startapp.common.c.a.a(httpURLConnection, str);
        if (!(str2 == null || str2.compareTo("-1") == 0)) {
            httpURLConnection.addRequestProperty("User-Agent", str2);
        }
        httpURLConnection.setRequestProperty(TheTvdb.HEADER_ACCEPT, "application/json;text/html;text/plain");
        httpURLConnection.setReadTimeout(10000);
        httpURLConnection.setConnectTimeout(10000);
        if (bArr != null) {
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setFixedLengthStreamingMode(bArr.length);
            if (str3 != null) {
                httpURLConnection.setRequestProperty(TraktV2.HEADER_CONTENT_TYPE, str3);
            }
            if (z) {
                httpURLConnection.setRequestProperty("Content-Encoding", "gzip");
            }
        } else {
            httpURLConnection.setRequestMethod("GET");
        }
        if (map != null) {
            for (Map.Entry next : map.entrySet()) {
                String str4 = (String) next.getKey();
                String str5 = (String) next.getValue();
                if (!(str4 == null || str5 == null)) {
                    httpURLConnection.setRequestProperty(str4, str5);
                }
            }
        }
        return httpURLConnection;
    }

    private static String c(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService("phone");
        return telephonyManager != null ? Integer.toString(telephonyManager.getNetworkType()) : "e101";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:42:0x007e, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x007f, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0081, code lost:
        r9 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x0082, code lost:
        r7 = r9;
        r11 = null;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x007e A[ExcHandler: all (th java.lang.Throwable), Splitter:B:4:0x0008] */
    /* JADX WARNING: Removed duplicated region for block: B:57:0x00ae  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static java.lang.String a(java.lang.String r8, byte[] r9, java.util.Map<java.lang.String, java.lang.String> r10, java.lang.String r11, boolean r12, java.lang.String r13) throws com.startapp.common.SDKException {
        /*
            r0 = 0
            r1 = 0
            java.net.HttpURLConnection r10 = b(r8, r9, r10, r11, r12, r13)     // Catch:{ IOException -> 0x0089, all -> 0x0085 }
            if (r9 == 0) goto L_0x0021
            int r11 = r9.length     // Catch:{ IOException -> 0x0081, all -> 0x007e }
            if (r11 <= 0) goto L_0x0021
            java.io.OutputStream r11 = r10.getOutputStream()     // Catch:{ all -> 0x001b }
            r11.write(r9)     // Catch:{ all -> 0x0019 }
            r11.flush()     // Catch:{ all -> 0x0019 }
            com.startapp.common.b.b.a((java.io.Closeable) r11)     // Catch:{ IOException -> 0x0081, all -> 0x007e }
            goto L_0x0021
        L_0x0019:
            r9 = move-exception
            goto L_0x001d
        L_0x001b:
            r9 = move-exception
            r11 = r1
        L_0x001d:
            com.startapp.common.b.b.a((java.io.Closeable) r11)     // Catch:{ IOException -> 0x0081, all -> 0x007e }
            throw r9     // Catch:{ IOException -> 0x0081, all -> 0x007e }
        L_0x0021:
            int r9 = r10.getResponseCode()     // Catch:{ IOException -> 0x0081, all -> 0x007e }
            r11 = 200(0xc8, float:2.8E-43)
            if (r9 != r11) goto L_0x0066
            java.io.InputStream r11 = r10.getInputStream()     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            if (r11 == 0) goto L_0x0058
            java.io.StringWriter r12 = new java.io.StringWriter     // Catch:{ IOException -> 0x0054 }
            r12.<init>()     // Catch:{ IOException -> 0x0054 }
            r13 = 1024(0x400, float:1.435E-42)
            char[] r13 = new char[r13]     // Catch:{ IOException -> 0x0054 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ IOException -> 0x0054 }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x0054 }
            java.lang.String r4 = "UTF-8"
            r3.<init>(r11, r4)     // Catch:{ IOException -> 0x0054 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0054 }
        L_0x0044:
            int r3 = r2.read(r13)     // Catch:{ IOException -> 0x0054 }
            r4 = -1
            if (r3 == r4) goto L_0x004f
            r12.write(r13, r0, r3)     // Catch:{ IOException -> 0x0054 }
            goto L_0x0044
        L_0x004f:
            java.lang.String r1 = r12.toString()     // Catch:{ IOException -> 0x0054 }
            goto L_0x0058
        L_0x0054:
            r12 = move-exception
            r5 = r9
            r7 = r12
            goto L_0x008e
        L_0x0058:
            com.startapp.common.b.b.a((java.io.Closeable) r11)
            if (r10 == 0) goto L_0x0060
            r10.disconnect()
        L_0x0060:
            return r1
        L_0x0061:
            r11 = move-exception
            r5 = r9
            r7 = r11
            r11 = r1
            goto L_0x008e
        L_0x0066:
            android.net.Uri r11 = android.net.Uri.parse(r8)     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            android.net.Uri$Builder r11 = r11.buildUpon()     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            android.net.Uri$Builder r11 = r11.query(r1)     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            android.net.Uri r11 = r11.build()     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            com.startapp.common.SDKException r12 = new com.startapp.common.SDKException     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            java.lang.String r13 = "POST"
            r12.<init>(r13, r11, r9, r0)     // Catch:{ IOException -> 0x0061, all -> 0x007e }
            throw r12     // Catch:{ IOException -> 0x0061, all -> 0x007e }
        L_0x007e:
            r8 = move-exception
            r11 = r1
            goto L_0x00a9
        L_0x0081:
            r9 = move-exception
            r7 = r9
            r11 = r1
            goto L_0x008d
        L_0x0085:
            r8 = move-exception
            r10 = r1
            r11 = r10
            goto L_0x00a9
        L_0x0089:
            r9 = move-exception
            r7 = r9
            r10 = r1
            r11 = r10
        L_0x008d:
            r5 = 0
        L_0x008e:
            android.net.Uri r8 = android.net.Uri.parse(r8)     // Catch:{ all -> 0x00a8 }
            android.net.Uri$Builder r8 = r8.buildUpon()     // Catch:{ all -> 0x00a8 }
            android.net.Uri$Builder r8 = r8.query(r1)     // Catch:{ all -> 0x00a8 }
            android.net.Uri r4 = r8.build()     // Catch:{ all -> 0x00a8 }
            com.startapp.common.SDKException r8 = new com.startapp.common.SDKException     // Catch:{ all -> 0x00a8 }
            java.lang.String r3 = "POST"
            r6 = 0
            r2 = r8
            r2.<init>(r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00a8 }
            throw r8     // Catch:{ all -> 0x00a8 }
        L_0x00a8:
            r8 = move-exception
        L_0x00a9:
            com.startapp.common.b.b.a((java.io.Closeable) r11)
            if (r10 == 0) goto L_0x00b1
            r10.disconnect()
        L_0x00b1:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.b.e.a(java.lang.String, byte[], java.util.Map, java.lang.String, boolean, java.lang.String):java.lang.String");
    }

    public final JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.f5944a);
        b.a(jSONObject, "videoEventsOwner", this.b);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.FALSE);
        return jSONObject;
    }

    public static Boolean b(Context context) {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager == null || !b.a(context, "android.permission.ACCESS_NETWORK_STATE") || (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) == null || !activeNetworkInfo.isConnected()) {
            return null;
        }
        return Boolean.valueOf(activeNetworkInfo.isRoaming());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:28:0x0071, code lost:
        r11 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:29:0x0072, code lost:
        r3 = r10;
        r5 = r11;
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x008f, code lost:
        r8 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0090, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00bf, code lost:
        r9.disconnect();
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x008f A[ExcHandler: all (th java.lang.Throwable), Splitter:B:3:0x000c] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00bf  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.startapp.common.b.e.a a(java.lang.String r8, java.util.Map<java.lang.String, java.lang.String> r9, java.lang.String r10, boolean r11) throws com.startapp.common.SDKException {
        /*
            r1 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r0 = r8
            r2 = r9
            r3 = r10
            r4 = r11
            java.net.HttpURLConnection r9 = b(r0, r1, r2, r3, r4, r5)     // Catch:{ IOException -> 0x009a, all -> 0x0096 }
            int r10 = r9.getResponseCode()     // Catch:{ IOException -> 0x0092, all -> 0x008f }
            r11 = 200(0xc8, float:2.8E-43)
            if (r10 != r11) goto L_0x0076
            int r11 = android.os.Build.VERSION.SDK_INT     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            r0 = 9
            if (r11 < r0) goto L_0x002b
            java.net.CookieManager r11 = com.startapp.common.c.a.a()     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            if (r11 == 0) goto L_0x002b
            java.net.URI r0 = java.net.URI.create(r8)     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            java.util.Map r1 = r9.getHeaderFields()     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            r11.put(r0, r1)     // Catch:{ IOException -> 0x0071, all -> 0x008f }
        L_0x002b:
            java.io.InputStream r11 = r9.getInputStream()     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            com.startapp.common.b.e$a r0 = new com.startapp.common.b.e$a     // Catch:{ IOException -> 0x006d }
            r0.<init>()     // Catch:{ IOException -> 0x006d }
            java.lang.String r1 = r9.getContentType()     // Catch:{ IOException -> 0x006d }
            r0.b(r1)     // Catch:{ IOException -> 0x006d }
            if (r11 == 0) goto L_0x0064
            java.io.StringWriter r1 = new java.io.StringWriter     // Catch:{ IOException -> 0x006d }
            r1.<init>()     // Catch:{ IOException -> 0x006d }
            r2 = 1024(0x400, float:1.435E-42)
            char[] r2 = new char[r2]     // Catch:{ IOException -> 0x006d }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ IOException -> 0x006d }
            java.io.InputStreamReader r4 = new java.io.InputStreamReader     // Catch:{ IOException -> 0x006d }
            java.lang.String r5 = "UTF-8"
            r4.<init>(r11, r5)     // Catch:{ IOException -> 0x006d }
            r3.<init>(r4)     // Catch:{ IOException -> 0x006d }
        L_0x0052:
            int r4 = r3.read(r2)     // Catch:{ IOException -> 0x006d }
            r5 = -1
            if (r4 == r5) goto L_0x005d
            r1.write(r2, r6, r4)     // Catch:{ IOException -> 0x006d }
            goto L_0x0052
        L_0x005d:
            java.lang.String r1 = r1.toString()     // Catch:{ IOException -> 0x006d }
            r0.a(r1)     // Catch:{ IOException -> 0x006d }
        L_0x0064:
            com.startapp.common.b.b.a((java.io.Closeable) r11)
            if (r9 == 0) goto L_0x006c
            r9.disconnect()
        L_0x006c:
            return r0
        L_0x006d:
            r0 = move-exception
            r3 = r10
            r5 = r0
            goto L_0x009f
        L_0x0071:
            r11 = move-exception
            r3 = r10
            r5 = r11
            r11 = r7
            goto L_0x009f
        L_0x0076:
            android.net.Uri r11 = android.net.Uri.parse(r8)     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            android.net.Uri$Builder r11 = r11.buildUpon()     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            android.net.Uri$Builder r11 = r11.query(r7)     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            android.net.Uri r11 = r11.build()     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            com.startapp.common.SDKException r0 = new com.startapp.common.SDKException     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            java.lang.String r1 = "GET"
            r2 = 1
            r0.<init>(r1, r11, r10, r2)     // Catch:{ IOException -> 0x0071, all -> 0x008f }
            throw r0     // Catch:{ IOException -> 0x0071, all -> 0x008f }
        L_0x008f:
            r8 = move-exception
            r11 = r7
            goto L_0x00ba
        L_0x0092:
            r11 = move-exception
            r5 = r11
            r11 = r7
            goto L_0x009e
        L_0x0096:
            r8 = move-exception
            r9 = r7
            r11 = r9
            goto L_0x00ba
        L_0x009a:
            r11 = move-exception
            r5 = r11
            r9 = r7
            r11 = r9
        L_0x009e:
            r3 = 0
        L_0x009f:
            android.net.Uri r8 = android.net.Uri.parse(r8)     // Catch:{ all -> 0x00b9 }
            android.net.Uri$Builder r8 = r8.buildUpon()     // Catch:{ all -> 0x00b9 }
            android.net.Uri$Builder r8 = r8.query(r7)     // Catch:{ all -> 0x00b9 }
            android.net.Uri r2 = r8.build()     // Catch:{ all -> 0x00b9 }
            com.startapp.common.SDKException r8 = new com.startapp.common.SDKException     // Catch:{ all -> 0x00b9 }
            java.lang.String r1 = "GET"
            r4 = 0
            r0 = r8
            r0.<init>(r1, r2, r3, r4, r5)     // Catch:{ all -> 0x00b9 }
            throw r8     // Catch:{ all -> 0x00b9 }
        L_0x00b9:
            r8 = move-exception
        L_0x00ba:
            com.startapp.common.b.b.a((java.io.Closeable) r11)
            if (r9 == 0) goto L_0x00c2
            r9.disconnect()
        L_0x00c2:
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.b.e.a(java.lang.String, java.util.Map, java.lang.String, boolean):com.startapp.common.b.e$a");
    }

    public final boolean b() {
        return Owner.NATIVE == this.b;
    }

    @SuppressLint({"MissingPermission"})
    public static String a(Context context) {
        String str;
        NetworkCapabilities networkCapabilities;
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
            if (connectivityManager == null) {
                str = "e100";
            } else if (!b.a(context, "android.permission.ACCESS_NETWORK_STATE")) {
                return "e105";
            } else {
                if (Build.VERSION.SDK_INT >= 23) {
                    Network activeNetwork = connectivityManager.getActiveNetwork();
                    if (!(activeNetwork == null || (networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork)) == null)) {
                        if (!networkCapabilities.hasTransport(1)) {
                            if (networkCapabilities.hasTransport(0)) {
                                str = c(context);
                            }
                        }
                    }
                    return "e102";
                }
                NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
                if (activeNetworkInfo != null && activeNetworkInfo.isConnected()) {
                    String typeName = activeNetworkInfo.getTypeName();
                    if (typeName.toLowerCase().compareTo("WIFI".toLowerCase()) != 0) {
                        if (typeName.toLowerCase().compareTo("MOBILE".toLowerCase()) == 0) {
                            str = c(context);
                        }
                    }
                }
                return "e102";
                return "WIFI";
            }
            return str;
        } catch (Exception unused) {
            return "e105";
        }
    }

    public static e a(Owner owner, Owner owner2) {
        com.iab.omid.library.startapp.b.a((Object) owner, "Impression owner is null");
        if (!owner.equals(Owner.NONE)) {
            return new e(owner, owner2);
        }
        throw new IllegalArgumentException("Impression owner is none");
    }

    public final boolean a() {
        return Owner.NATIVE == this.f5944a;
    }
}
