package com.startapp.common.b;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

public final class c {

    static class a extends FilterInputStream {
        public a(InputStream inputStream) {
            super(inputStream);
        }

        public final long skip(long j) throws IOException {
            long j2 = 0;
            while (j2 < j) {
                long skip = this.in.skip(j - j2);
                if (skip == 0) {
                    if (read() < 0) {
                        break;
                    }
                    skip = 1;
                }
                j2 += skip;
            }
            return j2;
        }
    }

    public static Bitmap a(String str) {
        byte[] decode = Base64.decode(str, 0);
        return BitmapFactory.decodeByteArray(decode, 0, decode.length);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:16:0x003c, code lost:
        if (r5 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x003f, code lost:
        return r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0027, code lost:
        if (r5 != null) goto L_0x0029;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0029, code lost:
        r5.disconnect();
     */
    /* JADX WARNING: Removed duplicated region for block: B:12:0x0037  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Bitmap b(java.lang.String r5) {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x003b, all -> 0x0031 }
            r1.<init>(r5)     // Catch:{ Exception -> 0x003b, all -> 0x0031 }
            java.net.URLConnection r5 = r1.openConnection()     // Catch:{ Exception -> 0x003b, all -> 0x0031 }
            java.net.HttpURLConnection r5 = (java.net.HttpURLConnection) r5     // Catch:{ Exception -> 0x003b, all -> 0x0031 }
            r5.connect()     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            java.io.InputStream r1 = r5.getInputStream()     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            java.io.BufferedInputStream r2 = new java.io.BufferedInputStream     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r2.<init>(r1)     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            com.startapp.common.b.c$a r3 = new com.startapp.common.b.c$a     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r3.<init>(r1)     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            android.graphics.Bitmap r0 = android.graphics.BitmapFactory.decodeStream(r3)     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r2.close()     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            r1.close()     // Catch:{ Exception -> 0x002d, all -> 0x002f }
            if (r5 == 0) goto L_0x003f
        L_0x0029:
            r5.disconnect()
            goto L_0x003f
        L_0x002d:
            goto L_0x003c
        L_0x002f:
            r0 = move-exception
            goto L_0x0035
        L_0x0031:
            r5 = move-exception
            r4 = r0
            r0 = r5
            r5 = r4
        L_0x0035:
            if (r5 == 0) goto L_0x003a
            r5.disconnect()
        L_0x003a:
            throw r0
        L_0x003b:
            r5 = r0
        L_0x003c:
            if (r5 == 0) goto L_0x003f
            goto L_0x0029
        L_0x003f:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.b.c.b(java.lang.String):android.graphics.Bitmap");
    }

    public static Drawable a(Resources resources, String str) {
        return new BitmapDrawable(resources, a(str));
    }
}
