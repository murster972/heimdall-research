package com.startapp.common.b;

import android.content.Context;
import android.util.Log;
import com.startapp.common.ThreadManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.util.ArrayList;
import java.util.List;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5942a = b.a((Class<?>) d.class);

    public static <T> T a(Context context, String str) {
        return d(context, str);
    }

    public static void b(final Context context, final String str, final Serializable serializable) {
        ThreadManager.a(ThreadManager.Priority.DEFAULT, (Runnable) new Runnable() {
            public final void run() {
                d.c(context, str, serializable);
            }
        });
    }

    public static void c(Context context, String str, Serializable serializable) {
        if (str != null) {
            try {
                a(e(context, (String) null), str, serializable);
            } catch (Exception e) {
                new StringBuilder("Failed writing to disk: ").append(e.getLocalizedMessage());
            }
        }
    }

    private static <T> T d(Context context, String str) {
        if (str == null) {
            return null;
        }
        try {
            return a(e(context, (String) null), str);
        } catch (Exception e) {
            String str2 = f5942a;
            Log.e(str2, "Failed to read from disk: " + e.getLocalizedMessage());
            return null;
        } catch (Error e2) {
            String str3 = f5942a;
            Log.e(str3, "Failed to read from disk: " + e2.getLocalizedMessage());
            return null;
        }
    }

    private static String e(Context context, String str) {
        String str2;
        StringBuilder sb = new StringBuilder();
        sb.append(context.getFilesDir().toString());
        if (str != null) {
            str2 = File.separator + str;
        } else {
            str2 = "";
        }
        sb.append(str2);
        return sb.toString();
    }

    private static String f(Context context, String str) {
        String str2;
        StringBuilder sb = new StringBuilder();
        sb.append(context.getCacheDir().toString());
        if (str != null) {
            str2 = File.separator + str;
        } else {
            str2 = "";
        }
        sb.append(str2);
        return sb.toString();
    }

    public static void a(Context context, String str, Serializable serializable) {
        c(context, str, serializable);
    }

    public static <T> List<T> b(Context context, String str) {
        ArrayList arrayList = new ArrayList();
        try {
            File file = new File(f(context, str));
            if (file.exists()) {
                if (file.isDirectory()) {
                    String[] list = file.list();
                    if (list == null) {
                        return null;
                    }
                    for (String file2 : list) {
                        File file3 = new File(file, file2);
                        Log.i(f5942a, "Reading file: " + file3.getPath());
                        arrayList.add(b(file3));
                    }
                    return arrayList;
                }
            }
            return null;
        } catch (Exception e) {
            Log.e(f5942a, "Failed to read from disk: " + e.getLocalizedMessage());
        }
    }

    public static void a(Context context, String str, String str2, Serializable serializable) {
        if (str2 != null) {
            try {
                a(f(context, str), str2, serializable);
            } catch (Exception e) {
                new StringBuilder("Failed writing to disk: ").append(e.getLocalizedMessage());
            }
        }
    }

    public static void c(Context context, String str) {
        if (str != null) {
            a(new File(e(context, str)));
            a(new File(f(context, str)));
        }
    }

    public static <T> T a(Context context, String str, String str2) {
        if (str2 == null) {
            return null;
        }
        try {
            return a(f(context, str), str2);
        } catch (Exception e) {
            String str3 = f5942a;
            Log.e(str3, "Failed to read from disk: " + e.getLocalizedMessage());
            return null;
        } catch (Error e2) {
            String str4 = f5942a;
            Log.e(str4, "Failed to read from disk: " + e2.getLocalizedMessage());
            return null;
        }
    }

    private static void a(String str, String str2, Serializable serializable) throws IOException {
        File file = new File(str);
        if (file.exists() || file.mkdirs()) {
            File file2 = new File(file, str2);
            String str3 = f5942a;
            Log.i(str3, "Writing file: " + file2.getPath());
            a(serializable, file2);
        }
    }

    private static <T> T b(File file) throws FileNotFoundException, StreamCorruptedException, IOException, OptionalDataException, ClassNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(file);
        ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        T readObject = objectInputStream.readObject();
        objectInputStream.close();
        fileInputStream.close();
        return readObject;
    }

    private static <T> T a(String str, String str2) throws IOException, ClassNotFoundException {
        File file = new File(str);
        if (!file.exists() || !file.isDirectory()) {
            return null;
        }
        File file2 = new File(file, str2);
        if (!file2.exists()) {
            return null;
        }
        String str3 = f5942a;
        Log.i(str3, "Reading file: " + file2.getPath());
        return b(file2);
    }

    private static void a(File file) {
        if (file.isDirectory()) {
            for (File a2 : file.listFiles()) {
                a(a2);
            }
        }
        file.delete();
    }

    private static void a(Serializable serializable, File file) throws FileNotFoundException, IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(serializable);
        objectOutputStream.close();
        fileOutputStream.close();
    }
}
