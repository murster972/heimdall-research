package com.startapp.common;

import android.os.Build;
import android.util.Log;
import com.startapp.common.b.b;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public final class ThreadManager {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5923a = b.a((Class<?>) ThreadManager.class);
    private static final int b = (Build.VERSION.SDK_INT < 22 ? 10 : 20);
    private static final int c = (Build.VERSION.SDK_INT < 22 ? 4 : 8);
    private static final ThreadFactory d = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f5924a = new AtomicInteger(1);

        public final Thread newThread(Runnable runnable) {
            return new Thread(runnable, "highPriorityThreadFactory #" + this.f5924a.getAndIncrement());
        }
    };
    private static final ThreadFactory e = new ThreadFactory() {

        /* renamed from: a  reason: collision with root package name */
        private final AtomicInteger f5925a = new AtomicInteger(1);

        public final Thread newThread(Runnable runnable) {
            return new Thread(runnable, "defaultPriorityThreadFactory #" + this.f5925a.getAndIncrement());
        }
    };
    private static final RejectedExecutionHandler f = new RejectedExecutionHandler() {
        public final void rejectedExecution(Runnable runnable, ThreadPoolExecutor threadPoolExecutor) {
            Log.e(ThreadManager.f5923a, "ThreadPoolExecutor rejected execution! ".concat(String.valueOf(threadPoolExecutor)));
        }
    };
    private static final Executor g;
    private static final Executor h;
    private static final ScheduledExecutorService i = new ScheduledThreadPoolExecutor(1);

    public enum Priority {
        DEFAULT,
        HIGH
    }

    static {
        int i2 = b;
        g = new ThreadPoolExecutor(i2, i2, 20, TimeUnit.SECONDS, new LinkedBlockingQueue(), d, f);
        int i3 = c;
        h = new ThreadPoolExecutor(i3, i3, 20, TimeUnit.SECONDS, new LinkedBlockingQueue(), e, f);
    }

    public static ScheduledFuture<?> a(Runnable runnable, long j) {
        return i.schedule(runnable, j, TimeUnit.MILLISECONDS);
    }

    public static void a(Priority priority, Runnable runnable) {
        Executor executor;
        try {
            if (priority.equals(Priority.HIGH)) {
                executor = g;
            } else {
                executor = h;
            }
            executor.execute(runnable);
        } catch (Exception e2) {
            Log.e(f5923a, "executeWithPriority failed to execute! ".concat(String.valueOf((Object) null)), e2);
        }
    }
}
