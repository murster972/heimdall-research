package com.startapp.common.c;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import java.net.CookieManager;
import java.net.CookieStore;
import java.net.HttpCookie;
import java.net.URI;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public final class b implements CookieStore {

    /* renamed from: a  reason: collision with root package name */
    private final CookieStore f5950a = new CookieManager().getCookieStore();
    private final SharedPreferences b;

    public b(Context context) {
        HttpCookie httpCookie;
        this.b = context.getApplicationContext().getSharedPreferences("com.startapp.android.publish.CookiePrefsFile", 0);
        String string = this.b.getString("names", (String) null);
        if (string != null) {
            for (String valueOf : TextUtils.split(string, ";")) {
                String string2 = this.b.getString("cookie_".concat(String.valueOf(valueOf)), (String) null);
                if (!(string2 == null || (httpCookie = (HttpCookie) com.startapp.common.parser.b.a(string2, HttpCookie.class)) == null)) {
                    if (httpCookie.hasExpired()) {
                        a(httpCookie);
                        a();
                    } else {
                        this.f5950a.add(URI.create(httpCookie.getDomain()), httpCookie);
                    }
                }
            }
        }
    }

    private void a(HttpCookie httpCookie) {
        SharedPreferences.Editor edit = this.b.edit();
        edit.remove("cookie_" + b(httpCookie));
        edit.commit();
    }

    private static String b(HttpCookie httpCookie) {
        return httpCookie.getDomain() + "_" + httpCookie.getName();
    }

    public final void add(URI uri, HttpCookie httpCookie) {
        String b2 = b(httpCookie);
        this.f5950a.add(uri, httpCookie);
        SharedPreferences.Editor edit = this.b.edit();
        edit.putString("cookie_".concat(String.valueOf(b2)), com.startapp.common.parser.b.a(httpCookie));
        edit.commit();
        a();
    }

    public final List<HttpCookie> get(URI uri) {
        return this.f5950a.get(uri);
    }

    public final List<HttpCookie> getCookies() {
        return this.f5950a.getCookies();
    }

    public final List<URI> getURIs() {
        return this.f5950a.getURIs();
    }

    public final boolean remove(URI uri, HttpCookie httpCookie) {
        if (!this.f5950a.remove(uri, httpCookie)) {
            return false;
        }
        a(httpCookie);
        a();
        return true;
    }

    public final boolean removeAll() {
        if (!this.f5950a.removeAll()) {
            return false;
        }
        SharedPreferences.Editor edit = this.b.edit();
        edit.clear();
        edit.commit();
        a();
        return true;
    }

    private Set<String> b() {
        HashSet hashSet = new HashSet();
        for (HttpCookie b2 : this.f5950a.getCookies()) {
            hashSet.add(b(b2));
        }
        return hashSet;
    }

    private void a() {
        SharedPreferences.Editor edit = this.b.edit();
        edit.putString("names", TextUtils.join(";", b()));
        edit.commit();
    }
}
