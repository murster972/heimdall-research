package com.startapp.common.c;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebView;
import com.iab.omid.library.startapp.adsession.AdSessionContextType;
import com.iab.omid.library.startapp.b;
import com.startapp.networkTest.utils.e;
import java.io.IOException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpURLConnection;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static CookieManager f5949a;
    private final com.startapp.networkTest.utils.a b;
    private final WebView c;
    private final List<e> d = new ArrayList();
    private final String e;
    private final String f;
    private final AdSessionContextType g;

    private a(com.startapp.networkTest.utils.a aVar, WebView webView, String str, List<e> list, String str2) {
        AdSessionContextType adSessionContextType;
        this.b = aVar;
        this.c = webView;
        this.e = str;
        if (list != null) {
            this.d.addAll(list);
            adSessionContextType = AdSessionContextType.NATIVE;
        } else {
            adSessionContextType = AdSessionContextType.HTML;
        }
        this.g = adSessionContextType;
        this.f = str2;
    }

    public static void a(Context context) {
        if (Build.VERSION.SDK_INT >= 9) {
            f5949a = new CookieManager(new b(context), CookiePolicy.ACCEPT_ALL);
        }
    }

    public final com.startapp.networkTest.utils.a b() {
        return this.b;
    }

    public final List<e> c() {
        return Collections.unmodifiableList(this.d);
    }

    public final WebView d() {
        return this.c;
    }

    public final String e() {
        return this.f;
    }

    public final String f() {
        return this.e;
    }

    public final AdSessionContextType g() {
        return this.g;
    }

    public static void a(HttpURLConnection httpURLConnection, String str) throws IOException {
        CookieManager cookieManager;
        Map<String, List<String>> map;
        if (Build.VERSION.SDK_INT >= 9 && (cookieManager = f5949a) != null && (map = cookieManager.get(URI.create(str), httpURLConnection.getRequestProperties())) != null && map.size() > 0 && map.get("Cookie").size() > 0) {
            httpURLConnection.addRequestProperty("Cookie", TextUtils.join("=", map.get("Cookie")));
        }
    }

    public static CookieManager a() {
        return f5949a;
    }

    public static a a(com.startapp.networkTest.utils.a aVar, WebView webView, String str) {
        b.a((Object) aVar, "Partner is null");
        b.a((Object) webView, "WebView is null");
        b.c(str, "CustomReferenceData is greater than 256 characters");
        return new a(aVar, webView, (String) null, (List<e>) null, str);
    }

    public static a a(com.startapp.networkTest.utils.a aVar, String str, List<e> list, String str2) {
        b.a((Object) aVar, "Partner is null");
        b.a((Object) str, "OMID JS script content is null");
        b.a((Object) list, "VerificationScriptResources is null");
        b.c(str2, "CustomReferenceData is greater than 256 characters");
        return new a(aVar, (WebView) null, str, list, str2);
    }
}
