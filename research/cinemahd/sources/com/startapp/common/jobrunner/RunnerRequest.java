package com.startapp.common.jobrunner;

import java.util.HashMap;
import java.util.Map;

public final class RunnerRequest {

    /* renamed from: a  reason: collision with root package name */
    private final a f5951a;

    public enum NetworkType {
        NONE,
        b,
        WIFI
    }

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public int f5953a;
        /* access modifiers changed from: private */
        public Map<String, String> b = new HashMap();
        /* access modifiers changed from: private */
        public long c;
        /* access modifiers changed from: private */
        public long d = 100;
        /* access modifiers changed from: private */
        public boolean e = false;
        /* access modifiers changed from: private */
        public boolean f = false;
        /* access modifiers changed from: private */
        public NetworkType g = NetworkType.NONE;

        public a(int i) {
            this.f5953a = i;
        }

        public final a a(Map<String, String> map) {
            if (map != null) {
                this.b.putAll(map);
            }
            return this;
        }

        public final RunnerRequest b() {
            return new RunnerRequest(this, (byte) 0);
        }

        public final a a(long j) {
            this.c = j;
            return this;
        }

        public final a a(boolean z) {
            this.e = z;
            return this;
        }

        public final a a(NetworkType networkType) {
            this.g = networkType;
            return this;
        }

        public final a a() {
            this.f = true;
            return this;
        }
    }

    /* synthetic */ RunnerRequest(a aVar, byte b) {
        this(aVar);
    }

    public final int a() {
        return this.f5951a.f5953a;
    }

    public final Map<String, String> b() {
        return this.f5951a.b;
    }

    public final long c() {
        return this.f5951a.c;
    }

    public final long d() {
        return this.f5951a.d;
    }

    public final boolean e() {
        return this.f5951a.e;
    }

    public final NetworkType f() {
        return this.f5951a.g;
    }

    public final boolean g() {
        return this.f5951a.f;
    }

    public final String toString() {
        return "RunnerRequest: " + this.f5951a.f5953a + " " + this.f5951a.c + " " + this.f5951a.e + " " + this.f5951a.d + " " + this.f5951a.b;
    }

    private RunnerRequest(a aVar) {
        this.f5951a = aVar;
    }
}
