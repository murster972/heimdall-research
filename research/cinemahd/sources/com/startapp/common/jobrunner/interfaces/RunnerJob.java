package com.startapp.common.jobrunner.interfaces;

import android.content.Context;

public interface RunnerJob {

    public enum Result {
        SUCCESS,
        FAILED,
        RESCHEDULE
    }

    public interface a {
        void a(Result result);
    }

    void a(Context context, a aVar);
}
