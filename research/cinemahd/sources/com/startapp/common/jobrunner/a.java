package com.startapp.common.jobrunner;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ServiceInfo;
import android.os.Build;
import android.os.PersistableBundle;
import android.os.SystemClock;
import android.util.Log;
import com.startapp.common.b.b;
import com.startapp.common.jobrunner.RunnerRequest;
import com.startapp.common.jobrunner.interfaces.RunnerJob;
import com.startapp.sdk.adsbase.InfoEventService;
import com.startapp.sdk.adsbase.PeriodicJobService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class a {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public static final String f5954a = "a";
    /* access modifiers changed from: private */
    @SuppressLint({"StaticFieldLeak"})
    public static volatile a b = null;
    private static volatile int c = 60000;
    private static final ExecutorService i = Executors.newSingleThreadExecutor();
    private static final ScheduledExecutorService j = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */
    public Context d;
    private List<com.startapp.common.jobrunner.interfaces.a> e = Collections.synchronizedList(new ArrayList());
    /* access modifiers changed from: private */
    public Map<Integer, Integer> f = new ConcurrentHashMap();
    private AtomicInteger g = new AtomicInteger(0);
    private boolean h;

    private a(Context context) {
        this.d = context.getApplicationContext();
        this.h = d(context);
    }

    private static int b(int i2, boolean z) {
        return z ? i2 | Integer.MIN_VALUE : i2;
    }

    public static void b() {
    }

    @TargetApi(21)
    private static JobScheduler c(Context context) {
        return (JobScheduler) context.getSystemService("jobscheduler");
    }

    public static void c() {
    }

    private static boolean f() {
        return Build.VERSION.SDK_INT >= 21;
    }

    @TargetApi(24)
    private static JobInfo b(Context context, int i2) {
        JobInfo.Builder minimumLatency = new JobInfo.Builder(i2, new ComponentName(context, PeriodicJobService.class)).setMinimumLatency(60000);
        if (b.a(context, "android.permission.RECEIVE_BOOT_COMPLETED")) {
            minimumLatency.setPersisted(true);
        }
        return minimumLatency.build();
    }

    private static boolean d(Context context) {
        try {
            for (ServiceInfo serviceInfo : context.getPackageManager().getPackageInfo(context.getPackageName(), 4).services) {
                if (serviceInfo.name.equals(InfoEventService.class.getName())) {
                    return true;
                }
            }
        } catch (Throwable unused) {
        }
        return false;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:4|5|(7:7|(1:9)|10|11|12|13|(5:15|(5:17|18|19|(4:21|(1:23)|24|42)(1:43)|25)|27|28|(3:30|(1:32)|33)))|34|35) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:34:0x008b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.startapp.common.jobrunner.a a(android.content.Context r10) {
        /*
            com.startapp.common.jobrunner.a r0 = b
            if (r0 != 0) goto L_0x0090
            java.lang.Class<com.startapp.common.jobrunner.a> r0 = com.startapp.common.jobrunner.a.class
            monitor-enter(r0)
            com.startapp.common.jobrunner.a r1 = b     // Catch:{ all -> 0x008d }
            if (r1 != 0) goto L_0x008b
            android.content.Context r1 = r10.getApplicationContext()     // Catch:{ all -> 0x008d }
            if (r1 == 0) goto L_0x0015
            android.content.Context r10 = r10.getApplicationContext()     // Catch:{ all -> 0x008d }
        L_0x0015:
            com.startapp.common.jobrunner.a r1 = new com.startapp.common.jobrunner.a     // Catch:{ all -> 0x008d }
            r1.<init>(r10)     // Catch:{ all -> 0x008d }
            b = r1     // Catch:{ all -> 0x008d }
            java.lang.String r1 = "com.startapp.android.publish.RunnerPrefsFile"
            r2 = 0
            android.content.SharedPreferences r10 = r10.getSharedPreferences(r1, r2)     // Catch:{ Exception -> 0x008b }
            java.lang.String r1 = "RegisteredClassesNames"
            r3 = 0
            java.lang.String r1 = r10.getString(r1, r3)     // Catch:{ Exception -> 0x008b }
            if (r1 == 0) goto L_0x008b
            java.lang.String r4 = ","
            java.lang.String[] r4 = r1.split(r4)     // Catch:{ Exception -> 0x008b }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x008b }
            int r6 = r1.length()     // Catch:{ Exception -> 0x008b }
            r5.<init>(r6)     // Catch:{ Exception -> 0x008b }
            int r6 = r4.length     // Catch:{ Exception -> 0x008b }
        L_0x003c:
            if (r2 >= r6) goto L_0x006a
            r7 = r4[r2]     // Catch:{ Exception -> 0x008b }
            java.lang.Class r8 = java.lang.Class.forName(r7)     // Catch:{ ClassNotFoundException -> 0x0067 }
            java.lang.Class<com.startapp.common.jobrunner.interfaces.a> r9 = com.startapp.common.jobrunner.interfaces.a.class
            boolean r9 = r9.isAssignableFrom(r8)     // Catch:{ ClassNotFoundException -> 0x0067 }
            if (r9 == 0) goto L_0x0067
            com.startapp.common.jobrunner.a r9 = b     // Catch:{ ClassNotFoundException -> 0x0067 }
            java.util.List<com.startapp.common.jobrunner.interfaces.a> r9 = r9.e     // Catch:{ ClassNotFoundException -> 0x0067 }
            java.lang.Object r8 = r8.newInstance()     // Catch:{ ClassNotFoundException -> 0x0067 }
            com.startapp.common.jobrunner.interfaces.a r8 = (com.startapp.common.jobrunner.interfaces.a) r8     // Catch:{ ClassNotFoundException -> 0x0067 }
            r9.add(r8)     // Catch:{ ClassNotFoundException -> 0x0067 }
            int r8 = r5.length()     // Catch:{ ClassNotFoundException -> 0x0067 }
            if (r8 <= 0) goto L_0x0064
            r8 = 44
            r5.append(r8)     // Catch:{ ClassNotFoundException -> 0x0067 }
        L_0x0064:
            r5.append(r7)     // Catch:{ ClassNotFoundException -> 0x0067 }
        L_0x0067:
            int r2 = r2 + 1
            goto L_0x003c
        L_0x006a:
            java.lang.String r2 = r5.toString()     // Catch:{ Exception -> 0x008b }
            boolean r1 = r2.equals(r1)     // Catch:{ Exception -> 0x008b }
            if (r1 != 0) goto L_0x008b
            android.content.SharedPreferences$Editor r10 = r10.edit()     // Catch:{ Exception -> 0x008b }
            java.lang.String r1 = "RegisteredClassesNames"
            int r2 = r5.length()     // Catch:{ Exception -> 0x008b }
            if (r2 <= 0) goto L_0x0084
            java.lang.String r3 = r5.toString()     // Catch:{ Exception -> 0x008b }
        L_0x0084:
            android.content.SharedPreferences$Editor r10 = r10.putString(r1, r3)     // Catch:{ Exception -> 0x008b }
            r10.commit()     // Catch:{ Exception -> 0x008b }
        L_0x008b:
            monitor-exit(r0)     // Catch:{ all -> 0x008d }
            goto L_0x0090
        L_0x008d:
            r10 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x008d }
            throw r10
        L_0x0090:
            com.startapp.common.jobrunner.a r10 = b
            return r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.jobrunner.a.a(android.content.Context):com.startapp.common.jobrunner.a");
    }

    private static boolean b(int i2, RunnerRequest runnerRequest) {
        AlarmManager b2 = b(b.d);
        if (b2 == null) {
            return false;
        }
        Intent intent = new Intent(b.d, InfoEventService.class);
        Map<String, String> b3 = runnerRequest.b();
        for (String next : b3.keySet()) {
            intent.putExtra(next, b3.get(next));
        }
        intent.putExtra("__RUNNER_TASK_ID__", i2);
        intent.putExtra("__RUNNER_RECURRING_ID__", runnerRequest.e());
        intent.putExtra("__RUNNER_TRIGGER_ID__", runnerRequest.c());
        PendingIntent service = PendingIntent.getService(b.d, i2, intent, 134217728);
        b2.cancel(service);
        if (runnerRequest.e()) {
            b2.setRepeating(0, System.currentTimeMillis() + runnerRequest.d(), runnerRequest.c(), service);
            return true;
        }
        b2.set(3, SystemClock.elapsedRealtime() + runnerRequest.c(), service);
        return true;
    }

    /* access modifiers changed from: private */
    public static boolean b(final RunnerRequest runnerRequest, final RunnerJob.a aVar) {
        StringBuilder sb = new StringBuilder("RunnerJob ");
        sb.append(runnerRequest.a());
        sb.append(" ");
        sb.append(runnerRequest.a() & Integer.MAX_VALUE);
        final int a2 = runnerRequest.a() & Integer.MAX_VALUE;
        final RunnerJob a3 = a(a2);
        if (a3 != null) {
            i.execute(new Runnable() {
                public final void run() {
                    a3.a(a.b.d, new RunnerJob.a() {
                        public final void a(RunnerJob.Result result) {
                            String unused = a.f5954a;
                            StringBuilder sb = new StringBuilder("job.execute ");
                            sb.append(runnerRequest.a());
                            sb.append(" ");
                            sb.append(result);
                            a.b();
                            if (result == RunnerJob.Result.RESCHEDULE && !runnerRequest.e()) {
                                a.a(runnerRequest);
                            }
                            aVar.a(result);
                        }
                    });
                }
            });
            return true;
        }
        i.execute(new Runnable() {
            public final void run() {
                String unused = a.f5954a;
                new StringBuilder("runJob: failed to get job for ID ").append(runnerRequest.a());
                a.b();
                aVar.a(RunnerJob.Result.FAILED);
            }
        });
        return true;
    }

    public static void a(com.startapp.common.jobrunner.interfaces.a aVar) {
        b.e.add(aVar);
        String name = aVar.getClass().getName();
        SharedPreferences sharedPreferences = b.d.getSharedPreferences("com.startapp.android.publish.RunnerPrefsFile", 0);
        String string = sharedPreferences.getString("RegisteredClassesNames", (String) null);
        if (string == null) {
            sharedPreferences.edit().putString("RegisteredClassesNames", name).commit();
        } else if (!string.contains(name)) {
            SharedPreferences.Editor edit = sharedPreferences.edit();
            edit.putString("RegisteredClassesNames", string + "," + name).commit();
        }
    }

    private static AlarmManager b(Context context) {
        return (AlarmManager) context.getSystemService("alarm");
    }

    public final void a() {
        if (b.e(this.d)) {
            Intent intent = new Intent(this.d, InfoEventService.class);
            intent.putExtra("__RUNNER_TASK_ID__", Integer.MAX_VALUE);
            this.d.startService(intent);
            return;
        }
        a(this.d, 2147483646);
    }

    @TargetApi(24)
    public static void a(Context context, int i2) {
        JobInfo a2;
        JobScheduler c2 = c(context);
        if (c2 != null && (a2 = a(context, c2, i2)) != null) {
            c2.schedule(a2);
        }
    }

    @TargetApi(24)
    private static JobInfo a(Context context, JobScheduler jobScheduler, int i2) {
        int i3 = Integer.MAX_VALUE;
        if (i2 == Integer.MAX_VALUE) {
            i3 = 2147483646;
        }
        JobInfo b2 = b(context, i3);
        JobInfo pendingJob = jobScheduler.getPendingJob(i3);
        if (pendingJob == null || !pendingJob.getService().equals(b2.getService())) {
            return b2;
        }
        Log.e(f5954a, "Cached process: 2 jobs are pending, must never happened");
        return null;
    }

    public static boolean a(final RunnerRequest runnerRequest) {
        try {
            final int b2 = b(runnerRequest.a(), runnerRequest.e());
            StringBuilder sb = new StringBuilder("schedule ");
            sb.append(b2);
            sb.append(" ");
            sb.append(runnerRequest);
            if (!b.h) {
                final int incrementAndGet = b.g.incrementAndGet();
                AnonymousClass1 r3 = new Runnable() {
                    public final void run() {
                        Integer num = (Integer) a.b.f.get(Integer.valueOf(b2));
                        if (num != null && num.intValue() == incrementAndGet) {
                            if (!runnerRequest.e()) {
                                a.b.f.remove(Integer.valueOf(b2));
                            }
                            boolean unused = a.b(runnerRequest, (RunnerJob.a) new RunnerJob.a() {
                                public final void a(RunnerJob.Result result) {
                                }
                            });
                        }
                    }
                };
                if (runnerRequest.e()) {
                    j.scheduleAtFixedRate(r3, runnerRequest.d(), runnerRequest.d(), TimeUnit.MILLISECONDS);
                } else {
                    j.schedule(r3, runnerRequest.c(), TimeUnit.MILLISECONDS);
                }
                b.f.put(Integer.valueOf(b2), Integer.valueOf(incrementAndGet));
                return true;
            } else if (f()) {
                return a(b2, runnerRequest);
            } else {
                return b(b2, runnerRequest);
            }
        } catch (Exception unused) {
            return false;
        }
    }

    @TargetApi(21)
    private static boolean a(int i2, RunnerRequest runnerRequest) {
        int i3;
        JobScheduler c2 = c(b.d);
        if (c2 == null) {
            return false;
        }
        PersistableBundle persistableBundle = new PersistableBundle();
        Map<String, String> b2 = runnerRequest.b();
        for (String next : b2.keySet()) {
            persistableBundle.putString(next, b2.get(next));
        }
        persistableBundle.putInt("__RUNNER_RECURRING_ID__", runnerRequest.e() ? 1 : 0);
        persistableBundle.putLong("__RUNNER_TRIGGER_ID__", runnerRequest.c());
        JobInfo.Builder builder = new JobInfo.Builder(i2, new ComponentName(b.d, PeriodicJobService.class));
        builder.setExtras(persistableBundle);
        if (runnerRequest.e()) {
            builder.setPeriodic(runnerRequest.c());
        } else {
            builder.setMinimumLatency(runnerRequest.c()).setOverrideDeadline(runnerRequest.c() + ((long) c));
        }
        if (runnerRequest.f() == RunnerRequest.NetworkType.WIFI) {
            i3 = 2;
        } else {
            i3 = runnerRequest.f() == RunnerRequest.NetworkType.b ? 1 : 0;
        }
        builder.setRequiredNetworkType(i3);
        if (runnerRequest.g() && b.a(b.d, "android.permission.RECEIVE_BOOT_COMPLETED")) {
            builder.setPersisted(true);
        }
        if (c2.schedule(builder.build()) == 1) {
            return true;
        }
        return false;
    }

    @SuppressLint({"NewApi"})
    public static void a(int i2, boolean z) {
        try {
            int b2 = b(i2, z);
            if (!b.h) {
                b.f.remove(Integer.valueOf(b2));
            } else if (f()) {
                JobScheduler c2 = c(b.d);
                if (c2 != null) {
                    c2.cancel(b2);
                }
            } else {
                AlarmManager b3 = b(b.d);
                if (b3 != null) {
                    Intent intent = new Intent(b.d, InfoEventService.class);
                    Context context = b.d;
                    PendingIntent service = PendingIntent.getService(context, b2, intent, 134217728);
                    if (PendingIntent.getService(context, 0, intent, 268435456) != null) {
                        b3.cancel(service);
                        service.cancel();
                    }
                }
            }
        } catch (Exception unused) {
        }
    }

    public static boolean a(Intent intent, RunnerJob.a aVar) {
        new StringBuilder("runJob ").append(intent != null ? intent : "NULL");
        if (intent == null) {
            return false;
        }
        b(a(intent), aVar);
        return true;
    }

    @TargetApi(21)
    public static boolean a(JobParameters jobParameters, RunnerJob.a aVar) {
        new StringBuilder("runJob ").append(jobParameters);
        return b(a(jobParameters), aVar);
    }

    /* JADX WARNING: Removed duplicated region for block: B:1:0x0009 A[LOOP:0: B:1:0x0009->B:4:0x0019, LOOP_START, PHI: r1 
      PHI: (r1v1 com.startapp.common.jobrunner.interfaces.RunnerJob) = (r1v0 com.startapp.common.jobrunner.interfaces.RunnerJob), (r1v5 com.startapp.common.jobrunner.interfaces.RunnerJob) binds: [B:0:0x0000, B:4:0x0019] A[DONT_GENERATE, DONT_INLINE]] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.startapp.common.jobrunner.interfaces.RunnerJob a(int r3) {
        /*
            com.startapp.common.jobrunner.a r0 = b
            java.util.List<com.startapp.common.jobrunner.interfaces.a> r0 = r0.e
            java.util.Iterator r0 = r0.iterator()
            r1 = 0
        L_0x0009:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x001b
            java.lang.Object r1 = r0.next()
            com.startapp.common.jobrunner.interfaces.a r1 = (com.startapp.common.jobrunner.interfaces.a) r1
            com.startapp.common.jobrunner.interfaces.RunnerJob r1 = r1.a(r3)
            if (r1 == 0) goto L_0x0009
        L_0x001b:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.jobrunner.a.a(int):com.startapp.common.jobrunner.interfaces.RunnerJob");
    }

    private static RunnerRequest a(Intent intent) {
        HashMap hashMap;
        int intExtra = intent.getIntExtra("__RUNNER_TASK_ID__", -1);
        boolean booleanExtra = intent.getBooleanExtra("__RUNNER_RECURRING_ID__", false);
        long longExtra = intent.getLongExtra("__RUNNER_TRIGGER_ID__", 0);
        if (intent.getExtras() != null) {
            hashMap = new HashMap(intent.getExtras().keySet().size());
            for (String str : intent.getExtras().keySet()) {
                Object obj = intent.getExtras().get(str);
                if (obj instanceof String) {
                    hashMap.put(str, (String) obj);
                }
            }
        } else {
            hashMap = null;
        }
        return new RunnerRequest.a(intExtra).a((Map<String, String>) hashMap).a(booleanExtra).a(longExtra).b();
    }

    @TargetApi(21)
    private static RunnerRequest a(JobParameters jobParameters) {
        PersistableBundle extras = jobParameters.getExtras();
        boolean z = true;
        if (extras.getInt("__RUNNER_RECURRING_ID__") != 1) {
            z = false;
        }
        long j2 = extras.getLong("__RUNNER_TRIGGER_ID__", 0);
        HashMap hashMap = new HashMap(extras.keySet().size());
        for (String str : extras.keySet()) {
            Object obj = extras.get(str);
            if (obj instanceof String) {
                hashMap.put(str, (String) obj);
            }
        }
        return new RunnerRequest.a(jobParameters.getJobId()).a((Map<String, String>) hashMap).a(z).a(j2).b();
    }
}
