package com.startapp.common;

import android.content.Context;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

public final class c {

    /* renamed from: a  reason: collision with root package name */
    private static volatile c f5946a;
    private volatile PhoneStateListener b = null;
    /* access modifiers changed from: private */
    public volatile String c = "e106";

    public static synchronized void b(final Context context) {
        synchronized (c.class) {
            if (f5946a == null) {
                f5946a = new c();
                new Thread(new Runnable() {
                    public final void run() {
                        Looper.prepare();
                        try {
                            c.a(c.a(), context);
                        } catch (Throwable unused) {
                            String unused2 = c.a().c = "e107";
                        }
                        Looper.loop();
                    }
                }).start();
            }
        }
    }

    public final void a(Context context) {
        a(context, 0);
    }

    private void a(Context context, int i) {
        ((TelephonyManager) context.getSystemService("phone")).listen(this.b, i);
    }

    public static c a() {
        return f5946a;
    }

    static /* synthetic */ void a(c cVar, Context context) {
        cVar.b = new PhoneStateListener() {
            /* JADX WARNING: Code restructure failed: missing block: B:14:?, code lost:
                com.startapp.common.c.a(r4.f5948a, "e104");
             */
            /* JADX WARNING: Code restructure failed: missing block: B:15:0x0036, code lost:
                return;
             */
            /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
            /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x002f */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public final void onSignalStrengthsChanged(android.telephony.SignalStrength r5) {
                /*
                    r4 = this;
                    int r0 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0037 }
                    r1 = 23
                    if (r0 < r1) goto L_0x0014
                    com.startapp.common.c r0 = com.startapp.common.c.this     // Catch:{ Exception -> 0x0037 }
                    int r5 = r5.getLevel()     // Catch:{ Exception -> 0x0037 }
                    java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ Exception -> 0x0037 }
                    java.lang.String unused = r0.c = r5     // Catch:{ Exception -> 0x0037 }
                    return
                L_0x0014:
                    java.lang.Class<android.telephony.SignalStrength> r0 = android.telephony.SignalStrength.class
                    r1 = 0
                    java.lang.Class[] r2 = new java.lang.Class[r1]     // Catch:{ NoSuchMethodException -> 0x002f }
                    java.lang.String r3 = "getLevel"
                    java.lang.reflect.Method r0 = r0.getMethod(r3, r2)     // Catch:{ NoSuchMethodException -> 0x002f }
                    com.startapp.common.c r2 = com.startapp.common.c.this     // Catch:{ NoSuchMethodException -> 0x002f }
                    java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ NoSuchMethodException -> 0x002f }
                    java.lang.Object r5 = r0.invoke(r5, r1)     // Catch:{ NoSuchMethodException -> 0x002f }
                    java.lang.String r5 = java.lang.String.valueOf(r5)     // Catch:{ NoSuchMethodException -> 0x002f }
                    java.lang.String unused = r2.c = r5     // Catch:{ NoSuchMethodException -> 0x002f }
                    return
                L_0x002f:
                    com.startapp.common.c r5 = com.startapp.common.c.this     // Catch:{ Exception -> 0x0037 }
                    java.lang.String r0 = "e104"
                    java.lang.String unused = r5.c = r0     // Catch:{ Exception -> 0x0037 }
                    return
                L_0x0037:
                    com.startapp.common.c r5 = com.startapp.common.c.this
                    java.lang.String r0 = "e105"
                    java.lang.String unused = r5.c = r0
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.c.AnonymousClass2.onSignalStrengthsChanged(android.telephony.SignalStrength):void");
            }
        };
        cVar.a(context, 256);
    }

    public final String b() {
        return this.c;
    }
}
