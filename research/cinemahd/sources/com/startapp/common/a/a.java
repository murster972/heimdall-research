package com.startapp.common.a;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final String f5930a;
    private final String b;
    private final boolean c;
    private final Throwable[] d;

    a(String str, String str2, boolean z) {
        this.f5930a = str;
        this.b = str2;
        this.c = z;
        this.d = null;
    }

    public final String a() {
        return this.f5930a;
    }

    public final String b() {
        return this.b;
    }

    public final boolean c() {
        return this.c;
    }

    public final Throwable[] d() {
        return this.d;
    }

    a(Throwable... thArr) {
        this.f5930a = "0";
        this.b = "";
        this.c = false;
        this.d = thArr;
    }
}
