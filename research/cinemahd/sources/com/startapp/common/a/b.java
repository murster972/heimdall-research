package com.startapp.common.a;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.IBinder;
import java.util.concurrent.LinkedBlockingQueue;

class b implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final LinkedBlockingQueue<IBinder> f5931a = new LinkedBlockingQueue<>(1);
    private boolean b = false;

    static {
        Class<b> cls = b.class;
    }

    b() {
    }

    public final IBinder a() throws InterruptedException {
        if (!this.b) {
            IBinder take = this.f5931a.take();
            if (take != null) {
                this.b = true;
                return take;
            }
            throw new IllegalStateException("Binder is null");
        }
        throw new IllegalStateException("Binder already retrieved");
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        try {
            this.f5931a.put(iBinder);
        } catch (InterruptedException unused) {
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
    }
}
