package com.startapp.common.parser;

public class JSONStreamException extends Exception {
    public JSONStreamException(String str) {
        super(str);
    }

    public JSONStreamException(String str, Throwable th) {
        super(str, th);
    }
}
