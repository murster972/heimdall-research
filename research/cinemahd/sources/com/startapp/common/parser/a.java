package com.startapp.common.parser;

import android.util.Log;
import com.startapp.common.b.b;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class a extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private static final String f5961a = b.a((Class<?>) a.class);
    private static Map<String, Class> d;
    private InputStream b = null;
    private String c;

    static {
        HashMap hashMap = new HashMap();
        d = hashMap;
        hashMap.put("int[]", Integer.class);
        d.put("long[]", Long.class);
        d.put("double[]", Double.class);
        d.put("float[]", Float.class);
        d.put("bool[]", Boolean.class);
        d.put("char[]", Character.class);
        d.put("byte[]", Byte.class);
        d.put("void[]", Void.class);
        d.put("short[]", Short.class);
    }

    public a(String str) {
        this.c = str;
    }

    private static <V> Set b(Class<V> cls, JSONArray jSONArray) throws JSONException, JSONStreamException {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < jSONArray.length(); i++) {
            hashSet.add(Enum.valueOf(cls, jSONArray.getString(i)));
        }
        return hashSet;
    }

    private <V> List<V> c(Class<V> cls, JSONArray jSONArray) throws JSONException, JSONStreamException {
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject == null) {
                arrayList.add(jSONArray.get(i));
            } else {
                arrayList.add(a(cls, optJSONObject));
            }
        }
        return arrayList;
    }

    public final <T> T a(Class<T> cls) {
        try {
            return a(cls, (JSONObject) null);
        } catch (JSONStreamException e) {
            String str = f5961a;
            Log.e(str, "Error while trying to parse object " + cls.toString(), e);
            return null;
        }
    }

    public void close() throws IOException {
        super.close();
    }

    @Deprecated
    public final int read() throws IOException {
        return 0;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:149:0x037f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x0387, code lost:
        throw new com.startapp.common.parser.JSONStreamException("Unknown error occurred ", r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:152:0x0388, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x0389, code lost:
        r12 = r4;
        r16 = r5;
        r10 = r6;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Removed duplicated region for block: B:149:0x037f A[ExcHandler: all (r0v10 'th' java.lang.Throwable A[CUSTOM_DECLARE]), Splitter:B:63:0x010a] */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00d9  */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:58:0x00f4  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x015e A[Catch:{ Exception -> 0x0388, all -> 0x037f }] */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x019d A[Catch:{ Exception -> 0x0388, all -> 0x037f }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <T> T a(java.lang.Class<T> r19, org.json.JSONObject r20) throws com.startapp.common.parser.JSONStreamException {
        /*
            r18 = this;
            r7 = r18
            r0 = r19
            java.lang.String r1 = "Problem instantiating object class "
            java.lang.String r2 = r7.c
            if (r2 == 0) goto L_0x03bf
            if (r2 != 0) goto L_0x001c
            java.lang.String r2 = a()     // Catch:{ Exception -> 0x0013 }
            r7.c = r2     // Catch:{ Exception -> 0x0013 }
            goto L_0x001c
        L_0x0013:
            r0 = move-exception
            com.startapp.common.parser.JSONStreamException r1 = new com.startapp.common.parser.JSONStreamException
            java.lang.String r2 = "Can't read input stream."
            r1.<init>(r2, r0)
            throw r1
        L_0x001c:
            if (r20 != 0) goto L_0x0030
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0027 }
            java.lang.String r3 = r7.c     // Catch:{ JSONException -> 0x0027 }
            r2.<init>(r3)     // Catch:{ JSONException -> 0x0027 }
            r8 = r2
            goto L_0x0032
        L_0x0027:
            r0 = move-exception
            com.startapp.common.parser.JSONStreamException r1 = new com.startapp.common.parser.JSONStreamException
            java.lang.String r2 = "Can't deserialize the object. Failed to create JSON object."
            r1.<init>(r2, r0)
            throw r1
        L_0x0030:
            r8 = r20
        L_0x0032:
            java.lang.Class<com.startapp.common.parser.c> r2 = com.startapp.common.parser.c.class
            java.lang.annotation.Annotation r2 = r0.getAnnotation(r2)     // Catch:{ Exception -> 0x03b6 }
            com.startapp.common.parser.c r2 = (com.startapp.common.parser.c) r2     // Catch:{ Exception -> 0x03b6 }
            int r3 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x03b6 }
            r4 = 9
            java.lang.String r9 = "."
            r10 = 2
            r11 = 0
            r12 = 1
            r13 = 0
            if (r3 < r4) goto L_0x0066
            java.lang.Class<java.net.HttpCookie> r3 = java.net.HttpCookie.class
            boolean r3 = r0.equals(r3)     // Catch:{ Exception -> 0x03b6 }
            if (r3 == 0) goto L_0x0066
            java.lang.reflect.Constructor[] r1 = r19.getDeclaredConstructors()     // Catch:{ Exception -> 0x03b6 }
            r1 = r1[r13]     // Catch:{ Exception -> 0x03b6 }
            r1.setAccessible(r12)     // Catch:{ Exception -> 0x03b6 }
            java.lang.Object[] r3 = new java.lang.Object[r10]     // Catch:{ Exception -> 0x03b6 }
            java.lang.String r4 = "name"
            r3[r13] = r4     // Catch:{ Exception -> 0x03b6 }
            java.lang.String r4 = "value"
            r3[r12] = r4     // Catch:{ Exception -> 0x03b6 }
            java.lang.Object r1 = r1.newInstance(r3)     // Catch:{ Exception -> 0x03b6 }
            goto L_0x00cc
        L_0x0066:
            boolean r3 = r19.isPrimitive()     // Catch:{ Exception -> 0x03b6 }
            if (r3 == 0) goto L_0x0071
            java.lang.Object r0 = r19.newInstance()     // Catch:{ Exception -> 0x03b6 }
            return r0
        L_0x0071:
            java.lang.Class<com.startapp.common.parser.c> r3 = com.startapp.common.parser.c.class
            java.lang.annotation.Annotation r3 = r0.getAnnotation(r3)     // Catch:{ Exception -> 0x03b6 }
            if (r3 == 0) goto L_0x00bd
            boolean r3 = r2.c()     // Catch:{ Exception -> 0x03b6 }
            if (r3 == 0) goto L_0x0080
            goto L_0x00bd
        L_0x0080:
            boolean r3 = r2.c()     // Catch:{ Exception -> 0x03b6 }
            if (r3 != 0) goto L_0x00bb
            java.lang.String r0 = r2.a()     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            java.lang.String r0 = r8.getString(r0)     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            java.lang.String r2 = r2.b()     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            r3.<init>()     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            r3.append(r2)     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            r3.append(r9)     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            r3.append(r0)     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            java.lang.String r0 = r3.toString()     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            java.lang.Class r0 = java.lang.Class.forName(r0)     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            java.lang.Object r0 = r7.a(r0, (org.json.JSONObject) r8)     // Catch:{ ClassNotFoundException -> 0x00b4, JSONException -> 0x00ad }
            return r0
        L_0x00ad:
            r0 = move-exception
            com.startapp.common.parser.JSONStreamException r2 = new com.startapp.common.parser.JSONStreamException     // Catch:{ Exception -> 0x03b6 }
            r2.<init>(r1, r0)     // Catch:{ Exception -> 0x03b6 }
            throw r2     // Catch:{ Exception -> 0x03b6 }
        L_0x00b4:
            r0 = move-exception
            com.startapp.common.parser.JSONStreamException r2 = new com.startapp.common.parser.JSONStreamException     // Catch:{ Exception -> 0x03b6 }
            r2.<init>(r1, r0)     // Catch:{ Exception -> 0x03b6 }
            throw r2     // Catch:{ Exception -> 0x03b6 }
        L_0x00bb:
            r14 = r11
            goto L_0x00cd
        L_0x00bd:
            java.lang.Class[] r1 = new java.lang.Class[r13]     // Catch:{ Exception -> 0x03b6 }
            java.lang.reflect.Constructor r1 = r0.getDeclaredConstructor(r1)     // Catch:{ Exception -> 0x03b6 }
            r1.setAccessible(r12)     // Catch:{ Exception -> 0x03b6 }
            java.lang.Object[] r3 = new java.lang.Object[r13]     // Catch:{ Exception -> 0x03b6 }
            java.lang.Object r1 = r1.newInstance(r3)     // Catch:{ Exception -> 0x03b6 }
        L_0x00cc:
            r14 = r1
        L_0x00cd:
            java.lang.reflect.Field[] r1 = r19.getDeclaredFields()
            if (r2 == 0) goto L_0x00ef
            boolean r2 = r2.c()
            if (r2 == 0) goto L_0x00ef
            int r2 = r1.length
            java.lang.Class r0 = r19.getSuperclass()
            java.lang.reflect.Field[] r0 = r0.getDeclaredFields()
            int r3 = r0.length
            int r4 = r2 + r3
            java.lang.reflect.Field[] r4 = new java.lang.reflect.Field[r4]
            java.lang.System.arraycopy(r1, r13, r4, r13, r2)
            java.lang.System.arraycopy(r0, r13, r4, r2, r3)
            r15 = r4
            goto L_0x00f0
        L_0x00ef:
            r15 = r1
        L_0x00f0:
            int r6 = r15.length
            r5 = 0
        L_0x00f2:
            if (r5 >= r6) goto L_0x03b5
            r0 = r15[r5]
            int r1 = r0.getModifiers()
            boolean r2 = java.lang.reflect.Modifier.isStatic(r1)
            if (r2 != 0) goto L_0x03a6
            boolean r1 = java.lang.reflect.Modifier.isTransient(r1)
            if (r1 != 0) goto L_0x03a6
            java.lang.String r4 = com.iab.omid.library.startapp.b.a((java.lang.reflect.Field) r0)
            boolean r1 = r8.has(r4)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r1 == 0) goto L_0x0367
            r0.setAccessible(r12)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.annotation.Annotation[] r1 = r0.getDeclaredAnnotations()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            int r1 = r1.length     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r1 <= 0) goto L_0x0148
            java.lang.annotation.Annotation[] r1 = r0.getDeclaredAnnotations()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r1 = r1[r13]     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class r2 = r1.annotationType()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class<com.startapp.common.parser.d> r3 = com.startapp.common.parser.d.class
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r2 == 0) goto L_0x0148
            com.startapp.common.parser.d r1 = (com.startapp.common.parser.d) r1     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class r2 = r1.b()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class r3 = r1.d()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class r16 = r1.c()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            boolean r17 = r1.a()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class r1 = r1.e()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r19 = r16
            r16 = r1
            r1 = 1
            goto L_0x0152
        L_0x0148:
            r19 = r11
            r2 = r19
            r3 = r2
            r16 = r3
            r1 = 0
            r17 = 0
        L_0x0152:
            java.lang.Class r10 = r0.getType()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class<com.startapp.common.parser.c> r13 = com.startapp.common.parser.c.class
            java.lang.annotation.Annotation r10 = r10.getAnnotation(r13)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r10 == 0) goto L_0x019d
            java.lang.Class r1 = r0.getType()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class<com.startapp.common.parser.c> r2 = com.startapp.common.parser.c.class
            java.lang.annotation.Annotation r1 = r1.getAnnotation(r2)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            com.startapp.common.parser.c r1 = (com.startapp.common.parser.c) r1     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            org.json.JSONObject r2 = r8.getJSONObject(r4)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.String r3 = r1.a()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.String r2 = r2.getString(r3)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.String r1 = r1.b()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r3.<init>()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r3.append(r1)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r3.append(r9)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r3.append(r2)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.String r1 = r3.toString()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            org.json.JSONObject r2 = r8.getJSONObject(r4)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Object r1 = r7.a(r1, (org.json.JSONObject) r2)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            goto L_0x03a6
        L_0x019d:
            if (r17 == 0) goto L_0x01b0
            java.lang.Class r1 = r0.getType()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            org.json.JSONObject r2 = r8.getJSONObject(r4)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.lang.Object r1 = r7.a(r1, (org.json.JSONObject) r2)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            goto L_0x03a6
        L_0x01b0:
            if (r1 == 0) goto L_0x022f
            java.lang.Class<java.util.Map> r1 = java.util.Map.class
            boolean r1 = r1.isAssignableFrom(r2)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r1 != 0) goto L_0x01c2
            java.lang.Class<java.util.Collection> r1 = java.util.Collection.class
            boolean r1 = r1.isAssignableFrom(r2)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r1 == 0) goto L_0x022f
        L_0x01c2:
            java.lang.Class<java.util.HashMap> r1 = java.util.HashMap.class
            boolean r1 = r2.equals(r1)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            if (r1 == 0) goto L_0x01e8
            org.json.JSONObject r10 = r8.getJSONObject(r4)     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            java.util.Iterator r13 = r10.keys()     // Catch:{ Exception -> 0x0388, all -> 0x037f }
            r1 = r18
            r2 = r3
            r3 = r19
            r12 = r4
            r4 = r16
            r16 = r5
            r5 = r10
            r10 = r6
            r6 = r13
            java.util.Map r1 = r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x01e8:
            r12 = r4
            r16 = r5
            r10 = r6
            java.lang.Class<java.util.ArrayList> r1 = java.util.ArrayList.class
            boolean r1 = r2.equals(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r1 == 0) goto L_0x0203
            org.json.JSONArray r1 = r8.getJSONArray(r12)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r3 = r19
            java.util.List r1 = r7.c(r3, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x0203:
            r3 = r19
            java.lang.Class<java.util.HashSet> r1 = java.util.HashSet.class
            boolean r1 = r2.equals(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r1 == 0) goto L_0x021a
            org.json.JSONArray r1 = r8.getJSONArray(r12)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.util.Set r1 = r7.a(r3, (org.json.JSONArray) r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x021a:
            java.lang.Class<java.util.EnumSet> r1 = java.util.EnumSet.class
            boolean r1 = r2.equals(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r1 == 0) goto L_0x03a9
            org.json.JSONArray r1 = r8.getJSONArray(r12)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.util.Set r1 = b(r3, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x022f:
            r12 = r4
            r16 = r5
            r10 = r6
            java.lang.Class r1 = r0.getType()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r1 = r1.isEnum()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r1 == 0) goto L_0x024c
            java.lang.Object r1 = r8.get(r12)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Enum r1 = java.lang.Enum.valueOf(r2, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x024c:
            java.lang.Class r1 = r0.getType()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r1 = r1.isPrimitive()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r1 == 0) goto L_0x02d9
            java.lang.Object r1 = r8.get(r12)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class r2 = r0.getType()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class r3 = r1.getClass()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r3 = r3.equals(r2)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 != 0) goto L_0x02d4
            java.lang.Class r3 = r1.getClass()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class<java.lang.String> r4 = java.lang.String.class
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 == 0) goto L_0x0289
            java.lang.Class r3 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x02d4
            java.lang.String r1 = com.iab.omid.library.startapp.b.a((java.lang.reflect.Field) r0)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            int r1 = r8.getInt(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x02d4
        L_0x0289:
            java.lang.Class r3 = java.lang.Integer.TYPE     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 == 0) goto L_0x029c
            java.lang.Number r1 = (java.lang.Number) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x02d4
        L_0x029c:
            java.lang.Class r3 = java.lang.Float.TYPE     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 == 0) goto L_0x02af
            java.lang.Number r1 = (java.lang.Number) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            float r1 = r1.floatValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Float r1 = java.lang.Float.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x02d4
        L_0x02af:
            java.lang.Class r3 = java.lang.Long.TYPE     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 == 0) goto L_0x02c2
            java.lang.Number r1 = (java.lang.Number) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            long r1 = r1.longValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x02d4
        L_0x02c2:
            java.lang.Class r3 = java.lang.Double.TYPE     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x02d4
            java.lang.Number r1 = (java.lang.Number) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            double r1 = r1.doubleValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Double r1 = java.lang.Double.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
        L_0x02d4:
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x02d9:
            java.lang.Class r1 = r0.getType()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r1 = r1.isArray()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r1 == 0) goto L_0x02f3
            if (r2 == 0) goto L_0x02ea
            java.lang.Object[] r1 = r7.a(r8, r2, r0)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x02ee
        L_0x02ea:
            java.lang.Object r1 = a((org.json.JSONObject) r8, (java.lang.reflect.Field) r0)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
        L_0x02ee:
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x02f3:
            java.lang.Object r1 = r8.get(r12)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class r2 = r0.getType()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class r3 = r1.getClass()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            boolean r3 = r3.equals(r2)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 != 0) goto L_0x0359
            java.lang.Class<java.lang.Integer> r3 = java.lang.Integer.class
            boolean r3 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r3 == 0) goto L_0x033b
            java.lang.Class r2 = r1.getClass()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class<java.lang.Double> r3 = java.lang.Double.class
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x0324
            java.lang.Double r1 = (java.lang.Double) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x0359
        L_0x0324:
            java.lang.Class r2 = r1.getClass()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class<java.lang.Long> r3 = java.lang.Long.class
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x0359
            java.lang.Long r1 = (java.lang.Long) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            int r1 = r1.intValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Integer r1 = java.lang.Integer.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x0359
        L_0x033b:
            java.lang.Class<java.lang.Long> r3 = java.lang.Long.class
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x0359
            java.lang.Class r2 = r1.getClass()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Class<java.lang.Integer> r3 = java.lang.Integer.class
            boolean r2 = r2.equals(r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x0359
            java.lang.Integer r1 = (java.lang.Integer) r1     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            long r1 = r1.longValue()     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.Long r1 = java.lang.Long.valueOf(r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
        L_0x0359:
            boolean r2 = r1.equals(r11)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            if (r2 == 0) goto L_0x0363
            r0.set(r14, r11)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x0363:
            r0.set(r14, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x0367:
            r12 = r4
            r16 = r5
            r10 = r6
            java.lang.String r0 = f5961a     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.String r1 = "Field [%s] doesn't exist. Keeping default value."
            r2 = 1
            java.lang.Object[] r3 = new java.lang.Object[r2]     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            r2 = 0
            r3[r2] = r12     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            java.lang.String r1 = java.lang.String.format(r1, r3)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            android.util.Log.i(r0, r1)     // Catch:{ Exception -> 0x037d, all -> 0x037f }
            goto L_0x03a9
        L_0x037d:
            r0 = move-exception
            goto L_0x038d
        L_0x037f:
            r0 = move-exception
            com.startapp.common.parser.JSONStreamException r1 = new com.startapp.common.parser.JSONStreamException
            java.lang.String r2 = "Unknown error occurred "
            r1.<init>(r2, r0)
            throw r1
        L_0x0388:
            r0 = move-exception
            r12 = r4
            r16 = r5
            r10 = r6
        L_0x038d:
            java.lang.String r1 = f5961a
            r2 = 2
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r12
            java.lang.String r0 = r0.toString()
            r5 = 1
            r3[r5] = r0
            java.lang.String r0 = "Failed to get field [%s] %s"
            java.lang.String r0 = java.lang.String.format(r0, r3)
            android.util.Log.e(r1, r0)
            goto L_0x03ac
        L_0x03a6:
            r16 = r5
            r10 = r6
        L_0x03a9:
            r2 = 2
            r4 = 0
            r5 = 1
        L_0x03ac:
            int r0 = r16 + 1
            r5 = r0
            r6 = r10
            r10 = 2
            r12 = 1
            r13 = 0
            goto L_0x00f2
        L_0x03b5:
            return r14
        L_0x03b6:
            r0 = move-exception
            com.startapp.common.parser.JSONStreamException r1 = new com.startapp.common.parser.JSONStreamException
            java.lang.String r2 = "Can't deserialize the object. Failed to instantiate object."
            r1.<init>(r2, r0)
            throw r1
        L_0x03bf:
            com.startapp.common.parser.JSONStreamException r0 = new com.startapp.common.parser.JSONStreamException
            java.lang.String r1 = "Can't read object, the input is null."
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.startapp.common.parser.a.a(java.lang.Class, org.json.JSONObject):java.lang.Object");
    }

    private static Object a(JSONObject jSONObject, Field field) throws JSONException, IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException, IllegalArgumentException, NoSuchFieldException {
        Object obj;
        JSONArray jSONArray = jSONObject.getJSONArray(com.iab.omid.library.startapp.b.a(field));
        int length = jSONArray.length();
        Class cls = d.get(field.getType().getSimpleName());
        Object newInstance = Array.newInstance((Class) cls.getField("TYPE").get((Object) null), length);
        for (int i = 0; i < length; i++) {
            String string = jSONArray.getString(i);
            Class<String> cls2 = String.class;
            if (cls.equals(Character.class)) {
                cls2 = Character.TYPE;
            }
            Constructor constructor = cls.getConstructor(new Class[]{cls2});
            if (cls.equals(Character.class)) {
                obj = constructor.newInstance(new Object[]{Character.valueOf(string.charAt(0))});
            } else {
                obj = constructor.newInstance(new Object[]{string});
            }
            Array.set(newInstance, i, obj);
        }
        return newInstance;
    }

    private <T> T[] a(JSONObject jSONObject, Class<T> cls, Field field) throws JSONStreamException, JSONException {
        JSONArray jSONArray = jSONObject.getJSONArray(com.iab.omid.library.startapp.b.a(field));
        int length = jSONArray.length();
        Object newInstance = Array.newInstance(cls, length);
        for (int i = 0; i < length; i++) {
            Array.set(newInstance, i, a(cls, jSONArray.getJSONObject(i)));
        }
        return (Object[]) newInstance;
    }

    private <K, V> Map<K, V> a(Class<K> cls, Class<V> cls2, Class cls3, JSONObject jSONObject, Iterator<K> it2) throws JSONException, JSONStreamException {
        HashMap hashMap = new HashMap();
        while (it2.hasNext()) {
            K next = it2.next();
            K cast = cls.equals(Integer.class) ? cls.cast(Integer.valueOf(Integer.parseInt((String) next))) : next;
            if (cls.isEnum()) {
                cast = Enum.valueOf(cls, cast.toString());
            }
            String str = (String) next;
            JSONObject optJSONObject = jSONObject.optJSONObject(str);
            if (optJSONObject == null) {
                JSONArray optJSONArray = jSONObject.optJSONArray(str);
                if (optJSONArray != null) {
                    hashMap.put(cast, a(cls3, optJSONArray));
                } else if (cls2.isEnum()) {
                    hashMap.put(cast, Enum.valueOf(cls2, (String) jSONObject.get(str)));
                } else {
                    hashMap.put(cast, jSONObject.get(str));
                }
            } else {
                hashMap.put(cast, a(cls2, optJSONObject));
            }
        }
        return hashMap;
    }

    private <V> Set<V> a(Class<V> cls, JSONArray jSONArray) throws JSONException, JSONStreamException {
        HashSet hashSet = new HashSet();
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject == null) {
                hashSet.add(jSONArray.get(i));
            } else {
                hashSet.add(a(cls, optJSONObject));
            }
        }
        return hashSet;
    }

    private static String a() throws Exception {
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader((InputStream) null));
            while (true) {
                try {
                    String readLine = bufferedReader.readLine();
                    if (readLine == null) {
                        return sb.toString();
                    }
                    sb.append(readLine);
                } catch (IOException e) {
                    Log.e(f5961a, String.format("Can't create BufferedReader [%s]", new Object[]{e.toString()}));
                    throw e;
                }
            }
        } catch (Exception e2) {
            Log.e(f5961a, String.format("Can't create BufferedReader [%s]", new Object[]{e2.toString()}));
            throw e2;
        }
    }
}
