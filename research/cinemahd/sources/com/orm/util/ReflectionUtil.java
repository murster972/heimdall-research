package com.orm.util;

import android.util.Log;
import com.orm.SugarRecord;
import com.orm.dsl.Ignore;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ReflectionUtil {
    public static List<Field> a(Class cls) {
        List<Field> a2 = SugarConfig.a(cls);
        if (a2 != null) {
            return a2;
        }
        Log.d(SugarRecord.SUGAR, "Fetching properties");
        ArrayList<Field> arrayList = new ArrayList<>();
        a(arrayList, cls);
        ArrayList arrayList2 = new ArrayList();
        for (Field field : arrayList) {
            if (!field.isAnnotationPresent(Ignore.class) && !Modifier.isStatic(field.getModifiers()) && !Modifier.isTransient(field.getModifiers())) {
                arrayList2.add(field);
            }
        }
        SugarConfig.a(cls, arrayList2);
        return arrayList2;
    }

    private static List<Field> a(List<Field> list, Class<?> cls) {
        Collections.addAll(list, cls.getDeclaredFields());
        return cls.getSuperclass() != null ? a(list, cls.getSuperclass()) : list;
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(5:79|80|81|82|103) */
    /* JADX WARNING: Code restructure failed: missing block: B:111:?, code lost:
        return;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:81:0x0176 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.database.Cursor r9, java.lang.reflect.Field r10, java.lang.Object r11) {
        /*
            java.lang.String r0 = "field set error"
            r1 = 1
            r10.setAccessible(r1)
            java.lang.Class r2 = r10.getType()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r3 = com.orm.util.NamingHelper.a((java.lang.reflect.Field) r10)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            int r4 = r9.getColumnIndex(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r4 >= 0) goto L_0x001c
            java.lang.String r9 = "SUGAR"
            java.lang.String r10 = "Invalid colName, you should upgrade database"
            android.util.Log.e(r9, r10)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            return
        L_0x001c:
            boolean r5 = r9.isNull(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r5 == 0) goto L_0x0023
            return
        L_0x0023:
            java.lang.String r5 = "id"
            boolean r3 = r3.equalsIgnoreCase(r5)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x0038
            long r1 = r9.getLong(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Long r9 = java.lang.Long.valueOf(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x0038:
            java.lang.Class r3 = java.lang.Long.TYPE     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 != 0) goto L_0x0200
            java.lang.Class<java.lang.Long> r3 = java.lang.Long.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x004a
            goto L_0x0200
        L_0x004a:
            java.lang.Class<java.lang.String> r3 = java.lang.String.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r5 = 0
            java.lang.String r6 = "null"
            if (r3 == 0) goto L_0x0067
            java.lang.String r9 = r9.getString(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r9 == 0) goto L_0x0062
            boolean r1 = r9.equals(r6)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r1 == 0) goto L_0x0062
            r9 = r5
        L_0x0062:
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x0067:
            java.lang.Class r3 = java.lang.Double.TYPE     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 != 0) goto L_0x01f4
            java.lang.Class<java.lang.Double> r3 = java.lang.Double.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x0079
            goto L_0x01f4
        L_0x0079:
            java.lang.Class r3 = java.lang.Boolean.TYPE     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 != 0) goto L_0x01e2
            java.lang.Class<java.lang.Boolean> r3 = java.lang.Boolean.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x008b
            goto L_0x01e2
        L_0x008b:
            java.lang.Class r3 = java.lang.Integer.TYPE     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 != 0) goto L_0x01d6
            java.lang.Class<java.lang.Integer> r3 = java.lang.Integer.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x009d
            goto L_0x01d6
        L_0x009d:
            java.lang.Class r3 = java.lang.Float.TYPE     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 != 0) goto L_0x01ca
            java.lang.Class<java.lang.Float> r3 = java.lang.Float.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x00af
            goto L_0x01ca
        L_0x00af:
            java.lang.Class r3 = java.lang.Short.TYPE     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 != 0) goto L_0x01be
            java.lang.Class<java.lang.Short> r3 = java.lang.Short.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x00c1
            goto L_0x01be
        L_0x00c1:
            java.lang.Class<java.math.BigDecimal> r3 = java.math.BigDecimal.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x00e0
            java.lang.String r9 = r9.getString(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r9 == 0) goto L_0x00d6
            boolean r1 = r9.equals(r6)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r1 == 0) goto L_0x00d6
            goto L_0x00db
        L_0x00d6:
            java.math.BigDecimal r5 = new java.math.BigDecimal     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r5.<init>(r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
        L_0x00db:
            r10.set(r11, r5)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x00e0:
            java.lang.Class<java.sql.Timestamp> r3 = java.sql.Timestamp.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x00f6
            long r1 = r9.getLong(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.sql.Timestamp r9 = new java.sql.Timestamp     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.<init>(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x00f6:
            java.lang.Class<java.util.Date> r3 = java.util.Date.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x010c
            long r1 = r9.getLong(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.util.Date r9 = new java.util.Date     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.<init>(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x010c:
            java.lang.Class<java.util.Calendar> r3 = java.util.Calendar.class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x0124
            long r1 = r9.getLong(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.util.Calendar r9 = java.util.Calendar.getInstance()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.setTimeInMillis(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x0124:
            java.lang.Class<byte[]> r3 = byte[].class
            boolean r3 = r2.equals(r3)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r3 == 0) goto L_0x0146
            byte[] r1 = r9.getBlob(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            if (r1 != 0) goto L_0x013d
            java.lang.String r9 = ""
            byte[] r9 = r9.getBytes()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x013d:
            byte[] r9 = r9.getBlob(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x0146:
            java.lang.Class<java.lang.Enum> r3 = java.lang.Enum.class
            boolean r2 = r3.isAssignableFrom(r2)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r3 = "Sugar"
            if (r2 == 0) goto L_0x0190
            java.lang.Class r2 = r10.getType()     // Catch:{ Exception -> 0x0176 }
            java.lang.String r5 = "valueOf"
            java.lang.Class[] r6 = new java.lang.Class[r1]     // Catch:{ Exception -> 0x0176 }
            java.lang.Class<java.lang.String> r7 = java.lang.String.class
            r8 = 0
            r6[r8] = r7     // Catch:{ Exception -> 0x0176 }
            java.lang.reflect.Method r2 = r2.getMethod(r5, r6)     // Catch:{ Exception -> 0x0176 }
            java.lang.String r9 = r9.getString(r4)     // Catch:{ Exception -> 0x0176 }
            java.lang.Class r4 = r10.getType()     // Catch:{ Exception -> 0x0176 }
            java.lang.Object[] r1 = new java.lang.Object[r1]     // Catch:{ Exception -> 0x0176 }
            r1[r8] = r9     // Catch:{ Exception -> 0x0176 }
            java.lang.Object r9 = r2.invoke(r4, r1)     // Catch:{ Exception -> 0x0176 }
            r10.set(r11, r9)     // Catch:{ Exception -> 0x0176 }
            goto L_0x021d
        L_0x0176:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.<init>()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r11 = "Enum cannot be read from Sqlite3 database. Please check the type of field "
            r9.append(r11)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r10 = r10.getName()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.append(r10)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r9 = r9.toString()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            android.util.Log.e(r3, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x0190:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.<init>()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r11 = "Class cannot be read from Sqlite3 database. Please check the type of field "
            r9.append(r11)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r11 = r10.getName()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.append(r11)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r11 = "("
            r9.append(r11)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Class r10 = r10.getType()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r10 = r10.getName()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r9.append(r10)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r10 = ")"
            r9.append(r10)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r9 = r9.toString()     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            android.util.Log.e(r3, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x01be:
            short r9 = r9.getShort(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Short r9 = java.lang.Short.valueOf(r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x01ca:
            float r9 = r9.getFloat(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Float r9 = java.lang.Float.valueOf(r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x01d6:
            int r9 = r9.getInt(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x01e2:
            java.lang.String r9 = r9.getString(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.String r1 = "1"
            boolean r9 = r9.equals(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Boolean r9 = java.lang.Boolean.valueOf(r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x01f4:
            double r1 = r9.getDouble(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Double r9 = java.lang.Double.valueOf(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x0200:
            long r1 = r9.getLong(r4)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            java.lang.Long r9 = java.lang.Long.valueOf(r1)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            r10.set(r11, r9)     // Catch:{ IllegalArgumentException -> 0x0215, IllegalAccessException -> 0x020c }
            goto L_0x021d
        L_0x020c:
            r9 = move-exception
            java.lang.String r9 = r9.getMessage()
            android.util.Log.e(r0, r9)
            goto L_0x021d
        L_0x0215:
            r9 = move-exception
            java.lang.String r9 = r9.getMessage()
            android.util.Log.e(r0, r9)
        L_0x021d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.orm.util.ReflectionUtil.a(android.database.Cursor, java.lang.reflect.Field, java.lang.Object):void");
    }
}
