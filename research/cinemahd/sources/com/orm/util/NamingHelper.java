package com.orm.util;

import com.orm.dsl.Column;
import com.orm.dsl.Table;
import java.lang.reflect.Field;

public class NamingHelper {
    public static String a(String str) {
        if (str.equalsIgnoreCase("_id")) {
            return "_id";
        }
        StringBuilder sb = new StringBuilder();
        char[] charArray = str.toCharArray();
        int i = 0;
        while (i < charArray.length) {
            char c = i > 0 ? charArray[i - 1] : 0;
            char c2 = charArray[i];
            boolean z = true;
            char c3 = i < charArray.length - 1 ? charArray[i + 1] : 0;
            if (i != 0) {
                z = false;
            }
            if (z || Character.isLowerCase(c2) || Character.isDigit(c2)) {
                sb.append(Character.toUpperCase(c2));
            } else if (Character.isUpperCase(c2)) {
                if (!Character.isLetterOrDigit(c)) {
                    sb.append(c2);
                } else if (Character.isLowerCase(c)) {
                    sb.append('_');
                    sb.append(c2);
                } else if (c3 <= 0 || !Character.isLowerCase(c3)) {
                    sb.append(c2);
                } else {
                    sb.append('_');
                    sb.append(c2);
                }
            }
            i++;
        }
        return sb.toString();
    }

    public static String a(Field field) {
        if (field.isAnnotationPresent(Column.class)) {
            return ((Column) field.getAnnotation(Column.class)).name();
        }
        return a(field.getName());
    }

    public static String a(Class<?> cls) {
        if (!cls.isAnnotationPresent(Table.class)) {
            return a(cls.getSimpleName());
        }
        Table table = (Table) cls.getAnnotation(Table.class);
        if ("".equals(table.name())) {
            return a(cls.getSimpleName());
        }
        return table.name();
    }
}
