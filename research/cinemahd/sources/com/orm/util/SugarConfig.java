package com.orm.util;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SugarConfig {

    /* renamed from: a  reason: collision with root package name */
    static Map<Class<?>, List<Field>> f5899a = new HashMap();

    public static void a(Class<?> cls, List<Field> list) {
        f5899a.put(cls, list);
    }

    public static List<Field> a(Class<?> cls) {
        if (f5899a.containsKey(cls)) {
            return Collections.synchronizedList(f5899a.get(cls));
        }
        return null;
    }
}
