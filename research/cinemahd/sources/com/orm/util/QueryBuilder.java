package com.orm.util;

public class QueryBuilder {
    public static String a(int i) {
        if (i >= 1) {
            StringBuilder sb = new StringBuilder((i * 2) - 1);
            sb.append("?");
            for (int i2 = 1; i2 < i; i2++) {
                sb.append(",?");
            }
            return sb.toString();
        }
        throw new RuntimeException("The number of arguments must be greater than or equal to 1.");
    }
}
