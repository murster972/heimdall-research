package com.orm;

import java.util.Map;

public class SugarContext {

    /* renamed from: a  reason: collision with root package name */
    private static SugarContext f5897a;

    public static SugarContext c() {
        SugarContext sugarContext = f5897a;
        if (sugarContext != null) {
            return sugarContext;
        }
        throw new NullPointerException("SugarContext has not been initialized properly. Call SugarContext.init(Context) in your Application.onCreate() method and SugarContext.terminate() in your Application.onTerminate() method.");
    }

    /* access modifiers changed from: package-private */
    public Map<Object, Long> a() {
        throw null;
    }

    /* access modifiers changed from: protected */
    public SugarDb b() {
        throw null;
    }
}
