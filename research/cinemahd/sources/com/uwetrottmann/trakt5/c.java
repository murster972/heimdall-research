package com.uwetrottmann.trakt5;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.uwetrottmann.trakt5.enums.Rating;
import java.lang.reflect.Type;

/* compiled from: lambda */
public final /* synthetic */ class c implements JsonSerializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ c f6568a = new c();

    private /* synthetic */ c() {
    }

    public final JsonElement a(Object obj, Type type, JsonSerializationContext jsonSerializationContext) {
        return TraktV2Helper.a((Rating) obj, type, jsonSerializationContext);
    }
}
