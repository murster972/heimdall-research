package com.uwetrottmann.trakt5.entities;

import java.util.List;

public class ListReorderResponse {
    public List<Long> skipped_ids;
    public Integer updated;
}
