package com.uwetrottmann.trakt5.entities;

import org.threeten.bp.OffsetDateTime;

public class ListsLastActivity {
    public OffsetDateTime commented_at;
    public OffsetDateTime liked_at;
    public OffsetDateTime updated_at;
}
