package com.uwetrottmann.trakt5.entities;

import org.threeten.bp.OffsetDateTime;

public class CheckinError {
    public OffsetDateTime expires_at;
}
