package com.uwetrottmann.trakt5;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.uwetrottmann.trakt5.enums.Rating;
import java.lang.reflect.Type;

/* compiled from: lambda */
public final /* synthetic */ class h implements JsonDeserializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ h f6573a = new h();

    private /* synthetic */ h() {
    }

    public final Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        return Rating.fromValue(jsonElement.d());
    }
}
