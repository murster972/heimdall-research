package com.uwetrottmann.trakt5.enums;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public enum SortHow implements TraktEnum {
    ASC("asc"),
    DESC("desc");
    
    private static final Map<String, SortHow> STRING_MAPPING = null;
    private final String value;

    static {
        int i;
        STRING_MAPPING = new HashMap();
        for (SortHow sortHow : values()) {
            STRING_MAPPING.put(sortHow.toString().toUpperCase(Locale.ROOT), sortHow);
        }
    }

    private SortHow(String str) {
        this.value = str;
    }

    public static SortHow fromValue(String str) {
        return STRING_MAPPING.get(str.toUpperCase(Locale.ROOT));
    }

    public String toString() {
        return this.value;
    }
}
