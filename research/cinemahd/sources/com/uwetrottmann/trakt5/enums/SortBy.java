package com.uwetrottmann.trakt5.enums;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public enum SortBy implements TraktEnum {
    RANK("rank"),
    ADDED("added"),
    TITLE("title"),
    RELEASED("released"),
    RUNTIME("runtime"),
    POPULARITY("popularity"),
    PERCENTAGE("percentage"),
    VOTES("votes"),
    MY_RATING("my_rating"),
    RANDOM("random");
    
    private static final Map<String, SortBy> STRING_MAPPING = null;
    private final String value;

    static {
        int i;
        STRING_MAPPING = new HashMap();
        for (SortBy sortBy : values()) {
            STRING_MAPPING.put(sortBy.toString().toUpperCase(Locale.ROOT), sortBy);
        }
    }

    private SortBy(String str) {
        this.value = str;
    }

    public static SortBy fromValue(String str) {
        return STRING_MAPPING.get(str.toUpperCase(Locale.ROOT));
    }

    public String toString() {
        return this.value;
    }
}
