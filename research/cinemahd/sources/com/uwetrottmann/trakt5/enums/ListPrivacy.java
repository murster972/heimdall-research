package com.uwetrottmann.trakt5.enums;

import java.util.HashMap;
import java.util.Map;

public enum ListPrivacy implements TraktEnum {
    PRIVATE("private"),
    FRIENDS("friends"),
    PUBLIC("public");
    
    private static final Map<String, ListPrivacy> STRING_MAPPING = null;
    public final String value;

    static {
        int i;
        STRING_MAPPING = new HashMap();
        for (ListPrivacy listPrivacy : values()) {
            STRING_MAPPING.put(listPrivacy.toString(), listPrivacy);
        }
    }

    private ListPrivacy(String str) {
        this.value = str;
    }

    public static ListPrivacy fromValue(String str) {
        return STRING_MAPPING.get(str);
    }

    public String toString() {
        return this.value;
    }
}
