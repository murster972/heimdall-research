package com.uwetrottmann.trakt5;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import java.lang.reflect.Type;
import org.threeten.bp.OffsetDateTime;

/* compiled from: lambda */
public final /* synthetic */ class f implements JsonDeserializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ f f6571a = new f();

    private /* synthetic */ f() {
    }

    public final Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        return OffsetDateTime.parse(jsonElement.i());
    }
}
