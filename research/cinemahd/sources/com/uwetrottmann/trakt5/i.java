package com.uwetrottmann.trakt5;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.uwetrottmann.trakt5.enums.ListPrivacy;
import java.lang.reflect.Type;

/* compiled from: lambda */
public final /* synthetic */ class i implements JsonDeserializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ i f6574a = new i();

    private /* synthetic */ i() {
    }

    public final Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        return ListPrivacy.fromValue(jsonElement.i());
    }
}
