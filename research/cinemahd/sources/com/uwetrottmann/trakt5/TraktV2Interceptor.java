package com.uwetrottmann.trakt5;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class TraktV2Interceptor implements Interceptor {
    private TraktV2 trakt;

    public TraktV2Interceptor(TraktV2 traktV2) {
        this.trakt = traktV2;
    }

    private static boolean accessTokenIsNotEmpty(String str) {
        return (str == null || str.length() == 0) ? false : true;
    }

    public static Response handleIntercept(Interceptor.Chain chain, String str, String str2) throws IOException {
        Request request = chain.request();
        if (!TraktV2.API_HOST.equals(request.url().host())) {
            return chain.proceed(request);
        }
        Request.Builder newBuilder = request.newBuilder();
        newBuilder.header(TraktV2.HEADER_CONTENT_TYPE, TraktV2.CONTENT_TYPE_JSON);
        newBuilder.header(TraktV2.HEADER_TRAKT_API_KEY, str);
        newBuilder.header(TraktV2.HEADER_TRAKT_API_VERSION, TraktV2.API_VERSION);
        if (hasNoAuthorizationHeader(request) && accessTokenIsNotEmpty(str2)) {
            newBuilder.header("Authorization", "Bearer " + str2);
        }
        return chain.proceed(newBuilder.build());
    }

    private static boolean hasNoAuthorizationHeader(Request request) {
        return request.header("Authorization") == null;
    }

    public Response intercept(Interceptor.Chain chain) throws IOException {
        return handleIntercept(chain, this.trakt.apiKey(), this.trakt.accessToken());
    }
}
