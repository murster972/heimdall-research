package com.uwetrottmann.trakt5;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.uwetrottmann.trakt5.enums.SortBy;
import java.lang.reflect.Type;

/* compiled from: lambda */
public final /* synthetic */ class b implements JsonDeserializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ b f6567a = new b();

    private /* synthetic */ b() {
    }

    public final Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        return SortBy.fromValue(jsonElement.i());
    }
}
