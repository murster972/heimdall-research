package com.uwetrottmann.trakt5;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import java.lang.reflect.Type;
import org.threeten.bp.OffsetDateTime;

/* compiled from: lambda */
public final /* synthetic */ class d implements JsonSerializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ d f6569a = new d();

    private /* synthetic */ d() {
    }

    public final JsonElement a(Object obj, Type type, JsonSerializationContext jsonSerializationContext) {
        return TraktV2Helper.a((OffsetDateTime) obj, type, jsonSerializationContext);
    }
}
