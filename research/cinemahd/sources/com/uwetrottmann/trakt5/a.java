package com.uwetrottmann.trakt5;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.uwetrottmann.trakt5.enums.Status;
import java.lang.reflect.Type;

/* compiled from: lambda */
public final /* synthetic */ class a implements JsonDeserializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ a f6566a = new a();

    private /* synthetic */ a() {
    }

    public final Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        return Status.fromValue(jsonElement.i());
    }
}
