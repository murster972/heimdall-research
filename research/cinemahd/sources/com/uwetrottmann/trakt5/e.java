package com.uwetrottmann.trakt5;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.uwetrottmann.trakt5.enums.SortHow;
import java.lang.reflect.Type;

/* compiled from: lambda */
public final /* synthetic */ class e implements JsonDeserializer {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ e f6570a = new e();

    private /* synthetic */ e() {
    }

    public final Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        return SortHow.fromValue(jsonElement.i());
    }
}
