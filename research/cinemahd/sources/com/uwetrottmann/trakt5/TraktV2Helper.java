package com.uwetrottmann.trakt5;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.uwetrottmann.trakt5.enums.ListPrivacy;
import com.uwetrottmann.trakt5.enums.Rating;
import com.uwetrottmann.trakt5.enums.SortBy;
import com.uwetrottmann.trakt5.enums.SortHow;
import com.uwetrottmann.trakt5.enums.Status;
import java.lang.reflect.Type;
import org.threeten.bp.LocalDate;
import org.threeten.bp.OffsetDateTime;

public class TraktV2Helper {
    public static GsonBuilder getGsonBuilder() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.a(OffsetDateTime.class, f.f6571a);
        gsonBuilder.a(OffsetDateTime.class, d.f6569a);
        gsonBuilder.a(LocalDate.class, g.f6572a);
        gsonBuilder.a(ListPrivacy.class, i.f6574a);
        gsonBuilder.a(Rating.class, h.f6573a);
        gsonBuilder.a(Rating.class, c.f6568a);
        gsonBuilder.a(SortBy.class, b.f6567a);
        gsonBuilder.a(SortHow.class, e.f6570a);
        gsonBuilder.a(Status.class, a.f6566a);
        return gsonBuilder;
    }

    static /* synthetic */ JsonElement a(OffsetDateTime offsetDateTime, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive(offsetDateTime.toString());
    }

    static /* synthetic */ JsonElement a(Rating rating, Type type, JsonSerializationContext jsonSerializationContext) {
        return new JsonPrimitive((Number) Integer.valueOf(rating.value));
    }
}
