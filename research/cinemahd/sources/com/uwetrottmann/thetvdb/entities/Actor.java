package com.uwetrottmann.thetvdb.entities;

public class Actor {
    public Integer id;
    public String image;
    public String imageAdded;
    public Integer imageAuthor;
    public String lastUpdated;
    public String name;
    public String role;
    public Integer seriesId;
    public Integer sortOrder;
}
