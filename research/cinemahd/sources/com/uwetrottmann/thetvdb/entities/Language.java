package com.uwetrottmann.thetvdb.entities;

public class Language {
    public String abbreviation;
    public String englishName;
    public int id;
    public String name;
}
