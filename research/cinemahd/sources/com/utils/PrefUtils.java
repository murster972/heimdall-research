package com.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.original.tase.Logger;
import com.yoku.marumovie.R;
import java.util.List;

public final class PrefUtils {

    /* renamed from: a  reason: collision with root package name */
    public static final String f6529a = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/cinemahd/%s/settings");

    public static int a(Context context) {
        return context.getSharedPreferences("hdmovies", 0).getInt("pref_is_movie_selected_v2", R.id.nav_tv_show);
    }

    public static void b(Context context, SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        context.getSharedPreferences("hdmovies", 0).unregisterOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public static String c(Context context) {
        return context.getSharedPreferences("hdmovies", 0).getString("pref_server_url_cache_v3", "");
    }

    public static SharedPreferences d(Context context) {
        return context.getSharedPreferences("hdmovies", 0);
    }

    public static void e(Context context) {
        context.getSharedPreferences("hdmovies", 0).edit().putInt("pref_last_open_count_v3", b(context) + 1).apply();
    }

    public static boolean f(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("hdmovies", 0);
        return sharedPreferences.getBoolean("pref_autoupdate_checkbox_v3" + Utils.w(), false);
    }

    public static String g(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences("hdmovies", 0);
        return sharedPreferences.getString("pref_last_app_config_v3" + Utils.v(), "");
    }

    public static void d(Context context, String str) {
        context.getSharedPreferences("hdmovies", 0).edit().putString("pref_search_query", str).apply();
    }

    public static void a(Context context, int i) {
        context.getSharedPreferences("hdmovies", 0).edit().putInt("pref_is_movie_selected_v2", i).apply();
    }

    public static void b(Context context, String str) {
        SharedPreferences.Editor edit = context.getSharedPreferences("hdmovies", 0).edit();
        edit.putString("pref_last_app_config_v3" + Utils.v(), str).apply();
    }

    /* JADX WARNING: Removed duplicated region for block: B:21:0x0046 A[SYNTHETIC, Splitter:B:21:0x0046] */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x0053 A[SYNTHETIC, Splitter:B:27:0x0053] */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0062 A[SYNTHETIC, Splitter:B:33:0x0062] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:18:0x0041=Splitter:B:18:0x0041, B:24:0x004e=Splitter:B:24:0x004e} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean c(android.content.Context r5, java.lang.String r6) {
        /*
            java.io.File r0 = new java.io.File
            java.lang.String r1 = f6529a
            r2 = 1
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r6
            java.lang.String r6 = java.lang.String.format(r1, r3)
            r0.<init>(r6)
            r6 = 0
            java.io.ObjectOutputStream r1 = new java.io.ObjectOutputStream     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0040 }
            java.io.FileOutputStream r3 = new java.io.FileOutputStream     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0040 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0040 }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x004d, IOException -> 0x0040 }
            java.lang.String r6 = "hdmovies"
            android.content.SharedPreferences r5 = r5.getSharedPreferences(r6, r4)     // Catch:{ FileNotFoundException -> 0x003b, IOException -> 0x0038, all -> 0x0035 }
            java.util.Map r5 = r5.getAll()     // Catch:{ FileNotFoundException -> 0x003b, IOException -> 0x0038, all -> 0x0035 }
            r1.writeObject(r5)     // Catch:{ FileNotFoundException -> 0x003b, IOException -> 0x0038, all -> 0x0035 }
            r1.flush()     // Catch:{ IOException -> 0x0030 }
            r1.close()     // Catch:{ IOException -> 0x0030 }
            goto L_0x005f
        L_0x0030:
            r5 = move-exception
            r5.printStackTrace()
            goto L_0x005f
        L_0x0035:
            r5 = move-exception
            r6 = r1
            goto L_0x0060
        L_0x0038:
            r5 = move-exception
            r6 = r1
            goto L_0x0041
        L_0x003b:
            r5 = move-exception
            r6 = r1
            goto L_0x004e
        L_0x003e:
            r5 = move-exception
            goto L_0x0060
        L_0x0040:
            r5 = move-exception
        L_0x0041:
            r5.printStackTrace()     // Catch:{ all -> 0x003e }
            if (r6 == 0) goto L_0x005e
            r6.flush()     // Catch:{ IOException -> 0x005a }
            r6.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x005e
        L_0x004d:
            r5 = move-exception
        L_0x004e:
            r5.printStackTrace()     // Catch:{ all -> 0x003e }
            if (r6 == 0) goto L_0x005e
            r6.flush()     // Catch:{ IOException -> 0x005a }
            r6.close()     // Catch:{ IOException -> 0x005a }
            goto L_0x005e
        L_0x005a:
            r5 = move-exception
            r5.printStackTrace()
        L_0x005e:
            r2 = 0
        L_0x005f:
            return r2
        L_0x0060:
            if (r6 == 0) goto L_0x006d
            r6.flush()     // Catch:{ IOException -> 0x0069 }
            r6.close()     // Catch:{ IOException -> 0x0069 }
            goto L_0x006d
        L_0x0069:
            r6 = move-exception
            r6.printStackTrace()
        L_0x006d:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.PrefUtils.c(android.content.Context, java.lang.String):boolean");
    }

    public static void a(Context context, SharedPreferences.OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {
        context.getSharedPreferences("hdmovies", 0).registerOnSharedPreferenceChangeListener(onSharedPreferenceChangeListener);
    }

    public static int b(Context context) {
        return context.getSharedPreferences("hdmovies", 0).getInt("pref_last_open_count_v3", 0);
    }

    public static void a(Context context, boolean z) {
        SharedPreferences.Editor edit = context.getSharedPreferences("hdmovies", 0).edit();
        edit.putBoolean("pref_autoupdate_checkbox_v3" + Utils.w(), z).apply();
    }

    public static <T> void a(String str, List<T> list) {
        try {
            a(str, new Gson().a((Object) list));
        } catch (Throwable unused) {
            Logger.a("recaptchar", "some time null");
        }
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [java.lang.reflect.Type, java.lang.Class<T[]>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> T[] a(java.lang.String r2, java.lang.Class<T[]> r3) {
        /*
            android.content.SharedPreferences r0 = com.movie.FreeMoviesApp.l()
            java.lang.String r1 = ""
            java.lang.String r2 = r0.getString(r2, r1)
            boolean r0 = r2.isEmpty()
            if (r0 == 0) goto L_0x0012
            r2 = 0
            return r2
        L_0x0012:
            com.google.gson.Gson r0 = new com.google.gson.Gson
            r0.<init>()
            java.lang.Object r2 = r0.a((java.lang.String) r2, (java.lang.reflect.Type) r3)
            java.lang.Object[] r2 = (java.lang.Object[]) r2
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.PrefUtils.a(java.lang.String, java.lang.Class):java.lang.Object[]");
    }

    public static void a(String str, String str2) {
        FreeMoviesApp.l().edit().putString(str, str2).apply();
    }

    /* JADX WARNING: Removed duplicated region for block: B:41:0x00af A[SYNTHETIC, Splitter:B:41:0x00af] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b9 A[SYNTHETIC, Splitter:B:47:0x00b9] */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00c3 A[SYNTHETIC, Splitter:B:53:0x00c3] */
    /* JADX WARNING: Removed duplicated region for block: B:59:0x00cf A[SYNTHETIC, Splitter:B:59:0x00cf] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:44:0x00b4=Splitter:B:44:0x00b4, B:38:0x00aa=Splitter:B:38:0x00aa, B:50:0x00be=Splitter:B:50:0x00be} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r7, java.lang.String r8) {
        /*
            java.io.File r0 = new java.io.File
            java.lang.String r1 = f6529a
            r2 = 1
            java.lang.Object[] r3 = new java.lang.Object[r2]
            r4 = 0
            r3[r4] = r8
            java.lang.String r8 = java.lang.String.format(r1, r3)
            r0.<init>(r8)
            r8 = 0
            java.io.ObjectInputStream r1 = new java.io.ObjectInputStream     // Catch:{ FileNotFoundException -> 0x00bd, IOException -> 0x00b3, ClassNotFoundException -> 0x00a9 }
            java.io.FileInputStream r3 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x00bd, IOException -> 0x00b3, ClassNotFoundException -> 0x00a9 }
            r3.<init>(r0)     // Catch:{ FileNotFoundException -> 0x00bd, IOException -> 0x00b3, ClassNotFoundException -> 0x00a9 }
            r1.<init>(r3)     // Catch:{ FileNotFoundException -> 0x00bd, IOException -> 0x00b3, ClassNotFoundException -> 0x00a9 }
            java.lang.String r8 = "hdmovies"
            android.content.SharedPreferences r7 = r7.getSharedPreferences(r8, r4)     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            android.content.SharedPreferences$Editor r7 = r7.edit()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r7.clear()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.lang.Object r8 = r1.readObject()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.util.Map r8 = (java.util.Map) r8     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.util.Set r8 = r8.entrySet()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.util.Iterator r8 = r8.iterator()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
        L_0x0037:
            boolean r0 = r8.hasNext()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            if (r0 == 0) goto L_0x008f
            java.lang.Object r0 = r8.next()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.lang.Object r3 = r0.getValue()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.lang.Object r0 = r0.getKey()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            boolean r5 = r3 instanceof java.lang.Boolean     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            if (r5 == 0) goto L_0x005b
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            boolean r3 = r3.booleanValue()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r7.putBoolean(r0, r3)     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            goto L_0x0037
        L_0x005b:
            boolean r5 = r3 instanceof java.lang.Float     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            if (r5 == 0) goto L_0x0069
            java.lang.Float r3 = (java.lang.Float) r3     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            float r3 = r3.floatValue()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r7.putFloat(r0, r3)     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            goto L_0x0037
        L_0x0069:
            boolean r5 = r3 instanceof java.lang.Integer     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            if (r5 == 0) goto L_0x0077
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            int r3 = r3.intValue()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r7.putInt(r0, r3)     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            goto L_0x0037
        L_0x0077:
            boolean r5 = r3 instanceof java.lang.Long     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            if (r5 == 0) goto L_0x0085
            java.lang.Long r3 = (java.lang.Long) r3     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            long r5 = r3.longValue()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r7.putLong(r0, r5)     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            goto L_0x0037
        L_0x0085:
            boolean r5 = r3 instanceof java.lang.String     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            if (r5 == 0) goto L_0x0037
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r7.putString(r0, r3)     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            goto L_0x0037
        L_0x008f:
            r7.commit()     // Catch:{ FileNotFoundException -> 0x00a3, IOException -> 0x00a0, ClassNotFoundException -> 0x009d, all -> 0x009b }
            r1.close()     // Catch:{ IOException -> 0x0096 }
            goto L_0x00cc
        L_0x0096:
            r7 = move-exception
            r7.printStackTrace()
            goto L_0x00cc
        L_0x009b:
            r7 = move-exception
            goto L_0x00cd
        L_0x009d:
            r7 = move-exception
            r8 = r1
            goto L_0x00aa
        L_0x00a0:
            r7 = move-exception
            r8 = r1
            goto L_0x00b4
        L_0x00a3:
            r7 = move-exception
            r8 = r1
            goto L_0x00be
        L_0x00a6:
            r7 = move-exception
            r1 = r8
            goto L_0x00cd
        L_0x00a9:
            r7 = move-exception
        L_0x00aa:
            r7.printStackTrace()     // Catch:{ all -> 0x00a6 }
            if (r8 == 0) goto L_0x00cb
            r8.close()     // Catch:{ IOException -> 0x00c7 }
            goto L_0x00cb
        L_0x00b3:
            r7 = move-exception
        L_0x00b4:
            r7.printStackTrace()     // Catch:{ all -> 0x00a6 }
            if (r8 == 0) goto L_0x00cb
            r8.close()     // Catch:{ IOException -> 0x00c7 }
            goto L_0x00cb
        L_0x00bd:
            r7 = move-exception
        L_0x00be:
            r7.printStackTrace()     // Catch:{ all -> 0x00a6 }
            if (r8 == 0) goto L_0x00cb
            r8.close()     // Catch:{ IOException -> 0x00c7 }
            goto L_0x00cb
        L_0x00c7:
            r7 = move-exception
            r7.printStackTrace()
        L_0x00cb:
            r2 = 0
        L_0x00cc:
            return r2
        L_0x00cd:
            if (r1 == 0) goto L_0x00d7
            r1.close()     // Catch:{ IOException -> 0x00d3 }
            goto L_0x00d7
        L_0x00d3:
            r8 = move-exception
            r8.printStackTrace()
        L_0x00d7:
            throw r7
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.PrefUtils.a(android.content.Context, java.lang.String):boolean");
    }
}
