package com.utils;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.Okio;

public class DownloadFile extends AsyncTask<String, String, String> {

    /* renamed from: a  reason: collision with root package name */
    private final DownloadCallback f6504a;
    private final Activity b;
    private File c;

    public DownloadFile(Activity activity, DownloadCallback downloadCallback) {
        this.b = activity;
        this.f6504a = downloadCallback;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        try {
            if (!PermissionHelper.b(this.b, 777)) {
                this.c = new File(this.b.getCacheDir(), strArr[1]);
                return null;
            }
            File externalStorageDirectory = Environment.getExternalStorageDirectory();
            File file = new File(externalStorageDirectory.getAbsolutePath() + "/Subtitles");
            if (!file.exists()) {
                file.mkdir();
            }
            this.c = new File(file, strArr[1]);
            a(strArr[0], this.c);
            return null;
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
            this.f6504a.a(e);
            return null;
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: b */
    public void onProgressUpdate(String... strArr) {
    }

    /* access modifiers changed from: protected */
    public void onPreExecute() {
        super.onPreExecute();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        DownloadCallback downloadCallback = this.f6504a;
        if (downloadCallback != null) {
            downloadCallback.a(this.c);
        }
    }

    private void a(String str, File file) throws IOException {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        hashMap.put("Cache-Control", "no-cache");
        hashMap.put(TheTvdb.HEADER_ACCEPT, "*/*");
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.6,en;q=0.4");
        ResponseBody c2 = HttpHelper.e().c(str, (Map<String, String>[]) new Map[]{hashMap});
        long contentLength = c2.contentLength();
        BufferedSource source = c2.source();
        BufferedSink a2 = Okio.a(Okio.b(file));
        Buffer j = a2.j();
        long j2 = 0;
        while (true) {
            long read = source.read(j, (long) 8192);
            if (read != -1) {
                a2.k();
                j2 += read;
                publishProgress(new String[]{String.valueOf((int) ((100 * j2) / contentLength))});
            } else {
                a2.flush();
                a2.close();
                source.close();
                c2.close();
                return;
            }
        }
    }
}
