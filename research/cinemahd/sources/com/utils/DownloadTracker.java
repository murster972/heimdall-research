package com.utils;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.os.HandlerThread;
import com.google.android.exoplayer2.offline.ActionFile;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.ui.DefaultTrackNameProvider;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.util.Log;
import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CopyOnWriteArraySet;

public class DownloadTracker implements DownloadManager.Listener {

    /* renamed from: a  reason: collision with root package name */
    private final CopyOnWriteArraySet<Listener> f6505a = new CopyOnWriteArraySet<>();
    private final HashMap<Uri, DownloadAction> b = new HashMap<>();
    private final ActionFile c;
    private final Handler d;

    public interface Listener {
        void a();
    }

    public DownloadTracker(Context context, DataSource.Factory factory, File file, DownloadAction.Deserializer... deserializerArr) {
        context.getApplicationContext();
        this.c = new ActionFile(file);
        new DefaultTrackNameProvider(context.getResources());
        HandlerThread handlerThread = new HandlerThread("DownloadTracker");
        handlerThread.start();
        this.d = new Handler(handlerThread.getLooper());
        a(deserializerArr.length <= 0 ? DownloadAction.b() : deserializerArr);
    }

    public List<StreamKey> a(Uri uri) {
        if (!this.b.containsKey(uri)) {
            return Collections.emptyList();
        }
        return this.b.get(uri).a();
    }

    public void a(DownloadManager downloadManager) {
    }

    public void b(DownloadManager downloadManager) {
    }

    public void a(DownloadManager downloadManager, DownloadManager.TaskState taskState) {
        DownloadAction downloadAction = taskState.f3385a;
        Uri uri = downloadAction.c;
        if (((downloadAction.d && taskState.b == 2) || (!downloadAction.d && taskState.b == 4)) && this.b.remove(uri) != null) {
            a();
        }
    }

    private void a(DownloadAction.Deserializer[] deserializerArr) {
        try {
            for (DownloadAction downloadAction : this.c.a(deserializerArr)) {
                this.b.put(downloadAction.c, downloadAction);
            }
        } catch (IOException e) {
            Log.a("DownloadTracker", "Failed to load tracked actions", e);
        }
    }

    private void a() {
        Iterator<Listener> it2 = this.f6505a.iterator();
        while (it2.hasNext()) {
            it2.next().a();
        }
        this.d.post(new a(this, (DownloadAction[]) this.b.values().toArray(new DownloadAction[0])));
    }

    public /* synthetic */ void a(DownloadAction[] downloadActionArr) {
        try {
            this.c.a(downloadActionArr);
        } catch (IOException e) {
            Log.a("DownloadTracker", "Failed to store tracked actions", e);
        }
    }
}
