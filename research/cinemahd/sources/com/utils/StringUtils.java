package com.utils;

import java.util.List;

public final class StringUtils {
    private StringUtils() {
        throw new AssertionError("No instances.");
    }

    public static String a(List<String> list, String str) {
        return a(list, str, new StringBuilder(list.size() * 8));
    }

    public static String a(List<String> list, String str, StringBuilder sb) {
        sb.setLength(0);
        if (list != null) {
            for (String next : list) {
                if (sb.length() > 0) {
                    sb.append(str);
                }
                sb.append(next);
            }
        }
        return sb.toString();
    }
}
