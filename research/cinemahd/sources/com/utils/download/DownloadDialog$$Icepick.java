package com.utils.download;

import android.os.Bundle;
import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.download.DownloadDialog;
import icepick.Bundler;
import icepick.Injector;
import java.util.HashMap;
import java.util.Map;

public class DownloadDialog$$Icepick<T extends DownloadDialog> extends Injector.Object<T> {
    private static final Map<String, Bundler<?>> BUNDLERS = new HashMap();
    private static final Injector.Helper H = new Injector.Helper("com.utils.download.DownloadDialog$$Icepick.", BUNDLERS);

    public void restore(T t, Bundle bundle) {
        if (bundle != null) {
            t.currentInfo = (MediaSource) H.getParcelable(bundle, "currentInfo");
            t.movieInfo = (MovieInfo) H.getParcelable(bundle, "movieInfo");
            super.restore(t, bundle);
        }
    }

    public void save(T t, Bundle bundle) {
        super.save(t, bundle);
        H.putParcelable(bundle, "currentInfo", t.currentInfo);
        H.putParcelable(bundle, "movieInfo", t.movieInfo);
    }
}
