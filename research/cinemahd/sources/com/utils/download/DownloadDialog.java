package com.utils.download;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.DialogFragment;
import com.Setting;
import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.FilenameUtils;
import com.utils.PermissionHelper;
import com.utils.Utils;
import com.yoku.marumovie.R;
import icepick.Icepick;
import icepick.State;
import us.shandian.giga.service.DownloadManagerService;

public class DownloadDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    long f6558a;
    private EditText b;
    /* access modifiers changed from: private */
    public TextView c;
    @State
    protected MediaSource currentInfo;
    private SeekBar d;
    private TextView e;
    @State
    protected MovieInfo movieInfo;

    private void b(MediaSource mediaSource) {
        this.currentInfo = mediaSource;
    }

    private void e() {
        String streamLink = this.currentInfo.getStreamLink();
        String string = FreeMoviesApp.l().getString("pref_dowload_path", Setting.b(Utils.i()).toLowerCase());
        String trim = this.b.getText().toString().trim();
        if (trim.isEmpty()) {
            trim = FilenameUtils.a(getContext(), this.currentInfo.getMovieName());
        }
        DownloadManagerService.a(getContext(), streamLink, string, trim + "." + Utils.g(streamLink), false, this.d.getProgress() + 1, this.currentInfo.getPlayHeader(), this.f6558a + "_" + this.movieInfo.getYear() + "_" + this.movieInfo.getSession() + "_" + this.movieInfo.getEps() + "_" + this.movieInfo.tmdbID + "_" + this.movieInfo.imdbIDStr);
        getDialog().dismiss();
        if (PermissionHelper.b(getActivity(), 777)) {
            startActivity(new Intent(getActivity(), DownloadActivity.class));
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (PermissionHelper.b(getActivity(), 778)) {
            Icepick.restoreInstanceState(this, bundle);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.download_dialog, viewGroup);
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(-2, -2);
        window.setGravity(17);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        Icepick.saveInstanceState(this, bundle);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.b = (EditText) view.findViewById(R.id.file_name);
        String movieName = this.currentInfo.getMovieName();
        String str = this.movieInfo.session;
        if (str != null && !str.isEmpty()) {
            movieName = this.movieInfo.getName() + " " + this.movieInfo.session + "x" + this.movieInfo.getEps() + " (" + this.movieInfo.getYear() + ")";
        }
        this.b.setText(FilenameUtils.a(getContext(), movieName));
        this.c = (TextView) view.findViewById(R.id.threads_count);
        this.d = (SeekBar) view.findViewById(R.id.threads);
        this.e = (TextView) view.findViewById(R.id.file_name_text_view);
        this.e.setText(String.format(getContext().getString(R.string.msg_name), new Object[]{this.currentInfo.getFileSizeString()}));
        a((Toolbar) view.findViewById(R.id.toolbar));
        this.c.setText(String.valueOf(3));
        this.d.setProgress(2);
        this.d.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int i, boolean z) {
                DownloadDialog.this.c.setText(String.valueOf(i + 1));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    public static DownloadDialog a(MediaSource mediaSource, MovieInfo movieInfo2, long j) {
        DownloadDialog downloadDialog = new DownloadDialog();
        downloadDialog.b(mediaSource);
        downloadDialog.a(movieInfo2, j);
        return downloadDialog;
    }

    private void a(MovieInfo movieInfo2, long j) {
        this.movieInfo = movieInfo2;
        this.f6558a = j;
    }

    private void a(Toolbar toolbar) {
        toolbar.a((int) R.menu.dialog_url);
        toolbar.setNavigationOnClickListener(new b(this));
        toolbar.setOnMenuItemClickListener(new a(this));
    }

    public /* synthetic */ void a(View view) {
        getDialog().dismiss();
    }

    public /* synthetic */ boolean a(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.okay) {
            return false;
        }
        e();
        return true;
    }
}
