package com.utils.download;

import android.view.MenuItem;
import androidx.appcompat.widget.Toolbar;

/* compiled from: lambda */
public final /* synthetic */ class a implements Toolbar.OnMenuItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ DownloadDialog f6560a;

    public /* synthetic */ a(DownloadDialog downloadDialog) {
        this.f6560a = downloadDialog;
    }

    public final boolean onMenuItemClick(MenuItem menuItem) {
        return this.f6560a.a(menuItem);
    }
}
