package com.utils.download;

import android.content.Intent;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import com.movie.ui.activity.SettingsActivity;
import com.utils.Utils;
import com.yoku.marumovie.R;
import us.shandian.giga.service.DownloadManagerService;
import us.shandian.giga.ui.fragment.AllMissionsFragment;

public class DownloadActivity extends AppCompatActivity {
    /* access modifiers changed from: private */
    public void c() {
        getSupportFragmentManager().b().b(R.id.frame, new AllMissionsFragment()).a(4099).a();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        Intent intent = new Intent();
        intent.setClass(this, DownloadManagerService.class);
        startService(intent);
        super.onCreate(bundle);
        setContentView((int) R.layout.activity_downloader);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.b((CharSequence) "Download Free size: " + ("[" + Formatter.formatFileSize(Utils.i(), Utils.m()) + "]"));
            supportActionBar.f(true);
        }
        getWindow().getDecorView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                DownloadActivity.this.c();
                DownloadActivity.this.getWindow().getDecorView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.download_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == 16908332) {
            onBackPressed();
            return true;
        } else if (itemId != R.id.action_settings) {
            return super.onOptionsItemSelected(menuItem);
        } else {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
    }
}
