package com.utils;

import com.original.tase.model.media.MediaSource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class IntentDataContainer {
    private static IntentDataContainer b;

    /* renamed from: a  reason: collision with root package name */
    private Map<String, ArrayList<MediaSource>> f6523a;

    public static synchronized IntentDataContainer a() {
        IntentDataContainer intentDataContainer;
        synchronized (IntentDataContainer.class) {
            if (b == null) {
                b = new IntentDataContainer();
                b.f6523a = new HashMap();
            }
            intentDataContainer = b;
        }
        return intentDataContainer;
    }

    public void a(String str, ArrayList<MediaSource> arrayList) {
        this.f6523a.put(str, arrayList);
    }

    public ArrayList<MediaSource> a(String str) {
        return this.f6523a.get(str);
    }
}
