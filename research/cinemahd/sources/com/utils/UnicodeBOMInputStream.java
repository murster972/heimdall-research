package com.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.PushbackInputStream;

public class UnicodeBOMInputStream extends InputStream {

    /* renamed from: a  reason: collision with root package name */
    private final PushbackInputStream f6548a;
    private final BOM b;

    public static final class BOM {
        public static final BOM b = new BOM(new byte[0], "NONE");
        public static final BOM c = new BOM(new byte[]{-17, -69, -65}, "UTF-8");
        public static final BOM d = new BOM(new byte[]{-1, -2}, "UTF-16 little-endian");
        public static final BOM e = new BOM(new byte[]{-2, -1}, "UTF-16 big-endian");
        public static final BOM f = new BOM(new byte[]{-1, -2, 0, 0}, "UTF-32 little-endian");
        public static final BOM g = new BOM(new byte[]{0, 0, -2, -1}, "UTF-32 big-endian");
        static final /* synthetic */ boolean h = false;

        /* renamed from: a  reason: collision with root package name */
        private final String f6549a;

        static {
            Class<UnicodeBOMInputStream> cls = UnicodeBOMInputStream.class;
        }

        private BOM(byte[] bArr, String str) {
            if (!h && bArr == null) {
                throw new AssertionError("invalid BOM: null is not allowed");
            } else if (!h && str == null) {
                throw new AssertionError("invalid description: null is not allowed");
            } else if (h || str.length() != 0) {
                this.f6549a = str;
            } else {
                throw new AssertionError("invalid description: empty string is not allowed");
            }
        }

        public final String toString() {
            return this.f6549a;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:44:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:48:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public UnicodeBOMInputStream(java.io.InputStream r9) throws java.lang.NullPointerException, java.io.IOException {
        /*
            r8 = this;
            r8.<init>()
            if (r9 == 0) goto L_0x0089
            java.io.PushbackInputStream r0 = new java.io.PushbackInputStream
            r1 = 4
            r0.<init>(r9, r1)
            r8.f6548a = r0
            byte[] r9 = new byte[r1]
            java.io.PushbackInputStream r0 = r8.f6548a
            int r0 = r0.read(r9)
            r2 = -2
            r3 = -1
            r4 = 2
            r5 = 1
            r6 = 0
            if (r0 == r4) goto L_0x0063
            r7 = 3
            if (r0 == r7) goto L_0x004c
            if (r0 == r1) goto L_0x0022
            goto L_0x007d
        L_0x0022:
            byte r1 = r9[r6]
            if (r1 != r3) goto L_0x0037
            byte r1 = r9[r5]
            if (r1 != r2) goto L_0x0037
            byte r1 = r9[r4]
            if (r1 != 0) goto L_0x0037
            byte r1 = r9[r7]
            if (r1 != 0) goto L_0x0037
            com.utils.UnicodeBOMInputStream$BOM r1 = com.utils.UnicodeBOMInputStream.BOM.f
            r8.b = r1
            goto L_0x0081
        L_0x0037:
            byte r1 = r9[r6]
            if (r1 != 0) goto L_0x004c
            byte r1 = r9[r5]
            if (r1 != 0) goto L_0x004c
            byte r1 = r9[r4]
            if (r1 != r2) goto L_0x004c
            byte r1 = r9[r7]
            if (r1 != r3) goto L_0x004c
            com.utils.UnicodeBOMInputStream$BOM r1 = com.utils.UnicodeBOMInputStream.BOM.g
            r8.b = r1
            goto L_0x0081
        L_0x004c:
            byte r1 = r9[r6]
            r7 = -17
            if (r1 != r7) goto L_0x0063
            byte r1 = r9[r5]
            r7 = -69
            if (r1 != r7) goto L_0x0063
            byte r1 = r9[r4]
            r4 = -65
            if (r1 != r4) goto L_0x0063
            com.utils.UnicodeBOMInputStream$BOM r1 = com.utils.UnicodeBOMInputStream.BOM.c
            r8.b = r1
            goto L_0x0081
        L_0x0063:
            byte r1 = r9[r6]
            if (r1 != r3) goto L_0x0070
            byte r1 = r9[r5]
            if (r1 != r2) goto L_0x0070
            com.utils.UnicodeBOMInputStream$BOM r1 = com.utils.UnicodeBOMInputStream.BOM.d
            r8.b = r1
            goto L_0x0081
        L_0x0070:
            byte r1 = r9[r6]
            if (r1 != r2) goto L_0x007d
            byte r1 = r9[r5]
            if (r1 != r3) goto L_0x007d
            com.utils.UnicodeBOMInputStream$BOM r1 = com.utils.UnicodeBOMInputStream.BOM.e
            r8.b = r1
            goto L_0x0081
        L_0x007d:
            com.utils.UnicodeBOMInputStream$BOM r1 = com.utils.UnicodeBOMInputStream.BOM.b
            r8.b = r1
        L_0x0081:
            if (r0 <= 0) goto L_0x0088
            java.io.PushbackInputStream r1 = r8.f6548a
            r1.unread(r9, r6, r0)
        L_0x0088:
            return
        L_0x0089:
            java.lang.NullPointerException r9 = new java.lang.NullPointerException
            java.lang.String r0 = "invalid input stream: null is not allowed"
            r9.<init>(r0)
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.UnicodeBOMInputStream.<init>(java.io.InputStream):void");
    }

    public int available() throws IOException {
        return this.f6548a.available();
    }

    public void close() throws IOException {
        this.f6548a.close();
    }

    public synchronized void mark(int i) {
        this.f6548a.mark(i);
    }

    public boolean markSupported() {
        return this.f6548a.markSupported();
    }

    public int read() throws IOException {
        return this.f6548a.read();
    }

    public synchronized void reset() throws IOException {
        this.f6548a.reset();
    }

    public final BOM s() {
        return this.b;
    }

    public long skip(long j) throws IOException {
        return this.f6548a.skip(j);
    }

    public int read(byte[] bArr) throws IOException, NullPointerException {
        return this.f6548a.read(bArr, 0, bArr.length);
    }

    public int read(byte[] bArr, int i, int i2) throws IOException, NullPointerException {
        return this.f6548a.read(bArr, i, i2);
    }
}
