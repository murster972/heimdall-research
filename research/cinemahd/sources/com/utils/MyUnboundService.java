package com.utils;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import com.yoku.marumovie.R;

public class MyUnboundService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private static Intent f6524a;

    public static Intent a(Context context) {
        if (f6524a == null) {
            f6524a = new Intent(context, MyUnboundService.class);
        }
        return f6524a;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            a();
        } else {
            startForeground(1, new Notification());
        }
    }

    private void a() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.notification_channel_id));
        builder.c(true);
        builder.b((int) R.mipmap.ic_launcher);
        builder.b((CharSequence) "Running in background");
        builder.a(5);
        builder.a("service");
        builder.a(true);
        Notification a2 = builder.a();
        Intent intent = new Intent("android.settings.APPLICATION_DETAILS_SETTINGS");
        intent.setData(Uri.parse("package:" + getPackageName()));
        a2.contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, intent, 0);
        startForeground(2, a2);
    }
}
