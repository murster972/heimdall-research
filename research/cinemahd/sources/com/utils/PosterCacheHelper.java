package com.utils;

import com.vincentbrison.openlibraries.android.dualcache.Builder;
import com.vincentbrison.openlibraries.android.dualcache.CacheSerializer;
import com.vincentbrison.openlibraries.android.dualcache.DualCache;

public class PosterCacheHelper {
    private static PosterCacheHelper d;

    /* renamed from: a  reason: collision with root package name */
    private long f6528a = Utils.b(Utils.i().getCacheDir());
    private String b = "poster_url_cache";
    private DualCache<String> c;

    PosterCacheHelper() {
        AnonymousClass1 r0 = new CacheSerializer<String>(this) {
            public String a(String str) {
                return str;
            }

            public String b(String str) {
                return str;
            }

            public /* bridge */ /* synthetic */ String a(Object obj) {
                String str = (String) obj;
                b(str);
                return str;
            }
        };
        Builder builder = new Builder(this.b, 1);
        builder.b();
        builder.a((int) this.f6528a, true, r0, Utils.i());
        builder.c();
        this.c = builder.a();
    }

    public static PosterCacheHelper a() {
        if (d == null) {
            d = new PosterCacheHelper();
        }
        return d;
    }

    public synchronized String b(long j, long j2, String str) {
        String str2;
        str2 = "";
        if (j > 0) {
            str2 = "tmdb-backdrop-" + j;
        } else if (j2 > 0) {
            str2 = "tvdb-backdrop-" + j2;
        } else if (str != null) {
            str2 = "imdb-backdrop-" + str;
        }
        return this.c.c(str2);
    }

    public synchronized String c(long j, long j2, String str) {
        String str2;
        str2 = "";
        if (j > 0) {
            str2 = "tmdb-poster-" + j;
        } else if (j2 > 0) {
            str2 = "tvdb-poster-" + j2;
        } else if (str != null) {
            str2 = "imdb-poster-" + str;
        }
        return this.c.c(str2);
    }

    public synchronized void a(long j, long j2, String str) {
        String str2 = "";
        String str3 = "";
        if (j > 0) {
            str2 = "tmdb-poster-" + j;
            str3 = "tmdb-backdrop-" + j;
        }
        if (j2 > 0) {
            str2 = "tvdb-poster-" + j2;
            str3 = "tvdb-backdrop-" + j2;
        }
        if (str != null) {
            str2 = "imdb-poster-" + str;
            str3 = "imdb-backdrop-" + str;
        }
        if (!str2.isEmpty()) {
            this.c.b(str2);
            this.c.b(str3);
        }
    }

    public synchronized void a(long j, long j2, String str, String str2, String str3) {
        if (j > 0) {
            try {
                String str4 = "tmdb-poster-" + j;
                if (!this.c.a(str4)) {
                    this.c.a(str4, str2);
                }
                String str5 = "tmdb-backdrop-" + j;
                if (!this.c.a(str5)) {
                    this.c.a(str5, str3);
                }
            } catch (Throwable th) {
                throw th;
            }
        }
        if (j2 > 0) {
            String str6 = "tvdb-poster-" + j2;
            if (!this.c.a(str6)) {
                this.c.a(str6, str2);
            }
            String str7 = "tvdb-backdrop-" + j2;
            if (!this.c.a(str7)) {
                this.c.a(str7, str3);
            }
        }
        if (str != null) {
            String str8 = "imdb-poster-" + str;
            if (!this.c.a(str8)) {
                this.c.a(str8, str2);
            }
            String str9 = "imdb-backdrop-" + str;
            if (!this.c.a(str9)) {
                this.c.a(str9, str3);
            }
        }
    }
}
