package com.utils.Subtitle.services;

import android.app.Activity;
import android.os.Environment;
import com.movie.data.model.MovieInfo;
import com.utils.DownloadCallback;
import com.utils.DownloadFile;
import com.utils.Subtitle.SubtitleInfo;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.schedulers.Schedulers;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public abstract class SubServiceBase {
    static /* synthetic */ ObservableSource a(MovieInfo movieInfo, ArrayList arrayList) throws Exception {
        if (arrayList.isEmpty()) {
            return new Subscene().a(movieInfo);
        }
        return Observable.just(arrayList);
    }

    public static Observable<ArrayList<SubtitleInfo>> b(MovieInfo movieInfo) {
        return new OpenSubtitleV2().a(movieInfo).subscribeOn(Schedulers.b()).flatMap(new a(movieInfo)).map(b.f6543a);
    }

    public abstract void a(MovieInfo movieInfo, ObservableEmitter<? super ArrayList<SubtitleInfo>> observableEmitter);

    static /* synthetic */ ArrayList a(ArrayList arrayList) throws Exception {
        if (!arrayList.isEmpty()) {
            return arrayList;
        }
        throw new Exception("No subtitle found");
    }

    public Observable<ArrayList<SubtitleInfo>> a(final MovieInfo movieInfo) {
        return Observable.create(new ObservableOnSubscribe<ArrayList<SubtitleInfo>>() {
            public void subscribe(ObservableEmitter<ArrayList<SubtitleInfo>> observableEmitter) throws Exception {
                SubServiceBase.this.a(movieInfo, (ObservableEmitter<? super ArrayList<SubtitleInfo>>) observableEmitter);
                observableEmitter.onComplete();
            }
        });
    }

    public static Observable<List<File>> a(final Activity activity, final String str, final String str2) {
        return Observable.create(new ObservableOnSubscribe<List<File>>() {
            public void subscribe(final ObservableEmitter<List<File>> observableEmitter) throws Exception {
                String str = str;
                if (str.contains("subscene")) {
                    str = Subscene.b(str);
                }
                new DownloadFile(activity, new DownloadCallback() {
                    public void a(File file) {
                        File externalStorageDirectory = Environment.getExternalStorageDirectory();
                        File file2 = new File(externalStorageDirectory.getAbsolutePath() + "/Subtitles/" + str2);
                        file2.mkdir();
                        try {
                            observableEmitter.onNext(SubServiceBase.a(file, file2));
                            observableEmitter.onComplete();
                        } catch (IOException e) {
                            e.printStackTrace();
                            observableEmitter.onComplete();
                        }
                    }

                    public void a(Exception exc) {
                        observableEmitter.onNext(null);
                        observableEmitter.onComplete();
                    }
                }).execute(new String[]{str, "subtitle.zip"});
            }
        });
    }

    public static List<File> a(File file, File file2) throws IOException {
        FileOutputStream fileOutputStream;
        ArrayList arrayList = new ArrayList();
        ZipInputStream zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(file)));
        try {
            byte[] bArr = new byte[8192];
            while (true) {
                ZipEntry nextEntry = zipInputStream.getNextEntry();
                if (nextEntry != null) {
                    File file3 = new File(file2, nextEntry.getName());
                    File parentFile = nextEntry.isDirectory() ? file3 : file3.getParentFile();
                    if (!parentFile.isDirectory()) {
                        if (!parentFile.mkdirs()) {
                            throw new FileNotFoundException("Failed to ensure directory: " + parentFile.getAbsolutePath());
                        }
                    }
                    if (!nextEntry.isDirectory()) {
                        arrayList.add(file3);
                        fileOutputStream = new FileOutputStream(file3);
                        while (true) {
                            int read = zipInputStream.read(bArr);
                            if (read == -1) {
                                break;
                            }
                            fileOutputStream.write(bArr, 0, read);
                        }
                        fileOutputStream.close();
                    }
                } else {
                    zipInputStream.close();
                    return arrayList;
                }
            }
        } catch (Throwable unused) {
            zipInputStream.close();
            return arrayList;
        }
    }
}
