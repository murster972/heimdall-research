package com.utils.Subtitle.services;

import com.movie.data.model.MovieInfo;
import io.reactivex.functions.Function;
import java.util.ArrayList;

/* compiled from: lambda */
public final /* synthetic */ class a implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieInfo f6542a;

    public /* synthetic */ a(MovieInfo movieInfo) {
        this.f6542a = movieInfo;
    }

    public final Object apply(Object obj) {
        return SubServiceBase.a(this.f6542a, (ArrayList) obj);
    }
}
