package com.utils.Subtitle.services;

import com.uwetrottmann.trakt5.TraktV2;
import java.util.ArrayList;
import okhttp3.internal.cache.DiskLruCache;

public class LanguageId {
    static LanguageId b;

    /* renamed from: a  reason: collision with root package name */
    private ArrayList<Language> f6536a;

    public static class Language {

        /* renamed from: a  reason: collision with root package name */
        private String f6537a;
        private String b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public String d;

        public Language(String str, String str2, String str3, String str4) {
            this.f6537a = str;
            this.b = str2;
            this.c = str3;
            this.d = str4;
        }

        public String a() {
            return this.f6537a;
        }

        public String b() {
            return this.b;
        }
    }

    public static LanguageId c() {
        if (b == null) {
            b = new LanguageId();
            b.f6536a = new ArrayList<>();
            b.f6536a.add(new Language("abk", "-1", "ab", "Abkhazian"));
            b.f6536a.add(new Language("afr", "-1", "af", "Afrikaans"));
            b.f6536a.add(new Language("alb", DiskLruCache.VERSION_1, "sq", "Albanian"));
            b.f6536a.add(new Language("ara", TraktV2.API_VERSION, "ar", "Arabic"));
            b.f6536a.add(new Language("arg", "-1", "an", "Aragonese"));
            b.f6536a.add(new Language("arm", "73", "hy", "Armenian"));
            b.f6536a.add(new Language("ast", "-1", "at", "Asturian"));
            b.f6536a.add(new Language("aze", "55", "az", "Azerbaijani"));
            b.f6536a.add(new Language("baq", "74", "eu", "Basque"));
            b.f6536a.add(new Language("bel", "68", "be", "Belarusian"));
            b.f6536a.add(new Language("ben", "54", "bn", "Bengali"));
            b.f6536a.add(new Language("bos", "60", "bs", "Bosnian"));
            b.f6536a.add(new Language("bre", "-1", "br", "Breton"));
            b.f6536a.add(new Language("bul", "5", "bg", "Bulgarian"));
            b.f6536a.add(new Language("bur", "61", "my", "Burmese"));
            b.f6536a.add(new Language("cat", "49", "ca", "Catalan"));
            b.f6536a.add(new Language("chi", "7", "zh", "Chinese (simplified)"));
            b.f6536a.add(new Language("cze", "9", "cs", "Czech"));
            b.f6536a.add(new Language("dan", "10", "da", "Danish"));
            b.f6536a.add(new Language("dut", "11", "nl", "Dutch"));
            b.f6536a.add(new Language("eng", "13", "en", "English"));
            b.f6536a.add(new Language("epo", "47", "eo", "Esperanto"));
            b.f6536a.add(new Language("est", "16", "et", "Estonian"));
            b.f6536a.add(new Language("fin", "17", "fi", "Finnish"));
            b.f6536a.add(new Language("fre", "18", "fr", "French"));
            b.f6536a.add(new Language("geo", "62", "ka", "Georgian"));
            b.f6536a.add(new Language("ger", "19", "de", "German"));
            b.f6536a.add(new Language("gla", "-1", "gd", "Gaelic"));
            b.f6536a.add(new Language("gle", "-1", "ga", "Irish"));
            b.f6536a.add(new Language("glg", "-1", "gl", "Galician"));
            b.f6536a.add(new Language("ell", "21", "el", "Greek"));
            b.f6536a.add(new Language("heb", "22", "he", "Hebrew"));
            b.f6536a.add(new Language("hin", "51", "hi", "Hindi"));
            b.f6536a.add(new Language("hrv", "8", "hr", "Croatian"));
            b.f6536a.add(new Language("hun", "23", "hu", "Hungarian"));
            b.f6536a.add(new Language("ibo", "-1", "ig", "Igbo"));
            b.f6536a.add(new Language("ice", "25", "is", "Icelandic"));
            b.f6536a.add(new Language("ina", "-1", "ia", "Interlingua"));
            b.f6536a.add(new Language("ind", "44", "id", "Indonesian"));
            b.f6536a.add(new Language("ita", "26", "it", "Italian"));
            b.f6536a.add(new Language("jpn", "27", "ja", "Japanese"));
            b.f6536a.add(new Language("kan", "78", "kn", "Kannada"));
            b.f6536a.add(new Language("kaz", "-1", "kk", "Kazakh"));
            b.f6536a.add(new Language("khm", "-1", "km", "Khmer"));
            b.f6536a.add(new Language("kor", "28", "ko", "Korean"));
            b.f6536a.add(new Language("kur", "52", "ku", "Kurdish"));
            b.f6536a.add(new Language("lav", "29", "lv", "Latvian"));
            b.f6536a.add(new Language("lit", "43", "lt", "Lithuanian"));
            b.f6536a.add(new Language("ltz", "-1", "lb", "Luxembourgish"));
            b.f6536a.add(new Language("mac", "48", "mk", "Macedonian"));
            b.f6536a.add(new Language("mal", "64", "ml", "Malayalam"));
            b.f6536a.add(new Language("may", "50", "ms", "Malay"));
            b.f6536a.add(new Language("mni", "65", "ma", "Manipuri"));
            b.f6536a.add(new Language("mon", "72", "mn", "Mongolian"));
            b.f6536a.add(new Language("nav", "-1", "nv", "Navajo"));
            b.f6536a.add(new Language("nor", "30", "no", "Norwegian"));
            b.f6536a.add(new Language("oci", "-1", "oc", "Occitan"));
            b.f6536a.add(new Language("per", "-1", "fa", "Persian"));
            b.f6536a.add(new Language("pol", "31", "pl", "Polish"));
            b.f6536a.add(new Language("por", "32", "pt", "Portuguese"));
            b.f6536a.add(new Language("rus", "34", "ru", "Russian"));
            b.f6536a.add(new Language("scc", "35", "sr", "Serbian"));
            b.f6536a.add(new Language("sin", "-1", "si", "Sinhalese"));
            b.f6536a.add(new Language("slo", "36", "sk", "Slovak"));
            b.f6536a.add(new Language("slv", "37", "sl", "Slovenian"));
            b.f6536a.add(new Language("sme", "-1", "se", "Northern Sami"));
            b.f6536a.add(new Language("snd", "-1", "sd", "Sindhi"));
            b.f6536a.add(new Language("som", "70", "so", "Somali"));
            b.f6536a.add(new Language("spa", "38", "es", "Spanish"));
            b.f6536a.add(new Language("swa", "75", "sw", "Swahili"));
            b.f6536a.add(new Language("swe", "39", "sv", "Swedish"));
            b.f6536a.add(new Language("syr", "-1", "sy", "Syriac"));
            b.f6536a.add(new Language("tam", "59", "ta", "Tamil"));
            b.f6536a.add(new Language("tat", "-1", "tt", "Tatar"));
            b.f6536a.add(new Language("tel", "63", "te", "Telugu"));
            b.f6536a.add(new Language("tgl", "53", "tl", "Tagalog"));
            b.f6536a.add(new Language("tha", "40", "th", "Thai"));
            b.f6536a.add(new Language("tur", "41", "tr", "Turkish"));
            b.f6536a.add(new Language("ukr", "56", "uk", "Ukrainian"));
            b.f6536a.add(new Language("urd", "42", "ur", "Urdu"));
            b.f6536a.add(new Language("vie", "45", "vi", "Vietnamese"));
            b.f6536a.add(new Language("rum", "33", "ro", "Romanian"));
            b.f6536a.add(new Language("pob", "-1", "pb", "Portuguese (BR)"));
            b.f6536a.add(new Language("mne", "-1", "me", "Montenegrin"));
            b.f6536a.add(new Language("zht", "7", "zt", "Chinese (traditional)"));
            b.f6536a.add(new Language("zhe", "-1", "ze", "Chinese bilingual"));
            b.f6536a.add(new Language("pom", "-1", "pm", "Portuguese (MZ)"));
            b.f6536a.add(new Language("ext", "-1", "ex", "Extremaduran"));
        }
        return b;
    }

    public String[] a() {
        String[] strArr = new String[this.f6536a.size()];
        for (int i = 0; i < this.f6536a.size(); i++) {
            strArr[i] = this.f6536a.get(i).c;
        }
        return strArr;
    }

    public String[] b() {
        String[] strArr = new String[this.f6536a.size()];
        for (int i = 0; i < this.f6536a.size(); i++) {
            strArr[i] = this.f6536a.get(i).d;
        }
        return strArr;
    }

    public String a(String str) {
        for (int i = 0; i < this.f6536a.size(); i++) {
            if (this.f6536a.get(i).d.equals(str)) {
                return this.f6536a.get(i).c;
            }
        }
        return "";
    }

    public String b(String str) {
        for (int i = 0; i < this.f6536a.size(); i++) {
            if (this.f6536a.get(i).c.equals(str)) {
                return this.f6536a.get(i).a();
            }
        }
        return "";
    }

    public String c(String str) {
        for (int i = 0; i < this.f6536a.size(); i++) {
            if (this.f6536a.get(i).c.equals(str) && !this.f6536a.get(i).b().equals("-1")) {
                return this.f6536a.get(i).b();
            }
        }
        return "";
    }
}
