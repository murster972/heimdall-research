package com.utils.Subtitle.services;

import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.utils.Subtitle.SubtitleInfo;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

public class OpenSubtitleV2 extends SubServiceBase {
    public void a(MovieInfo movieInfo, ObservableEmitter<? super ArrayList<SubtitleInfo>> observableEmitter) {
        MovieInfo movieInfo2 = movieInfo;
        ObservableEmitter<? super ArrayList<SubtitleInfo>> observableEmitter2 = observableEmitter;
        try {
            String opensubtitle_user_agent = GlobalVariable.c().a().getOpensubtitle_user_agent();
            char c = 0;
            boolean z = movieInfo.getType().intValue() == 1;
            StringBuilder sb = new StringBuilder();
            if (movieInfo2.fileName != null) {
                sb.append(String.format("/query-%s", new Object[]{movieInfo2.fileName.replace(" ", "%20")}));
            } else {
                if (movieInfo2.imdbIDStr != null) {
                    if (!movieInfo2.imdbIDStr.isEmpty()) {
                        sb.append(String.format("/imdbid-%s", new Object[]{movieInfo2.imdbIDStr.replaceAll("[^0-9]", "")}));
                    }
                }
                sb.append(String.format("/imdbid-%s", new Object[]{Utils.a(movieInfo2.tmdbID, z)}));
            }
            if (movieInfo.getSession().intValue() > -1) {
                sb.append(String.format("/season-%s", new Object[]{movieInfo2.session}));
            }
            if (movieInfo.getEps().intValue() > -1) {
                sb.append(String.format("/episode-%s", new Object[]{movieInfo2.eps}));
            }
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", opensubtitle_user_agent);
            ArrayList arrayList = new ArrayList();
            for (String b : FreeMoviesApp.l().getStringSet("pref_sub_language_international_v3", new HashSet(Arrays.asList(new String[]{Locale.getDefault().getLanguage()})))) {
                String b2 = LanguageId.c().b(b);
                StringBuilder sb2 = new StringBuilder("https://rest.opensubtitles.org/search" + sb);
                Object[] objArr = new Object[1];
                objArr[c] = b2;
                sb2.append(String.format("/sublanguageid-%s", objArr));
                HttpHelper e = HttpHelper.e();
                String sb3 = sb2.toString();
                Map[] mapArr = new Map[1];
                mapArr[c] = hashMap;
                String a2 = e.a(sb3, (Map<String, String>[]) mapArr);
                Logger.a("OpenSubtitlesV2", a2);
                OpenSubtitleModel[] openSubtitleModelArr = (OpenSubtitleModel[]) new Gson().a(a2, OpenSubtitleModel[].class);
                int length = openSubtitleModelArr.length;
                int i = 0;
                while (i < length) {
                    OpenSubtitleModel openSubtitleModel = openSubtitleModelArr[i];
                    if (openSubtitleModel.getSubLanguageID().contains(b2)) {
                        String subFileName = openSubtitleModel.getSubFileName();
                        arrayList.add(new SubtitleInfo(subFileName + " [Download count :" + " " + openSubtitleModel.getSubDownloadsCnt() + "]", openSubtitleModel.getZipDownloadLink(), openSubtitleModel.getLanguageName(), Integer.parseInt(openSubtitleModel.getSubDownloadsCnt())));
                    }
                    i++;
                    c = 0;
                }
            }
            observableEmitter2.onNext(arrayList);
        } catch (Exception unused) {
            observableEmitter2.onNext(new ArrayList());
        }
    }
}
