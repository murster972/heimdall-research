package com.utils.Subtitle.services;

import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import com.vungle.warren.model.CookieDBAdapter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class Subscene extends SubServiceBase {

    /* renamed from: a  reason: collision with root package name */
    private static final String[] f6541a = {"Specials", "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth", "Eleventh", "Twelfth", "Thirteenth", "Fourteenth", "Fifteenth", "Sixteenth", "Seventeenth", "Eighteenth", "Nineteenth", "Twentieth", "Twenty-first", "Twenty-second", "Twenty-third", "Twenty-fourth", "Twenty-fifth", "Twenty-sixth", "Twenty-seventh", "Twenty-eighth", "Twenty-ninth"};

    public static String a() {
        String str = "";
        for (String next : FreeMoviesApp.l().getStringSet("pref_sub_language_international_v3", new HashSet(Arrays.asList(new String[]{Locale.getDefault().getLanguage()})))) {
            if (!LanguageId.c().c(next).isEmpty()) {
                str = str + LanguageId.c().c(next) + ",";
            }
        }
        return !str.isEmpty() ? str.substring(0, str.length() - 1) : str;
    }

    public static String b(String str) {
        String a2 = Regex.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]), "<a href=\"(.+?)\" rel=\"nofollow\" onclick=\"DownloadSubtitle", 1);
        if (!a2.startsWith("/")) {
            return a2;
        }
        return "https://subscene.com" + a2;
    }

    private String c(MovieInfo movieInfo) {
        String a2 = a(movieInfo.getName());
        String str = " - " + f6541a[movieInfo.getSession().intValue()] + " Season ";
        int intValue = movieInfo.getSessionYear().intValue();
        HashMap hashMap = new HashMap();
        hashMap.put("authority", "subscene.com");
        hashMap.put("pragma", "no-cache");
        hashMap.put("cache-control", "no-cache");
        hashMap.put("origin", "https://subscene.com/");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("content-type", "application/x-www-form-urlencoded");
        hashMap.put("User-Agent", Constants.f5838a);
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("referer", "https://subscene.com/subtitles/searchbytitle");
        hashMap.put("accept-language", "q=0.9,en-US;q=0.8,en;q=0.7");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, HttpHelper.e().a("https://subscene.com/"));
        int i = 0;
        String a3 = HttpHelper.e().a("https://subscene.com/subtitles/searchbytitle", "query=" + movieInfo.name + str + "&l=", (Map<String, String>[]) new Map[]{hashMap});
        String str2 = null;
        if (a3.isEmpty()) {
            return null;
        }
        ArrayList<ArrayList<String>> b = Regex.b(a3, "<a href=\"(/subtitles/[^\"]+)\">([^<]+)\\((\\d{4})\\)</a>\\s*</div>", 3, 34);
        ArrayList arrayList = b.get(0);
        ArrayList arrayList2 = b.get(1);
        ArrayList arrayList3 = b.get(2);
        while (i < arrayList.size()) {
            String str3 = (String) arrayList.get(i);
            String str4 = (String) arrayList2.get(i);
            String str5 = (String) arrayList3.get(i);
            if (str4.toLowerCase().contains("season")) {
                if (str4.trim().toLowerCase().contains(str.replace("-", "").trim().toLowerCase()) || str4.contains(String.valueOf(movieInfo.getSession()))) {
                    String a4 = Regex.a(str4, "(.+?)\\s+-\\s+.*(?:season|special(?:s)?)", 1, 2);
                    if (a4.isEmpty()) {
                        a4 = Regex.a(str4, "(.+?)\\s+season", 1, 2);
                    }
                    if (a4.isEmpty()) {
                        a4 = str4;
                    }
                    if (TitleHelper.f(a2).equals(TitleHelper.f(a4)) && (str5.trim().isEmpty() || !Utils.b(str5.trim()) || intValue <= 0 || Integer.parseInt(str5.trim()) == intValue)) {
                        if (!str3.startsWith("/")) {
                            return str3;
                        }
                        return "https://subscene.com" + str3;
                    }
                }
            }
            i++;
            str2 = null;
        }
        return str2;
    }

    private String d(MovieInfo movieInfo) {
        HashMap hashMap = new HashMap();
        hashMap.put("authority", "subscene.com");
        hashMap.put("pragma", "no-cache");
        hashMap.put("cache-control", "no-cache");
        hashMap.put("origin", "https://subscene.com/");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("content-type", "application/x-www-form-urlencoded");
        hashMap.put("User-Agent", Constants.f5838a);
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("referer", "https://subscene.com/subtitles/searchbytitle");
        hashMap.put("accept-language", "q=0.9,en-US;q=0.8,en;q=0.7");
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, HttpHelper.e().a("https://subscene.com/"));
        HttpHelper e = HttpHelper.e();
        int i = 0;
        String a2 = e.a("https://subscene.com/subtitles/searchbytitle", "query=" + movieInfo.name + "&l=", (Map<String, String>[]) new Map[]{hashMap});
        if (a2.isEmpty()) {
            return null;
        }
        ArrayList<ArrayList<String>> b = Regex.b(a2, "<a href=\"(/subtitles/[^\"]+)\">([^<]+)\\((\\d{4})\\)</a>\\s*</div>", 3, 34);
        ArrayList arrayList = b.get(0);
        ArrayList arrayList2 = b.get(1);
        ArrayList arrayList3 = b.get(2);
        while (i < arrayList.size()) {
            String str = (String) arrayList.get(i);
            String str2 = (String) arrayList3.get(i);
            if (!TitleHelper.f(movieInfo.getName()).equals(TitleHelper.f((String) arrayList2.get(i))) || (!str2.trim().isEmpty() && Utils.b(str2.trim()) && movieInfo.getYear().intValue() > 0 && Integer.parseInt(str2.trim()) != movieInfo.getYear().intValue())) {
                i++;
            } else if (!str.startsWith("/")) {
                return str;
            } else {
                return "https://subscene.com" + str;
            }
        }
        return null;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0139, code lost:
        if (java.lang.Integer.parseInt(r19) == r23.getEps().intValue()) goto L_0x0144;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.movie.data.model.MovieInfo r23, io.reactivex.ObservableEmitter<? super java.util.ArrayList<com.utils.Subtitle.SubtitleInfo>> r24) {
        /*
            r22 = this;
            r0 = r24
            java.lang.String r1 = "|"
            java.lang.String r2 = "trailer"
            java.lang.String r3 = "no-cache"
            java.lang.String r4 = "https://subscene.com/"
            java.lang.String r5 = "(?:S|s)(\\d\\d)(?:E|e)(\\d\\d)"
            java.util.ArrayList r6 = new java.util.ArrayList     // Catch:{ Exception -> 0x0220 }
            r6.<init>()     // Catch:{ Exception -> 0x0220 }
            java.lang.Integer r7 = r23.getType()     // Catch:{ Exception -> 0x0220 }
            int r7 = r7.intValue()     // Catch:{ Exception -> 0x0220 }
            r8 = 0
            r9 = 1
            if (r7 != r9) goto L_0x001f
            r7 = 1
            goto L_0x0020
        L_0x001f:
            r7 = 0
        L_0x0020:
            if (r7 == 0) goto L_0x0027
            java.lang.String r10 = r22.d(r23)     // Catch:{ Exception -> 0x0220 }
            goto L_0x002b
        L_0x0027:
            java.lang.String r10 = r22.c(r23)     // Catch:{ Exception -> 0x0220 }
        L_0x002b:
            if (r10 == 0) goto L_0x021c
            boolean r11 = r10.isEmpty()     // Catch:{ Exception -> 0x0220 }
            if (r11 != 0) goto L_0x021c
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0220 }
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0220 }
            r12.<init>()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r13 = "LanguageFilter="
            r12.append(r13)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r13 = a()     // Catch:{ Exception -> 0x0220 }
            r12.append(r13)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r13 = ";"
            r12.append(r13)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0220 }
            r11.c((java.lang.String) r4, (java.lang.String) r12)     // Catch:{ Exception -> 0x0220 }
            java.util.HashMap r11 = new java.util.HashMap     // Catch:{ Exception -> 0x0220 }
            r11.<init>()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r12 = "authority"
            java.lang.String r13 = "subscene.com"
            r11.put(r12, r13)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r12 = "pragma"
            r11.put(r12, r3)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r12 = "cache-control"
            r11.put(r12, r3)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = "origin"
            r11.put(r3, r4)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = "upgrade-insecure-requests"
            java.lang.String r4 = "1"
            r11.put(r3, r4)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = "accept"
            java.lang.String r4 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
            r11.put(r3, r4)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = "referer"
            java.lang.String r4 = "https://subscene.com/subtitles/searchbytitle"
            r11.put(r3, r4)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = "accept-language"
            java.lang.String r4 = "en-US;q=0.8,en;q=0.7"
            r11.put(r3, r4)     // Catch:{ Exception -> 0x0220 }
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0220 }
            java.util.Map[] r4 = new java.util.Map[r9]     // Catch:{ Exception -> 0x0220 }
            r4[r8] = r11     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.a((java.lang.String) r10, (java.util.Map<java.lang.String, java.lang.String>[]) r4)     // Catch:{ Exception -> 0x0220 }
            android.content.SharedPreferences r4 = com.movie.FreeMoviesApp.l()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r10 = "pref_sub_language_international_v3"
            java.util.HashSet r11 = new java.util.HashSet     // Catch:{ Exception -> 0x0220 }
            java.lang.String[] r12 = new java.lang.String[r9]     // Catch:{ Exception -> 0x0220 }
            java.util.Locale r13 = java.util.Locale.getDefault()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r13 = r13.getLanguage()     // Catch:{ Exception -> 0x0220 }
            r12[r8] = r13     // Catch:{ Exception -> 0x0220 }
            java.util.List r12 = java.util.Arrays.asList(r12)     // Catch:{ Exception -> 0x0220 }
            r11.<init>(r12)     // Catch:{ Exception -> 0x0220 }
            java.util.Set r4 = r4.getStringSet(r10, r11)     // Catch:{ Exception -> 0x0220 }
            java.util.ArrayList r10 = new java.util.ArrayList     // Catch:{ Exception -> 0x0220 }
            r10.<init>()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r11 = "<td class=\"a1\">\\s*<a href=\"(/subtitles/[^\"]+)\">\\s*<span class=\"[^\"]+ (\\w+-icon)\">\\s*([^\r\n\t]+)\\s*</span>\\s*<span>\\s*([^\r\n\t]+)\\s*</span>\\s*</a>\\s*</td>"
            r12 = 4
            r13 = 34
            java.util.ArrayList r3 = com.original.tase.utils.Regex.b((java.lang.String) r3, (java.lang.String) r11, (int) r12, (int) r13)     // Catch:{ Exception -> 0x0220 }
            java.lang.Object r11 = r3.get(r8)     // Catch:{ Exception -> 0x0220 }
            java.util.ArrayList r11 = (java.util.ArrayList) r11     // Catch:{ Exception -> 0x0220 }
            java.lang.Object r12 = r3.get(r9)     // Catch:{ Exception -> 0x0220 }
            java.util.ArrayList r12 = (java.util.ArrayList) r12     // Catch:{ Exception -> 0x0220 }
            r13 = 2
            java.lang.Object r14 = r3.get(r13)     // Catch:{ Exception -> 0x0220 }
            java.util.ArrayList r14 = (java.util.ArrayList) r14     // Catch:{ Exception -> 0x0220 }
            r15 = 3
            java.lang.Object r3 = r3.get(r15)     // Catch:{ Exception -> 0x0220 }
            java.util.ArrayList r3 = (java.util.ArrayList) r3     // Catch:{ Exception -> 0x0220 }
            com.original.tase.helper.DirectoryIndexHelper r15 = new com.original.tase.helper.DirectoryIndexHelper     // Catch:{ Exception -> 0x0220 }
            r15.<init>()     // Catch:{ Exception -> 0x0220 }
            r15 = 0
        L_0x00e4:
            int r8 = r11.size()     // Catch:{ Exception -> 0x0220 }
            if (r15 >= r8) goto L_0x021c
            java.lang.Object r8 = r11.get(r15)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ Exception -> 0x0220 }
            java.lang.Object r16 = r12.get(r15)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r16 = (java.lang.String) r16     // Catch:{ Exception -> 0x0220 }
            java.lang.Object r17 = r14.get(r15)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r17 = (java.lang.String) r17     // Catch:{ Exception -> 0x0220 }
            java.lang.Object r18 = r3.get(r15)     // Catch:{ Exception -> 0x0220 }
            r13 = r18
            java.lang.String r13 = (java.lang.String) r13     // Catch:{ Exception -> 0x0220 }
            java.lang.String r18 = r13.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r9 = r18.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            r18 = r3
            if (r7 != 0) goto L_0x0142
            r3 = 1
            com.original.tase.utils.Regex.a((java.lang.String) r13, (java.lang.String) r5, (boolean) r3)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r20 = com.original.tase.utils.Regex.a((java.lang.String) r13, (java.lang.String) r5, (int) r3)     // Catch:{ Exception -> 0x0220 }
            r3 = 2
            java.lang.String r19 = com.original.tase.utils.Regex.a((java.lang.String) r13, (java.lang.String) r5, (int) r3)     // Catch:{ Exception -> 0x0220 }
            int r3 = java.lang.Integer.parseInt(r20)     // Catch:{ NumberFormatException -> 0x013c }
            java.lang.Integer r20 = r23.getSession()     // Catch:{ NumberFormatException -> 0x013c }
            r21 = r5
            int r5 = r20.intValue()     // Catch:{ NumberFormatException -> 0x013e }
            if (r3 != r5) goto L_0x013e
            int r3 = java.lang.Integer.parseInt(r19)     // Catch:{ NumberFormatException -> 0x013e }
            java.lang.Integer r5 = r23.getEps()     // Catch:{ NumberFormatException -> 0x013e }
            int r5 = r5.intValue()     // Catch:{ NumberFormatException -> 0x013e }
            if (r3 == r5) goto L_0x0144
            goto L_0x013e
        L_0x013c:
            r21 = r5
        L_0x013e:
            r19 = r11
            goto L_0x0210
        L_0x0142:
            r21 = r5
        L_0x0144:
            java.lang.String r3 = r16.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = "bad-icon"
            boolean r3 = r3.equals(r5)     // Catch:{ Exception -> 0x0220 }
            if (r3 != 0) goto L_0x0156
            r3 = 1
            goto L_0x0157
        L_0x0156:
            r3 = 0
        L_0x0157:
            com.utils.Subtitle.services.LanguageId r5 = com.utils.Subtitle.services.LanguageId.c()     // Catch:{ Exception -> 0x0220 }
            r19 = r11
            java.lang.String r11 = r17.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = r5.a(r11)     // Catch:{ Exception -> 0x0220 }
            boolean r5 = r4.contains(r5)     // Catch:{ Exception -> 0x0220 }
            if (r5 == 0) goto L_0x0210
            if (r3 == 0) goto L_0x0210
            if (r7 != 0) goto L_0x0185
            java.lang.String r3 = r23.getName()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = " "
            java.lang.String r11 = "."
            java.lang.String r3 = r3.replace(r5, r11)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            boolean r3 = r9.contains(r3)     // Catch:{ Exception -> 0x0220 }
            if (r3 == 0) goto L_0x0210
        L_0x0185:
            boolean r3 = r9.contains(r2)     // Catch:{ Exception -> 0x0220 }
            if (r3 == 0) goto L_0x019d
            java.lang.String r3 = r23.getName()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            boolean r3 = r3.contains(r2)     // Catch:{ Exception -> 0x0220 }
            if (r3 == 0) goto L_0x0210
        L_0x019d:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0220 }
            r3.<init>()     // Catch:{ Exception -> 0x0220 }
            r3.append(r13)     // Catch:{ Exception -> 0x0220 }
            r3.append(r1)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = r17.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = r5.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            r3.append(r5)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0220 }
            boolean r3 = r10.contains(r3)     // Catch:{ Exception -> 0x0220 }
            if (r3 != 0) goto L_0x0210
            java.lang.String r3 = r16.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = "positive"
            boolean r3 = r3.contains(r5)     // Catch:{ Exception -> 0x0220 }
            if (r3 == 0) goto L_0x01ea
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0220 }
            r3.<init>()     // Catch:{ Exception -> 0x0220 }
            r3.append(r13)     // Catch:{ Exception -> 0x0220 }
            r3.append(r1)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = r17.trim()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = r5.toLowerCase()     // Catch:{ Exception -> 0x0220 }
            r3.append(r5)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x0220 }
            r10.add(r3)     // Catch:{ Exception -> 0x0220 }
        L_0x01ea:
            java.lang.String r3 = "/"
            boolean r3 = r8.startsWith(r3)     // Catch:{ Exception -> 0x0220 }
            if (r3 == 0) goto L_0x0203
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0220 }
            r3.<init>()     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = "https://subscene.com"
            r3.append(r5)     // Catch:{ Exception -> 0x0220 }
            r3.append(r8)     // Catch:{ Exception -> 0x0220 }
            java.lang.String r8 = r3.toString()     // Catch:{ Exception -> 0x0220 }
        L_0x0203:
            com.utils.Subtitle.SubtitleInfo r3 = new com.utils.Subtitle.SubtitleInfo     // Catch:{ Exception -> 0x0220 }
            java.lang.String r5 = r17.trim()     // Catch:{ Exception -> 0x0220 }
            r9 = -1
            r3.<init>(r13, r8, r5, r9)     // Catch:{ Exception -> 0x0220 }
            r6.add(r3)     // Catch:{ Exception -> 0x0220 }
        L_0x0210:
            int r15 = r15 + 1
            r3 = r18
            r11 = r19
            r5 = r21
            r9 = 1
            r13 = 2
            goto L_0x00e4
        L_0x021c:
            r0.onNext(r6)     // Catch:{ Exception -> 0x0220 }
            goto L_0x0228
        L_0x0220:
            java.util.ArrayList r1 = new java.util.ArrayList
            r1.<init>()
            r0.onNext(r1)
        L_0x0228:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.services.Subscene.a(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    private String a(String str) {
        return str.replace("Marvel's ", "");
    }
}
