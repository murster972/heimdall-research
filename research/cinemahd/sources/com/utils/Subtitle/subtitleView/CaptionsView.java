package com.utils.Subtitle.subtitleView;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import androidx.appcompat.widget.AppCompatTextView;
import com.google.android.exoplayer2.Player;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class CaptionsView extends AppCompatTextView implements Runnable {
    private static String e;

    /* renamed from: a  reason: collision with root package name */
    private Player f6544a;
    private TreeMap<Long, Line> b;
    private CMime c;
    private CaptionsViewLoadListener d;

    public enum CMime {
        SUBRIP,
        WEBVTT
    }

    public interface CaptionsViewLoadListener {
        void a(String str, int i);

        void a(Throwable th, String str, int i);
    }

    private enum TrackParseState {
        NEW_TRACK,
        PARSED_CUE,
        PARSED_TIME
    }

    public CaptionsView(Context context) {
        super(context);
    }

    private String a(long j) {
        String str = "";
        for (Map.Entry next : this.b.entrySet()) {
            if (j < ((Long) next.getKey()).longValue()) {
                break;
            } else if (j < ((Line) next.getValue()).b) {
                str = ((Line) next.getValue()).c;
            }
        }
        return str;
    }

    private static boolean b(String str) {
        if (str.isEmpty()) {
            return false;
        }
        for (int i = 0; i < str.length(); i++) {
            if (i == 0 && str.charAt(i) == '-') {
                if (str.length() == 1) {
                    return false;
                }
            } else if (Character.digit(str.charAt(i), 10) < 0) {
                return false;
            }
        }
        return true;
    }

    private static long c(String str) {
        try {
            String[] split = str.split(":");
            String[] split2 = split[2].split(",");
            return (Long.parseLong(split[0].trim()) * 60 * 60 * 1000) + (Long.parseLong(split[1].trim()) * 60 * 1000) + (Long.parseLong(split2[0].trim()) * 1000) + Long.parseLong(split2[1].trim());
        } catch (Exception unused) {
            return 0;
        }
    }

    private static long d(String str) {
        long parseLong;
        long parseLong2;
        long j;
        String[] split = str.split(":");
        if (split.length == 3) {
            String[] split2 = split[2].split("\\.");
            long parseLong3 = Long.parseLong(split[0].trim());
            long parseLong4 = Long.parseLong(split[1].trim());
            parseLong = Long.parseLong(split2[0].trim());
            parseLong2 = Long.parseLong(split2[1].trim());
            j = (parseLong3 * 60 * 60 * 1000) + (parseLong4 * 60 * 1000);
        } else {
            String[] split3 = split[1].split("\\.");
            long parseLong5 = Long.parseLong(split[0].trim());
            parseLong = Long.parseLong(split3[0].trim());
            parseLong2 = Long.parseLong(split3[1].trim());
            j = parseLong5 * 60 * 1000;
        }
        return j + (parseLong * 1000) + parseLong2;
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        postDelayed(this, 300);
        setShadowLayer(6.0f, 6.0f, 6.0f, -16777216);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeCallbacks(this);
    }

    public void run() {
        Player player = this.f6544a;
        if (!(player == null || this.b == null)) {
            long o = player.o() / 1000;
            setText(Html.fromHtml("" + a(this.f6544a.getCurrentPosition())));
        }
        postDelayed(this, 50);
    }

    public void setActivity(Activity activity) {
    }

    public void setCaptionsSource(List<File> list) {
        this.b = a(list);
    }

    public void setCaptionsViewLoadListener(CaptionsViewLoadListener captionsViewLoadListener) {
        this.d = captionsViewLoadListener;
    }

    public void setPlayer(Player player) {
        this.f6544a = player;
    }

    public CaptionsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public CaptionsView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public static class Line {

        /* renamed from: a  reason: collision with root package name */
        long f6546a;
        long b;
        String c;

        public Line(long j, long j2, String str) {
            this.f6546a = j;
            this.b = j2;
            this.c = str;
        }

        public void a(String str) {
            this.c = str;
        }

        public Line(long j, long j2) {
            this.f6546a = j;
            this.b = j2;
        }
    }

    public static TreeMap<Long, Line> a(InputStream inputStream, CMime cMime) throws IOException {
        if (cMime == CMime.SUBRIP) {
            return a(inputStream);
        }
        if (cMime == CMime.WEBVTT) {
            return b(inputStream);
        }
        return a(inputStream);
    }

    public static TreeMap<Long, Line> b(InputStream inputStream) throws IOException {
        LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream, "UTF-8"));
        TreeMap<Long, Line> treeMap = new TreeMap<>();
        lineNumberReader.readLine();
        lineNumberReader.readLine();
        while (true) {
            String readLine = lineNumberReader.readLine();
            if (readLine == null) {
                return treeMap;
            }
            String str = "";
            while (true) {
                String readLine2 = lineNumberReader.readLine();
                if (readLine2 == null || readLine2.trim().equals("")) {
                    String substring = str.substring(0, str.length() - 5);
                    String[] split = readLine.split(" --> ");
                } else {
                    str = str + readLine2 + "<br/>";
                }
            }
            String substring2 = str.substring(0, str.length() - 5);
            String[] split2 = readLine.split(" --> ");
            if (split2.length == 2) {
                long d2 = d(split2[0]);
                treeMap.put(Long.valueOf(d2), new Line(d2, d(split2[1]), substring2));
            }
        }
    }

    public static TreeMap<Long, Line> a(InputStream inputStream) throws IOException {
        LineNumberReader lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream, e));
        TreeMap<Long, Line> treeMap = new TreeMap<>();
        StringBuilder sb = new StringBuilder();
        TrackParseState trackParseState = TrackParseState.NEW_TRACK;
        Line line = null;
        int i = 0;
        while (true) {
            String readLine = lineNumberReader.readLine();
            if (readLine == null) {
                break;
            }
            System.out.println(readLine);
            i++;
            if (trackParseState == TrackParseState.NEW_TRACK) {
                if (!readLine.isEmpty()) {
                    if (b(readLine)) {
                        trackParseState = TrackParseState.PARSED_CUE;
                        if (line != null && sb.length() > 0) {
                            String sb2 = sb.toString();
                            line.a(sb2.substring(0, sb2.length() - 5));
                            a(treeMap, line);
                            sb.setLength(0);
                            line = null;
                        }
                    } else if (sb.length() > 0) {
                        sb.append(readLine);
                        sb.append("<br/>");
                    } else {
                        Log.w("SubtitleView", "No cue number found at line: " + i);
                    }
                }
            }
            if (trackParseState == TrackParseState.PARSED_CUE) {
                String[] split = readLine.split("-->");
                if (split.length == 2) {
                    line = new Line(c(split[0]), c(split[1]));
                    trackParseState = TrackParseState.PARSED_TIME;
                } else {
                    Log.w("SubtitleView", "No time-code found at line: " + i);
                }
            }
            if (trackParseState == TrackParseState.PARSED_TIME) {
                if (!readLine.isEmpty()) {
                    sb.append(readLine);
                    sb.append("<br/>");
                } else {
                    trackParseState = TrackParseState.NEW_TRACK;
                }
            }
        }
        if (line != null && sb.length() > 0) {
            String sb3 = sb.toString();
            line.a(sb3.substring(0, sb3.length() - 5));
            a(treeMap, line);
        }
        return treeMap;
    }

    private static void a(TreeMap<Long, Line> treeMap, Line line) {
        treeMap.put(Long.valueOf(line.f6546a), line);
    }

    private String a(String str) {
        try {
            return str.substring(str.lastIndexOf(".") + 1);
        } catch (Exception unused) {
            return "";
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x0082 A[SYNTHETIC, Splitter:B:36:0x0082] */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a3 A[Catch:{ Exception -> 0x00c4, all -> 0x00c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00ad A[Catch:{ Exception -> 0x00c4, all -> 0x00c2 }] */
    /* JADX WARNING: Removed duplicated region for block: B:69:0x00dd A[Catch:{ all -> 0x00f7 }] */
    /* JADX WARNING: Removed duplicated region for block: B:72:0x00eb A[SYNTHETIC, Splitter:B:72:0x00eb] */
    /* JADX WARNING: Removed duplicated region for block: B:80:0x00fd A[SYNTHETIC, Splitter:B:80:0x00fd] */
    /* JADX WARNING: Removed duplicated region for block: B:87:0x010c A[SYNTHETIC, Splitter:B:87:0x010c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.util.TreeMap<java.lang.Long, com.utils.Subtitle.subtitleView.CaptionsView.Line> a(java.util.List<java.io.File> r9) {
        /*
            r8 = this;
            java.lang.String r0 = "UTF-8"
            java.util.Iterator r9 = r9.iterator()
            boolean r1 = r9.hasNext()
            r2 = 0
            if (r1 == 0) goto L_0x0033
            java.lang.Object r9 = r9.next()
            java.io.File r9 = (java.io.File) r9
            java.lang.String r1 = r9.getName()
            java.lang.String r1 = r8.a((java.lang.String) r1)
            com.utils.Subtitle.subtitleView.CaptionsView$CMime r3 = r8.c
            com.utils.Subtitle.subtitleView.CaptionsView$CMime r4 = com.utils.Subtitle.subtitleView.CaptionsView.CMime.WEBVTT
            if (r3 != r4) goto L_0x002e
            java.lang.String r3 = "vtt"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x002e
            com.utils.Subtitle.subtitleView.CaptionsView$CMime r1 = com.utils.Subtitle.subtitleView.CaptionsView.CMime.WEBVTT
            r8.c = r1
            goto L_0x0034
        L_0x002e:
            com.utils.Subtitle.subtitleView.CaptionsView$CMime r1 = com.utils.Subtitle.subtitleView.CaptionsView.CMime.SUBRIP
            r8.c = r1
            goto L_0x0034
        L_0x0033:
            r9 = r2
        L_0x0034:
            java.io.FileInputStream r1 = new java.io.FileInputStream     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            r1.<init>(r9)     // Catch:{ Exception -> 0x0078, all -> 0x0073 }
            com.utils.UnicodeBOMInputStream r3 = new com.utils.UnicodeBOMInputStream     // Catch:{ Exception -> 0x0070, all -> 0x006c }
            r3.<init>(r1)     // Catch:{ Exception -> 0x0070, all -> 0x006c }
            java.lang.String r4 = com.utils.Utils.a((java.io.InputStream) r3)     // Catch:{ Exception -> 0x006a }
            e = r4     // Catch:{ Exception -> 0x006a }
            java.lang.String r4 = e     // Catch:{ Exception -> 0x006a }
            if (r4 != 0) goto L_0x0052
            com.utils.UnicodeBOMInputStream$BOM r4 = r3.s()     // Catch:{ Exception -> 0x006a }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x006a }
            e = r4     // Catch:{ Exception -> 0x006a }
        L_0x0052:
            java.lang.String r4 = e     // Catch:{ Exception -> 0x006a }
            java.lang.String r5 = "NONE"
            boolean r4 = r4.equals(r5)     // Catch:{ Exception -> 0x006a }
            if (r4 == 0) goto L_0x005e
            e = r0     // Catch:{ Exception -> 0x006a }
        L_0x005e:
            r1.close()     // Catch:{ IOException -> 0x0065 }
            r3.close()     // Catch:{ IOException -> 0x0065 }
            goto L_0x0088
        L_0x0065:
            r0 = move-exception
            r0.printStackTrace()
            goto L_0x0088
        L_0x006a:
            r4 = move-exception
            goto L_0x007b
        L_0x006c:
            r9 = move-exception
            r3 = r2
            goto L_0x010a
        L_0x0070:
            r4 = move-exception
            r3 = r2
            goto L_0x007b
        L_0x0073:
            r9 = move-exception
            r1 = r2
            r3 = r1
            goto L_0x010a
        L_0x0078:
            r4 = move-exception
            r1 = r2
            r3 = r1
        L_0x007b:
            e = r0     // Catch:{ all -> 0x0109 }
            r4.printStackTrace()     // Catch:{ all -> 0x0109 }
            if (r1 == 0) goto L_0x0088
            r1.close()     // Catch:{ IOException -> 0x0065 }
            r3.close()     // Catch:{ IOException -> 0x0065 }
        L_0x0088:
            r0 = 0
            java.io.FileInputStream r4 = new java.io.FileInputStream     // Catch:{ Exception -> 0x00d5, all -> 0x00d1 }
            r4.<init>(r9)     // Catch:{ Exception -> 0x00d5, all -> 0x00d1 }
            com.utils.UnicodeBOMInputStream r1 = new com.utils.UnicodeBOMInputStream     // Catch:{ Exception -> 0x00cb, all -> 0x00c9 }
            r1.<init>(r4)     // Catch:{ Exception -> 0x00cb, all -> 0x00c9 }
            com.utils.Subtitle.subtitleView.CaptionsView$CMime r3 = r8.c     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            java.util.TreeMap r3 = a((java.io.InputStream) r1, (com.utils.Subtitle.subtitleView.CaptionsView.CMime) r3)     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            int r5 = r3.size()     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            if (r5 <= 0) goto L_0x00ad
            com.utils.Subtitle.subtitleView.CaptionsView$CaptionsViewLoadListener r5 = r8.d     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            if (r5 == 0) goto L_0x00ad
            com.utils.Subtitle.subtitleView.CaptionsView$CaptionsViewLoadListener r5 = r8.d     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            java.lang.String r6 = r9.getAbsolutePath()     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            r5.a(r6, r0)     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            goto L_0x00b6
        L_0x00ad:
            com.utils.Subtitle.subtitleView.CaptionsView$CaptionsViewLoadListener r5 = r8.d     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            java.lang.String r6 = r9.getAbsolutePath()     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
            r5.a(r2, r6, r0)     // Catch:{ Exception -> 0x00c4, all -> 0x00c2 }
        L_0x00b6:
            r4.close()     // Catch:{ IOException -> 0x00bd }
            r1.close()     // Catch:{ IOException -> 0x00bd }
            goto L_0x00c1
        L_0x00bd:
            r9 = move-exception
            r9.printStackTrace()
        L_0x00c1:
            return r3
        L_0x00c2:
            r9 = move-exception
            goto L_0x00fb
        L_0x00c4:
            r3 = move-exception
            r7 = r4
            r4 = r1
            r1 = r7
            goto L_0x00d9
        L_0x00c9:
            r9 = move-exception
            goto L_0x00d3
        L_0x00cb:
            r1 = move-exception
            r7 = r3
            r3 = r1
            r1 = r4
            r4 = r7
            goto L_0x00d9
        L_0x00d1:
            r9 = move-exception
            r4 = r1
        L_0x00d3:
            r1 = r3
            goto L_0x00fb
        L_0x00d5:
            r4 = move-exception
            r7 = r4
            r4 = r3
            r3 = r7
        L_0x00d9:
            com.utils.Subtitle.subtitleView.CaptionsView$CaptionsViewLoadListener r5 = r8.d     // Catch:{ all -> 0x00f7 }
            if (r5 == 0) goto L_0x00e6
            com.utils.Subtitle.subtitleView.CaptionsView$CaptionsViewLoadListener r5 = r8.d     // Catch:{ all -> 0x00f7 }
            java.lang.String r9 = r9.getAbsolutePath()     // Catch:{ all -> 0x00f7 }
            r5.a(r3, r9, r0)     // Catch:{ all -> 0x00f7 }
        L_0x00e6:
            r3.printStackTrace()     // Catch:{ all -> 0x00f7 }
            if (r1 == 0) goto L_0x00f6
            r1.close()     // Catch:{ IOException -> 0x00f2 }
            r4.close()     // Catch:{ IOException -> 0x00f2 }
            goto L_0x00f6
        L_0x00f2:
            r9 = move-exception
            r9.printStackTrace()
        L_0x00f6:
            return r2
        L_0x00f7:
            r9 = move-exception
            r7 = r4
            r4 = r1
            r1 = r7
        L_0x00fb:
            if (r4 == 0) goto L_0x0108
            r4.close()     // Catch:{ IOException -> 0x0104 }
            r1.close()     // Catch:{ IOException -> 0x0104 }
            goto L_0x0108
        L_0x0104:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0108:
            throw r9
        L_0x0109:
            r9 = move-exception
        L_0x010a:
            if (r1 == 0) goto L_0x0117
            r1.close()     // Catch:{ IOException -> 0x0113 }
            r3.close()     // Catch:{ IOException -> 0x0113 }
            goto L_0x0117
        L_0x0113:
            r0 = move-exception
            r0.printStackTrace()
        L_0x0117:
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.subtitleView.CaptionsView.a(java.util.List):java.util.TreeMap");
    }
}
