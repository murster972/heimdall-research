package com.utils.Subtitle;

import android.app.Activity;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ExpandableListSubtitleAdapter extends BaseExpandableListAdapter {

    /* renamed from: a  reason: collision with root package name */
    private Activity f6530a;
    private Map<String, List<SubtitleInfo>> b;
    private List<String> c = new ArrayList();

    public ExpandableListSubtitleAdapter(Activity activity, Map<String, List<SubtitleInfo>> map) {
        this.f6530a = activity;
        this.b = map;
        Object[] array = map.keySet().toArray();
        for (int i = 0; i < map.size(); i++) {
            this.c.add(array[i].toString());
        }
    }

    public Object getChild(int i, int i2) {
        return this.b.get(this.c.get(i)).get(i2);
    }

    public long getChildId(int i, int i2) {
        return (long) i2;
    }

    public View getChildView(int i, int i2, boolean z, View view, ViewGroup viewGroup) {
        SubtitleInfo subtitleInfo = (SubtitleInfo) getChild(i, i2);
        LayoutInflater layoutInflater = this.f6530a.getLayoutInflater();
        if (view == null) {
            view = layoutInflater.inflate(R.layout.child_listusbtitle_item, (ViewGroup) null);
        }
        ((TextView) view.findViewById(R.id.subtitle_child_name)).setText(subtitleInfo.f6531a);
        return view;
    }

    public int getChildrenCount(int i) {
        return this.b.get(this.c.get(i)).size();
    }

    public Object getGroup(int i) {
        return this.c.get(i);
    }

    public int getGroupCount() {
        return this.c.size();
    }

    public long getGroupId(int i) {
        return (long) i;
    }

    public View getGroupView(int i, boolean z, View view, ViewGroup viewGroup) {
        String str = (String) getGroup(i);
        if (view == null) {
            view = ((LayoutInflater) this.f6530a.getSystemService("layout_inflater")).inflate(R.layout.group_subtitle_list_item, (ViewGroup) null);
        }
        TextView textView = (TextView) view.findViewById(R.id.groupSubtitleItem);
        textView.setTypeface((Typeface) null, 1);
        textView.setText(str);
        return view;
    }

    public boolean hasStableIds() {
        return true;
    }

    public boolean isChildSelectable(int i, int i2) {
        return true;
    }
}
