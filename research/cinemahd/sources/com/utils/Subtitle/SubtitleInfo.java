package com.utils.Subtitle;

import android.os.Parcel;
import android.os.Parcelable;

public class SubtitleInfo implements Parcelable {
    public static final Parcelable.Creator<SubtitleInfo> CREATOR = new Parcelable.Creator<SubtitleInfo>() {
        public SubtitleInfo createFromParcel(Parcel parcel) {
            return new SubtitleInfo(parcel);
        }

        public SubtitleInfo[] newArray(int i) {
            return new SubtitleInfo[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public String f6531a;
    public String b;
    public String c;
    public int d;

    public SubtitleInfo() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f6531a);
        parcel.writeString(this.b);
        parcel.writeString(this.c);
        parcel.writeInt(this.d);
    }

    public SubtitleInfo(String str, String str2, String str3, int i) {
        this.f6531a = str;
        this.b = str2;
        this.c = str3;
        this.d = i;
    }

    protected SubtitleInfo(Parcel parcel) {
        this.f6531a = parcel.readString();
        this.b = parcel.readString();
        this.c = parcel.readString();
        this.d = parcel.readInt();
    }
}
