package com.utils.Subtitle;

public class SubtitlesConverter {
    /* JADX WARNING: Removed duplicated region for block: B:49:0x00a2 A[SYNTHETIC, Splitter:B:49:0x00a2] */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00b1 A[SYNTHETIC, Splitter:B:56:0x00b1] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r5, com.utils.Subtitle.converter.TimedTextFileFormat r6) {
        /*
            java.lang.String r0 = ".ass"
            boolean r0 = r5.contains(r0)
            if (r0 == 0) goto L_0x000e
            com.utils.Subtitle.converter.FormatASS r0 = new com.utils.Subtitle.converter.FormatASS
            r0.<init>()
            goto L_0x003d
        L_0x000e:
            java.lang.String r0 = ".scc"
            boolean r0 = r5.contains(r0)
            if (r0 == 0) goto L_0x001c
            com.utils.Subtitle.converter.FormatSCC r0 = new com.utils.Subtitle.converter.FormatSCC
            r0.<init>()
            goto L_0x003d
        L_0x001c:
            java.lang.String r0 = ".ssa"
            boolean r0 = r5.contains(r0)
            if (r0 == 0) goto L_0x002a
            com.utils.Subtitle.converter.FormatASS r0 = new com.utils.Subtitle.converter.FormatASS
            r0.<init>()
            goto L_0x003d
        L_0x002a:
            java.lang.String r0 = ".ttml"
            boolean r0 = r5.contains(r0)
            if (r0 == 0) goto L_0x0038
            com.utils.Subtitle.converter.FormatSCC r0 = new com.utils.Subtitle.converter.FormatSCC
            r0.<init>()
            goto L_0x003d
        L_0x0038:
            com.utils.Subtitle.converter.FormatSRT r0 = new com.utils.Subtitle.converter.FormatSRT
            r0.<init>()
        L_0x003d:
            java.io.File r1 = new java.io.File
            r1.<init>(r5)
            boolean r2 = r1.exists()
            r3 = 0
            if (r2 != 0) goto L_0x004a
            return r3
        L_0x004a:
            java.lang.String r5 = com.utils.Utils.d((java.lang.String) r5)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ Exception -> 0x009a, all -> 0x0096 }
            r2.<init>(r1)     // Catch:{ Exception -> 0x009a, all -> 0x0096 }
            com.utils.UnicodeBOMInputStream r4 = new com.utils.UnicodeBOMInputStream     // Catch:{ Exception -> 0x0093, all -> 0x0090 }
            r4.<init>(r2)     // Catch:{ Exception -> 0x0093, all -> 0x0090 }
            java.lang.String r1 = r1.getName()     // Catch:{ Exception -> 0x008e }
            com.utils.Subtitle.converter.TimedTextObject r5 = r0.a(r1, r4, r5)     // Catch:{ Exception -> 0x008e }
            java.lang.String r0 = "Cinema HD"
            r5.d = r0     // Catch:{ Exception -> 0x008e }
            java.lang.Object r5 = r6.a(r5)     // Catch:{ Exception -> 0x008e }
            if (r5 == 0) goto L_0x0082
            boolean r6 = r5 instanceof java.lang.String[]     // Catch:{ Exception -> 0x008e }
            if (r6 == 0) goto L_0x0082
            java.lang.String r6 = "\n"
            java.lang.String[] r5 = (java.lang.String[]) r5     // Catch:{ Exception -> 0x008e }
            java.lang.String r5 = android.text.TextUtils.join(r6, r5)     // Catch:{ Exception -> 0x008e }
            r2.close()     // Catch:{ IOException -> 0x007d }
            r4.close()     // Catch:{ IOException -> 0x007d }
            goto L_0x0081
        L_0x007d:
            r6 = move-exception
            r6.printStackTrace()
        L_0x0081:
            return r5
        L_0x0082:
            r2.close()     // Catch:{ IOException -> 0x0089 }
            r4.close()     // Catch:{ IOException -> 0x0089 }
            goto L_0x008d
        L_0x0089:
            r5 = move-exception
            r5.printStackTrace()
        L_0x008d:
            return r3
        L_0x008e:
            r5 = move-exception
            goto L_0x009d
        L_0x0090:
            r5 = move-exception
            r4 = r3
            goto L_0x00af
        L_0x0093:
            r5 = move-exception
            r4 = r3
            goto L_0x009d
        L_0x0096:
            r5 = move-exception
            r2 = r3
            r4 = r2
            goto L_0x00af
        L_0x009a:
            r5 = move-exception
            r2 = r3
            r4 = r2
        L_0x009d:
            r5.printStackTrace()     // Catch:{ all -> 0x00ae }
            if (r2 == 0) goto L_0x00ad
            r2.close()     // Catch:{ IOException -> 0x00a9 }
            r4.close()     // Catch:{ IOException -> 0x00a9 }
            goto L_0x00ad
        L_0x00a9:
            r5 = move-exception
            r5.printStackTrace()
        L_0x00ad:
            return r3
        L_0x00ae:
            r5 = move-exception
        L_0x00af:
            if (r2 == 0) goto L_0x00bc
            r2.close()     // Catch:{ IOException -> 0x00b8 }
            r4.close()     // Catch:{ IOException -> 0x00b8 }
            goto L_0x00bc
        L_0x00b8:
            r6 = move-exception
            r6.printStackTrace()
        L_0x00bc:
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.SubtitlesConverter.a(java.lang.String, com.utils.Subtitle.converter.TimedTextFileFormat):java.lang.String");
    }
}
