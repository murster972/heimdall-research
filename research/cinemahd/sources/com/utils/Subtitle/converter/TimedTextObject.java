package com.utils.Subtitle.converter;

import java.util.Hashtable;
import java.util.TreeMap;

public class TimedTextObject {

    /* renamed from: a  reason: collision with root package name */
    public String f6535a = "";
    public String b = "";
    public String c = "";
    public String d = "";
    public String e = "";
    public String f = "";
    public Hashtable<String, Style> g = new Hashtable<>();
    public TreeMap<Integer, Caption> h;
    public String i;
    public boolean j = true;
    public int k = 0;
    public boolean l = false;

    protected TimedTextObject() {
        new Hashtable();
        this.h = new TreeMap<>();
        this.i = "List of non fatal errors produced during parsing:\n\n";
    }

    /* access modifiers changed from: protected */
    public void a() {
        Hashtable<String, Style> hashtable = new Hashtable<>();
        for (Caption next : this.h.values()) {
            Style style = next.f6532a;
            if (style != null) {
                String str = style.f6533a;
                if (!hashtable.containsKey(str)) {
                    hashtable.put(str, next.f6532a);
                }
            }
        }
        this.g = hashtable;
    }
}
