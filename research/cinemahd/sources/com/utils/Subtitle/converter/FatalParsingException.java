package com.utils.Subtitle.converter;

public class FatalParsingException extends Exception {
    private static final long serialVersionUID = 6798827566637277804L;
    private String parsingErrror;

    public FatalParsingException(String str) {
        super(str);
        this.parsingErrror = str;
    }

    public String getLocalizedMessage() {
        return this.parsingErrror;
    }
}
