package com.utils.Subtitle.converter;

import com.facebook.imageutils.JfifUtil;
import com.facebook.react.uimanager.ViewProps;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import org.joda.time.DateTimeConstants;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class FormatTTML implements TimedTextFileFormat {
    /* JADX WARNING: Removed duplicated region for block: B:116:0x02bb A[Catch:{ Exception -> 0x0387 }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x02fa A[SYNTHETIC, Splitter:B:124:0x02fa] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0360 A[Catch:{ Exception -> 0x0387 }] */
    /* JADX WARNING: Removed duplicated region for block: B:136:0x0363 A[Catch:{ Exception -> 0x0387 }] */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x037f A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.utils.Subtitle.converter.TimedTextObject a(java.lang.String r17, java.io.InputStream r18, java.lang.String r19) throws java.io.IOException, com.utils.Subtitle.converter.FatalParsingException {
        /*
            r16 = this;
            r1 = r16
            java.lang.String r0 = "style"
            java.lang.String r2 = ""
            com.utils.Subtitle.converter.TimedTextObject r3 = new com.utils.Subtitle.converter.TimedTextObject
            r3.<init>()
            r4 = r17
            r3.e = r4
            javax.xml.parsers.DocumentBuilderFactory r4 = javax.xml.parsers.DocumentBuilderFactory.newInstance()
            javax.xml.parsers.DocumentBuilder r4 = r4.newDocumentBuilder()     // Catch:{ Exception -> 0x0387 }
            r5 = r18
            org.w3c.dom.Document r4 = r4.parse(r5)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.Element r5 = r4.getDocumentElement()     // Catch:{ Exception -> 0x0387 }
            r5.normalize()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r5 = "ttm:title"
            org.w3c.dom.NodeList r5 = r4.getElementsByTagName(r5)     // Catch:{ Exception -> 0x0387 }
            r6 = 0
            org.w3c.dom.Node r5 = r5.item(r6)     // Catch:{ Exception -> 0x0387 }
            if (r5 == 0) goto L_0x0037
            java.lang.String r5 = r5.getTextContent()     // Catch:{ Exception -> 0x0387 }
            r3.f6535a = r5     // Catch:{ Exception -> 0x0387 }
        L_0x0037:
            java.lang.String r5 = "ttm:copyright"
            org.w3c.dom.NodeList r5 = r4.getElementsByTagName(r5)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.Node r5 = r5.item(r6)     // Catch:{ Exception -> 0x0387 }
            if (r5 == 0) goto L_0x0049
            java.lang.String r5 = r5.getTextContent()     // Catch:{ Exception -> 0x0387 }
            r3.c = r5     // Catch:{ Exception -> 0x0387 }
        L_0x0049:
            java.lang.String r5 = "ttm:desc"
            org.w3c.dom.NodeList r5 = r4.getElementsByTagName(r5)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.Node r5 = r5.item(r6)     // Catch:{ Exception -> 0x0387 }
            if (r5 == 0) goto L_0x005b
            java.lang.String r5 = r5.getTextContent()     // Catch:{ Exception -> 0x0387 }
            r3.b = r5     // Catch:{ Exception -> 0x0387 }
        L_0x005b:
            org.w3c.dom.NodeList r5 = r4.getElementsByTagName(r0)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r7 = "p"
            org.w3c.dom.NodeList r7 = r4.getElementsByTagName(r7)     // Catch:{ Exception -> 0x0387 }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0387 }
            r8.<init>()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = r3.i     // Catch:{ Exception -> 0x0387 }
            r8.append(r9)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = "Styling attributes are only recognized inside a style definition, to be referenced later in the captions.\n\n"
            r8.append(r9)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0387 }
            r3.i = r8     // Catch:{ Exception -> 0x0387 }
            r8 = 0
        L_0x007b:
            int r9 = r5.getLength()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r10 = "end"
            if (r8 >= r9) goto L_0x024e
            com.utils.Subtitle.converter.Style r9 = new com.utils.Subtitle.converter.Style     // Catch:{ Exception -> 0x0387 }
            java.lang.String r12 = com.utils.Subtitle.converter.Style.a()     // Catch:{ Exception -> 0x0387 }
            r9.<init>(r12)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.Node r12 = r5.item(r8)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.NamedNodeMap r12 = r12.getAttributes()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = "id"
            org.w3c.dom.Node r13 = r12.getNamedItem(r13)     // Catch:{ Exception -> 0x0387 }
            if (r13 == 0) goto L_0x00a2
            java.lang.String r13 = r13.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            r9.f6533a = r13     // Catch:{ Exception -> 0x0387 }
        L_0x00a2:
            java.lang.String r13 = "xml:id"
            org.w3c.dom.Node r13 = r12.getNamedItem(r13)     // Catch:{ Exception -> 0x0387 }
            if (r13 == 0) goto L_0x00b0
            java.lang.String r13 = r13.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            r9.f6533a = r13     // Catch:{ Exception -> 0x0387 }
        L_0x00b0:
            org.w3c.dom.Node r13 = r12.getNamedItem(r0)     // Catch:{ Exception -> 0x0387 }
            if (r13 == 0) goto L_0x00d6
            java.util.Hashtable<java.lang.String, com.utils.Subtitle.converter.Style> r14 = r3.g     // Catch:{ Exception -> 0x0387 }
            java.lang.String r15 = r13.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            boolean r14 = r14.containsKey(r15)     // Catch:{ Exception -> 0x0387 }
            if (r14 == 0) goto L_0x00d6
            com.utils.Subtitle.converter.Style r14 = new com.utils.Subtitle.converter.Style     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = r9.f6533a     // Catch:{ Exception -> 0x0387 }
            java.util.Hashtable<java.lang.String, com.utils.Subtitle.converter.Style> r15 = r3.g     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = r13.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.Object r13 = r15.get(r13)     // Catch:{ Exception -> 0x0387 }
            com.utils.Subtitle.converter.Style r13 = (com.utils.Subtitle.converter.Style) r13     // Catch:{ Exception -> 0x0387 }
            r14.<init>(r9, r13)     // Catch:{ Exception -> 0x0387 }
            goto L_0x00d7
        L_0x00d6:
            r14 = r9
        L_0x00d7:
            java.lang.String r9 = "tts:backgroundColor"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x00e9
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = r1.a(r9, r3)     // Catch:{ Exception -> 0x0387 }
            r14.e = r9     // Catch:{ Exception -> 0x0387 }
        L_0x00e9:
            java.lang.String r9 = "tts:color"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x00fb
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = r1.a(r9, r3)     // Catch:{ Exception -> 0x0387 }
            r14.d = r9     // Catch:{ Exception -> 0x0387 }
        L_0x00fb:
            java.lang.String r9 = "tts:fontFamily"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x0109
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            r14.b = r9     // Catch:{ Exception -> 0x0387 }
        L_0x0109:
            java.lang.String r9 = "tts:fontSize"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x0117
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            r14.c = r9     // Catch:{ Exception -> 0x0387 }
        L_0x0117:
            java.lang.String r9 = "tts:fontStyle"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = "normal"
            if (r9 == 0) goto L_0x014a
            java.lang.String r15 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r11 = "italic"
            boolean r11 = r15.equalsIgnoreCase(r11)     // Catch:{ Exception -> 0x0387 }
            if (r11 != 0) goto L_0x0147
            java.lang.String r11 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r15 = "oblique"
            boolean r11 = r11.equalsIgnoreCase(r15)     // Catch:{ Exception -> 0x0387 }
            if (r11 == 0) goto L_0x013a
            goto L_0x0147
        L_0x013a:
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            boolean r9 = r9.equalsIgnoreCase(r13)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x014a
            r14.g = r6     // Catch:{ Exception -> 0x0387 }
            goto L_0x014a
        L_0x0147:
            r9 = 1
            r14.g = r9     // Catch:{ Exception -> 0x0387 }
        L_0x014a:
            java.lang.String r9 = "tts:fontWeight"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x016e
            java.lang.String r11 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r15 = "bold"
            boolean r11 = r11.equalsIgnoreCase(r15)     // Catch:{ Exception -> 0x0387 }
            if (r11 == 0) goto L_0x0162
            r11 = 1
            r14.h = r11     // Catch:{ Exception -> 0x0387 }
            goto L_0x016e
        L_0x0162:
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            boolean r9 = r9.equalsIgnoreCase(r13)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x016e
            r14.h = r6     // Catch:{ Exception -> 0x0387 }
        L_0x016e:
            java.lang.String r9 = "tts:opacity"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x01dd
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ NumberFormatException -> 0x01dd }
            float r9 = java.lang.Float.parseFloat(r9)     // Catch:{ NumberFormatException -> 0x01dd }
            r11 = 0
            r13 = 1065353216(0x3f800000, float:1.0)
            int r15 = (r9 > r13 ? 1 : (r9 == r13 ? 0 : -1))
            if (r15 <= 0) goto L_0x0186
            goto L_0x018d
        L_0x0186:
            int r13 = (r9 > r11 ? 1 : (r9 == r11 ? 0 : -1))
            if (r13 >= 0) goto L_0x018c
            r13 = 0
            goto L_0x018d
        L_0x018c:
            r13 = r9
        L_0x018d:
            r9 = 1132396544(0x437f0000, float:255.0)
            float r13 = r13 * r9
            int r9 = (int) r13     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r9 = java.lang.Integer.toHexString(r9)     // Catch:{ NumberFormatException -> 0x01dd }
            int r11 = r9.length()     // Catch:{ NumberFormatException -> 0x01dd }
            r13 = 2
            if (r11 >= r13) goto L_0x01ae
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x01dd }
            r11.<init>()     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r13 = "0"
            r11.append(r13)     // Catch:{ NumberFormatException -> 0x01dd }
            r11.append(r9)     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r9 = r11.toString()     // Catch:{ NumberFormatException -> 0x01dd }
        L_0x01ae:
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x01dd }
            r11.<init>()     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r13 = r14.d     // Catch:{ NumberFormatException -> 0x01dd }
            r15 = 6
            java.lang.String r13 = r13.substring(r6, r15)     // Catch:{ NumberFormatException -> 0x01dd }
            r11.append(r13)     // Catch:{ NumberFormatException -> 0x01dd }
            r11.append(r9)     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r11 = r11.toString()     // Catch:{ NumberFormatException -> 0x01dd }
            r14.d = r11     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NumberFormatException -> 0x01dd }
            r11.<init>()     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r13 = r14.e     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r13 = r13.substring(r6, r15)     // Catch:{ NumberFormatException -> 0x01dd }
            r11.append(r13)     // Catch:{ NumberFormatException -> 0x01dd }
            r11.append(r9)     // Catch:{ NumberFormatException -> 0x01dd }
            java.lang.String r9 = r11.toString()     // Catch:{ NumberFormatException -> 0x01dd }
            r14.e = r9     // Catch:{ NumberFormatException -> 0x01dd }
        L_0x01dd:
            java.lang.String r9 = "tts:textAlign"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x021d
            java.lang.String r11 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = "left"
            boolean r11 = r11.equalsIgnoreCase(r13)     // Catch:{ Exception -> 0x0387 }
            if (r11 != 0) goto L_0x0219
            java.lang.String r11 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = "start"
            boolean r11 = r11.equalsIgnoreCase(r13)     // Catch:{ Exception -> 0x0387 }
            if (r11 == 0) goto L_0x01fe
            goto L_0x0219
        L_0x01fe:
            java.lang.String r11 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = "right"
            boolean r11 = r11.equalsIgnoreCase(r13)     // Catch:{ Exception -> 0x0387 }
            if (r11 != 0) goto L_0x0214
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            boolean r9 = r9.equalsIgnoreCase(r10)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x021d
        L_0x0214:
            java.lang.String r9 = "bottom-right"
            r14.f = r9     // Catch:{ Exception -> 0x0387 }
            goto L_0x021d
        L_0x0219:
            java.lang.String r9 = "bottom-left"
            r14.f = r9     // Catch:{ Exception -> 0x0387 }
        L_0x021d:
            java.lang.String r9 = "tts:textDecoration"
            org.w3c.dom.Node r9 = r12.getNamedItem(r9)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x0243
            java.lang.String r10 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r11 = "underline"
            boolean r10 = r10.equalsIgnoreCase(r11)     // Catch:{ Exception -> 0x0387 }
            if (r10 == 0) goto L_0x0235
            r10 = 1
            r14.i = r10     // Catch:{ Exception -> 0x0387 }
            goto L_0x0243
        L_0x0235:
            java.lang.String r9 = r9.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r10 = "noUnderline"
            boolean r9 = r9.equalsIgnoreCase(r10)     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x0243
            r14.i = r6     // Catch:{ Exception -> 0x0387 }
        L_0x0243:
            java.util.Hashtable<java.lang.String, com.utils.Subtitle.converter.Style> r9 = r3.g     // Catch:{ Exception -> 0x0387 }
            java.lang.String r10 = r14.f6533a     // Catch:{ Exception -> 0x0387 }
            r9.put(r10, r14)     // Catch:{ Exception -> 0x0387 }
            int r8 = r8 + 1
            goto L_0x007b
        L_0x024e:
            r5 = 0
        L_0x024f:
            int r8 = r7.getLength()     // Catch:{ Exception -> 0x0387 }
            if (r5 >= r8) goto L_0x0383
            com.utils.Subtitle.converter.Caption r8 = new com.utils.Subtitle.converter.Caption     // Catch:{ Exception -> 0x0387 }
            r8.<init>()     // Catch:{ Exception -> 0x0387 }
            r8.d = r2     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.Node r9 = r7.item(r5)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.NamedNodeMap r11 = r9.getAttributes()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r12 = "begin"
            org.w3c.dom.Node r12 = r11.getNamedItem(r12)     // Catch:{ Exception -> 0x0387 }
            com.utils.Subtitle.converter.Time r13 = new com.utils.Subtitle.converter.Time     // Catch:{ Exception -> 0x0387 }
            r13.<init>(r2, r2)     // Catch:{ Exception -> 0x0387 }
            r8.b = r13     // Catch:{ Exception -> 0x0387 }
            com.utils.Subtitle.converter.Time r13 = new com.utils.Subtitle.converter.Time     // Catch:{ Exception -> 0x0387 }
            r13.<init>(r2, r2)     // Catch:{ Exception -> 0x0387 }
            r8.c = r13     // Catch:{ Exception -> 0x0387 }
            if (r12 == 0) goto L_0x0286
            com.utils.Subtitle.converter.Time r13 = r8.b     // Catch:{ Exception -> 0x0387 }
            java.lang.String r12 = r12.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            int r12 = r1.a((java.lang.String) r12, (com.utils.Subtitle.converter.TimedTextObject) r3, (org.w3c.dom.Document) r4)     // Catch:{ Exception -> 0x0387 }
            r13.f6534a = r12     // Catch:{ Exception -> 0x0387 }
        L_0x0286:
            org.w3c.dom.Node r12 = r11.getNamedItem(r10)     // Catch:{ Exception -> 0x0387 }
            if (r12 == 0) goto L_0x0299
            com.utils.Subtitle.converter.Time r13 = r8.c     // Catch:{ Exception -> 0x0387 }
            java.lang.String r12 = r12.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            int r12 = r1.a((java.lang.String) r12, (com.utils.Subtitle.converter.TimedTextObject) r3, (org.w3c.dom.Document) r4)     // Catch:{ Exception -> 0x0387 }
            r13.f6534a = r12     // Catch:{ Exception -> 0x0387 }
            goto L_0x02b2
        L_0x0299:
            java.lang.String r12 = "dur"
            org.w3c.dom.Node r12 = r11.getNamedItem(r12)     // Catch:{ Exception -> 0x0387 }
            if (r12 == 0) goto L_0x02b4
            com.utils.Subtitle.converter.Time r13 = r8.c     // Catch:{ Exception -> 0x0387 }
            com.utils.Subtitle.converter.Time r14 = r8.b     // Catch:{ Exception -> 0x0387 }
            int r14 = r14.f6534a     // Catch:{ Exception -> 0x0387 }
            java.lang.String r12 = r12.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            int r12 = r1.a((java.lang.String) r12, (com.utils.Subtitle.converter.TimedTextObject) r3, (org.w3c.dom.Document) r4)     // Catch:{ Exception -> 0x0387 }
            int r14 = r14 + r12
            r13.f6534a = r14     // Catch:{ Exception -> 0x0387 }
        L_0x02b2:
            r12 = 1
            goto L_0x02b5
        L_0x02b4:
            r12 = 0
        L_0x02b5:
            org.w3c.dom.Node r11 = r11.getNamedItem(r0)     // Catch:{ Exception -> 0x0387 }
            if (r11 == 0) goto L_0x02ed
            java.util.Hashtable<java.lang.String, com.utils.Subtitle.converter.Style> r13 = r3.g     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = r11.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            java.lang.Object r13 = r13.get(r14)     // Catch:{ Exception -> 0x0387 }
            com.utils.Subtitle.converter.Style r13 = (com.utils.Subtitle.converter.Style) r13     // Catch:{ Exception -> 0x0387 }
            if (r13 == 0) goto L_0x02cc
            r8.f6532a = r13     // Catch:{ Exception -> 0x0387 }
            goto L_0x02ed
        L_0x02cc:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0387 }
            r13.<init>()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = r3.i     // Catch:{ Exception -> 0x0387 }
            r13.append(r14)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = "unrecoginzed style referenced: "
            r13.append(r14)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r11 = r11.getNodeValue()     // Catch:{ Exception -> 0x0387 }
            r13.append(r11)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r11 = "\n\n"
            r13.append(r11)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r11 = r13.toString()     // Catch:{ Exception -> 0x0387 }
            r3.i = r11     // Catch:{ Exception -> 0x0387 }
        L_0x02ed:
            org.w3c.dom.NodeList r9 = r9.getChildNodes()     // Catch:{ Exception -> 0x0387 }
            r11 = 0
        L_0x02f2:
            int r13 = r9.getLength()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = "<br />"
            if (r11 >= r13) goto L_0x0350
            org.w3c.dom.Node r13 = r9.item(r11)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = r13.getNodeName()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r15 = "#text"
            boolean r13 = r13.equals(r15)     // Catch:{ Exception -> 0x0387 }
            if (r13 == 0) goto L_0x032a
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0387 }
            r13.<init>()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = r8.d     // Catch:{ Exception -> 0x0387 }
            r13.append(r14)     // Catch:{ Exception -> 0x0387 }
            org.w3c.dom.Node r14 = r9.item(r11)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = r14.getTextContent()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r14 = r14.trim()     // Catch:{ Exception -> 0x0387 }
            r13.append(r14)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0387 }
            r8.d = r13     // Catch:{ Exception -> 0x0387 }
            goto L_0x034d
        L_0x032a:
            org.w3c.dom.Node r13 = r9.item(r11)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = r13.getNodeName()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r15 = "br"
            boolean r13 = r13.equals(r15)     // Catch:{ Exception -> 0x0387 }
            if (r13 == 0) goto L_0x034d
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0387 }
            r13.<init>()     // Catch:{ Exception -> 0x0387 }
            java.lang.String r15 = r8.d     // Catch:{ Exception -> 0x0387 }
            r13.append(r15)     // Catch:{ Exception -> 0x0387 }
            r13.append(r14)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0387 }
            r8.d = r13     // Catch:{ Exception -> 0x0387 }
        L_0x034d:
            int r11 = r11 + 1
            goto L_0x02f2
        L_0x0350:
            java.lang.String r9 = r8.d     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = r9.replaceAll(r14, r2)     // Catch:{ Exception -> 0x0387 }
            java.lang.String r9 = r9.trim()     // Catch:{ Exception -> 0x0387 }
            boolean r9 = r9.isEmpty()     // Catch:{ Exception -> 0x0387 }
            if (r9 == 0) goto L_0x0361
            r12 = 0
        L_0x0361:
            if (r12 == 0) goto L_0x037f
            com.utils.Subtitle.converter.Time r9 = r8.b     // Catch:{ Exception -> 0x0387 }
            int r9 = r9.f6534a     // Catch:{ Exception -> 0x0387 }
        L_0x0367:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r11 = r3.h     // Catch:{ Exception -> 0x0387 }
            java.lang.Integer r12 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0387 }
            boolean r11 = r11.containsKey(r12)     // Catch:{ Exception -> 0x0387 }
            if (r11 == 0) goto L_0x0376
            int r9 = r9 + 1
            goto L_0x0367
        L_0x0376:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r11 = r3.h     // Catch:{ Exception -> 0x0387 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x0387 }
            r11.put(r9, r8)     // Catch:{ Exception -> 0x0387 }
        L_0x037f:
            int r5 = r5 + 1
            goto L_0x024f
        L_0x0383:
            r0 = 1
            r3.l = r0
            return r3
        L_0x0387:
            r0 = move-exception
            r0.printStackTrace()
            com.utils.Subtitle.converter.FatalParsingException r2 = new com.utils.Subtitle.converter.FatalParsingException
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Error during parsing: "
            r3.append(r4)
            java.lang.String r0 = r0.getMessage()
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r2.<init>(r0)
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.converter.FormatTTML.a(java.lang.String, java.io.InputStream, java.lang.String):com.utils.Subtitle.converter.TimedTextObject");
    }

    public String[] a(TimedTextObject timedTextObject) {
        String str;
        int i;
        String str2;
        if (!timedTextObject.l) {
            return null;
        }
        ArrayList arrayList = new ArrayList(timedTextObject.g.size() + 30 + timedTextObject.h.size());
        arrayList.add(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        arrayList.add(1, "<tt xml:lang=\"" + timedTextObject.f + "\" xmlns=\"http://www.w3.org/ns/ttml\" xmlns:tts=\"http://www.w3.org/ns/ttml#styling\">");
        arrayList.add(2, "\t<head>");
        arrayList.add(3, "\t\t<metadata xmlns:ttm=\"http://www.w3.org/ns/ttml#metadata\">");
        String str3 = timedTextObject.f6535a;
        if (str3 == null || str3.isEmpty()) {
            str = timedTextObject.e;
        } else {
            str = timedTextObject.f6535a;
        }
        arrayList.add(4, "\t\t\t<ttm:title>" + str + "</ttm:title>");
        String str4 = timedTextObject.c;
        if (str4 == null || str4.isEmpty()) {
            i = 5;
        } else {
            i = 6;
            arrayList.add(5, "\t\t\t<ttm:copyright>" + timedTextObject.c + "</ttm:copyright>");
        }
        String str5 = "Converted by the Online Subtitle Converter developed by J. David Requejo\n";
        String str6 = timedTextObject.d;
        if (str6 != null && !str6.isEmpty()) {
            str5 = str5 + "\n Original file by: " + timedTextObject.d + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
        }
        String str7 = timedTextObject.b;
        if (str7 != null && !str7.isEmpty()) {
            str5 = str5 + timedTextObject.b + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
        }
        int i2 = i + 1;
        arrayList.add(i, "\t\t\t<ttm:desc>" + str5 + "\t\t\t</ttm:desc>");
        int i3 = i2 + 1;
        arrayList.add(i2, "\t\t</metadata>");
        int i4 = i3 + 1;
        arrayList.add(i3, "\t\t<styling>");
        for (Style next : timedTextObject.g.values()) {
            String str8 = "\t\t\t<style xml:id=\"" + next.f6533a + "\"";
            if (next.d != null) {
                str8 = str8 + " tts:color=\"#" + next.d + "\"";
            }
            if (next.e != null) {
                str8 = str8 + " tts:backgroundColor=\"#" + next.e + "\"";
            }
            if (next.b != null) {
                str8 = str8 + " tts:fontFamily=\"" + next.b + "\"";
            }
            if (next.c != null) {
                str8 = str8 + " tts:fontSize=\"" + next.c + "\"";
            }
            if (next.g) {
                str8 = str8 + " tts:fontStyle=\"italic\"";
            }
            if (next.h) {
                str8 = str8 + " tts:fontWeight=\"bold\"";
            }
            String str9 = str8 + " tts:textAlign=\"";
            if (next.f.contains(ViewProps.LEFT)) {
                str2 = str9 + "left\"";
            } else if (next.f.contains(ViewProps.RIGHT)) {
                str2 = str9 + "rigth\"";
            } else {
                str2 = str9 + "center\"";
            }
            if (next.i) {
                str2 = str2 + " tts:textDecoration=\"underline\"";
            }
            arrayList.add(i4, str2 + " />");
            i4++;
        }
        int i5 = i4 + 1;
        arrayList.add(i4, "\t\t</styling>");
        int i6 = i5 + 1;
        arrayList.add(i5, "\t</head>");
        int i7 = i6 + 1;
        arrayList.add(i6, "\t<body>");
        int i8 = i7 + 1;
        arrayList.add(i7, "\t\t<div>");
        for (Caption next2 : timedTextObject.h.values()) {
            String str10 = ("\t\t\t<p begin=\"" + next2.b.a("hh:mm:ss,ms").replace(',', '.') + "\"") + " end=\"" + next2.c.a("hh:mm:ss,ms").replace(',', '.') + "\"";
            if (next2.f6532a != null) {
                str10 = str10 + " style=\"" + next2.f6532a.f6533a + "\"";
            }
            arrayList.add(i8, str10 + " >" + next2.d + "</p>\n");
            i8++;
        }
        int i9 = i8 + 1;
        arrayList.add(i8, "\t\t</div>");
        int i10 = i9 + 1;
        arrayList.add(i9, "\t</body>");
        arrayList.add(i10, "</tt>");
        arrayList.add(i10 + 1, "");
        String[] strArr = new String[arrayList.size()];
        for (int i11 = 0; i11 < strArr.length; i11++) {
            strArr[i11] = (String) arrayList.get(i11);
        }
        return strArr;
    }

    private String a(String str, TimedTextObject timedTextObject) {
        if (str.startsWith("#")) {
            if (str.length() == 7) {
                return str.substring(1) + "ff";
            } else if (str.length() == 9) {
                return str.substring(1);
            } else {
                timedTextObject.i += "Unrecoginzed format: " + str + "\n\n";
                return "ffffffff";
            }
        } else if (str.startsWith("rgb")) {
            boolean startsWith = str.startsWith("rgba");
            try {
                String[] split = str.split("(")[1].split(",");
                int i = JfifUtil.MARKER_FIRST_BYTE;
                int parseInt = Integer.parseInt(split[0]);
                int parseInt2 = Integer.parseInt(split[1]);
                int parseInt3 = Integer.parseInt(split[2].substring(0, 2));
                if (startsWith) {
                    i = Integer.parseInt(split[3].substring(0, 2));
                }
                split[0] = Integer.toHexString(parseInt);
                split[1] = Integer.toHexString(parseInt2);
                split[2] = Integer.toHexString(parseInt3);
                if (startsWith) {
                    split[2] = Integer.toHexString(i);
                }
                String str2 = "";
                for (int i2 = 0; i2 < split.length; i2++) {
                    if (split[i2].length() < 2) {
                        split[i2] = "0" + split[i2];
                    }
                    str2 = str2 + split[i2];
                }
                if (!startsWith) {
                    str2 = str2 + "ff";
                }
                return str2;
            } catch (Exception unused) {
                timedTextObject.i += "Unrecoginzed color: " + str + "\n\n";
                return "ffffffff";
            }
        } else {
            String a2 = Style.a(MediationMetaData.KEY_NAME, str);
            if (a2 != null && !a2.isEmpty()) {
                return a2;
            }
            timedTextObject.i += "Unrecoginzed color: " + str + "\n\n";
            return "ffffffff";
        }
    }

    private int a(String str, TimedTextObject timedTextObject, Document document) {
        Node item;
        double d;
        if (str.contains(":")) {
            String[] split = str.split(":");
            if (split.length == 3) {
                return (Integer.parseInt(split[0]) * DateTimeConstants.MILLIS_PER_HOUR) + (Integer.parseInt(split[1]) * DateTimeConstants.MILLIS_PER_MINUTE) + ((int) (Float.parseFloat(split[2]) * 1000.0f));
            } else if (split.length != 4) {
                return 0;
            } else {
                int i = 25;
                Node item2 = document.getElementsByTagName("ttp:frameRate").item(0);
                if (item2 != null) {
                    try {
                        i = Integer.parseInt(item2.getNodeValue());
                    } catch (NumberFormatException unused) {
                    }
                }
                return (Integer.parseInt(split[0]) * DateTimeConstants.MILLIS_PER_HOUR) + (Integer.parseInt(split[1]) * DateTimeConstants.MILLIS_PER_MINUTE) + (Integer.parseInt(split[2]) * 1000) + ((int) ((Float.parseFloat(split[3]) * 1000.0f) / ((float) i)));
            }
        } else {
            String substring = str.substring(str.length() - 1);
            try {
                double parseDouble = Double.parseDouble(str.substring(0, str.length() - 1).replace(',', '.').trim());
                if (substring.equalsIgnoreCase("h")) {
                    d = 3600000.0d;
                } else if (substring.equalsIgnoreCase("m")) {
                    d = 60000.0d;
                } else {
                    if (substring.equalsIgnoreCase("s")) {
                        parseDouble *= 1000.0d;
                    } else if (!substring.equalsIgnoreCase("ms")) {
                        if (substring.equalsIgnoreCase("f")) {
                            Node item3 = document.getElementsByTagName("ttp:frameRate").item(0);
                            if (item3 != null) {
                                return (int) ((parseDouble * 1000.0d) / ((double) Integer.parseInt(item3.getNodeValue())));
                            }
                            return 0;
                        } else if (!substring.equalsIgnoreCase("t") || (item = document.getElementsByTagName("ttp:tickRate").item(0)) == null) {
                            return 0;
                        } else {
                            parseDouble = (parseDouble * 1000.0d) / ((double) Integer.parseInt(item.getNodeValue()));
                        }
                    }
                    return (int) parseDouble;
                }
                parseDouble *= d;
                return (int) parseDouble;
            } catch (NumberFormatException unused2) {
                return 0;
            }
        }
    }
}
