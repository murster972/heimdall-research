package com.utils.Subtitle.converter;

import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import okhttp3.internal.ws.WebSocketProtocol;

public class FormatSCC implements TimedTextFileFormat {
    private String a(int i) {
        switch (i) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
                return "�";
            case 7:
                return "♪";
            case 8:
                return "�";
            case 9:
                return " ";
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
                return "�";
            default:
                return "";
        }
    }

    private void a(String str, int i) {
    }

    private void b(TimedTextObject timedTextObject) {
        Style style = new Style("white");
        timedTextObject.g.put(style.f6533a, style);
        Style style2 = new Style("whiteU", style);
        style2.i = true;
        timedTextObject.g.put(style2.f6533a, style2);
        Style style3 = new Style("whiteUI", style2);
        style3.g = true;
        timedTextObject.g.put(style3.f6533a, style3);
        Style style4 = new Style("whiteI", style3);
        style4.i = false;
        timedTextObject.g.put(style4.f6533a, style4);
        Style style5 = new Style("green");
        style5.d = Style.a(MediationMetaData.KEY_NAME, "green");
        timedTextObject.g.put(style5.f6533a, style5);
        Style style6 = new Style("greenU", style5);
        style6.i = true;
        timedTextObject.g.put(style6.f6533a, style6);
        Style style7 = new Style("greenUI", style6);
        style7.g = true;
        timedTextObject.g.put(style7.f6533a, style7);
        Style style8 = new Style("greenI", style7);
        style8.i = false;
        timedTextObject.g.put(style8.f6533a, style8);
        Style style9 = new Style("blue");
        style9.d = Style.a(MediationMetaData.KEY_NAME, "blue");
        timedTextObject.g.put(style9.f6533a, style9);
        Style style10 = new Style("blueU", style9);
        style10.i = true;
        timedTextObject.g.put(style10.f6533a, style10);
        Style style11 = new Style("blueUI", style10);
        style11.g = true;
        timedTextObject.g.put(style11.f6533a, style11);
        Style style12 = new Style("blueI", style11);
        style12.i = false;
        timedTextObject.g.put(style12.f6533a, style12);
        Style style13 = new Style("cyan");
        style13.d = Style.a(MediationMetaData.KEY_NAME, "cyan");
        timedTextObject.g.put(style13.f6533a, style13);
        Style style14 = new Style("cyanU", style13);
        style14.i = true;
        timedTextObject.g.put(style14.f6533a, style14);
        Style style15 = new Style("cyanUI", style14);
        style15.g = true;
        timedTextObject.g.put(style15.f6533a, style15);
        Style style16 = new Style("cyanI", style15);
        style16.i = false;
        timedTextObject.g.put(style16.f6533a, style16);
        Style style17 = new Style("red");
        style17.d = Style.a(MediationMetaData.KEY_NAME, "red");
        timedTextObject.g.put(style17.f6533a, style17);
        Style style18 = new Style("redU", style17);
        style18.i = true;
        timedTextObject.g.put(style18.f6533a, style18);
        Style style19 = new Style("redUI", style18);
        style19.g = true;
        timedTextObject.g.put(style19.f6533a, style19);
        Style style20 = new Style("redI", style19);
        style20.i = false;
        timedTextObject.g.put(style20.f6533a, style20);
        Style style21 = new Style("yellow");
        style21.d = Style.a(MediationMetaData.KEY_NAME, "yellow");
        timedTextObject.g.put(style21.f6533a, style21);
        Style style22 = new Style("yellowU", style21);
        style22.i = true;
        timedTextObject.g.put(style22.f6533a, style22);
        Style style23 = new Style("yellowUI", style22);
        style23.g = true;
        timedTextObject.g.put(style23.f6533a, style23);
        Style style24 = new Style("yellowI", style23);
        style24.i = false;
        timedTextObject.g.put(style24.f6533a, style24);
        Style style25 = new Style("magenta");
        style25.d = Style.a(MediationMetaData.KEY_NAME, "magenta");
        timedTextObject.g.put(style25.f6533a, style25);
        Style style26 = new Style("magentaU", style25);
        style26.i = true;
        timedTextObject.g.put(style26.f6533a, style26);
        Style style27 = new Style("magentaUI", style26);
        style27.g = true;
        timedTextObject.g.put(style27.f6533a, style27);
        Style style28 = new Style("magentaI", style27);
        style28.i = false;
        timedTextObject.g.put(style28.f6533a, style28);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(6:151|150|169|170|171|172) */
    /* JADX WARNING: Code restructure failed: missing block: B:121:0x02b9, code lost:
        r14 = r3;
        r13 = r5;
        r10 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:122:0x02bd, code lost:
        r14 = r3;
        r10 = r11;
        r13 = "yellow";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:123:0x02c2, code lost:
        r14 = r3;
        r10 = r11;
        r13 = "red";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x02c7, code lost:
        r14 = r3;
        r10 = r11;
        r13 = "cyan";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02cc, code lost:
        r14 = r3;
        r10 = r11;
        r13 = "blue";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:126:0x02d1, code lost:
        r14 = r3;
        r10 = r11;
        r13 = "green";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:166:0x0396, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:173:0x03bd, code lost:
        r28.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:174:0x03c0, code lost:
        throw r0;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:169:0x0399 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.utils.Subtitle.converter.TimedTextObject a(java.lang.String r27, java.io.InputStream r28, java.lang.String r29) throws java.io.IOException, com.utils.Subtitle.converter.FatalParsingException {
        /*
            r26 = this;
            r1 = r26
            r0 = r27
            com.utils.Subtitle.converter.TimedTextObject r2 = new com.utils.Subtitle.converter.TimedTextObject
            r2.<init>()
            java.io.BufferedReader r3 = new java.io.BufferedReader
            java.io.InputStreamReader r4 = new java.io.InputStreamReader
            r5 = r28
            r6 = r29
            r4.<init>(r5, r6)
            r3.<init>(r4)
            r2.e = r0
            r2.f6535a = r0
            java.lang.String r4 = r3.readLine()     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r4 = r4.trim()     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r6 = "Scenarist_SCC V1.0"
            boolean r4 = r4.equalsIgnoreCase(r6)     // Catch:{ NullPointerException -> 0x0398 }
            if (r4 == 0) goto L_0x038e
            r1.b(r2)     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0398 }
            r4.<init>()     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r6 = r2.i     // Catch:{ NullPointerException -> 0x0398 }
            r4.append(r6)     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r6 = "Only data from CC channel 1 will be extracted.\n\n"
            r4.append(r6)     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r4 = r4.toString()     // Catch:{ NullPointerException -> 0x0398 }
            r2.i = r4     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r4 = r3.readLine()     // Catch:{ NullPointerException -> 0x0398 }
            r6 = 0
            java.lang.String r7 = ""
            r13 = r6
            r12 = r7
            r9 = 1
            r10 = 0
            r11 = 1
            r14 = 0
            r15 = 0
        L_0x0051:
            java.lang.String r0 = "h:m:s:f/fps"
            if (r4 == 0) goto L_0x035d
            java.lang.String r4 = r4.trim()     // Catch:{ NullPointerException -> 0x0399 }
            int r9 = r9 + 1
            boolean r16 = r4.isEmpty()     // Catch:{ NullPointerException -> 0x035a }
            if (r16 != 0) goto L_0x0345
            java.lang.String r8 = "\t"
            java.lang.String[] r4 = r4.split(r8)     // Catch:{ NullPointerException -> 0x035a }
            com.utils.Subtitle.converter.Time r8 = new com.utils.Subtitle.converter.Time     // Catch:{ NullPointerException -> 0x035a }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x035a }
            r5.<init>()     // Catch:{ NullPointerException -> 0x035a }
            r29 = r9
            r16 = 0
            r9 = r4[r16]     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = "/29.97"
            r5.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r5 = r5.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r8.<init>(r0, r5)     // Catch:{ NullPointerException -> 0x0357 }
            r0 = 1
            r4 = r4[r0]     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r0 = " "
            java.lang.String[] r0 = r4.split(r0)     // Catch:{ NullPointerException -> 0x0357 }
            r4 = 0
        L_0x008d:
            int r5 = r0.length     // Catch:{ NullPointerException -> 0x0357 }
            if (r4 >= r5) goto L_0x033d
            r5 = r0[r4]     // Catch:{ NullPointerException -> 0x0357 }
            r9 = 16
            int r5 = java.lang.Integer.parseInt(r5, r9)     // Catch:{ NullPointerException -> 0x0357 }
            r5 = r5 & 32639(0x7f7f, float:4.5737E-41)
            r9 = r5 & 24576(0x6000, float:3.4438E-41)
            java.lang.String r18 = "yellow"
            java.lang.String r19 = "red"
            java.lang.String r20 = "cyan"
            java.lang.String r21 = "blue"
            java.lang.String r22 = "green"
            java.lang.String r23 = "white"
            if (r9 == 0) goto L_0x011d
            if (r10 == 0) goto L_0x0117
            r9 = 65280(0xff00, float:9.1477E-41)
            r9 = r9 & r5
            int r9 = r9 >>> 8
            byte r9 = (byte) r9
            r5 = r5 & 255(0xff, float:3.57E-43)
            byte r5 = (byte) r5
            if (r11 == 0) goto L_0x00e4
            r24 = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r12)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = r1.a((byte) r9)     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r9.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r9.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r1.a((byte) r5)     // Catch:{ NullPointerException -> 0x0357 }
            r9.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r9.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r12 = r3
            r25 = r11
            goto L_0x0132
        L_0x00e4:
            r24 = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r25 = r11
            java.lang.String r11 = r6.d     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r11)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = r1.a((byte) r9)     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r6.d = r3     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = r6.d     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r5 = r1.a((byte) r5)     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r5)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r6.d = r3     // Catch:{ NullPointerException -> 0x0357 }
            goto L_0x0132
        L_0x0117:
            r24 = r3
            r25 = r11
            r11 = r10
            goto L_0x0131
        L_0x011d:
            r24 = r3
            r25 = r11
            if (r5 != 0) goto L_0x0137
            int r3 = r8.f6534a     // Catch:{ NullPointerException -> 0x0357 }
            r11 = r10
            double r9 = (double) r3     // Catch:{ NullPointerException -> 0x0357 }
            r17 = 4629892762866901061(0x4040aef006d56045, double:33.366700033366705)
            double r9 = r9 + r17
            int r3 = (int) r9     // Catch:{ NullPointerException -> 0x0357 }
            r8.f6534a = r3     // Catch:{ NullPointerException -> 0x0357 }
        L_0x0131:
            r10 = r11
        L_0x0132:
            r11 = r25
            r3 = 1
            goto L_0x0338
        L_0x0137:
            r11 = r10
            int r3 = r4 + 1
            int r9 = r0.length     // Catch:{ NullPointerException -> 0x0357 }
            if (r3 >= r9) goto L_0x0148
            r9 = r0[r4]     // Catch:{ NullPointerException -> 0x0357 }
            r10 = r0[r3]     // Catch:{ NullPointerException -> 0x0357 }
            boolean r9 = r9.equals(r10)     // Catch:{ NullPointerException -> 0x0357 }
            if (r9 == 0) goto L_0x0148
            r4 = r3
        L_0x0148:
            r3 = r5 & 2048(0x800, float:2.87E-42)
            if (r3 != 0) goto L_0x0334
            r3 = r5 & 5744(0x1670, float:8.049E-42)
            r9 = 5152(0x1420, float:7.22E-42)
            if (r3 != r9) goto L_0x0237
            r3 = r5 & 256(0x100, float:3.59E-43)
            if (r3 != 0) goto L_0x0334
            r3 = r5 & 15
            if (r3 == 0) goto L_0x0231
            r5 = 9
            if (r3 == r5) goto L_0x0224
            r5 = 12
            if (r3 == r5) goto L_0x01ef
            r5 = 5
            if (r3 == r5) goto L_0x0195
            r5 = 6
            if (r3 == r5) goto L_0x0195
            r5 = 7
            if (r3 == r5) goto L_0x0195
            r5 = 14
            if (r3 == r5) goto L_0x0192
            r5 = 15
            if (r3 == r5) goto L_0x0175
            goto L_0x021e
        L_0x0175:
            com.utils.Subtitle.converter.Caption r3 = new com.utils.Subtitle.converter.Caption     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r3.b = r8     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r6 = r3.d     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r6)     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r12)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r5 = r5.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r3.d = r5     // Catch:{ NullPointerException -> 0x0357 }
        L_0x018f:
            r6 = r3
            goto L_0x021e
        L_0x0192:
            r12 = r7
            goto L_0x021e
        L_0x0195:
            if (r6 == 0) goto L_0x01e5
            r6.c = r8     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r7)     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r13)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0357 }
            if (r14 == 0) goto L_0x01bb
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = "U"
            r5.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r5.toString()     // Catch:{ NullPointerException -> 0x0357 }
        L_0x01bb:
            if (r15 == 0) goto L_0x01ce
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = "I"
            r5.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r5.toString()     // Catch:{ NullPointerException -> 0x0357 }
        L_0x01ce:
            java.util.Hashtable<java.lang.String, com.utils.Subtitle.converter.Style> r5 = r2.g     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.Object r3 = r5.get(r3)     // Catch:{ NullPointerException -> 0x0357 }
            com.utils.Subtitle.converter.Style r3 = (com.utils.Subtitle.converter.Style) r3     // Catch:{ NullPointerException -> 0x0357 }
            r6.f6532a = r3     // Catch:{ NullPointerException -> 0x0357 }
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r3 = r2.h     // Catch:{ NullPointerException -> 0x0357 }
            com.utils.Subtitle.converter.Time r5 = r6.b     // Catch:{ NullPointerException -> 0x0357 }
            int r5 = r5.f6534a     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NullPointerException -> 0x0357 }
            r3.put(r5, r6)     // Catch:{ NullPointerException -> 0x0357 }
        L_0x01e5:
            com.utils.Subtitle.converter.Caption r3 = new com.utils.Subtitle.converter.Caption     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r3.b = r8     // Catch:{ NullPointerException -> 0x0357 }
            r6 = r3
            r12 = r7
            goto L_0x022c
        L_0x01ef:
            if (r6 == 0) goto L_0x021e
            r6.c = r8     // Catch:{ NullPointerException -> 0x0357 }
            com.utils.Subtitle.converter.Time r3 = r6.b     // Catch:{ NullPointerException -> 0x0357 }
            if (r3 == 0) goto L_0x021e
            com.utils.Subtitle.converter.Time r3 = r6.b     // Catch:{ NullPointerException -> 0x0357 }
            int r3 = r3.f6534a     // Catch:{ NullPointerException -> 0x0357 }
        L_0x01fb:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r5 = r2.h     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r3)     // Catch:{ NullPointerException -> 0x0357 }
            boolean r5 = r5.containsKey(r9)     // Catch:{ NullPointerException -> 0x0357 }
            if (r5 == 0) goto L_0x020a
            int r3 = r3 + 1
            goto L_0x01fb
        L_0x020a:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r3 = r2.h     // Catch:{ NullPointerException -> 0x0357 }
            com.utils.Subtitle.converter.Time r5 = r6.b     // Catch:{ NullPointerException -> 0x0357 }
            int r5 = r5.f6534a     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NullPointerException -> 0x0357 }
            r3.put(r5, r6)     // Catch:{ NullPointerException -> 0x0357 }
            com.utils.Subtitle.converter.Caption r3 = new com.utils.Subtitle.converter.Caption     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            goto L_0x018f
        L_0x021e:
            r11 = r25
            r3 = 1
            r10 = 1
            goto L_0x0338
        L_0x0224:
            com.utils.Subtitle.converter.Caption r3 = new com.utils.Subtitle.converter.Caption     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r3.b = r8     // Catch:{ NullPointerException -> 0x0357 }
            r6 = r3
        L_0x022c:
            r3 = 1
            r10 = 1
            r11 = 0
            goto L_0x0338
        L_0x0231:
            r12 = r7
            r3 = 1
            r10 = 1
            r11 = 1
            goto L_0x0338
        L_0x0237:
            if (r11 == 0) goto L_0x0131
            r3 = r5 & 4160(0x1040, float:5.83E-42)
            r9 = 4160(0x1040, float:5.83E-42)
            if (r3 != r9) goto L_0x0297
            if (r25 == 0) goto L_0x0259
            boolean r3 = r12.isEmpty()     // Catch:{ NullPointerException -> 0x0357 }
            if (r3 != 0) goto L_0x0259
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r12)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = "<br />"
            r3.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r12 = r3
        L_0x0259:
            if (r25 != 0) goto L_0x0278
            java.lang.String r3 = r6.d     // Catch:{ NullPointerException -> 0x0357 }
            boolean r3 = r3.isEmpty()     // Catch:{ NullPointerException -> 0x0357 }
            if (r3 != 0) goto L_0x0278
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r3.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = r6.d     // Catch:{ NullPointerException -> 0x0357 }
            r3.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = "<br />"
            r3.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r6.d = r3     // Catch:{ NullPointerException -> 0x0357 }
        L_0x0278:
            r3 = r5 & 1
            r9 = 1
            if (r3 != r9) goto L_0x027f
            r3 = 1
            goto L_0x0280
        L_0x027f:
            r3 = 0
        L_0x0280:
            r10 = r5 & 16
            r13 = 16
            if (r10 == r13) goto L_0x02d6
            r5 = r5 & 14
            int r5 = r5 >> r9
            short r5 = (short) r5     // Catch:{ NullPointerException -> 0x0357 }
            switch(r5) {
                case 0: goto L_0x02d6;
                case 1: goto L_0x02d1;
                case 2: goto L_0x02cc;
                case 3: goto L_0x02c7;
                case 4: goto L_0x02c2;
                case 5: goto L_0x02bd;
                case 6: goto L_0x0294;
                case 7: goto L_0x028f;
                default: goto L_0x028d;
            }     // Catch:{ NullPointerException -> 0x0357 }
        L_0x028d:
            goto L_0x02d6
        L_0x028f:
            r14 = r3
            r10 = r11
            r13 = r23
            goto L_0x02b1
        L_0x0294:
            java.lang.String r5 = "magenta"
            goto L_0x02b9
        L_0x0297:
            r3 = r5 & 6000(0x1770, float:8.408E-42)
            r9 = 4384(0x1120, float:6.143E-42)
            if (r3 != r9) goto L_0x02df
            r3 = r5 & 1
            r9 = 1
            if (r3 != r9) goto L_0x02a4
            r3 = 1
            goto L_0x02a5
        L_0x02a4:
            r3 = 0
        L_0x02a5:
            r5 = r5 & 14
            int r5 = r5 >> r9
            short r5 = (short) r5     // Catch:{ NullPointerException -> 0x0357 }
            switch(r5) {
                case 0: goto L_0x02d6;
                case 1: goto L_0x02d1;
                case 2: goto L_0x02cc;
                case 3: goto L_0x02c7;
                case 4: goto L_0x02c2;
                case 5: goto L_0x02bd;
                case 6: goto L_0x02b7;
                case 7: goto L_0x02af;
                default: goto L_0x02ac;
            }     // Catch:{ NullPointerException -> 0x0357 }
        L_0x02ac:
            r14 = r3
            goto L_0x0131
        L_0x02af:
            r14 = r3
            r10 = r11
        L_0x02b1:
            r11 = r25
            r3 = 1
            r15 = 1
            goto L_0x0338
        L_0x02b7:
            java.lang.String r5 = "magenta"
        L_0x02b9:
            r14 = r3
            r13 = r5
            r10 = r11
            goto L_0x02da
        L_0x02bd:
            r14 = r3
            r10 = r11
            r13 = r18
            goto L_0x02da
        L_0x02c2:
            r14 = r3
            r10 = r11
            r13 = r19
            goto L_0x02da
        L_0x02c7:
            r14 = r3
            r10 = r11
            r13 = r20
            goto L_0x02da
        L_0x02cc:
            r14 = r3
            r10 = r11
            r13 = r21
            goto L_0x02da
        L_0x02d1:
            r14 = r3
            r10 = r11
            r13 = r22
            goto L_0x02da
        L_0x02d6:
            r14 = r3
            r10 = r11
            r13 = r23
        L_0x02da:
            r11 = r25
            r3 = 1
            r15 = 0
            goto L_0x0338
        L_0x02df:
            r9 = r5 & 6012(0x177c, float:8.425E-42)
            r10 = 5920(0x1720, float:8.296E-42)
            if (r9 != r10) goto L_0x02e7
            goto L_0x0131
        L_0x02e7:
            r9 = 4400(0x1130, float:6.166E-42)
            if (r3 != r9) goto L_0x031e
            r3 = r5 & 15
            if (r25 == 0) goto L_0x0305
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r12)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r1.a((int) r3)     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r5.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r12 = r3
            goto L_0x0131
        L_0x0305:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0357 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r9 = r6.d     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r9)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r1.a((int) r3)     // Catch:{ NullPointerException -> 0x0357 }
            r5.append(r3)     // Catch:{ NullPointerException -> 0x0357 }
            java.lang.String r3 = r5.toString()     // Catch:{ NullPointerException -> 0x0357 }
            r6.d = r3     // Catch:{ NullPointerException -> 0x0357 }
            goto L_0x0131
        L_0x031e:
            r3 = r5 & 5728(0x1660, float:8.027E-42)
            r9 = 4640(0x1220, float:6.502E-42)
            if (r3 != r9) goto L_0x0131
            r3 = r5 & 287(0x11f, float:4.02E-43)
            if (r25 == 0) goto L_0x032d
            r1.a(r12, r3)     // Catch:{ NullPointerException -> 0x0357 }
            goto L_0x0131
        L_0x032d:
            java.lang.String r5 = r6.d     // Catch:{ NullPointerException -> 0x0357 }
            r1.a(r5, r3)     // Catch:{ NullPointerException -> 0x0357 }
            goto L_0x0131
        L_0x0334:
            r11 = r25
            r3 = 1
            r10 = 0
        L_0x0338:
            int r4 = r4 + r3
            r3 = r24
            goto L_0x008d
        L_0x033d:
            r24 = r3
            r25 = r11
            r11 = r10
            r11 = r25
            goto L_0x034b
        L_0x0345:
            r24 = r3
            r29 = r9
            r16 = 0
        L_0x034b:
            java.lang.String r4 = r24.readLine()     // Catch:{ NullPointerException -> 0x0357 }
            r5 = r28
            r9 = r29
            r3 = r24
            goto L_0x0051
        L_0x0357:
            r9 = r29
            goto L_0x0399
        L_0x035a:
            r29 = r9
            goto L_0x0399
        L_0x035d:
            com.utils.Subtitle.converter.Time r3 = new com.utils.Subtitle.converter.Time     // Catch:{ NullPointerException -> 0x0399 }
            java.lang.String r4 = "99:59:59:29/29.97"
            r3.<init>(r0, r4)     // Catch:{ NullPointerException -> 0x0399 }
            r6.c = r3     // Catch:{ NullPointerException -> 0x0399 }
            com.utils.Subtitle.converter.Time r0 = r6.b     // Catch:{ NullPointerException -> 0x0399 }
            if (r0 == 0) goto L_0x038a
            com.utils.Subtitle.converter.Time r0 = r6.b     // Catch:{ NullPointerException -> 0x0399 }
            int r0 = r0.f6534a     // Catch:{ NullPointerException -> 0x0399 }
        L_0x036e:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r3 = r2.h     // Catch:{ NullPointerException -> 0x0399 }
            java.lang.Integer r4 = java.lang.Integer.valueOf(r0)     // Catch:{ NullPointerException -> 0x0399 }
            boolean r3 = r3.containsKey(r4)     // Catch:{ NullPointerException -> 0x0399 }
            if (r3 == 0) goto L_0x037d
            int r0 = r0 + 1
            goto L_0x036e
        L_0x037d:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r0 = r2.h     // Catch:{ NullPointerException -> 0x0399 }
            com.utils.Subtitle.converter.Time r3 = r6.b     // Catch:{ NullPointerException -> 0x0399 }
            int r3 = r3.f6534a     // Catch:{ NullPointerException -> 0x0399 }
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)     // Catch:{ NullPointerException -> 0x0399 }
            r0.put(r3, r6)     // Catch:{ NullPointerException -> 0x0399 }
        L_0x038a:
            r2.a()     // Catch:{ NullPointerException -> 0x0399 }
            goto L_0x03b6
        L_0x038e:
            com.utils.Subtitle.converter.FatalParsingException r0 = new com.utils.Subtitle.converter.FatalParsingException     // Catch:{ NullPointerException -> 0x0398 }
            java.lang.String r3 = "The fist line should define the file type: \"Scenarist_SCC V1.0\""
            r0.<init>(r3)     // Catch:{ NullPointerException -> 0x0398 }
            throw r0     // Catch:{ NullPointerException -> 0x0398 }
        L_0x0396:
            r0 = move-exception
            goto L_0x03bd
        L_0x0398:
            r9 = 1
        L_0x0399:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x0396 }
            r0.<init>()     // Catch:{ all -> 0x0396 }
            java.lang.String r3 = r2.i     // Catch:{ all -> 0x0396 }
            r0.append(r3)     // Catch:{ all -> 0x0396 }
            java.lang.String r3 = "unexpected end of file at line "
            r0.append(r3)     // Catch:{ all -> 0x0396 }
            r0.append(r9)     // Catch:{ all -> 0x0396 }
            java.lang.String r3 = ", maybe last caption is not complete.\n\n"
            r0.append(r3)     // Catch:{ all -> 0x0396 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0396 }
            r2.i = r0     // Catch:{ all -> 0x0396 }
        L_0x03b6:
            r28.close()
            r0 = 1
            r2.l = r0
            return r2
        L_0x03bd:
            r28.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.converter.FormatSCC.a(java.lang.String, java.io.InputStream, java.lang.String):com.utils.Subtitle.converter.TimedTextObject");
    }

    public String[] a(TimedTextObject timedTextObject) {
        String str;
        if (!timedTextObject.l) {
            return null;
        }
        ArrayList arrayList = new ArrayList((timedTextObject.h.size() * 2) + 20);
        arrayList.add(0, "Scenarist_SCC V1.0\n");
        Caption caption = new Caption();
        caption.d = "";
        caption.c = new Time("h:mm:ss.cs", "0:00:00.00");
        int i = 1;
        for (Caption next : timedTextObject.h.values()) {
            int i2 = caption.c.f6534a;
            Time time = next.b;
            int i3 = time.f6534a;
            if (i2 > i3) {
                next.d += "<br />" + caption.d;
                Time time2 = next.b;
                time2.f6534a = (int) (((double) time2.f6534a) - 33.366700033366705d);
                Time time3 = next.b;
                time3.f6534a = (int) (((double) time3.f6534a) + 33.366700033366705d);
                str = ("" + next.b.a("hh:mm:ss:ff/29.97") + "\t942c 942c ") + "94ae 94ae 9420 9420 ";
            } else if (i2 < i3) {
                Time time4 = next.b;
                time4.f6534a = (int) (((double) time4.f6534a) - 33.366700033366705d);
                str = ("" + caption.c.a("hh:mm:ss:ff/29.97") + "\t942c 942c\n\n") + next.b.a("hh:mm:ss:ff/29.97") + "\t94ae 94ae 9420 9420 ";
                Time time5 = next.b;
                time5.f6534a = (int) (((double) time5.f6534a) + 33.366700033366705d);
            } else {
                time.f6534a = (int) (((double) i3) - 33.366700033366705d);
                str = "" + next.b.a("hh:mm:ss:ff/29.97") + "\t942c 942c 94ae 94ae 9420 9420 ";
                Time time6 = next.b;
                time6.f6534a = (int) (((double) time6.f6534a) + 33.366700033366705d);
            }
            arrayList.add(i, (str + a(next)) + "8080 8080 942f 942f\n");
            caption = next;
            i++;
        }
        arrayList.add(i, "");
        String[] strArr = new String[arrayList.size()];
        for (int i4 = 0; i4 < strArr.length; i4++) {
            strArr[i4] = (String) arrayList.get(i4);
        }
        return strArr;
    }

    private String a(Caption caption) {
        String[] split = caption.d.split("<br />");
        if (split[0].length() > 32) {
            split[0] = split[0].substring(0, 32);
        }
        int length = ((32 - split[0].length()) / 2) % 4;
        String str = ("" + "1340 1340 ") + a(split[0].toCharArray());
        if (split.length <= 1) {
            return str;
        }
        if (split[1].length() > 32) {
            split[1] = split[1].substring(0, 32);
        }
        int length2 = ((32 - split[1].length()) / 2) % 4;
        String str2 = (str + "13e0 13e0 ") + a(split[1].toCharArray());
        if (split.length <= 2) {
            return str2;
        }
        if (split[2].length() > 32) {
            split[2] = split[2].substring(0, 32);
        }
        int length3 = ((32 - split[2].length()) / 2) % 4;
        String str3 = (str2 + "9440 9440 ") + a(split[2].toCharArray());
        if (split.length <= 3) {
            return str3;
        }
        if (split[3].length() > 32) {
            split[3] = split[3].substring(0, 32);
        }
        String str4 = str3 + "94e0 94e0 ";
        int length4 = ((32 - split[3].length()) / 2) % 4;
        return str4 + a(split[3].toCharArray());
    }

    private String a(char[] cArr) {
        String str = "";
        int i = 0;
        while (i < cArr.length) {
            char c = cArr[i];
            if (c == '|') {
                str = str + "7f";
            } else if (c != 65533) {
                switch (c) {
                    case ' ':
                        str = str + "20";
                        break;
                    case '!':
                        str = str + "a1";
                        break;
                    case '\"':
                        str = str + "a2";
                        break;
                    case '#':
                        str = str + "23";
                        break;
                    case '$':
                        str = str + "a4";
                        break;
                    case '%':
                        str = str + "25";
                        break;
                    case '&':
                        str = str + "26";
                        break;
                    case '\'':
                        str = str + "a7";
                        break;
                    case '(':
                        str = str + "a8";
                        break;
                    case ')':
                        str = str + "29";
                        break;
                    default:
                        switch (c) {
                            case '+':
                                str = str + "ab";
                                break;
                            case ',':
                                str = str + "2c";
                                break;
                            case '-':
                                str = str + "ad";
                                break;
                            case '.':
                                str = str + "ae";
                                break;
                            case '/':
                                str = str + "2f";
                                break;
                            case '0':
                                str = str + "b0";
                                break;
                            case '1':
                                str = str + "31";
                                break;
                            case '2':
                                str = str + "32";
                                break;
                            case '3':
                                str = str + "b3";
                                break;
                            case '4':
                                str = str + "34";
                                break;
                            case '5':
                                str = str + "b5";
                                break;
                            case '6':
                                str = str + "b6";
                                break;
                            case '7':
                                str = str + "37";
                                break;
                            case '8':
                                str = str + "38";
                                break;
                            case '9':
                                str = str + "b9";
                                break;
                            case ':':
                                str = str + "ba";
                                break;
                            case ';':
                                str = str + "3b";
                                break;
                            case '<':
                                str = str + "bc";
                                break;
                            case '=':
                                str = str + "3d";
                                break;
                            case '>':
                                str = str + "3e";
                                break;
                            case '?':
                                str = str + "bf";
                                break;
                            case '@':
                                str = str + "40";
                                break;
                            case 'A':
                                str = str + "c1";
                                break;
                            case 'B':
                                str = str + "c2";
                                break;
                            case 'C':
                                str = str + "43";
                                break;
                            case 'D':
                                str = str + "c4";
                                break;
                            case 'E':
                                str = str + "45";
                                break;
                            case 'F':
                                str = str + "46";
                                break;
                            case 'G':
                                str = str + "c7";
                                break;
                            case 'H':
                                str = str + "c8";
                                break;
                            case 'I':
                                str = str + "49";
                                break;
                            case 'J':
                                str = str + "4a";
                                break;
                            case 'K':
                                str = str + "cb";
                                break;
                            case 'L':
                                str = str + "4c";
                                break;
                            case 'M':
                                str = str + "cd";
                                break;
                            case 'N':
                                str = str + "ce";
                                break;
                            case 'O':
                                str = str + "4f";
                                break;
                            case 'P':
                                str = str + "d0";
                                break;
                            case 'Q':
                                str = str + "51";
                                break;
                            case 'R':
                                str = str + "52";
                                break;
                            case 'S':
                                str = str + "d3";
                                break;
                            case 'T':
                                str = str + "54";
                                break;
                            case 'U':
                                str = str + "d5";
                                break;
                            case 'V':
                                str = str + "d6";
                                break;
                            case 'W':
                                str = str + "57";
                                break;
                            case 'X':
                                str = str + "58";
                                break;
                            case 'Y':
                                str = str + "d9";
                                break;
                            case 'Z':
                                str = str + "da";
                                break;
                            case '[':
                                str = str + "5b";
                                break;
                            default:
                                switch (c) {
                                    case 'a':
                                        str = str + "61";
                                        break;
                                    case 'b':
                                        str = str + "62";
                                        break;
                                    case 'c':
                                        str = str + "e3";
                                        break;
                                    case 'd':
                                        str = str + "64";
                                        break;
                                    case 'e':
                                        str = str + "e5";
                                        break;
                                    case 'f':
                                        str = str + "e6";
                                        break;
                                    case 'g':
                                        str = str + "67";
                                        break;
                                    case 'h':
                                        str = str + "68";
                                        break;
                                    case 'i':
                                        str = str + "e9";
                                        break;
                                    case 'j':
                                        str = str + "ea";
                                        break;
                                    case 'k':
                                        str = str + "6b";
                                        break;
                                    case 'l':
                                        str = str + "ec";
                                        break;
                                    case 'm':
                                        str = str + "6d";
                                        break;
                                    case 'n':
                                        str = str + "6e";
                                        break;
                                    case 'o':
                                        str = str + "ef";
                                        break;
                                    case 'p':
                                        str = str + "70";
                                        break;
                                    case 'q':
                                        str = str + "f1";
                                        break;
                                    case 'r':
                                        str = str + "f2";
                                        break;
                                    case 's':
                                        str = str + "73";
                                        break;
                                    case 't':
                                        str = str + "f4";
                                        break;
                                    case 'u':
                                        str = str + "75";
                                        break;
                                    case 'v':
                                        str = str + "76";
                                        break;
                                    case 'w':
                                        str = str + "f7";
                                        break;
                                    case 'x':
                                        str = str + "f8";
                                        break;
                                    case 'y':
                                        str = str + "79";
                                        break;
                                    case 'z':
                                        str = str + "7a";
                                        break;
                                    default:
                                        str = str + "7f";
                                        break;
                                }
                        }
                }
            } else {
                str = str + "2a";
            }
            if (i % 2 == 1) {
                str = str + " ";
            }
            i++;
        }
        if (i % 2 != 1) {
            return str;
        }
        return str + "80 ";
    }

    private String a(byte b) {
        if (b == 0) {
            return "";
        }
        if (b == 42) {
            return "�";
        }
        if (b == 92) {
            return "é";
        }
        switch (b) {
            case 94:
                return "í";
            case 95:
                return "ó";
            case 96:
                return "ú";
            default:
                switch (b) {
                    case 123:
                        return "ç";
                    case 124:
                        return "�";
                    case 125:
                        return "Ñ";
                    case WebSocketProtocol.PAYLOAD_SHORT:
                        return "ñ";
                    case Byte.MAX_VALUE:
                        return "|";
                    default:
                        return "" + ((char) b);
                }
        }
    }
}
