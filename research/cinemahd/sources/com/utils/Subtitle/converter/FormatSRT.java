package com.utils.Subtitle.converter;

import java.util.ArrayList;

public class FormatSRT implements TimedTextFileFormat {
    /* JADX WARNING: Can't wrap try/catch for region: R(2:39|40) */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:11|12|13) */
    /* JADX WARNING: Can't wrap try/catch for region: R(5:15|16|17|18|19) */
    /* JADX WARNING: Code restructure failed: missing block: B:12:?, code lost:
        r1.i += r5 + " expected at line " + r2;
        r1.i += "\n skipping to next line\n\n";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x006f, code lost:
        r7 = r5 + 1;
        r5 = false;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0144, code lost:
        r12 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:?, code lost:
        r1.i += "unexpected end of file, maybe last caption is not complete.\n\n";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x015c, code lost:
        r13.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x015f, code lost:
        throw r12;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:11:0x003f */
    /* JADX WARNING: Missing exception handler attribute for start block: B:18:0x00a1 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:39:0x0146 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.utils.Subtitle.converter.TimedTextObject a(java.lang.String r12, java.io.InputStream r13, java.lang.String r14) throws java.io.IOException {
        /*
            r11 = this;
            java.lang.String r0 = "hh:mm:ss,ms"
            com.utils.Subtitle.converter.TimedTextObject r1 = new com.utils.Subtitle.converter.TimedTextObject
            r1.<init>()
            com.utils.Subtitle.converter.Caption r2 = new com.utils.Subtitle.converter.Caption
            r2.<init>()
            java.io.InputStreamReader r3 = new java.io.InputStreamReader
            r3.<init>(r13, r14)
            java.io.BufferedReader r14 = new java.io.BufferedReader
            r14.<init>(r3)
            r1.e = r12
            java.lang.String r12 = r14.readLine()
            r3 = 0
            r4 = 1
            r6 = r2
            r2 = 0
            r5 = 1
        L_0x0021:
            if (r12 == 0) goto L_0x0160
            java.lang.String r12 = r12.trim()     // Catch:{ NullPointerException -> 0x0146 }
            int r2 = r2 + r4
            boolean r7 = r12.isEmpty()     // Catch:{ NullPointerException -> 0x0146 }
            if (r7 != 0) goto L_0x013e
            int r7 = java.lang.Integer.parseInt(r12)     // Catch:{ Exception -> 0x003f }
            if (r7 != r5) goto L_0x0039
            int r5 = r5 + 1
            r7 = r5
            r5 = 1
            goto L_0x0073
        L_0x0039:
            java.lang.Exception r7 = new java.lang.Exception     // Catch:{ Exception -> 0x003f }
            r7.<init>()     // Catch:{ Exception -> 0x003f }
            throw r7     // Catch:{ Exception -> 0x003f }
        L_0x003f:
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0146 }
            r7.<init>()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = r1.i     // Catch:{ NullPointerException -> 0x0146 }
            r7.append(r8)     // Catch:{ NullPointerException -> 0x0146 }
            r7.append(r5)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = " expected at line "
            r7.append(r8)     // Catch:{ NullPointerException -> 0x0146 }
            r7.append(r2)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r7 = r7.toString()     // Catch:{ NullPointerException -> 0x0146 }
            r1.i = r7     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0146 }
            r7.<init>()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = r1.i     // Catch:{ NullPointerException -> 0x0146 }
            r7.append(r8)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = "\n skipping to next line\n\n"
            r7.append(r8)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r7 = r7.toString()     // Catch:{ NullPointerException -> 0x0146 }
            r1.i = r7     // Catch:{ NullPointerException -> 0x0146 }
            int r5 = r5 + 1
            r7 = r5
            r5 = 0
        L_0x0073:
            if (r5 == 0) goto L_0x00ba
            int r2 = r2 + 1
            java.lang.String r8 = r14.readLine()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r12 = r8.trim()     // Catch:{ Exception -> 0x00a1 }
            r8 = 12
            java.lang.String r9 = r12.substring(r3, r8)     // Catch:{ Exception -> 0x00a1 }
            int r10 = r12.length()     // Catch:{ Exception -> 0x00a1 }
            int r10 = r10 - r8
            int r8 = r12.length()     // Catch:{ Exception -> 0x00a1 }
            java.lang.String r8 = r12.substring(r10, r8)     // Catch:{ Exception -> 0x00a1 }
            com.utils.Subtitle.converter.Time r10 = new com.utils.Subtitle.converter.Time     // Catch:{ Exception -> 0x00a1 }
            r10.<init>(r0, r9)     // Catch:{ Exception -> 0x00a1 }
            r6.b = r10     // Catch:{ Exception -> 0x00a1 }
            com.utils.Subtitle.converter.Time r9 = new com.utils.Subtitle.converter.Time     // Catch:{ Exception -> 0x00a1 }
            r9.<init>(r0, r8)     // Catch:{ Exception -> 0x00a1 }
            r6.c = r9     // Catch:{ Exception -> 0x00a1 }
            goto L_0x00ba
        L_0x00a1:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0146 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = r1.i     // Catch:{ NullPointerException -> 0x0146 }
            r5.append(r8)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = "incorrect time format at line "
            r5.append(r8)     // Catch:{ NullPointerException -> 0x0146 }
            r5.append(r2)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r5 = r5.toString()     // Catch:{ NullPointerException -> 0x0146 }
            r1.i = r5     // Catch:{ NullPointerException -> 0x0146 }
            r5 = 0
        L_0x00ba:
            if (r5 == 0) goto L_0x0126
            int r2 = r2 + 1
            java.lang.String r12 = r14.readLine()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r12 = r12.trim()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r5 = ""
        L_0x00c8:
            boolean r8 = r12.isEmpty()     // Catch:{ NullPointerException -> 0x0146 }
            if (r8 != 0) goto L_0x00ed
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0146 }
            r8.<init>()     // Catch:{ NullPointerException -> 0x0146 }
            r8.append(r5)     // Catch:{ NullPointerException -> 0x0146 }
            r8.append(r12)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r12 = "<br />"
            r8.append(r12)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r5 = r8.toString()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r12 = r14.readLine()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r12 = r12.trim()     // Catch:{ NullPointerException -> 0x0146 }
            int r2 = r2 + 1
            goto L_0x00c8
        L_0x00ed:
            r6.d = r5     // Catch:{ NullPointerException -> 0x0146 }
            com.utils.Subtitle.converter.Time r5 = r6.b     // Catch:{ NullPointerException -> 0x0146 }
            int r5 = r5.f6534a     // Catch:{ NullPointerException -> 0x0146 }
        L_0x00f3:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r8 = r1.h     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r5)     // Catch:{ NullPointerException -> 0x0146 }
            boolean r8 = r8.containsKey(r9)     // Catch:{ NullPointerException -> 0x0146 }
            if (r8 == 0) goto L_0x0102
            int r5 = r5 + 1
            goto L_0x00f3
        L_0x0102:
            com.utils.Subtitle.converter.Time r8 = r6.b     // Catch:{ NullPointerException -> 0x0146 }
            int r8 = r8.f6534a     // Catch:{ NullPointerException -> 0x0146 }
            if (r5 == r8) goto L_0x011d
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0146 }
            r8.<init>()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r9 = r1.i     // Catch:{ NullPointerException -> 0x0146 }
            r8.append(r9)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r9 = "caption with same start time found...\n\n"
            r8.append(r9)     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r8 = r8.toString()     // Catch:{ NullPointerException -> 0x0146 }
            r1.i = r8     // Catch:{ NullPointerException -> 0x0146 }
        L_0x011d:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r8 = r1.h     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)     // Catch:{ NullPointerException -> 0x0146 }
            r8.put(r5, r6)     // Catch:{ NullPointerException -> 0x0146 }
        L_0x0126:
            boolean r12 = r12.isEmpty()     // Catch:{ NullPointerException -> 0x0146 }
            if (r12 != 0) goto L_0x0137
            java.lang.String r12 = r14.readLine()     // Catch:{ NullPointerException -> 0x0146 }
            java.lang.String r12 = r12.trim()     // Catch:{ NullPointerException -> 0x0146 }
            int r2 = r2 + 1
            goto L_0x0126
        L_0x0137:
            com.utils.Subtitle.converter.Caption r12 = new com.utils.Subtitle.converter.Caption     // Catch:{ NullPointerException -> 0x0146 }
            r12.<init>()     // Catch:{ NullPointerException -> 0x0146 }
            r6 = r12
            r5 = r7
        L_0x013e:
            java.lang.String r12 = r14.readLine()     // Catch:{ NullPointerException -> 0x0146 }
            goto L_0x0021
        L_0x0144:
            r12 = move-exception
            goto L_0x015c
        L_0x0146:
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ all -> 0x0144 }
            r12.<init>()     // Catch:{ all -> 0x0144 }
            java.lang.String r14 = r1.i     // Catch:{ all -> 0x0144 }
            r12.append(r14)     // Catch:{ all -> 0x0144 }
            java.lang.String r14 = "unexpected end of file, maybe last caption is not complete.\n\n"
            r12.append(r14)     // Catch:{ all -> 0x0144 }
            java.lang.String r12 = r12.toString()     // Catch:{ all -> 0x0144 }
            r1.i = r12     // Catch:{ all -> 0x0144 }
            goto L_0x0160
        L_0x015c:
            r13.close()
            throw r12
        L_0x0160:
            r13.close()
            r1.l = r4
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.converter.FormatSRT.a(java.lang.String, java.io.InputStream, java.lang.String):com.utils.Subtitle.converter.TimedTextObject");
    }

    public String[] a(TimedTextObject timedTextObject) {
        if (!timedTextObject.l) {
            return null;
        }
        ArrayList arrayList = new ArrayList(timedTextObject.h.size() * 5);
        int i = 0;
        int i2 = 1;
        for (Caption next : timedTextObject.h.values()) {
            int i3 = i + 1;
            StringBuilder sb = new StringBuilder();
            sb.append("");
            int i4 = i2 + 1;
            sb.append(i2);
            arrayList.add(i, sb.toString());
            int i5 = timedTextObject.k;
            if (i5 != 0) {
                next.b.f6534a += i5;
                next.c.f6534a += i5;
            }
            int i6 = i3 + 1;
            arrayList.add(i3, next.b.a("hh:mm:ss,ms") + " --> " + next.c.a("hh:mm:ss,ms"));
            int i7 = timedTextObject.k;
            if (i7 != 0) {
                next.b.f6534a -= i7;
                next.c.f6534a -= i7;
            }
            String[] a2 = a(next);
            int i8 = i6;
            for (int i9 = 0; i9 < a2.length; i9++) {
                arrayList.add(i8, "" + a2[i9]);
                i8++;
            }
            i = i8 + 1;
            arrayList.add(i8, "");
            i2 = i4;
        }
        String[] strArr = new String[arrayList.size()];
        for (int i10 = 0; i10 < strArr.length; i10++) {
            strArr[i10] = (String) arrayList.get(i10);
        }
        return strArr;
    }

    private String[] a(Caption caption) {
        String[] split = caption.d.split("<br />");
        for (int i = 0; i < split.length; i++) {
            split[i] = split[i].replaceAll("\\<.*?\\>", "");
        }
        return split;
    }
}
