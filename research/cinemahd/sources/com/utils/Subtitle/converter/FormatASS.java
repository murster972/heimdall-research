package com.utils.Subtitle.converter;

import com.chartboost.sdk.CBLocation;
import java.util.ArrayList;

public class FormatASS implements TimedTextFileFormat {
    private String b(boolean z, Style style) {
        String str;
        String str2;
        String str3 = style.h ? "-1," : "0,";
        if (style.g) {
            str = str3 + "-1,";
        } else {
            str = str3 + "0,";
        }
        if (!z) {
            return str;
        }
        if (style.i) {
            str2 = str + "-1,";
        } else {
            str2 = str + "0,";
        }
        return str2 + "0,100,100,0,0,";
    }

    /* JADX WARNING: Code restructure failed: missing block: B:100:0x030f, code lost:
        r0 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:102:?, code lost:
        r1.i += "unexpected end of file, maybe last caption is not complete.\n\n";
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:101:0x0311 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public com.utils.Subtitle.converter.TimedTextObject a(java.lang.String r20, java.io.InputStream r21, java.lang.String r22) throws java.io.IOException {
        /*
            r19 = this;
            java.lang.String r0 = "["
            com.utils.Subtitle.converter.TimedTextObject r1 = new com.utils.Subtitle.converter.TimedTextObject
            r1.<init>()
            r2 = r20
            r1.e = r2
            com.utils.Subtitle.converter.Caption r2 = new com.utils.Subtitle.converter.Caption
            r2.<init>()
            java.io.InputStreamReader r2 = new java.io.InputStreamReader
            r3 = r21
            r4 = r22
            r2.<init>(r3, r4)
            java.io.BufferedReader r4 = new java.io.BufferedReader
            r4.<init>(r2)
            r2 = 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            r6 = 1120403456(0x42c80000, float:100.0)
            r7 = 0
            r6 = 1
            r8 = 1120403456(0x42c80000, float:100.0)
        L_0x0029:
            if (r5 == 0) goto L_0x0308
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            boolean r9 = r5.startsWith(r0)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x0300
            java.lang.String r9 = "[Script info]"
            boolean r9 = r5.equalsIgnoreCase(r9)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = ":"
            if (r9 == 0) goto L_0x00e8
            int r6 = r6 + 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
        L_0x0049:
            boolean r9 = r5.startsWith(r0)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 != 0) goto L_0x0029
            java.lang.String r9 = "Title:"
            boolean r9 = r5.startsWith(r9)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x0065
            java.lang.String[] r5 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r5 = r5[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            r1.f6535a = r5     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x00dc
        L_0x0065:
            java.lang.String r9 = "Original Script:"
            boolean r9 = r5.startsWith(r9)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x007a
            java.lang.String[] r5 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r5 = r5[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            r1.d = r5     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x00dc
        L_0x007a:
            java.lang.String r9 = "Script Type:"
            boolean r9 = r5.startsWith(r9)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x00be
            java.lang.String[] r9 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r9 = r9[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r11 = "v4.00+"
            boolean r9 = r9.equalsIgnoreCase(r11)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x0096
            r7 = 1
            goto L_0x00dc
        L_0x0096:
            java.lang.String[] r5 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r5 = r5[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = "v4.00"
            boolean r5 = r5.equalsIgnoreCase(r9)     // Catch:{ NullPointerException -> 0x0311 }
            if (r5 != 0) goto L_0x00dc
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0311 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r5.append(r9)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = "Script version is older than 4.00, it may produce parsing errors."
            r5.append(r9)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.toString()     // Catch:{ NullPointerException -> 0x0311 }
            r1.i = r5     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x00dc
        L_0x00be:
            java.lang.String r9 = "Timer:"
            boolean r9 = r5.startsWith(r9)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x00dc
            java.lang.String[] r5 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r5 = r5[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            r8 = 44
            r9 = 46
            java.lang.String r5 = r5.replace(r8, r9)     // Catch:{ NullPointerException -> 0x0311 }
            float r8 = java.lang.Float.parseFloat(r5)     // Catch:{ NullPointerException -> 0x0311 }
        L_0x00dc:
            int r6 = r6 + 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x0049
        L_0x00e8:
            java.lang.String r9 = "[v4 Styles]"
            boolean r9 = r5.equalsIgnoreCase(r9)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r11 = "Format: (format definition) expected at line "
            java.lang.String r12 = ","
            java.lang.String r13 = "Format:"
            if (r9 != 0) goto L_0x0249
            java.lang.String r9 = "[v4 Styles+]"
            boolean r9 = r5.equalsIgnoreCase(r9)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            if (r9 != 0) goto L_0x0249
            java.lang.String r9 = "[v4+ Styles]"
            boolean r9 = r5.equalsIgnoreCase(r9)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            if (r9 == 0) goto L_0x0108
            goto L_0x0249
        L_0x0108:
            java.lang.String r9 = r5.trim()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r14 = "[Events]"
            boolean r9 = r9.equalsIgnoreCase(r14)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            if (r9 == 0) goto L_0x01cf
            int r6 = r6 + 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r9.<init>()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r14 = r1.i     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r9.append(r14)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r14 = "Only dialogue events are considered, all other events are ignored.\n\n"
            r9.append(r14)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r9 = r9.toString()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r1.i = r9     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            boolean r9 = r5.startsWith(r13)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            if (r9 != 0) goto L_0x0165
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0311 }
            r9.<init>()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r14 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r14)     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r11)     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r5)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r11 = " for the events section\n\n"
            r9.append(r11)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.toString()     // Catch:{ NullPointerException -> 0x0311 }
            r1.i = r9     // Catch:{ NullPointerException -> 0x0311 }
        L_0x0154:
            boolean r9 = r5.startsWith(r13)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 != 0) goto L_0x0165
            int r6 = r6 + 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x0154
        L_0x0165:
            java.lang.String[] r5 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r5 = r5[r2]     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String[] r5 = r5.split(r12)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            int r6 = r6 + r2
            java.lang.String r9 = r4.readLine()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
        L_0x017c:
            boolean r11 = r9.startsWith(r0)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            if (r11 != 0) goto L_0x01cb
            java.lang.String r11 = "Dialogue:"
            boolean r11 = r9.startsWith(r11)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            if (r11 == 0) goto L_0x01be
            r11 = 2
            java.lang.String[] r9 = r9.split(r10, r11)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r9 = r9[r2]     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r11 = 10
            java.lang.String[] r9 = r9.split(r12, r11)     // Catch:{ NullPointerException -> 0x0245, all -> 0x0240 }
            r15 = r19
            com.utils.Subtitle.converter.Caption r9 = r15.a(r9, r5, r8, r1)     // Catch:{ NullPointerException -> 0x0311 }
            com.utils.Subtitle.converter.Time r11 = r9.b     // Catch:{ NullPointerException -> 0x0311 }
            int r11 = r11.f6534a     // Catch:{ NullPointerException -> 0x0311 }
        L_0x01a5:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r13 = r1.h     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.Integer r14 = java.lang.Integer.valueOf(r11)     // Catch:{ NullPointerException -> 0x0311 }
            boolean r13 = r13.containsKey(r14)     // Catch:{ NullPointerException -> 0x0311 }
            if (r13 == 0) goto L_0x01b4
            int r11 = r11 + 1
            goto L_0x01a5
        L_0x01b4:
            java.util.TreeMap<java.lang.Integer, com.utils.Subtitle.converter.Caption> r13 = r1.h     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)     // Catch:{ NullPointerException -> 0x0311 }
            r13.put(r11, r9)     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x01c0
        L_0x01be:
            r15 = r19
        L_0x01c0:
            int r6 = r6 + 1
            java.lang.String r9 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x017c
        L_0x01cb:
            r15 = r19
            goto L_0x02fd
        L_0x01cf:
            r15 = r19
            java.lang.String r9 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = "[Fonts]"
            boolean r9 = r9.equalsIgnoreCase(r10)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 != 0) goto L_0x0215
            java.lang.String r9 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = "[Graphics]"
            boolean r9 = r9.equalsIgnoreCase(r10)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 == 0) goto L_0x01ea
            goto L_0x0215
        L_0x01ea:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0311 }
            r9.<init>()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r10)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = "Unrecognized section: "
            r9.append(r10)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r5)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = " all information there is ignored."
            r9.append(r5)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r9.toString()     // Catch:{ NullPointerException -> 0x0311 }
            r1.i = r5     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x0029
        L_0x0215:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0311 }
            r9.<init>()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r10)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r10 = "The section "
            r9.append(r10)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r5)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = " is not supported for conversion, all information there will be lost.\n\n"
            r9.append(r5)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r9.toString()     // Catch:{ NullPointerException -> 0x0311 }
            r1.i = r5     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x0029
        L_0x0240:
            r0 = move-exception
            r15 = r19
            goto L_0x032a
        L_0x0245:
            r15 = r19
            goto L_0x0311
        L_0x0249:
            r15 = r19
            java.lang.String r9 = "+"
            boolean r5 = r5.contains(r9)     // Catch:{ NullPointerException -> 0x0311 }
            if (r5 == 0) goto L_0x026b
            if (r7 != 0) goto L_0x026b
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0311 }
            r5.<init>()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r7 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r5.append(r7)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r7 = "ScriptType should be set to v4:00+ in the [Script Info] section.\n\n"
            r5.append(r7)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.toString()     // Catch:{ NullPointerException -> 0x0311 }
            r1.i = r5     // Catch:{ NullPointerException -> 0x0311 }
            r7 = 1
        L_0x026b:
            int r6 = r6 + 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            boolean r9 = r5.startsWith(r13)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 != 0) goto L_0x02a7
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0311 }
            r9.<init>()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r14 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r14)     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r11)     // Catch:{ NullPointerException -> 0x0311 }
            r9.append(r5)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r11 = " for the styles section\n\n"
            r9.append(r11)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.toString()     // Catch:{ NullPointerException -> 0x0311 }
            r1.i = r9     // Catch:{ NullPointerException -> 0x0311 }
        L_0x0296:
            boolean r9 = r5.startsWith(r13)     // Catch:{ NullPointerException -> 0x0311 }
            if (r9 != 0) goto L_0x02a7
            int r6 = r6 + 1
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            goto L_0x0296
        L_0x02a7:
            java.lang.String[] r5 = r5.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r5 = r5[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r5 = r5.trim()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String[] r5 = r5.split(r12)     // Catch:{ NullPointerException -> 0x0311 }
            int r6 = r6 + r2
            java.lang.String r9 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0311 }
        L_0x02be:
            boolean r11 = r9.startsWith(r0)     // Catch:{ NullPointerException -> 0x0311 }
            if (r11 != 0) goto L_0x02fd
            java.lang.String r11 = "Style:"
            boolean r11 = r9.startsWith(r11)     // Catch:{ NullPointerException -> 0x0311 }
            if (r11 == 0) goto L_0x02f0
            java.lang.String[] r9 = r9.split(r10)     // Catch:{ NullPointerException -> 0x0311 }
            r9 = r9[r2]     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String[] r14 = r9.split(r12)     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r1.i     // Catch:{ NullPointerException -> 0x0311 }
            r13 = r19
            r15 = r5
            r16 = r6
            r17 = r7
            r18 = r9
            com.utils.Subtitle.converter.Style r9 = r13.a(r14, r15, r16, r17, r18)     // Catch:{ NullPointerException -> 0x0311 }
            java.util.Hashtable<java.lang.String, com.utils.Subtitle.converter.Style> r11 = r1.g     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r13 = r9.f6533a     // Catch:{ NullPointerException -> 0x0311 }
            r11.put(r13, r9)     // Catch:{ NullPointerException -> 0x0311 }
        L_0x02f0:
            int r6 = r6 + 1
            java.lang.String r9 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            java.lang.String r9 = r9.trim()     // Catch:{ NullPointerException -> 0x0311 }
            r15 = r19
            goto L_0x02be
        L_0x02fd:
            r5 = r9
            goto L_0x0029
        L_0x0300:
            java.lang.String r5 = r4.readLine()     // Catch:{ NullPointerException -> 0x0311 }
            int r6 = r6 + 1
            goto L_0x0029
        L_0x0308:
            r1.a()     // Catch:{ NullPointerException -> 0x0311 }
        L_0x030b:
            r21.close()
            goto L_0x0327
        L_0x030f:
            r0 = move-exception
            goto L_0x032a
        L_0x0311:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ all -> 0x030f }
            r0.<init>()     // Catch:{ all -> 0x030f }
            java.lang.String r4 = r1.i     // Catch:{ all -> 0x030f }
            r0.append(r4)     // Catch:{ all -> 0x030f }
            java.lang.String r4 = "unexpected end of file, maybe last caption is not complete.\n\n"
            r0.append(r4)     // Catch:{ all -> 0x030f }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x030f }
            r1.i = r0     // Catch:{ all -> 0x030f }
            goto L_0x030b
        L_0x0327:
            r1.l = r2
            return r1
        L_0x032a:
            r21.close()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Subtitle.converter.FormatASS.a(java.lang.String, java.io.InputStream, java.lang.String):com.utils.Subtitle.converter.TimedTextObject");
    }

    public String[] a(TimedTextObject timedTextObject) {
        String str;
        String str2;
        int i;
        int i2;
        int i3;
        int i4;
        int i5;
        String str3;
        if (!timedTextObject.l) {
            return null;
        }
        ArrayList arrayList = new ArrayList(timedTextObject.g.size() + 30 + timedTextObject.h.size());
        arrayList.add(0, "[Script Info]");
        String str4 = timedTextObject.f6535a;
        if (str4 == null || str4.isEmpty()) {
            str = "Title: " + timedTextObject.e;
        } else {
            str = "Title: " + timedTextObject.f6535a;
        }
        arrayList.add(1, str);
        String str5 = timedTextObject.d;
        if (str5 == null || str5.isEmpty()) {
            str2 = "Original Script: " + "Unknown";
        } else {
            str2 = "Original Script: " + timedTextObject.d;
        }
        arrayList.add(2, str2);
        String str6 = timedTextObject.c;
        if (str6 == null || str6.isEmpty()) {
            i = 3;
        } else {
            i = 4;
            arrayList.add(3, "; " + timedTextObject.c);
        }
        String str7 = timedTextObject.b;
        if (str7 != null && !str7.isEmpty()) {
            arrayList.add(i, "; " + timedTextObject.b);
            i++;
        }
        int i6 = i + 1;
        arrayList.add(i, "; Converted by the Online Subtitle Converter developed by J. David Requejo");
        if (timedTextObject.j) {
            i2 = i6 + 1;
            arrayList.add(i6, "Script Type: V4.00+");
        } else {
            i2 = i6 + 1;
            arrayList.add(i6, "Script Type: V4.00");
        }
        int i7 = i2 + 1;
        arrayList.add(i2, "Collisions: Normal");
        int i8 = i7 + 1;
        arrayList.add(i7, "Timer: 100,0000");
        if (timedTextObject.j) {
            arrayList.add(i8, "WrapStyle: 1");
            i8++;
        }
        int i9 = i8 + 1;
        arrayList.add(i8, "");
        if (timedTextObject.j) {
            i3 = i9 + 1;
            arrayList.add(i9, "[V4+ Styles]");
        } else {
            i3 = i9 + 1;
            arrayList.add(i9, "[V4 Styles]");
        }
        if (timedTextObject.j) {
            i4 = i3 + 1;
            arrayList.add(i3, "Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding");
        } else {
            i4 = i3 + 1;
            arrayList.add(i3, "Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, TertiaryColour, BackColour, Bold, Italic, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, AlphaLevel, Encoding");
        }
        for (Style next : timedTextObject.g.values()) {
            String str8 = ((((((("Style: " + next.f6533a + ",") + next.b + ",") + next.c + ",") + a(timedTextObject.j, next)) + b(timedTextObject.j, next)) + "1,2,2,") + a(timedTextObject.j, next.f)) + ",0,0,0,";
            if (!timedTextObject.j) {
                str8 = str8 + "0,";
            }
            arrayList.add(i4, str8 + "0");
            i4++;
        }
        int i10 = i4 + 1;
        arrayList.add(i4, "");
        int i11 = i10 + 1;
        arrayList.add(i10, "[Events]");
        if (timedTextObject.j) {
            i5 = i11 + 1;
            arrayList.add(i11, "Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
        } else {
            i5 = i11 + 1;
            arrayList.add(i11, "Format: Marked, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text");
        }
        for (Caption next2 : timedTextObject.h.values()) {
            int i12 = timedTextObject.k;
            if (i12 != 0) {
                next2.b.f6534a += i12;
                next2.c.f6534a += i12;
            }
            String str9 = ("Dialogue: 0," + next2.b.a("h:mm:ss.cs") + ",") + next2.c.a("h:mm:ss.cs") + ",";
            int i13 = timedTextObject.k;
            if (i13 != 0) {
                next2.b.f6534a -= i13;
                next2.c.f6534a -= i13;
            }
            if (next2.f6532a != null) {
                str3 = str9 + next2.f6532a.f6533a;
            } else {
                str3 = str9 + CBLocation.LOCATION_DEFAULT;
            }
            arrayList.add(i5, (str3 + ",,0000,0000,0000,,") + next2.d.replaceAll("<br />", "�N").replaceAll("\\<.*?\\>", "").replace(65533, '\\'));
            i5++;
        }
        arrayList.add(i5, "");
        String[] strArr = new String[arrayList.size()];
        for (int i14 = 0; i14 < strArr.length; i14++) {
            strArr[i14] = (String) arrayList.get(i14);
        }
        return strArr;
    }

    private Style a(String[] strArr, String[] strArr2, int i, boolean z, String str) {
        String str2;
        String[] strArr3 = strArr;
        String[] strArr4 = strArr2;
        int i2 = i;
        Style style = new Style(Style.a());
        if (strArr3.length != strArr4.length) {
            str + "incorrectly formated line at " + i2 + "\n\n";
        } else {
            String str3 = str;
            int i3 = 0;
            while (i3 < strArr4.length) {
                if (strArr4[i3].trim().equalsIgnoreCase("Name")) {
                    style.f6533a = strArr3[i3].trim();
                } else if (strArr4[i3].trim().equalsIgnoreCase("Fontname")) {
                    style.b = strArr3[i3].trim();
                } else if (strArr4[i3].trim().equalsIgnoreCase("Fontsize")) {
                    style.c = strArr3[i3].trim();
                } else if (strArr4[i3].trim().equalsIgnoreCase("PrimaryColour")) {
                    String trim = strArr3[i3].trim();
                    if (z) {
                        if (trim.startsWith("&H")) {
                            style.d = Style.a("&HAABBGGRR", trim);
                        } else {
                            style.d = Style.a("decimalCodedAABBGGRR", trim);
                        }
                    } else if (trim.startsWith("&H")) {
                        style.d = Style.a("&HBBGGRR", trim);
                    } else {
                        style.d = Style.a("decimalCodedBBGGRR", trim);
                    }
                } else if (strArr4[i3].trim().equalsIgnoreCase("BackColour")) {
                    String trim2 = strArr3[i3].trim();
                    if (z) {
                        if (trim2.startsWith("&H")) {
                            style.e = Style.a("&HAABBGGRR", trim2);
                        } else {
                            style.e = Style.a("decimalCodedAABBGGRR", trim2);
                        }
                    } else if (trim2.startsWith("&H")) {
                        style.e = Style.a("&HBBGGRR", trim2);
                    } else {
                        style.e = Style.a("decimalCodedBBGGRR", trim2);
                    }
                } else if (strArr4[i3].trim().equalsIgnoreCase("Bold")) {
                    style.h = Boolean.parseBoolean(strArr3[i3].trim());
                } else if (strArr4[i3].trim().equalsIgnoreCase("Italic")) {
                    style.g = Boolean.parseBoolean(strArr3[i3].trim());
                } else if (strArr4[i3].trim().equalsIgnoreCase("Underline")) {
                    style.i = Boolean.parseBoolean(strArr3[i3].trim());
                } else if (strArr4[i3].trim().equalsIgnoreCase("Alignment")) {
                    int parseInt = Integer.parseInt(strArr3[i3].trim());
                    if (!z) {
                        switch (parseInt) {
                            case 1:
                                style.f = "mid-left";
                                continue;
                            case 2:
                                style.f = "mid-center";
                                continue;
                            case 3:
                                style.f = "mid-right";
                                continue;
                            case 5:
                                style.f = "top-left";
                                continue;
                            case 6:
                                style.f = "top-center";
                                continue;
                            case 7:
                                style.f = "top-right";
                                continue;
                            case 9:
                                style.f = "bottom-left";
                                continue;
                            case 10:
                                style.f = "bottom-center";
                                continue;
                            case 11:
                                style.f = "bottom-right";
                                continue;
                            default:
                                str2 = str3 + "undefined alignment for style at line " + i2 + "\n\n";
                                break;
                        }
                    } else {
                        switch (parseInt) {
                            case 1:
                                style.f = "bottom-left";
                                continue;
                            case 2:
                                style.f = "bottom-center";
                                continue;
                            case 3:
                                style.f = "bottom-right";
                                continue;
                            case 4:
                                style.f = "mid-left";
                                continue;
                            case 5:
                                style.f = "mid-center";
                                continue;
                            case 6:
                                style.f = "mid-right";
                                continue;
                            case 7:
                                style.f = "top-left";
                                continue;
                            case 8:
                                style.f = "top-center";
                                continue;
                            case 9:
                                style.f = "top-right";
                                continue;
                            default:
                                str2 = str3 + "undefined alignment for style at line " + i2 + "\n\n";
                                break;
                        }
                    }
                    str3 = str2;
                }
                i3++;
                strArr3 = strArr;
            }
        }
        return style;
    }

    private Caption a(String[] strArr, String[] strArr2, float f, TimedTextObject timedTextObject) {
        Caption caption = new Caption();
        caption.d = strArr[9].replaceAll("\\{.*?\\}", "").replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "<br />").replace("\\N", "<br />");
        for (int i = 0; i < strArr2.length; i++) {
            if (strArr2[i].trim().equalsIgnoreCase("Style")) {
                Style style = timedTextObject.g.get(strArr[i].trim());
                if (style != null) {
                    caption.f6532a = style;
                } else {
                    timedTextObject.i += "undefined style: " + strArr[i].trim() + "\n\n";
                }
            } else if (strArr2[i].trim().equalsIgnoreCase("Start")) {
                caption.b = new Time("h:mm:ss.cs", strArr[i].trim());
            } else if (strArr2[i].trim().equalsIgnoreCase("End")) {
                caption.c = new Time("h:mm:ss.cs", strArr[i].trim());
            }
        }
        if (f != 100.0f) {
            Time time = caption.b;
            float f2 = f / 100.0f;
            time.f6534a = (int) (((float) time.f6534a) / f2);
            Time time2 = caption.c;
            time2.f6534a = (int) (((float) time2.f6534a) / f2);
        }
        return caption;
    }

    private String a(boolean z, Style style) {
        if (z) {
            return Integer.parseInt("00" + style.d.substring(4, 6) + style.d.substring(2, 4) + style.d.substring(0, 2), 16) + ",16777215,0," + Long.parseLong("80" + style.e.substring(4, 6) + style.e.substring(2, 4) + style.e.substring(0, 2), 16) + ",";
        }
        String str = style.d.substring(4, 6) + style.d.substring(2, 4) + style.d.substring(0, 2);
        String str2 = style.e.substring(4, 6) + style.e.substring(2, 4) + style.e.substring(0, 2);
        return Long.parseLong(str, 16) + ",16777215,0," + Long.parseLong(str2, 16) + ",";
    }

    private int a(boolean z, String str) {
        String str2 = str;
        if (z) {
            if ("bottom-left".equals(str2)) {
                return 1;
            }
            if (!"bottom-center".equals(str2)) {
                if ("bottom-right".equals(str2)) {
                    return 3;
                }
                if ("mid-left".equals(str2)) {
                    return 4;
                }
                if ("mid-center".equals(str2)) {
                    return 5;
                }
                if ("mid-right".equals(str2)) {
                    return 6;
                }
                if ("top-left".equals(str2)) {
                    return 7;
                }
                if ("top-center".equals(str2)) {
                    return 8;
                }
                if ("top-right".equals(str2)) {
                    return 9;
                }
            }
            return 2;
        } else if ("bottom-left".equals(str2)) {
            return 9;
        } else {
            if (!"bottom-center".equals(str2)) {
                if ("bottom-right".equals(str2)) {
                    return 11;
                }
                if ("mid-left".equals(str2)) {
                    return 1;
                }
                if ("mid-center".equals(str2)) {
                    return 2;
                }
                if ("mid-right".equals(str2)) {
                    return 3;
                }
                if ("top-left".equals(str2)) {
                    return 5;
                }
                if ("top-center".equals(str2)) {
                    return 6;
                }
                if ("top-right".equals(str2)) {
                    return 7;
                }
            }
            return 10;
        }
    }
}
