package com.utils;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.List;

public class ImdbSearchSuggestionModel {
    private List<DBean> d;
    private String q;
    private int v;

    public static class DBean implements Parcelable {
        public static final Parcelable.Creator<DBean> CREATOR = new Parcelable.Creator<DBean>() {
            public DBean createFromParcel(Parcel parcel) {
                return new DBean(parcel);
            }

            public DBean[] newArray(int i) {
                return new DBean[i];
            }
        };
        private List<String> i;
        private String id;
        private String l;
        private String q;
        private String s;
        private int y;

        public DBean() {
        }

        public int describeContents() {
            return 0;
        }

        public List<String> getI() {
            return this.i;
        }

        public String getId() {
            return this.id;
        }

        public String getL() {
            return this.l;
        }

        public String getQ() {
            return this.q;
        }

        public String getS() {
            return this.s;
        }

        public int getY() {
            return this.y;
        }

        public void setI(List<String> list) {
            this.i = list;
        }

        public void setId(String str) {
            this.id = str;
        }

        public void setL(String str) {
            this.l = str;
        }

        public void setQ(String str) {
            this.q = str;
        }

        public void setS(String str) {
            this.s = str;
        }

        public void setY(int i2) {
            this.y = i2;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeStringList(this.i);
            parcel.writeString(this.id);
            parcel.writeString(this.l);
            parcel.writeString(this.q);
            parcel.writeString(this.s);
            parcel.writeInt(this.y);
        }

        protected DBean(Parcel parcel) {
            this.i = parcel.createStringArrayList();
            this.id = parcel.readString();
            this.l = parcel.readString();
            this.q = parcel.readString();
            this.s = parcel.readString();
            this.y = parcel.readInt();
        }
    }

    public List<DBean> getD() {
        return this.d;
    }

    public String getQ() {
        return this.q;
    }

    public int getV() {
        return this.v;
    }

    public void setD(List<DBean> list) {
        this.d = list;
    }

    public void setQ(String str) {
        this.q = str;
    }

    public void setV(int i) {
        this.v = i;
    }
}
