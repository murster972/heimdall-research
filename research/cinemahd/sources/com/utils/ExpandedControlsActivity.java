package com.utils;

import android.content.Context;
import android.view.Menu;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.media.widget.ExpandedControllerActivity;
import com.yoku.marumovie.R;

public class ExpandedControlsActivity extends ExpandedControllerActivity {
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.expand_controller, menu);
        CastButtonFactory.a((Context) this, menu, (int) R.id.media_route_menu_item);
        return true;
    }
}
