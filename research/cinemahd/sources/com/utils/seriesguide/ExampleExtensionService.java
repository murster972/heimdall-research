package com.utils.seriesguide;

import android.content.Intent;
import com.battlelancer.seriesguide.api.Action;
import com.battlelancer.seriesguide.api.Episode;
import com.battlelancer.seriesguide.api.Movie;
import com.battlelancer.seriesguide.api.SeriesGuideExtension;
import com.database.entitys.MovieEntity;
import com.google.gson.Gson;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.tmvdb.FindResult;
import com.movie.ui.activity.MovieDetailsActivity;
import com.movie.ui.activity.SourceActivity;
import com.original.tase.Logger;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.joda.time.DateTime;

public class ExampleExtensionService extends SeriesGuideExtension {
    public ExampleExtensionService() {
        super("ExampleExtension");
    }

    /* access modifiers changed from: protected */
    public void a(int i, final Episode episode) {
        Logger.a("ExampleExtensionService", "onRequest: episode " + episode.h().toString());
        Utils.a98c();
        String d = episode.d();
        if (d != null && !d.isEmpty() && d.length() >= 4) {
            d = d.trim().substring(0, 4);
            if (!d.contains("-")) {
                boolean b = com.original.tase.utils.Utils.b(d);
            }
        }
        String str = d;
        String f = episode.f();
        String a2 = Regex.a(f, "(.*?)\\s+\\(\\d{4}\\)", 1);
        MovieInfo movieInfo = new MovieInfo(!a2.isEmpty() ? a2 : f, str, String.valueOf(episode.c()), String.valueOf(episode.b()), "-1");
        try {
            if (episode.e() != null && !episode.e().isEmpty()) {
                String lowerCase = episode.e().toLowerCase();
                if (!lowerCase.startsWith("tt")) {
                    lowerCase = "tt" + lowerCase;
                }
                movieInfo.imdbIDStr = lowerCase;
            }
        } catch (Exception e) {
            Logger.a((Throwable) e, new boolean[0]);
        }
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setName(episode.f());
        movieEntity.setNumberSeason(episode.c().intValue());
        movieEntity.setRealeaseDate(episode.d());
        movieEntity.setTmdbID(0);
        movieEntity.setTvdbID((long) episode.g().intValue());
        movieEntity.setImdbIDStr(episode.a());
        movieEntity.setTV(true);
        Observable.create(new ObservableOnSubscribe<FindResult>(this) {
            public void subscribe(ObservableEmitter<FindResult> observableEmitter) throws Exception {
                FindResult c = Utils.c(episode.a());
                if (c != null) {
                    observableEmitter.onNext(c);
                }
                observableEmitter.onComplete();
            }
        }).observeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new c(movieEntity), new a(this, movieInfo, movieEntity, i), new b(this, movieInfo, movieEntity, i));
    }

    public /* synthetic */ void a(MovieInfo movieInfo, MovieEntity movieEntity, int i, Throwable th) throws Exception {
        String a2 = new Gson().a((Object) movieInfo, (Type) MovieInfo.class);
        String a3 = new Gson().a((Object) movieEntity, (Type) MovieEntity.class);
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("MovieInfo", a2);
        intent.putExtra("isFromAnotherApp", true);
        intent.putExtra("Movie", a3);
        Action.Builder builder = new Action.Builder("Play Show on Cinema HD", i);
        builder.a(intent);
        a(builder.a());
    }

    public /* synthetic */ void a(MovieInfo movieInfo, MovieEntity movieEntity, int i) throws Exception {
        String a2 = new Gson().a((Object) movieInfo, (Type) MovieInfo.class);
        String a3 = new Gson().a((Object) movieEntity, (Type) MovieEntity.class);
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("MovieInfo", a2);
        intent.putExtra("isFromAnotherApp", true);
        intent.putExtra("Movie", a3);
        Action.Builder builder = new Action.Builder("Play Show on Cinema HD", i);
        builder.a(intent);
        a(builder.a());
    }

    /* access modifiers changed from: protected */
    public void a(int i, Movie movie) {
        Utils.a98c();
        Date b = movie.b();
        if (b != null) {
            try {
                Calendar instance = Calendar.getInstance(TimeZone.getTimeZone("America/Los_Angeles"));
                if (instance != null) {
                    instance.setTime(b);
                    instance.get(1);
                } else {
                    b.getYear();
                }
            } catch (Exception e) {
                Logger.a((Throwable) e, new boolean[0]);
            }
        }
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setTV(false);
        movieEntity.setTmdbID((long) movie.d().intValue());
        movieEntity.setImdbIDStr(movie.a());
        movieEntity.setRealeaseDate(DateTimeHelper.b(new DateTime((Object) movie.b())));
        movieEntity.setName(movie.c());
        movieEntity.setOverview(movie.c());
        String a2 = new Gson().a((Object) movieEntity, (Type) MovieEntity.class);
        Intent intent = new Intent(this, MovieDetailsActivity.class);
        intent.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", a2);
        intent.putExtra("isFromAnotherApp", true);
        Action.Builder builder = new Action.Builder("Play Movie on Cinema HDMovies", i);
        builder.a(intent);
        a(builder.a());
    }
}
