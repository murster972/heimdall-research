package com.utils.seriesguide;

import com.database.entitys.MovieEntity;
import com.movie.data.model.MovieInfo;
import io.reactivex.functions.Action;

/* compiled from: lambda */
public final /* synthetic */ class b implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ExampleExtensionService f6564a;
    private final /* synthetic */ MovieInfo b;
    private final /* synthetic */ MovieEntity c;
    private final /* synthetic */ int d;

    public /* synthetic */ b(ExampleExtensionService exampleExtensionService, MovieInfo movieInfo, MovieEntity movieEntity, int i) {
        this.f6564a = exampleExtensionService;
        this.b = movieInfo;
        this.c = movieEntity;
        this.d = i;
    }

    public final void run() {
        this.f6564a.a(this.b, this.c, this.d);
    }
}
