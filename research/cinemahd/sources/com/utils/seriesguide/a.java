package com.utils.seriesguide;

import com.database.entitys.MovieEntity;
import com.movie.data.model.MovieInfo;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ExampleExtensionService f6563a;
    private final /* synthetic */ MovieInfo b;
    private final /* synthetic */ MovieEntity c;
    private final /* synthetic */ int d;

    public /* synthetic */ a(ExampleExtensionService exampleExtensionService, MovieInfo movieInfo, MovieEntity movieEntity, int i) {
        this.f6563a = exampleExtensionService;
        this.b = movieInfo;
        this.c = movieEntity;
        this.d = i;
    }

    public final void accept(Object obj) {
        this.f6563a.a(this.b, this.c, this.d, (Throwable) obj);
    }
}
