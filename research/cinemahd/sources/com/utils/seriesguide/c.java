package com.utils.seriesguide;

import com.database.entitys.MovieEntity;
import com.movie.data.model.tmvdb.FindResult;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieEntity f6565a;

    public /* synthetic */ c(MovieEntity movieEntity) {
        this.f6565a = movieEntity;
    }

    public final void accept(Object obj) {
        this.f6565a.setTmdbID(((FindResult) obj).getTv_episode_results().get(0).getShow_id());
    }
}
