package com.utils.Getlink.Resolver;

import com.facebook.common.util.UriUtil;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;

public class Mp4Upload extends BaseResolver {
    public String a() {
        return "Mp4Upload";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(mp4upload\\.(?:com))/(?:embed-)?([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            HttpHelper e = HttpHelper.e();
            Iterator<String> it2 = JsUnpacker.m30918(e.a("https://www.mp4upload.com/embed-" + a2 + ".html", (Map<String, String>[]) new Map[0])).iterator();
            while (it2.hasNext()) {
                String next = it2.next();
                Iterator it3 = Regex.b(next, "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
                if (!it3.hasNext()) {
                    it3 = Regex.b(next, "player.src\\(['\"]([^'\"]+[^'\"]*)['\"]\\)", 1, 2).get(0).iterator();
                }
                while (it3.hasNext()) {
                    String str = (String) it3.next();
                    if (!str.isEmpty() && !str.endsWith(".srt") && !str.endsWith(".vtt") && !str.endsWith(".png") && !str.endsWith(".jpg") && str.contains(UriUtil.HTTP_SCHEME)) {
                        observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str, mediaSource.getQuality())));
                    }
                }
            }
        }
    }
}
