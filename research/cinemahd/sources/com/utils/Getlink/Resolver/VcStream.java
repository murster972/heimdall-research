package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class VcStream extends BaseResolver {
    public String a() {
        return "VcStream";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        MediaSource mediaSource2 = mediaSource;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String a2 = Utils.a(mediaSource.getStreamLink(), "(?://|\\.)(vcstream\\.to)/(?:embed|f)/([0-9a-zA-Z-_]+)", 2, 2);
        String str = "https://vcstream.to/player?fid=" + a2 + "&page=embed";
        if (!a2.isEmpty()) {
            String a3 = Regex.a(HttpHelper.e().b(str, mediaSource.getStreamLink(), new Map[0]), "['\"]html['\"]\\s*:\\s*['\"](.*)['\"][^\\}]*\\}", 1, 2);
            if (!a3.isEmpty()) {
                CharSequence charSequence = "\\\\n";
                CharSequence charSequence2 = "/";
                CharSequence charSequence3 = "\\\\r";
                ArrayList<ArrayList<String>> b = Regex.b(a3.replace("\\\"", "\"").replace("\\'", "'").replace("\\\\\"", "\"").replace("\\\\'", "'").replace("\\r", "").replace("\\n", "").replace("\\\\r", "").replace("\\\\n", "").replace("\\\\\\/", "/").replace("\\/", "/").replace("/\\", "/"), "['\"]?src['\"]\\s*:\\s*['\"]([^'\"]+)['\"]\\s*[^\\}]+\\s*['\"]?label['\"]?\\s*:\\s*['\"]?(\\d+)p?", 2, true);
                ArrayList arrayList = b.get(0);
                ArrayList arrayList2 = b.get(1);
                if (arrayList.size() > 0) {
                    for (int i = 0; i < arrayList.size(); i++) {
                        try {
                            String str2 = (String) arrayList.get(i);
                            String str3 = (String) arrayList2.get(i);
                            if (!str2.isEmpty() && !str2.contains("/logo")) {
                                observableEmitter2.onNext(BaseResolver.a(mediaSource2, new ResolveResult(a(), str2, str3)));
                            }
                        } catch (Exception e) {
                            Logger.a((Throwable) e, new boolean[0]);
                        }
                    }
                    return;
                }
                CharSequence charSequence4 = charSequence2;
                ArrayList arrayList3 = Regex.b(a3.replace("\\\"", "\"").replace("\\'", "'").replace("\\\\\"", "\"").replace("\\\\'", "'").replace("\\r", "").replace("\\n", "").replace(charSequence3, "").replace(charSequence, "").replace("\\\\\\/", charSequence4).replace("\\/", charSequence4).replace("/\\", charSequence4), "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 2, true).get(0);
                HashMap hashMap = new HashMap();
                hashMap.put("Referer", mediaSource.getStreamLink());
                hashMap.put("Origin", "https://vcstream.to");
                hashMap.put("User-Agent", Constants.f5838a);
                for (int i2 = 0; i2 < arrayList3.size(); i2++) {
                    try {
                        String str4 = (String) arrayList3.get(i2);
                        if (!str4.isEmpty() && !str4.contains("/logo") && !str4.endsWith(".vtt")) {
                            observableEmitter2.onNext(BaseResolver.a(mediaSource2, new ResolveResult(a(), str4, "")));
                        }
                    } catch (Exception e2) {
                        Logger.a((Throwable) e2, new boolean[0]);
                    }
                }
            }
        }
    }
}
