package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import io.reactivex.ObservableEmitter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Fembed extends BaseResolver {
    public String a() {
        return "Fembed";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?:\\/|\\.)(fembed|24hd|gcloud|t21|feurl|club|bmoviesfree|mediashore)\\.(?:com|be|live|link|io)\\/(?:v|f)\\/([0-9a-zA-Z-]+)", 2, 2);
        if (!a2.isEmpty()) {
            String i = BaseProvider.i(streamLink);
            try {
                i = new URL(streamLink).getHost();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            String str = i;
            HashMap<String, String> a3 = Constants.a();
            a3.put("origin", "https://" + str);
            a3.put("referer", streamLink);
            HttpHelper e = HttpHelper.e();
            String replace = e.a("https://" + str + "/api/source/" + a2, "r=&d=" + str, (Map<String, String>[]) new Map[]{a3}).replace("\\/", "/");
            ArrayList arrayList = new ArrayList();
            arrayList.add(replace);
            if (JsUnpacker.m30920(replace)) {
                arrayList.addAll(JsUnpacker.m30916(replace));
            }
            Iterator<ResolveResult> it2 = a(streamLink, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            HashMap hashMap = new HashMap();
            hashMap.put("accept", "*/*");
            hashMap.put("origin", "https://" + str);
            hashMap.put("referer", streamLink);
            hashMap.put("user-agent", Constants.f5838a);
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                next.setPlayHeader(hashMap);
                observableEmitter.onNext(BaseResolver.a(mediaSource, next));
            }
        }
    }
}
