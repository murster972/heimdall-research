package com.utils.Getlink.Resolver;

public class EnterVideo extends GenericResolver {
    public String a() {
        return "EnterVideo";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return "http://entervideo.net/watch/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "(?://|\\.)(entervideo\\.net)/(?:watch/)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "http://entervideo.net";
    }
}
