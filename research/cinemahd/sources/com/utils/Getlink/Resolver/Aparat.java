package com.utils.Getlink.Resolver;

public class Aparat extends GenericResolver {
    public String a() {
        return "Aparat";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?:\\/|\\.)(aparat)\\.(?:cam|com|be|co|to|cc)\\/(?:embed-)([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://aparat.cam";
    }
}
