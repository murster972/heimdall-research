package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class GoVid extends BaseResolver {
    public String a() {
        return "GoVid";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        Iterator it2 = Jsoup.b(HttpHelper.e().a(mediaSource.getStreamLink(), (Map<String, String>[]) new Map[0])).g("div.mb-3").b("li").iterator();
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String a2 = Regex.a(element.toString(), "<small>\\s*(\\d{3,4}p)", 1);
            String replace = element.h("a").b("href").replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "");
            String a3 = a();
            if (a2.isEmpty()) {
                a2 = "HD";
            }
            ResolveResult resolveResult = new ResolveResult(a3, replace, a2);
            resolveResult.setPlayHeader(hashMap);
            observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
        }
    }
}
