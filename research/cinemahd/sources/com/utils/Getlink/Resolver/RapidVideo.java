package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RapidVideo extends PremiumResolver {
    public String a() {
        return "RapidVideo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        super.a(mediaSource, observableEmitter);
        if (!Utils.d) {
            String a2 = Regex.a(streamLink, "(?://|\\.)((?:rapidvideo|raptu|rapidvid|bitporno)\\.(?:to|com))/(?:[ev]/|view|embed/|\\?v=)?([0-9A-Za-z]+)", 1);
            String a3 = Regex.a(streamLink, "(?://|\\.)((?:rapidvideo|raptu|rapidvid|bitporno)\\.(?:to|com))/(?:[ev]/|view|embed/|\\?v=)?([0-9A-Za-z]+)", 2);
            if (!a2.isEmpty() && !a3.isEmpty()) {
                String str = "https://" + a2 + "/v/" + a3;
                String a4 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
                if (!a4.contains("We can't find the file you are looking for")) {
                    Iterator<ResolveResult> it2 = a(str, a4, false, (HashMap<String, String>) null, new String[0][]).iterator();
                    if (it2.hasNext()) {
                        while (it2.hasNext()) {
                            observableEmitter.onNext(BaseResolver.a(mediaSource, it2.next()));
                        }
                        return;
                    }
                    String str2 = "https://" + a2 + "/embed/" + a3;
                    String a5 = HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0]);
                    ArrayList arrayList = new ArrayList();
                    try {
                        Iterator it3 = Regex.b(a5, "<a.*?href=['\"]http.*?" + a3 + ".*?(?:\\?|&)q=(\\d{3,4})p", 1, true).get(0).iterator();
                        while (it3.hasNext()) {
                            String str3 = ((String) it3.next()) + "p";
                            arrayList.add(str2 + String.format("?q=%s", new Object[]{str3}));
                        }
                    } catch (Exception e) {
                        Logger.a((Throwable) e, new boolean[0]);
                    }
                    if (arrayList.isEmpty()) {
                        arrayList.add(str2);
                    }
                    Collections.reverse(arrayList);
                    a(observableEmitter, arrayList, mediaSource);
                }
            }
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, List<String> list, MediaSource mediaSource) {
        for (String next : list) {
            HashMap<String, String> a2 = BaseResolver.a(HttpHelper.e().a(next, (Map<String, String>[]) new Map[0]), (String) null);
            a2.put("confirm.y", String.valueOf(new Random().nextInt(120)));
            a2.put("confirm.x", String.valueOf(new Random().nextInt(120)));
            String str = next + "#";
            String a3 = com.original.tase.utils.Utils.a((Map<String, String>) a2);
            HashMap hashMap = new HashMap();
            hashMap.put("Referer", next);
            Iterator<ResolveResult> it2 = a(str, HttpHelper.e().a(str, a3, true, (Map<String, String>[]) new Map[]{hashMap}), true, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it2.hasNext()) {
                ResolveResult next2 = it2.next();
                if (next2.getResolvedQuality().contains("999")) {
                    if (mediaSource.getQuality().equals("CAM")) {
                        next2.setResolvedQuality("CAM");
                    } else {
                        next2.setResolvedQuality("HD");
                    }
                } else if (mediaSource.getQuality().equals("CAM")) {
                    next2.setResolvedQuality("CAM " + next2.getResolvedQuality());
                }
                observableEmitter.onNext(BaseResolver.a(mediaSource, next2));
            }
        }
    }
}
