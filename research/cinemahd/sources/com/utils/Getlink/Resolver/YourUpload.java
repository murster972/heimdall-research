package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class YourUpload extends BaseResolver {
    public String a() {
        return "YourUpload";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(yourupload\\.com|yucache\\.net)/(?:watch|embed)?/?([0-9A-Za-z]+)", 2);
        if (!a2.isEmpty()) {
            String str = "https://www.yourupload.com/embed/" + a2;
            ArrayList arrayList = new ArrayList();
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
            arrayList.add(a3);
            if (JsUnpacker.m30920(a3)) {
                arrayList.addAll(JsUnpacker.m30916(a3));
            }
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", Constants.f5838a);
            hashMap.put("Referer", str);
            hashMap.put("Cookie", HttpHelper.e().a(str));
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                Iterator it3 = Regex.b((String) it2.next(), "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
                while (it3.hasNext()) {
                    String str2 = (String) it3.next();
                    if (!str2.isEmpty() && !str2.endsWith(".srt") && !str2.endsWith(".vtt") && !str2.endsWith(".png") && !str2.endsWith(".jpg")) {
                        ResolveResult resolveResult = new ResolveResult(a(), str2, "HD");
                        resolveResult.setPlayHeader(hashMap);
                        observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
                    }
                }
            }
        }
    }
}
