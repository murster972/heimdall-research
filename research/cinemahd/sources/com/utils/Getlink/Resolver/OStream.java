package com.utils.Getlink.Resolver;

public class OStream extends GenericResolver {
    public String a() {
        return "OStream";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return "https://estream.to/" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "(?://|\\.)(ostream\\.tv)/([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "http://ostream.tv/f/";
    }
}
