package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Provider.BaseProvider;
import io.reactivex.ObservableEmitter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ABCvideo extends BaseResolver {
    public String a() {
        return "ABCvideo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        MediaSource mediaSource2 = mediaSource;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?:\\/|\\.)(abcvideo)\\.(?:com|be|co|to|cc)\\/(?:embed-)([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            String i = BaseProvider.i(streamLink);
            try {
                i = new URL(streamLink).getHost();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            String str = "https://" + i + "/" + a2;
            HashMap hashMap = new HashMap();
            hashMap.put("referer", str);
            hashMap.put("User-Agent", Constants.f5838a);
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
            String a4 = Regex.a(a3, "\\(['\"]([0-9a-zA-Z-]+)['\"]\\,['\"]([0-9a-zA-Z-]+)['\"]\\,['\"]([0-9a-zA-Z-]+)['\"]\\)", 1);
            String a5 = Regex.a(a3, "\\(['\"]([0-9a-zA-Z-]+)['\"]\\,['\"]([0-9a-zA-Z-]+)['\"]\\,['\"]([0-9a-zA-Z-]+)['\"]\\)", 2);
            String a6 = Regex.a(a3, "\\(['\"]([0-9a-zA-Z-]+)['\"]\\,['\"]([0-9a-zA-Z-]+)['\"]\\,['\"]([0-9a-zA-Z-]+)['\"]\\)", 3);
            ArrayList arrayList = new ArrayList();
            arrayList.add(a3);
            if (JsUnpacker.m30920(a3)) {
                arrayList.addAll(JsUnpacker.m30916(a3));
            }
            String a7 = HttpHelper.e().a("https://" + i + "/dl?op=download_orig" + String.format("&id=%s&mode=%s&hash=%s", new Object[]{a4, a5, a6}), (Map<String, String>[]) new Map[]{hashMap});
            Iterator it2 = Regex.b(a7, "href=['\"]([^'\"]+(mkv|mp4|avi))['\"]", 1, true).get(0).iterator();
            while (it2.hasNext()) {
                ResolveResult resolveResult = new ResolveResult(a(), (String) it2.next(), mediaSource.getQuality());
                resolveResult.setPlayHeader(hashMap);
                observableEmitter2.onNext(BaseResolver.a(mediaSource2, resolveResult));
            }
            if (JsUnpacker.m30920(a7)) {
                arrayList.addAll(JsUnpacker.m30916(a7));
            }
            Iterator<ResolveResult> it3 = a(str, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            while (it3.hasNext()) {
                ResolveResult next = it3.next();
                if (next.getResolvedQuality() == null || !next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality("HD");
                } else {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                next.setPlayHeader(hashMap);
                observableEmitter2.onNext(BaseResolver.a(mediaSource2, next));
            }
        }
    }
}
