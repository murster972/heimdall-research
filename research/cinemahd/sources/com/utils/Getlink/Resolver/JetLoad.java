package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;

public class JetLoad extends BaseResolver {
    public String a() {
        return "JetLoad";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = HttpHelper.e().a(streamLink, (Map<String, String>[]) new Map[0]);
        HashMap hashMap = new HashMap();
        hashMap.put("Origin", "https://jetload.net");
        hashMap.put("Referer", streamLink);
        hashMap.put("User-Agent", Constants.f5838a);
        String a3 = Regex.a(a2, "x_source\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        if (!a3.isEmpty()) {
            ResolveResult resolveResult = new ResolveResult(a(), a3, mediaSource.getQuality());
            resolveResult.setPlayHeader(hashMap);
            observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
        }
        String a4 = Regex.a(a2, "<input[^>]*file_low.*value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>", 1);
        String a5 = Regex.a(a2, "<input[^>]*file_med.*value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>", 1);
        String a6 = Regex.a(a2, "<input[^>]*file_name.*value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>", 1);
        String a7 = Regex.a(a2, "<input[^>]*srv.*value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>", 1);
        String a8 = Regex.a(a2, "<input[^>]*archive.*value\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>", 1);
        if (!a7.isEmpty()) {
            if (!a8.isEmpty()) {
                a6 = "archive/" + a6;
            }
            if (a4.equals(DiskLruCache.VERSION_1) && a5.equals(DiskLruCache.VERSION_1)) {
                ResolveResult resolveResult2 = new ResolveResult(a(), a7 + "/v2/schema/" + a6 + "/master.m3u8", mediaSource.getQuality());
                resolveResult2.setPlayHeader(hashMap);
                observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult2));
            }
            if (a4.equals(DiskLruCache.VERSION_1)) {
                ResolveResult resolveResult3 = new ResolveResult(a(), a7 + "/v2/schema/" + a6 + "/med.m3u8", mediaSource.getQuality());
                resolveResult3.setPlayHeader(hashMap);
                observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult3));
                return;
            }
            ResolveResult resolveResult4 = new ResolveResult(a(), a7 + "/v2/schema/" + a6 + "/low.m3u8", mediaSource.getQuality());
            resolveResult4.setPlayHeader(hashMap);
            observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult4));
        }
    }
}
