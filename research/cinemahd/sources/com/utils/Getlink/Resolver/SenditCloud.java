package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class SenditCloud extends PremiumResolver {
    public String a() {
        return "SenditCloud";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(sendit\\.cloud)/(?:embed-)([0-9a-zA-Z]+)", 2);
        if (!a2.isEmpty()) {
            super.a(mediaSource, observableEmitter);
            if (!Utils.d) {
                String str = "https://sendit.cloud/" + a2 + ".html";
                ArrayList arrayList = new ArrayList();
                String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
                arrayList.add(a3);
                if (JsUnpacker.m30920(a3)) {
                    arrayList.addAll(JsUnpacker.m30916(a3));
                }
                Iterator<ResolveResult> it2 = a(str, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
                while (it2.hasNext()) {
                    ResolveResult next = it2.next();
                    if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        next.setResolvedQuality(mediaSource.getQuality());
                    }
                    observableEmitter.onNext(BaseResolver.a(mediaSource, next));
                }
            }
        }
    }
}
