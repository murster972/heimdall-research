package com.utils.Getlink.Resolver;

public class MixDrop extends GenericResolver {
    public String a() {
        return "MixDrop";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/e/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    public String g() {
        return "(?:\\/|\\.)(mixdrop)\\.(?:com|be|co|to)\\/(?:v|f|e)\\/([0-9a-zA-Z-]+)";
    }

    public String h() {
        return "https://mixdrop.co";
    }
}
