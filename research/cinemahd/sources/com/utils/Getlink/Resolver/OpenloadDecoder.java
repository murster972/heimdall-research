package com.utils.Getlink.Resolver;

interface OpenloadDecoder {
    String decode(String str);

    boolean isEnabled();
}
