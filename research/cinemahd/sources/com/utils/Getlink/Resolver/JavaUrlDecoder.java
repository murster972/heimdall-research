package com.utils.Getlink.Resolver;

interface JavaUrlDecoder {
    String decode(String str);
}
