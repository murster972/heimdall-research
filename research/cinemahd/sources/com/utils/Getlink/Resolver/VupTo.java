package com.utils.Getlink.Resolver;

public class VupTo extends GenericResolver {
    public String a() {
        return "CloudVideo";
    }

    public boolean f() {
        return false;
    }

    public String g() {
        return "(?://|\\.)(vup\\.(?:tv|cc|si|to|com))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://vup.to";
    }
}
