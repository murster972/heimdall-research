package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Iterator;

public class PowerVideo extends BaseResolver {
    public String a() {
        return "PowerVideo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)((?:powvideo\\.net|povw1deo\\.com))/(?:embed-|iframe-|preview-)?([0-9a-zA-Z]+)", 2);
        if (!a2.isEmpty()) {
            String str = "http://powvideo.net/iframe-" + a2 + "-954x562.html";
            String b = HttpHelper.e().b(str, str.replace("iframe-", "preview-"));
            if (!b.contains("Video is processing now") && !b.contains("File was deleted")) {
                ArrayList arrayList = new ArrayList();
                arrayList.add(b);
                if (JsUnpacker.m30920(b)) {
                    arrayList.addAll(JsUnpacker.m30916(b));
                }
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    Iterator it3 = Regex.b((String) it2.next(), "src\\s*:\\s*[\"'](http[^\"']+\\.(?:mp4|m3u8))[\"']", 1, true).get(0).iterator();
                    while (it3.hasNext()) {
                        String str2 = (String) it3.next();
                        try {
                            String a3 = Regex.a(str2, "([0-9a-z]{40,})", 1, 2);
                            String sb = new StringBuilder(a3).reverse().toString();
                            observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str2.replace(a3, sb.substring(0, 2) + sb.substring(3, sb.length())), "HQ")));
                        } catch (Throwable th) {
                            Logger.a(th, new boolean[0]);
                        }
                    }
                }
            }
        }
    }
}
