package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Vidoza extends PremiumResolver {
    public String a() {
        return "Vidoza";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String i = i();
        String h = h();
        String a2 = Regex.a(streamLink, h, 2);
        if (!a2.isEmpty()) {
            if (i.isEmpty()) {
                i = "http://" + Regex.a(streamLink, h, 1);
            }
            super.a(mediaSource, observableEmitter);
            if (!Utils.d) {
                String b = b(i, a2);
                ArrayList arrayList = new ArrayList();
                String a3 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[0]);
                arrayList.add(a3);
                if (JsUnpacker.m30920(a3)) {
                    arrayList.addAll(JsUnpacker.m30916(a3));
                }
                HashMap hashMap = null;
                if (f()) {
                    hashMap = new HashMap();
                    hashMap.put("User-Agent", Constants.f5838a);
                    hashMap.put("Referer", b);
                    hashMap.put("Cookie", HttpHelper.e().a(b));
                }
                Iterator<ResolveResult> it2 = a(b, (ArrayList<String>) arrayList, f(), (HashMap<String, String>) hashMap, g()).iterator();
                while (it2.hasNext()) {
                    ResolveResult next = it2.next();
                    if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        next.setResolvedQuality("HQ");
                    }
                    observableEmitter.onNext(BaseResolver.a(mediaSource, next));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String[] g() {
        return new String[]{"sub_id", "empty.srt"};
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "(?://|\\.)(vidoza\\.net)/(?:embed-)?([0-9a-zA-Z]+)";
    }

    /* access modifiers changed from: protected */
    public String i() {
        return "http://vidoza.net";
    }
}
