package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class NovaMov extends PremiumResolver {
    public String a() {
        return "NovaMov";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, mediaSource);
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, MediaSource mediaSource) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(novamov.com|auroravid.to)/(?:video/|embed/\\?v=|embed\\.php\\?v=)([A-Za-z0-9]+)", 2, 2);
        if (!a2.isEmpty()) {
            if (RealDebridCredentialsHelper.c().isValid()) {
                try {
                    PremiumResolver.b(mediaSource, observableEmitter, true, false, false);
                } catch (Exception e) {
                    Logger.a(e.getMessage());
                }
                if (Utils.d) {
                    return;
                }
            }
            HttpHelper e2 = HttpHelper.e();
            String a3 = e2.a("http://www.auroravid.to/embed/?v=" + a2, (Map<String, String>[]) new Map[0]);
            String a4 = Regex.a(a3, "flashvars.filekey=(.+?);", 1);
            ArrayList arrayList = Regex.b(a4, "\\s+" + a4 + "=\"(.+?)\"", 1).get(0);
            if (arrayList.size() > 0) {
                a4 = (String) arrayList.get(arrayList.size() - 1);
            }
            if (a4.trim().isEmpty()) {
                Iterator it2 = Regex.b(a3, "source src=\"(.+?)\"", 1).get(0).iterator();
                while (it2.hasNext()) {
                    String str = (String) it2.next();
                    if (!str.contains(".srt") && !str.contains(".vtt")) {
                        observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str, "HQ")));
                    }
                }
                return;
            }
            String a5 = a();
            HttpHelper e3 = HttpHelper.e();
            observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a5, Regex.a(e3.a("http://www.auroravid.to/api/player.api.php?key=" + a4 + "&file=" + a2, (Map<String, String>[]) new Map[0]), "url=(.+?)&", 1), "HQ")));
        }
    }
}
