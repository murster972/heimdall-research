package com.utils.Getlink.Resolver;

import com.facebook.common.util.UriUtil;
import com.google.gson.JsonParser;
import com.original.tase.Logger;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;

public class AmazonDrive extends BaseResolver {
    public String a() {
        return "AmazonDrive";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?://|\\.)(amazon\\.com)/clouddrive/share/([0-9a-zA-Z]+)", 2);
        if (!a2.isEmpty()) {
            String b = HttpHelper.e().b("https://www.amazon.com/drive/v1/shares/" + a2 + "?shareId=" + a2 + "&resourceVersion=V2&ContentType=JSON&_=" + DateTimeHelper.c() + "29", streamLink);
            if (!b.isEmpty()) {
                try {
                    String b2 = HttpHelper.e().b("https://www.amazon.com/drive/v1/nodes/" + new JsonParser().a(b).f().a("nodeInfo").f().a("id").i() + "/children?asset=ALL&tempLink=true&limit=1&searchOnFamily=false&shareId=" + a2 + "&offset=0&resourceVersion=V2&ContentType=JSON&_=" + DateTimeHelper.c() + "30", streamLink);
                    if (!b2.isEmpty()) {
                        try {
                            String i = new JsonParser().a(b2).f().a(UriUtil.DATA_SCHEME).e().get(0).f().a("tempLink").i();
                            if (i.startsWith("//")) {
                                i = "http:" + i;
                            } else if (i.startsWith("/")) {
                                i = "https://www.amazon.com" + i;
                            } else if (!i.startsWith(UriUtil.HTTP_SCHEME)) {
                                i = "https://www.amazon.com/" + i;
                            }
                            observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), i, "HD")));
                        } catch (Exception e) {
                            Logger.a((Throwable) e, new boolean[0]);
                        }
                    }
                } catch (Exception e2) {
                    Logger.a((Throwable) e2, new boolean[0]);
                }
            }
        }
    }
}
