package com.utils.Getlink.Resolver;

public class TheVideoBee extends GenericResolver {
    public String a() {
        return "TheVideoBee";
    }

    public boolean f() {
        return true;
    }

    public String g() {
        return "(?://|\\.)(thevideobee\\.(?:to|cc|si))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://thevideobee.to";
    }
}
