package com.utils.Getlink.Resolver.premium.services;

import com.facebook.ads.AudienceNetworkActivity;
import com.movie.data.api.premiumize.PremiumizeApi;
import com.movie.data.api.premiumize.PremiumizeModule;
import com.movie.data.model.realdebrid.MagnetObject;
import com.original.tase.Logger;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeUserApi;
import com.original.tase.model.debrid.premiumize.PremiumizeCacheCheckResponse;
import com.original.tase.model.debrid.premiumize.PremiumizeDirectDL;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

public class Premiumzie extends BaseService {
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (mediaSource.isTorrent()) {
            b(mediaSource, observableEmitter);
        } else if (PremiumizeCredentialsHelper.b().isValid()) {
            try {
                PremiumizeApi b = PremiumizeModule.b();
                String encode = URLEncoder.encode(mediaSource.getStreamLink(), AudienceNetworkActivity.WEBVIEW_ENCODING);
                PremiumizeCacheCheckResponse body = b.getPremiumizeCacheCheckResponse(PremiumizeCredentialsHelper.b().getAccessToken(), new String[]{encode}).execute().body();
                if (body != null && body.status.contains("success")) {
                    for (int i = 0; i < body.response.length; i++) {
                        String str = body.response[i];
                        String str2 = body.transcoded[i];
                        String str3 = body.filename[i];
                        String str4 = body.filesize[i];
                        if (str != null && !str.isEmpty() && !str.equals("false") && str3 != null && !str3.isEmpty() && str3 != null && !str4.isEmpty() && !str4.equals("0") && !str3.endsWith(".rar") && !str3.endsWith(".7z") && !str3.endsWith(".zip") && !str3.endsWith(".iso") && !str3.endsWith(".avi") && !str3.endsWith(".flv") && !str3.endsWith(".sub") && !str3.endsWith(".pdf") && !str3.endsWith(".mp3")) {
                            MediaSource cloneDeeply = mediaSource.cloneDeeply();
                            cloneDeeply.setFileSize(Long.parseLong(str4));
                            cloneDeeply.setPremiumize(true);
                            cloneDeeply.setFilename(str3);
                            observableEmitter.onNext(cloneDeeply);
                        }
                    }
                }
            } catch (Throwable th) {
                Logger.a("PREMIUMIZE", th.getMessage());
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (PremiumizeCredentialsHelper.b().isValid() && PremiumizeUserApi.c().b()) {
            try {
                ArrayList<MagnetObject> magnetObjects = mediaSource.getMagnetObjects();
                magnetObjects.iterator();
                ArrayList arrayList = new ArrayList();
                Iterator<MagnetObject> it2 = magnetObjects.iterator();
                while (it2.hasNext()) {
                    arrayList.add(Regex.a(it2.next().getMagnet(), "magnet:\\?xt=urn:btih:([^&.]+)", 1).toLowerCase());
                }
                PremiumizeCacheCheckResponse body = PremiumizeModule.b().getPremiumizeCacheCheckResponse(PremiumizeCredentialsHelper.b().getAccessToken(), (String[]) arrayList.toArray(new String[0])).execute().body();
                if (body != null && body.status.contains("success")) {
                    for (int i = 0; i < body.response.length; i++) {
                        String str = body.response[i];
                        String str2 = body.transcoded[i];
                        String str3 = body.filename[i];
                        String str4 = body.filesize[i];
                        MagnetObject magnetObject = magnetObjects.get(i);
                        if (str != null && !str.isEmpty() && !str.equals("false") && str3 != null && !str3.isEmpty() && str3 != null && !str4.isEmpty() && !str4.equals("0") && !str3.endsWith(".rar") && !str3.endsWith(".7z") && !str3.endsWith(".zip") && !str3.endsWith(".iso") && !str3.endsWith(".avi") && !str3.endsWith(".flv") && !str3.endsWith(".sub") && !str3.endsWith(".pdf") && !str3.endsWith(".mp3")) {
                            MediaSource cloneDeeply = mediaSource.cloneDeeply();
                            cloneDeeply.setOriginalLink(magnetObject.getMagnet());
                            cloneDeeply.setStreamLink(magnetObject.getMagnet());
                            cloneDeeply.setFileSize(Long.parseLong(str4));
                            cloneDeeply.setQuality(magnetObject.getQuality());
                            cloneDeeply.setHostName(magnetObject.getHostName());
                            cloneDeeply.setPremiumize(true);
                            cloneDeeply.setProviderName(magnetObject.getProvider());
                            cloneDeeply.setFilename(magnetObject.getFileName());
                            observableEmitter.onNext(cloneDeeply);
                        }
                    }
                }
            } catch (Throwable th) {
                Logger.a("PREMIUMIZE", th.getMessage());
            }
        }
    }

    public void c(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        d(mediaSource, observableEmitter);
    }

    /* access modifiers changed from: protected */
    public void d(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (mediaSource.isTorrent()) {
            try {
                PremiumizeDirectDL body = PremiumizeModule.b().getPremiumizeDirectDL(PremiumizeCredentialsHelper.b().getAccessToken(), mediaSource.getStreamLink()).execute().body();
                if (body != null && body.getStatus().contains("success")) {
                    for (PremiumizeDirectDL.ContentBean next : body.getContent()) {
                        if (next.getStream_link() != null && !next.getStream_link().isEmpty() && next.getTranscode_status().contains("finished") && next.getSize() > 20971520) {
                            MediaSource cloneDeeply = mediaSource.cloneDeeply();
                            cloneDeeply.setStreamLink(next.getStream_link());
                            cloneDeeply.setPremiumize(true);
                            cloneDeeply.setResolved(true);
                            observableEmitter.onNext(cloneDeeply);
                        }
                    }
                }
            } catch (IOException e) {
                Logger.a("PREMIUMIZE", e.getMessage());
            }
        }
    }
}
