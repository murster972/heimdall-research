package com.utils.Getlink.Resolver.premium;

import com.original.tase.Logger;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.Getlink.Resolver.premium.services.AllDebrid;
import com.utils.Getlink.Resolver.premium.services.Premiumzie;
import com.utils.Getlink.Resolver.premium.services.Realdebrid;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;

public abstract class PremiumResolver extends BaseResolver {
    private static Realdebrid f = new Realdebrid();
    private static Premiumzie g = new Premiumzie();
    private static AllDebrid h = new AllDebrid();

    protected static void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter, boolean z, boolean z2, boolean z3) throws Exception {
        MediaSource cloneDeeply = mediaSource.cloneDeeply();
        cloneDeeply.setResolved(false);
        if (z && RealDebridCredentialsHelper.c().isValid()) {
            f.a(cloneDeeply, observableEmitter);
        }
        if (z2 && AllDebridCredentialsHelper.b().isValid()) {
            h.a(cloneDeeply, observableEmitter);
        }
        if (z3 && PremiumizeCredentialsHelper.b().isValid()) {
            g.a(cloneDeeply, observableEmitter);
        }
    }

    protected static void b(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter, boolean z, boolean z2, boolean z3) throws Exception {
        MediaSource cloneDeeply = mediaSource.cloneDeeply();
        if (z && RealDebridCredentialsHelper.c().isValid()) {
            f.c(cloneDeeply, observableEmitter);
        }
        if (z2 && AllDebridCredentialsHelper.b().isValid()) {
            h.c(cloneDeeply, observableEmitter);
        }
        if (z3 && PremiumizeCredentialsHelper.b().isValid()) {
            g.c(cloneDeeply, observableEmitter);
        }
    }

    public static Observable<MediaSource> c(final MediaSource mediaSource) {
        return Observable.create(new ObservableOnSubscribe<MediaSource>() {
            public void subscribe(ObservableEmitter<MediaSource> observableEmitter) throws Exception {
                if (mediaSource.isResolved()) {
                    observableEmitter.onNext(mediaSource);
                } else {
                    MediaSource mediaSource = mediaSource;
                    PremiumResolver.b(mediaSource, observableEmitter, mediaSource.isRealdebrid(), mediaSource.isAlldebrid(), mediaSource.isPremiumize());
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b());
    }

    public String a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        mediaSource.setHostName(a());
        try {
            if (!BaseResolver.f6518a) {
                if (!mediaSource.isTorrent()) {
                    b(mediaSource, observableEmitter, true, true, true);
                    return;
                }
            }
            a(mediaSource, observableEmitter, true, true, true);
        } catch (Exception e) {
            Logger.a(e.getMessage());
        }
    }
}
