package com.utils.Getlink.Resolver.premium.services;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.movie.FreeMoviesApp;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.model.realdebrid.AddMagnetResponse;
import com.movie.data.model.realdebrid.MagnetObject;
import com.movie.data.model.realdebrid.RealDebridTorrentInfoObject;
import com.movie.data.model.realdebrid.UnRestrictCheckObject;
import com.movie.data.model.realdebrid.UnRestrictObject;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.StringUtils;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class Realdebrid extends BaseService {

    /* renamed from: a  reason: collision with root package name */
    static RealDebridApi f6522a;
    public static List<RealDebridTorrentInfoObject> b;

    public Realdebrid() {
        f6522a = FreeMoviesApp.a(Utils.i()).d().b();
    }

    public static void a() {
        try {
            if (b == null) {
                b = f6522a.torrents((Integer) null, (Integer) null, (Integer) null, (String) null).execute().body();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void b(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) throws Exception {
        JsonObject jsonObject;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        a();
        ArrayList<MagnetObject> magnetObjects = mediaSource.getMagnetObjects();
        new ArrayList();
        List<RealDebridTorrentInfoObject> list = b;
        if (list != null) {
            for (RealDebridTorrentInfoObject next : list) {
                Iterator<MagnetObject> it2 = magnetObjects.iterator();
                while (it2.hasNext()) {
                    MagnetObject next2 = it2.next();
                    if (next.getHash().equalsIgnoreCase(Regex.a(next2.getMagnet(), "magnet:\\?xt=urn:btih:([^&.]+)", 1).toLowerCase()) && next.getStatus().equals("downloaded")) {
                        for (String next3 : next.getLinks()) {
                            MediaSource cloneDeeply = mediaSource.cloneDeeply();
                            if (mediaSource.getQuality() == null || mediaSource.getQuality().isEmpty()) {
                                cloneDeeply.setQuality("HD");
                            }
                            cloneDeeply.setStreamLink(next3);
                            cloneDeeply.setOriginalLink(next3);
                            cloneDeeply.setFileSize(next.getBytes());
                            cloneDeeply.setTorrent(false);
                            cloneDeeply.setRealdebrid(true);
                            cloneDeeply.setQuality(next2.getQuality());
                            cloneDeeply.setHostName(next2.getHostName());
                            cloneDeeply.setProviderName(next2.getProvider());
                            cloneDeeply.setFilename(next2.getFileName());
                            observableEmitter2.onNext(cloneDeeply);
                        }
                        it2.remove();
                    }
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        Iterator<MagnetObject> it3 = magnetObjects.iterator();
        while (it3.hasNext()) {
            arrayList.add(Regex.a(it3.next().getMagnet(), "magnet:\\?xt=urn:btih:([^&.]+)", 1).toLowerCase());
        }
        JsonObject f = new JsonParser().a(f6522a.instantAvailability(StringUtils.a(arrayList, "/")).execute().body().string()).f();
        if (f != null) {
            for (String next4 : f.o()) {
                JsonObject f2 = f.a(next4).f();
                if (f2 != null) {
                    long j = 0;
                    String str = "";
                    for (String b2 : f2.o()) {
                        Iterator<JsonElement> it4 = f2.b(b2).iterator();
                        while (it4.hasNext()) {
                            JsonObject jsonObject2 = (JsonObject) it4.next();
                            for (String next5 : jsonObject2.o()) {
                                JsonObject jsonObject3 = f;
                                jsonObject2.c(next5).a("filename").i();
                                String i = jsonObject2.c(next5).a("filesize").i();
                                if (i != null) {
                                    long longValue = Long.valueOf(i).longValue();
                                    if (longValue > j) {
                                        str = next5;
                                        j = longValue;
                                    }
                                }
                                f = jsonObject3;
                            }
                        }
                    }
                    jsonObject = f;
                    if (!str.isEmpty()) {
                        MediaSource cloneDeeply2 = mediaSource.cloneDeeply();
                        if (mediaSource.getQuality() == null || mediaSource.getQuality().isEmpty()) {
                            cloneDeeply2.setQuality("HD");
                        }
                        MagnetObject a2 = a(magnetObjects, next4);
                        cloneDeeply2.setFileSize(j);
                        cloneDeeply2.setRealdebrid(true);
                        cloneDeeply2.setTorrent(true);
                        cloneDeeply2.setQuality(a2.getQuality());
                        cloneDeeply2.setProviderName(a2.getProvider());
                        cloneDeeply2.setHostName(a2.getHostName());
                        cloneDeeply2.setTorrentFileID(str);
                        cloneDeeply2.setOriginalLink("magnet:\\?xt=urn:btih:" + next4);
                        cloneDeeply2.setStreamLink("magnet:\\?xt=urn:btih:" + next4);
                        cloneDeeply2.setFilename(a2.getFileName());
                        observableEmitter2.onNext(cloneDeeply2);
                    }
                } else {
                    jsonObject = f;
                }
                f = jsonObject;
            }
        }
    }

    public void c(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) throws Exception {
        List<ResolveResult> a2;
        String h;
        if (mediaSource.isTorrent()) {
            d(mediaSource, observableEmitter);
        } else if (RealDebridCredentialsHelper.c().isValid() && (a2 = a(mediaSource.getStreamLink(), mediaSource.getHostName())) != null && a2.size() > 0) {
            for (ResolveResult next : a2) {
                mediaSource.setResolved(true);
                mediaSource.setFileSize(next.getFilesize());
                mediaSource.setStreamLink(next.getResolvedLink());
                mediaSource.setRealdebrid(true);
                mediaSource.setFilename(next.getResolverFileName());
                if (mediaSource.getQuality().equals("HD") && (h = Utils.h(mediaSource.getFilename())) != null && !h.isEmpty()) {
                    mediaSource.setQuality(h);
                }
                observableEmitter.onNext(mediaSource);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void d(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) throws Exception {
        if (mediaSource.isRealdebrid()) {
            Response<AddMagnetResponse> execute = f6522a.addMagnet(mediaSource.getStreamLink(), "").execute();
            if (execute.code() == 201) {
                String id = execute.body().getId();
                if (id == null || id.isEmpty()) {
                    throw new Exception("Torrent ID not exist");
                }
                Response<ResponseBody> execute2 = f6522a.selectFiles(id, mediaSource.getTorrentFileID()).execute();
                if (execute2.code() == 204) {
                    Response<RealDebridTorrentInfoObject> execute3 = f6522a.torrentInfos(id).execute();
                    if (execute3.code() == 200) {
                        RealDebridTorrentInfoObject body = execute3.body();
                        if (body != null) {
                            if (body.getStatus().contains("downloaded")) {
                                for (String unrestrictLink : body.getLinks()) {
                                    Response<UnRestrictObject> execute4 = f6522a.unrestrictLink(unrestrictLink, "", "").execute();
                                    if (execute4.code() == 200) {
                                        UnRestrictObject body2 = execute4.body();
                                        if (mediaSource.getQuality() == null || mediaSource.getQuality().isEmpty()) {
                                            mediaSource.setQuality("HD");
                                        }
                                        mediaSource.setStreamLink(body2.getDownload());
                                        mediaSource.setFileSize(body2.getFilesize());
                                        mediaSource.setRealdebrid(true);
                                        mediaSource.setResolved(true);
                                        mediaSource.setFilename(body2.getFilename());
                                        observableEmitter.onNext(mediaSource);
                                    } else {
                                        throw new Exception("unRestrictObjectResponse Error : " + execute4.code());
                                    }
                                }
                            }
                            observableEmitter.onComplete();
                            return;
                        }
                        throw new Exception("Error code: " + execute3.code() + "\nMessage: " + execute3.message());
                    }
                    throw new Exception("realDebridTorrentInfoObjectResponse  Error : " + execute3.code());
                }
                throw new Exception("responseBodyResponseSelectFile Error : " + execute2.code());
            }
            throw new Exception("responseAddmagnet Error : " + execute.code());
        }
    }

    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) throws Exception {
        PrintStream printStream = System.out;
        printStream.println("checkExist=" + mediaSource.toStringAllObjs());
        if (mediaSource.isTorrent()) {
            b(mediaSource, observableEmitter);
            return;
        }
        Response<UnRestrictCheckObject> execute = f6522a.unrestrictCheck(mediaSource.getStreamLink(), (String) null).execute();
        if (execute != null && execute.isSuccessful()) {
            long filesize = execute.body().getFilesize();
            String link = execute.body().getLink();
            if (link != null && !link.isEmpty() && execute.body().getSupported() > 0) {
                mediaSource.setFileSize(filesize);
                mediaSource.setRealdebrid(true);
                mediaSource.setOriginalLink(link);
                mediaSource.setStreamLink(link);
                observableEmitter.onNext(mediaSource);
            }
        }
    }

    public MagnetObject a(ArrayList<MagnetObject> arrayList, String str) {
        Iterator<MagnetObject> it2 = arrayList.iterator();
        while (it2.hasNext()) {
            MagnetObject next = it2.next();
            if (next.getMagnet().toLowerCase().contains(str.toLowerCase())) {
                return next;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public List<ResolveResult> a(String str, String str2) throws Exception {
        String str3 = str2;
        ArrayList arrayList = new ArrayList();
        Response<UnRestrictObject> execute = f6522a.unrestrictLink(str, "", "").execute();
        if (execute.code() == 200) {
            UnRestrictObject body = execute.body();
            if (body != null) {
                String download = body.getDownload();
                if (!download.endsWith(".rar") && !download.endsWith(".7z") && !download.endsWith(".zip") && !download.endsWith(".iso") && !download.endsWith(".avi") && !download.endsWith(".flv") && !download.endsWith(".sub") && !download.endsWith(".pdf") && !download.endsWith(".mp3")) {
                    ResolveResult resolveResult = new ResolveResult(str3, download, String.valueOf(body.getFilesize()));
                    resolveResult.setResolverFileName(body.getFilename());
                    resolveResult.setFilesize(body.getFilesize());
                    resolveResult.setResolvedQuality(body.getType());
                    arrayList.add(resolveResult);
                }
                if (body.getAlternative() != null) {
                    for (UnRestrictObject.AlternativeBean next : body.getAlternative()) {
                        String download2 = next.getDownload();
                        if (!download2.endsWith(".rar") && !download2.endsWith(".7z") && !download2.endsWith(".zip") && !download2.endsWith(".iso") && !download2.endsWith(".avi") && !download2.endsWith(".flv") && !download2.endsWith(".sub") && !download2.endsWith(".pdf") && !download2.endsWith(".mp3")) {
                            ResolveResult resolveResult2 = new ResolveResult(str3, download2, (String) null);
                            resolveResult2.setResolverFileName(next.getFilename());
                            arrayList.add(resolveResult2);
                        }
                    }
                }
            }
            return arrayList;
        }
        throw new Exception("Error code: " + execute.code() + "\nMessage: " + execute.message());
    }
}
