package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JJDecoder;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MstreamTo extends BaseResolver {
    public String a() {
        return "MstreamTo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        if (!Utils.d) {
            ArrayList arrayList = new ArrayList();
            String a2 = HttpHelper.e().a(streamLink, (Map<String, String>[]) new Map[0]);
            if (JsUnpacker.m30920(a2)) {
                arrayList.addAll(JsUnpacker.m30916(a2));
            }
            String decode = new JJDecoder().decode(Regex.a(a2, "\\$=.*\\(\\)\\)\\(\\)\\;", 0).replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "").replace("\r", ""));
            arrayList.add(decode);
            Iterator<ResolveResult> it2 = a(streamLink, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
            if (!it2.hasNext()) {
                String a3 = Regex.a(decode, "['\"]src['\"]\\s*[,|:]\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
                if (!a3.isEmpty()) {
                    observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), a3, mediaSource.getQuality())));
                }
            }
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                    next.setResolvedQuality(mediaSource.getQuality());
                }
                observableEmitter.onNext(BaseResolver.a(mediaSource, next));
            }
        }
    }
}
