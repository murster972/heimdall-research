package com.utils.Getlink.Resolver;

import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Map;

public class VidCloud extends BaseResolver {
    public String a() {
        return "VidCloud";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        MediaSource mediaSource2 = mediaSource;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?://|\\.)(loadvid\\.online|vidcloud\\.co)/(?:v|embed)/([0-9A-Za-z]+)(?:\\?|/|$)", 2, 2);
        if (a2.isEmpty()) {
            a2 = Regex.a(streamLink, "(?://|\\.)(loadvid\\.online|vidcloud\\.co)/player.*?(?:\\?|&)fid=([0-9A-Za-z]+)(?:&|$)", 2, 2);
            if (a2.isEmpty()) {
                return;
            }
        }
        HttpHelper e = HttpHelper.e();
        String a3 = Regex.a(e.b("https://loadvid.online/player?fid=" + a2 + "&page=embed", streamLink, new Map[0]), "['\"]html['\"]\\s*:\\s*['\"](.*)['\"][^\\}]*\\}", 1, 2);
        if (!a3.isEmpty()) {
            CharSequence charSequence = "\\\\n";
            CharSequence charSequence2 = "/";
            CharSequence charSequence3 = "\\\\r";
            ArrayList<ArrayList<String>> b = Regex.b(a3.replace("\\\"", "\"").replace("\\'", "'").replace("\\\\\"", "\"").replace("\\\\'", "'").replace("\\r", "").replace("\\n", "").replace("\\\\r", "").replace("\\\\n", "").replace("\\\\\\/", "/").replace("\\/", "/").replace("/\\", "/"), "['\"]?src['\"]\\s*:\\s*['\"]([^'\"]+)['\"]\\s*[^\\}]+\\s*['\"]?label['\"]?\\s*:\\s*['\"]?(\\d+)p?", 2, true);
            ArrayList arrayList = b.get(0);
            ArrayList arrayList2 = b.get(1);
            if (arrayList.size() > 0) {
                for (int i = 0; i < arrayList.size(); i++) {
                    try {
                        String str = (String) arrayList.get(i);
                        String str2 = (String) arrayList2.get(i);
                        if (!str.isEmpty() && !str.contains("/logo")) {
                            observableEmitter2.onNext(BaseResolver.a(mediaSource2, new ResolveResult(a(), str, str2)));
                        }
                    } catch (Exception e2) {
                        Logger.a((Throwable) e2, new boolean[0]);
                    }
                }
                return;
            }
            CharSequence charSequence4 = charSequence2;
            ArrayList arrayList3 = Regex.b(a3.replace("\\\"", "\"").replace("\\'", "'").replace("\\\\\"", "\"").replace("\\\\'", "'").replace("\\r", "").replace("\\n", "").replace(charSequence3, "").replace(charSequence, "").replace("\\\\\\/", charSequence4).replace("\\/", charSequence4).replace("/\\", charSequence4), "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 2, true).get(0);
            for (int i2 = 0; i2 < arrayList3.size(); i2++) {
                try {
                    String str3 = (String) arrayList3.get(i2);
                    if (!str3.isEmpty() && !str3.contains("/logo")) {
                        observableEmitter2.onNext(BaseResolver.a(mediaSource2, new ResolveResult(a(), str3, "")));
                    }
                } catch (Exception e3) {
                    Logger.a((Throwable) e3, new boolean[0]);
                }
            }
        }
    }
}
