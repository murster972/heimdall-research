package com.utils.Getlink.Resolver;

public class Putload extends GenericResolver {
    public String a() {
        return "Putload";
    }

    public boolean f() {
        return false;
    }

    public String g() {
        return "(?://|\\.)(putload\\.tv|shitmovie\\.com)/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://putload.tv";
    }

    public String[] i() {
        return new String[]{"op=get_slides"};
    }
}
