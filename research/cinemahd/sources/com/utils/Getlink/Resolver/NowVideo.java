package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class NowVideo extends PremiumResolver {
    public String a() {
        return "NowVideo";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String i = i();
        String h = h();
        String a2 = Regex.a(mediaSource.getStreamLink(), h, 2);
        if (!a2.isEmpty()) {
            if (i.isEmpty()) {
                i = "http://" + Regex.a(mediaSource.getStreamLink(), h, 1);
            }
            super.a(mediaSource, observableEmitter);
            if (!Utils.d) {
                String b = b(i, a2);
                ArrayList arrayList = new ArrayList();
                String a3 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[0]);
                arrayList.add(a3);
                if (JsUnpacker.m30920(a3)) {
                    arrayList.addAll(JsUnpacker.m30916(a3));
                }
                HashMap hashMap = null;
                if (g()) {
                    hashMap = new HashMap();
                    hashMap.put("User-Agent", Constants.f5838a);
                    hashMap.put("Referer", b);
                    hashMap.put("Cookie", HttpHelper.e().a(b));
                }
                Iterator<ResolveResult> it2 = a(b, (ArrayList<String>) arrayList, g(), (HashMap<String, String>) hashMap, f()).iterator();
                while (it2.hasNext()) {
                    ResolveResult next = it2.next();
                    if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        next.setResolvedQuality("HQ");
                    }
                    observableEmitter.onNext(BaseResolver.a(mediaSource, next));
                }
            }
        }
    }

    public String b(String str, String str2) {
        return str + "/embed/?v=" + str2;
    }

    /* access modifiers changed from: protected */
    public String[] f() {
        return null;
    }

    public boolean g() {
        return true;
    }

    public String h() {
        return "(?://|\\.)(nowvideo\\.(?:eu|ch|sx|co|li|fo|at|ec))/(?:video/|embed\\.php\\?\\S*v=|embed/\\?v=)([A-Za-z0-9]+)";
    }

    public String i() {
        return "http://embed.nowvideo.sx";
    }
}
