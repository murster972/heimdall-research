package com.utils.Getlink.Resolver;

import android.text.TextUtils;
import android.util.SparseIntArray;
import com.facebook.common.util.UriUtil;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Map;

public class Streamango extends PremiumResolver {
    private String b(String str, int i) {
        String str2 = "";
        int i2 = 0;
        int i3 = 0;
        while (i2 < str.length() - 1) {
            try {
                while (i3 <= str.length() - 1) {
                    int indexOf = "=/+9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA".indexOf(str.charAt(i3));
                    int i4 = i3 + 1;
                    int indexOf2 = "=/+9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA".indexOf(str.charAt(i4));
                    int i5 = i4 + 1;
                    int indexOf3 = "=/+9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA".indexOf(str.charAt(i5));
                    i3 = i5 + 1 + 1;
                    int i6 = (indexOf << 2) | (indexOf2 >> 4);
                    int i7 = ((indexOf2 & 15) << 4) | (indexOf3 >> 2);
                    int indexOf4 = "=/+9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA".indexOf(str.charAt(i3)) | ((indexOf3 & 3) << 6);
                    str2 = String.valueOf(str2) + Character.toString((char) (i6 ^ i));
                    if (indexOf3 != 64) {
                        str2 = String.valueOf(str2) + Character.toString((char) i7);
                    }
                    if (indexOf3 != 64) {
                        str2 = String.valueOf(str2) + Character.toString((char) indexOf4);
                    }
                }
                i2++;
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                return null;
            }
        }
        return str2;
    }

    public String a() {
        return "Streamango";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = Regex.a(streamLink, "(?://|\\.)((?:stream(?:ango|cherry)|fruitstreams)\\.com)/(?:v/d|f|embed)/([0-9a-zA-Z]+)", 1);
        String a3 = Regex.a(streamLink, "(?://|\\.)((?:stream(?:ango|cherry)|fruitstreams)\\.com)/(?:v/d|f|embed)/([0-9a-zA-Z]+)", 2);
        if (!a2.isEmpty() && !a3.isEmpty()) {
            super.a(mediaSource, observableEmitter);
            if (!Utils.d) {
                String a4 = HttpHelper.e().a("http://" + a2 + "/embed/" + a3, (Map<String, String>[]) new Map[0]);
                if (!a4.contains(">Sorry!<")) {
                    try {
                        String a5 = Regex.a(a4, "srces\\.push\\s*\\(\\s*\\{\\s*['\"]?type['\"]?\\s*:\\s*['\"]video\\/mp4['\"]\\s*,\\s*['\"]?src['\"]?\\s*:\\w+\\s*\\(\\s*['\"]([^'\"]+)['\"]\\s*,\\s*(\\d+)", 1, true);
                        String a6 = Regex.a(a4, "srces\\.push\\s*\\(\\s*\\{\\s*['\"]?type['\"]?\\s*:\\s*['\"]video\\/mp4['\"]\\s*,\\s*['\"]?src['\"]?\\s*:\\w+\\s*\\(\\s*['\"]([^'\"]+)['\"]\\s*,\\s*(\\d+)", 2, true);
                        if (a5.isEmpty()) {
                            return;
                        }
                        if (!a6.isEmpty()) {
                            String a7 = a(a5, Integer.parseInt(a6));
                            if (a7 != null || (a7 = b(a5, Integer.parseInt(a6))) != null) {
                                String replace = a7.replace("@", "");
                                if (replace.startsWith("//")) {
                                    replace = "http:" + replace;
                                } else if (replace.startsWith("/")) {
                                    replace = "http://" + a2 + replace;
                                } else if (!replace.startsWith(UriUtil.HTTP_SCHEME)) {
                                    replace = "http://" + a2 + "/" + replace;
                                }
                                if (replace.contains("/")) {
                                    String[] split = replace.split("/");
                                    if (split.length > 0) {
                                        a6 = split[split.length - 1];
                                        if (!com.original.tase.utils.Utils.b(a6)) {
                                            split[split.length - 1] = a6.replaceAll("[^\\d]", "");
                                            replace = TextUtils.join("/", split);
                                        }
                                    }
                                }
                                if (a6.startsWith("7") || a6.startsWith("6")) {
                                    a6 = "720p";
                                }
                                if (a6.startsWith("4") || a6.startsWith("5")) {
                                    a6 = "480p";
                                }
                                String replace2 = replace.replace(" ", "%20");
                                if (!replace2.toLowerCase().contains("/externsub/")) {
                                    Logger.a("Streamango ", mediaSource.getQuality() + " " + a6);
                                    String a8 = a();
                                    if (Utils.b) {
                                        a6 = mediaSource.getQuality();
                                    }
                                    ResolveResult resolveResult = new ResolveResult(a8, replace2, a6);
                                    if (resolveResult.getResolvedQuality() != null && resolveResult.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                                        resolveResult.setResolvedQuality("HQ");
                                    }
                                    observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
                                }
                            }
                        }
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                }
            }
        }
    }

    private String a(String str, int i) {
        int i2;
        try {
            StringBuilder sb = new StringBuilder();
            String replaceAll = str.replaceAll("[^A-Za-z0-9+/=]", "");
            int length = replaceAll.length();
            SparseIntArray sparseIntArray = new SparseIntArray();
            for (int i3 = 0; i3 < length; i3 = i2) {
                i2 = i3;
                for (int i4 = 0; i4 < 4; i4++) {
                    sparseIntArray.put(i4 % 4, "=/+9876543210zyxwvutsrqponmlkjihgfedcbaZYXWVUTSRQPONMLKJIHGFEDCBA".indexOf(replaceAll.charAt(i2)));
                    i2++;
                }
                sb.append(Character.toString((char) (((sparseIntArray.get(0) << 2) | (sparseIntArray.get(1) >> 4)) ^ i)));
                if (sparseIntArray.get(2) != 64) {
                    sb.append(Character.toString((char) (((sparseIntArray.get(1) & 15) << 4) | (sparseIntArray.get(2) >> 2))));
                }
                if (sparseIntArray.get(3) != 64) {
                    sb.append(Character.toString((char) (sparseIntArray.get(3) | ((sparseIntArray.get(2) & 3) << 6))));
                }
            }
            return sb.toString();
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
            return null;
        }
    }
}
