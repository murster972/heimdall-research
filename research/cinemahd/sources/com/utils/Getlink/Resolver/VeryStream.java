package com.utils.Getlink.Resolver;

import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Map;

public class VeryStream extends BaseResolver {
    public String a() {
        return "CDN-Stream";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String a2 = HttpHelper.e().a(streamLink, (Map<String, String>[]) new Map[0]);
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", streamLink);
        String a3 = Regex.a(a2, "<p.*?id=\"videolink\">(.*)<\\/p>", 1);
        if (!a3.isEmpty()) {
            ResolveResult resolveResult = new ResolveResult(a(), "https://verystream.com/gettoken/" + a3 + "?mime=true", mediaSource.getQuality());
            resolveResult.setPlayHeader(hashMap);
            observableEmitter.onNext(BaseResolver.a(mediaSource, resolveResult));
        }
    }
}
