package com.utils.Getlink.Resolver;

public class HdTv extends GenericResolver {
    public String a() {
        return "HD-TV";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/player/embed_player.php?vid=" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    public String g() {
        return "\"(?://|\\\\.)(hqq\\\\.tv)/(?:player)/embed_player.php?vid=([A-Za-z0-9]+)";
    }

    public String h() {
        return "https://hqq.tv";
    }
}
