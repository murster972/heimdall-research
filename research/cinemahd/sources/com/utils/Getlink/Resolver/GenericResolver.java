package com.utils.Getlink.Resolver;

import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public abstract class GenericResolver extends BaseResolver {
    public String a() {
        return null;
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String streamLink = mediaSource.getStreamLink();
        String h = h();
        String g = g();
        String a2 = Regex.a(streamLink, g, 2);
        if (!a2.isEmpty()) {
            if (h.isEmpty()) {
                h = "http://" + Regex.a(streamLink, g, 1);
            }
            String b = b(h, a2);
            ArrayList arrayList = new ArrayList();
            String a3 = HttpHelper.e().a(b, (Map<String, String>[]) new Map[0]);
            arrayList.add(a3);
            if (JsUnpacker.m30920(a3)) {
                arrayList.addAll(JsUnpacker.m30916(a3));
            }
            HashMap hashMap = null;
            if (f()) {
                hashMap = new HashMap();
                hashMap.put("User-Agent", Constants.f5838a);
                hashMap.put("Referer", b);
                hashMap.put("Cookie", HttpHelper.e().a(b));
            }
            Iterator<ResolveResult> it2 = a(b, (ArrayList<String>) arrayList, f(), (HashMap<String, String>) hashMap, i()).iterator();
            while (it2.hasNext()) {
                ResolveResult next = it2.next();
                String resolvedLink = next.getResolvedLink();
                if (resolvedLink.contains(".mp4") || resolvedLink.contains("m3u8")) {
                    if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                        next.setResolvedQuality("HQ");
                    }
                    if (next.getResolvedQuality() == null || next.getResolvedQuality().isEmpty()) {
                        next.setResolvedQuality(mediaSource.getQuality());
                    }
                    observableEmitter.onNext(BaseResolver.a(mediaSource, next));
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return str + "/embed-" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public abstract boolean f();

    /* access modifiers changed from: protected */
    public abstract String g();

    /* access modifiers changed from: protected */
    public abstract String h();

    /* access modifiers changed from: protected */
    public String[] i() {
        return null;
    }
}
