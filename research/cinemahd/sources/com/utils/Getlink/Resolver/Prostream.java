package com.utils.Getlink.Resolver;

public class Prostream extends GenericResolver {
    public String a() {
        return "Prostream";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return "https://procdnvids.net/" + str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "(?://|\\.)((?:prostream|procdnvids)\\.(?:to|xyz|co|net))/(?:embed-)?([a-zA-Z0-9]+)";
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "https://procdnvids.net";
    }
}
