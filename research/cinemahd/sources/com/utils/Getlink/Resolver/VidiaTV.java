package com.utils.Getlink.Resolver;

public class VidiaTV extends GenericResolver {
    public String a() {
        return "VidiaTV";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return true;
    }

    public String g() {
        return "(?://|\\.)(vidia\\.tv)/(?:embed-)([a-zA-Z0-9]+)";
    }

    public String h() {
        return "https://vidia.tv";
    }
}
