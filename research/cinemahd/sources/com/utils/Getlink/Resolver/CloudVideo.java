package com.utils.Getlink.Resolver;

public class CloudVideo extends GenericResolver {
    public String a() {
        return "CloudVideo";
    }

    public boolean f() {
        return false;
    }

    public String g() {
        return "(?://|\\.)(cloudvideo\\.(?:tv|cc|si))/(?:embed-)?([0-9a-zA-Z]+)";
    }

    public String h() {
        return "https://cloudvideo.tv";
    }
}
