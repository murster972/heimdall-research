package com.utils.Getlink.Resolver;

public class EStream extends GenericResolver {
    public String a() {
        return "EStream";
    }

    /* access modifiers changed from: protected */
    public String b(String str, String str2) {
        return "https://estream.to/" + str2 + ".html";
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return false;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return "(?://|\\.)(estream\\.(?:to|xyz))/(?:embed-)?([a-zA-Z0-9]+)";
    }

    /* access modifiers changed from: protected */
    public String h() {
        return "https://estream.to";
    }
}
