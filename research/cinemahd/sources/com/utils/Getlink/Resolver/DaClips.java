package com.utils.Getlink.Resolver;

import com.facebook.common.util.UriUtil;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class DaClips extends BaseResolver {
    public String a() {
        return "DaClips";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Regex.a(mediaSource.getStreamLink(), "(?://|\\.)(daclips\\.(?:in|com))/(?:embed-)?([0-9a-zA-Z]+)", 2, 2);
        if (!a2.isEmpty()) {
            String str = "https://daclips.in/" + a2;
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
            if (!a3.contains("404 - File Not Found")) {
                ArrayList arrayList = Regex.b(a3, "file:\\s+\"(.+?)\"", 1).get(0);
                arrayList.addAll(Regex.b(a3, "[\"']?(?:file|url)[\"']?\\s*[:=]\\s*[\"']([^\"']+)", 1).get(0));
                arrayList.addAll(Regex.b(a3, "src\\s*:\\s*'([^']+)", 1).get(0));
                String a4 = Utils.a((Map<String, String>) BaseResolver.a(a3, (String) null));
                HashMap hashMap = new HashMap();
                hashMap.put("User-Agent", Constants.f5838a);
                hashMap.put("Referer", str);
                String a5 = HttpHelper.e().a(str, a4, (Map<String, String>[]) new Map[]{hashMap});
                arrayList.addAll(Regex.b(a5, "file:\\s+\"(.+?)\"", 1).get(0));
                arrayList.addAll(Regex.b(a5, "[\"']?(?:file|url)[\"']?\\s*[:=]\\s*[\"']([^\"']+)", 1).get(0));
                arrayList.addAll(Regex.b(a5, "src\\s*:\\s*'([^']+)", 1).get(0));
                Iterator it2 = arrayList.iterator();
                ArrayList arrayList2 = new ArrayList();
                int i = 0;
                while (it2.hasNext()) {
                    String str2 = (String) it2.next();
                    if (str2.contains(UriUtil.HTTP_SCHEME)) {
                        i++;
                        if (!arrayList2.contains(str2)) {
                            arrayList2.add(str2);
                            observableEmitter.onNext(BaseResolver.a(mediaSource, new ResolveResult(a(), str2, "HQ")));
                        }
                    }
                }
                if (i == 0) {
                    Iterator<ResolveResult> it3 = a(str, a5, false, (HashMap<String, String>) null, new String[0][]).iterator();
                    while (it3.hasNext()) {
                        observableEmitter.onNext(BaseResolver.a(mediaSource, it3.next()));
                    }
                }
            }
        }
    }
}
