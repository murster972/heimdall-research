package com.utils.Getlink.Resolver;

import android.util.Base64;
import com.facebook.common.util.UriUtil;
import com.google.gson.Gson;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.ResolveResult;
import com.original.tase.model.hydrax.StreamXResponse;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.uwetrottmann.trakt5.TraktV2;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class HydraX extends BaseResolver {
    public String a() {
        return "HydraX";
    }

    /* access modifiers changed from: protected */
    public void a(MediaSource mediaSource, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        MediaSource mediaSource2 = mediaSource;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String replace = mediaSource.getStreamLink().replace("\\/", "/");
        if (replace.endsWith("\\")) {
            replace = replace.substring(0, replace.length() - 1);
        }
        HashMap hashMap = new HashMap();
        if (mediaSource.getPlayHeader() == null) {
            hashMap.put("User-Agent", Constants.f5838a);
        } else {
            hashMap.putAll(mediaSource.getPlayHeader());
        }
        String a2 = HttpHelper.e().a(replace, (Map<String, String>[]) new Map[]{hashMap});
        HashMap hashMap2 = new HashMap();
        hashMap2.put("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36");
        String a3 = Regex.a(a2, "Player\\s*\\((\\{.*\\})\\);", 1);
        if (!a3.isEmpty()) {
            String a4 = Regex.a(a3, "[\"']key[\"']\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1);
            String a5 = Regex.a(a3, "[\"']type[\"']\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1);
            String a6 = Regex.a(a3, "[\"']value[\"']\\s*:\\s*[\"']([^\"']+[^\"'])[\"']", 1);
            if (a4 == null) {
                String a7 = Regex.a(a2, "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1);
                if (!a7.isEmpty()) {
                    ResolveResult resolveResult = new ResolveResult(a(), a7.replace("\\/", "/"), "HD");
                    resolveResult.setPlayHeader(hashMap2);
                    observableEmitter2.onNext(BaseResolver.a(mediaSource2, resolveResult));
                }
                String a8 = Regex.a(a2, "<iframe[^>]+src=\"([^\"]+)\"[^>]*>", 1, 2);
                if (!a8.isEmpty()) {
                    String a9 = HttpHelper.e().a(a8, (Map<String, String>[]) new Map[]{hashMap});
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(a9);
                    if (JsUnpacker.m30920(a9)) {
                        arrayList.addAll(JsUnpacker.m30916(a9));
                    }
                    Iterator<ResolveResult> it2 = a(a8, (ArrayList<String>) arrayList, false, (HashMap<String, String>) null, new String[0][]).iterator();
                    while (it2.hasNext()) {
                        ResolveResult next = it2.next();
                        if (next.getResolvedQuality() != null && next.getResolvedQuality().trim().toLowerCase().equals("sd")) {
                            next.setResolvedQuality(mediaSource.getQuality());
                        }
                        next.setPlayHeader(hashMap2);
                        observableEmitter2.onNext(BaseResolver.a(mediaSource2, next));
                    }
                }
            } else if (a6.isEmpty()) {
                HashMap<String, String> a10 = Constants.a();
                a10.put("accept", "application/json, text/javascript, */*; q=0.01");
                a10.put("origin", "https://play.voxzer.org");
                a10.put("content-type", TraktV2.CONTENT_TYPE_JSON);
                a10.put("referer", mediaSource.getStreamLink());
                String a11 = Regex.a(HttpHelper.e().a("https://play.voxzer.org/data", String.format("{\"code\":\"%s\"}", new Object[]{Regex.a(mediaSource.getStreamLink(), "\\?v=(.*)", 1)}), false, (Map<String, String>[]) new Map[]{a10}), "['\"]?url['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1);
                if (!a11.isEmpty()) {
                    if (!a11.startsWith(UriUtil.HTTP_SCHEME)) {
                        try {
                            str = new String(Base64.decode(a11, 10), "UTF-8");
                        } catch (Throwable th) {
                            Logger.a(th, new boolean[0]);
                            str = new String(Base64.decode(a11, 10));
                        }
                        a6 = str;
                    } else {
                        a6 = a11;
                    }
                }
                String format = String.format("https://multi.hydrax.net/%s?type=%s&value=%s", new Object[]{a4, a5, a6});
                StreamXResponse streamXResponse = (StreamXResponse) new Gson().a(HttpHelper.e().a(format, (Map<String, String>[]) new Map[]{hashMap2}), StreamXResponse.class);
                if (streamXResponse != null) {
                    String link = streamXResponse.getLink();
                    if (!link.isEmpty()) {
                        ResolveResult resolveResult2 = new ResolveResult(a(), link, mediaSource.getQuality());
                        resolveResult2.setPlayHeader(hashMap2);
                        observableEmitter2.onNext(BaseResolver.a(mediaSource2, resolveResult2));
                    }
                }
            }
        }
    }
}
