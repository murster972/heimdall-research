package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;

public class AfdahTV extends BaseProvider {
    String c = Utils.getProvider(26);

    private String j(String str) {
        try {
            HashMap hashMap = new HashMap();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < 26; i++) {
                char charAt = "abcdefghijklmnopqrstuvwxyz".charAt(i);
                char charAt2 = "abcdefghijklmnopqrstuvwxyz".charAt((i + 13) % 26);
                hashMap.put(String.valueOf(charAt), String.valueOf(charAt2));
                hashMap.put(String.valueOf(charAt).toUpperCase(), String.valueOf(charAt2).toUpperCase());
            }
            for (int i2 = 0; i2 < str.length(); i2++) {
                char charAt3 = str.charAt(i2);
                if (charAt3 < 'A' || charAt3 > 'Z') {
                    if (charAt3 >= 'a') {
                        if (charAt3 > 'z') {
                        }
                    }
                    sb.append(charAt3);
                }
                sb.append((String) hashMap.get(String.valueOf(charAt3)));
            }
            return sb.toString();
        } catch (Exception e) {
            Logger.a((Throwable) e, new boolean[0]);
            return "";
        }
    }

    public String a() {
        return "AfdahTV";
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:117:0x038a A[Catch:{ Exception -> 0x03c9 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.movie.data.model.MovieInfo r20, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r21) {
        /*
            r19 = this;
            r1 = r19
            r2 = r21
            java.lang.String r3 = ""
            r4 = 0
            r6 = r3
            r5 = 0
        L_0x0009:
            java.lang.String r7 = "href"
            java.lang.String r8 = " en-US,en;q=0.9,vi;q=0.8"
            java.lang.String r9 = "accept-language"
            java.lang.String r10 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
            java.lang.String r11 = "Accept"
            java.lang.String r12 = "Origin"
            r13 = 2
            java.lang.String r14 = "/"
            r15 = 1
            if (r5 >= r13) goto L_0x0160
            java.lang.String r0 = r20.getName()
            java.lang.String r13 = ":"
            boolean r0 = r0.contains(r13)
            if (r0 == 0) goto L_0x0036
            java.lang.String r0 = r20.getName()
            java.lang.String[] r0 = r0.split(r13)
            r0 = r0[r4]
            java.lang.String r0 = com.original.tase.helper.TitleHelper.e(r0)
            goto L_0x003e
        L_0x0036:
            java.lang.String r0 = r20.getName()
            java.lang.String r0 = com.original.tase.helper.TitleHelper.e(r0)
        L_0x003e:
            if (r5 != r15) goto L_0x0058
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r0)
            java.lang.String r0 = " "
            r13.append(r0)
            java.lang.Integer r0 = r20.getYear()
            r13.append(r0)
            java.lang.String r0 = r13.toString()
        L_0x0058:
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0076 }
            r13.<init>()     // Catch:{ Exception -> 0x0076 }
            r13.append(r0)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = "|||title"
            r13.append(r0)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r0 = r13.toString()     // Catch:{ Exception -> 0x0076 }
            java.lang.String r13 = "Watch Movies Online 2020"
            java.lang.String r0 = com.original.tase.helper.crypto.AES256Cryptor.a(r0, r13)     // Catch:{ Exception -> 0x0076 }
            java.lang.String r13 = "\n"
            java.lang.String r13 = r0.replace(r13, r3)     // Catch:{ Exception -> 0x0076 }
            goto L_0x007b
        L_0x0076:
            r0 = move-exception
            r0.printStackTrace()
            r13 = 0
        L_0x007b:
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r15 = "accept"
            java.lang.String r4 = "*/*"
            r0.put(r15, r4)
            java.lang.String r4 = r1.c
            r0.put(r12, r4)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r15 = r1.c
            r4.append(r15)
            r4.append(r14)
            java.lang.String r4 = r4.toString()
            java.lang.String r15 = "referer"
            r0.put(r15, r4)
            r0.put(r11, r10)
            r0.put(r9, r8)
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r16 = r6
            java.lang.String r6 = "process="
            r15.append(r6)
            r6 = 0
            boolean[] r2 = new boolean[r6]
            java.lang.String r2 = com.original.tase.utils.Utils.a((java.lang.String) r13, (boolean[]) r2)
            r15.append(r2)
            java.lang.String r2 = r15.toString()
            r13 = 1
            java.util.Map[] r15 = new java.util.Map[r13]
            r15[r6] = r0
            java.lang.String r0 = "https://search.afdah.info/"
            java.lang.String r0 = r4.a((java.lang.String) r0, (java.lang.String) r2, (java.util.Map<java.lang.String, java.lang.String>[]) r15)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r2 = "li"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r2)
            java.util.Iterator r0 = r0.iterator()
        L_0x00df:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x0153
            java.lang.Object r2 = r0.next()
            org.jsoup.nodes.Element r2 = (org.jsoup.nodes.Element) r2
            java.lang.String r4 = "a[href]"
            org.jsoup.nodes.Element r2 = r2.h(r4)
            if (r2 == 0) goto L_0x00df
            java.lang.String r4 = r2.b((java.lang.String) r7)
            java.lang.String r2 = r2.G()
            java.lang.String r6 = "(.+?)\\s+\\((\\d{4})\\)"
            r13 = 1
            java.lang.String r15 = com.original.tase.utils.Regex.a((java.lang.String) r2, (java.lang.String) r6, (int) r13)
            r13 = 2
            java.lang.String r6 = com.original.tase.utils.Regex.a((java.lang.String) r2, (java.lang.String) r6, (int) r13)
            boolean r13 = r15.isEmpty()
            if (r13 == 0) goto L_0x010e
            r15 = r2
        L_0x010e:
            java.lang.String r2 = r20.getName()
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r2)
            java.lang.String r13 = com.original.tase.helper.TitleHelper.f(r15)
            boolean r2 = r2.equals(r13)
            if (r2 == 0) goto L_0x00df
            java.lang.String r2 = r6.trim()
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x0150
            java.lang.String r2 = r6.trim()
            boolean r2 = com.original.tase.utils.Utils.b(r2)
            if (r2 == 0) goto L_0x0150
            java.lang.Integer r2 = r20.getYear()
            int r2 = r2.intValue()
            if (r2 <= 0) goto L_0x0150
            java.lang.String r2 = r6.trim()
            int r2 = java.lang.Integer.parseInt(r2)
            java.lang.Integer r6 = r20.getYear()
            int r6 = r6.intValue()
            if (r2 != r6) goto L_0x00df
        L_0x0150:
            r6 = r4
            r0 = 1
            goto L_0x0156
        L_0x0153:
            r6 = r16
            r0 = 0
        L_0x0156:
            if (r0 == 0) goto L_0x0159
            goto L_0x0162
        L_0x0159:
            int r5 = r5 + 1
            r2 = r21
            r4 = 0
            goto L_0x0009
        L_0x0160:
            r16 = r6
        L_0x0162:
            r6.isEmpty()
            boolean r0 = r6.isEmpty()
            if (r0 == 0) goto L_0x016c
            return
        L_0x016c:
            boolean r0 = r6.startsWith(r14)
            if (r0 == 0) goto L_0x0183
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = r1.c
            r0.append(r2)
            r0.append(r6)
            java.lang.String r6 = r0.toString()
        L_0x0183:
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r0 = r1.c
            r2.put(r12, r0)
            r2.put(r11, r10)
            r2.put(r9, r8)
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            r4 = 0
            java.util.Map[] r5 = new java.util.Map[r4]
            java.lang.String r4 = r0.a((java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r5)
            r5 = 34
            java.lang.String r0 = "(This movie is of poor quality)"
            r6 = 1
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r0, (int) r6, (int) r5)
            boolean r0 = r0.isEmpty()
            r8 = r0 ^ 1
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r4)
            java.lang.String r6 = "iframe[src]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r6)
            java.util.Iterator r6 = r0.iterator()
        L_0x01bb:
            boolean r0 = r6.hasNext()
            java.lang.String r9 = "HQ"
            if (r0 == 0) goto L_0x03ed
            java.lang.Object r0 = r6.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r10 = "src"
            java.lang.String r0 = r0.b((java.lang.String) r10)
            java.lang.String r10 = "trailer"
            boolean r10 = r0.contains(r10)
            if (r10 == 0) goto L_0x01d8
            goto L_0x01bb
        L_0x01d8:
            boolean r10 = r0.startsWith(r14)
            if (r10 == 0) goto L_0x01ef
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = r1.c
            r10.append(r11)
            r10.append(r0)
            java.lang.String r0 = r10.toString()
        L_0x01ef:
            r10 = r0
            java.lang.String r0 = "Referer"
            r2.put(r0, r10)
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            r11 = 1
            java.util.Map[] r12 = new java.util.Map[r11]
            r13 = 0
            r12[r13] = r2
            java.lang.String r15 = "play=continue&x=0&y=0"
            java.lang.String r0 = r0.a((java.lang.String) r10, (java.lang.String) r15, (boolean) r11, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
            java.lang.String r12 = "Image Verification"
            boolean r12 = r0.contains(r12)
            if (r12 == 0) goto L_0x021b
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r12 = new java.util.Map[r11]
            r12[r13] = r2
            java.lang.String r15 = "play=continue&x=352&y=188"
            java.lang.String r0 = r0.a((java.lang.String) r10, (java.lang.String) r15, (boolean) r11, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
        L_0x021b:
            java.lang.String r12 = "ajax\\s*\\(\\s*\\{\\s*url\\s*:\\s*['\"](.*?)['\"]"
            java.util.ArrayList r12 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r12, (int) r11, (boolean) r11)
            java.lang.Object r11 = r12.get(r13)
            java.util.ArrayList r11 = (java.util.ArrayList) r11
            java.util.Iterator r11 = r11.iterator()
        L_0x022b:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x0244
            com.original.tase.helper.http.HttpHelper r12 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.Object r15 = r11.next()
            java.lang.String r15 = (java.lang.String) r15
            java.util.Map[] r5 = new java.util.Map[r13]
            r12.a((java.lang.String) r15, (java.util.Map<java.lang.String, java.lang.String>[]) r5)
            r5 = 34
            r13 = 0
            goto L_0x022b
        L_0x0244:
            java.lang.String r5 = "salt\\(\"([^\"]+)"
            r11 = 34
            r12 = 1
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r5, (int) r12, (int) r11)
            boolean r13 = r5.isEmpty()
            if (r13 == 0) goto L_0x0259
            java.lang.String r5 = "tlas\\(\"([^\"]+)"
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r5, (int) r12, (int) r11)
        L_0x0259:
            boolean r13 = r5.isEmpty()
            if (r13 == 0) goto L_0x0265
            java.lang.String r5 = "decrypt\\(\"([^\"]+)"
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r5, (int) r12, (int) r11)
        L_0x0265:
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x03df
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x029d }
            r0.<init>()     // Catch:{ Exception -> 0x029d }
            r0.append(r3)     // Catch:{ Exception -> 0x029d }
            java.lang.String r11 = new java.lang.String     // Catch:{ Exception -> 0x029d }
            java.lang.String r12 = new java.lang.String     // Catch:{ Exception -> 0x029d }
            r13 = 0
            byte[] r15 = android.util.Base64.decode(r5, r13)     // Catch:{ Exception -> 0x029d }
            java.lang.String r13 = "UTF-8"
            r12.<init>(r15, r13)     // Catch:{ Exception -> 0x029d }
            java.lang.String r12 = r1.j(r12)     // Catch:{ Exception -> 0x029d }
            r13 = 0
            byte[] r12 = android.util.Base64.decode(r12, r13)     // Catch:{ Exception -> 0x029d }
            java.lang.String r13 = "UTF-8"
            r11.<init>(r12, r13)     // Catch:{ Exception -> 0x029d }
            r0.append(r11)     // Catch:{ Exception -> 0x029d }
            java.lang.String r11 = "\n\n"
            r0.append(r11)     // Catch:{ Exception -> 0x029d }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x029d }
            r11 = r0
            goto L_0x029e
        L_0x029d:
            r11 = r3
        L_0x029e:
            boolean r0 = r11.isEmpty()
            if (r0 != 0) goto L_0x02ac
            java.lang.String r0 = "player"
            boolean r0 = r11.contains(r0)
            if (r0 != 0) goto L_0x02c5
        L_0x02ac:
            java.lang.String r0 = "for(var getFStr,getFCount,END_OF_INPUT=-1,arrChrs=new Array(\"A\",\"B\",\"C\",\"D\",\"E\",\"F\",\"G\",\"H\",\"I\",\"J\",\"K\",\"L\",\"M\",\"N\",\"O\",\"P\",\"Q\",\"R\",\"S\",\"T\",\"U\",\"V\",\"W\",\"X\",\"Y\",\"Z\",\"a\",\"b\",\"c\",\"d\",\"e\",\"f\",\"g\",\"h\",\"i\",\"j\",\"k\",\"l\",\"m\",\"n\",\"o\",\"p\",\"q\",\"r\",\"s\",\"t\",\"u\",\"v\",\"w\",\"x\",\"y\",\"z\",\"0\",\"1\",\"2\",\"3\",\"4\",\"5\",\"6\",\"7\",\"8\",\"9\",\"+\",\"/\"),reversegetFChars=new Array,i=0;i<arrChrs.length;i++)reversegetFChars[arrChrs[i]]=i;function ntos(r){return 1==(r=r.toString(16)).length&&(r=\"0\"+r),r=\"%\"+r,unescape(r)}function readReversegetF(){if(!getFStr)return END_OF_INPUT;for(;;){if(getFCount>=getFStr.length)return END_OF_INPUT;var r=getFStr.charAt(getFCount);if(getFCount++,reversegetFChars[r])return reversegetFChars[r];if(\"A\"==r)return 0}return END_OF_INPUT}function readgetF(){if(!getFStr)return END_OF_INPUT;if(getFCount>=getFStr.length)return END_OF_INPUT;var r=255&getFStr.charCodeAt(getFCount);return getFCount++,r}function setgetFStr(r){getFStr=r,getFCount=0}function getF(r){setgetFStr(r);for(var e=\"\",t=new Array(4),n=!1;!n&&(t[0]=readReversegetF())!=END_OF_INPUT&&(t[1]=readReversegetF())!=END_OF_INPUT;)t[2]=readReversegetF(),t[3]=readReversegetF(),e+=ntos(t[0]<<2&255|t[1]>>4),t[2]!=END_OF_INPUT?(e+=ntos(t[1]<<4&255|t[2]>>2),t[3]!=END_OF_INPUT?e+=ntos(t[2]<<6&255|t[3]):n=!0):n=!0;return e}function tor(r){var e=[],t=\"abcdefghijklmnopqrstuvwxyz\",n=\"\";for(j=0;j<t.length;j++){var F=t.charAt(j),g=t.charAt((j+13)%26);e[F]=g,e[F.toUpperCase()]=g.toUpperCase()}for(j=0;j<r.length;j++){var a=r.charAt(j);n+=\"A\"<=a&&a<=\"Z\"||\"a\"<=a&&a<=\"z\"?e[a]:a}return n}function acb(){return tor(getF(tor(\"####\")))}acb();"
            java.lang.String r12 = "####"
            java.lang.String r0 = r0.replace(r12, r5)
            com.squareup.duktape.Duktape r5 = com.squareup.duktape.Duktape.create()
            java.lang.Object r0 = r5.evaluate(r0)     // Catch:{ all -> 0x02c7 }
            if (r0 == 0) goto L_0x02c2
            java.lang.String r11 = r0.toString()     // Catch:{ all -> 0x02c7 }
        L_0x02c2:
            r5.close()
        L_0x02c5:
            r12 = 0
            goto L_0x02d1
        L_0x02c7:
            r0 = move-exception
            r12 = 0
            boolean[] r13 = new boolean[r12]     // Catch:{ all -> 0x03da }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r13)     // Catch:{ all -> 0x03da }
            r5.close()
        L_0x02d1:
            boolean r0 = r11.isEmpty()
            if (r0 != 0) goto L_0x03df
            java.lang.String r0 = "\\{\\s*['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]\\s*,\\s*.*?['\"]?label['\"]?\\s*:\\s*['\"]?(\\d{3,4})p"
            r13 = 2
            r15 = 34
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r11, (java.lang.String) r0, (int) r13, (int) r15)
            java.lang.Object r5 = r0.get(r12)
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            r13 = 1
            java.lang.Object r0 = r0.get(r13)
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            r20 = r2
            java.lang.String r2 = "\\{\\s*['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+\\.m3u8[^'\"]*)['\"]\\s*"
            java.util.ArrayList r2 = com.original.tase.utils.Regex.b((java.lang.String) r11, (java.lang.String) r2, (int) r13, (int) r15)
            java.lang.Object r2 = r2.get(r12)
            java.util.ArrayList r2 = (java.util.ArrayList) r2
            java.lang.String r12 = "src\\s*=\\s*[\"']([^\"']+)"
            java.lang.String r11 = com.original.tase.utils.Regex.a((java.lang.String) r11, (java.lang.String) r12, (int) r13)
            r12 = 0
        L_0x0302:
            int r13 = r2.size()
            if (r12 >= r13) goto L_0x0317
            java.lang.Object r13 = r2.get(r12)
            r5.add(r13)
            java.lang.String r13 = "HD"
            r0.add(r13)
            int r12 = r12 + 1
            goto L_0x0302
        L_0x0317:
            boolean r2 = r11.isEmpty()
            if (r2 != 0) goto L_0x0329
            r2 = 1
            boolean[] r12 = new boolean[r2]
            r2 = 0
            r12[r2] = r8
            r2 = r21
            r1.a(r2, r11, r9, r12)
            goto L_0x032b
        L_0x0329:
            r2 = r21
        L_0x032b:
            r9 = 0
        L_0x032c:
            int r11 = r5.size()
            if (r9 >= r11) goto L_0x03d7
            java.lang.Object r11 = r5.get(r9)
            java.lang.String r11 = (java.lang.String) r11
            boolean r12 = r11.startsWith(r14)
            if (r12 == 0) goto L_0x034f
            java.lang.StringBuilder r12 = new java.lang.StringBuilder
            r12.<init>()
            java.lang.String r13 = r1.c
            r12.append(r13)
            r12.append(r11)
            java.lang.String r11 = r12.toString()
        L_0x034f:
            java.lang.Object r12 = r0.get(r9)     // Catch:{ Exception -> 0x03c9 }
            java.lang.String r12 = (java.lang.String) r12     // Catch:{ Exception -> 0x03c9 }
            boolean r13 = r12.isEmpty()     // Catch:{ Exception -> 0x03c9 }
            if (r13 != 0) goto L_0x035c
            goto L_0x035e
        L_0x035c:
            java.lang.String r12 = "HD"
        L_0x035e:
            java.lang.String r13 = "hsl"
            boolean r13 = r11.contains(r13)     // Catch:{ Exception -> 0x03c9 }
            if (r13 != 0) goto L_0x0379
            java.lang.String r13 = "m3u8"
            boolean r13 = r11.contains(r13)     // Catch:{ Exception -> 0x03c9 }
            if (r13 == 0) goto L_0x036f
            goto L_0x0379
        L_0x036f:
            com.original.tase.helper.http.HttpHelper r13 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x03c9 }
            r15 = 0
            java.lang.String r13 = r13.a((java.lang.String) r11, (boolean) r15, (java.lang.String) r10)     // Catch:{ Exception -> 0x03c9 }
            goto L_0x037a
        L_0x0379:
            r13 = r11
        L_0x037a:
            java.lang.String r15 = "http 111"
            com.original.tase.Logger.a((java.lang.String) r15, (java.lang.String) r11)     // Catch:{ Exception -> 0x03c9 }
            java.lang.String r11 = "http 222"
            com.original.tase.Logger.a((java.lang.String) r11, (java.lang.String) r13)     // Catch:{ Exception -> 0x03c9 }
            boolean r11 = r13.isEmpty()     // Catch:{ Exception -> 0x03c9 }
            if (r11 != 0) goto L_0x03c9
            boolean r11 = com.original.tase.helper.GoogleVideoHelper.k(r13)     // Catch:{ Exception -> 0x03c9 }
            com.original.tase.model.media.MediaSource r15 = new com.original.tase.model.media.MediaSource     // Catch:{ Exception -> 0x03c9 }
            if (r8 == 0) goto L_0x03ac
            r17 = r0
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03cb }
            r0.<init>()     // Catch:{ Exception -> 0x03cb }
            r18 = r3
            java.lang.String r3 = r19.a()     // Catch:{ Exception -> 0x03cd }
            r0.append(r3)     // Catch:{ Exception -> 0x03cd }
            java.lang.String r3 = " (CAM)"
            r0.append(r3)     // Catch:{ Exception -> 0x03cd }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x03cd }
            goto L_0x03b4
        L_0x03ac:
            r17 = r0
            r18 = r3
            java.lang.String r0 = r19.a()     // Catch:{ Exception -> 0x03cd }
        L_0x03b4:
            if (r11 == 0) goto L_0x03b9
            java.lang.String r3 = "GoogleVideo"
            goto L_0x03bb
        L_0x03b9:
            java.lang.String r3 = "CDN-FastServer"
        L_0x03bb:
            r11 = 0
            r15.<init>(r0, r3, r11)     // Catch:{ Exception -> 0x03cd }
            r15.setStreamLink(r13)     // Catch:{ Exception -> 0x03cd }
            r15.setQuality((java.lang.String) r12)     // Catch:{ Exception -> 0x03cd }
            r2.onNext(r15)     // Catch:{ Exception -> 0x03cd }
            goto L_0x03cd
        L_0x03c9:
            r17 = r0
        L_0x03cb:
            r18 = r3
        L_0x03cd:
            int r9 = r9 + 1
            r0 = r17
            r3 = r18
            r15 = 34
            goto L_0x032c
        L_0x03d7:
            r18 = r3
            goto L_0x03e5
        L_0x03da:
            r0 = move-exception
            r5.close()
            throw r0
        L_0x03df:
            r20 = r2
            r18 = r3
            r2 = r21
        L_0x03e5:
            r2 = r20
            r3 = r18
            r5 = 34
            goto L_0x01bb
        L_0x03ed:
            r2 = r21
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r4)
            java.lang.String r3 = "table"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r3)
            java.util.Iterator r0 = r0.iterator()
        L_0x03fd:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x044a
            java.lang.Object r3 = r0.next()
            org.jsoup.nodes.Element r3 = (org.jsoup.nodes.Element) r3
            java.lang.String r4 = "a[href][target=\"_blank\"]"
            org.jsoup.select.Elements r3 = r3.g((java.lang.String) r4)
            java.util.Iterator r3 = r3.iterator()
        L_0x0413:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x03fd
            java.lang.Object r4 = r3.next()
            org.jsoup.nodes.Element r4 = (org.jsoup.nodes.Element) r4
            java.lang.String r4 = r4.b((java.lang.String) r7)
            java.lang.String r5 = "http"
            boolean r5 = r4.startsWith(r5)
            if (r5 == 0) goto L_0x0447
            java.lang.String r5 = "((?:http|https)://)"
            r6 = 1
            java.util.ArrayList r5 = com.original.tase.utils.Regex.b((java.lang.String) r4, (java.lang.String) r5, (int) r6, (boolean) r6)
            r10 = 0
            java.lang.Object r5 = r5.get(r10)
            java.util.ArrayList r5 = (java.util.ArrayList) r5
            int r5 = r5.size()
            if (r5 > r6) goto L_0x0413
            boolean[] r5 = new boolean[r6]
            r5[r10] = r8
            r1.a(r2, r4, r9, r5)
            goto L_0x0413
        L_0x0447:
            r6 = 1
            r10 = 0
            goto L_0x0413
        L_0x044a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.AfdahTV.a(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }
}
