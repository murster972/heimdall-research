package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class SpaceMov extends BaseProvider {
    private String c = (Utils.getProvider(90) + "/");

    public String a() {
        return "SpaceMov";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        String str2;
        boolean z;
        boolean z2 = movieInfo.getType().intValue() == 1;
        String replace = str.replace("\\/", "/");
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        hashMap.put("referer", replace);
        HttpHelper.e().c(replace, replace);
        HttpHelper e = HttpHelper.e();
        Document b = Jsoup.b(e.a(replace + "watching/", (Map<String, String>[]) new Map[]{hashMap}));
        Element h = b.h("div[class=mvici-right]");
        if (h != null) {
            str2 = h.h("span[class=quality]").G();
            if (movieInfo.year.equals(h.h("a[rel=tag]").G())) {
                return;
            }
        } else {
            str2 = "HQ";
        }
        if (str2.toLowerCase().contains("cam") || str2.toLowerCase().contains("ts")) {
            str2 = "CAM";
            z = true;
        } else {
            z = false;
        }
        if (z2) {
            Iterator it2 = Regex.b(b.g("div[class=les-content]").b("a").toString(), "data-svv[0-9]=['\"](.+?)['\"]", 1, true).get(0).iterator();
            while (it2.hasNext()) {
                String obj = it2.next().toString();
                if (!obj.isEmpty()) {
                    a(observableEmitter, obj, str2, z);
                }
            }
            return;
        }
        Iterator it3 = b.g("div[class=les-content]").b("a").iterator();
        while (it3.hasNext()) {
            Element element = (Element) it3.next();
            String lowerCase = element.G().toLowerCase();
            if (lowerCase.contains("episode " + movieInfo.eps)) {
                Iterator it4 = Regex.b(element.toString(), "data-svv[0-9]=['\"](.+?)['\"]", 1, true).get(0).iterator();
                while (it4.hasNext()) {
                    String obj2 = it4.next().toString();
                    if (!obj2.isEmpty()) {
                        a(observableEmitter, obj2, str2, z);
                    }
                }
                return;
            }
        }
    }

    public String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        HashMap<String, String> a2 = Constants.a();
        a2.put("referer", this.c);
        a2.put("user-agent", Constants.f5838a);
        String e = TitleHelper.e(TitleHelper.g(movieInfo.getName()).replace("'", "-").replace("-", "+"));
        HttpHelper e2 = HttpHelper.e();
        Iterator it2 = Jsoup.b(e2.a(this.c + "search-query/" + e.toLowerCase() + "/", "g-recaptcha-response=03AOLTBLRAO34thW-Dwc-sQ74r0ShCIHECcz4sImuv-1i5ma7Zs9uwlyYIQ1u4bxDsdrjanmbtHL1aTck_UrlNOcXXsByRZ7VaDLMWzn2DwX82gA58khcWNWZowFi6iF0bxcQgHekoJn1nWnb6GyJCwZQqFe_NMjrZpvFBmhHE9ye8Y172SxczcXQvLp-seW40IVKlcEU62Fv-umXBjmiYDOlVXYAiXfYIc90RhXzHKj05Yq8dXSGlTl5TGCJnpO82UNHQY38geeUWmszyvxIoOK0LJrUwl4bPqsZJlE--" + DateTimeHelper.c() + "QKLm3NkpcCCTenol5xPUKCuoK_o_", (Map<String, String>[]) new Map[]{a2})).g("div.movies-list.movies-list-full").b("div.ml-item").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a[title]");
            if (h != null || h.x()) {
                String b = h.b("href");
                String replaceAll = h.b("title").replaceAll("\\<[^>]*>", "");
                if (!z) {
                    if (replaceAll.toLowerCase().equals(movieInfo.name.toLowerCase() + " - season " + movieInfo.session)) {
                        return b;
                    }
                } else if (movieInfo.name.toLowerCase().equals(replaceAll.toLowerCase())) {
                    return b;
                }
            }
        }
        return "";
    }
}
