package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.RxBus;
import com.original.tase.event.ReCaptchaRequiredEvent;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Map;

public class SeeHD extends BaseProvider {
    private String c = Utils.getProvider(59);

    public String a() {
        return "SeeHD";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (HttpHelper.e().a(this.c, Constants.f5838a, "http://www.seehd.pl/", (Map<String, String>[]) new Map[0]).contains("Please complete the security check to access")) {
            RxBus.b().a(new ReCaptchaRequiredEvent(a(), "http://www.seehd.pl"));
            return;
        }
        String str = this.c + "/" + TitleHelper.g(TitleHelper.e(movieInfo.name)) + "-watch-online/";
        String a2 = HttpHelper.e().a(str, Constants.f5838a, "http://www.seehd.pl/", (Map<String, String>[]) new Map[0]);
        if (a2.isEmpty() || a2.toLowerCase().contains("page not found")) {
            str = this.c + "/" + TitleHelper.g(TitleHelper.e(movieInfo.name)) + "-" + movieInfo.year + "-watch-online/";
        }
        a(observableEmitter, str, movieInfo, false);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (HttpHelper.e().a(this.c, Constants.f5838a, "http://www.seehd.pl/", (Map<String, String>[]) new Map[0]).contains("Please complete the security check to access")) {
            RxBus.b().a(new ReCaptchaRequiredEvent(a(), "http://www.seehd.pl/"));
            return;
        }
        a(observableEmitter, this.c + "/" + TitleHelper.g(TitleHelper.e(movieInfo.name.replace("Marvel's ", "").replace("DC's ", ""))) + "-s" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.session)) + "e" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.eps)) + "-watch-online/", movieInfo, true);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r8v16, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v7, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v34, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r15v15, resolved type: java.lang.String} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:55:0x0162  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r26, java.lang.String r27, com.movie.data.model.MovieInfo r28, boolean r29) {
        /*
            r25 = this;
            r0 = r25
            r1 = r26
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = com.original.Constants.f5838a
            r4 = 0
            java.util.Map[] r5 = new java.util.Map[r4]
            java.lang.String r6 = "http://www.seehd.pl/"
            r7 = r27
            java.lang.String r2 = r2.a((java.lang.String) r7, (java.lang.String) r3, (java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r5)
            boolean r3 = r2.isEmpty()
            if (r3 != 0) goto L_0x0472
            r3 = 2
            r5 = 1
            java.lang.String r6 = "Quality\\s*:\\s*(?:</strong>)?\\s*(.*?)<"
            java.lang.String r3 = com.original.tase.utils.Regex.a((java.lang.String) r2, (java.lang.String) r6, (int) r5, (int) r3)
            java.lang.String r3 = r3.trim()
            java.lang.String r3 = r3.toLowerCase()
            java.lang.String r6 = "ts"
            boolean r6 = r3.contains(r6)
            if (r6 != 0) goto L_0x003e
            java.lang.String r6 = "cam"
            boolean r6 = r3.contains(r6)
            if (r6 == 0) goto L_0x003c
            goto L_0x003e
        L_0x003c:
            r6 = 0
            goto L_0x003f
        L_0x003e:
            r6 = 1
        L_0x003f:
            org.jsoup.nodes.Document r2 = org.jsoup.Jsoup.b(r2)
            java.lang.String r8 = "div.tabcontent"
            org.jsoup.select.Elements r2 = r2.g((java.lang.String) r8)
            java.util.Iterator r2 = r2.iterator()
        L_0x004d:
            boolean r8 = r2.hasNext()
            if (r8 == 0) goto L_0x0472
            java.lang.Object r8 = r2.next()
            org.jsoup.nodes.Element r8 = (org.jsoup.nodes.Element) r8
            java.lang.String r9 = "source[src]"
            org.jsoup.select.Elements r9 = r8.g((java.lang.String) r9)
            java.util.Iterator r9 = r9.iterator()
        L_0x0063:
            boolean r10 = r9.hasNext()
            java.lang.String r11 = "CDN"
            java.lang.String r12 = " (CAM)"
            java.lang.String r13 = "GoogleVideo"
            java.lang.String r14 = "src"
            java.lang.String r15 = "HD"
            if (r10 == 0) goto L_0x00bc
            java.lang.Object r10 = r9.next()
            org.jsoup.nodes.Element r10 = (org.jsoup.nodes.Element) r10
            java.lang.String r10 = r10.b((java.lang.String) r14)
            boolean r14 = com.original.tase.helper.GoogleVideoHelper.k(r10)
            com.original.tase.model.media.MediaSource r5 = new com.original.tase.model.media.MediaSource
            if (r6 == 0) goto L_0x009b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r16 = r2
            java.lang.String r2 = r25.a()
            r4.append(r2)
            r4.append(r12)
            java.lang.String r2 = r4.toString()
            goto L_0x00a1
        L_0x009b:
            r16 = r2
            java.lang.String r2 = r25.a()
        L_0x00a1:
            if (r14 == 0) goto L_0x00a4
            r11 = r13
        L_0x00a4:
            r4 = 0
            r5.<init>(r2, r11, r4)
            r5.setStreamLink(r10)
            if (r14 == 0) goto L_0x00b1
            java.lang.String r15 = com.original.tase.helper.GoogleVideoHelper.h(r10)
        L_0x00b1:
            r5.setQuality((java.lang.String) r15)
            r1.onNext(r5)
            r2 = r16
            r4 = 0
            r5 = 1
            goto L_0x0063
        L_0x00bc:
            r16 = r2
            java.lang.String r2 = "iframe[src]"
            org.jsoup.select.Elements r4 = r8.g((java.lang.String) r2)
            java.util.Iterator r4 = r4.iterator()
            boolean r5 = r4.hasNext()
            java.lang.String r9 = "seehd"
            if (r5 != 0) goto L_0x015a
            java.lang.String r5 = "a[href]"
            org.jsoup.select.Elements r5 = r8.g((java.lang.String) r5)     // Catch:{ all -> 0x0151 }
            java.util.Iterator r5 = r5.iterator()     // Catch:{ all -> 0x0151 }
        L_0x00da:
            boolean r8 = r5.hasNext()     // Catch:{ all -> 0x0151 }
            if (r8 == 0) goto L_0x015a
            java.lang.Object r8 = r5.next()     // Catch:{ all -> 0x0151 }
            org.jsoup.nodes.Element r8 = (org.jsoup.nodes.Element) r8     // Catch:{ all -> 0x0151 }
            java.lang.String r10 = "href"
            java.lang.String r7 = r8.b((java.lang.String) r10)     // Catch:{ all -> 0x0151 }
            boolean r8 = r7.contains(r9)     // Catch:{ all -> 0x0151 }
            if (r8 != 0) goto L_0x0148
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x0151 }
            r27 = r3
            r10 = 0
            java.util.Map[] r3 = new java.util.Map[r10]     // Catch:{ all -> 0x0153 }
            java.lang.String r3 = r8.a((java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r3)     // Catch:{ all -> 0x0153 }
            java.lang.String r8 = "href=[\"]([^\"]*fullscreen[^\"]+)[\"]"
            r10 = 1
            java.lang.String r7 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r8, (int) r10)     // Catch:{ all -> 0x0153 }
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x0153 }
            r8 = 0
            java.util.Map[] r10 = new java.util.Map[r8]     // Catch:{ all -> 0x0153 }
            java.lang.String r3 = r3.a((java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r10)     // Catch:{ all -> 0x0153 }
            org.jsoup.nodes.Document r3 = org.jsoup.Jsoup.b(r3)     // Catch:{ all -> 0x0153 }
            java.lang.String r8 = "pre.prettyprint"
            org.jsoup.nodes.Element r3 = r3.h(r8)     // Catch:{ all -> 0x0153 }
            java.lang.String r3 = r3.G()     // Catch:{ all -> 0x0153 }
            java.lang.String r8 = "\n"
            java.lang.String[] r3 = r3.split(r8)     // Catch:{ all -> 0x0153 }
            int r8 = r3.length     // Catch:{ all -> 0x0153 }
            r10 = 0
        L_0x0127:
            if (r10 >= r8) goto L_0x0143
            r17 = r5
            r5 = r3[r10]     // Catch:{ all -> 0x0153 }
            r18 = r3
            r19 = r7
            r3 = 1
            boolean[] r7 = new boolean[r3]     // Catch:{ all -> 0x0155 }
            r3 = 0
            r7[r3] = r3     // Catch:{ all -> 0x0155 }
            r0.a(r1, r5, r15, r7)     // Catch:{ all -> 0x0155 }
            int r10 = r10 + 1
            r5 = r17
            r3 = r18
            r7 = r19
            goto L_0x0127
        L_0x0143:
            r17 = r5
            r19 = r7
            goto L_0x014c
        L_0x0148:
            r27 = r3
            r17 = r5
        L_0x014c:
            r3 = r27
            r5 = r17
            goto L_0x00da
        L_0x0151:
            r27 = r3
        L_0x0153:
            r19 = r7
        L_0x0155:
            r3 = r27
            r7 = r19
            goto L_0x015c
        L_0x015a:
            r27 = r3
        L_0x015c:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x0468
            java.lang.Object r5 = r4.next()
            org.jsoup.nodes.Element r5 = (org.jsoup.nodes.Element) r5
            java.lang.String r5 = r5.b((java.lang.String) r14)
            java.lang.String r8 = "songs2dl"
            boolean r8 = r5.contains(r8)
            if (r8 == 0) goto L_0x01f1
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r10 = com.original.Constants.f5838a
            r27 = r3
            r17 = r4
            r3 = 0
            java.util.Map[] r4 = new java.util.Map[r3]
            java.lang.String r3 = r8.a((java.lang.String) r5, (java.lang.String) r10, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r4)
            java.util.ArrayList r3 = r0.g(r3)
            java.util.Iterator r3 = r3.iterator()
            r4 = r27
        L_0x018f:
            boolean r5 = r3.hasNext()
            if (r5 == 0) goto L_0x01e2
            java.lang.Object r4 = r3.next()
            java.lang.String r4 = (java.lang.String) r4
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.k(r4)
            com.original.tase.model.media.MediaSource r8 = new com.original.tase.model.media.MediaSource
            if (r6 == 0) goto L_0x01b9
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r18 = r3
            java.lang.String r3 = r25.a()
            r10.append(r3)
            r10.append(r12)
            java.lang.String r3 = r10.toString()
            goto L_0x01bf
        L_0x01b9:
            r18 = r3
            java.lang.String r3 = r25.a()
        L_0x01bf:
            if (r5 == 0) goto L_0x01c5
            r19 = r11
            r10 = r13
            goto L_0x01c8
        L_0x01c5:
            r10 = r11
            r19 = r10
        L_0x01c8:
            r11 = 0
            r8.<init>(r3, r10, r11)
            r8.setStreamLink(r4)
            if (r5 == 0) goto L_0x01d6
            java.lang.String r3 = com.original.tase.helper.GoogleVideoHelper.h(r4)
            goto L_0x01d7
        L_0x01d6:
            r3 = r15
        L_0x01d7:
            r8.setQuality((java.lang.String) r3)
            r1.onNext(r8)
            r3 = r18
            r11 = r19
            goto L_0x018f
        L_0x01e2:
            r19 = r11
            r24 = r2
            r3 = r4
            r21 = r7
            r23 = r9
            r4 = 1
            r8 = 0
            r2 = r0
            r0 = r15
            goto L_0x045a
        L_0x01f1:
            r27 = r3
            r17 = r4
            r19 = r11
            boolean r3 = r5.contains(r9)
            java.lang.String r4 = "HQ"
            java.lang.String r8 = "raptu"
            java.lang.String r10 = "rapidvideo"
            if (r3 == 0) goto L_0x0436
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r11 = com.original.Constants.f5838a
            r18 = r4
            r20 = r15
            r4 = 0
            java.util.Map[] r15 = new java.util.Map[r4]
            java.lang.String r3 = r3.a((java.lang.String) r5, (java.lang.String) r11, (java.lang.String) r7, (java.util.Map<java.lang.String, java.lang.String>[]) r15)
            org.jsoup.nodes.Document r3 = org.jsoup.Jsoup.b(r3)
            org.jsoup.nodes.Element r3 = r3.h(r2)
            java.lang.String r4 = "Cookie"
            java.lang.String r11 = "User-Agent"
            if (r3 == 0) goto L_0x0373
            java.lang.String r15 = r3.b((java.lang.String) r14)
            r21 = r7
            java.lang.String r7 = "/"
            boolean r7 = r15.startsWith(r7)
            if (r7 == 0) goto L_0x0244
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r22 = r3
            java.lang.String r3 = r0.c
            r7.append(r3)
            r7.append(r15)
            java.lang.String r3 = r7.toString()
            goto L_0x0247
        L_0x0244:
            r22 = r3
            r3 = r15
        L_0x0247:
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.k(r3)
            if (r7 != 0) goto L_0x036d
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.j(r3)
            if (r7 != 0) goto L_0x036d
            java.lang.String r7 = r3.trim()
            java.lang.String r7 = r7.toLowerCase()
            boolean r7 = r7.contains(r9)
            if (r7 != 0) goto L_0x0271
            java.lang.String r7 = r3.trim()
            java.lang.String r7 = r7.toLowerCase()
            java.lang.String r15 = ".html"
            boolean r7 = r7.contains(r15)
            if (r7 == 0) goto L_0x036d
        L_0x0271:
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r15 = com.original.Constants.f5838a
            r23 = r9
            r9 = 0
            java.util.Map[] r0 = new java.util.Map[r9]
            java.lang.String r0 = r7.a((java.lang.String) r3, (java.lang.String) r15, (java.lang.String) r5, (java.util.Map<java.lang.String, java.lang.String>[]) r0)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            org.jsoup.nodes.Element r0 = r0.h(r2)
            if (r0 == 0) goto L_0x0368
            java.lang.String r0 = r0.b((java.lang.String) r14)
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.j(r0)
            if (r5 == 0) goto L_0x031d
            java.util.HashMap r5 = com.original.tase.helper.GoogleVideoHelper.g(r0)
            if (r5 == 0) goto L_0x0368
            boolean r7 = r5.isEmpty()
            if (r7 != 0) goto L_0x0368
            java.util.Set r5 = r5.entrySet()
            java.util.Iterator r5 = r5.iterator()
        L_0x02a8:
            boolean r7 = r5.hasNext()
            if (r7 == 0) goto L_0x0368
            java.lang.Object r7 = r5.next()
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7
            java.lang.Object r8 = r7.getKey()
            java.lang.String r8 = (java.lang.String) r8
            com.original.tase.model.media.MediaSource r9 = new com.original.tase.model.media.MediaSource
            if (r6 == 0) goto L_0x02d4
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r24 = r2
            java.lang.String r2 = r25.a()
            r15.append(r2)
            r15.append(r12)
            java.lang.String r2 = r15.toString()
            goto L_0x02da
        L_0x02d4:
            r24 = r2
            java.lang.String r2 = r25.a()
        L_0x02da:
            r15 = 0
            r9.<init>(r2, r13, r15)
            r9.setOriginalLink(r0)
            r9.setStreamLink(r8)
            java.lang.Object r2 = r7.getValue()
            java.lang.String r2 = (java.lang.String) r2
            boolean r2 = r2.isEmpty()
            if (r2 == 0) goto L_0x02f8
            java.lang.Object r2 = r7.getValue()
            r15 = r2
            java.lang.String r15 = (java.lang.String) r15
            goto L_0x02fa
        L_0x02f8:
            r15 = r20
        L_0x02fa:
            r9.setQuality((java.lang.String) r15)
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r8 = com.original.Constants.f5838a
            r2.put(r11, r8)
            java.lang.Object r7 = r7.getKey()
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.original.tase.helper.GoogleVideoHelper.c(r0, r7)
            r2.put(r4, r7)
            r9.setPlayHeader(r2)
            r1.onNext(r9)
            r2 = r24
            goto L_0x02a8
        L_0x031d:
            r24 = r2
            boolean r2 = r0.contains(r10)
            if (r2 != 0) goto L_0x034a
            boolean r2 = r0.contains(r8)
            if (r2 == 0) goto L_0x032c
            goto L_0x034a
        L_0x032c:
            boolean r2 = com.original.tase.helper.GoogleVideoHelper.k(r0)
            if (r2 == 0) goto L_0x036a
            com.original.tase.model.media.MediaSource r2 = new com.original.tase.model.media.MediaSource
            java.lang.String r5 = r25.a()
            r7 = 1
            r2.<init>(r5, r13, r7)
            r2.setStreamLink(r0)
            java.lang.String r0 = com.original.tase.helper.GoogleVideoHelper.h(r0)
            r2.setQuality((java.lang.String) r0)
            r1.onNext(r2)
            goto L_0x036a
        L_0x034a:
            boolean r2 = r0.contains(r10)
            if (r2 != 0) goto L_0x035a
            boolean r2 = r0.contains(r8)
            if (r2 == 0) goto L_0x0357
            goto L_0x035a
        L_0x0357:
            r3 = r18
            goto L_0x035c
        L_0x035a:
            r3 = r20
        L_0x035c:
            r2 = 1
            boolean[] r5 = new boolean[r2]
            r7 = 0
            r5[r7] = r2
            r2 = r25
            r2.a(r1, r0, r3, r5)
            goto L_0x037e
        L_0x0368:
            r24 = r2
        L_0x036a:
            r2 = r25
            goto L_0x037e
        L_0x036d:
            r24 = r2
            r23 = r9
            r2 = r0
            goto L_0x037e
        L_0x0373:
            r24 = r2
            r22 = r3
            r21 = r7
            r23 = r9
            r2 = r0
            r3 = r27
        L_0x037e:
            if (r22 == 0) goto L_0x0431
            r0 = r22
            java.lang.String r0 = r0.b((java.lang.String) r14)
            boolean r5 = com.original.tase.helper.GoogleVideoHelper.j(r0)
            if (r5 == 0) goto L_0x0407
            java.util.HashMap r5 = com.original.tase.helper.GoogleVideoHelper.g(r0)
            java.util.Set r5 = r5.entrySet()
            java.util.Iterator r5 = r5.iterator()
        L_0x0398:
            boolean r7 = r5.hasNext()
            if (r7 == 0) goto L_0x0431
            java.lang.Object r7 = r5.next()
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7
            java.lang.Object r8 = r7.getKey()
            java.lang.String r8 = (java.lang.String) r8
            com.original.tase.model.media.MediaSource r9 = new com.original.tase.model.media.MediaSource
            if (r6 == 0) goto L_0x03c2
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r15 = r25.a()
            r10.append(r15)
            r10.append(r12)
            java.lang.String r10 = r10.toString()
            goto L_0x03c6
        L_0x03c2:
            java.lang.String r10 = r25.a()
        L_0x03c6:
            r15 = 0
            r9.<init>(r10, r13, r15)
            r9.setOriginalLink(r0)
            r9.setStreamLink(r8)
            java.lang.Object r8 = r7.getValue()
            java.lang.String r8 = (java.lang.String) r8
            boolean r8 = r8.isEmpty()
            if (r8 == 0) goto L_0x03e4
            java.lang.Object r8 = r7.getValue()
            r15 = r8
            java.lang.String r15 = (java.lang.String) r15
            goto L_0x03e6
        L_0x03e4:
            r15 = r20
        L_0x03e6:
            r9.setQuality((java.lang.String) r15)
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            java.lang.String r10 = com.original.Constants.f5838a
            r8.put(r11, r10)
            java.lang.Object r7 = r7.getKey()
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r7 = com.original.tase.helper.GoogleVideoHelper.c(r0, r7)
            r8.put(r4, r7)
            r9.setPlayHeader(r8)
            r1.onNext(r9)
            goto L_0x0398
        L_0x0407:
            boolean r4 = r0.contains(r10)
            if (r4 == 0) goto L_0x0417
            r4 = 1
            boolean[] r5 = new boolean[r4]
            r7 = 0
            r5[r7] = r6
            r2.a(r1, r0, r3, r5)
            goto L_0x0431
        L_0x0417:
            r4 = 1
            com.original.tase.model.media.MediaSource r5 = new com.original.tase.model.media.MediaSource
            java.lang.String r7 = r25.a()
            java.lang.String r8 = r25.a()
            r5.<init>(r7, r8, r4)
            r5.setStreamLink(r0)
            r0 = r20
            r5.setQuality((java.lang.String) r0)
            r1.onNext(r5)
            goto L_0x0433
        L_0x0431:
            r0 = r20
        L_0x0433:
            r4 = 1
            r8 = 0
            goto L_0x045a
        L_0x0436:
            r24 = r2
            r18 = r4
            r21 = r7
            r23 = r9
            r2 = r0
            r0 = r15
            boolean r3 = r5.contains(r10)
            if (r3 != 0) goto L_0x0450
            boolean r3 = r5.contains(r8)
            if (r3 == 0) goto L_0x044d
            goto L_0x0450
        L_0x044d:
            r3 = r18
            goto L_0x0451
        L_0x0450:
            r3 = r0
        L_0x0451:
            r4 = 1
            boolean[] r7 = new boolean[r4]
            r8 = 0
            r7[r8] = r6
            r2.a(r1, r5, r3, r7)
        L_0x045a:
            r15 = r0
            r0 = r2
            r4 = r17
            r11 = r19
            r7 = r21
            r9 = r23
            r2 = r24
            goto L_0x015c
        L_0x0468:
            r27 = r3
            r21 = r7
            r2 = r16
            r4 = 0
            r5 = 1
            goto L_0x004d
        L_0x0472:
            r2 = r0
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.SeeHD.a(io.reactivex.ObservableEmitter, java.lang.String, com.movie.data.model.MovieInfo, boolean):void");
    }
}
