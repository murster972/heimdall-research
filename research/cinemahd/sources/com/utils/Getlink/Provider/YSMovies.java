package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class YSMovies extends BaseProvider {
    private String c = "https://ymovies.se";

    public String a() {
        return "YSMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        int i = 0;
        String format = String.format(this.c + "/?s=%s&genre=%s&years=%s", new Object[]{Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+"), movieInfo.genres.get(0), movieInfo.year});
        String a2 = Regex.a(HttpHelper.e().b(format, this.c + "/"), "[\"']?posts[\"']?\\s*:\\s*([\\[].*[\\]])", 1);
        try {
            JSONArray jSONArray = new JSONArray(a2);
            if (jSONArray.length() > 0) {
                while (i < jSONArray.length()) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    String string = jSONObject.getString("title");
                    String string2 = jSONObject.getString("year");
                    jSONObject.getString("ID");
                    if (!movieInfo.name.equals(string) || string2.isEmpty() || !string2.contains(movieInfo.year)) {
                        i++;
                    } else {
                        String string3 = jSONObject.getString("link");
                        if (string3.startsWith("/")) {
                            string3 = this.c + string3;
                        }
                        return string3.replace("\\/", "/");
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Iterator it2 = Jsoup.b(a2).g("div.tab-content").b("h2.title").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String G = element.G();
            String G2 = element.h("span").G();
            if (movieInfo.name.equals(G) && !G2.isEmpty() && G2.contains(movieInfo.year)) {
                String b = element.b("href");
                if (!b.startsWith("/")) {
                    return b;
                }
                return this.c + b;
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /*  JADX ERROR: JadxRuntimeException in pass: InitCodeVariables
        jadx.core.utils.exceptions.JadxRuntimeException: Several immutable types in one variable: [int, boolean], vars: [r9v0 ?, r9v1 ?, r9v11 ?, r9v2 ?, r9v3 ?, r9v4 ?, r9v7 ?, r9v8 ?, r9v10 ?]
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVarType(InitCodeVariables.java:102)
        	at jadx.core.dex.visitors.InitCodeVariables.setCodeVar(InitCodeVariables.java:78)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVar(InitCodeVariables.java:69)
        	at jadx.core.dex.visitors.InitCodeVariables.initCodeVars(InitCodeVariables.java:51)
        	at jadx.core.dex.visitors.InitCodeVariables.visit(InitCodeVariables.java:32)
        */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r20, java.lang.String r21) {
        /*
            r19 = this;
            r1 = r19
            r0 = r20
            r2 = r21
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r4 = com.original.Constants.f5838a
            java.lang.String r5 = "User-Agent"
            r3.put(r5, r4)
            java.lang.String r4 = "/"
            boolean r4 = r2.endsWith(r4)
            if (r4 == 0) goto L_0x002c
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r2)
            java.lang.String r5 = "watching/?playermode=#box"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            goto L_0x003d
        L_0x002c:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r2)
            java.lang.String r5 = "/watching/?playermode=#box"
            r4.append(r5)
            java.lang.String r4 = r4.toString()
        L_0x003d:
            com.original.tase.helper.http.HttpHelper r5 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r4 = r5.b((java.lang.String) r4, (java.lang.String) r2)
            r5 = 1
            java.lang.String r6 = "(\\(function\\s*\\(\\)\\{\\s*var.*eval\\(\\w+\\.join\\(\\'\\'\\)\\)\\;\\}\\))\\(\\)<\\/script>\\s*<div\\s*class=\\\"videoPlayer\\s*row\\\">"
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r6, (int) r5)
            java.lang.Object[] r6 = new java.lang.Object[r5]
            java.lang.String r7 = "return "
            java.lang.String r8 = "eval"
            java.lang.String r4 = r4.replace(r8, r7)
            r9 = 0
            r6[r9] = r4
            java.lang.String r4 = "var encode = %s\n\nfunction acb()\n{\n    var result = eval(encode);\n    return result();\n}\n acb();"
            java.lang.String r4 = java.lang.String.format(r4, r6)
            com.squareup.duktape.Duktape r6 = com.squareup.duktape.Duktape.create()
            java.lang.Object r4 = r6.evaluate(r4)     // Catch:{ all -> 0x0262 }
            r6.close()
            if (r4 == 0) goto L_0x0261
            java.lang.String r10 = r4.toString()
            boolean r10 = r10.isEmpty()
            if (r10 != 0) goto L_0x0261
            java.lang.String r4 = r4.toString()
            java.lang.String r10 = "window.parametros\\s*=\\s*[\"']([^\"']+[^\"'])[\"']"
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r10, (int) r5)
            java.lang.String r10 = "&"
            java.lang.String[] r4 = r4.split(r10)
            int r10 = r4.length
            r11 = r2
            r2 = 0
        L_0x0089:
            if (r2 >= r10) goto L_0x0261
            r12 = r4[r2]
            java.lang.String r13 = "="
            java.lang.String[] r12 = r12.split(r13)
            r13 = r12[r9]
            boolean r13 = r13.isEmpty()
            if (r13 == 0) goto L_0x00a2
        L_0x009b:
            r18 = r4
            r17 = r10
            r4 = 1
            goto L_0x0258
        L_0x00a2:
            r13 = r12[r9]
            java.lang.String r14 = "lox"
            boolean r13 = r13.equals(r14)
            java.lang.String r14 = "HD"
            if (r13 == 0) goto L_0x00c7
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "https://vidlox.tv/embed-"
            r11.append(r13)
            r12 = r12[r5]
            r11.append(r12)
            java.lang.String r11 = r11.toString()
        L_0x00c1:
            r18 = r4
            r17 = r10
            goto L_0x0246
        L_0x00c7:
            r13 = r12[r9]
            java.lang.String r15 = "jaw"
            boolean r13 = r13.equals(r15)
            if (r13 == 0) goto L_0x00e5
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "https://jawcloud.co/embed-"
            r11.append(r13)
            r12 = r12[r5]
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            goto L_0x00c1
        L_0x00e5:
            r13 = r12[r9]
            java.lang.String r15 = "vsh"
            boolean r13 = r13.equals(r15)
            if (r13 == 0) goto L_0x0103
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "https://vshare.eu/embed-"
            r11.append(r13)
            r12 = r12[r5]
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            goto L_0x00c1
        L_0x0103:
            r13 = r12[r9]
            java.lang.String r15 = "rpt"
            boolean r13 = r13.equals(r15)
            if (r13 == 0) goto L_0x0121
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "https://www.bitporno.com/e/"
            r11.append(r13)
            r12 = r12[r5]
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            goto L_0x00c1
        L_0x0121:
            r13 = r12[r9]
            java.lang.String r15 = "vza"
            boolean r13 = r13.equals(r15)
            if (r13 == 0) goto L_0x013f
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "https://vidoza.net/embed-"
            r11.append(r13)
            r12 = r12[r5]
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            goto L_0x00c1
        L_0x013f:
            r13 = r12[r9]
            java.lang.String r15 = "rpd"
            boolean r13 = r13.equals(r15)
            if (r13 == 0) goto L_0x015e
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r13 = "https://www.rapidvideo.com/embed/"
            r11.append(r13)
            r12 = r12[r5]
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            goto L_0x00c1
        L_0x015e:
            r13 = r12[r9]
            java.lang.String r15 = "pic"
            boolean r13 = r13.equals(r15)
            if (r13 == 0) goto L_0x00c1
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            java.lang.String r15 = r1.c
            r13.append(r15)
            java.lang.String r15 = "/playerlite/pk/pk/plugins/player_p2.php"
            r13.append(r15)
            java.lang.String r13 = r13.toString()
            r15 = 2
            java.lang.Object[] r15 = new java.lang.Object[r15]
            r16 = r12[r5]
            r15[r9] = r16
            r12 = r12[r9]
            r15[r5] = r12
            java.lang.String r12 = "top=1&fv=0&url=%s&sou=%s"
            java.lang.String r12 = java.lang.String.format(r12, r15)
            java.util.Map[] r15 = new java.util.Map[r9]
            java.lang.String r11 = r11.a((java.lang.String) r13, (java.lang.String) r12, (java.util.Map<java.lang.String, java.lang.String>[]) r15)
            java.lang.String r12 = "jscode[\"']\\s*:\\s*[\"'](.*)[\"']"
            java.lang.String r12 = com.original.tase.utils.Regex.a((java.lang.String) r11, (java.lang.String) r12, (int) r5)
            boolean r13 = r12.isEmpty()
            if (r13 == 0) goto L_0x0216
            java.lang.String r12 = "[\"']?url[\"']?\\s*:\\s*[\"']([^\"']+)"
            java.util.ArrayList r12 = com.original.tase.utils.Regex.b((java.lang.String) r11, (java.lang.String) r12, (int) r5, (boolean) r5)
            java.lang.Object r12 = r12.get(r9)
            java.util.ArrayList r12 = (java.util.ArrayList) r12
            java.util.Iterator r12 = r12.iterator()
        L_0x01b2:
            boolean r13 = r12.hasNext()
            if (r13 == 0) goto L_0x009b
            java.lang.Object r13 = r12.next()
            java.lang.String r13 = (java.lang.String) r13
            java.lang.String r15 = ".vtt"
            boolean r15 = r13.endsWith(r15)
            if (r15 != 0) goto L_0x01b2
            java.lang.String r15 = ".srt"
            boolean r15 = r13.endsWith(r15)
            if (r15 != 0) goto L_0x01b2
            java.lang.String r15 = ".png"
            boolean r15 = r13.endsWith(r15)
            if (r15 != 0) goto L_0x01b2
            java.lang.String r15 = ".jpg"
            boolean r15 = r13.endsWith(r15)
            if (r15 != 0) goto L_0x01b2
            boolean r15 = com.original.tase.helper.GoogleVideoHelper.k(r13)
            com.original.tase.model.media.MediaSource r5 = new com.original.tase.model.media.MediaSource
            java.lang.String r9 = r19.a()
            if (r15 == 0) goto L_0x01ed
            java.lang.String r17 = "GoogleVideo"
            goto L_0x01ef
        L_0x01ed:
            java.lang.String r17 = "CDN-FastServer"
        L_0x01ef:
            r18 = r4
            r4 = r17
            r17 = r10
            r10 = 0
            r5.<init>(r9, r4, r10)
            r5.setStreamLink(r13)
            if (r15 == 0) goto L_0x0201
            r5.setPlayHeader(r3)
        L_0x0201:
            if (r15 == 0) goto L_0x0208
            java.lang.String r4 = com.original.tase.helper.GoogleVideoHelper.h(r13)
            goto L_0x0209
        L_0x0208:
            r4 = r14
        L_0x0209:
            r5.setQuality((java.lang.String) r4)
            r0.onNext(r5)
            r10 = r17
            r4 = r18
            r5 = 1
            r9 = 0
            goto L_0x01b2
        L_0x0216:
            r18 = r4
            r17 = r10
            java.lang.String r4 = "var encode = \"%s\"\n\nfunction acb()\n{\n    var result = eval(encode);\n    return result();\n}\n acb();"
            r5 = 1
            java.lang.Object[] r9 = new java.lang.Object[r5]     // Catch:{ all -> 0x0245 }
            java.lang.String r5 = r12.replace(r8, r7)     // Catch:{ all -> 0x0245 }
            r10 = 0
            r9[r10] = r5     // Catch:{ all -> 0x0245 }
            java.lang.String r4 = java.lang.String.format(r4, r9)     // Catch:{ all -> 0x0245 }
            java.lang.Object r4 = r6.evaluate(r4)     // Catch:{ all -> 0x0245 }
            java.lang.String r5 = r4.toString()     // Catch:{ all -> 0x0245 }
            boolean r5 = r5.isEmpty()     // Catch:{ all -> 0x0245 }
            if (r5 != 0) goto L_0x0246
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0245 }
            java.lang.String r5 = "showiFrame\\([\"']([^\"']+[^\"'])[\"']"
            r9 = 1
            java.lang.String r4 = com.original.tase.utils.Regex.a((java.lang.String) r4, (java.lang.String) r5, (int) r9)     // Catch:{ all -> 0x0245 }
            r11 = r4
            goto L_0x0246
        L_0x0245:
        L_0x0246:
            boolean r4 = r11.isEmpty()
            if (r4 != 0) goto L_0x0256
            r4 = 1
            boolean[] r5 = new boolean[r4]
            r9 = 0
            r5[r9] = r9
            r1.a(r0, r11, r14, r5)
            goto L_0x0258
        L_0x0256:
            r4 = 1
            r9 = 0
        L_0x0258:
            int r2 = r2 + 1
            r10 = r17
            r4 = r18
            r5 = 1
            goto L_0x0089
        L_0x0261:
            return
        L_0x0262:
            r0 = move-exception
            r2 = r0
            r6.close()
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.YSMovies.a(io.reactivex.ObservableEmitter, java.lang.String):void");
    }
}
