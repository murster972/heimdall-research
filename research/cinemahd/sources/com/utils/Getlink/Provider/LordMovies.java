package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.facebook.react.uimanager.ViewProps;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.unity3d.ads.metadata.MediationMetaData;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class LordMovies extends BaseProvider {
    public static String f = "";
    private String c = Utils.getProvider(41);
    private String d = "HQ";
    private HashMap e = new HashMap();

    private String c(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        this.e.put("Host", this.c.replace("https://", "").replace("http://", ""));
        this.e.put("Origin", this.c);
        this.e.put("Referer", this.c + "/");
        this.e.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        this.e.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        this.e.put("User-Agent", Constants.f5838a);
        Element h = Jsoup.b(HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[]{this.e})).h("form[action][method]");
        String b = h.b("action");
        if (b.isEmpty()) {
            return "";
        }
        if (b.startsWith("/")) {
            b = this.c + b;
        }
        if (!b.contains(UriUtil.HTTP_SCHEME)) {
            b = this.c + "/" + b;
        }
        String str = b + "?";
        Iterator it2 = h.g("input").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b2 = element.b("type");
            String b3 = element.b("value");
            String b4 = element.b(MediationMetaData.KEY_NAME);
            if (b2.equalsIgnoreCase(ViewProps.HIDDEN)) {
                str = str + b4 + "=" + b3;
            }
            if (b2.equalsIgnoreCase("text")) {
                str = str + b4 + "=%s";
            }
            if (!b2.equalsIgnoreCase("submit") && it2.hasNext()) {
                str = str + "&";
            }
        }
        if (str.endsWith("&")) {
            str = str.substring(0, str.length() - 1);
        }
        String replace = com.original.tase.utils.Utils.a(movieInfo.getName(), new boolean[0]).replace("%20", "+");
        String a2 = TitleHelper.a(movieInfo.name.toLowerCase(), "-");
        String format = String.format(str, new Object[]{replace.toLowerCase()});
        String a3 = HttpHelper.e().a(format, (Map<String, String>[]) new Map[]{this.e});
        this.e.put("Referer", format);
        Iterator it3 = Jsoup.b(a3).g("div.showEntities.showEntitiesMovie.showEntitiesNull").b("div[id*=movie-]").iterator();
        while (it3.hasNext()) {
            Element element2 = (Element) it3.next();
            String b5 = element2.h("a").b("href");
            if (b5.isEmpty()) {
                b5 = element2.h("div.showRow.showRowImage.showRowImage").h("a").b("href");
            }
            this.d = element2.h("div[class*=movieQuality]").G();
            if (z || element2.h("div.movieTV") != null) {
                if (b5.contains("/watch-online-" + a2) && element2.toString().contains(movieInfo.year)) {
                    if (!b5.startsWith("/")) {
                        return b5;
                    }
                    return this.c + b5;
                }
            }
        }
        return "";
    }

    public String a() {
        return "LordMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String c2 = c(movieInfo);
        if (c2.isEmpty()) {
            c2 = b(movieInfo);
        }
        if (!c2.isEmpty()) {
            a(observableEmitter, movieInfo, c2);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String c2 = c(movieInfo);
        if (!c2.isEmpty()) {
            a(observableEmitter, movieInfo, c2);
        }
    }

    public String b(MovieInfo movieInfo) {
        String a2 = TitleHelper.a(movieInfo.name + " " + movieInfo.year, " ");
        String l = Utils.l();
        String str = "";
        for (String str2 : new String[]{"&start=0"}) {
            String a3 = com.original.tase.utils.Utils.a(l + "/search?q=Watch online " + a2 + " site:" + this.c, new boolean[0]);
            HttpHelper e2 = HttpHelper.e();
            StringBuilder sb = new StringBuilder();
            sb.append(a3);
            sb.append("##forceNoCache##");
            Iterator it2 = Jsoup.b(e2.a(sb.toString(), (Map<String, String>[]) new Map[0])).g("div[class=r]").iterator();
            if (!it2.hasNext()) {
                break;
            }
            while (true) {
                if (!it2.hasNext()) {
                    break;
                }
                try {
                    Element element = (Element) it2.next();
                    String b = element.h("a").b("href");
                    if (!b.isEmpty() && !b.contains("google.com") && !b.contains("imdb.com") && !b.contains("youtube.com")) {
                        if (TitleHelper.a(element.h("h3").G().toLowerCase(), "").startsWith(TitleHelper.a("watch online " + a2, "").toLowerCase())) {
                            str = b;
                            break;
                        }
                    }
                } catch (Throwable unused) {
                }
            }
        }
        return str;
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        Iterator it2;
        Duktape create;
        boolean z;
        boolean z2 = movieInfo.getType().intValue() == 1;
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{this.e});
        Iterator it3 = null;
        if (this.d.isEmpty()) {
            this.d = "HQ";
        }
        if (z2) {
            it2 = Jsoup.b(a2).g("tr.linkTr").iterator();
        } else {
            Elements g = Jsoup.b(a2).g("div[class=season][id=season" + movieInfo.session + "]");
            Iterator it4 = g.b("h3").iterator();
            Iterator it5 = g.b("table.table.table-striped.tableLinks").iterator();
            while (true) {
                if (it4.hasNext() && it5.hasNext()) {
                    String obj = it5.next().toString();
                    if (((Element) it4.next()).G().endsWith("Season " + movieInfo.session + " Serie " + movieInfo.eps)) {
                        it3 = Jsoup.b(obj).g("td.linkTr").iterator();
                        z = true;
                        break;
                    }
                } else {
                    z = false;
                }
            }
            if (z) {
                it2 = it3;
            } else {
                return;
            }
        }
        f = Regex.a(a2, "<script>\\s*(function\\s*dec_.*\\}).+?\\r?\\n<\\/script>", 1);
        while (it2.hasNext()) {
            try {
                Element element = (Element) it2.next();
                String G = element.h("td.linkHidden.linkHiddenFormat").G();
                String str2 = f + String.format("function abc()\n{\n    return dec_%s('%s');\n}\nabc();", new Object[]{Regex.a(element.toString(), "serverLink+(\\w\\d+)", 1), element.h("td.linkHidden.linkHiddenCode").G()});
                create = Duktape.create();
                Object evaluate = create.evaluate(str2);
                create.close();
                if (!evaluate.toString().isEmpty()) {
                    String format = String.format(G, new Object[]{evaluate.toString()});
                    if (format.startsWith("//")) {
                        format = "https:" + format;
                    }
                    if (!format.isEmpty()) {
                        if (GoogleVideoHelper.j(format)) {
                            HashMap<String, String> g2 = GoogleVideoHelper.g(format);
                            if (g2 != null) {
                                for (Map.Entry next : g2.entrySet()) {
                                    MediaSource mediaSource = new MediaSource(a(), "GoogleVideo", false);
                                    mediaSource.setOriginalLink(format);
                                    mediaSource.setStreamLink((String) next.getKey());
                                    mediaSource.setQuality((String) next.getValue());
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("User-Agent", Constants.f5838a);
                                    hashMap.put("Cookie", GoogleVideoHelper.c(format, (String) next.getKey()));
                                    mediaSource.setPlayHeader(hashMap);
                                    observableEmitter.onNext(mediaSource);
                                }
                            }
                        } else {
                            a(observableEmitter, format, this.d, false);
                        }
                    }
                }
            } catch (Throwable unused) {
            }
        }
    }
}
