package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Dizilab extends BaseProvider {
    private static String c = Utils.getProvider(100);

    public String a() {
        return "Dizilab";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String str2 = c + "/dizi/" + TitleHelper.e(TitleHelper.g(movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "")).replace("'", "-").replace("P.D.", "PD")) + "/sezon-" + movieInfo.session + "/bolum-" + movieInfo.eps + "/";
        HashMap<String, String> a2 = Constants.a();
        a2.put("accept", "application/json, text/javascript, */*; q=0.01");
        a2.put("origin", c);
        a2.put("referer", str2);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0])).g("div.alternatives-for-this").b("div[data-link]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("data-link");
            String str3 = c + "/ajax/service";
            String a3 = Regex.a(HttpHelper.e().b(Regex.a(HttpHelper.e().a(str3, String.format("link=%s&hash=%s&querytype=%s&type=videoGet", new Object[]{com.original.tase.utils.Utils.a(b, new boolean[0]), element.b("data-hash"), element.b("data-querytype")}), (Map<String, String>[]) new Map[]{a2}), "[\"']?api_iframe[\"']?\\s*:\\s*[\"']([^\"']+)", 1).replace("\\/", "/").replace("\\\\", ""), str2), "<iframe[^>]+src=\"([^\"]+)\"[^>]*>", 1);
            if (a3 != null && !a3.isEmpty()) {
                if (GoogleVideoHelper.j(a3)) {
                    HashMap<String, String> g = GoogleVideoHelper.g(a3);
                    if (g != null && !g.isEmpty()) {
                        for (Map.Entry next : g.entrySet()) {
                            MediaSource mediaSource = new MediaSource(a(), "GoogleVideo", false);
                            mediaSource.setOriginalLink(a3);
                            mediaSource.setStreamLink((String) next.getKey());
                            if (((String) next.getValue()).isEmpty()) {
                                str = "1080p";
                            } else {
                                str = (String) next.getValue();
                            }
                            mediaSource.setQuality(str);
                            HashMap hashMap = new HashMap();
                            hashMap.put("User-Agent", Constants.f5838a);
                            hashMap.put("Cookie", GoogleVideoHelper.c(a3, (String) next.getKey()));
                            mediaSource.setPlayHeader(hashMap);
                            observableEmitter.onNext(mediaSource);
                        }
                    }
                } else {
                    a(observableEmitter, a3, "HD", false);
                }
            }
        }
    }
}
