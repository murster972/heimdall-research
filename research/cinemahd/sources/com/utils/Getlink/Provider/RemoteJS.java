package com.utils.Getlink.Provider;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Base64;
import com.facebook.imagepipeline.common.RotationOptions;
import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.providers.Provider;
import com.movie.data.model.providers.RemoteJSModel;
import com.movie.data.model.providers.TProviderData;
import com.movie.data.remotejs.MyTaskService;
import com.movie.data.remotejs.RemoteJSModule;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import okhttp3.Request;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;

public class RemoteJS extends BaseProvider implements RemoteJSModule.ReactListener {
    private ObservableEmitter<? super MediaSource> c;
    private CompositeDisposable d;
    private List<String> e = new ArrayList();
    private int f = 0;

    static /* synthetic */ void c() throws Exception {
    }

    static /* synthetic */ boolean c(Provider provider) throws Exception {
        return !provider.getName().toLowerCase().contains(Deobfuscator$app$ProductionRelease.a(291));
    }

    public Observable<MediaSource> a(final MovieInfo movieInfo) {
        return Observable.create(new ObservableOnSubscribe<MediaSource>() {
            public void subscribe(ObservableEmitter<MediaSource> observableEmitter) throws Exception {
                if (!FreeMoviesApp.o()) {
                    try {
                        MovieInfo clone = movieInfo.clone();
                        clone.name = clone.name.replace(Deobfuscator$app$ProductionRelease.a(262), Deobfuscator$app$ProductionRelease.a(263));
                        if (clone.session.isEmpty()) {
                            RemoteJS.this.a(clone, (ObservableEmitter<? super MediaSource>) observableEmitter);
                        } else {
                            RemoteJS.this.b(clone, observableEmitter);
                        }
                    } catch (Exception e) {
                        Logger.a(Deobfuscator$app$ProductionRelease.a(264), e.getMessage());
                    }
                } else {
                    observableEmitter.onComplete();
                }
            }
        });
    }

    public void j(String str) {
        FreeMoviesApp.l().edit().apply();
    }

    public void onError(String str) {
    }

    private void a(final String str, MovieInfo movieInfo) {
        Set<String> k = Utils.k();
        FreeMoviesApp.a(Utils.i()).getReactNativeHost().getReactInstanceManager().getCurrentReactContext();
        FreeMoviesApp.a(Utils.i()).a().a((RemoteJSModule.ReactListener) this);
        this.e.clear();
        this.f = 0;
        this.d = new CompositeDisposable();
        this.d.b(Observable.create(new ObservableOnSubscribe<TProviderData>(this) {
            public void subscribe(ObservableEmitter<TProviderData> observableEmitter) throws Exception {
                Class cls = TProviderData.class;
                SharedPreferences l = FreeMoviesApp.l();
                Response execute = HttpHelper.e().b().newCall(new Request.Builder().url(str).build()).execute();
                if (execute.code() != 200 || execute.body() == null) {
                    String string = l.getString(Deobfuscator$app$ProductionRelease.a(266), Deobfuscator$app$ProductionRelease.a(267));
                    if (!string.isEmpty()) {
                        observableEmitter.onNext(new Gson().a(string, cls));
                    }
                } else {
                    String string2 = execute.body().string();
                    l.edit().putString(Deobfuscator$app$ProductionRelease.a(265), string2).apply();
                    observableEmitter.onNext((TProviderData) new Gson().a(string2, cls));
                }
                observableEmitter.onComplete();
            }
        }).flatMap(a.f6512a).filter(f.f6517a).flatMap(new e(this)).observeOn(AndroidSchedulers.a()).subscribe(new d(this, k, movieInfo), c.f6514a, b.f6513a));
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) throws UnsupportedEncodingException {
        a(movieInfo, observableEmitter);
    }

    public /* synthetic */ void a(Set set, MovieInfo movieInfo, Provider provider) throws Exception {
        if (provider != null && provider.getName() != null && !Utils.a(provider.getName(), (Set<String>) set)) {
            Intent intent = new Intent(Utils.i().getApplicationContext(), MyTaskService.class);
            Bundle bundle = new Bundle();
            bundle.putString(Deobfuscator$app$ProductionRelease.a(282), provider.getSrc());
            bundle.putString(Deobfuscator$app$ProductionRelease.a(283), movieInfo.getName());
            bundle.putString(Deobfuscator$app$ProductionRelease.a(284), movieInfo.getYear().toString());
            bundle.putString(Deobfuscator$app$ProductionRelease.a(285), movieInfo.getSession().toString());
            bundle.putString(Deobfuscator$app$ProductionRelease.a(286), movieInfo.getEps().toString());
            bundle.putString(Deobfuscator$app$ProductionRelease.a(287), Deobfuscator$app$ProductionRelease.a(movieInfo.getSession().intValue() > 0 ? 288 : 289));
            bundle.putString(Deobfuscator$app$ProductionRelease.a(290), provider.getName());
            intent.putExtras(bundle);
            Utils.i().getApplicationContext().startService(intent);
            this.e.add(provider.getName());
        }
    }

    /* renamed from: a */
    public Observable<Provider> b(final Provider provider) {
        return Observable.create(new ObservableOnSubscribe<Provider>(this) {
            public void subscribe(ObservableEmitter<Provider> observableEmitter) throws Exception {
                SharedPreferences l = FreeMoviesApp.l();
                String string = l.getString(provider.getName() + Deobfuscator$app$ProductionRelease.a(268) + provider.getVersion(), Deobfuscator$app$ProductionRelease.a(269));
                if (!string.isEmpty()) {
                    provider.setSrc(string);
                    observableEmitter.onNext(provider);
                } else {
                    Response execute = HttpHelper.e().b().newCall(new Request.Builder().url(provider.getUrl()).build()).execute();
                    if (execute.code() == 200 && execute.body() != null) {
                        String string2 = execute.body().string();
                        SharedPreferences.Editor edit = l.edit();
                        edit.putString(provider.getName() + Deobfuscator$app$ProductionRelease.a(RotationOptions.ROTATE_270) + provider.getVersion(), string2).apply();
                        provider.setSrc(string2);
                        observableEmitter.onNext(provider);
                    }
                }
                observableEmitter.onComplete();
            }
        });
    }

    public void a(String str, String str2) {
        if (!this.c.isDisposed()) {
            RemoteJSModel[] remoteJSModelArr = (RemoteJSModel[]) new Gson().a(str, RemoteJSModel[].class);
            String a2 = Deobfuscator$app$ProductionRelease.a(271);
            for (RemoteJSModel remoteJSModel : remoteJSModelArr) {
                String name = remoteJSModel.getProvider().getName();
                if (name.toLowerCase().contains(Deobfuscator$app$ProductionRelease.a(272)) || name.toLowerCase().contains(Deobfuscator$app$ProductionRelease.a(273))) {
                    Deobfuscator$app$ProductionRelease.a(TiffUtil.TIFF_TAG_ORIENTATION);
                } else {
                    MediaSource mediaSource = new MediaSource(StringUtils.a(name), Deobfuscator$app$ProductionRelease.a(275), false);
                    mediaSource.setStreamLink(remoteJSModel.getResult().getFile());
                    mediaSource.setNeedToSync(true);
                    String h = Utils.h(remoteJSModel.getResult().getFile());
                    if (h.isEmpty()) {
                        h = Deobfuscator$app$ProductionRelease.a(276);
                    }
                    mediaSource.setQuality(h);
                    this.c.onNext(mediaSource);
                }
            }
            if (!a2.isEmpty()) {
                Set<String> stringSet = FreeMoviesApp.l().getStringSet(Deobfuscator$app$ProductionRelease.a(277), new HashSet());
                stringSet.add(a2);
                Logger.a(Deobfuscator$app$ProductionRelease.a(278), a2);
                FreeMoviesApp.l().edit().putStringSet(Deobfuscator$app$ProductionRelease.a(279), stringSet).apply();
            }
        }
    }

    public void a(String str) {
        this.f++;
        if (this.f == this.e.size()) {
            this.c.onComplete();
            this.c.isDisposed();
            this.d.dispose();
        }
        if (this.c.isDisposed()) {
            j(Deobfuscator$app$ProductionRelease.a(280));
        }
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) throws UnsupportedEncodingException {
        this.c = observableEmitter;
        a(new String(Base64.decode(Utils.getProvider(106), 0), StandardCharsets.UTF_8), movieInfo);
    }

    public String a() {
        return Deobfuscator$app$ProductionRelease.a(281);
    }
}
