package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class TheYM extends BaseProvider {
    private String c = Utils.getProvider(46);

    public String a() {
        return "TheYM";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0238, code lost:
        if (r11.startsWith("//") == false) goto L_0x024d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x023a, code lost:
        r11 = "http:" + r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x0249, code lost:
        r1 = r18;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0251, code lost:
        if (r11.startsWith("/") == false) goto L_0x0265;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x0253, code lost:
        r11 = r0.c + r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0269, code lost:
        if (r11.startsWith(com.facebook.common.util.UriUtil.HTTP_SCHEME) != false) goto L_0x0249;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x026b, code lost:
        r11 = r0.c + "/" + r11;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r28, com.movie.data.model.MovieInfo r29) {
        /*
            r27 = this;
            r0 = r27
            r1 = r28
            r2 = r29
            java.lang.Integer r3 = r29.getType()
            int r3 = r3.intValue()
            r4 = 0
            r5 = 1
            if (r3 != r5) goto L_0x0014
            r3 = 1
            goto L_0x0015
        L_0x0014:
            r3 = 0
        L_0x0015:
            java.lang.String r6 = r2.name
            java.lang.String r6 = r6.toLowerCase()
            java.lang.String r7 = "-"
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r7)
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = r0.c
            r8.append(r9)
            java.lang.String r9 = "/"
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            java.util.Map[] r10 = new java.util.Map[r4]
            r7.a((java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r10)
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.String r8 = "accept"
            java.lang.String r10 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
            r7.put(r8, r10)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = r0.c
            r8.append(r10)
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            java.lang.String r10 = "referer"
            r7.put(r10, r8)
            java.lang.String r8 = "upgrade-insecure-requests"
            java.lang.String r10 = "1"
            r7.put(r8, r10)
            java.lang.String r8 = com.original.Constants.f5838a
            java.lang.String r10 = "user-agent"
            r7.put(r10, r8)
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = r0.c
            r10.append(r11)
            r10.append(r9)
            java.lang.String r10 = r10.toString()
            java.lang.String r8 = r8.a((java.lang.String) r10)
            java.lang.String r10 = "cookie"
            r7.put(r10, r8)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r10 = r0.c
            r8.append(r10)
            java.lang.String r10 = "/movie/search/"
            r8.append(r10)
            r8.append(r6)
            java.lang.String r8 = r8.toString()
            com.original.tase.helper.http.HttpHelper r10 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            r11.append(r8)
            java.lang.String r12 = "##forceNoCache##"
            r11.append(r12)
            java.lang.String r11 = r11.toString()
            java.util.Map[] r13 = new java.util.Map[r5]
            r13[r4] = r7
            java.lang.String r7 = r10.a((java.lang.String) r11, (java.util.Map<java.lang.String, java.lang.String>[]) r13)
            org.jsoup.nodes.Document r10 = org.jsoup.Jsoup.b(r7)
            java.lang.String r11 = "div.ml-item"
            org.jsoup.select.Elements r10 = r10.g((java.lang.String) r11)
            java.util.Iterator r10 = r10.iterator()
            boolean r13 = r10.hasNext()
            if (r13 != 0) goto L_0x0103
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r13 = "https://api.ocloud.stream/series/movie/search/"
            r10.append(r13)
            r10.append(r6)
            java.lang.String r6 = "?link_web="
            r10.append(r6)
            java.lang.String r6 = r0.c
            r10.append(r6)
            java.lang.String r6 = r10.toString()
            java.util.Map[] r10 = new java.util.Map[r4]
            java.lang.String r7 = r7.a((java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r10)
            org.jsoup.nodes.Document r6 = org.jsoup.Jsoup.b(r7)
            org.jsoup.select.Elements r6 = r6.g((java.lang.String) r11)
            java.util.Iterator r10 = r6.iterator()
        L_0x0103:
            java.lang.String r6 = r2.year
            java.lang.String r11 = ""
            r13 = r11
            r14 = 0
        L_0x0109:
            boolean r15 = r10.hasNext()
            java.lang.String r4 = "p"
            java.lang.String r5 = "a"
            r18 = r6
            java.lang.String r6 = "(\\d+)"
            r19 = r7
            java.lang.String r7 = "//"
            if (r15 == 0) goto L_0x038c
            java.lang.Object r15 = r10.next()
            org.jsoup.nodes.Element r15 = (org.jsoup.nodes.Element) r15
            org.jsoup.nodes.Element r15 = r15.h(r5)
            r20 = r10
            java.lang.String r10 = "data-url"
            java.lang.String r10 = r15.b((java.lang.String) r10)
            r21 = r11
            java.lang.String r11 = "title"
            java.lang.String r11 = r15.b((java.lang.String) r11)
            boolean r22 = r11.isEmpty()
            if (r22 == 0) goto L_0x0145
            java.lang.String r11 = "h2"
            org.jsoup.nodes.Element r11 = r15.h(r11)
            java.lang.String r11 = r11.G()
        L_0x0145:
            r22 = r13
            java.lang.String r13 = "href"
            r23 = r14
            java.lang.String r14 = "http"
            java.lang.String r1 = "http:"
            if (r3 == 0) goto L_0x02f7
            r24 = r3
            java.lang.String r3 = r2.name
            boolean r3 = r11.equals(r3)
            if (r3 == 0) goto L_0x0280
            boolean r3 = r10.isEmpty()
            if (r3 != 0) goto L_0x0210
            boolean r3 = r10.startsWith(r7)
            if (r3 == 0) goto L_0x0179
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            r3.append(r10)
            java.lang.String r10 = r3.toString()
            r25 = r5
            goto L_0x01af
        L_0x0179:
            boolean r3 = r10.startsWith(r9)
            if (r3 == 0) goto L_0x0193
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r25 = r5
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r10)
            java.lang.String r10 = r3.toString()
            goto L_0x01af
        L_0x0193:
            r25 = r5
            boolean r3 = r10.startsWith(r14)
            if (r3 != 0) goto L_0x01af
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r9)
            r3.append(r10)
            java.lang.String r10 = r3.toString()
        L_0x01af:
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r10)
            r5.append(r12)
            java.lang.String r5 = r5.toString()
            r26 = r12
            r10 = 1
            java.util.Map[] r12 = new java.util.Map[r10]
            java.util.HashMap r17 = com.original.Constants.a()
            r16 = 0
            r12[r16] = r17
            java.lang.String r3 = r3.b(r5, r8, r12)
            java.lang.String r5 = "\\\""
            java.lang.String r12 = "\""
            java.lang.String r3 = r3.replace(r5, r12)
            java.lang.String r5 = "\\/"
            java.lang.String r3 = r3.replace(r5, r9)
            java.lang.String r5 = "<div\\s+[^>]*class=\"jt-info\"[^>]*>\\s*(\\d{4})\\s*</div>"
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r5, (int) r10)
            java.lang.String r12 = "<div\\s+[^>]*class=\"jtip-quality\"[^>]*>\\s*([\\w\\s]+)\\s*</div>"
            java.lang.String r12 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r12, (int) r10)
            boolean r18 = r0.c(r12)
            java.lang.String r12 = com.original.tase.utils.Regex.a((java.lang.String) r12, (java.lang.String) r6, (int) r10)
            boolean r10 = r12.isEmpty()
            if (r10 != 0) goto L_0x020d
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r10.append(r12)
            r10.append(r4)
            java.lang.String r10 = r10.toString()
            r22 = r10
            goto L_0x021a
        L_0x020d:
            r22 = r12
            goto L_0x021a
        L_0x0210:
            r25 = r5
            r26 = r12
            r5 = r18
            r3 = r19
            r18 = r23
        L_0x021a:
            java.lang.String r10 = r2.year
            boolean r10 = r5.equals(r10)
            if (r10 != 0) goto L_0x0230
            boolean r10 = r3.isEmpty()
            if (r10 == 0) goto L_0x0229
            goto L_0x0230
        L_0x0229:
            r19 = r3
            r23 = r18
            r18 = r5
            goto L_0x0284
        L_0x0230:
            java.lang.String r11 = r15.b((java.lang.String) r13)
            boolean r3 = r11.startsWith(r7)
            if (r3 == 0) goto L_0x024d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            r3.append(r11)
            java.lang.String r11 = r3.toString()
        L_0x0249:
            r1 = r18
            goto L_0x0398
        L_0x024d:
            boolean r1 = r11.startsWith(r9)
            if (r1 == 0) goto L_0x0265
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            goto L_0x0249
        L_0x0265:
            boolean r1 = r11.startsWith(r14)
            if (r1 != 0) goto L_0x0249
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r9)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            goto L_0x0249
        L_0x0280:
            r25 = r5
            r26 = r12
        L_0x0284:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r2.name
            r3.append(r5)
            java.lang.String r5 = " ("
            r3.append(r5)
            java.lang.String r5 = r2.year
            r3.append(r5)
            java.lang.String r5 = ")"
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            boolean r3 = r11.equals(r3)
            if (r3 == 0) goto L_0x0376
            java.lang.String r11 = r15.b((java.lang.String) r13)
            boolean r3 = r11.startsWith(r7)
            if (r3 == 0) goto L_0x02c2
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            r3.append(r11)
            java.lang.String r11 = r3.toString()
            goto L_0x0396
        L_0x02c2:
            boolean r1 = r11.startsWith(r9)
            if (r1 == 0) goto L_0x02db
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            goto L_0x0396
        L_0x02db:
            boolean r1 = r11.startsWith(r14)
            if (r1 != 0) goto L_0x0396
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r9)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            goto L_0x0396
        L_0x02f7:
            r24 = r3
            r25 = r5
            r26 = r12
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r11)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            java.lang.String r10 = r29.getName()
            r5.append(r10)
            java.lang.String r10 = "season"
            r5.append(r10)
            java.lang.String r10 = r2.session
            r5.append(r10)
            java.lang.String r5 = r5.toString()
            java.lang.String r5 = com.original.tase.helper.TitleHelper.e(r5)
            java.lang.String r5 = com.original.tase.helper.TitleHelper.f(r5)
            boolean r3 = r3.startsWith(r5)
            if (r3 == 0) goto L_0x0376
            java.lang.String r11 = r15.b((java.lang.String) r13)
            boolean r3 = r11.startsWith(r7)
            if (r3 == 0) goto L_0x0343
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r1)
            r3.append(r11)
            java.lang.String r11 = r3.toString()
            goto L_0x0396
        L_0x0343:
            boolean r1 = r11.startsWith(r9)
            if (r1 == 0) goto L_0x035b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            goto L_0x0396
        L_0x035b:
            boolean r1 = r11.startsWith(r14)
            if (r1 != 0) goto L_0x0396
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r9)
            r1.append(r11)
            java.lang.String r11 = r1.toString()
            goto L_0x0396
        L_0x0376:
            r1 = r28
            r6 = r18
            r7 = r19
            r10 = r20
            r11 = r21
            r13 = r22
            r14 = r23
            r3 = r24
            r12 = r26
            r4 = 0
            r5 = 1
            goto L_0x0109
        L_0x038c:
            r24 = r3
            r25 = r5
            r21 = r11
            r22 = r13
            r23 = r14
        L_0x0396:
            r1 = r23
        L_0x0398:
            boolean r3 = r11.isEmpty()
            if (r3 == 0) goto L_0x039f
            return
        L_0x039f:
            java.lang.String r3 = ".html"
            boolean r3 = r11.endsWith(r3)
            if (r3 == 0) goto L_0x03b3
            int r3 = r11.length()
            int r3 = r3 + -5
            r5 = 0
            java.lang.String r11 = r11.substring(r5, r3)
            goto L_0x03b4
        L_0x03b3:
            r5 = 0
        L_0x03b4:
            boolean r3 = r11.endsWith(r9)
            if (r3 == 0) goto L_0x03c4
            int r3 = r11.length()
            r9 = 1
            int r3 = r3 - r9
            java.lang.String r11 = r11.substring(r5, r3)
        L_0x03c4:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r11)
            java.lang.String r5 = "/watching.html"
            r3.append(r5)
            java.lang.String r3 = r3.toString()
            com.original.tase.helper.http.HttpHelper r5 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r5 = r5.b((java.lang.String) r3, (java.lang.String) r8)
            org.jsoup.nodes.Document r5 = org.jsoup.Jsoup.b(r5)
            boolean r8 = r22.isEmpty()
            if (r8 == 0) goto L_0x0412
            java.lang.String r8 = "span.quality"
            org.jsoup.nodes.Element r8 = r5.h(r8)
            java.lang.String r8 = r8.G()
            java.lang.String r8 = r8.toLowerCase()
            r9 = 1
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r8, (java.lang.String) r6, (int) r9)
            boolean r9 = r8.isEmpty()
            if (r9 != 0) goto L_0x0410
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r8)
            r9.append(r4)
            java.lang.String r22 = r9.toString()
            goto L_0x0412
        L_0x0410:
            java.lang.String r22 = "HD"
        L_0x0412:
            r4 = r22
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            java.lang.String r9 = com.original.Constants.f5838a
            java.lang.String r10 = "User-Agent"
            r8.put(r10, r9)
            java.lang.String r9 = "div.les-content"
            org.jsoup.select.Elements r5 = r5.g((java.lang.String) r9)
            r9 = r25
            org.jsoup.select.Elements r5 = r5.b(r9)
            java.util.Iterator r5 = r5.iterator()
        L_0x0430:
            boolean r9 = r5.hasNext()
            if (r9 == 0) goto L_0x04da
            java.lang.Object r9 = r5.next()
            org.jsoup.nodes.Element r9 = (org.jsoup.nodes.Element) r9
            java.lang.String r10 = "player-data"
            java.lang.String r10 = r9.b((java.lang.String) r10)
            if (r24 != 0) goto L_0x045e
            java.lang.String r11 = "episode-data"
            java.lang.String r9 = r9.b((java.lang.String) r11)
            r11 = 1
            java.lang.String r9 = com.original.tase.utils.Regex.a((java.lang.String) r9, (java.lang.String) r6, (int) r11)
            boolean r11 = r9.isEmpty()
            if (r11 != 0) goto L_0x0430
            java.lang.String r11 = r2.eps
            boolean r9 = r9.equals(r11)
            if (r9 != 0) goto L_0x045e
            goto L_0x0430
        L_0x045e:
            boolean r9 = r10.startsWith(r7)
            if (r9 == 0) goto L_0x0475
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r11 = "https:"
            r9.append(r11)
            r9.append(r10)
            java.lang.String r10 = r9.toString()
        L_0x0475:
            java.lang.String r9 = "load.php"
            boolean r9 = r10.contains(r9)
            if (r9 == 0) goto L_0x047e
            goto L_0x0430
        L_0x047e:
            java.lang.String r9 = "vidcloud.icu"
            boolean r9 = r10.contains(r9)
            if (r9 == 0) goto L_0x04cd
            java.util.ArrayList r9 = r0.d(r10, r3)
            java.util.Iterator r9 = r9.iterator()
        L_0x048e:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x04c9
            java.lang.Object r10 = r9.next()
            java.lang.String r10 = r10.toString()
            boolean r11 = com.original.tase.helper.GoogleVideoHelper.k(r10)
            com.original.tase.model.media.MediaSource r12 = new com.original.tase.model.media.MediaSource
            java.lang.String r13 = r27.a()
            if (r11 == 0) goto L_0x04ab
            java.lang.String r14 = "GoogleVideo"
            goto L_0x04ad
        L_0x04ab:
            java.lang.String r14 = "CDN-FastServer"
        L_0x04ad:
            r12.<init>(r13, r14, r1)
            r12.setStreamLink(r10)
            if (r11 == 0) goto L_0x04b8
            r12.setPlayHeader(r8)
        L_0x04b8:
            if (r11 == 0) goto L_0x04bf
            java.lang.String r10 = com.original.tase.helper.GoogleVideoHelper.h(r10)
            goto L_0x04c0
        L_0x04bf:
            r10 = r4
        L_0x04c0:
            r12.setQuality((java.lang.String) r10)
            r11 = r28
            r11.onNext(r12)
            goto L_0x048e
        L_0x04c9:
            r11 = r28
            goto L_0x0430
        L_0x04cd:
            r11 = r28
            r9 = 1
            boolean[] r12 = new boolean[r9]
            r13 = 0
            r12[r13] = r1
            r0.a(r11, r10, r4, r12)
            goto L_0x0430
        L_0x04da:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.TheYM.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo):void");
    }
}
