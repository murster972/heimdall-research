package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Cartoon extends BaseProvider {
    private String c = Utils.getProvider(18);

    public String a() {
        return "Cartoon";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo) {
        String str;
        String str2;
        String str3;
        String str4;
        MovieInfo movieInfo2 = movieInfo;
        if (a(movieInfo2.genres)) {
            boolean z = movieInfo.getType().intValue() == 1;
            HashMap hashMap = new HashMap();
            hashMap.put("user-agent", Constants.f5838a);
            String lowerCase = com.original.tase.utils.Utils.a(movieInfo2.name, new boolean[0]).replace("%20", "+").toLowerCase();
            Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/Search/?s=" + lowerCase, (Map<String, String>[]) new Map[0])).g("div.ml-item").iterator();
            String str5 = "";
            while (true) {
                if (!it2.hasNext()) {
                    str = "";
                    str2 = str5;
                    break;
                }
                Element element = (Element) it2.next();
                str2 = element.b("data-movie-id");
                if (!str2.isEmpty()) {
                    Element h = element.h("a");
                    if (z) {
                        String a2 = Regex.a(h.toString(), "<em>(.*)<\\/em>\\s*(\\d+)", 1);
                        String a3 = Regex.a(h.toString(), "<em>(.*)<\\/em>\\s*(\\d+)", 2);
                        if (movieInfo2.name.equalsIgnoreCase(a2) && movieInfo2.year.equals(a3)) {
                            str = h.b("href");
                            break;
                        }
                    } else {
                        if (Regex.a(h.toString(), "<\\/span>(.*)<span>", 1).replaceAll("<[^>]*>", "").toLowerCase().contains(movieInfo2.name.toLowerCase() + " season " + movieInfo2.session)) {
                            str = h.b("href");
                            break;
                        }
                    }
                }
                str5 = str2;
            }
            if (!str.isEmpty()) {
                String str6 = str + "Episode-Movie?id=" + str2;
                if (!z) {
                    str3 = Jsoup.b(HttpHelper.e().b(str, this.c + "/")).h("div#mv-info").h("a").b("href");
                    Iterator it3 = Jsoup.b(HttpHelper.e().b(str3, str)).g("div.les-content").b("a").iterator();
                    while (true) {
                        if (!it3.hasNext()) {
                            str4 = "";
                            break;
                        }
                        str3 = ((Element) it3.next()).b("href");
                        if (str3.contains(com.original.tase.utils.Utils.a("000", movieInfo.getEps().intValue()))) {
                            str4 = Regex.a(str3, "id=(\\d+)", 1);
                            break;
                        }
                    }
                } else {
                    str3 = str6;
                    str4 = str2;
                }
                Iterator it4 = Jsoup.b(HttpHelper.e().b(str3, str)).g("select#selectServer").b("option").iterator();
                HashMap<String, String> a4 = Constants.a();
                a4.put("origin", this.c);
                a4.put("referer", str3);
                while (it4.hasNext()) {
                    String a5 = Regex.a(((Element) it4.next()).toString(), "s=(\\w+)", 1);
                    String a6 = HttpHelper.e().a(this.c + "/ajax/anime/load_episodes_v2?s=" + a5, "episode_id=" + str4, (Map<String, String>[]) new Map[]{a4});
                    hashMap.put("referer", str3 + "&s=" + a5);
                    String a7 = Regex.a(a6, "src\\s*=\\\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                    if (!a7.isEmpty()) {
                        MediaSource mediaSource = new MediaSource(a(), "", true);
                        mediaSource.setQuality("HD");
                        mediaSource.setStreamLink(a7);
                        mediaSource.setPlayHeader(hashMap);
                        observableEmitter.onNext(mediaSource);
                    } else {
                        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                    }
                }
            }
        }
    }
}
