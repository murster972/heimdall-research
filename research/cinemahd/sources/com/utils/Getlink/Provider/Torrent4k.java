package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.movie.data.model.realdebrid.MagnetObject;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Torrent4k extends BaseProvider {
    public String c = Utils.getProvider(12);

    public String a() {
        return "Torrent4k";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    public void c(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String str2;
        String str3;
        String str4;
        boolean z;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        int i;
        String str5;
        MovieInfo movieInfo2 = movieInfo;
        String str6 = "1080p";
        boolean z2 = movieInfo.getType().intValue() == 1;
        if (z2) {
            str = " " + movieInfo2.year;
        } else {
            str = " S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.eps));
        }
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/search_results.php?search=" + com.original.tase.utils.Utils.a(movieInfo2.name + str, new boolean[0]) + "&cat=1&incldead=0&inclexternal=0&lang=0&sort=size&order=desc", (Map<String, String>[]) new Map[0])).g("tbody").b("tr.t-row").iterator();
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        HashMap hashMap = new HashMap();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            try {
                String replaceAll = element.h("td.ttable_col2").h("a[title][href]").b("title").replaceAll("\\(|\\)", "");
                if (z2) {
                    if (TitleHelper.f(replaceAll).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName()) + movieInfo2.year))) {
                        hashMap.put(Regex.a(element.toString(), "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1), replaceAll);
                    }
                } else {
                    if (TitleHelper.a(replaceAll.toLowerCase().replace(movieInfo2.year, ""), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase() + str.toLowerCase(), ""))) {
                        hashMap.put(Regex.a(element.toString(), "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1), replaceAll);
                    }
                }
            } catch (Throwable unused) {
            }
        }
        new HashMap();
        ArrayList arrayList = new ArrayList();
        String str7 = "HQ";
        for (Map.Entry entry : hashMap.entrySet()) {
            try {
                String str8 = (String) entry.getValue();
                String[] split = str8.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                int length = split.length;
                str4 = str2;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        str3 = str6;
                        z = false;
                        break;
                    }
                    try {
                        String lowerCase = split[i2].toLowerCase();
                        if (lowerCase.contains("dvdscr") || lowerCase.contains("camrip") || lowerCase.contains("tsrip") || lowerCase.contains("hdcam") || lowerCase.contains("hdtc") || lowerCase.contains("hdts") || lowerCase.contains("dvdcam") || lowerCase.contains("dvdts") || lowerCase.contains("cam") || lowerCase.contains("telesync")) {
                            break;
                        } else if (lowerCase.contains("ts")) {
                            break;
                        } else {
                            boolean contains = lowerCase.contains(str6);
                            str3 = str6;
                            String str9 = "720p";
                            if (!contains) {
                                if (!lowerCase.equals("1080")) {
                                    if (!lowerCase.contains(str9)) {
                                        if (!lowerCase.equals("720")) {
                                            if (lowerCase.contains("brrip") || lowerCase.contains("bdrip") || lowerCase.contains("hdrip") || lowerCase.contains("web-dl")) {
                                                str9 = "HD";
                                            } else {
                                                i2++;
                                                str6 = str3;
                                            }
                                        }
                                    }
                                    str4 = str9;
                                    i2++;
                                    str6 = str3;
                                }
                            }
                            str4 = str3;
                            i2++;
                            str6 = str3;
                        }
                    } catch (Throwable unused2) {
                    }
                }
                str3 = str6;
                z = true;
                String str10 = (String) entry.getKey();
                String a2 = a();
                if (z2) {
                    parsedLinkModel = directoryIndexHelper.a(str8);
                } else {
                    parsedLinkModel = directoryIndexHelper.b(str8);
                }
                if (parsedLinkModel != null) {
                    str2 = parsedLinkModel.c();
                    if (str2.equalsIgnoreCase("HQ")) {
                        str2 = str4;
                    }
                    try {
                        i = 1;
                        str4 = str2;
                        a2 = a(parsedLinkModel.b(), true);
                    } catch (Throwable unused3) {
                        str4 = str2;
                        str7 = str4;
                        str6 = str3;
                    }
                } else {
                    i = 1;
                }
                String lowerCase2 = Regex.a(str10, "(magnet:\\?xt=urn:btih:[^&.]+)", i).toLowerCase();
                if (z) {
                    str5 = "CAM-" + str4;
                } else {
                    str5 = str4;
                }
                MagnetObject magnetObject = new MagnetObject(a2, lowerCase2, str5, a());
                magnetObject.setFileName(str8);
                arrayList.add(magnetObject);
            } catch (Throwable unused4) {
                str3 = str6;
                str4 = str2;
                str7 = str4;
                str6 = str3;
            }
            str7 = str4;
            str6 = str3;
        }
        if (arrayList.size() > 0) {
            MediaSource mediaSource = new MediaSource(a(), "Torrent", false);
            mediaSource.setTorrent(true);
            mediaSource.setMagnetObjects(arrayList);
            mediaSource.setStreamLink("magnet:Torrent4k");
            observableEmitter.onNext(mediaSource);
        }
    }
}
