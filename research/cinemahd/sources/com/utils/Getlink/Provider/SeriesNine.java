package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class SeriesNine extends BaseProvider {
    public String c = Utils.getProvider(33);

    public String a() {
        return "SeriesNine";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:46:0x02e3, code lost:
        if (r6.startsWith("//") == false) goto L_0x02f8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:47:0x02e5, code lost:
        r6 = r28 + r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x02fc, code lost:
        if (r6.startsWith("/") == false) goto L_0x0311;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x02fe, code lost:
        r6 = r0.c + r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:52:0x0317, code lost:
        if (r6.startsWith(r27) != false) goto L_0x0451;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0319, code lost:
        r6 = r0.c + "/" + r6;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r30, com.movie.data.model.MovieInfo r31) {
        /*
            r29 = this;
            r0 = r29
            r1 = r30
            r2 = r31
            java.lang.Integer r3 = r31.getType()
            int r3 = r3.intValue()
            r4 = 1
            r5 = 0
            if (r3 != r4) goto L_0x0014
            r3 = 1
            goto L_0x0015
        L_0x0014:
            r3 = 0
        L_0x0015:
            java.lang.String r6 = r2.name
            java.lang.String r6 = r6.toLowerCase()
            java.lang.String r7 = "-"
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r7)
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r8 = r0.c
            java.util.Map[] r9 = new java.util.Map[r5]
            r7.a((java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            java.util.HashMap r7 = new java.util.HashMap
            r7.<init>()
            java.lang.String r8 = "accept"
            java.lang.String r9 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8"
            r7.put(r8, r9)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = r0.c
            r9.append(r10)
            java.lang.String r10 = "/"
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.lang.String r11 = "referer"
            r7.put(r11, r9)
            java.lang.String r9 = "upgrade-insecure-requests"
            java.lang.String r12 = "1"
            r7.put(r9, r12)
            java.lang.String r9 = com.original.Constants.f5838a
            java.lang.String r12 = "user-agent"
            r7.put(r12, r9)
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r12 = r0.c
            java.lang.String r9 = r9.a((java.lang.String) r12)
            java.lang.String r12 = "cookie"
            r7.put(r12, r9)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r12 = r0.c
            r9.append(r12)
            java.lang.String r12 = "/movie/search/"
            r9.append(r12)
            r9.append(r6)
            java.lang.String r9 = r9.toString()
            com.original.tase.helper.http.HttpHelper r12 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r9)
            java.lang.String r14 = "##forceNoCache##"
            r13.append(r14)
            java.lang.String r13 = r13.toString()
            java.util.Map[] r15 = new java.util.Map[r4]
            r15[r5] = r7
            java.lang.String r7 = r12.a((java.lang.String) r13, (java.util.Map<java.lang.String, java.lang.String>[]) r15)
            org.jsoup.nodes.Document r12 = org.jsoup.Jsoup.b(r7)
            java.lang.String r13 = "div.ml-item"
            org.jsoup.select.Elements r12 = r12.g((java.lang.String) r13)
            java.util.Iterator r12 = r12.iterator()
            java.util.HashMap r15 = new java.util.HashMap
            r15.<init>()
            java.lang.String r4 = "*/*"
            r15.put(r8, r4)
            java.lang.String r4 = r0.c
            java.lang.String r5 = "origin"
            r15.put(r5, r4)
            r15.put(r11, r9)
            boolean r4 = r12.hasNext()
            java.lang.String r5 = "?link_web="
            if (r4 != 0) goto L_0x0112
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            java.lang.String r11 = "https://api.ocloud.stream/series/movie/search/"
            r7.append(r11)
            r7.append(r6)
            r7.append(r5)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r11 = r0.c
            r6.append(r11)
            r6.append(r10)
            java.lang.String r6 = r6.toString()
            r11 = 0
            boolean[] r12 = new boolean[r11]
            java.lang.String r6 = com.original.tase.utils.Utils.a((java.lang.String) r6, (boolean[]) r12)
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            r7 = 1
            java.util.Map[] r12 = new java.util.Map[r7]
            r12[r11] = r15
            java.lang.String r7 = r4.a((java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
            org.jsoup.nodes.Document r4 = org.jsoup.Jsoup.b(r7)
            org.jsoup.select.Elements r4 = r4.g((java.lang.String) r13)
            java.util.Iterator r12 = r4.iterator()
        L_0x0112:
            java.lang.String r4 = r2.year
            java.lang.String r6 = ""
            r11 = r4
            r4 = r6
            r13 = 0
        L_0x0119:
            boolean r16 = r12.hasNext()
            r17 = r4
            java.lang.String r4 = "(\\d+)"
            r18 = r6
            java.lang.String r6 = "a"
            r19 = r7
            java.lang.String r7 = "href"
            r20 = r11
            java.lang.String r11 = "//"
            if (r16 == 0) goto L_0x0448
            java.lang.Object r16 = r12.next()
            r21 = r12
            r12 = r16
            org.jsoup.nodes.Element r12 = (org.jsoup.nodes.Element) r12
            org.jsoup.nodes.Element r12 = r12.h(r6)
            r16 = r13
            java.lang.String r13 = "data-url"
            java.lang.String r13 = r12.b((java.lang.String) r13)
            java.lang.String r1 = "title"
            java.lang.String r1 = r12.b((java.lang.String) r1)
            boolean r22 = r1.isEmpty()
            if (r22 == 0) goto L_0x015b
            java.lang.String r1 = "h2"
            org.jsoup.nodes.Element r1 = r12.h(r1)
            java.lang.String r1 = r1.G()
        L_0x015b:
            r22 = r6
            java.lang.String r6 = "http"
            r23 = r7
            java.lang.String r7 = "http:"
            if (r3 == 0) goto L_0x03b1
            r24 = r3
            java.lang.String r3 = r2.name
            boolean r3 = r1.equals(r3)
            if (r3 == 0) goto L_0x032f
            boolean r3 = r13.isEmpty()
            if (r3 != 0) goto L_0x02b1
            boolean r3 = r13.startsWith(r11)
            if (r3 == 0) goto L_0x018d
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r7)
            r3.append(r13)
            java.lang.String r13 = r3.toString()
            r25 = r1
            goto L_0x01c3
        L_0x018d:
            boolean r3 = r13.startsWith(r10)
            if (r3 == 0) goto L_0x01a7
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r25 = r1
            java.lang.String r1 = r0.c
            r3.append(r1)
            r3.append(r13)
            java.lang.String r13 = r3.toString()
            goto L_0x01c3
        L_0x01a7:
            r25 = r1
            boolean r1 = r13.startsWith(r6)
            if (r1 != 0) goto L_0x01c3
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = r0.c
            r1.append(r3)
            r1.append(r10)
            r1.append(r13)
            java.lang.String r13 = r1.toString()
        L_0x01c3:
            java.lang.String r1 = "application/json, text/javascript, */*; q=0.01"
            r15.put(r8, r1)
            com.original.tase.helper.http.HttpHelper r1 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r13)
            r3.append(r5)
            r26 = r8
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r27 = r6
            java.lang.String r6 = r0.c
            r8.append(r6)
            r8.append(r10)
            java.lang.String r6 = r8.toString()
            r28 = r7
            r8 = 0
            boolean[] r7 = new boolean[r8]
            java.lang.String r6 = com.original.tase.utils.Utils.a((java.lang.String) r6, (boolean[]) r7)
            r3.append(r6)
            r3.append(r14)
            java.lang.String r3 = r3.toString()
            r6 = 1
            java.util.Map[] r7 = new java.util.Map[r6]
            r7[r8] = r15
            java.lang.String r1 = r1.a((java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r7)
            java.lang.String r3 = "\\\""
            java.lang.String r6 = "\""
            java.lang.String r1 = r1.replace(r3, r6)
            java.lang.String r3 = "\\/"
            java.lang.String r1 = r1.replace(r3, r10)
            boolean r3 = r1.isEmpty()
            if (r3 != 0) goto L_0x0224
            java.lang.String r3 = "404"
            boolean r3 = r1.contains(r3)
            if (r3 == 0) goto L_0x027e
        L_0x0224:
            java.lang.String r3 = "yesmovie.io"
            boolean r3 = r13.contains(r3)
            if (r3 == 0) goto L_0x027e
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = "yesmovie.io"
            java.lang.String r6 = "ocloud.stream"
            java.lang.String r3 = r13.replace(r3, r6)
            r1.append(r3)
            r1.append(r5)
            java.lang.String r3 = r0.c
            r6 = 0
            boolean[] r7 = new boolean[r6]
            java.lang.String r3 = com.original.tase.utils.Utils.a((java.lang.String) r3, (boolean[]) r7)
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r1)
            r6.append(r14)
            java.lang.String r1 = r6.toString()
            r6 = 1
            java.util.Map[] r7 = new java.util.Map[r6]
            java.util.HashMap r6 = com.original.Constants.a()
            r8 = 0
            r7[r8] = r6
            java.lang.String r1 = r3.b(r1, r9, r7)
            java.lang.String r3 = "\\\""
            java.lang.String r6 = "\""
            java.lang.String r1 = r1.replace(r3, r6)
            java.lang.String r3 = "\\/"
            java.lang.String r1 = r1.replace(r3, r10)
        L_0x027e:
            r7 = r1
            java.lang.String r1 = "<div\\s+[^>]*class=\"jt-info\"[^>]*>\\s*(\\d{4})\\s*</div>"
            r3 = 1
            java.lang.String r1 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r1, (int) r3)
            java.lang.String r6 = "<div\\s+[^>]*class=\"jtip-quality\"[^>]*>\\s*([\\w\\s]+)\\s*</div>"
            java.lang.String r6 = com.original.tase.utils.Regex.a((java.lang.String) r7, (java.lang.String) r6, (int) r3)
            boolean r13 = r0.c(r6)
            java.lang.String r6 = com.original.tase.utils.Regex.a((java.lang.String) r6, (java.lang.String) r4, (int) r3)
            boolean r3 = r6.isEmpty()
            if (r3 != 0) goto L_0x02ae
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r6)
            java.lang.String r6 = "p"
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            r17 = r3
            goto L_0x02bf
        L_0x02ae:
            r17 = r6
            goto L_0x02bf
        L_0x02b1:
            r25 = r1
            r27 = r6
            r28 = r7
            r26 = r8
            r13 = r16
            r7 = r19
            r1 = r20
        L_0x02bf:
            java.lang.String r3 = r2.year
            boolean r3 = r1.equals(r3)
            if (r3 != 0) goto L_0x02d9
            boolean r3 = r7.isEmpty()
            if (r3 == 0) goto L_0x02ce
            goto L_0x02d9
        L_0x02ce:
            r20 = r1
            r19 = r7
            r1 = r23
            r3 = r27
            r7 = r28
            goto L_0x0338
        L_0x02d9:
            r1 = r23
            java.lang.String r6 = r12.b((java.lang.String) r1)
            boolean r3 = r6.startsWith(r11)
            if (r3 == 0) goto L_0x02f8
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r7 = r28
            r3.append(r7)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x0451
        L_0x02f8:
            boolean r3 = r6.startsWith(r10)
            if (r3 == 0) goto L_0x0311
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x0451
        L_0x0311:
            r3 = r27
            boolean r3 = r6.startsWith(r3)
            if (r3 != 0) goto L_0x0451
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r10)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x0451
        L_0x032f:
            r25 = r1
            r3 = r6
            r26 = r8
            r1 = r23
            r13 = r16
        L_0x0338:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r8 = r2.name
            r6.append(r8)
            java.lang.String r8 = " ("
            r6.append(r8)
            java.lang.String r8 = r2.year
            r6.append(r8)
            java.lang.String r8 = ")"
            r6.append(r8)
            java.lang.String r6 = r6.toString()
            r8 = r25
            boolean r6 = r8.equals(r6)
            if (r6 == 0) goto L_0x03ad
            java.lang.String r6 = r12.b((java.lang.String) r1)
            boolean r5 = r6.startsWith(r11)
            if (r5 == 0) goto L_0x0378
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r7)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x0451
        L_0x0378:
            boolean r5 = r6.startsWith(r10)
            if (r5 == 0) goto L_0x0391
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x0451
        L_0x0391:
            boolean r3 = r6.startsWith(r3)
            if (r3 != 0) goto L_0x0451
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r10)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x0451
        L_0x03ad:
            r1 = r30
            goto L_0x0438
        L_0x03b1:
            r24 = r3
            r3 = r6
            r26 = r8
            r8 = r1
            r1 = r23
            java.lang.String r6 = com.original.tase.helper.TitleHelper.f(r8)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r13 = r31.getName()
            r8.append(r13)
            java.lang.String r13 = "season"
            r8.append(r13)
            java.lang.String r13 = r2.session
            r8.append(r13)
            java.lang.String r8 = r8.toString()
            java.lang.String r8 = com.original.tase.helper.TitleHelper.e(r8)
            java.lang.String r8 = com.original.tase.helper.TitleHelper.f(r8)
            boolean r6 = r6.startsWith(r8)
            if (r6 == 0) goto L_0x0434
            java.lang.String r6 = r12.b((java.lang.String) r1)
            boolean r5 = r6.startsWith(r11)
            if (r5 == 0) goto L_0x0401
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r7)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
        L_0x03fe:
            r13 = r16
            goto L_0x0451
        L_0x0401:
            boolean r5 = r6.startsWith(r10)
            if (r5 == 0) goto L_0x0419
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x03fe
        L_0x0419:
            boolean r3 = r6.startsWith(r3)
            if (r3 != 0) goto L_0x03fe
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r5 = r0.c
            r3.append(r5)
            r3.append(r10)
            r3.append(r6)
            java.lang.String r6 = r3.toString()
            goto L_0x03fe
        L_0x0434:
            r1 = r30
            r13 = r16
        L_0x0438:
            r4 = r17
            r6 = r18
            r7 = r19
            r11 = r20
            r12 = r21
            r3 = r24
            r8 = r26
            goto L_0x0119
        L_0x0448:
            r24 = r3
            r22 = r6
            r1 = r7
            r16 = r13
            r6 = r18
        L_0x0451:
            boolean r3 = r6.isEmpty()
            if (r3 == 0) goto L_0x0458
            return
        L_0x0458:
            java.lang.String r3 = ".html"
            boolean r3 = r6.endsWith(r3)
            if (r3 == 0) goto L_0x046c
            int r3 = r6.length()
            int r3 = r3 + -5
            r5 = 0
            java.lang.String r6 = r6.substring(r5, r3)
            goto L_0x046d
        L_0x046c:
            r5 = 0
        L_0x046d:
            boolean r3 = r6.endsWith(r10)
            if (r3 == 0) goto L_0x047d
            int r3 = r6.length()
            r7 = 1
            int r3 = r3 - r7
            java.lang.String r6 = r6.substring(r5, r3)
        L_0x047d:
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r6)
            java.lang.String r6 = "/watching.html"
            r3.append(r6)
            java.lang.String r3 = r3.toString()
            com.original.tase.helper.http.HttpHelper r6 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r7 = new java.util.Map[r5]
            java.lang.String r5 = r6.a((java.lang.String) r3, (java.util.Map<java.lang.String, java.lang.String>[]) r7)
            org.jsoup.nodes.Document r5 = org.jsoup.Jsoup.b(r5)
            boolean r6 = r17.isEmpty()
            if (r6 == 0) goto L_0x04cf
            java.lang.String r6 = "span.quality"
            org.jsoup.nodes.Element r6 = r5.h(r6)
            java.lang.String r6 = r6.G()
            java.lang.String r6 = r6.toLowerCase()
            r7 = 1
            java.lang.String r6 = com.original.tase.utils.Regex.a((java.lang.String) r6, (java.lang.String) r4, (int) r7)
            boolean r7 = r6.isEmpty()
            if (r7 != 0) goto L_0x04cd
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r6)
            java.lang.String r6 = "p"
            r7.append(r6)
            java.lang.String r17 = r7.toString()
            goto L_0x04cf
        L_0x04cd:
            java.lang.String r17 = "HD"
        L_0x04cf:
            r6 = r17
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.HashMap r8 = new java.util.HashMap
            r8.<init>()
            java.lang.String r9 = com.original.Constants.f5838a
            java.lang.String r10 = "User-Agent"
            r8.put(r10, r9)
            java.lang.String r9 = "div.mobile-btn"
            org.jsoup.nodes.Element r9 = r5.h(r9)     // Catch:{ all -> 0x0572 }
            java.lang.String r10 = "a[href]"
            org.jsoup.nodes.Element r9 = r9.h(r10)     // Catch:{ all -> 0x0572 }
            java.lang.String r9 = r9.b((java.lang.String) r1)     // Catch:{ all -> 0x0572 }
            boolean r10 = r9.startsWith(r11)     // Catch:{ all -> 0x0572 }
            if (r10 == 0) goto L_0x0509
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x0572 }
            r10.<init>()     // Catch:{ all -> 0x0572 }
            java.lang.String r12 = "https:"
            r10.append(r12)     // Catch:{ all -> 0x0572 }
            r10.append(r9)     // Catch:{ all -> 0x0572 }
            java.lang.String r9 = r10.toString()     // Catch:{ all -> 0x0572 }
        L_0x0509:
            com.original.tase.helper.http.HttpHelper r10 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x0572 }
            java.lang.String r9 = r10.b((java.lang.String) r9, (java.lang.String) r3)     // Catch:{ all -> 0x0572 }
            org.jsoup.nodes.Document r9 = org.jsoup.Jsoup.b(r9)     // Catch:{ all -> 0x0572 }
            java.lang.String r10 = "div.dowload"
            org.jsoup.select.Elements r9 = r9.g((java.lang.String) r10)     // Catch:{ all -> 0x0572 }
            r10 = r22
            org.jsoup.select.Elements r9 = r9.b(r10)     // Catch:{ all -> 0x056f }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ all -> 0x056f }
        L_0x0525:
            boolean r12 = r9.hasNext()     // Catch:{ all -> 0x056f }
            if (r12 == 0) goto L_0x056f
            java.lang.Object r12 = r9.next()     // Catch:{ all -> 0x056f }
            org.jsoup.nodes.Element r12 = (org.jsoup.nodes.Element) r12     // Catch:{ all -> 0x056f }
            java.lang.String r12 = r12.b((java.lang.String) r1)     // Catch:{ all -> 0x056f }
            r7.add(r12)     // Catch:{ all -> 0x056f }
            boolean r14 = com.original.tase.helper.GoogleVideoHelper.k(r12)     // Catch:{ all -> 0x056f }
            com.original.tase.model.media.MediaSource r15 = new com.original.tase.model.media.MediaSource     // Catch:{ all -> 0x056f }
            r23 = r1
            java.lang.String r1 = r29.a()     // Catch:{ all -> 0x056f }
            if (r14 == 0) goto L_0x0549
            java.lang.String r16 = "GoogleVideo"
            goto L_0x054b
        L_0x0549:
            java.lang.String r16 = "CDN-FastServer"
        L_0x054b:
            r17 = r9
            r9 = r16
            r15.<init>(r1, r9, r13)     // Catch:{ all -> 0x056f }
            r15.setStreamLink(r12)     // Catch:{ all -> 0x056f }
            if (r14 == 0) goto L_0x055a
            r15.setPlayHeader(r8)     // Catch:{ all -> 0x056f }
        L_0x055a:
            if (r14 == 0) goto L_0x0561
            java.lang.String r1 = com.original.tase.helper.GoogleVideoHelper.h(r12)     // Catch:{ all -> 0x056f }
            goto L_0x0562
        L_0x0561:
            r1 = r6
        L_0x0562:
            r15.setQuality((java.lang.String) r1)     // Catch:{ all -> 0x056f }
            r1 = r30
            r1.onNext(r15)     // Catch:{ all -> 0x0576 }
            r9 = r17
            r1 = r23
            goto L_0x0525
        L_0x056f:
            r1 = r30
            goto L_0x0576
        L_0x0572:
            r1 = r30
            r10 = r22
        L_0x0576:
            java.lang.String r9 = "div.les-content"
            org.jsoup.select.Elements r5 = r5.g((java.lang.String) r9)
            org.jsoup.select.Elements r5 = r5.b(r10)
            java.util.Iterator r5 = r5.iterator()
        L_0x0584:
            boolean r9 = r5.hasNext()
            if (r9 == 0) goto L_0x064c
            java.lang.Object r9 = r5.next()
            org.jsoup.nodes.Element r9 = (org.jsoup.nodes.Element) r9
            java.lang.String r10 = "player-data"
            java.lang.String r10 = r9.b((java.lang.String) r10)
            if (r10 == 0) goto L_0x059e
            boolean r12 = r10.isEmpty()
            if (r12 == 0) goto L_0x05a4
        L_0x059e:
            java.lang.String r10 = "data"
            java.lang.String r10 = r9.b((java.lang.String) r10)
        L_0x05a4:
            if (r24 != 0) goto L_0x05c0
            java.lang.String r12 = "episode-data"
            java.lang.String r9 = r9.b((java.lang.String) r12)
            r12 = 1
            java.lang.String r9 = com.original.tase.utils.Regex.a((java.lang.String) r9, (java.lang.String) r4, (int) r12)
            boolean r12 = r9.isEmpty()
            if (r12 != 0) goto L_0x0584
            java.lang.String r12 = r2.eps
            boolean r9 = r9.equals(r12)
            if (r9 != 0) goto L_0x05c0
            goto L_0x0584
        L_0x05c0:
            boolean r9 = r10.startsWith(r11)
            if (r9 == 0) goto L_0x05d7
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r12 = "https:"
            r9.append(r12)
            r9.append(r10)
            java.lang.String r10 = r9.toString()
        L_0x05d7:
            java.lang.String r9 = "load.php"
            boolean r9 = r10.contains(r9)
            if (r9 == 0) goto L_0x05e0
            goto L_0x0584
        L_0x05e0:
            java.lang.String r9 = "vidcloud.icu"
            boolean r9 = r10.contains(r9)
            if (r9 == 0) goto L_0x0636
            java.util.ArrayList r9 = r0.d(r10, r3)
            java.util.Iterator r9 = r9.iterator()
        L_0x05f0:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x0648
            java.lang.Object r10 = r9.next()
            java.lang.String r10 = r10.toString()
            boolean r12 = r7.contains(r10)
            if (r12 != 0) goto L_0x0633
            r7.add(r10)
            boolean r12 = com.original.tase.helper.GoogleVideoHelper.k(r10)
            com.original.tase.model.media.MediaSource r14 = new com.original.tase.model.media.MediaSource
            java.lang.String r15 = r29.a()
            if (r12 == 0) goto L_0x0616
            java.lang.String r16 = "GoogleVideo"
            goto L_0x0618
        L_0x0616:
            java.lang.String r16 = "CDN-FastServer"
        L_0x0618:
            r2 = r16
            r14.<init>(r15, r2, r13)
            r14.setStreamLink(r10)
            if (r12 == 0) goto L_0x0625
            r14.setPlayHeader(r8)
        L_0x0625:
            if (r12 == 0) goto L_0x062c
            java.lang.String r2 = com.original.tase.helper.GoogleVideoHelper.h(r10)
            goto L_0x062d
        L_0x062c:
            r2 = r6
        L_0x062d:
            r14.setQuality((java.lang.String) r2)
            r1.onNext(r14)
        L_0x0633:
            r2 = r31
            goto L_0x05f0
        L_0x0636:
            boolean r2 = r7.contains(r10)
            if (r2 != 0) goto L_0x0648
            r7.add(r10)
            r2 = 1
            boolean[] r9 = new boolean[r2]
            r12 = 0
            r9[r12] = r13
            r0.a(r1, r10, r6, r9)
        L_0x0648:
            r2 = r31
            goto L_0x0584
        L_0x064c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.SeriesNine.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo):void");
    }
}
