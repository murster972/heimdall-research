package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class dunia21 extends BaseProvider {
    private String c = Utils.getProvider(77);

    private String b(MovieInfo movieInfo) {
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/?s=" + com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+"), (Map<String, String>[]) new Map[0])).g("div[class=search-item]").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("div[class=col-xs-9 col-sm-10 search-content]");
            String b = h.h("a[href]").b("href");
            String b2 = h.h("a[href]").b("title");
            if ((movieInfo.name.toLowerCase() + " (" + movieInfo.year + ")").length() == b2.toLowerCase().length() && b2.contains(movieInfo.year)) {
                if (!b2.startsWith("/")) {
                    return b;
                }
                return this.c + b;
            }
        }
        return "";
    }

    public String a() {
        return "Dunia21";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = Utils.a();
        if (a2.toLowerCase().equals("id") || a2.toLowerCase().equals("idn")) {
            String b = b(movieInfo);
            if (!b.isEmpty()) {
                a(movieInfo, observableEmitter, b);
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        String G = Jsoup.b(HttpHelper.e().b(str, this.c + "/")).h("div[class=col-xs-10 content]").h("a").G();
        String str2 = "HD";
        if (G != null && !G.isEmpty() && G.toLowerCase().contains("cam")) {
            str2 = "CAM";
        }
        HashMap<String, String> a2 = Constants.a();
        a2.put(TheTvdb.HEADER_ACCEPT, "*/*");
        a2.put("Origin", this.c);
        a2.put("Referer", str);
        a2.put("User-Agent", Constants.f5838a);
        HttpHelper e = HttpHelper.e();
        String a3 = e.a(this.c + "/ajax/movie.php", "slug=" + Regex.a(str, "dunia21\\.\\w+\\/(.*)\\/", 1), false, (Map<String, String>[]) new Map[]{a2});
        if (!a3.isEmpty()) {
            Iterator it2 = Jsoup.b(a3).g("a[href]").iterator();
            while (it2.hasNext()) {
                String b = ((Element) it2.next()).b("href");
                if (!b.isEmpty() && b.contains(UriUtil.HTTP_SCHEME)) {
                    HashMap hashMap = new HashMap();
                    hashMap.put("Referer", str);
                    hashMap.put("User-Agent", Constants.f5838a);
                    MediaSource mediaSource = new MediaSource(a(), "", false);
                    mediaSource.setPlayHeader(hashMap);
                    mediaSource.setStreamLink(b);
                    mediaSource.setQuality(str2);
                    observableEmitter.onNext(mediaSource);
                }
            }
        }
    }
}
