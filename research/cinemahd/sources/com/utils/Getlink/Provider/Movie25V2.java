package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Movie25V2 extends BaseProvider {
    private String c = Utils.getProvider(64);

    private String b(MovieInfo movieInfo) {
        CharSequence charSequence;
        CharSequence charSequence2;
        MovieInfo movieInfo2 = movieInfo;
        String replace = this.c.replace("https://", "http://");
        String replace2 = movieInfo2.name.replace("Marvel's ", "").replace("DC's ", "");
        HttpHelper e = HttpHelper.e();
        String b = e.b("https://google.ch/search?q=" + com.original.tase.utils.Utils.a(replace2, new boolean[0]).replace("%20", "+") + "+" + movieInfo2.year + "+site:" + replace, "https://google.ch");
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US");
        CharSequence charSequence3 = "http://";
        hashMap.put("Origin", "https://duckduckgo.com");
        CharSequence charSequence4 = "https://";
        hashMap.put("Referer", "https://duckduckgo.com/");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        HttpHelper e2 = HttpHelper.e();
        String b2 = e2.b("https://www.bing.com/search?q=" + com.original.tase.utils.Utils.a(replace2, new boolean[0]).replace("%20", "+") + "+" + movieInfo2.year + "+site:" + replace, "https://www.bing.com");
        HashMap hashMap2 = new HashMap();
        hashMap2.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap2.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US");
        hashMap2.put("Host", "www.startpage.com");
        hashMap2.put("Origin", "https://www.startpage.com");
        hashMap2.put("Referer", "https://www.startpage.com/do/asearch");
        hashMap2.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        hashMap2.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(b);
        String str = "";
        arrayList.add(b2.replaceAll("(</?\\w{1,7}>)", str));
        for (String str2 : arrayList) {
            boolean contains = str2.contains("DuckDuckGo (HTML)");
            boolean contains2 = str2.contains("ixquick.com/");
            ArrayList<ArrayList<String>> b3 = Regex.b(str2, "<a[^>]+href=['\"]([^'\"]+)['\"][^>]*>(.+?)<(?:/a|div|h2)", 2, !contains);
            ArrayList arrayList2 = b3.get(0);
            ArrayList arrayList3 = b3.get(1);
            int i = 0;
            while (true) {
                if (i < arrayList2.size()) {
                    try {
                        String str3 = (String) arrayList2.get(i);
                        String replace3 = ((String) arrayList3.get(i)).replaceAll("\\<[uibp]\\>", str).replaceAll("\\</[uibp]\\>", str).replace("&amp;", "&");
                        if (contains2) {
                            Regex.a(replace3, "<span[^>]*>([^<]*)<", 1, true);
                        }
                        if (!str3.startsWith(UriUtil.HTTP_SCHEME) || !str3.contains("5movies") || str3.contains("/directlink/") || str3.contains("/link/") || str3.contains("/play/") || str3.contains("/stream.php") || str3.contains("//translate.") || str3.contains("startpage.com")) {
                            charSequence2 = charSequence3;
                            charSequence = charSequence4;
                        } else {
                            charSequence2 = charSequence3;
                            charSequence = charSequence4;
                            try {
                                if (str3.replace(charSequence, charSequence2).contains(replace)) {
                                    String a2 = Regex.a(replace3, "(?:^Watch |)(.+?)\\s+\\((\\d{4})\\)", 1, true);
                                    String trim = Regex.a(replace3, "(?:^Watch |)(.+?)\\s+\\((\\d{4})\\)", 2, true).trim();
                                    if (!a2.isEmpty() && !trim.isEmpty()) {
                                        int parseInt = Integer.parseInt(movieInfo2.year);
                                        boolean z = com.original.tase.utils.Utils.b(trim) && (trim.equals(movieInfo2.year) || trim.equals(String.valueOf(parseInt + 1)) || trim.equals(String.valueOf(parseInt + -1)));
                                        if (TitleHelper.f(movieInfo2.name).equals(TitleHelper.f(a2)) && z) {
                                            return URLDecoder.decode(str3, "UTF-8");
                                        }
                                    }
                                }
                            } catch (Throwable th) {
                                th = th;
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        charSequence2 = charSequence3;
                        charSequence = charSequence4;
                        Logger.a(th, new boolean[0]);
                        i++;
                        charSequence3 = charSequence2;
                        charSequence4 = charSequence;
                    }
                    i++;
                    charSequence3 = charSequence2;
                    charSequence4 = charSequence;
                }
            }
        }
        return str;
    }

    public String a() {
        return "Movie25V2";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        int i;
        String str;
        Element h;
        Element h2;
        MovieInfo movieInfo2 = movieInfo;
        StringBuilder sb = new StringBuilder();
        sb.append(this.c);
        sb.append("/search.php?q=");
        sb.append(com.original.tase.utils.Utils.a(movieInfo2.name + " " + movieInfo2.year, new boolean[0]));
        String sb2 = sb.toString();
        Iterator it2 = Jsoup.b(HttpHelper.e().a(sb2, (Map<String, String>[]) new Map[0])).g("div.ml-img").iterator();
        while (true) {
            i = 1;
            if (!it2.hasNext()) {
                str = "";
                break;
            }
            Element element = (Element) it2.next();
            h = element.h("a[href]");
            if (!(h == null || (h2 = element.h("img[alt]")) == null)) {
                String b = h2.b("alt");
                String a2 = Regex.a(b, "(.*?)\\s+\\((\\d{4})\\)", 1);
                String a3 = Regex.a(b, "(.*?)\\s+\\((\\d{4})\\)", 2);
                if (!a2.isEmpty() && !a3.isEmpty() && TitleHelper.f(a2).equals(TitleHelper.f(movieInfo2.name))) {
                    if (a3.trim().isEmpty() || !com.original.tase.utils.Utils.b(a3.trim()) || a3.contains(movieInfo2.year)) {
                        str = h.b("href");
                    }
                }
            }
        }
        str = h.b("href");
        if (str.isEmpty()) {
            str = b(movieInfo);
        }
        if (!str.isEmpty()) {
            if (str.startsWith("//")) {
                str = "http:" + str;
            } else if (str.startsWith("/")) {
                str = this.c + str;
            }
            String b2 = HttpHelper.e().b(str, sb2);
            if (!b2.toLowerCase().contains("link-button")) {
                b2 = HttpHelper.e().b(str.replace("5movies.to/", "movie25.unblocked.mx/"), sb2.replace("5movies.to/", "movie25.unblocked.mx/"));
            }
            Document b3 = Jsoup.b(b2);
            String lowerCase = Regex.a(b2, "Links\\s*-\\s*Quality\\s*(.*?)\\s*<", 1, 34).trim().toLowerCase();
            boolean equals = lowerCase.equals("cam");
            String str2 = lowerCase.equalsIgnoreCase("dvd") ? "HD" : "HQ";
            Iterator it3 = b3.g("li.link-button").iterator();
            HashMap hashMap = new HashMap();
            hashMap.put("accept", "*/*");
            hashMap.put("origin", this.c);
            while (it3.hasNext()) {
                Element element2 = (Element) it3.next();
                if (!observableEmitter.isDisposed()) {
                    Element h3 = element2.h("a[href]");
                    if (h3 != null) {
                        String b4 = h3.b("href");
                        if (b4.startsWith("//")) {
                            b4 = "https:" + b4;
                        } else if (b4.startsWith("/")) {
                            b4 = this.c + b4;
                        }
                        if (b4.trim().toLowerCase().startsWith("?lk=")) {
                            hashMap.put("referer", str + b4);
                            String a4 = Regex.a(b4, "\\?lk=(.*?)$", i);
                            String trim = HttpHelper.e().a(this.c + "/getlink.php?Action=get&lk=" + a4, "Action=get&lk=" + a4, (Map<String, String>[]) new Map[]{hashMap}).trim();
                            if (trim.contains("href=")) {
                                trim = Regex.a(trim, "href=['\"]([^'\"]+)", 1);
                            }
                            if (trim.startsWith("//")) {
                                trim = "https:" + trim;
                            } else if (trim.startsWith("/")) {
                                trim = "https://5movies.to" + trim;
                            }
                            i = 1;
                            a(observableEmitter, trim, str2, equals);
                        }
                    }
                    ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                } else {
                    return;
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }
}
