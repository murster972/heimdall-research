package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class FFilms extends BaseProvider {
    private String c = Utils.getProvider(98);

    public String a() {
        return "FFilms";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        String replace = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+");
        String e = BaseProvider.e(HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]), this.c);
        Iterator it2 = Jsoup.b(HttpHelper.e().b(String.format(e, new Object[]{replace}), this.c)).g("figure.visual-thumbnail").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("href");
            String b2 = element.h("img").b("title");
            if (b2.contains(movieInfo.name + " (" + movieInfo.year + ")")) {
                return b;
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public String c(String str, String str2) {
        if (str.equals("streamango")) {
            return "https://streamango.com/embed/" + str2;
        } else if (str.equals("rapidvideo")) {
            return "https://www.rapidvideo.com/e/" + str2;
        } else if (str.equals("estream")) {
            return "https://estream.to/embed-" + str2 + ".html";
        } else if (str.equals("openload")) {
            return "https://openload.co/embed/" + str2;
        } else if (str.equals("vlid")) {
            return "https://vidlox.me/embed-" + str2 + ".html";
        } else if (str.equals("vzid")) {
            return "https://vidoza.net/embed-" + str2 + ".html";
        } else if (str.equals("vsid")) {
            return "https://verystream.com/e/" + str2;
        } else if (str.equals("fid")) {
            return "https://www.fembed.com/v/" + str2;
        } else if (!str.equals("vid")) {
            return "";
        } else {
            return "https://yandexcdn.com/player/embed_player.php?vid=" + str2;
        }
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0])).g("div.table-responsive").b("td").b("a").iterator();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).b("href");
            String c2 = c(Regex.a(b, "(fid|vid|vsid|vlid|vzid)(?:=)([0-9a-zA-Z]+)", 1), Regex.a(b, "(fid|vid|vsid|vlid|vzid)(?:=)([0-9a-zA-Z]+)", 2));
            if (!c2.isEmpty()) {
                if (c2.startsWith("//")) {
                    c2 = "https:" + c2;
                }
                a(observableEmitter, c2, "HD", false);
            }
        }
    }
}
