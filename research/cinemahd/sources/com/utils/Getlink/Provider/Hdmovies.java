package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import io.reactivex.ObservableEmitter;

public class Hdmovies extends BaseProvider {
    public String[] c;

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String j(java.lang.String r6) {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            java.net.URLConnection r6 = r1.openConnection()     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            int r1 = r6.getResponseCode()     // Catch:{ Exception -> 0x003e }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0033
            java.io.InputStream r1 = r6.getInputStream()     // Catch:{ Exception -> 0x003e }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x003e }
            r2.<init>()     // Catch:{ Exception -> 0x003e }
        L_0x001d:
            int r3 = r1.read()     // Catch:{ Exception -> 0x003e }
            r4 = -1
            if (r3 != r4) goto L_0x002f
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x003e }
            byte[] r2 = r2.toByteArray()     // Catch:{ Exception -> 0x003e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003e }
            r0 = r1
            goto L_0x0033
        L_0x002f:
            r2.write(r3)     // Catch:{ Exception -> 0x003e }
            goto L_0x001d
        L_0x0033:
            if (r6 == 0) goto L_0x0038
            r6.disconnect()     // Catch:{ Exception -> 0x003e }
        L_0x0038:
            if (r6 == 0) goto L_0x003d
            r6.disconnect()
        L_0x003d:
            return r0
        L_0x003e:
            r1 = move-exception
            goto L_0x0047
        L_0x0040:
            r6 = move-exception
            r5 = r0
            r0 = r6
            r6 = r5
            goto L_0x0051
        L_0x0045:
            r1 = move-exception
            r6 = r0
        L_0x0047:
            r1.printStackTrace()     // Catch:{ all -> 0x0050 }
            if (r6 == 0) goto L_0x004f
            r6.disconnect()
        L_0x004f:
            return r0
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r6 == 0) goto L_0x0056
            r6.disconnect()
        L_0x0056:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Hdmovies.j(java.lang.String):java.lang.String");
    }

    public String a() {
        return "Hdmovies";
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0200, code lost:
        if (r8.equals(r1.name + " " + r1.year) != false) goto L_0x0202;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.movie.data.model.MovieInfo r25, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r26) {
        /*
            r24 = this;
            r0 = r24
            r1 = r25
            r2 = r26
            com.movie.data.api.GlobalVariable r3 = com.movie.data.api.GlobalVariable.c()
            com.movie.data.model.AppConfig r3 = r3.a()
            java.util.Map r3 = r3.getHdlist()
            java.lang.String r4 = "1"
            java.lang.Object r3 = r3.get(r4)
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r4 = "##"
            java.lang.String[] r3 = r3.split(r4)
            r0.c = r3
            java.lang.String[] r3 = r0.c
            int r4 = r3.length
            r5 = 0
            r6 = 0
        L_0x0027:
            if (r6 >= r4) goto L_0x02dc
            r7 = r3[r6]
            java.lang.String r8 = "<>"
            java.lang.String[] r7 = r7.split(r8)
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r9 = r7[r5]
            r8.append(r9)
            java.lang.String r9 = r25.getName()
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            java.lang.String r8 = j(r8)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "['\"]?"
            r9.append(r10)
            r11 = 1
            r12 = r7[r11]
            r9.append(r12)
            java.lang.String r12 = "?['\"]\\s*:\\s*['\"]([^'\"]+)['\"]"
            r9.append(r12)
            java.lang.String r9 = r9.toString()
            java.util.ArrayList r9 = com.original.tase.utils.Regex.b((java.lang.String) r8, (java.lang.String) r9, (int) r11, (boolean) r11)
            java.lang.Object r9 = r9.get(r5)
            java.util.ArrayList r9 = (java.util.ArrayList) r9
            java.util.Iterator r9 = r9.iterator()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r10)
            r14 = 2
            r14 = r7[r14]
            r13.append(r14)
            r13.append(r12)
            java.lang.String r13 = r13.toString()
            java.util.ArrayList r13 = com.original.tase.utils.Regex.b((java.lang.String) r8, (java.lang.String) r13, (int) r11, (boolean) r11)
            java.lang.Object r13 = r13.get(r5)
            java.util.ArrayList r13 = (java.util.ArrayList) r13
            java.util.Iterator r13 = r13.iterator()
            r14 = 3
            r14 = r7[r14]
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            r15.append(r10)
            r10 = 4
            r7 = r7[r10]
            r15.append(r7)
            r15.append(r12)
            java.lang.String r7 = r15.toString()
            java.util.ArrayList r7 = com.original.tase.utils.Regex.b((java.lang.String) r8, (java.lang.String) r7, (int) r11, (boolean) r11)
            java.lang.Object r7 = r7.get(r5)
            java.util.ArrayList r7 = (java.util.ArrayList) r7
            java.util.Iterator r7 = r7.iterator()
        L_0x00ba:
            boolean r8 = r9.hasNext()
            if (r8 == 0) goto L_0x02cc
            java.lang.Object r8 = r9.next()
            java.lang.String r8 = (java.lang.String) r8
            boolean r10 = r7.hasNext()
            java.lang.String r11 = "Cookie"
            java.lang.String r12 = "User-Agent"
            java.lang.String r15 = "1080p"
            java.lang.String r5 = "GoogleVideo"
            java.lang.String r0 = "https://drive.google.com/uc?id="
            if (r10 == 0) goto L_0x01b9
            java.lang.Object r10 = r7.next()
            java.lang.String r10 = (java.lang.String) r10
            r16 = r3
            java.lang.String r3 = r1.name
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x01b0
            java.lang.String r3 = r1.year
            boolean r3 = r10.equals(r3)
            if (r3 == 0) goto L_0x01b0
            java.lang.Object r3 = r13.next()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String[] r3 = r3.split(r14)
            int r8 = r3.length
            r10 = 0
        L_0x00fa:
            if (r10 >= r8) goto L_0x01b0
            r17 = r4
            r4 = r3[r10]
            r18 = r3
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r0)
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            if (r3 == 0) goto L_0x019c
            boolean r4 = r3.isEmpty()
            if (r4 != 0) goto L_0x019c
            boolean r4 = com.original.tase.helper.GoogleVideoHelper.j(r3)
            if (r4 == 0) goto L_0x019c
            java.util.HashMap r4 = com.original.tase.helper.GoogleVideoHelper.g(r3)
            if (r4 == 0) goto L_0x019c
            boolean r19 = r4.isEmpty()
            if (r19 != 0) goto L_0x019c
            java.util.Set r4 = r4.entrySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x0133:
            boolean r19 = r4.hasNext()
            if (r19 == 0) goto L_0x019c
            java.lang.Object r19 = r4.next()
            java.util.Map$Entry r19 = (java.util.Map.Entry) r19
            java.lang.Object r20 = r19.getKey()
            r21 = r4
            r4 = r20
            java.lang.String r4 = (java.lang.String) r4
            r20 = r7
            com.original.tase.model.media.MediaSource r7 = new com.original.tase.model.media.MediaSource
            r22 = r8
            java.lang.String r8 = r24.a()
            r23 = r9
            r9 = 0
            r7.<init>(r8, r5, r9)
            r7.setOriginalLink(r3)
            r7.setStreamLink(r4)
            java.lang.Object r4 = r19.getValue()
            java.lang.String r4 = (java.lang.String) r4
            boolean r4 = r4.isEmpty()
            if (r4 == 0) goto L_0x016d
            r4 = r15
            goto L_0x0173
        L_0x016d:
            java.lang.Object r4 = r19.getValue()
            java.lang.String r4 = (java.lang.String) r4
        L_0x0173:
            r7.setQuality((java.lang.String) r4)
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.lang.String r8 = com.original.Constants.f5838a
            r4.put(r12, r8)
            java.lang.Object r8 = r19.getKey()
            java.lang.String r8 = (java.lang.String) r8
            java.lang.String r8 = com.original.tase.helper.GoogleVideoHelper.c(r3, r8)
            r4.put(r11, r8)
            r7.setPlayHeader(r4)
            r2.onNext(r7)
            r7 = r20
            r4 = r21
            r8 = r22
            r9 = r23
            goto L_0x0133
        L_0x019c:
            r20 = r7
            r22 = r8
            r23 = r9
            int r10 = r10 + 1
            r4 = r17
            r3 = r18
            r7 = r20
            r8 = r22
            r9 = r23
            goto L_0x00fa
        L_0x01b0:
            r17 = r4
            r20 = r7
            r23 = r9
        L_0x01b6:
            r4 = 0
            goto L_0x02bd
        L_0x01b9:
            r16 = r3
            r17 = r4
            r20 = r7
            r23 = r9
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r1.name
            r3.append(r4)
            java.lang.String r4 = " ("
            r3.append(r4)
            java.lang.String r4 = r1.year
            r3.append(r4)
            java.lang.String r4 = ")"
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            boolean r3 = r8.equals(r3)
            if (r3 != 0) goto L_0x0202
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r1.name
            r3.append(r4)
            java.lang.String r4 = " "
            r3.append(r4)
            java.lang.String r4 = r1.year
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            boolean r3 = r8.equals(r3)
            if (r3 == 0) goto L_0x01b6
        L_0x0202:
            java.lang.Object r3 = r13.next()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String[] r3 = r3.split(r14)
            int r4 = r3.length
            r7 = 0
        L_0x020e:
            if (r7 >= r4) goto L_0x01b6
            r8 = r3[r7]
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r0)
            r9.append(r8)
            java.lang.String r8 = r9.toString()
            if (r8 == 0) goto L_0x02aa
            boolean r9 = r8.isEmpty()
            if (r9 != 0) goto L_0x02aa
            boolean r9 = com.original.tase.helper.GoogleVideoHelper.j(r8)
            if (r9 == 0) goto L_0x02aa
            java.util.HashMap r9 = com.original.tase.helper.GoogleVideoHelper.g(r8)
            if (r9 == 0) goto L_0x02aa
            boolean r10 = r9.isEmpty()
            if (r10 != 0) goto L_0x02aa
            java.util.Set r9 = r9.entrySet()
            java.util.Iterator r9 = r9.iterator()
        L_0x0243:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x02aa
            java.lang.Object r10 = r9.next()
            java.util.Map$Entry r10 = (java.util.Map.Entry) r10
            java.lang.Object r18 = r10.getKey()
            r19 = r0
            r0 = r18
            java.lang.String r0 = (java.lang.String) r0
            com.original.tase.model.media.MediaSource r1 = new com.original.tase.model.media.MediaSource
            r18 = r3
            java.lang.String r3 = r24.a()
            r21 = r4
            r4 = 0
            r1.<init>(r3, r5, r4)
            r1.setOriginalLink(r8)
            r1.setStreamLink(r0)
            java.lang.Object r0 = r10.getValue()
            java.lang.String r0 = (java.lang.String) r0
            boolean r0 = r0.isEmpty()
            if (r0 == 0) goto L_0x027b
            r0 = r15
            goto L_0x0281
        L_0x027b:
            java.lang.Object r0 = r10.getValue()
            java.lang.String r0 = (java.lang.String) r0
        L_0x0281:
            r1.setQuality((java.lang.String) r0)
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r3 = com.original.Constants.f5838a
            r0.put(r12, r3)
            java.lang.Object r3 = r10.getKey()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r3 = com.original.tase.helper.GoogleVideoHelper.c(r8, r3)
            r0.put(r11, r3)
            r1.setPlayHeader(r0)
            r2.onNext(r1)
            r1 = r25
            r3 = r18
            r0 = r19
            r4 = r21
            goto L_0x0243
        L_0x02aa:
            r19 = r0
            r18 = r3
            r21 = r4
            r4 = 0
            int r7 = r7 + 1
            r1 = r25
            r3 = r18
            r0 = r19
            r4 = r21
            goto L_0x020e
        L_0x02bd:
            r5 = 0
            r0 = r24
            r1 = r25
            r3 = r16
            r4 = r17
            r7 = r20
            r9 = r23
            goto L_0x00ba
        L_0x02cc:
            r16 = r3
            r17 = r4
            r4 = 0
            int r6 = r6 + 1
            r5 = 0
            r0 = r24
            r1 = r25
            r4 = r17
            goto L_0x0027
        L_0x02dc:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Hdmovies.a(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }
}
