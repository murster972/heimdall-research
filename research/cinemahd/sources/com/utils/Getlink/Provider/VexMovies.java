package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import okhttp3.internal.cache.DiskLruCache;

public class VexMovies extends BaseProvider {
    private String c = Utils.getProvider(72);

    private String b(MovieInfo movieInfo) {
        String replace = movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "");
        String replace2 = this.c.replace("https://", "http://");
        String b = HttpHelper.e().b("https://google.ch/search?q=" + com.original.tase.utils.Utils.a(replace, new boolean[0]).replace("%20", "+") + "+site:" + replace2, "https://google.ch");
        "kl=us-en&b=&q=" + com.original.tase.utils.Utils.a(replace + " site:" + replace2.replace("https://", "").replace("http://", ""), new boolean[0]);
        HashMap hashMap = new HashMap();
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US");
        hashMap.put("Origin", "https://duckduckgo.com");
        String str = b;
        hashMap.put("Referer", "https://duckduckgo.com/");
        hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        String b2 = HttpHelper.e().b("https://www.bing.com/search?q=" + com.original.tase.utils.Utils.a(replace, new boolean[0]).replace("%20", "+") + "+site:" + replace2, "https://www.bing.com");
        "cmd=process_search&language=english&enginecount=1&pl=&abp=1&hmb=1&ff=&theme=&flag_ac=0&cat=web&ycc=0&t=air&nj=0&query=" + com.original.tase.utils.Utils.a(replace + " " + replace2.replace("https://", "").replace("http://", ""), new boolean[0]);
        HashMap hashMap2 = new HashMap();
        hashMap2.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap2.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US");
        hashMap2.put("Host", "www.startpage.com");
        hashMap2.put("Origin", "https://www.startpage.com");
        hashMap2.put("Referer", "https://www.startpage.com/do/asearch");
        hashMap2.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        hashMap2.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.162 Safari/537.36");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(str);
        arrayList.add(b2.replaceAll("(</?\\w{1,7}>)", ""));
        String str2 = "(" + TitleHelper.g(replace.replace("$", "\\$").replace("*", "\\*").replace("(", "\\(").replace(")", "\\)").replace("[", "\\[").replace("]", "\\]")) + ")";
        for (String b3 : arrayList) {
            Iterator it2 = Regex.b(b3, "href=['\"](.+?)['\"]", 1, true).get(0).iterator();
            while (true) {
                if (it2.hasNext()) {
                    String str3 = (String) it2.next();
                    try {
                        if (str3.startsWith(UriUtil.HTTP_SCHEME) && str3.contains("vexmovie") && !str3.contains("//translate.") && !str3.contains("startpage.com") && str3.replace("https://", "http://").contains(replace2) && !Regex.a(str3.trim().toLowerCase(), str2, 1, 2).isEmpty()) {
                            return str3;
                        }
                    } catch (Exception e) {
                        Logger.a((Throwable) e, new boolean[0]);
                    }
                }
            }
        }
        return "";
    }

    public String a() {
        return "VexMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* JADX WARNING: Code restructure failed: missing block: B:286:0x069d, code lost:
        if (r0.toLowerCase().contains("cdn") == false) goto L_0x06a2;
     */
    /* JADX WARNING: Removed duplicated region for block: B:100:0x02f0  */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x05a4 A[SYNTHETIC, Splitter:B:228:0x05a4] */
    /* JADX WARNING: Removed duplicated region for block: B:236:0x05c2 A[SYNTHETIC, Splitter:B:236:0x05c2] */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x05e3  */
    /* JADX WARNING: Removed duplicated region for block: B:248:0x05eb  */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x05f0  */
    /* JADX WARNING: Removed duplicated region for block: B:258:0x0607 A[SYNTHETIC, Splitter:B:258:0x0607] */
    /* JADX WARNING: Removed duplicated region for block: B:260:0x060c  */
    /* JADX WARNING: Removed duplicated region for block: B:264:0x061d A[SYNTHETIC, Splitter:B:264:0x061d] */
    /* JADX WARNING: Removed duplicated region for block: B:99:0x02ef A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r23, com.movie.data.model.MovieInfo r24) {
        /*
            r22 = this;
            r1 = r22
            r2 = r23
            java.lang.String r3 = "Referer"
            java.lang.String r4 = "consistent.stream"
            java.lang.String r5 = "\\\""
            java.lang.String r6 = "\""
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r8 = r1.c
            r0.append(r8)
            java.lang.String r8 = "/?s="
            r0.append(r8)
            java.lang.String r8 = r24.getName()
            r9 = 0
            boolean[] r10 = new boolean[r9]
            java.lang.String r8 = com.original.tase.utils.Utils.a((java.lang.String) r8, (boolean[]) r10)
            r0.append(r8)
            java.lang.String r8 = r0.toString()
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r10 = r1.c
            java.lang.String r0 = r0.b((java.lang.String) r8, (java.lang.String) r10)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r10 = "div.item[id]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r10)
            java.util.Iterator r10 = r0.iterator()
        L_0x004a:
            boolean r0 = r10.hasNext()
            java.lang.String r11 = ""
            if (r0 == 0) goto L_0x00c6
            java.lang.Object r0 = r10.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r12 = "a[href]"
            org.jsoup.select.Elements r12 = r0.g((java.lang.String) r12)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r13 = "href"
            java.lang.String r12 = r12.a(r13)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r13 = "h2"
            org.jsoup.select.Elements r13 = r0.g((java.lang.String) r13)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r13 = r13.c()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r14 = "span.year"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r14)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r0.c()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r13 = com.original.tase.helper.TitleHelper.g(r13)     // Catch:{ Exception -> 0x00bf }
            java.lang.String r14 = r24.getName()     // Catch:{ Exception -> 0x00bf }
            java.lang.String r14 = com.original.tase.helper.TitleHelper.g(r14)     // Catch:{ Exception -> 0x00bf }
            boolean r13 = r13.equals(r14)     // Catch:{ Exception -> 0x00bf }
            if (r13 == 0) goto L_0x004a
            java.lang.String r13 = r0.trim()     // Catch:{ Exception -> 0x00bf }
            boolean r13 = r13.isEmpty()     // Catch:{ Exception -> 0x00bf }
            if (r13 != 0) goto L_0x004a
            java.lang.String r13 = r0.trim()     // Catch:{ Exception -> 0x00bf }
            boolean r13 = com.original.tase.utils.Utils.b(r13)     // Catch:{ Exception -> 0x00bf }
            if (r13 == 0) goto L_0x004a
            java.lang.Integer r13 = r24.getYear()     // Catch:{ Exception -> 0x00bf }
            int r13 = r13.intValue()     // Catch:{ Exception -> 0x00bf }
            if (r13 <= 0) goto L_0x004a
            java.lang.String r0 = r0.trim()     // Catch:{ Exception -> 0x00bf }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x00bf }
            java.lang.Integer r13 = r24.getYear()     // Catch:{ Exception -> 0x00bf }
            int r13 = r13.intValue()     // Catch:{ Exception -> 0x00bf }
            if (r0 != r13) goto L_0x004a
            goto L_0x00c7
        L_0x00bf:
            r0 = move-exception
            boolean[] r11 = new boolean[r9]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r11)
            goto L_0x004a
        L_0x00c6:
            r12 = r11
        L_0x00c7:
            boolean r0 = r12.isEmpty()
            java.lang.String r10 = "/"
            r14 = r24
            if (r0 == 0) goto L_0x012f
            java.lang.String r12 = r1.b(r14)
            boolean r0 = r12.isEmpty()
            if (r0 == 0) goto L_0x012d
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r12 = "https://consistent.stream/titles/"
            r0.append(r12)
            java.lang.String r12 = r24.getName()
            java.lang.String r15 = "'"
            java.lang.String r12 = r12.replace(r15, r11)
            java.lang.String r12 = com.original.tase.helper.TitleHelper.g(r12)
            r0.append(r12)
            java.lang.String r12 = "-"
            r0.append(r12)
            java.lang.Integer r12 = r24.getYear()
            r0.append(r12)
            java.lang.String r0 = r0.toString()
            r7.add(r0)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r12 = r1.c
            r0.append(r12)
            r0.append(r10)
            java.lang.String r12 = r24.getName()
            java.lang.String r12 = r12.replace(r15, r11)
            java.lang.String r12 = com.original.tase.helper.TitleHelper.g(r12)
            java.lang.String r12 = com.original.tase.helper.TitleHelper.g(r12)
            r0.append(r12)
            java.lang.String r12 = r0.toString()
        L_0x012d:
            r15 = 1
            goto L_0x0130
        L_0x012f:
            r15 = 0
        L_0x0130:
            boolean r0 = r12.isEmpty()
            java.lang.String r13 = "http"
            java.lang.String r9 = "http:"
            java.lang.String r14 = "//"
            if (r0 != 0) goto L_0x02e4
            boolean r0 = r12.startsWith(r14)
            if (r0 == 0) goto L_0x0154
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r9)
            r0.append(r12)
            java.lang.String r12 = r0.toString()
            r18 = r3
            goto L_0x018a
        L_0x0154:
            boolean r0 = r12.startsWith(r10)
            if (r0 == 0) goto L_0x016e
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r18 = r3
            java.lang.String r3 = r1.c
            r0.append(r3)
            r0.append(r12)
            java.lang.String r12 = r0.toString()
            goto L_0x018a
        L_0x016e:
            r18 = r3
            boolean r0 = r12.startsWith(r13)
            if (r0 != 0) goto L_0x018a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = r1.c
            r0.append(r3)
            r0.append(r10)
            r0.append(r12)
            java.lang.String r12 = r0.toString()
        L_0x018a:
            java.lang.String r0 = "(\\?print=(?:true|false))"
            java.lang.String r12 = r12.replaceAll(r0, r11)
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            r19 = r5
            r3 = 0
            java.util.Map[] r5 = new java.util.Map[r3]
            java.lang.String r0 = r0.b(r12, r8, r5)
            boolean r3 = r0.isEmpty()
            if (r3 != 0) goto L_0x02e8
            java.lang.String r3 = "<title>Page not found"
            boolean r3 = r0.contains(r3)
            if (r3 != 0) goto L_0x02e8
            org.jsoup.nodes.Document r3 = org.jsoup.Jsoup.b(r0)
            java.lang.String r0 = "span.calidad2"
            org.jsoup.nodes.Element r0 = r3.h(r0)     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r0 = r0.G()     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x01d3 }
            java.lang.String r5 = "cam"
            boolean r5 = r0.contains(r5)     // Catch:{ Exception -> 0x01d3 }
            if (r5 != 0) goto L_0x01d0
            java.lang.String r5 = "ts"
            boolean r0 = r0.contains(r5)     // Catch:{ Exception -> 0x01d3 }
            if (r0 == 0) goto L_0x01ce
            goto L_0x01d0
        L_0x01ce:
            r0 = 0
            goto L_0x01d1
        L_0x01d0:
            r0 = 1
        L_0x01d1:
            r5 = r0
            goto L_0x01db
        L_0x01d3:
            r0 = move-exception
            r5 = 0
            boolean[] r8 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r8)
            r5 = 0
        L_0x01db:
            if (r15 == 0) goto L_0x0264
            java.lang.String r0 = "p.meta"
            org.jsoup.nodes.Element r0 = r3.h(r0)     // Catch:{ Exception -> 0x0259 }
            if (r0 == 0) goto L_0x0260
            java.lang.String r8 = "a[rel=\"tag\"]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r8)     // Catch:{ Exception -> 0x0259 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x0259 }
        L_0x01ef:
            boolean r8 = r0.hasNext()     // Catch:{ Exception -> 0x0259 }
            if (r8 == 0) goto L_0x0260
            java.lang.Object r8 = r0.next()     // Catch:{ Exception -> 0x0259 }
            org.jsoup.nodes.Element r8 = (org.jsoup.nodes.Element) r8     // Catch:{ Exception -> 0x0259 }
            java.lang.String r8 = r8.G()     // Catch:{ Exception -> 0x0259 }
            java.lang.String r8 = r8.trim()     // Catch:{ Exception -> 0x0259 }
            java.lang.String r15 = " "
            java.lang.String r8 = r8.replace(r15, r11)     // Catch:{ Exception -> 0x0259 }
            java.lang.String r15 = "\r"
            java.lang.String r8 = r8.replace(r15, r11)     // Catch:{ Exception -> 0x0259 }
            java.lang.String r15 = "\n"
            java.lang.String r8 = r8.replace(r15, r11)     // Catch:{ Exception -> 0x0259 }
            boolean r15 = r8.isEmpty()     // Catch:{ Exception -> 0x0259 }
            if (r15 != 0) goto L_0x0232
            boolean r15 = com.original.tase.utils.Utils.b(r8)     // Catch:{ Exception -> 0x0259 }
            if (r15 == 0) goto L_0x0232
            int r15 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x0259 }
            java.lang.Integer r20 = r24.getYear()     // Catch:{ Exception -> 0x0259 }
            r21 = r0
            int r0 = r20.intValue()     // Catch:{ Exception -> 0x0259 }
            if (r15 == r0) goto L_0x0256
            goto L_0x0234
        L_0x0232:
            r21 = r0
        L_0x0234:
            int r0 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x0259 }
            java.lang.Integer r15 = r24.getYear()     // Catch:{ Exception -> 0x0259 }
            int r15 = r15.intValue()     // Catch:{ Exception -> 0x0259 }
            r16 = 1
            int r15 = r15 + 1
            if (r0 == r15) goto L_0x0256
            int r0 = java.lang.Integer.parseInt(r8)     // Catch:{ Exception -> 0x0259 }
            java.lang.Integer r8 = r24.getYear()     // Catch:{ Exception -> 0x0259 }
            int r8 = r8.intValue()     // Catch:{ Exception -> 0x0259 }
            if (r0 != r8) goto L_0x0256
            r0 = 1
            goto L_0x0261
        L_0x0256:
            r0 = r21
            goto L_0x01ef
        L_0x0259:
            r0 = move-exception
            r8 = 0
            boolean[] r15 = new boolean[r8]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r15)
        L_0x0260:
            r0 = 0
        L_0x0261:
            if (r0 != 0) goto L_0x0264
            return
        L_0x0264:
            java.lang.String r0 = "iframe[src]"
            org.jsoup.select.Elements r0 = r3.g((java.lang.String) r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x026e:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x02e9
            java.lang.Object r3 = r0.next()
            org.jsoup.nodes.Element r3 = (org.jsoup.nodes.Element) r3
            java.lang.String r8 = "src"
            java.lang.String r3 = r3.b((java.lang.String) r8)
            java.lang.String r8 = r3.toLowerCase()
            java.lang.String r15 = "youtube.com/"
            boolean r8 = r8.contains(r15)
            if (r8 != 0) goto L_0x026e
            java.lang.String r8 = r3.toLowerCase()
            java.lang.String r15 = "youtu.be/"
            boolean r8 = r8.contains(r15)
            if (r8 != 0) goto L_0x026e
            boolean r8 = r3.startsWith(r14)
            if (r8 == 0) goto L_0x02ae
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r9)
            r8.append(r3)
            java.lang.String r3 = r8.toString()
            goto L_0x02e0
        L_0x02ae:
            boolean r8 = r3.startsWith(r10)
            if (r8 == 0) goto L_0x02c6
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r15 = r1.c
            r8.append(r15)
            r8.append(r3)
            java.lang.String r3 = r8.toString()
            goto L_0x02e0
        L_0x02c6:
            boolean r8 = r3.startsWith(r13)
            if (r8 != 0) goto L_0x02e0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r15 = r1.c
            r8.append(r15)
            r8.append(r10)
            r8.append(r3)
            java.lang.String r3 = r8.toString()
        L_0x02e0:
            r7.add(r3)
            goto L_0x026e
        L_0x02e4:
            r18 = r3
            r19 = r5
        L_0x02e8:
            r5 = 0
        L_0x02e9:
            boolean r0 = r7.isEmpty()
            if (r0 == 0) goto L_0x02f0
            return
        L_0x02f0:
            java.util.ArrayList r0 = com.original.tase.utils.Utils.a(r7)
            java.util.Iterator r3 = r0.iterator()
        L_0x02f8:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x0755
            java.lang.Object r0 = r3.next()
            r7 = r0
            java.lang.String r7 = (java.lang.String) r7
            java.lang.String r0 = r7.toLowerCase()     // Catch:{ Exception -> 0x0738 }
            boolean r0 = r0.contains(r4)     // Catch:{ Exception -> 0x0738 }
            java.lang.String r8 = "HD"
            if (r0 != 0) goto L_0x0347
            r15 = 1
            boolean[] r0 = new boolean[r15]     // Catch:{ Exception -> 0x0738 }
            r15 = 0
            r0[r15] = r5     // Catch:{ Exception -> 0x0738 }
            r1.a(r2, r7, r8, r0)     // Catch:{ Exception -> 0x0738 }
            java.lang.String r0 = "/openload."
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x0738 }
            if (r0 != 0) goto L_0x0347
            java.lang.String r0 = "/oload."
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x0738 }
            if (r0 != 0) goto L_0x0347
            java.lang.String r0 = "/streamango."
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x0738 }
            if (r0 != 0) goto L_0x0347
            java.lang.String r0 = "/streamcherry"
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x0738 }
            if (r0 != 0) goto L_0x0347
            java.lang.String r0 = "/thevid."
            boolean r0 = r7.contains(r0)     // Catch:{ Exception -> 0x0738 }
            if (r0 != 0) goto L_0x0347
            java.lang.String r0 = "/thevideo."
            r7.contains(r0)     // Catch:{ Exception -> 0x0738 }
        L_0x0347:
            com.original.tase.helper.http.HttpHelper r15 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0738 }
            boolean r0 = r12.isEmpty()     // Catch:{ Exception -> 0x0738 }
            r24 = r3
            java.lang.String r3 = "https://consistent.stream"
            if (r0 == 0) goto L_0x0357
            r1 = r3
            goto L_0x0358
        L_0x0357:
            r1 = r12
        L_0x0358:
            java.net.URL r0 = new java.net.URL     // Catch:{ MalformedURLException -> 0x036b, Exception -> 0x035e }
            r0.<init>(r7)     // Catch:{ MalformedURLException -> 0x036b, Exception -> 0x035e }
            goto L_0x036f
        L_0x035e:
            r0 = move-exception
            r1 = r2
            r21 = r5
            r5 = r18
            r2 = 0
            r18 = r11
            r11 = r22
            goto L_0x0744
        L_0x036b:
            r0 = move-exception
            r0.printStackTrace()     // Catch:{ Exception -> 0x072d }
        L_0x036f:
            java.lang.String r0 = r15.b((java.lang.String) r7, (java.lang.String) r1)     // Catch:{ Exception -> 0x072d }
            boolean r1 = r0.isEmpty()     // Catch:{ Exception -> 0x072d }
            if (r1 != 0) goto L_0x0722
            java.lang.String r1 = "<player[^>]+expire=\"([^\"]+)\"[^>]*>"
            r15 = 2
            r20 = r8
            r8 = 1
            java.lang.String r1 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r1, (int) r8, (int) r15)     // Catch:{ Exception -> 0x072d }
            java.lang.String r2 = "<player[^>]+video=\"([^\"]+)\"[^>]*>"
            java.lang.String r2 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r2, (int) r8, (int) r15)     // Catch:{ Exception -> 0x071e }
            r21 = r5
            java.lang.String r5 = "<player[^>]+hash=\"([^\"]+)\"[^>]*>"
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r5, (int) r8, (int) r15)     // Catch:{ Exception -> 0x071a }
            boolean r5 = r2.isEmpty()     // Catch:{ Exception -> 0x071a }
            if (r5 != 0) goto L_0x0717
            boolean r5 = r0.isEmpty()     // Catch:{ Exception -> 0x071a }
            if (r5 != 0) goto L_0x0717
            java.lang.String r5 = "{\"expire\":\"%s\",\"video\":\"%s\",\"referrer\":\"%s\",\"key\":\"%s\" }"
            r8 = 4
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ Exception -> 0x071a }
            r17 = 0
            r8[r17] = r1     // Catch:{ Exception -> 0x071a }
            r1 = r19
            java.lang.String r2 = r2.replace(r6, r1)     // Catch:{ Exception -> 0x070f }
            r16 = 1
            r8[r16] = r2     // Catch:{ Exception -> 0x070f }
            boolean r2 = r12.isEmpty()     // Catch:{ Exception -> 0x070f }
            if (r2 == 0) goto L_0x03b7
            goto L_0x03ba
        L_0x03b7:
            r12.replace(r6, r1)     // Catch:{ Exception -> 0x070f }
        L_0x03ba:
            r8[r15] = r7     // Catch:{ Exception -> 0x070f }
            r2 = 3
            java.lang.String r0 = r0.replace(r6, r1)     // Catch:{ Exception -> 0x070f }
            r8[r2] = r0     // Catch:{ Exception -> 0x070f }
            java.lang.String r0 = java.lang.String.format(r5, r8)     // Catch:{ Exception -> 0x070f }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Exception -> 0x070f }
            r2.<init>()     // Catch:{ Exception -> 0x070f }
            java.lang.String r5 = "Content-Type"
            java.lang.String r8 = "application/json;charset=UTF-8"
            r2.put(r5, r8)     // Catch:{ Exception -> 0x070f }
            java.lang.String r5 = "Origin"
            r2.put(r5, r3)     // Catch:{ Exception -> 0x070f }
            r5 = r18
            r2.put(r5, r7)     // Catch:{ Exception -> 0x0705 }
            java.lang.String r8 = "Host"
            java.lang.String r15 = "https://"
            java.lang.String r3 = r3.replace(r15, r11)     // Catch:{ Exception -> 0x0705 }
            java.lang.String r15 = "http://"
            java.lang.String r3 = r3.replace(r15, r11)     // Catch:{ Exception -> 0x0705 }
            java.lang.String r3 = r3.replace(r10, r11)     // Catch:{ Exception -> 0x0705 }
            r2.put(r8, r3)     // Catch:{ Exception -> 0x0705 }
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0705 }
            java.lang.String r8 = "https://consistent.stream/api/getVideo"
            r18 = r11
            r15 = 1
            java.util.Map[] r11 = new java.util.Map[r15]     // Catch:{ Exception -> 0x06ff }
            r15 = 0
            r11[r15] = r2     // Catch:{ Exception -> 0x06ff }
            java.lang.String r0 = r3.a((java.lang.String) r8, (java.lang.String) r0, (boolean) r15, (java.util.Map<java.lang.String, java.lang.String>[]) r11)     // Catch:{ Exception -> 0x06ff }
            boolean r2 = r0.isEmpty()     // Catch:{ Exception -> 0x06ff }
            if (r2 != 0) goto L_0x06f8
            java.lang.String r2 = "&quot;"
            java.lang.String r0 = r0.replace(r2, r6)     // Catch:{ Exception -> 0x06ff }
            java.lang.String r2 = "&amp;"
            java.lang.String r3 = "&"
            java.lang.String r0 = r0.replace(r2, r3)     // Catch:{ Exception -> 0x06ff }
            java.lang.String r0 = r0.replace(r1, r6)     // Catch:{ Exception -> 0x06ff }
            java.lang.String r2 = "\\/"
            java.lang.String r0 = r0.replace(r2, r10)     // Catch:{ Exception -> 0x06ff }
            java.util.HashMap r2 = new java.util.HashMap     // Catch:{ Exception -> 0x06ff }
            r2.<init>()     // Catch:{ Exception -> 0x06ff }
            java.lang.String r3 = "Accept"
            java.lang.String r8 = "*/*"
            r2.put(r3, r8)     // Catch:{ Exception -> 0x06ff }
            java.lang.String r3 = "Accept-Language"
            java.lang.String r8 = "en-US,en;q=0.9"
            r2.put(r3, r8)     // Catch:{ Exception -> 0x06ff }
            r2.put(r5, r7)     // Catch:{ Exception -> 0x06ff }
            java.lang.String r3 = "User-Agent"
            java.lang.String r7 = com.original.Constants.f5838a     // Catch:{ Exception -> 0x06ff }
            r2.put(r3, r7)     // Catch:{ Exception -> 0x06ff }
            java.lang.String r3 = "['\"]sources['\"].+?['\"]([^'\"]*//[^'\"]+?)['\"]"
            r7 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r3, (int) r7, (boolean) r7)     // Catch:{ Exception -> 0x06ff }
            r3 = 0
            java.lang.Object r0 = r0.get(r3)     // Catch:{ Exception -> 0x06ff }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x06ff }
            java.util.ArrayList r0 = com.original.tase.utils.Utils.a(r0)     // Catch:{ Exception -> 0x06ff }
            java.util.Iterator r3 = r0.iterator()     // Catch:{ Exception -> 0x06ff }
        L_0x0455:
            boolean r0 = r3.hasNext()     // Catch:{ Exception -> 0x06ff }
            if (r0 == 0) goto L_0x06f8
            java.lang.Object r0 = r3.next()     // Catch:{ Exception -> 0x06ff }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ Exception -> 0x06ff }
            boolean r7 = r0.isEmpty()     // Catch:{ Exception -> 0x06e0 }
            if (r7 != 0) goto L_0x06d6
            boolean r7 = r0.startsWith(r14)     // Catch:{ Exception -> 0x06e0 }
            if (r7 == 0) goto L_0x0489
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x047d }
            r7.<init>()     // Catch:{ Exception -> 0x047d }
            r7.append(r9)     // Catch:{ Exception -> 0x047d }
            r7.append(r0)     // Catch:{ Exception -> 0x047d }
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x047d }
            goto L_0x04b8
        L_0x047d:
            r0 = move-exception
            r11 = r22
            r19 = r1
            r15 = r20
            r8 = 0
            r1 = r23
            goto L_0x06ea
        L_0x0489:
            boolean r7 = r0.startsWith(r10)     // Catch:{ Exception -> 0x06e0 }
            if (r7 == 0) goto L_0x04a1
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x047d }
            r7.<init>()     // Catch:{ Exception -> 0x047d }
            java.lang.String r8 = "http://vexmovies.org"
            r7.append(r8)     // Catch:{ Exception -> 0x047d }
            r7.append(r0)     // Catch:{ Exception -> 0x047d }
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x047d }
            goto L_0x04b8
        L_0x04a1:
            boolean r7 = r0.startsWith(r13)     // Catch:{ Exception -> 0x06e0 }
            if (r7 != 0) goto L_0x04b8
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x047d }
            r7.<init>()     // Catch:{ Exception -> 0x047d }
            java.lang.String r8 = "http://vexmovies.org/"
            r7.append(r8)     // Catch:{ Exception -> 0x047d }
            r7.append(r0)     // Catch:{ Exception -> 0x047d }
            java.lang.String r0 = r7.toString()     // Catch:{ Exception -> 0x047d }
        L_0x04b8:
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06e0 }
            java.lang.String r8 = "openload."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06e0 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "oload."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "thevid"
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "streamango."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "streamcherry."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "silkplay."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "d0stream.com"
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "vidlox."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 != 0) goto L_0x06bc
            java.lang.String r7 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = "uploadhaven."
            boolean r7 = r7.contains(r8)     // Catch:{ Exception -> 0x06b1 }
            if (r7 == 0) goto L_0x0526
            goto L_0x06bc
        L_0x0526:
            boolean r7 = com.original.tase.helper.GoogleVideoHelper.k(r0)     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r11 = "gphoto."
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x06b1 }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = "dfcdn."
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = "cloudfront"
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = "flenix."
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = "flxserver"
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = "flixserver"
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            boolean r8 = r8.contains(r4)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = "cdnvideo"
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 != 0) goto L_0x0597
            java.lang.String r8 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r11 = ".m3u8"
            boolean r8 = r8.contains(r11)     // Catch:{ Exception -> 0x047d }
            if (r8 == 0) goto L_0x0595
            goto L_0x0597
        L_0x0595:
            r8 = 0
            goto L_0x0598
        L_0x0597:
            r8 = 1
        L_0x0598:
            java.lang.String r11 = r0.toLowerCase()     // Catch:{ Exception -> 0x06b1 }
            java.lang.String r15 = "megaup."
            boolean r11 = r11.contains(r15)     // Catch:{ Exception -> 0x06b1 }
            if (r11 != 0) goto L_0x05bf
            java.lang.String r11 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r15 = "linkhost."
            boolean r11 = r11.contains(r15)     // Catch:{ Exception -> 0x047d }
            if (r11 != 0) goto L_0x05bf
            java.lang.String r11 = r0.toLowerCase()     // Catch:{ Exception -> 0x047d }
            java.lang.String r15 = "/resolve/"
            boolean r11 = r11.contains(r15)     // Catch:{ Exception -> 0x047d }
            if (r11 == 0) goto L_0x05bd
            goto L_0x05bf
        L_0x05bd:
            r11 = 0
            goto L_0x05c0
        L_0x05bf:
            r11 = 1
        L_0x05c0:
            if (r21 == 0) goto L_0x05e3
            java.lang.StringBuilder r15 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x05da }
            r15.<init>()     // Catch:{ Exception -> 0x05da }
            r19 = r1
            java.lang.String r1 = r22.a()     // Catch:{ Exception -> 0x05ee }
            r15.append(r1)     // Catch:{ Exception -> 0x05ee }
            java.lang.String r1 = " (CAM)"
            r15.append(r1)     // Catch:{ Exception -> 0x05ee }
            java.lang.String r1 = r15.toString()     // Catch:{ Exception -> 0x05ee }
            goto L_0x05e9
        L_0x05da:
            r0 = move-exception
            r19 = r1
        L_0x05dd:
            r11 = r22
            r1 = r23
            goto L_0x06d3
        L_0x05e3:
            r19 = r1
            java.lang.String r1 = r22.a()     // Catch:{ Exception -> 0x06af }
        L_0x05e9:
            if (r7 == 0) goto L_0x05f0
            java.lang.String r8 = "GoogleVideo"
            goto L_0x05fc
        L_0x05ee:
            r0 = move-exception
            goto L_0x05dd
        L_0x05f0:
            if (r8 == 0) goto L_0x05f5
            java.lang.String r8 = "CDN-FastServer"
            goto L_0x05fc
        L_0x05f5:
            if (r11 == 0) goto L_0x05fa
            java.lang.String r8 = "CDN"
            goto L_0x05fc
        L_0x05fa:
            java.lang.String r8 = "CDN-SlowServer"
        L_0x05fc:
            com.original.tase.model.media.MediaSource r11 = new com.original.tase.model.media.MediaSource     // Catch:{ Exception -> 0x06af }
            r15 = 0
            r11.<init>(r1, r8, r15)     // Catch:{ Exception -> 0x06af }
            r11.setStreamLink(r0)     // Catch:{ Exception -> 0x06af }
            if (r7 == 0) goto L_0x060c
            java.lang.String r8 = com.original.tase.helper.GoogleVideoHelper.h(r0)     // Catch:{ Exception -> 0x05ee }
            goto L_0x060e
        L_0x060c:
            r8 = r20
        L_0x060e:
            r11.setQuality((java.lang.String) r8)     // Catch:{ Exception -> 0x06af }
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x06af }
            java.lang.String r7 = "gphoto."
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x06af }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "dfcdn."
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "mentor."
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "flenix."
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "flxserver"
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "flixserver"
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = ".m3u8"
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "megaup."
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "linkhost."
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r7 = "/resolve/"
            boolean r1 = r1.contains(r7)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            boolean r1 = r1.contains(r4)     // Catch:{ Exception -> 0x05ee }
            if (r1 != 0) goto L_0x069f
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ Exception -> 0x05ee }
            java.lang.String r1 = "cdn"
            boolean r0 = r0.contains(r1)     // Catch:{ Exception -> 0x05ee }
            if (r0 == 0) goto L_0x06a2
        L_0x069f:
            r11.setPlayHeader(r2)     // Catch:{ Exception -> 0x06af }
        L_0x06a2:
            r1 = r23
            r1.onNext(r11)     // Catch:{ Exception -> 0x06ad }
            r7 = 1
            r11 = r22
            r15 = r20
            goto L_0x06ef
        L_0x06ad:
            r0 = move-exception
            goto L_0x06b6
        L_0x06af:
            r0 = move-exception
            goto L_0x06b4
        L_0x06b1:
            r0 = move-exception
            r19 = r1
        L_0x06b4:
            r1 = r23
        L_0x06b6:
            r8 = 0
            r11 = r22
            r15 = r20
            goto L_0x06ea
        L_0x06bc:
            r19 = r1
            r1 = r23
            r7 = 1
            boolean[] r8 = new boolean[r7]     // Catch:{ Exception -> 0x06d0 }
            r11 = 0
            r8[r11] = r21     // Catch:{ Exception -> 0x06d0 }
            r11 = r22
            r15 = r20
            r11.a(r1, r0, r15, r8)     // Catch:{ Exception -> 0x06ce }
            goto L_0x06ef
        L_0x06ce:
            r0 = move-exception
            goto L_0x06e9
        L_0x06d0:
            r0 = move-exception
            r11 = r22
        L_0x06d3:
            r15 = r20
            goto L_0x06e9
        L_0x06d6:
            r11 = r22
            r19 = r1
            r15 = r20
            r7 = 1
            r1 = r23
            goto L_0x06ef
        L_0x06e0:
            r0 = move-exception
            r11 = r22
            r19 = r1
            r15 = r20
            r1 = r23
        L_0x06e9:
            r8 = 0
        L_0x06ea:
            boolean[] r7 = new boolean[r8]     // Catch:{ Exception -> 0x06f5 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r7)     // Catch:{ Exception -> 0x06f5 }
        L_0x06ef:
            r20 = r15
            r1 = r19
            goto L_0x0455
        L_0x06f5:
            r0 = move-exception
            goto L_0x0743
        L_0x06f8:
            r11 = r22
            r19 = r1
            r1 = r23
            goto L_0x072b
        L_0x06ff:
            r0 = move-exception
            r11 = r22
            r19 = r1
            goto L_0x070c
        L_0x0705:
            r0 = move-exception
            r19 = r1
            r18 = r11
            r11 = r22
        L_0x070c:
            r1 = r23
            goto L_0x0743
        L_0x070f:
            r0 = move-exception
            r19 = r1
            r5 = r18
            r1 = r23
            goto L_0x0733
        L_0x0717:
            r1 = r23
            goto L_0x0725
        L_0x071a:
            r0 = move-exception
            r1 = r23
            goto L_0x0731
        L_0x071e:
            r0 = move-exception
            r1 = r23
            goto L_0x072f
        L_0x0722:
            r1 = r2
            r21 = r5
        L_0x0725:
            r5 = r18
            r18 = r11
            r11 = r22
        L_0x072b:
            r2 = 0
            goto L_0x0749
        L_0x072d:
            r0 = move-exception
            r1 = r2
        L_0x072f:
            r21 = r5
        L_0x0731:
            r5 = r18
        L_0x0733:
            r18 = r11
            r11 = r22
            goto L_0x0743
        L_0x0738:
            r0 = move-exception
            r24 = r3
            r21 = r5
            r5 = r18
            r18 = r11
            r11 = r1
            r1 = r2
        L_0x0743:
            r2 = 0
        L_0x0744:
            boolean[] r3 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r3)
        L_0x0749:
            r3 = r24
            r2 = r1
            r1 = r11
            r11 = r18
            r18 = r5
            r5 = r21
            goto L_0x02f8
        L_0x0755:
            r11 = r1
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.VexMovies.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo):void");
    }
}
