package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class AnimeShow extends BaseProvider {
    private String c = (Utils.getProvider(74) + "/");

    public String a() {
        return "AnimeShow";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(movieInfo, b, observableEmitter, DiskLruCache.VERSION_1, false);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(movieInfo, b, observableEmitter, movieInfo.eps, false);
        }
    }

    public String a(MovieInfo movieInfo, String str, ObservableEmitter<? super MediaSource> observableEmitter, String str2, boolean z) {
        Boolean.valueOf(movieInfo.getSession().intValue() > 0);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0])).g("div[class=e_l_r]").iterator();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).h("a").b("href");
            if (!b.isEmpty()) {
                if (!b.contains("-episode-" + str2 + "/")) {
                    if (b.contains("-episode-" + str2 + "-")) {
                    }
                }
                String a2 = Jsoup.b(HttpHelper.e().a(b, (Map<String, String>[]) new Map[0])).g("iframe[class=embed-responsive-item]").a("src");
                if (!a2.isEmpty()) {
                    MediaSource mediaSource = new MediaSource(a(), "", false);
                    mediaSource.setStreamLink(a2);
                    mediaSource.setQuality("HQ");
                    observableEmitter.onNext(mediaSource);
                }
                if (b.endsWith("/")) {
                    b = b.substring(0, b.length() - 1);
                }
                String str3 = b + "-mirror-2/";
                String a3 = Jsoup.b(HttpHelper.e().a(str3, (Map<String, String>[]) new Map[0])).g("iframe[class=embed-responsive-item]").a("src");
                if (!a3.isEmpty()) {
                    MediaSource mediaSource2 = new MediaSource(a(), "", false);
                    mediaSource2.setStreamLink(a3);
                    mediaSource2.setQuality("HD");
                    observableEmitter.onNext(mediaSource2);
                }
                return str3;
            }
        }
        return "";
    }

    public String b(MovieInfo movieInfo) {
        if (!a(movieInfo.genres)) {
            return "";
        }
        if (movieInfo.name.toLowerCase().equals("steins;gate") && movieInfo.getSessionYear().intValue() == 2018) {
            movieInfo.name += " 0";
        }
        Boolean valueOf = Boolean.valueOf(movieInfo.getSession().intValue() > 0);
        String replace = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+");
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "find.html?key=" + replace, (Map<String, String>[]) new Map[0])).g("div[class=genres_result]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.h("a[href]").b("href");
            if (b.isEmpty()) {
                return "";
            }
            String G = element.h("div[class=genres_result_dates]").G();
            String G2 = element.h("div[class=genres_result_title]").G();
            if (valueOf.booleanValue()) {
                if (G2.equals(movieInfo.name + " Season " + movieInfo.session) || (movieInfo.name.equalsIgnoreCase(G2) && G.contains(movieInfo.sessionYear))) {
                    return b;
                }
            } else if (movieInfo.name.toLowerCase().equals(G2.toLowerCase()) && G.contains(movieInfo.year)) {
                return b;
            }
        }
        return "";
    }
}
