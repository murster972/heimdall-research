package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class KRMovies extends BaseProvider {
    private String c = Utils.getProvider(10);

    private String b(MovieInfo movieInfo) {
        Iterator it2 = Jsoup.b(HttpHelper.e().a(String.format(this.c + "/?s=%s", new Object[]{com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0])}), (Map<String, String>[]) new Map[0])).g("div.Block").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String G = element.h("h2").G();
            if (!G.isEmpty() && TitleHelper.a(G.toLowerCase(), "").contains(TitleHelper.a(movieInfo.getNameAndYear().toLowerCase(), ""))) {
                return element.h("a").b("href");
            }
        }
        return "";
    }

    public String a() {
        return "KRMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        Iterator it2;
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("referer", this.c + "/");
        hashMap.put("user-agent", Constants.f5838a);
        String b = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).h("div.InsideSlider").h("form#WatchForm").b("action");
        hashMap.put("referer", b);
        String a2 = HttpHelper.e().a(b, "view=1", (Map<String, String>[]) new Map[]{hashMap});
        Document b2 = Jsoup.b(a2);
        String a3 = Regex.a(a2, "url\\s*(?:=|:)\\s*['\"]([^'\"].+servers.+[^'\"])['\"]", 1);
        if (!a3.isEmpty()) {
            Iterator it3 = b2.g("ul.serversList").b("li").iterator();
            String a4 = Regex.a(a2, "data\\s*(?:=|:)\\s*['\"](q=\\w\\d+\\&i=)['\"]", 1);
            HashMap<String, String> a5 = Constants.a();
            a5.put("accept", "*/*");
            a5.put("referer", b);
            while (it3.hasNext()) {
                String b3 = ((Element) it3.next()).b("data-server");
                if (!b3.isEmpty()) {
                    HttpHelper e = HttpHelper.e();
                    String a6 = Regex.a(e.a(a3, a4 + b3, false, (Map<String, String>[]) new Map[]{a5}), "(?:SRC|src)\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
                    if (!a6.isEmpty()) {
                        a(observableEmitter, a6, "HD", false);
                    }
                }
            }
            it2 = b2.g("div.DownloadList").b("a").iterator();
        } else {
            it2 = null;
        }
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String a7 = Regex.a(element.toString(), "(\\d{3,4})p", 1);
            if (a7.isEmpty()) {
                a7 = "HD";
            }
            String b4 = element.b("href");
            if (!b4.isEmpty()) {
                a(observableEmitter, b4, a7, false);
            }
        }
    }
}
