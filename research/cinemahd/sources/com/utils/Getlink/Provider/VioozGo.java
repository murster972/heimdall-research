package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class VioozGo extends BaseProvider {
    private String c = (Utils.getProvider(101) + "/");
    private String d = this.c;

    public String a() {
        return "VioozGo";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        HashMap hashMap = new HashMap();
        String str = this.c + "search?q=" + com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+") + "&s=t";
        this.d = str;
        hashMap.put("referer", this.c);
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("div[class=film boxed film_grid]").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.h("div[class=cont_display]").h("a").b("href");
            if (element.h("div[class=list_film_header]").h("h3[class=title_grid]").b("title").equalsIgnoreCase(movieInfo.name + " (" + movieInfo.year + ")")) {
                return b;
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.d);
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("li[class=menu_categorie]").iterator();
        while (it2.hasNext()) {
            String str2 = "";
            String replace = ((Element) it2.next()).h("a").b("href").replace("/go/", str2);
            try {
                str2 = new String(Base64.decode(replace, 10), "UTF-8").trim();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
                try {
                    str2 = new String(Base64.decode(replace, 10)).trim();
                } catch (Exception unused) {
                    Logger.a(th, new boolean[0]);
                }
            }
            if (!str2.isEmpty()) {
                Logger.a("http streamlink =  ", str2);
                a(observableEmitter, str2, "HQ", false);
            }
        }
    }
}
