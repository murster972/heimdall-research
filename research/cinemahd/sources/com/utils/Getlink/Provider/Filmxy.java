package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import com.uwetrottmann.trakt5.TraktV2;
import io.reactivex.ObservableEmitter;
import okhttp3.internal.cache.DiskLruCache;

public class Filmxy extends BaseProvider {
    private String[] c = {"l", DiskLruCache.VERSION_1, "m", DiskLruCache.VERSION_1, "s", DiskLruCache.VERSION_1, "l", TraktV2.API_VERSION, "m", TraktV2.API_VERSION, "s", TraktV2.API_VERSION, "l", "3", "m", "3", "s", "3", "l", "4", "m", "4", "s", "4"};
    private String d = Utils.getProvider(61);

    public Filmxy() {
        new String[]{"DownAce", "UsersCloud", "opendload", "streamango"};
    }

    public String a() {
        return "Filmxy";
    }

    /* JADX WARNING: type inference failed for: r11v0 */
    /* JADX WARNING: type inference failed for: r11v3 */
    /* access modifiers changed from: protected */
    /* JADX WARNING: Incorrect type for immutable var: ssa=int, code=?, for r11v1, types: [boolean, int] */
    /* JADX WARNING: Removed duplicated region for block: B:128:0x0444 A[Catch:{ all -> 0x0468 }] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x044a A[Catch:{ all -> 0x0468 }] */
    /* JADX WARNING: Removed duplicated region for block: B:60:0x02c4  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.movie.data.model.MovieInfo r23, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r24) {
        /*
            r22 = this;
            r1 = r22
            r0 = r23
            r2 = r24
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r4 = com.original.Constants.f5838a
            java.lang.String r5 = "user-agent"
            r3.put(r5, r4)
            java.lang.String r4 = ""
            r5 = 0
            r7 = r4
            r8 = r7
            r6 = 0
        L_0x0018:
            java.lang.String[] r9 = r1.c
            int r9 = r9.length
            java.lang.String r10 = "/"
            r11 = 1
            if (r6 >= r9) goto L_0x00e0
            com.original.tase.helper.http.HttpHelper r8 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r12 = "https://static."
            r9.append(r12)
            java.lang.String r12 = r1.d
            java.lang.String r12 = com.utils.Getlink.Provider.BaseProvider.h(r12)
            r9.append(r12)
            java.lang.String r12 = "/json/"
            r9.append(r12)
            java.lang.String[] r12 = r1.c
            r12 = r12[r6]
            r9.append(r12)
            r9.append(r10)
            java.lang.String[] r12 = r1.c
            int r13 = r6 + 1
            r12 = r12[r13]
            r9.append(r12)
            java.lang.String r12 = ".json"
            r9.append(r12)
            java.lang.String r9 = r9.toString()
            java.util.Map[] r12 = new java.util.Map[r11]
            r12[r5] = r3
            java.lang.String r8 = r8.a((java.lang.String) r9, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r12 = "['\"](\\d+)['\"]\\s*:\\s*['\"]("
            r9.append(r12)
            java.lang.String r13 = r0.name
            r9.append(r13)
            java.lang.String r13 = "\\s*\\("
            r9.append(r13)
            java.lang.String r14 = r0.year
            r9.append(r14)
            java.lang.String r14 = "\\))['\"]"
            r9.append(r14)
            java.lang.String r9 = r9.toString()
            java.lang.String r9 = com.original.tase.utils.Regex.a((java.lang.String) r8, (java.lang.String) r9, (int) r11)
            boolean r15 = r9.isEmpty()
            if (r15 != 0) goto L_0x00db
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r12)
            java.lang.String r12 = r0.name
            r7.append(r12)
            r7.append(r13)
            java.lang.String r12 = r0.year
            r7.append(r12)
            r7.append(r14)
            java.lang.String r7 = r7.toString()
            r12 = 2
            java.lang.String r7 = com.original.tase.utils.Regex.a((java.lang.String) r8, (java.lang.String) r7, (int) r12)
            java.lang.String r8 = com.original.tase.helper.TitleHelper.f(r7)
            java.lang.String r12 = r23.getName()
            java.lang.String r12 = com.original.tase.helper.TitleHelper.e(r12)
            java.lang.String r12 = com.original.tase.helper.TitleHelper.f(r12)
            boolean r8 = r8.startsWith(r12)
            if (r8 == 0) goto L_0x00db
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = r1.d
            r4.append(r6)
            java.lang.String r6 = "/?p="
            r4.append(r6)
            r4.append(r9)
            java.lang.String r4 = r4.toString()
            r8 = r9
            goto L_0x00e0
        L_0x00db:
            int r6 = r6 + 2
            r8 = r9
            goto L_0x0018
        L_0x00e0:
            boolean r6 = r4.isEmpty()
            if (r6 == 0) goto L_0x014b
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = r0.name
            java.lang.String r6 = r6.toLowerCase()
            r4.append(r6)
            java.lang.String r6 = " "
            r4.append(r6)
            java.lang.String r0 = r0.year
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            java.lang.String r4 = "-"
            java.lang.String r0 = com.original.tase.helper.TitleHelper.a(r0, r4)
            com.original.tase.helper.http.HttpHelper r4 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r6 = r22.c()
            java.util.Map[] r9 = new java.util.Map[r11]
            r9[r5] = r3
            java.lang.String r3 = r4.a((java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r6 = "['\"]([^'\"]*[^'\"]*"
            r4.append(r6)
            r4.append(r0)
            java.lang.String r0 = "[^'\"]+)['\"]"
            r4.append(r0)
            java.lang.String r0 = r4.toString()
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r0, (int) r11)
            java.lang.String r3 = "\\/"
            java.lang.String r0 = r0.replace(r3, r10)
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L_0x013f
            return
        L_0x013f:
            java.lang.String r3 = r1.d
            java.lang.String r3 = com.utils.Getlink.Provider.BaseProvider.h(r3)
            java.lang.String r4 = "filmxy.live"
            java.lang.String r4 = r0.replace(r4, r3)
        L_0x014b:
            java.util.ArrayList r0 = new java.util.ArrayList
            r0.<init>()
            r0.add(r4)
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L_0x015a
            return
        L_0x015a:
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.lang.String r4 = "accept"
            java.lang.String r6 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
            r3.put(r4, r6)
            java.lang.String r4 = "accept-language"
            java.lang.String r6 = "en-US;q=0.9,en;q=0.8"
            r3.put(r4, r6)
            java.lang.String r4 = "upgrade-insecure-requests"
            java.lang.String r6 = "1"
            r3.put(r4, r6)
            java.util.Iterator r4 = r0.iterator()
        L_0x0178:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0506
            java.lang.Object r0 = r4.next()
            r6 = r0
            java.lang.String r6 = (java.lang.String) r6
            boolean r0 = r1.d(r7)
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r12 = new java.util.Map[r11]
            r12[r5] = r3
            java.lang.String r9 = r9.a((java.lang.String) r6, (java.util.Map<java.lang.String, java.lang.String>[]) r12)
            org.jsoup.nodes.Document r9 = org.jsoup.Jsoup.b(r9)
            boolean r12 = r8.isEmpty()
            if (r12 != 0) goto L_0x01bb
            java.lang.String r12 = "div.movie-info"
            org.jsoup.nodes.Element r12 = r9.h(r12)
            java.lang.String r13 = "h2"
            org.jsoup.nodes.Element r12 = r12.h(r13)
            java.lang.String r12 = r12.G()
            if (r12 == 0) goto L_0x01bb
            boolean r13 = r12.isEmpty()
            if (r13 != 0) goto L_0x01bb
            boolean r0 = r1.c(r12)
        L_0x01bb:
            r12 = r0
            java.lang.String r0 = "div.watch-servers"
            org.jsoup.select.Elements r0 = r9.g((java.lang.String) r0)
            java.lang.String r0 = r0.toString()
            java.lang.String r13 = "&quot;"
            java.lang.String r14 = "\""
            java.lang.String r0 = r0.replace(r13, r14)
            java.lang.String r13 = "src\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]"
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r13, (int) r11, (boolean) r11)
            java.lang.Object r0 = r0.get(r5)
            java.util.ArrayList r0 = (java.util.ArrayList) r0
            java.util.Iterator r0 = r0.iterator()
            java.util.ArrayList r13 = new java.util.ArrayList
            r13.<init>()
        L_0x01e3:
            boolean r14 = r0.hasNext()
            java.lang.String r15 = "HD"
            java.lang.String r5 = "http"
            java.lang.String r11 = "http:"
            r23 = r3
            java.lang.String r3 = ":"
            r16 = r4
            java.lang.String r4 = "//"
            if (r14 == 0) goto L_0x0259
            java.lang.Object r14 = r0.next()
            java.lang.String r14 = (java.lang.String) r14
            boolean r4 = r14.startsWith(r4)
            if (r4 == 0) goto L_0x0213
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r11)
            r3.append(r14)
            java.lang.String r14 = r3.toString()
            goto L_0x0240
        L_0x0213:
            boolean r3 = r14.startsWith(r3)
            if (r3 == 0) goto L_0x0229
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            r3.append(r5)
            r3.append(r14)
            java.lang.String r14 = r3.toString()
            goto L_0x0240
        L_0x0229:
            boolean r3 = r14.startsWith(r10)
            if (r3 == 0) goto L_0x0240
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r1.d
            r3.append(r4)
            r3.append(r14)
            java.lang.String r14 = r3.toString()
        L_0x0240:
            boolean r3 = r13.contains(r14)
            if (r3 != 0) goto L_0x0252
            r13.add(r14)
            r3 = 1
            boolean[] r4 = new boolean[r3]
            r3 = 0
            r4[r3] = r12
            r1.a(r2, r14, r15, r4)
        L_0x0252:
            r3 = r23
            r4 = r16
            r5 = 0
            r11 = 1
            goto L_0x01e3
        L_0x0259:
            java.lang.String r0 = "div.video-container"
            org.jsoup.nodes.Element r0 = r9.h(r0)
            if (r0 == 0) goto L_0x0356
            java.lang.String r14 = "iframe[src]"
            org.jsoup.nodes.Element r0 = r0.h(r14)
            if (r0 == 0) goto L_0x0356
            java.lang.String r14 = "src"
            java.lang.String r0 = r0.b((java.lang.String) r14)
            boolean r14 = r0.startsWith(r4)
            if (r14 == 0) goto L_0x0287
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r11)
            r14.append(r0)
            java.lang.String r0 = r14.toString()
        L_0x0284:
            r17 = r7
            goto L_0x02b6
        L_0x0287:
            boolean r14 = r0.startsWith(r3)
            if (r14 == 0) goto L_0x029d
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r14.append(r5)
            r14.append(r0)
            java.lang.String r0 = r14.toString()
            goto L_0x0284
        L_0x029d:
            boolean r14 = r0.startsWith(r10)
            if (r14 == 0) goto L_0x0284
            java.lang.StringBuilder r14 = new java.lang.StringBuilder
            r14.<init>()
            r17 = r7
            java.lang.String r7 = r1.d
            r14.append(r7)
            r14.append(r0)
            java.lang.String r0 = r14.toString()
        L_0x02b6:
            com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r7.b((java.lang.String) r0, (java.lang.String) r6)
            boolean r7 = r0.isEmpty()
            if (r7 != 0) goto L_0x0358
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r7 = "a[data-server]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r7)
            java.util.Iterator r7 = r0.iterator()
        L_0x02d2:
            boolean r0 = r7.hasNext()
            if (r0 == 0) goto L_0x0358
            java.lang.Object r0 = r7.next()     // Catch:{ all -> 0x0342 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ all -> 0x0342 }
            java.lang.String r14 = "data-server"
            java.lang.String r0 = r0.b((java.lang.String) r14)     // Catch:{ all -> 0x0342 }
            java.lang.String r14 = "src=['\"]([^'\"]+)['\"]"
            r18 = r7
            r7 = 1
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r14, (int) r7)     // Catch:{ all -> 0x0340 }
            boolean r7 = r0.startsWith(r4)     // Catch:{ all -> 0x0340 }
            if (r7 == 0) goto L_0x0303
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0340 }
            r7.<init>()     // Catch:{ all -> 0x0340 }
            r7.append(r11)     // Catch:{ all -> 0x0340 }
            r7.append(r0)     // Catch:{ all -> 0x0340 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x0340 }
            goto L_0x0330
        L_0x0303:
            boolean r7 = r0.startsWith(r3)     // Catch:{ all -> 0x0340 }
            if (r7 == 0) goto L_0x0319
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0340 }
            r7.<init>()     // Catch:{ all -> 0x0340 }
            r7.append(r5)     // Catch:{ all -> 0x0340 }
            r7.append(r0)     // Catch:{ all -> 0x0340 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x0340 }
            goto L_0x0330
        L_0x0319:
            boolean r7 = r0.startsWith(r10)     // Catch:{ all -> 0x0340 }
            if (r7 == 0) goto L_0x0330
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x0340 }
            r7.<init>()     // Catch:{ all -> 0x0340 }
            java.lang.String r14 = r1.d     // Catch:{ all -> 0x0340 }
            r7.append(r14)     // Catch:{ all -> 0x0340 }
            r7.append(r0)     // Catch:{ all -> 0x0340 }
            java.lang.String r0 = r7.toString()     // Catch:{ all -> 0x0340 }
        L_0x0330:
            boolean r7 = r0.isEmpty()     // Catch:{ all -> 0x0340 }
            if (r7 != 0) goto L_0x0352
            r7 = 1
            boolean[] r14 = new boolean[r7]     // Catch:{ all -> 0x0340 }
            r7 = 0
            r14[r7] = r12     // Catch:{ all -> 0x0340 }
            r1.a(r2, r0, r15, r14)     // Catch:{ all -> 0x0340 }
            goto L_0x0352
        L_0x0340:
            r0 = move-exception
            goto L_0x0345
        L_0x0342:
            r0 = move-exception
            r18 = r7
        L_0x0345:
            r7 = 0
            boolean[] r14 = new boolean[r7]     // Catch:{ all -> 0x034c }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r14)     // Catch:{ all -> 0x034c }
            goto L_0x0352
        L_0x034c:
            r0 = move-exception
            boolean[] r14 = new boolean[r7]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r14)
        L_0x0352:
            r7 = r18
            goto L_0x02d2
        L_0x0356:
            r17 = r7
        L_0x0358:
            java.lang.String r0 = "a#main-down[href]"
            org.jsoup.nodes.Element r0 = r9.h(r0)
            if (r0 == 0) goto L_0x04f6
            java.lang.String r7 = "href"
            java.lang.String r0 = r0.b((java.lang.String) r7)
            boolean r9 = r0.startsWith(r4)
            if (r9 == 0) goto L_0x037c
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r11)
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            goto L_0x03a9
        L_0x037c:
            boolean r9 = r0.startsWith(r3)
            if (r9 == 0) goto L_0x0392
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r5)
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            goto L_0x03a9
        L_0x0392:
            boolean r9 = r0.startsWith(r10)
            if (r9 == 0) goto L_0x03a9
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r14 = r1.d
            r9.append(r14)
            r9.append(r0)
            java.lang.String r0 = r9.toString()
        L_0x03a9:
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r9.b((java.lang.String) r0, (java.lang.String) r6)
            org.jsoup.nodes.Document r6 = org.jsoup.Jsoup.b(r0)
            java.lang.String r0 = "div[class*=\"links_\"]"
            org.jsoup.select.Elements r0 = r6.g((java.lang.String) r0)
            java.util.Iterator r9 = r0.iterator()
        L_0x03bf:
            boolean r0 = r9.hasNext()
            if (r0 == 0) goto L_0x047d
            java.lang.Object r0 = r9.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r14 = r0.G()     // Catch:{ all -> 0x046c }
            java.lang.String r0 = r0.G()     // Catch:{ all -> 0x046c }
            r18 = r8
            java.lang.String r8 = "href=['\"]([^'\"]+)['\"]"
            r19 = r9
            r9 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b(r0, r8, r9)     // Catch:{ all -> 0x0468 }
            r8 = 0
            java.lang.Object r0 = r0.get(r8)     // Catch:{ all -> 0x0466 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x0468 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0468 }
        L_0x03e9:
            boolean r8 = r0.hasNext()     // Catch:{ all -> 0x0468 }
            if (r8 == 0) goto L_0x0477
            java.lang.Object r8 = r0.next()     // Catch:{ all -> 0x0468 }
            java.lang.String r8 = (java.lang.String) r8     // Catch:{ all -> 0x0468 }
            boolean r9 = r8.startsWith(r4)     // Catch:{ all -> 0x0468 }
            if (r9 == 0) goto L_0x040d
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r9.<init>()     // Catch:{ all -> 0x0468 }
            r9.append(r11)     // Catch:{ all -> 0x0468 }
            r9.append(r8)     // Catch:{ all -> 0x0468 }
            java.lang.String r8 = r9.toString()     // Catch:{ all -> 0x0468 }
        L_0x040a:
            r20 = r0
            goto L_0x043c
        L_0x040d:
            boolean r9 = r8.startsWith(r3)     // Catch:{ all -> 0x0468 }
            if (r9 == 0) goto L_0x0423
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r9.<init>()     // Catch:{ all -> 0x0468 }
            r9.append(r5)     // Catch:{ all -> 0x0468 }
            r9.append(r8)     // Catch:{ all -> 0x0468 }
            java.lang.String r8 = r9.toString()     // Catch:{ all -> 0x0468 }
            goto L_0x040a
        L_0x0423:
            boolean r9 = r8.startsWith(r10)     // Catch:{ all -> 0x0468 }
            if (r9 == 0) goto L_0x040a
            java.lang.StringBuilder r9 = new java.lang.StringBuilder     // Catch:{ all -> 0x0468 }
            r9.<init>()     // Catch:{ all -> 0x0468 }
            r20 = r0
            java.lang.String r0 = r1.d     // Catch:{ all -> 0x0468 }
            r9.append(r0)     // Catch:{ all -> 0x0468 }
            r9.append(r8)     // Catch:{ all -> 0x0468 }
            java.lang.String r8 = r9.toString()     // Catch:{ all -> 0x0468 }
        L_0x043c:
            java.lang.String r0 = "720"
            boolean r0 = r14.contains(r0)     // Catch:{ all -> 0x0468 }
            if (r0 == 0) goto L_0x044a
            java.lang.String r0 = "720p"
        L_0x0446:
            r21 = r14
        L_0x0448:
            r9 = 1
            goto L_0x0459
        L_0x044a:
            java.lang.String r0 = "1080"
            boolean r0 = r14.contains(r0)     // Catch:{ all -> 0x0468 }
            if (r0 == 0) goto L_0x0455
            java.lang.String r0 = "1080p"
            goto L_0x0446
        L_0x0455:
            r21 = r14
            r0 = r15
            goto L_0x0448
        L_0x0459:
            boolean[] r14 = new boolean[r9]     // Catch:{ all -> 0x0468 }
            r9 = 0
            r14[r9] = r12     // Catch:{ all -> 0x0468 }
            r1.a(r2, r8, r0, r14)     // Catch:{ all -> 0x0468 }
            r0 = r20
            r14 = r21
            goto L_0x03e9
        L_0x0466:
            r0 = move-exception
            goto L_0x0472
        L_0x0468:
            r0 = move-exception
            goto L_0x0471
        L_0x046a:
            r0 = move-exception
            goto L_0x046f
        L_0x046c:
            r0 = move-exception
            r18 = r8
        L_0x046f:
            r19 = r9
        L_0x0471:
            r8 = 0
        L_0x0472:
            boolean[] r9 = new boolean[r8]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r9)
        L_0x0477:
            r8 = r18
            r9 = r19
            goto L_0x03bf
        L_0x047d:
            r18 = r8
            java.lang.String r0 = "a.click-link[target=\"_blank\"]"
            org.jsoup.select.Elements r0 = r6.g((java.lang.String) r0)
            java.util.Iterator r0 = r0.iterator()
        L_0x0489:
            boolean r6 = r0.hasNext()
            if (r6 == 0) goto L_0x04f8
            java.lang.Object r6 = r0.next()
            org.jsoup.nodes.Element r6 = (org.jsoup.nodes.Element) r6
            java.lang.String r6 = r6.b((java.lang.String) r7)
            java.lang.String r6 = r6.toString()
            boolean r8 = r6.startsWith(r4)
            if (r8 == 0) goto L_0x04b3
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r11)
            r8.append(r6)
            java.lang.String r6 = r8.toString()
            goto L_0x04e0
        L_0x04b3:
            boolean r8 = r6.startsWith(r3)
            if (r8 == 0) goto L_0x04c9
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r5)
            r8.append(r6)
            java.lang.String r6 = r8.toString()
            goto L_0x04e0
        L_0x04c9:
            boolean r8 = r6.startsWith(r10)
            if (r8 == 0) goto L_0x04e0
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = r1.d
            r8.append(r9)
            r8.append(r6)
            java.lang.String r6 = r8.toString()
        L_0x04e0:
            boolean r8 = r13.contains(r6)
            if (r8 != 0) goto L_0x04f3
            r13.add(r6)
            r8 = 1
            boolean[] r9 = new boolean[r8]
            r14 = 0
            r9[r14] = r12
            r1.a(r2, r6, r15, r9)
            goto L_0x0489
        L_0x04f3:
            r8 = 1
            r14 = 0
            goto L_0x0489
        L_0x04f6:
            r18 = r8
        L_0x04f8:
            r8 = 1
            r14 = 0
            r3 = r23
            r4 = r16
            r7 = r17
            r8 = r18
            r5 = 0
            r11 = 1
            goto L_0x0178
        L_0x0506:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Filmxy.a(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public String c() {
        return "https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master/provider/Fxyposts.json";
    }
}
