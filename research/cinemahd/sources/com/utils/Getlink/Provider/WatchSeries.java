package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class WatchSeries extends BaseProvider {
    private static final String[] c = Utils.getProvider(28).split(",");
    private static String d = "";

    public String a() {
        return "WatchSeries";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:20:0x00cd, code lost:
        d = r14;
     */
    /* JADX WARNING: Removed duplicated region for block: B:105:0x02f0 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:108:0x031e  */
    /* JADX WARNING: Removed duplicated region for block: B:109:0x0330  */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0346  */
    /* JADX WARNING: Removed duplicated region for block: B:118:0x0362  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.movie.data.model.MovieInfo r21, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r22) {
        /*
            r20 = this;
            r1 = r21
            java.lang.String r0 = r21.getName()
            java.lang.String r2 = r21.getName()
            java.lang.String r3 = "The Daily Show with Trevor Noah"
            boolean r3 = r2.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x0015
            java.lang.String r2 = "The Daily Show"
            goto L_0x001f
        L_0x0015:
            java.lang.String r3 = "Will & Grace"
            boolean r3 = r2.equalsIgnoreCase(r3)
            if (r3 == 0) goto L_0x001f
            java.lang.String r2 = "Will "
        L_0x001f:
            java.lang.String[] r3 = c
            int r4 = r3.length
            java.lang.String r5 = ""
            r6 = 0
            r8 = r5
            r7 = 0
            r9 = 0
        L_0x0028:
            java.lang.String r10 = "a[href]"
            java.lang.String r11 = "href"
            java.lang.String r12 = "/"
            if (r7 >= r4) goto L_0x0220
            r14 = r3[r7]
            com.original.tase.helper.http.HttpHelper r15 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r14)
            r16 = r3
            java.lang.String r3 = "/search/"
            r13.append(r3)
            boolean[] r3 = new boolean[r6]
            java.lang.String r3 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r3)
            r13.append(r3)
            java.lang.String r3 = r13.toString()
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r14)
            r13.append(r12)
            java.lang.String r13 = r13.toString()
            java.lang.String r3 = r15.b((java.lang.String) r3, (java.lang.String) r13)
            org.jsoup.nodes.Document r3 = org.jsoup.Jsoup.b(r3)
            java.lang.String r13 = "div.table.infoside"
            org.jsoup.select.Elements r3 = r3.g((java.lang.String) r13)
            java.lang.String r13 = "a[itemprop]"
            org.jsoup.select.Elements r3 = r3.b(r13)
            java.util.Iterator r3 = r3.iterator()
        L_0x0079:
            boolean r13 = r3.hasNext()
            if (r13 == 0) goto L_0x00d0
            java.lang.Object r13 = r3.next()
            org.jsoup.nodes.Element r13 = (org.jsoup.nodes.Element) r13
            java.lang.String r15 = r13.b((java.lang.String) r11)
            java.lang.String r6 = "title"
            java.lang.String r6 = r13.b((java.lang.String) r6)
            boolean r17 = r6.isEmpty()
            if (r17 == 0) goto L_0x009f
            java.lang.String r6 = "h2"
            org.jsoup.select.Elements r6 = r13.g((java.lang.String) r6)
            java.lang.String r6 = r6.c()
        L_0x009f:
            boolean r13 = r6.contains(r2)
            if (r13 != 0) goto L_0x00cd
            java.lang.StringBuilder r13 = new java.lang.StringBuilder
            r13.<init>()
            r13.append(r2)
            r17 = r3
            java.lang.String r3 = " ("
            r13.append(r3)
            java.lang.String r3 = r1.year
            r13.append(r3)
            java.lang.String r3 = ")"
            r13.append(r3)
            java.lang.String r3 = r13.toString()
            boolean r3 = r6.contains(r3)
            if (r3 == 0) goto L_0x00c9
            goto L_0x00cd
        L_0x00c9:
            r3 = r17
            r6 = 0
            goto L_0x0079
        L_0x00cd:
            d = r14
            goto L_0x00d1
        L_0x00d0:
            r15 = r8
        L_0x00d1:
            boolean r3 = r15.isEmpty()
            if (r3 == 0) goto L_0x020a
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r14)
            java.lang.String r8 = "/suggest.php?ajax=1&s="
            r6.append(r8)
            r17 = r4
            r13 = 0
            boolean[] r4 = new boolean[r13]
            java.lang.String r4 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r4)
            r6.append(r4)
            java.lang.String r4 = "&type=TVShows"
            r6.append(r4)
            java.lang.String r6 = r6.toString()
            r18 = r9
            r13 = 1
            java.util.Map[] r9 = new java.util.Map[r13]
            java.util.HashMap r13 = com.original.Constants.a()
            r19 = r15
            r15 = 0
            r9[r15] = r13
            java.lang.String r3 = r3.a((java.lang.String) r6, (java.lang.String) r14, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            boolean r6 = r3.isEmpty()
            if (r6 == 0) goto L_0x0175
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            r6.append(r14)
            r6.append(r8)
            boolean[] r9 = new boolean[r15]
            java.lang.String r9 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r9)
            r6.append(r9)
            r6.append(r4)
            java.lang.String r4 = r6.toString()
            r6 = 1
            java.util.Map[] r9 = new java.util.Map[r6]
            java.util.HashMap r6 = com.original.Constants.a()
            r9[r15] = r6
            java.lang.String r3 = r3.a((java.lang.String) r4, (java.lang.String) r14, (java.util.Map<java.lang.String, java.lang.String>[]) r9)
            boolean r4 = r3.isEmpty()
            if (r4 == 0) goto L_0x0175
            com.original.tase.helper.http.HttpHelper r3 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r14)
            r4.append(r8)
            boolean[] r6 = new boolean[r15]
            java.lang.String r6 = com.original.tase.utils.Utils.a((java.lang.String) r2, (boolean[]) r6)
            r4.append(r6)
            java.lang.String r6 = ".&type=TVShows"
            r4.append(r6)
            java.lang.String r4 = r4.toString()
            r6 = 1
            java.util.Map[] r8 = new java.util.Map[r6]
            java.util.HashMap r6 = com.original.Constants.a()
            r8[r15] = r6
            java.lang.String r3 = r3.a((java.lang.String) r4, (java.lang.String) r14, (java.util.Map<java.lang.String, java.lang.String>[]) r8)
        L_0x0175:
            org.jsoup.nodes.Document r3 = org.jsoup.Jsoup.b(r3)
            org.jsoup.select.Elements r3 = r3.g((java.lang.String) r10)
            java.util.Iterator r3 = r3.iterator()
        L_0x0181:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x0207
            java.lang.Object r4 = r3.next()
            org.jsoup.nodes.Element r4 = (org.jsoup.nodes.Element) r4
            java.lang.String r6 = r4.b((java.lang.String) r11)
            java.lang.String r4 = r4.toString()
            java.lang.String r8 = "</?[^>]*>"
            java.lang.String r4 = r4.replaceAll(r8, r5)
            java.lang.String r8 = "\\((\\d{4})\\)$"
            r9 = 1
            java.lang.String r8 = com.original.tase.utils.Regex.a((java.lang.String) r6, (java.lang.String) r8, (int) r9)
            java.lang.String r9 = com.original.tase.helper.TitleHelper.f(r0)
            java.lang.String r4 = com.original.tase.helper.TitleHelper.f(r4)
            boolean r4 = r9.equals(r4)
            if (r4 == 0) goto L_0x0181
            java.lang.String r4 = r8.trim()
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x01e0
            java.lang.String r4 = r8.trim()
            boolean r4 = com.original.tase.utils.Utils.b(r4)
            if (r4 == 0) goto L_0x01e0
            java.lang.Integer r4 = r21.getYear()
            int r4 = r4.intValue()
            if (r4 <= 0) goto L_0x01e0
            java.lang.String r4 = r8.trim()
            int r4 = java.lang.Integer.parseInt(r4)
            java.lang.Integer r9 = r21.getYear()
            int r9 = r9.intValue()
            if (r4 != r9) goto L_0x0181
        L_0x01e0:
            d = r14
            java.lang.String r4 = r8.trim()
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x01f9
            java.lang.String r4 = r8.trim()
            boolean r4 = com.original.tase.utils.Utils.b(r4)
            if (r4 != 0) goto L_0x01f7
            goto L_0x01f9
        L_0x01f7:
            r4 = 0
            goto L_0x01fa
        L_0x01f9:
            r4 = 1
        L_0x01fa:
            java.lang.String r8 = d
            if (r8 == 0) goto L_0x0201
            r9 = r4
            r8 = r6
            goto L_0x0212
        L_0x0201:
            r18 = r4
            r19 = r6
            goto L_0x0181
        L_0x0207:
            r9 = r18
            goto L_0x0210
        L_0x020a:
            r17 = r4
            r18 = r9
            r19 = r15
        L_0x0210:
            r8 = r19
        L_0x0212:
            java.lang.String r3 = d
            if (r3 == 0) goto L_0x0217
            goto L_0x0220
        L_0x0217:
            int r7 = r7 + 1
            r3 = r16
            r4 = r17
            r6 = 0
            goto L_0x0028
        L_0x0220:
            r18 = r9
            java.lang.String r0 = d
            if (r0 == 0) goto L_0x0411
            boolean r0 = r8.isEmpty()
            if (r0 == 0) goto L_0x022e
            goto L_0x0411
        L_0x022e:
            java.lang.String r0 = "//"
            boolean r0 = r8.startsWith(r0)
            java.lang.String r2 = "http"
            if (r0 == 0) goto L_0x024a
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = "http:"
            r0.append(r3)
            r0.append(r8)
            java.lang.String r8 = r0.toString()
            goto L_0x027c
        L_0x024a:
            boolean r0 = r8.startsWith(r12)
            if (r0 == 0) goto L_0x0262
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = d
            r0.append(r3)
            r0.append(r8)
            java.lang.String r8 = r0.toString()
            goto L_0x027c
        L_0x0262:
            boolean r0 = r8.startsWith(r2)
            if (r0 != 0) goto L_0x027c
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r3 = d
            r0.append(r3)
            r0.append(r12)
            r0.append(r8)
            java.lang.String r8 = r0.toString()
        L_0x027c:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = d
            java.lang.String r3 = r0.b((java.lang.String) r8, (java.lang.String) r3)
            if (r18 == 0) goto L_0x02f1
            java.lang.String r0 = "<dd>\\s*(\\d{4})\\s*<"
            r4 = 34
            r5 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r3, (java.lang.String) r0, (int) r5, (int) r4)     // Catch:{ Exception -> 0x02e6 }
            r4 = 0
            java.lang.Object r0 = r0.get(r4)     // Catch:{ Exception -> 0x02e4 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x02e6 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x02e6 }
        L_0x029c:
            boolean r4 = r0.hasNext()     // Catch:{ Exception -> 0x02e6 }
            if (r4 == 0) goto L_0x02e2
            java.lang.Object r4 = r0.next()     // Catch:{ Exception -> 0x02e6 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x02e6 }
            boolean r5 = r4.isEmpty()     // Catch:{ Exception -> 0x02e6 }
            if (r5 != 0) goto L_0x029c
            boolean r5 = com.original.tase.utils.Utils.b(r4)     // Catch:{ Exception -> 0x02e6 }
            if (r5 == 0) goto L_0x029c
            int r4 = java.lang.Integer.parseInt(r4)     // Catch:{ Exception -> 0x02e6 }
            r5 = 1850(0x73a, float:2.592E-42)
            if (r4 < r5) goto L_0x029c
            r5 = 2030(0x7ee, float:2.845E-42)
            if (r4 > r5) goto L_0x029c
            java.lang.Integer r5 = r21.getYear()     // Catch:{ Exception -> 0x02e6 }
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x02e6 }
            if (r4 == r5) goto L_0x02ed
            java.lang.Integer r5 = r21.getYear()     // Catch:{ Exception -> 0x02e6 }
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x02e6 }
            r6 = 1
            int r5 = r5 + r6
            if (r4 == r5) goto L_0x02ed
            java.lang.Integer r5 = r21.getYear()     // Catch:{ Exception -> 0x02e6 }
            int r5 = r5.intValue()     // Catch:{ Exception -> 0x02e6 }
            int r5 = r5 - r6
            if (r4 != r5) goto L_0x029c
            goto L_0x02ed
        L_0x02e2:
            r13 = 0
            goto L_0x02ee
        L_0x02e4:
            r0 = move-exception
            goto L_0x02e8
        L_0x02e6:
            r0 = move-exception
            r4 = 0
        L_0x02e8:
            boolean[] r5 = new boolean[r4]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r5)
        L_0x02ed:
            r13 = 1
        L_0x02ee:
            if (r13 != 0) goto L_0x02f1
            return
        L_0x02f1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r4 = "href=\"([^\"]*s"
            r0.append(r4)
            java.lang.String r4 = r1.session
            r0.append(r4)
            java.lang.String r4 = "_e"
            r0.append(r4)
            java.lang.String r1 = r1.eps
            r0.append(r1)
            java.lang.String r1 = "(?!\\d)[^\"]*)"
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 1
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r0, (int) r1, (boolean) r1)
            boolean r1 = r0.startsWith(r12)
            if (r1 == 0) goto L_0x0330
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r3 = d
            r1.append(r3)
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            goto L_0x0337
        L_0x0330:
            boolean r1 = r0.isEmpty()
            if (r1 == 0) goto L_0x0337
            return
        L_0x0337:
            r1 = r0
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r0.b((java.lang.String) r1, (java.lang.String) r8)
            boolean r3 = r0.isEmpty()
            if (r3 == 0) goto L_0x034e
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r0.b((java.lang.String) r1, (java.lang.String) r8)
        L_0x034e:
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r3 = "table.W"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r3)
            java.util.Iterator r3 = r0.iterator()
        L_0x035c:
            boolean r0 = r3.hasNext()
            if (r0 == 0) goto L_0x040e
            java.lang.Object r0 = r3.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r4 = "tr"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r4)
            java.util.Iterator r4 = r0.iterator()
        L_0x0372:
            boolean r0 = r4.hasNext()
            if (r0 == 0) goto L_0x0408
            java.lang.Object r0 = r4.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            boolean r5 = r22.isDisposed()     // Catch:{ Exception -> 0x03fb }
            if (r5 == 0) goto L_0x0385
            return
        L_0x0385:
            org.jsoup.nodes.Element r0 = r0.h(r10)     // Catch:{ Exception -> 0x03fb }
            if (r0 == 0) goto L_0x03f4
            java.lang.String r0 = r0.b((java.lang.String) r11)     // Catch:{ Exception -> 0x03fb }
            boolean r5 = r0.startsWith(r12)     // Catch:{ Exception -> 0x03fb }
            if (r5 != 0) goto L_0x039b
            boolean r5 = r0.startsWith(r2)     // Catch:{ Exception -> 0x03fb }
            if (r5 != 0) goto L_0x03ac
        L_0x039b:
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x03fb }
            r5.<init>()     // Catch:{ Exception -> 0x03fb }
            java.lang.String r6 = d     // Catch:{ Exception -> 0x03fb }
            r5.append(r6)     // Catch:{ Exception -> 0x03fb }
            r5.append(r0)     // Catch:{ Exception -> 0x03fb }
            java.lang.String r0 = r5.toString()     // Catch:{ Exception -> 0x03fb }
        L_0x03ac:
            com.original.tase.helper.http.HttpHelper r5 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x03fb }
            java.lang.String r0 = r5.b((java.lang.String) r0, (java.lang.String) r1)     // Catch:{ Exception -> 0x03fb }
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)     // Catch:{ Exception -> 0x03fb }
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r10)     // Catch:{ Exception -> 0x03fb }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x03fb }
        L_0x03c0:
            boolean r5 = r0.hasNext()     // Catch:{ Exception -> 0x03fb }
            if (r5 == 0) goto L_0x03f4
            java.lang.Object r5 = r0.next()     // Catch:{ Exception -> 0x03fb }
            org.jsoup.nodes.Element r5 = (org.jsoup.nodes.Element) r5     // Catch:{ Exception -> 0x03fb }
            java.lang.String r6 = r5.toString()     // Catch:{ Exception -> 0x03fb }
            java.lang.String r6 = r6.toLowerCase()     // Catch:{ Exception -> 0x03fb }
            java.lang.String r7 = "click here to play"
            boolean r6 = r6.contains(r7)     // Catch:{ Exception -> 0x03fb }
            if (r6 == 0) goto L_0x03ef
            java.lang.String r5 = r5.b((java.lang.String) r11)     // Catch:{ Exception -> 0x03fb }
            java.lang.String r6 = "HQ"
            r7 = 0
            boolean[] r8 = new boolean[r7]     // Catch:{ Exception -> 0x03fb }
            r7 = r20
            r9 = r22
            r7.a(r9, r5, r6, r8)     // Catch:{ Exception -> 0x03ed }
            goto L_0x03c0
        L_0x03ed:
            r0 = move-exception
            goto L_0x0400
        L_0x03ef:
            r7 = r20
            r9 = r22
            goto L_0x03c0
        L_0x03f4:
            r7 = r20
            r9 = r22
            r5 = 0
            goto L_0x0372
        L_0x03fb:
            r0 = move-exception
            r7 = r20
            r9 = r22
        L_0x0400:
            r5 = 0
            boolean[] r6 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r6)
            goto L_0x0372
        L_0x0408:
            r7 = r20
            r9 = r22
            goto L_0x035c
        L_0x040e:
            r7 = r20
            return
        L_0x0411:
            r7 = r20
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.WatchSeries.b(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }
}
