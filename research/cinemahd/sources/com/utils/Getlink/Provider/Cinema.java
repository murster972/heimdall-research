package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Cinema extends BaseProvider {
    static {
        new String[]{"s3.svdl.ir", "dl.yoozdl", "dl1.yoozdl", "dl2.yoozdl", "dl3.yoozdl", "s1.0music", "178.216.250.167", "dl2.upload08", "dl3.upload08", "avadl.uploadt", "dl5.dlb3d", "dl4.dlb3d", "dl3.dlb3d", "dl2.dlb3d", "dl.dlb3d", "geryon.feralhosting", "ns502618.ip-192-99-8", "dl.muvie", "portal.ekniazi", "seedbox37", "5.9.40.180", "163.172.6.218", "s1.tinydl.info", "dl2.heyserver.in"};
    }

    public Cinema() {
        Utils.getProvider(42);
        new HashMap();
    }

    public String a() {
        return "Cinema";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (!Utils.b) {
            a(observableEmitter, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (!Utils.b) {
            a(observableEmitter, movieInfo);
        }
    }

    public ArrayList j(String str) {
        ArrayList arrayList = new ArrayList();
        String l = Utils.l();
        for (String str2 : new String[]{"&start=0"}) {
            String a2 = com.original.tase.utils.Utils.a(l + "/search?q=intitle:\"index+of\"+" + str, new boolean[0]);
            Iterator it2 = Jsoup.b(HttpHelper.e().a(a2 + "##forceNoCache##", (Map<String, String>[]) new Map[0])).g("div[class=r]").iterator();
            if (!it2.hasNext()) {
                break;
            }
            while (it2.hasNext()) {
                try {
                    String b = ((Element) it2.next()).h("a").b("href");
                    if (!b.isEmpty() && !b.contains("google.com") && !b.contains("imdb.com") && !b.contains("youtube.com") && !arrayList.contains(b)) {
                        arrayList.add(b);
                    }
                } catch (Throwable unused) {
                }
            }
        }
        return arrayList;
    }

    public ArrayList a(ArrayList arrayList, String str) {
        HttpHelper.e().a("https://www.bing.com", (Map<String, String>[]) new Map[0]);
        Iterator it2 = Jsoup.b(HttpHelper.e().b("https://www.bing.com/search?q=intitle%3Aindex+of+" + str, "https://www.bing.com")).g("h2").iterator();
        while (it2.hasNext()) {
            try {
                String b = ((Element) it2.next()).h("a").b("href");
                if (!b.contains("bing.com") && !b.contains("imdb.com") && !b.contains("youtube.com") && !b.isEmpty() && !arrayList.contains(b)) {
                    arrayList.add(b);
                }
            } catch (Throwable unused) {
            }
        }
        return arrayList;
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo) {
        String str;
        boolean z = true;
        if (movieInfo.getType().intValue() != 1) {
            z = false;
        }
        if (z) {
            str = " " + movieInfo.year;
        } else {
            str = " S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.eps));
        }
        String a2 = TitleHelper.a(movieInfo.name + str, "+");
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(j(a2));
        a(arrayList, a2);
        a(observableEmitter, arrayList, movieInfo, str);
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, ArrayList arrayList, MovieInfo movieInfo, String str) {
        boolean z;
        MovieInfo movieInfo2 = movieInfo;
        String str2 = str;
        int i = 0;
        boolean z2 = true;
        boolean z3 = movieInfo.getType().intValue() == 1;
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            Object next = it2.next();
            if (!observableEmitter.isDisposed()) {
                String str3 = (String) next;
                String a2 = HttpHelper.e().a(str3, (Map<String, String>[]) new Map[i]);
                String a3 = TitleHelper.a(movieInfo2.name + str2, ".");
                Iterator it3 = Regex.b(a2, "['\"]([^'\"]+(mka|mkv|mp4|avi))['\"]", z2 ? 1 : 0, z2).get(i).iterator();
                if (it3.hasNext()) {
                    Iterator it4 = Regex.b(a2, "href=['\"]([^'\"]*" + a3 + "[^'\"]*)['\"]", (int) z2, z2).get(i).iterator();
                    if (!it4.hasNext()) {
                        it4 = Regex.b(a2, "href=['\"]([^'\"]*" + a3.toLowerCase() + "[^'\"]*)['\"]", (int) z2, z2).get(0).iterator();
                    }
                    if (!z3) {
                        if (!it4.hasNext()) {
                            a3 = TitleHelper.a(movieInfo2.name + " " + movieInfo2.year + str2, ".");
                            StringBuilder sb = new StringBuilder();
                            sb.append("href=['\"]([^'\"]*");
                            sb.append(a3);
                            sb.append("[^'\"]*)['\"]");
                            it4 = Regex.b(a2, sb.toString(), 1, true).get(0).iterator();
                        }
                        if (!it4.hasNext()) {
                            a3 = TitleHelper.a(movieInfo2.name + " " + movieInfo2.year + str2, ".");
                            StringBuilder sb2 = new StringBuilder();
                            sb2.append("href=['\"]([^'\"]*");
                            sb2.append(a3.toLowerCase());
                            sb2.append("[^'\"]*)['\"]");
                            it4 = Regex.b(a2, sb2.toString(), 1, true).get(0).iterator();
                        }
                    }
                    if (!it4.hasNext()) {
                        z = true;
                    } else {
                        it3 = it4;
                        z = false;
                    }
                    while (it3.hasNext()) {
                        String str4 = (String) it3.next();
                        if (str4.isEmpty() || str4.toLowerCase().contains("trailer") || !TitleHelper.f(str4).contains(TitleHelper.f(TitleHelper.e(a3)))) {
                            ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                        } else {
                            if (z) {
                                str4 = "http://" + str4;
                            } else if (!str4.startsWith(UriUtil.HTTP_SCHEME)) {
                                str4 = str3 + str4;
                            }
                            String a4 = Regex.a(str4, "(?<=\\//)(.*?)(?=\\.)", 1);
                            if (!a4.isEmpty()) {
                                a4 = a4.substring(0, 1).toUpperCase() + a4.substring(1);
                            }
                            if (a4.isEmpty()) {
                                a4 = a();
                            }
                            DirectoryIndexHelper.ParsedLinkModel a5 = directoryIndexHelper.a(str4);
                            String str5 = "HQ";
                            if (a5 != null) {
                                String c = a5.c();
                                if (!c.equalsIgnoreCase(str5)) {
                                    str5 = c;
                                }
                                a4 = f(a5.b());
                            }
                            MediaSource mediaSource = new MediaSource(a4, "CDN", false);
                            mediaSource.setStreamLink(str4);
                            mediaSource.setPlayHeader(hashMap);
                            mediaSource.setQuality(str5);
                            observableEmitter.onNext(mediaSource);
                        }
                        MovieInfo movieInfo3 = movieInfo;
                    }
                    ObservableEmitter<? super MediaSource> observableEmitter3 = observableEmitter;
                    movieInfo2 = movieInfo;
                    i = 0;
                    z2 = true;
                }
            } else {
                return;
            }
        }
    }
}
