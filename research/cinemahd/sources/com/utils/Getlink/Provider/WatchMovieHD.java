package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class WatchMovieHD extends BaseProvider {
    private String c = Utils.getProvider(43);
    private HashMap d = new HashMap();

    public String a() {
        return "WatchMovieHD";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        Iterator it2;
        boolean z;
        boolean z2;
        boolean z3 = movieInfo.getType().intValue() == 1;
        this.d.put("referer", this.c);
        this.d.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        String a2 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{this.d});
        Document b = Jsoup.b(a2);
        String str2 = "HD";
        if (z3) {
            String a3 = Regex.a(a2, "<li\\s*class=\"quality\">(.*)<\\/li>", 1);
            if (a3.isEmpty()) {
                z = false;
            } else {
                z = c(a3);
                str2 = a3;
            }
            it2 = b.g("ul.streams-list.clearfix").b("li").iterator();
        } else {
            Iterator it3 = b.g("details.clearfix").iterator();
            while (true) {
                if (!it3.hasNext()) {
                    break;
                }
                Element element = (Element) it3.next();
                String a4 = Regex.a(element.toString(), "<summary>(.*)<\\/summary>", 1);
                if (a4.equalsIgnoreCase("Season " + movieInfo.session)) {
                    Iterator it4 = element.g("li.clearfix").iterator();
                    while (true) {
                        if (!it4.hasNext()) {
                            break;
                        }
                        Element element2 = (Element) it4.next();
                        if (movieInfo.eps.equals(element2.h("div.episode-number").G())) {
                            it3 = element2.g("ul").b("li").iterator();
                            z2 = true;
                            break;
                        }
                    }
                }
            }
            z2 = false;
            if (z2) {
                it2 = it3;
                z = false;
            } else {
                return;
            }
        }
        new ArrayList();
        while (it2.hasNext()) {
            try {
                String b2 = ((Element) it2.next()).h("a").b("href");
                if (b(b2)) {
                    a(observableEmitter, b2, str2, z);
                } else {
                    String a5 = HttpHelper.e().a(b2, false, str);
                    if (a5.isEmpty()) {
                        String a6 = Regex.a(HttpHelper.e().b(b2, str), "redirect\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                        if (!a6.isEmpty()) {
                            a(observableEmitter, a6, str2, z);
                        }
                    } else {
                        a(observableEmitter, a5, str2, z);
                    }
                }
            } catch (Throwable th) {
                Logger.a(th, false);
            }
        }
    }

    public String b(MovieInfo movieInfo) {
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        this.d.put("referer", this.c + "/");
        this.d.put("accept", DiskLruCache.VERSION_1);
        this.d.put("upgrade-insecure-requests", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/searching?q=" + TitleHelper.a(movieInfo.name.toLowerCase(), "+"), (Map<String, String>[]) new Map[]{this.d})).g("li.big-grid-item").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            if (h.h("div.title").G().equalsIgnoreCase(movieInfo.name + " (" + movieInfo.year + ")")) {
                return b;
            }
        }
        return "";
    }
}
