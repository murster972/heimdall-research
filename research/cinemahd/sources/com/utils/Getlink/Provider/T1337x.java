package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.movie.data.model.realdebrid.MagnetObject;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class T1337x extends BaseProvider {
    private String c = Utils.getProvider(23);
    private String d = "";

    public String a() {
        return "T1337x";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo);
        }
    }

    public String a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo) {
        String str;
        Iterator it2;
        String str2;
        String str3;
        boolean z;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        int i;
        String str4;
        MovieInfo movieInfo2 = movieInfo;
        String str5 = "1080p";
        if (this.d.isEmpty()) {
            this.d = HttpHelper.e().a("https://raw.githubusercontent.com/TeruSetephen/cinemaapk/master/provider/1337x.to.txt", (Map<String, String>[]) new Map[0]);
        }
        boolean z2 = movieInfo.getType().intValue() == 1;
        if (z2) {
            str = " " + movieInfo2.year;
        } else {
            str = " S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.eps));
        }
        String format = String.format(this.c + this.d, new Object[]{com.original.tase.utils.Utils.a(movieInfo2.name + str, new boolean[0])});
        Iterator it3 = Jsoup.b(HttpHelper.e().b(format, this.c + "/")).g("tbody").b("tr").iterator();
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        HashMap hashMap = new HashMap();
        while (it3.hasNext()) {
            Element h = ((Element) it3.next()).h("a[href*=/torrent]");
            String G = h.G();
            String b = h.b("href");
            if (z2) {
                if (TitleHelper.f(G).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName()) + movieInfo2.year))) {
                    hashMap.put(b, G);
                }
            } else {
                if (TitleHelper.a(G.toLowerCase().replace(movieInfo2.year, ""), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase() + str.toLowerCase(), ""))) {
                    hashMap.put(b, G);
                }
            }
        }
        ArrayList arrayList = new ArrayList();
        Iterator it4 = hashMap.entrySet().iterator();
        String str6 = "HQ";
        while (it4.hasNext()) {
            Map.Entry entry = (Map.Entry) it4.next();
            try {
                String str7 = (String) entry.getKey();
                if (str7.startsWith("/")) {
                    str7 = this.c + str7;
                }
                String b2 = HttpHelper.e().b(str7, this.c + "/");
                String str8 = (String) entry.getValue();
                String[] split = str8.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                int length = split.length;
                str3 = str6;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        str2 = str5;
                        it2 = it4;
                        z = false;
                        break;
                    }
                    try {
                        String lowerCase = split[i2].toLowerCase();
                        it2 = it4;
                        try {
                            if (lowerCase.contains("dvdscr") || lowerCase.contains("camrip") || lowerCase.contains("tsrip") || lowerCase.contains("hdcam") || lowerCase.contains("hdtc") || lowerCase.contains("hdts") || lowerCase.contains("dvdcam") || lowerCase.contains("dvdts") || lowerCase.contains("cam") || lowerCase.contains("telesync")) {
                                break;
                            } else if (lowerCase.contains("ts")) {
                                break;
                            } else {
                                boolean contains = lowerCase.contains(str5);
                                str2 = str5;
                                String str9 = "720p";
                                if (!contains) {
                                    if (!lowerCase.equals("1080")) {
                                        if (!lowerCase.contains(str9)) {
                                            if (!lowerCase.equals("720")) {
                                                if (lowerCase.contains("brrip") || lowerCase.contains("bdrip") || lowerCase.contains("hdrip") || lowerCase.contains("web-dl")) {
                                                    str9 = "HD";
                                                } else {
                                                    i2++;
                                                    it4 = it2;
                                                    str5 = str2;
                                                }
                                            }
                                        }
                                        str3 = str9;
                                        i2++;
                                        it4 = it2;
                                        str5 = str2;
                                    }
                                }
                                str3 = str2;
                                i2++;
                                it4 = it2;
                                str5 = str2;
                            }
                        } catch (Throwable unused) {
                        }
                    } catch (Throwable unused2) {
                        str2 = str5;
                        it2 = it4;
                    }
                }
                str2 = str5;
                z = true;
                String a2 = Regex.a(b2, "href\\s*=\\s*['\"](magnet.*)\\s*onclick.['\"]", 1);
                String a3 = a();
                if (z2) {
                    parsedLinkModel = directoryIndexHelper.a(str8);
                } else {
                    parsedLinkModel = directoryIndexHelper.b(str8);
                }
                if (parsedLinkModel != null) {
                    String c2 = parsedLinkModel.c();
                    if (!c2.equalsIgnoreCase("HQ")) {
                        str3 = c2;
                    }
                    String b3 = parsedLinkModel.b();
                    i = 1;
                    a3 = a(b3, true);
                } else {
                    i = 1;
                }
                String lowerCase2 = Regex.a(a2, "(magnet:\\?xt=urn:btih:[^&.]+)", i).toLowerCase();
                if (z) {
                    str4 = "CAM-" + str3;
                } else {
                    str4 = str3;
                }
                MagnetObject magnetObject = new MagnetObject(a3, lowerCase2, str4, a());
                magnetObject.setFileName(str8);
                arrayList.add(magnetObject);
            } catch (Throwable unused3) {
                str2 = str5;
                it2 = it4;
                str3 = str6;
            }
            str6 = str3;
            it4 = it2;
            str5 = str2;
        }
        if (arrayList.size() > 0) {
            MediaSource mediaSource = new MediaSource(a(), "Torrent", false);
            mediaSource.setTorrent(true);
            mediaSource.setMagnetObjects(arrayList);
            mediaSource.setStreamLink("magnet:1337");
            observableEmitter.onNext(mediaSource);
        }
        return "";
    }
}
