package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.search.SearchHelper;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class ReleaseBB extends BaseProvider {
    private static String c = Utils.getProvider(84);
    private static final String d = c.replace("http://", "").replace("https://", "").replace("/", "");

    public String a() {
        return "ReleaseBB";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo, "-1", "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:119:0x0329, code lost:
        if (r1.toLowerCase().equals(r28.toLowerCase()) == false) goto L_0x032b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:13:0x009e, code lost:
        if (com.original.tase.helper.TitleHelper.f(r9.replaceAll("(?i)(.*)([2-9]0\\d{2}|1[5-9]\\d{2})\\s+(S\\d+\\s*E\\d+)(.*)", "$1$3$4")).startsWith(com.original.tase.helper.TitleHelper.f(r26.getName() + r28.toLowerCase())) != false) goto L_0x00a0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x0115 A[ADDED_TO_REGION, Catch:{ all -> 0x0172, all -> 0x03ea }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r23, java.lang.String r24, java.lang.Object r25, com.movie.data.model.MovieInfo r26, java.lang.String r27, java.lang.String r28, com.original.tase.helper.DirectoryIndexHelper r29) {
        /*
            r22 = this;
            r1 = r26
            r2 = r29
            java.lang.String r3 = "http://"
            java.lang.String r4 = "/"
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            r7 = 0
            com.google.gson.JsonParser r0 = new com.google.gson.JsonParser     // Catch:{ all -> 0x03ea }
            r0.<init>()     // Catch:{ all -> 0x03ea }
            r8 = r24
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r8)     // Catch:{ all -> 0x03ea }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ all -> 0x03ea }
            java.lang.String r8 = "results"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r8)     // Catch:{ all -> 0x03ea }
            com.google.gson.JsonArray r0 = r0.e()     // Catch:{ all -> 0x03ea }
            java.util.Iterator r8 = r0.iterator()     // Catch:{ all -> 0x03ea }
        L_0x0030:
            boolean r0 = r8.hasNext()     // Catch:{ all -> 0x03ea }
            if (r0 == 0) goto L_0x03f1
            java.lang.Object r0 = r8.next()     // Catch:{ all -> 0x03ea }
            com.google.gson.JsonElement r0 = (com.google.gson.JsonElement) r0     // Catch:{ all -> 0x03ea }
            com.google.gson.JsonObject r9 = r0.f()     // Catch:{ all -> 0x03ea }
            java.lang.String r10 = "post_title"
            com.google.gson.JsonElement r9 = r9.a((java.lang.String) r10)     // Catch:{ all -> 0x03ea }
            java.lang.String r9 = r9.i()     // Catch:{ all -> 0x03ea }
            com.google.gson.JsonObject r10 = r0.f()     // Catch:{ all -> 0x03ea }
            java.lang.String r11 = "post_name"
            com.google.gson.JsonElement r10 = r10.a((java.lang.String) r11)     // Catch:{ all -> 0x03ea }
            java.lang.String r10 = r10.i()     // Catch:{ all -> 0x03ea }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ all -> 0x03ea }
            java.lang.String r11 = "domain"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r11)     // Catch:{ all -> 0x03ea }
            java.lang.String r0 = r0.i()     // Catch:{ all -> 0x03ea }
            boolean r11 = r5.contains(r10)     // Catch:{ all -> 0x03ea }
            if (r11 != 0) goto L_0x03e1
            r5.add(r10)     // Catch:{ all -> 0x03ea }
            java.lang.String r12 = ""
            if (r25 == 0) goto L_0x00a2
            java.lang.String r13 = "(?i)(.*)([2-9]0\\d{2}|1[5-9]\\d{2})\\s+(S\\d+\\s*E\\d+)(.*)"
            java.lang.String r14 = "$1$3$4"
            java.lang.String r9 = r9.replaceAll(r13, r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r9 = com.original.tase.helper.TitleHelper.f(r9)     // Catch:{ all -> 0x03ea }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x03ea }
            r13.<init>()     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r26.getName()     // Catch:{ all -> 0x03ea }
            r13.append(r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r28.toLowerCase()     // Catch:{ all -> 0x03ea }
            r13.append(r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x03ea }
            java.lang.String r13 = com.original.tase.helper.TitleHelper.f(r13)     // Catch:{ all -> 0x03ea }
            boolean r9 = r9.startsWith(r13)     // Catch:{ all -> 0x03ea }
            if (r9 == 0) goto L_0x0111
        L_0x00a0:
            r9 = 1
            goto L_0x0112
        L_0x00a2:
            java.lang.String r13 = r9.toLowerCase()     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r1.year     // Catch:{ all -> 0x03ea }
            java.lang.String r13 = r13.replace(r14, r12)     // Catch:{ all -> 0x03ea }
            java.lang.String r13 = com.original.tase.helper.TitleHelper.a(r13, r12)     // Catch:{ all -> 0x03ea }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x03ea }
            r14.<init>()     // Catch:{ all -> 0x03ea }
            java.lang.String r15 = r26.getName()     // Catch:{ all -> 0x03ea }
            java.lang.String r15 = r15.toLowerCase()     // Catch:{ all -> 0x03ea }
            r14.append(r15)     // Catch:{ all -> 0x03ea }
            java.lang.String r15 = r28.toLowerCase()     // Catch:{ all -> 0x03ea }
            r14.append(r15)     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = com.original.tase.helper.TitleHelper.a(r14, r12)     // Catch:{ all -> 0x03ea }
            boolean r13 = r13.startsWith(r14)     // Catch:{ all -> 0x03ea }
            if (r13 == 0) goto L_0x00d6
            goto L_0x00a0
        L_0x00d6:
            java.lang.String r9 = r9.toLowerCase()     // Catch:{ all -> 0x03ea }
            java.lang.String r9 = com.original.tase.helper.TitleHelper.a(r9, r12)     // Catch:{ all -> 0x03ea }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ all -> 0x03ea }
            r13.<init>()     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r26.getName()     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r14.toLowerCase()     // Catch:{ all -> 0x03ea }
            r13.append(r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = "s"
            r13.append(r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r1.session     // Catch:{ all -> 0x03ea }
            int r14 = java.lang.Integer.parseInt(r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = com.original.tase.utils.Utils.a((int) r14)     // Catch:{ all -> 0x03ea }
            r13.append(r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r13 = r13.toString()     // Catch:{ all -> 0x03ea }
            java.lang.String r13 = com.original.tase.helper.TitleHelper.a(r13, r12)     // Catch:{ all -> 0x03ea }
            boolean r9 = r9.startsWith(r13)     // Catch:{ all -> 0x03ea }
            if (r9 == 0) goto L_0x0111
            r9 = 1
            r13 = 1
            goto L_0x0113
        L_0x0111:
            r9 = 0
        L_0x0112:
            r13 = 0
        L_0x0113:
            if (r9 == 0) goto L_0x03e1
            if (r0 == 0) goto L_0x0125
            boolean r9 = r0.isEmpty()     // Catch:{ all -> 0x03ea }
            if (r9 != 0) goto L_0x0125
            java.lang.String r9 = ".unblockit."
            boolean r9 = r0.contains(r9)     // Catch:{ all -> 0x03ea }
            if (r9 == 0) goto L_0x0127
        L_0x0125:
            java.lang.String r0 = "rlsbb.ru"
        L_0x0127:
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ all -> 0x03ea }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x03ea }
            r14.<init>()     // Catch:{ all -> 0x03ea }
            r14.append(r3)     // Catch:{ all -> 0x03ea }
            r14.append(r0)     // Catch:{ all -> 0x03ea }
            r14.append(r4)     // Catch:{ all -> 0x03ea }
            r14.append(r10)     // Catch:{ all -> 0x03ea }
            java.lang.String r0 = r14.toString()     // Catch:{ all -> 0x03ea }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x03ea }
            r14.<init>()     // Catch:{ all -> 0x03ea }
            java.lang.String r15 = "http://search.rlsbb.ru/search/"
            r14.append(r15)     // Catch:{ all -> 0x03ea }
            r15 = r27
            r14.append(r15)     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r14.toString()     // Catch:{ all -> 0x03ea }
            java.util.Map[] r11 = new java.util.Map[r7]     // Catch:{ all -> 0x03ea }
            java.lang.String r0 = r9.b(r0, r14, r11)     // Catch:{ all -> 0x03ea }
            org.jsoup.nodes.Document r9 = org.jsoup.Jsoup.b(r0)     // Catch:{ all -> 0x03ea }
            java.util.ArrayList r11 = new java.util.ArrayList     // Catch:{ all -> 0x03ea }
            r11.<init>()     // Catch:{ all -> 0x03ea }
            java.lang.String r0 = "div.post[id]"
            org.jsoup.nodes.Element r0 = r9.h(r0)     // Catch:{ all -> 0x0172 }
            if (r0 == 0) goto L_0x0178
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0172 }
            r11.add(r0)     // Catch:{ all -> 0x0172 }
            goto L_0x0178
        L_0x0172:
            r0 = move-exception
            boolean[] r14 = new boolean[r7]     // Catch:{ all -> 0x03ea }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r14)     // Catch:{ all -> 0x03ea }
        L_0x0178:
            java.lang.String r0 = "div.messageBox"
            org.jsoup.select.Elements r0 = r9.g((java.lang.String) r0)     // Catch:{ all -> 0x03ea }
            java.util.Iterator r9 = r0.iterator()     // Catch:{ all -> 0x03ea }
        L_0x0182:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x03ea }
            if (r0 == 0) goto L_0x019d
            java.lang.Object r0 = r9.next()     // Catch:{ all -> 0x0196 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ all -> 0x0196 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x0196 }
            r11.add(r0)     // Catch:{ all -> 0x0196 }
            goto L_0x0182
        L_0x0196:
            r0 = move-exception
            boolean[] r14 = new boolean[r7]     // Catch:{ all -> 0x03ea }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r14)     // Catch:{ all -> 0x03ea }
            goto L_0x0182
        L_0x019d:
            java.util.Iterator r9 = r11.iterator()     // Catch:{ all -> 0x03ea }
        L_0x01a1:
            boolean r0 = r9.hasNext()     // Catch:{ all -> 0x03ea }
            if (r0 == 0) goto L_0x03e3
            java.lang.Object r0 = r9.next()     // Catch:{ all -> 0x03ea }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x03ea }
            java.lang.String r11 = "\\/"
            java.lang.String r0 = r0.replace(r11, r4)     // Catch:{ all -> 0x03ea }
            java.lang.String r11 = "\\\""
            java.lang.String r14 = "\""
            java.lang.String r0 = r0.replace(r11, r14)     // Catch:{ all -> 0x03ea }
            java.lang.String r11 = "<a[^<]*href=['\"](.*?)['\"].*?</a>"
            r14 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r11, (int) r14, (boolean) r14)     // Catch:{ all -> 0x03ea }
            java.lang.Object r0 = r0.get(r7)     // Catch:{ all -> 0x03ea }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ all -> 0x03ea }
            java.util.Iterator r11 = r0.iterator()     // Catch:{ all -> 0x03ea }
        L_0x01cc:
            boolean r0 = r11.hasNext()     // Catch:{ all -> 0x03ea }
            if (r0 == 0) goto L_0x03db
            java.lang.Object r0 = r11.next()     // Catch:{ all -> 0x03ea }
            java.lang.String r0 = (java.lang.String) r0     // Catch:{ all -> 0x03ea }
            java.lang.String r14 = r0.replace(r3, r12)     // Catch:{ all -> 0x03c4 }
            java.lang.String r7 = "https://"
            java.lang.String r7 = r14.replace(r7, r12)     // Catch:{ all -> 0x03c4 }
            java.lang.StringBuilder r14 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c4 }
            r14.<init>()     // Catch:{ all -> 0x03c4 }
            java.lang.String r1 = d     // Catch:{ all -> 0x03c4 }
            r14.append(r1)     // Catch:{ all -> 0x03c4 }
            r14.append(r4)     // Catch:{ all -> 0x03c4 }
            java.lang.String r1 = r14.toString()     // Catch:{ all -> 0x03c4 }
            boolean r1 = r7.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c4 }
            r1.<init>()     // Catch:{ all -> 0x03c4 }
            r1.append(r4)     // Catch:{ all -> 0x03c4 }
            java.lang.String r14 = d     // Catch:{ all -> 0x03c4 }
            r1.append(r14)     // Catch:{ all -> 0x03c4 }
            java.lang.String r1 = r1.toString()     // Catch:{ all -> 0x03c4 }
            boolean r1 = r7.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "#"
            boolean r1 = r0.startsWith(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "thepiratebay.org"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "protected.to"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "histats.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "opensubtitles.org"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "facebook.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "twitter.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "amazon.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "youtube.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "youtu.be"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "imgaa.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "imdb.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "tvguide.com"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".7z"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".png"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".rar"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".zip"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".iso"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".part"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".avi"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".flv"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".pdf"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "file-upload."
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = ".mp3"
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            java.lang.String r1 = "imdb."
            boolean r1 = r0.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            boolean r1 = r6.contains(r0)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03d2
            if (r13 == 0) goto L_0x030e
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ all -> 0x03c4 }
            java.lang.String r1 = com.original.tase.helper.TitleHelper.a(r1, r12)     // Catch:{ all -> 0x03c4 }
            java.lang.StringBuilder r7 = new java.lang.StringBuilder     // Catch:{ all -> 0x03c4 }
            r7.<init>()     // Catch:{ all -> 0x03c4 }
            java.lang.String r14 = r26.getName()     // Catch:{ all -> 0x03c4 }
            java.lang.String r14 = r14.toLowerCase()     // Catch:{ all -> 0x03c4 }
            r7.append(r14)     // Catch:{ all -> 0x03c4 }
            java.lang.String r14 = r28.toLowerCase()     // Catch:{ all -> 0x03c4 }
            r7.append(r14)     // Catch:{ all -> 0x03c4 }
            java.lang.String r7 = r7.toString()     // Catch:{ all -> 0x03c4 }
            java.lang.String r7 = com.original.tase.helper.TitleHelper.a(r7, r12)     // Catch:{ all -> 0x03c4 }
            boolean r1 = r1.contains(r7)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x030e
            goto L_0x032b
        L_0x030e:
            if (r25 != 0) goto L_0x032f
            java.lang.String r1 = "([s|S]\\d+[e|E]\\d+)"
            r7 = 1
            java.lang.String r1 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r1, (int) r7)     // Catch:{ all -> 0x03c4 }
            boolean r14 = r1.isEmpty()     // Catch:{ all -> 0x03c4 }
            if (r14 != 0) goto L_0x0330
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ all -> 0x03c4 }
            java.lang.String r14 = r28.toLowerCase()     // Catch:{ all -> 0x03c4 }
            boolean r1 = r1.equals(r14)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x0330
        L_0x032b:
            r1 = r26
            goto L_0x03d6
        L_0x032f:
            r7 = 1
        L_0x0330:
            r6.add(r0)     // Catch:{ all -> 0x03c4 }
            java.lang.String r1 = r22.a()     // Catch:{ all -> 0x03c4 }
            if (r25 == 0) goto L_0x033e
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r14 = r2.a(r0)     // Catch:{ all -> 0x03c4 }
            goto L_0x0342
        L_0x033e:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r14 = r2.b(r0)     // Catch:{ all -> 0x03c4 }
        L_0x0342:
            java.lang.String r7 = "HQ"
            if (r14 == 0) goto L_0x0360
            java.lang.String r1 = r14.c()     // Catch:{ all -> 0x035b }
            java.lang.String r14 = r14.b()     // Catch:{ all -> 0x035b }
            r16 = r1
            r1 = r22
            java.lang.String r14 = r1.f(r14)     // Catch:{ all -> 0x03c4 }
            r20 = r14
            r14 = r16
            goto L_0x0366
        L_0x035b:
            r0 = move-exception
            r1 = r22
            goto L_0x03c5
        L_0x0360:
            r14 = r1
            r1 = r22
            r20 = r14
            r14 = r7
        L_0x0366:
            boolean r16 = r14.equalsIgnoreCase(r7)     // Catch:{ all -> 0x03c4 }
            java.lang.String r1 = "1080p"
            if (r16 == 0) goto L_0x038b
            if (r25 == 0) goto L_0x0373
            r16 = r10
            goto L_0x0375
        L_0x0373:
            r16 = r0
        L_0x0375:
            java.lang.String r2 = r16.toLowerCase()     // Catch:{ all -> 0x03c4 }
            boolean r16 = r2.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r16 == 0) goto L_0x0381
            r14 = r1
            goto L_0x038b
        L_0x0381:
            java.lang.String r1 = "720p"
            boolean r1 = r2.contains(r1)     // Catch:{ all -> 0x03c4 }
            if (r1 == 0) goto L_0x038b
            java.lang.String r14 = "HD"
        L_0x038b:
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ all -> 0x03c4 }
            java.lang.String r2 = "vidzi."
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03b3
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ all -> 0x03c4 }
            java.lang.String r2 = "estream."
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x03c4 }
            if (r1 != 0) goto L_0x03b3
            java.lang.String r1 = r0.toLowerCase()     // Catch:{ all -> 0x03c4 }
            java.lang.String r2 = ".m3u8"
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x03c4 }
            if (r1 == 0) goto L_0x03b0
            goto L_0x03b3
        L_0x03b0:
            r19 = r14
            goto L_0x03b5
        L_0x03b3:
            r19 = r7
        L_0x03b5:
            r1 = 0
            boolean[] r2 = new boolean[r1]     // Catch:{ all -> 0x03c4 }
            r16 = r22
            r17 = r23
            r18 = r0
            r21 = r2
            r16.a(r17, r18, r19, r20, r21)     // Catch:{ all -> 0x03c4 }
            goto L_0x03d2
        L_0x03c4:
            r0 = move-exception
        L_0x03c5:
            r1 = 0
            boolean[] r2 = new boolean[r1]     // Catch:{ all -> 0x03cc }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ all -> 0x03cc }
            goto L_0x03d2
        L_0x03cc:
            r0 = move-exception
            boolean[] r2 = new boolean[r1]     // Catch:{ all -> 0x03d9 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ all -> 0x03ea }
        L_0x03d2:
            r1 = r26
            r2 = r29
        L_0x03d6:
            r7 = 0
            goto L_0x01cc
        L_0x03d9:
            r0 = move-exception
            goto L_0x03ec
        L_0x03db:
            r1 = r26
            r2 = r29
            goto L_0x01a1
        L_0x03e1:
            r15 = r27
        L_0x03e3:
            r1 = r26
            r2 = r29
            r7 = 0
            goto L_0x0030
        L_0x03ea:
            r0 = move-exception
            r1 = 0
        L_0x03ec:
            boolean[] r1 = new boolean[r1]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r1)
        L_0x03f1:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.ReleaseBB.a(io.reactivex.ObservableEmitter, java.lang.String, java.lang.Object, com.movie.data.model.MovieInfo, java.lang.String, java.lang.String, com.original.tase.helper.DirectoryIndexHelper):void");
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str, String str2) {
        String str3;
        List<String> list;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        String str4;
        String str5;
        String str6;
        String str7;
        MovieInfo movieInfo2 = movieInfo;
        Integer num = movieInfo.getType().intValue() == 1 ? 1 : null;
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        String name = movieInfo.getName();
        if (num != null) {
            str3 = String.valueOf(movieInfo.getYear());
        } else {
            str3 = "S" + com.original.tase.utils.Utils.a(Integer.parseInt(str)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(str2));
        }
        String str8 = str3;
        String a2 = com.original.tase.utils.Utils.a(name.replace(": ", " ") + " " + str8, new boolean[0]);
        String a3 = com.original.tase.utils.Utils.a(String.valueOf(new Random().nextDouble()), new boolean[0]);
        HashMap<String, String> a4 = Constants.a();
        a4.put(TheTvdb.HEADER_ACCEPT, "application/json, text/javascript, */*; q=0.01");
        a4.put("Host", "search.rlsbb.ru");
        a4.put("Referer", "http://search.rlsbb.ru/?s=" + a2);
        String a5 = Regex.a(HttpHelper.e().a("http://search.rlsbb.ru" + String.format("/Home/GetPost?phrase=%s&pindex=1&type=Simple&rand=%s", new Object[]{a2, a3}), (Map<String, String>[]) new Map[]{a4}), "(\\{.*?\\})$", 1, true);
        if (!a5.isEmpty()) {
            a(observableEmitter, a5, num, movieInfo, a2, str8, directoryIndexHelper);
        } else {
            new ArrayList();
            if (num != null) {
                String str9 = movieInfo2.name;
                String str10 = movieInfo2.year;
                list = SearchHelper.c(str9, str10, str10, c, "");
            } else {
                list = SearchHelper.c(movieInfo2.name, movieInfo2.year, str8, c, "");
            }
            List<String> list2 = list;
            if (list2.isEmpty()) {
                String a6 = com.original.tase.utils.Utils.a(String.valueOf(new Random().nextDouble()), new boolean[0]);
                String str11 = "http://search.proxybb.com" + String.format("/Home/GetPost?phrase=%s&pindex=1&type=Simple&rand=%s", new Object[]{a2, a6});
                HashMap<String, String> a7 = Constants.a();
                a7.put(TheTvdb.HEADER_ACCEPT, "application/json, text/javascript, */*; q=0.01");
                a7.put("Referer", "http://search.proxybb.com/?s=" + a2);
                String a8 = Regex.a(HttpHelper.e().a(str11, (Map<String, String>[]) new Map[]{a7}), "(\\{.*?\\})$", 1, true);
                if (!a8.isEmpty()) {
                    a(observableEmitter, a8, num, movieInfo, a2, str8, directoryIndexHelper);
                }
            }
            if (!list2.isEmpty()) {
                ArrayList arrayList = new ArrayList();
                for (String b : list2) {
                    Document b2 = Jsoup.b(HttpHelper.e().b(b, c + "/"));
                    ArrayList<String> arrayList2 = new ArrayList<>();
                    try {
                        Element h = b2.h("div.post[id]");
                        if (h != null) {
                            arrayList2.add(h.toString());
                        }
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                    }
                    Iterator it2 = b2.g("div.messageBox").iterator();
                    while (it2.hasNext()) {
                        try {
                            arrayList2.add(((Element) it2.next()).toString());
                        } catch (Throwable th2) {
                            Logger.a(th2, new boolean[0]);
                        }
                    }
                    for (String replace : arrayList2) {
                        Iterator it3 = Regex.b(replace.replace("\\/", "/").replace("\\\"", "\""), "<a[^<]*href=['\"](.*?)['\"].*?</a>", 1, true).get(0).iterator();
                        while (it3.hasNext()) {
                            String str12 = (String) it3.next();
                            try {
                                String replace2 = str12.replace("http://", "").replace("https://", "");
                                if (!replace2.contains(d + "/")) {
                                    if (!replace2.contains("/" + d) && !str12.startsWith("#") && !str12.contains("thepiratebay.org") && !str12.contains("protected.to") && !str12.contains("histats.com") && !str12.contains("opensubtitles.org") && !str12.contains("facebook.com") && !str12.contains("twitter.com") && !str12.contains("amazon.com") && !str12.contains("youtube.com") && !str12.contains("youtu.be") && !str12.contains("imgaa.com") && !str12.contains("imdb.com") && !str12.contains("tvguide.com") && !str12.contains(".7z") && !str12.contains(".png") && !str12.contains(".rar") && !str12.contains(".zip") && !str12.contains(".iso") && !str12.contains(".part") && !str12.contains(".avi") && !str12.contains(".flv") && !str12.contains(".pdf") && !str12.contains("file-upload.") && !str12.endsWith(".jpg") && !str12.contains(".mp3") && !str12.contains("imdb.") && !arrayList.contains(str12)) {
                                        if (num == null) {
                                            try {
                                                String a9 = Regex.a(str12, "([s|S]\\d+[e|E]\\d+)", 1);
                                                if (!a9.isEmpty() && !a9.toLowerCase().equals(str8.toLowerCase())) {
                                                }
                                            } catch (Throwable th3) {
                                                th = th3;
                                                try {
                                                    Logger.a(th, new boolean[0]);
                                                } catch (Throwable th4) {
                                                    Logger.a(th4, new boolean[0]);
                                                }
                                            }
                                        }
                                        arrayList.add(str12);
                                        String a10 = a();
                                        if (num != null) {
                                            parsedLinkModel = directoryIndexHelper.a(str12);
                                        } else {
                                            parsedLinkModel = directoryIndexHelper.b(str12);
                                        }
                                        if (parsedLinkModel != null) {
                                            str5 = parsedLinkModel.c();
                                            try {
                                                str4 = f(parsedLinkModel.b());
                                            } catch (Throwable th5) {
                                                th = th5;
                                                Logger.a(th, new boolean[0]);
                                            }
                                        } else {
                                            str4 = a10;
                                            str5 = "HQ";
                                        }
                                        if (str5.equalsIgnoreCase("HQ")) {
                                            if (num != null) {
                                                str7 = "HQ";
                                            } else {
                                                str7 = str12;
                                            }
                                            String lowerCase = str7.toLowerCase();
                                            if (lowerCase.contains("1080p")) {
                                                str5 = "1080p";
                                            } else if (lowerCase.contains("720p")) {
                                                str5 = "HD";
                                            }
                                        }
                                        if (!str12.toLowerCase().contains("vidzi.") && !str12.toLowerCase().contains("estream.")) {
                                            if (!str12.toLowerCase().contains(".m3u8")) {
                                                str6 = str5;
                                                a(observableEmitter, str12, str6, str4, new boolean[0]);
                                            }
                                        }
                                        str6 = "HQ";
                                        a(observableEmitter, str12, str6, str4, new boolean[0]);
                                    }
                                }
                            } catch (Throwable th6) {
                                th = th6;
                                Logger.a(th, new boolean[0]);
                            }
                        }
                    }
                }
            }
        }
    }
}
