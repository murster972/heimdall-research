package com.utils.Getlink.Provider;

import com.movie.data.api.GlobalVariable;
import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import okhttp3.internal.cache.DiskLruCache;

public class Crackle extends BaseProvider {
    public HashMap c = new HashMap();
    private String d = "";

    public Crackle() {
        this.c.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        this.c.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US");
        this.c.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        this.c.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36");
    }

    private static String c() {
        String str = GlobalVariable.c().a().getCbflist().get("0");
        return (str == null || str.isEmpty()) ? "" : str;
    }

    public String a() {
        return "Crackle";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        this.d = c();
        if (GlobalVariable.c().a().getAds() == null && BaseProvider.b()) {
            a(observableEmitter, movieInfo, "-1", "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        this.d = c();
        if (GlobalVariable.c().a().getAds() == null && BaseProvider.b()) {
            a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:62:0x01b1 A[Catch:{ Exception -> 0x036c }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r24, com.movie.data.model.MovieInfo r25, java.lang.String r26, java.lang.String r27) {
        /*
            r23 = this;
            r7 = r23
            java.lang.String r8 = "1080p"
            java.lang.String r9 = "/"
            java.lang.String r10 = ".iso"
            java.lang.Integer r0 = r25.getType()
            int r0 = r0.intValue()
            r11 = 1
            r12 = 0
            if (r0 != r11) goto L_0x0016
            r13 = 1
            goto L_0x0017
        L_0x0016:
            r13 = 0
        L_0x0017:
            com.original.tase.helper.DirectoryIndexHelper r14 = new com.original.tase.helper.DirectoryIndexHelper
            r14.<init>()
            java.lang.String r15 = r25.getName()
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.lang.String r1 = "User-Agent"
            java.lang.String r2 = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.101 Safari/537.36"
            r0.put(r1, r2)
            if (r13 == 0) goto L_0x0037
            java.lang.Integer r1 = r25.getYear()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            goto L_0x0060
        L_0x0037:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "S"
            r1.append(r2)
            int r2 = java.lang.Integer.parseInt(r26)
            java.lang.String r2 = com.original.tase.utils.Utils.a((int) r2)
            r1.append(r2)
            java.lang.String r2 = "E"
            r1.append(r2)
            int r2 = java.lang.Integer.parseInt(r27)
            java.lang.String r2 = com.original.tase.utils.Utils.a((int) r2)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
        L_0x0060:
            r6 = r1
            com.original.tase.helper.http.HttpHelper r1 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r2 = r7.d
            java.lang.Object[] r3 = new java.lang.Object[r11]
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r15)
            java.lang.String r5 = " "
            r4.append(r5)
            r4.append(r6)
            java.lang.String r4 = r4.toString()
            java.lang.String r11 = "(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)"
            java.lang.String r4 = r4.replaceAll(r11, r5)
            java.lang.String r11 = "  "
            java.lang.String r4 = r4.replace(r11, r5)
            boolean[] r11 = new boolean[r12]
            java.lang.String r4 = com.original.tase.utils.Utils.a((java.lang.String) r4, (boolean[]) r11)
            r3[r12] = r4
            java.lang.String r2 = java.lang.String.format(r2, r3)
            r3 = 1
            java.util.Map[] r4 = new java.util.Map[r3]
            r4[r12] = r0
            java.lang.String r0 = r1.a((java.lang.String) r2, (java.util.Map<java.lang.String, java.lang.String>[]) r4)
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            org.jsoup.parser.Parser r1 = org.jsoup.parser.Parser.b()
            java.lang.String r4 = ""
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.a(r0, r4, r1)
            java.lang.String r1 = "item"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r1)
            java.util.Iterator r17 = r0.iterator()
        L_0x00b7:
            boolean r0 = r17.hasNext()
            if (r0 == 0) goto L_0x039a
            java.lang.Object r0 = r17.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r1 = "title"
            org.jsoup.nodes.Element r1 = r0.h(r1)
            if (r1 == 0) goto L_0x0388
            java.lang.String r3 = r1.G()
            java.lang.String r1 = "enclosure[url]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r1)
            java.util.Iterator r18 = r0.iterator()
        L_0x00d9:
            boolean r0 = r18.hasNext()
            if (r0 == 0) goto L_0x0388
            java.lang.Object r0 = r18.next()     // Catch:{ Exception -> 0x036c }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ Exception -> 0x036c }
            java.lang.String r1 = "url"
            java.lang.String r0 = r0.b((java.lang.String) r1)     // Catch:{ Exception -> 0x036c }
            java.lang.String r1 = "openload"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".7z"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".rar"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".zip"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            boolean r1 = r0.contains(r10)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".avi"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".flv"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = "paste"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".xyxy"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".msi"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = "imdb."
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            boolean r1 = r0.contains(r10)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".jpg"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            java.lang.String r1 = ".exe"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            boolean r1 = r11.contains(r0)     // Catch:{ Exception -> 0x036c }
            if (r1 != 0) goto L_0x0361
            r11.add(r0)     // Catch:{ Exception -> 0x036c }
            if (r13 != 0) goto L_0x019c
            boolean r1 = r0.contains(r9)     // Catch:{ Exception -> 0x018f }
            if (r1 == 0) goto L_0x019c
            java.lang.String[] r1 = r0.split(r9)     // Catch:{ Exception -> 0x018f }
            int r2 = r1.length     // Catch:{ Exception -> 0x018f }
            if (r2 <= 0) goto L_0x019c
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x018f }
            r2.<init>()     // Catch:{ Exception -> 0x018f }
            java.lang.String r12 = "(720p|1080p)"
            java.lang.String r12 = r3.replaceAll(r12, r4)     // Catch:{ Exception -> 0x018f }
            r2.append(r12)     // Catch:{ Exception -> 0x018f }
            r2.append(r5)     // Catch:{ Exception -> 0x018f }
            int r12 = r1.length     // Catch:{ Exception -> 0x018f }
            r16 = 1
            int r12 = r12 + -1
            r1 = r1[r12]     // Catch:{ Exception -> 0x018f }
            r2.append(r1)     // Catch:{ Exception -> 0x018f }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x018f }
            goto L_0x019d
        L_0x018f:
            r0 = move-exception
            r21 = r3
            r22 = r4
            r20 = r5
            r19 = r9
            r1 = 0
        L_0x0199:
            r9 = r6
            goto L_0x0377
        L_0x019c:
            r1 = r3
        L_0x019d:
            java.lang.String r2 = com.original.tase.helper.TitleHelper.f(r15)     // Catch:{ Exception -> 0x036c }
            java.lang.String r12 = "(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)"
            java.lang.String r12 = r1.replaceAll(r12, r4)     // Catch:{ Exception -> 0x036c }
            java.lang.String r12 = com.original.tase.helper.TitleHelper.f(r12)     // Catch:{ Exception -> 0x036c }
            boolean r2 = r2.equals(r12)     // Catch:{ Exception -> 0x036c }
            if (r2 == 0) goto L_0x0361
            java.lang.String r2 = "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]"
            r12 = 1
            java.util.ArrayList r2 = com.original.tase.utils.Regex.b(r1, r2, r12)     // Catch:{ Exception -> 0x036c }
            r12 = 0
            java.lang.Object r2 = r2.get(r12)     // Catch:{ Exception -> 0x036c }
            java.util.List r2 = (java.util.List) r2     // Catch:{ Exception -> 0x036c }
            if (r13 != 0) goto L_0x01eb
            java.lang.String r12 = "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]"
            r19 = r9
            r9 = 1
            java.util.ArrayList r12 = com.original.tase.utils.Regex.b(r1, r12, r9)     // Catch:{ Exception -> 0x0359 }
            r9 = 0
            java.lang.Object r12 = r12.get(r9)     // Catch:{ Exception -> 0x0359 }
            java.util.Collection r12 = (java.util.Collection) r12     // Catch:{ Exception -> 0x0359 }
            r2.addAll(r12)     // Catch:{ Exception -> 0x0359 }
            java.lang.String r12 = "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]"
            r9 = 1
            java.util.ArrayList r12 = com.original.tase.utils.Regex.b(r1, r12, r9)     // Catch:{ Exception -> 0x0359 }
            r9 = 0
            java.lang.Object r12 = r12.get(r9)     // Catch:{ Exception -> 0x0359 }
            java.util.Collection r12 = (java.util.Collection) r12     // Catch:{ Exception -> 0x0359 }
            r2.addAll(r12)     // Catch:{ Exception -> 0x0359 }
            goto L_0x01ed
        L_0x01e6:
            r0 = move-exception
            r19 = r9
            goto L_0x035a
        L_0x01eb:
            r19 = r9
        L_0x01ed:
            int r9 = r2.size()     // Catch:{ Exception -> 0x0359 }
            if (r9 <= 0) goto L_0x0352
            java.util.Iterator r2 = r2.iterator()     // Catch:{ Exception -> 0x0359 }
        L_0x01f7:
            boolean r9 = r2.hasNext()     // Catch:{ Exception -> 0x0359 }
            if (r9 == 0) goto L_0x020f
            java.lang.Object r9 = r2.next()     // Catch:{ Exception -> 0x0359 }
            java.lang.String r9 = (java.lang.String) r9     // Catch:{ Exception -> 0x0359 }
            java.lang.String r9 = r9.toUpperCase()     // Catch:{ Exception -> 0x0359 }
            boolean r9 = r9.equals(r6)     // Catch:{ Exception -> 0x0359 }
            if (r9 == 0) goto L_0x01f7
            r2 = 1
            goto L_0x0210
        L_0x020f:
            r2 = 0
        L_0x0210:
            if (r2 == 0) goto L_0x0352
            java.lang.String r1 = r1.toUpperCase()     // Catch:{ Exception -> 0x0359 }
            java.lang.String r2 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r1 = r1.replaceAll(r2, r4)     // Catch:{ Exception -> 0x0359 }
            java.lang.String r2 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r1 = r1.split(r2)     // Catch:{ Exception -> 0x0359 }
            int r2 = r1.length     // Catch:{ Exception -> 0x0359 }
            java.lang.String r9 = "HQ"
            r20 = r9
            r12 = 0
        L_0x0228:
            if (r12 >= r2) goto L_0x0305
            r21 = r1[r12]     // Catch:{ Exception -> 0x0359 }
            r25 = r1
            java.lang.String r1 = r21.toLowerCase()     // Catch:{ Exception -> 0x0359 }
            r21 = r2
            java.lang.String r2 = "subs"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "sub"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "dubbed"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "dub"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "dvdscr"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "r5"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "r6"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "camrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "tsrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "hdcam"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "hdts"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "dvdcam"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "dvdts"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "cam"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "telesync"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x0303
            java.lang.String r2 = "ts"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 == 0) goto L_0x02b5
            goto L_0x0303
        L_0x02b5:
            boolean r2 = r1.contains(r8)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x02f9
            java.lang.String r2 = "1080"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 == 0) goto L_0x02c4
            goto L_0x02f9
        L_0x02c4:
            java.lang.String r2 = "720p"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x02f4
            java.lang.String r2 = "720"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x02f4
            java.lang.String r2 = "brrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x02f4
            java.lang.String r2 = "bdrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x02f4
            java.lang.String r2 = "hdrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r2 != 0) goto L_0x02f4
            java.lang.String r2 = "web-dl"
            boolean r1 = r1.contains(r2)     // Catch:{ Exception -> 0x0359 }
            if (r1 == 0) goto L_0x02fb
        L_0x02f4:
            java.lang.String r1 = "HD"
            r20 = r1
            goto L_0x02fb
        L_0x02f9:
            r20 = r8
        L_0x02fb:
            int r12 = r12 + 1
            r1 = r25
            r2 = r21
            goto L_0x0228
        L_0x0303:
            r1 = 1
            goto L_0x0306
        L_0x0305:
            r1 = 0
        L_0x0306:
            if (r1 != 0) goto L_0x0352
            java.lang.String r1 = r23.a()     // Catch:{ Exception -> 0x0359 }
            if (r13 == 0) goto L_0x0313
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r14.a(r0)     // Catch:{ Exception -> 0x0359 }
            goto L_0x0317
        L_0x0313:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r14.b(r0)     // Catch:{ Exception -> 0x0359 }
        L_0x0317:
            if (r2 == 0) goto L_0x032f
            java.lang.String r1 = r2.c()     // Catch:{ Exception -> 0x0359 }
            boolean r1 = r1.equalsIgnoreCase(r9)     // Catch:{ Exception -> 0x0359 }
            if (r1 != 0) goto L_0x0327
            java.lang.String r20 = r2.c()     // Catch:{ Exception -> 0x0359 }
        L_0x0327:
            java.lang.String r1 = r2.b()     // Catch:{ Exception -> 0x0359 }
            java.lang.String r1 = r7.f(r1)     // Catch:{ Exception -> 0x0359 }
        L_0x032f:
            r9 = r1
            r1 = 0
            boolean[] r12 = new boolean[r1]     // Catch:{ Exception -> 0x0349 }
            r1 = r23
            r2 = r24
            r21 = r3
            r3 = r0
            r22 = r4
            r4 = r20
            r20 = r5
            r5 = r9
            r9 = r6
            r6 = r12
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x0347 }
            goto L_0x036a
        L_0x0347:
            r0 = move-exception
            goto L_0x0376
        L_0x0349:
            r0 = move-exception
            r21 = r3
            r22 = r4
            r20 = r5
            goto L_0x0199
        L_0x0352:
            r21 = r3
            r22 = r4
            r20 = r5
            goto L_0x0369
        L_0x0359:
            r0 = move-exception
        L_0x035a:
            r21 = r3
            r22 = r4
            r20 = r5
            goto L_0x0375
        L_0x0361:
            r21 = r3
            r22 = r4
            r20 = r5
            r19 = r9
        L_0x0369:
            r9 = r6
        L_0x036a:
            r1 = 0
            goto L_0x037c
        L_0x036c:
            r0 = move-exception
            r21 = r3
            r22 = r4
            r20 = r5
            r19 = r9
        L_0x0375:
            r9 = r6
        L_0x0376:
            r1 = 0
        L_0x0377:
            boolean[] r2 = new boolean[r1]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)
        L_0x037c:
            r6 = r9
            r9 = r19
            r5 = r20
            r3 = r21
            r4 = r22
            r12 = 0
            goto L_0x00d9
        L_0x0388:
            r22 = r4
            r20 = r5
            r19 = r9
            r1 = 0
            r9 = r6
            r6 = r9
            r9 = r19
            r5 = r20
            r4 = r22
            r12 = 0
            goto L_0x00b7
        L_0x039a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Crackle.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo, java.lang.String, java.lang.String):void");
    }
}
