package com.utils.Getlink.Provider;

import com.movie.data.api.GlobalVariable;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.model.media.MediaSource;
import info.debatty.java.stringsimilarity.experimental.Sift4;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import io.reactivex.ObservableEmitter;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import org.json.JSONArray;
import org.json.JSONObject;

public class Hd4movies extends BaseProvider {
    public static String[] c;
    public static String d = Deobfuscator$app$ProductionRelease.a(253);

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String j(java.lang.String r6) {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            java.net.URLConnection r6 = r1.openConnection()     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            int r1 = r6.getResponseCode()     // Catch:{ Exception -> 0x003e }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0033
            java.io.InputStream r1 = r6.getInputStream()     // Catch:{ Exception -> 0x003e }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x003e }
            r2.<init>()     // Catch:{ Exception -> 0x003e }
        L_0x001d:
            int r3 = r1.read()     // Catch:{ Exception -> 0x003e }
            r4 = -1
            if (r3 != r4) goto L_0x002f
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x003e }
            byte[] r2 = r2.toByteArray()     // Catch:{ Exception -> 0x003e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003e }
            r0 = r1
            goto L_0x0033
        L_0x002f:
            r2.write(r3)     // Catch:{ Exception -> 0x003e }
            goto L_0x001d
        L_0x0033:
            if (r6 == 0) goto L_0x0038
            r6.disconnect()     // Catch:{ Exception -> 0x003e }
        L_0x0038:
            if (r6 == 0) goto L_0x003d
            r6.disconnect()
        L_0x003d:
            return r0
        L_0x003e:
            r1 = move-exception
            goto L_0x0047
        L_0x0040:
            r6 = move-exception
            r5 = r0
            r0 = r6
            r6 = r5
            goto L_0x0051
        L_0x0045:
            r1 = move-exception
            r6 = r0
        L_0x0047:
            r1.printStackTrace()     // Catch:{ all -> 0x0050 }
            if (r6 == 0) goto L_0x004f
            r6.disconnect()
        L_0x004f:
            return r0
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r6 == 0) goto L_0x0056
            r6.disconnect()
        L_0x0056:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Hd4movies.j(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        HashMap<String, String> g;
        String str;
        try {
            c = GlobalVariable.c().a().getHdlist().get(Deobfuscator$app$ProductionRelease.a(237)).split(Deobfuscator$app$ProductionRelease.a(238));
            for (String str2 : c) {
                JSONArray jSONArray = new JSONObject(j(str2 + URLEncoder.encode(movieInfo.getName(), Deobfuscator$app$ProductionRelease.a(239)))).getJSONArray(Deobfuscator$app$ProductionRelease.a(240));
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    jSONObject.getInt(Deobfuscator$app$ProductionRelease.a(241));
                    String string = jSONObject.getString(Deobfuscator$app$ProductionRelease.a(242));
                    String str3 = movieInfo.name + Deobfuscator$app$ProductionRelease.a(243) + movieInfo.year + Deobfuscator$app$ProductionRelease.a(244);
                    if (string.contains(movieInfo.year)) {
                        Sift4 sift4 = new Sift4();
                        sift4.a(5);
                        if (sift4.a(string, str3) <= 2.0d) {
                            String[] split = jSONObject.getString(Deobfuscator$app$ProductionRelease.a(245)).split(Deobfuscator$app$ProductionRelease.a(246));
                            String str4 = Deobfuscator$app$ProductionRelease.a(247) + split[new Random().nextInt(split.length)];
                            if (str4 != null && !str4.isEmpty() && GoogleVideoHelper.j(str4) && (g = GoogleVideoHelper.g(str4)) != null && !g.isEmpty()) {
                                for (Map.Entry next : g.entrySet()) {
                                    MediaSource mediaSource = new MediaSource(a(), Deobfuscator$app$ProductionRelease.a(248), false);
                                    mediaSource.setOriginalLink(str4);
                                    mediaSource.setStreamLink((String) next.getKey());
                                    if (((String) next.getValue()).isEmpty()) {
                                        str = Deobfuscator$app$ProductionRelease.a(249);
                                    } else {
                                        str = (String) next.getValue();
                                    }
                                    mediaSource.setQuality(str);
                                    HashMap hashMap = new HashMap();
                                    hashMap.put(Deobfuscator$app$ProductionRelease.a(250), Constants.f5838a);
                                    hashMap.put(Deobfuscator$app$ProductionRelease.a(251), GoogleVideoHelper.c(str4, (String) next.getKey()));
                                    mediaSource.setPlayHeader(hashMap);
                                    observableEmitter.onNext(mediaSource);
                                }
                            }
                        }
                    }
                }
                jSONArray.length();
            }
        } catch (Exception e) {
            Logger.a(d, e.getMessage());
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public String a() {
        return Deobfuscator$app$ProductionRelease.a(252);
    }
}
