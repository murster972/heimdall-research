package com.utils.Getlink.Provider;

import com.facebook.imageutils.JfifUtil;
import com.movie.data.api.GlobalVariable;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.model.socket.UserResponces;
import info.debatty.java.stringsimilarity.experimental.Sift4;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import io.reactivex.ObservableEmitter;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Hd2movies extends BaseProvider {
    public static String c = Deobfuscator$app$ProductionRelease.a(JfifUtil.MARKER_SOS);

    public static String c() {
        try {
            for (T t : Collections.list(NetworkInterface.getNetworkInterfaces())) {
                if (t.getName().equalsIgnoreCase(Deobfuscator$app$ProductionRelease.a(214))) {
                    byte[] hardwareAddress = t.getHardwareAddress();
                    if (hardwareAddress == null) {
                        return Deobfuscator$app$ProductionRelease.a(JfifUtil.MARKER_RST7);
                    }
                    StringBuilder sb = new StringBuilder();
                    for (byte valueOf : hardwareAddress) {
                        sb.append(String.format(Deobfuscator$app$ProductionRelease.a(JfifUtil.MARKER_SOI), new Object[]{Byte.valueOf(valueOf)}));
                    }
                    if (sb.length() > 0) {
                        sb.deleteCharAt(sb.length() - 1);
                    }
                    return sb.toString();
                }
            }
        } catch (Exception unused) {
        }
        return Deobfuscator$app$ProductionRelease.a(JfifUtil.MARKER_EOI);
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x004c  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x0053  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String j(java.lang.String r6) {
        /*
            r0 = 0
            java.net.URL r1 = new java.net.URL     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            r1.<init>(r6)     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            java.net.URLConnection r6 = r1.openConnection()     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            java.net.HttpURLConnection r6 = (java.net.HttpURLConnection) r6     // Catch:{ Exception -> 0x0045, all -> 0x0040 }
            int r1 = r6.getResponseCode()     // Catch:{ Exception -> 0x003e }
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 != r2) goto L_0x0033
            java.io.InputStream r1 = r6.getInputStream()     // Catch:{ Exception -> 0x003e }
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream     // Catch:{ Exception -> 0x003e }
            r2.<init>()     // Catch:{ Exception -> 0x003e }
        L_0x001d:
            int r3 = r1.read()     // Catch:{ Exception -> 0x003e }
            r4 = -1
            if (r3 != r4) goto L_0x002f
            java.lang.String r1 = new java.lang.String     // Catch:{ Exception -> 0x003e }
            byte[] r2 = r2.toByteArray()     // Catch:{ Exception -> 0x003e }
            r1.<init>(r2)     // Catch:{ Exception -> 0x003e }
            r0 = r1
            goto L_0x0033
        L_0x002f:
            r2.write(r3)     // Catch:{ Exception -> 0x003e }
            goto L_0x001d
        L_0x0033:
            if (r6 == 0) goto L_0x0038
            r6.disconnect()     // Catch:{ Exception -> 0x003e }
        L_0x0038:
            if (r6 == 0) goto L_0x003d
            r6.disconnect()
        L_0x003d:
            return r0
        L_0x003e:
            r1 = move-exception
            goto L_0x0047
        L_0x0040:
            r6 = move-exception
            r5 = r0
            r0 = r6
            r6 = r5
            goto L_0x0051
        L_0x0045:
            r1 = move-exception
            r6 = r0
        L_0x0047:
            r1.printStackTrace()     // Catch:{ all -> 0x0050 }
            if (r6 == 0) goto L_0x004f
            r6.disconnect()
        L_0x004f:
            return r0
        L_0x0050:
            r0 = move-exception
        L_0x0051:
            if (r6 == 0) goto L_0x0056
            r6.disconnect()
        L_0x0056:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.Hd2movies.j(java.lang.String):java.lang.String");
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        HashMap<String, String> g;
        String str;
        MovieInfo movieInfo2 = movieInfo;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String[] split = GlobalVariable.c().a().getHdlist().get(Deobfuscator$app$ProductionRelease.a(191)).split(Pattern.quote(Deobfuscator$app$ProductionRelease.a(JfifUtil.MARKER_SOFn)));
        String str2 = split[0];
        char c2 = 1;
        String str3 = split[1];
        try {
            JSONArray jSONArray = new JSONObject(j(String.format(str2, new Object[]{movieInfo.getName().replace(Deobfuscator$app$ProductionRelease.a(193), Deobfuscator$app$ProductionRelease.a(194)), c()}))).getJSONArray(Deobfuscator$app$ProductionRelease.a(195));
            if (jSONArray.length() > 0) {
                int i = 0;
                while (i < jSONArray.length()) {
                    JSONObject jSONObject = jSONArray.getJSONObject(i);
                    int i2 = jSONObject.getInt(Deobfuscator$app$ProductionRelease.a(196));
                    String string = jSONObject.getString(Deobfuscator$app$ProductionRelease.a(197));
                    Logger.a(c, string);
                    String str4 = movieInfo2.name + Deobfuscator$app$ProductionRelease.a(198) + movieInfo2.year + Deobfuscator$app$ProductionRelease.a(199);
                    if (string.contains(movieInfo2.year)) {
                        Sift4 sift4 = new Sift4();
                        sift4.a(5);
                        if (sift4.a(string, str4) <= 2.0d) {
                            Object[] objArr = new Object[2];
                            objArr[0] = Integer.valueOf(i2);
                            objArr[c2] = c();
                            String j = j(String.format(str3, objArr));
                            Logger.a(c, j);
                            JSONArray jSONArray2 = new JSONObject(j).getJSONArray(Deobfuscator$app$ProductionRelease.a(UserResponces.USER_RESPONCE_SUCCSES));
                            for (int i3 = 0; i3 < jSONArray2.length(); i3++) {
                                JSONObject jSONObject2 = jSONArray2.getJSONObject(i3);
                                String string2 = jSONObject2.getString(Deobfuscator$app$ProductionRelease.a(201));
                                jSONObject2.getString(Deobfuscator$app$ProductionRelease.a(202));
                                if (string2.contains(Deobfuscator$app$ProductionRelease.a(203))) {
                                    String str5 = Deobfuscator$app$ProductionRelease.a(204) + string2;
                                    if (str5 != null && !str5.isEmpty() && GoogleVideoHelper.j(str5) && (g = GoogleVideoHelper.g(str5)) != null && !g.isEmpty()) {
                                        for (Map.Entry next : g.entrySet()) {
                                            MediaSource mediaSource = new MediaSource(a(), Deobfuscator$app$ProductionRelease.a(205), false);
                                            mediaSource.setOriginalLink(str5);
                                            mediaSource.setStreamLink((String) next.getKey());
                                            if (((String) next.getValue()).isEmpty()) {
                                                str = Deobfuscator$app$ProductionRelease.a(206);
                                            } else {
                                                str = (String) next.getValue();
                                            }
                                            mediaSource.setQuality(str);
                                            HashMap hashMap = new HashMap();
                                            hashMap.put(Deobfuscator$app$ProductionRelease.a(207), Constants.f5838a);
                                            hashMap.put(Deobfuscator$app$ProductionRelease.a(JfifUtil.MARKER_RST0), GoogleVideoHelper.c(str5, (String) next.getKey()));
                                            mediaSource.setPlayHeader(hashMap);
                                            observableEmitter2.onNext(mediaSource);
                                        }
                                    }
                                } else {
                                    boolean k = GoogleVideoHelper.k(string2);
                                    MediaSource mediaSource2 = new MediaSource(Deobfuscator$app$ProductionRelease.a(209) + a(), k ? Deobfuscator$app$ProductionRelease.a(210) : Deobfuscator$app$ProductionRelease.a(211), false);
                                    mediaSource2.setStreamLink(string2);
                                    mediaSource2.setQuality(k ? GoogleVideoHelper.h(string2) : Deobfuscator$app$ProductionRelease.a(212));
                                    observableEmitter2.onNext(mediaSource2);
                                }
                            }
                        }
                    }
                    i++;
                    c2 = 1;
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public String a() {
        return Deobfuscator$app$ProductionRelease.a(213);
    }
}
