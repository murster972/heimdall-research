package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.squareup.duktape.Duktape;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class TopEuroPix extends BaseProvider {
    private String c = Utils.getProvider(103);

    private String b(MovieInfo movieInfo) {
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/search?search=" + com.original.tase.utils.Utils.a(movieInfo.getName(), new boolean[0]).replace("%20", "+"), (Map<String, String>[]) new Map[]{hashMap}).replace(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, "")).g("div.movsbox").b("figure").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String a2 = Regex.a(element.toString(), "<h3> (.*) </h3>", 1);
            String replace = element.h("a").b("href").replace("../", "/").replace("//", "/").replace("//", "/");
            String a3 = Regex.a(element.toString(), "\\((\\w+)\\)", 1);
            if (a3.isEmpty()) {
                a3 = replace;
            }
            if (a2.equalsIgnoreCase(movieInfo.name) && a3.contains(movieInfo.year) && replace.startsWith("/")) {
                return this.c + replace;
            }
        }
        return "";
    }

    public String a() {
        return "TopEuroPix";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        String str2;
        Iterator it2;
        int i;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String str3 = str;
        HashMap hashMap = new HashMap();
        hashMap.put("user-agent", Constants.f5838a);
        String a2 = HttpHelper.e().a(str3, (Map<String, String>[]) new Map[]{hashMap});
        Iterator it3 = Regex.b(a2, "src\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
        boolean z = false;
        while (it3.hasNext()) {
            String str4 = (String) it3.next();
            if (str4.contains("com/svop/movsr")) {
                try {
                    String a3 = HttpHelper.e().a(str4, (Map<String, String>[]) new Map[]{hashMap});
                    String b = Jsoup.b(a3).h("iframe").b("src");
                    if (b.isEmpty()) {
                        b = Regex.a(a3, "https:\\/\\/www.rapidvideo.com\\/e\\/\\w+", 1);
                    }
                    if (!b.isEmpty()) {
                        a(observableEmitter2, b, "HD", false);
                    }
                    z = true;
                } catch (Exception unused) {
                }
            }
        }
        if (!z) {
            String str5 = Regex.a(a2, "(function\\s*change\\(id\\).+?\\r?\\n[\\s\\S]+)+document.getElementById.*src\\s*=\\s*src;", 1) + "return src;}";
            String replace = Jsoup.b(a2).h("div#changeserv").h("script").b("src").replace("../", "/").replace("//", "/");
            String a4 = Regex.a(HttpHelper.e().b(this.c + replace, str3), "document.write\\(unescape\\(['\"](.*)['\"]\\)", 1);
            if (!a4.isEmpty()) {
                try {
                    str2 = URLDecoder.decode(a4, "UTF-8");
                } catch (UnsupportedEncodingException unused2) {
                    str2 = URLDecoder.decode(a4);
                }
                Duktape create = Duktape.create();
                for (Iterator it4 = Jsoup.b(str2).g("a[onclick]").iterator(); it4.hasNext(); it4 = it2) {
                    try {
                        Element element = (Element) it4.next();
                        boolean c2 = c(element.h("span#qual").G());
                        str5 = str5 + element.b("onclick") + ";";
                        String replace2 = create.evaluate(str5).toString().replace("../", "/");
                        if (replace2.startsWith("/")) {
                            replace2 = this.c + replace2;
                        }
                        if (b(replace2)) {
                            a(observableEmitter2, replace2, "HD", c2);
                        } else if (!replace2.contains("youtube")) {
                            try {
                                HttpHelper e = HttpHelper.e();
                                it2 = it4;
                                try {
                                    Map[] mapArr = new Map[1];
                                    try {
                                        mapArr[0] = hashMap;
                                        String a5 = e.a(replace2, (Map<String, String>[]) mapArr);
                                        String b2 = Jsoup.b(a5).h("iframe").b("src");
                                        if (b2.isEmpty()) {
                                            i = 1;
                                            try {
                                                b2 = Regex.a(a5, "https:\\/\\/www.rapidvideo.com\\/e\\/\\w+", 1);
                                            } catch (Throwable unused3) {
                                            }
                                        } else {
                                            i = 1;
                                        }
                                        if (!b2.isEmpty()) {
                                            boolean[] zArr = new boolean[i];
                                            try {
                                                zArr[0] = c2;
                                                a(observableEmitter2, b2, "HD", zArr);
                                            } catch (Throwable unused4) {
                                            }
                                        }
                                    } catch (Throwable unused5) {
                                    }
                                } catch (Throwable unused6) {
                                }
                            } catch (Throwable unused7) {
                            }
                        }
                        it2 = it4;
                    } catch (Throwable unused8) {
                        it2 = it4;
                    }
                }
            }
        }
    }
}
