package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class NTMovies extends BaseProvider {
    public String c = Utils.getProvider(25);

    public String a() {
        return "NTMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        String b = HttpHelper.e().b(str + "/season/" + movieInfo.session + "/e/" + com.original.tase.utils.Utils.a(movieInfo.getEps().intValue()), str);
        Iterator it2 = Jsoup.b(b).g("ul.links").b("a").iterator();
        while (it2.hasNext()) {
            a(observableEmitter, ((Element) it2.next()).b("href"), "HD", false);
        }
        Iterator it3 = Regex.b(b, "data-src\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0).iterator();
        while (it3.hasNext()) {
            a(observableEmitter, (String) it3.next(), "HD", false);
        }
    }

    public String b(MovieInfo movieInfo) {
        HashMap<String, String> a2 = Constants.a();
        a2.put("referer", this.c + "/");
        HttpHelper e = HttpHelper.e();
        String a3 = e.a(this.c + "/serieslist", (Map<String, String>[]) new Map[]{a2});
        Iterator it2 = Regex.b(a3, "['\"]?id['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]", 1, true).get(0).iterator();
        Iterator it3 = Regex.b(a3, "['\"]?titulo['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]", 1, true).get(0).iterator();
        while (it3.hasNext()) {
            String str = (String) it3.next();
            String str2 = (String) it2.next();
            if (str.toLowerCase().equals(movieInfo.name.toLowerCase())) {
                return this.c + "/serie/" + str2 + "-" + TitleHelper.a(str.toLowerCase(), "-");
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }
}
