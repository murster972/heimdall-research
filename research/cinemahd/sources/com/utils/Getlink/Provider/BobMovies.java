package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.vungle.warren.model.CookieDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class BobMovies extends BaseProvider {
    private String c = Utils.getProvider(73);

    public String a() {
        return "BobMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo) {
        String str;
        boolean z;
        String str2;
        String str3;
        String str4;
        boolean z2;
        String str5;
        String str6;
        int i;
        String str7;
        String str8;
        Element h;
        Iterator it2;
        String str9;
        boolean z3;
        String str10;
        String str11;
        MovieInfo movieInfo2 = movieInfo;
        boolean z4 = movieInfo.getType().intValue() == 1;
        String a2 = TitleHelper.a(movieInfo2.name.toLowerCase(), "-");
        HttpHelper.e().a(this.c + "/", (Map<String, String>[]) new Map[0]);
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        String str12 = "referer";
        hashMap.put(str12, this.c + "/");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("user-agent", Constants.f5838a);
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, HttpHelper.e().a(this.c + "/"));
        String str13 = this.c + "/search/" + a2;
        String a3 = HttpHelper.e().a(str13, (Map<String, String>[]) new Map[]{hashMap});
        Iterator it3 = Jsoup.b(a3).g("div.ml-item").iterator();
        if (!it3.hasNext()) {
            a3 = HttpHelper.e().a("https://api.ocloud.stream/series/movie/search/" + a2 + "?link_web=" + this.c, (Map<String, String>[]) new Map[0]);
            it3 = Jsoup.b(a3).g("div.ml-item").iterator();
        }
        String str14 = movieInfo2.year;
        boolean z5 = false;
        while (true) {
            String str15 = str14;
            if (!it3.hasNext()) {
                z = z4;
                str2 = "title";
                str3 = "a";
                str4 = str12;
                z2 = z5;
                str5 = "https:";
                str6 = "";
                break;
            }
            h = ((Element) it3.next()).h("a");
            String str16 = str;
            String b = h.b("data-url");
            String b2 = h.b("title");
            if (b2.isEmpty()) {
                it2 = it3;
                b2 = h.h("h2").G();
            } else {
                it2 = it3;
            }
            String str17 = b2;
            z2 = z5;
            str2 = "title";
            if (z4) {
                str3 = "a";
                str4 = str12;
                if (str17.equals(movieInfo2.name)) {
                    if (!b.isEmpty()) {
                        if (b.startsWith("//")) {
                            b = "http:" + b;
                            z = z4;
                        } else if (b.startsWith("/")) {
                            StringBuilder sb = new StringBuilder();
                            z = z4;
                            sb.append(this.c);
                            sb.append(b);
                            b = sb.toString();
                        } else {
                            z = z4;
                            if (!b.startsWith(UriUtil.HTTP_SCHEME)) {
                                b = this.c + "/" + b;
                            }
                        }
                        str9 = "https:";
                        str11 = HttpHelper.e().b(b + "##forceNoCache##", str13, Constants.a()).replace("\\\"", "\"").replace("\\/", "/");
                        str10 = Regex.a(str11, "<div\\s+[^>]*class=\"jt-info\"[^>]*>\\s*(\\d{4})\\s*</div>", 1);
                        String a4 = Regex.a(str11, "<div\\s+[^>]*class=\"jtip-quality\"[^>]*>\\s*([\\w\\s]+)\\s*</div>", 1);
                        z3 = c(a4);
                        String a5 = Regex.a(a4, "(\\d+)", 1);
                        if (!a5.isEmpty()) {
                            a5 + "p";
                        }
                    } else {
                        z = z4;
                        str9 = "https:";
                        str10 = str15;
                        str11 = str16;
                        z3 = z2;
                    }
                    if (str10.equals(movieInfo2.year) || str11.isEmpty()) {
                        str6 = h.b("href");
                    } else {
                        str = str11;
                        z2 = z3;
                        str15 = str10;
                    }
                } else {
                    z = z4;
                    str9 = "https:";
                    str = str16;
                }
                if (str17.equals(movieInfo2.name + " (" + movieInfo2.year + ")")) {
                    str6 = h.b("href");
                    if (str6.startsWith("//")) {
                        str6 = "http:" + str6;
                    } else if (str6.startsWith("/")) {
                        str6 = this.c + str6;
                    } else if (!str6.startsWith(UriUtil.HTTP_SCHEME)) {
                        str6 = this.c + "/" + str6;
                    }
                } else {
                    str14 = str15;
                }
            } else {
                z = z4;
                str3 = "a";
                str4 = str12;
                String str18 = "https:";
                if (TitleHelper.f(str17).contains(TitleHelper.f(TitleHelper.e(movieInfo.getName())))) {
                    str6 = h.b("href");
                    if (str6.startsWith("//")) {
                        StringBuilder sb2 = new StringBuilder();
                        str5 = str18;
                        sb2.append(str5);
                        sb2.append(str6);
                        str6 = sb2.toString();
                    } else {
                        str5 = str18;
                        if (str6.startsWith("/")) {
                            str6 = this.c + str6;
                        } else if (!str6.startsWith(UriUtil.HTTP_SCHEME)) {
                            str6 = this.c + "/" + str6;
                        }
                    }
                } else {
                    str14 = str15;
                    str = str16;
                }
            }
            z5 = z2;
            it3 = it2;
            str12 = str4;
            z4 = z;
        }
        str6 = h.b("href");
        if (str6.startsWith("//")) {
            str6 = "http:" + str6;
        } else if (str6.startsWith("/")) {
            str6 = this.c + str6;
        } else if (!str6.startsWith(UriUtil.HTTP_SCHEME)) {
            str6 = this.c + "/" + str6;
        }
        z2 = z3;
        str5 = str9;
        if (!str6.isEmpty()) {
            if (str6.endsWith(".html")) {
                i = 0;
                str7 = str6.substring(0, str6.length() - 5);
            } else {
                i = 0;
                str7 = str6;
            }
            if (str7.endsWith("/")) {
                str7 = str7.substring(i, str7.length() - 1);
            }
            if (!z) {
                str8 = str7 + "/s" + movieInfo2.session + "/watching.html";
            } else {
                str8 = str7 + "/watching.html";
            }
            String b3 = HttpHelper.e().b(str8, str6);
            String a6 = Regex.a(b3, "id\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
            String a7 = Regex.a(b3, "quality\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
            HashMap<String, String> a8 = Constants.a();
            a8.put(str4, str8);
            Document b4 = Jsoup.b(HttpHelper.e().a(this.c + "/ajax/v2_get_episodes/" + a6, (Map<String, String>[]) new Map[]{a8}));
            boolean isEmpty = a7.isEmpty();
            new HashMap().put("User-Agent", Constants.f5838a);
            Iterator it4 = b4.g("div.les-content").b(str3).iterator();
            while (it4.hasNext()) {
                Element element = (Element) it4.next();
                String b5 = element.b("episode-id");
                String str19 = str2;
                if (!z) {
                    String a9 = Regex.a(element.b(str19), "(\\d+)", 1);
                    if (!a9.isEmpty()) {
                        if (!a9.equals(movieInfo2.eps)) {
                        }
                    }
                    str2 = str19;
                }
                String G = element.G();
                if (G.isEmpty() || !z) {
                    G = "HD";
                }
                String a10 = HttpHelper.e().a(this.c + "/ajax/load_embed/" + b5, (Map<String, String>[]) new Map[]{a8});
                try {
                    a10 = HttpHelper.e().b(Regex.a(a10, "['\"]embed_url['\"]\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1).replace("\\/", "/"), str8);
                } catch (Throwable unused) {
                }
                ArrayList arrayList = new ArrayList();
                if (JsUnpacker.m30920(a10)) {
                    arrayList.addAll(JsUnpacker.m30916(a10));
                }
                Iterator it5 = arrayList.iterator();
                while (it5.hasNext()) {
                    String a11 = Regex.a((String) it5.next(), "var\\s*link\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
                    if (a11.startsWith("//")) {
                        a11 = str5 + a11;
                    }
                    a(observableEmitter, a11, G, z2);
                }
                ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                str2 = str19;
            }
        }
    }
}
