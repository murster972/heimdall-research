package com.utils.Getlink.Provider;

import com.applovin.sdk.AppLovinEventParameters;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class PHMovies extends BaseProvider {
    private String c = Utils.getProvider(39);

    public String a() {
        return "PHMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        a(observableEmitter, movieInfo);
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo) {
        String str;
        Iterator it2;
        String str2;
        boolean z;
        Element element;
        if (movieInfo.name.equalsIgnoreCase("Marvel’s Iron Fist")) {
            movieInfo.name = "iron fist";
        }
        boolean z2 = movieInfo.getType().intValue() == 1;
        String replace = TitleHelper.g(movieInfo.name.toLowerCase()).replaceAll("(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)", " ").replace("  ", " ").replace(" ", "-");
        if (!z2) {
            str = replace + "-season-" + movieInfo.session;
        } else {
            str = replace + "-" + movieInfo.year;
        }
        HashMap hashMap = new HashMap();
        String str3 = this.c + "/" + str + "/";
        hashMap.put("user-agent", Constants.f5838a);
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        String b = HttpHelper.e().b(str3, this.c + "/");
        if (b.contains("Attention Required! | Cloudflare")) {
            Logger.a("Need Verify Recaptcha", str3);
            Utils.a(str3, BaseProvider.h(str3));
        } else if (!b.contains("Not Found")) {
            if (z2) {
                it2 = Jsoup.b(b).g("div.box.download").b("a").iterator();
            } else {
                Iterator it3 = Jsoup.b(b).g("div.box.download").iterator();
                while (true) {
                    if (!it3.hasNext()) {
                        z = false;
                        break;
                    }
                    element = (Element) it3.next();
                    String G = element.h("span").G();
                    if (G.equals("Episode " + movieInfo.eps)) {
                        break;
                    }
                    if (G.contains("Episode " + movieInfo.eps + " ")) {
                        break;
                    }
                }
                it3 = element.g("a").iterator();
                z = true;
                if (z) {
                    it2 = it3;
                } else {
                    return;
                }
            }
            while (it2.hasNext()) {
                String b2 = ((Element) it2.next()).b("href");
                Regex.a(b2, "(?:ce)=(.*)", 1);
                String a2 = HttpHelper.e().a(b2, (Map<String, String>[]) new Map[0]);
                hashMap.put("referer", b2);
                HashMap<String, String> b3 = b(a2, "POST");
                if (!b3.isEmpty()) {
                    String a3 = HttpHelper.e().a(b3.get("action").toString(), b3.get(AppLovinEventParameters.SEARCH_QUERY).toString(), (Map<String, String>[]) new Map[]{hashMap});
                    HashMap<String, String> b4 = b(a3, "POST");
                    if (!b4.isEmpty()) {
                        a3 = HttpHelper.e().a(b4.get("action").toString(), b4.get(AppLovinEventParameters.SEARCH_QUERY).toString(), (Map<String, String>[]) new Map[]{hashMap});
                    }
                    str2 = Regex.a(a3, "var a\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                } else {
                    str2 = "";
                }
                if (!str2.isEmpty()) {
                    a(observableEmitter, Regex.a(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0]), "<a[^>]*href\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>Continue", 1, 34), "HD", false);
                }
            }
        }
    }
}
