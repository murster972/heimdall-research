package com.utils.Getlink.Provider;

import com.google.gson.JsonParser;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class FmovieCloud extends BaseProvider {
    private String c = Utils.getProvider(32);

    public String a() {
        return "FmovieCloud";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, movieInfo, b);
        }
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, MovieInfo movieInfo, String str) {
        String str2;
        String str3 = str;
        int i = 1;
        boolean z = movieInfo.getType().intValue() == 1;
        if (!str3.endsWith("/")) {
            str3 = str3 + "/";
        }
        String str4 = str3 + "watching.html";
        ArrayList arrayList = Regex.b(str4, "-(\\d+)", 1, true).get(0);
        String str5 = !arrayList.isEmpty() ? (String) arrayList.get(arrayList.size() - 1) : null;
        HashMap<String, String> a2 = Constants.a();
        a2.put("Referer", str4);
        try {
            str2 = new JsonParser().a(HttpHelper.e().a(this.c + "/ajax/movie_episodes/" + str5, (Map<String, String>[]) new Map[]{a2})).f().a("html").i();
        } catch (Exception e) {
            Logger.a((Throwable) e, new boolean[0]);
            str2 = "";
        }
        Iterator it2 = Jsoup.b(str2).g("div[id*=sv-]").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            element.b("id");
            String b = element.b("title");
            String b2 = element.b("data-id");
            element.b("data-server");
            if (!z) {
                if (Regex.a(b, "(Episode\\s+0*" + movieInfo.eps + "(?!\\d))", i, 2).isEmpty()) {
                }
            } else {
                MovieInfo movieInfo2 = movieInfo;
            }
            if (!z) {
                b = "HD";
            }
            HashMap<String, String> a3 = Constants.a();
            a3.put("referer", str4 + "?ep=" + b2);
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", Constants.f5838a);
            Iterator it3 = it2;
            String a4 = HttpHelper.e().a(this.c + "/ajax/movie_embed/" + b2, (Map<String, String>[]) new Map[]{a3});
            if (!a4.isEmpty() && a4.trim().toLowerCase().contains("src")) {
                String replace = Regex.a(a4, "[\"']?src[\"']?\\s*:\\s*[\"']([^\"']+)", 1).replace("\\/", "/").replace("\\\\", "");
                if (!replace.isEmpty()) {
                    boolean k = GoogleVideoHelper.k(replace);
                    hashMap.put("Referer", str4 + "?ep=" + b2);
                    MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", c(b));
                    mediaSource.setStreamLink(replace);
                    mediaSource.setPlayHeader(hashMap);
                    if (k) {
                        b = GoogleVideoHelper.h(replace);
                    }
                    mediaSource.setQuality(b);
                    observableEmitter.onNext(mediaSource);
                    it2 = it3;
                    i = 1;
                }
            }
            ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
            it2 = it3;
            i = 1;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:3:0x0063  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String b(com.movie.data.model.MovieInfo r9) {
        /*
            r8 = this;
            java.lang.String r0 = r9.name
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r1 = ""
            java.lang.String r2 = "(\\,|\\.|\\')"
            java.lang.String r0 = r0.replaceAll(r2, r1)
            java.lang.String r2 = " "
            java.lang.String r3 = "(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)"
            java.lang.String r0 = r0.replaceAll(r3, r2)
            java.lang.String r3 = "  "
            java.lang.String r0 = r0.replace(r3, r2)
            java.lang.String r3 = "+"
            java.lang.String r0 = r0.replace(r2, r3)
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = r8.c
            r3.append(r4)
            java.lang.String r4 = "/movie/search/"
            r3.append(r4)
            r3.append(r0)
            java.lang.String r0 = r3.toString()
            r3 = 0
            java.util.Map[] r3 = new java.util.Map[r3]
            java.lang.String r0 = r2.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r3)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r2 = "div.movies-list.movies-list-full"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r2)
            java.lang.String r2 = "div.ml-item"
            org.jsoup.select.Elements r0 = r0.b(r2)
            java.lang.String r2 = "a"
            org.jsoup.select.Elements r0 = r0.b(r2)
            java.util.Iterator r0 = r0.iterator()
        L_0x005d:
            boolean r2 = r0.hasNext()
            if (r2 == 0) goto L_0x00ab
            java.lang.Object r2 = r0.next()
            org.jsoup.nodes.Element r2 = (org.jsoup.nodes.Element) r2
            java.lang.String r3 = "data-url"
            java.lang.String r3 = r2.b((java.lang.String) r3)
            java.lang.String r4 = "title"
            java.lang.String r4 = r2.b((java.lang.String) r4)
            java.lang.String r5 = r4.toLowerCase()
            java.lang.String r5 = com.original.tase.helper.TitleHelper.a(r5, r1)
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r7 = r9.name
            java.lang.String r7 = r7.toLowerCase()
            r6.append(r7)
            java.lang.String r7 = r9.year
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            java.lang.String r6 = com.original.tase.helper.TitleHelper.a(r6, r1)
            boolean r5 = r5.equals(r6)
            if (r5 != 0) goto L_0x00a4
            boolean r3 = r8.a((com.movie.data.model.MovieInfo) r9, (java.lang.String) r4, (java.lang.String) r3)
            if (r3 == 0) goto L_0x005d
        L_0x00a4:
            java.lang.String r9 = "href"
            java.lang.String r9 = r2.b((java.lang.String) r9)
            return r9
        L_0x00ab:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.FmovieCloud.b(com.movie.data.model.MovieInfo):java.lang.String");
    }

    /* access modifiers changed from: package-private */
    public boolean a(MovieInfo movieInfo, String str, String str2) {
        if (!(movieInfo.getType().intValue() == 1)) {
            if (TitleHelper.f(str).equals(TitleHelper.f(movieInfo.name + " - Season " + movieInfo.session))) {
                if (str2.startsWith("/")) {
                    str2 = this.c + str2;
                }
                if (Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0])).h("div.jtip-top").toString().contains(movieInfo.sessionYear)) {
                    return true;
                }
            }
        } else if (TitleHelper.f(str).equals(TitleHelper.f(TitleHelper.e(movieInfo.getName())))) {
            if (str2.startsWith("/")) {
                str2 = this.c + str2;
            }
            if (Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0])).h("div.jtip-top").toString().contains(movieInfo.year)) {
                return true;
            }
        }
        return false;
    }
}
