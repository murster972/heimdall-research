package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.js.JsUnpacker;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class bioskopkeren extends BaseProvider {
    public static String c = (Utils.getProvider(78) + "/");

    public String a() {
        return "Bioskopkeren";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        String replace = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]).replace("%20", "+");
        String a2 = TitleHelper.a(movieInfo.name.toLowerCase(), "-");
        HttpHelper e = HttpHelper.e();
        Iterator it2 = Jsoup.b(e.a(c + "?s=" + replace, (Map<String, String>[]) new Map[0])).g("div[class=moviefilm]").iterator();
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("href");
            String b2 = h.h("img").b("src");
            String lowerCase = b.toLowerCase();
            if (!lowerCase.contains("nonton-" + a2 + "-" + movieInfo.year)) {
                String lowerCase2 = b.toLowerCase();
                if (!lowerCase2.contains("movie-" + a2 + "-" + movieInfo.year)) {
                    String lowerCase3 = b.toLowerCase();
                    if (!lowerCase3.contains("film-" + a2 + "-" + movieInfo.year)) {
                        String lowerCase4 = b.toLowerCase();
                        if (!lowerCase4.contains("nonton-" + a2 + "-subtitle")) {
                            continue;
                        }
                    }
                }
            }
            if (b2.toLowerCase().contains(movieInfo.year)) {
                return b;
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        String str2 = str;
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", str2);
        hashMap.put("User-Agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str2, (Map<String, String>[]) new Map[0])).g("iframe[src]").iterator();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).b("src");
            if (!b.isEmpty()) {
                try {
                    Iterator it3 = Jsoup.b(URLDecoder.decode(Jsoup.b(HttpHelper.e().a(b, (Map<String, String>[]) new Map[]{hashMap})).h("script[language=javascript]").u(), "UTF-8")).g("script").iterator();
                    while (it3.hasNext()) {
                        String u = ((Element) it3.next()).u();
                        if (JsUnpacker.m30920(u)) {
                            Iterator<String> it4 = JsUnpacker.m30918(u).iterator();
                            while (it4.hasNext()) {
                                Iterator it5 = Regex.b(it4.next(), "['\"]?file['\"]?\\s*:\\s*['\"]?([^'\"]+)", 1, 2).get(0).iterator();
                                ArrayList<MediaSource> arrayList = new ArrayList<>();
                                while (it5.hasNext()) {
                                    String str3 = (String) it5.next();
                                    if (!str3.contains("player") && !str3.contains("sub?") && !str3.contains(".srt")) {
                                        HashMap hashMap2 = new HashMap();
                                        hashMap2.put("User-Agent", Constants.f5838a);
                                        boolean k = GoogleVideoHelper.k(str3);
                                        MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                                        mediaSource.setStreamLink(str3);
                                        mediaSource.setPlayHeader(hashMap2);
                                        mediaSource.setQuality(k ? GoogleVideoHelper.h(str3) : "HD");
                                        arrayList.add(mediaSource);
                                    }
                                }
                                if (arrayList.size() > 0) {
                                    Collections.reverse(arrayList);
                                    for (MediaSource onNext : arrayList) {
                                        try {
                                            observableEmitter.onNext(onNext);
                                        } catch (Throwable th) {
                                            th = th;
                                        }
                                    }
                                }
                                ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                            }
                        }
                        ObservableEmitter<? super MediaSource> observableEmitter3 = observableEmitter;
                    }
                } catch (Throwable th2) {
                    th = th2;
                    ObservableEmitter<? super MediaSource> observableEmitter4 = observableEmitter;
                    Logger.a(th, new boolean[0]);
                }
            }
            ObservableEmitter<? super MediaSource> observableEmitter5 = observableEmitter;
        }
    }
}
