package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class YesMovies extends BaseProvider {
    private String c = Utils.getProvider(71);

    public String a() {
        return "YesMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, "-1", false);
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, "-1");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String a2 = a(movieInfo, movieInfo.session, false);
        if (!a2.isEmpty()) {
            a(observableEmitter, a2, movieInfo, movieInfo.eps);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:33:0x012c  */
    /* JADX WARNING: Removed duplicated region for block: B:60:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r17, java.lang.String r18, com.movie.data.model.MovieInfo r19, java.lang.String r20) {
        /*
            r16 = this;
            r1 = r16
            r0 = r18
            java.lang.String r2 = ""
            java.lang.Integer r3 = r19.getSession()
            int r3 = r3.intValue()
            r4 = 1
            r5 = 0
            if (r3 >= 0) goto L_0x0014
            r3 = 1
            goto L_0x0015
        L_0x0014:
            r3 = 0
        L_0x0015:
            java.lang.String r6 = "-(\\d+)"
            java.util.ArrayList r6 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r6, (int) r4, (boolean) r4)
            java.lang.Object r6 = r6.get(r5)
            java.util.ArrayList r6 = (java.util.ArrayList) r6
            boolean r7 = r6.isEmpty()
            if (r7 != 0) goto L_0x0033
            int r7 = r6.size()
            int r7 = r7 - r4
            java.lang.Object r6 = r6.get(r7)
            java.lang.String r6 = (java.lang.String) r6
            goto L_0x0034
        L_0x0033:
            r6 = 0
        L_0x0034:
            if (r6 == 0) goto L_0x01f7
            java.lang.String r7 = ".html"
            boolean r7 = r0.endsWith(r7)
            if (r7 == 0) goto L_0x0049
            int r7 = r18.length()
            int r7 = r7 + -5
            java.lang.String r7 = r0.substring(r5, r7)
            goto L_0x004a
        L_0x0049:
            r7 = r0
        L_0x004a:
            java.lang.String r8 = "/"
            boolean r9 = r7.endsWith(r8)
            if (r9 == 0) goto L_0x005b
            int r9 = r7.length()
            int r9 = r9 - r4
            java.lang.String r7 = r7.substring(r5, r9)
        L_0x005b:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r7)
            java.lang.String r7 = "/watching.html"
            r9.append(r7)
            java.lang.String r7 = r9.toString()
            com.original.tase.helper.http.HttpHelper r9 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r0 = r9.b((java.lang.String) r7, (java.lang.String) r0)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r9 = "span.quality"
            org.jsoup.nodes.Element r0 = r0.h(r9)
            if (r0 == 0) goto L_0x009e
            java.lang.String r0 = r0.G()
            java.lang.String r0 = r0.trim()
            java.lang.String r0 = r0.toLowerCase()
            java.lang.String r9 = "cam"
            boolean r9 = r0.contains(r9)
            if (r9 != 0) goto L_0x009c
            java.lang.String r9 = "ts"
            boolean r0 = r0.contains(r9)
            if (r0 == 0) goto L_0x009e
        L_0x009c:
            r9 = 1
            goto L_0x009f
        L_0x009e:
            r9 = 0
        L_0x009f:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = r1.c
            r10.append(r11)
            java.lang.String r11 = "/user/status.html"
            r10.append(r11)
            java.lang.String r10 = r10.toString()
            java.util.Map[] r11 = new java.util.Map[r5]
            r0.b((java.lang.String) r10, (java.util.Map<java.lang.String, java.lang.String>[]) r11)
            java.util.HashMap r10 = com.original.Constants.a()
            java.lang.String r0 = "accept"
            java.lang.String r11 = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3"
            r10.put(r0, r11)
            java.lang.String r0 = "referer"
            r10.put(r0, r7)
            java.lang.String r0 = com.original.Constants.f5838a
            java.lang.String r7 = "user-agent"
            r10.put(r7, r0)
            java.lang.String r0 = "accept-language"
            java.lang.String r7 = "en-US,en;q=0.5"
            r10.put(r0, r7)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r7 = r1.c
            r0.append(r7)
            java.lang.String r7 = "/movie_episodes/"
            r0.append(r7)
            r0.append(r6)
            java.lang.String r0 = r0.toString()
            com.google.gson.JsonParser r7 = new com.google.gson.JsonParser     // Catch:{ Exception -> 0x0113 }
            r7.<init>()     // Catch:{ Exception -> 0x0113 }
            com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0113 }
            java.util.Map[] r12 = new java.util.Map[r4]     // Catch:{ Exception -> 0x0113 }
            r12[r5] = r10     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r11.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r12)     // Catch:{ Exception -> 0x0113 }
            com.google.gson.JsonElement r0 = r7.a((java.lang.String) r0)     // Catch:{ Exception -> 0x0113 }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ Exception -> 0x0113 }
            java.lang.String r7 = "html"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r7)     // Catch:{ Exception -> 0x0113 }
            java.lang.String r0 = r0.i()     // Catch:{ Exception -> 0x0113 }
            goto L_0x011a
        L_0x0113:
            r0 = move-exception
            boolean[] r7 = new boolean[r5]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r7)
            r0 = r2
        L_0x011a:
            boolean r7 = r0.isEmpty()
            if (r7 != 0) goto L_0x01f7
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r7 = "div.pas-list"
            org.jsoup.nodes.Element r0 = r0.h(r7)
            if (r0 == 0) goto L_0x01f7
            java.lang.String r7 = "li[data-id][data-server]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r7)
            java.util.Iterator r0 = r0.iterator()
        L_0x0136:
            boolean r7 = r0.hasNext()
            if (r7 == 0) goto L_0x01f7
            java.lang.Object r7 = r0.next()
            org.jsoup.nodes.Element r7 = (org.jsoup.nodes.Element) r7
            java.lang.String r11 = "a[title]"
            org.jsoup.nodes.Element r11 = r7.h(r11)
            if (r11 == 0) goto L_0x01ef
            java.lang.String r12 = "data-id"
            java.lang.String r12 = r7.b((java.lang.String) r12)
            java.lang.String r13 = "data-server"
            java.lang.String r7 = r7.b((java.lang.String) r13)
            java.lang.String r13 = "title"
            java.lang.String r11 = r11.b((java.lang.String) r13)
            if (r3 != 0) goto L_0x0160
            java.lang.String r11 = "HD"
        L_0x0160:
            r13 = r19
            if (r3 != 0) goto L_0x0166
            java.lang.String r12 = r13.eps
        L_0x0166:
            com.original.tase.helper.http.HttpHelper r14 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r15 = new java.lang.StringBuilder
            r15.<init>()
            java.lang.String r5 = r1.c
            r15.append(r5)
            java.lang.String r5 = "/movie_embed/"
            r15.append(r5)
            r15.append(r6)
            r15.append(r8)
            r15.append(r12)
            r15.append(r8)
            r15.append(r7)
            java.lang.String r5 = "##forceNoCache##"
            r15.append(r5)
            java.lang.String r5 = r15.toString()
            java.util.Map[] r7 = new java.util.Map[r4]
            r12 = 0
            r7[r12] = r10
            java.lang.String r5 = r14.a((java.lang.String) r5, (java.util.Map<java.lang.String, java.lang.String>[]) r7)
            java.lang.String r7 = "http 2 ------o "
            com.original.tase.Logger.a((java.lang.String) r7, (java.lang.String) r5)
            boolean r7 = r5.isEmpty()
            if (r7 != 0) goto L_0x01ec
            java.lang.String r7 = r5.trim()
            java.lang.String r7 = r7.toLowerCase()
            java.lang.String r14 = "src"
            boolean r7 = r7.contains(r14)
            if (r7 == 0) goto L_0x01ec
            java.lang.String r7 = "[\"']?src[\"']?\\s*:\\s*[\"']([^\"']+)"
            java.lang.String r5 = com.original.tase.utils.Regex.a((java.lang.String) r5, (java.lang.String) r7, (int) r4)
            java.lang.String r7 = "\\/"
            java.lang.String r5 = r5.replace(r7, r8)
            java.lang.String r7 = "\\\\"
            java.lang.String r5 = r5.replace(r7, r2)
            boolean r7 = r5.isEmpty()
            if (r7 != 0) goto L_0x01ec
            java.lang.String r7 = "http movie_episodes------ "
            com.original.tase.Logger.a((java.lang.String) r7, (java.lang.String) r5)
            com.original.tase.model.media.MediaSource r7 = new com.original.tase.model.media.MediaSource
            java.lang.String r14 = r16.a()
            java.lang.String r15 = "CDN-FastServer"
            r7.<init>(r14, r15, r9)
            r7.setStreamLink(r5)
            r7.setQuality((java.lang.String) r11)
            r7.setPlayHeader(r10)
            r5 = r17
            r5.onNext(r7)
            goto L_0x01f4
        L_0x01ec:
            r5 = r17
            goto L_0x01f4
        L_0x01ef:
            r5 = r17
            r13 = r19
            r12 = 0
        L_0x01f4:
            r5 = 0
            goto L_0x0136
        L_0x01f7:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.YesMovies.a(io.reactivex.ObservableEmitter, java.lang.String, com.movie.data.model.MovieInfo, java.lang.String):void");
    }

    /* JADX WARNING: Removed duplicated region for block: B:104:0x0246 A[Catch:{ Exception -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x02e6 A[Catch:{ Exception -> 0x034e }] */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x01fb A[SYNTHETIC, Splitter:B:85:0x01fb] */
    /* JADX WARNING: Removed duplicated region for block: B:95:0x0223 A[SYNTHETIC, Splitter:B:95:0x0223] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(com.movie.data.model.MovieInfo r20, java.lang.String r21, boolean r22) {
        /*
            r19 = this;
            r1 = r19
            r2 = r21
            java.lang.String r3 = "img[alt]"
            java.lang.String r4 = "span.mli-info"
            java.lang.Integer r0 = r20.getSession()
            int r0 = r0.intValue()
            r5 = 1
            r6 = 0
            if (r0 >= 0) goto L_0x0016
            r7 = 1
            goto L_0x0017
        L_0x0016:
            r7 = 0
        L_0x0017:
            if (r7 == 0) goto L_0x001c
            r0 = -1
            r8 = -1
            goto L_0x0025
        L_0x001c:
            java.lang.Integer r0 = r20.getSessionYear()
            int r0 = r0.intValue()
            r8 = r0
        L_0x0025:
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = r1.c
            r9.append(r10)
            java.lang.String r10 = "/user/status.html"
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            java.util.Map[] r10 = new java.util.Map[r6]
            r0.b((java.lang.String) r9, (java.util.Map<java.lang.String, java.lang.String>[]) r10)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r9 = r1.c
            r0.append(r9)
            java.lang.String r9 = "/searching/"
            r0.append(r9)
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = r20.getName()
            java.lang.String r11 = ""
            java.lang.String r12 = "Marvel's "
            java.lang.String r10 = r10.replace(r12, r11)
            java.lang.String r12 = "[^A-Za-z0-9 ]"
            java.lang.String r10 = r10.replaceAll(r12, r11)
            java.lang.String r12 = " "
            java.lang.String r13 = "  "
            java.lang.String r10 = r10.replace(r13, r12)
            java.lang.String r10 = com.original.tase.helper.TitleHelper.e(r10)
            r9.append(r10)
            if (r22 == 0) goto L_0x008c
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            r10.append(r12)
            java.lang.Integer r12 = r20.getYear()
            r10.append(r12)
            java.lang.String r10 = r10.toString()
            goto L_0x008d
        L_0x008c:
            r10 = r11
        L_0x008d:
            r9.append(r10)
            java.lang.String r9 = r9.toString()
            boolean[] r10 = new boolean[r6]
            java.lang.String r9 = com.original.tase.utils.Utils.a((java.lang.String) r9, (boolean[]) r10)
            java.lang.String r10 = "%20"
            java.lang.String r12 = "+"
            java.lang.String r9 = r9.replace(r10, r12)
            r0.append(r9)
            java.lang.String r9 = ".html"
            r0.append(r9)
            java.lang.String r9 = r0.toString()
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r12 = r1.c
            r10.append(r12)
            java.lang.String r12 = "/"
            r10.append(r12)
            java.lang.String r10 = r10.toString()
            java.lang.String r0 = r0.b((java.lang.String) r9, (java.lang.String) r10)
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.b(r0)
            java.lang.String r10 = "div.ml-item"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r10)
            java.util.Iterator r10 = r0.iterator()
        L_0x00d7:
            boolean r0 = r10.hasNext()
            if (r0 == 0) goto L_0x036f
            java.lang.Object r0 = r10.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r13 = "a[href]"
            org.jsoup.nodes.Element r13 = r0.h(r13)     // Catch:{ Exception -> 0x035a }
            if (r13 != 0) goto L_0x00ec
            goto L_0x00d7
        L_0x00ec:
            org.jsoup.select.Elements r14 = r0.g((java.lang.String) r4)     // Catch:{ Exception -> 0x035a }
            int r14 = r14.size()     // Catch:{ Exception -> 0x035a }
            if (r14 <= 0) goto L_0x0109
            org.jsoup.select.Elements r14 = r0.g((java.lang.String) r4)     // Catch:{ Exception -> 0x00ff }
            java.lang.String r14 = r14.c()     // Catch:{ Exception -> 0x00ff }
            goto L_0x010a
        L_0x00ff:
            r0 = move-exception
            r18 = r3
        L_0x0102:
            r17 = r4
            r3 = 0
            r4 = r20
            goto L_0x0362
        L_0x0109:
            r14 = r11
        L_0x010a:
            boolean r15 = r14.isEmpty()     // Catch:{ Exception -> 0x035a }
            if (r15 == 0) goto L_0x0116
            java.lang.String r14 = "title"
            java.lang.String r14 = r13.b((java.lang.String) r14)     // Catch:{ Exception -> 0x00ff }
        L_0x0116:
            java.lang.String r15 = "href"
            java.lang.String r13 = r13.b((java.lang.String) r15)     // Catch:{ Exception -> 0x035a }
            boolean r15 = r14.isEmpty()     // Catch:{ Exception -> 0x035a }
            if (r15 != 0) goto L_0x0352
            boolean r15 = r13.isEmpty()     // Catch:{ Exception -> 0x035a }
            if (r15 != 0) goto L_0x0352
            java.lang.String r15 = "span.mli-eps"
            org.jsoup.select.Elements r15 = r0.g((java.lang.String) r15)     // Catch:{ Exception -> 0x035a }
            int r15 = r15.size()     // Catch:{ Exception -> 0x035a }
            if (r15 <= 0) goto L_0x0136
            r15 = 1
            goto L_0x0137
        L_0x0136:
            r15 = 0
        L_0x0137:
            if (r7 == 0) goto L_0x013b
            if (r15 != 0) goto L_0x0352
        L_0x013b:
            if (r7 != 0) goto L_0x013f
            if (r15 == 0) goto L_0x0352
        L_0x013f:
            org.jsoup.select.Elements r15 = r0.g((java.lang.String) r3)     // Catch:{ Exception -> 0x035a }
            int r15 = r15.size()     // Catch:{ Exception -> 0x035a }
            if (r15 <= 0) goto L_0x0154
            org.jsoup.nodes.Element r15 = r0.h(r3)     // Catch:{ Exception -> 0x00ff }
            java.lang.String r6 = "alt"
            java.lang.String r6 = r15.b((java.lang.String) r6)     // Catch:{ Exception -> 0x00ff }
            goto L_0x0155
        L_0x0154:
            r6 = r11
        L_0x0155:
            java.lang.String r15 = "\\s*-\\s*(\\s*\\d{4})\\s*$"
            java.lang.String r15 = com.original.tase.utils.Regex.a((java.lang.String) r6, (java.lang.String) r15, (int) r5)     // Catch:{ Exception -> 0x035a }
            boolean r17 = r15.isEmpty()     // Catch:{ Exception -> 0x035a }
            if (r17 == 0) goto L_0x0167
            java.lang.String r15 = "\\s+\\(\\s*(\\d{4})\\s*\\)$"
            java.lang.String r15 = com.original.tase.utils.Regex.a((java.lang.String) r6, (java.lang.String) r15, (int) r5)     // Catch:{ Exception -> 0x00ff }
        L_0x0167:
            boolean r6 = r15.isEmpty()     // Catch:{ Exception -> 0x035a }
            java.lang.String r5 = "http"
            if (r6 == 0) goto L_0x01e4
            java.lang.String r6 = "a[data-url*=\"movie_info\"]"
            org.jsoup.nodes.Element r0 = r0.h(r6)     // Catch:{ Exception -> 0x00ff }
            if (r0 == 0) goto L_0x01e4
            java.lang.String r6 = "data-url"
            java.lang.String r0 = r0.b((java.lang.String) r6)     // Catch:{ Exception -> 0x00ff }
            java.lang.String r6 = "movie_info"
            boolean r6 = r0.contains(r6)     // Catch:{ Exception -> 0x00ff }
            if (r6 == 0) goto L_0x01e4
            boolean r6 = r0.startsWith(r12)     // Catch:{ Exception -> 0x00ff }
            if (r6 == 0) goto L_0x019c
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ff }
            r6.<init>()     // Catch:{ Exception -> 0x00ff }
            java.lang.String r15 = r1.c     // Catch:{ Exception -> 0x00ff }
            r6.append(r15)     // Catch:{ Exception -> 0x00ff }
            r6.append(r0)     // Catch:{ Exception -> 0x00ff }
            java.lang.String r0 = r6.toString()     // Catch:{ Exception -> 0x00ff }
        L_0x019c:
            boolean r6 = r0.startsWith(r5)     // Catch:{ Exception -> 0x00ff }
            if (r6 != 0) goto L_0x01b6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00ff }
            r6.<init>()     // Catch:{ Exception -> 0x00ff }
            java.lang.String r15 = r1.c     // Catch:{ Exception -> 0x00ff }
            r6.append(r15)     // Catch:{ Exception -> 0x00ff }
            r6.append(r12)     // Catch:{ Exception -> 0x00ff }
            r6.append(r0)     // Catch:{ Exception -> 0x00ff }
            java.lang.String r0 = r6.toString()     // Catch:{ Exception -> 0x00ff }
        L_0x01b6:
            com.original.tase.helper.http.HttpHelper r6 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x00ff }
            r18 = r3
            r15 = 1
            java.util.Map[] r3 = new java.util.Map[r15]     // Catch:{ Exception -> 0x01e1 }
            java.util.HashMap r15 = com.original.Constants.a()     // Catch:{ Exception -> 0x01e1 }
            r16 = 0
            r3[r16] = r15     // Catch:{ Exception -> 0x01e1 }
            java.lang.String r0 = r6.b(r0, r9, r3)     // Catch:{ Exception -> 0x01e1 }
            java.lang.String r3 = "\\\""
            java.lang.String r6 = "\""
            java.lang.String r0 = r0.replace(r3, r6)     // Catch:{ Exception -> 0x01e1 }
            java.lang.String r3 = "\\/"
            java.lang.String r0 = r0.replace(r3, r12)     // Catch:{ Exception -> 0x01e1 }
            java.lang.String r3 = "<div\\s+[^>]*class=\"jt-info\"[^>]*>\\s*(\\d{4})\\s*</div>"
            r6 = 1
            java.lang.String r15 = com.original.tase.utils.Regex.a((java.lang.String) r0, (java.lang.String) r3, (int) r6, (boolean) r6)     // Catch:{ Exception -> 0x01e1 }
            goto L_0x01e6
        L_0x01e1:
            r0 = move-exception
            goto L_0x0102
        L_0x01e4:
            r18 = r3
        L_0x01e6:
            java.lang.String r0 = "</?h2>"
            java.lang.String r0 = r14.replaceAll(r0, r11)     // Catch:{ Exception -> 0x0350 }
            java.lang.String r3 = "\\s+\\(?\\d{4}\\)?$"
            java.lang.String r3 = r0.replaceAll(r3, r11)     // Catch:{ Exception -> 0x0350 }
            java.lang.String r0 = "\\s*(?:\\:|-)?\\s*(?:S|s)eason\\s*(\\d+)"
            r6 = 1
            java.lang.String r14 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r0, (int) r6)     // Catch:{ Exception -> 0x0350 }
            if (r7 != 0) goto L_0x021f
            boolean r0 = r14.equals(r2)     // Catch:{ Exception -> 0x0215 }
            if (r0 != 0) goto L_0x021f
            int r0 = java.lang.Integer.parseInt(r14)     // Catch:{ Exception -> 0x0215 }
            java.lang.String r0 = com.original.tase.utils.Utils.a((int) r0)     // Catch:{ Exception -> 0x0215 }
            int r17 = java.lang.Integer.parseInt(r21)     // Catch:{ Exception -> 0x0215 }
            java.lang.String r6 = com.original.tase.utils.Utils.a((int) r17)     // Catch:{ Exception -> 0x0215 }
            r0.equals(r6)     // Catch:{ Exception -> 0x0215 }
            goto L_0x021f
        L_0x0215:
            r0 = move-exception
            r17 = r4
            r6 = 0
            boolean[] r4 = new boolean[r6]     // Catch:{ Exception -> 0x034e }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r4)     // Catch:{ Exception -> 0x034e }
            goto L_0x0221
        L_0x021f:
            r17 = r4
        L_0x0221:
            if (r7 != 0) goto L_0x0244
            boolean r0 = r14.equals(r2)     // Catch:{ Exception -> 0x023d }
            if (r0 != 0) goto L_0x0244
            int r0 = java.lang.Integer.parseInt(r14)     // Catch:{ Exception -> 0x023d }
            java.lang.String r0 = com.original.tase.utils.Utils.a((int) r0)     // Catch:{ Exception -> 0x023d }
            int r4 = java.lang.Integer.parseInt(r21)     // Catch:{ Exception -> 0x023d }
            java.lang.String r4 = com.original.tase.utils.Utils.a((int) r4)     // Catch:{ Exception -> 0x023d }
            r0.equals(r4)     // Catch:{ Exception -> 0x023d }
            goto L_0x0244
        L_0x023d:
            r0 = move-exception
            r4 = 0
            boolean[] r6 = new boolean[r4]     // Catch:{ Exception -> 0x034e }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r6)     // Catch:{ Exception -> 0x034e }
        L_0x0244:
            if (r7 == 0) goto L_0x02e6
            java.lang.String r0 = r20.getName()     // Catch:{ Exception -> 0x034e }
            java.lang.String r0 = com.original.tase.helper.TitleHelper.f(r0)     // Catch:{ Exception -> 0x034e }
            java.lang.String r4 = "\\s*(?:\\:|-)?\\s*(?:S|s)eason\\s*\\d+"
            java.lang.String r3 = r3.replaceAll(r4, r11)     // Catch:{ Exception -> 0x034e }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r3)     // Catch:{ Exception -> 0x034e }
            boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x034e }
            if (r0 == 0) goto L_0x0356
            java.lang.String r0 = r15.trim()     // Catch:{ Exception -> 0x034e }
            boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x034e }
            if (r0 != 0) goto L_0x02b2
            java.lang.String r0 = r15.trim()     // Catch:{ Exception -> 0x034e }
            boolean r0 = com.original.tase.utils.Utils.b(r0)     // Catch:{ Exception -> 0x034e }
            if (r0 == 0) goto L_0x02b2
            java.lang.Integer r0 = r20.getYear()     // Catch:{ Exception -> 0x034e }
            int r0 = r0.intValue()     // Catch:{ Exception -> 0x034e }
            if (r0 <= 0) goto L_0x02b2
            if (r7 == 0) goto L_0x0290
            java.lang.String r0 = r15.trim()     // Catch:{ Exception -> 0x034e }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x034e }
            java.lang.Integer r3 = r20.getYear()     // Catch:{ Exception -> 0x034e }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x034e }
            if (r0 == r3) goto L_0x02b2
        L_0x0290:
            if (r7 != 0) goto L_0x02a6
            if (r8 > 0) goto L_0x02a6
            java.lang.String r0 = r15.trim()     // Catch:{ Exception -> 0x034e }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x034e }
            java.lang.Integer r3 = r20.getYear()     // Catch:{ Exception -> 0x034e }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x034e }
            if (r0 == r3) goto L_0x02b2
        L_0x02a6:
            if (r7 != 0) goto L_0x0356
            java.lang.String r0 = r15.trim()     // Catch:{ Exception -> 0x034e }
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ Exception -> 0x034e }
            if (r0 != r8) goto L_0x0356
        L_0x02b2:
            boolean r0 = r13.startsWith(r12)     // Catch:{ Exception -> 0x034e }
            if (r0 == 0) goto L_0x02ca
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x034e }
            r0.<init>()     // Catch:{ Exception -> 0x034e }
            java.lang.String r3 = r1.c     // Catch:{ Exception -> 0x034e }
            r0.append(r3)     // Catch:{ Exception -> 0x034e }
            r0.append(r13)     // Catch:{ Exception -> 0x034e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x034e }
            return r0
        L_0x02ca:
            boolean r0 = r13.startsWith(r5)     // Catch:{ Exception -> 0x034e }
            if (r0 == 0) goto L_0x02d1
            return r13
        L_0x02d1:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x034e }
            r0.<init>()     // Catch:{ Exception -> 0x034e }
            java.lang.String r3 = r1.c     // Catch:{ Exception -> 0x034e }
            r0.append(r3)     // Catch:{ Exception -> 0x034e }
            r0.append(r12)     // Catch:{ Exception -> 0x034e }
            r0.append(r13)     // Catch:{ Exception -> 0x034e }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x034e }
            return r0
        L_0x02e6:
            java.lang.String r0 = com.original.tase.helper.TitleHelper.f(r3)     // Catch:{ Exception -> 0x034e }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x034e }
            r3.<init>()     // Catch:{ Exception -> 0x034e }
            java.lang.String r4 = r20.getName()     // Catch:{ Exception -> 0x034e }
            r3.append(r4)     // Catch:{ Exception -> 0x034e }
            java.lang.String r4 = "season"
            r3.append(r4)     // Catch:{ Exception -> 0x034e }
            r4 = r20
            java.lang.String r6 = r4.session     // Catch:{ Exception -> 0x034c }
            r3.append(r6)     // Catch:{ Exception -> 0x034c }
            java.lang.String r3 = r3.toString()     // Catch:{ Exception -> 0x034c }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.e(r3)     // Catch:{ Exception -> 0x034c }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r3)     // Catch:{ Exception -> 0x034c }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r3)     // Catch:{ Exception -> 0x034c }
            boolean r0 = r0.equals(r3)     // Catch:{ Exception -> 0x034c }
            if (r0 == 0) goto L_0x0358
            boolean r0 = r13.startsWith(r12)     // Catch:{ Exception -> 0x034c }
            if (r0 == 0) goto L_0x0330
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x034c }
            r0.<init>()     // Catch:{ Exception -> 0x034c }
            java.lang.String r3 = r1.c     // Catch:{ Exception -> 0x034c }
            r0.append(r3)     // Catch:{ Exception -> 0x034c }
            r0.append(r13)     // Catch:{ Exception -> 0x034c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x034c }
            return r0
        L_0x0330:
            boolean r0 = r13.startsWith(r5)     // Catch:{ Exception -> 0x034c }
            if (r0 == 0) goto L_0x0337
            return r13
        L_0x0337:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x034c }
            r0.<init>()     // Catch:{ Exception -> 0x034c }
            java.lang.String r3 = r1.c     // Catch:{ Exception -> 0x034c }
            r0.append(r3)     // Catch:{ Exception -> 0x034c }
            r0.append(r12)     // Catch:{ Exception -> 0x034c }
            r0.append(r13)     // Catch:{ Exception -> 0x034c }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x034c }
            return r0
        L_0x034c:
            r0 = move-exception
            goto L_0x0361
        L_0x034e:
            r0 = move-exception
            goto L_0x035f
        L_0x0350:
            r0 = move-exception
            goto L_0x035d
        L_0x0352:
            r18 = r3
            r17 = r4
        L_0x0356:
            r4 = r20
        L_0x0358:
            r3 = 0
            goto L_0x0367
        L_0x035a:
            r0 = move-exception
            r18 = r3
        L_0x035d:
            r17 = r4
        L_0x035f:
            r4 = r20
        L_0x0361:
            r3 = 0
        L_0x0362:
            boolean[] r5 = new boolean[r3]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r5)
        L_0x0367:
            r4 = r17
            r3 = r18
            r5 = 1
            r6 = 0
            goto L_0x00d7
        L_0x036f:
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.YesMovies.a(com.movie.data.model.MovieInfo, java.lang.String, boolean):java.lang.String");
    }
}
