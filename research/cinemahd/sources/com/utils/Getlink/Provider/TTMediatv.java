package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.vungle.warren.model.CookieDBAdapter;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.lang3.StringEscapeUtils;

public class TTMediatv extends BaseProvider {
    public String c = Utils.getProvider(31);

    public String a() {
        return "TTMediatv";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        String a2 = com.original.tase.utils.Utils.a(movieInfo.name, new boolean[0]);
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        HashMap hashMap = new HashMap();
        hashMap.put(CookieDBAdapter.CookieColumns.TABLE_NAME, HttpHelper.e().a(this.c));
        HttpHelper e = HttpHelper.e();
        String a3 = e.a(this.c + "/oz/search/" + a2 + "/groups", (Map<String, String>[]) new Map[]{hashMap});
        Iterator it2 = Regex.b(a3, "['\"]id['\"]\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1).get(0).iterator();
        Iterator it3 = Regex.b(a3, "['\"]title['\"]\\s*:\\s*['\"]([^'\"]+[^'\"])['\"]", 1).get(0).iterator();
        Iterator it4 = Regex.b(a3, "['\"]year['\"]\\s*:\\s*(\\d+)", 1).get(0).iterator();
        while (it3.hasNext()) {
            try {
                String obj = it3.next().toString();
                String obj2 = it2.next().toString();
                String obj3 = it4.next().toString();
                if (obj.equals(movieInfo.name) && movieInfo.year.equals(obj3)) {
                    return this.c + "/movies/" + obj2 + "/" + TitleHelper.a(movieInfo.name.toLowerCase(), "_");
                }
            } catch (Throwable unused) {
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        Iterator it2 = Regex.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]), "['\"]?url['\"]?\\s*:\\s*['\"]([^'\"]+)", 1, true).get(0).iterator();
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        while (it2.hasNext()) {
            String a2 = StringEscapeUtils.a((String) it2.next());
            if (a2.contains("m3u8") || a2.contains("token")) {
                MediaSource mediaSource = new MediaSource(a(), "CDN", false);
                mediaSource.setStreamLink(a2);
                mediaSource.setPlayHeader(hashMap);
                mediaSource.setQuality("HD");
                observableEmitter.onNext(mediaSource);
            }
        }
    }
}
