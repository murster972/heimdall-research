package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Dizist extends BaseProvider {
    private String c = Utils.getProvider(99);
    private HashMap d = new HashMap();

    public String a() {
        return "Dizist";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String str2;
        MovieInfo movieInfo2 = movieInfo;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String e = TitleHelper.e(TitleHelper.g(movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "")).replace("'", "-").replace("P.D.", "PD"));
        String str3 = this.c + "/izle/" + e + "-" + movieInfo2.session + "-sezon-" + movieInfo2.eps + "-bolum/";
        this.d.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        this.d.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        this.d.put("accept-language", "en-US;q=0.8,en;q=0.7");
        String str4 = "referer";
        this.d.put(str4, this.c + "/dizi/" + e + "/");
        int i = 13000;
        int i2 = 1;
        char c2 = 0;
        try {
            str = Jsoup.a(str3).a(Constants.f5838a).a(13000).execute().body();
        } catch (IOException unused) {
            str = HttpHelper.e().a(str3, (Map<String, String>[]) new Map[]{this.d});
        }
        Document b = Jsoup.b(str);
        this.d.put(str4, str3);
        Iterator it2 = b.g("div.span-nine.pull-left").b("a").iterator();
        while (it2.hasNext()) {
            String b2 = ((Element) it2.next()).b("href");
            if (b2.startsWith("//")) {
                b2 = "http:" + b2;
            } else if (b2.startsWith("/")) {
                b2 = this.c + b2;
            }
            try {
                str2 = Jsoup.a(b2).a(Constants.f5838a).a(i).execute().body();
            } catch (IOException unused2) {
                HttpHelper e2 = HttpHelper.e();
                Map[] mapArr = new Map[i2];
                mapArr[c2] = this.d;
                str2 = e2.a(b2, (Map<String, String>[]) mapArr);
            }
            this.d.put(str4, b2);
            Iterator it3 = Jsoup.b(str2).g("div.embed-responsive-item").iterator();
            while (it3.hasNext()) {
                Element element = (Element) it3.next();
                Iterator it4 = element.g("iframe[src]").iterator();
                while (it4.hasNext()) {
                    String str5 = "src";
                    String b3 = ((Element) it4.next()).b(str5);
                    if (b3.startsWith("//")) {
                        b3 = "https:" + b3;
                    } else if (b3.startsWith("/")) {
                        b3 = this.c + b3;
                    }
                    if (b3.contains("specialcdn")) {
                        Iterator it5 = Jsoup.b(HttpHelper.e().a(b3, (Map<String, String>[]) new Map[0])).g("source").iterator();
                        while (it5.hasNext()) {
                            HashMap hashMap = new HashMap();
                            hashMap.put("Referer", this.c);
                            hashMap.put("User-Agent", Constants.f5838a);
                            Element element2 = (Element) it5.next();
                            String b4 = element2.b(str5);
                            String b5 = element2.b("label");
                            String str6 = str5;
                            if (!b5.toLowerCase().contains("hd")) {
                                b5 = "HQ";
                            }
                            String str7 = str4;
                            Iterator it6 = it5;
                            MediaSource mediaSource = new MediaSource(a(), b4.contains(".fbcdn.") ? "FB-CDN" : "CDN", false);
                            mediaSource.setStreamLink(b4);
                            mediaSource.setQuality(b5);
                            if (!b4.contains(".fbcdn.")) {
                                mediaSource.setPlayHeader(hashMap);
                            }
                            observableEmitter2.onNext(mediaSource);
                            str5 = str6;
                            str4 = str7;
                            it5 = it6;
                        }
                    } else {
                        String str8 = str4;
                        a(observableEmitter2, b3, "HD", new boolean[0]);
                        String a2 = Regex.a(b3, "okru(?:\\?|\\&)e=(\\d+)(?:$|\\&)", 1);
                        if (!a2.isEmpty()) {
                            a(observableEmitter2, "https://www.ok.ru/video/" + a2, "HD", new boolean[0]);
                        }
                        if (b3.contains("/okru")) {
                            Iterator it7 = Jsoup.b(HttpHelper.e().b(b3, str3)).g("div[data-module][data-options]").iterator();
                            while (it7.hasNext()) {
                                String a3 = Regex.a(((Element) it7.next()).b("data-options"), "['\"]([^'\"]*//[^'\"]+\\.ru/video/[^'\"]+)['\"]", 1, true);
                                if (!a3.isEmpty()) {
                                    a(observableEmitter2, a3, "HD", new boolean[0]);
                                }
                            }
                        }
                        str4 = str8;
                    }
                }
                String str9 = str4;
                String str10 = str3;
                String a4 = Regex.a(element.toString().replace("\\/", "/").replace("\\\"", "\""), "videoembed/([\\d-]+)", 1, 34);
                if (a4.isEmpty()) {
                    a4 = Regex.a(element.toString().replace("\\/", "/").replace("\\\"", "\""), "videoembed/(\\d+)", 1, 34);
                }
                if (!a4.isEmpty()) {
                    a(observableEmitter2, "https://www.ok.ru/video/" + a4, "HD", new boolean[0]);
                }
                str3 = str10;
                str4 = str9;
                i = 13000;
                i2 = 1;
                c2 = 0;
            }
            String str11 = str4;
        }
    }
}
