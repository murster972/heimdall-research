package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class ExtraWiki extends BaseProvider {
    public String c = Utils.getProvider(15);
    public String d = "HQ";

    public String a() {
        return "ExtraWiki";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    public String b(MovieInfo movieInfo) {
        int intValue = movieInfo.getType().intValue();
        String a2 = TitleHelper.a(movieInfo.name, "+");
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.c);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/?s=" + a2, (Map<String, String>[]) new Map[]{hashMap})).g("h2.entry-title").b("a").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String b = element.b("title");
            if (TitleHelper.a(b, "").startsWith(TitleHelper.a(movieInfo.name + "" + movieInfo.year, ""))) {
                String b2 = element.b("href");
                if (b2.startsWith("/")) {
                    b2 = this.c + b2;
                }
                this.d = Regex.a(b, "(\\d{3,4}p)", 1);
                if (this.d.isEmpty()) {
                    this.d = "HQ";
                }
                return b2;
            }
        }
        return "";
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        String str2;
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0])).g("div.ttdbox").b("h4").b("a").iterator();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).b("href");
            String a2 = Regex.a(b, "url=(.*)", 1);
            if (!a2.isEmpty()) {
                String c2 = c(b, a2);
                if (!c2.isEmpty()) {
                    a(observableEmitter, c2, this.d, false);
                }
            } else {
                String a3 = Regex.a(b, "link=(.*)", 1);
                if (!a3.isEmpty()) {
                    try {
                        str2 = new String(Base64.decode(a3, 8), "UTF-8");
                    } catch (Exception e) {
                        Logger.a((Throwable) e, new boolean[0]);
                        try {
                            str2 = new String(Base64.decode(a3, 8));
                        } catch (Exception e2) {
                            Logger.a((Throwable) e2, new boolean[0]);
                            str2 = b;
                        }
                    }
                    if (str2 != null && !str2.isEmpty()) {
                        a(observableEmitter, str2, this.d, false);
                    }
                }
            }
        }
    }
}
