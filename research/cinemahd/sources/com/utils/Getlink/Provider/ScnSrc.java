package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.search.SearchHelper;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class ScnSrc extends BaseProvider {
    private String c = Utils.getProvider(85);
    public HashMap d = new HashMap();

    public ScnSrc() {
        this.d.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        this.d.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.9,en;q=0.8");
        this.d.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        this.d.put("User-Agent", Constants.f5838a);
    }

    private boolean j(String str) {
        String lowerCase = str.toLowerCase();
        return !lowerCase.contains("sample") && !lowerCase.contains("uploadkadeh") && !lowerCase.contains("wordpress") && !lowerCase.contains("crazy4tv") && !lowerCase.contains("imdb.com") && !lowerCase.contains("youtube") && !lowerCase.contains("userboard") && !lowerCase.contains("kumpulbagi") && !lowerCase.contains("mexashare") && !lowerCase.contains("myvideolink.xyz") && !lowerCase.contains("myvideolinks.xyz") && !lowerCase.contains("costaction") && !lowerCase.contains("crazydl") && !lowerCase.contains(".rar") && !lowerCase.contains(".avi") && !lowerCase.contains(".flv") && !lowerCase.contains("ul.to") && !lowerCase.contains("safelinking") && !lowerCase.contains("linx.") && !lowerCase.contains("upload.so") && !lowerCase.contains(".zip") && !lowerCase.contains("go4up") && !lowerCase.contains("adf.ly") && !lowerCase.contains(".jpg") && !lowerCase.contains(".jpeg") && !lowerCase.contains(".png") && !lowerCase.contains(".txt") && !lowerCase.contains("file-upload.") && !lowerCase.contains(".subs") && !lowerCase.contains(".7z") && !lowerCase.contains(".iso");
    }

    public String a() {
        return "ScnSrc";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            List<String> a2 = a(movieInfo, "-1", "-1");
            if (a2.isEmpty()) {
                String str = movieInfo.name;
                String str2 = movieInfo.year;
                a2 = SearchHelper.c(str, str2, str2, this.c, "");
            }
            a(observableEmitter, movieInfo, a2, "");
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            String str = "S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo.eps));
            List<String> a2 = a(movieInfo, movieInfo.session, movieInfo.eps);
            if (a2.isEmpty()) {
                a2 = SearchHelper.c(movieInfo.name, movieInfo.year, str, this.c, "");
            }
            a(observableEmitter, movieInfo, a2, str);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:43:0x00d9, code lost:
        if (r2.equalsIgnoreCase("SD") != false) goto L_0x00db;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r19, com.movie.data.model.MovieInfo r20, java.util.List<java.lang.String> r21, java.lang.String r22) {
        /*
            r18 = this;
            r7 = r18
            java.lang.Integer r0 = r20.getType()
            int r0 = r0.intValue()
            r8 = 1
            if (r0 != r8) goto L_0x0012
            java.lang.Integer r0 = java.lang.Integer.valueOf(r8)
            goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            r9 = r0
            com.original.tase.helper.DirectoryIndexHelper r10 = new com.original.tase.helper.DirectoryIndexHelper
            r10.<init>()
            java.util.ArrayList r11 = new java.util.ArrayList
            r11.<init>()
            java.util.Iterator r12 = r21.iterator()
            r13 = 0
            r0 = 0
        L_0x0024:
            boolean r1 = r12.hasNext()
            if (r1 == 0) goto L_0x012f
            java.lang.Object r1 = r12.next()
            java.lang.String r1 = (java.lang.String) r1
            com.original.tase.helper.http.HttpHelper r2 = com.original.tase.helper.http.HttpHelper.e()
            java.util.Map[] r3 = new java.util.Map[r8]
            java.util.HashMap r4 = r7.d
            r3[r13] = r4
            java.lang.String r1 = r2.a((java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r3)
            org.jsoup.nodes.Document r1 = org.jsoup.Jsoup.b(r1)
            java.lang.String r2 = "div.comm_content"
            org.jsoup.select.Elements r14 = r1.g((java.lang.String) r2)
            r1 = r0
            r15 = 0
        L_0x004a:
            int r0 = r14.size()
            if (r15 >= r0) goto L_0x012c
            java.lang.Object r0 = r14.get(r15)     // Catch:{ all -> 0x0124 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ all -> 0x0124 }
            java.lang.String r2 = "a[href]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r2)     // Catch:{ all -> 0x0124 }
            java.util.Iterator r16 = r0.iterator()     // Catch:{ all -> 0x0124 }
        L_0x0060:
            boolean r0 = r16.hasNext()     // Catch:{ all -> 0x0124 }
            if (r0 == 0) goto L_0x0120
            java.lang.Object r0 = r16.next()     // Catch:{ all -> 0x0118 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ all -> 0x0118 }
            java.lang.String r2 = "href"
            java.lang.String r3 = r0.b((java.lang.String) r2)     // Catch:{ all -> 0x0118 }
            if (r9 != 0) goto L_0x008f
            java.lang.String r0 = "([s|S]\\d+[e|E]\\d+)"
            java.lang.String r0 = com.original.tase.utils.Regex.a((java.lang.String) r3, (java.lang.String) r0, (int) r8)     // Catch:{ all -> 0x0118 }
            boolean r2 = r0.isEmpty()     // Catch:{ all -> 0x0118 }
            if (r2 != 0) goto L_0x008f
            java.lang.String r0 = r0.toLowerCase()     // Catch:{ all -> 0x0118 }
            java.lang.String r2 = r22.toLowerCase()     // Catch:{ all -> 0x0118 }
            boolean r0 = r0.equals(r2)     // Catch:{ all -> 0x0118 }
            if (r0 != 0) goto L_0x008f
            goto L_0x0060
        L_0x008f:
            boolean r0 = r7.j(r3)     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x0060
            boolean r0 = r11.contains(r3)     // Catch:{ all -> 0x0118 }
            if (r0 != 0) goto L_0x0060
            r11.add(r3)     // Catch:{ all -> 0x0118 }
            java.lang.String r0 = ""
            java.lang.String r2 = ".html"
            if (r9 == 0) goto L_0x00ad
            java.lang.String r0 = r3.replace(r2, r0)     // Catch:{ all -> 0x0118 }
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r0 = r10.a(r0)     // Catch:{ all -> 0x0118 }
            goto L_0x00b5
        L_0x00ad:
            java.lang.String r0 = r3.replace(r2, r0)     // Catch:{ all -> 0x0118 }
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r0 = r10.b(r0)     // Catch:{ all -> 0x0118 }
        L_0x00b5:
            java.lang.String r2 = r18.a()     // Catch:{ all -> 0x0118 }
            java.lang.String r4 = "HQ"
            if (r0 == 0) goto L_0x00cb
            java.lang.String r2 = r0.c()     // Catch:{ all -> 0x0118 }
            java.lang.String r0 = r0.b()     // Catch:{ all -> 0x0118 }
            java.lang.String r0 = r7.f(r0)     // Catch:{ all -> 0x0118 }
            r5 = r0
            goto L_0x00cd
        L_0x00cb:
            r5 = r2
            r2 = r4
        L_0x00cd:
            boolean r0 = r2.equalsIgnoreCase(r4)     // Catch:{ all -> 0x0118 }
            java.lang.String r6 = "SD"
            if (r0 != 0) goto L_0x00db
            boolean r0 = r2.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x00f1
        L_0x00db:
            java.lang.String r0 = "1080"
            boolean r0 = r3.contains(r0)     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x00e6
            java.lang.String r0 = "1080p"
            goto L_0x00f2
        L_0x00e6:
            java.lang.String r0 = "720"
            boolean r0 = r3.contains(r0)     // Catch:{ all -> 0x0118 }
            if (r0 == 0) goto L_0x00f1
            java.lang.String r0 = "HD"
            goto L_0x00f2
        L_0x00f1:
            r0 = r2
        L_0x00f2:
            boolean r2 = r0.equalsIgnoreCase(r4)     // Catch:{ all -> 0x0118 }
            if (r2 != 0) goto L_0x00fe
            boolean r2 = r0.equalsIgnoreCase(r6)     // Catch:{ all -> 0x0118 }
            if (r2 == 0) goto L_0x0104
        L_0x00fe:
            r2 = 40
            if (r1 >= r2) goto L_0x0104
            int r1 = r1 + 1
        L_0x0104:
            r17 = r1
            boolean[] r6 = new boolean[r13]     // Catch:{ all -> 0x0114 }
            r1 = r18
            r2 = r19
            r4 = r0
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ all -> 0x0114 }
            r1 = r17
            goto L_0x0060
        L_0x0114:
            r0 = move-exception
            r1 = r17
            goto L_0x0119
        L_0x0118:
            r0 = move-exception
        L_0x0119:
            boolean[] r2 = new boolean[r13]     // Catch:{ all -> 0x0124 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ all -> 0x0124 }
            goto L_0x0060
        L_0x0120:
            int r15 = r15 + 1
            goto L_0x004a
        L_0x0124:
            r0 = move-exception
            boolean[] r2 = new boolean[r13]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)
            goto L_0x004a
        L_0x012c:
            r0 = r1
            goto L_0x0024
        L_0x012f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.ScnSrc.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo, java.util.List, java.lang.String):void");
    }

    private List<String> a(MovieInfo movieInfo, String str, String str2) {
        String str3;
        String str4;
        boolean z = movieInfo.getType().intValue() == 1;
        if (z) {
            str3 = "";
        } else {
            str3 = "S" + com.original.tase.utils.Utils.a(Integer.parseInt(str)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(str2));
        }
        ArrayList arrayList = new ArrayList();
        if (z) {
            str4 = TitleHelper.e(movieInfo.getName().replace("'", "")) + " " + movieInfo.getYear();
        } else {
            str4 = TitleHelper.e(movieInfo.getName().replace("'", "")) + " " + str3;
        }
        String str5 = this.c + "/?s=" + com.original.tase.utils.Utils.a(str4, new boolean[0]) + "&x=0&y=0";
        String b = HttpHelper.e().b(str5, this.c + "/", this.d);
        if (b.contains("Attention Required! | Cloudflare")) {
            Logger.a("Need Verify Recaptcha", str5);
            Utils.a(str5, BaseProvider.h(str5));
        }
        if (b.contains("403 Forbidden") || !b.contains("post")) {
            b = HttpHelper.e().b(this.c + "/?s=" + com.original.tase.utils.Utils.a(str4, new boolean[0]) + "&x=12&y=14", this.c + "/", this.d);
        }
        if (b.contains("403 Forbidden") || !b.contains("post")) {
            b = HttpHelper.e().b(this.c + "/?s=" + com.original.tase.utils.Utils.a(str4, new boolean[0]), this.c + "/", this.d);
        }
        Iterator it2 = Jsoup.b(b).g("div.post").iterator();
        while (it2.hasNext()) {
            try {
                Element h = ((Element) it2.next()).h("a[href][title]");
                if (h != null) {
                    String b2 = h.b("href");
                    String replaceAll = h.b("title").replaceAll("\\<[uibp]\\>", "").replaceAll("\\</[uibp]\\>", "");
                    String lowerCase = replaceAll.toLowerCase();
                    if (!z || (!lowerCase.contains(" cam") && !lowerCase.contains("cam ") && !lowerCase.contains("hdts ") && !lowerCase.contains(" hdts") && !lowerCase.contains(" ts ") && !lowerCase.contains(" telesync") && !lowerCase.contains("telesync ") && !lowerCase.contains("hdtc ") && !lowerCase.contains(" hdtc") && !lowerCase.contains(" tc ") && !lowerCase.contains(" telecine") && !lowerCase.contains("telecine "))) {
                        if (b2.startsWith("/")) {
                            b2 = this.c + b2;
                        }
                        if (replaceAll.toLowerCase().startsWith("goto")) {
                            replaceAll = replaceAll.substring(4, replaceAll.length()).trim();
                        }
                        if (z) {
                            String f = TitleHelper.f(replaceAll);
                            StringBuilder sb = new StringBuilder();
                            sb.append(movieInfo.getName());
                            try {
                                sb.append(movieInfo.year);
                                if (f.startsWith(TitleHelper.f(TitleHelper.e(sb.toString())))) {
                                    arrayList.add(b2);
                                }
                            } catch (Throwable th) {
                                th = th;
                                Logger.a(th, new boolean[0]);
                            }
                        } else {
                            MovieInfo movieInfo2 = movieInfo;
                            if (TitleHelper.f(replaceAll).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName()))) && replaceAll.contains(str3)) {
                                arrayList.add(b2);
                            }
                        }
                    }
                }
                MovieInfo movieInfo3 = movieInfo;
            } catch (Throwable th2) {
                th = th2;
                MovieInfo movieInfo4 = movieInfo;
                Logger.a(th, new boolean[0]);
            }
        }
        return arrayList;
    }
}
