package com.utils.Getlink.Provider;

import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventTypes;
import com.facebook.common.util.UriUtil;
import com.facebook.react.uimanager.ViewProps;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.alldebrid.AllDebridUserApi;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeUserApi;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import com.unity3d.ads.metadata.MediationMetaData;
import com.utils.Getlink.Resolver.BaseResolver;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public abstract class BaseProvider {
    public static BaseProvider[] b = {new ZeroTV(), new RemoteJS(), new Hd4movies(), new Hd3movies(), new Hd2movies(), new Hdmovies(), new Afdah(), new M4UFree(), new SeeHD(), new XMovies8(), new Filmxy(), new PFTV(), new MyWS(), new Movie25V2(), new DayT(), new PLockerSK(), new WatchEpisodes(), new MiraDeTodo(), new PrimeWire(), new TV21(), new YesMovies(), new SolarMoviez(), new VexMovies(), new BobMovies(), new TheYM(), new Wawani(), new Pubfilm(), new AnimeShow(), new Cokepopcorn(), new FliXanityBased(), new dunia21(), new bioskopkeren(), new Fmovies(), new Hdpopcorns(), new OneMovie(), new NovaMovie(), new GoFilms4u(), new Hdmega(), new ReleaseBB(), new ScnSrc(), new TwoDDL(), new WrzCraft(), new DDLValley(), new YesGG(), new SpaceMov(), new GoGoAnime(), new TTMediatv(), new FmovieCloud(), new SeriesNine(), new Sezonlukdizi(), new GoGoMovies(), new DDLTV(), new WatchSeries(), new Vidics(), new AfdahTV(), new NTMovies(), new GoMovies(), new OnePutlocker(), new SwatchSeries(), new TwoPutlocker(), new Dizigold(), new OdbMovies(), new FFilms(), new Dizist(), new Dizilab(), new VioozGo(), new AZMovies(), new TopEuroPix(), new MovieFileHD(), new ClickMovies(), new DaxivMovies(), new Cinema(), new WatchMovieHD(), new BestFlix(), new LordMovies(), new CloudMovies(), new MovieGL(), new WorldUS(), new BestTvFix(), new BestFilms(), new CMovies(), new PHMovies(), new WatchFMovies(), new VidTv(), new OneLMovie(), new T1337x(), new TTPB(), new CDNRelease(), new MoviesDBZar(), new TYSee(), new AnimeTop(), new Cartoon(), new OneEMovie(), new Hd5movies(), new QQMovies(), new DLLLink(), new YSMovies(), new Vliver(), new ExtraWiki(), new FixOne(), new Crackle(), new Torrent4k(), new SRLS(), new CBB(), new KRMovies(), new LIME(), new HD7Movies(), new KickassTorrents(), new Eztv(), new Rarbg(), new Zooqle()};

    /* renamed from: a  reason: collision with root package name */
    private String[] f6506a;

    public static final boolean b() {
        return (RealDebridCredentialsHelper.c().isValid() && !RealDebridCredentialsHelper.b()) || (AllDebridCredentialsHelper.b().isValid() && AllDebridUserApi.d().b()) || (PremiumizeCredentialsHelper.b().isValid() && PremiumizeUserApi.c().b());
    }

    public static String h(String str) {
        try {
            return new URL(str).getHost();
        } catch (Exception unused) {
            return str;
        }
    }

    public static String i(String str) {
        try {
            URL url = new URL(str);
            return url.getProtocol() + "://" + url.getHost();
        } catch (Exception unused) {
            return str;
        }
    }

    public abstract String a();

    /* access modifiers changed from: protected */
    public abstract void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) throws UnsupportedEncodingException;

    /* access modifiers changed from: protected */
    public final void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, String str2, boolean... zArr) {
        String str3;
        String str4;
        boolean z = zArr != null && zArr.length > 0 && zArr[0];
        try {
            URL url = new URL(str);
            String host = url.getHost();
            str4 = url.getProtocol() + "://" + url.getHost();
            str3 = host;
        } catch (Exception unused) {
            str4 = "";
            str3 = str;
        }
        if (str.contains("vidnode.net") || str.contains("vidcloud.icu") || str.contains("vidstreaming.io") || str.contains("vidcloud9.com") || str.contains("/streaming.php")) {
            ArrayList d = d(str, str4 + "/");
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", Constants.f5838a);
            Iterator it2 = d.iterator();
            while (it2.hasNext()) {
                String obj = it2.next().toString();
                if (!obj.endsWith(".vtt")) {
                    boolean k = GoogleVideoHelper.k(obj);
                    MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", zArr[0]);
                    mediaSource.setStreamLink(obj);
                    if (!k) {
                        HashMap hashMap2 = new HashMap();
                        hashMap2.put("accept", "*/*");
                        hashMap2.put("origin", str4);
                        hashMap2.put("referer", str4 + "/");
                        hashMap2.put("User-Agent", Constants.f5838a);
                        mediaSource.setPlayHeader(hashMap2);
                    } else {
                        mediaSource.setPlayHeader(hashMap);
                    }
                    mediaSource.setQuality(k ? GoogleVideoHelper.h(obj) : str2);
                    observableEmitter.onNext(mediaSource);
                }
            }
            return;
        }
        a(observableEmitter, str, str2, a(), str3, z, new boolean[0]);
    }

    /* access modifiers changed from: protected */
    public abstract void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) throws UnsupportedEncodingException;

    public boolean c(String str) {
        String lowerCase = str.toLowerCase();
        return lowerCase.contains("cam") || lowerCase.contains("ts") || lowerCase.contains("hc");
    }

    public boolean d(String str) {
        String lowerCase = str.toLowerCase();
        return lowerCase.contains("-cam-") || lowerCase.contains("-ts-") || lowerCase.contains("-tc-") || lowerCase.contains("_cam_") || lowerCase.contains("_ts_") || lowerCase.contains("_tc_");
    }

    public boolean e(String str) {
        return str.contains("HD") || str.contains("1080") || str.contains("720") || str.contains("4K") || str.contains("2K") || str.contains("1440");
    }

    /* access modifiers changed from: protected */
    public final String f(String str) {
        return a(str, false);
    }

    /* access modifiers changed from: protected */
    public final ArrayList<String> g(String str) {
        ArrayList arrayList = Regex.b(str, "['\"]?sources?['\"]?\\s*:\\s*\\[(.*?)\\]", 1, true).get(0);
        arrayList.addAll(Regex.b(str, "['\"]?sources?[\"']?\\s*:\\s*\\[(.*?)\\}\\s*,?\\s*\\]", 1, true).get(0));
        arrayList.addAll(Regex.b(str, "['\"]?sources?['\"]?\\s*:\\s*\\{(.*?)\\}", 1, true).get(0));
        arrayList.addAll(Regex.a(str, "['\"]?sources?[\"']?\\s*:\\s*[\\{\\[](\\s*)[\\}\\]]", true));
        ArrayList arrayList2 = new ArrayList();
        arrayList2.add("");
        arrayList.removeAll(arrayList2);
        ArrayList a2 = Utils.a(arrayList);
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        if (a2.isEmpty()) {
            arrayList4.addAll(Regex.b(str, "\\{\\s*['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)['\"]", 1, true).get(0));
        } else {
            Iterator it2 = a2.iterator();
            while (it2.hasNext()) {
                arrayList4.addAll(Regex.b((String) it2.next(), "['\"]?file['\"]?\\s*:\\s*['\"]([^'\"]+)", 1, true).get(0));
            }
        }
        Iterator it3 = arrayList4.iterator();
        while (it3.hasNext()) {
            arrayList3.add(((String) it3.next()).replace("\\/", "/").replace("\\\\", ""));
        }
        return Utils.a(arrayList3);
    }

    public static String e(String str, String str2) {
        try {
            Element h = Jsoup.b(str).h("form[action]");
            String b2 = h.b("action");
            if (b2.isEmpty()) {
                return "";
            }
            if (b2.startsWith("/")) {
                b2 = str2 + b2;
            } else if (!b2.contains(UriUtil.HTTP_SCHEME)) {
                b2 = str2 + "/" + b2;
            }
            String str3 = b2 + "?";
            Iterator it2 = h.g("input").iterator();
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                String b3 = element.b("type");
                String b4 = element.b("value");
                String b5 = element.b(MediationMetaData.KEY_NAME);
                if (b3.equalsIgnoreCase(ViewProps.HIDDEN)) {
                    str3 = str3 + b5 + "=" + b4;
                }
                if (b3.equalsIgnoreCase("text") || b3.equalsIgnoreCase(AppLovinEventTypes.USER_EXECUTED_SEARCH)) {
                    str3 = str3 + b5 + "=%s";
                }
                if (!b3.equalsIgnoreCase("submit")) {
                    if (it2.hasNext()) {
                        str3 = str3 + "&";
                    }
                }
            }
            return str3.endsWith("&") ? str3.substring(0, str3.length() - 1) : str3;
        } catch (Throwable unused) {
            return "";
        }
    }

    public final boolean b(String str) {
        try {
            str = new URL(str).getHost();
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
        try {
            if (str.isEmpty() || str.equals("thepiratebay.org") || str.equals("protected.to") || str.equals("thetvdb.com") || str.equals("extratorrent.cc") || str.equals("imdb.com")) {
                return false;
            }
            if (this.f6506a == null) {
                this.f6506a = BaseResolver.e();
                if (b()) {
                    this.f6506a = Utils.a(this.f6506a, BaseResolver.d());
                }
            }
            for (String str2 : com.utils.Utils.d ? BaseResolver.d() : this.f6506a) {
                if (TitleHelper.f(str).contains(TitleHelper.f(str2)) || TitleHelper.f(str2).contains(TitleHelper.f(str))) {
                    return true;
                }
            }
            return false;
        } catch (Exception unused) {
            return false;chain
        } else if (str.contains("openload")) {
            return "https://openload.co/embed/" + str2;
        } else if (str.contains("verystream")) {
            return "https://verystream.com/e/" + str2;
        } else if (str.contains("uptostream")) {
            return "https://uptostream.com/iframe/" + str2;
        } else if (!str.contains("vidoza")) {
            return "";
        } else {
            return "https://vidoza.net/embed-" + str2 + ".html";
        }
    }

    public ArrayList d(String str, String str2) {
        HttpHelper e = HttpHelper.e();
        String b2 = e.b(str, str2 + "/");
        ArrayList arrayList = new ArrayList();
        Iterator it2 = Jsoup.b(b2).g("li.linkserver").iterator();
        while (it2.hasNext()) {
            String b3 = ((Element) it2.next()).b("data-video");
            if (!b3.isEmpty() && !b3.contains("load.php")) {
                if (str2.contains("api.")) {
                    String c = c(Regex.a(b3, "type=(\\w+)", 1), Regex.a(b2, "sid=(\\w+)", 1));
                    if (!c.isEmpty()) {
                        arrayList.add(c);
                    }
                } else {
                    arrayList.add(b3);
                }
            }
        }
        arrayList.addAll(g(b2));
        String a2 = Regex.a(b2, "window.urlVideo\\s*=\\s*['\"]([^'\"]+[^'\"])['\"]", 1);
        if (!a2.isEmpty()) {
            arrayList.add(a2);
        }
        return arrayList;
    }

    public HashMap<String, String> b(String str, String str2) {
        String str3;
        HashMap<String, String> hashMap = new HashMap<>();
        Document b2 = Jsoup.b(str);
        Element h = b2.h("form[method=" + str2 + "][action]");
        if (h != null) {
            String b3 = h.b("action");
            Iterator it2 = h.g("input").iterator();
            String str4 = "";
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                String format = String.format("%s=%s", new Object[]{element.b(MediationMetaData.KEY_NAME), element.b("value")});
                StringBuilder sb = new StringBuilder();
                sb.append(str4);
                sb.append(format);
                if (it2.hasNext()) {
                    str3 = "&";
                } else {
                    str3 = "";
                }
                sb.append(str3);
                str4 = sb.toString();
            }
            hashMap.put("action", b3);
            hashMap.put(AppLovinEventParameters.SEARCH_QUERY, str4);
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public final void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, String str2, String str3, boolean... zArr) {
        String str4;
        String str5;
        try {
            URL url = new URL(str);
            String host = url.getHost();
            str5 = url.getProtocol() + "://" + url.getHost();
            str4 = host;
        } catch (Exception unused) {
            str5 = "";
            str4 = str;
        }
        if (str.contains("vidnode.net") || str.contains("vidcloud.icu") || str.contains("vidstreaming.io") || str.contains("vidcloud9.com") || str.contains("/streaming.php")) {
            ArrayList d = d(str, str5 + "/");
            HashMap hashMap = new HashMap();
            hashMap.put("User-Agent", Constants.f5838a);
            Iterator it2 = d.iterator();
            while (it2.hasNext()) {
                String obj = it2.next().toString();
                if (!obj.endsWith(".vtt")) {
                    boolean k = GoogleVideoHelper.k(obj);
                    MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                    mediaSource.setStreamLink(obj);
                    if (!k) {
                        HashMap hashMap2 = new HashMap();
                        hashMap2.put("accept", "*/*");
                        hashMap2.put("origin", str5);
                        hashMap2.put("referer", str5 + "/");
                        hashMap2.put("User-Agent", Constants.f5838a);
                        mediaSource.setPlayHeader(hashMap2);
                    } else {
                        mediaSource.setPlayHeader(hashMap);
                    }
                    mediaSource.setQuality(k ? GoogleVideoHelper.h(obj) : str2);
                    observableEmitter.onNext(mediaSource);
                }
            }
            return;
        }
        a(observableEmitter, str, str2, str3, str4, false, zArr);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0083, code lost:
        r13 = true;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r9, java.lang.String r10, java.lang.String r11, java.lang.String r12, java.lang.String r13, boolean r14, boolean... r15) {
        /*
            r8 = this;
            boolean r0 = r13.isEmpty()     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            boolean r0 = r9.isDisposed()     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            java.lang.String r0 = "thepiratebay.org"
            boolean r0 = r13.equals(r0)     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            java.lang.String r0 = "protected.to"
            boolean r0 = r13.equals(r0)     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            java.lang.String r0 = "thetvdb.com"
            boolean r0 = r13.equals(r0)     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            java.lang.String r0 = "extratorrent.cc"
            boolean r0 = r13.equals(r0)     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            java.lang.String r0 = "imdb.com"
            boolean r0 = r13.equals(r0)     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0128
            java.lang.String[] r0 = r8.f6506a     // Catch:{ Exception -> 0x0128 }
            if (r0 != 0) goto L_0x0050
            java.lang.String[] r0 = com.utils.Getlink.Resolver.BaseResolver.e()     // Catch:{ Exception -> 0x0128 }
            r8.f6506a = r0     // Catch:{ Exception -> 0x0128 }
            boolean r0 = b()     // Catch:{ Exception -> 0x0128 }
            if (r0 == 0) goto L_0x0050
            java.lang.String[] r0 = r8.f6506a     // Catch:{ Exception -> 0x0128 }
            java.lang.String[] r1 = com.utils.Getlink.Resolver.BaseResolver.d()     // Catch:{ Exception -> 0x0128 }
            java.lang.String[] r0 = com.original.tase.utils.Utils.a((java.lang.String[]) r0, (java.lang.String[]) r1)     // Catch:{ Exception -> 0x0128 }
            r8.f6506a = r0     // Catch:{ Exception -> 0x0128 }
        L_0x0050:
            boolean r0 = com.utils.Utils.d     // Catch:{ Exception -> 0x0128 }
            if (r0 == 0) goto L_0x0059
            java.lang.String[] r0 = com.utils.Getlink.Resolver.BaseResolver.d()     // Catch:{ Exception -> 0x0128 }
            goto L_0x005b
        L_0x0059:
            java.lang.String[] r0 = r8.f6506a     // Catch:{ Exception -> 0x0128 }
        L_0x005b:
            int r1 = r0.length     // Catch:{ Exception -> 0x0128 }
            r2 = 0
            r3 = 0
        L_0x005e:
            r4 = 1
            if (r3 >= r1) goto L_0x0085
            r5 = r0[r3]     // Catch:{ Exception -> 0x0128 }
            java.lang.String r6 = com.original.tase.helper.TitleHelper.f(r13)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r7 = com.original.tase.helper.TitleHelper.f(r5)     // Catch:{ Exception -> 0x0128 }
            boolean r6 = r6.contains(r7)     // Catch:{ Exception -> 0x0128 }
            if (r6 != 0) goto L_0x0083
            java.lang.String r5 = com.original.tase.helper.TitleHelper.f(r5)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r6 = com.original.tase.helper.TitleHelper.f(r13)     // Catch:{ Exception -> 0x0128 }
            boolean r5 = r5.contains(r6)     // Catch:{ Exception -> 0x0128 }
            if (r5 == 0) goto L_0x0080
            goto L_0x0083
        L_0x0080:
            int r3 = r3 + 1
            goto L_0x005e
        L_0x0083:
            r13 = 1
            goto L_0x0086
        L_0x0085:
            r13 = 0
        L_0x0086:
            boolean r0 = com.utils.Utils.b     // Catch:{ Exception -> 0x0128 }
            java.lang.String r1 = " "
            if (r0 == 0) goto L_0x00c7
            if (r13 == 0) goto L_0x00c7
            boolean r13 = r8.e(r11)     // Catch:{ Exception -> 0x0128 }
            if (r13 == 0) goto L_0x00af
            if (r14 != 0) goto L_0x00af
            java.lang.String r13 = "pref_show_hd_only true: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128 }
            r0.<init>()     // Catch:{ Exception -> 0x0128 }
            r0.append(r11)     // Catch:{ Exception -> 0x0128 }
            r0.append(r1)     // Catch:{ Exception -> 0x0128 }
            r0.append(r12)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0128 }
            com.original.tase.Logger.a((java.lang.String) r13, (java.lang.String) r0)     // Catch:{ Exception -> 0x0128 }
            r13 = 1
            goto L_0x00c7
        L_0x00af:
            java.lang.String r13 = "pref_show_hd_only false: "
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128 }
            r0.<init>()     // Catch:{ Exception -> 0x0128 }
            r0.append(r11)     // Catch:{ Exception -> 0x0128 }
            r0.append(r1)     // Catch:{ Exception -> 0x0128 }
            r0.append(r12)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0128 }
            com.original.tase.Logger.a((java.lang.String) r13, (java.lang.String) r0)     // Catch:{ Exception -> 0x0128 }
            r13 = 0
        L_0x00c7:
            if (r13 == 0) goto L_0x010d
            if (r11 == 0) goto L_0x00d1
            boolean r13 = r11.isEmpty()     // Catch:{ Exception -> 0x0128 }
            if (r13 == 0) goto L_0x00d3
        L_0x00d1:
            java.lang.String r11 = ""
        L_0x00d3:
            if (r14 == 0) goto L_0x00ea
            java.lang.StringBuilder r12 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128 }
            r12.<init>()     // Catch:{ Exception -> 0x0128 }
            java.lang.String r13 = r8.a()     // Catch:{ Exception -> 0x0128 }
            r12.append(r13)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r13 = " (CAM)"
            r12.append(r13)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r12 = r12.toString()     // Catch:{ Exception -> 0x0128 }
        L_0x00ea:
            com.original.tase.model.media.MediaSource r13 = new com.original.tase.model.media.MediaSource     // Catch:{ Exception -> 0x0128 }
            r13.<init>(r12, r11, r4)     // Catch:{ Exception -> 0x0128 }
            if (r15 == 0) goto L_0x0103
            int r12 = r15.length     // Catch:{ Exception -> 0x0128 }
            if (r12 != r4) goto L_0x00fa
            boolean r12 = r15[r2]     // Catch:{ Exception -> 0x0128 }
            r13.setCachedLink(r12)     // Catch:{ Exception -> 0x0128 }
            goto L_0x0103
        L_0x00fa:
            int r12 = r15.length     // Catch:{ Exception -> 0x0128 }
            r14 = 2
            if (r12 != r14) goto L_0x0103
            boolean r12 = r15[r4]     // Catch:{ Exception -> 0x0128 }
            r13.setNeedToSync(r12)     // Catch:{ Exception -> 0x0128 }
        L_0x0103:
            r13.setQuality((java.lang.String) r11)     // Catch:{ Exception -> 0x0128 }
            r13.setStreamLink(r10)     // Catch:{ Exception -> 0x0128 }
            r9.onNext(r13)     // Catch:{ Exception -> 0x0128 }
            goto L_0x0128
        L_0x010d:
            java.lang.String r9 = "NEEDRESOLVER "
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0128 }
            r11.<init>()     // Catch:{ Exception -> 0x0128 }
            r11.append(r10)     // Catch:{ Exception -> 0x0128 }
            r11.append(r1)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r10 = r8.a()     // Catch:{ Exception -> 0x0128 }
            r11.append(r10)     // Catch:{ Exception -> 0x0128 }
            java.lang.String r10 = r11.toString()     // Catch:{ Exception -> 0x0128 }
            com.original.tase.Logger.a((java.lang.String) r9, (java.lang.String) r10)     // Catch:{ Exception -> 0x0128 }
        L_0x0128:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.BaseProvider.a(io.reactivex.ObservableEmitter, java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, boolean[]):void");
    }

    /* access modifiers changed from: protected */
    public final String a(String str, boolean z) {
        String a2 = a();
        if (z) {
            a2 = "Torrent";
        }
        String lowerCase = str.trim().toLowerCase();
        if (lowerCase.contains("web")) {
            a2 = a2 + " (WEB-DL)";
        } else if (lowerCase.contains("hdtv")) {
            a2 = a2 + " (HDTV)";
        }
        if (lowerCase.contains("264")) {
            a2 = a2 + " (x264)";
        } else if (lowerCase.contains("265") || lowerCase.contains("hevc")) {
            a2 = a2 + " (x265)";
        }
        if (lowerCase.contains("5.1") || lowerCase.contains("6ch")) {
            a2 = a2 + " (5.1CH)";
        } else if (lowerCase.contains("7.1")) {
            a2 = a2 + " (7.1CH)";
        } else if (lowerCase.contains("8ch")) {
            a2 = a2 + " (8CH)";
        }
        if (lowerCase.contains("bluray")) {
            a2 = a2 + " (BluRay)";
        }
        if (lowerCase.contains("10bit")) {
            a2 = a2 + " (10Bit)";
        }
        if (lowerCase.contains("truehd")) {
            return a2 + " (TrueHD)";
        } else if (lowerCase.contains("atmos")) {
            return a2 + " (Atmos)";
        } else if (!lowerCase.contains("3d")) {
            return a2;
        } else {
            return a2 + " (3D)";
        }
    }

    public boolean a(List<String> list) {
        return list.contains("Animation");
    }

    public Observable<MediaSource> a(final MovieInfo movieInfo) {
        return Observable.create(new ObservableOnSubscribe<MediaSource>() {
            public void subscribe(ObservableEmitter<MediaSource> observableEmitter) throws Exception {
                try {
                    MovieInfo clone = movieInfo.clone();
                    clone.name = clone.name.replace("&", "and");
                    if (clone.session.isEmpty()) {
                        BaseProvider.this.a(clone, (ObservableEmitter<? super MediaSource>) observableEmitter);
                    } else {
                        BaseProvider.this.b(clone, (ObservableEmitter<? super MediaSource>) observableEmitter);
                    }
                } catch (Exception e) {
                    Logger.a("BaseProvider", e.getMessage());
                }
                observableEmitter.onComplete();
            }
        });
    }
}
