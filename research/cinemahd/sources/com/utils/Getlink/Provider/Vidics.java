package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Vidics extends BaseProvider {
    String c = Utils.getProvider(29);

    public String a() {
        return "Vidics";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        boolean z;
        Element h;
        String str2;
        MovieInfo movieInfo2 = movieInfo;
        String replace = movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "").replace("The 100", "The Hundred");
        if (!replace.equals("American Dad!")) {
            replace = TitleHelper.e(replace);
        }
        Document b = Jsoup.b(HttpHelper.e().a(this.c + "/Category-FilmsAndTV/Genre-Any/Letter-Any/ByPopularity/1/Search-" + com.original.tase.utils.Utils.a(TitleHelper.g(replace), new boolean[0]) + ".htm", (Map<String, String>[]) new Map[0]));
        String[] e = BaseResolver.e();
        if (RealDebridCredentialsHelper.c().isValid()) {
            e = com.original.tase.utils.Utils.a(e, BaseResolver.d());
        }
        Iterator it2 = b.g("div.searchResult").iterator();
        while (true) {
            if (!it2.hasNext()) {
                str = "";
                break;
            }
            Element element = (Element) it2.next();
            Element h2 = element.h("a[href][itemprop=\"url\"]");
            Element h3 = element.h("span[itemprop=\"name\"]");
            Element h4 = element.h("span[itemprop=\"copyrightYear\"]");
            if (!(h2 == null || h3 == null)) {
                str = h2.b("href");
                String G = h3.G();
                if (h4 != null) {
                    str2 = h4.G();
                } else {
                    str2 = "";
                }
                if (str.trim().toLowerCase().contains("/serie/") && TitleHelper.f(movieInfo.getName()).equals(TitleHelper.f(G))) {
                    if (str2.trim().isEmpty() || !com.original.tase.utils.Utils.b(str2.trim()) || movieInfo.getYear().intValue() <= 0 || Integer.parseInt(str2.trim()) == movieInfo.getYear().intValue()) {
                        break;
                    }
                }
            }
        }
        if (str.startsWith("//")) {
            str = "http:" + str;
        } else if (str.startsWith("/")) {
            str = this.c + str;
        } else if (str.isEmpty()) {
            return;
        }
        String a2 = Regex.a(HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]), "href=\"(/Serie/[^\"]+-Season-" + movieInfo2.session + "-Episode-" + movieInfo2.eps + ")", 1, true);
        if (a2.startsWith("/")) {
            a2 = "https://www.vidics.to" + a2;
        } else if (a2.isEmpty()) {
            return;
        }
        Document b2 = Jsoup.b(HttpHelper.e().b(a2, str));
        HashMap hashMap = new HashMap();
        hashMap.put("Referer", a2);
        hashMap.put("Host", this.c.replace("https://", "").replace("http://", "").replace("/", ""));
        hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
        hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.6,en;q=0.4");
        Iterator it3 = b2.g("div.lang").iterator();
        while (it3.hasNext()) {
            Element element2 = (Element) it3.next();
            if (Regex.a(element2.toString(), "title=\"Language\\s+Flag\\s+([^\"]*)\"", 1, true).trim().toLowerCase().equals("english")) {
                Iterator it4 = element2.g("a.p1[href]").iterator();
                while (it4.hasNext()) {
                    Element element3 = (Element) it4.next();
                    if (!observableEmitter.isDisposed()) {
                        try {
                            String b3 = element3.b("href");
                            String trim = element3.G().trim();
                            int length = e.length;
                            int i = 0;
                            while (true) {
                                if (i >= length) {
                                    z = false;
                                    break;
                                }
                                String str3 = e[i];
                                if (TitleHelper.f(trim).contains(TitleHelper.f(str3))) {
                                    break;
                                } else if (TitleHelper.f(str3).contains(TitleHelper.f(trim))) {
                                    break;
                                } else {
                                    i++;
                                }
                            }
                            z = true;
                            if (z) {
                                if (b3.startsWith("/")) {
                                    b3 = this.c + b3;
                                }
                                String a3 = HttpHelper.e().a(b3, (Map<String, String>[]) new Map[]{hashMap});
                                if (!a3.isEmpty() && (h = Jsoup.b(a3).h("a.blue[href][target=\"_blank\"]")) != null) {
                                    try {
                                        a(observableEmitter, h.b("href"), "HQ", new boolean[0]);
                                    } catch (Exception e2) {
                                        e = e2;
                                    }
                                }
                            }
                            ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
                        } catch (Exception e3) {
                            e = e3;
                            ObservableEmitter<? super MediaSource> observableEmitter3 = observableEmitter;
                            Logger.a((Throwable) e, new boolean[0]);
                        }
                    } else {
                        return;
                    }
                }
                continue;
            }
            ObservableEmitter<? super MediaSource> observableEmitter4 = observableEmitter;
        }
    }
}
