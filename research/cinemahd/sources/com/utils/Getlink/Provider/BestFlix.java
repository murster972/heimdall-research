package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class BestFlix extends BaseProvider {
    private String c = Utils.getProvider(44);
    private String d = "HD";

    public String a() {
        return "BestFlix";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo.eps);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, String str2) {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("div.pas-list").b("li").iterator();
        ArrayList arrayList = new ArrayList();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            Element h = element.h("a");
            if (element.b("id").equals(str2)) {
                if (str.startsWith("/")) {
                    str = this.c + str;
                }
                String b = h.b("player-data");
                if (b.contains("vidcloud.icu/streaming.php")) {
                    if (b.startsWith("//")) {
                        b = "https:" + b;
                    }
                    Iterator it3 = d(b, str).iterator();
                    while (it3.hasNext()) {
                        String obj = it3.next().toString();
                        if (!arrayList.contains(obj)) {
                            arrayList.add(obj);
                            boolean k = GoogleVideoHelper.k(obj);
                            MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                            mediaSource.setStreamLink(obj);
                            if (k) {
                                mediaSource.setPlayHeader(hashMap);
                            }
                            mediaSource.setQuality(k ? GoogleVideoHelper.h(obj) : this.d);
                            observableEmitter.onNext(mediaSource);
                        }
                    }
                } else if (!b.contains("vidcloud") && !arrayList.contains(b)) {
                    a(observableEmitter, b, this.d, false);
                }
            }
        }
    }

    private String b(MovieInfo movieInfo) {
        String str;
        boolean z;
        String str2;
        String str3;
        MovieInfo movieInfo2 = movieInfo;
        boolean z2 = movieInfo.getType().intValue() == 1;
        String a2 = TitleHelper.a(movieInfo2.name.toLowerCase(), "-");
        HashMap hashMap = new HashMap();
        String str4 = "accept";
        hashMap.put(str4, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        hashMap.put("referer", this.c + "/");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("user-agent", Constants.f5838a);
        String str5 = this.c + "/movie/search/" + a2;
        String a3 = HttpHelper.e().a(str5, (Map<String, String>[]) new Map[]{hashMap});
        Iterator it2 = Jsoup.b(a3).g("div.ml-item").iterator();
        HashMap hashMap2 = new HashMap();
        hashMap2.put(str4, "*/*");
        hashMap2.put("origin", this.c);
        hashMap2.put("referer", str5);
        String str6 = "?link_web=";
        if (!it2.hasNext()) {
            HttpHelper e = HttpHelper.e();
            StringBuilder sb = new StringBuilder();
            sb.append("https://api.ocloud.stream/series/movie/search/");
            sb.append(a2);
            sb.append(str6);
            sb.append(com.original.tase.utils.Utils.a(this.c + "/", new boolean[0]));
            a3 = e.a(sb.toString(), (Map<String, String>[]) new Map[]{hashMap2});
            it2 = Jsoup.b(a3).g("div.ml-item").iterator();
        }
        String str7 = movieInfo2.year;
        while (it2.hasNext()) {
            Element h = ((Element) it2.next()).h("a");
            String b = h.b("data-url");
            String b2 = h.b("title");
            if (b2.isEmpty()) {
                b2 = h.h("h2").G();
            }
            String str8 = str7;
            if (z2) {
                z = z2;
                if (b2.equals(movieInfo2.name)) {
                    if (!b.isEmpty()) {
                        if (b.startsWith("//")) {
                            b = "http:" + b;
                        } else if (b.startsWith("/")) {
                            b = this.c + b;
                        } else if (!b.startsWith(UriUtil.HTTP_SCHEME)) {
                            b = this.c + "/" + b;
                        }
                        hashMap2.put(str4, "application/json, text/javascript, */*; q=0.01");
                        HttpHelper e2 = HttpHelper.e();
                        StringBuilder sb2 = new StringBuilder();
                        sb2.append(b);
                        sb2.append(str6);
                        StringBuilder sb3 = new StringBuilder();
                        str = str4;
                        sb3.append(this.c);
                        sb3.append("/");
                        str2 = str6;
                        sb2.append(com.original.tase.utils.Utils.a(sb3.toString(), new boolean[0]));
                        sb2.append("##forceNoCache##");
                        a3 = e2.a(sb2.toString(), (Map<String, String>[]) new Map[]{hashMap2}).replace("\\\"", "\"").replace("\\/", "/");
                        str3 = Regex.a(a3, "<div\\s+[^>]*class=\"jt-info\"[^>]*>\\s*(\\d{4})\\s*</div>", 1);
                        String a4 = Regex.a(a3, "<div\\s+[^>]*class=\"jtip-quality\"[^>]*>\\s*([\\w\\s]+)\\s*</div>", 1);
                        c(a4);
                        String a5 = Regex.a(a4, "(\\d+)", 1);
                        if (!a5.isEmpty()) {
                            a5 + "p";
                        }
                    } else {
                        str = str4;
                        str2 = str6;
                        str3 = str8;
                    }
                    if (str3.equals(movieInfo2.year) || a3.isEmpty()) {
                        String b3 = h.b("href");
                        if (b3.startsWith("//")) {
                            return "http:" + b3;
                        } else if (b3.startsWith("/")) {
                            return this.c + b3;
                        } else if (b3.startsWith(UriUtil.HTTP_SCHEME)) {
                            return b3;
                        } else {
                            return this.c + "/" + b3;
                        }
                    } else {
                        str8 = str3;
                    }
                } else {
                    str = str4;
                    str2 = str6;
                }
                if (b2.equals(movieInfo2.name + " (" + movieInfo2.year + ")")) {
                    String b4 = h.b("href");
                    if (b4.startsWith("//")) {
                        return "http:" + b4;
                    } else if (b4.startsWith("/")) {
                        return this.c + b4;
                    } else if (b4.startsWith(UriUtil.HTTP_SCHEME)) {
                        return b4;
                    } else {
                        return this.c + "/" + b4;
                    }
                }
            } else {
                z = z2;
                str = str4;
                str2 = str6;
                if (TitleHelper.f(b2).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName() + "season" + movieInfo2.session)))) {
                    String b5 = h.b("href");
                    if (b5.startsWith("//")) {
                        return "http:" + b5;
                    } else if (b5.startsWith("/")) {
                        return this.c + b5;
                    } else if (b5.startsWith(UriUtil.HTTP_SCHEME)) {
                        return b5;
                    } else {
                        return this.c + "/" + b5;
                    }
                }
            }
            str6 = str2;
            str7 = str8;
            z2 = z;
            str4 = str;
        }
        return "";
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str) {
        HashMap hashMap = new HashMap();
        hashMap.put("User-Agent", Constants.f5838a);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(str, (Map<String, String>[]) new Map[]{hashMap})).g("div.pas-list").b("li").iterator();
        ArrayList arrayList = new ArrayList();
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).h("a").b("player-data");
            if (b.contains("vidcloud.icu/streaming.php")) {
                if (b.startsWith("//")) {
                    b = "https:" + b;
                }
                Iterator it3 = d(b, str).iterator();
                while (it3.hasNext()) {
                    String obj = it3.next().toString();
                    if (!arrayList.contains(obj)) {
                        arrayList.add(obj);
                        boolean k = GoogleVideoHelper.k(obj);
                        MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                        mediaSource.setStreamLink(obj);
                        if (k) {
                            mediaSource.setPlayHeader(hashMap);
                        }
                        mediaSource.setQuality(k ? GoogleVideoHelper.h(obj) : this.d);
                        observableEmitter.onNext(mediaSource);
                    }
                }
            } else if (!b.contains("vidcloud") && !arrayList.contains(b)) {
                a(observableEmitter, b, this.d, false);
            }
        }
    }
}
