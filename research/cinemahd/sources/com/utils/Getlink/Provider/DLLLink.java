package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class DLLLink extends BaseProvider {
    public static String d = "HD";
    private String c = "https://www.full4movies.me";

    public String a() {
        return "PrimeWire";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        Iterator it2;
        boolean z = movieInfo.getType().intValue() == 1;
        String b = HttpHelper.e().b(str, this.c);
        if (z) {
            it2 = Jsoup.b(b).g("div.entry-content").b("a.myButton").iterator();
        } else {
            String a2 = Regex.a(b, "href\\s*=\\s*['\"]([^'\"]+[^'\"]*" + movieInfo.session + "x" + movieInfo.eps + ")['\"]", 1);
            if (!a2.isEmpty()) {
                it2 = Jsoup.b(HttpHelper.e().b(a2, str)).g("div.linkstv").b("a[href]").iterator();
            } else {
                return;
            }
        }
        boolean c2 = c(d);
        while (it2.hasNext()) {
            a(observableEmitter, ((Element) it2.next()).b("href"), "HD", c2);
        }
    }

    private String b(MovieInfo movieInfo) {
        String a2 = Utils.a(movieInfo.name.toLowerCase(), new boolean[0]);
        Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/?s=" + a2, (Map<String, String>[]) new Map[0])).g("div.item").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String a3 = Regex.a(element.toString(), "<h2>(.*)<\\/h2>", 1);
            String a4 = Regex.a(element.toString(), "<span\\s*class=\"year\">(\\w+)<\\/span>", 1);
            d = Regex.a(element.toString(), "<span\\s*class=\"calidad2\">(.*)<\\/span>", 1);
            if (!a3.isEmpty() && a3.toLowerCase().startsWith(movieInfo.name.toLowerCase()) && a4.equalsIgnoreCase(movieInfo.year)) {
                String b = element.h("a").b("href");
                if (!b.startsWith("/")) {
                    return b;
                }
                return this.c + b;
            }
        }
        return "";
    }
}
