package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class AZMovies extends BaseProvider {
    private String c = Utils.getProvider(102);

    public String a() {
        return "AZMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        c(movieInfo, observableEmitter);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    public void c(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        boolean z;
        String a2 = TitleHelper.a(movieInfo.name.toLowerCase() + " " + movieInfo.year, "-");
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        String str = this.c + "/movies/" + a2;
        String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
        Iterator it2 = Jsoup.b(a3).g("tbody#stream-list").b("div.embed-details").b("a").iterator();
        String a4 = Regex.a(a3, "<span\\s*class=\"quality\">(\\w+)<\\/span>", 1);
        if (a4.isEmpty()) {
            a4 = "HQ";
            z = false;
        } else {
            z = c(a4);
        }
        while (it2.hasNext()) {
            String b = ((Element) it2.next()).b("href");
            if (!b.isEmpty()) {
                if (b(b)) {
                    a(observableEmitter, b, a4, z);
                } else {
                    String a5 = HttpHelper.e().a(b, false, str);
                    if (a5.isEmpty()) {
                        String a6 = Regex.a(HttpHelper.e().b(b, str), "redirect\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
                        if (!a6.isEmpty()) {
                            a(observableEmitter, a6, a4, z);
                        }
                    } else {
                        a(observableEmitter, a5, a4, z);
                    }
                }
            }
        }
    }
}
