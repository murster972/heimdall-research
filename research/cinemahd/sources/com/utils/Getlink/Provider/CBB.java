package com.utils.Getlink.Provider;

import com.movie.data.api.GlobalVariable;
import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import io.reactivex.ObservableEmitter;
import java.util.HashMap;
import okhttp3.internal.cache.DiskLruCache;

public class CBB extends BaseProvider {
    public HashMap c = new HashMap();
    private String d = "";
    private String[] e = null;
    private String f = "";
    private boolean g = false;

    public CBB() {
        this.c.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
        this.c.put("accept-Language", "en-US");
        this.c.put("upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
        this.c.put("user-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36");
    }

    private static String[] c() {
        String str = GlobalVariable.c().a().getCbflist().get(DiskLruCache.VERSION_1);
        return (str == null || str.isEmpty()) ? new String[0] : str.split(",");
    }

    public String a() {
        return "CBB";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        this.e = c();
        if (BaseProvider.b()) {
            for (String str : this.e) {
                String[] split = str.split("##");
                if (split.length > 1) {
                    this.d = split[0];
                    this.f = split[1];
                    this.g = true;
                } else {
                    this.d = str;
                    this.g = false;
                }
                a(observableEmitter, movieInfo, "-1", "-1");
            }
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        this.e = c();
        if (BaseProvider.b()) {
            for (String str : this.e) {
                String[] split = str.split("##");
                if (split.length > 1) {
                    this.d = split[0];
                    this.f = split[1];
                    this.g = true;
                } else {
                    this.d = str;
                    this.g = false;
                }
                a(observableEmitter, movieInfo, movieInfo.session, movieInfo.eps);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:173:0x01f2 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x018a A[Catch:{ Exception -> 0x0361 }] */
    /* JADX WARNING: Removed duplicated region for block: B:62:0x01d6 A[Catch:{ Exception -> 0x034b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r24, com.movie.data.model.MovieInfo r25, java.lang.String r26, java.lang.String r27) {
        /*
            r23 = this;
            r7 = r23
            java.lang.String r8 = "1080p"
            java.util.ArrayList r9 = new java.util.ArrayList
            r9.<init>()
            java.lang.Integer r0 = r25.getType()
            int r0 = r0.intValue()
            r10 = 1
            r11 = 0
            if (r0 != r10) goto L_0x0017
            r12 = 1
            goto L_0x0018
        L_0x0017:
            r12 = 0
        L_0x0018:
            com.original.tase.helper.DirectoryIndexHelper r13 = new com.original.tase.helper.DirectoryIndexHelper
            r13.<init>()
            java.lang.String r14 = r25.getName()
            if (r12 == 0) goto L_0x002c
            java.lang.Integer r0 = r25.getYear()
            java.lang.String r0 = java.lang.String.valueOf(r0)
            goto L_0x0055
        L_0x002c:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "S"
            r0.append(r1)
            int r1 = java.lang.Integer.parseInt(r26)
            java.lang.String r1 = com.original.tase.utils.Utils.a((int) r1)
            r0.append(r1)
            java.lang.String r1 = "E"
            r0.append(r1)
            int r1 = java.lang.Integer.parseInt(r27)
            java.lang.String r1 = com.original.tase.utils.Utils.a((int) r1)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0055:
            r15 = r0
            boolean r0 = r7.g
            if (r0 == 0) goto L_0x005d
            java.lang.String r0 = r7.f
            goto L_0x005f
        L_0x005d:
            java.lang.String r0 = "search"
        L_0x005f:
            com.original.tase.helper.http.HttpHelper r1 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = r7.d
            r2.append(r3)
            java.lang.String r6 = "/"
            r2.append(r6)
            r2.append(r0)
            r2.append(r6)
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r14)
            java.lang.String r5 = " "
            r0.append(r5)
            r0.append(r15)
            java.lang.String r0 = r0.toString()
            java.lang.String r3 = "+"
            java.lang.String r0 = com.original.tase.helper.TitleHelper.a(r0, r3)
            boolean[] r3 = new boolean[r11]
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r3)
            r2.append(r0)
            java.lang.String r0 = "/feed/rss2"
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            java.util.Map[] r2 = new java.util.Map[r11]
            java.lang.String r0 = r1.a((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>()
            org.jsoup.parser.Parser r1 = org.jsoup.parser.Parser.b()
            java.lang.String r3 = ""
            org.jsoup.nodes.Document r0 = org.jsoup.Jsoup.a(r0, r3, r1)
            java.lang.String r1 = "item"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r1)
            java.util.Iterator r16 = r0.iterator()
        L_0x00c3:
            boolean r0 = r16.hasNext()
            if (r0 == 0) goto L_0x0397
            java.lang.Object r0 = r16.next()
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0
            java.lang.String r1 = "title"
            org.jsoup.nodes.Element r1 = r0.h(r1)
            if (r1 == 0) goto L_0x0380
            java.lang.String r2 = r1.G()
            java.lang.String r1 = "enclosure[url]"
            org.jsoup.select.Elements r0 = r0.g((java.lang.String) r1)
            java.util.Iterator r17 = r0.iterator()
        L_0x00e5:
            boolean r0 = r17.hasNext()
            if (r0 == 0) goto L_0x0380
            java.lang.Object r0 = r17.next()     // Catch:{ Exception -> 0x0361 }
            org.jsoup.nodes.Element r0 = (org.jsoup.nodes.Element) r0     // Catch:{ Exception -> 0x0361 }
            java.lang.String r1 = "url"
            java.lang.String r0 = r0.b((java.lang.String) r1)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r1 = ".7z"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            java.lang.String r1 = ".rar"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            java.lang.String r1 = ".zip"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            java.lang.String r1 = ".iso"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            java.lang.String r1 = ".avi"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            java.lang.String r1 = ".flv"
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            java.lang.String r1 = "imdb."
            boolean r1 = r0.contains(r1)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            boolean r1 = r4.contains(r0)     // Catch:{ Exception -> 0x0361 }
            if (r1 != 0) goto L_0x0355
            r4.add(r0)     // Catch:{ Exception -> 0x0361 }
            if (r12 != 0) goto L_0x0175
            boolean r1 = r0.contains(r6)     // Catch:{ Exception -> 0x0167 }
            if (r1 == 0) goto L_0x0175
            java.lang.String[] r1 = r0.split(r6)     // Catch:{ Exception -> 0x0167 }
            int r11 = r1.length     // Catch:{ Exception -> 0x0167 }
            if (r11 <= 0) goto L_0x0175
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0167 }
            r11.<init>()     // Catch:{ Exception -> 0x0167 }
            java.lang.String r10 = "(720p|1080p)"
            java.lang.String r10 = r2.replaceAll(r10, r3)     // Catch:{ Exception -> 0x0167 }
            r11.append(r10)     // Catch:{ Exception -> 0x0167 }
            r11.append(r5)     // Catch:{ Exception -> 0x0167 }
            int r10 = r1.length     // Catch:{ Exception -> 0x0167 }
            r18 = 1
            int r10 = r10 + -1
            r1 = r1[r10]     // Catch:{ Exception -> 0x0167 }
            r11.append(r1)     // Catch:{ Exception -> 0x0167 }
            java.lang.String r1 = r11.toString()     // Catch:{ Exception -> 0x0167 }
            goto L_0x0176
        L_0x0167:
            r0 = move-exception
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            r19 = r14
            r1 = 0
            r14 = r2
            goto L_0x036d
        L_0x0175:
            r1 = r2
        L_0x0176:
            java.lang.String r10 = com.original.tase.helper.TitleHelper.f(r14)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r11 = "(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)"
            java.lang.String r11 = r1.replaceAll(r11, r3)     // Catch:{ Exception -> 0x0361 }
            java.lang.String r11 = com.original.tase.helper.TitleHelper.f(r11)     // Catch:{ Exception -> 0x0361 }
            boolean r10 = r10.equals(r11)     // Catch:{ Exception -> 0x0361 }
            if (r10 == 0) goto L_0x0355
            java.lang.String r10 = "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]"
            r11 = 1
            java.util.ArrayList r10 = com.original.tase.utils.Regex.b(r1, r10, r11)     // Catch:{ Exception -> 0x0361 }
            r11 = 0
            java.lang.Object r10 = r10.get(r11)     // Catch:{ Exception -> 0x0361 }
            java.util.List r10 = (java.util.List) r10     // Catch:{ Exception -> 0x0361 }
            if (r12 != 0) goto L_0x01c4
            java.lang.String r11 = "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]"
            r19 = r14
            r14 = 1
            java.util.ArrayList r11 = com.original.tase.utils.Regex.b(r1, r11, r14)     // Catch:{ Exception -> 0x034b }
            r14 = 0
            java.lang.Object r11 = r11.get(r14)     // Catch:{ Exception -> 0x034b }
            java.util.Collection r11 = (java.util.Collection) r11     // Catch:{ Exception -> 0x034b }
            r10.addAll(r11)     // Catch:{ Exception -> 0x034b }
            java.lang.String r11 = "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]"
            r14 = 1
            java.util.ArrayList r11 = com.original.tase.utils.Regex.b(r1, r11, r14)     // Catch:{ Exception -> 0x034b }
            r14 = 0
            java.lang.Object r11 = r11.get(r14)     // Catch:{ Exception -> 0x034b }
            java.util.Collection r11 = (java.util.Collection) r11     // Catch:{ Exception -> 0x034b }
            r10.addAll(r11)     // Catch:{ Exception -> 0x034b }
            goto L_0x01c6
        L_0x01bf:
            r0 = move-exception
            r19 = r14
            goto L_0x034c
        L_0x01c4:
            r19 = r14
        L_0x01c6:
            int r11 = r10.size()     // Catch:{ Exception -> 0x034b }
            if (r11 <= 0) goto L_0x0342
            java.util.Iterator r10 = r10.iterator()     // Catch:{ Exception -> 0x034b }
        L_0x01d0:
            boolean r11 = r10.hasNext()     // Catch:{ Exception -> 0x034b }
            if (r11 == 0) goto L_0x01f2
            java.lang.Object r11 = r10.next()     // Catch:{ Exception -> 0x034b }
            java.lang.String r11 = (java.lang.String) r11     // Catch:{ Exception -> 0x034b }
            java.lang.String r14 = r11.toUpperCase()     // Catch:{ Exception -> 0x034b }
            boolean r14 = r14.equals(r15)     // Catch:{ Exception -> 0x034b }
            if (r14 != 0) goto L_0x01f0
            java.lang.String r11 = r11.toLowerCase()     // Catch:{ Exception -> 0x034b }
            boolean r11 = r9.contains(r11)     // Catch:{ Exception -> 0x034b }
            if (r11 == 0) goto L_0x01d0
        L_0x01f0:
            r10 = 1
            goto L_0x01f3
        L_0x01f2:
            r10 = 0
        L_0x01f3:
            if (r10 == 0) goto L_0x0342
            java.lang.String r1 = r1.toUpperCase()     // Catch:{ Exception -> 0x034b }
            java.lang.String r10 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r1 = r1.replaceAll(r10, r3)     // Catch:{ Exception -> 0x034b }
            java.lang.String r10 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r1 = r1.split(r10)     // Catch:{ Exception -> 0x034b }
            int r10 = r1.length     // Catch:{ Exception -> 0x034b }
            java.lang.String r11 = "HQ"
            r20 = r11
            r14 = 0
        L_0x020b:
            if (r14 >= r10) goto L_0x02e8
            r21 = r1[r14]     // Catch:{ Exception -> 0x034b }
            r25 = r1
            java.lang.String r1 = r21.toLowerCase()     // Catch:{ Exception -> 0x034b }
            r26 = r2
            java.lang.String r2 = "subs"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "sub"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "dubbed"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "dub"
            boolean r2 = r1.endsWith(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "dvdscr"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "r5"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "r6"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "camrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "tsrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "hdcam"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "hdts"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "dvdcam"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "dvdts"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "cam"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "telesync"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02e6
            java.lang.String r2 = "ts"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 == 0) goto L_0x0298
            goto L_0x02e6
        L_0x0298:
            boolean r2 = r1.contains(r8)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02dc
            java.lang.String r2 = "1080"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 == 0) goto L_0x02a7
            goto L_0x02dc
        L_0x02a7:
            java.lang.String r2 = "720p"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02d7
            java.lang.String r2 = "720"
            boolean r2 = r1.equals(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02d7
            java.lang.String r2 = "brrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02d7
            java.lang.String r2 = "bdrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02d7
            java.lang.String r2 = "hdrip"
            boolean r2 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r2 != 0) goto L_0x02d7
            java.lang.String r2 = "web-dl"
            boolean r1 = r1.contains(r2)     // Catch:{ Exception -> 0x033b }
            if (r1 == 0) goto L_0x02de
        L_0x02d7:
            java.lang.String r1 = "HD"
            r20 = r1
            goto L_0x02de
        L_0x02dc:
            r20 = r8
        L_0x02de:
            int r14 = r14 + 1
            r1 = r25
            r2 = r26
            goto L_0x020b
        L_0x02e6:
            r1 = 1
            goto L_0x02eb
        L_0x02e8:
            r26 = r2
            r1 = 0
        L_0x02eb:
            if (r1 != 0) goto L_0x033f
            java.lang.String r1 = r23.a()     // Catch:{ Exception -> 0x033b }
            if (r12 == 0) goto L_0x02f8
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r13.a(r0)     // Catch:{ Exception -> 0x033b }
            goto L_0x02fc
        L_0x02f8:
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r13.b(r0)     // Catch:{ Exception -> 0x033b }
        L_0x02fc:
            if (r2 == 0) goto L_0x0314
            java.lang.String r1 = r2.c()     // Catch:{ Exception -> 0x033b }
            boolean r1 = r1.equalsIgnoreCase(r11)     // Catch:{ Exception -> 0x033b }
            if (r1 != 0) goto L_0x030c
            java.lang.String r20 = r2.c()     // Catch:{ Exception -> 0x033b }
        L_0x030c:
            java.lang.String r1 = r2.b()     // Catch:{ Exception -> 0x033b }
            java.lang.String r1 = r7.f(r1)     // Catch:{ Exception -> 0x033b }
        L_0x0314:
            r10 = r1
            r1 = 0
            boolean[] r11 = new boolean[r1]     // Catch:{ Exception -> 0x0330 }
            r1 = r23
            r14 = r26
            r2 = r24
            r21 = r3
            r3 = r0
            r22 = r4
            r4 = r20
            r20 = r5
            r5 = r10
            r10 = r6
            r6 = r11
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x032e }
            goto L_0x035f
        L_0x032e:
            r0 = move-exception
            goto L_0x036c
        L_0x0330:
            r0 = move-exception
            r14 = r26
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            goto L_0x036d
        L_0x033b:
            r0 = move-exception
            r14 = r26
            goto L_0x034d
        L_0x033f:
            r14 = r26
            goto L_0x0343
        L_0x0342:
            r14 = r2
        L_0x0343:
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            goto L_0x035f
        L_0x034b:
            r0 = move-exception
        L_0x034c:
            r14 = r2
        L_0x034d:
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            goto L_0x036c
        L_0x0355:
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            r19 = r14
            r14 = r2
        L_0x035f:
            r1 = 0
            goto L_0x0372
        L_0x0361:
            r0 = move-exception
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            r19 = r14
            r14 = r2
        L_0x036c:
            r1 = 0
        L_0x036d:
            boolean[] r2 = new boolean[r1]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)
        L_0x0372:
            r6 = r10
            r2 = r14
            r14 = r19
            r5 = r20
            r3 = r21
            r4 = r22
            r10 = 1
            r11 = 0
            goto L_0x00e5
        L_0x0380:
            r21 = r3
            r22 = r4
            r20 = r5
            r10 = r6
            r19 = r14
            r1 = 0
            r6 = r10
            r14 = r19
            r5 = r20
            r3 = r21
            r4 = r22
            r10 = 1
            r11 = 0
            goto L_0x00c3
        L_0x0397:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.CBB.a(io.reactivex.ObservableEmitter, com.movie.data.model.MovieInfo, java.lang.String, java.lang.String):void");
    }
}
