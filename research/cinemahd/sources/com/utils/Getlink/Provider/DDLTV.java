package com.utils.Getlink.Provider;

import android.util.Base64;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;

public class DDLTV extends BaseProvider {
    String c = Utils.getProvider(27);

    private String c() {
        try {
            return new String(Base64.decode("QUZCRjhFMzNBMTk3ODdEMQ==", 0), "UTF-8");
        } catch (Exception e) {
            Logger.a((Throwable) e, true);
            try {
                return new String(Base64.decode("QUZCRjhFMzNBMTk3ODdEMQ==", 0));
            } catch (Exception e2) {
                Logger.a((Throwable) e2, true);
                return "";
            }
        }
    }

    public String a() {
        return "DDLTV";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:109:0x0285, code lost:
        if (r2.equals("720") == false) goto L_0x028b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:93:0x0260, code lost:
        r1 = true;
     */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x02a5 A[Catch:{ Exception -> 0x032b }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.movie.data.model.MovieInfo r22, io.reactivex.ObservableEmitter<? super com.original.tase.model.media.MediaSource> r23) {
        /*
            r21 = this;
            r7 = r21
            r0 = r22
            java.lang.String r8 = ""
            boolean r1 = com.utils.Getlink.Provider.BaseProvider.b()
            if (r1 == 0) goto L_0x034c
            com.original.tase.helper.DirectoryIndexHelper r9 = new com.original.tase.helper.DirectoryIndexHelper
            r9.<init>()
            java.lang.String r10 = r22.getName()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "S"
            r1.append(r2)
            java.lang.String r2 = r0.session
            int r2 = java.lang.Integer.parseInt(r2)
            java.lang.String r2 = com.original.tase.utils.Utils.a((int) r2)
            r1.append(r2)
            java.lang.String r2 = "E"
            r1.append(r2)
            java.lang.String r0 = r0.eps
            int r0 = java.lang.Integer.parseInt(r0)
            java.lang.String r0 = com.original.tase.utils.Utils.a((int) r0)
            r1.append(r0)
            java.lang.String r11 = r1.toString()
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            r0.append(r10)
            java.lang.String r1 = " "
            r0.append(r1)
            r0.append(r11)
            java.lang.String r0 = r0.toString()
            java.lang.String r2 = "(\\\\\\|/| -|:|;|\\*|\\?|\"|\\'|<|>|\\|)"
            java.lang.String r0 = r0.replaceAll(r2, r1)
            java.lang.String r2 = "  "
            java.lang.String r0 = r0.replace(r2, r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = r7.c
            r1.append(r2)
            r2 = 2
            java.lang.Object[] r2 = new java.lang.Object[r2]
            java.lang.String r3 = r21.c()
            r12 = 0
            r2[r12] = r3
            boolean[] r3 = new boolean[r12]
            java.lang.String r0 = com.original.tase.utils.Utils.a((java.lang.String) r0, (boolean[]) r3)
            r13 = 1
            r2[r13] = r0
            com.original.tase.helper.http.HttpHelper r0 = com.original.tase.helper.http.HttpHelper.e()
            java.lang.String r3 = "/api?key=%s&keyword=%s&limit=50"
            java.lang.String r2 = java.lang.String.format(r3, r2)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.util.Map[] r2 = new java.util.Map[r12]
            java.lang.String r0 = r0.a((java.lang.String) r1, (java.util.Map<java.lang.String, java.lang.String>[]) r2)
            java.util.ArrayList r14 = new java.util.ArrayList
            r14.<init>()
            com.google.gson.JsonParser r1 = new com.google.gson.JsonParser     // Catch:{ Exception -> 0x0345 }
            r1.<init>()     // Catch:{ Exception -> 0x0345 }
            com.google.gson.JsonElement r0 = r1.a((java.lang.String) r0)     // Catch:{ Exception -> 0x0345 }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ Exception -> 0x0345 }
            java.lang.String r1 = "results"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r1)     // Catch:{ Exception -> 0x0345 }
            com.google.gson.JsonArray r0 = r0.e()     // Catch:{ Exception -> 0x0345 }
            java.util.Iterator r15 = r0.iterator()     // Catch:{ Exception -> 0x0345 }
        L_0x00b6:
            boolean r0 = r15.hasNext()     // Catch:{ Exception -> 0x0345 }
            if (r0 == 0) goto L_0x034c
            java.lang.Object r0 = r15.next()     // Catch:{ Exception -> 0x0345 }
            com.google.gson.JsonElement r0 = (com.google.gson.JsonElement) r0     // Catch:{ Exception -> 0x0345 }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ Exception -> 0x0345 }
            java.lang.String r1 = "release"
            com.google.gson.JsonElement r1 = r0.a((java.lang.String) r1)     // Catch:{ Exception -> 0x0345 }
            java.lang.String r1 = r1.i()     // Catch:{ Exception -> 0x0345 }
            java.lang.String r2 = "quality"
            com.google.gson.JsonElement r2 = r0.a((java.lang.String) r2)     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = r2.i()     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = r2.trim()     // Catch:{ Exception -> 0x00e3 }
            java.lang.String r2 = r2.toLowerCase()     // Catch:{ Exception -> 0x00e3 }
            goto L_0x00e4
        L_0x00e3:
            r2 = r8
        L_0x00e4:
            java.lang.String r3 = "links"
            com.google.gson.JsonElement r0 = r0.a((java.lang.String) r3)     // Catch:{ Exception -> 0x0333 }
            com.google.gson.JsonObject r0 = r0.f()     // Catch:{ Exception -> 0x0333 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0333 }
            java.lang.String r3 = com.original.tase.helper.TitleHelper.f(r10)     // Catch:{ Exception -> 0x0333 }
            java.lang.String r4 = "(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d+|3D)(\\.|\\)|\\]|\\s|)(.+|)"
            java.lang.String r4 = r1.replaceAll(r4, r8)     // Catch:{ Exception -> 0x0333 }
            java.lang.String r4 = com.original.tase.helper.TitleHelper.f(r4)     // Catch:{ Exception -> 0x0333 }
            boolean r3 = r3.equals(r4)     // Catch:{ Exception -> 0x0333 }
            if (r3 == 0) goto L_0x032f
            java.lang.String r3 = "[\\.|\\(|\\[|\\s]([2-9]0\\d{2}|1[5-9]\\d{2})[\\.|\\)|\\]|\\s]"
            java.util.ArrayList r3 = com.original.tase.utils.Regex.b(r1, r3, r13)     // Catch:{ Exception -> 0x0333 }
            java.lang.Object r3 = r3.get(r12)     // Catch:{ Exception -> 0x0333 }
            java.util.List r3 = (java.util.List) r3     // Catch:{ Exception -> 0x0333 }
            java.lang.String r4 = "[\\.|\\(|\\[|\\s](S\\d*E\\d*)[\\.|\\)|\\]|\\s]"
            java.util.ArrayList r4 = com.original.tase.utils.Regex.b(r1, r4, r13)     // Catch:{ Exception -> 0x0333 }
            java.lang.Object r4 = r4.get(r12)     // Catch:{ Exception -> 0x0333 }
            java.util.Collection r4 = (java.util.Collection) r4     // Catch:{ Exception -> 0x0333 }
            r3.addAll(r4)     // Catch:{ Exception -> 0x0333 }
            java.lang.String r4 = "[\\.|\\(|\\[|\\s](S\\d*)[\\.|\\)|\\]|\\s]"
            java.util.ArrayList r4 = com.original.tase.utils.Regex.b(r1, r4, r13)     // Catch:{ Exception -> 0x0333 }
            java.lang.Object r4 = r4.get(r12)     // Catch:{ Exception -> 0x0333 }
            java.util.Collection r4 = (java.util.Collection) r4     // Catch:{ Exception -> 0x0333 }
            r3.addAll(r4)     // Catch:{ Exception -> 0x0333 }
            int r4 = r3.size()     // Catch:{ Exception -> 0x0333 }
            if (r4 <= 0) goto L_0x032f
            java.util.Iterator r3 = r3.iterator()     // Catch:{ Exception -> 0x0333 }
        L_0x013a:
            boolean r4 = r3.hasNext()     // Catch:{ Exception -> 0x0333 }
            if (r4 == 0) goto L_0x0159
            java.lang.Object r4 = r3.next()     // Catch:{ Exception -> 0x0152 }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ Exception -> 0x0152 }
            java.lang.String r4 = r4.toUpperCase()     // Catch:{ Exception -> 0x0152 }
            boolean r4 = r4.equals(r11)     // Catch:{ Exception -> 0x0152 }
            if (r4 == 0) goto L_0x013a
            r3 = 1
            goto L_0x015a
        L_0x0152:
            r0 = move-exception
            r18 = r8
        L_0x0155:
            r1 = 0
            r8 = 1
            goto L_0x0338
        L_0x0159:
            r3 = 0
        L_0x015a:
            if (r3 == 0) goto L_0x032f
            java.lang.String r1 = r1.toUpperCase()     // Catch:{ Exception -> 0x0333 }
            java.lang.String r3 = "(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)"
            java.lang.String r1 = r1.replaceAll(r3, r8)     // Catch:{ Exception -> 0x0333 }
            java.lang.String r3 = "\\.|\\(|\\)|\\[|\\]|\\s|\\-"
            java.lang.String[] r1 = r1.split(r3)     // Catch:{ Exception -> 0x0333 }
            int r3 = r1.length     // Catch:{ Exception -> 0x0333 }
            java.lang.String r6 = "HQ"
            r5 = r6
            r4 = 0
        L_0x0171:
            java.lang.String r12 = "720"
            java.lang.String r16 = "HD"
            java.lang.String r13 = "720p"
            r17 = r5
            java.lang.String r5 = "1080"
            r18 = r8
            java.lang.String r8 = "1080p"
            if (r4 >= r3) goto L_0x0265
            r19 = r1[r4]     // Catch:{ Exception -> 0x0262 }
            r20 = r1
            java.lang.String r1 = r19.toLowerCase()     // Catch:{ Exception -> 0x0262 }
            r19 = r3
            java.lang.String r3 = "subs"
            boolean r3 = r1.endsWith(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "sub"
            boolean r3 = r1.endsWith(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "dubbed"
            boolean r3 = r1.endsWith(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "dub"
            boolean r3 = r1.endsWith(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "dvdscr"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "r5"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "r6"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "camrip"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "tsrip"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "hdcam"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "hdts"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "dvdcam"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "dvdts"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "cam"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "telesync"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "ts"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0260
            java.lang.String r3 = "3d"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 == 0) goto L_0x0214
            goto L_0x0260
        L_0x0214:
            boolean r3 = r1.contains(r8)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0254
            boolean r3 = r1.equals(r5)     // Catch:{ Exception -> 0x0262 }
            if (r3 == 0) goto L_0x0221
            goto L_0x0254
        L_0x0221:
            boolean r3 = r1.contains(r13)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0251
            boolean r3 = r1.equals(r12)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0251
            java.lang.String r3 = "brrip"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0251
            java.lang.String r3 = "bdrip"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0251
            java.lang.String r3 = "hdrip"
            boolean r3 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r3 != 0) goto L_0x0251
            java.lang.String r3 = "web-dl"
            boolean r1 = r1.contains(r3)     // Catch:{ Exception -> 0x0262 }
            if (r1 == 0) goto L_0x024e
            goto L_0x0251
        L_0x024e:
            r5 = r17
            goto L_0x0255
        L_0x0251:
            r5 = r16
            goto L_0x0255
        L_0x0254:
            r5 = r8
        L_0x0255:
            int r4 = r4 + 1
            r8 = r18
            r3 = r19
            r1 = r20
            r13 = 1
            goto L_0x0171
        L_0x0260:
            r1 = 1
            goto L_0x0266
        L_0x0262:
            r0 = move-exception
            goto L_0x0155
        L_0x0265:
            r1 = 0
        L_0x0266:
            if (r1 != 0) goto L_0x0331
            boolean r1 = r2.isEmpty()     // Catch:{ Exception -> 0x032d }
            if (r1 != 0) goto L_0x028b
            boolean r1 = r2.contains(r8)     // Catch:{ Exception -> 0x0262 }
            if (r1 != 0) goto L_0x0288
            boolean r1 = r2.equals(r5)     // Catch:{ Exception -> 0x0262 }
            if (r1 == 0) goto L_0x027b
            goto L_0x0288
        L_0x027b:
            boolean r1 = r2.contains(r13)     // Catch:{ Exception -> 0x0262 }
            if (r1 != 0) goto L_0x028d
            boolean r1 = r2.equals(r12)     // Catch:{ Exception -> 0x0262 }
            if (r1 == 0) goto L_0x028b
            goto L_0x028d
        L_0x0288:
            r16 = r8
            goto L_0x028d
        L_0x028b:
            r16 = r17
        L_0x028d:
            java.lang.String r1 = "['\"](http.+?)['\"]"
            r8 = 1
            java.util.ArrayList r0 = com.original.tase.utils.Regex.b((java.lang.String) r0, (java.lang.String) r1, (int) r8, (boolean) r8)     // Catch:{ Exception -> 0x032b }
            r1 = 0
            java.lang.Object r0 = r0.get(r1)     // Catch:{ Exception -> 0x0329 }
            java.util.ArrayList r0 = (java.util.ArrayList) r0     // Catch:{ Exception -> 0x032b }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ Exception -> 0x032b }
        L_0x029f:
            boolean r1 = r0.hasNext()     // Catch:{ Exception -> 0x032b }
            if (r1 == 0) goto L_0x033d
            java.lang.Object r1 = r0.next()     // Catch:{ Exception -> 0x032b }
            r3 = r1
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ Exception -> 0x032b }
            java.lang.String r1 = "openload"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = ".7z"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = ".rar"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = ".zip"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = ".iso"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = ".avi"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = ".flv"
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            java.lang.String r1 = "imdb."
            boolean r1 = r3.contains(r1)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            boolean r1 = r14.contains(r3)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x0325
            r14.add(r3)     // Catch:{ Exception -> 0x032b }
            java.lang.String r1 = r21.a()     // Catch:{ Exception -> 0x032b }
            com.original.tase.helper.DirectoryIndexHelper$ParsedLinkModel r2 = r9.b(r3)     // Catch:{ Exception -> 0x032b }
            if (r2 == 0) goto L_0x0315
            java.lang.String r1 = r2.c()     // Catch:{ Exception -> 0x032b }
            boolean r1 = r1.equalsIgnoreCase(r6)     // Catch:{ Exception -> 0x032b }
            if (r1 != 0) goto L_0x030d
            java.lang.String r16 = r2.c()     // Catch:{ Exception -> 0x032b }
        L_0x030d:
            java.lang.String r1 = r2.b()     // Catch:{ Exception -> 0x032b }
            java.lang.String r1 = r7.f(r1)     // Catch:{ Exception -> 0x032b }
        L_0x0315:
            r5 = r1
            r1 = 0
            boolean[] r12 = new boolean[r1]     // Catch:{ Exception -> 0x0329 }
            r1 = r21
            r2 = r23
            r4 = r16
            r13 = r6
            r6 = r12
            r1.a(r2, r3, r4, r5, r6)     // Catch:{ Exception -> 0x032b }
            goto L_0x0326
        L_0x0325:
            r13 = r6
        L_0x0326:
            r6 = r13
            goto L_0x029f
        L_0x0329:
            r0 = move-exception
            goto L_0x0338
        L_0x032b:
            r0 = move-exception
            goto L_0x0337
        L_0x032d:
            r0 = move-exception
            goto L_0x0336
        L_0x032f:
            r18 = r8
        L_0x0331:
            r8 = 1
            goto L_0x033d
        L_0x0333:
            r0 = move-exception
            r18 = r8
        L_0x0336:
            r8 = 1
        L_0x0337:
            r1 = 0
        L_0x0338:
            boolean[] r2 = new boolean[r1]     // Catch:{ Exception -> 0x0343 }
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)     // Catch:{ Exception -> 0x0345 }
        L_0x033d:
            r8 = r18
            r12 = 0
            r13 = 1
            goto L_0x00b6
        L_0x0343:
            r0 = move-exception
            goto L_0x0347
        L_0x0345:
            r0 = move-exception
            r1 = 0
        L_0x0347:
            boolean[] r1 = new boolean[r1]
            com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r1)
        L_0x034c:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.Getlink.Provider.DDLTV.b(com.movie.data.model.MovieInfo, io.reactivex.ObservableEmitter):void");
    }
}
