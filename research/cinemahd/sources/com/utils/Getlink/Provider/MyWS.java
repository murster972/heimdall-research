package com.utils.Getlink.Provider;

import android.util.Base64;
import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.net.URL;
import java.util.Iterator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MyWS extends BaseProvider {
    private String c = Utils.getProvider(63);

    public String a() {
        return "MyWS";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String str2;
        String str3;
        String str4;
        String str5;
        if (movieInfo.name.equalsIgnoreCase("The Daily Show with Trevor Noah")) {
            str = "The Daily Show";
        } else {
            str = movieInfo.name;
        }
        String b = HttpHelper.e().b(this.c + "/search/" + com.original.tase.utils.Utils.a(TitleHelper.e(str.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.c);
        if (!b.toLowerCase().contains("search result")) {
            this.c = "http://watchtvseries.unblckd.gdn";
            b = HttpHelper.e().b(this.c + "/search/" + com.original.tase.utils.Utils.a(TitleHelper.e(str.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.c);
            if (!b.toLowerCase().contains("search result")) {
                this.c = "http://watchseries.bypassed.org";
                b = HttpHelper.e().b(this.c + "/search/" + com.original.tase.utils.Utils.a(TitleHelper.e(str.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.c);
                if (!b.toLowerCase().contains("search result")) {
                    this.c = "http://watchseries.bypassed.bz";
                    b = HttpHelper.e().b(this.c + "/search/" + com.original.tase.utils.Utils.a(TitleHelper.e(str.replace("Marvel's ", "").replace("DC's ", "")), new boolean[0]), this.c);
                }
            }
        }
        Iterator it2 = Jsoup.b(b).g("a[href][title][target=\"_blank\"]").iterator();
        while (true) {
            if (!it2.hasNext()) {
                str2 = "";
                break;
            }
            Element element = (Element) it2.next();
            Element h = element.h("strong");
            if (h != null) {
                str2 = element.b("href");
                String G = h.G();
                String a2 = Regex.a(G, "(.*?)\\s*\\((\\d{4})\\)", 1);
                String a3 = Regex.a(G, "(.*?)\\s*\\((\\d{4})\\)", 2);
                if (a2.isEmpty()) {
                    G = a2;
                }
                if (TitleHelper.f(movieInfo.name.replace("Marvel's ", "").replace("DC's ", "")).equals(TitleHelper.f(G.replace("Marvel's ", "").replace("DC's ", "")))) {
                    if (a3.trim().isEmpty() || !com.original.tase.utils.Utils.b(a3.trim()) || a3.trim().equals(movieInfo.year)) {
                        break;
                    }
                } else {
                    continue;
                }
            }
        }
        if (!str2.isEmpty()) {
            if (str2.startsWith("//")) {
                str3 = "http:" + str2;
            } else if (str2.startsWith("/")) {
                str3 = this.c + str2;
            } else if (!str2.startsWith(UriUtil.HTTP_SCHEME)) {
                str3 = this.c + "/" + str2;
            } else {
                str3 = str2;
            }
            String a4 = Regex.a(HttpHelper.e().b(str3, this.c), "href=\"([^\"]*s" + movieInfo.session + "_e" + movieInfo.eps + "(?!\\d)[^\"]*)", 1, true);
            if (a4.isEmpty()) {
                try {
                    String substring = str3.endsWith("/") ? str3.substring(0, str3.length() - 1) : str3;
                    a4 = substring.replace("/serie/", "/episode/") + "_s" + substring + "_e" + str2 + ".html";
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
            if (a4.startsWith("//")) {
                a4 = "http:" + a4;
            } else if (a4.startsWith("/")) {
                a4 = this.c + a4;
            } else if (a4.isEmpty()) {
                return;
            }
            Document b2 = Jsoup.b(HttpHelper.e().b(a4, str3));
            Elements g = b2.g("a.buttonlink[href]");
            g.addAll(b2.g("a.watchlink[href]"));
            if (g.isEmpty()) {
                g.addAll(b2.g("a[class][href][onclick][target=\"_blank\"][rel=\"nofollow\"]"));
            }
            Iterator it3 = g.iterator();
            int i = 0;
            while (it3.hasNext()) {
                Element element2 = (Element) it3.next();
                if (!observableEmitter.isDisposed()) {
                    try {
                        String b3 = element2.b("href");
                        if (b3.contains("cale.html")) {
                            str4 = com.original.tase.utils.Utils.a(new URL(b3)).get("r");
                            str5 = new String(Base64.decode(str4, 0), "UTF-8");
                            a(observableEmitter, str5, "HQ", new boolean[0]);
                        }
                    } catch (Throwable th2) {
                        Logger.a(th2, new boolean[0]);
                    }
                    i++;
                    if (i > 20) {
                        return;
                    }
                } else {
                    return;
                }
            }
        }
    }
}
