package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class PrimeWire extends BaseProvider {
    private String c = Utils.getProvider(68);

    public String a() {
        return "PrimeWire";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String b = b(movieInfo);
        if (!b.isEmpty()) {
            a(observableEmitter, b, movieInfo);
        }
    }

    private void a(ObservableEmitter<? super MediaSource> observableEmitter, String str, MovieInfo movieInfo) {
        String str2;
        String str3 = str;
        MovieInfo movieInfo2 = movieInfo;
        if (str3.startsWith("/")) {
            str3 = this.c + str3;
        }
        boolean z = movieInfo.getType().intValue() == 1;
        HashMap hashMap = new HashMap();
        hashMap.put("referer", this.c + "/");
        String a2 = HttpHelper.e().a(str3, (Map<String, String>[]) new Map[]{hashMap});
        hashMap.put("referer", str3);
        String a3 = Regex.a(a2, "data-id\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1);
        String a4 = Regex.a(a2, "<strong>(\\w+)<\\/strong>", 1);
        if (a4.isEmpty()) {
            a4 = "HD";
        }
        hashMap.put("x-requested-with", "XMLHttpRequest");
        if (!z) {
            Iterator it2 = Jsoup.b(HttpHelper.e().a(this.c + "/ajax/tv/seasons/" + a3, (Map<String, String>[]) new Map[]{hashMap})).g("ul.ulclear.slcs-ul").b("li").iterator();
            while (true) {
                if (!it2.hasNext()) {
                    a3 = "";
                    break;
                }
                Element element = (Element) it2.next();
                String a5 = element.g("a").a("title");
                if (!a5.isEmpty() && Regex.a(a5, "(?:S|s)eason\\s*(?::|)\\s*(\\d+)", 1).equalsIgnoreCase(movieInfo2.session)) {
                    a3 = Regex.a(element.toString(), "episodes\\((\\d+)\\)", 1);
                    break;
                }
            }
            if (!a3.isEmpty()) {
                str2 = "season";
            } else {
                return;
            }
        } else {
            str2 = "movie";
        }
        Iterator it3 = Jsoup.b(HttpHelper.e().a(this.c + "/ajax/" + str2 + "/episodes/" + a3, (Map<String, String>[]) new Map[]{hashMap})).g("a[id][data-linkid]").iterator();
        HashMap hashMap2 = new HashMap();
        hashMap2.put("user-agent", Constants.f5838a);
        while (it3.hasNext()) {
            Element element2 = (Element) it3.next();
            String b = element2.b("data-linkid");
            if (z || Regex.a(element2.G(), "(?:E|e)pisode\\s*(\\d+)\\s*", 1).equalsIgnoreCase(movieInfo2.eps)) {
                String a6 = HttpHelper.e().a(this.c + "/ajax/get_link/" + b + "?_token=03AHaCkAaKMGBOR8oAUVhZbYYFfefnyny4kRPaWtRadtU6flgTn6c7H9kPgQ64Jw_WISSAluBINJSa8UIg-8Hp3lEjtca56WCuz7xpCbqZ-OgZlfNHyoBcU_KPimb_Jbe5fIHNtx2C7k63yLrifHsZbwLsXu6m0IVu5p8-lI4QQhadSWac-CCoJ-9SLBUfygEO7fcVjrnxzcOVt45vTyPtW83gza2FOXXA5Q_v0XooOrcOviIFgUekKlWVvuQEsuxaS9EiEKr_8YIMaUXJxImkIegWB08RUJ7h8HmOPxLfX_NFG6cDSHooAk-" + Utils.a(85), (Map<String, String>[]) new Map[]{hashMap});
                if (a6.isEmpty() || a6.contains("error")) {
                    a6 = HttpHelper.e().a(this.c + "/ajax/get_link/" + b, (Map<String, String>[]) new Map[]{hashMap});
                }
                if (!a6.isEmpty()) {
                    ArrayList arrayList = Regex.b(a6, "['\"]file['\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0);
                    if (arrayList.isEmpty()) {
                        arrayList = Regex.b(a6, "['\"]src['\"]\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0);
                    }
                    Iterator it4 = arrayList.iterator();
                    while (it4.hasNext()) {
                        String replace = it4.next().toString().replace("\\/", "/");
                        boolean k = GoogleVideoHelper.k(replace);
                        MediaSource mediaSource = new MediaSource(a(), k ? "GoogleVideo" : "CDN-FastServer", false);
                        mediaSource.setStreamLink(replace);
                        if (k) {
                            mediaSource.setPlayHeader(hashMap2);
                        }
                        mediaSource.setQuality(k ? GoogleVideoHelper.h(replace) : a4);
                        observableEmitter.onNext(mediaSource);
                    }
                }
                ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
            }
        }
    }

    public String b(MovieInfo movieInfo) {
        boolean z = movieInfo.getType().intValue() == 1;
        HttpHelper e = HttpHelper.e();
        Iterator it2 = Jsoup.b(e.b(this.c + "/search/" + TitleHelper.a(movieInfo.name.toLowerCase(), "-"), this.c + "/")).g("div.film-detail").iterator();
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            String G = element.h("a").G();
            if (z) {
                String a2 = Regex.a(element.toString(), "<span\\s*class=\"fdi-item\">(\\d+)<\\/span>", 1);
                if (movieInfo.name.equalsIgnoreCase(G) && movieInfo.year.equalsIgnoreCase(a2)) {
                    return element.h("a").b("href");
                }
            } else if (TitleHelper.f(G).equalsIgnoreCase(TitleHelper.f(TitleHelper.e(movieInfo.getName()))) && element.h("a").b("href").contains("/tv/")) {
                return element.h("a").b("href");
            }
        }
        return "";
    }
}
