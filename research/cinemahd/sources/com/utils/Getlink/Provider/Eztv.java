package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.movie.data.model.realdebrid.MagnetObject;
import com.original.tase.helper.DirectoryIndexHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import okhttp3.internal.cache.DiskLruCache;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class Eztv extends BaseProvider {
    public String c = Utils.getProvider(5);

    public String a() {
        return "KickassTorrents";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        if (BaseProvider.b()) {
            c(movieInfo, observableEmitter);
        }
    }

    public void c(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        Iterator it2;
        String str2;
        boolean z;
        DirectoryIndexHelper.ParsedLinkModel parsedLinkModel;
        int i;
        MovieInfo movieInfo2 = movieInfo;
        String str3 = "1080p";
        boolean z2 = movieInfo.getType().intValue() == 1;
        if (z2) {
            str = " " + movieInfo2.year;
        } else {
            str = " S" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.session)) + "E" + com.original.tase.utils.Utils.a(Integer.parseInt(movieInfo2.eps));
        }
        HttpHelper.e().a(this.c, (Map<String, String>[]) new Map[0]);
        HashMap hashMap = new HashMap();
        hashMap.put("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        hashMap.put("upgrade-insecure-requests", DiskLruCache.VERSION_1);
        hashMap.put("accept-language", "en-US;q=0.9,en;q=0.8");
        String a2 = com.original.tase.utils.Utils.a(movieInfo2.name + str, new boolean[0]);
        hashMap.put("referer", this.c + "/search/" + TitleHelper.a(a2, "-"));
        Iterator it3 = Jsoup.b(HttpHelper.e().a(this.c + "/search/?q1=" + a2 + "&q2=&search=Search", (Map<String, String>[]) new Map[]{hashMap})).g("td.forum_thread_post").b("a.magnet[title]").iterator();
        DirectoryIndexHelper directoryIndexHelper = new DirectoryIndexHelper();
        ArrayList arrayList = new ArrayList();
        HashMap hashMap2 = new HashMap();
        while (it3.hasNext()) {
            Element element = (Element) it3.next();
            element.b("href");
            String b = element.b("title");
            if (z2) {
                if (TitleHelper.f(b).startsWith(TitleHelper.f(TitleHelper.e(movieInfo.getName()) + movieInfo2.year))) {
                    hashMap2.put(Regex.a(element.toString(), "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1), b);
                }
            } else {
                if (TitleHelper.a(b.toLowerCase().replace(movieInfo2.year, ""), "").startsWith(TitleHelper.a(movieInfo.getName().toLowerCase() + str.toLowerCase(), ""))) {
                    hashMap2.put(Regex.a(element.toString(), "href\\s*=\\s*['\"](magnet.*?[^'\"]+)['\"]?", 1), b);
                }
            }
            movieInfo2 = movieInfo;
        }
        new HashMap();
        Iterator it4 = hashMap2.entrySet().iterator();
        while (it4.hasNext()) {
            Map.Entry entry = (Map.Entry) it4.next();
            try {
                String replace = TitleHelper.a((String) entry.getValue(), ".").replace("..", ".");
                String[] split = replace.toUpperCase().replaceAll("(.+)(\\.|\\(|\\[|\\s)([2-9]0\\d{2}|1[5-9]\\d{2}|S\\d*E\\d*|S\\d*)(\\.|\\)|\\]|\\s)", "").split("\\.|\\(|\\)|\\[|\\]|\\s|\\-");
                int length = split.length;
                String str4 = "HQ";
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        it2 = it4;
                        str2 = str3;
                        z = false;
                        break;
                    }
                    String lowerCase = split[i2].toLowerCase();
                    it2 = it4;
                    try {
                        if (lowerCase.contains("dvdscr") || lowerCase.contains("camrip") || lowerCase.contains("tsrip") || lowerCase.contains("hdcam") || lowerCase.contains("hdtc") || lowerCase.contains("hdts") || lowerCase.contains("dvdcam") || lowerCase.contains("dvdts") || lowerCase.contains("cam") || lowerCase.contains("telesync")) {
                            break;
                        } else if (lowerCase.contains("ts")) {
                            break;
                        } else {
                            boolean contains = lowerCase.contains(str3);
                            str2 = str3;
                            String str5 = "720p";
                            if (!contains) {
                                try {
                                    if (!lowerCase.equals("1080")) {
                                        if (!lowerCase.contains(str5)) {
                                            if (!lowerCase.equals("720")) {
                                                if (lowerCase.contains("brrip") || lowerCase.contains("bdrip") || lowerCase.contains("hdrip") || lowerCase.contains("web-dl")) {
                                                    str5 = "HD";
                                                } else {
                                                    i2++;
                                                    it4 = it2;
                                                    str3 = str2;
                                                }
                                            }
                                        }
                                        str4 = str5;
                                        i2++;
                                        it4 = it2;
                                        str3 = str2;
                                    }
                                } catch (Throwable unused) {
                                }
                            }
                            str4 = str2;
                            i2++;
                            it4 = it2;
                            str3 = str2;
                        }
                    } catch (Throwable unused2) {
                        str2 = str3;
                        it4 = it2;
                        str3 = str2;
                    }
                }
                str2 = str3;
                z = true;
                String str6 = (String) entry.getKey();
                String a3 = a();
                if (z2) {
                    parsedLinkModel = directoryIndexHelper.a(replace);
                } else {
                    parsedLinkModel = directoryIndexHelper.b(replace);
                }
                if (parsedLinkModel != null) {
                    String c2 = parsedLinkModel.c();
                    if (!c2.equalsIgnoreCase("HQ")) {
                        str4 = c2;
                    }
                    String b2 = parsedLinkModel.b();
                    i = 1;
                    a3 = a(b2, true);
                } else {
                    i = 1;
                }
                String lowerCase2 = Regex.a(str6, "(magnet:\\?xt=urn:btih:[^&.]+)", i).toLowerCase();
                if (z) {
                    str4 = "CAM-" + str4;
                }
                arrayList.add(new MagnetObject(a3, lowerCase2, str4, a()));
            } catch (Throwable unused3) {
                it2 = it4;
                str2 = str3;
                it4 = it2;
                str3 = str2;
            }
            it4 = it2;
            str3 = str2;
        }
        if (arrayList.size() > 0) {
            MediaSource mediaSource = new MediaSource(a(), "", false);
            mediaSource.setTorrent(true);
            mediaSource.setMagnetObjects(arrayList);
            mediaSource.setStreamLink("magnet:KickassTorrents");
            observableEmitter.onNext(mediaSource);
        }
    }
}
