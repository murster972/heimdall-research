package com.utils.Getlink.Provider;

import com.facebook.common.util.UriUtil;
import com.movie.data.model.MovieInfo;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.original.tase.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

public class QQMovies extends BaseProvider {
    public String c = "http://qqmovies.co";

    public String a() {
        return "QQMovies";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        c(movieInfo, observableEmitter);
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        c(movieInfo, observableEmitter);
    }

    public void c(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        Iterator it2;
        String str;
        boolean z = movieInfo.getType().intValue() == 1;
        String str2 = this.c + "/watch/" + (TitleHelper.a(movieInfo.name.toLowerCase(), "-") + ".html");
        String b = HttpHelper.e().b(str2, this.c);
        String a2 = Regex.a(b, "<span[^>]+class=\"label label-primary\"[^>]*>(\\w+)<", 1);
        if (!z) {
            b = HttpHelper.e().b(Regex.a(b, "<a[^>]+href=\"([^\"]+)\"[^>]*>S" + Utils.a(Integer.parseInt(movieInfo.session)) + "E" + Utils.a(Integer.parseInt(movieInfo.eps)), 1), str2);
        }
        if (z) {
            it2 = Jsoup.b(b).g("div.season").b("a").iterator();
        } else {
            it2 = Jsoup.b(b).g("iframe.responsive-embed-item").iterator();
        }
        while (it2.hasNext()) {
            Element element = (Element) it2.next();
            if (z) {
                str = Regex.a(HttpHelper.e().b(element.b("href"), str2), "<iframe[^>]+src=\"([^\"]+)\"[^>]*>", 1);
            } else {
                str = element.b("src");
            }
            String a3 = HttpHelper.e().a(str, (Map<String, String>[]) new Map[0]);
            ArrayList arrayList = new ArrayList();
            String a4 = Regex.a(a3, "href\\s*=\\s*['\"]([^'\"]*http+[^'\"]*)['\"]>", 1);
            if (!a4.isEmpty()) {
                arrayList.add(a4);
            }
            if (arrayList.size() == 0) {
                arrayList.addAll(Regex.b(a3, "file\\s*:\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0));
            }
            if (arrayList.size() == 0) {
                arrayList.addAll(Regex.b(a3, "pageUrl*\\s*=\\s*['\"]([^'\"]+[^'\"]*)['\"]", 1, true).get(0));
            }
            if (arrayList.size() == 0) {
                arrayList.add(Regex.a(a3, "content*\\s*=\\s*['\"]([^'\"]*openload+[^'\"]*)['\"]", 1));
            }
            arrayList.add(Regex.a(a3, "<iframe.*src\\s*=\\s*['\"]([^'\"]+)['\"]", 1));
            Iterator it3 = arrayList.iterator();
            while (it3.hasNext()) {
                String str3 = (String) it3.next();
                if (str3 != null && !str3.isEmpty() && str3.contains(UriUtil.HTTP_SCHEME)) {
                    MediaSource mediaSource = new MediaSource(a(), a2, false);
                    mediaSource.setStreamLink(str3);
                    mediaSource.setQuality(a2);
                    observableEmitter.onNext(mediaSource);
                }
            }
        }
    }
}
