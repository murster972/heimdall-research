package com.utils.Getlink.Provider;

import com.movie.data.model.MovieInfo;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.event.ReCaptchaRequiredEvent;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.TitleHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import io.reactivex.ObservableEmitter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Sezonlukdizi extends BaseProvider {
    String c = Utils.getProvider(34);

    public String a() {
        return "Sezonlukdizi";
    }

    /* access modifiers changed from: protected */
    public void a(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
    }

    /* access modifiers changed from: protected */
    public void b(MovieInfo movieInfo, ObservableEmitter<? super MediaSource> observableEmitter) {
        String str;
        String str2;
        String str3;
        ArrayList arrayList;
        Iterator it2;
        Object obj;
        int i;
        ArrayList arrayList2;
        HashMap hashMap;
        String str4;
        String a2;
        String str5;
        boolean z;
        String str6;
        Iterator it3;
        Iterator it4;
        String str7;
        String str8;
        Sezonlukdizi sezonlukdizi = this;
        MovieInfo movieInfo2 = movieInfo;
        ObservableEmitter<? super MediaSource> observableEmitter2 = observableEmitter;
        String replace = TitleHelper.e(movieInfo.getName().replace("Marvel's ", "").replace("DC's ", "")).replace("'", "");
        if (replace.equalsIgnoreCase("Outlander")) {
            replace = "outlanderr";
        }
        if (replace.endsWith(".")) {
            replace = replace.substring(0, replace.length() - 1) + "-";
        }
        String str9 = sezonlukdizi.c + "/" + TitleHelper.g(replace) + "/" + movieInfo2.session + "-sezon-" + movieInfo2.eps + "-bolum.html";
        String replaceAll = HttpHelper.e().a(str9, Constants.f5838a, sezonlukdizi.c, (Map<String, String>[]) new Map[0]).replaceAll("[^\\x00-\\x7F]+", " ");
        if (replaceAll.contains("Please complete the security check to access")) {
            RxBus.b().a(new ReCaptchaRequiredEvent(a(), sezonlukdizi.c));
            return;
        }
        Document b = Jsoup.b(replaceAll);
        Elements elements = new Elements();
        Elements g = b.g("div#embed");
        HashMap<String, String> a3 = Constants.a();
        a3.put("User-Agent", Constants.f5838a);
        a3.put("Referer", str9);
        Iterator it5 = b.g("div#playerMenu").b("div[data-id][class=item]").iterator();
        String str10 = "/ajax/dataEmbed.asp";
        String str11 = "data-id";
        Object obj2 = "Referer";
        String str12 = "iframe[src]";
        if (!it5.hasNext()) {
            Element h = b.h("div#playerMenu").h("div#dilsec");
            if (h != null) {
                String b2 = h.b(str11);
                if (!b2.isEmpty()) {
                    str = str9;
                    for (Iterator it6 = Regex.b(HttpHelper.e().a(sezonlukdizi.c + "/ajax/dataAlternatif.asp", String.format("bid=%s&dil=1", new Object[]{b2}), (Map<String, String>[]) new Map[]{a3}), "['\"]id['\"]\\s*:(\\d\\w+)", 1, true).get(0).iterator(); it6.hasNext(); it6 = it6) {
                        StringBuilder sb = new StringBuilder();
                        sb.append("id=");
                        sb.append(com.original.tase.utils.Utils.a((String) it6.next(), new boolean[0]));
                        g.addAll(Jsoup.b(HttpHelper.e().a(sezonlukdizi.c + str10, sb.toString(), (Map<String, String>[]) new Map[]{a3})).g(str12));
                    }
                }
            }
            str = str9;
        } else {
            str = str9;
            while (it5.hasNext()) {
                String b3 = ((Element) it5.next()).b(str11);
                g.addAll(Jsoup.b(HttpHelper.e().a(sezonlukdizi.c + str10, "id=" + com.original.tase.utils.Utils.a(b3, new boolean[0]), (Map<String, String>[]) new Map[]{a3})).g(str12));
                str11 = str11;
                str10 = str10;
            }
        }
        Iterator it7 = g.iterator();
        while (it7.hasNext()) {
            elements.addAll(((Element) it7.next()).g(str12));
        }
        ArrayList arrayList3 = new ArrayList();
        Iterator it8 = elements.iterator();
        while (it8.hasNext()) {
            Element element = (Element) it8.next();
            if (!observableEmitter.isDisposed()) {
                try {
                    String b4 = element.b("src");
                    if (!arrayList3.contains(b4)) {
                        arrayList3.add(b4);
                        String str13 = "http:";
                        if (b4.startsWith("//")) {
                            try {
                                b4 = "https:" + b4;
                            } catch (Exception e) {
                                e = e;
                                arrayList = arrayList3;
                                it2 = it8;
                                str3 = str12;
                                str2 = str;
                                i = 0;
                                obj = obj2;
                                Logger.a((Throwable) e, new boolean[i]);
                                sezonlukdizi = this;
                                obj2 = obj;
                                it8 = it2;
                                arrayList3 = arrayList;
                                str12 = str3;
                                str = str2;
                            }
                        } else if (b4.startsWith("/")) {
                            b4 = sezonlukdizi.c + b4;
                        } else if (!b4.startsWith(str13)) {
                            if (!b4.startsWith("https:")) {
                                b4 = "https:" + b4;
                            }
                        }
                        String str14 = b4;
                        if (GoogleVideoHelper.j(str14)) {
                            HashMap<String, String> g2 = GoogleVideoHelper.g(str14);
                            if (g2 != null && !g2.isEmpty()) {
                                Iterator<Map.Entry<String, String>> it9 = g2.entrySet().iterator();
                                while (it9.hasNext()) {
                                    Map.Entry next = it9.next();
                                    String str15 = (String) next.getKey();
                                    Iterator<Map.Entry<String, String>> it10 = it9;
                                    arrayList = arrayList3;
                                    try {
                                        MediaSource mediaSource = new MediaSource(a(), "GoogleVideo", false);
                                        mediaSource.setOriginalLink(str14);
                                        mediaSource.setStreamLink(str15);
                                        if (((String) next.getValue()).isEmpty()) {
                                            str8 = "HD";
                                        } else {
                                            str8 = (String) next.getValue();
                                        }
                                        mediaSource.setQuality(str8);
                                        HashMap hashMap2 = new HashMap();
                                        hashMap2.put("User-Agent", Constants.f5838a);
                                        hashMap2.put("Cookie", GoogleVideoHelper.c(str14, (String) next.getKey()));
                                        mediaSource.setPlayHeader(hashMap2);
                                        observableEmitter2.onNext(mediaSource);
                                        it9 = it10;
                                        arrayList3 = arrayList;
                                    } catch (Exception e2) {
                                        e = e2;
                                        it2 = it8;
                                        str3 = str12;
                                        str2 = str;
                                        i = 0;
                                        obj = obj2;
                                        Logger.a((Throwable) e, new boolean[i]);
                                        sezonlukdizi = this;
                                        obj2 = obj;
                                        it8 = it2;
                                        arrayList3 = arrayList;
                                        str12 = str3;
                                        str = str2;
                                    }
                                }
                            }
                            arrayList = arrayList3;
                        } else {
                            arrayList = arrayList3;
                            try {
                                if (!str14.contains(".asp")) {
                                    sezonlukdizi.a(observableEmitter2, str14, "HD", false);
                                    arrayList3 = arrayList;
                                }
                            } catch (Exception e3) {
                                e = e3;
                                it2 = it8;
                                str3 = str12;
                                str2 = str;
                                obj = obj2;
                                i = 0;
                                Logger.a((Throwable) e, new boolean[i]);
                                sezonlukdizi = this;
                                obj2 = obj;
                                it8 = it2;
                                arrayList3 = arrayList;
                                str12 = str3;
                                str = str2;
                            }
                        }
                        String str16 = str;
                        try {
                            String a4 = HttpHelper.e().a(str14, Constants.f5838a, str16, (Map<String, String>[]) new Map[0]);
                            Iterator it11 = Jsoup.b(a4).g(str12).iterator();
                            while (it11.hasNext()) {
                                String b5 = ((Element) it11.next()).b("src");
                                if (!GoogleVideoHelper.b(b5) || !GoogleVideoHelper.j(b5)) {
                                    it3 = it11;
                                    it4 = it8;
                                    str3 = str12;
                                    str2 = str16;
                                    sezonlukdizi.a(observableEmitter2, b5, "HD", false);
                                } else {
                                    HashMap<String, String> g3 = GoogleVideoHelper.g(b5);
                                    if (g3 != null && !g3.isEmpty()) {
                                        for (Map.Entry next2 : g3.entrySet()) {
                                            Iterator it12 = it11;
                                            String str17 = (String) next2.getKey();
                                            it2 = it8;
                                            try {
                                                str3 = str12;
                                                try {
                                                    str2 = str16;
                                                    try {
                                                        MediaSource mediaSource2 = new MediaSource(a(), "GoogleVideo", false);
                                                        mediaSource2.setStreamLink(str17);
                                                        if (((String) next2.getValue()).isEmpty()) {
                                                            str7 = "HD";
                                                        } else {
                                                            str7 = (String) next2.getValue();
                                                        }
                                                        mediaSource2.setQuality(str7);
                                                        HashMap hashMap3 = new HashMap();
                                                        hashMap3.put("User-Agent", Constants.f5838a);
                                                        hashMap3.put("Cookie", GoogleVideoHelper.c(b5, (String) next2.getKey()));
                                                        mediaSource2.setPlayHeader(hashMap3);
                                                        observableEmitter2.onNext(mediaSource2);
                                                        it8 = it2;
                                                        it11 = it12;
                                                        str12 = str3;
                                                        str16 = str2;
                                                    } catch (Exception e4) {
                                                        e = e4;
                                                        obj = obj2;
                                                        i = 0;
                                                        Logger.a((Throwable) e, new boolean[i]);
                                                        sezonlukdizi = this;
                                                        obj2 = obj;
                                                        it8 = it2;
                                                        arrayList3 = arrayList;
                                                        str12 = str3;
                                                        str = str2;
                                                    }
                                                } catch (Exception e5) {
                                                    e = e5;
                                                    str2 = str16;
                                                    obj = obj2;
                                                    i = 0;
                                                    Logger.a((Throwable) e, new boolean[i]);
                                                    sezonlukdizi = this;
                                                    obj2 = obj;
                                                    it8 = it2;
                                                    arrayList3 = arrayList;
                                                    str12 = str3;
                                                    str = str2;
                                                }
                                            } catch (Exception e6) {
                                                e = e6;
                                                str3 = str12;
                                                str2 = str16;
                                                obj = obj2;
                                                i = 0;
                                                Logger.a((Throwable) e, new boolean[i]);
                                                sezonlukdizi = this;
                                                obj2 = obj;
                                                it8 = it2;
                                                arrayList3 = arrayList;
                                                str12 = str3;
                                                str = str2;
                                            }
                                        }
                                    }
                                    it3 = it11;
                                    it4 = it8;
                                    str3 = str12;
                                    str2 = str16;
                                }
                                it8 = it4;
                                it11 = it3;
                                str12 = str3;
                                str16 = str2;
                            }
                            it2 = it8;
                            str3 = str12;
                            str2 = str16;
                            if (!Regex.a(a4, "['\"]?kind['\"]?\\s*:\\s*['\"]?(captions|subtitles)['\"]?", 1, true).isEmpty()) {
                                ArrayList arrayList4 = Regex.b(a4, "['\"]?\\s*file\\s*['\"]?\\s*[:=,]?\\s*['\"]([^'\"]+)", 1).get(0);
                                HashMap hashMap4 = new HashMap();
                                hashMap4.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36");
                                obj = obj2;
                                try {
                                    hashMap4.put(obj, str14);
                                    int i2 = 0;
                                    while (i2 < arrayList4.size()) {
                                        try {
                                            String trim = ((String) arrayList4.get(i2)).replace("\\/", "/").trim();
                                            if (!trim.isEmpty() && !trim.toLowerCase().endsWith(".vtt") && !trim.toLowerCase().endsWith(".srt") && !trim.toLowerCase().endsWith(".png")) {
                                                if (trim.startsWith("//")) {
                                                    trim = str13 + trim;
                                                } else if (trim.startsWith("/")) {
                                                    trim = "https://sezonlukdizi.org" + trim;
                                                } else if (!trim.startsWith(str13) && !trim.startsWith("https:")) {
                                                    trim = str13 + trim;
                                                }
                                                if (trim.length() >= 10) {
                                                    try {
                                                        String a5 = HttpHelper.e().a(trim, false, (Map<String, String>[]) new Map[]{hashMap4});
                                                        boolean k = GoogleVideoHelper.k(a5);
                                                        int i3 = 1;
                                                        while (i3 <= 2) {
                                                            arrayList2 = arrayList4;
                                                            try {
                                                                a2 = a();
                                                                if (k) {
                                                                    hashMap = hashMap4;
                                                                    str4 = str13;
                                                                    str5 = "GoogleVideo";
                                                                } else {
                                                                    hashMap = hashMap4;
                                                                    str5 = "CDN";
                                                                    str4 = str13;
                                                                }
                                                            } catch (Exception e7) {
                                                                e = e7;
                                                                hashMap = hashMap4;
                                                                str4 = str13;
                                                                i = 0;
                                                                try {
                                                                    try {
                                                                        Logger.a((Throwable) e, new boolean[0]);
                                                                    } catch (Exception e8) {
                                                                        e = e8;
                                                                    }
                                                                } catch (Exception e9) {
                                                                    e = e9;
                                                                    try {
                                                                        Logger.a((Throwable) e, new boolean[i]);
                                                                        i2++;
                                                                        arrayList4 = arrayList2;
                                                                        str13 = str4;
                                                                        hashMap4 = hashMap;
                                                                    } catch (Exception e10) {
                                                                        e = e10;
                                                                        Logger.a((Throwable) e, new boolean[i]);
                                                                        sezonlukdizi = this;
                                                                        obj2 = obj;
                                                                        it8 = it2;
                                                                        arrayList3 = arrayList;
                                                                        str12 = str3;
                                                                        str = str2;
                                                                    }
                                                                }
                                                                i2++;
                                                                arrayList4 = arrayList2;
                                                                str13 = str4;
                                                                hashMap4 = hashMap;
                                                            }
                                                            try {
                                                                MediaSource mediaSource3 = new MediaSource(a2, str5, false);
                                                                if (k) {
                                                                    str6 = GoogleVideoHelper.h(a5);
                                                                    z = k;
                                                                } else {
                                                                    str6 = Regex.a(a5, "(?:\\.|-)(\\d{3,4})p?\\.", 1);
                                                                    if (str6.isEmpty()) {
                                                                        str6 = "HD";
                                                                    }
                                                                    HashMap hashMap5 = new HashMap();
                                                                    hashMap5.put("User-Agent", Constants.f5838a);
                                                                    if (i3 % 2 == 0) {
                                                                        hashMap5.put(obj, str14);
                                                                        z = k;
                                                                        hashMap5.put("Accept-Encoding", "identity;q=1, *;q=0");
                                                                    } else {
                                                                        z = k;
                                                                    }
                                                                    mediaSource3.setPlayHeader(hashMap5);
                                                                }
                                                                mediaSource3.setStreamLink(a5);
                                                                mediaSource3.setQuality(str6);
                                                                observableEmitter2.onNext(mediaSource3);
                                                                i3++;
                                                                arrayList4 = arrayList2;
                                                                str13 = str4;
                                                                hashMap4 = hashMap;
                                                                k = z;
                                                            } catch (Exception e11) {
                                                                e = e11;
                                                                i = 0;
                                                                Logger.a((Throwable) e, new boolean[0]);
                                                                i2++;
                                                                arrayList4 = arrayList2;
                                                                str13 = str4;
                                                                hashMap4 = hashMap;
                                                            }
                                                        }
                                                    } catch (Exception e12) {
                                                        e = e12;
                                                        arrayList2 = arrayList4;
                                                        hashMap = hashMap4;
                                                        str4 = str13;
                                                        i = 0;
                                                        Logger.a((Throwable) e, new boolean[0]);
                                                        i2++;
                                                        arrayList4 = arrayList2;
                                                        str13 = str4;
                                                        hashMap4 = hashMap;
                                                    }
                                                }
                                            }
                                            arrayList2 = arrayList4;
                                            hashMap = hashMap4;
                                            str4 = str13;
                                        } catch (Exception e13) {
                                            e = e13;
                                            arrayList2 = arrayList4;
                                            hashMap = hashMap4;
                                            str4 = str13;
                                            i = 0;
                                            Logger.a((Throwable) e, new boolean[i]);
                                            i2++;
                                            arrayList4 = arrayList2;
                                            str13 = str4;
                                            hashMap4 = hashMap;
                                        }
                                        i2++;
                                        arrayList4 = arrayList2;
                                        str13 = str4;
                                        hashMap4 = hashMap;
                                    }
                                } catch (Exception e14) {
                                    e = e14;
                                }
                                sezonlukdizi = this;
                                obj2 = obj;
                                it8 = it2;
                                arrayList3 = arrayList;
                                str12 = str3;
                                str = str2;
                            }
                        } catch (Exception e15) {
                            e = e15;
                            it2 = it8;
                            str3 = str12;
                            str2 = str16;
                            obj = obj2;
                            i = 0;
                            Logger.a((Throwable) e, new boolean[i]);
                            sezonlukdizi = this;
                            obj2 = obj;
                            it8 = it2;
                            arrayList3 = arrayList;
                            str12 = str3;
                            str = str2;
                        }
                    } else {
                        arrayList = arrayList3;
                        it2 = it8;
                        str3 = str12;
                        str2 = str;
                    }
                    obj = obj2;
                } catch (Exception e16) {
                    e = e16;
                    arrayList = arrayList3;
                    it2 = it8;
                    str3 = str12;
                    str2 = str;
                    obj = obj2;
                    i = 0;
                    Logger.a((Throwable) e, new boolean[i]);
                    sezonlukdizi = this;
                    obj2 = obj;
                    it8 = it2;
                    arrayList3 = arrayList;
                    str12 = str3;
                    str = str2;
                }
                sezonlukdizi = this;
                obj2 = obj;
                it8 = it2;
                arrayList3 = arrayList;
                str12 = str3;
                str = str2;
            } else {
                return;
            }
        }
    }
}
