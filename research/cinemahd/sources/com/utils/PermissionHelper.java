package com.utils;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionHelper {
    public static boolean a(Activity activity, int i) {
        if (ContextCompat.a((Context) activity, "android.permission.READ_EXTERNAL_STORAGE") == 0) {
            return true;
        }
        ActivityCompat.a(activity, new String[]{"android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE"}, i);
        return false;
    }

    public static boolean b(Activity activity, int i) {
        if (Build.VERSION.SDK_INT < 16 || a(activity, i)) {
            return c(activity, i);
        }
        return false;
    }

    public static boolean c(Activity activity, int i) {
        if (ContextCompat.a((Context) activity, "android.permission.WRITE_EXTERNAL_STORAGE") == 0) {
            return true;
        }
        ActivityCompat.a(activity, new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, i);
        return false;
    }
}
