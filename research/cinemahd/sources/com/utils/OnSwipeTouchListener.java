package com.utils;

import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public abstract class OnSwipeTouchListener implements View.OnTouchListener {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f6525a;
    private final Handler b;
    private Runnable c = new Runnable() {
        public void run() {
            OnSwipeTouchListener.this.b();
        }
    };
    private int d;
    protected float e;
    protected float f;
    private float g;
    private float h;
    private long i = 0;

    protected enum Direction {
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    public OnSwipeTouchListener(boolean z) {
        this.f6525a = z;
        this.b = new Handler();
    }

    public static String a(long j, boolean z) {
        long hours = TimeUnit.MILLISECONDS.toHours(j);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(j);
        long seconds = TimeUnit.MILLISECONDS.toSeconds(j);
        String str = "-";
        if (hours > 0) {
            Locale locale = Locale.getDefault();
            Object[] objArr = new Object[4];
            if (!z) {
                str = "";
            }
            objArr[0] = str;
            objArr[1] = Long.valueOf(hours);
            objArr[2] = Long.valueOf(minutes - TimeUnit.HOURS.toMinutes(hours));
            objArr[3] = Long.valueOf(seconds - TimeUnit.MINUTES.toSeconds(minutes));
            return String.format(locale, "%s%02d:%02d:%02d", objArr);
        }
        Locale locale2 = Locale.getDefault();
        Object[] objArr2 = new Object[3];
        if (!z) {
            str = "";
        }
        objArr2[0] = str;
        objArr2[1] = Long.valueOf(minutes);
        objArr2[2] = Long.valueOf(seconds - TimeUnit.MINUTES.toSeconds(minutes));
        return String.format(locale2, "%s%02d:%02d", objArr2);
    }

    public abstract void a();

    public abstract void a(MotionEvent motionEvent);

    public abstract void a(Direction direction);

    public abstract void a(Direction direction, float f2);

    public abstract void b();

    public boolean onTouch(View view, MotionEvent motionEvent) {
        float f2;
        float f3;
        float f4;
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 0) {
            this.e = motionEvent.getX();
            this.f = motionEvent.getY();
            this.d = 0;
        } else if (actionMasked != 1) {
            if (actionMasked == 2) {
                if (this.d == 0) {
                    f2 = motionEvent.getX() - this.e;
                    f4 = motionEvent.getY();
                    f3 = this.f;
                } else {
                    f2 = motionEvent.getX() - this.g;
                    f4 = motionEvent.getY();
                    f3 = this.h;
                }
                float f5 = f4 - f3;
                if (this.d == 0 && Math.abs(f2) > 100.0f) {
                    this.d = 1;
                    this.g = motionEvent.getX();
                    this.h = motionEvent.getY();
                    if (f2 > 0.0f) {
                        a(Direction.RIGHT);
                    } else {
                        a(Direction.LEFT);
                    }
                } else if (this.d == 0 && Math.abs(f5) > 100.0f) {
                    this.d = 2;
                    this.g = motionEvent.getX();
                    this.h = motionEvent.getY();
                    if (f5 > 0.0f) {
                        a(Direction.DOWN);
                    } else {
                        a(Direction.UP);
                    }
                }
                int i2 = this.d;
                if (i2 == 1) {
                    if (f2 > 0.0f) {
                        a(Direction.RIGHT, f2);
                    } else {
                        a(Direction.LEFT, -f2);
                    }
                } else if (i2 == 2) {
                    if (f5 > 0.0f) {
                        a(Direction.DOWN, f5);
                    } else {
                        a(Direction.UP, -f5);
                    }
                }
            }
        } else if (this.d == 0) {
            if (this.f6525a) {
                long currentTimeMillis = System.currentTimeMillis();
                long j = this.i;
                if (currentTimeMillis - j <= 150 && j != 0) {
                    this.b.removeCallbacks(this.c);
                    a(motionEvent);
                    return true;
                }
            }
            this.i = System.currentTimeMillis();
            if (this.f6525a) {
                this.b.postDelayed(this.c, 150);
            } else {
                this.b.post(this.c);
            }
            return true;
        } else {
            a();
            this.d = 0;
            return true;
        }
        return true;
    }
}
