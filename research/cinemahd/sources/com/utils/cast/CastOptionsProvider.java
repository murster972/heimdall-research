package com.utils.cast;

import android.content.Context;
import android.content.res.Resources;
import com.google.android.gms.cast.LaunchOptions;
import com.google.android.gms.cast.framework.CastOptions;
import com.google.android.gms.cast.framework.OptionsProvider;
import com.google.android.gms.cast.framework.SessionProvider;
import com.google.android.gms.cast.framework.media.CastMediaOptions;
import com.google.android.gms.cast.framework.media.NotificationOptions;
import com.movie.FreeMoviesApp;
import com.utils.ExpandedControlsActivity;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.LocaleUtils;

public class CastOptionsProvider implements OptionsProvider {
    public List<SessionProvider> getAdditionalSessionProviders(Context context) {
        return null;
    }

    public CastOptions getCastOptions(Context context) {
        Locale locale;
        ArrayList arrayList = new ArrayList();
        arrayList.add("com.google.android.gms.cast.framework.action.REWIND");
        arrayList.add("com.google.android.gms.cast.framework.action.TOGGLE_PLAYBACK");
        arrayList.add("com.google.android.gms.cast.framework.action.FORWARD");
        arrayList.add("com.google.android.gms.cast.framework.action.STOP_CASTING");
        String trim = FreeMoviesApp.l().getString("pref_app_lang", "").trim();
        if (trim.isEmpty()) {
            locale = Resources.getSystem().getConfiguration().locale;
        } else {
            locale = LocaleUtils.e(trim);
        }
        return new CastOptions.Builder().a(context.getString(R.string.google_cast_app_id)).a(new CastMediaOptions.Builder().a(new NotificationOptions.Builder().a(ExpandedControlsActivity.class.getName()).a(arrayList, new int[]{1, 3}).a()).a(ExpandedControlsActivity.class.getName()).a()).a(new LaunchOptions.Builder().a(locale).a()).a(true).b(true).a(0.1d).a();
    }
}
