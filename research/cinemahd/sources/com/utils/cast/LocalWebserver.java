package com.utils.cast;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import com.original.tase.Logger;

public class LocalWebserver extends Service {
    private void a() {
        WebServerManager.d().a();
    }

    private boolean b() {
        boolean c = WebServerManager.d().c();
        if (!c) {
            Logger.a((Throwable) new RuntimeException("Failed to start subtitles server..."), true);
        }
        return c;
    }

    public IBinder onBind(Intent intent) {
        return null;
    }

    public void onCreate() {
        super.onCreate();
    }

    public void onDestroy() {
        super.onDestroy();
        a();
    }

    public int onStartCommand(Intent intent, int i, int i2) {
        if (!b() || intent == null) {
            return 2;
        }
        try {
            if (intent.getExtras() == null) {
                return 2;
            }
            Bundle extras = intent.getExtras();
            if (extras.isEmpty() || !extras.containsKey("isNeededToRefreshTracks") || !extras.getBoolean("isNeededToRefreshTracks", false) || !extras.containsKey("videoAndSubTrackIdArray") || !extras.containsKey("videoOnlyTrackIdArray")) {
                return 2;
            }
            CastHelper.a((Context) this, extras.getLongArray("videoAndSubTrackIdArray"), extras.getLongArray("videoOnlyTrackIdArray"));
            return 2;
        } catch (Exception e) {
            Logger.a((Throwable) e, true);
            return 2;
        }
    }
}
