package com.utils.cast;

import com.original.tase.Logger;
import com.uwetrottmann.trakt5.TraktV2;
import fi.iki.elonen.NanoHTTPD;
import java.util.Map;

public class CastSubtitlesWebServer extends NanoHTTPD {
    private Map<String, String> n;

    public CastSubtitlesWebServer(int i) {
        super(i);
    }

    public void a(Map<String, String> map) {
        this.n = map;
    }

    public NanoHTTPD.Response a(NanoHTTPD.IHTTPSession iHTTPSession) {
        Map<String, String> map = this.n;
        if (map == null || map.isEmpty()) {
            Logger.a((Throwable) new RuntimeException("mSubsMap is null"), true);
            return NanoHTTPD.a(NanoHTTPD.Response.Status.NOT_FOUND, "text/plain", "");
        }
        String uri = iHTTPSession.getUri();
        if (uri.startsWith("/")) {
            uri = uri.substring(1, uri.length());
        }
        if (!uri.endsWith(".ttml") && uri.contains(".ttml")) {
            uri = uri.substring(0, uri.lastIndexOf(".ttml"));
        }
        String str = null;
        if (this.n.containsKey(uri)) {
            str = this.n.get(uri);
        }
        if (str == null || str.isEmpty()) {
            Logger.a((Throwable) new RuntimeException("mSubsMap doesn't contain the corresponding subtitles key"), true);
            return NanoHTTPD.a(NanoHTTPD.Response.Status.NOT_FOUND, "text/plain", "");
        }
        NanoHTTPD.Response a2 = NanoHTTPD.a(NanoHTTPD.Response.Status.OK, "application/ttml+xml", str);
        a2.a("Access-Control-Allow-Origin", "*");
        a2.a(TraktV2.HEADER_CONTENT_TYPE, "application/ttml+xml; charset=utf-8");
        return a2;
    }
}
