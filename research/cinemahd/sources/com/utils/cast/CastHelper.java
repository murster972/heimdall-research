package com.utils.cast;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import com.database.entitys.MovieEntity;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.MediaTrack;
import com.google.android.gms.cast.TextTrackStyle;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.SessionManager;
import com.google.android.gms.common.images.WebImage;
import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.utils.ImageUtils;
import com.utils.Utils;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTimeConstants;

public class CastHelper {
    public static MediaMetadata a(MovieEntity movieEntity, MovieInfo movieInfo, MediaSource mediaSource) {
        MediaMetadata mediaMetadata = new MediaMetadata(movieEntity.getTV().booleanValue() ? 1 : 2);
        mediaMetadata.a("com.google.android.gms.cast.metadata.TITLE", movieInfo.getNameAndYear());
        mediaMetadata.a(new WebImage(Uri.parse(ImageUtils.a(movieEntity.getPoster_path(), 500))));
        mediaMetadata.a(new WebImage(Uri.parse(ImageUtils.a(movieEntity.getBackdrop_path(), 500))));
        String lowerCase = mediaSource.getQuality().toLowerCase();
        int i = 1920;
        int i2 = 1080;
        if (lowerCase.contains("hd")) {
            i2 = DateTimeConstants.MINUTES_PER_DAY;
            i = 2560;
        } else if (!lowerCase.contains("1080") && !lowerCase.contains("720")) {
            if (lowerCase.contains("2K")) {
                i = -1;
                i2 = -1;
            } else {
                i2 = 2160;
                i = 3840;
            }
        }
        if (i2 > -1) {
            mediaMetadata.a("com.google.android.gms.cast.metadata.HEIGHT", i2);
        }
        if (i > -1) {
            mediaMetadata.a("com.google.android.gms.cast.metadata.WIDTH", i);
        }
        if (movieEntity.getTV().booleanValue()) {
            mediaMetadata.a("com.google.android.gms.cast.metadata.SERIES_TITLE", movieInfo.getNameAndYear());
            mediaMetadata.a("com.google.android.gms.cast.metadata.SEASON_NUMBER", movieInfo.getSession().intValue());
            mediaMetadata.a("com.google.android.gms.cast.metadata.EPISODE_NUMBER", movieInfo.getEps().intValue());
        }
        return mediaMetadata;
    }

    private static MediaInfo.Builder b(MediaMetadata mediaMetadata, MediaSource mediaSource) {
        int i;
        int i2;
        float floatValue = Float.valueOf(FreeMoviesApp.l().getString("pref_cc_subs_font_scale", "1.00")).floatValue();
        try {
            i = Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        } catch (Exception e) {
            Logger.a((Throwable) e, new boolean[0]);
            i = Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
        }
        try {
            i2 = Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        } catch (Exception e2) {
            Logger.a((Throwable) e2, new boolean[0]);
            i2 = Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_bg_color", "#00FFFFFF"));
        }
        TextTrackStyle textTrackStyle = new TextTrackStyle();
        textTrackStyle.b(i2);
        textTrackStyle.f(i);
        textTrackStyle.a(floatValue);
        textTrackStyle.g(Color.parseColor("#00AA00FF"));
        textTrackStyle.e("Droid Sans");
        textTrackStyle.d(1);
        textTrackStyle.e(0);
        textTrackStyle.h(10);
        textTrackStyle.i(0);
        textTrackStyle.c(Color.parseColor("#FF000000"));
        MediaInfo.Builder builder = new MediaInfo.Builder(mediaSource.getStreamLink());
        builder.a(1);
        builder.a(mediaSource.isHLS() ? "application/x-mpegURL" : "video/mp4");
        builder.a(mediaMetadata);
        builder.a(textTrackStyle);
        return builder;
    }

    public static MediaInfo a(MediaMetadata mediaMetadata, MediaSource mediaSource, List<String> list, List<String> list2) {
        String o = Utils.o();
        if (o.isEmpty()) {
            o = "127.0.0.1";
        }
        ArrayList arrayList = new ArrayList();
        int i = 1;
        for (int i2 = 0; i2 < list.size(); i2++) {
            try {
                MediaTrack.Builder builder = new MediaTrack.Builder((long) i, 1);
                builder.c("en-US");
                builder.a("http://" + o + ":" + 34507 + "/" + list2.get(i2));
                builder.a(1);
                builder.d(new File(list.get(i2)).getName());
                builder.b("application/ttml+xml");
                arrayList.add(builder.a());
                i++;
            } catch (Exception e) {
                Logger.a((Throwable) e, true);
            }
        }
        MediaInfo.Builder b = b(mediaMetadata, mediaSource);
        b.a((List<MediaTrack>) arrayList);
        return b.a();
    }

    public static MediaInfo a(MediaMetadata mediaMetadata, MediaSource mediaSource) {
        return b(mediaMetadata, mediaSource).a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001a, code lost:
        r1 = r1.a().h();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(android.content.Context r1, final long[] r2, long[] r3) {
        /*
            if (r2 == 0) goto L_0x0038
            if (r3 == 0) goto L_0x0038
            int r0 = r2.length
            if (r0 <= 0) goto L_0x0038
            int r0 = r3.length
            if (r0 <= 0) goto L_0x0038
            com.google.android.gms.cast.framework.CastContext r1 = com.google.android.gms.cast.framework.CastContext.a((android.content.Context) r1)     // Catch:{ Exception -> 0x0031 }
            com.google.android.gms.cast.framework.SessionManager r1 = r1.c()     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x0038
            com.google.android.gms.cast.framework.CastSession r0 = r1.a()     // Catch:{ Exception -> 0x0031 }
            if (r0 == 0) goto L_0x0038
            com.google.android.gms.cast.framework.CastSession r1 = r1.a()     // Catch:{ Exception -> 0x0031 }
            com.google.android.gms.cast.framework.media.RemoteMediaClient r1 = r1.h()     // Catch:{ Exception -> 0x0031 }
            if (r1 == 0) goto L_0x0038
            com.google.android.gms.common.api.PendingResult r3 = r1.a((long[]) r3)     // Catch:{ Exception -> 0x0031 }
            com.utils.cast.CastHelper$1 r0 = new com.utils.cast.CastHelper$1     // Catch:{ Exception -> 0x0031 }
            r0.<init>(r1, r2)     // Catch:{ Exception -> 0x0031 }
            r3.setResultCallback(r0)     // Catch:{ Exception -> 0x0031 }
            goto L_0x0038
        L_0x0031:
            r1 = move-exception
            r2 = 0
            boolean[] r2 = new boolean[r2]
            com.original.tase.Logger.a((java.lang.Throwable) r1, (boolean[]) r2)
        L_0x0038:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.utils.cast.CastHelper.a(android.content.Context, long[], long[]):void");
    }

    public static boolean a(Context context) {
        try {
            SessionManager c = CastContext.a(context).c();
            if (c == null || c.a() == null) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }
}
