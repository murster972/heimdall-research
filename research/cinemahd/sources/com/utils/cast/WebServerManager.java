package com.utils.cast;

import com.original.tase.Logger;
import fi.iki.elonen.NanoHTTPD;
import java.util.Map;

public class WebServerManager {
    private static volatile WebServerManager b;

    /* renamed from: a  reason: collision with root package name */
    private NanoHTTPD f6556a;

    public static WebServerManager d() {
        WebServerManager webServerManager = b;
        if (webServerManager == null) {
            synchronized (WebServerManager.class) {
                webServerManager = b;
                if (webServerManager == null) {
                    webServerManager = new WebServerManager();
                    b = webServerManager;
                }
            }
        }
        return webServerManager;
    }

    public void a(NanoHTTPD nanoHTTPD) {
        NanoHTTPD nanoHTTPD2 = this.f6556a;
        if (nanoHTTPD2 != null) {
            nanoHTTPD2.a();
        }
        this.f6556a = nanoHTTPD;
    }

    public NanoHTTPD b() {
        return this.f6556a;
    }

    public boolean c() {
        NanoHTTPD nanoHTTPD = this.f6556a;
        if (nanoHTTPD != null) {
            try {
                if (nanoHTTPD.c()) {
                    return true;
                }
                this.f6556a.a(45000, true);
                return true;
            } catch (Exception e) {
                Logger.a((Throwable) e, true);
            }
        }
        return false;
    }

    public void a() {
        NanoHTTPD nanoHTTPD = this.f6556a;
        if (nanoHTTPD != null && nanoHTTPD.c()) {
            this.f6556a.d();
        }
    }

    public void a(Map<String, String> map) {
        NanoHTTPD nanoHTTPD = this.f6556a;
        if (nanoHTTPD != null && (nanoHTTPD instanceof CastSubtitlesWebServer)) {
            ((CastSubtitlesWebServer) nanoHTTPD).a(map);
        }
    }
}
