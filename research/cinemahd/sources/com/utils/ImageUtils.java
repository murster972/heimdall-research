package com.utils;

public final class ImageUtils {
    private ImageUtils() {
        throw new AssertionError("No instances.");
    }

    public static String a(String str, int i) {
        String str2 = i <= 92 ? "/w92" : i <= 154 ? "/w154" : i <= 185 ? "/w185" : i <= 342 ? "/w342" : i <= 500 ? "/w500" : i <= 780 ? "/w780" : i <= 1920 ? "/w1280" : "/original";
        if (str != null && (str.contains("http://") || str.contains("https://"))) {
            return str;
        }
        return "http://image.tmdb.org/t/p" + str2 + str;
    }
}
