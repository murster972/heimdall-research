package com.jaunt;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

final class e implements h {
    private static final HashMap<String, i> c = new HashMap<>();
    private static final HashSet<String> d = new HashSet<>(80);
    private static final HashSet<String> e = new HashSet<>(60);
    private static final HashSet<String> f = new HashSet<>(140);
    private static final Map<String, String[]> g;
    private static final Map<String, String[]> h;
    private static final Map<String, String> i;

    /* renamed from: a  reason: collision with root package name */
    private short f4936a = 1;
    private short b = 2;

    static {
        d.add("TT");
        d.add("I");
        d.add("B");
        d.add("U");
        d.add("S");
        d.add("STRIKE");
        d.add("BIG");
        d.add("SMALL");
        d.add("EM");
        d.add("STRONG");
        d.add("DFN");
        d.add("CODE");
        d.add("SAMP");
        d.add("KBD");
        d.add("VAR");
        d.add("CITE");
        d.add("ABBR");
        d.add("ACRONYM");
        d.add("A");
        d.add("IMG");
        d.add("APPLET");
        d.add("OBJECT");
        d.add("FONT");
        d.add("BASEFONT");
        d.add("BR");
        d.add("SCRIPT");
        d.add("MAP");
        d.add("Q");
        d.add("SUB");
        d.add("SUP");
        d.add("SPAN");
        d.add("BDO");
        d.add("IFRAME");
        d.add("INPUT");
        d.add("SELECT");
        d.add("TEXTAREA");
        d.add("LABEL");
        d.add("BUTTON");
        d.add("BDI");
        d.add("MARK");
        d.add("METER");
        d.add("PROGRESS");
        d.add("RP");
        d.add("RT");
        d.add("TIME");
        d.add("WBR");
        e.add("P");
        e.add("LI");
        e.add("H1");
        e.add("H2");
        e.add("H3");
        e.add("H4");
        e.add("H5");
        e.add("H6");
        e.add("UL");
        e.add("OL");
        e.add("DIR");
        e.add("MENU");
        e.add("PRE");
        e.add("DL");
        e.add("DIV");
        e.add("CENTER");
        e.add("NOSCRIPT");
        e.add("NOFRAMES");
        e.add("BLOCKQUOTE");
        e.add("FORM");
        e.add("ISINDEX");
        e.add("HR");
        e.add("TABLE");
        e.add("FIELDSET");
        e.add("ADDRESS");
        e.add("ARTICLE");
        e.add("ASIDE");
        e.add("DETAILS");
        e.add("DIALOG");
        e.add("FIGCAPTION");
        e.add("FIGURE");
        e.add("FOOTER");
        e.add("HEADER");
        e.add("MAIN");
        e.add("MENU");
        e.add("MENUITEM");
        e.add("NAV");
        e.add("RUBY");
        e.add("SECTION");
        e.add("SUMMARY");
        e.add("NOBR");
        e.add("SPACER");
        e.add("STYLE");
        e.add("SCRIPT");
        f.addAll(e);
        f.addAll(d);
        HashMap hashMap = new HashMap(14);
        g = hashMap;
        hashMap.put("area", new String[]{"href"});
        g.put("link", new String[]{"Href"});
        g.put("img", new String[]{"Src", "Usemap"});
        g.put("meta", new String[]{"content"});
        g.put("frame", new String[]{"SRC"});
        g.put("a", new String[]{"href"});
        g.put("form", new String[]{"action"});
        g.put("iframe", new String[]{"SRC", "Longdesc"});
        g.put("script", new String[]{"Src"});
        g.put("body", new String[]{"Background"});
        g.put("table", new String[]{"Background"});
        g.put("td", new String[]{"Background"});
        g.put("th", new String[]{"Background"});
        g.put("menuitem", new String[]{"Icon"});
        HashMap hashMap2 = new HashMap(9);
        h = hashMap2;
        hashMap2.put("link", new String[]{"Href"});
        h.put("img", new String[]{"Src", "Usemap"});
        h.put("iframe", new String[]{"Longdesc"});
        h.put("script", new String[]{"Src"});
        h.put("body", new String[]{"Background"});
        h.put("table", new String[]{"Background"});
        h.put("td", new String[]{"Background"});
        h.put("th", new String[]{"Background"});
        h.put("menuitem", new String[]{"Icon"});
        HashMap hashMap3 = new HashMap(2);
        i = hashMap3;
        hashMap3.put("frame", "SRC");
        i.put("iframe", "SRC");
        c.put("BR", new i(i.c));
        c.put("INPUT", new i(i.c));
        c.put("AREA", new i(i.c));
        c.put("LINK", new i(i.c));
        c.put("IMG", new i(i.c));
        c.put("PARAM", new i(i.c));
        c.put("HR", new i(i.c));
        c.put("COL", new i(i.c));
        c.put("BASE", new i(i.c));
        c.put("META", new i(i.c));
        c.put("BASEFONT", new i(i.c));
        c.put("FRAME", new i(i.c));
        c.put("ISINDEX", new i(i.c));
        c.put("WBR", new i(i.c));
        c.put("SPACER", new i(i.c));
        c.put("P", new i(i.d, f, "P"));
        c.put("DT", new i(i.d, f, "DD"));
        c.put("DD", new i(i.d, (Set<String>) f));
        c.put("LI", new i(i.d, f, "LI"));
        c.put("THEAD", new i(i.d, new String[]{"TR", "FORM", "SCRIPT"}));
        c.put("TFOOT", new i(i.d, new String[]{"TR", "FORM", "SCRIPT"}));
        c.put("TR", new i(i.d, new String[]{"TD", "TH", "FORM", "SCRIPT"}));
        c.put("TD", new i(i.d, (Set<String>) f));
        c.put("TH", new i(i.d, (Set<String>) f));
        c.put("OPTION", new i(i.d, (Set<String>) f));
        c.put("COLGROUP", new i(i.d, "COL"));
    }

    public final short a(String str) {
        i iVar = c.get(str.toUpperCase());
        if (iVar == null) {
            return i.e;
        }
        return iVar.a();
    }

    public final String[] a(Element element, short s) {
        if (s == this.f4936a) {
            return g.get(element.f().toLowerCase());
        }
        if (s != this.b) {
            return null;
        }
        String f2 = element.f();
        if (!f2.equals("link")) {
            return h.get(element.f().toLowerCase());
        }
        if (element.d("rel").matches("(?i)(stylesheet|icon)")) {
            return h.get(f2);
        }
        return null;
    }

    public final boolean a(String str, String str2) {
        i iVar;
        if (str.equalsIgnoreCase("DIV") || (iVar = c.get(str.toUpperCase())) == null) {
            return true;
        }
        if (!str.equalsIgnoreCase("TD") && !str.equalsIgnoreCase("TH")) {
            return iVar.a(str2);
        }
        if (str2.equalsIgnoreCase("TR") || str2.equalsIgnoreCase("TD") || str2.equalsIgnoreCase("TH")) {
            return false;
        }
        return true;
    }
}
