package com.jaunt;

import com.jaunt.component.Meta;
import com.jaunt.util.MultiMap;
import com.original.tase.model.socket.UserResponces;
import java.util.ArrayList;
import java.util.List;

public class Document extends Element {
    private static String j = "#document";
    private UserAgent h;
    private Meta i;

    Document(UserAgent userAgent, String str, MultiMap<String, String> multiMap, Element element, List<Element> list, int i2, short s, boolean z) {
        super(element, 9, j, 1, true);
        this.h = userAgent;
        if (element != null) {
            new ArrayList(list.size());
            for (Element meta : list) {
                Meta meta2 = new Meta(meta);
                if (meta2.a()) {
                    this.i = meta2;
                }
            }
        }
    }

    static String e(String str) {
        String replaceAll = str.replaceAll("\\W+", "");
        int length = replaceAll.length();
        return length > 200 ? replaceAll.substring(length - UserResponces.USER_RESPONCE_SUCCSES) : replaceAll;
    }

    public boolean b(int i2) throws ResponseException {
        Meta meta = this.i;
        if (meta == null) {
            return false;
        }
        meta.a(this.h, i2);
        return true;
    }
}
