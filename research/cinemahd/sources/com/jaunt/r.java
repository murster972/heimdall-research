package com.jaunt;

import com.jaunt.JNode;
import java.util.regex.Pattern;

final class r {
    private static final Pattern i = Pattern.compile("^(?i)[-+]?[0-9]*(\\.[0-9]*)?([0-9]e[+-]?[1-9][0-9]*)?$");

    /* renamed from: a  reason: collision with root package name */
    private boolean f4942a = false;
    q b;
    private int c;
    private int d;
    private StringBuilder e = null;
    private StringBuilder f = null;
    private JNode g = null;
    private int h = -1;

    r() {
    }

    private String j() {
        StringBuilder sb = this.e;
        if (sb == null) {
            return null;
        }
        String sb2 = sb.toString();
        this.e = null;
        return sb2;
    }

    public final void a() {
        this.f4942a = false;
        this.b = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = -1;
    }

    public final void b() {
        if (!this.f4942a) {
            if (this.b == null) {
                this.b = new q((JNode) null, (String) null, -1);
                this.g = this.b;
            } else if (this.e != null) {
                String j = j();
                if (this.c == 0) {
                    j = j.trim();
                }
                q qVar = new q(this.g, j, this.c);
                this.g.a(qVar);
                this.g = qVar;
            } else {
                q qVar2 = new q(this.g, (String) null, -1);
                this.g.a(qVar2);
                this.g = qVar2;
            }
        }
    }

    public final void c() {
        JNode b2;
        while (!this.f4942a) {
            if (this.g.c() == JNode.Type.OBJECT) {
                JNode b3 = this.g.b();
                if (b3 != null) {
                    this.g = b3;
                    return;
                } else {
                    this.f4942a = true;
                    return;
                }
            } else if (this.g.c() == JNode.Type.ARRAY && (b2 = this.g.b()) != null) {
                this.g = b2;
            } else {
                return;
            }
        }
    }

    public final void d() {
        if (this.f4942a) {
        }
    }

    public final void e() {
        if (!this.f4942a) {
            if (this.e != null) {
                String j = j();
                if (this.c == 0) {
                    j = j.trim();
                }
                l lVar = new l(this.g, j, this.c);
                this.g.a(lVar);
                this.g = lVar;
                return;
            }
            l lVar2 = new l(this.g, (String) null, -1);
            this.g.a(lVar2);
            this.g = lVar2;
        }
    }

    public final void f() {
        while (!this.f4942a) {
            if (this.g.c() == JNode.Type.ARRAY) {
                this.g = this.g.b();
                return;
            } else if (this.g.c() == JNode.Type.OBJECT) {
                this.g = this.g.b();
            } else {
                return;
            }
        }
    }

    public final void g() {
        if (!this.f4942a) {
            String j = j();
            if (this.c == 0) {
                j = j.trim();
            }
            String sb = this.f.toString();
            int i2 = this.d;
            if (i2 == 0) {
                String trim = sb.trim();
                if (trim.equalsIgnoreCase("null")) {
                    JNode jNode = this.g;
                    jNode.a(JNode.b(jNode, j, this.c));
                } else if (trim.equalsIgnoreCase("true") || trim.equalsIgnoreCase("T")) {
                    JNode jNode2 = this.g;
                    jNode2.a(JNode.a(jNode2, j, this.c, true));
                } else if (trim.equalsIgnoreCase("false") || trim.equalsIgnoreCase("F")) {
                    JNode jNode3 = this.g;
                    jNode3.a(JNode.a(jNode3, j, this.c, false));
                } else if (trim.equals("") || !i.matcher(trim).find()) {
                    this.g.a(JNode.a(this.g, j, this.c, trim, this.d));
                } else {
                    if (trim.startsWith("-.")) {
                        trim = "-0." + trim.substring(2);
                    } else if (trim.startsWith("+.")) {
                        trim = "0." + trim.substring(2);
                    } else if (trim.startsWith(".")) {
                        trim = "0".concat(String.valueOf(trim));
                    }
                    this.g.a(JNode.a(this.g, j, this.c, trim));
                }
            } else {
                this.g.a(JNode.a(this.g, j, this.c, sb, i2));
            }
        }
    }

    public final void h() {
        if (!this.f4942a) {
            String sb = this.f.toString();
            int i2 = this.d;
            if (i2 == 0) {
                String trim = sb.trim();
                if (trim.equalsIgnoreCase("null")) {
                    JNode jNode = this.g;
                    jNode.a(JNode.b(jNode, (String) null, this.c));
                } else if (trim.equalsIgnoreCase("true") || trim.equalsIgnoreCase("T")) {
                    JNode jNode2 = this.g;
                    jNode2.a(JNode.a(jNode2, (String) null, this.c, true));
                } else if (trim.equalsIgnoreCase("false") || trim.equalsIgnoreCase("F")) {
                    JNode jNode3 = this.g;
                    jNode3.a(JNode.a(jNode3, (String) null, this.c, false));
                } else if (i.matcher(trim).find()) {
                    if (trim.startsWith("-.")) {
                        trim = "-0." + trim.substring(2);
                    } else if (trim.startsWith("+.")) {
                        trim = "0." + trim.substring(2);
                    } else if (trim.startsWith(".")) {
                        trim = "0".concat(String.valueOf(trim));
                    }
                    this.g.a(JNode.a(this.g, trim));
                } else {
                    this.g.a(JNode.a(this.g, trim, this.d));
                }
            } else {
                this.g.a(JNode.a(this.g, sb, i2));
            }
        }
    }

    public final void i() {
        if (!this.f4942a) {
            this.f4942a = true;
        }
    }

    public final void a(int i2) {
        if (!this.f4942a) {
            this.c = i2;
            this.e = new StringBuilder();
            this.h = 1;
        }
    }

    public final void c(int i2) {
        if (!this.f4942a) {
            this.d = i2;
            this.f = new StringBuilder();
            this.h = 2;
        }
    }

    public final void a(char c2) {
        a(c2, false);
    }

    public final void a(char c2, boolean z) {
        if (!this.f4942a) {
            int i2 = this.h;
            if (i2 == 1) {
                a(c2, this.e, z);
            } else if (i2 == 2) {
                a(c2, this.f, z);
            }
        }
    }

    public final void b(int i2) {
        if (!this.f4942a) {
            this.d = i2;
            this.f = new StringBuilder();
            this.h = 2;
        }
    }

    private static void a(char c2, StringBuilder sb, boolean z) {
        if (c2 < 14 && c2 >= 8) {
            switch (c2) {
                case 8:
                    sb.append("\\b");
                    return;
                case 9:
                    sb.append("\\t");
                    return;
                case 10:
                    sb.append("\\n");
                    return;
                case 12:
                    sb.append("\\f");
                    return;
                case 13:
                    sb.append("\\r");
                    return;
                default:
                    sb.append(c2);
                    return;
            }
        } else if (c2 == '\"') {
            sb.append('\\');
            sb.append(c2);
        } else if (c2 == '/') {
            sb.append('\\');
            sb.append(c2);
        } else if (c2 == '\\') {
            sb.append('\\');
            sb.append(c2);
        } else if (c2 < ' ') {
            String str = "000" + Integer.toHexString(c2);
            sb.append("\\u" + str.substring(str.length() - 4));
        } else {
            if (z) {
                if (c2 == 'u' || c2 == 'n' || c2 == 'b' || c2 == 'r' || c2 == 't' || c2 == 'f') {
                    sb.append("\\");
                } else {
                    sb.append("\\\\");
                }
            }
            sb.append(c2);
        }
    }
}
