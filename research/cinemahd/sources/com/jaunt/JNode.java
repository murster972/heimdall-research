package com.jaunt;

import java.util.Iterator;

public class JNode implements Iterable<JNode> {

    /* renamed from: a  reason: collision with root package name */
    private String f4922a;
    protected JNode b;
    private Type c;
    private Object d;

    public enum Type {
        UNDEFINED,
        OBJECT,
        ARRAY,
        STRING,
        BOOLEAN,
        NUMBER
    }

    JNode(JNode jNode, String str, int i, Type type) {
        this.f4922a = str;
        this.b = jNode;
        this.c = type;
        this.d = null;
    }

    static JNode a(JNode jNode, String str, int i, String str2, int i2) {
        return new JNode(jNode, str, i, Type.STRING, str2, i2);
    }

    static JNode b(JNode jNode, String str, int i) {
        return new JNode(jNode, str, i, Type.UNDEFINED, (Object) null, -1);
    }

    /* access modifiers changed from: package-private */
    public void a(JNode jNode) {
    }

    public Type c() {
        return this.c;
    }

    public boolean d() {
        Type type = this.c;
        return type == Type.UNDEFINED || type == Type.STRING || type == Type.BOOLEAN || type == Type.NUMBER;
    }

    /* access modifiers changed from: protected */
    public boolean e() {
        JNode jNode = this.b;
        if (jNode != null && jNode.c == Type.OBJECT) {
            return true;
        }
        return false;
    }

    public Iterator<JNode> iterator() {
        return null;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(this.d);
        return sb.toString();
    }

    static JNode a(JNode jNode, String str, int i) {
        return new JNode(jNode, (String) null, -1, Type.STRING, str, i);
    }

    public JNode b() {
        return this.b;
    }

    static JNode a(JNode jNode, String str, int i, String str2) {
        return new JNode(jNode, str, i, Type.NUMBER, str2, -1);
    }

    static JNode a(JNode jNode, String str) {
        return new JNode(jNode, (String) null, -1, Type.NUMBER, str, -1);
    }

    static JNode a(JNode jNode, String str, int i, boolean z) {
        return new JNode(jNode, str, i, Type.BOOLEAN, Boolean.valueOf(z), -1);
    }

    private JNode(JNode jNode, String str, int i, Type type, Object obj, int i2) {
        this.b = jNode;
        this.f4922a = str;
        this.c = type;
        this.d = obj;
    }

    public String a() {
        return this.f4922a;
    }

    /* access modifiers changed from: package-private */
    public final String a(int i, boolean z, boolean z2, boolean z3) {
        StringBuilder sb = new StringBuilder();
        Type type = this.c;
        if (type == Type.UNDEFINED) {
            if (!e()) {
                a(sb, i);
            }
            sb.append("null");
            if (z) {
                sb.append(",");
            }
            if (z2) {
                sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        } else if (type == Type.STRING) {
            if (!e()) {
                a(sb, i);
            }
            sb.append("\"" + this.d + "\"");
            if (z) {
                sb.append(",");
            }
            if (z2) {
                sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        } else if (type == Type.BOOLEAN) {
            if (!e()) {
                a(sb, i);
            }
            sb.append(this.d);
            if (z) {
                sb.append(",");
            }
            if (z2) {
                sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        } else if (type == Type.NUMBER) {
            if (!e()) {
                a(sb, i);
            }
            sb.append(this.d);
            if (z) {
                sb.append(",");
            }
            if (z2) {
                sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        } else if (type == Type.OBJECT) {
            return ((q) this).a(i, z, z3);
        } else {
            if (type == Type.ARRAY) {
                return ((l) this).a(i, z, z3);
            }
        }
        return sb.toString();
    }

    private static void a(StringBuilder sb, int i) {
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(" ");
        }
    }
}
