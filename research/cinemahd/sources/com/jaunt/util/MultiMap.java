package com.jaunt.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MultiMap<K, V> implements Serializable {
    private static final long serialVersionUID = -2470005590918308929L;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public int f4945a;
    private LinkedHashMap<K, List<V>> b;

    public MultiMap() {
        this.b = new LinkedHashMap<>();
    }

    public V b(K k) {
        List list = this.b.get(k);
        if (list != null) {
            return list.get(0);
        }
        return null;
    }

    public List<V> c(K k) {
        ArrayList arrayList = new ArrayList();
        for (K next : this.b.keySet()) {
            if (k.toString().equalsIgnoreCase(next.toString())) {
                arrayList.addAll(this.b.get(next));
            }
        }
        return arrayList;
    }

    public String toString() {
        return a(":", ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE, false, "null");
    }

    public void a(K k, V v) {
        if (this.b.containsKey(k)) {
            this.b.get(k).add(v);
            return;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(v);
        this.b.put(k, arrayList);
    }

    public MultiMap(int i, int i2) {
        this.f4945a = i2;
        this.b = new LinkedHashMap<K, List<V>>(i, 0.75f, true) {
            private static final long serialVersionUID = 1;

            /* access modifiers changed from: protected */
            public final boolean removeEldestEntry(Map.Entry<K, List<V>> entry) {
                return size() > MultiMap.this.f4945a;
            }
        };
    }

    public List<V> b() {
        ArrayList arrayList = new ArrayList();
        for (List<V> addAll : this.b.values()) {
            arrayList.addAll(addAll);
        }
        return arrayList;
    }

    public List<V> a(K k) {
        return this.b.get(k);
    }

    public Set<K> a() {
        return this.b.keySet();
    }

    public String a(String str, String str2, boolean z, String str3) {
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList(a());
        for (int i = 0; i < arrayList.size(); i++) {
            Object obj = arrayList.get(i);
            List list = this.b.get(obj);
            for (int i2 = 0; i2 < list.size(); i2++) {
                Object obj2 = list.get(i2);
                String obj3 = obj2 != null ? obj2.toString() : str3;
                if (i != 0 || i2 != 0) {
                    sb.append(str2);
                }
                sb.append(obj + str + obj3);
            }
        }
        if (z) {
            sb.append(str2);
        }
        return sb.toString();
    }
}
