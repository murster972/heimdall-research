package com.jaunt.util;

public interface Cache {
    String get(String str) throws CacheException;

    void put(String str, String str2) throws CacheException;
}
