package com.jaunt.util;

import com.jaunt.HttpResponse;
import com.jaunt.UserAgent;
import java.io.BufferedReader;
import java.io.InputStream;

public interface Handler<T> {
    void a(UserAgent userAgent, HttpResponse httpResponse, InputStream inputStream, BufferedReader bufferedReader) throws Exception;

    T getContent();
}
