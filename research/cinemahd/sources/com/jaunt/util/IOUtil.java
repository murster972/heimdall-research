package com.jaunt.util;

import com.facebook.common.util.ByteConstants;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;

public class IOUtil {
    public static void a(String str) {
        PrintStream printStream = System.err;
        printStream.println("AN UNEXPECTED ERROR HAS OCCURED IN JAUNT:\n" + str + "\nTo report this error, please visit https://groups.google.com/forum/#!forum/jaunt-api");
    }

    public static void a(File file, String str) throws IOException {
        a(file, str, "UTF-8");
    }

    public static void a(File file, String str, String str2) throws IOException {
        Charset forName = Charset.forName(str2);
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, forName);
        PrintWriter printWriter = new PrintWriter(outputStreamWriter);
        printWriter.println(str);
        printWriter.close();
        outputStreamWriter.close();
        fileOutputStream.close();
    }

    public static String a(InputStream inputStream) throws IOException {
        InputStreamReader inputStreamReader;
        char[] cArr = new char[ByteConstants.KB];
        StringBuilder sb = new StringBuilder();
        try {
            inputStreamReader = new InputStreamReader(inputStream, "UTF-8");
            while (true) {
                int read = inputStreamReader.read(cArr, 0, ByteConstants.KB);
                if (read < 0) {
                    break;
                }
                sb.append(cArr, 0, read);
            }
            inputStreamReader.close();
            inputStream.close();
        } catch (UnsupportedEncodingException unused) {
        } catch (Throwable th) {
            inputStreamReader.close();
            inputStream.close();
            throw th;
        }
        return sb.toString();
    }

    public static String a(Throwable th) {
        StringBuilder sb = new StringBuilder();
        for (StackTraceElement stackTraceElement : th.getStackTrace()) {
            sb.append(stackTraceElement.toString());
            sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        return sb.toString();
    }
}
