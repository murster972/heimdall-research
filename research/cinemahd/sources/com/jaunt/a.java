package com.jaunt;

final class a extends Node implements Cloneable {
    private String c;
    private String d;
    private short e;

    public a(Element element, String str, String str2, short s) {
        super(2);
        a(element);
        this.c = str;
        this.d = str2;
        this.e = s;
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        this.d = str;
    }

    public final Object clone() {
        return new a(b(), this.c, this.d, this.e);
    }

    public final String d() {
        if (this.d == null) {
            return this.c;
        }
        return String.valueOf(this.c) + "=" + h();
    }

    public final String e() {
        return this.c;
    }

    public final String f() {
        return this.d;
    }

    public final String g() {
        short s = this.e;
        if (s == 1) {
            return this.d;
        }
        if (s == 2) {
            return "'" + this.d + "'";
        } else if (s != 3) {
            return this.d;
        } else {
            return "\"" + this.d + "\"";
        }
    }

    public final String h() {
        short s = this.e;
        if (s == 1) {
            return "\"" + this.d + "\"";
        } else if (s == 2) {
            return "'" + this.d + "'";
        } else if (s != 3) {
            return this.d;
        } else {
            return "\"" + this.d + "\"";
        }
    }

    public final String toString() {
        if (this.d == null) {
            return this.c;
        }
        return String.valueOf(this.c) + "=" + g();
    }
}
