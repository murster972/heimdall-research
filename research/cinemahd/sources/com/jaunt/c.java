package com.jaunt;

import java.io.BufferedReader;
import java.io.IOException;

final class c {

    /* renamed from: a  reason: collision with root package name */
    private BufferedReader f4933a = null;

    public c(BufferedReader bufferedReader) {
        this.f4933a = bufferedReader;
    }

    public final void a() throws IOException {
        this.f4933a.close();
    }

    public final void b() throws IOException {
        this.f4933a.reset();
    }

    public final int c() throws IOException {
        return this.f4933a.read();
    }

    public final void a(int i) throws IOException {
        this.f4933a.mark(i);
    }
}
