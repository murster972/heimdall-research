package com.jaunt;

class b {

    /* renamed from: a  reason: collision with root package name */
    private String f4932a;
    private String b;
    private t c;

    /* JADX WARNING: type inference failed for: r15v1, types: [byte[]] */
    /* JADX WARNING: type inference failed for: r9v1, types: [byte] */
    /* JADX WARNING: type inference failed for: r9v7, types: [byte] */
    /* JADX WARNING: type inference failed for: r9v10, types: [byte] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(java.lang.String r15) {
        /*
            byte[] r15 = r15.getBytes()
            r0 = 4
            char[] r1 = new char[r0]
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1
            r7 = 0
            r8 = 0
        L_0x0012:
            int r9 = r15.length
            java.lang.String r10 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
            r11 = 3
            r12 = 2
            if (r5 < r9) goto L_0x0042
            r15 = 61
            if (r6 == r12) goto L_0x002e
            if (r6 == r11) goto L_0x0020
            goto L_0x003d
        L_0x0020:
            int r0 = r7 << 2
            char r0 = r10.charAt(r0)
            r1[r12] = r0
            r1[r11] = r15
            r2.append(r1)
            goto L_0x003d
        L_0x002e:
            int r0 = r7 << 4
            char r0 = r10.charAt(r0)
            r1[r4] = r0
            r1[r11] = r15
            r1[r12] = r15
            r2.append(r1)
        L_0x003d:
            java.lang.String r15 = r2.toString()
            return r15
        L_0x0042:
            byte r9 = r15[r5]
            if (r9 < 0) goto L_0x0049
            byte r9 = r15[r5]
            goto L_0x004f
        L_0x0049:
            byte r9 = r15[r5]
            r9 = r9 & 127(0x7f, float:1.78E-43)
            int r9 = r9 + 128
        L_0x004f:
            if (r6 == r4) goto L_0x0086
            if (r6 == r12) goto L_0x0078
            if (r6 == r11) goto L_0x0056
            goto L_0x0090
        L_0x0056:
            int r13 = r7 << 2
            int r14 = r9 >>> 6
            r13 = r13 | r14
            char r13 = r10.charAt(r13)
            r1[r12] = r13
            r9 = r9 & 63
            char r9 = r10.charAt(r9)
            r1[r11] = r9
            r2.append(r1)
            int r8 = r8 + 1
            int r9 = r8 % 19
            if (r9 != 0) goto L_0x0090
            java.lang.String r9 = "\r\n"
            r2.append(r9)
            goto L_0x0090
        L_0x0078:
            int r7 = r7 << 4
            int r12 = r9 >>> 4
            r7 = r7 | r12
            char r7 = r10.charAt(r7)
            r1[r4] = r7
            r7 = r9 & 15
            goto L_0x0090
        L_0x0086:
            int r7 = r9 >>> 2
            char r7 = r10.charAt(r7)
            r1[r3] = r7
            r7 = r9 & 3
        L_0x0090:
            if (r6 >= r11) goto L_0x0095
            int r6 = r6 + 1
            goto L_0x0096
        L_0x0095:
            r6 = 1
        L_0x0096:
            int r5 = r5 + 1
            goto L_0x0012
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.b.a(java.lang.String):java.lang.String");
    }

    public static String b(String str) {
        return new String(c(str));
    }

    /* JADX WARNING: Removed duplicated region for block: B:28:0x005c  */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x008e  */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x0098  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static byte[] c(java.lang.String r13) {
        /*
            r0 = 3
            byte[] r1 = new byte[r0]
            java.io.ByteArrayOutputStream r2 = new java.io.ByteArrayOutputStream
            int r3 = r13.length()
            r2.<init>(r3)
            r3 = 0
            r4 = 1
            r5 = 0
            r6 = 1
        L_0x0010:
            int r7 = r13.length()
            r8 = 61
            r9 = 0
            r10 = 4
            r11 = 2
            if (r5 < r7) goto L_0x001d
            goto L_0x009d
        L_0x001d:
            char r7 = r13.charAt(r5)
            boolean r12 = java.lang.Character.isWhitespace(r7)
            if (r12 != 0) goto L_0x00d5
            r12 = 65
            if (r7 < r12) goto L_0x0033
            r12 = 90
            if (r7 > r12) goto L_0x0033
            int r7 = r7 + -65
        L_0x0031:
            byte r7 = (byte) r7
            goto L_0x005a
        L_0x0033:
            r12 = 97
            if (r7 < r12) goto L_0x0040
            r12 = 122(0x7a, float:1.71E-43)
            if (r7 > r12) goto L_0x0040
            int r7 = r7 + -97
            int r7 = r7 + 26
            goto L_0x0031
        L_0x0040:
            r12 = 48
            if (r7 < r12) goto L_0x004d
            r12 = 57
            if (r7 > r12) goto L_0x004d
            int r7 = r7 + -48
            int r7 = r7 + 52
            goto L_0x0031
        L_0x004d:
            r12 = 43
            if (r7 != r12) goto L_0x0054
            r7 = 62
            goto L_0x005a
        L_0x0054:
            r12 = 47
            if (r7 != r12) goto L_0x009a
            r7 = 63
        L_0x005a:
            if (r6 == r4) goto L_0x008e
            if (r6 == r11) goto L_0x007e
            if (r6 == r0) goto L_0x006d
            if (r6 == r10) goto L_0x0063
            goto L_0x0093
        L_0x0063:
            byte r8 = r1[r11]
            r7 = r7 | r8
            byte r7 = (byte) r7
            r1[r11] = r7
            r2.write(r1, r3, r0)
            goto L_0x0093
        L_0x006d:
            byte r8 = r1[r4]
            int r9 = r7 >>> 2
            byte r9 = (byte) r9
            r8 = r8 | r9
            byte r8 = (byte) r8
            r1[r4] = r8
            r7 = r7 & 3
            int r7 = r7 << 6
            byte r7 = (byte) r7
            r1[r11] = r7
            goto L_0x0093
        L_0x007e:
            byte r8 = r1[r3]
            int r9 = r7 >>> 4
            byte r9 = (byte) r9
            r8 = r8 | r9
            byte r8 = (byte) r8
            r1[r3] = r8
            r7 = r7 & 15
            int r7 = r7 << r10
            byte r7 = (byte) r7
            r1[r4] = r7
            goto L_0x0093
        L_0x008e:
            int r7 = r7 << 2
            byte r7 = (byte) r7
            r1[r3] = r7
        L_0x0093:
            if (r6 >= r10) goto L_0x0098
            int r6 = r6 + 1
            goto L_0x00d5
        L_0x0098:
            r6 = 1
            goto L_0x00d5
        L_0x009a:
            if (r7 == r8) goto L_0x009d
            return r9
        L_0x009d:
            int r7 = r13.length()
            if (r5 >= r7) goto L_0x00cd
            if (r6 == r0) goto L_0x00b7
            if (r6 == r10) goto L_0x00a8
            return r9
        L_0x00a8:
            r2.write(r1, r3, r11)
            char r13 = r13.charAt(r5)
            if (r13 != r8) goto L_0x00b6
            byte[] r13 = r2.toByteArray()
            return r13
        L_0x00b6:
            return r9
        L_0x00b7:
            r2.write(r1, r3, r4)
            char r0 = r13.charAt(r5)
            if (r0 != r8) goto L_0x00cc
            int r5 = r5 + r4
            char r13 = r13.charAt(r5)
            if (r13 != r8) goto L_0x00cc
            byte[] r13 = r2.toByteArray()
            return r13
        L_0x00cc:
            return r9
        L_0x00cd:
            if (r6 != r4) goto L_0x00d4
            byte[] r13 = r2.toByteArray()
            return r13
        L_0x00d4:
            return r9
        L_0x00d5:
            int r5 = r5 + 1
            goto L_0x0010
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.b.c(java.lang.String):byte[]");
    }

    public String toString() {
        return "username: " + this.f4932a + "\npassword: " + this.b + "\nrealm: " + this.c.toString();
    }

    public String b() {
        return a(String.valueOf(this.f4932a) + ":" + this.b);
    }

    /* access modifiers changed from: package-private */
    public t a() {
        return this.c;
    }
}
