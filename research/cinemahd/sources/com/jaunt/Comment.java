package com.jaunt;

import com.jaunt.util.IOUtil;

public class Comment extends Node {
    private String c;
    private short d;

    public Comment(String str, short s) {
        super(8);
        this.d = s;
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return toString();
    }

    public String toString() {
        short s = this.d;
        if (s == 8) {
            return "<!--" + this.c + "-->";
        } else if (s == 0) {
            return "<!" + this.c + ">";
        } else if (s == 4) {
            return "<![CDATA[" + this.c + "]]>";
        } else if (s == 10) {
            return "<!DOCTYPE " + this.c + ">";
        } else if (s == 7) {
            return "<" + this.c + ">";
        } else {
            IOUtil.a("Comment.toString; invalid commentType; commentType: " + this.d);
            return null;
        }
    }
}
