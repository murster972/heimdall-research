package com.jaunt;

import com.jaunt.util.IOUtil;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;

final class s {

    /* renamed from: a  reason: collision with root package name */
    private p f4943a;
    private StringBuilder b;
    private int c = -1;
    private int d = 0;

    public s(p pVar) {
        this.f4943a = pVar;
        this.b = new StringBuilder();
    }

    private static boolean a(char c2) {
        return c2 == ' ' || c2 == 10 || c2 == 13 || c2 == 9;
    }

    private static boolean b(char c2) {
        if (c2 == '(') {
            return true;
        }
        if (c2 < 'A' || c2 > 'Z') {
            return c2 >= 'a' && c2 <= 'z';
        }
        return true;
    }

    private char c(char c2) {
        this.b.append(c2);
        return c2;
    }

    private boolean d() {
        try {
            String b2 = b.b("amF2YS5sYW5nLlN5c3RlbQ==");
            long longValue = ((Long) Class.forName(b2).getDeclaredMethod(b.b("Y3VycmVudFRpbWVNaWxsaXM="), new Class[0]).invoke((Object) null, (Object[]) null)).longValue();
            String b3 = b.b("MTYwOTQ4ODAwMDAwMA==");
            String b4 = b.b("MTYwNDA0MTIwMDAwMA==");
            if (longValue >= Long.parseLong(b3) || longValue <= Long.parseLong(b4)) {
                return false;
            }
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public final void a(int i) {
        this.c = i;
    }

    public final String b() {
        return this.b.toString();
    }

    private static boolean b(String str, char c2, c cVar) throws IOException {
        String upperCase = str.toUpperCase();
        String lowerCase = str.toLowerCase();
        if (upperCase.charAt(0) != c2 && lowerCase.charAt(0) != c2) {
            return false;
        }
        int length = str.length();
        cVar.a(length + 1);
        int i = 1;
        while (i < length) {
            int c3 = cVar.c();
            if (c3 == -1) {
                cVar.b();
                return false;
            }
            char c4 = (char) c3;
            if (upperCase.charAt(i) == c4 || lowerCase.charAt(i) == c4) {
                i++;
            } else {
                cVar.b();
                return false;
            }
        }
        char c5 = (char) cVar.c();
        if (!(a(c5) || c5 == '/' || c5 == '>')) {
            cVar.b();
            return false;
        }
        cVar.b();
        return true;
    }

    public final p a() {
        return this.f4943a;
    }

    public final d c() {
        return this.f4943a.b();
    }

    public final void a(String str, String str2) {
        try {
            StringReader stringReader = new StringReader(str);
            a(new BufferedReader(stringReader), str2);
            stringReader.close();
        } catch (IOException e) {
            IOUtil.a("Parser.parse(String); IOException; e: ".concat(String.valueOf(e)));
        }
    }

    public final void a(BufferedReader bufferedReader, String str) throws IOException {
        a(new c(bufferedReader), str);
    }

    /* JADX WARNING: Removed duplicated region for block: B:353:0x0a0b A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.jaunt.c r32, java.lang.String r33) throws java.io.IOException {
        /*
            r31 = this;
            r0 = r31
            r1 = r32
            com.jaunt.p r2 = r0.f4943a
            r2.c()
            com.jaunt.p r2 = r0.f4943a
            r3 = r33
            r2.a((java.lang.String) r3)
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r0.b = r2
            boolean r2 = r31.d()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r3 = 0
            r0.d = r3
            r5 = 0
            r6 = 1
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
            r19 = 0
            r20 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 0
            r25 = 0
            r26 = 0
            r27 = 0
        L_0x0046:
            int r3 = r32.c()
            java.lang.String r4 = "script"
            r29 = r2
            r2 = -1
            if (r3 != r2) goto L_0x0052
            goto L_0x0067
        L_0x0052:
            int r2 = r0.d
            r28 = 1
            int r2 = r2 + 1
            r0.d = r2
            int r2 = r0.c
            if (r2 < 0) goto L_0x0149
            int r1 = r0.d
            if (r1 <= r2) goto L_0x0149
            com.jaunt.p r1 = r0.f4943a
            r1.a()
        L_0x0067:
            r32.a()
            if (r6 == 0) goto L_0x0073
            com.jaunt.p r1 = r0.f4943a
            r1.o()
            goto L_0x0141
        L_0x0073:
            if (r8 != 0) goto L_0x0141
            if (r7 == 0) goto L_0x0084
            com.jaunt.p r1 = r0.f4943a
            r1.g()
            com.jaunt.p r1 = r0.f4943a
            r2 = -1
            r1.a((int) r2)
            goto L_0x0141
        L_0x0084:
            r2 = -1
            if (r9 == 0) goto L_0x008e
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x008e:
            if (r10 == 0) goto L_0x009c
            com.jaunt.p r1 = r0.f4943a
            r1.i()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x009c:
            if (r12 == 0) goto L_0x00aa
            com.jaunt.p r1 = r0.f4943a
            r1.i()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00aa:
            if (r14 == 0) goto L_0x00b3
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00b3:
            if (r17 != 0) goto L_0x0141
            if (r5 == 0) goto L_0x00c3
            com.jaunt.p r1 = r0.f4943a
            r1.m()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00c3:
            if (r18 == 0) goto L_0x00d1
            com.jaunt.p r1 = r0.f4943a
            r1.m()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00d1:
            if (r19 == 0) goto L_0x00de
            com.jaunt.p r1 = r0.f4943a
            r1.m()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00de:
            if (r20 == 0) goto L_0x00eb
            com.jaunt.p r1 = r0.f4943a
            r1.m()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00eb:
            if (r21 == 0) goto L_0x00f8
            com.jaunt.p r1 = r0.f4943a
            r1.m()
            com.jaunt.p r1 = r0.f4943a
            r1.a((int) r2)
            goto L_0x0141
        L_0x00f8:
            if (r15 == 0) goto L_0x0100
            com.jaunt.p r1 = r0.f4943a
            r1.q()
            goto L_0x0141
        L_0x0100:
            if (r22 == 0) goto L_0x0108
            com.jaunt.p r1 = r0.f4943a
            r1.s()
            goto L_0x0141
        L_0x0108:
            if (r23 == 0) goto L_0x0110
            com.jaunt.p r1 = r0.f4943a
            r1.u()
            goto L_0x0141
        L_0x0110:
            if (r25 == 0) goto L_0x0118
            com.jaunt.p r1 = r0.f4943a
            r1.w()
            goto L_0x0141
        L_0x0118:
            if (r27 == 0) goto L_0x0120
            com.jaunt.p r1 = r0.f4943a
            r1.y()
            goto L_0x0141
        L_0x0120:
            if (r24 == 0) goto L_0x0141
            com.jaunt.p r1 = r0.f4943a
            r1.o()
            com.jaunt.p r1 = r0.f4943a
            r1.e()
            com.jaunt.p r1 = r0.f4943a
            r1.f()
            com.jaunt.p r1 = r0.f4943a
            r1.b((java.lang.String) r4)
            com.jaunt.p r1 = r0.f4943a
            r1.g()
            com.jaunt.p r1 = r0.f4943a
            r2 = -1
            r1.a((int) r2)
        L_0x0141:
            com.jaunt.p r1 = r0.f4943a
            int r2 = r0.d
            r1.b((int) r2)
            return
        L_0x0149:
            char r1 = (char) r3
            java.lang.StringBuilder r2 = r0.b
            r2.append(r1)
            r2 = 60
            if (r6 == 0) goto L_0x0166
            if (r1 != r2) goto L_0x015b
            r2 = r32
            r6 = 0
            r8 = 1
            goto L_0x0a07
        L_0x015b:
            com.jaunt.p r2 = r0.f4943a
            r2.a((char) r1)
        L_0x0160:
            r2 = r32
        L_0x0162:
            r30 = r5
            goto L_0x09fc
        L_0x0166:
            r3 = 34
            r2 = 39
            if (r5 == 0) goto L_0x019c
            if (r1 != r3) goto L_0x0182
            if (r11 != 0) goto L_0x017c
            com.jaunt.p r2 = r0.f4943a
            r2.m()
            r2 = r32
            r9 = r29
        L_0x0179:
            r5 = 0
            goto L_0x0a07
        L_0x017c:
            com.jaunt.p r2 = r0.f4943a
            r2.a((char) r1)
            goto L_0x0160
        L_0x0182:
            if (r1 != r2) goto L_0x0196
            if (r11 != 0) goto L_0x0190
            com.jaunt.p r2 = r0.f4943a
            r2.a((char) r1)
            r2 = r32
            r20 = r29
            goto L_0x0179
        L_0x0190:
            com.jaunt.p r2 = r0.f4943a
            r2.a((char) r1)
            goto L_0x0160
        L_0x0196:
            com.jaunt.p r2 = r0.f4943a
            r2.a((char) r1)
            goto L_0x0160
        L_0x019c:
            java.lang.String r3 = "/>"
            r2 = 62
            if (r10 == 0) goto L_0x0234
            if (r1 != r2) goto L_0x01ce
            com.jaunt.p r2 = r0.f4943a
            r2.i()
            com.jaunt.p r2 = r0.f4943a
            r3 = -1
            r2.a((int) r3)
            com.jaunt.p r2 = r0.f4943a
            r2.n()
            if (r13 == 0) goto L_0x01be
            r2 = r32
            r24 = r29
            r10 = 0
        L_0x01bb:
            r13 = 0
            goto L_0x0a07
        L_0x01be:
            if (r16 == 0) goto L_0x01c9
            r2 = r32
            r26 = r29
            r10 = 0
        L_0x01c5:
            r16 = 0
            goto L_0x0a07
        L_0x01c9:
            r2 = r32
        L_0x01cb:
            r6 = r29
            goto L_0x01dd
        L_0x01ce:
            boolean r2 = a((char) r1)
            if (r2 == 0) goto L_0x01e0
            com.jaunt.p r2 = r0.f4943a
            r2.i()
            r2 = r32
            r12 = r29
        L_0x01dd:
            r10 = 0
            goto L_0x0a07
        L_0x01e0:
            r2 = r32
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x0215
            com.jaunt.p r3 = r0.f4943a
            r3.i()
            com.jaunt.p r3 = r0.f4943a
            r4 = 3
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            if (r13 == 0) goto L_0x020a
            r6 = r29
            r10 = 0
        L_0x0205:
            r13 = 0
        L_0x0206:
            r24 = 0
            goto L_0x0a07
        L_0x020a:
            if (r16 == 0) goto L_0x01cb
            r6 = r29
            r10 = 0
        L_0x020f:
            r16 = 0
        L_0x0211:
            r26 = 0
            goto L_0x0a07
        L_0x0215:
            r3 = 47
            if (r1 != r3) goto L_0x0221
            com.jaunt.p r3 = r0.f4943a
            r3.i()
            r9 = r29
            goto L_0x01dd
        L_0x0221:
            r3 = 61
            if (r1 != r3) goto L_0x022d
            com.jaunt.p r3 = r0.f4943a
            r3.i()
            r14 = r29
            goto L_0x01dd
        L_0x022d:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x0162
        L_0x0234:
            if (r7 == 0) goto L_0x0291
            boolean r4 = a((char) r1)
            if (r4 == 0) goto L_0x0248
            com.jaunt.p r2 = r0.f4943a
            r2.g()
            r2 = r32
        L_0x0243:
            r9 = r29
        L_0x0245:
            r7 = 0
            goto L_0x0a07
        L_0x0248:
            if (r1 != r2) goto L_0x025f
            com.jaunt.p r2 = r0.f4943a
            r2.g()
            com.jaunt.p r2 = r0.f4943a
            r3 = -1
            r2.a((int) r3)
            com.jaunt.p r2 = r0.f4943a
            r2.n()
            r2 = r32
        L_0x025c:
            r6 = r29
            goto L_0x0245
        L_0x025f:
            r2 = r32
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x0280
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            com.jaunt.p r3 = r0.f4943a
            r4 = 3
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            goto L_0x025c
        L_0x0280:
            r3 = 47
            if (r1 != r3) goto L_0x028a
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            goto L_0x0243
        L_0x028a:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x0162
        L_0x0291:
            r2 = r32
            r30 = r5
            r5 = 47
            if (r8 == 0) goto L_0x053e
            if (r1 != r5) goto L_0x02a2
            r17 = r29
        L_0x029d:
            r5 = r30
        L_0x029f:
            r8 = 0
            goto L_0x0a07
        L_0x02a2:
            boolean r3 = b(r4, r1, r2)
            if (r3 == 0) goto L_0x0326
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.d()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            com.jaunt.p r3 = r0.f4943a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.b((java.lang.String) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r4 = 62
            if (r3 != r4) goto L_0x031e
            com.jaunt.p r3 = r0.f4943a
            r4 = -1
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r24 = r29
            goto L_0x0321
        L_0x031e:
            r9 = r29
            r13 = r9
        L_0x0321:
            r5 = r30
            r6 = 0
            goto L_0x029f
        L_0x0326:
            java.lang.String r3 = "style"
            boolean r3 = b(r3, r1, r2)
            if (r3 == 0) goto L_0x039e
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.d()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            com.jaunt.p r3 = r0.f4943a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r1)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.b((java.lang.String) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r4 = 62
            if (r3 != r4) goto L_0x0399
            com.jaunt.p r3 = r0.f4943a
            r4 = -1
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r26 = r29
            goto L_0x0321
        L_0x0399:
            r9 = r29
            r16 = r9
            goto L_0x0321
        L_0x039e:
            r3 = 33
            if (r1 != r3) goto L_0x04e7
            java.lang.String r3 = "!-->"
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x03da
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.p()
            com.jaunt.p r3 = r0.f4943a
            r3.q()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
        L_0x03d6:
            r6 = r29
            goto L_0x029d
        L_0x03da:
            java.lang.String r3 = "!--->"
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x0417
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.p()
            com.jaunt.p r3 = r0.f4943a
            r3.q()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            goto L_0x03d6
        L_0x0417:
            java.lang.String r3 = "!--"
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x043d
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.p()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r15 = r29
            goto L_0x0321
        L_0x043d:
            java.lang.String r3 = "!doctype"
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x048b
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.t()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r23 = r29
            goto L_0x0321
        L_0x048b:
            java.lang.String r3 = "![CDATA["
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x04d9
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.x()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r27 = r29
            goto L_0x0321
        L_0x04d9:
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.r()
            r22 = r29
            goto L_0x0321
        L_0x04e7:
            r3 = 63
            if (r1 != r3) goto L_0x0504
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.v()
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            com.jaunt.p r3 = r0.f4943a
            r4 = 1
            r3.a((short) r4)
            r25 = r29
            goto L_0x0321
        L_0x0504:
            boolean r3 = b(r1)
            if (r3 == 0) goto L_0x0522
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.d()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r7 = r29
            goto L_0x0321
        L_0x0522:
            r3 = 60
            if (r1 != r3) goto L_0x052d
            com.jaunt.p r4 = r0.f4943a
            r4.a((char) r3)
            goto L_0x09fc
        L_0x052d:
            com.jaunt.p r3 = r0.f4943a
            java.lang.String r4 = java.lang.String.valueOf(r1)
            java.lang.String r5 = "<"
            java.lang.String r4 = r5.concat(r4)
            r3.b((java.lang.String) r4)
            goto L_0x03d6
        L_0x053e:
            if (r15 == 0) goto L_0x0570
            java.lang.String r3 = "-->"
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x0569
            com.jaunt.p r3 = r0.f4943a
            r3.q()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r6 = r29
            r5 = r30
            r15 = 0
            goto L_0x0a07
        L_0x0569:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0570:
            if (r17 == 0) goto L_0x05dd
            r4 = 62
            if (r1 != r4) goto L_0x059f
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.e()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            com.jaunt.p r3 = r0.f4943a
            r4 = 47
            r3.a((char) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            com.jaunt.p r3 = r0.f4943a
            r4 = -1
            r3.a((int) r4)
            r6 = r29
            r5 = r30
        L_0x059b:
            r17 = 0
            goto L_0x0a07
        L_0x059f:
            boolean r3 = a((char) r1)
            if (r3 == 0) goto L_0x05c3
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.d()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            com.jaunt.p r3 = r0.f4943a
            r4 = 47
            r3.a((char) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            r9 = r29
            goto L_0x05d9
        L_0x05c3:
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.e()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r7 = r29
        L_0x05d9:
            r5 = r30
            r6 = 0
            goto L_0x059b
        L_0x05dd:
            if (r9 == 0) goto L_0x0650
            r4 = 62
            if (r1 != r4) goto L_0x0607
            com.jaunt.p r3 = r0.f4943a
            r4 = -1
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            if (r13 == 0) goto L_0x05f7
            r24 = r29
            r5 = r30
            r9 = 0
            goto L_0x01bb
        L_0x05f7:
            if (r16 == 0) goto L_0x0600
            r26 = r29
            r5 = r30
            r9 = 0
            goto L_0x01c5
        L_0x0600:
            r6 = r29
        L_0x0602:
            r5 = r30
            r9 = 0
            goto L_0x0a07
        L_0x0607:
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x0638
            com.jaunt.p r3 = r0.f4943a
            r4 = 3
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r4 = 1
            r3.a((short) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            if (r13 == 0) goto L_0x062f
            r6 = r29
            r5 = r30
            r9 = 0
            goto L_0x0205
        L_0x062f:
            if (r16 == 0) goto L_0x0600
            r6 = r29
            r5 = r30
            r9 = 0
            goto L_0x020f
        L_0x0638:
            r3 = 39
            r4 = 1
            if (r1 == r3) goto L_0x09fc
            boolean r3 = a((char) r1)
            if (r3 != 0) goto L_0x09fc
            com.jaunt.p r3 = r0.f4943a
            r3.h()
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r10 = r29
            goto L_0x0602
        L_0x0650:
            r4 = 1
            if (r12 == 0) goto L_0x06cb
            r5 = 62
            if (r1 != r5) goto L_0x067b
            com.jaunt.p r3 = r0.f4943a
            r5 = -1
            r3.a((int) r5)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            if (r13 == 0) goto L_0x066b
            r24 = r29
            r5 = r30
            r12 = 0
            goto L_0x01bb
        L_0x066b:
            if (r16 == 0) goto L_0x0674
            r26 = r29
            r5 = r30
            r12 = 0
            goto L_0x01c5
        L_0x0674:
            r6 = r29
        L_0x0676:
            r5 = r30
            r12 = 0
            goto L_0x0a07
        L_0x067b:
            r5 = 39
            if (r1 == r5) goto L_0x09fc
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x06aa
            com.jaunt.p r3 = r0.f4943a
            r5 = 3
            r3.a((int) r5)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            if (r13 == 0) goto L_0x06a1
            r6 = r29
            r5 = r30
            r12 = 0
            goto L_0x0205
        L_0x06a1:
            if (r16 == 0) goto L_0x0674
            r6 = r29
            r5 = r30
            r12 = 0
            goto L_0x020f
        L_0x06aa:
            r3 = 47
            if (r1 != r3) goto L_0x06b1
            r9 = r29
            goto L_0x0676
        L_0x06b1:
            r3 = 61
            if (r1 != r3) goto L_0x06b8
            r14 = r29
            goto L_0x0676
        L_0x06b8:
            boolean r3 = a((char) r1)
            if (r3 != 0) goto L_0x09fc
            com.jaunt.p r3 = r0.f4943a
            r3.h()
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r10 = r29
            goto L_0x0676
        L_0x06cb:
            if (r14 == 0) goto L_0x075b
            r5 = 62
            if (r1 != r5) goto L_0x06fa
            com.jaunt.p r3 = r0.f4943a
            r3.j()
            com.jaunt.p r3 = r0.f4943a
            r3.m()
            com.jaunt.p r3 = r0.f4943a
            r5 = -1
            r3.a((int) r5)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            if (r13 == 0) goto L_0x06ee
            r24 = r29
            r5 = r30
            r13 = 0
            goto L_0x0705
        L_0x06ee:
            if (r16 == 0) goto L_0x06f7
            r26 = r29
            r5 = r30
            r14 = 0
            goto L_0x01c5
        L_0x06f7:
            r6 = r29
            goto L_0x0713
        L_0x06fa:
            r5 = 34
            if (r1 != r5) goto L_0x0708
            com.jaunt.p r3 = r0.f4943a
            r3.l()
            r5 = r29
        L_0x0705:
            r14 = 0
            goto L_0x0a07
        L_0x0708:
            r5 = 39
            if (r1 != r5) goto L_0x0716
            com.jaunt.p r3 = r0.f4943a
            r3.k()
            r18 = r29
        L_0x0713:
            r5 = r30
            goto L_0x0705
        L_0x0716:
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x0742
            com.jaunt.p r3 = r0.f4943a
            r5 = 3
            r3.a((int) r5)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            if (r13 == 0) goto L_0x0739
            r6 = r29
            r5 = r30
            r13 = 0
            r14 = 0
            goto L_0x0206
        L_0x0739:
            if (r16 == 0) goto L_0x06f7
            r6 = r29
            r5 = r30
            r14 = 0
            goto L_0x020f
        L_0x0742:
            boolean r3 = a((char) r1)
            if (r3 != 0) goto L_0x09fc
            com.jaunt.p r3 = r0.f4943a
            r3.j()
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            com.jaunt.p r3 = r0.f4943a
            r5 = 2
            r3.a((short) r5)
            r19 = r29
            goto L_0x0713
        L_0x075b:
            if (r18 == 0) goto L_0x0793
            r3 = 34
            if (r1 != r3) goto L_0x0777
            if (r11 != 0) goto L_0x0770
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r21 = r29
        L_0x076a:
            r5 = r30
            r18 = 0
            goto L_0x0a07
        L_0x0770:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0777:
            r3 = 39
            if (r1 != r3) goto L_0x078c
            if (r11 != 0) goto L_0x0785
            com.jaunt.p r3 = r0.f4943a
            r3.m()
            r9 = r29
            goto L_0x076a
        L_0x0785:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x078c:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0793:
            if (r19 == 0) goto L_0x07d7
            r3 = 62
            if (r1 != r3) goto L_0x07bd
            com.jaunt.p r3 = r0.f4943a
            r3.m()
            com.jaunt.p r3 = r0.f4943a
            r5 = -1
            r3.a((int) r5)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            if (r13 == 0) goto L_0x07b1
            r24 = r29
            r5 = r30
            r13 = 0
            goto L_0x07cc
        L_0x07b1:
            if (r16 == 0) goto L_0x07ba
            r26 = r29
            r5 = r30
            r16 = 0
            goto L_0x07cc
        L_0x07ba:
            r6 = r29
            goto L_0x07ca
        L_0x07bd:
            boolean r3 = a((char) r1)
            if (r3 == 0) goto L_0x07d0
            com.jaunt.p r3 = r0.f4943a
            r3.m()
            r9 = r29
        L_0x07ca:
            r5 = r30
        L_0x07cc:
            r19 = 0
            goto L_0x0a07
        L_0x07d0:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x07d7:
            if (r20 == 0) goto L_0x080f
            r3 = 39
            if (r1 != r3) goto L_0x07f1
            if (r11 != 0) goto L_0x07ea
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r5 = r29
        L_0x07e6:
            r20 = 0
            goto L_0x0a07
        L_0x07ea:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x07f1:
            r3 = 34
            if (r1 != r3) goto L_0x0808
            if (r11 != 0) goto L_0x0801
            com.jaunt.p r3 = r0.f4943a
            r3.m()
            r9 = r29
            r5 = r30
            goto L_0x07e6
        L_0x0801:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0808:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x080f:
            if (r21 == 0) goto L_0x0847
            r3 = 39
            if (r1 != r3) goto L_0x082b
            if (r11 != 0) goto L_0x0824
            com.jaunt.p r3 = r0.f4943a
            r3.m()
            r9 = r29
        L_0x081e:
            r5 = r30
            r21 = 0
            goto L_0x0a07
        L_0x0824:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x082b:
            r3 = 34
            if (r1 != r3) goto L_0x0840
            if (r11 != 0) goto L_0x0839
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            r18 = r29
            goto L_0x081e
        L_0x0839:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0840:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0847:
            if (r24 == 0) goto L_0x08e4
            java.lang.String r3 = "</script"
            boolean r3 = b(r3, r1, r2)
            if (r3 == 0) goto L_0x08dd
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.e()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            com.jaunt.p r3 = r0.f4943a
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            int r4 = r32.c()
            char r4 = (char) r4
            r0.c(r4)
            r5.append(r4)
            int r4 = r32.c()
            char r4 = (char) r4
            r0.c(r4)
            r5.append(r4)
            int r4 = r32.c()
            char r4 = (char) r4
            r0.c(r4)
            r5.append(r4)
            int r4 = r32.c()
            char r4 = (char) r4
            r0.c(r4)
            r5.append(r4)
            int r4 = r32.c()
            char r4 = (char) r4
            r0.c(r4)
            r5.append(r4)
            int r4 = r32.c()
            char r4 = (char) r4
            r0.c(r4)
            r5.append(r4)
            java.lang.String r4 = r5.toString()
            r3.b((java.lang.String) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r4 = 62
            if (r3 != r4) goto L_0x08d7
            com.jaunt.p r3 = r0.f4943a
            r4 = -1
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r6 = r29
            goto L_0x08d9
        L_0x08d7:
            r9 = r29
        L_0x08d9:
            r5 = r30
            goto L_0x0206
        L_0x08dd:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x08e4:
            if (r26 == 0) goto L_0x0976
            java.lang.String r3 = "</style"
            boolean r3 = b(r3, r1, r2)
            if (r3 == 0) goto L_0x096f
            com.jaunt.p r3 = r0.f4943a
            r3.o()
            com.jaunt.p r3 = r0.f4943a
            r3.e()
            com.jaunt.p r3 = r0.f4943a
            r3.f()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            com.jaunt.p r3 = r0.f4943a
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            int r5 = r32.c()
            char r5 = (char) r5
            r0.c(r5)
            r4.append(r5)
            java.lang.String r4 = r4.toString()
            r3.b((java.lang.String) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.g()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r4 = 62
            if (r3 != r4) goto L_0x0969
            com.jaunt.p r3 = r0.f4943a
            r4 = -1
            r3.a((int) r4)
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r6 = r29
            goto L_0x096b
        L_0x0969:
            r9 = r29
        L_0x096b:
            r5 = r30
            goto L_0x0211
        L_0x096f:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x0976:
            if (r27 == 0) goto L_0x09a8
            java.lang.String r3 = "]]>"
            boolean r3 = a(r3, r1, r2)
            if (r3 == 0) goto L_0x09a2
            com.jaunt.p r3 = r0.f4943a
            r3.y()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            int r3 = r32.c()
            char r3 = (char) r3
            r0.c(r3)
            r6 = r29
            r5 = r30
            r27 = 0
            goto L_0x0a07
        L_0x09a2:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x09a8:
            if (r25 == 0) goto L_0x09c5
            r3 = 62
            if (r1 != r3) goto L_0x09bf
            com.jaunt.p r3 = r0.f4943a
            r3.w()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r6 = r29
            r5 = r30
            r25 = 0
            goto L_0x0a07
        L_0x09bf:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x09c5:
            r3 = 62
            if (r23 == 0) goto L_0x09e2
            if (r1 != r3) goto L_0x09dc
            com.jaunt.p r3 = r0.f4943a
            r3.u()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r6 = r29
            r5 = r30
            r23 = 0
            goto L_0x0a07
        L_0x09dc:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
            goto L_0x09fc
        L_0x09e2:
            if (r22 == 0) goto L_0x09ff
            if (r1 != r3) goto L_0x09f7
            com.jaunt.p r3 = r0.f4943a
            r3.s()
            com.jaunt.p r3 = r0.f4943a
            r3.n()
            r6 = r29
            r5 = r30
            r22 = 0
            goto L_0x0a07
        L_0x09f7:
            com.jaunt.p r3 = r0.f4943a
            r3.a((char) r1)
        L_0x09fc:
            r5 = r30
            goto L_0x0a07
        L_0x09ff:
            java.lang.String r3 = "Parser.parse; invalid state"
            com.jaunt.util.IOUtil.a((java.lang.String) r3)
            r5 = r30
            r6 = 1
        L_0x0a07:
            r3 = 92
            if (r1 != r3) goto L_0x0a13
            if (r11 != 0) goto L_0x0a13
            r1 = r2
            r2 = r29
            r11 = r2
            goto L_0x0046
        L_0x0a13:
            r1 = r2
            r2 = r29
            r11 = 0
            goto L_0x0046
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.s.a(com.jaunt.c, java.lang.String):void");
    }

    private static boolean a(String str, char c2, c cVar) throws IOException {
        String upperCase = str.toUpperCase();
        String lowerCase = str.toLowerCase();
        if (upperCase.charAt(0) != c2 && lowerCase.charAt(0) != c2) {
            return false;
        }
        int length = str.length();
        cVar.a(length + 1);
        int i = 1;
        while (i < length) {
            int c3 = cVar.c();
            if (c3 == -1) {
                cVar.b();
                return false;
            }
            char c4 = (char) c3;
            if (upperCase.charAt(i) == c4 || lowerCase.charAt(i) == c4) {
                i++;
            } else {
                cVar.b();
                return false;
            }
        }
        cVar.b();
        return true;
    }
}
