package com.jaunt;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Stack;

final class m {

    /* renamed from: a  reason: collision with root package name */
    private r f4939a;
    private StringBuilder b = new StringBuilder();
    private Stack<a> c;

    enum a {
        LIST,
        MEMBER,
        OUTER
    }

    private static boolean a(char c2) {
        return c2 == ' ' || c2 == 10 || c2 == 13 || c2 == 9;
    }

    private void c() {
        this.c.push(a.MEMBER);
    }

    private void d() {
        this.c.push(a.LIST);
    }

    private void e() {
        this.c.pop();
    }

    private a f() {
        if (this.c.size() > 0) {
            return this.c.peek();
        }
        return a.OUTER;
    }

    public final void a(r rVar) {
        this.f4939a = rVar;
    }

    public final String b() {
        return this.b.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:289:0x0534  */
    /* JADX WARNING: Removed duplicated region for block: B:307:0x0028 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(java.io.BufferedReader r23) throws java.io.IOException {
        /*
            r22 = this;
            r0 = r22
            com.jaunt.r r1 = r0.f4939a
            r1.a()
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r0.b = r1
            java.util.Stack r1 = new java.util.Stack
            r1.<init>()
            r0.c = r1
            r3 = 1
            r4 = 0
            r5 = 0
            r6 = 0
            r7 = 0
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 0
            r12 = 0
            r13 = 0
            r14 = 0
            r15 = 0
            r16 = 0
            r17 = 0
            r18 = 0
        L_0x0028:
            int r1 = r23.read()
            r2 = -1
            if (r1 == r2) goto L_0x0537
            char r1 = (char) r1
            java.lang.StringBuilder r2 = r0.b
            r2.append(r1)
            if (r4 != 0) goto L_0x003d
            r2 = 92
            if (r1 != r2) goto L_0x003d
            r4 = 1
            goto L_0x0028
        L_0x003d:
            r2 = 34
            if (r8 == 0) goto L_0x00c3
            if (r9 == 0) goto L_0x0064
            if (r1 != r2) goto L_0x005b
            if (r4 != 0) goto L_0x0051
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            r8 = 1
            r9 = 0
        L_0x004e:
            r16 = 1
            goto L_0x0074
        L_0x0051:
            com.jaunt.r r2 = r0.f4939a
            r21 = r8
            r8 = 0
            r2.a(r1, r8)
            goto L_0x03bd
        L_0x005b:
            r21 = r8
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x0064:
            r21 = r8
            if (r11 == 0) goto L_0x00a4
            r2 = 44
            if (r1 != r2) goto L_0x0078
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            r8 = 1
            r11 = 0
            r14 = 1
        L_0x0074:
            r21 = 0
            goto L_0x0530
        L_0x0078:
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 != r2) goto L_0x008c
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            r8 = 1
            r11 = 0
            goto L_0x0074
        L_0x008c:
            r2 = 13
            if (r1 == r2) goto L_0x009c
            r2 = 10
            if (r1 != r2) goto L_0x0095
            goto L_0x009c
        L_0x0095:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x009c:
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            r8 = 1
            r11 = 0
            goto L_0x004e
        L_0x00a4:
            if (r13 == 0) goto L_0x03bd
            r2 = 39
            if (r1 != r2) goto L_0x00bc
            if (r4 != 0) goto L_0x00b4
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            r8 = 1
            r13 = 0
            goto L_0x004e
        L_0x00b4:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.a(r1, r8)
            goto L_0x03bd
        L_0x00bc:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x00c3:
            r21 = r8
            r8 = 58
            if (r7 == 0) goto L_0x0152
            if (r9 == 0) goto L_0x00e9
            if (r1 != r2) goto L_0x00e2
            if (r4 != 0) goto L_0x00da
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            r7 = 0
            r8 = 1
            r9 = 0
            r10 = 1
            goto L_0x0530
        L_0x00da:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.a(r1, r8)
            goto L_0x03bd
        L_0x00e2:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x00e9:
            if (r11 == 0) goto L_0x0130
            if (r1 != r8) goto L_0x00f6
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            r7 = 0
            r8 = 1
            r11 = 0
            goto L_0x015f
        L_0x00f6:
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 != r2) goto L_0x0117
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            com.jaunt.r r1 = r0.f4939a
            r2 = 0
            r1.b(r2)
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            r7 = 0
            r8 = 1
        L_0x0114:
            r11 = 0
            goto L_0x0530
        L_0x0117:
            r2 = 13
            if (r1 == r2) goto L_0x0127
            r2 = 10
            if (r1 != r2) goto L_0x0120
            goto L_0x0127
        L_0x0120:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x0127:
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            r7 = 0
            r8 = 1
            r10 = 1
            goto L_0x0114
        L_0x0130:
            if (r13 == 0) goto L_0x03bd
            r2 = 39
            if (r1 != r2) goto L_0x014b
            if (r4 != 0) goto L_0x0143
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            r7 = 0
            r8 = 1
            r10 = 1
            r13 = 0
            goto L_0x0530
        L_0x0143:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.a(r1, r8)
            goto L_0x03bd
        L_0x014b:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x0152:
            r2 = 2
            if (r10 == 0) goto L_0x01d7
            boolean r19 = a((char) r1)
            if (r19 != 0) goto L_0x03bd
            if (r1 != r8) goto L_0x0162
            r8 = 1
            r10 = 0
        L_0x015f:
            r12 = 1
            goto L_0x0530
        L_0x0162:
            r8 = 34
            if (r1 != r8) goto L_0x0170
            com.jaunt.r r1 = r0.f4939a
            r1.b(r2)
            r8 = 1
            r9 = 1
            r10 = 0
            goto L_0x01eb
        L_0x0170:
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 != r2) goto L_0x0181
            com.jaunt.r r1 = r0.f4939a
            r1.b()
            r22.c()
            r5 = 1
        L_0x017d:
            r8 = 1
            r10 = 0
            goto L_0x0530
        L_0x0181:
            r2 = 91
            if (r1 != r2) goto L_0x018f
            com.jaunt.r r1 = r0.f4939a
            r1.e()
            r22.d()
            r6 = 1
            goto L_0x017d
        L_0x018f:
            r2 = 44
            if (r1 != r2) goto L_0x01a2
            com.jaunt.r r1 = r0.f4939a
            r2 = 0
            r1.b(r2)
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            r8 = 1
            r10 = 0
            goto L_0x021f
        L_0x01a2:
            r2 = 0
            r8 = 125(0x7d, float:1.75E-43)
            if (r1 != r8) goto L_0x01ba
            com.jaunt.r r1 = r0.f4939a
            r1.b(r2)
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            goto L_0x017d
        L_0x01ba:
            r2 = 39
            if (r1 != r2) goto L_0x01c8
            com.jaunt.r r1 = r0.f4939a
            r2 = 1
            r1.b(r2)
            r8 = 1
            r10 = 0
            goto L_0x0249
        L_0x01c8:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.b(r8)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r8 = 1
            r10 = 0
            r11 = 1
            goto L_0x01eb
        L_0x01d7:
            if (r12 == 0) goto L_0x0259
            boolean r8 = a((char) r1)
            if (r8 != 0) goto L_0x03bd
            r8 = 34
            if (r1 != r8) goto L_0x01ef
            com.jaunt.r r1 = r0.f4939a
            r1.b(r2)
            r8 = 1
            r9 = 1
        L_0x01ea:
            r12 = 0
        L_0x01eb:
            r21 = 1
            goto L_0x0530
        L_0x01ef:
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 != r2) goto L_0x0200
            com.jaunt.r r1 = r0.f4939a
            r1.b()
            r22.c()
            r5 = 1
        L_0x01fc:
            r8 = 1
            r12 = 0
            goto L_0x0530
        L_0x0200:
            r2 = 91
            if (r1 != r2) goto L_0x020e
            com.jaunt.r r1 = r0.f4939a
            r1.e()
            r22.d()
            r6 = 1
            goto L_0x01fc
        L_0x020e:
            r2 = 44
            if (r1 != r2) goto L_0x0222
            com.jaunt.r r1 = r0.f4939a
            r2 = 0
            r1.b(r2)
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            r8 = 1
            r12 = 0
        L_0x021f:
            r14 = 1
            goto L_0x0530
        L_0x0222:
            r2 = 0
            r8 = 125(0x7d, float:1.75E-43)
            if (r1 != r8) goto L_0x023d
            com.jaunt.r r1 = r0.f4939a
            r1.b(r2)
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            r8 = 1
            r12 = 0
            goto L_0x0074
        L_0x023d:
            r2 = 39
            if (r1 != r2) goto L_0x024b
            com.jaunt.r r1 = r0.f4939a
            r2 = 1
            r1.b(r2)
            r8 = 1
            r12 = 0
        L_0x0249:
            r13 = 1
            goto L_0x01eb
        L_0x024b:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.b(r8)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r8 = 1
            r11 = 1
            goto L_0x01ea
        L_0x0259:
            if (r5 != 0) goto L_0x04da
            if (r14 == 0) goto L_0x025f
            goto L_0x04da
        L_0x025f:
            r8 = 93
            if (r15 == 0) goto L_0x02e3
            if (r9 == 0) goto L_0x0286
            r2 = 34
            if (r1 != r2) goto L_0x027f
            if (r4 != 0) goto L_0x0277
            com.jaunt.r r1 = r0.f4939a
            r1.h()
            r8 = 1
            r9 = 0
        L_0x0272:
            r15 = 0
            r18 = 1
            goto L_0x0530
        L_0x0277:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.a(r1, r8)
            goto L_0x03bd
        L_0x027f:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x0286:
            if (r11 == 0) goto L_0x02c4
            r2 = 44
            if (r1 != r2) goto L_0x0298
            com.jaunt.r r1 = r0.f4939a
            r1.h()
            r8 = 1
            r11 = 0
            r15 = 0
        L_0x0294:
            r17 = 1
            goto L_0x0530
        L_0x0298:
            if (r1 != r8) goto L_0x02ac
            com.jaunt.r r1 = r0.f4939a
            r1.h()
            com.jaunt.r r1 = r0.f4939a
            r1.f()
            r22.e()
            r8 = 1
            r11 = 0
            r15 = 0
            goto L_0x0530
        L_0x02ac:
            r2 = 13
            if (r1 == r2) goto L_0x02bc
            r2 = 10
            if (r1 != r2) goto L_0x02b5
            goto L_0x02bc
        L_0x02b5:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x02bc:
            com.jaunt.r r1 = r0.f4939a
            r1.h()
            r8 = 1
            r11 = 0
            goto L_0x0272
        L_0x02c4:
            if (r13 == 0) goto L_0x03bd
            r2 = 39
            if (r1 != r2) goto L_0x02dc
            if (r4 != 0) goto L_0x02d4
            com.jaunt.r r1 = r0.f4939a
            r1.h()
            r8 = 1
            r13 = 0
            goto L_0x0272
        L_0x02d4:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.a(r1, r8)
            goto L_0x03bd
        L_0x02dc:
            com.jaunt.r r2 = r0.f4939a
            r2.a(r1, r4)
            goto L_0x03bd
        L_0x02e3:
            if (r16 == 0) goto L_0x033f
            boolean r8 = a((char) r1)
            if (r8 != 0) goto L_0x03bd
            r8 = 44
            if (r1 != r8) goto L_0x02f5
            r8 = 1
            r14 = 1
        L_0x02f1:
            r16 = 0
            goto L_0x0530
        L_0x02f5:
            r8 = 125(0x7d, float:1.75E-43)
            if (r1 != r8) goto L_0x0303
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            r8 = 1
            goto L_0x02f1
        L_0x0303:
            r8 = 34
            if (r1 != r8) goto L_0x0310
            com.jaunt.r r1 = r0.f4939a
            r1.a((int) r2)
            r7 = 1
            r8 = 1
            r9 = 1
            goto L_0x02f1
        L_0x0310:
            r2 = 58
            if (r1 != r2) goto L_0x0322
            com.jaunt.r r1 = r0.f4939a
            r2 = 0
            r1.a((int) r2)
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            r8 = 1
            r12 = 1
            goto L_0x02f1
        L_0x0322:
            r2 = 0
            r7 = 39
            if (r1 != r7) goto L_0x0331
            com.jaunt.r r1 = r0.f4939a
            r7 = 1
            r1.a((int) r7)
            r7 = 1
            r8 = 1
            r13 = 1
            goto L_0x02f1
        L_0x0331:
            com.jaunt.r r7 = r0.f4939a
            r7.a((int) r2)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r7 = 1
            r8 = 1
            r11 = 1
            goto L_0x02f1
        L_0x033f:
            if (r6 != 0) goto L_0x0476
            if (r17 == 0) goto L_0x0345
            goto L_0x0476
        L_0x0345:
            if (r18 == 0) goto L_0x03ad
            boolean r19 = a((char) r1)
            if (r19 != 0) goto L_0x03bd
            r2 = 44
            if (r1 != r2) goto L_0x0358
            r8 = 1
            r17 = 1
        L_0x0354:
            r18 = 0
            goto L_0x0530
        L_0x0358:
            if (r1 != r8) goto L_0x0364
            com.jaunt.r r1 = r0.f4939a
            r1.f()
            r22.e()
        L_0x0362:
            r8 = 1
            goto L_0x0354
        L_0x0364:
            r2 = 34
            if (r1 != r2) goto L_0x0372
            com.jaunt.r r1 = r0.f4939a
            r2 = 2
            r1.c(r2)
            r8 = 1
            r9 = 1
        L_0x0370:
            r15 = 1
            goto L_0x0354
        L_0x0372:
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 != r2) goto L_0x0380
            com.jaunt.r r1 = r0.f4939a
            r1.b()
            r22.c()
            r5 = 1
            goto L_0x0362
        L_0x0380:
            r2 = 91
            if (r1 != r2) goto L_0x038e
            com.jaunt.r r1 = r0.f4939a
            r1.e()
            r22.d()
            r6 = 1
            goto L_0x0362
        L_0x038e:
            r2 = 39
            if (r1 != r2) goto L_0x039f
            com.jaunt.r r1 = r0.f4939a
            r2 = 1
            r1.c(r2)
            r6 = 0
            r8 = 1
            r13 = 1
            r15 = 1
            r17 = 0
            goto L_0x0354
        L_0x039f:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.c(r8)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r8 = 1
            r11 = 1
            goto L_0x0370
        L_0x03ad:
            if (r3 == 0) goto L_0x03c0
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 != r2) goto L_0x03bd
            com.jaunt.r r1 = r0.f4939a
            r1.b()
            r22.c()
            r3 = 0
        L_0x03bc:
            r5 = 1
        L_0x03bd:
            r8 = 1
            goto L_0x0530
        L_0x03c0:
            com.jaunt.m$a r2 = r22.f()
            boolean r20 = a((char) r1)
            if (r20 != 0) goto L_0x03bd
            com.jaunt.m$a r8 = com.jaunt.m.a.MEMBER
            if (r2 != r8) goto L_0x0410
            r8 = 44
            if (r1 != r8) goto L_0x03d5
            r8 = 1
            goto L_0x021f
        L_0x03d5:
            r2 = 34
            if (r1 != r2) goto L_0x03e4
            com.jaunt.r r1 = r0.f4939a
            r2 = 2
            r1.a((int) r2)
            r7 = 1
            r8 = 1
            r9 = 1
            goto L_0x0530
        L_0x03e4:
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 != r2) goto L_0x03f1
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            goto L_0x03bd
        L_0x03f1:
            r2 = 39
            if (r1 != r2) goto L_0x0400
            com.jaunt.r r1 = r0.f4939a
            r2 = 1
            r1.a((int) r2)
            r7 = 1
            r8 = 1
            r13 = 1
            goto L_0x0530
        L_0x0400:
            com.jaunt.r r2 = r0.f4939a
            r7 = 0
            r2.a((int) r7)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r7 = 1
            r8 = 1
            r11 = 1
            goto L_0x0530
        L_0x0410:
            com.jaunt.m$a r8 = com.jaunt.m.a.LIST
            if (r2 != r8) goto L_0x046f
            r8 = 44
            if (r1 != r8) goto L_0x041b
            r8 = 1
            goto L_0x0294
        L_0x041b:
            r2 = 34
            if (r1 != r2) goto L_0x042a
            com.jaunt.r r1 = r0.f4939a
            r2 = 2
            r1.c(r2)
            r8 = 1
            r9 = 1
        L_0x0427:
            r15 = 1
            goto L_0x0530
        L_0x042a:
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 != r2) goto L_0x0437
            com.jaunt.r r1 = r0.f4939a
            r1.b()
            r22.c()
            goto L_0x03bc
        L_0x0437:
            r2 = 91
            if (r1 != r2) goto L_0x0446
            com.jaunt.r r1 = r0.f4939a
            r1.e()
            r22.d()
            r6 = 1
            goto L_0x03bd
        L_0x0446:
            r2 = 93
            if (r1 != r2) goto L_0x0454
            com.jaunt.r r1 = r0.f4939a
            r1.f()
            r22.e()
            goto L_0x03bd
        L_0x0454:
            r2 = 39
            if (r1 != r2) goto L_0x0461
            com.jaunt.r r1 = r0.f4939a
            r2 = 1
            r1.c(r2)
            r8 = 1
            r13 = 1
            goto L_0x0427
        L_0x0461:
            com.jaunt.r r2 = r0.f4939a
            r8 = 0
            r2.c(r8)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r8 = 1
            r11 = 1
            goto L_0x0427
        L_0x046f:
            com.jaunt.m$a r1 = com.jaunt.m.a.OUTER
            if (r2 != r1) goto L_0x03bd
            r3 = 1
            goto L_0x03bd
        L_0x0476:
            boolean r2 = a((char) r1)
            if (r2 != 0) goto L_0x03bd
            r2 = 34
            if (r1 != r2) goto L_0x048e
            com.jaunt.r r1 = r0.f4939a
            r2 = 2
            r1.c(r2)
            r6 = 0
            r8 = 1
            r9 = 1
        L_0x0489:
            r15 = 1
        L_0x048a:
            r17 = 0
            goto L_0x0530
        L_0x048e:
            r2 = 123(0x7b, float:1.72E-43)
            if (r1 != r2) goto L_0x049e
            com.jaunt.r r1 = r0.f4939a
            r1.b()
            r22.c()
            r5 = 1
        L_0x049b:
            r6 = 0
        L_0x049c:
            r8 = 1
            goto L_0x048a
        L_0x049e:
            r2 = 91
            if (r1 != r2) goto L_0x04ac
            com.jaunt.r r1 = r0.f4939a
            r1.e()
            r22.d()
            r6 = 1
            goto L_0x049c
        L_0x04ac:
            r2 = 93
            if (r1 != r2) goto L_0x04b9
            com.jaunt.r r1 = r0.f4939a
            r1.f()
            r22.e()
            goto L_0x049b
        L_0x04b9:
            r2 = 44
            if (r1 == r2) goto L_0x03bd
            r2 = 39
            if (r1 != r2) goto L_0x04cb
            com.jaunt.r r1 = r0.f4939a
            r2 = 1
            r1.c(r2)
            r6 = 0
            r8 = 1
            r13 = 1
            goto L_0x0489
        L_0x04cb:
            com.jaunt.r r2 = r0.f4939a
            r6 = 0
            r2.c(r6)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r6 = 0
            r8 = 1
            r11 = 1
            goto L_0x0489
        L_0x04da:
            boolean r2 = a((char) r1)
            if (r2 != 0) goto L_0x03bd
            r2 = 34
            if (r1 != r2) goto L_0x04f0
            com.jaunt.r r1 = r0.f4939a
            r2 = 2
            r1.a((int) r2)
            r5 = 0
            r7 = 1
            r8 = 1
            r9 = 1
        L_0x04ee:
            r14 = 0
            goto L_0x0530
        L_0x04f0:
            r2 = 125(0x7d, float:1.75E-43)
            if (r1 != r2) goto L_0x04ff
            com.jaunt.r r1 = r0.f4939a
            r1.c()
            r22.e()
            r5 = 0
            r8 = 1
            goto L_0x04ee
        L_0x04ff:
            r2 = 58
            if (r1 != r2) goto L_0x0512
            com.jaunt.r r1 = r0.f4939a
            r2 = 0
            r1.a((int) r2)
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            r5 = 0
            r8 = 1
            r12 = 1
            goto L_0x04ee
        L_0x0512:
            r2 = 0
            r5 = 39
            if (r1 != r5) goto L_0x0521
            com.jaunt.r r1 = r0.f4939a
            r8 = 1
            r1.a((int) r8)
            r5 = 0
            r7 = 1
            r13 = 1
            goto L_0x04ee
        L_0x0521:
            r8 = 1
            com.jaunt.r r5 = r0.f4939a
            r5.a((int) r2)
            com.jaunt.r r2 = r0.f4939a
            r2.a((char) r1)
            r5 = 0
            r7 = 1
            r11 = 1
            goto L_0x04ee
        L_0x0530:
            r8 = r21
            if (r4 == 0) goto L_0x0028
            r4 = 0
            goto L_0x0028
        L_0x0537:
            r21 = r8
            r23.close()
            if (r3 != 0) goto L_0x0569
            if (r5 != 0) goto L_0x0569
            if (r6 != 0) goto L_0x0569
            if (r7 != 0) goto L_0x0559
            if (r10 != 0) goto L_0x0559
            if (r12 == 0) goto L_0x0549
            goto L_0x0559
        L_0x0549:
            if (r21 == 0) goto L_0x0551
            com.jaunt.r r1 = r0.f4939a
            r1.g()
            goto L_0x0569
        L_0x0551:
            if (r15 == 0) goto L_0x0569
            com.jaunt.r r1 = r0.f4939a
            r1.h()
            goto L_0x0569
        L_0x0559:
            com.jaunt.r r1 = r0.f4939a
            r1.d()
            com.jaunt.r r1 = r0.f4939a
            r2 = 0
            r1.b(r2)
            com.jaunt.r r1 = r0.f4939a
            r1.g()
        L_0x0569:
            com.jaunt.r r1 = r0.f4939a
            r1.i()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.m.b(java.io.BufferedReader):void");
    }

    public final r a() {
        return this.f4939a;
    }

    public final void a(BufferedReader bufferedReader) throws IOException {
        b(bufferedReader);
    }
}
