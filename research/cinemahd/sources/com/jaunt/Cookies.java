package com.jaunt;

import com.jaunt.util.MultiMap;
import java.util.List;

public class Cookies {

    /* renamed from: a  reason: collision with root package name */
    private MultiMap<String, Cookie> f4921a;
    private UserAgentSettings b;

    Cookies(UserAgentSettings userAgentSettings) {
        this.b = userAgentSettings;
        this.f4921a = new MultiMap<>(10, userAgentSettings.p);
    }

    public List<Cookie> a(String str) {
        return this.f4921a.a(str.toLowerCase());
    }

    public String toString() {
        return this.f4921a.toString();
    }

    public void a(Cookie cookie) {
        String lowerCase = cookie.b().toLowerCase();
        List<Cookie> a2 = a(lowerCase);
        if (a2 != null) {
            if (a2.size() > this.b.q) {
                a2.remove(0);
            }
            int size = a2.size() - 1;
            while (true) {
                if (size >= 0) {
                    Cookie cookie2 = a2.get(size);
                    if (cookie2.c().equals(cookie.c()) && cookie2.d().equals(cookie.d())) {
                        a2.remove(size);
                        break;
                    }
                    size--;
                } else {
                    break;
                }
            }
        }
        this.f4921a.a(lowerCase, cookie);
    }

    public List<Cookie> a() {
        return this.f4921a.b();
    }
}
