package com.jaunt;

import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import java.util.LinkedHashMap;
import java.util.Map;

public class UserAgentSettings {

    /* renamed from: a  reason: collision with root package name */
    public String f4931a = "UTF-8";
    public boolean b = true;
    public boolean c = false;
    public boolean d = false;
    public boolean e = false;
    public boolean f = false;
    public boolean g = true;
    public boolean h = false;
    public boolean i = false;
    public boolean j = false;
    public String k = System.getProperty("user.dir");
    String l = "LAST_VISITED";
    public int m = 5;
    public int n = -1;
    public Map<String, String> o = new LinkedHashMap();
    int p = 100;
    int q = 50;
    int r = 20000;
    int s = HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT;
    public long t = 0;

    UserAgentSettings() {
        this.o.put("accept", "*/*");
        this.o.put("accept-language", "en-us");
        this.o.put("accept-encoding", "gzip");
        this.o.put("connection", "Keep-Alive");
        this.o.put("user-agent", "Jaunt/1.5");
    }

    private static String a(Map<String, String> map) {
        StringBuilder sb = new StringBuilder();
        for (String next : map.keySet()) {
            sb.append("  " + next + ": " + map.get(next) + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        return sb.toString();
    }

    public String toString() {
        return "showWarnings: " + this.c + "\nshowHeaders: " + this.d + "\nshowTravel: " + this.e + "\ngenericXMLMode: " + this.f + "\ncharset: " + this.f4931a + "\noutputPath: " + this.k + "\nautoRedirect: " + this.b + "\nmaxRedirects: " + this.m + "\nmaxBytes: " + this.n + "\nresponseTimout: " + this.t + "\ncheckSSLCerts: " + this.g + "\nautoSaveAsHTML: " + this.h + "\nautoSaveAsXML: " + this.i + "\nautoSaveJSON: " + this.j + "\ndefaultRequestHeaders:\n" + a(this.o);
    }
}
