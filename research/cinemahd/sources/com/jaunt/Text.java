package com.jaunt;

public class Text extends Node {
    private String c;

    public Text(String str) {
        super(3);
        this.c = str;
    }

    /* access modifiers changed from: package-private */
    public String d() {
        return toString();
    }

    public String e() {
        return this.c;
    }

    public String toString() {
        return e();
    }
}
