package com.jaunt.component;

import com.jaunt.Document;
import com.jaunt.Element;
import com.jaunt.ResponseException;
import com.jaunt.UserAgent;

public class Meta {

    /* renamed from: a  reason: collision with root package name */
    private String f4934a = null;
    private boolean b = false;
    public Element c;

    public Meta(Element element) {
        int i;
        this.c = element;
        if (this.c.d("http-equiv").equalsIgnoreCase("refresh")) {
            String d = this.c.d("content");
            int indexOf = d.indexOf(59);
            int indexOf2 = d.indexOf(61);
            if (indexOf != -1 && indexOf2 != -1 && d.length() > (i = indexOf2 + 1)) {
                this.b = true;
                String trim = d.substring(i).trim();
                boolean z = trim.startsWith("'") && trim.endsWith("'");
                boolean z2 = trim.startsWith("\"") && trim.endsWith("\"");
                if (z || z2) {
                    this.f4934a = trim.substring(1, trim.length() - 1);
                } else {
                    this.f4934a = trim;
                }
                try {
                    d.substring(0, indexOf).trim();
                } catch (Exception unused) {
                }
            }
        }
    }

    public boolean a() {
        return this.b;
    }

    public Document a(UserAgent userAgent, int i) throws ResponseException {
        if (!this.b) {
            return null;
        }
        return userAgent.a(this.f4934a, i + 1);
    }
}
