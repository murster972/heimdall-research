package com.jaunt;

public abstract class Node {

    /* renamed from: a  reason: collision with root package name */
    private short f4924a;
    private Element b = null;

    Node(short s) {
        this.f4924a = s;
    }

    /* access modifiers changed from: package-private */
    public void a() {
        this.b = null;
    }

    public Element b() {
        return this.b;
    }

    public short c() {
        return this.f4924a;
    }

    /* access modifiers changed from: package-private */
    public abstract String d();

    static boolean a(Node node) {
        return (node.c() == 1 || node.c() == 9) && ((Element) node).e() != 2;
    }

    /* access modifiers changed from: package-private */
    public final void a(Element element) {
        this.b = element;
    }
}
