package com.jaunt;

import com.facebook.common.util.UriUtil;
import com.jaunt.util.IOUtil;

final class p {

    /* renamed from: a  reason: collision with root package name */
    private d f4941a;
    private String b = null;
    private StringBuilder c;
    private String d;
    private int e;
    private StringBuilder f;
    private String g;
    private StringBuilder h;
    private int i;
    private StringBuilder j;
    private StringBuilder k;
    private String[] l;
    private Element m;
    private Element n;
    private Text o;
    private Comment p;
    private StringBuilder q;
    private h r = null;

    public p(h hVar) {
        this.r = hVar;
        this.f4941a = new d(hVar);
        c();
    }

    private void z() {
        this.j.setLength(0);
    }

    public final void a() {
        this.f4941a.c();
    }

    public final d b() {
        return this.f4941a;
    }

    public final void c() {
        this.f4941a.d();
        this.c = new StringBuilder();
        this.d = null;
        this.f = new StringBuilder();
        this.g = null;
        this.h = new StringBuilder();
        this.j = new StringBuilder();
        this.k = new StringBuilder();
        this.l = null;
        this.m = null;
        this.n = new Element("#BOGUS", 1);
        this.o = null;
        this.p = null;
        this.q = null;
    }

    public final void d() {
        this.e = 0;
    }

    public final void e() {
        this.e = 2;
    }

    public final void f() {
        this.q = this.c;
    }

    public final void g() {
        this.d = this.c.toString();
        int i2 = this.e;
        if (i2 == 2) {
            this.m = this.n;
            this.f4941a.a(this.d);
        } else if (i2 == 0) {
            this.m = new Element(this.d, 1);
        } else {
            IOUtil.a("NodeBuilder.endOfElementName; invalid tag type; tagType: " + this.e);
        }
    }

    public final void h() {
        this.q = this.f;
    }

    public final void i() {
        this.g = this.f.toString();
        while (this.g.startsWith("/") && this.g.length() > 1) {
            this.g = this.g.substring(1);
        }
        this.m.b(this.g);
        this.f.setLength(0);
    }

    public final void j() {
        this.i = 1;
        this.q = this.h;
    }

    public final void k() {
        this.i = 2;
        this.q = this.h;
    }

    public final void l() {
        this.i = 3;
        this.q = this.h;
    }

    public final void m() {
        String sb = this.h.toString();
        int i2 = this.i;
        if (i2 == 1) {
            this.m.a(sb, 1);
        } else if (i2 == 2) {
            this.m.a(sb, 2);
        } else if (i2 == 3) {
            this.m.a(sb, 3);
        }
        this.h.setLength(0);
    }

    public final void n() {
        this.q = this.k;
    }

    public final void o() {
        String sb = this.k.toString();
        if (sb.length() > 0) {
            this.o = new Text(sb);
            this.f4941a.a(this.o);
            this.k.setLength(0);
        }
    }

    public final void p() {
        this.q = this.j;
    }

    public final void q() {
        this.p = new Comment(this.j.toString(), 8);
        this.f4941a.a(this.p);
        z();
    }

    public final void r() {
        this.q = this.j;
    }

    public final void s() {
        this.p = new Comment(this.j.toString(), 0);
        this.f4941a.a(this.p);
        z();
    }

    public final void t() {
        this.q = this.j;
    }

    public final void u() {
        this.p = new Comment(this.j.toString().replaceAll("^\\s", ""), 10);
        this.f4941a.a(this.p);
        z();
    }

    public final void v() {
        this.q = this.j;
    }

    public final void w() {
        this.p = new Comment(this.j.toString(), 7);
        this.f4941a.a(this.p);
        z();
    }

    public final void x() {
        this.q = this.j;
    }

    public final void y() {
        this.p = new Comment(this.j.toString(), 4);
        this.f4941a.a(this.p);
        z();
    }

    public final void a(String str) {
        this.b = str;
    }

    public final void b(String str) {
        this.q.append(str);
    }

    public final void a(h hVar) {
        this.r = hVar;
    }

    public final void b(int i2) {
        this.f4941a.a(i2);
    }

    public final void a(short s) {
        if (s == 1) {
            this.f4941a.h++;
        } else if (s == 2) {
            this.f4941a.i++;
        }
    }

    public final void a(char c2) {
        this.q.append(c2);
    }

    public final void a(int i2) {
        boolean z;
        String str;
        boolean z2;
        int i3;
        if (this.e == 0) {
            if (i2 == 3) {
                this.m.a(3);
            }
            if (this.r != null) {
                z = this.d.equalsIgnoreCase("meta");
                if (this.b != null) {
                    if (this.d.equalsIgnoreCase("base")) {
                        String d2 = this.m.d("href");
                        if (d2.toLowerCase().startsWith(UriUtil.HTTP_SCHEME)) {
                            this.b = d2;
                        }
                    } else {
                        this.l = this.r.a(this.m, 1);
                        String[] strArr = this.l;
                        if (strArr != null) {
                            String str2 = "0";
                            for (String str3 : strArr) {
                                a a2 = this.m.a(str3);
                                if (a2 != null) {
                                    String f2 = a2.f();
                                    if (!str3.equals("content") || f2 == null) {
                                        str = str2;
                                        z2 = false;
                                    } else {
                                        int indexOf = f2.indexOf(59);
                                        int indexOf2 = f2.indexOf(61);
                                        if (!(indexOf == -1 || indexOf2 == -1 || f2.length() <= (i3 = indexOf2 + 1))) {
                                            String trim = f2.substring(0, indexOf).trim();
                                            f2 = f2.substring(i3).trim();
                                            boolean z3 = f2.startsWith("'") && f2.endsWith("'");
                                            boolean z4 = f2.startsWith("\"") && f2.endsWith("\"");
                                            if (z3 || z4) {
                                                f2 = f2.substring(1, f2.length() - 1);
                                            }
                                            str = trim;
                                            z2 = true;
                                        }
                                    }
                                    try {
                                        String b2 = UserAgent.b(this.b, f2);
                                        if (z2) {
                                            b2 = String.valueOf(str) + ";url=" + b2;
                                        }
                                        a2.a(b2);
                                    } catch (Exception unused) {
                                    }
                                    str2 = str;
                                }
                            }
                        }
                    }
                }
            } else {
                z = false;
            }
            this.f4941a.a(this.m, z ? (short) 1 : 0);
        }
        this.c.setLength(0);
    }
}
