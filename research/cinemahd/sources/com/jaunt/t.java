package com.jaunt;

final class t {

    /* renamed from: a  reason: collision with root package name */
    private String f4944a;
    private String b = null;

    public t(String str, String str2) {
        this.f4944a = str;
        if (str2 != null) {
            this.b = str2;
        }
    }

    public final String a() {
        return this.b;
    }

    public final boolean equals(Object obj) {
        boolean z;
        try {
            t tVar = (t) obj;
            String str = tVar.b;
            boolean equals = tVar.f4944a.equals(this.f4944a);
            if (str != null) {
                if (this.b != null) {
                    if (!str.toLowerCase().startsWith(this.b.toLowerCase()) && !this.b.toLowerCase().startsWith(str.toLowerCase())) {
                        z = false;
                        if (equals || !z) {
                            return false;
                        }
                        return true;
                    }
                }
            }
            z = true;
            if (equals) {
            }
        } catch (Exception unused) {
        }
        return false;
    }

    public final int hashCode() {
        return this.f4944a.hashCode();
    }

    public final String toString() {
        return "name: " + this.f4944a + "\nprotectedDir: " + this.b;
    }

    public final void a(String str) {
        this.b = str;
    }
}
