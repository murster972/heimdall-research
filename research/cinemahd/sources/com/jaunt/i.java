package com.jaunt;

import java.util.HashSet;
import java.util.Set;

final class i {
    static short c = 1;
    static short d = 2;
    static short e = 3;

    /* renamed from: a  reason: collision with root package name */
    private short f4937a;
    private Set<String> b;

    public i(short s) {
        this.b = null;
        this.f4937a = s;
        this.b = null;
    }

    public final short a() {
        return this.f4937a;
    }

    public final boolean a(String str) {
        Set<String> set = this.b;
        if (set == null) {
            return false;
        }
        return set.contains(str.toUpperCase());
    }

    public i(short s, Set<String> set) {
        this(s, set, (String) null);
    }

    public i(short s, Set<String> set, String str) {
        this.b = null;
        this.f4937a = s;
        this.b = set;
        if (str != null) {
            this.b.remove(str);
        }
    }

    public i(short s, String str) {
        this.b = null;
        this.f4937a = s;
        this.b = new HashSet(1);
        this.b.add(str);
    }

    public i(short s, String[] strArr) {
        this.b = null;
        this.f4937a = s;
        this.b = new HashSet();
        for (String add : strArr) {
            this.b.add(add);
        }
    }
}
