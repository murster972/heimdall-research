package com.jaunt;

import com.jaunt.util.FilterCallback;
import com.jaunt.util.IOUtil;
import java.util.ArrayList;
import java.util.List;

final class d {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4935a = false;
    private Element b;
    private List<Element> c;
    private Element d;
    private h e;
    private int f;
    private FilterCallback g = null;
    int h = 0;
    int i = 0;

    public d(h hVar) {
        this.e = hVar;
        d();
    }

    public final void a(h hVar) {
        this.e = hVar;
    }

    public final boolean b() {
        return this.f4935a;
    }

    public final void c() {
        this.f4935a = true;
    }

    public final void d() {
        this.b = new Element("#root", 1);
        this.c = new ArrayList();
        this.d = this.b;
    }

    public final int e() {
        return this.f;
    }

    public final Element f() {
        return this.b;
    }

    public final List<Element> g() {
        return this.c;
    }

    public final short a() {
        return this.h > this.i ? (short) 1 : 2;
    }

    public final void a(Element element, short s) {
        FilterCallback filterCallback = this.g;
        if (filterCallback == null || (filterCallback != null && filterCallback.a(this.d, element))) {
            short e2 = element.e();
            h hVar = this.e;
            if (hVar != null) {
                if (hVar.a(element.f()) == i.c) {
                    if (e2 != 3) {
                        element.b(3);
                        this.i++;
                        e2 = 3;
                    }
                } else if (e2 == 3) {
                    element.b(1);
                    e2 = 1;
                }
            }
            if (e2 == 1) {
                h hVar2 = this.e;
                if (hVar2 == null || hVar2.a(this.d.f(), element.f())) {
                    this.d.b((Node) element);
                    this.d = element;
                    if (s == 1) {
                        this.c.add(element);
                        return;
                    }
                    return;
                }
                this.d = this.d.b();
                if (this.d != null) {
                    a(element, s);
                }
            } else if (e2 == 3) {
                h hVar3 = this.e;
                if (hVar3 == null || hVar3.a(this.d.f(), element.f())) {
                    this.d.b((Node) element);
                    if (s == 1) {
                        this.c.add(element);
                        return;
                    }
                    return;
                }
                this.d = this.d.b();
                if (this.d != null) {
                    a(element, s);
                }
            } else {
                IOUtil.a("DOMBuilder.add; unknown element type: " + element.e());
            }
        }
    }

    public final void a(String str) {
        String lowerCase = str.toLowerCase();
        if (this.d.f().equals(lowerCase)) {
            this.d = this.d.b();
            return;
        }
        Element a2 = a(this.d, lowerCase);
        if (a2 != null) {
            this.d = a2.b();
        }
    }

    private Element a(Element element, String str) {
        do {
            element = element.b();
            if (element == null) {
                return null;
            }
            if (element.f().equals("table") && !str.equals("table")) {
                return null;
            }
        } while (!element.f().equalsIgnoreCase(str));
        return element;
    }

    public final void a(Text text) {
        FilterCallback filterCallback = this.g;
        if (filterCallback == null || (filterCallback != null && filterCallback.a(this.d, text))) {
            this.d.b((Node) text);
        }
    }

    public final void a(Comment comment) {
        FilterCallback filterCallback = this.g;
        if (filterCallback == null || (filterCallback != null && filterCallback.a(this.d, comment))) {
            this.d.b((Node) comment);
        }
    }

    public final void a(int i2) {
        this.f = i2;
    }
}
