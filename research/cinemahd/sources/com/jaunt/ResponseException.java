package com.jaunt;

public class ResponseException extends JauntException {
    private static final long serialVersionUID = 761900087176554290L;

    /* renamed from: a  reason: collision with root package name */
    private HttpResponse f4925a;
    private String b;
    private String c;

    public ResponseException(String str, HttpResponse httpResponse, String str2) {
        this.b = str;
        this.f4925a = httpResponse;
        this.c = str2;
    }

    public String getMessage() {
        return this.b;
    }

    public String toString() {
        if (this.f4925a != null) {
            return "message: " + this.b + "\nrequestUrl: " + this.c + "\nresponse: \n" + this.f4925a;
        }
        return "message: " + this.b + "\nrequestUrl: " + this.c + "\nresponse: [none]";
    }
}
