package com.jaunt;

public class JauntException extends Exception {
    private static final long serialVersionUID = 5673515700413913056L;

    JauntException() {
    }

    public JauntException(String str) {
        super(str);
    }
}
