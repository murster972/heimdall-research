package com.jaunt;

public class ExpirationException extends RuntimeException {
    public ExpirationException(String str) {
        super(str);
    }
}
