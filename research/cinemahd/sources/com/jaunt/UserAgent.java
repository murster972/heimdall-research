package com.jaunt;

import com.facebook.ads.AudienceNetworkActivity;
import com.facebook.common.util.ByteConstants;
import com.jaunt.util.Cache;
import com.jaunt.util.Handler;
import com.jaunt.util.IOUtil;
import com.jaunt.util.MultiMap;
import com.moat.analytics.mobile.cha.BuildConfig;
import com.uwetrottmann.trakt5.TraktV2;
import com.vungle.warren.model.CookieDBAdapter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class UserAgent {
    private static final Pattern C = Pattern.compile("/\\.(?=(/|$))");
    private static final Pattern D = Pattern.compile("/\\.\\.(?=(/|$))");
    private static final Pattern E = Pattern.compile("(?<!/)/[^/]+/\\.\\.(?=(/|$))");
    private static final String F = ("===" + System.currentTimeMillis() + "===");
    private static final Pattern G = Pattern.compile("^(?i)([a-z](?:[a-z]|-)+):");
    private HostnameVerifier A = HttpsURLConnection.getDefaultHostnameVerifier();
    private HostnameVerifier B = new HostnameVerifier(this) {
        public final boolean verify(String str, SSLSession sSLSession) {
            return true;
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private Object[] f4926a;
    /* access modifiers changed from: private */
    public MultiMap<String, String> b = new MultiMap<>();
    /* access modifiers changed from: private */
    public int c;
    /* access modifiers changed from: private */
    public String d;
    private ArrayList<k> e;
    private k f = new k() {
        public final void a() {
            UserAgent.this.m = new StringBuilder();
        }

        public final void b(String str, String str2) {
            StringBuilder a2 = UserAgent.this.m;
            a2.append(String.valueOf(str) + ": " + str2 + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }

        public final void c() {
            if (UserAgent.this.h.d) {
                PrintStream printStream = System.out;
                printStream.println("Response Headers (status " + UserAgent.this.c + " " + UserAgent.this.d + "):\n" + UserAgent.this.l.toString());
            }
        }

        public final void a(int i, String str) {
            UserAgent.this.b = new MultiMap();
            UserAgent.this.l = new StringBuilder();
            UserAgent.this.c = i;
            UserAgent.this.d = str;
        }

        public final void b() {
            if (UserAgent.this.h.d) {
                PrintStream printStream = System.out;
                printStream.println("Request Headers:\n" + UserAgent.this.m.toString());
            }
        }

        public final void a(String str, String str2) {
            StringBuilder b = UserAgent.this.l;
            b.append(String.valueOf(str) + ": " + str2 + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            UserAgent.this.b.a(str.toLowerCase(), str2);
        }
    };
    public HttpResponse g = null;
    public UserAgentSettings h = new UserAgentSettings();
    public Document i = null;
    public JNode j = null;
    public Cookies k;
    /* access modifiers changed from: private */
    public StringBuilder l = new StringBuilder();
    /* access modifiers changed from: private */
    public StringBuilder m = new StringBuilder();
    private ArrayList<Object> n;
    private s o;
    private String p = null;
    private HashMap<t, b> q = null;
    private Map<String, Handler> r = new HashMap(1);
    private String s = null;
    private int t = 0;
    private Cache u;
    private boolean v = false;
    private final SimpleDateFormat w = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
    private boolean x = false;
    private TrustManager[] y = {new X509TrustManager(this) {
        public final void checkClientTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public final void checkServerTrusted(X509Certificate[] x509CertificateArr, String str) {
        }

        public final X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }};
    private SSLSocketFactory z = HttpsURLConnection.getDefaultSSLSocketFactory();

    enum a {
        POST,
        MULTIPART_POST,
        PUT
    }

    public UserAgent() throws ExpirationException {
        if (new Date().getTime() <= 1609488000000L) {
            this.k = new Cookies(this.h);
            this.o = new s(new p(new e()));
            this.p = null;
            this.q = new HashMap<>();
            this.n = new ArrayList<>();
            this.e = new ArrayList<>(1);
            this.e.add(this.f);
            j jVar = new j();
            a("text/json", (Handler) jVar);
            a(TraktV2.CONTENT_TYPE_JSON, (Handler) jVar);
            a("text/x-json", (Handler) jVar);
            return;
        }
        throw new ExpirationException("JAUNT HAS EXPIRED! [http://jaunt-api.com] \nversion:Jaunt 1.6.1 Redistributable Edition, Expiry Dec. 31, 2020");
    }

    private static String f(String str) {
        boolean z2;
        StringBuffer stringBuffer = null;
        String str2 = str;
        boolean z3 = true;
        while (z3) {
            Matcher matcher = E.matcher(str2);
            stringBuffer = new StringBuffer();
            if (matcher.find()) {
                matcher.appendReplacement(stringBuffer, "");
                z2 = true;
            } else {
                z2 = false;
            }
            matcher.appendTail(stringBuffer);
            boolean z4 = z2;
            str2 = stringBuffer.toString();
            z3 = z4;
        }
        return stringBuffer.toString();
    }

    private static String g(String str) {
        return C.matcher(str).replaceAll("");
    }

    private static String h(String str) {
        return D.matcher(str).replaceAll("");
    }

    private String i(String str) {
        StringBuilder sb = new StringBuilder();
        List<Cookie> a2 = this.k.a();
        try {
            URL url = new URL(str);
            boolean z2 = true;
            for (int i2 = 0; i2 < a2.size(); i2++) {
                Cookie cookie = a2.get(i2);
                boolean endsWith = url.getHost().toLowerCase().endsWith(cookie.b().toLowerCase());
                String d2 = cookie.d();
                String lowerCase = url.getPath().toLowerCase();
                if (lowerCase.equals("")) {
                    lowerCase = "/";
                }
                boolean startsWith = lowerCase.startsWith(d2.toLowerCase());
                boolean e2 = cookie.e();
                if (endsWith && startsWith && !e2) {
                    if (!z2) {
                        sb.append("; ");
                    }
                    sb.append(cookie.a());
                    z2 = false;
                }
            }
            if (sb.toString().equals("")) {
                return null;
            }
            return sb.toString();
        } catch (MalformedURLException unused) {
            return null;
        }
    }

    private static boolean j(String str) {
        String trim = str.toLowerCase().trim();
        return trim.equals("text/json") || trim.equals(TraktV2.CONTENT_TYPE_JSON) || trim.equals("text/x-json");
    }

    private static boolean c(String str) {
        String[] split = str.toLowerCase().split(";");
        if (split[0].startsWith(AudienceNetworkActivity.WEBVIEW_MIME_TYPE) || split[0].startsWith("text/xml") || split[0].startsWith("application/xml") || split[0].endsWith("+xml") || split[0].endsWith("+html")) {
            return true;
        }
        return false;
    }

    private static String d(String str) throws Exception {
        try {
            URL url = new URL(str);
            String protocol = url.getProtocol();
            String host = url.getHost();
            String path = url.getPath();
            int lastIndexOf = path.lastIndexOf(47);
            if (lastIndexOf != -1) {
                String substring = path.substring(0, lastIndexOf);
                return String.valueOf(protocol) + "://" + host + substring;
            }
            return String.valueOf(protocol) + "://" + host;
        } catch (Exception e2) {
            throw new Exception("UserAgent.getDir; " + e2.toString() + "; urlStr: " + str);
        }
    }

    private static String e(String str) {
        return str.indexOf("/.") != -1 ? h(f(g(str))) : str;
    }

    static /* synthetic */ Document g(UserAgent userAgent) throws ResponseException {
        Object[] objArr = userAgent.f4926a;
        int intValue = ((Integer) objArr[1]).intValue();
        Object[] objArr2 = userAgent.f4926a;
        int intValue2 = ((Integer) objArr2[10]).intValue();
        Object[] objArr3 = userAgent.f4926a;
        Document b2 = userAgent.b((String) objArr[0], intValue, (String) objArr2[2], (String) objArr2[3], (MultiMap) objArr2[4], (String) objArr2[5], (String) objArr2[6], (String) objArr2[7], (String) objArr2[8], (Map) objArr2[9], intValue2, (a) objArr3[11], (MultiMap) objArr3[12]);
        userAgent.f4926a = null;
        return b2;
    }

    private String b(String str) {
        for (t next : this.q.keySet()) {
            String a2 = next.a();
            if (a2 != null) {
                try {
                    a2 = b(str, a2);
                } catch (Exception e2) {
                    if (this.h.c) {
                        System.out.println("WARNING UserAgent.getBasicAuthenticationHeader; exception: ".concat(String.valueOf(e2)));
                    }
                }
                if (str.toLowerCase().startsWith(a2.toLowerCase())) {
                    return "Basic " + this.q.get(next).b();
                }
            }
        }
        return null;
    }

    public boolean a() {
        return this.v;
    }

    private void a(Document document) {
        this.i = document;
        for (int i2 = 0; i2 < this.n.size(); i2++) {
            this.n.get(i2);
        }
    }

    static /* synthetic */ Document f(UserAgent userAgent) throws ResponseException {
        short shortValue = ((Short) userAgent.f4926a[0]).shortValue();
        Object[] objArr = userAgent.f4926a;
        int intValue = ((Integer) objArr[2]).intValue();
        Object[] objArr2 = userAgent.f4926a;
        Document b2 = userAgent.b(shortValue, (String) objArr[1], intValue, (String) objArr2[3], (String) objArr2[4], (String) objArr2[5], (String) objArr2[6], (Map) objArr2[7], (File) objArr2[8], ((Boolean) objArr2[9]).booleanValue(), ((Integer) userAgent.f4926a[10]).intValue());
        userAgent.f4926a = null;
        return b2;
    }

    public UserAgent a(String str, Handler handler) {
        this.r.put(str.toLowerCase().split(";")[0], handler);
        return this;
    }

    private void a(String str, String str2, int i2) {
        for (int i3 = 0; i3 < this.e.size(); i3++) {
            k kVar = this.e.get(i3);
            if (i2 == 1) {
                kVar.a(str, str2);
            } else if (i2 == 2) {
                kVar.b(str, str2);
            } else {
                IOUtil.a("UserAgent.notifyHeaderListeners; unknown header type, headerType: ".concat(String.valueOf(i2)));
            }
        }
    }

    public String b() {
        if (!this.x) {
            return this.o.b();
        }
        j jVar = (j) this.r.get("text/json");
        return jVar != null ? jVar.a() : "";
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v52, resolved type: com.jaunt.HttpResponse} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v54, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v55, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v63, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v6, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v7, resolved type: com.jaunt.HttpResponse} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v8, resolved type: com.jaunt.HttpResponse} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v10, resolved type: com.jaunt.HttpResponse} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v11, resolved type: com.jaunt.Document} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v12, resolved type: com.jaunt.Document} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v13, resolved type: com.jaunt.Document} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v14, resolved type: com.jaunt.Document} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v128, resolved type: com.jaunt.HttpResponse} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v132, resolved type: com.jaunt.HttpResponse} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v15, resolved type: com.jaunt.Document} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v16, resolved type: com.jaunt.Document} */
    /* JADX WARNING: Code restructure failed: missing block: B:118:0x02b4, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:119:0x02b5, code lost:
        r3 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:121:?, code lost:
        r4.close();
        r1.close();
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:125:0x02d8, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + "; " + r3, (com.jaunt.HttpResponse) null, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:128:0x02e6, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:130:0x02e9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:131:0x02ea, code lost:
        r7 = r26;
        r2 = r0;
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x032c, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:148:0x032d, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:?, code lost:
        r4.close();
        r5.close();
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:151:0x034c, code lost:
        r11 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:154:0x0350, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + "; " + r1, (com.jaunt.HttpResponse) null, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:160:0x035a, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:161:0x035b, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:163:?, code lost:
        r4.close();
        r5.close();
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:164:0x037d, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + "; " + r1, (com.jaunt.HttpResponse) null, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x03c9, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:181:0x03ca, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:183:?, code lost:
        r4.close();
        r5.close();
        r10.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:184:0x03ec, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + "; " + r1, (com.jaunt.HttpResponse) null, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0422, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:192:0x0423, code lost:
        r7 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:196:0x0457, code lost:
        r1 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:198:0x0459, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:199:0x045a, code lost:
        r7 = r26;
        r2 = r0;
        r1 = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:200:0x045f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:201:0x0460, code lost:
        r26 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:207:0x0491, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:208:0x0492, code lost:
        r7 = r26;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:209:0x0495, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:210:0x0498, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:211:0x0499, code lost:
        r26 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:212:0x049c, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x049e, code lost:
        r26 = r7;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:215:0x04a0, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:217:0x04a1, code lost:
        r2 = r26;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x04a4, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:220:0x04a5, code lost:
        r26 = r7;
        r9 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:221:0x04a9, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:222:0x04aa, code lost:
        r2 = r0;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:223:0x04ac, code lost:
        r1 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:225:0x04ae, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:226:0x04af, code lost:
        r9 = r21;
        r1 = null;
        r2 = r0;
        r7 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x04b4, code lost:
        r3 = r23;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x04b7, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:229:0x04b8, code lost:
        r9 = r21;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:230:0x04ba, code lost:
        r13 = 1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x04bd, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x04be, code lost:
        r9 = r6;
        r1 = null;
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x04c3, code lost:
        r1 = null;
        r23 = "; Connection error";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x04c6, code lost:
        r2 = r1;
        r1 = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:237:0x04c7, code lost:
        if (r2 != null) goto L_0x04c9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:?, code lost:
        com.jaunt.util.IOUtil.a(r2.getErrorStream());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x04d0, code lost:
        r12.g = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x04eb, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + r23, r12.g, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:244:0x04ec, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:246:0x04ee, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:247:0x04ef, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:248:0x04f0, code lost:
        r9 = r6;
        r1 = null;
        r3 = "; Connection error";
        r2 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:253:0x0519, code lost:
        r12.g = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:254:0x0534, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + "; Connection error - invalid SSL certificate (to accept invalid SSL certificates, see UserAgentSettings.checkSSLCerts)", r12.g, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:255:0x0535, code lost:
        r12.g = r1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:256:0x0537, code lost:
        if (r7 != null) goto L_0x0539;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:?, code lost:
        r9.a(com.jaunt.util.IOUtil.a(r7.getErrorStream()));
        r12.g = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:261:0x054a, code lost:
        if (r12.g != null) goto L_0x054c;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:263:0x0565, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + "; response error", r12.g, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:265:0x057d, code lost:
        throw new com.jaunt.ResponseException(java.lang.String.valueOf(r16) + r3, r12.g, r14);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:266:0x057e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:267:0x057f, code lost:
        r22 = "; e:\n";
        r13 = 1;
        r9 = r6;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:268:0x0583, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0099, code lost:
        if (r12.h.d != false) goto L_0x009b;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x0155, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x0156, code lost:
        r1 = r0;
        r13 = 1;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:193:0x0426 A[Catch:{ IOException -> 0x03c9, NullPointerException -> 0x0495, ResponseException -> 0x04ec, Exception -> 0x0457, IOException -> 0x035a, NullPointerException -> 0x0495, ResponseException -> 0x04ec, Exception -> 0x0457, IOException -> 0x032c, NullPointerException -> 0x0495, ResponseException -> 0x04ec, Exception -> 0x04a0, NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }] */
    /* JADX WARNING: Removed duplicated region for block: B:197:? A[ExcHandler: Exception (unused java.lang.Exception), PHI: r11 
      PHI: (r11v8 com.jaunt.HttpResponse) = (r11v10 com.jaunt.HttpResponse), (r11v10 com.jaunt.HttpResponse), (r11v10 com.jaunt.HttpResponse), (r11v12 com.jaunt.Document), (r11v12 com.jaunt.Document), (r11v12 com.jaunt.Document), (r11v14 com.jaunt.Document), (r11v16 com.jaunt.Document) binds: [B:178:0x03c5, B:182:0x03cb, B:179:?, B:158:0x0356, B:162:0x035c, B:159:?, B:152:0x034d, B:80:0x01ab] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:152:0x034d] */
    /* JADX WARNING: Removed duplicated region for block: B:209:0x0495 A[ExcHandler: NullPointerException (e java.lang.NullPointerException), PHI: r13 
      PHI: (r13v9 int) = (r13v10 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v13 int), (r13v7 int) binds: [B:204:0x046c, B:133:0x02f1, B:178:0x03c5, B:182:0x03cb, B:179:?, B:158:0x0356, B:162:0x035c, B:159:?, B:145:0x0328, B:149:0x032e, B:152:0x034d, B:146:?, B:80:0x01ab] A[DONT_GENERATE, DONT_INLINE], Splitter:B:133:0x02f1] */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x049c A[ExcHandler: NullPointerException (e java.lang.NullPointerException), Splitter:B:61:0x016a] */
    /* JADX WARNING: Removed duplicated region for block: B:214:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:40:0x00db] */
    /* JADX WARNING: Removed duplicated region for block: B:216:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:80:0x01ab] */
    /* JADX WARNING: Removed duplicated region for block: B:228:0x04b7 A[ExcHandler: NullPointerException (e java.lang.NullPointerException), Splitter:B:38:0x00d7] */
    /* JADX WARNING: Removed duplicated region for block: B:235:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:13:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:238:0x04c9  */
    /* JADX WARNING: Removed duplicated region for block: B:244:0x04ec A[ExcHandler: ResponseException (r0v6 'e' com.jaunt.ResponseException A[CUSTOM_DECLARE]), Splitter:B:29:0x008f] */
    /* JADX WARNING: Removed duplicated region for block: B:253:0x0519  */
    /* JADX WARNING: Removed duplicated region for block: B:255:0x0535  */
    /* JADX WARNING: Removed duplicated region for block: B:266:0x057e A[ExcHandler: NullPointerException (e java.lang.NullPointerException), Splitter:B:13:0x0057] */
    /* JADX WARNING: Removed duplicated region for block: B:272:0x05b2  */
    /* JADX WARNING: Removed duplicated region for block: B:281:0x05f5  */
    /* JADX WARNING: Removed duplicated region for block: B:282:0x0619  */
    /* JADX WARNING: Removed duplicated region for block: B:285:0x0642 A[SYNTHETIC, Splitter:B:285:0x0642] */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x067a  */
    /* JADX WARNING: Removed duplicated region for block: B:298:0x06cb  */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x071c  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x0155 A[Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }, ExcHandler: NullPointerException (r0v27 'e' java.lang.NullPointerException A[CUSTOM_DECLARE, Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }]), Splitter:B:49:0x0144] */
    /* JADX WARNING: Removed duplicated region for block: B:70:0x017f A[Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jaunt.Document b(short r25, java.lang.String r26, int r27, java.lang.String r28, java.lang.String r29, java.lang.String r30, java.lang.String r31, java.util.Map<java.lang.String, java.lang.String> r32, java.io.File r33, boolean r34, int r35) throws com.jaunt.ResponseException {
        /*
            r24 = this;
            r12 = r24
            r13 = r25
            r14 = r28
            r15 = r33
            r11 = r35
            java.lang.String r10 = "; Connection error"
            java.lang.String r9 = "; e:\n"
            r8 = 0
            r12.x = r8
            java.lang.String r1 = "GET"
            r7 = 0
            r2 = 2
            if (r13 != r2) goto L_0x001d
            java.lang.String r2 = "UserAgent.sendGET"
            r5 = r1
            r16 = r2
            goto L_0x0040
        L_0x001d:
            r2 = 4
            if (r13 != r2) goto L_0x0028
            java.lang.String r2 = "UserAgent.sendDELETE"
            java.lang.String r3 = "DELETE"
            r16 = r2
            r5 = r3
            goto L_0x0040
        L_0x0028:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = "UserAgent.doHttpMethod, invalid requestMethod, requestMethod:"
            r2.<init>(r3)
            java.lang.String r3 = com.jaunt.HttpRequest.a(r25)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r2)
            r5 = r7
            r16 = r5
        L_0x0040:
            com.jaunt.UserAgentSettings r2 = r12.h
            boolean r2 = r2.f
            if (r2 == 0) goto L_0x0049
            r12.a((com.jaunt.h) r7)
        L_0x0049:
            com.jaunt.HttpResponse r6 = new com.jaunt.HttpResponse
            r6.<init>(r14)
            com.jaunt.UserAgentSettings r2 = r12.h
            int r2 = r2.m
            java.lang.String r4 = ";"
            if (r11 > r2) goto L_0x0726
            r3 = 1
            boolean r2 = r24.a()     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04ef, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            if (r2 == 0) goto L_0x008f
            boolean r1 = r5.equals(r1)     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            if (r1 == 0) goto L_0x008f
            if (r15 != 0) goto L_0x008f
            com.jaunt.util.Cache r1 = r12.u     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            java.lang.String r1 = r1.get(r14)     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            if (r1 == 0) goto L_0x007d
            r12.a((java.lang.String) r1, (java.lang.String) r14)     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            r6.a((boolean) r3)     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            r1 = 200(0xc8, float:2.8E-43)
            r6.a((int) r1)     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            r12.g = r6     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            com.jaunt.Document r1 = r12.i     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            return r1
        L_0x007d:
            r6.a((boolean) r8)     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            goto L_0x008f
        L_0x0081:
            r0 = move-exception
            r2 = r0
            r9 = r6
            r1 = r7
            goto L_0x04c1
        L_0x0087:
            r0 = move-exception
            r1 = r0
            r22 = r9
            r13 = 1
            r9 = r6
            goto L_0x0584
        L_0x008f:
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            boolean r1 = r1.e     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            if (r1 != 0) goto L_0x009b
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            boolean r1 = r1.d     // Catch:{ NullPointerException -> 0x0087, IOException -> 0x0081, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            if (r1 == 0) goto L_0x00b6
        L_0x009b:
            java.io.PrintStream r1 = java.lang.System.out     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            java.lang.String r3 = "Requesting ("
            r2.<init>(r3)     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            r2.append(r5)     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            java.lang.String r3 = " request): "
            r2.append(r3)     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            r2.append(r14)     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
            r1.println(r2)     // Catch:{ NullPointerException -> 0x057e, IOException -> 0x04bd, ResponseException -> 0x04ec, Exception -> 0x04c3 }
        L_0x00b6:
            r18 = 11
            r19 = -1
            r1 = r24
            r2 = r26
            r3 = r27
            r20 = r4
            r4 = r28
            r21 = r6
            r6 = r18
            r7 = r29
            r8 = r30
            r22 = r9
            r9 = r31
            r23 = r10
            r10 = r32
            r13 = r11
            r11 = r19
            java.net.HttpURLConnection r7 = r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04ae, ResponseException -> 0x04ec, Exception -> 0x04ac }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            int r1 = r1.r     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            r7.setConnectTimeout(r1)     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            int r1 = r1.s     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            r7.setReadTimeout(r1)     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            r3 = 2
            r5 = -1
            r6 = 0
            r1 = r24
            r2 = r7
            r4 = r28
            r1.a((java.net.HttpURLConnection) r2, (int) r3, (java.lang.String) r4, (int) r5, (java.lang.String) r6)     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            int r8 = r7.getResponseCode()     // Catch:{ NullPointerException -> 0x04b7, IOException -> 0x04a4, ResponseException -> 0x04ec, Exception -> 0x049e }
            r9 = r21
            r9.a((int) r8)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r6 = r7.getResponseMessage()     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            r9.a((java.lang.String) r6)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            r3 = 1
            r4 = 0
            r1 = r24
            r2 = r7
            r5 = r8
            r1.a((java.net.HttpURLConnection) r2, (int) r3, (java.lang.String) r4, (int) r5, (java.lang.String) r6)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            r9.a((com.jaunt.util.MultiMap<java.lang.String, java.lang.String>) r1)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.io.InputStream r10 = r7.getInputStream()     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r1 = "date"
            java.lang.String r11 = r9.b(r1)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.util.Date r1 = new java.util.Date     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            r1.<init>()     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            long r3 = r1.getTime()     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            r5 = 2764800000(0xa4cb8000, double:1.3659926976E-314)
            r1 = r24
            r2 = r11
            boolean r1 = r1.a((java.lang.String) r2, (long) r3, (long) r5)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0498, ResponseException -> 0x04ec, Exception -> 0x049e }
            if (r1 == 0) goto L_0x0468
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            r12.a((java.lang.String) r14, (com.jaunt.util.MultiMap<java.lang.String, java.lang.String>) r1)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r2 = "content-type"
            java.lang.Object r1 = r1.b(r2)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            if (r1 == 0) goto L_0x015a
            java.lang.String r1 = r1.toLowerCase()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }
            r2 = r20
            java.lang.String[] r2 = r1.split(r2)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }
            r6 = 0
            r2 = r2[r6]     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }
            goto L_0x015c
        L_0x0152:
            r0 = move-exception
            goto L_0x0462
        L_0x0155:
            r0 = move-exception
            r1 = r0
            r13 = 1
            goto L_0x0584
        L_0x015a:
            r6 = 0
            r2 = 0
        L_0x015c:
            r3 = 400(0x190, float:5.6E-43)
            if (r8 >= r3) goto L_0x0433
            if (r1 == 0) goto L_0x016a
            java.lang.String r3 = "text/plain"
            boolean r3 = r1.startsWith(r3)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }
            if (r3 == 0) goto L_0x017c
        L_0x016a:
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r3 = r12.b     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r4 = "location"
            java.util.List r3 = r3.a(r4)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            if (r3 == 0) goto L_0x017c
            int r3 = r3.size()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0152, ResponseException -> 0x04ec, Exception -> 0x049e }
            if (r3 <= 0) goto L_0x017c
            r3 = 1
            goto L_0x017d
        L_0x017c:
            r3 = 0
        L_0x017d:
            if (r3 != 0) goto L_0x0426
            if (r1 != 0) goto L_0x0183
            java.lang.String r1 = "text/html"
        L_0x0183:
            java.util.Map<java.lang.String, com.jaunt.util.Handler> r3 = r12.r     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.Object r3 = r3.get(r2)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            com.jaunt.util.Handler r3 = (com.jaunt.util.Handler) r3     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r4 = r4.f4931a     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.nio.charset.Charset r4 = java.nio.charset.Charset.forName(r4)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x045f, ResponseException -> 0x04ec, Exception -> 0x049e }
            java.lang.String r5 = "; response contains unsupported content-encoding ("
            java.lang.String r8 = "deflate"
            java.lang.String r11 = "gzip"
            java.lang.String r6 = "content-encoding"
            r26 = r7
            java.lang.String r7 = ")"
            if (r3 != 0) goto L_0x026e
            if (r15 == 0) goto L_0x01a7
            if (r34 == 0) goto L_0x01a7
            goto L_0x026e
        L_0x01a7:
            if (r15 == 0) goto L_0x01b3
            if (r34 != 0) goto L_0x01b3
            a((java.io.File) r15, (java.io.InputStream) r10)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
        L_0x01ae:
            r10 = r22
            r13 = 1
            goto L_0x05a8
        L_0x01b3:
            boolean r2 = c((java.lang.String) r1)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x024b
            com.jaunt.s r1 = r12.o     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.UserAgentSettings r2 = r12.h     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            int r2 = r2.n     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.a((int) r2)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.Object r1 = r1.b(r6)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r1 != 0) goto L_0x01e2
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.<init>(r10, r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.<init>(r1)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.s r3 = r12.o     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.a((java.io.BufferedReader) r2, (java.lang.String) r14)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            goto L_0x01ae
        L_0x01e2:
            boolean r2 = r1.startsWith(r11)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x0206
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.<init>(r10)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.<init>(r2)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.s r4 = r12.o     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.a((java.io.BufferedReader) r3, (java.lang.String) r14)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            goto L_0x01ae
        L_0x0206:
            boolean r2 = r1.startsWith(r8)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x022a
            java.util.zip.InflaterInputStream r1 = new java.util.zip.InflaterInputStream     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.<init>(r10)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.InputStreamReader r2 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.<init>(r2)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.s r4 = r12.o     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.a((java.io.BufferedReader) r3, (java.lang.String) r14)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            goto L_0x01ae
        L_0x022a:
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r5)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r7)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.<init>(r1, r9, r14)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            throw r2     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
        L_0x024b:
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r4 = "; response is of unsupported content type ("
            r3.append(r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r7)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r2.<init>(r1, r9, r14)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            throw r2     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
        L_0x026e:
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.Object r1 = r1.b(r6)     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NullPointerException -> 0x049c, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r6 = "; "
            if (r1 != 0) goto L_0x02f0
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.<init>(r10, r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.<init>(r1)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r3 == 0) goto L_0x02ad
            r3.a(r12, r9, r10, r4)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            boolean r2 = j(r2)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x02ab
            java.lang.Object r2 = r3.getContent()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.JNode r2 = (com.jaunt.JNode) r2     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r12.j = r2     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.UserAgentSettings r2 = r12.h     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            boolean r2 = r2.j     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x02a2
            com.jaunt.JNode r2 = r12.j     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r12.a((com.jaunt.JNode) r2)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
        L_0x02a2:
            if (r15 == 0) goto L_0x02a7
            a((java.io.File) r15, (java.io.InputStream) r10)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
        L_0x02a7:
            r2 = 1
            r12.x = r2     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            goto L_0x02d9
        L_0x02ab:
            r2 = 1
            goto L_0x02d9
        L_0x02ad:
            r2 = 1
            if (r15 == 0) goto L_0x02d9
            a((java.io.File) r15, (java.io.InputStream) r10)     // Catch:{ IOException -> 0x02b4, NullPointerException -> 0x0155, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            goto L_0x02d9
        L_0x02b4:
            r0 = move-exception
            r3 = r0
            r4.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r5 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.<init>(r5)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.append(r6)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.append(r3)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r3 = r4.toString()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r5 = 0
            r1.<init>(r3, r5, r14)     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x02e9, ResponseException -> 0x04ec, Exception -> 0x02e6 }
            throw r1     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x02e9, ResponseException -> 0x04ec, Exception -> 0x02e6 }
        L_0x02d9:
            r5 = 0
            r4.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x02e9, ResponseException -> 0x04ec, Exception -> 0x02e6 }
            r1.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x02e9, ResponseException -> 0x04ec, Exception -> 0x02e6 }
            r10.close()     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x02e9, ResponseException -> 0x04ec, Exception -> 0x02e6 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0155, IOException -> 0x02e9, ResponseException -> 0x04ec, Exception -> 0x02e6 }
            return r5
        L_0x02e6:
            r1 = r5
            goto L_0x04a1
        L_0x02e9:
            r0 = move-exception
            r7 = r26
            r2 = r0
            r1 = r5
            goto L_0x04b4
        L_0x02f0:
            r13 = 1
            boolean r11 = r1.startsWith(r11)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r11 == 0) goto L_0x038d
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r1.<init>(r10)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r5.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r4.<init>(r5)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r3 == 0) goto L_0x0353
            r3.a(r12, r9, r10, r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            boolean r2 = j(r2)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x0351
            java.lang.Object r2 = r3.getContent()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.JNode r2 = (com.jaunt.JNode) r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r12.j = r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.UserAgentSettings r2 = r12.h     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            boolean r2 = r2.j     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r2 == 0) goto L_0x0324
            com.jaunt.JNode r2 = r12.j     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r12.a((com.jaunt.JNode) r2)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
        L_0x0324:
            r12.x = r13     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            if (r15 == 0) goto L_0x0351
            a((java.io.File) r15, (java.io.InputStream) r1)     // Catch:{ IOException -> 0x032c, NullPointerException -> 0x0495, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            goto L_0x0351
        L_0x032c:
            r0 = move-exception
            r1 = r0
            r4.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r5.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0422, ResponseException -> 0x04ec, Exception -> 0x04a0 }
            r11 = 0
            r2.<init>(r1, r11, r14)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            throw r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x0351:
            r11 = 0
            goto L_0x037e
        L_0x0353:
            r11 = 0
            if (r15 == 0) goto L_0x037e
            a((java.io.File) r15, (java.io.InputStream) r1)     // Catch:{ IOException -> 0x035a, NullPointerException -> 0x0495, ResponseException -> 0x04ec, Exception -> 0x0457 }
            goto L_0x037e
        L_0x035a:
            r0 = move-exception
            r1 = r0
            r4.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r5.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r2.<init>(r1, r11, r14)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            throw r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x037e:
            r4.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r5.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r1.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            return r11
        L_0x038d:
            r11 = 0
            boolean r8 = r1.startsWith(r8)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            if (r8 == 0) goto L_0x0401
            java.util.zip.InflaterInputStream r1 = new java.util.zip.InflaterInputStream     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r1.<init>(r10)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.io.InputStreamReader r5 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r5.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r4.<init>(r5)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            if (r3 == 0) goto L_0x03ed
            r3.a(r12, r9, r10, r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            boolean r2 = j(r2)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            if (r2 == 0) goto L_0x03f2
            java.lang.Object r2 = r3.getContent()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            com.jaunt.JNode r2 = (com.jaunt.JNode) r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.j = r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            com.jaunt.UserAgentSettings r2 = r12.h     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            boolean r2 = r2.j     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            if (r2 == 0) goto L_0x03c1
            com.jaunt.JNode r2 = r12.j     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.a((com.jaunt.JNode) r2)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x03c1:
            r12.x = r13     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            if (r15 == 0) goto L_0x03f2
            a((java.io.File) r15, (java.io.InputStream) r1)     // Catch:{ IOException -> 0x03c9, NullPointerException -> 0x0495, ResponseException -> 0x04ec, Exception -> 0x0457 }
            goto L_0x03f2
        L_0x03c9:
            r0 = move-exception
            r1 = r0
            r4.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r5.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r2.<init>(r1, r11, r14)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            throw r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x03ed:
            if (r15 == 0) goto L_0x03f2
            a((java.io.File) r15, (java.io.InputStream) r1)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x03f2:
            r4.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r5.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r1.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            return r11
        L_0x0401:
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r5)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r3.append(r7)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r2.<init>(r1, r9, r14)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            throw r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x0422:
            r0 = move-exception
            r7 = r26
            goto L_0x0462
        L_0x0426:
            r26 = r7
            r11 = 0
            r13 = 1
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r10 = r22
            goto L_0x05a8
        L_0x0433:
            r26 = r7
            r11 = 0
            r13 = 1
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r3 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r2.<init>(r3)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r3 = "; response code "
            r2.append(r3)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r2.append(r8)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            r1.<init>(r2, r9, r14)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
            throw r1     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0459, ResponseException -> 0x04ec, Exception -> 0x0457 }
        L_0x0457:
            r1 = r11
            goto L_0x04a1
        L_0x0459:
            r0 = move-exception
            r7 = r26
            r2 = r0
            r1 = r11
            goto L_0x04b4
        L_0x045f:
            r0 = move-exception
            r26 = r7
        L_0x0462:
            r2 = r0
            r3 = r23
            r1 = 0
            goto L_0x04f4
        L_0x0468:
            r26 = r7
            r1 = 0
            r13 = 1
            r10.close()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            java.lang.String r4 = java.lang.String.valueOf(r16)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            java.lang.String r4 = "; response date "
            r3.append(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            r3.append(r11)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            java.lang.String r4 = " cannot be reconciled"
            r3.append(r4)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            r2.<init>(r3, r9, r14)     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
            throw r2     // Catch:{ NullPointerException -> 0x0495, IOException -> 0x0491, ResponseException -> 0x04ec, Exception -> 0x04a1 }
        L_0x0491:
            r0 = move-exception
            r7 = r26
            goto L_0x04aa
        L_0x0495:
            r0 = move-exception
            goto L_0x0583
        L_0x0498:
            r0 = move-exception
            r26 = r7
            goto L_0x04a9
        L_0x049c:
            r0 = move-exception
            goto L_0x04ba
        L_0x049e:
            r26 = r7
        L_0x04a0:
            r1 = 0
        L_0x04a1:
            r2 = r26
            goto L_0x04c7
        L_0x04a4:
            r0 = move-exception
            r26 = r7
            r9 = r21
        L_0x04a9:
            r1 = 0
        L_0x04aa:
            r2 = r0
            goto L_0x04b4
        L_0x04ac:
            r1 = 0
            goto L_0x04c6
        L_0x04ae:
            r0 = move-exception
            r9 = r21
            r1 = 0
            r2 = r0
            r7 = r1
        L_0x04b4:
            r3 = r23
            goto L_0x04f4
        L_0x04b7:
            r0 = move-exception
            r9 = r21
        L_0x04ba:
            r13 = 1
            goto L_0x0583
        L_0x04bd:
            r0 = move-exception
            r9 = r6
            r1 = r7
            r2 = r0
        L_0x04c1:
            r3 = r10
            goto L_0x04f4
        L_0x04c3:
            r1 = r7
            r23 = r10
        L_0x04c6:
            r2 = r1
        L_0x04c7:
            if (r2 == 0) goto L_0x04d0
            java.io.InputStream r2 = r2.getErrorStream()
            com.jaunt.util.IOUtil.a((java.io.InputStream) r2)     // Catch:{ Exception -> 0x04d0 }
        L_0x04d0:
            r12.g = r1
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r16)
            r2.<init>(r3)
            r3 = r23
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r14)
            throw r1
        L_0x04ec:
            r0 = move-exception
            r1 = r0
            throw r1
        L_0x04ef:
            r0 = move-exception
            r9 = r6
            r1 = r7
            r3 = r10
            r2 = r0
        L_0x04f4:
            java.io.PrintStream r4 = java.lang.System.err
            java.lang.String r5 = java.lang.String.valueOf(r2)
            java.lang.String r6 = "****"
            java.lang.String r5 = r6.concat(r5)
            r4.println(r5)
            boolean r4 = r2 instanceof javax.net.ssl.SSLHandshakeException
            if (r4 == 0) goto L_0x0535
            java.lang.String r2 = r2.toString()
            java.lang.String r2 = r2.toLowerCase()
            java.lang.String r4 = "pkix path building failed"
            int r2 = r2.indexOf(r4)
            r4 = -1
            if (r2 != r4) goto L_0x0519
            goto L_0x0535
        L_0x0519:
            r12.g = r9
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r16)
            r2.<init>(r3)
            java.lang.String r3 = "; Connection error - invalid SSL certificate (to accept invalid SSL certificates, see UserAgentSettings.checkSSLCerts)"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r14)
            throw r1
        L_0x0535:
            r12.g = r1
            if (r7 == 0) goto L_0x0566
            java.io.InputStream r1 = r7.getErrorStream()     // Catch:{ Exception -> 0x0547 }
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.io.InputStream) r1)     // Catch:{ Exception -> 0x0547 }
            r9.a((java.lang.String) r1)     // Catch:{ Exception -> 0x0547 }
            r12.g = r9     // Catch:{ Exception -> 0x0547 }
            goto L_0x0548
        L_0x0547:
        L_0x0548:
            com.jaunt.HttpResponse r1 = r12.g
            if (r1 == 0) goto L_0x0566
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r16)
            r2.<init>(r3)
            java.lang.String r3 = "; response error"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r14)
            throw r1
        L_0x0566:
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r16)
            r2.<init>(r4)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r14)
            throw r1
        L_0x057e:
            r0 = move-exception
            r22 = r9
            r13 = 1
            r9 = r6
        L_0x0583:
            r1 = r0
        L_0x0584:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r16)
            r2.<init>(r3)
            java.lang.String r3 = "; NullPointerException for location "
            r2.append(r3)
            r2.append(r14)
            r10 = r22
            r2.append(r10)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x05a8:
            r12.g = r9
            com.jaunt.s r1 = r12.o
            com.jaunt.d r11 = r1.c()
            if (r15 != 0) goto L_0x05b4
            r12.p = r14
        L_0x05b4:
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r2 = r12.b
            r4 = 0
            r1 = r24
            r3 = r28
            r5 = r25
            r8 = 0
            r6 = r33
            r7 = r35
            boolean r1 = r1.a((com.jaunt.util.MultiMap<java.lang.String, java.lang.String>) r2, (java.lang.String) r3, (java.lang.String) r4, (short) r5, (java.io.File) r6, (int) r7)
            if (r1 != 0) goto L_0x0723
            r17 = 0
            r1 = r24
            r2 = r9
            r3 = r28
            r4 = r33
            r5 = r35
            r6 = r25
            r7 = r32
            r15 = 0
            r8 = r17
            boolean r1 = r1.a(r2, r3, r4, r5, r6, r7, r8)
            if (r1 != 0) goto L_0x0723
            com.jaunt.Element r1 = r11.f()
            com.jaunt.Element r1 = r1.a((int) r15)
            if (r1 == 0) goto L_0x0619
            com.jaunt.Element r1 = r11.f()
            com.jaunt.Element r1 = r1.a((int) r13)
            if (r1 == 0) goto L_0x05f5
            goto L_0x0619
        L_0x05f5:
            com.jaunt.Document r13 = new com.jaunt.Document
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r4 = r12.b
            com.jaunt.Element r5 = r11.f()
            java.util.List r6 = r11.g()
            int r7 = r11.e()
            short r8 = r11.a()
            boolean r9 = r11.b()
            r1 = r13
            r2 = r24
            r3 = r28
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r12.a((com.jaunt.Document) r13)
            goto L_0x063c
        L_0x0619:
            com.jaunt.Document r13 = new com.jaunt.Document
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r4 = r12.b
            com.jaunt.Element r5 = r11.f()
            java.util.List r6 = r11.g()
            int r7 = r11.e()
            short r8 = r11.a()
            boolean r9 = r11.b()
            r1 = r13
            r2 = r24
            r3 = r28
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r12.a((com.jaunt.Document) r13)
        L_0x063c:
            boolean r1 = r24.a()
            if (r1 == 0) goto L_0x0670
            com.jaunt.util.Cache r1 = r12.u     // Catch:{ CacheException -> 0x064c }
            java.lang.String r2 = r24.b()     // Catch:{ CacheException -> 0x064c }
            r1.put(r14, r2)     // Catch:{ CacheException -> 0x064c }
            goto L_0x0670
        L_0x064c:
            r0 = move-exception
            r1 = r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r3 = java.lang.String.valueOf(r16)
            r2.<init>(r3)
            java.lang.String r3 = ", CacheException for location "
            r2.append(r3)
            r2.append(r14)
            r2.append(r10)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x0670:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.h
            java.lang.String r2 = "/"
            java.lang.String r3 = "; IOException when attempting autosave; e: "
            if (r1 == 0) goto L_0x06c5
            com.jaunt.Document r1 = r12.i
            java.lang.String r1 = r1.h()
            java.io.File r4 = new java.io.File     // Catch:{ IOException -> 0x06a9 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x06a9 }
            com.jaunt.UserAgentSettings r6 = r12.h     // Catch:{ IOException -> 0x06a9 }
            java.lang.String r6 = r6.k     // Catch:{ IOException -> 0x06a9 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ IOException -> 0x06a9 }
            r5.<init>(r6)     // Catch:{ IOException -> 0x06a9 }
            r5.append(r2)     // Catch:{ IOException -> 0x06a9 }
            com.jaunt.UserAgentSettings r6 = r12.h     // Catch:{ IOException -> 0x06a9 }
            java.lang.String r6 = r6.l     // Catch:{ IOException -> 0x06a9 }
            r5.append(r6)     // Catch:{ IOException -> 0x06a9 }
            java.lang.String r6 = ".html"
            r5.append(r6)     // Catch:{ IOException -> 0x06a9 }
            java.lang.String r5 = r5.toString()     // Catch:{ IOException -> 0x06a9 }
            r4.<init>(r5)     // Catch:{ IOException -> 0x06a9 }
            com.jaunt.util.IOUtil.a(r4, r1)     // Catch:{ IOException -> 0x06a9 }
            goto L_0x06c5
        L_0x06a9:
            r0 = move-exception
            r1 = r0
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r16)
            r4.<init>(r5)
            r4.append(r3)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x06c5:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.i
            if (r1 == 0) goto L_0x0716
            com.jaunt.Document r1 = r12.i
            java.lang.String r1 = r1.i()
            java.io.File r4 = new java.io.File     // Catch:{ Exception -> 0x06fa }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x06fa }
            com.jaunt.UserAgentSettings r6 = r12.h     // Catch:{ Exception -> 0x06fa }
            java.lang.String r6 = r6.k     // Catch:{ Exception -> 0x06fa }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ Exception -> 0x06fa }
            r5.<init>(r6)     // Catch:{ Exception -> 0x06fa }
            r5.append(r2)     // Catch:{ Exception -> 0x06fa }
            com.jaunt.UserAgentSettings r2 = r12.h     // Catch:{ Exception -> 0x06fa }
            java.lang.String r2 = r2.l     // Catch:{ Exception -> 0x06fa }
            r5.append(r2)     // Catch:{ Exception -> 0x06fa }
            java.lang.String r2 = ".xml"
            r5.append(r2)     // Catch:{ Exception -> 0x06fa }
            java.lang.String r2 = r5.toString()     // Catch:{ Exception -> 0x06fa }
            r4.<init>(r2)     // Catch:{ Exception -> 0x06fa }
            com.jaunt.util.IOUtil.a(r4, r1)     // Catch:{ Exception -> 0x06fa }
            goto L_0x0716
        L_0x06fa:
            r0 = move-exception
            r1 = r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r4 = java.lang.String.valueOf(r16)
            r2.<init>(r4)
            r2.append(r3)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x0716:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.b
            if (r1 == 0) goto L_0x0723
            com.jaunt.Document r1 = r12.i
            r2 = r35
            r1.b(r2)
        L_0x0723:
            com.jaunt.Document r1 = r12.i
            return r1
        L_0x0726:
            r2 = r4
            r9 = r6
            r15 = 0
            r1 = 409(0x199, float:5.73E-43)
            r9.a((int) r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r3 = "Exceeded maximum allowable redirects ("
            r1.<init>(r3)
            com.jaunt.UserAgentSettings r3 = r12.h
            int r3 = r3.m
            r1.append(r3)
            java.lang.String r3 = ")."
            r1.append(r3)
            java.lang.String r1 = r1.toString()
            r9.a((java.lang.String) r1)
            r9.a((boolean) r15)
            r12.g = r9
            com.jaunt.ResponseException r3 = new com.jaunt.ResponseException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r16)
            r4.<init>(r5)
            r4.append(r2)
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r3.<init>(r1, r9, r14)
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.UserAgent.b(short, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map, java.io.File, boolean, int):com.jaunt.Document");
    }

    private void a(int i2, int i3, String str, int i4, String str2) {
        for (int i5 = 0; i5 < this.e.size(); i5++) {
            k kVar = this.e.get(i5);
            if (i2 == 3) {
                if (i3 == 1) {
                    kVar.a(i4, str2);
                } else if (i3 == 2) {
                    kVar.a();
                } else {
                    IOUtil.a("UserAgent.notifyHeaderListeners; unknown header type, headerType: ".concat(String.valueOf(i3)));
                }
            } else if (i2 != 4) {
                IOUtil.a("UserAgent.notifyHeaderListeners; unknown action, action: ".concat(String.valueOf(i2)));
            } else if (i3 == 1) {
                kVar.c();
            } else if (i3 == 2) {
                kVar.b();
            } else {
                IOUtil.a("UserAgent.notifyHeaderListeners; unknown header type, headerType: ".concat(String.valueOf(i3)));
            }
        }
    }

    private void a(HttpURLConnection httpURLConnection, int i2, String str, int i3, String str2) {
        if (i2 == 2) {
            a(3, 2, str, -1, (String) null);
            Map requestProperties = httpURLConnection.getRequestProperties();
            for (String str3 : requestProperties.keySet()) {
                List list = (List) requestProperties.get(str3);
                for (int i4 = 0; i4 < list.size(); i4++) {
                    a(str3, (String) list.get(i4), i2);
                }
            }
            a(4, 2, (String) null, -1, (String) null);
        } else if (i2 == 1) {
            a(3, 1, (String) null, i3, str2);
            int i5 = 1;
            while (true) {
                String headerFieldKey = httpURLConnection.getHeaderFieldKey(i5);
                if (headerFieldKey != null) {
                    a(headerFieldKey, httpURLConnection.getHeaderField(i5), 1);
                    i5++;
                } else {
                    a(4, 1, (String) null, -1, (String) null);
                    return;
                }
            }
        } else {
            IOUtil.a("UserAgent.notifyHeaderListeners; unknown headerType, headerType: ".concat(String.valueOf(i2)));
        }
    }

    public Document a(String str, String str2) throws ResponseException {
        this.x = false;
        this.g = null;
        if (this.h.f) {
            a((h) null);
        }
        this.o.a(this.h.n);
        this.o.a(str, str2);
        this.p = "";
        d c2 = this.o.c();
        if (c2.f().a(0) == null || c2.f().a(1) != null) {
            a(new Document(this, (String) null, new MultiMap(), c2.f(), c2.g(), c2.e(), c2.a(), c2.b()));
        } else {
            a(new Document(this, (String) null, new MultiMap(), c2.f(), c2.g(), c2.e(), c2.a(), c2.b()));
        }
        if (this.h.b) {
            this.i.b(0);
        }
        return this.i;
    }

    public Document a(String str) throws ResponseException {
        return a(str, 0);
    }

    public Document a(String str, int i2) throws ResponseException {
        String i3 = i(str);
        String b2 = b(str);
        UserAgentSettings userAgentSettings = this.h;
        long j2 = userAgentSettings.t;
        if (j2 > 0) {
            userAgentSettings.s = (int) j2;
            userAgentSettings.r = (int) j2;
            return a(2, this.s, this.t, str, this.p, i3, b2, (Map<String, String>) null, (File) null, true, i2);
        }
        return b(2, this.s, this.t, str, this.p, i3, b2, (Map<String, String>) null, (File) null, true, i2);
    }

    private Document a(String str, File file, int i2, String... strArr) throws ResponseException {
        return b(2, this.s, this.t, str, this.p, i(str), b(str), a((String[]) null), file, true, i2);
    }

    private static Map<String, String> a(String[] strArr) {
        HashMap hashMap = new HashMap();
        if (strArr != null) {
            for (String str : strArr) {
                if (str != null) {
                    int indexOf = str.indexOf(":");
                    hashMap.put(str.substring(0, indexOf), indexOf == str.length() + -1 ? "" : str.substring(indexOf + 1));
                } else {
                    IOUtil.a("UserAgent.toMap, invalid request header value, header: null");
                }
            }
        }
        return hashMap;
    }

    public Document a(String str, String... strArr) throws ResponseException {
        String i2 = i(str);
        String b2 = b(str);
        UserAgentSettings userAgentSettings = this.h;
        long j2 = userAgentSettings.t;
        if (j2 > 0) {
            userAgentSettings.s = (int) j2;
            userAgentSettings.r = (int) j2;
            return a(4, this.s, this.t, str, this.p, i2, b2, a(strArr), (File) null, true, 0);
        }
        return b(4, this.s, this.t, str, this.p, i2, b2, a(strArr), (File) null, true, 0);
    }

    private boolean a(String str, long j2, long j3) {
        if (str == null) {
            return true;
        }
        try {
            if (Long.valueOf(this.w.parse(str).getTime()).longValue() - j2 < 2764800000L) {
                return true;
            }
            return false;
        } catch (ParseException unused) {
            return true;
        }
    }

    private void a(JNode jNode) {
        try {
            IOUtil.a(new File(String.valueOf(this.h.k) + "/" + this.h.l + ".json"), jNode.toString());
        } catch (Exception e2) {
            IOUtil.a("IOException when attempting autosave; e: " + IOUtil.a((Throwable) e2));
        }
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:12:0x007b */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jaunt.Document a(short r10, java.lang.String r11, int r12, java.lang.String r13, java.lang.String r14, java.lang.String r15, java.lang.String r16, java.util.Map<java.lang.String, java.lang.String> r17, java.io.File r18, boolean r19, int r20) throws com.jaunt.ResponseException {
        /*
            r9 = this;
            r1 = r9
            r0 = r10
            r2 = r13
            r3 = 4
            r4 = 2
            r5 = 0
            if (r0 != r4) goto L_0x000b
            java.lang.String r6 = "UserAgent.sendGET"
            goto L_0x0026
        L_0x000b:
            if (r0 != r3) goto L_0x0010
            java.lang.String r6 = "UserAgent.sendDELETE"
            goto L_0x0026
        L_0x0010:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            java.lang.String r7 = "UserAgent.doHttpMethod, invalid requestMethod, requestMethod:"
            r6.<init>(r7)
            java.lang.String r7 = com.jaunt.HttpRequest.a(r10)
            r6.append(r7)
            java.lang.String r6 = r6.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r6)
            r6 = r5
        L_0x0026:
            r7 = 11
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r8 = 0
            java.lang.Short r0 = java.lang.Short.valueOf(r10)
            r7[r8] = r0
            r0 = 1
            r7[r0] = r11
            java.lang.Integer r0 = java.lang.Integer.valueOf(r12)
            r7[r4] = r0
            r0 = 3
            r7[r0] = r2
            r7[r3] = r14
            r0 = 5
            r7[r0] = r15
            r0 = 6
            r7[r0] = r16
            r0 = 7
            r7[r0] = r17
            r0 = 8
            r7[r0] = r18
            r0 = 9
            java.lang.Boolean r3 = java.lang.Boolean.TRUE
            r7[r0] = r3
            r0 = 10
            java.lang.Integer r3 = java.lang.Integer.valueOf(r20)
            r7[r0] = r3
            r1.f4926a = r7
            java.util.concurrent.ExecutorService r3 = java.util.concurrent.Executors.newCachedThreadPool()
            com.jaunt.UserAgent$4 r0 = new com.jaunt.UserAgent$4
            r0.<init>()
            java.util.concurrent.Future r0 = r3.submit(r0)
            com.jaunt.UserAgentSettings r4 = r1.h     // Catch:{ ExecutionException -> 0x00ab, TimeoutException -> 0x0093, InterruptedException -> 0x007b }
            long r7 = r4.t     // Catch:{ ExecutionException -> 0x00ab, TimeoutException -> 0x0093, InterruptedException -> 0x007b }
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ ExecutionException -> 0x00ab, TimeoutException -> 0x0093, InterruptedException -> 0x007b }
            java.lang.Object r0 = r0.get(r7, r4)     // Catch:{ ExecutionException -> 0x00ab, TimeoutException -> 0x0093, InterruptedException -> 0x007b }
            com.jaunt.Document r0 = (com.jaunt.Document) r0     // Catch:{ ExecutionException -> 0x00ab, TimeoutException -> 0x0093, InterruptedException -> 0x007b }
            r3.shutdown()
            return r0
        L_0x0079:
            r0 = move-exception
            goto L_0x00d7
        L_0x007b:
            com.jaunt.ResponseException r0 = new com.jaunt.ResponseException     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x0079 }
            r4.<init>(r6)     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = "; interrupted"
            r4.append(r6)     // Catch:{ all -> 0x0079 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0079 }
            r0.<init>(r4, r5, r13)     // Catch:{ all -> 0x0079 }
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x0093:
            com.jaunt.ResponseException r0 = new com.jaunt.ResponseException     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = java.lang.String.valueOf(r6)     // Catch:{ all -> 0x0079 }
            r4.<init>(r6)     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = "; response timed out"
            r4.append(r6)     // Catch:{ all -> 0x0079 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0079 }
            r0.<init>(r4, r5, r13)     // Catch:{ all -> 0x0079 }
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x00ab:
            r0 = move-exception
            java.lang.Throwable r2 = r0.getCause()     // Catch:{ all -> 0x0079 }
            boolean r2 = r2 instanceof com.jaunt.ResponseException     // Catch:{ all -> 0x0079 }
            if (r2 == 0) goto L_0x00bb
            java.lang.Throwable r0 = r0.getCause()     // Catch:{ all -> 0x0079 }
            com.jaunt.ResponseException r0 = (com.jaunt.ResponseException) r0     // Catch:{ all -> 0x0079 }
            throw r0     // Catch:{ all -> 0x0079 }
        L_0x00bb:
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ all -> 0x0079 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0079 }
            java.lang.String r6 = "UserAgent.doHttpMethod_Timed, stack trace:"
            r4.<init>(r6)     // Catch:{ all -> 0x0079 }
            java.lang.Throwable r0 = r0.getCause()     // Catch:{ all -> 0x0079 }
            java.lang.String r0 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r0)     // Catch:{ all -> 0x0079 }
            r4.append(r0)     // Catch:{ all -> 0x0079 }
            java.lang.String r0 = r4.toString()     // Catch:{ all -> 0x0079 }
            r2.<init>(r0, r5, r5)     // Catch:{ all -> 0x0079 }
            throw r2     // Catch:{ all -> 0x0079 }
        L_0x00d7:
            r3.shutdown()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.UserAgent.a(short, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map, java.io.File, boolean, int):com.jaunt.Document");
    }

    private HttpURLConnection a(String str, int i2, String str2, String str3, short s2, String str4, String str5, String str6, Map<String, String> map, int i3) throws MalformedURLException, IOException, ProtocolException {
        HttpURLConnection httpURLConnection;
        String str7 = str;
        String str8 = str3;
        if (!this.h.g) {
            try {
                HttpsURLConnection.setDefaultHostnameVerifier(this.B);
                SSLContext instance = SSLContext.getInstance("SSL");
                instance.init((KeyManager[]) null, this.y, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(instance.getSocketFactory());
            } catch (Exception e2) {
                if (this.h.c) {
                    System.out.println("java security exception: ".concat(String.valueOf(e2)));
                }
            }
        } else {
            HttpsURLConnection.setDefaultHostnameVerifier(this.A);
            HttpsURLConnection.setDefaultSSLSocketFactory(this.z);
        }
        String str9 = str2;
        URL url = new URL(str2);
        String host = url.getHost();
        int port = url.getPort();
        if (port != -1) {
            host = String.valueOf(host) + ":" + port;
        }
        String str10 = host;
        if (System.getProperty("https.proxyHost") != null || System.getProperty("http.proxyHost") != null) {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        } else if (str7 != null) {
            int i4 = i2;
            httpURLConnection = (HttpURLConnection) url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(str, i2)));
        } else {
            httpURLConnection = (HttpURLConnection) url.openConnection();
        }
        if (str3.equals("POST") || str3.equals("PUT")) {
            httpURLConnection.setDoOutput(true);
        }
        httpURLConnection.setRequestMethod(str3);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setInstanceFollowRedirects(false);
        if (!str3.equals("POST")) {
            str3.equals("PUT");
        } else if (s2 == 10) {
            httpURLConnection.setRequestProperty(TraktV2.HEADER_CONTENT_TYPE, "multipart/form-data; boundary=" + F);
            httpURLConnection.setDoInput(true);
            httpURLConnection.setUseCaches(false);
        } else {
            httpURLConnection.setRequestProperty("content-type", "application/x-www-form-urlencoded");
            httpURLConnection.setRequestProperty("content-length", String.valueOf(i3));
            httpURLConnection.setRequestProperty("cache-control", "no-cache");
        }
        a(httpURLConnection, str4, str5, str6, str10, map);
        return httpURLConnection;
    }

    private void a(HttpURLConnection httpURLConnection, String str, String str2, String str3, String str4, Map<String, String> map) {
        httpURLConnection.setRequestProperty("host", str4);
        ArrayList arrayList = new ArrayList(this.h.o.keySet());
        for (int i2 = 0; i2 < arrayList.size(); i2++) {
            String str5 = (String) arrayList.get(i2);
            httpURLConnection.setRequestProperty(str5, this.h.o.get(str5));
        }
        if (map != null) {
            ArrayList arrayList2 = new ArrayList(map.keySet());
            for (int i3 = 0; i3 < arrayList2.size(); i3++) {
                String str6 = (String) arrayList2.get(i3);
                httpURLConnection.setRequestProperty(str6, map.get(str6));
            }
        }
        if (str != null) {
            httpURLConnection.setRequestProperty("referer", str);
        }
        if (str2 != null) {
            httpURLConnection.setRequestProperty(CookieDBAdapter.CookieColumns.TABLE_NAME, str2);
        }
        if (str3 != null) {
            httpURLConnection.setRequestProperty("authorization", str3);
        }
    }

    private boolean a(MultiMap<String, String> multiMap, String str, String str2, short s2, File file, int i2) throws ResponseException {
        String str3 = str;
        String str4 = str2;
        short s3 = s2;
        List<String> c2 = multiMap.c("www-authenticate");
        if (c2.size() > 0) {
            if (c2.size() > 1 && this.h.c) {
                System.out.println("WARNING UserAgent.sendAuthorization; multiple www-authenticate headers; requestUrl: ".concat(String.valueOf(str)));
            }
            String trim = c2.get(0).trim();
            String lowerCase = trim.toLowerCase();
            if (lowerCase.startsWith(BuildConfig.FLAVOR)) {
                int indexOf = lowerCase.indexOf("realm", 5);
                int indexOf2 = lowerCase.indexOf("=", indexOf + 5);
                int indexOf3 = lowerCase.indexOf(34, indexOf2 + 1);
                int i3 = indexOf3 + 1;
                int indexOf4 = lowerCase.indexOf(34, i3);
                if (indexOf == -1 || indexOf2 == -1 || indexOf3 == -1 || indexOf4 == -1) {
                    if (this.h.c) {
                        PrintStream printStream = System.out;
                        printStream.println("WARNING UserAgent.sendAuthorization; malformed www-authenticate (basic) header at " + str3 + ":\n" + trim);
                    }
                    return false;
                }
                b bVar = this.q.get(new t(trim.substring(i3, indexOf4), str3));
                if (bVar != null) {
                    if (bVar.a().a() != null) {
                        if (this.h.c) {
                            PrintStream printStream2 = System.out;
                            printStream2.println("WARNING UserAgent.sendAuthorization; authenticator's realm:\n" + bVar.a().a() + "\nand requestUrl:\n" + str3 + "\nuser not authorized by authenticator, not resending.");
                        }
                        return false;
                    }
                    try {
                        bVar.a().a(d(str));
                    } catch (Exception e2) {
                        if (this.h.c) {
                            PrintStream printStream3 = System.out;
                            printStream3.println("WARNING UserAgent.sendAuthorization; invalid requestUrl: " + e2.toString() + ", proceeding");
                        }
                    }
                    if (s3 == 2) {
                        a(str3, file, i2, (String[]) null);
                        return true;
                    } else if (s3 == 1) {
                        a(str3, str4, (String[]) null);
                        return true;
                    } else if (s3 == 4) {
                        a(str3, (String[]) null);
                        return true;
                    } else if (s3 == 5) {
                        b(str3, str4, (String[]) null);
                        return true;
                    } else if (this.h.c) {
                        PrintStream printStream4 = System.out;
                        printStream4.println("WARNING UserAgent.sendAuthorization; unknown requestType: " + HttpRequest.a(s2));
                    }
                }
                return false;
            } else if (trim.startsWith("digest")) {
                if (this.h.c) {
                    PrintStream printStream5 = System.out;
                    printStream5.println("WARNING UserAgent.sendAuthorization; unsupported www-authenticate (digest) header at " + str3 + ":\n" + trim);
                }
                return false;
            } else if (trim.toUpperCase().startsWith("PASSPORT")) {
                if (this.h.c) {
                    PrintStream printStream6 = System.out;
                    printStream6.println("WARNING UserAgent.sendAuthorization; non-negotiable passport www-authentication header at " + str3 + ":\n" + trim);
                }
                return false;
            } else if (this.h.c) {
                PrintStream printStream7 = System.out;
                printStream7.println("WARNING UserAgent.sendAuthorization; malformed www-authenticate header at " + str3 + ":\n" + trim);
            }
        }
        return false;
    }

    private boolean a(HttpResponse httpResponse, String str, File file, int i2, short s2, Map<String, String> map, String str2) throws ResponseException {
        MultiMap<String, String> a2 = httpResponse.a();
        int b2 = httpResponse.b();
        List<String> c2 = a2.c("location");
        if (c2.size() > 0) {
            if (c2.size() > 1 && this.h.c) {
                System.out.println("WARNING UserAgent.followHttpRedirect; multiple location headers; baseUrl: ".concat(String.valueOf(str)));
            }
            try {
                String b3 = b(str, c2.get(0));
                int i3 = i2 + 1;
                if ((b2 == 307 || b2 == 302) && s2 == 1) {
                    String i4 = i(b3);
                    String b4 = b(b3);
                    b(this.s, this.t, b3, str2, (MultiMap<String, String>) null, (String) null, this.p, i4, b4, map, 0, a.POST, (MultiMap<String, File>) null);
                } else {
                    b(2, this.s, this.t, b3, this.p, i(b3), b(b3), map, file, true, i3);
                }
                return true;
            } catch (Exception unused) {
            }
        }
        return false;
    }

    static String b(String str, String str2) throws Exception {
        int i2;
        String str3;
        if (G.matcher(str2).find()) {
            return str2;
        }
        URL url = new URL(str);
        if (str2.startsWith("//")) {
            return e(new URL(String.valueOf(url.getProtocol()) + ":" + str2).toString());
        } else if (str2.startsWith("/")) {
            return e(new URL(url.getProtocol(), url.getHost(), url.getPort(), str2).toString());
        } else {
            if (str2.equals("")) {
                return "";
            }
            String path = url.getPath();
            String query = url.getQuery();
            if (query != null) {
                path = String.valueOf(path) + "?" + query;
            }
            int indexOf = path.indexOf(63);
            if (indexOf == -1) {
                i2 = path.lastIndexOf(47);
            } else {
                i2 = path.substring(0, indexOf).lastIndexOf(47);
            }
            if (i2 == -1) {
                str3 = "/".concat(String.valueOf(str2));
            } else if (!str2.startsWith("#") || path.endsWith("/")) {
                str3 = String.valueOf(path.substring(0, i2)) + "/" + str2;
            } else {
                str3 = String.valueOf(path) + str2;
            }
            return e(new URL(url.getProtocol(), url.getHost(), url.getPort(), str3).toString());
        }
    }

    private Document a(String str, int i2, String str2, String str3, String str4, String str5, String str6, Map<String, String> map, int i3) throws ResponseException {
        return b(str, i2, str2, (String) null, (MultiMap<String, String>) null, str3, str4, str5, str6, map, 0, a.PUT, (MultiMap<String, File>) null);
    }

    private void a(String str, MultiMap<String, String> multiMap) {
        List<String> c2 = multiMap.c("Set-Cookie");
        for (int i2 = 0; i2 < c2.size(); i2++) {
            try {
                this.k.a(new Cookie(str, c2.get(i2)));
            } catch (Exception e2) {
                if (this.h.c) {
                    PrintStream printStream = System.out;
                    printStream.println("WARNING addCookies; Exception creating cookie; e:\n" + IOUtil.a((Throwable) e2));
                }
            }
        }
    }

    public Document a(String str, String str2, String... strArr) throws ResponseException {
        String i2 = i(str);
        String b2 = b(str);
        UserAgentSettings userAgentSettings = this.h;
        long j2 = userAgentSettings.t;
        if (j2 > 0) {
            userAgentSettings.s = (int) j2;
            userAgentSettings.r = (int) j2;
            return a(this.s, this.t, str, str2, (MultiMap<String, String>) null, (String) null, this.p, i2, b2, a(strArr), 0, a.POST, (MultiMap<String, File>) null);
        }
        return b(this.s, this.t, str, str2, (MultiMap<String, String>) null, (String) null, this.p, i2, b2, a(strArr), 0, a.POST, (MultiMap<String, File>) null);
    }

    /* JADX WARNING: Missing exception handler attribute for start block: B:6:0x005c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jaunt.Document a(java.lang.String r1, int r2, java.lang.String r3, java.lang.String r4, com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r5, java.lang.String r6, java.lang.String r7, java.lang.String r8, java.lang.String r9, java.util.Map<java.lang.String, java.lang.String> r10, int r11, com.jaunt.UserAgent.a r12, com.jaunt.util.MultiMap<java.lang.String, java.io.File> r13) throws com.jaunt.ResponseException {
        /*
            r0 = this;
            java.lang.String r5 = "UserAgent.do"
            r6 = 13
            java.lang.Object[] r6 = new java.lang.Object[r6]
            r11 = 0
            r6[r11] = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r2)
            r2 = 1
            r6[r2] = r1
            r1 = 2
            r6[r1] = r3
            r1 = 3
            r6[r1] = r4
            r1 = 0
            r2 = 4
            r6[r2] = r1
            r2 = 5
            r6[r2] = r1
            r2 = 6
            r6[r2] = r7
            r2 = 7
            r6[r2] = r8
            r2 = 8
            r6[r2] = r9
            r2 = 9
            r6[r2] = r10
            java.lang.Integer r2 = java.lang.Integer.valueOf(r11)
            r4 = 10
            r6[r4] = r2
            r2 = 11
            r6[r2] = r12
            r2 = 12
            r6[r2] = r1
            r0.f4926a = r6
            java.util.concurrent.ExecutorService r2 = java.util.concurrent.Executors.newCachedThreadPool()
            com.jaunt.UserAgent$5 r4 = new com.jaunt.UserAgent$5
            r4.<init>()
            java.util.concurrent.Future r4 = r2.submit(r4)
            com.jaunt.UserAgentSettings r6 = r0.h     // Catch:{ ExecutionException -> 0x008a, TimeoutException -> 0x0073, InterruptedException -> 0x005c }
            long r6 = r6.t     // Catch:{ ExecutionException -> 0x008a, TimeoutException -> 0x0073, InterruptedException -> 0x005c }
            java.util.concurrent.TimeUnit r8 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ ExecutionException -> 0x008a, TimeoutException -> 0x0073, InterruptedException -> 0x005c }
            java.lang.Object r4 = r4.get(r6, r8)     // Catch:{ ExecutionException -> 0x008a, TimeoutException -> 0x0073, InterruptedException -> 0x005c }
            com.jaunt.Document r4 = (com.jaunt.Document) r4     // Catch:{ ExecutionException -> 0x008a, TimeoutException -> 0x0073, InterruptedException -> 0x005c }
            r2.shutdown()
            return r4
        L_0x005a:
            r1 = move-exception
            goto L_0x0092
        L_0x005c:
            com.jaunt.ResponseException r4 = new com.jaunt.ResponseException     // Catch:{ all -> 0x005a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
            r6.<init>(r5)     // Catch:{ all -> 0x005a }
            r6.append(r12)     // Catch:{ all -> 0x005a }
            java.lang.String r5 = "; interrupted"
            r6.append(r5)     // Catch:{ all -> 0x005a }
            java.lang.String r5 = r6.toString()     // Catch:{ all -> 0x005a }
            r4.<init>(r5, r1, r3)     // Catch:{ all -> 0x005a }
            throw r4     // Catch:{ all -> 0x005a }
        L_0x0073:
            com.jaunt.ResponseException r4 = new com.jaunt.ResponseException     // Catch:{ all -> 0x005a }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x005a }
            r6.<init>(r5)     // Catch:{ all -> 0x005a }
            r6.append(r12)     // Catch:{ all -> 0x005a }
            java.lang.String r5 = "; response timed out"
            r6.append(r5)     // Catch:{ all -> 0x005a }
            java.lang.String r5 = r6.toString()     // Catch:{ all -> 0x005a }
            r4.<init>(r5, r1, r3)     // Catch:{ all -> 0x005a }
            throw r4     // Catch:{ all -> 0x005a }
        L_0x008a:
            r1 = move-exception
            java.lang.Throwable r1 = r1.getCause()     // Catch:{ all -> 0x005a }
            com.jaunt.ResponseException r1 = (com.jaunt.ResponseException) r1     // Catch:{ all -> 0x005a }
            throw r1     // Catch:{ all -> 0x005a }
        L_0x0092:
            r2.shutdown()
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.UserAgent.a(java.lang.String, int, java.lang.String, java.lang.String, com.jaunt.util.MultiMap, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map, int, com.jaunt.UserAgent$a, com.jaunt.util.MultiMap):com.jaunt.Document");
    }

    public Document b(String str, String str2, String... strArr) throws ResponseException {
        return a(this.s, this.t, str, str2, this.p, i(str), b(str), a(strArr), 0);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v12, resolved type: int} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v13, resolved type: int} */
    /* JADX WARNING: type inference failed for: r10v9 */
    /* JADX WARNING: type inference failed for: r10v11 */
    /* JADX WARNING: type inference failed for: r4v36 */
    /* JADX WARNING: type inference failed for: r4v37 */
    /* JADX WARNING: type inference failed for: r10v16 */
    /* JADX WARNING: type inference failed for: r10v41 */
    /* JADX WARNING: type inference failed for: r10v42 */
    /* JADX WARNING: type inference failed for: r10v43 */
    /* JADX WARNING: type inference failed for: r10v44 */
    /* JADX WARNING: type inference failed for: r10v45 */
    /* JADX WARNING: type inference failed for: r10v46 */
    /* JADX WARNING: type inference failed for: r10v47 */
    /* JADX WARNING: type inference failed for: r4v74 */
    /* JADX WARNING: Code restructure failed: missing block: B:113:0x030b, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:114:0x030c, code lost:
        r1 = r0;
        r14 = r20;
        r3 = r21;
        r4 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:115:0x0315, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:116:0x0316, code lost:
        r10 = 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:117:0x0317, code lost:
        r1 = r0;
        r14 = r20;
        r10 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:124:0x032d, code lost:
        r0 = e;
        r10 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x039c, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:155:0x039f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x03a0, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:157:0x03a1, code lost:
        r1 = r0;
        r14 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:171:0x03ec, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:191:0x0457, code lost:
        r14 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:193:0x045b, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:194:0x045c, code lost:
        r14 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:213:0x0550, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:218:0x055d, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:219:0x055e, code lost:
        r14 = r20;
        r10 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:227:0x057e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:228:0x057f, code lost:
        r4 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:231:0x05ac, code lost:
        r0 = e;
        r4 = r4;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:232:0x05ae, code lost:
        r0 = e;
        r10 = r10;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:233:0x05b1, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:234:0x05b2, code lost:
        r14 = r20;
        r4 = r22;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:235:0x05b7, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:236:0x05b8, code lost:
        r14 = r20;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:238:0x05be, code lost:
        r14 = r20;
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:240:0x05c2, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:241:0x05c3, code lost:
        r14 = r20;
        r4 = r22;
        r9 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:242:0x05c9, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:243:0x05ca, code lost:
        r1 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:249:0x05dc, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:250:0x05dd, code lost:
        r8 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:258:?, code lost:
        r7.getInputStream().close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:262:0x060f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:264:0x0611, code lost:
        throw r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:271:0x0632, code lost:
        r12.g = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:272:0x064c, code lost:
        throw new com.jaunt.ResponseException(r14 + r15 + "; Connection error - invalid SSL certificate (to accept invalid SSL certificates, see UserAgentSettings.checkSSLCerts)", r12.g, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:273:0x064d, code lost:
        r12.g = r11;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:274:0x064f, code lost:
        if (r7 != null) goto L_0x0651;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:276:?, code lost:
        r9.a(com.jaunt.util.IOUtil.a(r7.getErrorStream()));
        r12.g = r9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:281:0x067a, code lost:
        throw new com.jaunt.ResponseException(r14 + r15 + r4, r12.g, r13);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:76:0x024f, code lost:
        r0 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:77:0x0250, code lost:
        r1 = r0;
        r14 = r20;
        r3 = r21;
        r4 = r22;
        r9 = r23;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x0259, code lost:
        r11 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:90:0x027f, code lost:
        r0 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x0280, code lost:
        r8 = r19;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:99:0x029e, code lost:
        r0 = e;
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:55:0x0156, B:85:0x026b, B:100:0x02a0] */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [B:55:0x0156, B:88:0x0272, B:100:0x02a0] */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x030b A[Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }, ExcHandler: IOException (r0v29 'e' java.io.IOException A[CUSTOM_DECLARE, Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }]), Splitter:B:108:0x02fd] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x032d A[ExcHandler: NullPointerException (e java.lang.NullPointerException), PHI: r10 
      PHI: (r10v11 ?) = (r10v9 ?), (r10v16 ?), (r10v41 ?), (r10v42 ?), (r10v43 ?), (r10v44 ?), (r10v45 ?), (r10v46 ?), (r10v47 ?) binds: [B:108:0x02fd, B:111:0x0308, B:176:0x03f6, B:162:0x03af, B:142:0x0366, B:150:0x038f, B:151:?, B:145:0x0379, B:128:0x0339] A[DONT_GENERATE, DONT_INLINE], Splitter:B:111:0x0308] */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0344  */
    /* JADX WARNING: Removed duplicated region for block: B:192:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:159:0x03a9] */
    /* JADX WARNING: Removed duplicated region for block: B:212:0x0546 A[Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }] */
    /* JADX WARNING: Removed duplicated region for block: B:218:0x055d A[ExcHandler: NullPointerException (e java.lang.NullPointerException), Splitter:B:125:0x032f] */
    /* JADX WARNING: Removed duplicated region for block: B:232:0x05ae A[ExcHandler: NullPointerException (e java.lang.NullPointerException), PHI: r14 
      PHI: (r14v24 java.lang.String) = (r14v25 java.lang.String), (r14v25 java.lang.String), (r14v25 java.lang.String), (r14v34 java.lang.String), (r14v40 java.lang.String) binds: [B:221:0x0565, B:222:?, B:224:0x0573, B:159:0x03a9, B:188:0x043e] A[DONT_GENERATE, DONT_INLINE], Splitter:B:188:0x043e] */
    /* JADX WARNING: Removed duplicated region for block: B:239:? A[ExcHandler: Exception (unused java.lang.Exception), SYNTHETIC, Splitter:B:100:0x02a0] */
    /* JADX WARNING: Removed duplicated region for block: B:240:0x05c2 A[ExcHandler: IOException (e java.io.IOException), Splitter:B:52:0x0135] */
    /* JADX WARNING: Removed duplicated region for block: B:249:0x05dc A[ExcHandler: NullPointerException (e java.lang.NullPointerException), Splitter:B:50:0x0131] */
    /* JADX WARNING: Removed duplicated region for block: B:256:? A[ExcHandler: Exception (unused java.lang.Exception), PHI: r11 r14 
      PHI: (r11v13 com.jaunt.HttpResponse) = (r11v14 com.jaunt.HttpResponse), (r11v14 com.jaunt.HttpResponse), (r11v14 com.jaunt.HttpResponse), (r11v23 com.jaunt.HttpResponse), (r11v23 com.jaunt.HttpResponse) binds: [B:221:0x0565, B:222:?, B:224:0x0573, B:159:0x03a9, B:188:0x043e] A[DONT_GENERATE, DONT_INLINE]
      PHI: (r14v23 java.lang.String) = (r14v25 java.lang.String), (r14v25 java.lang.String), (r14v25 java.lang.String), (r14v34 java.lang.String), (r14v40 java.lang.String) binds: [B:221:0x0565, B:222:?, B:224:0x0573, B:159:0x03a9, B:188:0x043e] A[DONT_GENERATE, DONT_INLINE], SYNTHETIC, Splitter:B:188:0x043e] */
    /* JADX WARNING: Removed duplicated region for block: B:257:0x05ed A[SYNTHETIC, Splitter:B:257:0x05ed] */
    /* JADX WARNING: Removed duplicated region for block: B:262:0x060f A[ExcHandler: ResponseException (r0v6 'e' com.jaunt.ResponseException A[CUSTOM_DECLARE]), Splitter:B:100:0x02a0] */
    /* JADX WARNING: Removed duplicated region for block: B:271:0x0632  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x064d  */
    /* JADX WARNING: Removed duplicated region for block: B:280:0x0664  */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x06ca  */
    /* JADX WARNING: Removed duplicated region for block: B:292:0x06ce  */
    /* JADX WARNING: Removed duplicated region for block: B:301:0x070a  */
    /* JADX WARNING: Removed duplicated region for block: B:302:0x072e  */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x0757  */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x07ab  */
    /* JADX WARNING: Removed duplicated region for block: B:319:0x07ff  */
    /* JADX WARNING: Removed duplicated region for block: B:322:0x0809  */
    /* JADX WARNING: Removed duplicated region for block: B:42:0x0104  */
    /* JADX WARNING: Removed duplicated region for block: B:76:0x024f A[ExcHandler: IOException (r0v34 'e' java.io.IOException A[CUSTOM_DECLARE]), Splitter:B:55:0x0156] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.jaunt.Document b(java.lang.String r25, int r26, java.lang.String r27, java.lang.String r28, com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r29, java.lang.String r30, java.lang.String r31, java.lang.String r32, java.lang.String r33, java.util.Map<java.lang.String, java.lang.String> r34, int r35, com.jaunt.UserAgent.a r36, com.jaunt.util.MultiMap<java.lang.String, java.io.File> r37) throws com.jaunt.ResponseException {
        /*
            r24 = this;
            r12 = r24
            r13 = r27
            r14 = r29
            r15 = r35
            r11 = r36
            r10 = r37
            java.lang.String r9 = "; Connection error"
            r8 = 0
            r12.x = r8
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.POST
            if (r11 != r1) goto L_0x001a
            r1 = 11
            r6 = 11
            goto L_0x0026
        L_0x001a:
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.MULTIPART_POST
            if (r11 != r1) goto L_0x0023
            r1 = 10
            r6 = 10
            goto L_0x0026
        L_0x0023:
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.PUT
            r6 = -1
        L_0x0026:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.f
            r5 = 0
            if (r1 == 0) goto L_0x0030
            r12.a((com.jaunt.h) r5)
        L_0x0030:
            java.lang.String r1 = ""
            if (r28 != 0) goto L_0x0036
            r4 = r1
            goto L_0x0038
        L_0x0036:
            r4 = r28
        L_0x0038:
            com.jaunt.UserAgent$a r2 = com.jaunt.UserAgent.a.MULTIPART_POST
            java.lang.String r3 = "&"
            java.lang.String r5 = "="
            if (r11 != r2) goto L_0x0047
            if (r10 == 0) goto L_0x0047
            java.lang.String r2 = r10.a(r5, r3, r8, r1)
            goto L_0x0048
        L_0x0047:
            r2 = r1
        L_0x0048:
            com.jaunt.UserAgentSettings r7 = r12.h
            boolean r8 = r7.e
            if (r8 != 0) goto L_0x0056
            boolean r7 = r7.d
            if (r7 == 0) goto L_0x0053
            goto L_0x0056
        L_0x0053:
            r18 = r9
            goto L_0x0073
        L_0x0056:
            java.io.PrintStream r7 = java.lang.System.out
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r18 = r9
            java.lang.String r9 = "Sending "
            r8.<init>(r9)
            r8.append(r11)
            java.lang.String r9 = " request to "
            r8.append(r9)
            r8.append(r13)
            java.lang.String r8 = r8.toString()
            r7.println(r8)
        L_0x0073:
            com.jaunt.UserAgent$a r7 = com.jaunt.UserAgent.a.MULTIPART_POST
            java.lang.String r8 = "]"
            if (r11 != r7) goto L_0x00a6
            com.jaunt.HttpResponse r7 = new com.jaunt.HttpResponse
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            java.lang.String r10 = java.lang.String.valueOf(r27)
            r9.<init>(r10)
            java.lang.String r10 = " [multipart-posting "
            r9.append(r10)
            r10 = 0
            java.lang.String r1 = r14.a(r5, r3, r10, r1)
            r9.append(r1)
            java.lang.String r1 = " "
            r9.append(r1)
            r9.append(r2)
            r9.append(r8)
            java.lang.String r1 = r9.toString()
            r7.<init>(r1)
            r10 = r7
        L_0x00a4:
            r8 = 0
            goto L_0x00f7
        L_0x00a6:
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.POST
            if (r11 != r1) goto L_0x00ea
            int r1 = r4.length()
            r2 = 200(0xc8, float:2.8E-43)
            if (r1 <= r2) goto L_0x00ca
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r3 = 0
            java.lang.String r2 = r4.substring(r3, r2)
            java.lang.String r2 = java.lang.String.valueOf(r2)
            r1.<init>(r2)
            java.lang.String r2 = "..."
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            goto L_0x00cb
        L_0x00ca:
            r1 = r4
        L_0x00cb:
            com.jaunt.HttpResponse r2 = new com.jaunt.HttpResponse
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            java.lang.String r5 = java.lang.String.valueOf(r27)
            r3.<init>(r5)
            java.lang.String r5 = " [posting "
            r3.append(r5)
            r3.append(r1)
            r3.append(r8)
            java.lang.String r1 = r3.toString()
            r2.<init>(r1)
            r10 = r2
            goto L_0x00a4
        L_0x00ea:
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.PUT
            if (r11 != r1) goto L_0x00f5
            com.jaunt.HttpResponse r1 = new com.jaunt.HttpResponse
            r1.<init>(r13)
            r10 = r1
            goto L_0x00a4
        L_0x00f5:
            r8 = 0
            r10 = 0
        L_0x00f7:
            r10.a((boolean) r8)
            com.jaunt.UserAgentSettings r1 = r12.h
            int r1 = r1.m
            java.lang.String r9 = "; response error"
            java.lang.String r7 = "UserAgent.do"
            if (r15 > r1) goto L_0x0809
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.PUT
            if (r11 != r1) goto L_0x010b
            java.lang.String r1 = "PUT"
            goto L_0x010d
        L_0x010b:
            java.lang.String r1 = "POST"
        L_0x010d:
            r5 = r1
            r3 = 1
            int r17 = r4.length()     // Catch:{ NullPointerException -> 0x0692, IOException -> 0x0612, ResponseException -> 0x060f, Exception -> 0x05e5 }
            r1 = r24
            r2 = r25
            r15 = 1
            r3 = r26
            r19 = r4
            r4 = r27
            r20 = r7
            r7 = r31
            r8 = r32
            r22 = r9
            r21 = r18
            r9 = r33
            r23 = r10
            r10 = r34
            r15 = r11
            r11 = r17
            java.net.HttpURLConnection r7 = r1.a(r2, r3, r4, r5, r6, r7, r8, r9, r10, r11)     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05cf, ResponseException -> 0x060f, Exception -> 0x05cc }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            int r1 = r1.r     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            r7.setConnectTimeout(r1)     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            int r1 = r1.s     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            r7.setReadTimeout(r1)     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            r3 = 2
            r5 = -1
            r6 = 0
            r1 = r24
            r2 = r7
            r4 = r27
            r1.a((java.net.HttpURLConnection) r2, (int) r3, (java.lang.String) r4, (int) r5, (java.lang.String) r6)     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.OutputStream r1 = r7.getOutputStream()     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.UserAgent$a r2 = com.jaunt.UserAgent.a.MULTIPART_POST     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r15 != r2) goto L_0x0267
            java.io.PrintWriter r2 = new java.io.PrintWriter     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.OutputStreamWriter r3 = new java.io.OutputStreamWriter     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = r4.f4931a     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r4 = 1
            r2.<init>(r3, r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Set r4 = r29.a()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
        L_0x0172:
            boolean r4 = r3.hasNext()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r5 = "--"
            java.lang.String r6 = "\r\n"
            if (r4 != 0) goto L_0x01d8
            java.util.ArrayList r3 = new java.util.ArrayList     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Set r4 = r37.a()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.<init>(r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Iterator r3 = r3.iterator()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
        L_0x0189:
            boolean r4 = r3.hasNext()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r4 != 0) goto L_0x01b7
            r2.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.flush()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.<init>(r5)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = F     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r5)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.PrintWriter r3 = r2.append(r3)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.close()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r1.close()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r8 = r19
            goto L_0x02a0
        L_0x01b7:
            java.lang.Object r4 = r3.next()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r8 = r37
            java.util.List r9 = r8.a(r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
        L_0x01c7:
            boolean r10 = r9.hasNext()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r10 != 0) goto L_0x01ce
            goto L_0x0189
        L_0x01ce:
            java.lang.Object r10 = r9.next()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.File r10 = (java.io.File) r10     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            a((java.io.OutputStream) r1, (java.io.PrintWriter) r2, (java.lang.String) r4, (java.io.File) r10)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            goto L_0x01c7
        L_0x01d8:
            r8 = r37
            java.lang.Object r4 = r3.next()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = (java.lang.String) r4     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.List r9 = r14.a(r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Iterator r9 = r9.iterator()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
        L_0x01e8:
            boolean r10 = r9.hasNext()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r10 != 0) goto L_0x01ef
            goto L_0x0172
        L_0x01ef:
            java.lang.Object r10 = r9.next()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r10 = (java.lang.String) r10     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.StringBuilder r11 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r11.<init>(r5)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r25 = r3
            java.lang.String r3 = F     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r11.append(r3)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = r11.toString()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.PrintWriter r3 = r2.append(r3)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r11 = "Content-Disposition: form-data; name=\""
            r3.<init>(r11)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r4)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r11 = "\""
            r3.append(r11)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.PrintWriter r3 = r2.append(r3)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r11 = "Content-Type: text/plain; charset="
            r3.<init>(r11)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.UserAgentSettings r11 = r12.h     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r11 = r11.f4931a     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r11)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = r3.toString()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.PrintWriter r3 = r2.append(r3)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.PrintWriter r3 = r2.append(r10)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.flush()     // Catch:{ NullPointerException -> 0x025c, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3 = r25
            goto L_0x01e8
        L_0x024f:
            r0 = move-exception
            r1 = r0
            r14 = r20
            r3 = r21
            r4 = r22
            r9 = r23
        L_0x0259:
            r11 = 0
            goto L_0x061c
        L_0x025c:
            r0 = move-exception
            r1 = r0
            r8 = r19
        L_0x0260:
            r14 = r20
            r9 = r23
            r10 = 0
            goto L_0x0699
        L_0x0267:
            com.jaunt.UserAgent$a r2 = com.jaunt.UserAgent.a.POST     // Catch:{ NullPointerException -> 0x05dc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r15 != r2) goto L_0x0284
            java.io.PrintWriter r2 = new java.io.PrintWriter     // Catch:{ NullPointerException -> 0x027f, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.<init>(r1)     // Catch:{ NullPointerException -> 0x027f, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r8 = r19
            r2.print(r8)     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.flush()     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.close()     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r1.close()     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            goto L_0x02a0
        L_0x027f:
            r0 = move-exception
            r8 = r19
        L_0x0282:
            r1 = r0
            goto L_0x0260
        L_0x0284:
            r8 = r19
            com.jaunt.UserAgent$a r2 = com.jaunt.UserAgent.a.PUT     // Catch:{ NullPointerException -> 0x05bc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r15 != r2) goto L_0x02a0
            java.io.PrintWriter r2 = new java.io.PrintWriter     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.<init>(r1)     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r3 = r30
            r2.print(r3)     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.flush()     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r2.close()     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            r1.close()     // Catch:{ NullPointerException -> 0x029e, IOException -> 0x024f, ResponseException -> 0x060f, Exception -> 0x05be }
            goto L_0x02a0
        L_0x029e:
            r0 = move-exception
            goto L_0x0282
        L_0x02a0:
            int r1 = r7.getResponseCode()     // Catch:{ NullPointerException -> 0x05bc, IOException -> 0x05c2, ResponseException -> 0x060f, Exception -> 0x05be }
            r9 = r23
            r9.a((int) r1)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r2 = r7.getResponseMessage()     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            r9.a((java.lang.String) r2)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            r3 = 1
            r4 = 0
            r28 = r24
            r29 = r7
            r30 = r3
            r31 = r4
            r32 = r1
            r33 = r2
            r28.a((java.net.HttpURLConnection) r29, (int) r30, (java.lang.String) r31, (int) r32, (java.lang.String) r33)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r2 = r12.b     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            r9.a((com.jaunt.util.MultiMap<java.lang.String, java.lang.String>) r2)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            java.io.InputStream r2 = r7.getInputStream()     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = "date"
            java.lang.String r3 = r9.b(r3)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            java.util.Date r4 = new java.util.Date     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            r4.<init>()     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            long r4 = r4.getTime()     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            r10 = 2764800000(0xa4cb8000, double:1.3659926976E-314)
            r28 = r24
            r29 = r3
            r30 = r4
            r32 = r10
            boolean r4 = r28.a((java.lang.String) r29, (long) r30, (long) r32)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r4 == 0) goto L_0x0582
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r3 = r12.b     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            r12.a((java.lang.String) r13, (com.jaunt.util.MultiMap<java.lang.String, java.lang.String>) r3)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r3 = r12.b     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = "content-type"
            java.lang.Object r3 = r3.b(r4)     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ NullPointerException -> 0x05b7, IOException -> 0x05b1, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r3 == 0) goto L_0x031c
            java.lang.String r3 = r3.toLowerCase()     // Catch:{ NullPointerException -> 0x0315, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = ";"
            java.lang.String[] r4 = r3.split(r4)     // Catch:{ NullPointerException -> 0x0315, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            r10 = 0
            r5 = r4[r10]     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            goto L_0x031e
        L_0x030b:
            r0 = move-exception
            r1 = r0
            r14 = r20
            r3 = r21
            r4 = r22
            goto L_0x0259
        L_0x0315:
            r0 = move-exception
            r10 = 0
        L_0x0317:
            r1 = r0
            r14 = r20
            goto L_0x0699
        L_0x031c:
            r10 = 0
            r5 = 0
        L_0x031e:
            r4 = 400(0x190, float:5.6E-43)
            if (r1 >= r4) goto L_0x0562
            if (r3 == 0) goto L_0x032f
            java.lang.String r1 = "text/plain"
            boolean r1 = r3.startsWith(r1)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r1 == 0) goto L_0x0341
            goto L_0x032f
        L_0x032d:
            r0 = move-exception
            goto L_0x0317
        L_0x032f:
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = "location"
            java.util.List r1 = r1.a(r4)     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r1 == 0) goto L_0x0341
            int r1 = r1.size()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r1 <= 0) goto L_0x0341
            r1 = 1
            goto L_0x0342
        L_0x0341:
            r1 = 0
        L_0x0342:
            if (r1 != 0) goto L_0x0546
            if (r3 == 0) goto L_0x0527
            java.util.Map<java.lang.String, com.jaunt.util.Handler> r1 = r12.r     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.Object r1 = r1.get(r5)     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.util.Handler r1 = (com.jaunt.util.Handler) r1     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r4 = r4.f4931a     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.nio.charset.Charset r4 = java.nio.charset.Charset.forName(r4)     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r6 = ")"
            if (r1 == 0) goto L_0x0460
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r3 = r12.b     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r11 = "content-encoding"
            java.lang.Object r3 = r3.b(r11)     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            java.lang.String r3 = (java.lang.String) r3     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x0552, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r3 != 0) goto L_0x03a6
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r3.<init>(r2, r4)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r4.<init>(r3)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r1.a(r12, r9, r2, r4)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            boolean r5 = j(r5)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            if (r5 == 0) goto L_0x038f
            java.lang.Object r1 = r1.getContent()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.JNode r1 = (com.jaunt.JNode) r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            r12.j = r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            boolean r1 = r1.j     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            if (r1 == 0) goto L_0x038c
            com.jaunt.JNode r1 = r12.j     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
            r12.a((com.jaunt.JNode) r1)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
        L_0x038c:
            r1 = 1
            r12.x = r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x030b, ResponseException -> 0x060f, Exception -> 0x05be }
        L_0x038f:
            r4.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r3.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r2.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r12.g = r9     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x039f, ResponseException -> 0x060f, Exception -> 0x039c }
            r11 = 0
            return r11
        L_0x039c:
            r11 = 0
            goto L_0x0457
        L_0x039f:
            r0 = move-exception
            r11 = 0
        L_0x03a1:
            r1 = r0
            r14 = r20
            goto L_0x0557
        L_0x03a6:
            r11 = 0
            java.lang.String r14 = "gzip"
            boolean r14 = r3.startsWith(r14)     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x045b, ResponseException -> 0x060f, Exception -> 0x0457 }
            if (r14 == 0) goto L_0x03ee
            java.util.zip.GZIPInputStream r3 = new java.util.zip.GZIPInputStream     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r3.<init>(r2)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r6.<init>(r3, r4)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r4.<init>(r6)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r1.a(r12, r9, r2, r4)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            boolean r5 = j(r5)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            if (r5 == 0) goto L_0x03dd
            java.lang.Object r1 = r1.getContent()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            com.jaunt.JNode r1 = (com.jaunt.JNode) r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.j = r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            boolean r1 = r1.j     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            if (r1 == 0) goto L_0x03da
            com.jaunt.JNode r1 = r12.j     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.a((com.jaunt.JNode) r1)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
        L_0x03da:
            r1 = 1
            r12.x = r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
        L_0x03dd:
            r4.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r6.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r3.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r2.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            return r11
        L_0x03ec:
            r0 = move-exception
            goto L_0x03a1
        L_0x03ee:
            java.lang.String r14 = "deflate"
            boolean r14 = r3.startsWith(r14)     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x045b, ResponseException -> 0x060f, Exception -> 0x0457 }
            if (r14 == 0) goto L_0x0433
            java.util.zip.InflaterInputStream r3 = new java.util.zip.InflaterInputStream     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r3.<init>(r2)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            java.io.InputStreamReader r6 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r6.<init>(r3, r4)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r4.<init>(r6)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r1.a(r12, r9, r2, r4)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            boolean r5 = j(r5)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            if (r5 == 0) goto L_0x0424
            java.lang.Object r1 = r1.getContent()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            com.jaunt.JNode r1 = (com.jaunt.JNode) r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.j = r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            com.jaunt.UserAgentSettings r1 = r12.h     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            boolean r1 = r1.j     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            if (r1 == 0) goto L_0x0421
            com.jaunt.JNode r1 = r12.j     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.a((com.jaunt.JNode) r1)     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
        L_0x0421:
            r1 = 1
            r12.x = r1     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
        L_0x0424:
            r4.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r6.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r3.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r2.close()     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x032d, IOException -> 0x03ec, ResponseException -> 0x060f, Exception -> 0x0457 }
            return r11
        L_0x0433:
            r2.close()     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x045b, ResponseException -> 0x060f, Exception -> 0x0457 }
            r12.g = r9     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x045b, ResponseException -> 0x060f, Exception -> 0x0457 }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x045b, ResponseException -> 0x060f, Exception -> 0x0457 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x055d, IOException -> 0x045b, ResponseException -> 0x060f, Exception -> 0x0457 }
            r14 = r20
            r2.<init>(r14)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r15)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r4 = "; response contains unsupported content-encoding ("
            r2.append(r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r6)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2, r9, r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            throw r1     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
        L_0x0457:
            r14 = r20
            goto L_0x05eb
        L_0x045b:
            r0 = move-exception
            r14 = r20
            goto L_0x0556
        L_0x0460:
            r14 = r20
            r11 = 0
            boolean r1 = c((java.lang.String) r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            if (r1 == 0) goto L_0x0505
            com.jaunt.s r1 = r12.o     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.UserAgentSettings r3 = r12.h     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            int r3 = r3.n     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.a((int) r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r1 = r12.b     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r3 = "content-encoding"
            java.lang.Object r1 = r1.b(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            if (r1 != 0) goto L_0x0495
            java.io.InputStreamReader r1 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2, r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.io.BufferedReader r3 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.<init>(r1)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.s r4 = r12.o     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r4.a((java.io.BufferedReader) r3, (java.lang.String) r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            goto L_0x06bc
        L_0x0495:
            java.lang.String r3 = "gzip"
            boolean r3 = r1.startsWith(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            if (r3 == 0) goto L_0x04bc
            java.util.zip.GZIPInputStream r1 = new java.util.zip.GZIPInputStream     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r4.<init>(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.s r5 = r12.o     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r5.a((java.io.BufferedReader) r4, (java.lang.String) r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            goto L_0x06bc
        L_0x04bc:
            java.lang.String r3 = "deflate"
            boolean r3 = r1.startsWith(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            if (r3 == 0) goto L_0x04e3
            java.util.zip.InflaterInputStream r1 = new java.util.zip.InflaterInputStream     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.<init>(r1, r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.io.BufferedReader r4 = new java.io.BufferedReader     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r4.<init>(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.s r5 = r12.o     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r5.a((java.io.BufferedReader) r4, (java.lang.String) r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            goto L_0x06bc
        L_0x04e3:
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r12.g = r9     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.ResponseException r2 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.<init>(r14)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.append(r15)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r4 = "; response has unsupported content-encoding("
            r3.append(r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.append(r1)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r3.append(r6)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r1 = r3.toString()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.<init>(r1, r9, r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            throw r2     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
        L_0x0505:
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r12.g = r9     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.<init>(r14)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r15)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r4 = "; response contains unsupported content-type ("
            r2.append(r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r6)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2, r9, r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            throw r1     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
        L_0x0527:
            r14 = r20
            r11 = 0
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r12.g = r9     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.<init>(r14)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r15)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r3 = "; response fails to specify content-type"
            r2.append(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2, r9, r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            throw r1     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
        L_0x0546:
            r14 = r20
            r11 = 0
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            r12.g = r9     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x0550, ResponseException -> 0x060f, Exception -> 0x05eb }
            goto L_0x06bc
        L_0x0550:
            r0 = move-exception
            goto L_0x0556
        L_0x0552:
            r0 = move-exception
            r14 = r20
            r11 = 0
        L_0x0556:
            r1 = r0
        L_0x0557:
            r3 = r21
            r4 = r22
            goto L_0x061c
        L_0x055d:
            r0 = move-exception
            r14 = r20
            goto L_0x0698
        L_0x0562:
            r14 = r20
            r11 = 0
            r12.g = r9     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x057e, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x057e, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x057e, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.<init>(r14)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x057e, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r15)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x057e, ResponseException -> 0x060f, Exception -> 0x05eb }
            r4 = r22
            r2.append(r4)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2, r9, r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            throw r1     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
        L_0x057e:
            r0 = move-exception
            r4 = r22
            goto L_0x05ca
        L_0x0582:
            r14 = r20
            r4 = r22
            r10 = 0
            r11 = 0
            r2.close()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            r12.g = r9     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.<init>(r14)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r15)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r5 = "; response date "
            r2.append(r5)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            r2.append(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r3 = " cannot be reconciled"
            r2.append(r3)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            java.lang.String r2 = r2.toString()     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            r1.<init>(r2, r9, r13)     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
            throw r1     // Catch:{ NullPointerException -> 0x05ae, IOException -> 0x05ac, ResponseException -> 0x060f, Exception -> 0x05eb }
        L_0x05ac:
            r0 = move-exception
            goto L_0x05ca
        L_0x05ae:
            r0 = move-exception
            goto L_0x0698
        L_0x05b1:
            r0 = move-exception
            r14 = r20
            r4 = r22
            goto L_0x05c9
        L_0x05b7:
            r0 = move-exception
            r14 = r20
            goto L_0x0697
        L_0x05bc:
            r0 = move-exception
            goto L_0x05df
        L_0x05be:
            r14 = r20
            r11 = 0
            goto L_0x05eb
        L_0x05c2:
            r0 = move-exception
            r14 = r20
            r4 = r22
            r9 = r23
        L_0x05c9:
            r11 = 0
        L_0x05ca:
            r1 = r0
            goto L_0x05d9
        L_0x05cc:
            r14 = r20
            goto L_0x05e9
        L_0x05cf:
            r0 = move-exception
            r14 = r20
            r4 = r22
            r9 = r23
            r11 = 0
            r1 = r0
            r7 = r11
        L_0x05d9:
            r3 = r21
            goto L_0x061c
        L_0x05dc:
            r0 = move-exception
            r8 = r19
        L_0x05df:
            r14 = r20
            r9 = r23
            goto L_0x0697
        L_0x05e5:
            r14 = r7
            r15 = r11
            r21 = r18
        L_0x05e9:
            r11 = 0
            r7 = r11
        L_0x05eb:
            if (r7 == 0) goto L_0x05f4
            java.io.InputStream r1 = r7.getInputStream()     // Catch:{ Exception -> 0x05f4 }
            r1.close()     // Catch:{ Exception -> 0x05f4 }
        L_0x05f4:
            r12.g = r11
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            r3 = r21
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r13)
            throw r1
        L_0x060f:
            r0 = move-exception
            r1 = r0
            throw r1
        L_0x0612:
            r0 = move-exception
            r14 = r7
            r4 = r9
            r9 = r10
            r15 = r11
            r3 = r18
            r11 = 0
            r1 = r0
            r7 = r11
        L_0x061c:
            boolean r2 = r1 instanceof javax.net.ssl.SSLHandshakeException
            if (r2 == 0) goto L_0x064d
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = r1.toLowerCase()
            java.lang.String r2 = "pkix path building failed"
            int r1 = r1.indexOf(r2)
            r2 = -1
            if (r1 != r2) goto L_0x0632
            goto L_0x064d
        L_0x0632:
            r12.g = r9
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            java.lang.String r3 = "; Connection error - invalid SSL certificate (to accept invalid SSL certificates, see UserAgentSettings.checkSSLCerts)"
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r13)
            throw r1
        L_0x064d:
            r12.g = r11
            if (r7 == 0) goto L_0x067b
            java.io.InputStream r1 = r7.getErrorStream()     // Catch:{ Exception -> 0x065f }
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.io.InputStream) r1)     // Catch:{ Exception -> 0x065f }
            r9.a((java.lang.String) r1)     // Catch:{ Exception -> 0x065f }
            r12.g = r9     // Catch:{ Exception -> 0x065f }
            goto L_0x0660
        L_0x065f:
        L_0x0660:
            com.jaunt.HttpResponse r1 = r12.g
            if (r1 == 0) goto L_0x067b
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r13)
            throw r1
        L_0x067b:
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            com.jaunt.HttpResponse r3 = r12.g
            r1.<init>(r2, r3, r13)
            throw r1
        L_0x0692:
            r0 = move-exception
            r8 = r4
            r14 = r7
            r9 = r10
            r15 = r11
        L_0x0697:
            r10 = 0
        L_0x0698:
            r1 = r0
        L_0x0699:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            java.lang.String r3 = "; NullPointerException for location "
            r2.append(r3)
            r2.append(r13)
            java.lang.String r3 = "; e:\n"
            r2.append(r3)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x06bc:
            com.jaunt.s r1 = r12.o
            com.jaunt.d r11 = r1.c()
            r12.p = r13
            r12.g = r9
            com.jaunt.UserAgent$a r1 = com.jaunt.UserAgent.a.PUT
            if (r15 != r1) goto L_0x06ce
            r1 = 5
            r16 = 5
            goto L_0x06d0
        L_0x06ce:
            r16 = 1
        L_0x06d0:
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r2 = r12.b
            r6 = 0
            r1 = r24
            r3 = r27
            r4 = r8
            r5 = r16
            r7 = r35
            boolean r1 = r1.a((com.jaunt.util.MultiMap<java.lang.String, java.lang.String>) r2, (java.lang.String) r3, (java.lang.String) r4, (short) r5, (java.io.File) r6, (int) r7)
            if (r1 != 0) goto L_0x0806
            r4 = 0
            r1 = r24
            r2 = r9
            r3 = r27
            r5 = r35
            r6 = r16
            r7 = r34
            boolean r1 = r1.a(r2, r3, r4, r5, r6, r7, r8)
            if (r1 != 0) goto L_0x0806
            com.jaunt.Element r1 = r11.f()
            com.jaunt.Element r1 = r1.a((int) r10)
            if (r1 == 0) goto L_0x072e
            com.jaunt.Element r1 = r11.f()
            r2 = 1
            com.jaunt.Element r1 = r1.a((int) r2)
            if (r1 == 0) goto L_0x070a
            goto L_0x072e
        L_0x070a:
            com.jaunt.Document r10 = new com.jaunt.Document
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r4 = r12.b
            com.jaunt.Element r5 = r11.f()
            java.util.List r6 = r11.g()
            int r7 = r11.e()
            short r8 = r11.a()
            boolean r9 = r11.b()
            r1 = r10
            r2 = r24
            r3 = r27
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r12.a((com.jaunt.Document) r10)
            goto L_0x0751
        L_0x072e:
            com.jaunt.Document r10 = new com.jaunt.Document
            com.jaunt.util.MultiMap<java.lang.String, java.lang.String> r4 = r12.b
            com.jaunt.Element r5 = r11.f()
            java.util.List r6 = r11.g()
            int r7 = r11.e()
            short r8 = r11.a()
            boolean r9 = r11.b()
            r1 = r10
            r2 = r24
            r3 = r27
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9)
            r12.a((com.jaunt.Document) r10)
        L_0x0751:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.h
            if (r1 == 0) goto L_0x07a5
            com.jaunt.Document r1 = r12.i
            java.lang.String r1 = r1.h()
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x0788 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x0788 }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ IOException -> 0x0788 }
            java.lang.String r4 = r4.k     // Catch:{ IOException -> 0x0788 }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ IOException -> 0x0788 }
            r3.<init>(r4)     // Catch:{ IOException -> 0x0788 }
            java.lang.String r4 = "/"
            r3.append(r4)     // Catch:{ IOException -> 0x0788 }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ IOException -> 0x0788 }
            java.lang.String r4 = r4.l     // Catch:{ IOException -> 0x0788 }
            r3.append(r4)     // Catch:{ IOException -> 0x0788 }
            java.lang.String r4 = ".html"
            r3.append(r4)     // Catch:{ IOException -> 0x0788 }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x0788 }
            r2.<init>(r3)     // Catch:{ IOException -> 0x0788 }
            com.jaunt.util.IOUtil.a(r2, r1)     // Catch:{ IOException -> 0x0788 }
            goto L_0x07a5
        L_0x0788:
            r0 = move-exception
            r1 = r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            java.lang.String r3 = "; IOException when attempting autosave; e: "
            r2.append(r3)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x07a5:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.i
            if (r1 == 0) goto L_0x07f9
            com.jaunt.Document r1 = r12.i
            java.lang.String r1 = r1.i()
            java.io.File r2 = new java.io.File     // Catch:{ IOException -> 0x07dc }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x07dc }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ IOException -> 0x07dc }
            java.lang.String r4 = r4.k     // Catch:{ IOException -> 0x07dc }
            java.lang.String r4 = java.lang.String.valueOf(r4)     // Catch:{ IOException -> 0x07dc }
            r3.<init>(r4)     // Catch:{ IOException -> 0x07dc }
            java.lang.String r4 = "/"
            r3.append(r4)     // Catch:{ IOException -> 0x07dc }
            com.jaunt.UserAgentSettings r4 = r12.h     // Catch:{ IOException -> 0x07dc }
            java.lang.String r4 = r4.l     // Catch:{ IOException -> 0x07dc }
            r3.append(r4)     // Catch:{ IOException -> 0x07dc }
            java.lang.String r4 = ".xml"
            r3.append(r4)     // Catch:{ IOException -> 0x07dc }
            java.lang.String r3 = r3.toString()     // Catch:{ IOException -> 0x07dc }
            r2.<init>(r3)     // Catch:{ IOException -> 0x07dc }
            com.jaunt.util.IOUtil.a(r2, r1)     // Catch:{ IOException -> 0x07dc }
            goto L_0x07f9
        L_0x07dc:
            r0 = move-exception
            r1 = r0
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            java.lang.String r3 = "; IOException when attempting autosave; e: "
            r2.append(r3)
            java.lang.String r1 = com.jaunt.util.IOUtil.a((java.lang.Throwable) r1)
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            com.jaunt.util.IOUtil.a((java.lang.String) r1)
        L_0x07f9:
            com.jaunt.UserAgentSettings r1 = r12.h
            boolean r1 = r1.b
            if (r1 == 0) goto L_0x0806
            com.jaunt.Document r1 = r12.i
            r2 = r35
            r1.b(r2)
        L_0x0806:
            com.jaunt.Document r1 = r12.i
            return r1
        L_0x0809:
            r14 = r7
            r4 = r9
            r9 = r10
            r15 = r11
            r1 = 409(0x199, float:5.73E-43)
            r9.a((int) r1)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            java.lang.String r2 = "Exceeded maximum allowable redirects ("
            r1.<init>(r2)
            com.jaunt.UserAgentSettings r2 = r12.h
            int r2 = r2.m
            r1.append(r2)
            java.lang.String r2 = ")."
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r9.a((java.lang.String) r1)
            r12.g = r9
            com.jaunt.ResponseException r1 = new com.jaunt.ResponseException
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>(r14)
            r2.append(r15)
            r2.append(r4)
            java.lang.String r2 = r2.toString()
            r1.<init>(r2, r9, r13)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.UserAgent.b(java.lang.String, int, java.lang.String, java.lang.String, com.jaunt.util.MultiMap, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map, int, com.jaunt.UserAgent$a, com.jaunt.util.MultiMap):com.jaunt.Document");
    }

    private static void a(OutputStream outputStream, PrintWriter printWriter, String str, File file) throws IOException {
        String name = file.getName();
        printWriter.append("--" + F).append("\r\n");
        printWriter.append("Content-Disposition: form-data; name=\"" + str + "\"; filename=\"" + name + "\"").append("\r\n");
        StringBuilder sb = new StringBuilder("Content-Type: ");
        sb.append(URLConnection.guessContentTypeFromName(name));
        printWriter.append(sb.toString()).append("\r\n");
        printWriter.append("Content-Transfer-Encoding: binary").append("\r\n");
        printWriter.append("\r\n");
        printWriter.flush();
        FileInputStream fileInputStream = new FileInputStream(file);
        byte[] bArr = new byte[4096];
        while (true) {
            int read = fileInputStream.read(bArr);
            if (read == -1) {
                outputStream.flush();
                fileInputStream.close();
                printWriter.append("\r\n");
                printWriter.flush();
                return;
            }
            outputStream.write(bArr, 0, read);
        }
    }

    private static void a(File file, InputStream inputStream) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        byte[] bArr = new byte[ByteConstants.KB];
        while (true) {
            int read = inputStream.read(bArr);
            if (read == -1) {
                inputStream.close();
                fileOutputStream.flush();
                fileOutputStream.close();
                return;
            }
            fileOutputStream.write(bArr, 0, read);
        }
    }

    private UserAgent a(h hVar) {
        this.o.a().b().a((h) null);
        this.o.a().a((h) null);
        return this;
    }
}
