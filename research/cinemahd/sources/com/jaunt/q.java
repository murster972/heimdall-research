package com.jaunt;

import com.jaunt.JNode;
import java.util.Iterator;
import java.util.LinkedHashMap;

final class q extends JNode implements Iterable<JNode> {
    private LinkedHashMap<String, JNode> e = new LinkedHashMap<>();

    q(JNode jNode, String str, int i) {
        super(jNode, str, i, JNode.Type.OBJECT);
    }

    /* access modifiers changed from: package-private */
    public final String a(int i, boolean z, boolean z2) {
        StringBuilder sb = new StringBuilder();
        if (!e() || z2) {
            sb.append(a(i));
        }
        sb.append("{\n");
        Iterator<String> it2 = this.e.keySet().iterator();
        while (it2.hasNext()) {
            String next = it2.next();
            int i2 = i + 2;
            sb.append(String.valueOf(a(i2)) + "\"" + next + "\":");
            sb.append(this.e.get(next).a(i2, it2.hasNext(), true, false));
        }
        sb.append(String.valueOf(a(i)) + "}");
        if (z) {
            sb.append(",");
        }
        sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        return sb.toString();
    }

    public final Iterator<JNode> iterator() {
        return this.e.values().iterator();
    }

    public final String toString() {
        return a(0, false, false);
    }

    private static String a(int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final void a(JNode jNode) {
        this.e.put(jNode.a(), jNode);
        jNode.b = this;
    }
}
