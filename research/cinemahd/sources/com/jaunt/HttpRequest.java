package com.jaunt;

public class HttpRequest {
    public static String a(short s) {
        if (s == 1) {
            return "POST";
        }
        if (s == 2) {
            return "GET";
        }
        if (s == 3) {
            return "HEAD";
        }
        if (s != 4) {
            return s != 5 ? String.valueOf(s) : "PUT";
        }
        return "DELETE";
    }
}
