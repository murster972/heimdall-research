package com.jaunt;

import com.jaunt.util.IOUtil;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class Element extends Node {
    private String c;
    private short d;
    private LinkedHashMap<String, a> e;
    private ArrayList<Node> f;
    private String g;

    Element(Element element, short s, String str, short s2, boolean z) {
        this(s, str, 1);
        ArrayList<Node> arrayList;
        if (element == null) {
            arrayList = null;
        } else {
            arrayList = element.f;
        }
        if (arrayList != null) {
            this.f = new ArrayList<>(arrayList);
            if (z) {
                for (int i = 0; i < this.f.size(); i++) {
                    this.f.get(i).a(this);
                }
            }
        }
    }

    /* access modifiers changed from: package-private */
    public final void a(short s) {
        this.d = 3;
    }

    /* access modifiers changed from: package-private */
    public final short b(short s) {
        this.d = s;
        return s;
    }

    public String c(String str) throws NotFound {
        LinkedHashMap<String, a> linkedHashMap = this.e;
        if (linkedHashMap != null) {
            a aVar = linkedHashMap.get(str.toLowerCase());
            if (aVar != null) {
                String f2 = aVar.f();
                if (f2 != null) {
                    return f2;
                }
                throw new NotFound("Element.getAt; no attribute value for attributeName; attributeName: ".concat(String.valueOf(str)));
            }
            throw new NotFound("Element.getAt; non-existent attributeName; attributeName: ".concat(String.valueOf(str)));
        }
        throw new NotFound("Element.getAt; non-existent attributeName; attributeName: ".concat(String.valueOf(str)));
    }

    public String d(String str) {
        try {
            return c(str);
        } catch (NotFound unused) {
            return "";
        }
    }

    /* access modifiers changed from: package-private */
    public final short e() {
        return this.d;
    }

    public String f() {
        return this.c.toLowerCase();
    }

    public String g() {
        return this.c;
    }

    public String h() {
        StringBuilder sb = new StringBuilder();
        a(this, this, sb, 0, 4, 1, false, (h) null, (String) null);
        return sb.toString();
    }

    public String i() {
        StringBuilder sb = new StringBuilder();
        a(this, this, sb, 0, 2, 1, false, (h) null, (String) null);
        return sb.toString();
    }

    public String toString() {
        return a(false, (h) null, (String) null);
    }

    /* access modifiers changed from: package-private */
    public final void a() {
        super.a();
        this.f = new ArrayList<>();
    }

    public void b(Node node) {
        a(node, true);
    }

    public String d() {
        return b(false, (h) null, (String) null);
    }

    /* access modifiers changed from: package-private */
    public final void b(String str) {
        if (str == null) {
            IOUtil.a("Element.setAttributeName; attributeName is null");
        }
        a(str, (String) null, 1);
        this.g = str;
    }

    private void a(Node node, boolean z) {
        if (z) {
            node.a(this);
        }
        this.f.add(node);
    }

    private String b(boolean z, h hVar, String str) {
        String[] a2;
        StringBuilder sb = new StringBuilder();
        LinkedHashMap<String, a> linkedHashMap = this.e;
        if (linkedHashMap != null) {
            for (String str2 : linkedHashMap.keySet()) {
                a aVar = this.e.get(str2);
                String e2 = aVar.e();
                String h = aVar.h();
                if (!(!z || hVar == null || h == null || (a2 = hVar.a(this, 2)) == null)) {
                    for (String equalsIgnoreCase : a2) {
                        if (equalsIgnoreCase.equalsIgnoreCase(e2)) {
                            h = "\"" + str + "/" + Document.e(aVar.f()) + "\"";
                        }
                    }
                }
                sb.append(" ".concat(String.valueOf(e2)));
                if (h != null) {
                    sb.append("=".concat(String.valueOf(h)));
                }
            }
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append('<');
        if (this.d == 2) {
            sb2.append('/');
        }
        sb2.append(String.valueOf(g()) + sb);
        if (this.d == 3) {
            sb2.append('/');
        }
        sb2.append(">");
        return sb2.toString();
    }

    /* access modifiers changed from: package-private */
    public final Element a(int i) {
        if (i >= this.f.size()) {
            return null;
        }
        int i2 = 0;
        for (int i3 = 0; i3 < this.f.size(); i3++) {
            Node node = this.f.get(i3);
            if (Node.a(node)) {
                if (i2 == i) {
                    return (Element) node;
                }
                i2++;
            }
        }
        return null;
    }

    Element(String str, short s) {
        this(1, str, s);
    }

    private Element(short s, String str, short s2) {
        super(s);
        this.c = str;
        this.d = s2;
        a();
    }

    /* access modifiers changed from: package-private */
    public final a a(String str) {
        LinkedHashMap<String, a> linkedHashMap = this.e;
        if (linkedHashMap == null) {
            return null;
        }
        return linkedHashMap.get(str.toLowerCase());
    }

    private void a(String str, String str2, short s) {
        String lowerCase = str.toLowerCase();
        if (this.e == null) {
            this.e = new LinkedHashMap<>(10);
        }
        this.e.put(lowerCase, new a(this, str, str2, s));
    }

    /* access modifiers changed from: package-private */
    public final void a(String str, short s) {
        if (str == null || this.g == null || !(s == 2 || s == 3 || s == 1)) {
            IOUtil.a("Element.setAttributeValue; invalid state; attributeValue/quoteType/lastModifiedAttName: " + str + "/" + s + "/" + this.g);
            return;
        }
        a(this.g, str, s);
    }

    private String a(boolean z, h hVar, String str) {
        String[] a2;
        StringBuilder sb = new StringBuilder();
        LinkedHashMap<String, a> linkedHashMap = this.e;
        if (linkedHashMap != null) {
            for (String str2 : linkedHashMap.keySet()) {
                a aVar = this.e.get(str2);
                String e2 = aVar.e();
                String g2 = aVar.g();
                if (!(!z || hVar == null || g2 == null || (a2 = hVar.a(this, 2)) == null)) {
                    for (String equalsIgnoreCase : a2) {
                        if (equalsIgnoreCase.equalsIgnoreCase(e2)) {
                            g2 = "\"" + str + "/" + Document.e(aVar.f()) + "\"";
                        }
                    }
                }
                sb.append(" ".concat(String.valueOf(e2)));
                if (g2 != null) {
                    sb.append("=".concat(String.valueOf(g2)));
                }
            }
        }
        StringBuilder sb2 = new StringBuilder();
        sb2.append('<');
        if (this.d == 2) {
            sb2.append('/');
        }
        sb2.append(String.valueOf(g()) + sb + ">");
        return sb2.toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:50:0x00b9 A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:74:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    static void a(com.jaunt.Node r25, com.jaunt.Node r26, java.lang.StringBuilder r27, int r28, short r29, short r30, boolean r31, com.jaunt.h r32, java.lang.String r33) {
        /*
            r0 = r25
            r1 = r26
            r11 = r27
            r2 = r28
            r12 = r29
            r13 = r30
            r14 = r31
            r15 = r32
            r10 = r33
            r9 = 4
            java.lang.String r8 = "  "
            r7 = 2
            r6 = 3
            r16 = 0
            r5 = 1
            if (r0 == r1) goto L_0x0074
            if (r12 == r5) goto L_0x0020
            if (r12 != r6) goto L_0x0023
        L_0x0020:
            r3 = 0
        L_0x0021:
            if (r3 < r2) goto L_0x006e
        L_0x0023:
            if (r12 == r5) goto L_0x004f
            if (r12 != r9) goto L_0x0028
            goto L_0x004f
        L_0x0028:
            if (r12 == r7) goto L_0x003c
            if (r12 != r6) goto L_0x002d
            goto L_0x003c
        L_0x002d:
            java.lang.String r4 = java.lang.String.valueOf(r29)
            java.lang.String r3 = "Element.toStringBuilder(); unknown displayMode; displayMode: "
            java.lang.String r3 = r3.concat(r4)
            com.jaunt.util.IOUtil.a((java.lang.String) r3)
            r3 = 0
            goto L_0x0061
        L_0x003c:
            boolean r3 = com.jaunt.Node.a((com.jaunt.Node) r25)
            if (r3 == 0) goto L_0x004a
            r3 = r0
            com.jaunt.Element r3 = (com.jaunt.Element) r3
            java.lang.String r3 = r3.b(r14, r15, r10)
            goto L_0x0061
        L_0x004a:
            java.lang.String r3 = r25.d()
            goto L_0x0061
        L_0x004f:
            boolean r3 = com.jaunt.Node.a((com.jaunt.Node) r25)
            if (r3 == 0) goto L_0x005d
            r3 = r0
            com.jaunt.Element r3 = (com.jaunt.Element) r3
            java.lang.String r3 = r3.a((boolean) r14, (com.jaunt.h) r15, (java.lang.String) r10)
            goto L_0x0061
        L_0x005d:
            java.lang.String r3 = r25.toString()
        L_0x0061:
            r11.append(r3)
            if (r12 == r5) goto L_0x0068
            if (r12 != r6) goto L_0x0074
        L_0x0068:
            java.lang.String r3 = "\n"
            r11.append(r3)
            goto L_0x0074
        L_0x006e:
            r11.append(r8)
            int r3 = r3 + 1
            goto L_0x0021
        L_0x0074:
            boolean r3 = com.jaunt.Node.a((com.jaunt.Node) r25)
            if (r3 == 0) goto L_0x015d
            com.jaunt.Element r0 = (com.jaunt.Element) r0
            java.util.ArrayList<com.jaunt.Node> r4 = r0.f
            int r17 = r2 + 1
            r3 = 0
        L_0x0081:
            int r2 = r4.size()
            if (r3 < r2) goto L_0x0128
            int r2 = r17 + -1
            if (r0 != r1) goto L_0x008d
        L_0x008b:
            r1 = 0
            goto L_0x00b7
        L_0x008d:
            short r1 = r0.d
            if (r1 != r6) goto L_0x0093
            r1 = 1
            goto L_0x0094
        L_0x0093:
            r1 = 0
        L_0x0094:
            short r3 = r0.d
            if (r3 != r7) goto L_0x009a
            r3 = 1
            goto L_0x009b
        L_0x009a:
            r3 = 0
        L_0x009b:
            if (r13 == r5) goto L_0x00b1
            if (r13 == r7) goto L_0x00a0
            goto L_0x00b6
        L_0x00a0:
            if (r1 != 0) goto L_0x008b
            if (r3 != 0) goto L_0x008b
            java.lang.String r1 = r0.f()
            java.lang.String r3 = "form"
            boolean r1 = r1.equals(r3)
            if (r1 == 0) goto L_0x00b6
            goto L_0x008b
        L_0x00b1:
            if (r1 != 0) goto L_0x008b
            if (r3 == 0) goto L_0x00b6
            goto L_0x008b
        L_0x00b6:
            r1 = 1
        L_0x00b7:
            if (r1 == 0) goto L_0x015d
            if (r12 == r5) goto L_0x00bd
            if (r12 != r6) goto L_0x00c0
        L_0x00bd:
            r1 = 0
        L_0x00be:
            if (r1 < r2) goto L_0x0122
        L_0x00c0:
            com.jaunt.Element r1 = new com.jaunt.Element
            java.lang.String r0 = r0.g()
            r1.<init>(r0, r7)
            r0 = 10
            if (r12 != r5) goto L_0x00e5
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r1 = r1.toString()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r2.<init>(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r11.append(r0)
            return
        L_0x00e5:
            if (r12 != r9) goto L_0x00ef
            java.lang.String r0 = r1.toString()
            r11.append(r0)
            return
        L_0x00ef:
            if (r12 != r7) goto L_0x00f9
            java.lang.String r0 = r1.d()
            r11.append(r0)
            return
        L_0x00f9:
            if (r12 != r6) goto L_0x0113
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            java.lang.String r1 = r1.d()
            java.lang.String r1 = java.lang.String.valueOf(r1)
            r2.<init>(r1)
            r2.append(r0)
            java.lang.String r0 = r2.toString()
            r11.append(r0)
            return
        L_0x0113:
            java.io.PrintStream r0 = java.lang.System.err
            java.lang.String r2 = "error Element.toStringBuilder(): unknown displayMode"
            r0.println(r2)
            java.lang.String r0 = r1.toString()
            r11.append(r0)
            goto L_0x015d
        L_0x0122:
            r11.append(r8)
            int r1 = r1 + 1
            goto L_0x00be
        L_0x0128:
            java.lang.Object r2 = r4.get(r3)
            com.jaunt.Node r2 = (com.jaunt.Node) r2
            r18 = 0
            r19 = r3
            r3 = r18
            r18 = r4
            r4 = r27
            r20 = 1
            r5 = r17
            r21 = 3
            r6 = r29
            r22 = 2
            r7 = r30
            r23 = r8
            r8 = r31
            r24 = 4
            r9 = r32
            r10 = r33
            a(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            int r3 = r19 + 1
            r4 = r18
            r8 = r23
            r5 = 1
            r6 = 3
            r7 = 2
            r9 = 4
            goto L_0x0081
        L_0x015d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.jaunt.Element.a(com.jaunt.Node, com.jaunt.Node, java.lang.StringBuilder, int, short, short, boolean, com.jaunt.h, java.lang.String):void");
    }
}
