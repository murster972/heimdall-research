package com.jaunt;

import java.io.Serializable;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.StringTokenizer;

public class Cookie implements Serializable {
    private static final long serialVersionUID = 2153335302483778211L;

    /* renamed from: a  reason: collision with root package name */
    private String f4920a;
    private String b;
    private String c;
    private String d;
    private String e;
    private boolean f;
    private boolean g;

    static {
        new ThreadLocal<DateFormat>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object initialValue() {
                return new SimpleDateFormat("EEE, dd-MMM-yyyy hh:mm:ss z");
            }
        };
        new ThreadLocal<DateFormat>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object initialValue() {
                return new SimpleDateFormat("EEE, dd MMM yyyy hh:mm:ss z");
            }
        };
        new ThreadLocal<DateFormat>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object initialValue() {
                return new SimpleDateFormat("EEE, dd-MM-yyyy hh:mm:ss z");
            }
        };
        new ThreadLocal<DateFormat>() {
            /* access modifiers changed from: protected */
            public final /* synthetic */ Object initialValue() {
                return new SimpleDateFormat("EEE, dd MM yyyy hh:mm:ss z");
            }
        };
    }

    public Cookie(String str, String str2) throws JauntException {
        try {
            URL url = new URL(str);
            this.d = url.getPath();
            this.e = url.getHost();
            if (str2.indexOf(59) == -1) {
                try {
                    this.f4920a = str2.substring(0, str2.indexOf(61)).trim();
                    this.b = str2.substring(str2.indexOf(61) + 1).trim();
                } catch (Exception unused) {
                    throw new JauntException("Cookie.Cookie; corrupt name-value field; nameValueStr: ".concat(String.valueOf(str2)));
                }
            } else {
                String substring = str2.substring(0, str2.indexOf(59));
                try {
                    this.f4920a = substring.substring(0, substring.indexOf(61)).trim();
                    this.b = substring.substring(substring.indexOf(61) + 1).trim();
                    StringTokenizer stringTokenizer = new StringTokenizer(str2.substring(str2.indexOf(59) + 1), ";");
                    while (stringTokenizer.hasMoreElements()) {
                        String trim = stringTokenizer.nextToken().trim();
                        if (trim.equalsIgnoreCase("secure") || trim.equalsIgnoreCase("httpOnly")) {
                            a(trim, (String) null);
                        } else {
                            try {
                                a(trim.substring(0, trim.indexOf(61)).trim(), trim.substring(trim.indexOf(61) + 1).trim());
                            } catch (Exception unused2) {
                            }
                        }
                    }
                } catch (Exception unused3) {
                    throw new JauntException("Cookie.Cookie; corrupt name-value field; nameValueStr: ".concat(String.valueOf(substring)));
                }
            }
        } catch (Exception unused4) {
            throw new JauntException("Cookie.Cookie; invalid sourceUrl; sourceUrl: ".concat(String.valueOf(str)));
        }
    }

    private void a(String str, String str2) throws JauntException {
        if (str.equalsIgnoreCase("expires")) {
            this.c = str2;
        } else if (str.equalsIgnoreCase("path")) {
            this.d = str2;
        } else if (str.equalsIgnoreCase("domain")) {
            this.e = str2;
        } else if (str.equalsIgnoreCase("secure") && (str2 == null || str2.equals(""))) {
            this.f = true;
        } else if (!str.equalsIgnoreCase("httpOnly") || (str2 != null && !str2.equals(""))) {
            throw new JauntException("Cookie.adField; unrecognized field; fieldName/fieldValue: " + str + "/" + str2);
        } else {
            this.g = true;
        }
    }

    public String b() {
        return this.e;
    }

    public String c() {
        return this.f4920a;
    }

    public String d() {
        return this.d;
    }

    public boolean e() {
        return false;
    }

    public String toString() {
        return "name: " + this.f4920a + "\nvalue: " + this.b + "\nexpires: " + this.c + "\npath: " + this.d + "\ndomain: " + this.e + "\nsecure: " + this.f + "\nhttpOnly: " + this.g + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE;
    }

    /* access modifiers changed from: package-private */
    public final String a() {
        return String.valueOf(this.f4920a) + "=" + this.b;
    }
}
