package com.jaunt;

import com.jaunt.JNode;
import java.util.ArrayList;
import java.util.Iterator;

final class l extends JNode {
    private boolean e = false;
    private ArrayList<JNode> f = new ArrayList<>();

    l(JNode jNode, String str, int i) {
        super(jNode, str, i, JNode.Type.ARRAY);
    }

    /* access modifiers changed from: package-private */
    public final String a(int i, boolean z, boolean z2) {
        StringBuilder sb = new StringBuilder();
        if (!e() || z2) {
            sb.append(a(i));
        }
        sb.append("[");
        if (this.e) {
            sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        }
        int i2 = 0;
        while (i2 < this.f.size()) {
            JNode jNode = this.f.get(i2);
            boolean z3 = true;
            boolean z4 = i2 != this.f.size() - 1;
            boolean z5 = this.e;
            int i3 = z5 ? i + 2 : 0;
            if (jNode.b() == this) {
                z3 = false;
            }
            sb.append(jNode.a(i3, z4, z5, z3));
            i2++;
        }
        if (this.e) {
            sb.append(a(i));
        }
        sb.append("]");
        if (z) {
            sb.append(",");
        }
        sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        return sb.toString();
    }

    public final Iterator<JNode> iterator() {
        return this.f.iterator();
    }

    public final String toString() {
        return a(0, false, false);
    }

    private static String a(int i) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < i; i2++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    /* access modifiers changed from: package-private */
    public final void a(JNode jNode) {
        a(jNode, true);
    }

    /* access modifiers changed from: package-private */
    public final void a(JNode jNode, boolean z) {
        this.f.add(jNode);
        if (z) {
            jNode.b = this;
        }
        if (jNode != null && !jNode.d()) {
            this.e = true;
        }
    }
}
