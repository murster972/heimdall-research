package com.jaunt;

import com.jaunt.util.Handler;
import java.io.BufferedReader;
import java.io.InputStream;

final class j implements Handler<JNode> {

    /* renamed from: a  reason: collision with root package name */
    private m f4938a = null;

    public final void a(UserAgent userAgent, HttpResponse httpResponse, InputStream inputStream, BufferedReader bufferedReader) throws Exception {
        try {
            if (this.f4938a == null) {
                this.f4938a = new m();
                this.f4938a.a(new r());
            }
            this.f4938a.a(bufferedReader);
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (Exception e) {
            throw e;
        } catch (Throwable th) {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
            throw th;
        }
    }

    /* renamed from: b */
    public final JNode getContent() {
        return this.f4938a.a().b;
    }

    public final String a() {
        m mVar = this.f4938a;
        return mVar != null ? mVar.b() : "";
    }
}
