package com.jaunt;

public class SearchException extends JauntException {
    private static final long serialVersionUID = -4526293879171734530L;

    public SearchException(String str) {
        super(str);
    }
}
