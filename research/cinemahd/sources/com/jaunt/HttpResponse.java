package com.jaunt;

import com.jaunt.util.MultiMap;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class HttpResponse extends LinkedHashMap {
    HttpResponse(String str) {
        put("requestURL", str);
    }

    /* access modifiers changed from: package-private */
    public final void a(MultiMap<String, String> multiMap) {
        put("responseHeaders", multiMap);
    }

    public String b(String str) {
        List<String> c;
        MultiMap<String, String> a2 = a();
        if (a2 == null || (c = a2.c(str)) == null || c.size() <= 0) {
            return null;
        }
        return c.get(c.size() - 1);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        ArrayList arrayList = new ArrayList(keySet());
        for (int i = 0; i < arrayList.size(); i++) {
            String obj = arrayList.get(i).toString();
            Object obj2 = get(obj);
            String str = "null";
            if (obj2 instanceof MultiMap) {
                sb.append("  " + obj + ":\n");
                StringBuilder sb2 = new StringBuilder(String.valueOf("    " + ((MultiMap) obj2).a(": ", "\n    ", false, str)));
                sb2.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
                sb.append(sb2.toString());
            } else {
                if (obj2 != null) {
                    str = obj2.toString();
                }
                sb.append("  " + obj + ": " + str + ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
            }
        }
        return sb.toString();
    }

    public MultiMap<String, String> a() {
        return (MultiMap) get("responseHeaders");
    }

    /* access modifiers changed from: package-private */
    public final void a(String str) {
        put("message", str);
    }

    /* access modifiers changed from: package-private */
    public final void a(int i) {
        put(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, Integer.valueOf(i));
    }

    /* access modifiers changed from: package-private */
    public final void a(boolean z) {
        put("cached", Boolean.valueOf(z));
    }

    public int b() {
        Integer num = (Integer) get(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS);
        if (num == null) {
            return -1;
        }
        return num.intValue();
    }
}
