package com.jaunt;

public class NotFound extends SearchException {
    private static final long serialVersionUID = -8340848985517956652L;

    public NotFound(String str) {
        super(str);
    }
}
