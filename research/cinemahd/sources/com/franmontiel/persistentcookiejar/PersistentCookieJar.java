package com.franmontiel.persistentcookiejar;

import com.franmontiel.persistentcookiejar.cache.CookieCache;
import com.franmontiel.persistentcookiejar.persistence.CookiePersistor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import okhttp3.Cookie;
import okhttp3.HttpUrl;

public class PersistentCookieJar implements ClearableCookieJar {

    /* renamed from: a  reason: collision with root package name */
    private CookieCache f3107a;
    private CookiePersistor b;

    public PersistentCookieJar(CookieCache cookieCache, CookiePersistor cookiePersistor) {
        this.f3107a = cookieCache;
        this.b = cookiePersistor;
        this.f3107a.addAll(cookiePersistor.a());
    }

    private static List<Cookie> a(List<Cookie> list) {
        ArrayList arrayList = new ArrayList();
        for (Cookie next : list) {
            if (next.persistent()) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public synchronized List<Cookie> loadForRequest(HttpUrl httpUrl) {
        ArrayList arrayList;
        ArrayList arrayList2 = new ArrayList();
        arrayList = new ArrayList();
        Iterator it2 = this.f3107a.iterator();
        while (it2.hasNext()) {
            Cookie cookie = (Cookie) it2.next();
            if (a(cookie)) {
                arrayList2.add(cookie);
                it2.remove();
            } else if (cookie.matches(httpUrl)) {
                arrayList.add(cookie);
            }
        }
        this.b.removeAll(arrayList2);
        return arrayList;
    }

    public synchronized void saveFromResponse(HttpUrl httpUrl, List<Cookie> list) {
        this.f3107a.addAll(list);
        this.b.a(a(list));
    }

    private static boolean a(Cookie cookie) {
        return cookie.expiresAt() < System.currentTimeMillis();
    }
}
