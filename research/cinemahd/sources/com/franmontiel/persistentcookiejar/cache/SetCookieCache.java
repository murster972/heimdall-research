package com.franmontiel.persistentcookiejar.cache;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import okhttp3.Cookie;

public class SetCookieCache implements CookieCache {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Set<IdentifiableCookie> f3109a = new HashSet();

    private class SetCookieCacheIterator implements Iterator<Cookie> {

        /* renamed from: a  reason: collision with root package name */
        private Iterator<IdentifiableCookie> f3110a;

        public SetCookieCacheIterator(SetCookieCache setCookieCache) {
            this.f3110a = setCookieCache.f3109a.iterator();
        }

        public boolean hasNext() {
            return this.f3110a.hasNext();
        }

        public void remove() {
            this.f3110a.remove();
        }

        public Cookie next() {
            return this.f3110a.next().a();
        }
    }

    public void addAll(Collection<Cookie> collection) {
        for (IdentifiableCookie next : IdentifiableCookie.a(collection)) {
            this.f3109a.remove(next);
            this.f3109a.add(next);
        }
    }

    public Iterator<Cookie> iterator() {
        return new SetCookieCacheIterator(this);
    }
}
