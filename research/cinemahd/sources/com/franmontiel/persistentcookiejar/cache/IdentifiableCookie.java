package com.franmontiel.persistentcookiejar.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import okhttp3.Cookie;

class IdentifiableCookie {

    /* renamed from: a  reason: collision with root package name */
    private Cookie f3108a;

    IdentifiableCookie(Cookie cookie) {
        this.f3108a = cookie;
    }

    static List<IdentifiableCookie> a(Collection<Cookie> collection) {
        ArrayList arrayList = new ArrayList(collection.size());
        for (Cookie identifiableCookie : collection) {
            arrayList.add(new IdentifiableCookie(identifiableCookie));
        }
        return arrayList;
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof IdentifiableCookie)) {
            return false;
        }
        IdentifiableCookie identifiableCookie = (IdentifiableCookie) obj;
        if (!identifiableCookie.f3108a.name().equals(this.f3108a.name()) || !identifiableCookie.f3108a.domain().equals(this.f3108a.domain()) || !identifiableCookie.f3108a.path().equals(this.f3108a.path()) || identifiableCookie.f3108a.secure() != this.f3108a.secure() || identifiableCookie.f3108a.hostOnly() != this.f3108a.hostOnly()) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return ((((((((527 + this.f3108a.name().hashCode()) * 31) + this.f3108a.domain().hashCode()) * 31) + this.f3108a.path().hashCode()) * 31) + (this.f3108a.secure() ^ true ? 1 : 0)) * 31) + (this.f3108a.hostOnly() ^ true ? 1 : 0);
    }

    /* access modifiers changed from: package-private */
    public Cookie a() {
        return this.f3108a;
    }
}
