package com.franmontiel.persistentcookiejar;

public final class R {

    public static final class string {
        public static final int app_name = 2131755064;

        private string() {
        }
    }

    private R() {
    }
}
