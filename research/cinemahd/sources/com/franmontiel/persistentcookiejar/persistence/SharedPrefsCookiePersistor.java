package com.franmontiel.persistentcookiejar.persistence;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import com.facebook.common.util.UriUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import okhttp3.Cookie;

@SuppressLint({"CommitPrefEdits"})
public class SharedPrefsCookiePersistor implements CookiePersistor {

    /* renamed from: a  reason: collision with root package name */
    private final SharedPreferences f3112a;

    public SharedPrefsCookiePersistor(Context context) {
        this(context.getSharedPreferences("CookiePersistence", 0));
    }

    public List<Cookie> a() {
        ArrayList arrayList = new ArrayList(this.f3112a.getAll().size());
        for (Map.Entry<String, ?> value : this.f3112a.getAll().entrySet()) {
            Cookie a2 = new SerializableCookie().a((String) value.getValue());
            if (a2 != null) {
                arrayList.add(a2);
            }
        }
        return arrayList;
    }

    public void removeAll(Collection<Cookie> collection) {
        SharedPreferences.Editor edit = this.f3112a.edit();
        for (Cookie a2 : collection) {
            edit.remove(a(a2));
        }
        edit.commit();
    }

    public SharedPrefsCookiePersistor(SharedPreferences sharedPreferences) {
        this.f3112a = sharedPreferences;
    }

    public void a(Collection<Cookie> collection) {
        SharedPreferences.Editor edit = this.f3112a.edit();
        for (Cookie next : collection) {
            edit.putString(a(next), new SerializableCookie().a(next));
        }
        edit.commit();
    }

    private static String a(Cookie cookie) {
        StringBuilder sb = new StringBuilder();
        sb.append(cookie.secure() ? UriUtil.HTTPS_SCHEME : UriUtil.HTTP_SCHEME);
        sb.append("://");
        sb.append(cookie.domain());
        sb.append(cookie.path());
        sb.append("|");
        sb.append(cookie.name());
        return sb.toString();
    }
}
