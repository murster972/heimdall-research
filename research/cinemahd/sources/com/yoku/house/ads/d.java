package com.yoku.house.ads;

import android.view.View;
import com.yoku.house.ads.model.DialogModal;

/* compiled from: lambda */
public final /* synthetic */ class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ HouseAdsDialog f6602a;
    private final /* synthetic */ DialogModal b;

    public /* synthetic */ d(HouseAdsDialog houseAdsDialog, DialogModal dialogModal) {
        this.f6602a = houseAdsDialog;
        this.b = dialogModal;
    }

    public final void onClick(View view) {
        this.f6602a.a(this.b, view);
    }
}
