package com.yoku.house.ads;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.facebook.common.util.UriUtil;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.yoku.house.ads.helper.HouseAdsHelper;
import com.yoku.house.ads.helper.cacheImages.PicassoHelper;
import com.yoku.house.ads.listener.AdListener;
import com.yoku.house.ads.model.InterstitialModal;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HouseAdsInterstitial {
    /* access modifiers changed from: private */
    public static AdListener d = null;
    /* access modifiers changed from: private */
    public static boolean e = false;
    /* access modifiers changed from: private */
    public static Bitmap f;
    /* access modifiers changed from: private */
    public static String g;

    /* renamed from: a  reason: collision with root package name */
    private final Context f6594a;
    private int b = 0;
    private String c;

    public static class InterstitialActivity extends Activity {
        public /* synthetic */ void a(View view) {
            if (HouseAdsInterstitial.g.startsWith(UriUtil.HTTP_SCHEME)) {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse(HouseAdsInterstitial.g));
                intent.setPackage("com.android.chrome");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse(HouseAdsInterstitial.g)));
                }
                if (HouseAdsInterstitial.d != null) {
                    HouseAdsInterstitial.d.a();
                }
                finish();
                return;
            }
            try {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + HouseAdsInterstitial.g)));
                if (HouseAdsInterstitial.d != null) {
                    HouseAdsInterstitial.d.a();
                }
                finish();
            } catch (ActivityNotFoundException unused) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + HouseAdsInterstitial.g)));
                if (HouseAdsInterstitial.d != null) {
                    HouseAdsInterstitial.d.a();
                }
                finish();
            }
        }

        public /* synthetic */ void b(View view) {
            finish();
            if (HouseAdsInterstitial.d != null) {
                HouseAdsInterstitial.d.onAdClosed();
            }
        }

        public void onBackPressed() {
            if (HouseAdsInterstitial.d != null) {
                HouseAdsInterstitial.d.onAdClosed();
            }
            finish();
        }

        /* access modifiers changed from: protected */
        public void onCreate(Bundle bundle) {
            super.onCreate(bundle);
            if (HouseAdsInterstitial.d != null) {
                HouseAdsInterstitial.d.b();
            }
            setContentView(R$layout.house_ads_interstitial_layout);
            ImageView imageView = (ImageView) findViewById(R$id.image);
            imageView.setImageBitmap(HouseAdsInterstitial.f);
            imageView.setOnClickListener(new f(this));
            ((ImageButton) findViewById(R$id.button_close)).setOnClickListener(new e(this));
        }
    }

    public HouseAdsInterstitial(Context context, String str) {
        this.f6594a = context;
        this.c = str;
    }

    public void b() {
        if (!this.c.trim().isEmpty()) {
            a(this.c);
            return;
        }
        AdListener adListener = d;
        if (adListener != null) {
            adListener.a(new Exception("Null Response"));
        }
    }

    public void c() {
        Context context = this.f6594a;
        context.startActivity(new Intent(context, InterstitialActivity.class));
        Context context2 = this.f6594a;
        if (context2 instanceof AppCompatActivity) {
            ((AppCompatActivity) context2).overridePendingTransition(0, 0);
        }
    }

    public void a(AdListener adListener) {
        d = adListener;
    }

    public boolean a() {
        return e;
    }

    private void a(String str) {
        ArrayList arrayList = new ArrayList();
        StringBuilder sb = new StringBuilder();
        sb.append(str);
        try {
            JSONArray optJSONArray = new JSONObject(new String(sb)).optJSONArray("apps");
            for (int i = 0; i < optJSONArray.length(); i++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i);
                if (jSONObject.optString("app_adType").equals("interstitial")) {
                    boolean optBoolean = jSONObject.has("hideIfAppInstalled") ? jSONObject.optBoolean("hideIfAppInstalled") : true;
                    if (jSONObject.optString("app_uri").startsWith(UriUtil.HTTP_SCHEME) || !optBoolean || !HouseAdsHelper.a(this.f6594a, jSONObject.optString("app_uri"))) {
                        InterstitialModal interstitialModal = new InterstitialModal();
                        interstitialModal.a(jSONObject.optString("app_interstitial_url"));
                        interstitialModal.b(jSONObject.optString("app_uri"));
                        arrayList.add(interstitialModal);
                    }
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (arrayList.size() > 0) {
            InterstitialModal interstitialModal2 = (InterstitialModal) arrayList.get(this.b);
            if (this.b == arrayList.size() - 1) {
                this.b = 0;
            } else {
                this.b++;
            }
            PicassoHelper.a(interstitialModal2.a()).into((Target) new Target(this) {
                public void onBitmapFailed(Exception exc, Drawable drawable) {
                    if (HouseAdsInterstitial.d != null) {
                        HouseAdsInterstitial.d.a(exc);
                    }
                    boolean unused = HouseAdsInterstitial.e = false;
                }

                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                    Bitmap unused = HouseAdsInterstitial.f = bitmap;
                    if (HouseAdsInterstitial.d != null) {
                        HouseAdsInterstitial.d.onAdLoaded();
                    }
                    boolean unused2 = HouseAdsInterstitial.e = true;
                }

                public void onPrepareLoad(Drawable drawable) {
                }
            });
            g = interstitialModal2.b();
        }
    }
}
