package com.yoku.house.ads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.palette.graphics.Palette;
import com.facebook.common.util.UriUtil;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.yoku.house.ads.helper.HouseAdsHelper;
import com.yoku.house.ads.helper.cacheImages.PicassoHelper;
import com.yoku.house.ads.listener.AdListener;
import com.yoku.house.ads.model.DialogModal;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HouseAdsDialog {
    private static int k;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f6591a;
    private String b = "";
    private boolean c = true;
    private boolean d = true;
    /* access modifiers changed from: private */
    public boolean e = true;
    private int f = 25;
    private int g = 25;
    /* access modifiers changed from: private */
    public AdListener h;
    private AlertDialog i;
    private String j;

    public HouseAdsDialog(Context context, String str) {
        this.f6591a = context;
        this.j = str;
    }

    static /* synthetic */ boolean c(boolean z) {
        return z;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void b(boolean z) {
        this.e = z;
    }

    public /* synthetic */ void c(DialogInterface dialogInterface) {
        AdListener adListener = this.h;
        if (adListener != null) {
            adListener.onAdClosed();
        }
    }

    public void a(AdListener adListener) {
        this.h = adListener;
    }

    public /* synthetic */ void b(DialogInterface dialogInterface) {
        AdListener adListener = this.h;
        if (adListener != null) {
            adListener.onAdClosed();
        }
    }

    public void a() {
        if (this.d || this.b.isEmpty()) {
            if (!this.j.trim().isEmpty()) {
                String str = this.j;
                this.b = str;
                a(str);
            } else {
                AdListener adListener = this.h;
                if (adListener != null) {
                    adListener.a(new Exception("Null Response"));
                }
            }
        }
        if (!this.d && !this.b.trim().isEmpty()) {
            a(this.b);
        }
    }

    private void a(String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this.f6591a);
        ArrayList arrayList = new ArrayList();
        try {
            JSONArray optJSONArray = new JSONObject(str).optJSONArray("apps");
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i2);
                if (jSONObject.optString("app_adType").equals("dialog")) {
                    boolean optBoolean = jSONObject.has("hideIfAppInstalled") ? jSONObject.optBoolean("hideIfAppInstalled") : true;
                    if (jSONObject.optString("app_uri").startsWith(UriUtil.HTTP_SCHEME) || !optBoolean || !HouseAdsHelper.a(this.f6591a, jSONObject.optString("app_uri"))) {
                        DialogModal dialogModal = new DialogModal();
                        dialogModal.b(jSONObject.optString("app_title"));
                        dialogModal.a(jSONObject.optString("app_desc"));
                        dialogModal.d(jSONObject.optString("app_icon"));
                        dialogModal.e(jSONObject.optString("app_header_image"));
                        dialogModal.c(jSONObject.optString("app_cta_text"));
                        dialogModal.f(jSONObject.optString("app_uri"));
                        dialogModal.h(jSONObject.optString("app_rating"));
                        dialogModal.g(jSONObject.optString("app_price"));
                        arrayList.add(dialogModal);
                    }
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
        if (arrayList.size() > 0) {
            DialogModal dialogModal2 = (DialogModal) arrayList.get(k);
            if (k == arrayList.size() - 1) {
                k = 0;
            } else {
                k++;
            }
            View inflate = View.inflate(this.f6591a, R$layout.house_ads_dialog_layout, (ViewGroup) null);
            if (dialogModal2.d().trim().isEmpty() || !dialogModal2.d().trim().startsWith(UriUtil.HTTP_SCHEME)) {
                throw new IllegalArgumentException("Icon URL should not be Null or Blank & should start with \"http\"");
            } else if (!dialogModal2.e().trim().isEmpty() && !dialogModal2.e().trim().startsWith(UriUtil.HTTP_SCHEME)) {
                throw new IllegalArgumentException("Header Image URL should start with \"http\"");
            } else if (dialogModal2.b().trim().isEmpty() || dialogModal2.a().trim().isEmpty()) {
                throw new IllegalArgumentException("Title & description should not be Null or Blank.");
            } else {
                ((CardView) inflate.findViewById(R$id.houseAds_card_view)).setRadius((float) this.f);
                Button button = (Button) inflate.findViewById(R$id.houseAds_cta);
                ((GradientDrawable) button.getBackground()).setCornerRadius((float) this.g);
                ImageView imageView = (ImageView) inflate.findViewById(R$id.houseAds_app_icon);
                final ImageView imageView2 = (ImageView) inflate.findViewById(R$id.houseAds_header_image);
                TextView textView = (TextView) inflate.findViewById(R$id.houseAds_title);
                AnonymousClass1 r9 = r1;
                final ImageView imageView3 = imageView;
                RequestCreator a2 = PicassoHelper.a(dialogModal2.d());
                final Button button2 = button;
                AlertDialog.Builder builder2 = builder;
                TextView textView2 = (TextView) inflate.findViewById(R$id.houseAds_price);
                final DialogModal dialogModal3 = dialogModal2;
                View view = inflate;
                TextView textView3 = (TextView) inflate.findViewById(R$id.houseAds_description);
                final RatingBar ratingBar = (RatingBar) inflate.findViewById(R$id.houseAds_rating);
                AnonymousClass1 r1 = new Callback() {
                    public void onError(Exception exc) {
                        HouseAdsDialog.c(false);
                        if (HouseAdsDialog.this.h != null) {
                            HouseAdsDialog.this.h.a(exc);
                        }
                        imageView3.setVisibility(8);
                    }

                    public void onSuccess() {
                        HouseAdsDialog.c(true);
                        if (HouseAdsDialog.this.h != null) {
                            HouseAdsDialog.this.h.onAdLoaded();
                        }
                        if (imageView3.getVisibility() == 8) {
                            imageView3.setVisibility(0);
                        }
                        int a2 = ContextCompat.a(HouseAdsDialog.this.f6591a, R$color.colorAccent);
                        if (HouseAdsDialog.this.e) {
                            a2 = Palette.a(((BitmapDrawable) imageView3.getDrawable()).getBitmap()).a().a(ContextCompat.a(HouseAdsDialog.this.f6591a, R$color.colorAccent));
                        }
                        ((GradientDrawable) button2.getBackground()).setColor(a2);
                        if (dialogModal3.h() > 0.0f) {
                            ratingBar.setRating(dialogModal3.h());
                            DrawableCompat.b(ratingBar.getProgressDrawable(), a2);
                            return;
                        }
                        ratingBar.setVisibility(8);
                    }
                };
                a2.into(imageView, r9);
                if (dialogModal2.e().trim().isEmpty() || !this.c) {
                    imageView2.setVisibility(8);
                } else {
                    PicassoHelper.a(dialogModal2.e()).into((Target) new Target(this) {
                        public void onBitmapFailed(Exception exc, Drawable drawable) {
                            imageView2.setVisibility(8);
                        }

                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                            imageView2.setImageBitmap(bitmap);
                            imageView2.setVisibility(0);
                        }

                        public void onPrepareLoad(Drawable drawable) {
                        }
                    });
                }
                textView.setText(dialogModal2.b());
                textView3.setText(dialogModal2.a());
                button.setText(dialogModal2.c());
                if (dialogModal2.g().trim().isEmpty()) {
                    textView2.setVisibility(8);
                } else {
                    textView2.setText(String.format("Price: %s", new Object[]{dialogModal2.g()}));
                }
                AlertDialog.Builder builder3 = builder2;
                builder3.b(view);
                this.i = builder3.a();
                this.i.getWindow().setBackgroundDrawableResource(17170445);
                this.i.setOnShowListener(new b(this));
                this.i.setOnCancelListener(new a(this));
                this.i.setOnDismissListener(new c(this));
                button.setOnClickListener(new d(this, dialogModal2));
            }
        }
    }

    public /* synthetic */ void a(DialogInterface dialogInterface) {
        AdListener adListener = this.h;
        if (adListener != null) {
            adListener.b();
        }
    }

    public /* synthetic */ void a(DialogModal dialogModal, View view) {
        this.i.dismiss();
        String f2 = dialogModal.f();
        if (f2.trim().startsWith(UriUtil.HTTP_SCHEME)) {
            this.f6591a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(f2)));
            AdListener adListener = this.h;
            if (adListener != null) {
                adListener.a();
                return;
            }
            return;
        }
        try {
            Context context = this.f6591a;
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + f2)));
            if (this.h != null) {
                this.h.a();
            }
        } catch (ActivityNotFoundException unused) {
            AdListener adListener2 = this.h;
            if (adListener2 != null) {
                adListener2.a();
            }
            Context context2 = this.f6591a;
            context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + f2)));
        }
    }
}
