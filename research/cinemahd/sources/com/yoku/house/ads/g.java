package com.yoku.house.ads;

import android.view.View;
import com.yoku.house.ads.model.DialogModal;

/* compiled from: lambda */
public final /* synthetic */ class g implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ HouseAdsNative f6605a;
    private final /* synthetic */ DialogModal b;

    public /* synthetic */ g(HouseAdsNative houseAdsNative, DialogModal dialogModal) {
        this.f6605a = houseAdsNative;
        this.b = dialogModal;
    }

    public final void onClick(View view) {
        this.f6605a.a(this.b, view);
    }
}
