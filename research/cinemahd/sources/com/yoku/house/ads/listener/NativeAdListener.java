package com.yoku.house.ads.listener;

import android.view.View;

public interface NativeAdListener {

    public interface CallToActionListener {
        void a(View view);
    }

    void a(Exception exc);

    void onAdLoaded();
}
