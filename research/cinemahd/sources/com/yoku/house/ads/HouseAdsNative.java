package com.yoku.house.ads;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.DrawableCompat;
import androidx.palette.graphics.Palette;
import com.facebook.common.util.UriUtil;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;
import com.yoku.house.ads.helper.HouseAdsHelper;
import com.yoku.house.ads.helper.RemoveJsonObjectCompat;
import com.yoku.house.ads.helper.cacheImages.PicassoHelper;
import com.yoku.house.ads.listener.NativeAdListener;
import com.yoku.house.ads.model.DialogModal;
import com.yoku.house.ads.model.HouseAdsNativeView;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HouseAdsNative {
    private static int j;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final Context f6595a;
    private final String b;
    /* access modifiers changed from: private */
    public boolean c = true;
    /* access modifiers changed from: private */
    public boolean d = false;
    private HouseAdsNativeView e;
    private View f;
    /* access modifiers changed from: private */
    public NativeAdListener g;
    private NativeAdListener.CallToActionListener h;
    ArrayList<DialogModal> i = new ArrayList<>();

    public HouseAdsNative(Context context, String str) {
        this.f6595a = context;
        this.b = str;
    }

    public void a(boolean z) {
        this.c = z;
    }

    public void a(NativeAdListener nativeAdListener) {
        this.g = nativeAdListener;
    }

    public void a() {
        this.d = false;
        if (!this.b.trim().isEmpty()) {
            a(this.b);
            return;
        }
        NativeAdListener nativeAdListener = this.g;
        if (nativeAdListener != null) {
            nativeAdListener.a(new Exception("Null Response"));
        }
    }

    public boolean a(ViewGroup viewGroup, boolean z) {
        TextView textView;
        TextView textView2;
        View findViewById;
        TextView textView3;
        ImageView imageView;
        final ImageView imageView2;
        RatingBar ratingBar;
        if (this.i.size() > 0) {
            if (z) {
                this.f = View.inflate(viewGroup.getContext(), R$layout.house_ads_native_banner, (ViewGroup) null);
            } else {
                this.f = View.inflate(viewGroup.getContext(), R$layout.house_ads_native_layout, (ViewGroup) null);
            }
            viewGroup.addView(this.f);
            DialogModal dialogModal = this.i.get(j);
            if (j == this.i.size() - 1) {
                j = 0;
            } else {
                j++;
            }
            HouseAdsNativeView houseAdsNativeView = this.e;
            if (houseAdsNativeView != null) {
                TextView g2 = houseAdsNativeView.g();
                textView = houseAdsNativeView.b();
                textView2 = houseAdsNativeView.e();
                View a2 = houseAdsNativeView.a();
                ImageView d2 = houseAdsNativeView.d();
                ImageView c2 = houseAdsNativeView.c();
                ratingBar = houseAdsNativeView.f();
                textView3 = g2;
                findViewById = a2;
                imageView = d2;
                imageView2 = c2;
            } else {
                View view = this.f;
                if (view != null) {
                    textView = (TextView) this.f.findViewById(R$id.houseAds_description);
                    textView2 = (TextView) this.f.findViewById(R$id.houseAds_price);
                    findViewById = this.f.findViewById(R$id.houseAds_cta);
                    textView3 = (TextView) view.findViewById(R$id.houseAds_title);
                    imageView = (ImageView) this.f.findViewById(R$id.houseAds_app_icon);
                    imageView2 = (ImageView) this.f.findViewById(R$id.houseAds_header_image);
                    ratingBar = (RatingBar) this.f.findViewById(R$id.houseAds_rating);
                } else {
                    throw new NullPointerException("NativeAdView is Null. Either pass HouseAdsNativeView or a View in showNative()");
                }
            }
            TextView textView4 = textView;
            TextView textView5 = textView2;
            if (dialogModal.d().trim().isEmpty() || !dialogModal.d().trim().contains(UriUtil.HTTP_SCHEME)) {
                throw new IllegalArgumentException("Icon URL should not be Null or Blank & should start with \"http\"");
            } else if (!dialogModal.e().trim().isEmpty() && !dialogModal.e().trim().contains(UriUtil.HTTP_SCHEME)) {
                throw new IllegalArgumentException("Header Image URL should start with \"http\"");
            } else if (dialogModal.b().trim().isEmpty() || dialogModal.a().trim().isEmpty()) {
                throw new IllegalArgumentException("Title & description should not be Null or Blank.");
            } else {
                final ImageView imageView3 = imageView;
                AnonymousClass1 r9 = r0;
                final View view2 = findViewById;
                RequestCreator a3 = PicassoHelper.a(dialogModal.d());
                final DialogModal dialogModal2 = dialogModal;
                View view3 = findViewById;
                TextView textView6 = textView5;
                final RatingBar ratingBar2 = ratingBar;
                RatingBar ratingBar3 = ratingBar;
                TextView textView7 = textView4;
                final ImageView imageView4 = imageView2;
                AnonymousClass1 r0 = new Callback() {
                    public void onError(Exception exc) {
                        boolean unused = HouseAdsNative.this.d = false;
                        if ((imageView4 == null || dialogModal2.e().isEmpty()) && HouseAdsNative.this.g != null) {
                            HouseAdsNative.this.g.a(exc);
                        }
                    }

                    public void onSuccess() {
                        if (HouseAdsNative.this.c) {
                            int a2 = Palette.a(((BitmapDrawable) imageView3.getDrawable()).getBitmap()).a().a(ContextCompat.a(HouseAdsNative.this.f6595a, R$color.colorAccent));
                            if (view2.getBackground() instanceof ColorDrawable) {
                                if (Build.VERSION.SDK_INT >= 16) {
                                    view2.setBackground(new GradientDrawable());
                                } else {
                                    view2.setBackgroundDrawable(new GradientDrawable());
                                }
                            }
                            ((GradientDrawable) view2.getBackground()).setColor(a2);
                            if (dialogModal2.h() > 0.0f) {
                                ratingBar2.setRating(dialogModal2.h());
                                DrawableCompat.b(ratingBar2.getProgressDrawable(), a2);
                            } else {
                                ratingBar2.setVisibility(8);
                            }
                        }
                        if (dialogModal2.e().trim().isEmpty()) {
                            boolean unused = HouseAdsNative.this.d = true;
                            if (HouseAdsNative.this.g != null) {
                                HouseAdsNative.this.g.onAdLoaded();
                            }
                        }
                    }
                };
                a3.into(imageView, r9);
                if (!z && !dialogModal.e().trim().isEmpty()) {
                    PicassoHelper.a(dialogModal.e()).into((Target) new Target() {
                        public void onBitmapFailed(Exception exc, Drawable drawable) {
                            if (HouseAdsNative.this.g != null) {
                                HouseAdsNative.this.g.a(exc);
                            }
                            boolean unused = HouseAdsNative.this.d = false;
                        }

                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
                            ImageView imageView = imageView2;
                            if (imageView != null) {
                                imageView.setVisibility(0);
                                imageView2.setImageBitmap(bitmap);
                            }
                            boolean unused = HouseAdsNative.this.d = true;
                            if (HouseAdsNative.this.g != null) {
                                HouseAdsNative.this.g.onAdLoaded();
                            }
                        }

                        public void onPrepareLoad(Drawable drawable) {
                        }
                    });
                } else if (imageView2 != null) {
                    imageView2.setVisibility(8);
                }
                textView3.setText(dialogModal.b());
                textView7.setText(dialogModal.a());
                if (textView6 != null) {
                    textView6.setVisibility(0);
                    if (!dialogModal.g().trim().isEmpty()) {
                        textView6.setText(String.format("Price: %s", new Object[]{dialogModal.g()}));
                    } else {
                        textView6.setVisibility(8);
                    }
                }
                if (ratingBar3 != null) {
                    RatingBar ratingBar4 = ratingBar3;
                    ratingBar4.setVisibility(0);
                    if (dialogModal.h() > 0.0f) {
                        ratingBar4.setRating(dialogModal.h());
                    } else {
                        ratingBar4.setVisibility(8);
                    }
                }
                if (view3 != null) {
                    View view4 = view3;
                    boolean z2 = view4 instanceof TextView;
                    if (z2) {
                        ((TextView) view4).setText(dialogModal.c());
                    }
                    if (view4 instanceof Button) {
                        ((Button) view4).setText(dialogModal.c());
                    }
                    if (z2) {
                        view4.setOnClickListener(new g(this, dialogModal));
                    } else {
                        throw new IllegalArgumentException("Call to Action View must be either a Button or a TextView");
                    }
                }
            }
        }
        return this.d;
    }

    public /* synthetic */ void a(DialogModal dialogModal, View view) {
        NativeAdListener.CallToActionListener callToActionListener = this.h;
        if (callToActionListener != null) {
            callToActionListener.a(view);
            return;
        }
        String f2 = dialogModal.f();
        if (f2.trim().startsWith(UriUtil.HTTP_SCHEME)) {
            this.f6595a.startActivity(new Intent("android.intent.action.VIEW", Uri.parse(f2)));
            return;
        }
        try {
            Context context = this.f6595a;
            context.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + f2)));
        } catch (ActivityNotFoundException unused) {
            Context context2 = this.f6595a;
            context2.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + f2)));
        }
    }

    private void a(String str) {
        try {
            JSONArray optJSONArray = new JSONObject(str).optJSONArray("apps");
            for (int i2 = 0; i2 < optJSONArray.length(); i2++) {
                JSONObject jSONObject = optJSONArray.getJSONObject(i2);
                if (jSONObject.optString("app_adType").equals("native")) {
                    boolean optBoolean = jSONObject.has("hideIfAppInstalled") ? jSONObject.optBoolean("hideIfAppInstalled") : true;
                    if (jSONObject.optString("app_uri").startsWith(UriUtil.HTTP_SCHEME) || !optBoolean || !HouseAdsHelper.a(this.f6595a, jSONObject.optString("app_uri"))) {
                        DialogModal dialogModal = new DialogModal();
                        dialogModal.b(jSONObject.optString("app_title"));
                        dialogModal.a(jSONObject.optString("app_desc"));
                        dialogModal.d(jSONObject.optString("app_icon"));
                        dialogModal.e(jSONObject.optString("app_header_image"));
                        dialogModal.c(jSONObject.optString("app_cta_text"));
                        dialogModal.f(jSONObject.optString("app_uri"));
                        dialogModal.h(jSONObject.optString("app_rating"));
                        dialogModal.g(jSONObject.optString("app_price"));
                        this.i.add(dialogModal);
                    }
                } else {
                    new RemoveJsonObjectCompat(i2, optJSONArray).execute(new JSONArray[0]);
                }
            }
        } catch (JSONException e2) {
            e2.printStackTrace();
        }
    }
}
