package com.yoku.house.ads.model;

public class InterstitialModal {

    /* renamed from: a  reason: collision with root package name */
    private String f6611a;
    private String b;

    public String a() {
        return this.f6611a;
    }

    public String b() {
        return this.b;
    }

    public void a(String str) {
        this.f6611a = str;
    }

    public void b(String str) {
        this.b = str;
    }
}
