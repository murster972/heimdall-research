package com.yoku.house.ads.model;

public class DialogModal {

    /* renamed from: a  reason: collision with root package name */
    private String f6609a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private String g;
    private String h;

    public String a() {
        return this.c;
    }

    public String b() {
        return this.b;
    }

    public String c() {
        return this.f;
    }

    public String d() {
        return this.f6609a;
    }

    public String e() {
        return this.d;
    }

    public String f() {
        return this.e;
    }

    public String g() {
        return this.g;
    }

    public float h() {
        if (!this.h.isEmpty()) {
            return Float.parseFloat(this.h);
        }
        return 0.0f;
    }

    public void a(String str) {
        this.c = str;
    }

    public void b(String str) {
        this.b = str;
    }

    public void c(String str) {
        this.f = str;
    }

    public void d(String str) {
        this.f6609a = str;
    }

    public void e(String str) {
        this.d = str;
    }

    public void f(String str) {
        this.e = str;
    }

    public void g(String str) {
        this.g = str;
    }

    public void h(String str) {
        this.h = str;
    }
}
