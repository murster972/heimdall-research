package com.yoku.house.ads.model;

import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class HouseAdsNativeView {

    /* renamed from: a  reason: collision with root package name */
    private TextView f6610a;
    private TextView b;
    private TextView c;
    private ImageView d;
    private ImageView e;
    private View f;
    private RatingBar g;

    public View a() {
        return this.f;
    }

    public TextView b() {
        return this.b;
    }

    public ImageView c() {
        return this.e;
    }

    public ImageView d() {
        return this.d;
    }

    public TextView e() {
        return this.c;
    }

    public RatingBar f() {
        return this.g;
    }

    public TextView g() {
        return this.f6610a;
    }
}
