package com.yoku.house.ads.helper;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.TextView;

public class RunningTextView extends TextView {
    public RunningTextView(Context context) {
        super(context);
    }

    public boolean isFocused() {
        return true;
    }

    /* access modifiers changed from: protected */
    public void onFocusChanged(boolean z, int i, Rect rect) {
        if (z) {
            super.onFocusChanged(z, i, rect);
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (z) {
            super.onWindowFocusChanged(z);
        }
    }

    public RunningTextView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public RunningTextView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
    }

    public RunningTextView(Context context, AttributeSet attributeSet, int i, int i2) {
        super(context, attributeSet, i, i2);
    }
}
