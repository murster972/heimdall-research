package com.yoku.house.ads.helper;

import android.os.AsyncTask;
import android.util.Log;

public class JsonPullerTask extends AsyncTask<String, String, String> {

    /* renamed from: a  reason: collision with root package name */
    private final String f6606a;
    private final JsonPullerListener b;

    public interface JsonPullerListener {
        void a(String str);
    }

    public JsonPullerTask(String str, JsonPullerListener jsonPullerListener) {
        this.f6606a = str;
        this.b = jsonPullerListener;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(String... strArr) {
        return HouseAdsHelper.a(this.f6606a);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        this.b.a(str);
        Log.d("Response", str);
    }
}
