package com.yoku.house.ads.helper;

import android.os.AsyncTask;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

public class RemoveJsonObjectCompat extends AsyncTask<JSONArray, JSONArray, JSONArray> {

    /* renamed from: a  reason: collision with root package name */
    private final JSONArray f6607a;
    private final int b;

    public RemoveJsonObjectCompat(int i, JSONArray jSONArray) {
        this.f6607a = jSONArray;
        this.b = i;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public JSONArray doInBackground(JSONArray... jSONArrayArr) {
        return a(this.b, this.f6607a);
    }

    private JSONArray a(int i, JSONArray jSONArray) {
        List<JSONObject> a2 = a(jSONArray);
        a2.remove(i);
        JSONArray jSONArray2 = new JSONArray();
        for (JSONObject put : a2) {
            jSONArray2.put(put);
        }
        return jSONArray2;
    }

    private List<JSONObject> a(JSONArray jSONArray) {
        int length = jSONArray.length();
        ArrayList arrayList = new ArrayList(length);
        for (int i = 0; i < length; i++) {
            JSONObject optJSONObject = jSONArray.optJSONObject(i);
            if (optJSONObject != null) {
                arrayList.add(optJSONObject);
            }
        }
        return arrayList;
    }
}
