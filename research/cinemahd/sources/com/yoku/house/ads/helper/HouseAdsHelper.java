package com.yoku.house.ads.helper;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;
import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.trakt5.TraktV2;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HouseAdsHelper {
    static String a(String str) {
        Document document;
        try {
            document = Jsoup.a(str.trim()).a(true).a((int) HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT).a("Connection", "keep-alive").a("Cache-Control", "max-age=0").a(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8").a("User-Agent", "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/36.0.1985.125 Safari/537.36").a(TraktV2.HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded").a("Referer", "HouseAds (App)").a("Accept-Encoding", "gzip,deflate,sdch").a(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US,en;q=0.8,ru;q=0.6").get();
        } catch (Exception e) {
            Log.e("HouseAds", e.getMessage());
            document = null;
        }
        return document != null ? document.H().G() : "";
    }

    public static boolean a(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (PackageManager.NameNotFoundException unused) {
            return false;
        }
    }
}
