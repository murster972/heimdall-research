package com.yoku.house.ads.helper.cacheImages;

import android.content.Context;
import com.squareup.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class PicassoHelper {

    /* renamed from: a  reason: collision with root package name */
    private static boolean f6608a = false;

    public static void a(Context context) {
        if (!f6608a) {
            File a2 = CacheUtils.a(context);
            Picasso.setSingletonInstance(new Picasso.Builder(context).indicatorsEnabled(false).downloader(new OkHttp3Downloader(new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).cache(new Cache(a2, CacheUtils.a(a2))).build())).loggingEnabled(false).build());
            f6608a = true;
        }
    }

    public static RequestCreator a(String str) {
        if (!f6608a) {
            System.out.println("Must initialize first");
        }
        return Picasso.get().load(str);
    }
}
