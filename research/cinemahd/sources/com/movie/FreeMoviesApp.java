package com.movie;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.ProcessLifecycleOwner;
import androidx.multidex.MultiDex;
import com.facebook.ads.AdError;
import com.facebook.soloader.SoLoader;
import com.google.android.exoplayer2.offline.DownloadAction;
import com.google.android.exoplayer2.offline.DownloadManager;
import com.google.android.exoplayer2.offline.DownloaderConstructorHelper;
import com.google.android.exoplayer2.offline.ProgressiveDownloadAction;
import com.google.android.exoplayer2.source.dash.offline.DashDownloadAction;
import com.google.android.exoplayer2.source.hls.offline.HlsDownloadAction;
import com.google.android.exoplayer2.source.smoothstreaming.offline.SsDownloadAction;
import com.google.android.exoplayer2.upstream.DataSink;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.cache.Cache;
import com.google.android.exoplayer2.upstream.cache.CacheDataSource;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.upstream.cache.NoOpCacheEvictor;
import com.google.android.exoplayer2.upstream.cache.SimpleCache;
import com.google.gson.Gson;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.AppConfig;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.cinema.SyncLink;
import com.movie.data.remotejs.MyReactApplication;
import com.movie.ui.activity.SettingsActivity;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.model.media.MediaSource;
import com.utils.DownloadTracker;
import com.utils.PrefUtils;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.regex.Pattern;
import javax.inject.Inject;
import okhttp3.ResponseBody;
import timber.log.Timber;

public final class FreeMoviesApp extends MyReactApplication implements LifecycleObserver {
    private static boolean j;
    private File c;
    private Cache d;
    private DownloadManager e;
    private DownloadTracker f;
    @Inject
    MoviesApi g;
    private CompositeDisposable h;
    private AppComponent i;

    static {
        DownloadAction.Deserializer[] deserializerArr = {DashDownloadAction.DESERIALIZER, HlsDownloadAction.DESERIALIZER, SsDownloadAction.DESERIALIZER, ProgressiveDownloadAction.h};
        System.loadLibrary("mvlib");
    }

    private static CacheDataSourceFactory a(DefaultDataSourceFactory defaultDataSourceFactory, Cache cache) {
        return new CacheDataSourceFactory(cache, defaultDataSourceFactory, new FileDataSourceFactory(), (DataSink.Factory) null, 2, (CacheDataSource.EventListener) null);
    }

    static /* synthetic */ void a(ResponseBody responseBody) throws Exception {
    }

    private synchronized Cache j() {
        if (this.d == null) {
            this.d = new SimpleCache(new File(k(), "downloads"), new NoOpCacheEvictor());
        }
        return this.d;
    }

    private File k() {
        if (this.c == null) {
            this.c = getExternalFilesDir((String) null);
            if (this.c == null) {
                this.c = getFilesDir();
            }
        }
        return this.c;
    }

    public static SharedPreferences l() {
        return PrefUtils.d(Utils.i());
    }

    private void m() {
        RxJavaPlugins.a((Consumer<? super Throwable>) new Consumer<Throwable>(this) {
            /* renamed from: a */
            public void accept(Throwable th) throws Exception {
                Logger.a("handleUndeliverableExceptions", th.getMessage());
                th.printStackTrace();
            }
        });
    }

    private synchronized void n() {
        if (this.e == null) {
            this.e = new DownloadManager(new DownloaderConstructorHelper(j(), c()), 2, 5, new File(k(), "actions"), new DownloadAction.Deserializer[0]);
            this.f = new DownloadTracker(this, b(), new File(k(), "tracked_actions"), new DownloadAction.Deserializer[0]);
            this.e.a((DownloadManager.Listener) this.f);
        }
    }

    public static boolean o() {
        return j;
    }

    private void p() {
        this.i = DaggerAppComponent.m().a(new AppModule(this)).a();
        this.i.a(this);
    }

    /* access modifiers changed from: protected */
    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        Utils.a((ContextWrapper) this);
        MultiDex.d(this);
        m();
    }

    public DataSource.Factory b() {
        return a(new DefaultDataSourceFactory((Context) this, (DataSource.Factory) c()), j());
    }

    public HttpDataSource.Factory c() {
        return new DefaultHttpDataSourceFactory(Constants.f5838a);
    }

    public AppComponent d() {
        return this.i;
    }

    public CompositeDisposable e() {
        return this.h;
    }

    public DownloadTracker f() {
        n();
        return this.f;
    }

    public void g() {
        j = l().getBoolean("pref_low_profilev2", Build.VERSION.SDK_INT <= 20);
        this.h.b(this.g.getConfig().subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new a(this), b.f5007a));
    }

    public void h() {
        if (Build.VERSION.SDK_INT >= 26) {
            String string = getString(R.string.notification_channel_id);
            String string2 = getString(R.string.notification_channel_name);
            String string3 = getString(R.string.notification_channel_description);
            NotificationChannel notificationChannel = new NotificationChannel(string, string2, 2);
            notificationChannel.setDescription(string3);
            String string4 = getString(R.string.app_update_notification_channel_id);
            String string5 = getString(R.string.app_update_notification_channel_name);
            String string6 = getString(R.string.app_update_notification_channel_description);
            NotificationChannel notificationChannel2 = new NotificationChannel(string4, string5, 2);
            notificationChannel2.setDescription(string6);
            ((NotificationManager) getSystemService(NotificationManager.class)).createNotificationChannels(Arrays.asList(new NotificationChannel[]{notificationChannel, notificationChannel2}));
        }
    }

    public boolean i() {
        return false;
    }

    public void onCreate() {
        super.onCreate();
        Timber.a(new Timber.DebugTree());
        Utils.c((Context) this);
        p();
        SettingsActivity.a((Context) this);
        h();
        this.h = new CompositeDisposable();
        g();
        SoLoader.init((Context) this, false);
        ProcessLifecycleOwner.g().getLifecycle().a(this);
        getReactNativeHost().getReactInstanceManager().getCurrentReactContext();
    }

    public void onTerminate() {
        this.h.dispose();
        super.onTerminate();
    }

    public static FreeMoviesApp a(Context context) {
        return (FreeMoviesApp) context.getApplicationContext();
    }

    public static void a(CompositeDisposable compositeDisposable, MoviesApi moviesApi, MovieInfo movieInfo, ArrayList<MediaSource> arrayList) {
        boolean z;
        if (movieInfo.tmdbID >= 1) {
            if (((double) GlobalVariable.c().a().getSync().getSync_rate()) >= Math.random() * 100.0d) {
                SyncLink syncLink = new SyncLink();
                syncLink.i = String.valueOf(movieInfo.tmdbID);
                syncLink.s = movieInfo.session;
                syncLink.e = movieInfo.eps;
                syncLink.v = String.valueOf(Utils.v());
                syncLink.linkList = new ArrayList();
                for (int i2 = 0; i2 < 4; i2++) {
                    MediaSource mediaSource = arrayList.get(i2);
                    if (mediaSource.isNeedToSync() && (!mediaSource.isHLS() || mediaSource.getPlayHeader() == null)) {
                        Iterator<SyncLink.Link> it2 = syncLink.linkList.iterator();
                        while (true) {
                            if (it2.hasNext()) {
                                if (it2.next().l.equals(mediaSource.getOriginalLink())) {
                                    z = true;
                                    break;
                                }
                            } else {
                                z = false;
                                break;
                            }
                        }
                        if (!z && !mediaSource.isCachedLink() && !Pattern.compile(".*(ip=|expire)").matcher(mediaSource.getOriginalLink()).find()) {
                            syncLink.linkList.add(mediaSource.convertToSynLink());
                        }
                    }
                }
                compositeDisposable.b(moviesApi.syncLink(syncLink).subscribeOn(Schedulers.b()).subscribe(c.f5008a, d.f5009a));
            }
        }
    }

    public /* synthetic */ void a(AppConfig appConfig) throws Exception {
        if (appConfig != null && appConfig.getOs_type().contains("android")) {
            GlobalVariable.c().a(new Gson().a((Object) appConfig));
            AppConfig a2 = GlobalVariable.c().a();
            a(a2.getUpdate().getLink(), a2.getUpdate().getVersionCode());
        }
    }

    private void a(String str, int i2) {
        if (96 < i2) {
            PendingIntent activity = PendingIntent.getActivity(this, 0, new Intent("android.intent.action.VIEW", Uri.parse(str)), 0);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, getString(R.string.app_update_notification_channel_id));
            builder.b((int) R.mipmap.ic_launcher);
            builder.c(1);
            builder.a(activity);
            builder.a(true);
            builder.b((CharSequence) "New update is available!");
            builder.a((CharSequence) "Tap to download");
            NotificationManagerCompat.a((Context) this).a(AdError.SERVER_ERROR_CODE, builder.a());
        }
    }
}
