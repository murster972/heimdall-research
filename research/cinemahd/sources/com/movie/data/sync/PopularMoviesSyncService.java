package com.movie.data.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import timber.log.Timber;

public class PopularMoviesSyncService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f5048a = new Object();
    private static PopularMoviesSyncAdapter b;

    public IBinder onBind(Intent intent) {
        return b.getSyncAdapterBinder();
    }

    public void onCreate() {
        Timber.a("init", new Object[0]);
        synchronized (f5048a) {
            if (b == null) {
                b = new PopularMoviesSyncAdapter(getApplicationContext(), true);
            }
        }
    }
}
