package com.movie.data.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class PopularMoviesAuthenticatorService extends Service {

    /* renamed from: a  reason: collision with root package name */
    private PopularMoviesAuthenticator f5047a;

    public IBinder onBind(Intent intent) {
        return this.f5047a.getIBinder();
    }

    public void onCreate() {
        this.f5047a = new PopularMoviesAuthenticator(this);
    }
}
