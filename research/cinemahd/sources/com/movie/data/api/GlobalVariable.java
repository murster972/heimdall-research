package com.movie.data.api;

import com.google.gson.GsonBuilder;
import com.movie.data.model.AppConfig;
import com.movie.data.model.ServerConfig;
import com.utils.PrefUtils;
import com.utils.Utils;

public class GlobalVariable {
    private static GlobalVariable b;

    /* renamed from: a  reason: collision with root package name */
    private AppConfig f5013a = null;

    public GlobalVariable() {
        new ServerConfig();
    }

    public static void b() {
        b = null;
    }

    public static GlobalVariable c() {
        if (b == null) {
            b = new GlobalVariable();
        }
        return b;
    }

    public AppConfig a() {
        if (this.f5013a == null) {
            boolean z = false;
            String g = PrefUtils.g(Utils.i());
            if (g.isEmpty() || !g.contains("android")) {
                z = !Utils.q().isEmpty();
                g = Utils.getFallbackCf();
            }
            this.f5013a = (AppConfig) new GsonBuilder().a().a(g, AppConfig.class);
            if (z) {
                this.f5013a.setAds((AppConfig.AdsBean) null);
            }
        }
        return this.f5013a;
    }

    public void a(String str) {
        if (str.contains("android")) {
            PrefUtils.b(Utils.i(), str);
            this.f5013a = null;
        }
    }
}
