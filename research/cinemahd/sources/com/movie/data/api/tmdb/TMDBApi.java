package com.movie.data.api.tmdb;

import com.movie.data.model.cinema.Review;
import com.movie.data.model.cinema.Session;
import com.movie.data.model.cinema.Video;
import com.movie.data.model.tmvdb.External;
import com.movie.data.model.tmvdb.ExternalID;
import com.movie.data.model.tmvdb.ExternalTV;
import com.movie.data.model.tmvdb.FindResult;
import com.movie.data.model.tmvdb.GenreTMDB;
import com.movie.data.model.tmvdb.ListResult;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.data.model.tmvdb.SearchTMDB;
import com.movie.data.model.tmvdb.SeasonTMDB;
import com.movie.data.model.tmvdb.TvTMDB;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TMDBApi {
    @GET("discover/movie?append_to_response=external_id")
    Observable<MovieTMDB> discoverMovies(@Query("sort_by") String str, @Query("page") int i, @Query("with_genres") String str2, @Query("primary_release_year") Integer num);

    @GET("search/multi?append_to_response=external_id")
    Observable<SearchTMDB> discoverMoviesByQuery(@Query(encoded = true, value = "query") String str, @Query("page") int i);

    @GET("discover/session?append_to_response=external_id")
    Observable<Session.Response> discoverSession(@Query("tvName") long j);

    @GET("discover/tv?append_to_response=external_id")
    Observable<TvTMDB> discoverTvShows(@Query("sort_by") String str, @Query("page") int i, @Query("with_genres") String str2, @Query("first_air_date_year") Integer num);

    @GET("find/{external_id}?external_source=imdb_id")
    Observable<FindResult> findbyImdbID(@Path("external_id") String str);

    @GET("list/{list_id}")
    Observable<ListResult> getList(@Path("list_id") long j);

    @GET("movie/{movie_id}/external_ids")
    Observable<ExternalID> getMVExternalID(@Path("movie_id") long j);

    @GET("movie/{movie_id}/recommendations")
    Observable<MovieTMDB> getMVRecomendation(@Path("movie_id") long j, @Query("page") int i);

    @GET("movie/{movie_id}?append_to_response=credits,external_id")
    Observable<MovieTMDB.ResultsBean> getMovieDetails(@Path("movie_id") long j, @Query("language") String str);

    @GET("movie/{reqParam}?&sort_by=popularity.desc&append_to_response=external_id")
    Observable<MovieTMDB> getMvExtraList(@Path("reqParam") String str, @Query("page") int i, @Query("primary_release_year") Integer num);

    @GET("tv/{tv_id}/season/{season_number}")
    Observable<SeasonTMDB> getSeasonDetails(@Path("tv_id") long j, @Path("season_number") int i);

    @GET("tv/{movie_id}?append_to_response=external_id")
    Observable<TvTMDB.ResultsBean> getTVDetails(@Path("movie_id") long j, @Query("language") String str);

    @GET("find/{external_id}?language=en-US")
    Observable<ExternalTV> getTVDetails(@Path("external_id") String str, @Query("external_source") String str2);

    @GET("tv/{tv_id}/external_ids")
    Observable<ExternalID> getTVExternalID(@Path("tv_id") long j);

    @GET("tv/{tv_id}/recommendations")
    Observable<TvTMDB> getTVRecomendation(@Path("tv_id") long j, @Query("page") int i);

    @GET("tv/{tmdbID}?language=en-US")
    Observable<TvTMDB.ResultsBean> getTvDetails(@Path("tmdbID") long j);

    @GET("list/{external_id}?&sort_by=popularity.desc&append_to_response=external_id")
    Observable<External> getTvExternList(@Path("external_id") long j, @Query("page") int i, @Query("first_air_date_year") Integer num);

    @GET("tv/{reqParam}?&sort_by=popularity.desc&append_to_response=external_id")
    Observable<TvTMDB> getTvExtraList(@Path("reqParam") String str, @Query("page") int i, @Query("first_air_date_year") Integer num);

    @GET("genre/movie/list")
    Observable<GenreTMDB> movieGenres();

    @GET("movie/{id}/reviews")
    Observable<Review.Response> reviews(@Path("id") long j, @Query("page") int i);

    @GET("genre/tv/list")
    Observable<GenreTMDB> tvGenres();

    @GET("movie/{id}/videos")
    Observable<Video.Response> videos(@Path("id") long j);
}
