package com.movie.data.api.tmdb;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import retrofit2.Retrofit;

public final class TMDBModule_ProvideTMDBApiFactory implements Factory<TMDBApi> {
    public static TMDBApi a(TMDBModule tMDBModule, Retrofit retrofit) {
        TMDBApi a2 = tMDBModule.a(retrofit);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
