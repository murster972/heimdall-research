package com.movie.data.api.tmdb;

import android.app.Application;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.utils.Utils;
import dagger.Module;
import dagger.Provides;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Named;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class TMDBModule {
    static {
        Deobfuscator$app$ProductionRelease.a(47);
    }

    public static String a(String str, List<String> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            if (str.contains(list.get(i))) {
                return list.get(i + 1);
            }
        }
        return Deobfuscator$app$ProductionRelease.a(45);
    }

    static OkHttpClient b(Application application) {
        return new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).callTimeout(20, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).retryOnConnectionFailure(false).cache(new Cache(new File(application.getCacheDir(), Deobfuscator$app$ProductionRelease.a(44)), 52428800)).addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                String a2;
                String string = FreeMoviesApp.l().getString(Deobfuscator$app$ProductionRelease.a(37), GlobalVariable.c().a().getTmdb_api_keys().get(0));
                Request request = chain.request();
                HttpUrl build = request.url().newBuilder().addQueryParameter(Deobfuscator$app$ProductionRelease.a(38), string).build();
                Request build2 = request.newBuilder().url(build).build();
                Response proceed = chain.proceed(build2);
                try {
                    if (proceed.code() == 401) {
                        Deobfuscator$app$ProductionRelease.a(39);
                        Response response = proceed;
                        do {
                            a2 = TMDBModule.a(response.request().url().query(), GlobalVariable.c().a().getTmdb_api_keys());
                            response.close();
                            response = chain.proceed(build2.newBuilder().url(response.request().url().newBuilder().setQueryParameter(Deobfuscator$app$ProductionRelease.a(40), a2).build()).build());
                            if (a2.isEmpty() || response.code() != 401) {
                            }
                            a2 = TMDBModule.a(response.request().url().query(), GlobalVariable.c().a().getTmdb_api_keys());
                            response.close();
                            response = chain.proceed(build2.newBuilder().url(response.request().url().newBuilder().setQueryParameter(Deobfuscator$app$ProductionRelease.a(40), a2).build()).build());
                            break;
                        } while (response.code() != 401);
                        if (!a2.isEmpty()) {
                            if (response.code() != 401) {
                                FreeMoviesApp.l().edit().putString(Deobfuscator$app$ProductionRelease.a(43), a2).apply();
                                return response;
                            }
                        }
                        response.close();
                        FreeMoviesApp.l().edit().putString(Deobfuscator$app$ProductionRelease.a(41), GlobalVariable.c().a().getTmdb_api_keys().get(0)).apply();
                        return chain.proceed(build2.newBuilder().url(HttpUrl.parse(build.toString().replace(Deobfuscator$app$ProductionRelease.a(42), Utils.jha()))).build());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return proceed;
            }
        }).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("TMDB")
    public Retrofit a(@Named("TMDB") OkHttpClient okHttpClient, @Named("TMDB") Gson gson) {
        return new Retrofit.Builder().baseUrl(Deobfuscator$app$ProductionRelease.a(46)).client(okHttpClient).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public TMDBApi a(@Named("TMDB") Retrofit retrofit) {
        return (TMDBApi) retrofit.create(TMDBApi.class);
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("TMDB")
    public Gson a() {
        return new GsonBuilder().b().a();
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("TMDB")
    public OkHttpClient a(Application application) {
        return b(application);
    }
}
