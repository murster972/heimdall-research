package com.movie.data.api.tmdb;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class TMDBModule_ProvideGsonFactory implements Factory<Gson> {
    public static Gson a(TMDBModule tMDBModule) {
        Gson a2 = tMDBModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
