package com.movie.data.api.tmdb;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

public final class TMDBModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
    public static OkHttpClient a(TMDBModule tMDBModule, Application application) {
        OkHttpClient a2 = tMDBModule.a(application);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
