package com.movie.data.api.realdebrid;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class RealDebridModule_ProvideGsonFactory implements Factory<Gson> {

    /* renamed from: a  reason: collision with root package name */
    private final RealDebridModule f5016a;

    public RealDebridModule_ProvideGsonFactory(RealDebridModule realDebridModule) {
        this.f5016a = realDebridModule;
    }

    public static RealDebridModule_ProvideGsonFactory a(RealDebridModule realDebridModule) {
        return new RealDebridModule_ProvideGsonFactory(realDebridModule);
    }

    public static Gson b(RealDebridModule realDebridModule) {
        return c(realDebridModule);
    }

    public static Gson c(RealDebridModule realDebridModule) {
        Gson a2 = realDebridModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    public Gson get() {
        return b(this.f5016a);
    }
}
