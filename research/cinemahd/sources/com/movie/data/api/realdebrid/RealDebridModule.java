package com.movie.data.api.realdebrid;

import android.app.Application;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.original.Constants;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.model.debrid.realdebrid.RealDebridCredentialsInfo;
import com.original.tase.model.debrid.realdebrid.RealDebridGetTokenResult;
import com.original.tase.utils.Utils;
import dagger.Module;
import dagger.Provides;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Named;
import javax.inject.Singleton;
import okhttp3.Authenticator;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RealDebridModule {
    static OkHttpClient b(Application application) {
        return new OkHttpClient.Builder().connectTimeout(30, TimeUnit.SECONDS).writeTimeout(30, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).authenticator(new Authenticator() {
            public Request authenticate(Route route, Response response) throws IOException {
                String clientId = RealDebridCredentialsHelper.c().getClientId();
                String clientSecret = RealDebridCredentialsHelper.c().getClientSecret();
                String refreshToken = RealDebridCredentialsHelper.c().getRefreshToken();
                HashMap hashMap = new HashMap();
                hashMap.put("User-Agent", Constants.f5838a);
                HttpHelper e = HttpHelper.e();
                String a2 = e.a("https://api.real-debrid.com/oauth/v2/token", "client_id=" + Utils.a(clientId, new boolean[0]) + "&client_secret=" + Utils.a(clientSecret, new boolean[0]) + "&code=" + Utils.a(refreshToken, new boolean[0]) + "&grant_type=http://oauth.net/grant_type/device/1.0", false, (Map<String, String>[]) new Map[]{hashMap});
                if (!a2.contains("access_token")) {
                    return null;
                }
                RealDebridGetTokenResult realDebridGetTokenResult = (RealDebridGetTokenResult) new Gson().a(a2, RealDebridGetTokenResult.class);
                RealDebridCredentialsInfo realDebridCredentialsInfo = new RealDebridCredentialsInfo();
                realDebridCredentialsInfo.setAccessToken(realDebridGetTokenResult.getAccess_token());
                realDebridCredentialsInfo.setRefreshToken(realDebridGetTokenResult.getRefresh_token());
                realDebridCredentialsInfo.setClientId(clientId);
                realDebridCredentialsInfo.setClientSecret(clientSecret);
                RealDebridCredentialsHelper.a(realDebridCredentialsInfo);
                Request.Builder newBuilder = response.request().newBuilder();
                Request.Builder header = newBuilder.header("Authorization", "Bearer " + realDebridGetTokenResult.getAccess_token());
                return header.header("User-Agent", "Bearer " + Constants.f5838a).build();
            }
        }).addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request.Builder newBuilder = chain.request().newBuilder();
                Request.Builder addHeader = newBuilder.addHeader("Authorization", "Bearer " + RealDebridCredentialsHelper.c().getAccessToken());
                return chain.proceed(addHeader.addHeader("User-Agent", "Bearer " + Constants.f5838a).build());
            }
        }).cache(new Cache(new File(application.getCacheDir(), "httprd"), 52428800)).build();
    }

    /* access modifiers changed from: package-private */
    @Singleton
    @Provides
    @Named("RealDebrid")
    public Retrofit a(@Named("RealDebrid") OkHttpClient okHttpClient, @Named("RealDebrid") Gson gson) {
        return new Retrofit.Builder().baseUrl("https://api.real-debrid.com/").client(okHttpClient).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public RealDebridApi a(@Named("RealDebrid") Retrofit retrofit) {
        return (RealDebridApi) retrofit.create(RealDebridApi.class);
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("RealDebrid")
    public Gson a() {
        return new GsonBuilder().b().a();
    }

    /* access modifiers changed from: package-private */
    @Singleton
    @Provides
    @Named("RealDebrid")
    public OkHttpClient a(Application application) {
        return b(application);
    }
}
