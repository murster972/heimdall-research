package com.movie.data.api.realdebrid;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public final class RealDebridModule_ProvideRestAdapterFactory implements Factory<Retrofit> {

    /* renamed from: a  reason: collision with root package name */
    private final RealDebridModule f5018a;
    private final Provider<OkHttpClient> b;
    private final Provider<Gson> c;

    public RealDebridModule_ProvideRestAdapterFactory(RealDebridModule realDebridModule, Provider<OkHttpClient> provider, Provider<Gson> provider2) {
        this.f5018a = realDebridModule;
        this.b = provider;
        this.c = provider2;
    }

    public static RealDebridModule_ProvideRestAdapterFactory a(RealDebridModule realDebridModule, Provider<OkHttpClient> provider, Provider<Gson> provider2) {
        return new RealDebridModule_ProvideRestAdapterFactory(realDebridModule, provider, provider2);
    }

    public static Retrofit b(RealDebridModule realDebridModule, Provider<OkHttpClient> provider, Provider<Gson> provider2) {
        return a(realDebridModule, provider.get(), provider2.get());
    }

    public static Retrofit a(RealDebridModule realDebridModule, OkHttpClient okHttpClient, Gson gson) {
        Retrofit a2 = realDebridModule.a(okHttpClient, gson);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    public Retrofit get() {
        return b(this.f5018a, this.b, this.c);
    }
}
