package com.movie.data.api.realdebrid;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import retrofit2.Retrofit;

public final class RealDebridModule_ProvideRealDebridApiFactory implements Factory<RealDebridApi> {
    public static RealDebridApi a(RealDebridModule realDebridModule, Retrofit retrofit) {
        RealDebridApi a2 = realDebridModule.a(retrofit);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
