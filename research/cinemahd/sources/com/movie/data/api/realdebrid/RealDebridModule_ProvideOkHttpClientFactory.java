package com.movie.data.api.realdebrid;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import okhttp3.OkHttpClient;

public final class RealDebridModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {

    /* renamed from: a  reason: collision with root package name */
    private final RealDebridModule f5017a;
    private final Provider<Application> b;

    public RealDebridModule_ProvideOkHttpClientFactory(RealDebridModule realDebridModule, Provider<Application> provider) {
        this.f5017a = realDebridModule;
        this.b = provider;
    }

    public static RealDebridModule_ProvideOkHttpClientFactory a(RealDebridModule realDebridModule, Provider<Application> provider) {
        return new RealDebridModule_ProvideOkHttpClientFactory(realDebridModule, provider);
    }

    public static OkHttpClient b(RealDebridModule realDebridModule, Provider<Application> provider) {
        return a(realDebridModule, provider.get());
    }

    public static OkHttpClient a(RealDebridModule realDebridModule, Application application) {
        OkHttpClient a2 = realDebridModule.a(application);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    public OkHttpClient get() {
        return b(this.f5017a, this.b);
    }
}
