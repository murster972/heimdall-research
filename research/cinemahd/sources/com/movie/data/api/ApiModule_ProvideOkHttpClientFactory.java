package com.movie.data.api;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

public final class ApiModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
    public static OkHttpClient a(ApiModule apiModule, Application application) {
        OkHttpClient a2 = apiModule.a(application);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
