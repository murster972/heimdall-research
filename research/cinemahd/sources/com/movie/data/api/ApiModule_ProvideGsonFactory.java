package com.movie.data.api;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class ApiModule_ProvideGsonFactory implements Factory<Gson> {
    public static Gson a(ApiModule apiModule) {
        Gson a2 = apiModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
