package com.movie.data.api;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import retrofit2.Retrofit;

public final class ApiModule_ProvideMoviesApiFactory implements Factory<MoviesApi> {
    public static MoviesApi a(ApiModule apiModule, Retrofit retrofit) {
        MoviesApi a2 = apiModule.a(retrofit);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
