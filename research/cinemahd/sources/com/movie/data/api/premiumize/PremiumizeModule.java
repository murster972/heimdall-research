package com.movie.data.api.premiumize;

import com.original.Constants;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class PremiumizeModule {

    /* renamed from: a  reason: collision with root package name */
    private static Retrofit f5015a;
    private static PremiumizeApi b;

    static OkHttpClient a() {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                return chain.proceed(chain.request().newBuilder().addHeader("accept", TraktV2.CONTENT_TYPE_JSON).addHeader("User-Agent", Constants.f5838a).build());
            }
        }).build();
    }

    public static PremiumizeApi b() {
        if (b == null) {
            b = (PremiumizeApi) c().create(PremiumizeApi.class);
        }
        return b;
    }

    private static Retrofit c() {
        if (f5015a == null) {
            f5015a = new Retrofit.Builder().baseUrl("https://www.premiumize.me").addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).client(a()).build();
        }
        return f5015a;
    }
}
