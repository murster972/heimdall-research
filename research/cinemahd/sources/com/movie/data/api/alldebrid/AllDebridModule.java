package com.movie.data.api.alldebrid;

import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.utils.Utils;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class AllDebridModule {

    /* renamed from: a  reason: collision with root package name */
    private static Retrofit f5014a;
    private static AllDebridApi b;

    static OkHttpClient a() {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                if (AllDebridCredentialsHelper.b().getApikey().isEmpty() || AllDebridCredentialsHelper.c()) {
                    return null;
                }
                Request request = chain.request();
                return chain.proceed(request.newBuilder().url(request.url().newBuilder().addQueryParameter("agent", Utils.k).addQueryParameter("apikey", AllDebridCredentialsHelper.b().getApikey()).build()).build());
            }
        }).build();
    }

    public static AllDebridApi b() {
        if (b == null) {
            b = (AllDebridApi) c().create(AllDebridApi.class);
        }
        return b;
    }

    private static Retrofit c() {
        if (f5014a == null) {
            f5014a = new Retrofit.Builder().baseUrl("https://api.alldebrid.com/").addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create()).client(a()).build();
        }
        return f5014a;
    }
}
