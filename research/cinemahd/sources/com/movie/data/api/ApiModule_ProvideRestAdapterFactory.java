package com.movie.data.api;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public final class ApiModule_ProvideRestAdapterFactory implements Factory<Retrofit> {
    public static Retrofit a(ApiModule apiModule, OkHttpClient okHttpClient, Gson gson) {
        Retrofit a2 = apiModule.a(okHttpClient, gson);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
