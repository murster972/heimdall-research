package com.movie.data.api.tvmaze;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import retrofit2.Retrofit;

public final class TVMazeModule_ProvideTVMazeApiFactory implements Factory<TVMazeApi> {
    public static TVMazeApi a(TVMazeModule tVMazeModule, Retrofit retrofit) {
        TVMazeApi a2 = tVMazeModule.a(retrofit);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
