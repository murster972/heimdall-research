package com.movie.data.api.tvmaze;

import android.app.Application;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dagger.Module;
import dagger.Provides;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import java.io.File;
import java.util.concurrent.TimeUnit;
import javax.inject.Named;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public final class TVMazeModule {
    static OkHttpClient b(Application application) {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).cache(new Cache(new File(application.getCacheDir(), Deobfuscator$app$ProductionRelease.a(48)), 52428800)).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("TVMaze")
    public Retrofit a(@Named("TVMaze") OkHttpClient okHttpClient, @Named("TVMaze") Gson gson) {
        return new Retrofit.Builder().baseUrl(Deobfuscator$app$ProductionRelease.a(49)).client(okHttpClient).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public TVMazeApi a(@Named("TVMaze") Retrofit retrofit) {
        return (TVMazeApi) retrofit.create(TVMazeApi.class);
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("TVMaze")
    public Gson a() {
        return new GsonBuilder().b().a();
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("TVMaze")
    public OkHttpClient a(Application application) {
        return b(application);
    }
}
