package com.movie.data.api.tvmaze;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

public final class TVMazeModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
    public static OkHttpClient a(TVMazeModule tVMazeModule, Application application) {
        OkHttpClient a2 = tVMazeModule.a(application);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
