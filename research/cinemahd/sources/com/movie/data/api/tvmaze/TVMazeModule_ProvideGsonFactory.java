package com.movie.data.api.tvmaze;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class TVMazeModule_ProvideGsonFactory implements Factory<Gson> {
    public static Gson a(TVMazeModule tVMazeModule) {
        Gson a2 = tVMazeModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
