package com.movie.data.api;

import android.app.Application;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.utils.Utils;
import dagger.Module;
import dagger.Provides;
import io.michaelrocks.paranoid.Deobfuscator$app$ProductionRelease;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public final class ApiModule {
    static OkHttpClient b(Application application) {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).cache(new Cache(new File(application.getCacheDir(), Deobfuscator$app$ProductionRelease.a(18)), 52428800)).addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                String configKey = Utils.getConfigKey();
                String q = Utils.q();
                Request request = chain.request();
                Request.Builder addHeader = request.newBuilder().addHeader(Deobfuscator$app$ProductionRelease.a(11), configKey).addHeader(Deobfuscator$app$ProductionRelease.a(12), Utils.b()).addHeader(Deobfuscator$app$ProductionRelease.a(13), Integer.toString(Utils.v())).addHeader(Deobfuscator$app$ProductionRelease.a(14), Utils.j()).addHeader(Deobfuscator$app$ProductionRelease.a(15), q);
                if (request.headers().get(Deobfuscator$app$ProductionRelease.a(16)) == null) {
                    addHeader.addHeader(Deobfuscator$app$ProductionRelease.a(17), Utils.f());
                }
                return chain.proceed(addHeader.build());
            }
        }).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public Retrofit a(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder().baseUrl(Utils.u()).client(okHttpClient).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).addConverterFactory(ScalarsConverterFactory.create()).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public MoviesApi a(Retrofit retrofit) {
        return (MoviesApi) retrofit.create(MoviesApi.class);
    }

    /* access modifiers changed from: package-private */
    @Provides
    public Gson a() {
        return new GsonBuilder().c().b().a();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public OkHttpClient a(Application application) {
        return b(application);
    }
}
