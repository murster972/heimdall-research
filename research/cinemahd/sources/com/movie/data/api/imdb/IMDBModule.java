package com.movie.data.api.imdb;

import android.app.Application;
import com.facebook.common.util.UriUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movie.data.api.GlobalVariable;
import com.utils.Getlink.Interceptor.CloudflareInterceptor;
import dagger.Module;
import dagger.Provides;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import javax.inject.Named;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class IMDBModule {
    static OkHttpClient b(Application application) {
        return new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).cache(new Cache(new File(application.getCacheDir(), UriUtil.HTTP_SCHEME), 52428800)).addInterceptor(new Interceptor() {
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request request = chain.request();
                return chain.proceed(request.newBuilder().url(request.url().newBuilder().addQueryParameter("api_key", GlobalVariable.c().a().getTmdb_api_keys().get(0)).build()).build());
            }
        }).addInterceptor(new CloudflareInterceptor()).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("IMDBM")
    public Retrofit a(@Named("IMDBM") OkHttpClient okHttpClient, @Named("IMDBM") Gson gson) {
        return new Retrofit.Builder().baseUrl("https://www.imdb.com/").client(okHttpClient).addCallAdapterFactory(RxJava2CallAdapterFactory.create()).addConverterFactory(GsonConverterFactory.create(gson)).build();
    }

    /* access modifiers changed from: package-private */
    @Provides
    public IMDBApi a(@Named("IMDBM") Retrofit retrofit) {
        return (IMDBApi) retrofit.create(IMDBApi.class);
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("IMDBM")
    public Gson a() {
        return new GsonBuilder().b().a();
    }

    /* access modifiers changed from: package-private */
    @Provides
    @Named("IMDBM")
    public OkHttpClient a(Application application) {
        return b(application);
    }
}
