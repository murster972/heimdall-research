package com.movie.data.api.imdb;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class IMDBModule_ProvideGsonFactory implements Factory<Gson> {
    public static Gson a(IMDBModule iMDBModule) {
        Gson a2 = iMDBModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
