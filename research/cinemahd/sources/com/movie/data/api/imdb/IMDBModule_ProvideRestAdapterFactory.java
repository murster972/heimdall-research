package com.movie.data.api.imdb;

import com.google.gson.Gson;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public final class IMDBModule_ProvideRestAdapterFactory implements Factory<Retrofit> {
    public static Retrofit a(IMDBModule iMDBModule, OkHttpClient okHttpClient, Gson gson) {
        Retrofit a2 = iMDBModule.a(okHttpClient, gson);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
