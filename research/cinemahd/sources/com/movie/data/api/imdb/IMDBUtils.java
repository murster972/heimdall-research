package com.movie.data.api.imdb;

import com.database.entitys.MovieEntity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import okhttp3.ResponseBody;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class IMDBUtils {
    public static MovieEntity a(String str, ResponseBody responseBody, boolean z) throws IOException {
        Document b = Jsoup.b(responseBody.string());
        if (!z) {
            Element h = b.h("div.title_wrapper");
            String G = h.h(".originalTitle").G();
            Elements g = h.g(".subtext a");
            String c = b.g("div.ratingValue strong span[itemprop]").c();
            ArrayList arrayList = new ArrayList();
            Iterator it2 = g.iterator();
            String str2 = "";
            while (it2.hasNext()) {
                Element element = (Element) it2.next();
                if (element.toString().contains("genres")) {
                    arrayList.add(element.G());
                } else if (element.toString().contains("releaseinfo")) {
                    str2 = element.G();
                }
            }
            String G2 = b.h("div.summary_text").G();
            String b2 = b.h(".poster a img[src]").b("src");
            MovieEntity movieEntity = new MovieEntity();
            movieEntity.setImdbIDStr(str);
            movieEntity.setTV(false);
            movieEntity.setPoster_path(b2);
            movieEntity.setName(G);
            movieEntity.setRealeaseDate(str2);
            movieEntity.setVote(Double.valueOf(c));
            movieEntity.setOverview(G2);
            return movieEntity;
        }
        String b3 = b.h(".poster a img[src]").b("src");
        MovieEntity movieEntity2 = new MovieEntity();
        movieEntity2.setPoster_path(b3);
        return movieEntity2;
    }
}
