package com.movie.data.api.imdb;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

public final class IMDBModule_ProvideOkHttpClientFactory implements Factory<OkHttpClient> {
    public static OkHttpClient a(IMDBModule iMDBModule, Application application) {
        OkHttpClient a2 = iMDBModule.a(application);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
