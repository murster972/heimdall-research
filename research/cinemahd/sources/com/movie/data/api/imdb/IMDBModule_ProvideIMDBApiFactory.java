package com.movie.data.api.imdb;

import dagger.internal.Factory;
import dagger.internal.Preconditions;
import retrofit2.Retrofit;

public final class IMDBModule_ProvideIMDBApiFactory implements Factory<IMDBApi> {
    public static IMDBApi a(IMDBModule iMDBModule, Retrofit retrofit) {
        IMDBApi a2 = iMDBModule.a(retrofit);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
