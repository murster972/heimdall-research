package com.movie.data.api.tvdb;

import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class TvdbModule_ProvideTheTvdbFactory implements Factory<TheTvdb> {
    public static TheTvdb a(TvdbModule tvdbModule) {
        TheTvdb a2 = tvdbModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
