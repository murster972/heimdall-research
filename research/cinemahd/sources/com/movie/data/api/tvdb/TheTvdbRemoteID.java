package com.movie.data.api.tvdb;

import com.database.entitys.MovieEntity;
import com.original.tase.helper.http.HttpHelper;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import okhttp3.Request;
import okhttp3.Response;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

public class TheTvdbRemoteID {
    /* access modifiers changed from: private */
    public static XmlPullParser b(InputStream inputStream) throws XmlPullParserException {
        XmlPullParser newPullParser = XmlPullParserFactory.newInstance().newPullParser();
        newPullParser.setFeature("http://xmlpull.org/v1/doc/features.html#process-namespaces", false);
        newPullParser.setInput(inputStream, (String) null);
        return newPullParser;
    }

    public static Observable<List<MovieEntity>> a(final String str) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                Response execute = HttpHelper.e().b().newCall(new Request.Builder().url("http://thetvdb.com/api/GetSeriesByRemoteID.php?imdbid=" + str).build()).execute();
                if (execute.code() == 200) {
                    XmlPullParser a2 = TheTvdbRemoteID.b(execute.body().byteStream());
                    MovieEntity movieEntity = new MovieEntity();
                    movieEntity.setTvdbID(0);
                    movieEntity.setImdbIDStr(str);
                    movieEntity.setTV(true);
                    for (int eventType = a2.getEventType(); eventType != 1; eventType = a2.next()) {
                        if (eventType == 2) {
                            String name = a2.getName();
                            if ("seriesid".equals(name)) {
                                movieEntity.setTvdbID(Long.valueOf(a2.nextText()).longValue());
                            } else if ("SeriesName".equals(name)) {
                                movieEntity.setName(a2.nextText());
                            } else if ("Overview".equals(name)) {
                                movieEntity.setOverview(a2.nextText());
                            } else if ("FirstAired".equals(name)) {
                                movieEntity.setRealeaseDate(a2.nextText());
                            }
                        }
                    }
                    ArrayList arrayList = new ArrayList();
                    if (movieEntity.getTvdbID() != 0) {
                        arrayList.add(movieEntity);
                    }
                    observableEmitter.onNext(arrayList);
                    observableEmitter.onComplete();
                }
            }
        }).subscribeOn(Schedulers.b());
    }
}
