package com.movie.data.api.trakt;

import com.facebook.common.util.UriUtil;
import com.utils.Utils;
import com.uwetrottmann.trakt5.TraktV2;
import java.io.File;
import java.util.concurrent.TimeUnit;
import okhttp3.Cache;
import okhttp3.OkHttpClient;

public class TraktV2Cachced extends TraktV2 {

    /* renamed from: a  reason: collision with root package name */
    private OkHttpClient f5019a;

    public TraktV2Cachced(String str, String str2, String str3) {
        super(str, str2, str3);
    }

    /* access modifiers changed from: protected */
    public synchronized OkHttpClient okHttpClient() {
        if (this.f5019a == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder();
            setOkHttpClientDefaults(builder);
            builder.connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).cache(new Cache(new File(Utils.i().getCacheDir(), UriUtil.HTTP_SCHEME), 20971520));
            this.f5019a = builder.build();
        }
        return this.f5019a;
    }
}
