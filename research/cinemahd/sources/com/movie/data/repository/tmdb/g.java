package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.TvTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class g implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TMDBRepositoryImpl f5033a;

    public /* synthetic */ g(TMDBRepositoryImpl tMDBRepositoryImpl) {
        this.f5033a = tMDBRepositoryImpl;
    }

    public final Object apply(Object obj) {
        return this.f5033a.a((TvTMDB.ResultsBean) obj);
    }
}
