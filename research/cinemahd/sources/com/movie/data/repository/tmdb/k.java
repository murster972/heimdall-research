package com.movie.data.repository.tmdb;

import com.movie.data.model.cinema.Video;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class k implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ k f5037a = new k();

    private /* synthetic */ k() {
    }

    public final Object apply(Object obj) {
        Video.Response response = (Video.Response) obj;
        TMDBRepositoryImpl.a(response);
        return response;
    }
}
