package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.MovieTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class a implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ a f5027a = new a();

    private /* synthetic */ a() {
    }

    public final Object apply(Object obj) {
        return TMDBRepositoryImpl.a(((MovieTMDB) obj).getResults(), false);
    }
}
