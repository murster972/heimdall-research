package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.MovieTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class i implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ i f5035a = new i();

    private /* synthetic */ i() {
    }

    public final Object apply(Object obj) {
        return TMDBRepositoryImpl.a(((MovieTMDB) obj).getResults(), false);
    }
}
