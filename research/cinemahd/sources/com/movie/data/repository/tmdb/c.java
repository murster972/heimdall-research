package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.SearchTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class c implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ c f5029a = new c();

    private /* synthetic */ c() {
    }

    public final Object apply(Object obj) {
        return TMDBRepositoryImpl.b(((SearchTMDB) obj).getResults());
    }
}
