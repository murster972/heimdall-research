package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.SeasonTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class b implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ b f5028a = new b();

    private /* synthetic */ b() {
    }

    public final Object apply(Object obj) {
        return ((SeasonTMDB) obj).getEpisodes();
    }
}
