package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.TvTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class d implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ d f5030a = new d();

    private /* synthetic */ d() {
    }

    public final Object apply(Object obj) {
        return TMDBRepositoryImpl.b(((TvTMDB) obj).getResults(), true);
    }
}
