package com.movie.data.repository.tmdb;

import android.util.SparseArray;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.Categorys;
import com.movie.data.model.cinema.Video;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.data.model.tmvdb.SearchTMDB;
import com.movie.data.model.tmvdb.SeasonTMDB;
import com.movie.data.model.tmvdb.TvTMDB;
import com.movie.data.repository.MoviesRepository;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;

public final class TMDBRepositoryImpl implements MoviesRepository {

    /* renamed from: a  reason: collision with root package name */
    private final TMDBApi f5026a;
    private final MvDatabase b;

    static {
        f fVar = f.f5032a;
    }

    public TMDBRepositoryImpl(TMDBApi tMDBApi, MvDatabase mvDatabase) {
        this.f5026a = tMDBApi;
        this.b = mvDatabase;
    }

    static /* synthetic */ Video.Response a(Video.Response response) throws Exception {
        return response;
    }

    public Observable<List<MovieEntity>> a(int i, int i2, int i3) {
        String str = null;
        Integer valueOf = i3 == -1 ? null : Integer.valueOf(i3);
        TMDBApi tMDBApi = this.f5026a;
        if (i != -1) {
            str = String.valueOf(i);
        }
        return tMDBApi.discoverMovies("popularity.desc", i2, str, valueOf).map(a.f5027a).subscribeOn(Schedulers.b());
    }

    public Observable<List<MovieEntity>> b(int i, int i2, int i3) {
        String str = null;
        Integer valueOf = i3 == -1 ? null : Integer.valueOf(i3);
        TMDBApi tMDBApi = this.f5026a;
        if (i != -1) {
            str = String.valueOf(i);
        }
        return tMDBApi.discoverTvShows("popularity.desc", i2, str, valueOf).map(e.f5031a).subscribeOn(Schedulers.b());
    }

    public Observable<List<SeasonTMDB.EpisodesBean>> c(long j, int i) {
        return this.f5026a.getSeasonDetails(j, i).map(b.f5028a).subscribeOn(Schedulers.b());
    }

    public Observable<List<SeasonEntity>> discoverSession(long j) {
        return this.f5026a.getTVDetails(j, (String) null).map(new g(this));
    }

    public Observable<Video.Response> videos(long j) {
        return this.f5026a.videos(j).map(k.f5037a);
    }

    public static List<MovieEntity> b(List<SearchTMDB.ResultsBean> list) {
        ArrayList arrayList = new ArrayList();
        for (SearchTMDB.ResultsBean next : list) {
            MovieEntity convert = next.convert();
            if (convert != null) {
                if (convert.getTV().booleanValue()) {
                    a(convert, next, Categorys.getTVCategory());
                } else {
                    a(convert, next, Categorys.getMVCategory());
                }
                arrayList.add(convert);
            }
        }
        return arrayList;
    }

    public Observable<List<MovieEntity>> a(String str, int i) {
        return this.f5026a.discoverMoviesByQuery(str, i).map(c.f5029a).subscribeOn(Schedulers.b());
    }

    /* access modifiers changed from: private */
    public static List<MovieEntity> a(List<MovieTMDB.ResultsBean> list, boolean z) {
        ArrayList arrayList = new ArrayList();
        for (MovieTMDB.ResultsBean next : list) {
            MovieEntity convert = next.convert();
            convert.setTV(Boolean.valueOf(z));
            if (convert.getTV().booleanValue()) {
                a(convert, next, Categorys.getTVCategory());
            } else {
                a(convert, next, Categorys.getMVCategory());
            }
            arrayList.add(convert);
        }
        return arrayList;
    }

    /* access modifiers changed from: private */
    public static List<MovieEntity> b(List<TvTMDB.ResultsBean> list, boolean z) {
        ArrayList arrayList = new ArrayList();
        for (TvTMDB.ResultsBean next : list) {
            MovieEntity convert = next.convert();
            convert.setTV(Boolean.valueOf(z));
            if (convert.getTV().booleanValue()) {
                a(convert, next, Categorys.getTVCategory());
            } else {
                a(convert, next, Categorys.getMVCategory());
            }
            arrayList.add(convert);
        }
        return arrayList;
    }

    private static MovieEntity b(TvTMDB.ResultsBean resultsBean) {
        MovieEntity convert = resultsBean.convert();
        convert.setTV(true);
        convert.setNumberSeason(resultsBean.getNumber_of_seasons());
        if (convert.getTV().booleanValue()) {
            a(convert, resultsBean, Categorys.getTVCategory());
        } else {
            a(convert, resultsBean, Categorys.getMVCategory());
        }
        return convert;
    }

    public /* synthetic */ List a(TvTMDB.ResultsBean resultsBean) throws Exception {
        MovieEntity b2 = b(resultsBean);
        this.b.m().a(b2.getNumberSeason(), b2.getTmdbID(), b2.getImdbIDStr(), b2.getTraktID(), b2.getTvdbID());
        return a(resultsBean.getSeasons());
    }

    public static void a(MovieEntity movieEntity, MovieTMDB.ResultsBean resultsBean, SparseArray<String> sparseArray) {
        if (sparseArray != null) {
            ArrayList arrayList = new ArrayList();
            for (Integer intValue : resultsBean.getGenre_ids()) {
                int intValue2 = intValue.intValue();
                if (sparseArray.get(intValue2) != null) {
                    arrayList.add(sparseArray.get(intValue2));
                }
            }
            movieEntity.setGenres(arrayList);
        }
    }

    public Observable<List<MovieEntity>> b(String str, int i, int i2) {
        return this.f5026a.getMvExtraList(str, i, i2 == -1 ? null : Integer.valueOf(i2)).map(i.f5035a).subscribeOn(Schedulers.b());
    }

    public static void a(MovieEntity movieEntity, TvTMDB.ResultsBean resultsBean, SparseArray<String> sparseArray) {
        if (sparseArray != null) {
            ArrayList arrayList = new ArrayList();
            if (resultsBean.getGenre_ids() != null) {
                for (Integer intValue : resultsBean.getGenre_ids()) {
                    int intValue2 = intValue.intValue();
                    if (sparseArray.get(intValue2) != null) {
                        arrayList.add(sparseArray.get(intValue2));
                    }
                }
            } else if (resultsBean.getGenres() != null) {
                for (TvTMDB.ResultsBean.GenresBean next : resultsBean.getGenres()) {
                    if (sparseArray.get(next.getId()) != null) {
                        arrayList.add(next.getName());
                    }
                }
            }
            movieEntity.setGenres(arrayList);
        }
    }

    public Observable<List<MovieEntity>> b(long j, int i) {
        return this.f5026a.getTVRecomendation(j, i).map(d.f5030a).subscribeOn(Schedulers.b());
    }

    public static void a(MovieEntity movieEntity, SearchTMDB.ResultsBean resultsBean, SparseArray<String> sparseArray) {
        if (sparseArray != null && resultsBean.getGenre_ids() != null) {
            ArrayList arrayList = new ArrayList();
            for (Integer intValue : resultsBean.getGenre_ids()) {
                int intValue2 = intValue.intValue();
                if (sparseArray.get(intValue2) != null) {
                    arrayList.add(sparseArray.get(intValue2));
                }
            }
            movieEntity.setGenres(arrayList);
        }
    }

    public List<SeasonEntity> a(List<TvTMDB.ResultsBean.SeasonsBean> list) {
        ArrayList arrayList = new ArrayList();
        for (TvTMDB.ResultsBean.SeasonsBean convert : list) {
            arrayList.add(convert.convert());
        }
        return arrayList;
    }

    public Observable<List<MovieEntity>> a(String str, int i, int i2) {
        return this.f5026a.getTvExtraList(str, i, i2 == -1 ? null : Integer.valueOf(i2)).map(h.f5034a).subscribeOn(Schedulers.b());
    }

    public Observable<List<MovieEntity>> a(long j, int i) {
        return this.f5026a.getMVRecomendation(j, i).map(j.f5036a).subscribeOn(Schedulers.b());
    }
}
