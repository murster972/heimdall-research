package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.TvTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class h implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ h f5034a = new h();

    private /* synthetic */ h() {
    }

    public final Object apply(Object obj) {
        return TMDBRepositoryImpl.b(((TvTMDB) obj).getResults(), true);
    }
}
