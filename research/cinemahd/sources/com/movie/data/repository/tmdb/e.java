package com.movie.data.repository.tmdb;

import com.movie.data.model.tmvdb.TvTMDB;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class e implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ e f5031a = new e();

    private /* synthetic */ e() {
    }

    public final Object apply(Object obj) {
        return TMDBRepositoryImpl.b(((TvTMDB) obj).getResults(), true);
    }
}
