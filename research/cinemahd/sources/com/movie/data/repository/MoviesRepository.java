package com.movie.data.repository;

import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.movie.data.model.cinema.Video;
import io.reactivex.Observable;
import java.util.List;

public interface MoviesRepository {
    Observable<List<MovieEntity>> a(int i, int i2, int i3);

    Observable<List<MovieEntity>> a(long j, int i);

    Observable<List<MovieEntity>> a(String str, int i);

    Observable<List<MovieEntity>> a(String str, int i, int i2);

    Observable<List<MovieEntity>> b(int i, int i2, int i3);

    Observable<List<MovieEntity>> b(long j, int i);

    Observable<List<MovieEntity>> b(String str, int i, int i2);

    Observable<List<SeasonEntity>> discoverSession(long j);

    Observable<Video.Response> videos(long j);
}
