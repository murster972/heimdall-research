package com.movie.data.repository;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.tmdb.TMDBRepositoryImpl;
import dagger.Module;
import dagger.Provides;

@Module
public final class RepositoryModule {
    @Provides
    public MoviesRepository a(MoviesApi moviesApi, TMDBApi tMDBApi, MvDatabase mvDatabase) {
        return new TMDBRepositoryImpl(tMDBApi, mvDatabase);
    }
}
