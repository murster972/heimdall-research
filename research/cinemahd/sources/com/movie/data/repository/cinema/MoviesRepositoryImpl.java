package com.movie.data.repository.cinema;

import android.util.SparseArray;
import com.database.entitys.MovieEntity;
import com.movie.data.model.cinema.Movie;
import java.util.ArrayList;

public final class MoviesRepositoryImpl {
    static {
        a aVar = a.f5025a;
    }

    public static void a(MovieEntity movieEntity, Movie movie, SparseArray<String> sparseArray) {
        if (sparseArray != null) {
            ArrayList arrayList = new ArrayList();
            for (Integer intValue : movie.getGenreIds()) {
                int intValue2 = intValue.intValue();
                if (sparseArray.get(intValue2) != null) {
                    arrayList.add(sparseArray.get(intValue2));
                }
            }
            movieEntity.setGenres(arrayList);
        }
    }
}
