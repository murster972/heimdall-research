package com.movie.data.repository.trakt;

import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.movie.data.model.cinema.Video;
import com.movie.data.repository.MoviesRepository;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.trakt.TraktHelper;
import com.uwetrottmann.trakt5.TraktV2;
import com.uwetrottmann.trakt5.entities.Movie;
import com.uwetrottmann.trakt5.entities.SearchResult;
import com.uwetrottmann.trakt5.entities.Show;
import com.uwetrottmann.trakt5.entities.TrendingShow;
import com.uwetrottmann.trakt5.enums.Extended;
import com.uwetrottmann.trakt5.services.Movies;
import com.uwetrottmann.trakt5.services.Shows;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import org.joda.time.DateTime;
import org.threeten.bp.LocalDate;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.format.DateTimeFormatter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TraktRepositoryImpl implements MoviesRepository {

    /* renamed from: a  reason: collision with root package name */
    TraktV2 f5038a;
    Shows b;
    Movies c;

    public TraktRepositoryImpl(MvDatabase mvDatabase) {
        this.f5038a = null;
        this.b = null;
        this.c = null;
        this.f5038a = TraktHelper.a();
        this.b = this.f5038a.shows();
        this.c = this.f5038a.movies();
    }

    public Observable<List<MovieEntity>> a(int i, final int i2, int i3) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            public void subscribe(final ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                TraktRepositoryImpl.this.c.popular(Integer.valueOf(i2), 20, Extended.FULL).enqueue(new Callback<List<Movie>>() {
                    public void onFailure(Call<List<Movie>> call, Throwable th) {
                        observableEmitter.onComplete();
                    }

                    public void onResponse(Call<List<Movie>> call, Response<List<Movie>> response) {
                        ArrayList arrayList = new ArrayList();
                        if (!(response == null || response.body() == null)) {
                            for (Movie a2 : response.body()) {
                                MovieEntity a3 = TraktRepositoryImpl.this.a(a2);
                                if (a3 != null) {
                                    arrayList.add(a3);
                                }
                            }
                        }
                        observableEmitter.onNext(arrayList);
                        observableEmitter.onComplete();
                    }
                });
            }
        }).observeOn(Schedulers.b());
    }

    public Observable<List<MovieEntity>> a(long j, int i) {
        return null;
    }

    public Observable<List<MovieEntity>> a(String str, int i, int i2) {
        return null;
    }

    public Observable<List<MovieEntity>> b(int i, final int i2, int i3) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            public void subscribe(final ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                TraktRepositoryImpl.this.b.popular(Integer.valueOf(i2), 20, Extended.FULL).enqueue(new Callback<List<Show>>() {
                    public void onFailure(Call<List<Show>> call, Throwable th) {
                        observableEmitter.onComplete();
                    }

                    public void onResponse(Call<List<Show>> call, Response<List<Show>> response) {
                        ArrayList arrayList = new ArrayList();
                        if (!(response == null || response.body() == null)) {
                            for (Show a2 : response.body()) {
                                MovieEntity a3 = TraktRepositoryImpl.this.a(a2);
                                if (a3 != null) {
                                    arrayList.add(a3);
                                }
                            }
                        }
                        observableEmitter.onNext(arrayList);
                        observableEmitter.onComplete();
                    }
                });
            }
        }).observeOn(Schedulers.b());
    }

    public Observable<List<MovieEntity>> b(long j, int i) {
        return null;
    }

    public Observable<List<MovieEntity>> b(String str, int i, int i2) {
        return null;
    }

    public Observable<List<SeasonEntity>> discoverSession(long j) {
        return null;
    }

    public Observable<Video.Response> videos(long j) {
        return null;
    }

    public Observable<List<MovieEntity>> a(final int i) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            public void subscribe(final ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                TraktRepositoryImpl.this.b.trending(Integer.valueOf(i), (Integer) null, Extended.FULL).enqueue(new Callback<List<TrendingShow>>() {
                    public void onFailure(Call<List<TrendingShow>> call, Throwable th) {
                    }

                    public void onResponse(Call<List<TrendingShow>> call, Response<List<TrendingShow>> response) {
                        ArrayList arrayList = new ArrayList();
                        if (!(response == null || response.body() == null)) {
                            for (TrendingShow trendingShow : response.body()) {
                                MovieEntity a2 = TraktRepositoryImpl.this.a(trendingShow.show);
                                if (a2 != null) {
                                    arrayList.add(a2);
                                }
                            }
                        }
                        observableEmitter.onNext(arrayList);
                        observableEmitter.onComplete();
                    }
                });
            }
        }).observeOn(Schedulers.b());
    }

    public Observable<List<MovieEntity>> a(final String str, final int i) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                Response<List<SearchResult>> execute = TraktRepositoryImpl.this.f5038a.search().textQueryMovie(str, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Extended) null, Integer.valueOf(i), 20).execute();
                if (execute.isSuccessful()) {
                    ArrayList arrayList = new ArrayList();
                    if (!(execute == null || execute.body() == null)) {
                        for (SearchResult searchResult : execute.body()) {
                            MovieEntity a2 = TraktRepositoryImpl.this.a(searchResult.movie);
                            if (a2 != null) {
                                arrayList.add(a2);
                            }
                        }
                    }
                    observableEmitter.onNext(arrayList);
                    observableEmitter.onComplete();
                }
            }
        }).subscribeOn(Schedulers.b());
    }

    public Observable<List<MovieEntity>> a(final String str, final String str2, final int i) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                ObservableEmitter<List<MovieEntity>> observableEmitter2 = observableEmitter;
                Response<List<SearchResult>> execute = TraktRepositoryImpl.this.f5038a.search().textQueryMovie(str, str2.isEmpty() ? null : str2, (String) null, "en", (String) null, (String) null, (String) null, (String) null, (Extended) null, Integer.valueOf(i), 20).execute();
                if (execute.isSuccessful()) {
                    ArrayList arrayList = new ArrayList();
                    if (!(execute == null || execute.body() == null)) {
                        for (SearchResult searchResult : execute.body()) {
                            MovieEntity a2 = TraktRepositoryImpl.this.a(searchResult.movie);
                            if (a2 != null) {
                                arrayList.add(a2);
                            }
                        }
                    }
                    observableEmitter2.onNext(arrayList);
                }
                Response<List<SearchResult>> execute2 = TraktRepositoryImpl.this.f5038a.search().textQueryShow(str, str2, (String) null, "en", (String) null, (String) null, (String) null, (String) null, (String) null, (String) null, (Extended) null, Integer.valueOf(i), 20).execute();
                if (execute2.isSuccessful()) {
                    ArrayList arrayList2 = new ArrayList();
                    if (!(execute == null || execute2.body() == null)) {
                        for (SearchResult searchResult2 : execute2.body()) {
                            MovieEntity a3 = TraktRepositoryImpl.this.a(searchResult2.show);
                            if (a3 != null) {
                                arrayList2.add(a3);
                            }
                        }
                    }
                    observableEmitter2.onNext(arrayList2);
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b());
    }

    public MovieEntity a(Show show) {
        if (show == null || show.ids == null) {
            return null;
        }
        MovieEntity movieEntity = new MovieEntity();
        Integer num = show.ids.tmdb;
        long j = 0;
        movieEntity.setTmdbID(num != null ? (long) num.intValue() : 0);
        movieEntity.setImdbIDStr(show.ids.imdb);
        movieEntity.setTraktID((long) show.ids.trakt.intValue());
        Integer num2 = show.ids.tvdb;
        if (num2 != null) {
            j = (long) num2.intValue();
        }
        movieEntity.setTvdbID(j);
        movieEntity.setGenres(show.genres);
        movieEntity.setName(show.title);
        movieEntity.setOverview(show.overview);
        OffsetDateTime offsetDateTime = show.first_aired;
        movieEntity.setRealeaseDate(offsetDateTime != null ? DateTimeHelper.b(new DateTime(offsetDateTime.toInstant().toEpochMilli())) : "1970-1-1");
        movieEntity.setTV(true);
        movieEntity.setVote(Double.valueOf(show.rating.doubleValue()));
        return movieEntity;
    }

    public MovieEntity a(Movie movie) {
        String str;
        if (movie == null || movie.ids == null) {
            return null;
        }
        MovieEntity movieEntity = new MovieEntity();
        Integer num = movie.ids.tmdb;
        long j = 0;
        movieEntity.setTmdbID(num != null ? (long) num.intValue() : 0);
        movieEntity.setImdbIDStr(movie.ids.imdb);
        Integer num2 = movie.ids.trakt;
        if (num2 != null) {
            j = (long) num2.intValue();
        }
        movieEntity.setTraktID(j);
        movieEntity.setGenres(movie.genres);
        movieEntity.setName(movie.title);
        movieEntity.setOverview(movie.overview);
        LocalDate localDate = movie.released;
        if (localDate != null) {
            str = localDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        } else {
            str = String.valueOf(movie.year) + "-1-1";
        }
        movieEntity.setRealeaseDate(str);
        movieEntity.setTV(false);
        Double d = movie.rating;
        movieEntity.setVote(Double.valueOf(d != null ? d.doubleValue() : 0.0d));
        return movieEntity;
    }
}
