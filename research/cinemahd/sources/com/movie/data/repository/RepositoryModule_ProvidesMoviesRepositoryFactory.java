package com.movie.data.repository;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.tmdb.TMDBApi;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class RepositoryModule_ProvidesMoviesRepositoryFactory implements Factory<MoviesRepository> {
    public static MoviesRepository a(RepositoryModule repositoryModule, MoviesApi moviesApi, TMDBApi tMDBApi, MvDatabase mvDatabase) {
        MoviesRepository a2 = repositoryModule.a(moviesApi, tMDBApi, mvDatabase);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
