package com.movie.data;

import android.content.Context;
import com.bumptech.glide.Glide;
import com.bumptech.glide.GlideBuilder;
import com.bumptech.glide.Registry;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.Options;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelLoader;
import com.bumptech.glide.load.model.ModelLoaderFactory;
import com.bumptech.glide.load.model.MultiModelLoaderFactory;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;
import com.bumptech.glide.module.GlideModule;
import com.movie.FreeMoviesApp;
import com.utils.ImageUtils;
import java.io.InputStream;
import javax.inject.Inject;
import okhttp3.OkHttpClient;

public final class GlideSetup implements GlideModule {
    @Inject

    /* renamed from: a  reason: collision with root package name */
    OkHttpClient f5012a;

    private static class ImageLoader extends BaseGlideUrlLoader<String> {

        public static class Factory implements ModelLoaderFactory<String, InputStream> {
            public ModelLoader<String, InputStream> a(MultiModelLoaderFactory multiModelLoaderFactory) {
                return new ImageLoader(multiModelLoaderFactory.a(GlideUrl.class, InputStream.class));
            }

            public void a() {
            }
        }

        ImageLoader(ModelLoader<GlideUrl, InputStream> modelLoader) {
            super(modelLoader);
        }

        public boolean a(String str) {
            return true;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public String d(String str, int i, int i2, Options options) {
            return ImageUtils.a(str, i);
        }
    }

    public void a(Context context, Glide glide, Registry registry) {
        DaggerGlideSetupComponent.a().a(FreeMoviesApp.a(context).d()).a().a(this);
        registry.b(String.class, InputStream.class, new ImageLoader.Factory());
        registry.b(GlideUrl.class, InputStream.class, new OkHttpUrlLoader.Factory(this.f5012a));
    }

    public void a(Context context, GlideBuilder glideBuilder) {
    }
}
