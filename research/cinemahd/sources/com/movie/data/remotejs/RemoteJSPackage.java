package com.movie.data.remotejs;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.movie.data.remotejs.RemoteJSModule;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RemoteJSPackage implements ReactPackage, RemoteJSModule.ReactListener {

    /* renamed from: a  reason: collision with root package name */
    private RemoteJSModule f5024a;
    RemoteJSModule.ReactListener b;

    public void a(String str, String str2) {
        RemoteJSModule.ReactListener reactListener = this.b;
        if (reactListener != null) {
            reactListener.a(str, str2);
        }
    }

    public List<NativeModule> createNativeModules(ReactApplicationContext reactApplicationContext) {
        ArrayList arrayList = new ArrayList();
        this.f5024a = new RemoteJSModule(reactApplicationContext);
        this.f5024a.setReactListener(this);
        arrayList.add(this.f5024a);
        return arrayList;
    }

    public List<ViewManager> createViewManagers(ReactApplicationContext reactApplicationContext) {
        return Collections.emptyList();
    }

    public void onError(String str) {
        RemoteJSModule.ReactListener reactListener = this.b;
        if (reactListener != null) {
            reactListener.onError(str);
        }
    }

    public void a(String str) {
        RemoteJSModule.ReactListener reactListener = this.b;
        if (reactListener != null) {
            reactListener.a(str);
        }
    }

    public void a(RemoteJSModule.ReactListener reactListener) {
        this.b = reactListener;
    }
}
