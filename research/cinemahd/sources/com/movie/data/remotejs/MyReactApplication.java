package com.movie.data.remotejs;

import android.app.Application;
import com.facebook.react.ReactApplication;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import java.util.Arrays;
import java.util.List;

public class MyReactApplication extends Application implements ReactApplication {

    /* renamed from: a  reason: collision with root package name */
    private final ReactNativeHost f5022a = new ReactNativeHost(this) {
        /* access modifiers changed from: protected */
        public String getJSMainModuleName() {
            return "index";
        }

        /* access modifiers changed from: protected */
        public List<ReactPackage> getPackages() {
            MyReactApplication.this.b = new RemoteJSPackage();
            return Arrays.asList(new ReactPackage[]{new MainReactPackage(), MyReactApplication.this.b});
        }

        public boolean getUseDeveloperSupport() {
            return false;
        }
    };
    RemoteJSPackage b;

    public RemoteJSPackage a() {
        return this.b;
    }

    public ReactNativeHost getReactNativeHost() {
        return this.f5022a;
    }
}
