package com.movie.data.model;

public class CalendarItem {
    public String airTime;
    public String backdrop;
    public int episode;
    public String episodeName;
    public String imdbID;
    public boolean isNotTmdb = false;
    public String poster;
    public int season;
    public String showName;
    public long tmdbID;
    public long traktID;
    public long tvdnID;
}
