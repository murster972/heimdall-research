package com.movie.data.model.realdebrid;

public class AddMagnetResponse {
    private String id;
    private String uri;

    public String getId() {
        return this.id;
    }

    public String getUri() {
        return this.uri;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setUri(String str) {
        this.uri = str;
    }
}
