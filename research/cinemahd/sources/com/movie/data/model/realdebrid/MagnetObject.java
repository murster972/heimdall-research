package com.movie.data.model.realdebrid;

import android.os.Parcel;
import android.os.Parcelable;

public class MagnetObject implements Parcelable {
    public static final Parcelable.Creator<MagnetObject> CREATOR = new Parcelable.Creator<MagnetObject>() {
        public MagnetObject createFromParcel(Parcel parcel) {
            return new MagnetObject(parcel);
        }

        public MagnetObject[] newArray(int i) {
            return new MagnetObject[i];
        }
    };
    private String fileName;
    private String fileSize;
    private String hostName;
    private String magnet;
    private String provider;
    private String quality;

    public MagnetObject(String str, String str2, String str3, String str4) {
        this.hostName = str;
        this.magnet = str2;
        this.quality = str3;
        this.provider = str4;
    }

    public int describeContents() {
        return 0;
    }

    public String getFileName() {
        return this.fileName;
    }

    public String getFileSize() {
        return this.fileSize;
    }

    public String getHostName() {
        return this.hostName;
    }

    public String getMagnet() {
        return this.magnet;
    }

    public String getProvider() {
        return this.provider;
    }

    public String getQuality() {
        return this.quality;
    }

    public void setFileName(String str) {
        this.fileName = str;
    }

    public void setFileSize(String str) {
        this.fileSize = str;
    }

    public void setHostName(String str) {
        this.hostName = str;
    }

    public void setMagnet(String str) {
        this.magnet = str;
    }

    public void setProvider(String str) {
        this.provider = str;
    }

    public void setQuality(String str) {
        this.quality = str;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.hostName);
        parcel.writeString(this.magnet);
        parcel.writeString(this.quality);
        parcel.writeString(this.provider);
        parcel.writeString(this.fileName);
    }

    protected MagnetObject(Parcel parcel) {
        this.hostName = parcel.readString();
        this.magnet = parcel.readString();
        this.quality = parcel.readString();
        this.provider = parcel.readString();
        this.fileName = parcel.readString();
    }
}
