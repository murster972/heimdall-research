package com.movie.data.model;

import android.util.SparseArray;

public class Categorys {
    private static SparseArray<String> mvlist;
    private static SparseArray<String> tvlist;

    public static boolean canFilter(int i, boolean z) {
        return z ? (i == -6 || i == -7 || i == -8 || i == -9 || i == -10) ? false : true : (i == -6 || i == -7 || i == -8) ? false : true;
    }

    public static SparseArray<String> getMVCategory() {
        if (mvlist == null) {
            mvlist = new SparseArray<>();
            mvlist.put(-6, "Top Rated");
            mvlist.put(-7, "Upcoming");
            mvlist.put(-8, "Now Playing");
            mvlist.put(-9, "Popular");
            mvlist.put(-1, "New HD Release");
            mvlist.put(-2, "DVD Release");
            mvlist.put(28, "Action");
            mvlist.put(12, "Adventure");
            mvlist.put(16, "Animation");
            mvlist.put(35, "Comedy");
            mvlist.put(80, "Crime");
            mvlist.put(99, "Documentary");
            mvlist.put(18, "Drama");
            mvlist.put(10751, "Family");
            mvlist.put(14, "Fantasy");
            mvlist.put(36, "History");
            mvlist.put(27, "Horror");
            mvlist.put(10402, "Music");
            mvlist.put(9648, "Mystery");
            mvlist.put(10749, "Romance");
            mvlist.put(878, "Science Fiction");
            mvlist.put(10770, "TV Movie");
            mvlist.put(53, "Thriller");
            mvlist.put(10752, "War");
            mvlist.put(37, "Western");
        }
        return mvlist;
    }

    public static SparseArray<String> getMvExtraList() {
        SparseArray<String> sparseArray = new SparseArray<>();
        sparseArray.put(-6, "top_rated");
        sparseArray.put(-7, "upcoming");
        sparseArray.put(-8, "now_playing");
        sparseArray.put(-9, "popular");
        return sparseArray;
    }

    public static SparseArray<String> getTVCategory() {
        if (tvlist == null) {
            tvlist = new SparseArray<>();
            tvlist.put(93408, "Netflix™ List");
            tvlist.put(93405, "Amazon™ List");
            tvlist.put(93403, "Hulu™ List");
            tvlist.put(93401, "YouTube™ Red");
            tvlist.put(-2, "DVD Release");
            tvlist.put(-6, "On The Air");
            tvlist.put(-7, "Airing Today");
            tvlist.put(-9, "Top Rated");
            tvlist.put(-11, "Popular");
            tvlist.put(-10, "Trending");
            tvlist.put(10759, "Action & Adventure");
            tvlist.put(16, "Animation");
            tvlist.put(35, "Comedy");
            tvlist.put(80, "Crime");
            tvlist.put(99, "Documentary");
            tvlist.put(18, "Drama");
            tvlist.put(10751, "Family");
            tvlist.put(10762, "Kids");
            tvlist.put(9648, "Mystery");
            tvlist.put(10763, "News");
            tvlist.put(10764, "Reality");
            tvlist.put(10765, "Sci-Fi & Fantasy");
            tvlist.put(10766, "Soap");
            tvlist.put(10767, "Talk");
            tvlist.put(10768, "War & Politics");
            tvlist.put(37, "Western");
        }
        return tvlist;
    }

    public static SparseArray<String> getTvExtraList() {
        SparseArray<String> sparseArray = new SparseArray<>();
        sparseArray.put(-6, "on_the_air");
        sparseArray.put(-7, "airing_today");
        sparseArray.put(-8, "latest");
        sparseArray.put(-9, "top_rated");
        sparseArray.put(-10, "popular");
        return sparseArray;
    }
}
