package com.movie.data.model.providers;

public class RemoteJSModel {
    private ProviderBean provider;
    private ResultBean result;

    public static class ProviderBean {
        private String name;
        private String url;

        public String getName() {
            return this.name;
        }

        public String getUrl() {
            return this.url;
        }

        public void setName(String str) {
            this.name = str;
        }

        public void setUrl(String str) {
            this.url = str;
        }
    }

    public static class ResultBean {
        private String file;
        private String label;
        private String type;

        public String getFile() {
            return this.file;
        }

        public String getLabel() {
            return this.label;
        }

        public String getType() {
            return this.type;
        }

        public void setFile(String str) {
            this.file = str;
        }

        public void setLabel(String str) {
            this.label = str;
        }

        public void setType(String str) {
            this.type = str;
        }
    }

    public ProviderBean getProvider() {
        return this.provider;
    }

    public ResultBean getResult() {
        return this.result;
    }

    public void setProvider(ProviderBean providerBean) {
        this.provider = providerBean;
    }

    public void setResult(ResultBean resultBean) {
        this.result = resultBean;
    }
}
