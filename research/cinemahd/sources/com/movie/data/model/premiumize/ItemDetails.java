package com.movie.data.model.premiumize;

import java.util.List;

public class ItemDetails {
    private String acodec;
    private List<String> audio_track_names;
    private double bitrate;
    private int created_at;
    private String duration;
    private String folder_id;
    private String id;
    private String link;
    private String mime_type;
    private String name;
    private String opensubtitles_hash;
    private String resx;
    private String resy;
    private long size;
    private String stream_link;
    private String transcode_status;
    private String type;
    private String vcodec;
    private String virus_scan;

    public String getAcodec() {
        return this.acodec;
    }

    public List<String> getAudio_track_names() {
        return this.audio_track_names;
    }

    public double getBitrate() {
        return this.bitrate;
    }

    public int getCreated_at() {
        return this.created_at;
    }

    public String getDuration() {
        return this.duration;
    }

    public String getFolder_id() {
        return this.folder_id;
    }

    public String getId() {
        return this.id;
    }

    public String getLink() {
        return this.link;
    }

    public String getMime_type() {
        return this.mime_type;
    }

    public String getName() {
        return this.name;
    }

    public String getOpensubtitles_hash() {
        return this.opensubtitles_hash;
    }

    public String getResx() {
        return this.resx;
    }

    public String getResy() {
        return this.resy;
    }

    public long getSize() {
        return this.size;
    }

    public String getStream_link() {
        return this.stream_link;
    }

    public String getTranscode_status() {
        return this.transcode_status;
    }

    public String getType() {
        return this.type;
    }

    public String getVcodec() {
        return this.vcodec;
    }

    public String getVirus_scan() {
        return this.virus_scan;
    }

    public void setAcodec(String str) {
        this.acodec = str;
    }

    public void setAudio_track_names(List<String> list) {
        this.audio_track_names = list;
    }

    public void setBitrate(double d) {
        this.bitrate = d;
    }

    public void setCreated_at(int i) {
        this.created_at = i;
    }

    public void setDuration(String str) {
        this.duration = str;
    }

    public void setFolder_id(String str) {
        this.folder_id = str;
    }

    public void setId(String str) {
        this.id = str;
    }

    public void setLink(String str) {
        this.link = str;
    }

    public void setMime_type(String str) {
        this.mime_type = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setOpensubtitles_hash(String str) {
        this.opensubtitles_hash = str;
    }

    public void setResx(String str) {
        this.resx = str;
    }

    public void setResy(String str) {
        this.resy = str;
    }

    public void setSize(long j) {
        this.size = j;
    }

    public void setStream_link(String str) {
        this.stream_link = str;
    }

    public void setTranscode_status(String str) {
        this.transcode_status = str;
    }

    public void setType(String str) {
        this.type = str;
    }

    public void setVcodec(String str) {
        this.vcodec = str;
    }

    public void setVirus_scan(String str) {
        this.virus_scan = str;
    }
}
