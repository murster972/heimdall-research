package com.movie.data.model.cinema;

import java.util.List;

public class SyncLink {
    public String e;
    public String i;
    public List<Link> linkList;
    public String s;
    public String v;

    public static class Link {
        public String d;
        public String h;
        public String l;
        public String p;
        public String q;
        public String t;
        public String z;
    }
}
