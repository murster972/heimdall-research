package com.movie.data.model.cinema;

public class HDListResponse {
    int linkCount;
    long tmdbID;

    public long getTmdbID() {
        return this.tmdbID;
    }

    public void setTmdbID(long j) {
        this.tmdbID = j;
    }
}
