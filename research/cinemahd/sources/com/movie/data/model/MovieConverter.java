package com.movie.data.model;

import com.database.entitys.MovieEntity;

public interface MovieConverter {
    MovieEntity convert();
}
