package com.movie.data;

import com.movie.AppComponent;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

public final class DaggerGlideSetupComponent implements GlideSetupComponent {

    /* renamed from: a  reason: collision with root package name */
    private AppComponent f5010a;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public AppComponent f5011a;

        private Builder() {
        }

        public GlideSetupComponent a() {
            if (this.f5011a != null) {
                return new DaggerGlideSetupComponent(this);
            }
            throw new IllegalStateException(AppComponent.class.getCanonicalName() + " must be set");
        }

        public Builder a(AppComponent appComponent) {
            Preconditions.a(appComponent);
            this.f5011a = appComponent;
            return this;
        }
    }

    public static Builder a() {
        return new Builder();
    }

    private GlideSetup b(GlideSetup glideSetup) {
        OkHttpClient i = this.f5010a.i();
        Preconditions.a(i, "Cannot return null from a non-@Nullable component method");
        GlideSetup_MembersInjector.a(glideSetup, i);
        return glideSetup;
    }

    private DaggerGlideSetupComponent(Builder builder) {
        a(builder);
    }

    private void a(Builder builder) {
        this.f5010a = builder.f5011a;
    }

    public void a(GlideSetup glideSetup) {
        b(glideSetup);
    }
}
