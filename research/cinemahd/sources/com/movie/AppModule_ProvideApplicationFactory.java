package com.movie;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class AppModule_ProvideApplicationFactory implements Factory<Application> {

    /* renamed from: a  reason: collision with root package name */
    private final AppModule f5003a;

    public AppModule_ProvideApplicationFactory(AppModule appModule) {
        this.f5003a = appModule;
    }

    public static AppModule_ProvideApplicationFactory a(AppModule appModule) {
        return new AppModule_ProvideApplicationFactory(appModule);
    }

    public static Application b(AppModule appModule) {
        return c(appModule);
    }

    public static Application c(AppModule appModule) {
        Application a2 = appModule.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    public Application get() {
        return b(this.f5003a);
    }
}
