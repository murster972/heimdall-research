package com.movie;

import android.app.Application;
import com.database.DatabaseModule;
import com.database.DatabaseModule_ProviderDemoDatabaseFactory;
import com.database.MvDatabase;
import com.google.gson.Gson;
import com.movie.data.api.ApiModule;
import com.movie.data.api.ApiModule_ProvideGsonFactory;
import com.movie.data.api.ApiModule_ProvideMoviesApiFactory;
import com.movie.data.api.ApiModule_ProvideOkHttpClientFactory;
import com.movie.data.api.ApiModule_ProvideRestAdapterFactory;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.imdb.IMDBModule;
import com.movie.data.api.imdb.IMDBModule_ProvideGsonFactory;
import com.movie.data.api.imdb.IMDBModule_ProvideIMDBApiFactory;
import com.movie.data.api.imdb.IMDBModule_ProvideOkHttpClientFactory;
import com.movie.data.api.imdb.IMDBModule_ProvideRestAdapterFactory;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.api.realdebrid.RealDebridModule;
import com.movie.data.api.realdebrid.RealDebridModule_ProvideGsonFactory;
import com.movie.data.api.realdebrid.RealDebridModule_ProvideOkHttpClientFactory;
import com.movie.data.api.realdebrid.RealDebridModule_ProvideRealDebridApiFactory;
import com.movie.data.api.realdebrid.RealDebridModule_ProvideRestAdapterFactory;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.api.tmdb.TMDBModule;
import com.movie.data.api.tmdb.TMDBModule_ProvideGsonFactory;
import com.movie.data.api.tmdb.TMDBModule_ProvideOkHttpClientFactory;
import com.movie.data.api.tmdb.TMDBModule_ProvideRestAdapterFactory;
import com.movie.data.api.tmdb.TMDBModule_ProvideTMDBApiFactory;
import com.movie.data.api.tvdb.TvdbModule;
import com.movie.data.api.tvdb.TvdbModule_ProvideTheTvdbFactory;
import com.movie.data.api.tvmaze.TVMazeApi;
import com.movie.data.api.tvmaze.TVMazeModule;
import com.movie.data.api.tvmaze.TVMazeModule_ProvideGsonFactory;
import com.movie.data.api.tvmaze.TVMazeModule_ProvideOkHttpClientFactory;
import com.movie.data.api.tvmaze.TVMazeModule_ProvideRestAdapterFactory;
import com.movie.data.api.tvmaze.TVMazeModule_ProvideTVMazeApiFactory;
import com.movie.data.repository.MoviesRepository;
import com.movie.data.repository.RepositoryModule;
import com.movie.data.repository.RepositoryModule_ProvidesMoviesRepositoryFactory;
import com.movie.ui.helper.MoviesHelper;
import com.movie.ui.helper.MoviesHelper_ProviderMoviesHelperFactory;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.internal.DoubleCheck;
import dagger.internal.Preconditions;
import javax.inject.Provider;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;

public final class DaggerAppComponent implements AppComponent {

    /* renamed from: a  reason: collision with root package name */
    private RepositoryModule f5004a;
    private ApiModule b;
    private TMDBModule c;
    private TVMazeModule d;
    private RealDebridModule e;
    private MoviesHelper f;
    private IMDBModule g;
    private TvdbModule h;
    private Provider<Application> i;
    private Provider<MvDatabase> j;
    private Provider<OkHttpClient> k;
    private RealDebridModule_ProvideGsonFactory l;
    private Provider<Retrofit> m;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public AppModule f5005a;
        /* access modifiers changed from: private */
        public ApiModule b;
        /* access modifiers changed from: private */
        public TMDBModule c;
        /* access modifiers changed from: private */
        public DatabaseModule d;
        /* access modifiers changed from: private */
        public RepositoryModule e;
        /* access modifiers changed from: private */
        public TVMazeModule f;
        /* access modifiers changed from: private */
        public RealDebridModule g;
        /* access modifiers changed from: private */
        public MoviesHelper h;
        /* access modifiers changed from: private */
        public IMDBModule i;
        /* access modifiers changed from: private */
        public TvdbModule j;

        private Builder() {
        }

        public AppComponent a() {
            if (this.f5005a != null) {
                if (this.b == null) {
                    this.b = new ApiModule();
                }
                if (this.c == null) {
                    this.c = new TMDBModule();
                }
                if (this.d == null) {
                    this.d = new DatabaseModule();
                }
                if (this.e == null) {
                    this.e = new RepositoryModule();
                }
                if (this.f == null) {
                    this.f = new TVMazeModule();
                }
                if (this.g == null) {
                    this.g = new RealDebridModule();
                }
                if (this.h == null) {
                    this.h = new MoviesHelper();
                }
                if (this.i == null) {
                    this.i = new IMDBModule();
                }
                if (this.j == null) {
                    this.j = new TvdbModule();
                }
                return new DaggerAppComponent(this);
            }
            throw new IllegalStateException(AppModule.class.getCanonicalName() + " must be set");
        }

        public Builder a(AppModule appModule) {
            Preconditions.a(appModule);
            this.f5005a = appModule;
            return this;
        }
    }

    private void a(Builder builder) {
        this.f5004a = builder.e;
        this.i = DoubleCheck.a(AppModule_ProvideApplicationFactory.a(builder.f5005a));
        this.j = DoubleCheck.a(DatabaseModule_ProviderDemoDatabaseFactory.a(builder.d, this.i));
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.f;
        this.e = builder.g;
        this.k = DoubleCheck.a(RealDebridModule_ProvideOkHttpClientFactory.a(builder.g, this.i));
        this.l = RealDebridModule_ProvideGsonFactory.a(builder.g);
        this.m = DoubleCheck.a(RealDebridModule_ProvideRestAdapterFactory.a(builder.g, this.k, (Provider<Gson>) this.l));
        this.f = builder.h;
        this.g = builder.i;
        this.h = builder.j;
    }

    public static Builder m() {
        return new Builder();
    }

    private OkHttpClient n() {
        return TMDBModule_ProvideOkHttpClientFactory.a(this.c, this.i.get());
    }

    private OkHttpClient o() {
        return TVMazeModule_ProvideOkHttpClientFactory.a(this.d, this.i.get());
    }

    private OkHttpClient p() {
        return IMDBModule_ProvideOkHttpClientFactory.a(this.g, this.i.get());
    }

    private Retrofit q() {
        return TMDBModule_ProvideRestAdapterFactory.a(this.c, n(), TMDBModule_ProvideGsonFactory.a(this.c));
    }

    private Retrofit r() {
        return TVMazeModule_ProvideRestAdapterFactory.a(this.d, o(), TVMazeModule_ProvideGsonFactory.a(this.d));
    }

    private Retrofit s() {
        return IMDBModule_ProvideRestAdapterFactory.a(this.g, p(), IMDBModule_ProvideGsonFactory.a(this.g));
    }

    public RealDebridApi b() {
        return RealDebridModule_ProvideRealDebridApiFactory.a(this.e, this.m.get());
    }

    public MoviesHelper c() {
        return MoviesHelper_ProviderMoviesHelperFactory.a(this.f, f(), this.j.get());
    }

    public MoviesApi d() {
        return ApiModule_ProvideMoviesApiFactory.a(this.b, l());
    }

    public TMDBApi e() {
        return TMDBModule_ProvideTMDBApiFactory.a(this.c, q());
    }

    public MoviesRepository f() {
        return RepositoryModule_ProvidesMoviesRepositoryFactory.a(this.f5004a, d(), e(), this.j.get());
    }

    public TheTvdb g() {
        return TvdbModule_ProvideTheTvdbFactory.a(this.h);
    }

    public IMDBApi h() {
        return IMDBModule_ProvideIMDBApiFactory.a(this.g, s());
    }

    public OkHttpClient i() {
        return ApiModule_ProvideOkHttpClientFactory.a(this.b, this.i.get());
    }

    public TVMazeApi j() {
        return TVMazeModule_ProvideTVMazeApiFactory.a(this.d, r());
    }

    public OkHttpClient k() {
        return this.k.get();
    }

    public Retrofit l() {
        return ApiModule_ProvideRestAdapterFactory.a(this.b, i(), ApiModule_ProvideGsonFactory.a(this.b));
    }

    private DaggerAppComponent(Builder builder) {
        a(builder);
    }

    private FreeMoviesApp b(FreeMoviesApp freeMoviesApp) {
        FreeMoviesApp_MembersInjector.a(freeMoviesApp, d());
        return freeMoviesApp;
    }

    public void a(FreeMoviesApp freeMoviesApp) {
        b(freeMoviesApp);
    }

    public MvDatabase a() {
        return this.j.get();
    }
}
