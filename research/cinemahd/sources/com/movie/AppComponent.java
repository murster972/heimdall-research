package com.movie;

import com.database.DatabaseModule;
import com.database.MvDatabase;
import com.movie.data.api.ApiModule;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.imdb.IMDBModule;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.api.realdebrid.RealDebridModule;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.api.tmdb.TMDBModule;
import com.movie.data.api.tvdb.TvdbModule;
import com.movie.data.api.tvmaze.TVMazeApi;
import com.movie.data.api.tvmaze.TVMazeModule;
import com.movie.data.repository.MoviesRepository;
import com.movie.data.repository.RepositoryModule;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.Component;
import javax.inject.Named;
import javax.inject.Singleton;
import okhttp3.OkHttpClient;

@Singleton
@Component(modules = {AppModule.class, TMDBModule.class, TVMazeModule.class, RepositoryModule.class, ApiModule.class, DatabaseModule.class, RealDebridModule.class, MoviesHelper.class, IMDBModule.class, TvdbModule.class})
public interface AppComponent {
    MvDatabase a();

    void a(FreeMoviesApp freeMoviesApp);

    RealDebridApi b();

    MoviesHelper c();

    MoviesApi d();

    TMDBApi e();

    MoviesRepository f();

    TheTvdb g();

    IMDBApi h();

    OkHttpClient i();

    TVMazeApi j();

    @Named("RealDebrid")
    OkHttpClient k();
}
