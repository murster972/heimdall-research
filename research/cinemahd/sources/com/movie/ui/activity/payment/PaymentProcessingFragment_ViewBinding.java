package com.movie.ui.activity.payment;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public class PaymentProcessingFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PaymentProcessingFragment f5310a;
    private View b;
    private View c;
    private View d;

    public PaymentProcessingFragment_ViewBinding(final PaymentProcessingFragment paymentProcessingFragment, View view) {
        this.f5310a = paymentProcessingFragment;
        paymentProcessingFragment.tvaddress = (TextView) Utils.findRequiredViewAsType(view, R.id.tvaddress, "field 'tvaddress'", TextView.class);
        paymentProcessingFragment.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTitlte, "field 'tvTitle'", TextView.class);
        paymentProcessingFragment.imgAddressQR = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgAddressQR, "field 'imgAddressQR'", ImageView.class);
        paymentProcessingFragment.progressBarCircle = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.progressBarCircle, "field 'progressBarCircle'", ProgressBar.class);
        paymentProcessingFragment.textViewTime = (TextView) Utils.findRequiredViewAsType(view, R.id.textViewTime, "field 'textViewTime'", TextView.class);
        paymentProcessingFragment.tvBTC = (TextView) Utils.findRequiredViewAsType(view, R.id.tvBTC, "field 'tvBTC'", TextView.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.btnCancelPayment, "field 'btnCancelPayment' and method 'onBtnCancelPaymentClick'");
        paymentProcessingFragment.btnCancelPayment = (Button) Utils.castView(findRequiredView, R.id.btnCancelPayment, "field 'btnCancelPayment'", Button.class);
        this.b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                paymentProcessingFragment.onBtnCancelPaymentClick();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.btnCopy, "field 'btnCopyAddress' and method 'onBtnCopyClick'");
        paymentProcessingFragment.btnCopyAddress = (Button) Utils.castView(findRequiredView2, R.id.btnCopy, "field 'btnCopyAddress'", Button.class);
        this.c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                paymentProcessingFragment.onBtnCopyClick();
            }
        });
        paymentProcessingFragment.progressbar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.progressbar, "field 'progressbar'", ProgressBar.class);
        paymentProcessingFragment.layout_address_content = (ConstraintLayout) Utils.findRequiredViewAsType(view, R.id.layout_address_content, "field 'layout_address_content'", ConstraintLayout.class);
        View findRequiredView3 = Utils.findRequiredView(view, R.id.btnCopyBTC, "method 'onBtnCopyBTCClick' and method 'onBtnCopyBTCLongClick'");
        this.d = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                paymentProcessingFragment.onBtnCopyBTCClick();
            }
        });
        findRequiredView3.setOnLongClickListener(new View.OnLongClickListener(this) {
            public boolean onLongClick(View view) {
                paymentProcessingFragment.onBtnCopyBTCLongClick();
                return true;
            }
        });
    }

    public void unbind() {
        PaymentProcessingFragment paymentProcessingFragment = this.f5310a;
        if (paymentProcessingFragment != null) {
            this.f5310a = null;
            paymentProcessingFragment.tvaddress = null;
            paymentProcessingFragment.tvTitle = null;
            paymentProcessingFragment.imgAddressQR = null;
            paymentProcessingFragment.progressBarCircle = null;
            paymentProcessingFragment.textViewTime = null;
            paymentProcessingFragment.tvBTC = null;
            paymentProcessingFragment.btnCancelPayment = null;
            paymentProcessingFragment.btnCopyAddress = null;
            paymentProcessingFragment.progressbar = null;
            paymentProcessingFragment.layout_address_content = null;
            this.b.setOnClickListener((View.OnClickListener) null);
            this.b = null;
            this.c.setOnClickListener((View.OnClickListener) null);
            this.c = null;
            this.d.setOnClickListener((View.OnClickListener) null);
            this.d.setOnLongClickListener((View.OnLongClickListener) null);
            this.d = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
