package com.movie.ui.activity.payment;

import androidx.appcompat.app.AlertDialog;
import com.movie.ui.activity.payment.PaymentProcessingFragment;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;

/* compiled from: lambda */
public final /* synthetic */ class h implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentProcessingFragment.AnonymousClass2 f5324a;
    private final /* synthetic */ AlertDialog b;

    public /* synthetic */ h(PaymentProcessingFragment.AnonymousClass2 r1, AlertDialog alertDialog) {
        this.f5324a = r1;
        this.b = alertDialog;
    }

    public final void accept(Object obj) {
        this.f5324a.a(this.b, (ResponseBody) obj);
    }
}
