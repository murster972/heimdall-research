package com.movie.ui.activity.payment.keyManager;

import com.movie.data.model.cinema.KeyResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ KeyManager f5335a;

    public /* synthetic */ d(KeyManager keyManager) {
        this.f5335a = keyManager;
    }

    public final void accept(Object obj) {
        this.f5335a.a((KeyResponse) obj);
    }
}
