package com.movie.ui.activity.payment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.payment.bitcoin.ProductResponse;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;

public class ChooseProductFragment extends BaseFragment {
    @BindView(2131296421)
    Button btnNext;
    @Inject
    MoviesApi c;
    @BindView(2131296482)
    CheckBox cbSplitKey;
    CompositeDisposable d;
    private List<ProductResponse.ResultsBean> e;
    @BindView(2131297177)
    EditText edtEmail;
    /* access modifiers changed from: private */
    public ProductResponse.ResultsBean f;
    private String g;
    private ChooseProductListListener h;
    @BindView(2131296701)
    ImageButton imgbtnDetails;
    @BindView(2131296958)
    ProgressBar progressBarloading;
    @BindView(2131296965)
    RadioGroup radioGroupProducts;
    @BindView(2131297196)
    TextView tvValidate;

    public interface ChooseProductListListener {
        void a(ProductResponse.ResultsBean resultsBean, String str, Boolean bool);
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    /* access modifiers changed from: private */
    public boolean f() {
        String str = "";
        this.tvValidate.setText(str);
        this.g = this.edtEmail.getText().toString();
        if (this.g.isEmpty()) {
            str = str + "\n- Missing email";
        } else if (!a((CharSequence) this.g)) {
            str = str + "\n- Invalid email format";
        }
        List<ProductResponse.ResultsBean> list = this.e;
        if (list == null || list.size() == 0) {
            str = str + "\n- Can't load product list";
        } else if (this.f == null) {
            str = str + "\n- You must select a product";
        }
        this.tvValidate.setText(str);
        if (!str.isEmpty()) {
            this.tvValidate.setVisibility(0);
        } else {
            this.tvValidate.setVisibility(8);
        }
        if (str.isEmpty()) {
            this.btnNext.setBackgroundColor(getResources().getColor(R.color.material_green300));
        } else {
            this.btnNext.setBackgroundColor(-7829368);
        }
        return str.isEmpty();
    }

    public /* synthetic */ void e() throws Exception {
        f();
        this.progressBarloading.setVisibility(8);
    }

    @OnClick({2131296421})
    public void onBtnNextClick() {
        if (f() && this.h != null) {
            FreeMoviesApp.l().edit().putString("pref_payment_bit_mail", this.g).apply();
            FreeMoviesApp.l().edit().putString("pref_payment_bit_product_id", new Gson().a((Object) this.f)).apply();
            this.h.a(this.f, this.g, Boolean.valueOf(this.cbSplitKey.isChecked()));
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_bitcoin_choose_products, viewGroup, false);
    }

    public void onDestroy() {
        this.d.dispose();
        super.onDestroy();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        String string = FreeMoviesApp.l().getString("pref_payment_bit_mail", "");
        if (!string.isEmpty()) {
            this.edtEmail.setText(string);
        }
        this.progressBarloading.setVisibility(0);
        view.getContext();
        this.d = new CompositeDisposable();
        this.d.b(this.c.productList().subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new b(this), d.f5320a, new c(this)));
    }

    public static ChooseProductFragment a(ChooseProductListListener chooseProductListListener) {
        ChooseProductFragment chooseProductFragment = new ChooseProductFragment();
        chooseProductFragment.setArguments(new Bundle());
        chooseProductFragment.h = chooseProductListListener;
        return chooseProductFragment;
    }

    public static final boolean a(CharSequence charSequence) {
        return !TextUtils.isEmpty(charSequence) && Patterns.EMAIL_ADDRESS.matcher(charSequence).matches();
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ void a(ProductResponse productResponse) throws Exception {
        a(productResponse.getResults());
    }

    private void a(List<ProductResponse.ResultsBean> list) {
        this.e = list;
        for (ProductResponse.ResultsBean next : list) {
            final RadioButton radioButton = new RadioButton(getActivity());
            radioButton.setText(next.getPrice() + " USD / " + next.getDescription());
            radioButton.setTag(next);
            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    if (z) {
                        ProductResponse.ResultsBean unused = ChooseProductFragment.this.f = (ProductResponse.ResultsBean) radioButton.getTag();
                    }
                    boolean unused2 = ChooseProductFragment.this.f();
                }
            });
            this.radioGroupProducts.addView(radioButton, 0, new RadioGroup.LayoutParams(-2, -2));
        }
    }
}
