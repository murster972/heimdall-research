package com.movie.ui.activity.payment;

public interface IOnBackPressed {
    boolean onBackPressed();
}
