package com.movie.ui.activity.payment;

import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import com.movie.data.model.payment.bitcoin.ProductResponse;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.activity.payment.ChooseProductFragment;
import com.movie.ui.activity.payment.PaymentProcessingFragment;
import com.yoku.marumovie.R;

public class BitcoinGatewayActivity extends BaseActivity implements ChooseProductFragment.ChooseProductListListener, PaymentProcessingFragment.PaymentProcessingFragmentListener {

    /* renamed from: a  reason: collision with root package name */
    Boolean f5301a = false;
    @BindView(2131297158)
    Toolbar toolbar;

    public /* synthetic */ void a(View view) {
        for (Fragment next : getSupportFragmentManager().p()) {
            if (next instanceof IOnBackPressed) {
                ((IOnBackPressed) next).onBackPressed();
                return;
            }
        }
        finish();
    }

    public void onBackPressed() {
        for (Fragment next : getSupportFragmentManager().p()) {
            if (next instanceof IOnBackPressed) {
                ((IOnBackPressed) next).onBackPressed();
                return;
            }
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_bitcoin_gateway);
        setSupportActionBar(this.toolbar);
        this.f5301a = Boolean.valueOf(getIntent().getExtras().getBoolean("isSplitKey"));
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.e(true);
            this.toolbar.setTitle((CharSequence) "Bitcoin payment with 3 step");
            this.toolbar.setNavigationOnClickListener(new a(this));
        }
        String string = FreeMoviesApp.l().getString("pref_payment_bit_address", "");
        if (string.isEmpty()) {
            a((Fragment) ChooseProductFragment.a((ChooseProductFragment.ChooseProductListListener) this));
            return;
        }
        a((Fragment) PaymentProcessingFragment.a(this, string, (ProductResponse.ResultsBean) new Gson().a(FreeMoviesApp.l().getString("pref_payment_bit_product_id", ""), ProductResponse.ResultsBean.class), FreeMoviesApp.l().getString("pref_payment_bit_mail", ""), this.f5301a));
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }

    public void a(ProductResponse.ResultsBean resultsBean, String str, Boolean bool) {
        this.f5301a = bool;
        a((Fragment) PaymentProcessingFragment.a(this, "", resultsBean, str, bool));
    }

    private void a(Fragment fragment) {
        getSupportFragmentManager().b().b(R.id.content, fragment, "payment_process_step").a((int) R.anim.slide_in_right, (int) R.anim.slide_out_left, (int) R.anim.slide_in_left, (int) R.anim.slide_out_right).b();
    }

    public void a(BitcoinPaymentInfo bitcoinPaymentInfo) {
        a((Fragment) PaymentResultFragment.a(bitcoinPaymentInfo));
    }
}
