package com.movie.ui.activity.payment;

import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class k implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentProcessingFragment f5327a;
    private final /* synthetic */ CompositeDisposable b;

    public /* synthetic */ k(PaymentProcessingFragment paymentProcessingFragment, CompositeDisposable compositeDisposable) {
        this.f5327a = paymentProcessingFragment;
        this.b = compositeDisposable;
    }

    public final void accept(Object obj) {
        this.f5327a.a(this.b, (BitcoinPaymentInfo) obj);
    }
}
