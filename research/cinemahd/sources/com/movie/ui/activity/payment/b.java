package com.movie.ui.activity.payment;

import com.movie.data.model.payment.bitcoin.ProductResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ChooseProductFragment f5318a;

    public /* synthetic */ b(ChooseProductFragment chooseProductFragment) {
        this.f5318a = chooseProductFragment;
    }

    public final void accept(Object obj) {
        this.f5318a.a((ProductResponse) obj);
    }
}
