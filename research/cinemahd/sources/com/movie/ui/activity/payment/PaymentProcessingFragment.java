package com.movie.ui.activity.payment;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import com.applovin.sdk.AppLovinEventTypes;
import com.google.android.gms.ads.AdRequest;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.payment.bitcoin.BitcoinAddressResponse;
import com.movie.data.model.payment.bitcoin.BitcoinAdressRequest;
import com.movie.data.model.payment.bitcoin.BitcoinCancelPaymentRequest;
import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import com.movie.data.model.payment.bitcoin.ProductResponse;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.ResponseBody;

public class PaymentProcessingFragment extends BaseFragment implements IOnBackPressed {
    @BindView(2131296417)
    Button btnCancelPayment;
    @BindView(2131296418)
    Button btnCopyAddress;
    @Inject
    MoviesApi c;
    CompositeDisposable d;
    long e = 0;
    long f = 0;
    private PaymentProcessingFragmentListener g;
    private ProductResponse.ResultsBean h;
    /* access modifiers changed from: private */
    public String i;
    @BindView(2131296695)
    ImageView imgAddressQR;
    /* access modifiers changed from: private */
    public String j;
    private String k;
    private Boolean l = false;
    @BindView(2131296726)
    ConstraintLayout layout_address_content;
    private CountDownTimer m;
    /* access modifiers changed from: private */
    public long n = 60000;
    @BindView(2131296957)
    ProgressBar progressBarCircle;
    @BindView(2131296962)
    ProgressBar progressbar;
    @BindView(2131297132)
    TextView textViewTime;
    @BindView(2131297176)
    TextView tvBTC;
    @BindView(2131297195)
    TextView tvTitle;
    @BindView(2131297200)
    TextView tvaddress;

    public interface PaymentProcessingFragmentListener {
        void a(BitcoinPaymentInfo bitcoinPaymentInfo);
    }

    static /* synthetic */ void b(Throwable th) throws Exception {
    }

    /* access modifiers changed from: private */
    public void f() {
        this.progressBarCircle.setMax(((int) this.n) / 1000);
        this.progressBarCircle.setProgress(((int) this.n) / 1000);
    }

    private void g() {
        CountDownTimer countDownTimer = this.m;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
        this.m = new CountDownTimer(this.n, 1000) {
            public void onFinish() {
                PaymentProcessingFragment paymentProcessingFragment = PaymentProcessingFragment.this;
                paymentProcessingFragment.textViewTime.setText(paymentProcessingFragment.a(paymentProcessingFragment.n));
                PaymentProcessingFragment.this.f();
            }

            public void onTick(long j) {
                PaymentProcessingFragment paymentProcessingFragment = PaymentProcessingFragment.this;
                paymentProcessingFragment.textViewTime.setText(paymentProcessingFragment.a(j));
                PaymentProcessingFragment.this.progressBarCircle.setProgress((int) (j / 1000));
            }
        }.start();
        this.m.start();
    }

    private void h() {
        CountDownTimer countDownTimer = this.m;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    public void e() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.a((CharSequence) "Do you want to cancel payment process ? \n(Sometime Blockchain service is busy, receipt maybe come lately to our server. If you have already sent the BTC, please click WAIT button)");
        final AlertDialog a2 = builder.a();
        a2.setCancelable(false);
        a2.a(-1, "YES", new DialogInterface.OnClickListener() {
            public /* synthetic */ void a(AlertDialog alertDialog, ResponseBody responseBody) throws Exception {
                if (responseBody.string().contains("done")) {
                    alertDialog.dismiss();
                    PaymentProcessingFragment.this.getActivity().finish();
                    FreeMoviesApp.l().edit().putString("pref_payment_bit_address", "").apply();
                    FreeMoviesApp.l().edit().putBoolean("isSplitKey", false);
                    return;
                }
                PaymentProcessingFragment.this.c("cancel error");
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                BitcoinCancelPaymentRequest bitcoinCancelPaymentRequest = new BitcoinCancelPaymentRequest();
                bitcoinCancelPaymentRequest.setAddress(PaymentProcessingFragment.this.j);
                bitcoinCancelPaymentRequest.setEmail(PaymentProcessingFragment.this.i);
                bitcoinCancelPaymentRequest.setDeviceID(Utils.f());
                PaymentProcessingFragment paymentProcessingFragment = PaymentProcessingFragment.this;
                paymentProcessingFragment.d.b(paymentProcessingFragment.c.cancelPayment(bitcoinCancelPaymentRequest).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new h(this, a2), new g(this)));
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                PaymentProcessingFragment.this.c("cancel error");
            }
        });
        a2.a(-2, "NO", new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                a2.dismiss();
            }
        });
        a2.a(-3, "Wait", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Utils.a((Activity) PaymentProcessingFragment.this.getActivity(), "Please come back later ...");
                PaymentProcessingFragment.this.getActivity().finish();
            }
        });
        a2.show();
    }

    public boolean onBackPressed() {
        e();
        return true;
    }

    @OnClick({2131296417})
    public void onBtnCancelPaymentClick() {
        e();
    }

    @OnClick({2131296419})
    public void onBtnCopyBTCClick() {
        Utils.a((Activity) getActivity(), this.k, false);
    }

    @OnLongClick({2131296419})
    public void onBtnCopyBTCLongClick() {
        Utils.a((Activity) getActivity(), this.k, true);
    }

    @OnClick({2131296418})
    public void onBtnCopyClick() {
        Utils.a((Activity) getActivity(), this.j, false);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_bitcoin_processing, viewGroup, false);
    }

    public void onDestroy() {
        h();
        super.onDestroy();
    }

    public void onPause() {
        this.e = System.currentTimeMillis();
        this.d.dispose();
        super.onPause();
    }

    public void onResume() {
        super.onResume();
        this.f = System.currentTimeMillis();
        BitcoinAdressRequest bitcoinAdressRequest = new BitcoinAdressRequest();
        bitcoinAdressRequest.setDeviceID(Utils.f());
        bitcoinAdressRequest.setEmail(this.i);
        bitcoinAdressRequest.setDeviceName(Utils.j());
        bitcoinAdressRequest.setProductID(this.h.getId());
        bitcoinAdressRequest.setTest(false);
        bitcoinAdressRequest.setAddress(this.j);
        bitcoinAdressRequest.setSplitKey(this.l);
        this.layout_address_content.setVisibility(8);
        this.progressbar.setVisibility(0);
        this.d = new CompositeDisposable();
        this.d.b(this.c.requestAddress(bitcoinAdressRequest).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new i(this), e.f5321a));
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        Bundle arguments = getArguments();
        view.getContext();
        this.i = arguments.getString("email");
        this.h = (ProductResponse.ResultsBean) arguments.getParcelable(AppLovinEventTypes.USER_VIEWED_PRODUCT);
        this.j = arguments.getString("oldAdress");
        this.l = Boolean.valueOf(arguments.getBoolean("isSplitKey"));
    }

    public void d(String str) {
        try {
            BitMatrix a2 = new QRCodeWriter().a(str, BarcodeFormat.QR_CODE, (int) AdRequest.MAX_CONTENT_URL_LENGTH, (int) AdRequest.MAX_CONTENT_URL_LENGTH);
            int b = a2.b();
            int a3 = a2.a();
            Bitmap createBitmap = Bitmap.createBitmap(b, a3, Bitmap.Config.RGB_565);
            for (int i2 = 0; i2 < b; i2++) {
                for (int i3 = 0; i3 < a3; i3++) {
                    createBitmap.setPixel(i2, i3, a2.a(i2, i3) ? -16777216 : -1);
                }
            }
            this.imgAddressQR.setImageBitmap(createBitmap);
        } catch (WriterException e2) {
            e2.printStackTrace();
        }
    }

    public static PaymentProcessingFragment a(PaymentProcessingFragmentListener paymentProcessingFragmentListener, String str, ProductResponse.ResultsBean resultsBean, String str2, Boolean bool) {
        PaymentProcessingFragment paymentProcessingFragment = new PaymentProcessingFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppLovinEventTypes.USER_VIEWED_PRODUCT, resultsBean);
        bundle.putString("email", str2);
        bundle.putString("oldAdress", str);
        bundle.putBoolean("isSplitKey", bool.booleanValue());
        paymentProcessingFragment.setArguments(bundle);
        paymentProcessingFragment.g = paymentProcessingFragmentListener;
        return paymentProcessingFragment;
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ void a(BitcoinAddressResponse bitcoinAddressResponse) throws Exception {
        this.progressbar.setVisibility(8);
        this.layout_address_content.setVisibility(0);
        FreeMoviesApp.l().edit().putBoolean("pref_payment_bit_split_keys_mode", bitcoinAddressResponse.getSplitKey().booleanValue()).apply();
        String address = bitcoinAddressResponse.getAddress();
        if (address == null || address.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.a((CharSequence) bitcoinAddressResponse.getMessage());
            final AlertDialog a2 = builder.a();
            a2.setCancelable(false);
            a2.a(-1, "ok", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    a2.dismiss();
                    FreeMoviesApp.l().edit().putString("pref_payment_bit_address", "").apply();
                    PaymentProcessingFragment.this.getActivity().finish();
                }
            });
            a2.show();
            return;
        }
        FreeMoviesApp.l().edit().putString("pref_payment_bit_address", address).apply();
        if (bitcoinAddressResponse.getRemainingTime() != null) {
            this.n = bitcoinAddressResponse.getRemainingTime().longValue();
        } else {
            this.n -= this.f - this.e;
        }
        g();
        this.tvaddress.setText(bitcoinAddressResponse.getAddress());
        this.k = bitcoinAddressResponse.getBtc();
        Utils.a(this.tvBTC, String.format("Send exactly %s BTC to above address", new Object[]{this.k}), this.k, -16711936, 1.3f);
        this.j = bitcoinAddressResponse.getAddress();
        d(bitcoinAddressResponse.getAddress());
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        compositeDisposable.b(Observable.interval(1000, 5000, TimeUnit.MILLISECONDS).subscribeOn(Schedulers.b()).subscribe(new j(this, compositeDisposable, bitcoinAddressResponse)));
        this.d.b(compositeDisposable);
    }

    public /* synthetic */ void a(CompositeDisposable compositeDisposable, BitcoinAddressResponse bitcoinAddressResponse, Long l2) throws Exception {
        Log.d("PAYMENT", "moviesApi.paymentInfo count = " + l2);
        compositeDisposable.b(this.c.paymentInfo(bitcoinAddressResponse.getAddress(), this.i, Utils.f(), Utils.j(), this.l).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new k(this, compositeDisposable), f.f5322a));
    }

    public /* synthetic */ void a(CompositeDisposable compositeDisposable, BitcoinPaymentInfo bitcoinPaymentInfo) throws Exception {
        if (bitcoinPaymentInfo != null && bitcoinPaymentInfo.getKey() != null) {
            this.g.a(bitcoinPaymentInfo);
            compositeDisposable.dispose();
            FreeMoviesApp.l().edit().putString("pref_payment_bit_address", "").apply();
            FreeMoviesApp.l().edit().putBoolean("pref_payment_bit_split_keys_mode", false).apply();
        }
    }

    /* access modifiers changed from: private */
    public String a(long j2) {
        return String.format("%02d:%02d", new Object[]{Long.valueOf(TimeUnit.MILLISECONDS.toMinutes(j2) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(j2))), Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(j2) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(j2)))});
    }
}
