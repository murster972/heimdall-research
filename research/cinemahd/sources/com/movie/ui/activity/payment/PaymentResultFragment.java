package com.movie.ui.activity.payment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.OnClick;
import com.ads.videoreward.AdsManager;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.AppConfig;
import com.movie.data.model.cinema.KeyResponse;
import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.original.tase.helper.DateTimeHelper;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;
import org.joda.time.DateTime;

public class PaymentResultFragment extends BaseFragment {
    @BindView(2131296424)
    Button btnRestart;
    @Inject
    MoviesApi c;
    CompositeDisposable d;
    @BindView(2131296962)
    ProgressBar progressBar;
    @BindView(2131297191)
    TextView tvStatus;
    @BindView(2131297195)
    TextView tvTitlte;

    public static PaymentResultFragment a(BitcoinPaymentInfo bitcoinPaymentInfo) {
        PaymentResultFragment paymentResultFragment = new PaymentResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("bitcoinPaymentInfo", bitcoinPaymentInfo);
        paymentResultFragment.setArguments(bundle);
        return paymentResultFragment;
    }

    public /* synthetic */ ObservableSource b(BitcoinPaymentInfo bitcoinPaymentInfo, AppConfig appConfig) throws Exception {
        return this.c.getActivateInfo(bitcoinPaymentInfo.getKey(), (String) null, (String) null);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_bitcoin_payment_result, viewGroup, false);
    }

    public void onDestroy() {
        this.d.dispose();
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296424})
    public void onRestartClick() {
        Utils.c((Activity) getActivity());
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.tvTitlte.setText("Activation");
        BitcoinPaymentInfo bitcoinPaymentInfo = (BitcoinPaymentInfo) getArguments().getParcelable("bitcoinPaymentInfo");
        this.d = new CompositeDisposable();
        if (bitcoinPaymentInfo.getStatus().intValue() == 0) {
            this.d.b(this.c.activeKey(bitcoinPaymentInfo.getKey()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).map(new l(this, bitcoinPaymentInfo)).observeOn(Schedulers.b()).flatMap(new n(this, bitcoinPaymentInfo)).observeOn(AndroidSchedulers.a()).subscribe(new o(this, bitcoinPaymentInfo), new m(this)));
            return;
        }
        Boolean valueOf = Boolean.valueOf(FreeMoviesApp.l().getBoolean("pref_payment_bit_split_keys_mode", false));
        String string = FreeMoviesApp.l().getString("pref_payment_bit_mail", "");
        if (valueOf.booleanValue()) {
            TextView textView = this.tvStatus;
            textView.setText(this.tvStatus.getText() + "\nYour multiple keys will send to " + string);
        }
        this.progressBar.setVisibility(8);
        this.btnRestart.setVisibility(0);
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ AppConfig a(BitcoinPaymentInfo bitcoinPaymentInfo, AppConfig appConfig) throws Exception {
        this.tvStatus.setText("Member code is activated. You must restart app to get effect");
        if (appConfig != null && appConfig.getAds() == null) {
            AdsManager.l().a();
            GlobalVariable.c().a(new Gson().a((Object) appConfig));
            Utils.j(bitcoinPaymentInfo.getKey());
        }
        return appConfig;
    }

    public /* synthetic */ void a(BitcoinPaymentInfo bitcoinPaymentInfo, KeyResponse keyResponse) throws Exception {
        TextView textView = this.tvStatus;
        textView.setText(this.tvStatus.getText() + "\nKey code : " + bitcoinPaymentInfo.getKey());
        TextView textView2 = this.tvStatus;
        textView2.setText(this.tvStatus.getText() + "\nExpired time : " + DateTimeHelper.b(DateTime.parse(keyResponse.getStartTime()).plus(keyResponse.getTtl()).toString()));
        TextView textView3 = this.tvStatus;
        textView3.setText(this.tvStatus.getText() + "\nRemaining devices : " + (keyResponse.getLimit() - keyResponse.getCurrentNumberOfDevice()));
        this.progressBar.setVisibility(8);
        this.btnRestart.setVisibility(0);
        Utils.j(bitcoinPaymentInfo.getKey());
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        TextView textView = this.tvStatus;
        textView.setText(this.tvStatus.getText() + "\n\nError : " + th.getMessage());
        this.progressBar.setVisibility(8);
    }
}
