package com.movie.ui.activity.payment;

import com.movie.data.model.payment.bitcoin.BitcoinAddressResponse;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class j implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentProcessingFragment f5326a;
    private final /* synthetic */ CompositeDisposable b;
    private final /* synthetic */ BitcoinAddressResponse c;

    public /* synthetic */ j(PaymentProcessingFragment paymentProcessingFragment, CompositeDisposable compositeDisposable, BitcoinAddressResponse bitcoinAddressResponse) {
        this.f5326a = paymentProcessingFragment;
        this.b = compositeDisposable;
        this.c = bitcoinAddressResponse;
    }

    public final void accept(Object obj) {
        this.f5326a.a(this.b, this.c, (Long) obj);
    }
}
