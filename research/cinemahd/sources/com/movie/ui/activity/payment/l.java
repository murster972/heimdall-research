package com.movie.ui.activity.payment;

import com.movie.data.model.AppConfig;
import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class l implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentResultFragment f5338a;
    private final /* synthetic */ BitcoinPaymentInfo b;

    public /* synthetic */ l(PaymentResultFragment paymentResultFragment, BitcoinPaymentInfo bitcoinPaymentInfo) {
        this.f5338a = paymentResultFragment;
        this.b = bitcoinPaymentInfo;
    }

    public final Object apply(Object obj) {
        return this.f5338a.a(this.b, (AppConfig) obj);
    }
}
