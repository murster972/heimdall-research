package com.movie.ui.activity.payment;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public class PaymentResultFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private PaymentResultFragment f5315a;
    private View b;

    public PaymentResultFragment_ViewBinding(final PaymentResultFragment paymentResultFragment, View view) {
        this.f5315a = paymentResultFragment;
        paymentResultFragment.tvStatus = (TextView) Utils.findRequiredViewAsType(view, R.id.tvStatus, "field 'tvStatus'", TextView.class);
        paymentResultFragment.tvTitlte = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTitlte, "field 'tvTitlte'", TextView.class);
        paymentResultFragment.progressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.progressbar, "field 'progressBar'", ProgressBar.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.btnRestart, "field 'btnRestart' and method 'onRestartClick'");
        paymentResultFragment.btnRestart = (Button) Utils.castView(findRequiredView, R.id.btnRestart, "field 'btnRestart'", Button.class);
        this.b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                paymentResultFragment.onRestartClick();
            }
        });
    }

    public void unbind() {
        PaymentResultFragment paymentResultFragment = this.f5315a;
        if (paymentResultFragment != null) {
            this.f5315a = null;
            paymentResultFragment.tvStatus = null;
            paymentResultFragment.tvTitlte = null;
            paymentResultFragment.progressBar = null;
            paymentResultFragment.btnRestart = null;
            this.b.setOnClickListener((View.OnClickListener) null);
            this.b = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
