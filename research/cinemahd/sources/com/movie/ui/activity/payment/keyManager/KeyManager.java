package com.movie.ui.activity.payment.keyManager;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.movie.AppComponent;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.AppConfig;
import com.movie.data.model.cinema.KeyResponse;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.activity.DaggerBaseActivityComponent;
import com.movie.ui.activity.payment.keyManager.DevicesApdater;
import com.movie.ui.widget.AnimatorStateView;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Iterator;
import javax.inject.Inject;

public class KeyManager extends BaseActivity implements DevicesApdater.DeviceItemListener {

    /* renamed from: a  reason: collision with root package name */
    DevicesApdater f5331a;
    @Inject
    MoviesApi b;
    String c;
    String d;
    String e;
    CompositeDisposable f = new CompositeDisposable();
    @BindView(2131296956)
    ProgressBar loading;
    @BindView(2131297000)
    RecyclerView rvDeviceItems;
    @BindView(2131297158)
    Toolbar toolbar;
    @BindView(2131297225)
    AnimatorStateView view_error;

    public /* synthetic */ void a(View view) {
        finish();
    }

    public /* synthetic */ void b(View view) {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_key_manager);
        setSupportActionBar(this.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.e(true);
            this.toolbar.setTitle((CharSequence) "Devices List");
            this.toolbar.setNavigationOnClickListener(new b(this));
        }
        getToolbar().setNavigationOnClickListener(new f(this));
        this.rvDeviceItems.setLayoutManager(new LinearLayoutManager(this));
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.c = extras.getString("TRANSACTION");
            this.d = extras.getString("EMAIL");
            this.e = extras.getString("KEY");
        }
        a(this.c, this.d, this.e);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f.dispose();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    private void a(String str, String str2, String str3) {
        this.f.b(this.b.getActivateInfo(str3, str, str2).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new d(this), new c(this)));
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        Utils.a((Activity) this, th.getMessage());
        this.view_error.setVisibility(0);
        this.view_error.setMessageText(th.getMessage());
        this.loading.setVisibility(4);
    }

    public /* synthetic */ void a(KeyResponse keyResponse) throws Exception {
        this.f5331a = new DevicesApdater();
        this.f5331a.a((DevicesApdater.DeviceItemListener) this);
        this.f5331a.a(keyResponse.getDevices());
        this.rvDeviceItems.setAdapter(this.f5331a);
        this.loading.setVisibility(4);
    }

    public void a(KeyResponse.DevicesBean devicesBean) {
        this.f.b(this.b.deactiveKey(this.e, devicesBean.getId()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new a(this, devicesBean), new e(this)));
    }

    public /* synthetic */ void a(KeyResponse.DevicesBean devicesBean, AppConfig appConfig) throws Exception {
        Iterator<KeyResponse.DevicesBean> it2 = this.f5331a.b().iterator();
        while (true) {
            if (!it2.hasNext()) {
                break;
            }
            KeyResponse.DevicesBean next = it2.next();
            if (next.getId().equals(devicesBean.getId())) {
                this.f5331a.b().remove(next);
                this.f5331a.notifyDataSetChanged();
                break;
            }
        }
        Utils.a((Activity) this, "deleted");
        this.loading.setVisibility(4);
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) this, th.getMessage());
        this.view_error.setVisibility(0);
        this.view_error.setMessageText(th.getMessage());
        this.loading.setVisibility(4);
    }
}
