package com.movie.ui.activity.payment.keyManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.movie.data.model.cinema.KeyResponse;
import com.original.tase.helper.DateTimeHelper;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.List;

public class DevicesApdater extends RecyclerView.Adapter<DeviceHolder> {

    /* renamed from: a  reason: collision with root package name */
    List<KeyResponse.DevicesBean> f5328a;
    DeviceItemListener b;

    static class DeviceHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f5330a;
        TextView b;
        TextView c;
        Button d;

        public DeviceHolder(View view) {
            super(view);
            this.f5330a = (TextView) view.findViewById(R.id.deviceName);
            this.b = (TextView) view.findViewById(R.id.deviceID);
            this.c = (TextView) view.findViewById(R.id.startTime);
            this.d = (Button) view.findViewById(R.id.deleteBtn);
        }
    }

    interface DeviceItemListener {
        void a(KeyResponse.DevicesBean devicesBean);
    }

    public void a(List<KeyResponse.DevicesBean> list) {
        this.f5328a = list;
    }

    public List<KeyResponse.DevicesBean> b() {
        return this.f5328a;
    }

    public int getItemCount() {
        return this.f5328a.size();
    }

    public void a(DeviceItemListener deviceItemListener) {
        this.b = deviceItemListener;
    }

    public DeviceHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.device_item, viewGroup, false);
        viewGroup.getContext();
        return new DeviceHolder(inflate);
    }

    /* renamed from: a */
    public void onBindViewHolder(DeviceHolder deviceHolder, int i) {
        final KeyResponse.DevicesBean devicesBean = this.f5328a.get(i);
        TextView textView = deviceHolder.b;
        textView.setText("Device ID      : " + devicesBean.getId());
        String b2 = DateTimeHelper.b(devicesBean.getStartTime());
        TextView textView2 = deviceHolder.c;
        textView2.setText("Start Time    : " + b2);
        if (devicesBean.getId().equals(Utils.f())) {
            deviceHolder.d.setVisibility(8);
            TextView textView3 = deviceHolder.f5330a;
            textView3.setText("[" + devicesBean.getName() + "]");
            return;
        }
        deviceHolder.f5330a.setText(devicesBean.getName());
        deviceHolder.d.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                DeviceItemListener deviceItemListener = DevicesApdater.this.b;
                if (deviceItemListener != null) {
                    deviceItemListener.a(devicesBean);
                }
            }
        });
    }
}
