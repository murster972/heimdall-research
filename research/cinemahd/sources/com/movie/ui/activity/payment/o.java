package com.movie.ui.activity.payment;

import com.movie.data.model.cinema.KeyResponse;
import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class o implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentResultFragment f5341a;
    private final /* synthetic */ BitcoinPaymentInfo b;

    public /* synthetic */ o(PaymentResultFragment paymentResultFragment, BitcoinPaymentInfo bitcoinPaymentInfo) {
        this.f5341a = paymentResultFragment;
        this.b = bitcoinPaymentInfo;
    }

    public final void accept(Object obj) {
        this.f5341a.a(this.b, (KeyResponse) obj);
    }
}
