package com.movie.ui.activity.payment.keyManager;

import com.movie.data.model.AppConfig;
import com.movie.data.model.cinema.KeyResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ KeyManager f5332a;
    private final /* synthetic */ KeyResponse.DevicesBean b;

    public /* synthetic */ a(KeyManager keyManager, KeyResponse.DevicesBean devicesBean) {
        this.f5332a = keyManager;
        this.b = devicesBean;
    }

    public final void accept(Object obj) {
        this.f5332a.a(this.b, (AppConfig) obj);
    }
}
