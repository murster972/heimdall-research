package com.movie.ui.activity.payment;

import com.movie.data.model.payment.bitcoin.BitcoinAddressResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class i implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentProcessingFragment f5325a;

    public /* synthetic */ i(PaymentProcessingFragment paymentProcessingFragment) {
        this.f5325a = paymentProcessingFragment;
    }

    public final void accept(Object obj) {
        this.f5325a.a((BitcoinAddressResponse) obj);
    }
}
