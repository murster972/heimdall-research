package com.movie.ui.activity.payment;

import com.movie.data.model.AppConfig;
import com.movie.data.model.payment.bitcoin.BitcoinPaymentInfo;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class n implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PaymentResultFragment f5340a;
    private final /* synthetic */ BitcoinPaymentInfo b;

    public /* synthetic */ n(PaymentResultFragment paymentResultFragment, BitcoinPaymentInfo bitcoinPaymentInfo) {
        this.f5340a = paymentResultFragment;
        this.b = bitcoinPaymentInfo;
    }

    public final Object apply(Object obj) {
        return this.f5340a.b(this.b, (AppConfig) obj);
    }
}
