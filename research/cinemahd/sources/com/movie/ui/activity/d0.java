package com.movie.ui.activity;

import com.movie.data.model.tmvdb.MovieTMDB;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class d0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieDetailsActivity f5217a;
    private final /* synthetic */ boolean b;

    public /* synthetic */ d0(MovieDetailsActivity movieDetailsActivity, boolean z) {
        this.f5217a = movieDetailsActivity;
        this.b = z;
    }

    public final void accept(Object obj) {
        this.f5217a.a(this.b, (MovieTMDB.ResultsBean) obj);
    }
}
