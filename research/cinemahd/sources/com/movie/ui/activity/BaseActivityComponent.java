package com.movie.ui.activity;

import com.movie.AppComponent;
import com.movie.ui.activity.gamechallenge.GameChallenge;
import com.movie.ui.activity.movies.MovieActivity;
import com.movie.ui.activity.payment.keyManager.KeyManager;
import com.movie.ui.activity.shows.ShowActivity;
import dagger.Component;

@Component(dependencies = {AppComponent.class})
public interface BaseActivityComponent {
    void a(CalendarActivity calendarActivity);

    void a(MainActivity mainActivity);

    void a(MemberActivationActivity memberActivationActivity);

    void a(MovieDetailsActivity movieDetailsActivity);

    void a(PlayerActivity playerActivity);

    void a(SettingsActivity settingsActivity);

    void a(SourceActivity sourceActivity);

    void a(TestCrappers testCrappers);

    void a(GameChallenge gameChallenge);

    void a(MovieActivity movieActivity);

    void a(KeyManager keyManager);

    void a(ShowActivity showActivity);
}
