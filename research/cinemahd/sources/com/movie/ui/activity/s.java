package com.movie.ui.activity;

import com.movie.data.model.AppConfig;
import com.movie.ui.activity.MemberActivationActivity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class s implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MemberActivationActivity.AnonymousClass1 f5350a;

    public /* synthetic */ s(MemberActivationActivity.AnonymousClass1 r1) {
        this.f5350a = r1;
    }

    public final void accept(Object obj) {
        this.f5350a.a((AppConfig) obj);
    }
}
