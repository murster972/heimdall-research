package com.movie.ui.activity.shows.seasons;

import com.uwetrottmann.thetvdb.entities.EpisodesResponse;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class e implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SeasonFragment f5480a;
    private final /* synthetic */ int b;
    private final /* synthetic */ int c;

    public /* synthetic */ e(SeasonFragment seasonFragment, int i, int i2) {
        this.f5480a = seasonFragment;
        this.b = i;
        this.c = i2;
    }

    public final Object apply(Object obj) {
        return this.f5480a.a(this.b, this.c, (EpisodesResponse) obj);
    }
}
