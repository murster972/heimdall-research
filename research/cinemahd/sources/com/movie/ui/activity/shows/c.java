package com.movie.ui.activity.shows;

import com.database.entitys.MovieEntity;
import com.movie.data.model.tmvdb.ExternalID;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieEntity f5437a;

    public /* synthetic */ c(MovieEntity movieEntity) {
        this.f5437a = movieEntity;
    }

    public final void accept(Object obj) {
        ShowActivity.a(this.f5437a, (ExternalID) obj);
    }
}
