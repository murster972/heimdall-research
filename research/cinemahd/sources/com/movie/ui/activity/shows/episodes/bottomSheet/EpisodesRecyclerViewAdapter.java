package com.movie.ui.activity.shows.episodes.bottomSheet;

import android.graphics.drawable.Drawable;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.database.entitys.SeasonEntity;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.activity.shows.episodes.bottomSheet.EpisodeBottomSheetFragment;
import com.yoku.marumovie.R;
import java.util.List;

public class EpisodesRecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private final List<EpisodeItem> f5444a;
    /* access modifiers changed from: private */
    public final EpisodeBottomSheetFragment.OnListFragmentInteractionListener b;
    /* access modifiers changed from: private */
    public SeasonEntity c;
    private Fragment d;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        /* renamed from: a  reason: collision with root package name */
        public final View f5446a;
        public CheckBox b = ((CheckBox) this.f5446a.findViewById(R.id.watched));
        public EpisodeItem c;

        public ViewHolder(View view) {
            super(view);
            this.f5446a = view;
            this.f5446a.setOnCreateContextMenuListener(this);
        }

        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            MenuItem add = contextMenu.add(1, 123, 2, "Add to watched list");
            MenuItem add2 = contextMenu.add(1, 124, 3, "Remove from Favourite");
            add.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    EpisodeBottomSheetFragment.OnListFragmentInteractionListener a2 = EpisodesRecyclerViewAdapter.this.b;
                    ViewHolder viewHolder = ViewHolder.this;
                    a2.a(viewHolder.f5446a, EpisodesRecyclerViewAdapter.this.c.g(), ViewHolder.this.c.f5440a.intValue());
                    ViewHolder.this.b.setChecked(true);
                    return true;
                }
            });
            add2.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                public boolean onMenuItemClick(MenuItem menuItem) {
                    EpisodeBottomSheetFragment.OnListFragmentInteractionListener a2 = EpisodesRecyclerViewAdapter.this.b;
                    ViewHolder viewHolder = ViewHolder.this;
                    a2.b(viewHolder.f5446a, EpisodesRecyclerViewAdapter.this.c.g(), ViewHolder.this.c.f5440a.intValue());
                    ViewHolder.this.b.setChecked(false);
                    return true;
                }
            });
        }

        public String toString() {
            return super.toString() + " '" + this.c.c + "'";
        }
    }

    public EpisodesRecyclerViewAdapter(Fragment fragment, List<EpisodeItem> list, EpisodeBottomSheetFragment.OnListFragmentInteractionListener onListFragmentInteractionListener, SeasonEntity seasonEntity) {
        this.f5444a = list;
        this.b = onListFragmentInteractionListener;
        this.c = seasonEntity;
        this.d = fragment;
    }

    public int getItemCount() {
        return this.f5444a.size();
    }

    /* renamed from: a */
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        EpisodeItem episodeItem = this.f5444a.get(i);
        TextView textView = (TextView) viewHolder.f5446a.findViewById(R.id.tvTabTitle);
        if (episodeItem.c != null) {
            textView.setText(String.format("%02d. ", new Object[]{episodeItem.f5440a}) + episodeItem.c);
        } else {
            textView.setText(String.format("Episode %02d", new Object[]{episodeItem.f5440a}));
        }
        TextView textView2 = (TextView) viewHolder.f5446a.findViewById(R.id.tvOverviewExpand);
        String str = episodeItem.f;
        if (str != null) {
            textView2.setText(str);
        } else {
            textView2.setVisibility(8);
        }
        TextView textView3 = (TextView) viewHolder.f5446a.findViewById(R.id.tvAiredDate);
        String str2 = episodeItem.g;
        if (str2 != null) {
            textView3.setText(str2);
        } else {
            textView3.setVisibility(8);
        }
        ImageView imageView = (ImageView) viewHolder.f5446a.findViewById(R.id.epi_cover);
        imageView.setImageDrawable((Drawable) null);
        Glide.a(this.d).a(episodeItem.e).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b()).a(imageView);
        ((CheckBox) viewHolder.f5446a.findViewById(R.id.watched)).setChecked(episodeItem.b.booleanValue());
        if (!episodeItem.h) {
            textView.setTextColor(-7829368);
            textView2.setTextColor(-7829368);
        }
        viewHolder.c = episodeItem;
        viewHolder.f5446a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (EpisodesRecyclerViewAdapter.this.b != null) {
                    EpisodesRecyclerViewAdapter.this.b.a(viewHolder.c, EpisodesRecyclerViewAdapter.this.c);
                }
            }
        });
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.episode_item, viewGroup, false);
        viewGroup.getContext();
        return new ViewHolder(inflate);
    }
}
