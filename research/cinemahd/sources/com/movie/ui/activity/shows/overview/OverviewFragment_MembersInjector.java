package com.movie.ui.activity.shows.overview;

import com.movie.data.api.tmdb.TMDBApi;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.MembersInjector;

public final class OverviewFragment_MembersInjector implements MembersInjector<OverviewFragment> {
    public static void a(OverviewFragment overviewFragment, TMDBApi tMDBApi) {
        overviewFragment.d = tMDBApi;
    }

    public static void a(OverviewFragment overviewFragment, TheTvdb theTvdb) {
        overviewFragment.f = theTvdb;
    }

    public static void a(OverviewFragment overviewFragment, MoviesHelper moviesHelper) {
        overviewFragment.g = moviesHelper;
    }
}
