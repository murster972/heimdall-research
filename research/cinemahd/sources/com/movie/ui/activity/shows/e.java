package com.movie.ui.activity.shows;

import android.view.View;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class e implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ShowActivity f5439a;
    private final /* synthetic */ View b;

    public /* synthetic */ e(ShowActivity showActivity, View view) {
        this.f5439a = showActivity;
        this.b = view;
    }

    public final void accept(Object obj) {
        this.f5439a.a(this.b, (Throwable) obj);
    }
}
