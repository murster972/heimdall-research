package com.movie.ui.activity.shows;

import android.view.View;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ShowActivity f5435a;
    private final /* synthetic */ View b;

    public /* synthetic */ a(ShowActivity showActivity, View view) {
        this.f5435a = showActivity;
        this.b = view;
    }

    public final void accept(Object obj) {
        this.f5435a.a(this.b, (String) obj);
    }
}
