package com.movie.ui.activity.shows.seasons;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class l implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SeasonFragment f5487a;
    private final /* synthetic */ Observer b;

    public /* synthetic */ l(SeasonFragment seasonFragment, Observer observer) {
        this.f5487a = seasonFragment;
        this.b = observer;
    }

    public final void accept(Object obj) {
        this.f5487a.a(this.b, (Throwable) obj);
    }
}
