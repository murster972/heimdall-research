package com.movie.ui.activity.shows.seasons;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.MembersInjector;

public final class SeasonFragment_MembersInjector implements MembersInjector<SeasonFragment> {
    public static void a(SeasonFragment seasonFragment, MoviesRepository moviesRepository) {
        seasonFragment.g = moviesRepository;
    }

    public static void a(SeasonFragment seasonFragment, TheTvdb theTvdb) {
        seasonFragment.h = theTvdb;
    }

    public static void a(SeasonFragment seasonFragment, MvDatabase mvDatabase) {
        seasonFragment.i = mvDatabase;
    }

    public static void a(SeasonFragment seasonFragment, TMDBApi tMDBApi) {
        seasonFragment.j = tMDBApi;
    }

    public static void a(SeasonFragment seasonFragment, MoviesApi moviesApi) {
        seasonFragment.k = moviesApi;
    }
}
