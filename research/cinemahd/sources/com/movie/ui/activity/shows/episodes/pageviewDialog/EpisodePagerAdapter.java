package com.movie.ui.activity.shows.episodes.pageviewDialog;

import android.content.Context;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.activity.shows.episodes.pageviewDialog.EpisodeDetailsFragment;
import java.util.List;

class EpisodePagerAdapter extends FragmentStatePagerAdapter {
    private final List<EpisodeItem> h;
    EpisodeDetailsFragment.EpisodeListener i;
    private int j;

    EpisodePagerAdapter(Context context, FragmentManager fragmentManager, List<EpisodeItem> list, int i2) {
        super(fragmentManager);
        this.h = list;
        this.j = i2;
    }

    public void a(EpisodeDetailsFragment.EpisodeListener episodeListener) {
        this.i = episodeListener;
    }

    public int getCount() {
        return this.h.size();
    }

    public int getItemPosition(Object obj) {
        return -2;
    }

    public CharSequence getPageTitle(int i2) {
        EpisodeItem episodeItem = this.h.get(i2);
        StringBuilder sb = new StringBuilder();
        sb.append(this.j);
        sb.append("x");
        sb.append(episodeItem.f5440a);
        sb.append(episodeItem.b.booleanValue() ? "(*)" : "");
        return sb.toString();
    }

    public Fragment a(int i2) {
        EpisodeDetailsFragment b = EpisodeDetailsFragment.b(this.h.get(i2));
        b.a(this.i);
        return b;
    }
}
