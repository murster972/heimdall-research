package com.movie.ui.activity.shows;

import com.database.entitys.MovieEntity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ShowActivity f5436a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ b(ShowActivity showActivity, MovieEntity movieEntity) {
        this.f5436a = showActivity;
        this.b = movieEntity;
    }

    public final void accept(Object obj) {
        this.f5436a.a(this.b, (Throwable) obj);
    }
}
