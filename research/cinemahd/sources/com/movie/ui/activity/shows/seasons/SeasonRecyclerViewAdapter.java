package com.movie.ui.activity.shows.seasons;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.database.entitys.SeasonEntity;
import com.database.entitys.TvWatchedEpisode;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.movie.FreeMoviesApp;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.activity.shows.seasons.SeasonFragment;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SeasonRecyclerViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private List<SeasonEntity> f5471a;
    private List<TvWatchedEpisode> b;
    /* access modifiers changed from: private */
    public ArrayList<EpisodeItem> c;
    /* access modifiers changed from: private */
    public final SeasonFragment.OnListFragmentInteractionListener d;
    private Context e;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {

        /* renamed from: a  reason: collision with root package name */
        public final View f5474a;
        public SeasonEntity b;
        public ImageView c;
        public TextView d;
        public TextView e;
        public TextView f;
        public TextView g;
        public ProgressBar h;
        public TextView i;
        public FrameLayout j;

        public ViewHolder(SeasonRecyclerViewAdapter seasonRecyclerViewAdapter, View view) {
            super(view);
            this.f5474a = view;
            this.c = (ImageView) view.findViewById(R.id.movie_poster);
            this.d = (TextView) view.findViewById(R.id.movie_title);
            this.e = (TextView) view.findViewById(R.id.tvOverview);
            this.f = (TextView) view.findViewById(R.id.tvOverviewExpand);
            this.g = (TextView) view.findViewById(R.id.movie_release_date);
            this.h = (ProgressBar) view.findViewById(R.id.progressBar2);
            this.i = (TextView) view.findViewById(R.id.textView7);
            this.j = (FrameLayout) view.findViewById(R.id.background);
            this.f5474a.setOnCreateContextMenuListener(this);
        }

        public void a(String str) {
            this.e.setText(str);
            int height = this.e.getHeight();
            int scrollY = this.e.getScrollY();
            Layout layout = this.e.getLayout();
            if (layout != null) {
                layout.getLineForVertical(scrollY);
                int i2 = scrollY + height;
                int lineForVertical = layout.getLineForVertical(i2);
                if (layout.getLineBottom(lineForVertical) > i2) {
                    lineForVertical--;
                }
                String charSequence = this.e.getText().toString();
                int lineEnd = this.e.getLayout().getLineEnd(Math.min(lineForVertical - 1, layout.getLineCount() - 1));
                if (lineEnd >= charSequence.length() - 1 || layout.getLineBottom(layout.getLineCount() - 1) <= height) {
                    this.e.setGravity(51);
                    this.f.setVisibility(8);
                    return;
                }
                String substring = charSequence.substring(0, lineEnd);
                if (substring.endsWith(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE)) {
                    substring = substring.substring(0, substring.length() - 1);
                }
                this.e.setText(substring);
                this.e.setGravity(83);
                this.f.setVisibility(0);
                this.f.setText(charSequence.substring(lineEnd, charSequence.length() - 1));
                return;
            }
            this.e.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                public void onGlobalLayout() {
                    ViewHolder.this.e.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    int height = ViewHolder.this.e.getHeight();
                    int scrollY = ViewHolder.this.e.getScrollY();
                    Layout layout = ViewHolder.this.e.getLayout();
                    if (layout != null) {
                        layout.getLineForVertical(scrollY);
                        int i = scrollY + height;
                        int lineForVertical = layout.getLineForVertical(i);
                        if (layout.getLineBottom(lineForVertical) > i) {
                            lineForVertical--;
                        }
                        String charSequence = ViewHolder.this.e.getText().toString();
                        int lineEnd = ViewHolder.this.e.getLayout().getLineEnd(Math.min(lineForVertical - 1, layout.getLineCount() - 1));
                        if (lineEnd >= charSequence.length() - 1 || layout.getLineBottom(layout.getLineCount() - 1) <= height) {
                            ViewHolder.this.e.setGravity(51);
                            ViewHolder.this.f.setVisibility(8);
                            return;
                        }
                        String substring = charSequence.substring(0, lineEnd);
                        if (substring.endsWith(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE)) {
                            substring = substring.substring(0, substring.length() - 1);
                        }
                        ViewHolder.this.e.setText(substring);
                        ViewHolder.this.e.setGravity(83);
                        ViewHolder.this.f.setVisibility(0);
                        ViewHolder.this.f.setText(charSequence.substring(lineEnd, charSequence.length() - 1));
                    }
                }
            });
        }

        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(getAdapterPosition(), 121, 0, "Add all to watched list");
            contextMenu.add(getAdapterPosition(), 122, 1, "Remove all from watched list");
            contextMenu.add(getAdapterPosition(), 124, 2, "Load season packs");
        }

        public String toString() {
            return super.toString() + " '" + this.b.d() + "'";
        }
    }

    public SeasonRecyclerViewAdapter(List<SeasonEntity> list, List<TvWatchedEpisode> list2, SeasonFragment.OnListFragmentInteractionListener onListFragmentInteractionListener) {
        this.f5471a = list;
        this.b = list2;
        this.d = onListFragmentInteractionListener;
    }

    public int getItemCount() {
        return this.f5471a.size();
    }

    public void a(ArrayList<EpisodeItem> arrayList) {
        this.c = arrayList;
    }

    public void b(List<TvWatchedEpisode> list) {
        this.b = list;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View inflate = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.session_item, viewGroup, false);
        this.e = viewGroup.getContext();
        return new ViewHolder(this, inflate);
    }

    public void a(List<SeasonEntity> list) {
        this.f5471a = list;
    }

    /* renamed from: a */
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {
        viewHolder.b = this.f5471a.get(i);
        viewHolder.f5474a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (SeasonRecyclerViewAdapter.this.d == null) {
                    return;
                }
                if (SeasonRecyclerViewAdapter.this.c != null) {
                    ArrayList arrayList = new ArrayList();
                    Iterator it2 = SeasonRecyclerViewAdapter.this.c.iterator();
                    while (it2.hasNext()) {
                        EpisodeItem episodeItem = (EpisodeItem) it2.next();
                        if (episodeItem.j.intValue() == viewHolder.b.g()) {
                            arrayList.add(episodeItem);
                        }
                    }
                    SeasonRecyclerViewAdapter.this.d.a(viewHolder.b, arrayList);
                    return;
                }
                SeasonRecyclerViewAdapter.this.d.a(viewHolder.b, (ArrayList<EpisodeItem>) null);
            }
        });
        RequestBuilder<Drawable> a2 = Glide.d(this.e).a(viewHolder.b.f()).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b());
        boolean z = FreeMoviesApp.l().getBoolean("pref_change_bg_color", true);
        if (!FreeMoviesApp.o() && z) {
            a2.b((RequestListener<Drawable>) GlidePalette.a(viewHolder.b.f()).a((BitmapPalette.CallBack) new BitmapPalette.CallBack(this) {
                public void a(Palette palette) {
                    if (palette.b() != null) {
                        viewHolder.j.setBackgroundColor(palette.b().d());
                    }
                }
            }));
        }
        a2.a(viewHolder.c);
        viewHolder.d.setText(viewHolder.b.d());
        viewHolder.a(viewHolder.b.e());
        viewHolder.g.setText(Utils.e(viewHolder.b.a()));
        List<TvWatchedEpisode> a3 = a(this.b, viewHolder.b.g());
        viewHolder.h.setProgress(Double.valueOf(((((double) a3.size()) * 1.0d) / ((double) viewHolder.b.b())) * 100.0d).intValue());
        TextView textView = viewHolder.i;
        textView.setText(a3.size() + "/" + viewHolder.b.b() + " watched");
    }

    public SeasonRecyclerViewAdapter(List<SeasonEntity> list, List<TvWatchedEpisode> list2, ArrayList<EpisodeItem> arrayList, SeasonFragment.OnListFragmentInteractionListener onListFragmentInteractionListener) {
        this.f5471a = list;
        this.b = list2;
        this.c = arrayList;
        this.d = onListFragmentInteractionListener;
    }

    private List<TvWatchedEpisode> a(List<TvWatchedEpisode> list, int i) {
        ArrayList arrayList = new ArrayList();
        for (TvWatchedEpisode next : list) {
            if (next.e() == i) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }
}
