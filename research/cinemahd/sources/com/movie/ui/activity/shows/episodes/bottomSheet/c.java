package com.movie.ui.activity.shows.episodes.bottomSheet;

import com.movie.data.model.tmvdb.ExternalID;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class c implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ EpisodeBottomSheetFragment f5451a;

    public /* synthetic */ c(EpisodeBottomSheetFragment episodeBottomSheetFragment) {
        this.f5451a = episodeBottomSheetFragment;
    }

    public final Object apply(Object obj) {
        return this.f5451a.a((ExternalID) obj);
    }
}
