package com.movie.ui.activity.shows.episodes;

import android.os.Parcel;
import android.os.Parcelable;
import com.movie.data.model.tmvdb.SeasonTMDB;
import com.original.tase.helper.DateTimeHelper;
import com.uwetrottmann.thetvdb.entities.Episode;

public class EpisodeItem implements Parcelable, Comparable<EpisodeItem> {
    public static final Parcelable.Creator<EpisodeItem> CREATOR = new Parcelable.Creator<EpisodeItem>() {
        public EpisodeItem createFromParcel(Parcel parcel) {
            return new EpisodeItem(parcel);
        }

        public EpisodeItem[] newArray(int i) {
            return new EpisodeItem[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    public Integer f5440a;
    public Boolean b;
    public String c;
    public Integer d;
    public String e;
    public String f;
    public String g;
    public boolean h;
    public String i;
    public Integer j;

    public EpisodeItem(SeasonTMDB.EpisodesBean episodesBean, boolean z, int i2, String str) {
        this.f5440a = Integer.valueOf(episodesBean.getEpisode_number());
        this.e = episodesBean.getStill_path();
        this.b = Boolean.valueOf(z);
        this.d = Integer.valueOf(i2);
        this.f = episodesBean.getOverview();
        this.c = episodesBean.getName();
        this.g = episodesBean.getAir_date();
        this.h = DateTimeHelper.a(DateTimeHelper.d(episodesBean.getAir_date()));
        this.i = str;
    }

    /* renamed from: a */
    public int compareTo(EpisodeItem episodeItem) {
        return this.f5440a.intValue() > episodeItem.f5440a.intValue() ? 1 : -1;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (this.f5440a == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.f5440a.intValue());
        }
        Boolean bool = this.b;
        parcel.writeByte((byte) (bool == null ? 0 : bool.booleanValue() ? 1 : 2));
        parcel.writeString(this.c);
        if (this.d == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeInt(this.d.intValue());
        }
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeString(this.g);
        parcel.writeByte(this.h ? (byte) 1 : 0);
        parcel.writeString(this.i);
        if (this.j == null) {
            parcel.writeByte((byte) 0);
            return;
        }
        parcel.writeByte((byte) 1);
        parcel.writeInt(this.j.intValue());
    }

    public EpisodeItem(Episode episode, boolean z, int i2, String str) {
        this.f5440a = episode.airedEpisodeNumber;
        this.e = "http://thetvdb.com/banners/" + episode.filename;
        this.b = Boolean.valueOf(z);
        this.d = Integer.valueOf(i2);
        this.f = episode.overview;
        this.c = episode.episodeName;
        String str2 = episode.firstAired;
        this.g = str2;
        this.h = DateTimeHelper.a(DateTimeHelper.d(str2));
        this.i = str;
    }

    public EpisodeItem(Integer num, Boolean bool, String str, Integer num2, String str2, String str3, boolean z, String str4, Integer num3, String str5) {
        this.f5440a = num;
        this.b = bool;
        this.c = str;
        this.d = num2;
        this.e = str2;
        this.f = str3;
        this.h = z;
        this.i = str4;
        this.j = num3;
        this.g = str5;
    }

    protected EpisodeItem(Parcel parcel) {
        Boolean bool;
        if (parcel.readByte() == 0) {
            this.f5440a = null;
        } else {
            this.f5440a = Integer.valueOf(parcel.readInt());
        }
        byte readByte = parcel.readByte();
        boolean z = false;
        if (readByte == 0) {
            bool = null;
        } else {
            bool = Boolean.valueOf(readByte == 1);
        }
        this.b = bool;
        this.c = parcel.readString();
        if (parcel.readByte() == 0) {
            this.d = null;
        } else {
            this.d = Integer.valueOf(parcel.readInt());
        }
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readString();
        this.h = parcel.readByte() != 0 ? true : z;
        this.i = parcel.readString();
        if (parcel.readByte() == 0) {
            this.j = null;
        } else {
            this.j = Integer.valueOf(parcel.readInt());
        }
    }
}
