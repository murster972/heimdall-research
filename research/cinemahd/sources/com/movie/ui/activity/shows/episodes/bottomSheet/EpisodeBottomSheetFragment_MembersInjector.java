package com.movie.ui.activity.shows.episodes.bottomSheet;

import com.database.MvDatabase;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import dagger.MembersInjector;

public final class EpisodeBottomSheetFragment_MembersInjector implements MembersInjector<EpisodeBottomSheetFragment> {
    public static void a(EpisodeBottomSheetFragment episodeBottomSheetFragment, MoviesRepository moviesRepository) {
        episodeBottomSheetFragment.f5441a = moviesRepository;
    }

    public static void a(EpisodeBottomSheetFragment episodeBottomSheetFragment, MvDatabase mvDatabase) {
        episodeBottomSheetFragment.b = mvDatabase;
    }

    public static void a(EpisodeBottomSheetFragment episodeBottomSheetFragment, TMDBApi tMDBApi) {
        episodeBottomSheetFragment.c = tMDBApi;
    }
}
