package com.movie.ui.activity.shows.episodes.pageviewDialog;

import com.database.MvDatabase;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import dagger.MembersInjector;

public final class PageViewDialog_MembersInjector implements MembersInjector<PageViewDialog> {
    public static void a(PageViewDialog pageViewDialog, MoviesRepository moviesRepository) {
        pageViewDialog.b = moviesRepository;
    }

    public static void a(PageViewDialog pageViewDialog, MvDatabase mvDatabase) {
        pageViewDialog.c = mvDatabase;
    }

    public static void a(PageViewDialog pageViewDialog, TMDBApi tMDBApi) {
        pageViewDialog.d = tMDBApi;
    }
}
