package com.movie.ui.activity.shows;

import com.database.MvDatabase;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.MembersInjector;

public final class ShowActivity_MembersInjector implements MembersInjector<ShowActivity> {
    public static void a(ShowActivity showActivity, MvDatabase mvDatabase) {
        showActivity.f5431a = mvDatabase;
    }

    public static void a(ShowActivity showActivity, MoviesHelper moviesHelper) {
        showActivity.b = moviesHelper;
    }

    public static void a(ShowActivity showActivity, TMDBApi tMDBApi) {
        showActivity.c = tMDBApi;
    }

    public static void a(ShowActivity showActivity, TheTvdb theTvdb) {
        showActivity.d = theTvdb;
    }
}
