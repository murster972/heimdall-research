package com.movie.ui.activity.shows.overview;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import com.ads.videoreward.AdsManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.database.entitys.MovieEntity;
import com.movie.AppComponent;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.helper.MoviesHelper;
import com.movie.ui.widget.AspectLockedImageView;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.thetvdb.entities.Series;
import com.uwetrottmann.thetvdb.entities.SeriesResponse;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;
import retrofit2.Response;

public class OverviewFragment extends BaseFragment {
    @BindView(2131296339)
    FrameLayout adView;
    /* access modifiers changed from: private */
    public MovieEntity c;
    @BindView(2131296514)
    ConstraintLayout content;
    @Inject
    TMDBApi d;
    CompositeDisposable e;
    @Inject
    TheTvdb f;
    @Inject
    MoviesHelper g;
    @BindView(2131296765)
    ProgressBar loading;
    @BindView(2131296802)
    AspectLockedImageView movie_cover;
    @BindView(2131297185)
    TextView tvName;
    @BindView(2131297188)
    TextView tvOverview;
    @BindView(2131297189)
    TextView tvOverviewExpand;
    @BindView(2131296525)
    TextView tvRating;
    @BindView(2131297201)
    TextView tvtime;

    public interface OnFragmentInteractionListener {
    }

    /* access modifiers changed from: package-private */
    public void d(String str) {
        this.tvOverview.setText(str);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            OnFragmentInteractionListener onFragmentInteractionListener = (OnFragmentInteractionListener) context;
            return;
        }
        throw new RuntimeException(context.toString() + " must implement OnFragmentInteractionListener");
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_overview, viewGroup, false);
    }

    public void onDestroyView() {
        this.e.dispose();
        super.onDestroyView();
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        AdsManager.l().a(this.adView);
        this.loading.setVisibility(0);
        this.c = (MovieEntity) getArguments().getParcelable("arg_movie");
        this.e = new CompositeDisposable();
        this.e.b(Observable.create(new ObservableOnSubscribe<Series>() {
            public void subscribe(ObservableEmitter<Series> observableEmitter) throws Exception {
                Response<SeriesResponse> execute = OverviewFragment.this.f.series().series((int) OverviewFragment.this.c.getTvdbID(), "en").execute();
                if (execute.isSuccessful()) {
                    observableEmitter.onNext(execute.body().data);
                }
                observableEmitter.onComplete();
            }
        }).observeOn(AndroidSchedulers.a()).subscribe(new a(this, view), new b(this, view)));
    }

    public static OverviewFragment a(MovieEntity movieEntity) {
        OverviewFragment overviewFragment = new OverviewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("arg_movie", movieEntity);
        overviewFragment.setArguments(bundle);
        return overviewFragment;
    }

    public /* synthetic */ void a(View view, Series series) throws Exception {
        this.tvName.setText(series.seriesName);
        TextView textView = this.tvtime;
        textView.setText("Release : " + series.firstAired);
        d(series.overview);
        TextView textView2 = this.tvRating;
        StringBuilder sb = new StringBuilder();
        sb.append("Rating : ");
        Double d2 = series.siteRating;
        sb.append(Utils.a(d2 == null ? 0.0d : d2.doubleValue(), 2));
        textView2.setText(sb.toString());
        Glide.d(view.getContext()).a(this.c.getPoster_path()).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b()).a((ImageView) this.movie_cover);
        this.content.setVisibility(0);
        this.loading.setVisibility(8);
    }

    public /* synthetic */ void a(View view, Throwable th) throws Exception {
        this.tvName.setText(this.c.getName());
        TextView textView = this.tvtime;
        textView.setText("Release : " + this.c.getRealeaseDate());
        d(this.c.getOverview());
        TextView textView2 = this.tvRating;
        StringBuilder sb = new StringBuilder();
        sb.append("Rating : ");
        sb.append(Utils.a(this.c.getVote() == null ? 0.0d : this.c.getVote().doubleValue(), 2));
        textView2.setText(sb.toString());
        Glide.d(view.getContext()).a(this.c.getPoster_path()).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b()).a((ImageView) this.movie_cover);
        this.content.setVisibility(0);
        this.loading.setVisibility(8);
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }
}
