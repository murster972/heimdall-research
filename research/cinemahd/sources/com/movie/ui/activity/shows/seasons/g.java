package com.movie.ui.activity.shows.seasons;

import io.reactivex.Observer;
import io.reactivex.functions.Action;

/* compiled from: lambda */
public final /* synthetic */ class g implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SeasonFragment f5482a;
    private final /* synthetic */ Observer b;

    public /* synthetic */ g(SeasonFragment seasonFragment, Observer observer) {
        this.f5482a = seasonFragment;
        this.b = observer;
    }

    public final void run() {
        this.f5482a.a(this.b);
    }
}
