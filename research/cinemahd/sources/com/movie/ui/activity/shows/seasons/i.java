package com.movie.ui.activity.shows.seasons;

import com.database.entitys.MovieEntity;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class i implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SeasonFragment f5484a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ i(SeasonFragment seasonFragment, MovieEntity movieEntity) {
        this.f5484a = seasonFragment;
        this.b = movieEntity;
    }

    public final void subscribe(Observer observer) {
        this.f5484a.a(this.b, observer);
    }
}
