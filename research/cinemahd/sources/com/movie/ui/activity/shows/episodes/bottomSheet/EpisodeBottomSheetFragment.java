package com.movie.ui.activity.shows.episodes.bottomSheet;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.database.entitys.TvWatchedEpisode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.tmvdb.ExternalID;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.thetvdb.entities.Episode;
import com.uwetrottmann.thetvdb.entities.EpisodesResponse;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import retrofit2.Response;

public class EpisodeBottomSheetFragment extends BottomSheetDialogFragment {
    @Inject

    /* renamed from: a  reason: collision with root package name */
    MoviesRepository f5441a;
    @Inject
    MvDatabase b;
    @Inject
    TMDBApi c;
    TheTvdb d = new TheTvdb("6UMSCJSYNU96S28F");
    LinearLayoutManager e = null;
    private Unbinder f;
    private OnListFragmentInteractionListener g;
    private CompositeDisposable h;
    private SeasonEntity i;
    private MovieEntity j;
    private EpisodesRecyclerViewAdapter k;
    @BindView(2131296765)
    ProgressBar loading;
    @BindView(2131297001)
    RecyclerView rvList;

    public interface OnListFragmentInteractionListener {
        void a(View view, int i, int i2);

        void a(EpisodeItem episodeItem, SeasonEntity seasonEntity);

        void b(View view, int i, int i2);
    }

    public /* synthetic */ List a(ExternalID externalID) throws Exception {
        ArrayList<EpisodeItem> arrayList = new ArrayList<>();
        Response<EpisodesResponse> execute = this.d.series().episodesQuery(externalID.getTvdb_id(), (Integer) null, Integer.valueOf(this.i.g()), (Integer) null, (Integer) null, (Double) null, (String) null, (String) null, 1, "en").execute();
        if (execute.isSuccessful()) {
            for (Episode episodeItem : execute.body().data) {
                arrayList.add(new EpisodeItem(episodeItem, false, this.i.b(), "TVDB"));
            }
            List<TvWatchedEpisode> a2 = this.b.o().a(this.j.getTmdbID(), this.j.getImdbIDStr(), this.j.getTraktID(), this.j.getTvdbID(), this.i.g());
            for (EpisodeItem episodeItem2 : arrayList) {
                for (TvWatchedEpisode b2 : a2) {
                    if (episodeItem2.f5440a.intValue() == b2.b()) {
                        episodeItem2.b = true;
                    }
                }
            }
        }
        return arrayList;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            this.g = (OnListFragmentInteractionListener) context;
            DaggerBaseFragmentComponent.a().a(FreeMoviesApp.a((Context) (Activity) context).d()).a().a(this);
            return;
        }
        throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.episode_bottom_sheet, viewGroup, false);
        this.f = ButterKnife.bind((Object) this, inflate);
        return inflate;
    }

    public void onDestroyView() {
        this.f.unbind();
        this.h.dispose();
        super.onDestroyView();
    }

    public void onDetach() {
        super.onDetach();
        this.g = null;
    }

    public void onResume() {
        super.onResume();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.h = new CompositeDisposable();
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                BottomSheetBehavior from = BottomSheetBehavior.from((FrameLayout) ((BottomSheetDialog) EpisodeBottomSheetFragment.this.getDialog()).findViewById(R.id.design_bottom_sheet));
                from.c(3);
                from.b(0);
            }
        });
        this.e = new LinearLayoutManager(view.getContext());
        this.rvList.setLayoutManager(this.e);
        this.loading.setVisibility(0);
        this.h.b(this.c.getTVExternalID(this.j.getTmdbID()).subscribeOn(Schedulers.b()).map(new c(this)).observeOn(AndroidSchedulers.a()).subscribe(new a(this), b.f5450a));
    }

    public /* synthetic */ void a(List list) throws Exception {
        this.k = new EpisodesRecyclerViewAdapter(this, list, this.g, this.i);
        this.rvList.setAdapter(this.k);
        this.loading.setVisibility(8);
    }
}
