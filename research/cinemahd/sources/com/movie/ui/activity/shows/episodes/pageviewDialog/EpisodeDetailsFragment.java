package com.movie.ui.activity.shows.episodes.pageviewDialog;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.movie.AppComponent;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.yoku.marumovie.R;

public class EpisodeDetailsFragment extends BaseFragment {
    EpisodeItem c;
    @BindView(2131296483)
    CheckBox checkBox;
    @BindView(2131296514)
    ConstraintLayout content;
    /* access modifiers changed from: private */
    public EpisodeListener d;
    @BindView(2131296765)
    ProgressBar loading;
    @BindView(2131296802)
    ImageView mCoverImage;
    @BindView(2131296689)
    ImageButton playbtn;
    @BindView(2131297127)
    TextView textViewEpisodeOverview;
    @BindView(2131297128)
    TextView textViewReleaseTime;
    @BindView(2131297130)
    TextView textViewSource;
    @BindView(2131297129)
    TextView textViewTitle;

    public interface EpisodeListener {
        void a(EpisodeItem episodeItem);

        void a(EpisodeItem episodeItem, boolean z);
    }

    public static EpisodeDetailsFragment b(EpisodeItem episodeItem) {
        EpisodeDetailsFragment episodeDetailsFragment = new EpisodeDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("episode_tvdbid", episodeItem);
        episodeDetailsFragment.setArguments(bundle);
        return episodeDetailsFragment;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Bundle arguments = getArguments();
        if (arguments != null) {
            this.c = (EpisodeItem) arguments.getParcelable("episode_tvdbid");
            return;
        }
        throw new IllegalArgumentException("Missing arguments");
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_episode, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.c = (EpisodeItem) getArguments().getParcelable("episode_tvdbid");
        this.textViewTitle.setText(this.c.c);
        TextView textView = this.textViewReleaseTime;
        StringBuilder sb = new StringBuilder();
        sb.append(this.c.h ? "Aired Date : " : "Air Date :");
        sb.append(this.c.g);
        textView.setText(sb.toString());
        TextView textView2 = this.textViewEpisodeOverview;
        textView2.setText("Summary : " + this.c.f);
        TextView textView3 = this.textViewSource;
        textView3.setText("Source : " + this.c.i);
        if (!this.c.h) {
            view.setBackgroundColor(-7829368);
        }
        Glide.a((Fragment) this).a(this.c.e).a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).a((int) R.color.list_dropdown_foreground_selected)).b()).a(new DrawableTransitionOptions().b()).a(this.mCoverImage);
        this.playbtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (EpisodeDetailsFragment.this.d != null) {
                    EpisodeDetailsFragment.this.d.a(EpisodeDetailsFragment.this.c);
                }
            }
        });
        this.checkBox.setChecked(this.c.b.booleanValue());
        this.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (EpisodeDetailsFragment.this.d != null) {
                    EpisodeDetailsFragment.this.d.a(EpisodeDetailsFragment.this.c, z);
                }
            }
        });
        this.content.setVisibility(0);
        this.loading.setVisibility(4);
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public void a(EpisodeListener episodeListener) {
        this.d = episodeListener;
    }
}
