package com.movie.ui.activity.shows.episodes.pageviewDialog;

import com.movie.data.model.tmvdb.ExternalID;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class d implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PageViewDialog f5461a;

    public /* synthetic */ d(PageViewDialog pageViewDialog) {
        this.f5461a = pageViewDialog;
    }

    public final Object apply(Object obj) {
        return this.f5461a.a((ExternalID) obj);
    }
}
