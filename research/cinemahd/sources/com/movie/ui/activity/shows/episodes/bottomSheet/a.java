package com.movie.ui.activity.shows.episodes.bottomSheet;

import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ EpisodeBottomSheetFragment f5449a;

    public /* synthetic */ a(EpisodeBottomSheetFragment episodeBottomSheetFragment) {
        this.f5449a = episodeBottomSheetFragment;
    }

    public final void accept(Object obj) {
        this.f5449a.a((List) obj);
    }
}
