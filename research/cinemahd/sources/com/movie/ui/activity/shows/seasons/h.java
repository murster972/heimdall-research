package com.movie.ui.activity.shows.seasons;

import com.uwetrottmann.thetvdb.entities.EpisodesResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class h implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SeasonFragment f5483a;

    public /* synthetic */ h(SeasonFragment seasonFragment) {
        this.f5483a = seasonFragment;
    }

    public final void accept(Object obj) {
        this.f5483a.a((EpisodesResponse) obj);
    }
}
