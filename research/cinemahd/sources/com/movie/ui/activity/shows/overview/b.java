package com.movie.ui.activity.shows.overview;

import android.view.View;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ OverviewFragment f5465a;
    private final /* synthetic */ View b;

    public /* synthetic */ b(OverviewFragment overviewFragment, View view) {
        this.f5465a = overviewFragment;
        this.b = view;
    }

    public final void accept(Object obj) {
        this.f5465a.a(this.b, (Throwable) obj);
    }
}
