package com.movie.ui.activity.shows;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;
import androidx.palette.graphics.Palette;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.database.entitys.TvWatchedEpisode;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.tmvdb.ExternalID;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.activity.DaggerBaseActivityComponent;
import com.movie.ui.activity.MovieDetailsActivity;
import com.movie.ui.activity.SourceActivity;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.activity.shows.episodes.bottomSheet.EpisodeBottomSheetFragment;
import com.movie.ui.activity.shows.episodes.pageviewDialog.PageViewDialog;
import com.movie.ui.activity.shows.overview.OverviewFragment;
import com.movie.ui.activity.shows.seasons.SeasonFragment;
import com.movie.ui.fragment.BrowseMoviesFragment;
import com.movie.ui.fragment.MoviesFragment;
import com.movie.ui.helper.MoviesHelper;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import javax.inject.Inject;

public class ShowActivity extends BaseActivity implements EpisodeBottomSheetFragment.OnListFragmentInteractionListener, PageViewDialog.OnListFragmentInteractionListener, SeasonFragment.OnListFragmentInteractionListener, OverviewFragment.OnFragmentInteractionListener, MoviesFragment.Listener {
    @Inject

    /* renamed from: a  reason: collision with root package name */
    MvDatabase f5431a;
    @Inject
    MoviesHelper b;
    @Inject
    TMDBApi c;
    @BindView(2131296507)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @Inject
    TheTvdb d;
    CompositeDisposable e;
    private MovieEntity f;
    @BindView(2131297160)
    ImageView imageView;
    @BindView(2131296998)
    CoordinatorLayout root_view;
    @BindView(2131297096)
    TabLayout tabLayout;
    @BindView(2131297158)
    Toolbar toolbar;
    @BindView(2131297230)
    ViewPager viewPager;

    public static class ShowPagerAdapter extends FragmentStatePagerAdapter {
        private MovieEntity h;
        SeasonFragment i;
        OverviewFragment j;
        BrowseMoviesFragment k;
        CompositeDisposable l = this.l;

        public ShowPagerAdapter(FragmentManager fragmentManager, MovieEntity movieEntity) {
            super(fragmentManager);
            this.h = movieEntity;
        }

        public Fragment a(int i2) {
            return b(i2);
        }

        public Fragment b(int i2) {
            if (i2 == 0) {
                if (this.i == null) {
                    this.i = SeasonFragment.a(1, this.h);
                }
                return this.i;
            } else if (i2 == 1) {
                if (this.j == null) {
                    this.j = OverviewFragment.a(this.h);
                }
                return this.j;
            } else if (i2 != 2) {
                OverviewFragment overviewFragment = new OverviewFragment();
                overviewFragment.setArguments(new Bundle());
                return overviewFragment;
            } else {
                if (this.k == null) {
                    this.k = BrowseMoviesFragment.a(Integer.valueOf((int) this.h.getTmdbID()), BrowseMoviesFragment.Type.TV_RECOMENDATION);
                }
                return this.k;
            }
        }

        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int i2) {
            return "OBJECT " + (i2 + 1);
        }
    }

    private void b(MovieEntity movieEntity) {
        TabLayout tabLayout2 = this.tabLayout;
        tabLayout2.a(tabLayout2.b().b((CharSequence) "Seasons"));
        TabLayout tabLayout3 = this.tabLayout;
        tabLayout3.a(tabLayout3.b().b((CharSequence) "Overview"));
        TabLayout tabLayout4 = this.tabLayout;
        tabLayout4.a(tabLayout4.b().b((CharSequence) "Recommendations"));
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                ShowActivity.this.tabLayout.b(i).g();
            }
        });
        this.tabLayout.a((TabLayout.BaseOnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
            public void a(TabLayout.Tab tab) {
                ShowActivity.this.viewPager.setCurrentItem(tab.c());
            }

            public void b(TabLayout.Tab tab) {
            }

            public void c(TabLayout.Tab tab) {
            }
        });
        if (movieEntity.getBackdrop_path() == null || Utils.a((Activity) this) == 2) {
            this.imageView.setVisibility(8);
        } else {
            this.imageView.setVisibility(0);
            RequestBuilder<Drawable> a2 = Glide.a((FragmentActivity) this).a(movieEntity.getBackdrop_path()).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b());
            boolean z = FreeMoviesApp.l().getBoolean("pref_change_bg_color", true);
            if (!FreeMoviesApp.o() && z) {
                a2.b((RequestListener<Drawable>) GlidePalette.a(movieEntity.getBackdrop_path()).a((BitmapPalette.CallBack) new BitmapPalette.CallBack() {
                    public void a(Palette palette) {
                        ShowActivity.this.a(palette.b());
                    }
                }));
            }
            a2.a(this.imageView);
        }
        if (movieEntity.getTvdbID() > 0) {
            this.viewPager.setAdapter(new ShowPagerAdapter(getSupportFragmentManager(), movieEntity));
        } else {
            this.e.b(this.c.getTVExternalID(movieEntity.getTmdbID()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new c(movieEntity), new b(this, movieEntity), new d(this, movieEntity)));
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_show);
        setSupportActionBar(this.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.e(true);
        }
        this.f = (MovieEntity) getIntent().getParcelableExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE");
        if (this.f.getName().toLowerCase().contains("law & order:")) {
            this.f.setName("Law and Order SVU");
        }
        this.e = new CompositeDisposable();
        b(this.f);
        this.toolbar.setTitle((CharSequence) this.f.getName());
        this.collapsingToolbarLayout.setTitle(this.f.getName());
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_show, menu);
        MenuItem findItem = menu.findItem(R.id.menu_favorite);
        if (this.f.getCollected_at() == null) {
            findItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_border));
            return true;
        }
        findItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_full));
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.e.dispose();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.menu_favorite) {
            MoviesHelper moviesHelper = this.b;
            MovieEntity movieEntity = this.f;
            moviesHelper.a(this, movieEntity, movieEntity.getCollected_at() == null);
            if (this.f.getCollected_at() == null) {
                menuItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_border));
            } else {
                menuItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_full));
            }
            return true;
        }
        menuItem.getItemId();
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    static /* synthetic */ void a(MovieEntity movieEntity, ExternalID externalID) throws Exception {
        movieEntity.setTvdbID((long) externalID.getTvdb_id());
        movieEntity.setImdbIDStr(externalID.getImdb_id());
    }

    public /* synthetic */ void a(MovieEntity movieEntity, Throwable th) throws Exception {
        this.viewPager.setAdapter(new ShowPagerAdapter(getSupportFragmentManager(), movieEntity));
    }

    public /* synthetic */ void a(MovieEntity movieEntity) throws Exception {
        this.viewPager.setAdapter(new ShowPagerAdapter(getSupportFragmentManager(), movieEntity));
    }

    /* access modifiers changed from: private */
    public void a(Palette.Swatch swatch) {
        if (swatch != null) {
            this.tabLayout.setBackgroundColor(swatch.d());
        }
    }

    public void a(EpisodeItem episodeItem, SeasonEntity seasonEntity) {
        Utils.a((Activity) this, "This function will take a few seconds");
        episodeItem.b = true;
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("Movie", this.f);
        String str = this.f.getRealeaseDate().isEmpty() ? "" : this.f.getRealeaseDate().split("-")[0];
        MovieInfo movieInfo = new MovieInfo(this.f.getName(), str, "" + seasonEntity.g(), "" + episodeItem.f5440a, seasonEntity.a() == null ? "1970" : seasonEntity.a().split("-")[0], this.f.getGenres());
        movieInfo.epsCount = episodeItem.d.intValue();
        intent.putExtra("MovieInfo", movieInfo);
        startActivity(intent);
    }

    public void a(MovieEntity movieEntity, View view) {
        if (movieEntity.getTV().booleanValue()) {
            Intent intent = new Intent(this, ShowActivity.class);
            intent.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent);
        } else {
            Intent intent2 = new Intent(this, MovieDetailsActivity.class);
            intent2.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent2);
        }
        finish();
    }

    public void a(View view, int i, int i2) {
        a(view, i, i2, true, true);
    }

    public void b(View view, int i, int i2) {
        a(view, i, i2, false, true);
    }

    /* access modifiers changed from: package-private */
    public void a(View view, int i, int i2, boolean z, boolean z2) {
        TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
        tvWatchedEpisode.a(i2);
        tvWatchedEpisode.c(i);
        tvWatchedEpisode.c(this.f.getTmdbID());
        tvWatchedEpisode.a(this.f.getImdbIDStr());
        tvWatchedEpisode.e(this.f.getTvdbID());
        tvWatchedEpisode.d(this.f.getTraktID());
        this.e.b(this.b.a(this.f, tvWatchedEpisode, z, z2).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new a(this, view), new e(this, view)));
    }

    public void b(String str) {
        a(str, (View) this.root_view);
    }

    public /* synthetic */ void a(View view, Throwable th) throws Exception {
        a(th.getMessage(), view);
    }

    public void a(SeasonEntity seasonEntity, ArrayList<EpisodeItem> arrayList) {
        FragmentTransaction b2 = getSupportFragmentManager().b();
        Fragment b3 = getSupportFragmentManager().b("fragment_edit_name");
        if (b3 != null) {
            b2.a(b3);
        }
        b2.a((String) null);
        PageViewDialog a2 = PageViewDialog.a("Some Title", this.f, seasonEntity, arrayList);
        a2.a((PageViewDialog.OnListFragmentInteractionListener) this);
        a2.show(b2, "fragment_edit_name");
    }

    public void a(String str) {
        a(str, (View) this.root_view);
    }

    public void a(String str, View view) {
        Snackbar a2 = Snackbar.a(view, str, 0);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) a2.f().getLayoutParams();
        layoutParams.bottomMargin = 0;
        a2.f().setLayoutParams(layoutParams);
        a2.k();
    }
}
