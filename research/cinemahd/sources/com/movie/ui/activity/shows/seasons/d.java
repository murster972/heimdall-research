package com.movie.ui.activity.shows.seasons;

import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5479a;

    public /* synthetic */ d(ObservableEmitter observableEmitter) {
        this.f5479a = observableEmitter;
    }

    public final void accept(Object obj) {
        this.f5479a.onComplete();
    }
}
