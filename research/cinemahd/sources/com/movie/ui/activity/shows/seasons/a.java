package com.movie.ui.activity.shows.seasons;

import com.uwetrottmann.thetvdb.entities.EpisodesResponse;
import io.reactivex.Observable;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class a implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ a f5476a = new a();

    private /* synthetic */ a() {
    }

    public final Object apply(Object obj) {
        return Observable.fromArray((EpisodesResponse) obj);
    }
}
