package com.movie.ui.activity.shows;

import com.database.entitys.MovieEntity;
import io.reactivex.functions.Action;

/* compiled from: lambda */
public final /* synthetic */ class d implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ShowActivity f5438a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ d(ShowActivity showActivity, MovieEntity movieEntity) {
        this.f5438a = showActivity;
        this.b = movieEntity;
    }

    public final void run() {
        this.f5438a.a(this.b);
    }
}
