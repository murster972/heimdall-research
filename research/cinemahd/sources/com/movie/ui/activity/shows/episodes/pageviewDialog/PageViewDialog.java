package com.movie.ui.activity.shows.episodes.pageviewDialog;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import androidx.fragment.app.DialogFragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.database.entitys.TvWatchedEpisode;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.tmvdb.ExternalID;
import com.movie.data.model.tmvdb.SeasonTMDB;
import com.movie.data.repository.MoviesRepository;
import com.movie.data.repository.tmdb.TMDBRepositoryImpl;
import com.movie.ui.activity.SourceActivity;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.activity.shows.episodes.pageviewDialog.EpisodeDetailsFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.widget.SlidingTabLayout;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.thetvdb.entities.Episode;
import com.uwetrottmann.thetvdb.entities.EpisodesResponse;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import retrofit2.Response;

public class PageViewDialog extends DialogFragment implements EpisodeDetailsFragment.EpisodeListener {

    /* renamed from: a  reason: collision with root package name */
    OnListFragmentInteractionListener f5455a;
    @Inject
    MoviesRepository b;
    @Inject
    MvDatabase c;
    @Inject
    TMDBApi d;
    TheTvdb e = new TheTvdb("6UMSCJSYNU96S28F");
    ArrayList<EpisodeItem> f = new ArrayList<>();
    private Unbinder g;
    private CompositeDisposable h;
    private BehaviorSubject<Observable<List<EpisodeItem>>> i = BehaviorSubject.b();
    @BindView(2131296263)
    ImageButton imgBtnrevertIndex;
    private SeasonEntity j;
    private MovieEntity k;
    private EpisodePagerAdapter l;
    @BindView(2131296765)
    ProgressBar loading;
    private boolean m = true;
    @BindView(2131297096)
    SlidingTabLayout tabLayout;
    @BindView(2131297230)
    ViewPager viewPager;

    public interface OnListFragmentInteractionListener {
        void a(View view, int i, int i2);

        void b(View view, int i, int i2);
    }

    public static PageViewDialog a(String str, MovieEntity movieEntity, SeasonEntity seasonEntity, ArrayList<EpisodeItem> arrayList) {
        PageViewDialog pageViewDialog = new PageViewDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title", str);
        bundle.putParcelable("movie", movieEntity);
        bundle.putParcelable("season", seasonEntity);
        bundle.putParcelableArrayList("episodeList", arrayList);
        pageViewDialog.setArguments(bundle);
        return pageViewDialog;
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296263})
    public void OnImgBtnrevertIndexClick() {
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(this.f);
        this.f.clear();
        this.m = !this.m;
        a((List<EpisodeItem>) arrayList, true);
    }

    public /* synthetic */ void b(List list) throws Exception {
        a((List<EpisodeItem>) list, false);
    }

    public /* synthetic */ List c(List list) throws Exception {
        try {
            ArrayList<EpisodeItem> arrayList = new ArrayList<>();
            Iterator it2 = list.iterator();
            while (it2.hasNext()) {
                arrayList.add(new EpisodeItem((SeasonTMDB.EpisodesBean) it2.next(), false, this.j.b(), "TMDB"));
            }
            List<TvWatchedEpisode> a2 = this.c.o().a(this.k.getTmdbID(), this.k.getImdbIDStr(), this.k.getTraktID(), this.k.getTvdbID(), this.j.g());
            for (EpisodeItem episodeItem : arrayList) {
                for (TvWatchedEpisode b2 : a2) {
                    if (episodeItem.f5440a.intValue() == b2.b()) {
                        episodeItem.b = true;
                    }
                }
            }
            return arrayList;
        } catch (Exception unused) {
            return new ArrayList();
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getDialog().getWindow().getAttributes().windowAnimations = 2131820555;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        if (Utils.a((Activity) getActivity()) == 1) {
            getDialog().getWindow().setLayout((int) (((double) getResources().getDisplayMetrics().widthPixels) * 0.9d), (int) (((double) getResources().getDisplayMetrics().heightPixels) * 0.7d));
        } else {
            int i2 = getResources().getDisplayMetrics().heightPixels;
            getDialog().getWindow().setLayout((int) (((double) getResources().getDisplayMetrics().widthPixels) * 0.7d), i2);
        }
        setHasOptionsMenu(true);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        DaggerBaseFragmentComponent.a().a(FreeMoviesApp.a((Context) (Activity) context).d()).a().a(this);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.dialog_pageview_episode, viewGroup, false);
        this.g = ButterKnife.bind((Object) this, inflate);
        this.h = new CompositeDisposable();
        return inflate;
    }

    public void onDestroyView() {
        this.g.unbind();
        this.h.dispose();
        super.onDestroyView();
    }

    public void onPause() {
        this.h.dispose();
        super.onPause();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        getArguments().getString("title", "Enter Name");
        this.k = (MovieEntity) getArguments().getParcelable("movie");
        this.j = (SeasonEntity) getArguments().getParcelable("season");
        ArrayList parcelableArrayList = getArguments().getParcelableArrayList("episodeList");
        this.tabLayout.a(R.layout.tabstrip_item_transparent, R.id.textViewTabStripItem);
        if (parcelableArrayList == null || parcelableArrayList.size() <= 0) {
            this.h.b(Observable.concat(this.i).observeOn(AndroidSchedulers.a()).subscribe(new b(this), c.f5460a));
            this.i.onNext(((TMDBRepositoryImpl) this.b).c(this.k.getTmdbID(), this.j.g()).subscribeOn(Schedulers.b()).map(new a(this)));
            this.i.onNext(this.d.getTVExternalID(this.k.getTmdbID()).subscribeOn(Schedulers.b()).map(new d(this)));
            return;
        }
        a((List<EpisodeItem>) parcelableArrayList, false);
    }

    public void a(OnListFragmentInteractionListener onListFragmentInteractionListener) {
        this.f5455a = onListFragmentInteractionListener;
    }

    public /* synthetic */ List a(ExternalID externalID) throws Exception {
        try {
            ArrayList<EpisodeItem> arrayList = new ArrayList<>();
            Response<EpisodesResponse> execute = this.e.series().episodesQuery(externalID.getTvdb_id(), (Integer) null, Integer.valueOf(this.j.g()), (Integer) null, (Integer) null, (Double) null, (String) null, (String) null, 1, "en").execute();
            if (execute.isSuccessful()) {
                for (Episode episodeItem : execute.body().data) {
                    arrayList.add(new EpisodeItem(episodeItem, false, this.j.b(), "TVDB"));
                }
                List<TvWatchedEpisode> a2 = this.c.o().a(this.k.getTmdbID(), this.k.getImdbIDStr(), this.k.getTraktID(), this.k.getTvdbID(), this.j.g());
                for (EpisodeItem episodeItem2 : arrayList) {
                    for (TvWatchedEpisode b2 : a2) {
                        if (episodeItem2.f5440a.intValue() == b2.b()) {
                            episodeItem2.b = true;
                        }
                    }
                }
            }
            return arrayList;
        } catch (Exception unused) {
            return new ArrayList();
        }
    }

    /* access modifiers changed from: package-private */
    public ArrayList<EpisodeItem> a(List<EpisodeItem> list) {
        ArrayList<EpisodeItem> arrayList = new ArrayList<>();
        for (EpisodeItem next : list) {
            if (next.h) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    private void a(List<EpisodeItem> list, boolean z) {
        boolean z2;
        ArrayList<EpisodeItem> arrayList = list;
        if (FreeMoviesApp.l().getBoolean("pref_show_aried_eps_only2", true)) {
            arrayList = a(list);
        }
        this.imgBtnrevertIndex.setVisibility(0);
        if (this.m) {
            Collections.sort(arrayList);
        } else {
            Collections.reverse(arrayList);
        }
        if (this.f.size() == 0) {
            this.f.addAll(arrayList);
            this.l = new EpisodePagerAdapter(getActivity(), getChildFragmentManager(), this.f, this.j.g());
            this.l.a((EpisodeDetailsFragment.EpisodeListener) this);
            this.viewPager.setAdapter(this.l);
            this.tabLayout.setViewPager(this.viewPager);
            this.viewPager.setCurrentItem(0, false);
        } else {
            for (EpisodeItem next : arrayList) {
                Iterator<EpisodeItem> it2 = this.f.iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (next.f5440a == it2.next().f5440a) {
                            z2 = true;
                            break;
                        }
                    } else {
                        z2 = false;
                        break;
                    }
                }
                if (!z2) {
                    this.f.add(next);
                }
            }
            this.l.notifyDataSetChanged();
        }
        int size = this.f.size() - 1;
        while (true) {
            if (size < 0) {
                size = 0;
                break;
            } else if (!this.f.get(size).b.booleanValue()) {
                size--;
            } else if (!z && this.f.size() > 0) {
                this.viewPager.setCurrentItem(size + 1);
            }
        }
        this.loading.setVisibility(8);
        this.tabLayout.setVisibility(0);
        this.viewPager.setVisibility(0);
        if (size == 0) {
            this.imgBtnrevertIndex.setFocusable(true);
            this.imgBtnrevertIndex.setFocusableInTouchMode(true);
            this.imgBtnrevertIndex.requestFocus();
        }
    }

    public void a(EpisodeItem episodeItem) {
        episodeItem.b = true;
        Intent intent = new Intent(getActivity(), SourceActivity.class);
        intent.putExtra("Movie", this.k);
        String str = (this.k.getRealeaseDate() == null || this.k.getRealeaseDate().isEmpty()) ? "" : this.k.getRealeaseDate().split("-")[0];
        MovieInfo movieInfo = new MovieInfo(this.k.getName(), str, "" + this.j.g(), "" + episodeItem.f5440a, this.j.a() == null ? "1970" : this.j.a().split("-")[0], this.k.getGenres());
        movieInfo.epsCount = episodeItem.d.intValue();
        intent.putExtra("MovieInfo", movieInfo);
        startActivity(intent);
    }

    public void a(EpisodeItem episodeItem, boolean z) {
        OnListFragmentInteractionListener onListFragmentInteractionListener = this.f5455a;
        if (onListFragmentInteractionListener == null) {
            return;
        }
        if (z) {
            onListFragmentInteractionListener.a(getView(), this.j.g(), episodeItem.f5440a.intValue());
        } else {
            onListFragmentInteractionListener.b(getView(), this.j.g(), episodeItem.f5440a.intValue());
        }
    }
}
