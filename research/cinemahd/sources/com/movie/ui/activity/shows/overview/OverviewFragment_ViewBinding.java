package com.movie.ui.activity.shows.overview;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.movie.ui.widget.AspectLockedImageView;
import com.yoku.marumovie.R;

public class OverviewFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private OverviewFragment f5463a;

    public OverviewFragment_ViewBinding(OverviewFragment overviewFragment, View view) {
        this.f5463a = overviewFragment;
        overviewFragment.tvName = (TextView) Utils.findRequiredViewAsType(view, R.id.tvName, "field 'tvName'", TextView.class);
        overviewFragment.tvtime = (TextView) Utils.findRequiredViewAsType(view, R.id.tvtime, "field 'tvtime'", TextView.class);
        overviewFragment.tvOverview = (TextView) Utils.findRequiredViewAsType(view, R.id.tvOverview, "field 'tvOverview'", TextView.class);
        overviewFragment.tvOverviewExpand = (TextView) Utils.findRequiredViewAsType(view, R.id.tvOverviewExpand, "field 'tvOverviewExpand'", TextView.class);
        overviewFragment.tvRating = (TextView) Utils.findRequiredViewAsType(view, R.id.ctRating, "field 'tvRating'", TextView.class);
        overviewFragment.movie_cover = (AspectLockedImageView) Utils.findRequiredViewAsType(view, R.id.movie_cover, "field 'movie_cover'", AspectLockedImageView.class);
        overviewFragment.content = (ConstraintLayout) Utils.findRequiredViewAsType(view, R.id.content, "field 'content'", ConstraintLayout.class);
        overviewFragment.loading = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.loading, "field 'loading'", ProgressBar.class);
        overviewFragment.adView = (FrameLayout) Utils.findRequiredViewAsType(view, R.id.adView, "field 'adView'", FrameLayout.class);
    }

    public void unbind() {
        OverviewFragment overviewFragment = this.f5463a;
        if (overviewFragment != null) {
            this.f5463a = null;
            overviewFragment.tvName = null;
            overviewFragment.tvtime = null;
            overviewFragment.tvOverview = null;
            overviewFragment.tvOverviewExpand = null;
            overviewFragment.tvRating = null;
            overviewFragment.movie_cover = null;
            overviewFragment.content = null;
            overviewFragment.loading = null;
            overviewFragment.adView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
