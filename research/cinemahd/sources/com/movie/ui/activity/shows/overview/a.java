package com.movie.ui.activity.shows.overview;

import android.view.View;
import com.uwetrottmann.thetvdb.entities.Series;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ OverviewFragment f5464a;
    private final /* synthetic */ View b;

    public /* synthetic */ a(OverviewFragment overviewFragment, View view) {
        this.f5464a = overviewFragment;
        this.b = view;
    }

    public final void accept(Object obj) {
        this.f5464a.a(this.b, (Series) obj);
    }
}
