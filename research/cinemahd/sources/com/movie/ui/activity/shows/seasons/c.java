package com.movie.ui.activity.shows.seasons;

import com.movie.ui.activity.shows.seasons.SeasonFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class c implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5478a;

    public /* synthetic */ c(ObservableEmitter observableEmitter) {
        this.f5478a = observableEmitter;
    }

    public final void accept(Object obj) {
        SeasonFragment.AnonymousClass2.a(this.f5478a, (List) obj);
    }
}
