package com.movie.ui.activity.shows.seasons;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.ads.videoreward.AdsManager;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.SeasonEntity;
import com.database.entitys.TvWatchedEpisode;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.activity.shows.episodes.EpisodeItem;
import com.movie.ui.customdialog.AddMagnetDialog;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.widget.AnimatorStateView;
import com.original.tase.api.TraktUserApi;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.original.tase.utils.DeviceUtils;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.thetvdb.entities.Episode;
import com.uwetrottmann.thetvdb.entities.EpisodesResponse;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import javax.inject.Inject;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import retrofit2.Response;

public class SeasonFragment extends BaseFragment {
    private int c = 1;
    /* access modifiers changed from: private */
    public MovieEntity d;
    private OnListFragmentInteractionListener e;
    GridLayoutManager f;
    @Inject
    MoviesRepository g;
    @Inject
    TheTvdb h;
    @Inject
    MvDatabase i;
    @Inject
    TMDBApi j;
    @Inject
    MoviesApi k;
    private boolean l = false;
    /* access modifiers changed from: private */
    public CompositeDisposable m;
    /* access modifiers changed from: private */
    public List<SeasonEntity> n;
    /* access modifiers changed from: private */
    public List<TvWatchedEpisode> o;
    private ArrayList<EpisodeItem> p;
    @BindView(2131296768)
    ProgressBar progressBar;
    private boolean q = false;
    @BindView(2131296755)
    RecyclerView recyclerView;
    @BindView(2131297224)
    AnimatorStateView viewEmty;

    public interface OnListFragmentInteractionListener {
        void a(SeasonEntity seasonEntity, ArrayList<EpisodeItem> arrayList);

        void a(String str);

        void b(String str);
    }

    public /* synthetic */ void d(String str) throws Exception {
        if (this.recyclerView.getAdapter() == null) {
            this.recyclerView.setAdapter(new SeasonRecyclerViewAdapter(this.n, this.o, this.e));
        } else {
            SeasonRecyclerViewAdapter seasonRecyclerViewAdapter = (SeasonRecyclerViewAdapter) this.recyclerView.getAdapter();
            seasonRecyclerViewAdapter.a(this.p);
            seasonRecyclerViewAdapter.a(this.n);
            seasonRecyclerViewAdapter.b(this.o);
        }
        this.recyclerView.getAdapter().notifyDataSetChanged();
        this.e.b(str);
    }

    public /* synthetic */ void e(String str) throws Exception {
        this.recyclerView.setAdapter(new SeasonRecyclerViewAdapter(this.n, this.o, this.p, this.e));
        this.recyclerView.getAdapter().notifyDataSetChanged();
        this.e.a(str);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            this.e = (OnListFragmentInteractionListener) context;
            return;
        }
        throw new RuntimeException(context.toString() + " must implement OnListFragmentInteractionListener");
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        int i2 = configuration.orientation;
        if (i2 == 2) {
            this.f.c(this.c * 2);
        } else if (i2 == 1) {
            this.f.c(this.c);
        }
    }

    public boolean onContextItemSelected(final MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case 121:
                this.m.b(Observable.create(new ObservableOnSubscribe<String>() {
                    public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                        try {
                            ArrayList arrayList = new ArrayList();
                            for (int i = 1; i <= ((SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId())).b(); i++) {
                                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                                tvWatchedEpisode.a(i);
                                tvWatchedEpisode.c(((SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId())).g());
                                tvWatchedEpisode.c(SeasonFragment.this.d.getTmdbID());
                                tvWatchedEpisode.a(SeasonFragment.this.d.getImdbIDStr());
                                tvWatchedEpisode.e(SeasonFragment.this.d.getTvdbID());
                                tvWatchedEpisode.d(SeasonFragment.this.d.getTraktID());
                                DateTimeHelper.a(DateTimeHelper.d(((SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId())).a()));
                                arrayList.add(tvWatchedEpisode);
                            }
                            SeasonFragment.this.d.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
                            SeasonFragment.this.i.m().a(SeasonFragment.this.d);
                            SeasonFragment.this.i.o().b((TvWatchedEpisode[]) arrayList.toArray(new TvWatchedEpisode[arrayList.size()]));
                            List unused = SeasonFragment.this.o = SeasonFragment.this.i.o().a(SeasonFragment.this.d.getTmdbID(), SeasonFragment.this.d.getImdbIDStr(), SeasonFragment.this.d.getTraktID(), SeasonFragment.this.d.getTvdbID());
                            observableEmitter.onNext("Add season to history success");
                        } catch (Exception unused2) {
                            observableEmitter.onNext("Add season to history fail");
                        }
                        try {
                            if (TraktCredentialsHelper.b().isValid()) {
                                TraktUserApi.f().a(SeasonFragment.this.d, (SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId()), true);
                            }
                            observableEmitter.onNext("Add season to trakt success");
                        } catch (Exception unused3) {
                            observableEmitter.onNext("Add season to trakt fail");
                        }
                        observableEmitter.onComplete();
                    }
                }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new f(this), new n(this)));
                return true;
            case 122:
                this.m.b(Observable.create(new ObservableOnSubscribe<String>() {
                    public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                        try {
                            ArrayList arrayList = new ArrayList();
                            for (int i = 1; i <= ((SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId())).b(); i++) {
                                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                                tvWatchedEpisode.a(i);
                                tvWatchedEpisode.c(((SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId())).g());
                                tvWatchedEpisode.c(SeasonFragment.this.d.getTmdbID());
                                tvWatchedEpisode.a(SeasonFragment.this.d.getImdbIDStr());
                                tvWatchedEpisode.e(SeasonFragment.this.d.getTvdbID());
                                tvWatchedEpisode.d(SeasonFragment.this.d.getTraktID());
                                arrayList.add(tvWatchedEpisode);
                            }
                            SeasonFragment.this.i.o().a((TvWatchedEpisode[]) arrayList.toArray(new TvWatchedEpisode[arrayList.size()]));
                            List<TvWatchedEpisode> a2 = SeasonFragment.this.i.o().a(SeasonFragment.this.d.getTmdbID(), SeasonFragment.this.d.getImdbIDStr(), SeasonFragment.this.d.getTraktID(), SeasonFragment.this.d.getTvdbID());
                            if (a2 == null || a2.size() == 0) {
                                SeasonFragment.this.d.setWatched_at((OffsetDateTime) null);
                                SeasonFragment.this.i.m().b(SeasonFragment.this.d);
                            }
                            List unused = SeasonFragment.this.o = SeasonFragment.this.i.o().a(SeasonFragment.this.d.getTmdbID(), SeasonFragment.this.d.getImdbIDStr(), SeasonFragment.this.d.getTraktID(), SeasonFragment.this.d.getTvdbID());
                            observableEmitter.onNext("Remove season from history success");
                        } catch (Exception unused2) {
                            observableEmitter.onNext("Remove season from history fail");
                        }
                        try {
                            if (TraktCredentialsHelper.b().isValid()) {
                                TraktUserApi.f().a(SeasonFragment.this.d, (SeasonEntity) SeasonFragment.this.n.get(menuItem.getGroupId()), false);
                            }
                            observableEmitter.onNext("Remove season from trakt success");
                        } catch (Exception unused3) {
                            observableEmitter.onNext("Remove season from trakt fail");
                        }
                        observableEmitter.onComplete();
                    }
                }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new j(this), new k(this)));
                return true;
            case 123:
                break;
            case 124:
                Utils.a((Activity) getActivity(), "comming soon!!!");
                break;
            default:
                return super.onContextItemSelected(menuItem);
        }
        return true;
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
        this.q = FreeMoviesApp.l().getBoolean("pre_season_inc_sort", false);
        this.l = FreeMoviesApp.l().getBoolean("pre_force_tv_db", false);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_season_fragment, menu);
        MenuItem findItem = menu.findItem(R.id.forceTVDB);
        if (this.l) {
            findItem.setIcon(getResources().getDrawable(R.drawable.tvdb_focus));
        } else {
            findItem.setIcon(getResources().getDrawable(R.drawable.tvdb));
        }
        menu.findItem(R.id.magnet).setVisible(RealDebridCredentialsHelper.c().isValid());
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_season_list, viewGroup, false);
    }

    public void onDestroy() {
        this.m.dispose();
        super.onDestroy();
    }

    public void onDetach() {
        super.onDetach();
        this.e = null;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.resorting) {
            this.q = !this.q;
            List<SeasonEntity> list = this.n;
            if (list != null) {
                if (this.q) {
                    Collections.sort(list);
                } else {
                    Collections.reverse(list);
                }
                this.recyclerView.getAdapter().notifyDataSetChanged();
            }
            FreeMoviesApp.l().edit().putBoolean("pre_season_inc_sort", this.q).apply();
            return true;
        } else if (menuItem.getItemId() == R.id.forceTVDB) {
            this.l = !this.l;
            if (this.l) {
                menuItem.setIcon(getResources().getDrawable(R.drawable.tvdb_focus));
            } else {
                menuItem.setIcon(getResources().getDrawable(R.drawable.tvdb));
            }
            List<SeasonEntity> list2 = this.n;
            if (list2 != null) {
                list2.clear();
            }
            ArrayList<EpisodeItem> arrayList = this.p;
            if (arrayList != null) {
                arrayList.clear();
            }
            List<TvWatchedEpisode> list3 = this.o;
            if (list3 != null) {
                list3.clear();
            }
            a(this.d, this.l);
            FreeMoviesApp.l().edit().putBoolean("pre_force_tv_db", this.l).apply();
            return true;
        } else {
            if (menuItem.getItemId() == R.id.magnet) {
                AddMagnetDialog a2 = AddMagnetDialog.a(this.d, (MovieInfo) null);
                FragmentTransaction b = getChildFragmentManager().b();
                Fragment b2 = getChildFragmentManager().b("fragment_add_magnet");
                if (b2 != null) {
                    b.a(b2);
                }
                b.a((String) null);
                a2.show(b, "fragment_add_magnet");
            }
            return super.onOptionsItemSelected(menuItem);
        }
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.f = new GridLayoutManager(view.getContext(), this.c);
        int a2 = Utils.a((Activity) getActivity());
        if (a2 == 2) {
            this.f.c(this.c * 2);
        } else if (a2 == 1) {
            this.f.c(this.c);
        }
        this.recyclerView.setLayoutManager(this.f);
        this.m = new CompositeDisposable();
        this.d = (MovieEntity) getArguments().getParcelable("arg_movie");
        a(this.d, this.l);
        if (!DeviceUtils.b() && !DeviceUtils.a()) {
            AdsManager.l().a((ViewGroup) view.findViewById(R.id.adView));
        }
    }

    private Observable<EpisodesResponse> b(int i2, final int i3) {
        return Observable.create(new ObservableOnSubscribe<EpisodesResponse>() {
            public void subscribe(ObservableEmitter<EpisodesResponse> observableEmitter) throws Exception {
                Response<EpisodesResponse> execute = SeasonFragment.this.h.series().episodes((int) SeasonFragment.this.d.getTvdbID(), Integer.valueOf(i3), "en").execute();
                if (execute.isSuccessful()) {
                    observableEmitter.onNext(execute.body());
                }
                observableEmitter.onComplete();
            }
        }).concatMap(new e(this, i3, i2));
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        this.viewEmty.setVisibility(0);
        this.viewEmty.setMessageText(th.getMessage());
    }

    public static SeasonFragment a(int i2, MovieEntity movieEntity) {
        SeasonFragment seasonFragment = new SeasonFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("arg_movie", movieEntity);
        seasonFragment.c = i2;
        seasonFragment.setArguments(bundle);
        return seasonFragment;
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        this.e.a(th.getMessage());
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ ObservableSource a(int i2, int i3, EpisodesResponse episodesResponse) throws Exception {
        if (episodesResponse.links.last.intValue() > i2) {
            return Observable.just(episodesResponse).concatWith(b(i3, episodesResponse.links.next.intValue()));
        }
        return Observable.just(episodesResponse);
    }

    private void a(final MovieEntity movieEntity, final boolean z) {
        this.progressBar.setVisibility(0);
        this.m.b(Observable.create(new ObservableOnSubscribe<List<SeasonEntity>>() {
            static /* synthetic */ void a(ObservableEmitter observableEmitter, List list) throws Exception {
                if (list.size() > 0) {
                    observableEmitter.onNext(list);
                }
                observableEmitter.onComplete();
            }

            public void subscribe(ObservableEmitter<List<SeasonEntity>> observableEmitter) throws Exception {
                SeasonFragment seasonFragment = SeasonFragment.this;
                List unused = seasonFragment.o = seasonFragment.i.o().a(movieEntity.getTmdbID(), movieEntity.getImdbIDStr(), movieEntity.getTraktID(), movieEntity.getTvdbID());
                if (z) {
                    observableEmitter.onComplete();
                } else {
                    SeasonFragment.this.m.b(SeasonFragment.this.g.discoverSession(movieEntity.getTmdbID()).subscribeOn(Schedulers.b()).subscribe(new c(observableEmitter), new d(observableEmitter)));
                }
            }
        }).subscribeOn(Schedulers.b()).switchIfEmpty(new i(this, movieEntity)).observeOn(AndroidSchedulers.a()).subscribe(new b(this), new m(this)));
    }

    public /* synthetic */ void a(MovieEntity movieEntity, Observer observer) {
        this.m.b(b((int) movieEntity.getTvdbID(), 1).observeOn(Schedulers.b()).concatMap(a.f5476a).subscribe(new h(this), new l(this, observer), new g(this, observer)));
    }

    public /* synthetic */ void a(EpisodesResponse episodesResponse) throws Exception {
        new ArrayList();
        if (this.p == null) {
            this.p = new ArrayList<>();
        }
        if (this.n == null) {
            this.n = new ArrayList();
        }
        for (Episode next : episodesResponse.data) {
            TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
            tvWatchedEpisode.a(next.airedEpisodeNumber.intValue());
            tvWatchedEpisode.c(next.airedSeason.intValue());
            boolean z = false;
            boolean z2 = false;
            for (TvWatchedEpisode next2 : this.o) {
                if (next2.e() == next.airedSeason.intValue() && next2.b() == next.airedEpisodeNumber.intValue()) {
                    z2 = true;
                }
            }
            Integer num = next.airedEpisodeNumber;
            Boolean valueOf = Boolean.valueOf(z2);
            String str = next.episodeName;
            Integer num2 = next.airedEpisodeNumber;
            this.p.add(new EpisodeItem(num, valueOf, str, num2, "http://thetvdb.com/banners/" + next.filename, next.overview, true, "TVDB", next.airedSeason, next.firstAired));
            for (SeasonEntity next3 : this.n) {
                if (next3.c() == next.airedSeason.intValue()) {
                    next3.a(next3.b() + 1);
                    z = true;
                }
            }
            if (!z) {
                SeasonEntity seasonEntity = new SeasonEntity();
                seasonEntity.b(next.airedSeason.intValue());
                seasonEntity.c(next.airedSeason.intValue());
                seasonEntity.b("Season " + next.airedSeason);
                seasonEntity.c("unknow");
                seasonEntity.a(1);
                this.n.add(seasonEntity);
            }
        }
    }

    public /* synthetic */ void a(Observer observer, Throwable th) throws Exception {
        observer.onNext(this.n);
    }

    public /* synthetic */ void a(Observer observer) throws Exception {
        observer.onNext(this.n);
    }

    public /* synthetic */ void a(List list) throws Exception {
        this.n = list;
        this.progressBar.setVisibility(8);
        Iterator it2 = list.iterator();
        Boolean valueOf = Boolean.valueOf(FreeMoviesApp.l().getBoolean("pref_show_special_season", false));
        while (it2.hasNext()) {
            if (((SeasonEntity) it2.next()).g() < 1 && !valueOf.booleanValue()) {
                it2.remove();
            }
        }
        if (this.q) {
            Collections.sort(this.n);
        } else {
            Collections.reverse(this.n);
        }
        this.recyclerView.setAdapter(new SeasonRecyclerViewAdapter(this.n, this.o, this.p, this.e));
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        this.e.b(th.getMessage());
    }
}
