package com.movie.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import com.google.android.gms.ads.AdRequest;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.movie.AppComponent;
import com.movie.ui.widget.AspectLockedImageView;
import com.yoku.marumovie.R;

public class BarcodeActivity extends BaseActivity {
    @BindView(2131296700)
    AspectLockedImageView imgbarcode;
    @BindView(2131297158)
    Toolbar toolbar;
    @BindView(2131297194)
    TextView tvText;

    public /* synthetic */ void a(View view) {
        finish();
    }

    public void c(String str) {
        try {
            BitMatrix a2 = new QRCodeWriter().a(str, BarcodeFormat.QR_CODE, (int) AdRequest.MAX_CONTENT_URL_LENGTH, (int) AdRequest.MAX_CONTENT_URL_LENGTH);
            int b = a2.b();
            int a3 = a2.a();
            Bitmap createBitmap = Bitmap.createBitmap(b, a3, Bitmap.Config.RGB_565);
            for (int i = 0; i < b; i++) {
                for (int i2 = 0; i2 < a3; i2++) {
                    createBitmap.setPixel(i, i2, a2.a(i, i2) ? -16777216 : -1);
                }
            }
            this.imgbarcode.setImageBitmap(createBitmap);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_barcode);
        setSupportActionBar(this.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.e(true);
            this.toolbar.setTitle((CharSequence) "Barcode");
            this.toolbar.setNavigationOnClickListener(new b(this));
        }
        Bundle extras = getIntent().getExtras();
        String string = extras.getString("extra_barcode");
        String string2 = extras.getString("extra_barcode");
        if (string2 == null || string2.isEmpty()) {
            this.tvText.setText(string);
        } else {
            this.tvText.setText(string2);
        }
        c(string);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }
}
