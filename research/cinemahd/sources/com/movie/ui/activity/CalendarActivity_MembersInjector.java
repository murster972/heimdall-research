package com.movie.ui.activity;

import com.database.MvDatabase;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.api.tvmaze.TVMazeApi;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.MembersInjector;

public final class CalendarActivity_MembersInjector implements MembersInjector<CalendarActivity> {
    public static void a(CalendarActivity calendarActivity, TVMazeApi tVMazeApi) {
        calendarActivity.c = tVMazeApi;
    }

    public static void a(CalendarActivity calendarActivity, MvDatabase mvDatabase) {
        calendarActivity.d = mvDatabase;
    }

    public static void a(CalendarActivity calendarActivity, TMDBApi tMDBApi) {
        calendarActivity.e = tMDBApi;
    }

    public static void a(CalendarActivity calendarActivity, IMDBApi iMDBApi) {
        calendarActivity.f = iMDBApi;
    }

    public static void a(CalendarActivity calendarActivity, TheTvdb theTvdb) {
        calendarActivity.g = theTvdb;
    }

    public static void a(CalendarActivity calendarActivity, MoviesHelper moviesHelper) {
        calendarActivity.h = moviesHelper;
    }
}
