package com.movie.ui.activity;

import com.movie.data.model.payment.bitcoin.BitcoinAddressResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class z implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MemberActivationActivity f5514a;

    public /* synthetic */ z(MemberActivationActivity memberActivationActivity) {
        this.f5514a = memberActivationActivity;
    }

    public final void accept(Object obj) {
        this.f5514a.a((BitcoinAddressResponse) obj);
    }
}
