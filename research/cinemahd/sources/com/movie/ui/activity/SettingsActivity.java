package com.movie.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.CheckBoxPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceCategory;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;
import androidx.preference.SeekBarPreference;
import androidx.recyclerview.widget.RecyclerView;
import com.Setting;
import com.ads.videoreward.AdsManager;
import com.database.MvDatabase;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.model.Categorys;
import com.movie.ui.activity.settings.CategoryRetrictionDialog;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.api.TraktUserApi;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.alldebrid.AllDebridUserApi;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeUserApi;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.event.ApiDebridGetTokenFailedEvent;
import com.original.tase.event.ApiDebridGetTokenSuccessEvent;
import com.original.tase.event.ApiDebridWaitingToVerifyEvent;
import com.original.tase.event.trakt.TraktGetTokenFailedEvent;
import com.original.tase.event.trakt.TraktGetTokenSuccessEvent;
import com.original.tase.event.trakt.TraktWaitingToVerifyEvent;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.original.tase.model.debrid.alldebrid.ADUserInfor;
import com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo;
import com.original.tase.model.debrid.realdebrid.RealDebridCheckAuthResult;
import com.original.tase.model.debrid.realdebrid.RealDebridGetDeviceCodeResult;
import com.original.tase.model.debrid.realdebrid.RealDebridGetTokenResult;
import com.original.tase.model.debrid.realdebrid.RealDebridUserInfor;
import com.original.tase.model.socket.UserPlayerPluginInfo;
import com.original.tase.socket.Client;
import com.original.tase.utils.NetworkUtils;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.PrefUtils;
import com.utils.Subtitle.services.LanguageId;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;
import javax.inject.Inject;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class SettingsActivity extends BaseActivity {
    public static int e = 1122;
    public static int f = 1123;
    public static String g = "";
    public static Boolean h;
    public static Boolean i;

    /* renamed from: a  reason: collision with root package name */
    CompositeDisposable f5125a = new CompositeDisposable();
    @Inject
    MvDatabase b;
    @Inject
    RealDebridApi c;
    @Inject
    MoviesApi d;

    public static void a(Context context) {
        Setting.a(context);
    }

    static /* synthetic */ void a(Void voidR) throws Exception {
    }

    public /* synthetic */ void c() throws Exception {
        Utils.a((Activity) this, "Restore successful");
    }

    public Intent getParentActivityIntent() {
        return super.getParentActivityIntent().addFlags(67108864);
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, final Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i2 == f && i3 == -1) {
            this.f5125a.b(Observable.create(new ObservableOnSubscribe<Void>() {
                public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                    File a2 = com.nononsenseapps.filepicker.Utils.a(intent.getData());
                    Logger.a("Setting", "XML path Uri (" + a2.toString() + ")");
                    String name = a2.getName();
                    MvDatabase.b(SettingsActivity.this, name);
                    PrefUtils.a((Context) SettingsActivity.this, name);
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(m1.f5263a, new n1(this), new o1(this)));
        } else if (i2 == e && i3 == -1) {
            File a2 = com.nononsenseapps.filepicker.Utils.a(intent.getData());
            Logger.a("Setting", "XML path Uri (" + a2.toString() + ")");
            String file = a2.toString();
            SettingsFragment.pref_dowload_path.setSummary((CharSequence) file);
            FreeMoviesApp.l().edit().putString(SettingsFragment.pref_dowload_path.getKey(), file).apply();
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_settings);
        AdsManager.l().a((ViewGroup) findViewById(R.id.adView));
        this.needToCancelHttpHelper = false;
        Toolbar toolbar = this.mToolbar;
        if (toolbar != null) {
            ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
            this.mToolbar.setNavigationOnClickListener(new w0(this));
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.d(true);
                supportActionBar.c((int) R.string.title_settings);
                supportActionBar.f(true);
            }
        }
        SettingsFragment settingsFragment = new SettingsFragment();
        settingsFragment.setMvDatabase(this.b);
        settingsFragment.setRealDebridApi(this.c);
        if (bundle == null) {
            getSupportFragmentManager().b().a((int) R.id.container, (Fragment) settingsFragment).a();
        }
    }

    public void onDestroy() {
        this.f5125a.dispose();
        super.onDestroy();
    }

    public void onPause() {
        super.onPause();
    }

    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ void a(View view) {
        finish();
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) this, "Restore failed : " + th.getMessage());
    }

    public static class SettingsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener, Preference.OnPreferenceClickListener, Preference.OnPreferenceChangeListener, BasePlayerHelper.OnChoosePlayerListener {
        public static CheckBoxPreference pre_show_my_calenda;
        public static Preference pref_auth_All_debrid;
        public static Preference pref_auth_premiumize_debrid;
        public static Preference pref_auth_real_debrid;
        public static Preference pref_auth_trakt_tv;
        public static CheckBoxPreference pref_auto_backup_when_exit_app;
        public static CheckBoxPreference pref_auto_next_eps;
        public static Preference pref_auto_next_eps_number_of_link;
        public static CheckBoxPreference pref_auto_next_with_fisrt_sub;
        public static Preference pref_backup;
        public static Preference pref_category_restriction;
        public static Preference pref_cc_subs_font_color;
        public static Preference pref_cc_subs_font_scale;
        public static Preference pref_cc_unlock_full_version;
        public static CheckBoxPreference pref_change_bg_color;
        public static Preference pref_changelog;
        public static Preference pref_choose_default_action;
        public static Preference pref_choose_default_player;
        public static Preference pref_choose_default_tab;
        public static Preference pref_choose_host_priority3;
        public static Preference pref_choose_provider_enabled2;
        public static Preference pref_clear_cache;
        public static Preference pref_column_in_main;
        public static Preference pref_dowload_path;
        public static CheckBoxPreference pref_filter_cam;
        public static CheckBoxPreference pref_force_tv_mode;
        public static CheckBoxPreference pref_keep_alive;
        public static CheckBoxPreference pref_low_profilev2;
        public static SeekBarPreference pref_mark_saving_percent;
        public static CheckBoxPreference pref_off_premium_resolve4;
        public static SeekBarPreference pref_rd_request_per_second;
        public static Preference pref_recoptchar;
        public static Preference pref_restore;
        public static CheckBoxPreference pref_restrict_search;
        public static Preference pref_set_password;
        public static CheckBoxPreference pref_show_aried_eps_only2;
        public static CheckBoxPreference pref_show_debrid_only;
        public static CheckBoxPreference pref_show_hd_only;
        public static CheckBoxPreference pref_show_sort_link_by_quality;
        public static CheckBoxPreference pref_show_sort_link_by_size2;
        public static CheckBoxPreference pref_show_special_season;
        public static Preference pref_sub_language_international_v3;
        public static Preference pref_use_player_plugin;
        CompositeDisposable compositeDisposable = new CompositeDisposable();
        ProgressDialog dialog = null;
        protected CompositeDisposable mSubscriptions;
        MvDatabase mvDatabase;
        RealDebridApi realDebridApi;
        public int reconnect = 0;

        static /* synthetic */ void a(Void voidR) throws Exception {
        }

        static /* synthetic */ void b(Void voidR) throws Exception {
        }

        private void checkAllDebird() {
            if (!NetworkUtils.a()) {
                Utils.a((Activity) getActivity(), I18N.a(R.string.no_internet));
            } else if (!AllDebridCredentialsHelper.b().isValid()) {
                Utils.a((Activity) getActivity(), I18N.a(R.string.msg_wait));
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.a((CharSequence) "Do you want logout to All-Debrid?");
                final AlertDialog a2 = builder.a();
                a2.a(-1, "ok", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AllDebridCredentialsHelper.a();
                        CookieManager.getInstance().removeAllCookie();
                        SettingsFragment.pref_auth_All_debrid.setSummary((CharSequence) "All-Debird");
                        FreeMoviesApp.l().edit().putString("pref_ad_expiration", "").commit();
                        SettingsFragment.pref_auth_All_debrid.setTitle((CharSequence) "Login to All-Debird");
                        a2.dismiss();
                    }
                });
                a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        a2.dismiss();
                    }
                });
                a2.show();
            }
        }

        private void checkRealDebird() {
            if (!NetworkUtils.a()) {
                Utils.a((Activity) getActivity(), I18N.a(R.string.no_internet));
            } else if (!RealDebridCredentialsHelper.c().isValid()) {
                Utils.a((Activity) getActivity(), I18N.a(R.string.msg_wait));
                this.mSubscriptions.b(Observable.create(new ObservableOnSubscribe<RealDebridGetTokenResult>() {
                    public void subscribe(ObservableEmitter<RealDebridGetTokenResult> observableEmitter) throws Exception {
                        String rd_client_id = GlobalVariable.c().a().getRd_config().getRd_client_id();
                        RealDebridGetDeviceCodeResult body = SettingsFragment.this.realDebridApi.oauthDeviceCode(rd_client_id).execute().body();
                        String verification_url = body.getVerification_url();
                        String user_code = body.getUser_code();
                        String device_code = body.getDevice_code();
                        int expires_in = body.getExpires_in();
                        int interval = body.getInterval();
                        RxBus.b().a(new ApiDebridWaitingToVerifyEvent(verification_url, user_code));
                        int i = 0;
                        while (true) {
                            if (i >= expires_in || SettingsFragment.this.mSubscriptions.isDisposed()) {
                                break;
                            }
                            try {
                                Thread.sleep(1000);
                                if (((float) i) % ((float) interval) == 0.0f) {
                                    RealDebridCheckAuthResult body2 = SettingsFragment.this.realDebridApi.oauthDeviceCredentials(rd_client_id, device_code).execute().body();
                                    try {
                                        String client_id = body2.getClient_id();
                                        String client_secret = body2.getClient_secret();
                                        if (client_id != null && client_secret != null && !client_id.isEmpty() && !client_secret.isEmpty()) {
                                            HashMap hashMap = new HashMap();
                                            hashMap.put("client_id", client_id);
                                            hashMap.put("client_secret", client_secret);
                                            hashMap.put("code", device_code);
                                            hashMap.put("grant_type", "http://oauth.net/grant_type/device/1.0");
                                            RealDebridGetTokenResult body3 = SettingsFragment.this.realDebridApi.oauthtoken(hashMap).execute().body();
                                            if (body3 != null) {
                                                body3.setLast_clientID(client_id);
                                                body3.setLast_clientSecret(client_secret);
                                                observableEmitter.onNext(body3);
                                                break;
                                            }
                                        }
                                    } catch (Exception e) {
                                        Logger.a((Throwable) e, new boolean[0]);
                                    }
                                }
                            } catch (Exception e2) {
                                Logger.a((Throwable) e2, new boolean[0]);
                            }
                            i++;
                        }
                        observableEmitter.onComplete();
                    }
                }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new j1(this), h1.f5243a));
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.a((CharSequence) "Do you want logout to Real-Debrid?");
                final AlertDialog a2 = builder.a();
                a2.a(-1, "ok", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RealDebridCredentialsHelper.a();
                        CookieManager.getInstance().removeAllCookie();
                        HttpHelper.e().c("https://api.real-debrid.com", "__beaconTrackerID=; __gacid=;");
                        SettingsFragment.pref_auth_real_debrid.setSummary((CharSequence) "Real-Debird");
                        FreeMoviesApp.l().edit().putString("pref_rd_expiration", "").commit();
                        SettingsFragment.pref_auth_real_debrid.setTitle((CharSequence) "Login to Real-Debird");
                        FreeMoviesApp.l().edit().putBoolean("pref_show_debrid_only", false).apply();
                        SettingsFragment.pref_show_debrid_only.setChecked(false);
                        SettingsFragment.pref_show_debrid_only.setEnabled(false);
                        SettingsFragment.pref_off_premium_resolve4.setEnabled(false);
                        SettingsFragment.pref_off_premium_resolve4.setChecked(false);
                        SettingsFragment.pref_rd_request_per_second.setEnabled(true);
                        a2.dismiss();
                    }
                });
                a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        a2.dismiss();
                    }
                });
                a2.show();
            }
        }

        private void checktrakltv() {
            if (!NetworkUtils.a()) {
                Utils.a((Activity) getActivity(), I18N.a(R.string.no_internet));
            } else if (!TraktCredentialsHelper.b().isValid()) {
                Utils.a((Activity) getActivity(), I18N.a(R.string.msg_wait));
                this.mSubscriptions.b(TraktUserApi.f().e().subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new Consumer<Boolean>() {
                    /* renamed from: a */
                    public void accept(Boolean bool) throws Exception {
                        boolean isValid = TraktCredentialsHelper.b().isValid();
                        if (isValid) {
                            SettingsFragment.pref_auth_trakt_tv.setSummary((CharSequence) !isValid ? "Trakt-Tv" : "Trakt-Tv authorized");
                            SettingsFragment.pre_show_my_calenda.setEnabled(true);
                            SettingsFragment.pre_show_my_calenda.setChecked(true);
                            FreeMoviesApp.l().edit().putBoolean("pre_show_my_calenda_shows_only", true).apply();
                        }
                        b(Boolean.valueOf(isValid));
                    }

                    public void b(Boolean bool) {
                        RxBus.b().a(bool.booleanValue() ? new TraktGetTokenSuccessEvent() : new TraktGetTokenFailedEvent());
                        TraktUserApi.f().a(FreeMoviesApp.a((Context) SettingsFragment.this.getActivity()).e(), (Activity) SettingsFragment.this.getActivity(), SettingsFragment.this.mvDatabase);
                    }
                }, e1.f5222a));
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.a((CharSequence) "Do you want logout to Trakt-TV?");
                final AlertDialog a2 = builder.a();
                a2.a(-1, "ok", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        TraktCredentialsHelper.a();
                        CookieManager.getInstance().removeAllCookie();
                        HttpHelper.e().c("https://api.trakt.tv", "__beaconTrackerID=; __gacid=;");
                        SettingsFragment.pref_auth_trakt_tv.setSummary((CharSequence) "Trakt-Tv (Unauthorize)");
                        SettingsFragment.pref_auth_trakt_tv.setTitle((CharSequence) "Login to Trakt TV");
                        a2.dismiss();
                    }
                });
                a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        a2.dismiss();
                    }
                });
                a2.show();
            }
        }

        static /* synthetic */ void e(Throwable th) throws Exception {
        }

        static /* synthetic */ void f(Throwable th) throws Exception {
        }

        static /* synthetic */ void g(Throwable th) throws Exception {
        }

        public static void getaduserInfor() {
            String str;
            AllDebridCredentialsInfo b = AllDebridCredentialsHelper.b();
            if (b.isValid()) {
                try {
                    str = "https://api.alldebrid.com/v4/user?agent=" + URLEncoder.encode(Utils.k, "UTF-8") + "&apikey=" + URLEncoder.encode(b.getApikey(), "UTF-8");
                } catch (UnsupportedEncodingException unused) {
                    str = "https://api.alldebrid.com/v4/user?agent=" + Utils.k + "&apikey=" + b.getApikey();
                }
                new OkHttpClient().newCall(new Request.Builder().url(str).build()).enqueue(new Callback() {
                    public void onFailure(Call call, IOException iOException) {
                    }

                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            String string = response.body().string();
                            if (!string.isEmpty()) {
                                ADUserInfor aDUserInfor = (ADUserInfor) new Gson().a(string, ADUserInfor.class);
                                Logger.a("All Debrid ", aDUserInfor.toString());
                                if (aDUserInfor != null && aDUserInfor.getStatus().contains("success")) {
                                    String c = DateTimeHelper.c(aDUserInfor.getData().getUser().getPremiumUntil());
                                    FreeMoviesApp.l().edit().putBoolean("pref_alldebrid_type", aDUserInfor.getData().getUser().isIsPremium()).commit();
                                    FreeMoviesApp.l().edit().putString("pref_alldebrid_expiration_str", aDUserInfor.getData().getUser().getPremiumUntil()).commit();
                                    if (aDUserInfor.getData().getUser().isIsPremium()) {
                                        SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
                                        edit.putString("pref_ad_expiration", "\nUsername : " + aDUserInfor.getData().getUser().getUsername() + "\nType : Premium \nExpiration : " + c).commit();
                                    } else {
                                        SharedPreferences.Editor edit2 = FreeMoviesApp.l().edit();
                                        edit2.putString("pref_ad_expiration", "\nUsername : " + aDUserInfor.getData().getUser().getUsername() + "\nType : Free").commit();
                                    }
                                    AllDebridUserApi.d().a();
                                }
                            }
                        } catch (Throwable unused) {
                        }
                    }
                });
            }
        }

        static /* synthetic */ void h(Throwable th) throws Exception {
        }

        private void m32399() {
            Set<String> stringSet = FreeMoviesApp.l().getStringSet("pref_sub_language_international_v3", new HashSet(Arrays.asList(new String[]{Locale.getDefault().getLanguage()})));
            final String[] a2 = LanguageId.c().a();
            String[] b = LanguageId.c().b();
            final boolean[] zArr = new boolean[a2.length];
            for (int i = 0; i < a2.length; i++) {
                zArr[i] = false;
                Iterator<String> it2 = stringSet.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        break;
                    }
                    if (a2[i].equals(it2.next())) {
                        zArr[i] = true;
                        break;
                    }
                }
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) I18N.a(R.string.pref_subtitle));
            builder.a((CharSequence[]) b, zArr, (DialogInterface.OnMultiChoiceClickListener) new DialogInterface.OnMultiChoiceClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                    zArr[i] = z;
                }
            });
            builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    HashSet hashSet = new HashSet();
                    int i2 = 0;
                    while (true) {
                        boolean[] zArr = zArr;
                        if (i2 < zArr.length) {
                            if (zArr[i2]) {
                                hashSet.add(a2[i2].toString());
                            }
                            i2++;
                        } else {
                            FreeMoviesApp.l().edit().putStringSet("pref_sub_language_international_v3", hashSet).apply();
                            dialogInterface.dismiss();
                            return;
                        }
                    }
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.c();
        }

        private void showColorPicker() {
            FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF");
            int parseColor = Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF"));
            ColorPickerDialogBuilder a2 = ColorPickerDialogBuilder.a((Context) getActivity());
            a2.a("Choose subtitle color");
            a2.b(parseColor);
            a2.a(true);
            a2.a(ColorPickerView.WHEEL_TYPE.FLOWER);
            a2.a(12);
            a2.a((OnColorSelectedListener) new OnColorSelectedListener() {
                public void a(int i) {
                    FragmentActivity activity = SettingsFragment.this.getActivity();
                    Utils.a((Activity) activity, "onColorSelected: 0x" + Integer.toHexString(i));
                }
            });
            a2.a((CharSequence) "ok", (ColorPickerClickListener) new ColorPickerClickListener() {
                public void a(DialogInterface dialogInterface, int i, Integer[] numArr) {
                    String hexString = Integer.toHexString(i);
                    Utils.a((Activity) SettingsFragment.this.getActivity(), "onColorSelected: 0x" + hexString);
                    String str = "#" + hexString.toUpperCase();
                    SettingsFragment.pref_cc_subs_font_color.setSummary((CharSequence) str);
                    FreeMoviesApp.l().edit().putString(SettingsFragment.pref_cc_subs_font_color.getKey(), str).apply();
                }
            });
            a2.a((CharSequence) "cancel", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            a2.b().show();
        }

        private void showUIInputIP() {
            if (FreeMoviesApp.l().getBoolean("use_player_plugin", false)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.a((CharSequence) "Do you want disconnect to Player Plugin?");
                final AlertDialog a2 = builder.a();
                a2.a(-1, "ok", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        SettingsFragment.pref_use_player_plugin.setSummary((CharSequence) "IP : None");
                        Client.getIntance().disconnect();
                        Client.getIntance().disconnectall();
                        FreeMoviesApp.l().edit().putBoolean("use_player_plugin", false).apply();
                        a2.dismiss();
                    }
                });
                a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        a2.dismiss();
                    }
                });
                a2.show();
                return;
            }
            final Dialog dialog2 = new Dialog(getActivity());
            dialog2.requestWindowFeature(1);
            dialog2.setContentView(R.layout.dialog_input_ip);
            dialog2.findViewById(R.id.content_ip).setVisibility(8);
            dialog2.findViewById(R.id.loadingPanel).setVisibility(0);
            Button button = (Button) dialog2.findViewById(R.id.diag_btn_confirm_no);
            Button button2 = (Button) dialog2.findViewById(R.id.diag_btnconfirm_yes);
            String string = FreeMoviesApp.l().getString("ip_player_plugin", "");
            final TextView textView = (TextView) dialog2.findViewById(R.id.textip1);
            final TextView textView2 = (TextView) dialog2.findViewById(R.id.textip2);
            final TextView textView3 = (TextView) dialog2.findViewById(R.id.textip3);
            final TextView textView4 = (TextView) dialog2.findViewById(R.id.textip4);
            ((Button) dialog2.findViewById(R.id.diag_btn_load_confirm_no)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    dialog2.findViewById(R.id.content_ip).setVisibility(0);
                    dialog2.findViewById(R.id.loadingPanel).setVisibility(8);
                    SettingsFragment.this.mSubscriptions.a();
                }
            });
            if (!string.isEmpty()) {
                String[] split = string.split("\\.");
                textView.setText(split[0]);
                textView2.setText(split[1]);
                textView3.setText(split[2]);
                textView4.setText(split[3]);
            }
            button.setOnClickListener(new View.OnClickListener(this) {
                public void onClick(View view) {
                    dialog2.dismiss();
                }
            });
            final Dialog dialog3 = dialog2;
            button2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    SettingsFragment.this.connectPlayerPlugin(String.format("%s.%s.%s.%s", new Object[]{textView.getText(), textView2.getText(), textView3.getText(), textView4.getText()}));
                    dialog3.dismiss();
                    Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.plaese_wait));
                }
            });
            dialog2.show();
            this.mSubscriptions.b(Observable.create(new ObservableOnSubscribe<String>(this) {
                public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                    Client.getIntance().autoconnect(observableEmitter);
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new g1(this, dialog2)));
            dialog2.setOnDismissListener(new DialogInterface.OnDismissListener(this) {
                public void onDismiss(DialogInterface dialogInterface) {
                }
            });
        }

        private void showchooseprovider() {
            String string = FreeMoviesApp.l().getString("pref_choose_provider_enabled2", "");
            final ArrayList arrayList = new ArrayList();
            ArrayList<String> arrayList2 = new ArrayList<>();
            arrayList2.addAll(Utils.y());
            if (string.contains(",")) {
                for (String str : string.split(",")) {
                    if (!arrayList.contains(str)) {
                        arrayList.add(str);
                    }
                }
            } else {
                arrayList.addAll(arrayList2);
            }
            final ArrayList arrayList3 = new ArrayList();
            ArrayList arrayList4 = new ArrayList();
            Collections.sort(arrayList2);
            for (String str2 : arrayList2) {
                try {
                    if (!arrayList3.contains(str2)) {
                        if (arrayList.contains(str2)) {
                            arrayList4.add(true);
                        } else {
                            arrayList4.add(false);
                        }
                        arrayList3.add(str2);
                    }
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                }
            }
            Boolean[] boolArr = (Boolean[]) arrayList4.toArray(new Boolean[arrayList4.size()]);
            boolean[] zArr = new boolean[boolArr.length];
            for (int i = 0; i < boolArr.length; i++) {
                zArr[i] = boolArr[i].booleanValue();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) I18N.a(R.string.pref_choose_provider));
            builder.a((CharSequence[]) arrayList3.toArray(new CharSequence[arrayList3.size()]), zArr, (DialogInterface.OnMultiChoiceClickListener) new DialogInterface.OnMultiChoiceClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i, boolean z) {
                    if (arrayList.contains(arrayList3.get(i))) {
                        arrayList.remove(arrayList3.get(i));
                    } else {
                        arrayList.add(arrayList3.get(i));
                    }
                }
            });
            builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    StringBuilder sb = new StringBuilder();
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        String str = (String) it2.next();
                        if (sb.indexOf(str + ",") == -1) {
                            sb.append(str);
                            sb.append(",");
                        }
                    }
                    String sb2 = sb.toString();
                    String substring = sb2.length() + -1 > 0 ? sb2.substring(0, sb2.length() - 1) : "";
                    FreeMoviesApp.l().edit().putString("pref_choose_provider_enabled2", substring).apply();
                    FreeMoviesApp.l().edit().putBoolean("pref_zerotv_enabled", substring.contains("ZeroTV")).apply();
                    dialogInterface.dismiss();
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.c();
        }

        private void showdialogfontscale() {
            String string = FreeMoviesApp.l().getString("pref_cc_subs_font_scale", "1.00");
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) I18N.a(R.string.pref_cc_subs_font_scale));
            AlertDialog a2 = builder.a();
            a2.a((CharSequence) I18N.a(R.string.default_value, Float.toString(1.0f)));
            final EditText editText = new EditText(getActivity());
            editText.setText(string, TextView.BufferType.EDITABLE);
            editText.setInputType(12290);
            editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View view, final boolean z) {
                    editText.post(new Runnable() {
                        public void run() {
                            InputMethodManager inputMethodManager = (InputMethodManager) SettingsFragment.this.getActivity().getSystemService("input_method");
                            if (z) {
                                inputMethodManager.showSoftInput(editText, 1);
                            } else {
                                inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                            }
                        }
                    });
                }
            });
            a2.a((View) editText);
            a2.a(-1, I18N.a(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    float f;
                    try {
                        f = Float.parseFloat(editText.getText().toString());
                    } catch (Throwable th) {
                        Logger.a(th, new boolean[0]);
                        f = -1.0f;
                    }
                    if (f < 0.0f) {
                        Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.value_must_be_positive));
                        return;
                    }
                    FreeMoviesApp.l().edit().putFloat("pref_cc_subs_font_scale", f).apply();
                    SettingsFragment.pref_cc_subs_font_scale.setSummary((CharSequence) Float.toString(f));
                }
            });
            a2.a(-2, I18N.a(R.string.cancel), new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            a2.show();
        }

        public void AuthPremiumizeAccount() {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) "Authorize Premiumize");
            AlertDialog a2 = builder.a();
            a2.a((CharSequence) " Enter Apikey. Available at https://www.premiumize.me/account");
            final EditText editText = new EditText(getActivity());
            editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
            editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View view, final boolean z) {
                    editText.post(new Runnable() {
                        public void run() {
                            InputMethodManager inputMethodManager = (InputMethodManager) SettingsFragment.this.getActivity().getSystemService("input_method");
                            if (z) {
                                inputMethodManager.showSoftInput(editText, 1);
                            } else {
                                inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                            }
                        }
                    });
                }
            });
            a2.a((View) editText);
            a2.a(-1, I18N.a(R.string.ok), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    String obj = editText.getText().toString();
                    if (obj.isEmpty()) {
                        Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.premiumiz_error_incorrect_apikey));
                        return;
                    }
                    Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.msg_wait));
                    SettingsFragment.this.mSubscriptions.b(PremiumizeUserApi.c().a(obj).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new Consumer<Boolean>() {
                        /* renamed from: a */
                        public void accept(Boolean bool) throws Exception {
                            if (bool.booleanValue()) {
                                SettingsFragment.pref_auth_premiumize_debrid.setTitle((CharSequence) "Logout to Premiumize");
                                SettingsFragment.pref_show_debrid_only.setEnabled(true);
                                SettingsFragment.pref_off_premium_resolve4.setEnabled(true);
                                SettingsFragment.pref_off_premium_resolve4.setChecked(true);
                                SettingsFragment.pref_show_debrid_only.setChecked(false);
                                return;
                            }
                            Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.premiumiz_error_incorrect_apikey));
                        }
                    }));
                }
            });
            a2.a(-2, I18N.a(R.string.cancel), new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            a2.show();
        }

        public /* synthetic */ void a(Dialog dialog2, String str) throws Exception {
            if (!str.isEmpty()) {
                pref_use_player_plugin.setSummary((CharSequence) String.format("IP : %s\nConnected", new Object[]{str}));
                FreeMoviesApp.l().edit().putBoolean("use_player_plugin", true).apply();
                FreeMoviesApp.l().edit().putString("ip_player_plugin", str).apply();
                dialog2.dismiss();
                this.mSubscriptions.a();
                return;
            }
            dialog2.findViewById(R.id.content_ip).setVisibility(0);
        }

        public /* synthetic */ void b(Throwable th) throws Exception {
            FragmentActivity activity = getActivity();
            Utils.a((Activity) activity, "Restore failed : " + th.getMessage());
        }

        public /* synthetic */ void c(Throwable th) throws Exception {
            FragmentActivity activity = getActivity();
            Utils.a((Activity) activity, "Backup failed : " + th.getMessage());
        }

        public void choosePlayer(String str) {
            pref_choose_default_player.setSummary((CharSequence) str);
            if (FreeMoviesApp.l().getString("pref_choose_default_action", "Always ask").equalsIgnoreCase("Play with subtitles") && str.equalsIgnoreCase("cinema")) {
                pref_choose_default_action.setSummary((CharSequence) "Play");
                FreeMoviesApp.l().edit().putString("pref_choose_default_action", "Play").apply();
            }
        }

        public void connectPlayerPlugin(String str) {
            this.reconnect = 0;
            this.mSubscriptions.b(Client.getIntance().createObservable(str).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new Consumer<Object>() {
                public void accept(Object obj) throws Exception {
                    if (obj instanceof UserPlayerPluginInfo) {
                        UserPlayerPluginInfo userPlayerPluginInfo = (UserPlayerPluginInfo) obj;
                        if (userPlayerPluginInfo.iConnect) {
                            SettingsFragment.pref_use_player_plugin.setSummary((CharSequence) String.format("IP : %s\nConnected", new Object[]{userPlayerPluginInfo.serverIP}));
                            FreeMoviesApp.l().edit().putBoolean("use_player_plugin", true).apply();
                            FreeMoviesApp.l().edit().putString("ip_player_plugin", userPlayerPluginInfo.serverIP).apply();
                            Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.player_plugin_connected));
                            return;
                        }
                        SettingsFragment settingsFragment = SettingsFragment.this;
                        if (settingsFragment.reconnect > 3) {
                            settingsFragment.mSubscriptions.a();
                            SettingsFragment.pref_use_player_plugin.setSummary((CharSequence) I18N.a(R.string.player_plugin_try));
                            FreeMoviesApp.l().edit().putBoolean("use_player_plugin", false).apply();
                            Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.player_plugin_cannot));
                        } else {
                            SettingsFragment.pref_use_player_plugin.setSummary((CharSequence) String.format("IP : %s\nConnecting", new Object[]{userPlayerPluginInfo.serverIP}));
                            Utils.a((Activity) SettingsFragment.this.getActivity(), I18N.a(R.string.player_plugin_try));
                        }
                        SettingsFragment.this.reconnect++;
                    }
                }
            }));
        }

        public /* synthetic */ void e() throws Exception {
            Utils.a((Activity) getActivity(), "Restore successful");
        }

        public /* synthetic */ void f() throws Exception {
            Utils.a((Activity) getActivity(), "Backup successful.");
        }

        public void getuserInfor() {
            this.mSubscriptions.b(Observable.create(new ObservableOnSubscribe<RealDebridUserInfor>() {
                public void subscribe(ObservableEmitter<RealDebridUserInfor> observableEmitter) throws Exception {
                    retrofit2.Response<RealDebridUserInfor> execute = SettingsFragment.this.realDebridApi.getUserInfo().execute();
                    if (execute.code() == 200) {
                        observableEmitter.onNext(execute.body());
                    }
                    if (execute.code() != 401) {
                        observableEmitter.onComplete();
                        return;
                    }
                    throw new Exception(execute.message());
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(x0.f5507a, new c1(this)));
        }

        /* access modifiers changed from: protected */
        public RecyclerView.Adapter onCreateAdapter(PreferenceScreen preferenceScreen) {
            return super.onCreateAdapter(preferenceScreen);
        }

        public void onCreatePreferences(Bundle bundle, String str) {
            addPreferencesFromResource(R.xml.preferences);
            this.mSubscriptions = new CompositeDisposable();
            PrefUtils.a((Context) getActivity(), (SharedPreferences.OnSharedPreferenceChangeListener) this);
            pref_auth_real_debrid = findPreference("pref_auth_real_debrid");
            pref_use_player_plugin = findPreference("pref_use_player_plugin");
            boolean z = false;
            if (FreeMoviesApp.l().getBoolean("use_player_plugin", false)) {
                pref_use_player_plugin.setSummary((CharSequence) String.format("IP : %s\nConnected", new Object[]{FreeMoviesApp.l().getString("ip_player_plugin", "")}));
            }
            boolean isValid = RealDebridCredentialsHelper.c().isValid();
            boolean z2 = !FreeMoviesApp.l().getString("pref_restrict_password", "").isEmpty();
            pref_auth_real_debrid.setSummary((CharSequence) !isValid ? "Real-Debird" : "Real-Debrid authorized");
            pref_show_debrid_only = (CheckBoxPreference) findPreference("pref_show_debrid_only");
            pref_force_tv_mode = (CheckBoxPreference) findPreference("pref_force_tv_mode");
            pref_keep_alive = (CheckBoxPreference) findPreference("pref_keep_alive");
            pref_off_premium_resolve4 = (CheckBoxPreference) findPreference("pref_off_premium_resolve4");
            pref_rd_request_per_second = (SeekBarPreference) findPreference("pref_rd_request_per_second");
            pre_show_my_calenda = (CheckBoxPreference) findPreference("pre_show_my_calenda_shows_only");
            boolean z3 = FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false);
            boolean z4 = FreeMoviesApp.l().getBoolean("pref_off_premium_resolve4", true);
            if (isValid) {
                getuserInfor();
                String string = FreeMoviesApp.l().getString("pref_rd_expiration", "");
                pref_auth_real_debrid.setTitle((CharSequence) "Logout to Real-Debrid");
                pref_auth_real_debrid.setSummary((CharSequence) "Real-Debrid authorized " + string);
            }
            if (BaseProvider.b()) {
                pref_show_debrid_only.setChecked(z3);
                pref_off_premium_resolve4.setChecked(z4);
            } else {
                pref_show_debrid_only.setEnabled(false);
                pref_show_debrid_only.setChecked(false);
                pref_off_premium_resolve4.setEnabled(false);
                pref_off_premium_resolve4.setChecked(false);
                pref_rd_request_per_second.setEnabled(false);
            }
            pref_category_restriction = findPreference("pref_category_restriction");
            pref_category_restriction.setEnabled(z2);
            pref_set_password = findPreference("pref_set_password");
            pref_set_password.setEnabled(true);
            boolean z5 = FreeMoviesApp.l().getBoolean("pref_restrict_search", true);
            pref_restrict_search = (CheckBoxPreference) findPreference("pref_restrict_search");
            pref_restrict_search.setEnabled(z2);
            pref_restrict_search.setChecked(z5);
            pref_auth_All_debrid = findPreference("pref_auth_All_debrid");
            if (AllDebridCredentialsHelper.b().isValid()) {
                getaduserInfor();
                String string2 = FreeMoviesApp.l().getString("pref_ad_expiration", "");
                pref_auth_All_debrid.setTitle((CharSequence) "Logout to All-Debrid");
                pref_auth_All_debrid.setSummary((CharSequence) "All-Debrid authorized " + string2);
            } else {
                String string3 = FreeMoviesApp.l().getString("pref_ad_expiration", "");
                pref_auth_All_debrid.setSummary((CharSequence) "All-Debrid " + string3);
            }
            pref_auth_premiumize_debrid = findPreference("pref_auth_premiumize_debrid");
            if (PremiumizeCredentialsHelper.b().isValid()) {
                pref_auth_premiumize_debrid.setTitle((CharSequence) "Logout to Premiumize");
                String string4 = FreeMoviesApp.l().getString("pref_premiumize_expiration", "");
                pref_auth_premiumize_debrid.setSummary((CharSequence) "Premiumize authorized " + string4);
            } else {
                String string5 = FreeMoviesApp.l().getString("pref_premiumize_expiration", "");
                pref_auth_premiumize_debrid.setSummary((CharSequence) "Premiumize " + string5);
            }
            pref_auth_trakt_tv = findPreference("pref_auth_trakt_tv");
            boolean isValid2 = TraktCredentialsHelper.b().isValid();
            boolean z6 = FreeMoviesApp.l().getBoolean("pre_show_my_calenda_shows_only", true);
            pref_auth_trakt_tv.setSummary((CharSequence) !isValid2 ? "Trakt-Tv" : "Trakt-Tv authorized");
            if (isValid2) {
                pre_show_my_calenda.setChecked(z6);
                pref_auth_trakt_tv.setTitle((CharSequence) "Logout to Trakt TV");
            } else {
                pre_show_my_calenda.setEnabled(false);
                pre_show_my_calenda.setChecked(false);
            }
            pref_dowload_path = findPreference("pref_dowload_path");
            pref_dowload_path.setSummary((CharSequence) FreeMoviesApp.l().getString(pref_dowload_path.getKey(), Setting.b(Utils.i()).toLowerCase()));
            pref_choose_default_player = findPreference("pref_choose_default_player");
            String string6 = FreeMoviesApp.l().getString("pref_choose_default_player", BasePlayerHelper.d().b());
            pref_choose_default_player.setSummary((CharSequence) string6);
            pref_sub_language_international_v3 = findPreference("pref_sub_language_international_v3");
            pref_sub_language_international_v3.setSummary((CharSequence) FreeMoviesApp.l().getStringSet(pref_sub_language_international_v3.getKey(), new HashSet(Arrays.asList(new String[]{Locale.getDefault().getLanguage()}))).toString());
            pref_cc_subs_font_color = findPreference("pref_cc_subs_font_color");
            pref_cc_subs_font_color.setSummary((CharSequence) FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF").toUpperCase());
            pref_cc_subs_font_scale = findPreference("pref_cc_subs_font_scale");
            String f = Float.toString(FreeMoviesApp.l().getFloat("pref_cc_subs_font_scale", 1.0f));
            pref_cc_subs_font_scale.setSummary((CharSequence) f);
            pref_cc_subs_font_scale.setDefaultValue(f);
            pref_cc_unlock_full_version = findPreference("pref_cc_unlock_full_version");
            String q = Utils.q();
            pref_cc_unlock_full_version.setSummary((CharSequence) q);
            pref_cc_unlock_full_version.setDefaultValue(q);
            pref_column_in_main = findPreference("pref_column_in_main");
            pref_column_in_main.setSummary((CharSequence) FreeMoviesApp.l().getString("pref_column_in_main", "Large"));
            PreferenceCategory preferenceCategory = (PreferenceCategory) findPreference("setting_general_title");
            pref_column_in_main.setOnPreferenceClickListener(this);
            pref_choose_default_action = findPreference("pref_choose_default_action");
            String string7 = FreeMoviesApp.l().getString("pref_choose_default_action", "Always ask");
            pref_choose_default_action.setSummary((CharSequence) string7);
            if (string7.equalsIgnoreCase("Play with subtitles") && string6.equalsIgnoreCase("cinema")) {
                pref_choose_default_action.setSummary((CharSequence) "Play");
                FreeMoviesApp.l().edit().putString("pref_choose_default_action", "Play").apply();
            }
            pref_choose_default_tab = findPreference("pref_choose_default_tab");
            pref_choose_default_tab.setSummary((CharSequence) FreeMoviesApp.l().getString("pref_choose_default_tab", "Tv/Shows"));
            pref_choose_host_priority3 = findPreference("pref_choose_host_priority3");
            pref_choose_host_priority3.setSummary((CharSequence) FreeMoviesApp.l().getString("pref_choose_host_priority3", BaseResolver.c()));
            boolean z7 = FreeMoviesApp.l().getBoolean("pref_force_tv_mode", false);
            pref_force_tv_mode = (CheckBoxPreference) findPreference("pref_force_tv_mode");
            pref_force_tv_mode.setChecked(z7);
            boolean z8 = FreeMoviesApp.l().getBoolean("pref_keep_alive", false);
            pref_keep_alive = (CheckBoxPreference) findPreference("pref_keep_alive");
            pref_keep_alive.setChecked(z8);
            boolean z9 = FreeMoviesApp.l().getBoolean("pref_show_aried_eps_only2", false);
            pref_show_aried_eps_only2 = (CheckBoxPreference) findPreference("pref_show_aried_eps_only2");
            pref_show_aried_eps_only2.setChecked(z9);
            boolean z10 = FreeMoviesApp.l().getBoolean("pref_show_special_season", false);
            pref_show_special_season = (CheckBoxPreference) findPreference("pref_show_special_season");
            pref_show_special_season.setChecked(z10);
            boolean z11 = FreeMoviesApp.l().getBoolean("pref_show_sort_link_by_size2", true);
            pref_show_sort_link_by_size2 = (CheckBoxPreference) findPreference("pref_show_sort_link_by_size2");
            pref_show_sort_link_by_size2.setChecked(z11);
            boolean z12 = FreeMoviesApp.l().getBoolean("pref_show_sort_link_by_quality", true);
            pref_show_sort_link_by_quality = (CheckBoxPreference) findPreference("pref_show_sort_link_by_quality");
            pref_show_sort_link_by_quality.setChecked(z12);
            if (z11 || z12) {
                pref_choose_host_priority3.setEnabled(false);
            }
            SettingsActivity.h = Boolean.valueOf(z11);
            SettingsActivity.i = Boolean.valueOf(z12);
            boolean z13 = FreeMoviesApp.l().getBoolean("pref_show_hd_only", false);
            pref_show_hd_only = (CheckBoxPreference) findPreference("pref_show_hd_only");
            pref_show_hd_only.setChecked(z13);
            boolean z14 = FreeMoviesApp.l().getBoolean("pref_filter_cam", false);
            pref_filter_cam = (CheckBoxPreference) findPreference("pref_filter_cam");
            pref_filter_cam.setChecked(z14);
            boolean z15 = FreeMoviesApp.l().getBoolean("pref_auto_backup_when_exit_app", false);
            pref_auto_backup_when_exit_app = (CheckBoxPreference) findPreference("pref_auto_backup_when_exit_app");
            pref_auto_backup_when_exit_app.setChecked(z15);
            boolean z16 = FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
            pref_auto_next_eps = (CheckBoxPreference) findPreference("pref_auto_next_eps");
            pref_auto_next_eps.setChecked(z16);
            boolean z17 = FreeMoviesApp.l().getBoolean("pref_change_bg_color", true);
            pref_change_bg_color = (CheckBoxPreference) findPreference("pref_change_bg_color");
            pref_change_bg_color.setChecked(z17);
            pref_auto_next_with_fisrt_sub = (CheckBoxPreference) findPreference("pref_auto_next_with_fisrt_sub");
            pref_auto_next_with_fisrt_sub.setChecked(FreeMoviesApp.l().getBoolean("pref_auto_next_with_fisrt_sub", false));
            pref_low_profilev2 = (CheckBoxPreference) findPreference("pref_low_profilev2");
            SharedPreferences l = FreeMoviesApp.l();
            if (Build.VERSION.SDK_INT <= 20) {
                z = true;
            }
            pref_low_profilev2.setChecked(l.getBoolean("pref_low_profilev2", z));
            pref_rd_request_per_second.setValue(FreeMoviesApp.l().getInt("pref_rd_request_per_second", 4));
            int i = FreeMoviesApp.l().getInt("pref_auto_next_eps_number_of_link", 10);
            pref_auto_next_eps_number_of_link = findPreference("pref_auto_next_eps_number_of_link");
            pref_auto_next_eps_number_of_link.setSummary((CharSequence) String.valueOf(i));
            pref_auto_next_eps_number_of_link.setEnabled(z16);
            pref_auto_next_with_fisrt_sub.setEnabled(z16);
            pref_change_bg_color.setEnabled(z17);
            pref_changelog = findPreference("pref_changelog");
            pref_backup = findPreference("pref_backup");
            pref_restore = findPreference("pref_restore");
            pref_clear_cache = findPreference("pref_clear_cache");
            pref_recoptchar = findPreference("pref_recoptchar");
            pref_choose_provider_enabled2 = findPreference("pref_choose_provider_enabled2");
            pref_use_player_plugin.setOnPreferenceClickListener(this);
            pref_auth_real_debrid.setOnPreferenceClickListener(this);
            pref_auth_All_debrid.setOnPreferenceClickListener(this);
            pref_auth_premiumize_debrid.setOnPreferenceClickListener(this);
            pref_auth_trakt_tv.setOnPreferenceClickListener(this);
            pref_dowload_path.setOnPreferenceClickListener(this);
            pref_sub_language_international_v3.setOnPreferenceClickListener(this);
            pref_choose_default_player.setOnPreferenceClickListener(this);
            pref_cc_subs_font_color.setOnPreferenceClickListener(this);
            pref_cc_subs_font_scale.setOnPreferenceClickListener(this);
            pref_cc_unlock_full_version.setOnPreferenceClickListener(this);
            pref_choose_default_action.setOnPreferenceClickListener(this);
            pref_choose_default_tab.setOnPreferenceClickListener(this);
            pref_choose_host_priority3.setOnPreferenceClickListener(this);
            pref_show_aried_eps_only2.setOnPreferenceClickListener(this);
            pref_show_special_season.setOnPreferenceClickListener(this);
            pref_show_sort_link_by_size2.setOnPreferenceClickListener(this);
            pref_show_sort_link_by_quality.setOnPreferenceClickListener(this);
            pref_show_hd_only.setOnPreferenceClickListener(this);
            pref_filter_cam.setOnPreferenceClickListener(this);
            pref_auto_backup_when_exit_app.setOnPreferenceClickListener(this);
            pref_auto_next_eps.setOnPreferenceClickListener(this);
            pref_change_bg_color.setOnPreferenceClickListener(this);
            pref_auto_next_with_fisrt_sub.setOnPreferenceClickListener(this);
            pref_low_profilev2.setOnPreferenceClickListener(this);
            pref_auto_next_eps_number_of_link.setOnPreferenceClickListener(this);
            pref_show_debrid_only.setOnPreferenceClickListener(this);
            pref_set_password.setOnPreferenceClickListener(this);
            pref_restrict_search.setOnPreferenceClickListener(this);
            pref_category_restriction.setOnPreferenceClickListener(this);
            pref_force_tv_mode.setOnPreferenceClickListener(this);
            pref_keep_alive.setOnPreferenceClickListener(this);
            pref_off_premium_resolve4.setOnPreferenceClickListener(this);
            pref_rd_request_per_second.setOnPreferenceChangeListener(this);
            pre_show_my_calenda.setOnPreferenceClickListener(this);
            pref_choose_provider_enabled2.setOnPreferenceClickListener(this);
            pref_changelog.setOnPreferenceClickListener(this);
            pref_backup.setOnPreferenceClickListener(this);
            pref_restore.setOnPreferenceClickListener(this);
            pref_clear_cache.setOnPreferenceClickListener(this);
            pref_recoptchar.setOnPreferenceClickListener(this);
        }

        public void onDestroy() {
            this.mSubscriptions.dispose();
            super.onDestroy();
            PrefUtils.b((Context) getActivity(), (SharedPreferences.OnSharedPreferenceChangeListener) this);
        }

        public void onDestroyView() {
            this.compositeDisposable.dispose();
            super.onDestroyView();
            if (!BaseProvider.b()) {
                FreeMoviesApp.l().edit().putBoolean("pref_show_debrid_only", false).apply();
                FreeMoviesApp.l().edit().putBoolean("pref_off_premium_resolve4", true).apply();
            }
        }

        public boolean onPreferenceChange(Preference preference, Object obj) {
            if (!preference.getKey().contains("pref_rd_request_per_second")) {
                return false;
            }
            Integer num = (Integer) obj;
            FreeMoviesApp.l().edit().putInt("pref_rd_request_per_second", num.intValue()).apply();
            pref_rd_request_per_second.setValue(num.intValue());
            return false;
        }

        public boolean onPreferenceClick(Preference preference) {
            final String key = preference.getKey();
            if (!key.equalsIgnoreCase("pref_choose_theme")) {
                if (key.equalsIgnoreCase("pref_auth_real_debrid")) {
                    checkRealDebird();
                    RxBus.b().a().subscribe(new Consumer<Object>() {
                        public void accept(Object obj) throws Exception {
                            if (obj instanceof ApiDebridWaitingToVerifyEvent) {
                                ApiDebridWaitingToVerifyEvent apiDebridWaitingToVerifyEvent = (ApiDebridWaitingToVerifyEvent) obj;
                                Intent intent = new Intent(SettingsFragment.this.getActivity(), RealDebridAuthWebViewActivity.class);
                                intent.putExtra("verificationUrl", apiDebridWaitingToVerifyEvent.b());
                                intent.putExtra("userCode", apiDebridWaitingToVerifyEvent.a());
                                SettingsFragment.this.getActivity().startActivity(intent);
                            }
                        }
                    }, f1.f5226a);
                } else if (key.equalsIgnoreCase("pref_auth_All_debrid")) {
                    checkAllDebird();
                    RxBus.b().a().subscribe(new Consumer<Object>() {
                        public void accept(Object obj) throws Exception {
                            if (obj instanceof ApiDebridWaitingToVerifyEvent) {
                                ApiDebridWaitingToVerifyEvent apiDebridWaitingToVerifyEvent = (ApiDebridWaitingToVerifyEvent) obj;
                                Intent intent = new Intent(SettingsFragment.this.getActivity(), AllDebridAuthWebViewActivity.class);
                                intent.putExtra("verificationUrl", apiDebridWaitingToVerifyEvent.b());
                                intent.putExtra("pin", apiDebridWaitingToVerifyEvent.a());
                                SettingsFragment.this.getActivity().startActivity(intent);
                            }
                        }
                    }, a1.f5201a);
                } else if (key.equalsIgnoreCase("pref_auth_premiumize_debrid")) {
                    if (PremiumizeCredentialsHelper.b().isValid()) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.a((CharSequence) "Do you want logout to Premiumize?");
                        final AlertDialog a2 = builder.a();
                        a2.a(-1, "ok", new DialogInterface.OnClickListener(this) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                PremiumizeCredentialsHelper.a();
                                SettingsFragment.pref_auth_premiumize_debrid.setTitle((CharSequence) "Login to Premiumize");
                                SettingsFragment.pref_auth_premiumize_debrid.setSummary((CharSequence) "Premiumize");
                                FreeMoviesApp.l().edit().putString("pref_premiumize_expiration", "").commit();
                                a2.dismiss();
                            }
                        });
                        a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                a2.dismiss();
                            }
                        });
                        a2.show();
                    } else {
                        AuthPremiumizeAccount();
                    }
                } else if (key.equalsIgnoreCase("pref_auth_trakt_tv")) {
                    checktrakltv();
                    RxBus.b().a().subscribe(new Consumer<Object>() {
                        public void accept(Object obj) throws Exception {
                            if (obj instanceof TraktWaitingToVerifyEvent) {
                                TraktWaitingToVerifyEvent traktWaitingToVerifyEvent = (TraktWaitingToVerifyEvent) obj;
                                Intent intent = new Intent(SettingsFragment.this.getActivity(), TraktAuthWebViewActivity.class);
                                intent.putExtra("verificationUrl", traktWaitingToVerifyEvent.b());
                                intent.putExtra("userCode", traktWaitingToVerifyEvent.a());
                                SettingsFragment.this.getActivity().startActivity(intent);
                            }
                        }
                    }, i1.f5247a);
                } else if (key.equalsIgnoreCase("pref_dowload_path")) {
                    new Thread(new Runnable() {
                        public void run() {
                            Intent intent = new Intent(SettingsFragment.this.getActivity(), FilePickerActivity.class);
                            intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", false);
                            intent.putExtra("nononsense.intent.ALLOW_CREATE_DIR", true);
                            intent.putExtra("nononsense.intent.MODE", 1);
                            intent.putExtra("nononsense.intent.START_PATH", FreeMoviesApp.l().getString(key, Setting.b(Utils.i()).toLowerCase()));
                            SettingsFragment.this.getActivity().startActivityForResult(intent, SettingsActivity.e);
                        }
                    }).start();
                } else if (key.equalsIgnoreCase("pref_sub_language_international_v3")) {
                    m32399();
                } else if (key.equalsIgnoreCase("pref_choose_default_player")) {
                    BasePlayerHelper.a((Activity) getActivity(), (BasePlayerHelper.OnChoosePlayerListener) this);
                } else if (key.equalsIgnoreCase("pref_cc_subs_font_color")) {
                    showColorPicker();
                } else if (key.equalsIgnoreCase("pref_cc_subs_font_scale")) {
                    showdialogfontscale();
                } else if (!key.equalsIgnoreCase("pref_cc_unlock_full_version")) {
                    if (key.equalsIgnoreCase("pref_column_in_main")) {
                        showdialogportersize();
                    } else if (key.equalsIgnoreCase("pref_changelog")) {
                        showChangeLog();
                    } else if (key.equalsIgnoreCase("pref_backup")) {
                        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Void>() {
                            public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/cinemahd/" + "backup");
                                if (!file.exists() && !file.mkdirs()) {
                                    Logger.a("SettingActivity", "Problem creating Image folder");
                                }
                                MvDatabase.a((Context) SettingsFragment.this.getActivity(), "backup");
                                PrefUtils.c(SettingsFragment.this.getActivity(), "backup");
                                observableEmitter.onComplete();
                            }
                        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(b1.f5210a, new z0(this), new y0(this)));
                    } else if (key.equalsIgnoreCase("pref_restore")) {
                        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Void>() {
                            public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                                MvDatabase.b(SettingsFragment.this.getActivity(), "backup");
                                PrefUtils.a((Context) SettingsFragment.this.getActivity(), "backup");
                                observableEmitter.onComplete();
                            }
                        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(l1.f5259a, new d1(this), new k1(this)));
                    } else if (key.equalsIgnoreCase("pref_recoptchar")) {
                        getActivity().startActivityForResult(new Intent(Utils.i(), HelpRecaptchar.class), 5);
                    } else if (key.equalsIgnoreCase("pref_clear_cache")) {
                        Utils.b((Context) getActivity());
                        Utils.a((Activity) getActivity(), "Cache cleared.");
                    } else if (key.equalsIgnoreCase("pref_choose_default_action")) {
                        showdialogaction();
                    } else if (key.equalsIgnoreCase("pref_choose_default_tab")) {
                        showDialogTab();
                    } else if (key.equalsIgnoreCase("pref_choose_host_priority3")) {
                        showHostStreamPriority();
                    } else {
                        boolean z = false;
                        if (key.equalsIgnoreCase("pref_show_aried_eps_only2")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_show_aried_eps_only2", !FreeMoviesApp.l().getBoolean("pref_show_aried_eps_only2", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_show_special_season")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_show_special_season", !FreeMoviesApp.l().getBoolean("pref_show_special_season", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_force_tv_mode")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_force_tv_mode", !FreeMoviesApp.l().getBoolean("pref_force_tv_mode", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_keep_alive")) {
                            boolean z2 = FreeMoviesApp.l().getBoolean("pref_keep_alive", false);
                            FreeMoviesApp.l().edit().putBoolean("pref_keep_alive", !z2).apply();
                            if (!z2) {
                                Utils.d((Context) getActivity());
                            } else {
                                Utils.e(getContext());
                            }
                        } else if (key.equalsIgnoreCase("pre_show_my_calenda_shows_only")) {
                            FreeMoviesApp.l().edit().putBoolean("pre_show_my_calenda_shows_only", !FreeMoviesApp.l().getBoolean("pre_show_my_calenda_shows_only", true)).apply();
                        } else if (key.equalsIgnoreCase("pref_show_sort_link_by_size2")) {
                            boolean z3 = FreeMoviesApp.l().getBoolean("pref_show_sort_link_by_size2", true);
                            FreeMoviesApp.l().edit().putBoolean("pref_show_sort_link_by_size2", !z3).apply();
                            Preference preference2 = pref_choose_host_priority3;
                            if (z3 && !SettingsActivity.i.booleanValue()) {
                                z = true;
                            }
                            preference2.setEnabled(z);
                            SettingsActivity.h = Boolean.valueOf(!z3);
                        } else if (key.equalsIgnoreCase("pref_show_sort_link_by_quality")) {
                            boolean z4 = FreeMoviesApp.l().getBoolean("pref_show_sort_link_by_quality", true);
                            FreeMoviesApp.l().edit().putBoolean("pref_show_sort_link_by_quality", !z4).apply();
                            Preference preference3 = pref_choose_host_priority3;
                            if (z4 && !SettingsActivity.h.booleanValue()) {
                                z = true;
                            }
                            preference3.setEnabled(z);
                            SettingsActivity.i = Boolean.valueOf(!z4);
                        } else if (key.equalsIgnoreCase("pref_show_hd_only")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_show_hd_only", !FreeMoviesApp.l().getBoolean("pref_show_hd_only", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_filter_cam")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_filter_cam", !FreeMoviesApp.l().getBoolean("pref_filter_cam", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_auto_backup_when_exit_app")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_auto_backup_when_exit_app", !FreeMoviesApp.l().getBoolean("pref_auto_backup_when_exit_app", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_auto_next_eps_number_of_link")) {
                            showNumberOfLinkToAutoPlay();
                        } else if (key.equalsIgnoreCase("pref_auto_next_eps")) {
                            boolean z5 = FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
                            FreeMoviesApp.l().edit().putBoolean("pref_auto_next_eps", !z5).apply();
                            pref_auto_next_eps_number_of_link.setEnabled(!z5);
                            pref_auto_next_with_fisrt_sub.setEnabled(!z5);
                        } else if (key.equalsIgnoreCase("pref_change_bg_color")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_change_bg_color", !FreeMoviesApp.l().getBoolean("pref_change_bg_color", true)).apply();
                        } else if (key.equalsIgnoreCase("pref_low_profilev2")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_low_profilev2", !FreeMoviesApp.l().getBoolean("pref_low_profilev2", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_auto_next_with_fisrt_sub")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_auto_next_with_fisrt_sub", !FreeMoviesApp.l().getBoolean("pref_auto_next_with_fisrt_sub", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_show_debrid_only")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_show_debrid_only", !FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false)).apply();
                        } else if (key.equalsIgnoreCase("pref_off_premium_resolve4")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_off_premium_resolve4", !FreeMoviesApp.l().getBoolean("pref_off_premium_resolve4", true)).apply();
                        } else if (key.equalsIgnoreCase("pref_choose_provider_enabled2")) {
                            showchooseprovider();
                        } else if (key.equalsIgnoreCase("pref_use_player_plugin")) {
                            showUIInputIP();
                        } else if (key.equalsIgnoreCase("pref_set_password")) {
                            showSetPassword();
                        } else if (key.equalsIgnoreCase("pref_category_restriction")) {
                            showCategoryRetriction();
                        } else if (key.equalsIgnoreCase("pref_restrict_search")) {
                            FreeMoviesApp.l().edit().putBoolean("pref_restrict_search", !FreeMoviesApp.l().getBoolean("pref_restrict_search", true)).apply();
                        }
                    }
                }
            }
            return true;
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
            if (str.contains("pref_sub_language_international_v3")) {
                pref_sub_language_international_v3.setSummary((CharSequence) FreeMoviesApp.l().getStringSet(pref_sub_language_international_v3.getKey(), new HashSet(Arrays.asList(new String[]{Locale.getDefault().getLanguage()}))).toString());
            }
            if (str.contains("pref_rd_expiration")) {
                String string = FreeMoviesApp.l().getString(str, "");
                boolean isValid = RealDebridCredentialsHelper.c().isValid();
                Preference preference = pref_auth_real_debrid;
                StringBuilder sb = new StringBuilder();
                sb.append(!isValid ? "Real-Debird " : "Real-Debrid authorized ");
                sb.append(string);
                preference.setSummary((CharSequence) sb.toString());
            }
            if (str.contains("pref_ad_expiration")) {
                String string2 = FreeMoviesApp.l().getString(str, "");
                boolean isValid2 = AllDebridCredentialsHelper.b().isValid();
                Preference preference2 = pref_auth_All_debrid;
                StringBuilder sb2 = new StringBuilder();
                sb2.append(!isValid2 ? "All-Debird " : "All-Debrid authorized ");
                sb2.append(string2);
                preference2.setSummary((CharSequence) sb2.toString());
            }
            if (str.contains("pref_premiumize_expiration")) {
                String string3 = FreeMoviesApp.l().getString(str, "");
                boolean isValid3 = AllDebridCredentialsHelper.b().isValid();
                Preference preference3 = pref_auth_premiumize_debrid;
                StringBuilder sb3 = new StringBuilder();
                sb3.append(!isValid3 ? "Premiumize " : "Premiumize authorized ");
                sb3.append(string3);
                preference3.setSummary((CharSequence) sb3.toString());
            }
            FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false);
            pref_off_premium_resolve4.setEnabled(BaseProvider.b());
            pref_rd_request_per_second.setEnabled(BaseProvider.b());
        }

        public void setMvDatabase(MvDatabase mvDatabase2) {
            this.mvDatabase = mvDatabase2;
        }

        public void setNewPassword() {
            final EditText editText = new EditText(getActivity());
            Utils.a((Activity) getActivity(), "Enter new password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    SettingsFragment.pref_set_password.setSummary((CharSequence) "********");
                    FreeMoviesApp.l().edit().putString("pref_restrict_password", editText.getText().toString()).apply();
                    SettingsFragment.pref_restrict_search.setEnabled(true);
                    SettingsFragment.pref_category_restriction.setEnabled(true);
                }
            }, editText, (DialogInterface.OnDismissListener) null);
        }

        public void setRealDebridApi(RealDebridApi realDebridApi2) {
            this.realDebridApi = realDebridApi2;
        }

        public void showCategoryRetriction() {
            ArrayList arrayList = new ArrayList();
            ArrayList arrayList2 = new ArrayList();
            for (int i = 0; i < Categorys.getMVCategory().size(); i++) {
                arrayList.add(Categorys.getMVCategory().valueAt(i));
            }
            for (int i2 = 0; i2 < Categorys.getTVCategory().size(); i2++) {
                arrayList2.add(Categorys.getTVCategory().valueAt(i2));
            }
            FragmentManager supportFragmentManager = ((SettingsActivity) getActivity()).getSupportFragmentManager();
            FragmentTransaction b = supportFragmentManager.b();
            Fragment b2 = supportFragmentManager.b("CategoryRetrictionDialog");
            if (b2 != null) {
                b.a(b2);
            }
            b.a((String) null);
            CategoryRetrictionDialog.a(TextUtils.join(",", arrayList), TextUtils.join(",", arrayList2)).show(b, "CategoryRetrictionDialog");
        }

        public void showChangeLog() {
            new SpannableStringBuilder("Change Logs").setSpan(new ForegroundColorSpan(-256), 0, 11, 33);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) "Change Logs");
            builder.b(getActivity().getLayoutInflater().inflate(R.layout.dialog_changelog, (ViewGroup) null));
            builder.b((CharSequence) "OK", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.a().show();
        }

        public void showDialogTab() {
            String string = FreeMoviesApp.l().getString("pref_choose_default_tab", "Tv/Shows");
            final ArrayList arrayList = new ArrayList();
            arrayList.add("None");
            arrayList.add("Tv/Shows");
            arrayList.add("Movies");
            arrayList.add("Favorites");
            arrayList.add("History");
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) "Choose default pref_choose_default_tab action");
            builder.a(true);
            builder.a((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), arrayList.indexOf(string), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    String str = (String) arrayList.get(i);
                    FreeMoviesApp.l().edit().putString("pref_choose_default_tab", str).apply();
                    SettingsFragment.pref_choose_default_tab.setSummary((CharSequence) str);
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.c();
        }

        public void showHostStreamPriority() {
            String string = FreeMoviesApp.l().getString("pref_choose_host_priority3", BaseResolver.c());
            final ArrayList arrayList = new ArrayList();
            if (string.contains(",")) {
                for (String add : string.split(",")) {
                    arrayList.add(add);
                }
            }
            final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), 17367046, arrayList);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) I18N.a(R.string.host_stream_priority));
            builder.a((ListAdapter) arrayAdapter, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    StringBuilder sb = new StringBuilder();
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        String str = (String) it2.next();
                        if (sb.indexOf(str + ",") == -1) {
                            sb.append(str);
                            sb.append(",");
                        }
                    }
                    String sb2 = sb.toString();
                    String substring = sb2.length() + -1 > 0 ? sb2.substring(0, sb2.length() - 1) : "";
                    FreeMoviesApp.l().edit().putString("pref_choose_host_priority3", substring).apply();
                    SettingsFragment.pref_choose_host_priority3.setSummary((CharSequence) substring);
                    SettingsActivity.g = substring;
                    dialogInterface.dismiss();
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.c().b().setOnItemClickListener(new AdapterView.OnItemClickListener(this) {
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                    if (i > 0) {
                        Collections.swap(arrayList, i, i - 1);
                        arrayAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

        public void showNumberOfLinkToAutoPlay() {
            int intValue = Integer.valueOf(FreeMoviesApp.l().getString("pref_auto_next_eps_number_of_link", "10")).intValue();
            final ArrayList arrayList = new ArrayList();
            for (int i = 2; i < 20; i++) {
                arrayList.add(String.valueOf(i * 2));
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) "Number of links");
            builder.a(true);
            builder.a((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), arrayList.indexOf(String.valueOf(intValue)), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    String str = (String) arrayList.get(i);
                    FreeMoviesApp.l().edit().putInt("pref_auto_next_eps_number_of_link", Integer.parseInt(str)).apply();
                    SettingsFragment.pref_auto_next_eps_number_of_link.setSummary((CharSequence) str);
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.c();
        }

        public void showResultUnlock(final boolean z) {
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    AlertDialog.Builder builder = new AlertDialog.Builder(SettingsFragment.this.getActivity());
                    builder.b((CharSequence) I18N.a(z ? R.string.unlock_result_success : R.string.unlock_result_failded));
                    builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            AnonymousClass40 r3 = AnonymousClass40.this;
                            if (z) {
                                SettingsFragment.this.getActivity().finishAffinity();
                            }
                            dialogInterface.dismiss();
                        }
                    });
                    builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    builder.c();
                }
            });
        }

        public void showSetPassword() {
            final String string = FreeMoviesApp.l().getString("pref_restrict_password", "");
            if (string.isEmpty()) {
                setNewPassword();
                return;
            }
            final EditText editText = new EditText(getActivity());
            Utils.a((Activity) getActivity(), "Enter old password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (string.equals(editText.getText().toString())) {
                        SettingsFragment.this.setNewPassword();
                    } else {
                        Utils.a((Activity) SettingsFragment.this.getActivity(), "Password is wrong");
                    }
                }
            }, editText, (DialogInterface.OnDismissListener) null);
        }

        public void showWaitingDialog(String str) {
            FragmentActivity activity = getActivity();
            this.dialog = ProgressDialog.show(activity, "", str + ". Please wait...", true);
        }

        public void showdialogaction() {
            String string = FreeMoviesApp.l().getString("pref_choose_default_action", "Always ask");
            final ArrayList arrayList = new ArrayList();
            for (String add : Utils.a(true).keySet()) {
                arrayList.add(add);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) "Choose default play action");
            builder.a(true);
            builder.a((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), arrayList.indexOf(string), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    String str = (String) arrayList.get(i);
                    FreeMoviesApp.l().edit().putString("pref_choose_default_action", str).apply();
                    SettingsFragment.pref_choose_default_action.setSummary((CharSequence) str);
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.c();
        }

        public void showdialogportersize() {
            String string = FreeMoviesApp.l().getString("pref_column_in_main", "Large");
            final ArrayList arrayList = new ArrayList();
            for (String add : Utils.t().keySet()) {
                arrayList.add(add);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.b((CharSequence) "Choose size poster (Landscape Mode)");
            builder.a(true);
            builder.a((CharSequence[]) arrayList.toArray(new String[arrayList.size()]), arrayList.indexOf(string), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    String str = (String) arrayList.get(i);
                    FreeMoviesApp.l().edit().putString("pref_column_in_main", str).apply();
                    SettingsFragment.pref_column_in_main.setSummary((CharSequence) str);
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                        Utils.c((Activity) SettingsFragment.this.getActivity());
                    }
                }
            });
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!SettingsFragment.this.getActivity().isFinishing() && dialogInterface != null) {
                        dialogInterface.dismiss();
                    }
                }
            });
            builder.c();
        }

        public /* synthetic */ void a(RealDebridGetTokenResult realDebridGetTokenResult) throws Exception {
            RealDebridCredentialsHelper.a(realDebridGetTokenResult.getAccess_token(), realDebridGetTokenResult.getRefresh_token(), realDebridGetTokenResult.getLast_clientID(), realDebridGetTokenResult.getLast_clientSecret());
            getuserInfor();
            RxBus.b().a(realDebridGetTokenResult != null ? new ApiDebridGetTokenSuccessEvent() : new ApiDebridGetTokenFailedEvent());
        }

        static /* synthetic */ void a(RealDebridUserInfor realDebridUserInfor) throws Exception {
            Logger.a("Real Debrid ", realDebridUserInfor.toString());
            boolean equalsIgnoreCase = realDebridUserInfor.type.equalsIgnoreCase("premium");
            FreeMoviesApp.l().edit().putBoolean("pref_realdebrid_type", equalsIgnoreCase).commit();
            FreeMoviesApp.l().edit().putString("pref_realdebrid_expiration_str", realDebridUserInfor.expiration).commit();
            String b = DateTimeHelper.b(realDebridUserInfor.expiration);
            if (equalsIgnoreCase) {
                SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
                edit.putString("pref_rd_expiration", "\nUsername : " + realDebridUserInfor.username + "\nType : " + realDebridUserInfor.type + "\nExpiration : " + b).commit();
            } else {
                SharedPreferences.Editor edit2 = FreeMoviesApp.l().edit();
                edit2.putString("pref_rd_expiration", "\nUsername : " + realDebridUserInfor.username + "\nType : " + realDebridUserInfor.type).commit();
            }
            pref_off_premium_resolve4.setChecked(true);
        }

        public /* synthetic */ void a(Throwable th) throws Exception {
            RealDebridCredentialsHelper.a();
            FreeMoviesApp.l().edit().putString("pref_rd_expiration", "").commit();
            pref_auth_real_debrid.setTitle((CharSequence) "Login to Real-Debird");
            FreeMoviesApp.l().edit().putBoolean("pref_show_debrid_only", false).apply();
            FragmentActivity activity = getActivity();
            Utils.a((Activity) activity, "RealDebrid Error " + th.getMessage());
        }
    }
}
