package com.movie.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.palette.graphics.Palette;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.database.entitys.MovieEntity;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.ui.fragment.MovieFragment;
import com.movie.ui.helper.MoviesHelper;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.http.HttpHelper;
import com.utils.PosterCacheHelper;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public final class MovieDetailsActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private MovieEntity f5098a;
    @BindView(2131296374)
    AppBarLayout appBarLayout;
    private SessionManagerListener b = null;
    /* access modifiers changed from: private */
    public CastSession c = null;
    @BindView(2131296507)
    CollapsingToolbarLayout collapsingToolbarLayout;
    private CastContext d = null;
    /* access modifiers changed from: private */
    public IntroductoryOverlay e;
    /* access modifiers changed from: private */
    public MenuItem f;
    private CastStateListener g;
    @Inject
    TMDBApi h;
    @Inject
    MoviesHelper i;
    CompositeDisposable j;
    @BindView(2131297160)
    ImageView toolbar_image;
    @BindView(2131297198)
    TextView tv_genres_duration;

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    private void d() {
        this.b = new SessionManagerListener<CastSession>() {
            /* renamed from: a */
            public void onSessionEnding(CastSession castSession) {
            }

            /* renamed from: a */
            public void onSessionEnded(CastSession castSession, int i) {
                a();
            }

            /* renamed from: a */
            public void onSessionResuming(CastSession castSession, String str) {
            }

            /* renamed from: b */
            public void onSessionStarting(CastSession castSession) {
            }

            /* renamed from: b */
            public void onSessionResumeFailed(CastSession castSession, int i) {
                a();
            }

            /* renamed from: c */
            public void onSessionStartFailed(CastSession castSession, int i) {
                a();
            }

            /* renamed from: d */
            public void onSessionSuspended(CastSession castSession, int i) {
            }

            private void c(CastSession castSession) {
                CastSession unused = MovieDetailsActivity.this.c = castSession;
                Utils.a((Activity) MovieDetailsActivity.this, (int) R.string.cast_connected);
            }

            /* renamed from: a */
            public void onSessionResumed(CastSession castSession, boolean z) {
                c(castSession);
            }

            /* renamed from: b */
            public void onSessionStarted(CastSession castSession, String str) {
                c(castSession);
            }

            private void a() {
                CastSession unused = MovieDetailsActivity.this.c = null;
                MovieDetailsActivity.this.invalidateOptionsMenu();
            }
        };
    }

    /* access modifiers changed from: private */
    public void e() {
        IntroductoryOverlay introductoryOverlay = this.e;
        if (introductoryOverlay != null) {
            introductoryOverlay.remove();
        }
        MenuItem menuItem = this.f;
        if (menuItem != null && menuItem.isVisible()) {
            new Handler().post(new Runnable() {
                public void run() {
                    MovieDetailsActivity movieDetailsActivity = MovieDetailsActivity.this;
                    IntroductoryOverlay.Builder builder = new IntroductoryOverlay.Builder(movieDetailsActivity, movieDetailsActivity.f);
                    builder.a(MovieDetailsActivity.this.getString(R.string.introducing_cast));
                    builder.a((int) R.color.theme_primary);
                    builder.c();
                    builder.a((IntroductoryOverlay.OnOverlayDismissedListener) new IntroductoryOverlay.OnOverlayDismissedListener() {
                        public void a() {
                            IntroductoryOverlay unused = MovieDetailsActivity.this.e = null;
                        }
                    });
                    IntroductoryOverlay unused = movieDetailsActivity.e = builder.a();
                    MovieDetailsActivity.this.e.show();
                }
            });
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        CastContext castContext = this.d;
        if (castContext != null) {
            return castContext.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_movie_details);
        if (RealDebridCredentialsHelper.c().isValid()) {
            Utils.f6550a = 250;
        }
        boolean z = false;
        boolean z2 = FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false);
        boolean z3 = FreeMoviesApp.l().getBoolean("pref_low_profilev2", Build.VERSION.SDK_INT <= 20);
        if (!z2 && !z3) {
            Utils.f6550a = 1000;
        }
        this.j = new CompositeDisposable();
        Bundle extras = getIntent().getExtras();
        if (extras.getBoolean("isFromAnotherApp")) {
            this.f5098a = (MovieEntity) new Gson().a(extras.getString("com.freeapp.freemovies.extras.EXTRA_MOVIE"), MovieEntity.class);
        } else {
            this.f5098a = (MovieEntity) getIntent().getParcelableExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE");
        }
        if (bundle == null) {
            MovieFragment c2 = MovieFragment.c(this.f5098a);
            c2.a(this.appBarLayout);
            getSupportFragmentManager().b().b(R.id.movie_details_container, c2, "fragment_movie").a();
        }
        Toolbar toolbar = this.mToolbar;
        if (toolbar != null) {
            ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
            this.mToolbar.setNavigationOnClickListener(new f0(this));
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.a(0, 8);
                supportActionBar.d(true);
                supportActionBar.e(true);
            }
            this.collapsingToolbarLayout.setTitle(this.f5098a.getName());
        }
        if (Utils.z()) {
            try {
                d();
                this.d = CastContext.a((Context) this);
                this.c = this.d.c().a();
                this.g = new CastStateListener() {
                    public void a(int i) {
                        if (i != 1) {
                            MovieDetailsActivity.this.e();
                        }
                    }
                };
            } catch (Exception unused) {
                this.d = null;
            }
        }
        if (this.f5098a.getBackdrop_path() != null && !this.f5098a.getBackdrop_path().isEmpty()) {
            z = true;
        }
        if (this.f5098a.getBackdrop_path() == null || this.f5098a.getBackdrop_path().isEmpty()) {
            String b2 = PosterCacheHelper.a().b(this.f5098a.getTmdbID(), this.f5098a.getTvdbID(), this.f5098a.getImdbIDStr());
            if (b2 != null) {
                Glide.a((FragmentActivity) this).a(b2).a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).a((int) R.color.list_dropdown_foreground_selected)).b()).a(new DrawableTransitionOptions().b()).a(this.toolbar_image);
            }
        } else if (z) {
            c(this.f5098a.getBackdrop_path());
        }
        this.j.b(this.h.getMovieDetails(this.f5098a.getTmdbID(), (String) null).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new d0(this, z), e0.f5221a));
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_movie_details, menu);
        this.f = CastButtonFactory.a(getApplicationContext(), menu, (int) R.id.media_route_menu_item);
        e();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        this.j.dispose();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        CastContext castContext;
        if (Utils.z() && (castContext = this.d) != null) {
            castContext.c().b(this.b, CastSession.class);
            this.d.b(this.g);
        }
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        CastContext castContext;
        if (Utils.z() && (castContext = this.d) != null) {
            castContext.c().a(this.b, CastSession.class);
            this.d.a(this.g);
            CastSession castSession = this.c;
            if (castSession == null || !castSession.b()) {
                Log.i("MOVIES_TAG", "CAST SESSION RESUME DIS_CONNECTED");
            } else {
                Log.i("MOVIES_TAG", "CAST SESSION RESUME CONNECTED");
            }
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        HttpHelper.e().a(HttpHelper.d);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    private void c(String str) {
        Glide.a((FragmentActivity) this).a(str).a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).a((int) R.color.list_dropdown_foreground_selected)).b()).a(new DrawableTransitionOptions().b()).b(GlidePalette.a(str).a((BitmapPalette.CallBack) new BitmapPalette.CallBack() {
            public void a(Palette palette) {
                MovieDetailsActivity.this.a(palette.b());
            }
        })).a(this.toolbar_image);
    }

    public /* synthetic */ void a(View view) {
        finish();
    }

    public /* synthetic */ void a(boolean z, MovieTMDB.ResultsBean resultsBean) throws Exception {
        this.f5098a.setDuration(resultsBean.getRuntime() * 60);
        StringBuilder sb = new StringBuilder();
        sb.append(Utils.a(this.f5098a.getGenres(), ".", new StringBuilder(30)));
        sb.append(" - ");
        sb.append((resultsBean.getRuntime() / 60) + "hr " + (resultsBean.getRuntime() % 60) + "min");
        this.tv_genres_duration.setText(sb.toString());
        PosterCacheHelper.a().a((long) resultsBean.getId(), -1, resultsBean.getImdb_id(), resultsBean.getPoster_path(), resultsBean.getBackdrop_path());
        if (!z) {
            this.f5098a.setBackdrop_path(resultsBean.getBackdrop_path());
            c(this.f5098a.getBackdrop_path());
        }
    }

    public boolean c() {
        return this.c != null;
    }

    /* access modifiers changed from: private */
    public void a(Palette.Swatch swatch) {
        if (FreeMoviesApp.l().getBoolean("pref_change_bg_color", true) && swatch != null) {
            getWindow().getDecorView().setBackgroundColor(swatch.d());
            TextView textView = this.tv_genres_duration;
            if (textView != null) {
                textView.setBackgroundColor(swatch.d());
                this.tv_genres_duration.setTextColor(swatch.e());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0005, code lost:
        r0 = r0.h();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.google.android.gms.cast.MediaInfo r4, int r5, boolean r6) {
        /*
            r3 = this;
            com.google.android.gms.cast.framework.CastSession r0 = r3.c
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r0.h()
            if (r0 != 0) goto L_0x000c
            return
        L_0x000c:
            com.movie.ui.activity.MovieDetailsActivity$6 r1 = new com.movie.ui.activity.MovieDetailsActivity$6
            r1.<init>(r0)
            r0.a((com.google.android.gms.cast.framework.media.RemoteMediaClient.Listener) r1)
            long r1 = (long) r5
            r0.a((com.google.android.gms.cast.MediaInfo) r4, (boolean) r6, (long) r1)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.activity.MovieDetailsActivity.a(com.google.android.gms.cast.MediaInfo, int, boolean):void");
    }
}
