package com.movie.ui.activity;

import com.movie.data.api.tmdb.TMDBApi;
import com.movie.ui.helper.MoviesHelper;
import dagger.MembersInjector;

public final class MovieDetailsActivity_MembersInjector implements MembersInjector<MovieDetailsActivity> {
    public static void a(MovieDetailsActivity movieDetailsActivity, TMDBApi tMDBApi) {
        movieDetailsActivity.h = tMDBApi;
    }

    public static void a(MovieDetailsActivity movieDetailsActivity, MoviesHelper moviesHelper) {
        movieDetailsActivity.i = moviesHelper;
    }
}
