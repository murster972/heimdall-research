package com.movie.ui.activity.gamechallenge;

import android.graphics.drawable.Drawable;

public class PromotionAppModel {

    /* renamed from: a  reason: collision with root package name */
    private String f5236a;
    private String b;
    private String c;
    private Drawable d;
    private String e;

    public void a(Drawable drawable) {
        this.d = drawable;
    }

    public Drawable b() {
        return this.d;
    }

    public void c(String str) {
        this.c = str;
    }

    public String d() {
        return this.c;
    }

    public String e() {
        return this.b;
    }

    public String a() {
        return this.f5236a;
    }

    public void b(String str) {
        this.e = str;
    }

    public String c() {
        return this.e;
    }

    public void d(String str) {
        this.b = str;
    }

    public void a(String str) {
        this.f5236a = str;
    }
}
