package com.movie.ui.activity.gamechallenge;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.movie.AppComponent;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.gamechallenge.GameChallengeModel;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.activity.DaggerBaseActivityComponent;
import com.movie.ui.activity.gamechallenge.GameItemAdapter;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

public class GameChallenge extends BaseActivity implements GameItemAdapter.GameAdapterListener {

    /* renamed from: a  reason: collision with root package name */
    GameItemAdapter f5232a;
    @Inject
    MoviesApi b;
    CompositeDisposable c;
    @BindView(2131296999)
    RecyclerView rvApllication;

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    public /* synthetic */ void a(PackageManager packageManager, List list, GameChallengeModel gameChallengeModel) throws Exception {
        if (gameChallengeModel != null) {
            for (GameChallengeModel.AndroidBean next : gameChallengeModel.getAndroid()) {
                PromotionAppModel promotionAppModel = new PromotionAppModel();
                promotionAppModel.d(next.getPackageX());
                promotionAppModel.c(next.getName());
                promotionAppModel.a(next.getDescription());
                promotionAppModel.b(next.getIcon());
                try {
                    PackageInfo packageInfo = packageManager.getPackageInfo(next.getPackageX(), 0);
                    if (packageInfo != null) {
                        promotionAppModel.a(packageInfo.applicationInfo.loadIcon(packageManager));
                    }
                } catch (Exception unused) {
                }
                list.add(promotionAppModel);
                this.f5232a.a((List<PromotionAppModel>) list);
            }
        }
        this.f5232a.notifyDataSetChanged();
    }

    /* access modifiers changed from: package-private */
    public List<PromotionAppModel> c() {
        ArrayList arrayList = new ArrayList();
        this.c.b(this.b.getGameChallengeList("android", Utils.f()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new d(this, getPackageManager(), arrayList), c.f5239a));
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_game_challenge);
        this.c = new CompositeDisposable();
        this.rvApllication.setLayoutManager(new LinearLayoutManager(this));
        this.f5232a = GameItemAdapter.a((List<PromotionAppModel>) new ArrayList(), (Context) this);
        this.rvApllication.setAdapter(this.f5232a);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.c.dispose();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        c();
        this.f5232a.a((GameItemAdapter.GameAdapterListener) this);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    public void a(PromotionAppModel promotionAppModel) {
        Utils.a((Activity) this, promotionAppModel.e());
        this.c.b(this.b.getGameChallengeData(promotionAppModel.e(), Utils.f()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(b.f5238a, a.f5237a));
    }
}
