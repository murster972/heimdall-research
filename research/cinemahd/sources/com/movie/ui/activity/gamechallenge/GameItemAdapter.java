package com.movie.ui.activity.gamechallenge;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.List;

public class GameItemAdapter extends RecyclerView.Adapter<ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private List<PromotionAppModel> f5233a;
    /* access modifiers changed from: private */
    public GameAdapterListener b;
    Context c;

    interface GameAdapterListener {
        void a(PromotionAppModel promotionAppModel);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        ImageView f5235a;
        ImageButton b;
        TextView c;
        TextView d;

        public ViewHolder(GameItemAdapter gameItemAdapter, View view) {
            super(view);
            this.f5235a = (ImageView) view.findViewById(R.id.imgIcon);
            this.b = (ImageButton) view.findViewById(R.id.btnAction);
            this.c = (TextView) view.findViewById(R.id.txtName);
            this.d = (TextView) view.findViewById(R.id.txtDescription);
        }
    }

    public int getItemCount() {
        return this.f5233a.size();
    }

    public static GameItemAdapter a(List<PromotionAppModel> list, Context context) {
        GameItemAdapter gameItemAdapter = new GameItemAdapter();
        gameItemAdapter.f5233a = list;
        gameItemAdapter.c = context;
        return gameItemAdapter;
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.game_item, viewGroup, false));
    }

    public void a(List<PromotionAppModel> list) {
        this.f5233a = list;
    }

    public void a(GameAdapterListener gameAdapterListener) {
        this.b = gameAdapterListener;
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final PromotionAppModel promotionAppModel = this.f5233a.get(i);
        viewHolder.c.setText(promotionAppModel.d());
        viewHolder.d.setText(promotionAppModel.a());
        if (promotionAppModel.b() != null) {
            viewHolder.f5235a.setImageDrawable(promotionAppModel.b());
        } else if (!promotionAppModel.c().isEmpty()) {
            Glide.d(this.c).a(promotionAppModel.c()).a((BaseRequestOptions<?>) ((RequestOptions) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).a((int) R.color.list_dropdown_foreground_selected)).b()).a(new DrawableTransitionOptions().b()).a(viewHolder.f5235a);
        }
        if (Utils.i(promotionAppModel.e())) {
            viewHolder.b.setImageDrawable(this.c.getResources().getDrawable(R.drawable.ic_play_circle_filled_black_48dp));
        } else {
            viewHolder.b.setImageDrawable(this.c.getResources().getDrawable(R.drawable.ic_download));
        }
        if (this.b != null) {
            viewHolder.b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    GameItemAdapter.this.b.a(promotionAppModel);
                }
            });
        }
    }
}
