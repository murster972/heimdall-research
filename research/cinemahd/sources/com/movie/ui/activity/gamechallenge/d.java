package com.movie.ui.activity.gamechallenge;

import android.content.pm.PackageManager;
import com.movie.data.model.gamechallenge.GameChallengeModel;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ GameChallenge f5240a;
    private final /* synthetic */ PackageManager b;
    private final /* synthetic */ List c;

    public /* synthetic */ d(GameChallenge gameChallenge, PackageManager packageManager, List list) {
        this.f5240a = gameChallenge;
        this.b = packageManager;
        this.c = list;
    }

    public final void accept(Object obj) {
        this.f5240a.a(this.b, this.c, (GameChallengeModel) obj);
    }
}
