package com.movie.ui.activity;

import com.movie.data.api.MoviesApi;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.helper.MoviesHelper;
import dagger.MembersInjector;

public final class PlayerActivity_MembersInjector implements MembersInjector<PlayerActivity> {
    public static void a(PlayerActivity playerActivity, MoviesRepository moviesRepository) {
        playerActivity.t = moviesRepository;
    }

    public static void a(PlayerActivity playerActivity, MoviesApi moviesApi) {
        playerActivity.u = moviesApi;
    }

    public static void a(PlayerActivity playerActivity, MoviesHelper moviesHelper) {
        playerActivity.v = moviesHelper;
    }
}
