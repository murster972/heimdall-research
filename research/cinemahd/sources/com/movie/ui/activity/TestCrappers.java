package com.movie.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import butterknife.BindView;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.providers.Provider;
import com.movie.data.model.providers.TProviderData;
import com.movie.data.remotejs.MyTaskService;
import com.original.tase.Logger;
import com.vungle.warren.model.ReportDBAdapter;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TestCrappers extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    CompositeDisposable f5194a = new CompositeDisposable();
    @Inject
    IMDBApi b;
    @Inject
    TMDBApi c;
    @BindView(2131296772)
    ListView lvSources;

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    private void c(final String str) {
        this.f5194a.b(Observable.create(new ObservableOnSubscribe<TProviderData>(this) {
            public void subscribe(ObservableEmitter<TProviderData> observableEmitter) throws Exception {
                Response execute = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).writeTimeout(10, TimeUnit.SECONDS).readTimeout(30, TimeUnit.SECONDS).build().newCall(new Request.Builder().url(str).build()).execute();
                if (execute.code() == 200 && execute.body() != null) {
                    observableEmitter.onNext((TProviderData) new Gson().a(execute.body().string(), TProviderData.class));
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new x2(this), y2.f5513a));
    }

    public /* synthetic */ void a(TProviderData tProviderData) throws Exception {
        Logger.a(tProviderData.toString());
        for (Map.Entry<String, Provider> value : tProviderData.getProviders().entrySet()) {
            Intent intent = new Intent(getApplicationContext(), MyTaskService.class);
            Bundle bundle = new Bundle();
            bundle.putString(ReportDBAdapter.ReportColumns.COLUMN_URL, ((Provider) value.getValue()).getUrl());
            intent.putExtras(bundle);
            getApplicationContext().startService(intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_test_crappers);
        ((FreeMoviesApp) getApplication()).getReactNativeHost();
        c("https://raw.githubusercontent.com/orchidshl1/free/master/add-ons/es5/addon_list.json?fbclid=IwAR2ec7oTtCJTL5PlypUBKyRU10bLcyMrz7ui_ZwIPSn-3lrKpPhOu8IWupE");
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f5194a.dispose();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }
}
