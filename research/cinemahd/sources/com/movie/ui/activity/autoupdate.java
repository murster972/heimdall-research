package com.movie.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.core.content.FileProvider;
import com.facebook.common.util.ByteConstants;
import com.facebook.imagepipeline.producers.HttpUrlConnectionNetworkFetcher;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.utils.PrefUtils;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class autoupdate {

    /* renamed from: a  reason: collision with root package name */
    public static ProgressBar f5203a = null;
    public static TextView b = null;
    public static ImageView c = null;
    public static DownloadTask d = null;
    public static Dialog e = null;
    public static Dialog f = null;
    public static Dialog g = null;
    public static Dialog h = null;
    public static Activity i = null;
    public static String j = "";
    public static String k = "";
    public static Integer l = 100;
    public static String m = "";
    public static String n = "";
    public static int o = 22597408;
    public static int p = 0;
    public static boolean q = false;
    public static int r = 0;

    public static boolean a(Activity activity, boolean z) {
        String str;
        int v = Utils.v();
        i = activity;
        l = Integer.valueOf(GlobalVariable.c().a().getUpdate().getVersionCode());
        m = GlobalVariable.c().a().getUpdate().getLink();
        n = GlobalVariable.c().a().getUpdate().getDescription();
        o = GlobalVariable.c().a().getUpdate().getSize();
        q = GlobalVariable.c().a().getUpdate().isForceUpdate();
        int i2 = FreeMoviesApp.l().getInt("pref_clean_apk", -1);
        if (i2 < 0) {
            for (int i3 = 55; i3 <= l.intValue(); i3++) {
                a(String.format(b(), new Object[]{Integer.valueOf(i3)}));
            }
            FreeMoviesApp.l().edit().putInt("pref_clean_apk", l.intValue()).apply();
        } else {
            a(String.format(b(), new Object[]{Integer.valueOf(i2)}));
        }
        if ((!q && !z && PrefUtils.f(i)) || l.intValue() <= v || (str = m) == null || str.equals("")) {
            return false;
        }
        b(activity);
        return true;
    }

    public static void b(final Activity activity) {
        i = activity;
        g = new Dialog(activity);
        g.requestWindowFeature(1);
        g.setCancelable(false);
        g.setContentView(R.layout.dialog_autoupdate);
        TextView textView = (TextView) g.findViewById(R.id.message_autoupdate);
        String str = n;
        if (str != null && !str.equals("")) {
            textView.setText(n);
        }
        Button button = (Button) g.findViewById(R.id.btn_update);
        Button button2 = (Button) g.findViewById(R.id.diag_btn_autoupdate_cancel);
        CheckBox checkBox = (CheckBox) g.findViewById(R.id.autoupdate_checkBox4);
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                if (compoundButton.isChecked()) {
                    PrefUtils.a((Context) autoupdate.i, true);
                } else {
                    PrefUtils.a((Context) autoupdate.i, false);
                }
            }
        });
        if (q) {
            checkBox.setVisibility(8);
            button2.setVisibility(8);
        } else {
            a(activity, g, 1);
            button2.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    autoupdate.g.dismiss();
                }
            });
        }
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                autoupdate.g.dismiss();
                autoupdate.g = null;
                autoupdate.a(activity);
            }
        });
        button.requestFocus();
        g.show();
    }

    public static void c(Activity activity) {
        i = activity;
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_autoupdate_unknowsource);
        ((Button) dialog.findViewById(R.id.btn_update_unknowsource_oke)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                dialog.dismiss();
                autoupdate.c();
            }
        });
        dialog.show();
    }

    public static void d(Activity activity) {
        if (f == null) {
            d.a();
            f = new Dialog(activity);
            f.requestWindowFeature(1);
            f.setCancelable(false);
            f.setContentView(R.layout.dialog_confirm_yesno);
            ((Button) f.findViewById(R.id.diag_btn_confirm_no)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    autoupdate.d.b();
                    autoupdate.f.dismiss();
                    autoupdate.f = null;
                }
            });
            ((Button) f.findViewById(R.id.diag_btnconfirm_yes)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    autoupdate.d.cancel(true);
                    autoupdate.e.dismiss();
                    autoupdate.f.dismiss();
                    autoupdate.f = null;
                }
            });
            f.show();
        }
    }

    public static void e(final Activity activity) {
        h = new Dialog(activity);
        h.requestWindowFeature(1);
        h.setCancelable(false);
        h.setContentView(R.layout.dialog_autoupdate_error);
        a(activity, h, 4);
        ((Button) h.findViewById(R.id.btn_update_error)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                autoupdate.h.dismiss();
                autoupdate.a(activity);
            }
        });
        ((Button) h.findViewById(R.id.dig_autoupdate_error_cancel)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                autoupdate.h.dismiss();
            }
        });
        h.show();
    }

    private static class DownloadTask extends AsyncTask<String, Integer, Long> {

        /* renamed from: a  reason: collision with root package name */
        public boolean f5207a;
        public double b;

        private DownloadTask() {
            this.f5207a = false;
            this.b = (double) System.currentTimeMillis();
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public Long doInBackground(String... strArr) {
            long j;
            int length = strArr.length;
            try {
                autoupdate.j = autoupdate.i.getExternalFilesDir((String) null).toString() + "/" + autoupdate.l + ".apk";
                j = (long) a(strArr[0]);
            } catch (IOException e) {
                e.printStackTrace();
                j = -1;
            }
            return Long.valueOf(j);
        }

        public void b() {
            this.f5207a = false;
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onProgressUpdate(Integer... numArr) {
            double currentTimeMillis = (double) System.currentTimeMillis();
            if (currentTimeMillis - this.b >= 100.0d) {
                autoupdate.a(numArr[0].intValue());
                this.b = currentTimeMillis;
            }
        }

        /* access modifiers changed from: protected */
        /* renamed from: a */
        public void onPostExecute(Long l) {
            if (l.longValue() == 1) {
                if (autoupdate.a()) {
                    autoupdate.c();
                } else {
                    autoupdate.c(autoupdate.i);
                }
                autoupdate.e.dismiss();
                return;
            }
            autoupdate.e.dismiss();
            autoupdate.e(autoupdate.i);
        }

        public int a(String str) throws IOException {
            HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
            httpURLConnection.setConnectTimeout(HttpUrlConnectionNetworkFetcher.HTTP_DEFAULT_TIMEOUT);
            int responseCode = httpURLConnection.getResponseCode();
            if (responseCode == 200) {
                httpURLConnection.getHeaderField("Content-Disposition");
                httpURLConnection.getContentType();
                httpURLConnection.getContentLength();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(httpURLConnection.getInputStream());
                new BufferedInputStream(httpURLConnection.getInputStream());
                FileOutputStream fileOutputStream = new FileOutputStream(autoupdate.j);
                float f = 0.0f;
                autoupdate.p = bufferedInputStream.available();
                byte[] bArr = new byte[ByteConstants.KB];
                if (autoupdate.p == 0) {
                    autoupdate.p = autoupdate.o;
                }
                int read = bufferedInputStream.read(bArr);
                while (read != -1) {
                    if (!this.f5207a) {
                        f += (float) read;
                        publishProgress(new Integer[]{Integer.valueOf((int) f)});
                        fileOutputStream.write(bArr, 0, read);
                        read = bufferedInputStream.read(bArr);
                    }
                }
                publishProgress(new Integer[]{100});
                fileOutputStream.close();
                bufferedInputStream.close();
                return 1;
            }
            PrintStream printStream = System.out;
            printStream.println("No file to download. Server replied HTTP code: " + responseCode);
            httpURLConnection.disconnect();
            return -1;
        }

        public void a() {
            this.f5207a = true;
        }
    }

    public static void c() {
        Uri uri;
        try {
            FreeMoviesApp.l().edit().putInt("pref_clean_apk", l.intValue()).apply();
            File file = new File(j);
            Intent intent = new Intent();
            intent.setAction("android.intent.action.INSTALL_PACKAGE");
            if (Build.VERSION.SDK_INT < 24) {
                uri = Uri.fromFile(file);
            } else {
                Activity activity = i;
                uri = FileProvider.a(activity, i.getPackageName() + ".provider", file);
                intent.setFlags(1);
            }
            intent.setDataAndType(uri, "application/vnd.android.package-archive");
            intent.addFlags(268468224);
            intent.putExtra("android.intent.extra.INSTALLER_PACKAGE_NAME", i.getPackageName());
            intent.putExtra("android.intent.extra.NOT_UNKNOWN_SOURCE", true);
            i.startActivityForResult(intent, 444);
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }

    public static void a(Activity activity, Dialog dialog, int i2) {
        r = i2;
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                if (i != 4) {
                    return false;
                }
                int i2 = autoupdate.r;
                if (i2 == 1) {
                    autoupdate.g.dismiss();
                } else if (i2 == 2) {
                    autoupdate.d(autoupdate.i);
                } else if (i2 == 3) {
                    autoupdate.f.dismiss();
                } else if (i2 == 4) {
                    autoupdate.h.dismiss();
                }
                return true;
            }
        });
    }

    public static void a(Activity activity) {
        e = new Dialog(activity);
        e.requestWindowFeature(1);
        e.setCancelable(false);
        e.setContentView(R.layout.dialog_autoupdate_processdownload);
        f5203a = (ProgressBar) e.findViewById(R.id.autoupdate_progressBar);
        c = (ImageView) e.findViewById(R.id.diag_image_close);
        b = (TextView) e.findViewById(R.id.diag_tv_prosess_kb);
        if (q) {
            c.setVisibility(8);
        } else {
            a(activity, e, 2);
        }
        c.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                autoupdate.d.a();
                autoupdate.d(autoupdate.i);
            }
        });
        f5203a.setProgress(1);
        e.show();
        String str = m;
        a(j);
        d = new DownloadTask();
        d.execute(new String[]{str});
    }

    public static String b() {
        if (k.isEmpty()) {
            k = i.getExternalFilesDir((String) null).toString() + "/%s.apk";
        }
        return k;
    }

    public static void a(int i2) {
        int i3 = (int) ((((float) i2) / ((float) p)) * 100.0f);
        if (i3 > 2) {
            TextView textView = b;
            textView.setText("" + (i2 / ByteConstants.KB) + "kb");
            b.invalidate();
            f5203a.setProgress(i3);
        }
    }

    public static void a(String str) {
        try {
            File file = new File(str);
            if (file.exists() && !file.isDirectory()) {
                file.delete();
            }
        } catch (Throwable unused) {
        }
    }

    public static boolean a() {
        try {
            return Settings.Secure.getInt(i.getContentResolver(), "install_non_market_apps") == 1;
        } catch (Settings.SettingNotFoundException e2) {
            e2.printStackTrace();
            return false;
        }
    }
}
