package com.movie.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import com.ads.videoreward.AcolonyAds;
import com.ads.videoreward.AdsBase;
import com.ads.videoreward.ChartboostAds;
import com.ads.videoreward.Unity_Ads;
import com.ads.videoreward.VungleAds;
import com.movie.AppComponent;
import com.movie.data.api.GlobalVariable;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class AdsActivity extends BaseActivity implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    Map<AdsBase.Tag, AdsBase> f5049a = new HashMap();
    Button b;
    Button c;
    Button d;
    Button e;

    public AdsActivity() {
        new ArrayList();
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
    }

    /* access modifiers changed from: package-private */
    public void c() {
        if (GlobalVariable.c().a().getAds().getAdcolony().isEnable()) {
            AcolonyAds acolonyAds = new AcolonyAds();
            acolonyAds.d();
            this.f5049a.put(AdsBase.Tag.ADCOLONY, acolonyAds);
        }
        if (GlobalVariable.c().a().getAds().getVungle().isEnable()) {
            VungleAds vungleAds = new VungleAds();
            vungleAds.d();
            this.f5049a.put(AdsBase.Tag.VUNGLE, vungleAds);
        }
        if (GlobalVariable.c().a().getAds().getUnity_ads().isEnable()) {
            Unity_Ads unity_Ads = new Unity_Ads();
            unity_Ads.d();
            this.f5049a.put(AdsBase.Tag.UNITY, unity_Ads);
        }
        if (GlobalVariable.c().a().getAds().getChartBoost().isEnable()) {
            ChartboostAds chartboostAds = new ChartboostAds();
            chartboostAds.d();
            this.f5049a.put(AdsBase.Tag.CHARTBOOST, chartboostAds);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (Map.Entry<AdsBase.Tag, AdsBase> value : this.f5049a.entrySet()) {
            ((AdsBase) value.getValue()).a(i, i2, intent);
        }
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.adcolony_btn:
                this.f5049a.get(AdsBase.Tag.ADCOLONY).k();
                return;
            case R.id.chartboost_btn:
                this.f5049a.get(AdsBase.Tag.CHARTBOOST).k();
                return;
            case R.id.unityads_btn:
                this.f5049a.get(AdsBase.Tag.UNITY).k();
                return;
            case R.id.vungle_btn:
                this.f5049a.get(AdsBase.Tag.VUNGLE).k();
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_ads);
        this.b = (Button) findViewById(R.id.adcolony_btn);
        this.c = (Button) findViewById(R.id.unityads_btn);
        this.d = (Button) findViewById(R.id.vungle_btn);
        this.e = (Button) findViewById(R.id.chartboost_btn);
        this.b.setVisibility(4);
        this.c.setVisibility(4);
        this.d.setVisibility(4);
        this.e.setVisibility(4);
        this.b.setOnClickListener(this);
        this.c.setOnClickListener(this);
        this.d.setOnClickListener(this);
        this.e.setOnClickListener(this);
        c();
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        super.onDestroy();
        for (Map.Entry<AdsBase.Tag, AdsBase> value : this.f5049a.entrySet()) {
            ((AdsBase) value.getValue()).e();
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        for (Map.Entry<AdsBase.Tag, AdsBase> value : this.f5049a.entrySet()) {
            ((AdsBase) value.getValue()).f();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        for (Map.Entry<AdsBase.Tag, AdsBase> value : this.f5049a.entrySet()) {
            ((AdsBase) value.getValue()).g();
        }
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        for (Map.Entry<AdsBase.Tag, AdsBase> value : this.f5049a.entrySet()) {
            ((AdsBase) value.getValue()).h();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        for (Map.Entry<AdsBase.Tag, AdsBase> value : this.f5049a.entrySet()) {
            ((AdsBase) value.getValue()).i();
        }
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }
}
