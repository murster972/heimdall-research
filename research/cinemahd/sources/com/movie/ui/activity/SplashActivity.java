package com.movie.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.security.ProviderInstaller;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.original.tase.Logger;
import com.original.tase.utils.NetworkUtils;
import com.utils.PermissionHelper;
import com.utils.PrefUtils;
import com.utils.Utils;
import com.utils.download.DownloadActivity;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yoku.marumovie.R;
import java.util.Calendar;

public class SplashActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private long f5190a = 0;

    private void d() {
        Utils.ac(PrefUtils.b(this));
        if (!FreeMoviesApp.l().getBoolean("PREF_KEY_SHORTCUT_ADDED", false)) {
            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
            intent.addFlags(268435456);
            intent.addFlags(67108864);
            Intent intent2 = new Intent();
            intent2.putExtra("android.intent.extra.shortcut.INTENT", intent);
            intent2.putExtra("android.intent.extra.shortcut.NAME", getResources().getString(R.string.app_name));
            intent2.putExtra("android.intent.extra.shortcut.ICON_RESOURCE", Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_launcher));
            intent2.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            getApplicationContext().sendBroadcast(intent2);
            FreeMoviesApp.l().edit().putBoolean("PREF_KEY_SHORTCUT_ADDED", true).apply();
        }
    }

    public void c() {
        boolean z = true;
        if (Utils.B()) {
            if (!FreeMoviesApp.l().getBoolean("pref_low_profilev2", Build.VERSION.SDK_INT <= 20)) {
                z = false;
            }
        }
        if (z) {
            try {
                ProviderInstaller.a(this);
            } catch (GooglePlayServicesRepairableException e) {
                Logger.a((Throwable) e, new boolean[0]);
            } catch (GooglePlayServicesNotAvailableException e2) {
                Logger.a((Throwable) e2, new boolean[0]);
            }
        }
        this.f5190a = Calendar.getInstance().getTime().getTime();
        if (NetworkUtils.a()) {
            Utils.z();
            long time = Calendar.getInstance().getTime().getTime() - this.f5190a;
            new Handler().postDelayed(new Runnable() {
                public void run() {
                    SplashActivity.this.startActivity(new Intent(SplashActivity.this, MainActivity.class));
                }
            }, time < 200 ? 200 - time : 0);
            return;
        }
        LovelyStandardDialog lovelyStandardDialog = new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.HORIZONTAL);
        lovelyStandardDialog.e(R.color.indigo);
        LovelyStandardDialog lovelyStandardDialog2 = lovelyStandardDialog;
        lovelyStandardDialog2.h(R.color.darkDeepOrange);
        lovelyStandardDialog2.c((int) R.drawable.ic_star_border_white_36dp);
        LovelyStandardDialog lovelyStandardDialog3 = lovelyStandardDialog2;
        lovelyStandardDialog3.b((CharSequence) "Offline Mode");
        LovelyStandardDialog lovelyStandardDialog4 = lovelyStandardDialog3;
        lovelyStandardDialog4.a((CharSequence) "Network not available!, App will showVideo downloaded movie only!");
        LovelyStandardDialog lovelyStandardDialog5 = lovelyStandardDialog4;
        lovelyStandardDialog5.a(17039370, (View.OnClickListener) new View.OnClickListener() {
            public void onClick(View view) {
                Intent intent = new Intent(SplashActivity.this.getApplicationContext(), DownloadActivity.class);
                intent.setFlags(603979776);
                SplashActivity.this.startActivity(intent);
                SplashActivity.this.finish();
            }
        });
        lovelyStandardDialog5.d();
    }

    public Intent getParentActivityIntent() {
        return super.getParentActivityIntent().addFlags(67108864);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (getIntent().hasExtra("exit")) {
            finish();
        } else if (isTaskRoot() || ((!getIntent().hasCategory("android.intent.category.LEANBACK_LAUNCHER") && !getIntent().hasCategory("android.intent.category.LAUNCHER")) || getIntent().getAction() == null || !getIntent().getAction().equals("android.intent.action.MAIN"))) {
            if (PermissionHelper.b(this, 777)) {
                c();
            }
            if (FreeMoviesApp.l().getBoolean("pref_keep_alive", false)) {
                Utils.d((Context) this);
            }
        } else {
            finish();
        }
    }

    public void onDestroy() {
        super.onDestroy();
        Utils.ac(PrefUtils.b(this));
    }

    public void onPause() {
        super.onPause();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (i == 777) {
            if (iArr.length <= 0 || iArr[0] != 0) {
                LovelyStandardDialog lovelyStandardDialog = new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.HORIZONTAL);
                lovelyStandardDialog.e(R.color.indigo);
                LovelyStandardDialog lovelyStandardDialog2 = lovelyStandardDialog;
                lovelyStandardDialog2.h(R.color.darkDeepOrange);
                lovelyStandardDialog2.c((int) R.drawable.ic_star_border_white_36dp);
                LovelyStandardDialog lovelyStandardDialog3 = lovelyStandardDialog2;
                lovelyStandardDialog3.b((CharSequence) "Warning");
                LovelyStandardDialog lovelyStandardDialog4 = lovelyStandardDialog3;
                lovelyStandardDialog4.a((CharSequence) "Permission is not granted, you have to turn on storage permission");
                LovelyStandardDialog lovelyStandardDialog5 = lovelyStandardDialog4;
                lovelyStandardDialog5.a(17039370, (View.OnClickListener) new View.OnClickListener() {
                    public void onClick(View view) {
                        Intent intent = new Intent();
                        intent.setAction("android.settings.APPLICATION_DETAILS_SETTINGS");
                        intent.setData(Uri.fromParts("package", Utils.s(), (String) null));
                        SplashActivity.this.startActivity(intent);
                        SplashActivity.this.finishAffinity();
                    }
                });
                lovelyStandardDialog5.d();
                return;
            }
            c();
        }
    }

    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        d();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }
}
