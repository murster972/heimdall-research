package com.movie.ui.activity;

import com.movie.data.model.AppConfig;
import com.movie.ui.activity.MemberActivationActivity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class x implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MemberActivationActivity.AnonymousClass4 f5506a;
    private final /* synthetic */ String b;

    public /* synthetic */ x(MemberActivationActivity.AnonymousClass4 r1, String str) {
        this.f5506a = r1;
        this.b = str;
    }

    public final void accept(Object obj) {
        this.f5506a.a(this.b, (AppConfig) obj);
    }
}
