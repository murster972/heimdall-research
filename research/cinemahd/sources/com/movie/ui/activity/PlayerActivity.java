package com.movie.ui.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import com.database.entitys.MovieEntity;
import com.database.entitys.TvWatchedEpisode;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackPreparer;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.RenderersFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.analytics.AnalyticsListener;
import com.google.android.exoplayer2.drm.DefaultDrmSessionManager;
import com.google.android.exoplayer2.drm.DrmSessionManager;
import com.google.android.exoplayer2.drm.FrameworkMediaCrypto;
import com.google.android.exoplayer2.drm.FrameworkMediaDrm;
import com.google.android.exoplayer2.drm.HttpMediaDrmCallback;
import com.google.android.exoplayer2.drm.UnsupportedDrmException;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.mediacodec.MediaCodecRenderer;
import com.google.android.exoplayer2.mediacodec.MediaCodecUtil;
import com.google.android.exoplayer2.offline.FilteringManifestParser;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.source.BehindLiveWindowException;
import com.google.android.exoplayer2.source.ConcatenatingMediaSource;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.DefaultDashChunkSource;
import com.google.android.exoplayer2.source.dash.manifest.DashManifest;
import com.google.android.exoplayer2.source.dash.manifest.DashManifestParser;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.source.hls.playlist.DefaultHlsPlaylistParserFactory;
import com.google.android.exoplayer2.source.hls.playlist.HlsPlaylistParserFactory;
import com.google.android.exoplayer2.source.smoothstreaming.DefaultSsChunkSource;
import com.google.android.exoplayer2.source.smoothstreaming.SsMediaSource;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifest;
import com.google.android.exoplayer2.source.smoothstreaming.manifest.SsManifestParser;
import com.google.android.exoplayer2.text.CaptionStyleCompat;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.MappingTrackSelector;
import com.google.android.exoplayer2.trackselection.RandomTrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DataSpec;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.FileDataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.ParsingLoadable;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.google.android.exoplayer2.upstream.cache.CacheDataSourceFactory;
import com.google.android.exoplayer2.util.ErrorMessageProvider;
import com.google.android.exoplayer2.util.EventLogger;
import com.google.android.exoplayer2.util.Util;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.customdialog.CustomDialog;
import com.movie.ui.helper.MoviesHelper;
import com.original.Constants;
import com.original.tase.I18N;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.utils.SourceUtils;
import com.utils.IntentDataContainer;
import com.utils.OnSwipeTouchListener;
import com.utils.PermissionHelper;
import com.utils.Subtitle.ExpandableListSubtitleAdapter;
import com.utils.Subtitle.SubtitleInfo;
import com.utils.Subtitle.services.SubServiceBase;
import com.utils.Subtitle.subtitleView.CaptionsView;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.inject.Inject;
import okhttp3.CacheControl;
import timber.log.Timber;

public class PlayerActivity extends BaseActivity implements View.OnClickListener, PlaybackPreparer, PlayerControlView.VisibilityListener, CaptionsView.CaptionsViewLoadListener {
    private static final DefaultBandwidthMeter J = new DefaultBandwidthMeter();
    private static final CookieManager K = new CookieManager();
    CompositeDisposable A = null;
    /* access modifiers changed from: private */
    public MovieEntity B;
    /* access modifiers changed from: private */
    public boolean C = false;
    ArrayList<SubtitleInfo> D = null;
    /* access modifiers changed from: private */
    public int E = 1;
    /* access modifiers changed from: private */
    public TextView F;
    /* access modifiers changed from: private */
    public Window G;
    /* access modifiers changed from: private */
    public AudioManager H;
    boolean I = false;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public PlayerView f5105a;
    private DataSource.Factory b;
    /* access modifiers changed from: private */
    public SimpleExoPlayer c;
    private MediaSource d;
    /* access modifiers changed from: private */
    public DefaultTrackSelector e;
    private DefaultTrackSelector.Parameters f;
    /* access modifiers changed from: private */
    public TrackGroupArray g;
    private boolean h;
    private int i;
    /* access modifiers changed from: private */
    public long j;
    boolean k = false;
    /* access modifiers changed from: private */
    public MovieInfo l;
    int m = 0;
    ArrayList<com.original.tase.model.media.MediaSource> n;
    private ExpandableListView o;
    /* access modifiers changed from: private */
    public ExpandableListSubtitleAdapter p;
    Map<String, List<SubtitleInfo>> q = null;
    /* access modifiers changed from: private */
    public AlertDialog r;
    Dialog s = null;
    @Inject
    MoviesRepository t;
    @Inject
    MoviesApi u;
    @Inject
    MoviesHelper v;
    /* access modifiers changed from: private */
    public CaptionsView w;
    /* access modifiers changed from: private */
    public Dialog x = null;
    private boolean y = false;
    private SubtitleInfo z = null;

    private class PlayerErrorMessageProvider implements ErrorMessageProvider<ExoPlaybackException> {
        private PlayerErrorMessageProvider() {
        }

        public Pair<Integer, String> a(ExoPlaybackException exoPlaybackException) {
            String string = PlayerActivity.this.getString(R.string.error_generic);
            if (exoPlaybackException.type == 1) {
                Exception a2 = exoPlaybackException.a();
                if (a2 instanceof MediaCodecRenderer.DecoderInitializationException) {
                    MediaCodecRenderer.DecoderInitializationException decoderInitializationException = (MediaCodecRenderer.DecoderInitializationException) a2;
                    String str = decoderInitializationException.decoderName;
                    if (str != null) {
                        string = PlayerActivity.this.getString(R.string.error_instantiating_decoder, new Object[]{str});
                    } else if (decoderInitializationException.getCause() instanceof MediaCodecUtil.DecoderQueryException) {
                        string = PlayerActivity.this.getString(R.string.error_querying_decoders);
                    } else if (decoderInitializationException.secureDecoderRequired) {
                        string = PlayerActivity.this.getString(R.string.error_no_secure_decoder, new Object[]{decoderInitializationException.mimeType});
                    } else {
                        string = PlayerActivity.this.getString(R.string.error_no_decoder, new Object[]{decoderInitializationException.mimeType});
                    }
                }
            }
            return Pair.create(0, string);
        }
    }

    private class PlayerEventListener extends Player.DefaultEventListener {
        private PlayerEventListener() {
        }

        public /* synthetic */ void a(View view) {
            PlayerActivity.this.finish();
        }

        public void b(int i) {
            if (i == 1 || !(PlayerActivity.this.c == null || PlayerActivity.this.c.e() == null)) {
                PlayerActivity.this.b(false);
            }
            if (i == 0) {
                boolean z = FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
                if (!PlayerActivity.this.B.getTV().booleanValue() || !z) {
                    PlayerActivity.this.finish();
                    return;
                }
                PlayerActivity.this.setResult(-1, new Intent());
                PlayerActivity.this.finish();
            }
        }

        public void onPlayerError(ExoPlaybackException exoPlaybackException) {
            String str;
            PlayerActivity playerActivity = PlayerActivity.this;
            if (playerActivity.m >= playerActivity.n.size()) {
                if (PlayerActivity.b(exoPlaybackException)) {
                    PlayerActivity.this.d();
                    PlayerActivity.this.e();
                } else {
                    PlayerActivity.this.b(false);
                    PlayerActivity.this.k();
                    PlayerActivity.this.i();
                }
                if (!PlayerActivity.this.C) {
                    if (PlayerActivity.this.x == null) {
                        PlayerActivity playerActivity2 = PlayerActivity.this;
                        Dialog unused = playerActivity2.x = CustomDialog.a(playerActivity2, "Playback failed, Please back and select another server...");
                    }
                    PlayerActivity.this.x.show();
                    PlayerActivity.this.setResult(0, new Intent());
                    boolean unused2 = PlayerActivity.this.C = true;
                    return;
                }
                return;
            }
            PlayerActivity.this.b(false);
            PlayerActivity playerActivity3 = PlayerActivity.this;
            long unused3 = playerActivity3.j = Math.max(5, playerActivity3.c.x() ? PlayerActivity.this.j : Math.min(5, PlayerActivity.this.c.getDuration()));
            try {
                PlayerActivity.this.m++;
                while (!PlayerActivity.this.e(PlayerActivity.this.m).isResolved()) {
                    PlayerActivity.this.m++;
                }
                if (PlayerActivity.this.m >= PlayerActivity.this.n.size()) {
                    PlayerActivity.this.m = PlayerActivity.this.n.size() - 1;
                    PlayerActivity.this.e("Something was wrong, Please back and select another server...");
                    return;
                }
                PlayerActivity.this.c.a(PlayerActivity.this.m, PlayerActivity.this.j - 5);
                PlayerActivity.this.c.z();
                if (PlayerActivity.this.mToolbar != null) {
                    if (PlayerActivity.this.l.session.isEmpty()) {
                        str = "";
                    } else {
                        str = " [" + PlayerActivity.this.l.session + "x" + PlayerActivity.this.l.eps + "] ";
                    }
                    PlayerActivity.this.mToolbar.setTitle((CharSequence) PlayerActivity.this.B.getName() + str + PlayerActivity.this.e(PlayerActivity.this.m).toString());
                    PlayerActivity.this.mToolbar.setNavigationOnClickListener(new m0(this));
                }
                PlayerActivity.this.e("Playback failed!!! Auto switching link...");
            } catch (Exception unused4) {
                PlayerActivity.this.e("Something was wrong, Please back and select another server...");
            }
        }

        public void onPlayerStateChanged(boolean z, int i) {
            if (i == 4) {
                boolean z2 = FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
                if (!PlayerActivity.this.B.getTV().booleanValue() || !z2) {
                    PlayerActivity.this.i();
                } else {
                    PlayerActivity.this.setResult(-1, new Intent());
                    PlayerActivity.this.finish();
                }
            }
            if (i == 3) {
                PlayerActivity.this.c();
            }
            PlayerActivity.this.k();
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
            PlayerActivity.this.k();
            if (trackGroupArray != PlayerActivity.this.g) {
                MappingTrackSelector.MappedTrackInfo c = PlayerActivity.this.e.c();
                if (c != null) {
                    if (c.d(2) == 1) {
                        PlayerActivity.this.g((int) R.string.error_unsupported_video);
                    }
                    if (c.d(1) == 1) {
                        PlayerActivity.this.g((int) R.string.error_unsupported_audio);
                    }
                }
                TrackGroupArray unused = PlayerActivity.this.g = trackGroupArray;
            }
        }
    }

    static {
        K.setCookiePolicy(CookiePolicy.ACCEPT_ORIGINAL_SERVER);
    }

    static /* synthetic */ void c(String str) throws Exception {
    }

    static /* synthetic */ void d(String str) throws Exception {
    }

    static /* synthetic */ void d(Throwable th) throws Exception {
    }

    private void setupToolbar() {
        this.mToolbar = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar = this.mToolbar;
        if (toolbar == null) {
            Timber.b("Didn't find a toolbar", new Object[0]);
            return;
        }
        ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
        setSupportActionBar(this.mToolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.g(true);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return this.f5105a.dispatchKeyEvent(keyEvent) || super.dispatchKeyEvent(keyEvent);
    }

    public void onBackPressed() {
        boolean z2 = FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
        if (this.B.getTV().booleanValue() && z2) {
            setResult(0, new Intent());
        }
        super.onBackPressed();
    }

    public void onClick(View view) {
    }

    public void onCreate(Bundle bundle) {
        String str;
        super.onCreate(bundle);
        this.b = a(true);
        CookieHandler cookieHandler = CookieHandler.getDefault();
        CookieManager cookieManager = K;
        if (cookieHandler != cookieManager) {
            CookieHandler.setDefault(cookieManager);
        }
        setContentView(R.layout.activity_player);
        findViewById(R.id.root).setOnClickListener(this);
        this.A = new CompositeDisposable();
        this.w = (CaptionsView) findViewById(R.id.subs_box);
        this.w.setCaptionsViewLoadListener(this);
        this.w.setTextSize(0, ((float) getResources().getDimensionPixelSize(R.dimen.text_size_large)) * Float.valueOf(FreeMoviesApp.l().getString("pref_cc_subs_font_scale", "1.00")).floatValue());
        this.w.setTextColor(Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF")));
        this.w.setActivity(this);
        this.f5105a = (PlayerView) findViewById(R.id.player_view);
        this.f5105a.setControllerVisibilityListener(this);
        this.f5105a.setErrorMessageProvider(new PlayerErrorMessageProvider());
        this.f5105a.requestFocus();
        this.f5105a.setShowBuffering(true);
        this.f5105a.getSubtitleView().setStyle(new CaptionStyleCompat(-1, 0, 0, 1, -16777216, (Typeface) null));
        this.f5105a.setControllerShowTimeoutMs(2500);
        if (bundle != null) {
            this.f = (DefaultTrackSelector.Parameters) bundle.getParcelable("track_selector_parameters");
            this.h = bundle.getBoolean("auto_play");
            this.i = bundle.getInt("window");
            this.j = bundle.getLong(ViewProps.POSITION);
        } else {
            this.f = new DefaultTrackSelector.ParametersBuilder().a();
            d();
        }
        Bundle extras = getIntent().getExtras();
        this.n = IntentDataContainer.a().a("MediaSouce");
        if (this.n != null) {
            if (extras.getString("LINKID") != null) {
                extras.getString("LINKID");
            }
            this.B = (MovieEntity) extras.getParcelable("Movie");
            this.l = (MovieInfo) extras.getParcelable("MovieInfo");
            this.m = extras.getInt("streamID");
            this.y = extras.getBoolean("ISLOCAL");
            this.z = (SubtitleInfo) extras.getParcelable("SubtitleInfo");
            this.j = this.B.getPosition();
            setupToolbar();
            if (this.mToolbar != null) {
                if (this.l.session.isEmpty()) {
                    str = "";
                } else {
                    str = " [" + this.l.session + "x" + this.l.eps + "] ";
                }
                this.mToolbar.setTitle((CharSequence) this.B.getName() + str + e(this.m).toString());
                this.mToolbar.setNavigationOnClickListener(new l0(this));
            }
            this.C = false;
            return;
        }
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.video_player_menu, menu);
        MenuItem findItem = menu.findItem(R.id.rateme);
        if (findItem != null) {
            findItem.setVisible(false);
        }
        menu.findItem(R.id.video_player_quality);
        return true;
    }

    public void onDestroy() {
        this.A.a();
        this.A.dispose();
        super.onDestroy();
    }

    public void onNewIntent(Intent intent) {
        g();
        d();
        setIntent(intent);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.video_player_quality) {
            j();
        } else if (itemId == R.id.video_player_subtile) {
            if (Utils.A()) {
                if (!this.B.getRealeaseDate().isEmpty()) {
                    String str = this.B.getRealeaseDate().split("-")[0];
                }
                a(this.l, e(this.m));
            } else {
                Toast.makeText(this, "This funcion require internet...", 0).show();
            }
        } else if (itemId == R.id.video_player_save_favorite) {
            h();
        } else if (itemId == R.id.rateme) {
            f();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onPause() {
        super.onPause();
        this.c.a(false);
        this.c.getPlaybackState();
        this.w.setVisibility(4);
        if (Util.f3651a <= 23) {
            g();
        }
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    public void onRequestPermissionsResult(int i2, String[] strArr, int[] iArr) {
        if (iArr.length != 0) {
            if (iArr[0] == 0) {
                e();
                return;
            }
            g((int) R.string.storage_permission_denied);
            finish();
        }
    }

    public void onResume() {
        SimpleExoPlayer simpleExoPlayer;
        super.onResume();
        if (Util.f3651a <= 23 || (simpleExoPlayer = this.c) == null) {
            e();
        } else {
            simpleExoPlayer.a(true);
            this.c.getPlaybackState();
        }
        this.w.setPlayer(this.c);
        this.w.setVisibility(0);
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        l();
        b(false);
        bundle.putParcelable("track_selector_parameters", this.f);
        bundle.putBoolean("auto_play", this.h);
        bundle.putInt("window", this.i);
        bundle.putLong(ViewProps.POSITION, this.j);
    }

    public void onStart() {
        super.onStart();
        if (Util.f3651a > 23) {
            e();
        }
    }

    public void onStop() {
        super.onStop();
        if (Util.f3651a > 23) {
            g();
        }
    }

    public void onWindowFocusChanged(boolean z2) {
        super.onWindowFocusChanged(z2);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    private void f() {
        if (this.s == null) {
            this.s = new Dialog(this);
            this.s.setContentView(R.layout.dialog_rating);
            this.s.getWindow().setBackgroundDrawable(new ColorDrawable(0));
            ((Button) this.s.findViewById(R.id.rating_btn)).setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + Utils.s()));
                    intent.addFlags(1208483840);
                    try {
                        PlayerActivity.this.startActivity(intent);
                    } catch (ActivityNotFoundException unused) {
                        PlayerActivity playerActivity = PlayerActivity.this;
                        playerActivity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + Utils.s())));
                    }
                }
            });
        }
        this.s.show();
    }

    private void g() {
        if (this.c != null) {
            l();
            b(false);
            this.c.y();
            this.c = null;
            this.d = null;
            this.e = null;
        }
    }

    private void h() {
        this.v.a(this, this.B, true);
    }

    /* access modifiers changed from: private */
    public void i() {
        this.mToolbar.setVisibility(0);
    }

    private void j() {
        ArrayList arrayList = new ArrayList();
        for (int i2 = 0; i2 < this.n.size(); i2++) {
            arrayList.add(e(i2).toString2());
        }
        ArrayAdapter arrayAdapter = new ArrayAdapter(this, 17367046, arrayList);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.b((CharSequence) I18N.a(R.string.select_link));
        builder.a((ListAdapter) arrayAdapter, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String str;
                PlayerActivity playerActivity = PlayerActivity.this;
                if (playerActivity.m == i) {
                    return;
                }
                if (playerActivity.e(i).getStreamLink().contains("magnet:")) {
                    PlayerActivity.this.e("This torrent link hasn't resolved yet!");
                    return;
                }
                PlayerActivity.this.b(true);
                PlayerActivity playerActivity2 = PlayerActivity.this;
                long unused = playerActivity2.j = Math.max(5, playerActivity2.c.x() ? PlayerActivity.this.j : Math.min(5, PlayerActivity.this.c.getDuration()));
                PlayerActivity.this.c.a(i, PlayerActivity.this.j - 5);
                PlayerActivity.this.c.z();
                PlayerActivity playerActivity3 = PlayerActivity.this;
                if (playerActivity3.mToolbar != null) {
                    if (playerActivity3.l.session.isEmpty()) {
                        str = "";
                    } else {
                        str = " [" + PlayerActivity.this.l.session + "x" + PlayerActivity.this.l.eps + "] ";
                    }
                    PlayerActivity.this.mToolbar.setTitle((CharSequence) PlayerActivity.this.B.getName() + str + PlayerActivity.this.e(i).toString());
                }
                PlayerActivity.this.m = i;
            }
        });
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c();
    }

    /* access modifiers changed from: private */
    public void k() {
        if (this.c == null) {
        }
    }

    private void l() {
        DefaultTrackSelector defaultTrackSelector = this.e;
        if (defaultTrackSelector != null) {
            this.f = defaultTrackSelector.d();
        }
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        g((int) R.string.subtitleLoadedFail);
    }

    public void d(int i2) {
        if (i2 == R.id.exo_cc) {
            boolean z2 = false;
            if (this.w.getVisibility() == 0) {
                this.w.setVisibility(8);
            } else {
                this.w.setVisibility(0);
            }
            PlayerControlView controller = this.f5105a.getController();
            if (this.w.getVisibility() == 0) {
                z2 = true;
            }
            controller.setCcButtonVisible(z2);
        }
    }

    /* access modifiers changed from: package-private */
    public com.original.tase.model.media.MediaSource e(int i2) {
        if (i2 < this.n.size() || this.n.size() <= 1) {
            return this.n.get(i2);
        }
        Utils.a((Activity) this, "Wrong index");
        return e(this.n.size() - 1);
    }

    public void b(ArrayList<SubtitleInfo> arrayList) {
        AlertDialog alertDialog = this.r;
        if (alertDialog == null || !alertDialog.isShowing()) {
            this.q = new HashMap();
            Iterator<SubtitleInfo> it2 = arrayList.iterator();
            while (it2.hasNext()) {
                SubtitleInfo next = it2.next();
                List list = this.q.get(next.c);
                if (list == null) {
                    list = new ArrayList();
                    this.q.put(next.c, list);
                }
                list.add(next);
            }
            if (this.r == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                View inflate = getLayoutInflater().inflate(R.layout.dialog_listsubtitle, (ViewGroup) null);
                builder.b(inflate);
                builder.b((int) R.string.close_msg, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                this.r = builder.a();
                this.o = (ExpandableListView) inflate.findViewById(R.id.subtitle_expand_view);
                this.o.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    static /* synthetic */ void a(Throwable th) throws Exception {
                    }

                    public /* synthetic */ void a(List list) throws Exception {
                        PlayerActivity.this.w.setCaptionsSource(list);
                        PlayerActivity.this.B.setSubtitlepath(((File) list.get(0)).getAbsolutePath());
                    }

                    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
                        SubtitleInfo subtitleInfo = (SubtitleInfo) PlayerActivity.this.p.getChild(i, i2);
                        PlayerActivity.this.r.dismiss();
                        if (!PermissionHelper.b(PlayerActivity.this, 777)) {
                            return false;
                        }
                        PlayerActivity.this.w.setPlayer(PlayerActivity.this.c);
                        PlayerActivity playerActivity = PlayerActivity.this;
                        SubServiceBase.a(playerActivity, subtitleInfo.b, playerActivity.l.getNameAndYear()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new i0(this), h0.f5242a);
                        return true;
                    }
                });
            }
            this.p = new ExpandableListSubtitleAdapter(this, this.q);
            this.o.setAdapter(this.p);
            if (this.q.keySet().size() == 1) {
                this.o.expandGroup(0);
            }
            this.r.show();
        }
    }

    public void c(int i2) {
        Toolbar toolbar = this.mToolbar;
        if (toolbar != null) {
            toolbar.setVisibility(i2);
        }
        f(i2);
    }

    /* access modifiers changed from: private */
    public void d() {
        this.h = true;
        this.i = -1;
        this.j = -9223372036854775807L;
    }

    /* access modifiers changed from: private */
    public void e() {
        Uri[] uriArr;
        DefaultDrmSessionManager<FrameworkMediaCrypto> defaultDrmSessionManager;
        TrackSelection.Factory factory;
        int i2;
        boolean z2 = true;
        if (this.c == null) {
            Intent intent = getIntent();
            String action = intent.getAction();
            if ("com.google.android.exoplayer.demo.action.VIEW".equals(action)) {
                uriArr = new Uri[]{intent.getData()};
                new String[1][0] = intent.getStringExtra("extension");
            } else if ("com.google.android.exoplayer.demo.action.VIEW_LIST".equals(action)) {
                String[] stringArrayExtra = intent.getStringArrayExtra("uri_list");
                Uri[] uriArr2 = new Uri[stringArrayExtra.length];
                for (int i3 = 0; i3 < stringArrayExtra.length; i3++) {
                    uriArr2[i3] = Uri.parse(stringArrayExtra[i3]);
                }
                if (intent.getStringArrayExtra("extension_list") == null) {
                    String[] strArr = new String[stringArrayExtra.length];
                }
                uriArr = uriArr2;
            } else {
                e(getString(R.string.unexpected_intent_action, new Object[]{action}));
                finish();
                return;
            }
            if (!Util.a((Activity) this, uriArr)) {
                String str = "drm_scheme";
                if (intent.hasExtra(str) || intent.hasExtra("drm_scheme_uuid")) {
                    String stringExtra = intent.getStringExtra("drm_license_url");
                    String[] stringArrayExtra2 = intent.getStringArrayExtra("drm_key_request_properties");
                    boolean booleanExtra = intent.getBooleanExtra("drm_multi_session", false);
                    if (Util.f3651a < 18) {
                        defaultDrmSessionManager = null;
                        i2 = R.string.error_drm_not_supported;
                    } else {
                        i2 = R.string.error_drm_unsupported_scheme;
                        try {
                            if (!intent.hasExtra(str)) {
                                str = "drm_scheme_uuid";
                            }
                            UUID b2 = Util.b(intent.getStringExtra(str));
                            if (b2 == null) {
                                defaultDrmSessionManager = null;
                            } else {
                                defaultDrmSessionManager = a(b2, stringExtra, stringArrayExtra2, booleanExtra);
                                i2 = R.string.error_drm_unknown;
                            }
                        } catch (UnsupportedDrmException e2) {
                            i2 = e2.reason == 1 ? R.string.error_drm_unsupported_scheme : R.string.error_drm_unknown;
                        }
                    }
                    if (defaultDrmSessionManager == null) {
                        g(i2);
                        finish();
                        return;
                    }
                } else {
                    defaultDrmSessionManager = null;
                }
                String stringExtra2 = intent.getStringExtra("abr_algorithm");
                if (stringExtra2 == null || "default".equals(stringExtra2)) {
                    factory = new AdaptiveTrackSelection.Factory(J);
                } else if ("random".equals(stringExtra2)) {
                    factory = new RandomTrackSelection.Factory();
                } else {
                    g((int) R.string.error_unrecognized_abr_algorithm);
                    finish();
                    return;
                }
                boolean booleanExtra2 = intent.getBooleanExtra("prefer_extension_decoders", false);
                boolean i4 = ((FreeMoviesApp) getApplication()).i();
                DefaultRenderersFactory defaultRenderersFactory = new DefaultRenderersFactory(this, 1);
                this.e = new DefaultTrackSelector(factory);
                this.e.a(this.f);
                this.g = null;
                this.c = ExoPlayerFactory.a((Context) this, (RenderersFactory) defaultRenderersFactory, (TrackSelector) this.e, (DrmSessionManager<FrameworkMediaCrypto>) defaultDrmSessionManager);
                this.c.b((Player.EventListener) new PlayerEventListener());
                this.c.a(this.h);
                this.c.a((AnalyticsListener) new EventLogger(this.e));
                this.c.setRepeatMode(0);
                this.f5105a.setPlayer(this.c);
                this.f5105a.setPlaybackPreparer(this);
                if (this.y) {
                    DataSpec dataSpec = new DataSpec(Uri.parse(this.l.tempStreamLink));
                    final FileDataSource fileDataSource = new FileDataSource();
                    try {
                        fileDataSource.a(dataSpec);
                    } catch (FileDataSource.FileDataSourceException e3) {
                        e3.printStackTrace();
                    }
                    this.d = new ExtractorMediaSource(fileDataSource.getUri(), new DataSource.Factory(this) {
                        public DataSource a() {
                            return fileDataSource;
                        }
                    }, new DefaultExtractorsFactory(), (Handler) null, (ExtractorMediaSource.EventListener) null);
                } else {
                    MediaSource[] mediaSourceArr = new MediaSource[this.n.size()];
                    for (int i5 = 0; i5 < mediaSourceArr.length; i5++) {
                        String streamLink = e(i5).getStreamLink();
                        mediaSourceArr[i5] = a(Uri.parse(streamLink), Utils.g(streamLink), i5);
                    }
                    this.d = mediaSourceArr.length == 1 ? mediaSourceArr[0] : new ConcatenatingMediaSource(mediaSourceArr);
                }
                if (this.B.getSubtitlepath() != null && !this.B.getSubtitlepath().isEmpty()) {
                    this.w.setPlayer(this.c);
                    ArrayList arrayList = new ArrayList();
                    arrayList.add(new File(this.B.getSubtitlepath()));
                    this.w.setCaptionsSource(arrayList);
                } else if (this.z != null && PermissionHelper.b(this, 777)) {
                    this.w.setPlayer(this.c);
                    SubServiceBase.a(this, this.z.b, this.l.getNameAndYear()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new o0(this), g0.f5229a);
                }
            } else {
                return;
            }
        }
        SimpleExoPlayer simpleExoPlayer = this.c;
        MediaSource mediaSource = this.d;
        if (this.j > 0) {
            z2 = false;
        }
        simpleExoPlayer.a(mediaSource, z2, false);
        try {
            this.c.a(this.m, this.j);
        } catch (Exception unused) {
            this.m = 0;
            this.c.a(this.m, 0);
        }
        k();
    }

    public /* synthetic */ void c(List list) throws Exception {
        this.w.setCaptionsSource(list);
    }

    private HttpDataSource.Factory c(boolean z2, int i2) {
        com.original.tase.model.media.MediaSource e2 = e(i2);
        String str = Constants.f5838a;
        HashMap<String, String> a2 = SourceUtils.a(e2.getPlayHeader());
        if (a2 != null && z2) {
            if (a2.containsKey("User-Agent")) {
                str = a2.get("User-Agent");
            } else if (a2.containsKey("user-agent")) {
                str = a2.get("user-agent");
            }
        }
        OkHttpDataSourceFactory okHttpDataSourceFactory = new OkHttpDataSourceFactory(HttpHelper.e().b(), str, (TransferListener) null, CacheControl.FORCE_NETWORK);
        HttpDataSource.RequestProperties b2 = okHttpDataSourceFactory.b();
        if (a2 != null && z2) {
            for (Map.Entry next : a2.entrySet()) {
                if (!(next.getKey() == null || next.getValue() == null || ((String) next.getKey()).equalsIgnoreCase("user-agent"))) {
                    b2.a((String) next.getKey(), (String) next.getValue());
                }
            }
        }
        return okHttpDataSourceFactory;
    }

    private void f(int i2) {
        if (i2 == 0) {
            Utils.d((Activity) this);
        } else {
            Utils.b((Activity) this);
        }
    }

    /* access modifiers changed from: private */
    public void g(int i2) {
        e(getString(i2));
    }

    public /* synthetic */ void a(View view) {
        finish();
    }

    public void a(MovieInfo movieInfo, com.original.tase.model.media.MediaSource mediaSource) {
        ArrayList<SubtitleInfo> arrayList = this.D;
        if (arrayList == null || arrayList.size() == 0) {
            Toast.makeText(this, R.string.subtitleLoading, 0).show();
            movieInfo.tmdbID = this.B.getTmdbID();
            movieInfo.imdbIDStr = this.B.getImdbIDStr();
            this.D = new ArrayList<>();
            this.A.b(SubServiceBase.b(movieInfo).observeOn(AndroidSchedulers.a()).subscribe(new j0(this), new r0(this)));
            return;
        }
        b(this.D);
    }

    public /* synthetic */ void a(ArrayList arrayList) throws Exception {
        this.D.addAll(arrayList);
        if (!this.k) {
            b(this.D);
            this.k = true;
        }
    }

    public void c() {
        if (!this.I) {
            this.f5105a.getController().setCcButtonEnable(this.w.getVisibility() == 0);
            this.F = (TextView) findViewById(R.id.position_textview);
            this.F.setShadowLayer(3.0f, 3.0f, 3.0f, -16777216);
            this.F.setTextColor(getResources().getColor(R.color.material_white));
            this.H = (AudioManager) Utils.i().getSystemService("audio");
            final float width = (float) this.f5105a.getOverlayFrameLayout().getWidth();
            final float height = (float) this.f5105a.getOverlayFrameLayout().getHeight();
            this.G = getWindow();
            AnonymousClass7 r3 = new OnSwipeTouchListener(true) {
                float j = -1.0f;
                float k = -1.0f;
                int l;
                int m;
                int n = FreeMoviesApp.l().getInt("BETTER_VIDEO_PLAYER_BRIGHTNESS", 90);
                int o;

                public void a(MotionEvent motionEvent) {
                }

                public void a(OnSwipeTouchListener.Direction direction, float f) {
                    if (PlayerActivity.this.E == 1) {
                        if (direction == OnSwipeTouchListener.Direction.LEFT || direction == OnSwipeTouchListener.Direction.RIGHT) {
                            if (PlayerActivity.this.c.getDuration() <= 60) {
                                this.j = (((float) PlayerActivity.this.c.getDuration()) * f) / width;
                            } else {
                                this.j = (f * 60000.0f) / width;
                            }
                            if (direction == OnSwipeTouchListener.Direction.LEFT) {
                                this.j *= -1.0f;
                            }
                            this.k = ((float) PlayerActivity.this.c.getCurrentPosition()) + this.j;
                            float f2 = this.k;
                            if (f2 < 0.0f) {
                                this.k = 0.0f;
                            } else if (f2 > ((float) PlayerActivity.this.c.getDuration())) {
                                this.k = (float) PlayerActivity.this.c.getDuration();
                            }
                            this.j = this.k - ((float) PlayerActivity.this.c.getCurrentPosition());
                            StringBuilder sb = new StringBuilder();
                            sb.append(OnSwipeTouchListener.a((long) this.k, false));
                            sb.append(" [");
                            sb.append(direction == OnSwipeTouchListener.Direction.LEFT ? "-" : "+");
                            sb.append(OnSwipeTouchListener.a((long) Math.abs(this.j), false));
                            sb.append("]");
                            PlayerActivity.this.F.setText(sb.toString());
                            return;
                        }
                        this.k = -1.0f;
                        if (this.e >= width / 2.0f || PlayerActivity.this.G == null) {
                            float f3 = (((float) this.m) * f) / (height / 2.0f);
                            if (direction == OnSwipeTouchListener.Direction.DOWN) {
                                f3 = -f3;
                            }
                            int i = this.l + ((int) f3);
                            if (i < 0) {
                                i = 0;
                            } else {
                                int i2 = this.m;
                                if (i > i2) {
                                    i = i2;
                                }
                            }
                            PlayerActivity.this.F.setText(String.format(PlayerActivity.this.getResources().getString(R.string.volume), new Object[]{Integer.valueOf(i)}));
                            PlayerActivity.this.H.setStreamVolume(3, i, 0);
                        } else if (this.e < width / 2.0f) {
                            float f4 = (((float) this.o) * f) / height;
                            if (direction == OnSwipeTouchListener.Direction.DOWN) {
                                f4 = -f4;
                            }
                            int i3 = this.n + ((int) f4);
                            if (i3 < 0) {
                                i3 = 0;
                            } else {
                                int i4 = this.o;
                                if (i3 > i4) {
                                    i3 = i4;
                                }
                            }
                            PlayerActivity.this.F.setText(String.format(PlayerActivity.this.getResources().getString(R.string.brightness), new Object[]{Integer.valueOf(i3)}));
                            WindowManager.LayoutParams attributes = PlayerActivity.this.G.getAttributes();
                            attributes.screenBrightness = ((float) i3) / 100.0f;
                            PlayerActivity.this.G.setAttributes(attributes);
                        }
                    }
                }

                public void b() {
                    if (PlayerActivity.this.f5105a.getController().b()) {
                        PlayerActivity.this.f5105a.c();
                    } else {
                        PlayerActivity.this.f5105a.d();
                    }
                }

                public void a() {
                    if (this.k >= 0.0f && PlayerActivity.this.E == 1) {
                        PlayerActivity.this.c.a((long) ((int) this.k));
                    }
                    PlayerActivity.this.F.setVisibility(8);
                    FreeMoviesApp.l().edit().putInt("BETTER_VIDEO_PLAYER_BRIGHTNESS", this.n).apply();
                }

                public void a(OnSwipeTouchListener.Direction direction) {
                    if (PlayerActivity.this.E == 1) {
                        if (direction == OnSwipeTouchListener.Direction.LEFT || direction == OnSwipeTouchListener.Direction.RIGHT) {
                            PlayerActivity.this.F.setVisibility(0);
                            return;
                        }
                        this.o = 100;
                        this.m = PlayerActivity.this.H.getStreamMaxVolume(3);
                        this.l = PlayerActivity.this.H.getStreamVolume(3);
                        PlayerActivity.this.F.setVisibility(0);
                        if (PlayerActivity.this.G != null) {
                            this.n = (int) (PlayerActivity.this.G.getAttributes().screenBrightness * 100.0f);
                        }
                    }
                }
            };
            WindowManager.LayoutParams attributes = this.G.getAttributes();
            attributes.screenBrightness = ((float) FreeMoviesApp.l().getInt("BETTER_VIDEO_PLAYER_BRIGHTNESS", 90)) / 100.0f;
            this.G.setAttributes(attributes);
            this.f5105a.getOverlayFrameLayout().setOnTouchListener(r3);
            this.I = true;
        }
    }

    private List<StreamKey> b(Uri uri) {
        return ((FreeMoviesApp) getApplication()).f().a(uri);
    }

    public void a() {
        e();
    }

    private MediaSource a(Uri uri, String str, int i2) {
        int a2 = Util.a(uri, str);
        if (a2 == 0) {
            DashMediaSource.Factory factory = new DashMediaSource.Factory(new DefaultDashChunkSource.Factory(this.b), a(false));
            factory.a((ParsingLoadable.Parser<? extends DashManifest>) new FilteringManifestParser(new DashManifestParser(), (List<StreamKey>) null));
            return factory.a(uri);
        } else if (a2 == 1) {
            SsMediaSource.Factory factory2 = new SsMediaSource.Factory(new DefaultSsChunkSource.Factory(this.b), a(false));
            factory2.a((ParsingLoadable.Parser<? extends SsManifest>) new FilteringManifestParser(new SsManifestParser(), (List<StreamKey>) null));
            return factory2.a(uri);
        } else if (a2 == 2) {
            HlsMediaSource.Factory factory3 = new HlsMediaSource.Factory((DataSource.Factory) a(true, i2));
            factory3.a((HlsPlaylistParserFactory) new DefaultHlsPlaylistParserFactory(b(uri)));
            return factory3.a(uri);
        } else if (a2 == 3) {
            String str2 = this.l.tempStreamLink;
            if (str2 == null || str2.isEmpty()) {
                return new ExtractorMediaSource.Factory(a(true, i2)).a(uri);
            }
            ExtractorMediaSource.Factory factory4 = new ExtractorMediaSource.Factory((CacheDataSourceFactory) a(false));
            String str3 = this.l.tempStreamLink;
            if (str3 != null) {
                uri = Uri.parse(str3);
            }
            return factory4.a(uri);
        } else {
            throw new IllegalStateException("Unsupported type: " + a2);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        SimpleExoPlayer simpleExoPlayer = this.c;
        if (simpleExoPlayer != null) {
            this.h = simpleExoPlayer.m();
            this.i = this.c.f();
            this.j = Math.max(0, z2 ? this.B.getPosition() : this.c.o());
            this.B.setPosition(this.j);
            if (!this.B.getTV().booleanValue()) {
                this.B.setDuration(this.c.getDuration());
            }
            if (this.j <= 10000) {
                return;
            }
            if (this.B.getTV().booleanValue()) {
                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                tvWatchedEpisode.a(this.l.getEps().intValue());
                tvWatchedEpisode.c(this.l.getSession().intValue());
                tvWatchedEpisode.c(this.B.getTmdbID());
                tvWatchedEpisode.a(this.B.getImdbIDStr());
                tvWatchedEpisode.e(this.B.getTvdbID());
                tvWatchedEpisode.d(this.B.getTraktID());
                tvWatchedEpisode.b(this.j);
                tvWatchedEpisode.a(this.c.getDuration());
                tvWatchedEpisode.b(this.B.getSubtitlepath());
                this.A.b(this.v.a(this.B, tvWatchedEpisode, true, false).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(p0.f5298a, new n0(this)));
                return;
            }
            this.A.b(this.v.a(this.B, false).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(q0.f5343a, new k0(this)));
        }
    }

    private DefaultDrmSessionManager<FrameworkMediaCrypto> a(UUID uuid, String str, String[] strArr, boolean z2) throws UnsupportedDrmException {
        HttpMediaDrmCallback httpMediaDrmCallback = new HttpMediaDrmCallback(str, ((FreeMoviesApp) getApplication()).c());
        if (strArr != null) {
            for (int i2 = 0; i2 < strArr.length - 1; i2 += 2) {
                httpMediaDrmCallback.a(strArr[i2], strArr[i2 + 1]);
            }
        }
        return new DefaultDrmSessionManager(uuid, FrameworkMediaDrm.b(uuid), httpMediaDrmCallback, (HashMap<String, String>) null, z2);
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        Utils.a((Activity) this, th.getMessage());
    }

    /* access modifiers changed from: private */
    public static boolean b(ExoPlaybackException exoPlaybackException) {
        if (exoPlaybackException.type != 0) {
            return false;
        }
        for (Throwable b2 = exoPlaybackException.b(); b2 != null; b2 = b2.getCause()) {
            if (b2 instanceof BehindLiveWindowException) {
                return true;
            }
        }
        return false;
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) this, th.getMessage());
    }

    private DataSource.Factory a(boolean z2) {
        return ((FreeMoviesApp) getApplication()).b();
    }

    public void a(String str, int i2) {
        PlayerControlView controller = this.f5105a.getController();
        if (controller != null) {
            controller.setCcButtonEnable(true);
            this.w.setVisibility(0);
        }
        Toast.makeText(this, R.string.subtitleLoadedSuccess, 0).show();
    }

    private HttpDataSource.Factory b(boolean z2, int i2) {
        String str;
        com.original.tase.model.media.MediaSource e2 = e(i2);
        String str2 = Constants.f5838a;
        HashMap<String, String> a2 = SourceUtils.a(e2.getPlayHeader());
        if (a2 == null || !z2) {
            DefaultHttpDataSourceFactory defaultHttpDataSourceFactory = new DefaultHttpDataSourceFactory(str2, (TransferListener) null, 45000, 45000, true);
            HttpDataSource.RequestProperties b2 = defaultHttpDataSourceFactory.b();
            for (Map.Entry next : a2.entrySet()) {
                b2.a((String) next.getKey(), (String) next.getValue());
            }
            Map<String, String> a3 = b2.a();
            if (a3.containsKey("Cache-Control")) {
                b2.a("Cache-Control", "no-cache");
            }
            if (a3.containsKey(TheTvdb.HEADER_ACCEPT)) {
                b2.a(TheTvdb.HEADER_ACCEPT, "*/*");
            }
            if (a3.containsKey(TheTvdb.HEADER_ACCEPT_LANGUAGE)) {
                b2.a(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.6,en;q=0.4");
            }
            return defaultHttpDataSourceFactory;
        }
        if (a2.containsKey("User-Agent")) {
            str = a2.get("User-Agent");
        } else {
            str = a2.containsKey("user-agent") ? a2.get("user-agent") : "";
        }
        DefaultHttpDataSourceFactory defaultHttpDataSourceFactory2 = new DefaultHttpDataSourceFactory(str, (TransferListener) null, 45000, 45000, true);
        HttpDataSource.RequestProperties b3 = defaultHttpDataSourceFactory2.b();
        if (a2 != null && z2) {
            for (Map.Entry next2 : a2.entrySet()) {
                if (!(next2.getKey() == null || next2.getValue() == null || ((String) next2.getKey()).equalsIgnoreCase("user-agent"))) {
                    b3.a((String) next2.getKey(), (String) next2.getValue());
                }
            }
        }
        Map<String, String> a4 = b3.a();
        if (!a4.containsKey("Cache-Control")) {
            b3.a("Cache-Control", "no-cache");
        }
        if (!a4.containsKey(TheTvdb.HEADER_ACCEPT)) {
            b3.a(TheTvdb.HEADER_ACCEPT, "*/*");
        }
        if (!a4.containsKey(TheTvdb.HEADER_ACCEPT_LANGUAGE)) {
            b3.a(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.6,en;q=0.4");
        }
        return defaultHttpDataSourceFactory2;
    }

    public void a(Throwable th, String str, int i2) {
        PlayerControlView controller = this.f5105a.getController();
        if (controller != null) {
            controller.setCcButtonEnable(false);
        }
        Toast.makeText(this, R.string.subtitleLoadedFail, 0).show();
    }

    private HttpDataSource.Factory a(boolean z2, int i2) {
        if (GoogleVideoHelper.e(e(i2).getStreamLink())) {
            return b(z2, i2);
        }
        return c(z2, i2);
    }

    /* access modifiers changed from: private */
    public void e(String str) {
        Toast.makeText(getApplicationContext(), str, 1).show();
    }
}
