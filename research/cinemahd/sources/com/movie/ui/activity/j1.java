package com.movie.ui.activity;

import com.movie.ui.activity.SettingsActivity;
import com.original.tase.model.debrid.realdebrid.RealDebridGetTokenResult;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class j1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SettingsActivity.SettingsFragment f5251a;

    public /* synthetic */ j1(SettingsActivity.SettingsFragment settingsFragment) {
        this.f5251a = settingsFragment;
    }

    public final void accept(Object obj) {
        this.f5251a.a((RealDebridGetTokenResult) obj);
    }
}
