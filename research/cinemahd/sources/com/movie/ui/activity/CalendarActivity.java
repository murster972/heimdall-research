package com.movie.ui.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.ads.videoreward.AdsManager;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.api.tvmaze.TVMazeApi;
import com.movie.data.model.CalendarItem;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.tmvdb.ExternalTV;
import com.movie.data.model.tmvdb.TvTMDB;
import com.movie.data.model.tvmaze.MazeTVEpisodeItem;
import com.movie.ui.adapter.CalendarAdapter;
import com.movie.ui.helper.MoviesHelper;
import com.movie.ui.widget.AnimatorStateView;
import com.original.tase.api.TraktUserApi;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.original.tase.utils.DeviceUtils;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.uwetrottmann.trakt5.entities.CalendarShowEntry;
import com.uwetrottmann.trakt5.entities.ShowIds;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import javax.inject.Inject;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalDate;
import org.joda.time.ReadableInstant;
import org.joda.time.format.DateTimeFormat;
import org.threeten.bp.ZoneId;
import timber.log.Timber;

public class CalendarActivity extends BaseActivity implements CalendarAdapter.OnCalendarClickListener {

    /* renamed from: a  reason: collision with root package name */
    private ProgressDialog f5053a = null;
    @BindView(2131296339)
    FrameLayout ad_view;
    private CompositeDisposable b;
    @Inject
    TVMazeApi c;
    @Inject
    MvDatabase d;
    @Inject
    public TMDBApi e;
    @Inject
    public IMDBApi f;
    @Inject
    public TheTvdb g;
    @Inject
    MoviesHelper h;
    protected GridLayoutManager i;
    private CalendarAdapter j;
    List<CalendarItem> k = new ArrayList();
    String l = DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd"));
    /* access modifiers changed from: private */
    public CALENDAR_API m = CALENDAR_API.MAZE;
    @BindView(2131296460)
    RecyclerView mRecyclerView;
    private int n = 0;
    /* access modifiers changed from: private */
    public Toolbar o;
    String p = "";
    @BindView(2131297224)
    AnimatorStateView view_empty;

    enum CALENDAR_API {
        TRAKT,
        MAZE
    }

    static /* synthetic */ void b(Throwable th) throws Exception {
    }

    /* access modifiers changed from: private */
    public void c(String str) {
        this.n = 0;
        CalendarAdapter calendarAdapter = this.j;
        if (calendarAdapter != null) {
            calendarAdapter.b();
            this.k.clear();
            this.b.a();
        }
        this.b.b(this.c.getCalendar(str).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new i(this), new h(this)));
    }

    /* access modifiers changed from: private */
    public void d(final String str) {
        this.n = 0;
        CalendarAdapter calendarAdapter = this.j;
        if (calendarAdapter != null) {
            calendarAdapter.b();
            this.k.clear();
            this.b.a();
        }
        this.b.b(Observable.create(new ObservableOnSubscribe<List<CalendarShowEntry>>(this) {
            public void subscribe(ObservableEmitter<List<CalendarShowEntry>> observableEmitter) throws Exception {
                boolean z = FreeMoviesApp.l().getBoolean("pre_show_my_calenda_shows_only", true);
                if (!TraktCredentialsHelper.b().isValid() || !z) {
                    List<CalendarShowEntry> a2 = TraktUserApi.f().a(str);
                    if (a2 != null) {
                        observableEmitter.onNext(a2);
                    }
                    observableEmitter.onComplete();
                    return;
                }
                List<CalendarShowEntry> b = TraktUserApi.f().b(str);
                if (b != null) {
                    observableEmitter.onNext(b);
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new g(this), e.f5220a, new f(this)));
    }

    private void e(List<CalendarItem> list) {
        if (this.i == null) {
            this.j = new CalendarAdapter(this, this.k);
            this.j.a((CalendarAdapter.OnCalendarClickListener) this);
            int i2 = getResources().getConfiguration().orientation == 2 ? 2 : 1;
            this.i = new GridLayoutManager(this, getResources().getInteger(R.integer.movies_columns));
            int a2 = Utils.a((Activity) this);
            if (a2 == 2 || a2 == 0) {
                this.i.c(i2);
            } else {
                this.i.c(1);
            }
            this.i.setOrientation(1);
            this.mRecyclerView.setLayoutManager(this.i);
            this.mRecyclerView.setAdapter(this.j);
        }
        this.k.addAll(list);
        int size = this.k.size() / 20;
        int i3 = this.n;
        if (size > i3) {
            this.n = i3 + 1;
            this.j.notifyDataSetChanged();
        }
    }

    private void f(List<CalendarItem> list) {
        this.k = list;
        this.j = new CalendarAdapter(this, list);
        this.j.a((CalendarAdapter.OnCalendarClickListener) this);
        int i2 = getResources().getConfiguration().orientation == 2 ? 2 : 1;
        this.i = new GridLayoutManager(this, getResources().getInteger(R.integer.movies_columns));
        int a2 = Utils.a((Activity) this);
        if (a2 == 2 || a2 == 0) {
            this.i.c(i2);
        } else {
            this.i.c(1);
        }
        this.i.setOrientation(1);
        this.mRecyclerView.setLayoutManager(this.i);
        this.mRecyclerView.setAdapter(this.j);
    }

    private void setupToolbar() {
        this.o = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar = this.o;
        if (toolbar == null) {
            Timber.b("Didn't find a toolbar", new Object[0]);
            return;
        }
        ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
        setSupportActionBar(this.o);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.g(true);
            supportActionBar.a((CharSequence) this.l);
        }
    }

    public void hideWaitingDialog() {
        ProgressDialog progressDialog = this.f5053a;
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = new CompositeDisposable();
        setContentView(R.layout.activity_calendar);
        if (!DeviceUtils.b()) {
            AdsManager.l().a((ViewGroup) this.ad_view);
        }
        boolean z = FreeMoviesApp.l().getBoolean("pref_use_trakt_calendar2", true);
        if (z) {
            this.m = CALENDAR_API.TRAKT;
        } else {
            this.m = CALENDAR_API.MAZE;
        }
        this.view_empty.setVisibility(4);
        setupToolbar();
        a(z);
        this.mRecyclerView.requestFocus();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_calendar, menu);
        boolean z = FreeMoviesApp.l().getBoolean("pref_use_trakt_calendar2", true);
        CheckBox checkBox = (CheckBox) menu.findItem(R.id.use_trakt).getActionView();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                FreeMoviesApp.l().edit().putBoolean("pref_use_trakt_calendar2", z).apply();
                if (z) {
                    CALENDAR_API unused = CalendarActivity.this.m = CALENDAR_API.TRAKT;
                } else {
                    CALENDAR_API unused2 = CalendarActivity.this.m = CALENDAR_API.MAZE;
                }
                CalendarActivity.this.a(z);
                if (CalendarActivity.this.m == CALENDAR_API.MAZE) {
                    CalendarActivity calendarActivity = CalendarActivity.this;
                    calendarActivity.c(calendarActivity.l);
                    return;
                }
                CalendarActivity calendarActivity2 = CalendarActivity.this;
                calendarActivity2.d(calendarActivity2.l);
            }
        });
        if (checkBox.isChecked() == z) {
            if (this.m == CALENDAR_API.MAZE) {
                c(this.l);
            } else {
                d(this.l);
            }
        }
        checkBox.setChecked(z);
        checkBox.setText("Use Trakt Calendars");
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.b.dispose();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        DateTime now = DateTime.now(DateTimeZone.forTimeZone(TimeZone.getDefault()));
        int itemId = menuItem.getItemId();
        if (itemId == R.id.action_pickdate) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, (DatePickerDialog.OnDateSetListener) null, now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth());
            datePickerDialog.getDatePicker().init(now.getYear(), now.getMonthOfYear() - 1, now.getDayOfMonth(), new DatePicker.OnDateChangedListener() {
                public void onDateChanged(DatePicker datePicker, int i, int i2, int i3) {
                    CalendarActivity.this.p = new LocalDate(i, i2 + 1, i3).toString(DateTimeFormat.forPattern("yyyy-MM-dd"));
                }
            });
            datePickerDialog.setButton(-1, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (!CalendarActivity.this.p.isEmpty()) {
                        CalendarActivity calendarActivity = CalendarActivity.this;
                        calendarActivity.l = calendarActivity.p;
                    }
                    CalendarActivity.this.view_empty.setVisibility(4);
                    if (CalendarActivity.this.m == CALENDAR_API.MAZE) {
                        CalendarActivity calendarActivity2 = CalendarActivity.this;
                        calendarActivity2.c(calendarActivity2.l);
                    } else {
                        CalendarActivity calendarActivity3 = CalendarActivity.this;
                        calendarActivity3.d(calendarActivity3.l);
                    }
                    CalendarActivity.this.a(FreeMoviesApp.l().getBoolean("pref_use_trakt_calendar2", false));
                    CalendarActivity calendarActivity4 = CalendarActivity.this;
                    Toolbar unused = calendarActivity4.o = (Toolbar) calendarActivity4.findViewById(R.id.toolbar);
                    if (CalendarActivity.this.o != null) {
                        String print = DateTimeFormat.forPattern("EE").print((ReadableInstant) DateTimeHelper.d(CalendarActivity.this.l));
                        Toolbar b = CalendarActivity.this.o;
                        b.setSubtitle((CharSequence) CalendarActivity.this.l + "(" + print + ")");
                    }
                }
            });
            datePickerDialog.setButton(-2, "Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    CalendarActivity.this.p = "";
                }
            });
            datePickerDialog.show();
        } else if (itemId == R.id.use_trakt) {
            CheckBox checkBox = (CheckBox) menuItem.getActionView();
            checkBox.setChecked(!checkBox.isChecked());
            return true;
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    public void showWaitingDialog(String str) {
        if (this.f5053a == null) {
            this.f5053a = new ProgressDialog(this);
            try {
                this.f5053a.show();
            } catch (WindowManager.BadTokenException unused) {
            }
            this.f5053a.setCancelable(true);
            this.f5053a.getWindow().setBackgroundDrawableResource(R.color.transparent);
            this.f5053a.setContentView(R.layout.progressbar);
            TextView textView = (TextView) this.f5053a.findViewById(R.id.tv_title);
            if (!str.isEmpty()) {
                textView.setText(str);
                textView.setVisibility(0);
            } else {
                textView.setVisibility(8);
            }
        }
        this.f5053a.show();
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        this.view_empty.setVisibility(0);
    }

    /* access modifiers changed from: package-private */
    public Observable<?> a(long j2, long j3, String str) {
        if (j2 > 0) {
            return this.e.getTvDetails(j2);
        }
        if (str != null && !str.isEmpty()) {
            return this.e.getTVDetails(str, "imdb_id");
        }
        if (j3 > 0) {
            return this.e.getTVDetails(String.valueOf(j3), "tvdb_id");
        }
        return null;
    }

    public void a(boolean z) {
        this.o = (Toolbar) findViewById(R.id.toolbar);
        if (z) {
            this.o.setTitle((CharSequence) "TV Calendar (Trakt.tv)");
        } else {
            this.o.setTitle((CharSequence) "TV Calendar (MazeTv)");
        }
    }

    public /* synthetic */ void c(List list) throws Exception {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            MazeTVEpisodeItem mazeTVEpisodeItem = (MazeTVEpisodeItem) it2.next();
            CalendarItem calendarItem = new CalendarItem();
            calendarItem.airTime = mazeTVEpisodeItem.getAirdate();
            calendarItem.episode = mazeTVEpisodeItem.getNumber();
            calendarItem.season = mazeTVEpisodeItem.getSeason();
            if (mazeTVEpisodeItem.getShow().getImage() != null) {
                calendarItem.backdrop = mazeTVEpisodeItem.getShow().getImage().getOriginal();
                calendarItem.poster = mazeTVEpisodeItem.getShow().getImage().getMedium();
            }
            calendarItem.showName = mazeTVEpisodeItem.getShow().getName();
            calendarItem.episodeName = mazeTVEpisodeItem.getName();
            calendarItem.imdbID = mazeTVEpisodeItem.getShow().getExternals().getImdb();
            calendarItem.tmdbID = -1;
            calendarItem.tvdnID = (long) mazeTVEpisodeItem.getShow().getExternals().getThetvdb();
            calendarItem.traktID = -1;
            calendarItem.isNotTmdb = true;
            arrayList.add(calendarItem);
        }
        f(arrayList);
    }

    public /* synthetic */ void d(List list) throws Exception {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            CalendarShowEntry calendarShowEntry = (CalendarShowEntry) it2.next();
            try {
                if (!(calendarShowEntry.show == null || calendarShowEntry.show.ids == null)) {
                    ShowIds showIds = calendarShowEntry.show.ids;
                    CalendarItem calendarItem = new CalendarItem();
                    if (calendarShowEntry.first_aired != null) {
                        calendarItem.airTime = calendarShowEntry.first_aired.atZoneSameInstant(ZoneId.systemDefault()).toLocalTime().toString();
                    } else {
                        calendarItem.airTime = "unknow";
                    }
                    calendarItem.episode = calendarShowEntry.episode.number.intValue();
                    calendarItem.season = calendarShowEntry.episode.season.intValue();
                    calendarItem.showName = calendarShowEntry.show.title;
                    calendarItem.episodeName = calendarShowEntry.episode.title;
                    calendarItem.imdbID = showIds.imdb;
                    long j2 = -1;
                    calendarItem.tmdbID = showIds.tmdb != null ? (long) showIds.tmdb.intValue() : -1;
                    calendarItem.traktID = showIds.trakt != null ? (long) showIds.trakt.intValue() : -1;
                    if (showIds.tvdb != null) {
                        j2 = (long) showIds.tvdb.intValue();
                    }
                    calendarItem.tvdnID = j2;
                    arrayList.add(calendarItem);
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
        e(arrayList);
    }

    public void a(CalendarItem calendarItem, View view, int i2) {
        if (calendarItem.isNotTmdb) {
            a(calendarItem.tmdbID, calendarItem.tvdnID, calendarItem.imdbID).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new c(this, calendarItem), new d(this, calendarItem));
            return;
        }
        Intent intent = new Intent(this, SourceActivity.class);
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setName(calendarItem.showName);
        movieEntity.setNumberSeason(calendarItem.season);
        movieEntity.setRealeaseDate(calendarItem.airTime);
        movieEntity.setTmdbID(calendarItem.tmdbID);
        movieEntity.setTraktID(calendarItem.traktID);
        movieEntity.setImdbIDStr(calendarItem.imdbID);
        movieEntity.setTvdbID(calendarItem.tvdnID);
        movieEntity.setTV(true);
        intent.putExtra("Movie", movieEntity);
        String str = movieEntity.getRealeaseDate().isEmpty() ? "" : movieEntity.getRealeaseDate().split("-")[0];
        String name = movieEntity.getName();
        String str2 = "" + calendarItem.season;
        String str3 = "" + calendarItem.episode;
        String str4 = calendarItem.airTime;
        if (str4 == null) {
            str4 = "1970";
        }
        MovieInfo movieInfo = new MovieInfo(name, str, str2, str3, str4, new ArrayList());
        movieInfo.epsCount = 10000;
        intent.putExtra("MovieInfo", movieInfo);
        startActivity(intent);
    }

    public /* synthetic */ void c() throws Exception {
        CalendarAdapter calendarAdapter = this.j;
        if (calendarAdapter == null || calendarAdapter.d() == null || this.j.d().size() == 0) {
            boolean z = true;
            if (!FreeMoviesApp.l().getBoolean("pre_show_my_calenda_shows_only", true) || !TraktCredentialsHelper.b().isValid()) {
                z = false;
            }
            if (z) {
                AnimatorStateView animatorStateView = this.view_empty;
                animatorStateView.setMessageText("No my show avaialbe for " + this.l + "\nTry turn off 'Show My Calendar Show only' to show all\nOr select another day");
            } else {
                AnimatorStateView animatorStateView2 = this.view_empty;
                animatorStateView2.setMessageText("No my show avaialbe for " + this.l);
            }
            this.view_empty.setVisibility(0);
            return;
        }
        this.j.notifyDataSetChanged();
    }

    public /* synthetic */ void a(CalendarItem calendarItem, Object obj) throws Exception {
        MovieEntity movieEntity;
        if (obj instanceof ExternalTV) {
            movieEntity = ((ExternalTV) obj).getTv_results().get(0).convert();
        } else {
            movieEntity = obj instanceof TvTMDB.ResultsBean ? ((TvTMDB.ResultsBean) obj).convert() : null;
        }
        Intent intent = new Intent(this, SourceActivity.class);
        intent.putExtra("Movie", movieEntity);
        String str = movieEntity.getRealeaseDate().isEmpty() ? "" : movieEntity.getRealeaseDate().split("-")[0];
        String name = movieEntity.getName();
        String str2 = "" + calendarItem.season;
        String str3 = "" + calendarItem.episode;
        String str4 = calendarItem.airTime;
        MovieInfo movieInfo = new MovieInfo(name, str, str2, str3, str4 == null ? "1970" : str4.split("-")[0], movieEntity.getGenres());
        movieInfo.epsCount = -1;
        intent.putExtra("MovieInfo", movieInfo);
        startActivity(intent);
    }

    public /* synthetic */ void a(CalendarItem calendarItem, Throwable th) throws Exception {
        Intent intent = new Intent(this, SourceActivity.class);
        MovieEntity movieEntity = new MovieEntity();
        movieEntity.setName(calendarItem.showName);
        movieEntity.setNumberSeason(calendarItem.season);
        movieEntity.setRealeaseDate(calendarItem.airTime);
        movieEntity.setTmdbID(calendarItem.tmdbID);
        movieEntity.setTraktID(calendarItem.traktID);
        movieEntity.setImdbIDStr(calendarItem.imdbID);
        movieEntity.setTvdbID(calendarItem.tvdnID);
        movieEntity.setTV(true);
        intent.putExtra("Movie", movieEntity);
        String str = movieEntity.getRealeaseDate().isEmpty() ? "" : movieEntity.getRealeaseDate().split("-")[0];
        String name = movieEntity.getName();
        String str2 = "" + calendarItem.season;
        String str3 = "" + calendarItem.episode;
        String str4 = calendarItem.airTime;
        if (str4 == null) {
            str4 = "1970";
        }
        MovieInfo movieInfo = new MovieInfo(name, str, str2, str3, str4, new ArrayList());
        movieInfo.epsCount = 10000;
        intent.putExtra("MovieInfo", movieInfo);
        startActivity(intent);
    }
}
