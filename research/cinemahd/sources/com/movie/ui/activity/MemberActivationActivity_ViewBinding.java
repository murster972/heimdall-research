package com.movie.ui.activity;

import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public class MemberActivationActivity_ViewBinding extends BaseActivity_ViewBinding {
    private MemberActivationActivity b;
    private View c;
    private View d;
    private View e;
    private View f;
    private View g;

    public MemberActivationActivity_ViewBinding(final MemberActivationActivity memberActivationActivity, View view) {
        super(memberActivationActivity, view);
        this.b = memberActivationActivity;
        memberActivationActivity.introLayout = (ConstraintLayout) Utils.findRequiredViewAsType(view, R.id.intro_layout, "field 'introLayout'", ConstraintLayout.class);
        memberActivationActivity.activateResult = (ConstraintLayout) Utils.findRequiredViewAsType(view, R.id.activateResult, "field 'activateResult'", ConstraintLayout.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.activenow, "field 'activeNow' and method 'onActivateClick'");
        memberActivationActivity.activeNow = (Button) Utils.castView(findRequiredView, R.id.activenow, "field 'activeNow'", Button.class);
        this.c = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                memberActivationActivity.onActivateClick();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.btn_bitcoin, "field 'btn_bitcoin' and method 'onBtnBitcoinClick'");
        memberActivationActivity.btn_bitcoin = (Button) Utils.castView(findRequiredView2, R.id.btn_bitcoin, "field 'btn_bitcoin'", Button.class);
        this.d = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                memberActivationActivity.onBtnBitcoinClick();
            }
        });
        View findRequiredView3 = Utils.findRequiredView(view, R.id.btn_game_challenge, "field 'btn_game_challenge' and method 'onGameChallengeClick'");
        memberActivationActivity.btn_game_challenge = (Button) Utils.castView(findRequiredView3, R.id.btn_game_challenge, "field 'btn_game_challenge'", Button.class);
        this.e = findRequiredView3;
        findRequiredView3.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                memberActivationActivity.onGameChallengeClick();
            }
        });
        memberActivationActivity.btn_amz_gift = (Button) Utils.findRequiredViewAsType(view, R.id.btn_amz_gift, "field 'btn_amz_gift'", Button.class);
        View findRequiredView4 = Utils.findRequiredView(view, R.id.btnCopy, "field 'btnCopy', method 'onCopyCodeClick', and method 'onCopyCodeLongClick'");
        memberActivationActivity.btnCopy = (Button) Utils.castView(findRequiredView4, R.id.btnCopy, "field 'btnCopy'", Button.class);
        this.f = findRequiredView4;
        findRequiredView4.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                memberActivationActivity.onCopyCodeClick();
            }
        });
        findRequiredView4.setOnLongClickListener(new View.OnLongClickListener(this) {
            public boolean onLongClick(View view) {
                memberActivationActivity.onCopyCodeLongClick();
                return true;
            }
        });
        View findRequiredView5 = Utils.findRequiredView(view, R.id.btnRemove, "field 'btnRemove' and method 'onRemoveClick'");
        memberActivationActivity.btnRemove = (Button) Utils.castView(findRequiredView5, R.id.btnRemove, "field 'btnRemove'", Button.class);
        this.g = findRequiredView5;
        findRequiredView5.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                memberActivationActivity.onRemoveClick();
            }
        });
        memberActivationActivity.code = (TextView) Utils.findRequiredViewAsType(view, R.id.code, "field 'code'", TextView.class);
        memberActivationActivity.loading = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.loading, "field 'loading'", ProgressBar.class);
        memberActivationActivity.pbbitcoin = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.pbbitcoin, "field 'pbbitcoin'", ProgressBar.class);
        memberActivationActivity.toolbar = (Toolbar) Utils.findRequiredViewAsType(view, R.id.toolbar, "field 'toolbar'", Toolbar.class);
    }

    public void unbind() {
        MemberActivationActivity memberActivationActivity = this.b;
        if (memberActivationActivity != null) {
            this.b = null;
            memberActivationActivity.introLayout = null;
            memberActivationActivity.activateResult = null;
            memberActivationActivity.activeNow = null;
            memberActivationActivity.btn_bitcoin = null;
            memberActivationActivity.btn_game_challenge = null;
            memberActivationActivity.btn_amz_gift = null;
            memberActivationActivity.btnCopy = null;
            memberActivationActivity.btnRemove = null;
            memberActivationActivity.code = null;
            memberActivationActivity.loading = null;
            memberActivationActivity.pbbitcoin = null;
            memberActivationActivity.toolbar = null;
            this.c.setOnClickListener((View.OnClickListener) null);
            this.c = null;
            this.d.setOnClickListener((View.OnClickListener) null);
            this.d = null;
            this.e.setOnClickListener((View.OnClickListener) null);
            this.e = null;
            this.f.setOnClickListener((View.OnClickListener) null);
            this.f.setOnLongClickListener((View.OnLongClickListener) null);
            this.f = null;
            this.g.setOnClickListener((View.OnClickListener) null);
            this.g = null;
            super.unbind();
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
