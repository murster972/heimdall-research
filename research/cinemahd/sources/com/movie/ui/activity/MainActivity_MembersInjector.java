package com.movie.ui.activity;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.repository.MoviesRepository;
import dagger.MembersInjector;

public final class MainActivity_MembersInjector implements MembersInjector<MainActivity> {
    public static void a(MainActivity mainActivity, MoviesRepository moviesRepository) {
        mainActivity.e = moviesRepository;
    }

    public static void a(MainActivity mainActivity, MvDatabase mvDatabase) {
        mainActivity.f = mvDatabase;
    }

    public static void a(MainActivity mainActivity, MoviesApi moviesApi) {
        mainActivity.g = moviesApi;
    }
}
