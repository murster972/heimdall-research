package com.movie.ui.activity;

import com.movie.data.model.MovieInfo;
import io.reactivex.functions.Consumer;
import java.util.ArrayList;

/* compiled from: lambda */
public final /* synthetic */ class l2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5260a;
    private final /* synthetic */ MovieInfo b;

    public /* synthetic */ l2(SourceActivity sourceActivity, MovieInfo movieInfo) {
        this.f5260a = sourceActivity;
        this.b = movieInfo;
    }

    public final void accept(Object obj) {
        this.f5260a.a(this.b, (ArrayList) obj);
    }
}
