package com.movie.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.TextView;
import com.androidadvance.topsnackbar.TSnackbar;
import com.movie.AppComponent;
import com.original.Constants;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.event.trakt.TraktGetTokenFailedEvent;
import com.original.tase.event.trakt.TraktGetTokenSuccessEvent;
import com.original.tase.event.trakt.TraktUserCancelledAuthEvent;
import com.original.tase.utils.DeviceUtils;
import com.original.tase.utils.NetworkUtils;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class TraktAuthWebViewActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public TSnackbar f5196a;
    /* access modifiers changed from: private */
    public boolean b;
    private Disposable c;
    private WebView d;
    /* access modifiers changed from: private */
    public String e = null;

    class C50911 extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        final TraktAuthWebViewActivity f5197a;

        C50911(TraktAuthWebViewActivity traktAuthWebViewActivity) {
            this.f5197a = traktAuthWebViewActivity;
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (webView.getTitle().equals("Trakt.tv") && str.endsWith("/authorize")) {
                Utils.a((Activity) this.f5197a, I18N.a(R.string.plaese_wait));
                if (this.f5197a.f5196a != null) {
                    this.f5197a.f5196a.c();
                }
                this.f5197a.a(true);
            } else if (webView.getTitle().toLowerCase().contains("activate your device") || (TraktAuthWebViewActivity.this.e != null && str.contains(TraktAuthWebViewActivity.this.e))) {
                if (this.f5197a.f5196a != null) {
                    this.f5197a.f5196a.c();
                }
                this.f5197a.a(false);
            } else {
                if (this.f5197a.f5196a != null) {
                    this.f5197a.f5196a.a();
                }
                this.f5197a.a(false);
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            this.f5197a.a(true);
            return false;
        }
    }

    class C51482 implements Consumer<Object> {

        /* renamed from: a  reason: collision with root package name */
        final TraktAuthWebViewActivity f5198a;

        C51482(TraktAuthWebViewActivity traktAuthWebViewActivity, TraktAuthWebViewActivity traktAuthWebViewActivity2) {
            this.f5198a = traktAuthWebViewActivity2;
        }

        public void accept(Object obj) {
            boolean z = obj instanceof TraktGetTokenSuccessEvent;
            if (z || (obj instanceof TraktGetTokenFailedEvent)) {
                if (z) {
                    boolean unused = this.f5198a.b = true;
                }
                this.f5198a.finish();
            }
        }
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    public void finish() {
        if (!this.b) {
            RxBus.b().a(new TraktUserCancelledAuthEvent());
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_web_view);
        this.needToCancelHttpHelper = false;
        Bundle extras = getIntent().getExtras();
        String[] strArr = null;
        if (extras.getString("verificationUrl", (String) null) == null || extras.getString("verificationUrl", (String) null).isEmpty() || extras.getString("userCode", (String) null) == null || extras.getString("userCode", (String) null).isEmpty() || !NetworkUtils.a()) {
            if (NetworkUtils.a()) {
                Utils.a((Activity) this, I18N.a(R.string.error));
            } else {
                Utils.a((Activity) this, I18N.a(R.string.no_internet));
            }
            setResult(0);
            finish();
            return;
        }
        String string = extras.getString("verificationUrl");
        String string2 = extras.getString("userCode");
        if (string != null && string.contains("/")) {
            strArr = string.split("/");
        }
        if (strArr != null) {
            String str = strArr[strArr.length - 1];
        }
        setTitle("Trakt Auth");
        if (DeviceUtils.a(new boolean[0])) {
            TextView textView = (TextView) findViewById(R.id.tvPleaseWait);
            textView.setTextSize(2, 24.0f);
            textView.setText(String.format("1) Visit \"%s\" in a browser of any of your devices\n2) Login to Trakt.tv\n3) Input the following code: %s\n\nThis window will be closed automatically after you have granted the Trakt.tv API access to Cinema TV", new Object[]{string, string2}));
            a(true);
        } else {
            View findViewById = findViewById(R.id.webViewActivityRoot);
            this.f5196a = TSnackbar.a(findViewById, Html.fromHtml("<font color=\"#ffffff\">Enter the code: " + string2 + "</font>"), -2);
            try {
                WebViewDatabase.getInstance(this).clearFormData();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            this.d = (WebView) findViewById(R.id.webView);
            this.d.getSettings().setJavaScriptEnabled(true);
            this.d.getSettings().setAllowFileAccess(false);
            this.d.getSettings().setSaveFormData(false);
            this.d.getSettings().setSavePassword(false);
            this.d.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
            this.d.getSettings().setCacheMode(2);
            this.d.getSettings().setAppCacheEnabled(false);
            this.d.getSettings().setUserAgentString(Constants.f5838a);
            try {
                this.d.clearCache(true);
                this.d.clearFormData();
            } catch (Throwable th2) {
                Logger.a(th2, new boolean[0]);
            }
            CookieManager.getInstance().setAcceptCookie(true);
            this.d.setWebViewClient(new C50911(this));
            this.d.loadUrl(string);
        }
        this.c = RxBus.b().a().subscribe(new C51482(this, this), z2.f5517a);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WebView webView = this.d;
        if (webView != null) {
            if (webView.getParent() != null && (this.d.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.d.getParent()).removeView(this.d);
            }
            this.d.removeAllViews();
            this.d.destroy();
        }
        Disposable disposable = this.c;
        if (disposable != null && !disposable.isDisposed()) {
            this.c.dispose();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        setResult(0);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        WebView webView = this.d;
        if (webView != null) {
            webView.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        WebView webView = this.d;
        if (webView != null) {
            webView.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        int i = 8;
        findViewById(R.id.webView).setVisibility(z ? 8 : 0);
        findViewById(R.id.tvPleaseWait).setVisibility(z ? 0 : 8);
        View findViewById = findViewById(R.id.pbPleaseWait);
        if (z) {
            i = 0;
        }
        findViewById.setVisibility(i);
    }
}
