package com.movie.ui.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import com.movie.AppComponent;
import com.movie.data.model.ItemHelpCaptcha;
import com.original.Constants;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.utils.NetworkUtils;
import com.original.tase.utils.Regex;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.vungle.warren.model.ReportDBAdapter;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.HashMap;
import java.util.Map;
import okhttp3.HttpUrl;
import okhttp3.internal.cache.DiskLruCache;

public class RecaptchaWebViewActivity extends BaseActivity {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Disposable f5119a;
    private WebView b;
    /* access modifiers changed from: private */
    public ItemHelpCaptcha c;
    public RecaptchaWebViewActivity d = null;

    class C50931 implements Consumer<String> {

        /* renamed from: a  reason: collision with root package name */
        final RecaptchaWebViewActivity f5124a;

        C50931(RecaptchaWebViewActivity recaptchaWebViewActivity, RecaptchaWebViewActivity recaptchaWebViewActivity2) {
            this.f5124a = recaptchaWebViewActivity2;
        }

        /* renamed from: a */
        public void accept(String str) throws Exception {
            if (!this.f5124a.isFinishing()) {
                this.f5124a.setResult(-1);
                this.f5124a.finish();
            }
        }
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    static /* synthetic */ void b(Throwable th) throws Exception {
    }

    @SuppressLint({"WrongConstant"})
    public void c(String str) {
        this.d.findViewById(R.id.webView).setVisibility(8);
        this.d.findViewById(R.id.tvPleaseWait).setVisibility(0);
        this.d.findViewById(R.id.pbPleaseWait).setVisibility(0);
        this.d.c();
        for (String str2 : CookieManager.getInstance().getCookie(str).split(";")) {
            if (str2.contains("cf_clearance")) {
                HttpHelper.e().c(str, str2);
            }
        }
        this.d.f5119a = Observable.create(new ObservableOnSubscribe<String>() {
            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                observableEmitter.onNext("finish");
                RxBus.b().a(RecaptchaWebViewActivity.this.c);
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new C50931(this, this.d), v0.f5499a);
    }

    @SuppressLint({"WrongConstant"})
    public void d(final String str) {
        this.d.findViewById(R.id.webView).setVisibility(8);
        this.d.findViewById(R.id.tvPleaseWait).setVisibility(0);
        this.d.findViewById(R.id.pbPleaseWait).setVisibility(0);
        this.d.c();
        this.d.f5119a = Observable.create(new ObservableOnSubscribe<String>() {
            private String a(String str) {
                try {
                    HttpUrl parse = HttpUrl.parse(str);
                    return parse.scheme() + "://" + parse.host();
                } catch (Throwable th) {
                    Logger.a(th, new boolean[0]);
                    return "";
                }
            }

            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                a((ObservableEmitter<? super String>) observableEmitter);
            }

            public void a(ObservableEmitter<? super String> observableEmitter) {
                HashMap hashMap = new HashMap();
                String replace = a(RecaptchaWebViewActivity.this.c.getLink()).replace("https://", "").replace("http://", "");
                hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3");
                hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US;q=0.9,en;q=0.8");
                hashMap.put("Host", replace.replace("https://", "").replace("http://", "").replace("/", ""));
                hashMap.put("Upgrade-Insecure-Requests", DiskLruCache.VERSION_1);
                hashMap.put("User-Agent", Constants.f5838a);
                HttpHelper.e().a(str, Constants.f5838a, RecaptchaWebViewActivity.this.c.getLink(), (Map<String, String>[]) new Map[]{hashMap});
                observableEmitter.onNext("finish");
                RxBus.b().a(RecaptchaWebViewActivity.this.c);
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new C50931(this, this.d), u0.f5495a);
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled", "RestrictedApi", "WrongConstant"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_web_view);
        this.d = this;
        this.needToCancelHttpHelper = false;
        Bundle extras = getIntent().getExtras();
        if (extras.getString(ReportDBAdapter.ReportColumns.COLUMN_URL) == null || extras.getString(ReportDBAdapter.ReportColumns.COLUMN_URL).isEmpty() || !NetworkUtils.a()) {
            setResult(0);
            finish();
            return;
        }
        setTitle(I18N.a(R.string.verify));
        String string = extras.getString(ReportDBAdapter.ReportColumns.COLUMN_URL);
        String string2 = extras.getString("providername");
        if (string == null) {
            setResult(0);
            finish();
            return;
        }
        this.c = new ItemHelpCaptcha(string2, string);
        try {
            WebViewDatabase.getInstance(this).clearFormData();
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
        this.b = (WebView) findViewById(R.id.webView);
        this.b.getSettings().setJavaScriptEnabled(true);
        this.b.getSettings().setAllowFileAccess(false);
        this.b.getSettings().setSaveFormData(false);
        this.b.getSettings().setSavePassword(false);
        this.b.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
        this.b.getSettings().setCacheMode(2);
        this.b.getSettings().setAppCacheEnabled(false);
        this.b.getSettings().setUserAgentString(Constants.f5838a);
        try {
            this.b.clearCache(true);
            this.b.clearFormData();
        } catch (Throwable th2) {
            Logger.a(th2, new boolean[0]);
        }
        CookieManager.getInstance().setAcceptCookie(true);
        this.b.setWebViewClient(new WebViewClient() {
            static /* synthetic */ void a(Throwable th) throws Exception {
            }

            public void onPageFinished(WebView webView, String str) {
                super.onPageFinished(webView, str);
                String title = webView.getTitle();
                if (title == null || title.isEmpty() || title.toLowerCase().contains("attention required") || title.equalsIgnoreCase("Watch Free MOvies Tv Shows Online 1080p HD Stream Free without registration at Mehlizmovieshd.com")) {
                    RecaptchaWebViewActivity.this.d.findViewById(R.id.webView).setVisibility(0);
                    RecaptchaWebViewActivity.this.d.findViewById(R.id.tvPleaseWait).setVisibility(8);
                    RecaptchaWebViewActivity.this.d.findViewById(R.id.pbPleaseWait).setVisibility(8);
                } else if (RecaptchaWebViewActivity.this.d.isFinishing()) {
                } else {
                    if (str.contains("__cf_chl_captcha_tk__")) {
                        RecaptchaWebViewActivity.this.c(str);
                        return;
                    }
                    RecaptchaWebViewActivity.this.d.setResult(-1);
                    RecaptchaWebViewActivity.this.d.finish();
                }
            }

            public void onPageStarted(WebView webView, String str, Bitmap bitmap) {
                if (str.contains("__cf_chl_captcha_tk__")) {
                    RecaptchaWebViewActivity.this.c(str);
                } else if (str.contains("/cdn-cgi/l/chk_captcha")) {
                    RecaptchaWebViewActivity.this.d(str);
                }
            }

            public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
                String cookie = CookieManager.getInstance().getCookie(RecaptchaWebViewActivity.this.c.getLink());
                if (cookie == null || cookie.isEmpty()) {
                    return true;
                }
                for (String str : cookie.split(";")) {
                    if (str.contains("cf_clearance")) {
                        RecaptchaWebViewActivity.this.d.findViewById(R.id.webView).setVisibility(8);
                        RecaptchaWebViewActivity.this.d.findViewById(R.id.tvPleaseWait).setVisibility(0);
                        RecaptchaWebViewActivity.this.d.findViewById(R.id.pbPleaseWait).setVisibility(0);
                        RecaptchaWebViewActivity.this.d.c();
                        HttpHelper.e().c(RecaptchaWebViewActivity.this.c.getLink(), str);
                        RecaptchaWebViewActivity recaptchaWebViewActivity = RecaptchaWebViewActivity.this.d;
                        Observable observeOn = Observable.create(new ObservableOnSubscribe<String>() {
                            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                                observableEmitter.onNext("finish");
                                RxBus.b().a(RecaptchaWebViewActivity.this.c);
                            }
                        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a());
                        RecaptchaWebViewActivity recaptchaWebViewActivity2 = RecaptchaWebViewActivity.this;
                        Disposable unused = recaptchaWebViewActivity.f5119a = observeOn.subscribe(new C50931(recaptchaWebViewActivity2, recaptchaWebViewActivity2.d), t0.f5491a);
                    }
                }
                return true;
            }

            public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                String a2 = Regex.a(str, "(?:\\/\\/www\\.|\\/\\/)(\\w+\\.\\w+)", 1);
                if (RecaptchaWebViewActivity.this.c.getLink().equals(str) || (!a2.isEmpty() && str.contains(a2))) {
                    RecaptchaWebViewActivity.this.c(str);
                    return true;
                }
                if (str.contains("/cdn-cgi/l/chk_captcha")) {
                    RecaptchaWebViewActivity.this.d(str);
                } else {
                    webView.loadUrl(str);
                }
                return true;
            }
        });
        this.b.loadUrl(string);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        c();
        WebView webView = this.b;
        if (webView != null) {
            if (webView.getParent() != null && (this.b.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.b.getParent()).removeView(this.b);
            }
            this.b.removeAllViews();
            this.b.destroy();
        }
        CookieManager.getInstance().removeAllCookie();
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        setResult(0);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.b.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        this.b.onResume();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }

    /* access modifiers changed from: private */
    public void c() {
        Disposable disposable = this.f5119a;
        if (disposable != null && !disposable.isDisposed()) {
            this.f5119a.dispose();
        }
        this.f5119a = null;
    }
}
