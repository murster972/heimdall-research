package com.movie.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.movie.AppComponent;
import com.movie.data.model.ItemHelpCaptcha;
import com.movie.ui.adapter.HelpRecaptchaViewAdapter;
import com.original.tase.RxBus;
import com.utils.Utils;
import com.vungle.warren.model.ReportDBAdapter;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Iterator;

public class HelpRecaptchar extends BaseActivity implements HelpRecaptchaViewAdapter.ItemClickListener {

    /* renamed from: a  reason: collision with root package name */
    public HelpRecaptchaViewAdapter f5062a;
    public RecyclerView b = null;
    public ArrayList<ItemHelpCaptcha> c;

    class C51191 implements Consumer<Object> {

        /* renamed from: a  reason: collision with root package name */
        final HelpRecaptchar f5063a;

        C51191(HelpRecaptchar helpRecaptchar, HelpRecaptchar helpRecaptchar2) {
            this.f5063a = helpRecaptchar2;
        }

        public void accept(Object obj) {
            if (obj instanceof ItemHelpCaptcha) {
                this.f5063a.a((ItemHelpCaptcha) obj);
            }
        }
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    public /* synthetic */ void a(View view) {
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_help_recaptchar);
        RxBus.b().a().subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new C51191(this, this), k.f5253a);
        Toolbar toolbar = this.mToolbar;
        if (toolbar != null) {
            ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
            this.mToolbar.setNavigationOnClickListener(new j(this));
            ActionBar supportActionBar = getSupportActionBar();
            if (supportActionBar != null) {
                supportActionBar.d(true);
                supportActionBar.b((CharSequence) "Verify Recaptcha");
                supportActionBar.f(true);
            }
        }
        ((FloatingActionButton) findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener(this) {
            public void onClick(View view) {
                Snackbar a2 = Snackbar.a(view, "Replace with your own action", 0);
                a2.a("Action", (View.OnClickListener) null);
                a2.k();
            }
        });
        ArrayList<ItemHelpCaptcha> arrayList = Utils.g;
        if (arrayList == null || arrayList.isEmpty()) {
            this.c = Utils.x();
        } else {
            this.c = Utils.g;
        }
        ArrayList<ItemHelpCaptcha> arrayList2 = this.c;
        if (arrayList2 != null && !arrayList2.isEmpty()) {
            this.b = (RecyclerView) findViewById(R.id.idRecyclerView);
            this.b.setLayoutManager(new LinearLayoutManager(this));
            this.f5062a = new HelpRecaptchaViewAdapter(this, this.c, this);
            this.b.setAdapter(this.f5062a);
        }
    }

    public void onDestroy() {
        Utils.C();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }

    public void a(View view, ItemHelpCaptcha itemHelpCaptcha) {
        Intent intent = new Intent(Utils.i(), RecaptchaWebViewActivity.class);
        intent.putExtra(ReportDBAdapter.ReportColumns.COLUMN_URL, itemHelpCaptcha.getLink());
        intent.putExtra("providername", itemHelpCaptcha.getProviderName());
        startActivityForResult(intent, 5);
    }

    public void a(ItemHelpCaptcha itemHelpCaptcha) {
        Iterator<ItemHelpCaptcha> it2 = this.c.iterator();
        int i = -1;
        while (it2.hasNext()) {
            ItemHelpCaptcha next = it2.next();
            if (next.getLink().equals(itemHelpCaptcha.getLink()) && next.getProviderName().equals(itemHelpCaptcha.getProviderName())) {
                i = this.c.indexOf(next);
                this.f5062a.notifyItemRemoved(i);
            }
        }
        if (i != -1) {
            this.c.remove(i);
        }
    }
}
