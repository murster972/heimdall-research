package com.movie.ui.activity;

import com.database.entitys.TvWatchedEpisode;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5215a;

    public /* synthetic */ c2(SourceActivity sourceActivity) {
        this.f5215a = sourceActivity;
    }

    public final void accept(Object obj) {
        this.f5215a.a((TvWatchedEpisode) obj);
    }
}
