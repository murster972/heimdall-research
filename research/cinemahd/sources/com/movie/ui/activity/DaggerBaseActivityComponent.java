package com.movie.ui.activity;

import com.database.MvDatabase;
import com.movie.AppComponent;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.api.tvmaze.TVMazeApi;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.activity.gamechallenge.GameChallenge;
import com.movie.ui.activity.gamechallenge.GameChallenge_MembersInjector;
import com.movie.ui.activity.movies.MovieActivity;
import com.movie.ui.activity.movies.MovieActivity_MembersInjector;
import com.movie.ui.activity.payment.keyManager.KeyManager;
import com.movie.ui.activity.payment.keyManager.KeyManager_MembersInjector;
import com.movie.ui.activity.shows.ShowActivity;
import com.movie.ui.activity.shows.ShowActivity_MembersInjector;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;

public final class DaggerBaseActivityComponent implements BaseActivityComponent {

    /* renamed from: a  reason: collision with root package name */
    private AppComponent f5060a;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public AppComponent f5061a;

        private Builder() {
        }

        public BaseActivityComponent a() {
            if (this.f5061a != null) {
                return new DaggerBaseActivityComponent(this);
            }
            throw new IllegalStateException(AppComponent.class.getCanonicalName() + " must be set");
        }

        public Builder a(AppComponent appComponent) {
            Preconditions.a(appComponent);
            this.f5061a = appComponent;
            return this;
        }
    }

    public static Builder a() {
        return new Builder();
    }

    private MainActivity b(MainActivity mainActivity) {
        MoviesRepository f = this.f5060a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        MainActivity_MembersInjector.a(mainActivity, f);
        MvDatabase a2 = this.f5060a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        MainActivity_MembersInjector.a(mainActivity, a2);
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        MainActivity_MembersInjector.a(mainActivity, d);
        return mainActivity;
    }

    private DaggerBaseActivityComponent(Builder builder) {
        a(builder);
    }

    private void a(Builder builder) {
        this.f5060a = builder.f5061a;
    }

    public void a(MainActivity mainActivity) {
        b(mainActivity);
    }

    public void a(PlayerActivity playerActivity) {
        b(playerActivity);
    }

    public void a(SourceActivity sourceActivity) {
        b(sourceActivity);
    }

    public void a(SettingsActivity settingsActivity) {
        b(settingsActivity);
    }

    public void a(CalendarActivity calendarActivity) {
        b(calendarActivity);
    }

    public void a(ShowActivity showActivity) {
        b(showActivity);
    }

    public void a(MemberActivationActivity memberActivationActivity) {
        b(memberActivationActivity);
    }

    public void a(GameChallenge gameChallenge) {
        b(gameChallenge);
    }

    public void a(KeyManager keyManager) {
        b(keyManager);
    }

    public void a(TestCrappers testCrappers) {
        b(testCrappers);
    }

    private PlayerActivity b(PlayerActivity playerActivity) {
        MoviesRepository f = this.f5060a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        PlayerActivity_MembersInjector.a(playerActivity, f);
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        PlayerActivity_MembersInjector.a(playerActivity, d);
        MoviesHelper c = this.f5060a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        PlayerActivity_MembersInjector.a(playerActivity, c);
        return playerActivity;
    }

    public void a(MovieActivity movieActivity) {
        b(movieActivity);
    }

    public void a(MovieDetailsActivity movieDetailsActivity) {
        b(movieDetailsActivity);
    }

    private SourceActivity b(SourceActivity sourceActivity) {
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        SourceActivity_MembersInjector.a(sourceActivity, d);
        MoviesHelper c = this.f5060a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        SourceActivity_MembersInjector.a(sourceActivity, c);
        OkHttpClient k = this.f5060a.k();
        Preconditions.a(k, "Cannot return null from a non-@Nullable component method");
        SourceActivity_MembersInjector.a(sourceActivity, k);
        return sourceActivity;
    }

    private SettingsActivity b(SettingsActivity settingsActivity) {
        MvDatabase a2 = this.f5060a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        SettingsActivity_MembersInjector.a(settingsActivity, a2);
        RealDebridApi b = this.f5060a.b();
        Preconditions.a(b, "Cannot return null from a non-@Nullable component method");
        SettingsActivity_MembersInjector.a(settingsActivity, b);
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        SettingsActivity_MembersInjector.a(settingsActivity, d);
        return settingsActivity;
    }

    private CalendarActivity b(CalendarActivity calendarActivity) {
        TVMazeApi j = this.f5060a.j();
        Preconditions.a(j, "Cannot return null from a non-@Nullable component method");
        CalendarActivity_MembersInjector.a(calendarActivity, j);
        MvDatabase a2 = this.f5060a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        CalendarActivity_MembersInjector.a(calendarActivity, a2);
        TMDBApi e = this.f5060a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        CalendarActivity_MembersInjector.a(calendarActivity, e);
        IMDBApi h = this.f5060a.h();
        Preconditions.a(h, "Cannot return null from a non-@Nullable component method");
        CalendarActivity_MembersInjector.a(calendarActivity, h);
        TheTvdb g = this.f5060a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        CalendarActivity_MembersInjector.a(calendarActivity, g);
        MoviesHelper c = this.f5060a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        CalendarActivity_MembersInjector.a(calendarActivity, c);
        return calendarActivity;
    }

    private ShowActivity b(ShowActivity showActivity) {
        MvDatabase a2 = this.f5060a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        ShowActivity_MembersInjector.a(showActivity, a2);
        MoviesHelper c = this.f5060a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        ShowActivity_MembersInjector.a(showActivity, c);
        TMDBApi e = this.f5060a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        ShowActivity_MembersInjector.a(showActivity, e);
        TheTvdb g = this.f5060a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        ShowActivity_MembersInjector.a(showActivity, g);
        return showActivity;
    }

    private MemberActivationActivity b(MemberActivationActivity memberActivationActivity) {
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        MemberActivationActivity_MembersInjector.a(memberActivationActivity, d);
        return memberActivationActivity;
    }

    private GameChallenge b(GameChallenge gameChallenge) {
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        GameChallenge_MembersInjector.a(gameChallenge, d);
        return gameChallenge;
    }

    private KeyManager b(KeyManager keyManager) {
        MoviesApi d = this.f5060a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        KeyManager_MembersInjector.a(keyManager, d);
        return keyManager;
    }

    private TestCrappers b(TestCrappers testCrappers) {
        IMDBApi h = this.f5060a.h();
        Preconditions.a(h, "Cannot return null from a non-@Nullable component method");
        TestCrappers_MembersInjector.a(testCrappers, h);
        TMDBApi e = this.f5060a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        TestCrappers_MembersInjector.a(testCrappers, e);
        return testCrappers;
    }

    private MovieActivity b(MovieActivity movieActivity) {
        TMDBApi e = this.f5060a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MovieActivity_MembersInjector.a(movieActivity, e);
        return movieActivity;
    }

    private MovieDetailsActivity b(MovieDetailsActivity movieDetailsActivity) {
        TMDBApi e = this.f5060a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MovieDetailsActivity_MembersInjector.a(movieDetailsActivity, e);
        MoviesHelper c = this.f5060a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        MovieDetailsActivity_MembersInjector.a(movieDetailsActivity, c);
        return movieDetailsActivity;
    }
}
