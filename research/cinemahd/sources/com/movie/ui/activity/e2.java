package com.movie.ui.activity;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Action;

/* compiled from: lambda */
public final /* synthetic */ class e2 implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5223a;
    private final /* synthetic */ MediaSource b;
    private final /* synthetic */ int c;

    public /* synthetic */ e2(SourceActivity sourceActivity, MediaSource mediaSource, int i) {
        this.f5223a = sourceActivity;
        this.b = mediaSource;
        this.c = i;
    }

    public final void run() {
        this.f5223a.a(this.b, this.c);
    }
}
