package com.movie.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.SearchManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.MenuItemCompat;
import androidx.cursoradapter.widget.CursorAdapter;
import androidx.cursoradapter.widget.SimpleCursorAdapter;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import com.ads.videoreward.AdsBase;
import com.ads.videoreward.AdsManager;
import com.ads.videoreward.VungleAds;
import com.applovin.sdk.AppLovinEventParameters;
import com.applovin.sdk.AppLovinEventTypes;
import com.database.MvDatabase;
import com.database.entitys.CrawlCount;
import com.database.entitys.MovieEntity;
import com.google.android.material.navigation.NavigationView;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.AppConfig;
import com.movie.data.model.Categorys;
import com.movie.data.model.cinema.CrawlBody;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.activity.settings.SettingsActivity;
import com.movie.ui.activity.shows.ShowActivity;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.BrowseMoviesFragment;
import com.movie.ui.fragment.FavoredPageFragment;
import com.movie.ui.fragment.HistoryPageFragment;
import com.movie.ui.fragment.MoviesFragment;
import com.movie.ui.fragment.TorrentManagerFragment;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.api.TraktUserApi;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.original.tase.model.socket.UserPlayerPluginInfo;
import com.original.tase.socket.Client;
import com.original.tase.utils.DeviceUtils;
import com.utils.ImdbSearchSuggestionModel;
import com.utils.PermissionHelper;
import com.utils.PrefUtils;
import com.utils.Utils;
import com.utils.download.DownloadActivity;
import com.yarolegovich.lovelydialog.LovelyCustomDialog;
import com.yarolegovich.lovelydialog.LovelyStandardDialog;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.inject.Inject;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MainActivity extends BaseActivity implements MoviesFragment.Listener, NavigationView.OnNavigationItemSelectedListener, SearchView.OnSuggestionListener, BasePlayerHelper.OnChoosePlayerListener {

    /* renamed from: a  reason: collision with root package name */
    protected CompositeDisposable f5064a;
    /* access modifiers changed from: private */
    public SearchView b = null;
    /* access modifiers changed from: private */
    public Integer c = 28;
    /* access modifiers changed from: private */
    public NavigationView d = null;
    @Inject
    MoviesRepository e;
    @Inject
    volatile MvDatabase f;
    @Inject
    MoviesApi g;
    LovelyCustomDialog h = null;
    List<ImdbSearchSuggestionModel.DBean> i = new ArrayList();
    CursorAdapter j = null;

    public class CustomArrayAdapter extends ArrayAdapter<Integer> {

        /* renamed from: a  reason: collision with root package name */
        private final LayoutInflater f5082a;
        private final SparseArray<String> b;
        private final int c;

        public CustomArrayAdapter(MainActivity mainActivity, Context context, int i, SparseArray<String> sparseArray) {
            super(context, i, 0);
            this.f5082a = LayoutInflater.from(context);
            this.c = i;
            this.b = sparseArray;
        }

        private View a(int i, View view, ViewGroup viewGroup) {
            View inflate = this.f5082a.inflate(this.c, viewGroup, false);
            inflate.setTag(getItem(i));
            ((TextView) inflate.findViewById(16908308)).setText(this.b.valueAt(i));
            return inflate;
        }

        public int getCount() {
            return this.b.size();
        }

        public View getDropDownView(int i, View view, ViewGroup viewGroup) {
            return a(i, view, viewGroup);
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            return a(i, view, viewGroup);
        }

        public Integer getItem(int i) {
            return Integer.valueOf(this.b.keyAt(i));
        }
    }

    public static class Sort implements Parcelable {
        public static final Parcelable.Creator<Sort> CREATOR = new Parcelable.Creator<Sort>() {
            public Sort createFromParcel(Parcel parcel) {
                return new Sort(parcel);
            }

            public Sort[] newArray(int i) {
                return new Sort[i];
            }
        };

        /* renamed from: a  reason: collision with root package name */
        public String f5083a;
        public String b;

        protected Sort(Parcel parcel) {
            this.f5083a = parcel.readString();
            this.b = parcel.readString();
        }

        public int describeContents() {
            return 0;
        }

        public void writeToParcel(Parcel parcel, int i) {
            parcel.writeString(this.f5083a);
            parcel.writeString(this.b);
        }
    }

    public MainActivity() {
        BehaviorSubject.b();
    }

    static /* synthetic */ void c(List list) throws Exception {
    }

    private boolean d() {
        AppConfig.UpdateBean update = GlobalVariable.c().a().getUpdate();
        if (update == null || update.getVersionCode() <= Utils.v() || update.getPackagename().isEmpty()) {
            return false;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(update.getPackagename());
        if (!Utils.a((ArrayList<String>) arrayList)) {
            return false;
        }
        try {
            Drawable applicationIcon = getPackageManager().getApplicationIcon(update.getPackagename());
            LovelyStandardDialog lovelyStandardDialog = new LovelyStandardDialog(this, LovelyStandardDialog.ButtonLayout.HORIZONTAL);
            lovelyStandardDialog.e(R.color.indigo);
            LovelyStandardDialog lovelyStandardDialog2 = lovelyStandardDialog;
            lovelyStandardDialog2.h(R.color.darkDeepOrange);
            lovelyStandardDialog2.a(applicationIcon);
            LovelyStandardDialog lovelyStandardDialog3 = lovelyStandardDialog2;
            lovelyStandardDialog3.b((CharSequence) "OUT OF UPDATE");
            LovelyStandardDialog lovelyStandardDialog4 = lovelyStandardDialog3;
            lovelyStandardDialog4.a((CharSequence) "You already have The New Version, please uninstall this version");
            LovelyStandardDialog lovelyStandardDialog5 = lovelyStandardDialog4;
            lovelyStandardDialog5.a(17039370, (View.OnClickListener) new View.OnClickListener() {
                public void onClick(View view) {
                    MainActivity.this.startActivity(new Intent("android.intent.action.UNINSTALL_PACKAGE", Uri.parse("package:" + Utils.s())));
                    MainActivity.this.finishAffinity();
                }
            });
            lovelyStandardDialog5.d();
            return true;
        } catch (PackageManager.NameNotFoundException e2) {
            e2.printStackTrace();
            return false;
        }
    }

    private int f(String str) {
        if (str.contains("Tv/Shows")) {
            return R.id.nav_tv_show;
        }
        if (str.contains("Movies")) {
            return R.id.nav_movie;
        }
        if (str.contains("Favorites")) {
            return R.id.nav_favorite;
        }
        if (str.contains("History")) {
            return R.id.nav_history;
        }
        if (str.contains("Calendar")) {
            return R.id.nav_calendar;
        }
        return PrefUtils.a(this);
    }

    static /* synthetic */ void g(String str) throws Exception {
        if (!str.isEmpty()) {
            FreeMoviesApp.l().edit().putBoolean("use_player_plugin", true).apply();
            FreeMoviesApp.l().edit().putString("ip_player_plugin", str).apply();
        }
    }

    public boolean a(int i2) {
        return false;
    }

    public void choosePlayer(String str) {
        if (str != null) {
            str.isEmpty();
        }
    }

    /* access modifiers changed from: package-private */
    public void e(int i2) {
        if (i2 == R.id.nav_favorite) {
            a((BaseFragment) new FavoredPageFragment());
        } else if (i2 == R.id.nav_history) {
            a((BaseFragment) new HistoryPageFragment());
        } else if (i2 == R.id.nav_calendar) {
            Intent intent = new Intent(this, CalendarActivity.class);
            intent.addFlags(67108864);
            startActivity(intent);
        } else {
            String event_category = GlobalVariable.c().a().getEvent_category();
            if (i2 == R.id.nav_tv_show) {
                this.c = Integer.valueOf(FreeMoviesApp.l().getInt("pref_lastCagtegory_show", -11));
            } else if (event_category == null || event_category.isEmpty()) {
                this.c = Integer.valueOf(FreeMoviesApp.l().getInt("pref_lastCagtegory_movie", -10));
            } else {
                this.c = Integer.valueOf(FreeMoviesApp.l().getInt("pref_lastCagtegory_movie", -9999));
            }
            Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
            Spinner spinner = (Spinner) toolbar.findViewById(R.id.spinner);
            toolbar.setSubtitle((CharSequence) "");
            final boolean z = i2 == R.id.nav_tv_show;
            spinner.setVisibility(0);
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long j) {
                    final Integer valueOf = Integer.valueOf(Integer.parseInt(adapterView.getItemAtPosition(i).toString()));
                    Integer unused = MainActivity.this.c = valueOf;
                    final String string = FreeMoviesApp.l().getString("pref_restrict_password", "");
                    final EditText editText = new EditText(MainActivity.this);
                    if (z) {
                        String string2 = FreeMoviesApp.l().getString("ref_restrict_show_category", "");
                        if (string.isEmpty() || !string2.contains(Categorys.getTVCategory().get(MainActivity.this.c.intValue()))) {
                            MainActivity.this.a((BaseFragment) BrowseMoviesFragment.a(valueOf, BrowseMoviesFragment.Type.TV));
                            FreeMoviesApp.l().edit().putInt("pref_lastCagtegory_show", MainActivity.this.c.intValue()).apply();
                            return;
                        }
                        Utils.a((Activity) MainActivity.this, "Enter password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (editText.getText().equals(string)) {
                                    MainActivity.this.a((BaseFragment) BrowseMoviesFragment.a(valueOf, BrowseMoviesFragment.Type.TV));
                                    FreeMoviesApp.l().edit().putInt("pref_lastCagtegory_show", MainActivity.this.c.intValue()).apply();
                                    return;
                                }
                                Utils.a((Activity) MainActivity.this, "password is wrong");
                            }
                        }, editText, (DialogInterface.OnDismissListener) null);
                        return;
                    }
                    String string3 = FreeMoviesApp.l().getString("ref_restrict_mv_category", "");
                    if (string.isEmpty() || !string3.contains(Categorys.getMVCategory().get(MainActivity.this.c.intValue()))) {
                        MainActivity.this.a((BaseFragment) BrowseMoviesFragment.a(valueOf, BrowseMoviesFragment.Type.MV));
                        FreeMoviesApp.l().edit().putInt("pref_lastCagtegory_movie", MainActivity.this.c.intValue()).apply();
                        return;
                    }
                    Utils.a((Activity) MainActivity.this, "Enter password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (editText.getText().equals(string)) {
                                MainActivity.this.a((BaseFragment) BrowseMoviesFragment.a(valueOf, BrowseMoviesFragment.Type.MV));
                                FreeMoviesApp.l().edit().putInt("pref_lastCagtegory_movie", MainActivity.this.c.intValue()).apply();
                                return;
                            }
                            Utils.a((Activity) MainActivity.this, "password is wrong");
                        }
                    }, editText, (DialogInterface.OnDismissListener) null);
                }

                public void onNothingSelected(AdapterView<?> adapterView) {
                }
            });
            if (i2 == R.id.nav_tv_show) {
                spinner.setAdapter(new CustomArrayAdapter(this, toolbar.getContext(), 17367043, Categorys.getTVCategory()));
                spinner.setSelection(a(this.c.intValue(), Categorys.getTVCategory()), false);
                return;
            }
            SparseArray<String> mVCategory = Categorys.getMVCategory();
            if (event_category != null && !event_category.isEmpty()) {
                mVCategory.append(-9999, event_category);
            }
            spinner.setAdapter(new CustomArrayAdapter(this, toolbar.getContext(), 17367043, mVCategory));
            spinner.setSelection(a(this.c.intValue(), mVCategory), false);
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i2, int i3, Intent intent) {
        super.onActivityResult(i2, i3, intent);
        if (i3 == 444) {
            finish();
        }
        for (Fragment onActivityResult : getSupportFragmentManager().p()) {
            onActivityResult.onActivityResult(i2, i3, intent);
        }
    }

    public void onBackPressed() {
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawerLayout.isDrawerOpen(8388611)) {
            drawerLayout.closeDrawer(8388611);
            return;
        }
        if (this.h == null) {
            LovelyCustomDialog lovelyCustomDialog = new LovelyCustomDialog(this);
            lovelyCustomDialog.g(R.layout.dialog_exit_container);
            lovelyCustomDialog.e(R.color.material_orange800);
            LovelyCustomDialog lovelyCustomDialog2 = lovelyCustomDialog;
            lovelyCustomDialog2.c((CharSequence) "EXIT APP !!");
            LovelyCustomDialog lovelyCustomDialog3 = lovelyCustomDialog2;
            lovelyCustomDialog3.a(o.f5293a);
            lovelyCustomDialog3.a(R.id.btn_yes, new View.OnClickListener() {
                static /* synthetic */ void a(Void voidR) throws Exception {
                }

                public /* synthetic */ void a(Throwable th) throws Exception {
                    MainActivity mainActivity = MainActivity.this;
                    Utils.a((Activity) mainActivity, "Backup failed : " + th.getMessage());
                    Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                    intent.addFlags(67108864);
                    intent.putExtra("exit", true);
                    MainActivity.this.startActivity(intent);
                }

                public void onClick(View view) {
                    MainActivity.this.h.a();
                    GlobalVariable.c();
                    GlobalVariable.b();
                    Utils.saveOpenCout(PrefUtils.b(MainActivity.this));
                    AdsManager.l().a();
                    Utils.e((Context) MainActivity.this);
                    if (FreeMoviesApp.l().getBoolean("pref_auto_backup_when_exit_app", false)) {
                        MainActivity.this.f5064a.b(Observable.create(new ObservableOnSubscribe<Void>() {
                            public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/cinemahd/" + "backup");
                                if (!file.exists() && !file.mkdirs()) {
                                    Logger.a("MainActivity", "Problem creating Image folder");
                                }
                                MvDatabase.a((Context) MainActivity.this, "backup");
                                PrefUtils.c(MainActivity.this, "backup");
                                observableEmitter.onComplete();
                                observableEmitter.onComplete();
                            }
                        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(n.f5289a, new m(this), new l(this)));
                        return;
                    }
                    Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                    intent.addFlags(67108864);
                    intent.putExtra("exit", true);
                    MainActivity.this.startActivity(intent);
                }

                public /* synthetic */ void a() throws Exception {
                    Utils.a((Activity) MainActivity.this, "Backup successful.");
                    Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                    intent.addFlags(67108864);
                    intent.putExtra("exit", true);
                    MainActivity.this.startActivity(intent);
                }
            });
            lovelyCustomDialog3.a(R.id.btn_no, new View.OnClickListener() {
                public void onClick(View view) {
                    MainActivity.this.h.a();
                }
            });
            this.h = lovelyCustomDialog3;
        }
        this.h.d();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        this.f5064a = new CompositeDisposable();
        e(f(FreeMoviesApp.l().getString("pref_choose_default_tab", "Tv/Shows")));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().f(false);
        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        AnonymousClass1 r0 = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                MainActivity.this.d.clearFocus();
            }

            public void onDrawerOpened(View view) {
                super.onDrawerOpened(view);
                MainActivity.this.d.requestFocus();
            }
        };
        drawerLayout.addDrawerListener(r0);
        r0.b();
        this.d = (NavigationView) findViewById(R.id.nav_view);
        this.d.bringToFront();
        this.d.setNavigationItemSelectedListener(this);
        this.d.getMenu().findItem(R.id.nav_rate).setVisible(false);
        this.d.getMenu().findItem(R.id.nav_torrent_manager).setVisible(RealDebridCredentialsHelper.c().isValid() || AllDebridCredentialsHelper.b().isValid() || PremiumizeCredentialsHelper.b().isValid());
        int v = Utils.v();
        int versionCode = GlobalVariable.c().a().getUpdate().getVersionCode();
        String link = GlobalVariable.c().a().getUpdate().getLink();
        if (versionCode > v && link != null && !link.equals("")) {
            SpannableString spannableString = new SpannableString("New Update available !!!");
            spannableString.setSpan(new ForegroundColorSpan(-16711936), 0, spannableString.length(), 0);
            this.d.getMenu().findItem(R.id.nav_checkLastest).setTitle(spannableString);
        }
        ((TextView) this.d.a(0).findViewById(R.id.header_version)).setText(String.format("Cinema HD v2 (%s)", new Object[]{Utils.w()}));
        if (!d()) {
            FreeMoviesApp.l().getBoolean("pref_show_choose_sub_lang", false);
            SharedPreferences l = FreeMoviesApp.l();
            if (!l.getBoolean("pref_show_changlog_v2" + Utils.w(), false)) {
                new SpannableStringBuilder("Change Logs").setSpan(new ForegroundColorSpan(-256), 0, 11, 33);
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.b((CharSequence) "Change Logs");
                builder.b(getLayoutInflater().inflate(R.layout.dialog_changelog, (ViewGroup) null));
                builder.b((CharSequence) "OK", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogInterface != null) {
                            dialogInterface.dismiss();
                        }
                    }
                });
                builder.a().show();
                SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
                edit.putBoolean("pref_show_changlog_v2" + Utils.w(), true).apply();
            }
            String notification = GlobalVariable.c().a().getNotification();
            if (notification != null && !notification.isEmpty()) {
                View inflate = getLayoutInflater().inflate(R.layout.notification_layout, (ViewGroup) null);
                AlertDialog.Builder builder2 = new AlertDialog.Builder(this);
                builder2.b(inflate);
                builder2.b((CharSequence) "OK", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogInterface != null) {
                            dialogInterface.dismiss();
                        }
                    }
                });
                AlertDialog a2 = builder2.a();
                ((TextView) inflate.findViewById(R.id.notification)).setText(notification);
                a2.show();
            }
            SharedPreferences l2 = FreeMoviesApp.l();
            if (!l2.getBoolean("pref_show_disclaimer" + Utils.s(), false)) {
                AlertDialog.Builder builder3 = new AlertDialog.Builder(this);
                builder3.b((CharSequence) "DISCLAIMER");
                builder3.a((int) R.drawable.warning_36x36);
                builder3.b((int) R.string.disclaimer);
                builder3.b((CharSequence) "ACCEPT", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (dialogInterface != null) {
                            dialogInterface.dismiss();
                        }
                        SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
                        edit.putBoolean("pref_show_disclaimer" + Utils.s(), true).apply();
                    }
                });
                builder3.a((CharSequence) "DECLINE", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                        intent.addFlags(67108864);
                        intent.putExtra("exit", true);
                        MainActivity.this.startActivity(intent);
                    }
                });
                builder3.a().show();
            }
            autoupdate.a(this, false);
            this.f5064a.b(Observable.create(new ObservableOnSubscribe<List<CrawlCount>>() {
                public void subscribe(ObservableEmitter<List<CrawlCount>> observableEmitter) throws Exception {
                    List<CrawlCount> all = MainActivity.this.f.l().getAll();
                    if (all != null) {
                        observableEmitter.onNext(all);
                    }
                    new CrawlBody().setList(all);
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(q.f5342a, r.f5346a));
        }
        Utils.g();
        AdsManager.l().a((Activity) this);
        if (!DeviceUtils.b() && !DeviceUtils.a()) {
            AdsManager.l().a((ViewGroup) findViewById(R.id.adView));
        }
        String user_agent = GlobalVariable.c().a().getUser_agent();
        if (user_agent != null && !user_agent.isEmpty()) {
            Constants.f5838a = user_agent;
        }
        if (FreeMoviesApp.l().getBoolean("use_player_plugin", false)) {
            c(FreeMoviesApp.l().getString("ip_player_plugin", ""));
        }
        if (TraktCredentialsHelper.b().isValid() && FreeMoviesApp.l().getBoolean("pref_trakt_sync_from_startup", false)) {
            TraktUserApi.f().a(this.f5064a, (Activity) this, this.f);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem findItem = menu.findItem(R.id.action_search);
        SearchManager searchManager = (SearchManager) getSystemService(AppLovinEventTypes.USER_EXECUTED_SEARCH);
        if (findItem != null) {
            this.b = (SearchView) findItem.getActionView();
            MenuItemCompat.a(findItem, (MenuItemCompat.OnActionExpandListener) new MenuItemCompat.OnActionExpandListener() {
                public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                    if (PrefUtils.a(MainActivity.this) == R.id.nav_tv_show) {
                        MainActivity mainActivity = MainActivity.this;
                        mainActivity.a((BaseFragment) BrowseMoviesFragment.a(mainActivity.c, BrowseMoviesFragment.Type.TV));
                    } else {
                        MainActivity mainActivity2 = MainActivity.this;
                        mainActivity2.a((BaseFragment) BrowseMoviesFragment.a(mainActivity2.c, BrowseMoviesFragment.Type.MV));
                    }
                    return true;
                }

                public boolean onMenuItemActionExpand(MenuItem menuItem) {
                    return true;
                }
            });
        }
        SearchView searchView = this.b;
        if (searchView == null) {
            return true;
        }
        try {
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
            return true;
        } catch (Exception e2) {
            e2.printStackTrace();
            return true;
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f5064a.dispose();
        super.onDestroy();
    }

    /* access modifiers changed from: protected */
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if ("android.intent.action.SEARCH".equals(intent.getAction())) {
            intent.getBundleExtra("app_data");
            final String stringExtra = intent.getStringExtra(AppLovinEventParameters.SEARCH_QUERY);
            if (stringExtra != null && !stringExtra.isEmpty()) {
                this.b.a((CharSequence) stringExtra, false);
                final String string = FreeMoviesApp.l().getString("pref_restrict_password", "");
                boolean z = FreeMoviesApp.l().getBoolean("pref_restrict_search", true);
                if (string.isEmpty() || !z) {
                    ImdbSearchSuggestionModel.DBean dBean = new ImdbSearchSuggestionModel.DBean();
                    dBean.setL(stringExtra);
                    dBean.setQ("");
                    a((BaseFragment) BrowseMoviesFragment.a(dBean));
                    PrefUtils.d(this, stringExtra);
                    return;
                }
                final EditText editText = new EditText(this);
                Utils.a((Activity) this, "Enter password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (string.equals(editText.getText().toString())) {
                            ImdbSearchSuggestionModel.DBean dBean = new ImdbSearchSuggestionModel.DBean();
                            dBean.setL(stringExtra);
                            dBean.setQ("");
                            MainActivity.this.a((BaseFragment) BrowseMoviesFragment.a(dBean));
                            PrefUtils.d(MainActivity.this, stringExtra);
                            return;
                        }
                        Utils.a((Activity) MainActivity.this, "Password is worng");
                    }
                }, editText, (DialogInterface.OnDismissListener) null);
            }
        }
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != R.id.action_search) {
            return super.onOptionsItemSelected(menuItem);
        }
        a(false);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        ((InputMethodManager) getSystemService("input_method")).hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        PrefUtils.e(this);
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    public boolean b(int i2) {
        if (i2 >= this.i.size()) {
            return false;
        }
        this.b.a((CharSequence) this.i.get(i2).getL(), true);
        Bundle bundle = new Bundle();
        bundle.putParcelable("app_data", this.i.get(i2));
        this.b.setAppSearchData(bundle);
        return false;
    }

    public void c() {
        this.f5064a.b(Observable.create(new ObservableOnSubscribe<String>(this) {
            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                Client.getIntance().autoconnect(observableEmitter);
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(p.f5297a));
    }

    static /* synthetic */ void a(View view) {
        for (AdsBase adsBase : AdsManager.l().c()) {
            if (adsBase instanceof VungleAds) {
                AdsManager.l().a((FrameLayout) view.findViewById(R.id.ad_holder));
                return;
            }
        }
    }

    public void c(String str) {
        this.f5064a.b(Client.getIntance().createObservable(str).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new Consumer<Object>() {
            public void accept(Object obj) throws Exception {
                if ((obj instanceof UserPlayerPluginInfo) && !((UserPlayerPluginInfo) obj).iConnect) {
                    MainActivity.this.c();
                }
            }
        }));
    }

    private int a(int i2, SparseArray<String> sparseArray) {
        for (int i3 = 0; i3 < sparseArray.size(); i3++) {
            if (sparseArray.keyAt(i3) == i2) {
                return i3;
            }
        }
        return 0;
    }

    public void a(MovieEntity movieEntity, View view) {
        if (movieEntity.getTV().booleanValue()) {
            Intent intent = new Intent(this, ShowActivity.class);
            intent.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(this, MovieDetailsActivity.class);
        intent2.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
        startActivity(intent2);
    }

    public List<ImdbSearchSuggestionModel.DBean> d(String str) {
        String a2 = a(str, "\\((.*)\\)", 1, true);
        ArrayList arrayList = new ArrayList();
        for (ImdbSearchSuggestionModel.DBean next : ((ImdbSearchSuggestionModel) new Gson().a(a2, ImdbSearchSuggestionModel.class)).getD()) {
            try {
                String l = next.getL();
                if (l != null && !l.trim().isEmpty() && !arrayList.contains(l)) {
                    arrayList.add(next);
                }
            } catch (Throwable unused) {
            }
        }
        return arrayList;
    }

    public boolean a(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.nav_tv_show || itemId == R.id.nav_movie) {
            PrefUtils.a((Context) this, itemId);
            e(itemId);
        } else if (itemId == R.id.nav_favorite) {
            PrefUtils.a((Context) this, itemId);
            a((BaseFragment) new FavoredPageFragment());
        } else if (itemId == R.id.nav_history) {
            PrefUtils.a((Context) this, itemId);
            a((BaseFragment) new HistoryPageFragment());
        } else if (itemId == R.id.nav_download) {
            if (!PermissionHelper.b(this, 777)) {
                return false;
            }
            startActivity(new Intent(this, DownloadActivity.class));
        } else if (itemId == R.id.nav_share) {
            String share_url = GlobalVariable.c().a().getShare_url();
            Intent intent = new Intent("android.intent.action.SEND");
            intent.setType("text/plain");
            intent.putExtra("android.intent.extra.SUBJECT", "Cinema");
            intent.putExtra("android.intent.extra.TEXT", "Get the Latest movie/tvShow app at " + share_url);
            startActivity(Intent.createChooser(intent, share_url));
        } else if (itemId == R.id.nav_donate) {
            startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GlobalVariable.c().a().getDonate_url())));
        } else if (itemId == R.id.nav_rate) {
            Intent intent2 = new Intent("android.intent.action.VIEW", Uri.parse("market://details?id=" + Utils.s()));
            intent2.addFlags(1208483840);
            try {
                startActivity(intent2);
            } catch (ActivityNotFoundException unused) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://play.google.com/store/apps/details?id=" + Utils.s())));
            }
        } else if (itemId == R.id.nav_checkLastest) {
            if (!autoupdate.a(this, true)) {
                startActivity(new Intent("android.intent.action.VIEW", Uri.parse(GlobalVariable.c().a().getShare_url())));
            }
        } else if (itemId == R.id.nav_setting) {
            startActivity(new Intent(this, SettingsActivity.class));
        } else if (itemId == R.id.nav_calendar) {
            startActivity(new Intent(this, CalendarActivity.class));
        } else if (itemId == R.id.nav_activate) {
            startActivity(new Intent(this, MemberActivationActivity.class));
        } else if (itemId == R.id.nav_torrent_manager) {
            a((BaseFragment) TorrentManagerFragment.f());
        }
        ((DrawerLayout) findViewById(R.id.drawer_layout)).closeDrawer(8388611);
        return true;
    }

    public void e(String str) {
        new OkHttpClient().newCall(new Request.Builder().url("https://v2.sg.media-imdb.com" + String.format("/suggests/%s/%s.json", new Object[]{Character.valueOf(str.charAt(0)), str.replaceAll("\\s+", "")})).build()).enqueue(new Callback() {
            public void onFailure(Call call, IOException iOException) {
            }

            public void onResponse(Call call, Response response) throws IOException {
                try {
                    MainActivity.this.i = MainActivity.this.d(response.body().string());
                    final MatrixCursor matrixCursor = new MatrixCursor(new String[]{"_id", "suggest_text_1", "suggest_intent_data"});
                    for (int i = 0; i < MainActivity.this.i.size(); i++) {
                        matrixCursor.addRow(new String[]{Integer.toString(i), MainActivity.this.i.get(i).getL(), MainActivity.this.i.get(i).getL()});
                    }
                    MainActivity.this.runOnUiThread(new Runnable() {
                        public void run() {
                            CursorAdapter cursorAdapter = MainActivity.this.j;
                            if (cursorAdapter != null) {
                                cursorAdapter.b(matrixCursor);
                            }
                        }
                    });
                } catch (Throwable unused) {
                    MainActivity.this.i.clear();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(BaseFragment baseFragment) {
        getSupportFragmentManager().b().b(R.id.container, baseFragment, "fragment_movies").a((int) R.anim.slide_in_right, (int) R.anim.slide_out_left, (int) R.anim.slide_in_left, (int) R.anim.slide_out_right).b();
    }

    private void a(boolean z) {
        this.b.setOnSuggestionListener(this);
        if (z) {
            this.b.requestFocusFromTouch();
        }
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 1);
        this.j = new SimpleCursorAdapter(this, R.layout.search_suggestion, (Cursor) null, new String[]{"suggest_text_1"}, new int[]{16908308}, 0);
        this.b.setSuggestionsAdapter(this.j);
        this.b.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            public boolean a(int i) {
                return false;
            }

            public boolean b(final int i) {
                final String string = FreeMoviesApp.l().getString("pref_restrict_password", "");
                boolean z = FreeMoviesApp.l().getBoolean("pref_restrict_search", true);
                if (string.isEmpty() || !z) {
                    Cursor cursor = (Cursor) MainActivity.this.j.getItem(i);
                    if (cursor != null) {
                        MainActivity.this.b.a((CharSequence) cursor.getString(1), true);
                    }
                    Bundle bundle = new Bundle();
                    if (MainActivity.this.i.size() <= i) {
                        return false;
                    }
                    bundle.putParcelable("app_data", MainActivity.this.i.get(i));
                    MainActivity.this.b.setAppSearchData(bundle);
                    return false;
                }
                final EditText editText = new EditText(MainActivity.this);
                Utils.a((Activity) MainActivity.this, "Enter password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (string.equals(editText.getText().toString())) {
                            Cursor cursor = (Cursor) MainActivity.this.j.getItem(i);
                            if (cursor != null) {
                                MainActivity.this.b.a((CharSequence) cursor.getString(1), true);
                            }
                            Bundle bundle = new Bundle();
                            int size = MainActivity.this.i.size();
                            int i2 = i;
                            if (size > i2) {
                                bundle.putParcelable("app_data", MainActivity.this.i.get(i2));
                                MainActivity.this.b.setAppSearchData(bundle);
                                return;
                            }
                            return;
                        }
                        Utils.a((Activity) MainActivity.this, "Password is worng");
                    }
                }, editText, (DialogInterface.OnDismissListener) null);
                return false;
            }
        });
        this.b.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean a(String str) {
                if (str.isEmpty()) {
                    return false;
                }
                MainActivity.this.e(str);
                return false;
            }

            public boolean b(String str) {
                return false;
            }
        });
    }

    @SuppressLint({"WrongConstant"})
    public static String a(String str, String str2, int i2, boolean z) {
        Matcher matcher;
        if (str != null && !str.isEmpty()) {
            if (z) {
                matcher = Pattern.compile(str2, 32).matcher(str);
            } else {
                matcher = Pattern.compile(str2).matcher(str);
            }
            if (matcher.find()) {
                return matcher.group(i2);
            }
        }
        return "";
    }
}
