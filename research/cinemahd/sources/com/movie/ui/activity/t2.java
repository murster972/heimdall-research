package com.movie.ui.activity;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class t2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5493a;
    private final /* synthetic */ MediaSource b;

    public /* synthetic */ t2(SourceActivity sourceActivity, MediaSource mediaSource) {
        this.f5493a = sourceActivity;
        this.b = mediaSource;
    }

    public final void accept(Object obj) {
        this.f5493a.a(this.b, (Throwable) obj);
    }
}
