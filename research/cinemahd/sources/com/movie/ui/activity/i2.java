package com.movie.ui.activity;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class i2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5248a;
    private final /* synthetic */ MediaSource b;

    public /* synthetic */ i2(SourceActivity sourceActivity, MediaSource mediaSource) {
        this.f5248a = sourceActivity;
        this.b = mediaSource;
    }

    public final void accept(Object obj) {
        this.f5248a.a(this.b, (List) obj);
    }
}
