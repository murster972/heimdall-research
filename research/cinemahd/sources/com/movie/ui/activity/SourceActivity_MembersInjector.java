package com.movie.ui.activity;

import com.movie.data.api.MoviesApi;
import com.movie.ui.helper.MoviesHelper;
import dagger.MembersInjector;
import okhttp3.OkHttpClient;

public final class SourceActivity_MembersInjector implements MembersInjector<SourceActivity> {
    public static void a(SourceActivity sourceActivity, MoviesApi moviesApi) {
        sourceActivity.p = moviesApi;
    }

    public static void a(SourceActivity sourceActivity, MoviesHelper moviesHelper) {
        sourceActivity.q = moviesHelper;
    }

    public static void a(SourceActivity sourceActivity, OkHttpClient okHttpClient) {
        sourceActivity.D = okHttpClient;
    }
}
