package com.movie.ui.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.ads.videoreward.AdsManager;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.original.tase.helper.http.HttpHelper;
import com.yoku.marumovie.R;
import timber.log.Timber;

public abstract class BaseActivity extends AppCompatActivity {
    @BindView(2131297158)
    protected Toolbar mToolbar;
    protected boolean needToCancelHttpHelper = true;
    Unbinder unbinder;
    private ProgressDialog waitingDialog = null;

    private void setupToolbar() {
        Toolbar toolbar = this.mToolbar;
        if (toolbar == null) {
            Timber.b("Didn't find a toolbar", new Object[0]);
            return;
        }
        ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
        setSupportActionBar(this.mToolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(false);
            supportActionBar.g(false);
        }
    }

    public final Toolbar getToolbar() {
        return this.mToolbar;
    }

    public void hideWaitingDialog() {
        ProgressDialog progressDialog = this.waitingDialog;
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        AdsManager.l().a(i, i2, intent);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setupComponent(FreeMoviesApp.a((Context) this).d());
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        Unbinder unbinder2 = this.unbinder;
        if (unbinder2 != null) {
            unbinder2.unbind();
        }
        super.onDestroy();
        if (this.needToCancelHttpHelper) {
            HttpHelper.e().c();
        }
        AdsManager.l().d();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        AdsManager.l().e();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        AdsManager.l().f();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        AdsManager.l().g();
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        AdsManager.l().h();
    }

    public void setContentView(int i) {
        super.setContentView(i);
        this.unbinder = ButterKnife.bind((Activity) this);
        setupToolbar();
    }

    /* access modifiers changed from: protected */
    public abstract void setupComponent(AppComponent appComponent);

    public void shareMyApp() {
        String share_url = GlobalVariable.c().a().getShare_url();
        Intent intent = new Intent("android.intent.action.SEND");
        intent.setType("text/plain");
        intent.setType("text/plain");
        intent.putExtra("android.intent.extra.SUBJECT", "Cinema");
        intent.putExtra("android.intent.extra.TEXT", "Get the lastest movie/tvShow app at " + share_url);
        startActivity(Intent.createChooser(intent, share_url));
    }

    public void showWaitingDialog(String str) {
        if (this.waitingDialog == null) {
            this.waitingDialog = new ProgressDialog(this);
            try {
                this.waitingDialog.show();
            } catch (WindowManager.BadTokenException unused) {
            }
        }
        this.waitingDialog.setCancelable(true);
        this.waitingDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        this.waitingDialog.setContentView(R.layout.progressbar);
        TextView textView = (TextView) this.waitingDialog.findViewById(R.id.tv_title);
        if (!str.isEmpty()) {
            textView.setText(str);
            textView.setVisibility(0);
        } else {
            textView.setVisibility(8);
        }
        this.waitingDialog.show();
    }
}
