package com.movie.ui.activity;

import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class a2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5202a;
    private final /* synthetic */ List b;

    public /* synthetic */ a2(SourceActivity sourceActivity, List list) {
        this.f5202a = sourceActivity;
        this.b = list;
    }

    public final void accept(Object obj) {
        this.f5202a.a(this.b, (Throwable) obj);
    }
}
