package com.movie.ui.activity;

import com.original.tase.model.media.MediaSource;
import com.utils.Getlink.Resolver.BaseResolver;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/* compiled from: lambda */
public final /* synthetic */ class v1 implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ v1 f5500a = new v1();

    private /* synthetic */ v1() {
    }

    public final Object apply(Object obj) {
        return BaseResolver.b((MediaSource) obj).subscribeOn(Schedulers.b());
    }
}
