package com.movie.ui.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnLongClick;
import com.ads.videoreward.AdsManager;
import com.facebook.react.modules.appstate.AppStateModule;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.AppConfig;
import com.movie.data.model.payment.bitcoin.BitcoinAddressResponse;
import com.movie.data.model.payment.bitcoin.BitcoinAdressRequest;
import com.movie.data.model.payment.bitcoin.ProductResponse;
import com.movie.ui.activity.gamechallenge.GameChallenge;
import com.movie.ui.activity.payment.BitcoinGatewayActivity;
import com.movie.ui.activity.payment.keyManager.KeyManager;
import com.original.tase.I18N;
import com.original.tase.model.socket.UserResponces;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import javax.inject.Inject;

public class MemberActivationActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    CompositeDisposable f5084a;
    @BindView(2131296336)
    ConstraintLayout activateResult;
    @BindView(2131296337)
    Button activeNow;
    @Inject
    MoviesApi b;
    @BindView(2131296418)
    Button btnCopy;
    @BindView(2131296423)
    Button btnRemove;
    @BindView(2131296426)
    Button btn_amz_gift;
    @BindView(2131296427)
    Button btn_bitcoin;
    @BindView(2131296429)
    Button btn_game_challenge;
    /* access modifiers changed from: private */
    public String c = "";
    @BindView(2131296505)
    TextView code;
    @BindView(2131296709)
    ConstraintLayout introLayout;
    @BindView(2131296765)
    ProgressBar loading;
    @BindView(2131296945)
    ProgressBar pbbitcoin;
    @BindView(2131297158)
    Toolbar toolbar;

    private void f() {
        View inflate = getLayoutInflater().inflate(R.layout.input_key_dialog, (ViewGroup) null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.d(R.layout.input_key_dialog);
        AlertDialog a2 = builder.a();
        a2.a(inflate);
        final EditText editText = (EditText) inflate.findViewById(R.id.edt_key_input);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, final boolean z) {
                editText.post(new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) MemberActivationActivity.this.getSystemService("input_method");
                        if (z) {
                            inputMethodManager.showSoftInput(editText, 1);
                        } else {
                            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    }
                });
            }
        });
        a2.a(-1, AppStateModule.APP_STATE_ACTIVE, new DialogInterface.OnClickListener() {
            public /* synthetic */ void a(String str, AppConfig appConfig) throws Exception {
                if (appConfig == null || appConfig.getAds() != null) {
                    MemberActivationActivity.this.a((int) UserResponces.USER_RESPONCE_FAIL, "The key not available or reach maximum registered devices");
                    return;
                }
                AdsManager.l().a();
                GlobalVariable.c().a(new Gson().a((Object) appConfig));
                MemberActivationActivity memberActivationActivity = MemberActivationActivity.this;
                memberActivationActivity.a((int) UserResponces.USER_RESPONCE_SUCCSES, memberActivationActivity.getString(R.string.unlock_result_success));
                Utils.j(str);
                String unused = MemberActivationActivity.this.c = str;
                MemberActivationActivity memberActivationActivity2 = MemberActivationActivity.this;
                memberActivationActivity2.c(memberActivationActivity2.c);
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                String obj = editText.getText().toString();
                MemberActivationActivity.this.e();
                MemberActivationActivity memberActivationActivity = MemberActivationActivity.this;
                memberActivationActivity.f5084a.b(memberActivationActivity.b.activeKey(obj).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new x(this, obj), new w(this), new y(this)));
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                MemberActivationActivity.this.a((int) UserResponces.USER_RESPONCE_FAIL, th.getMessage());
                MemberActivationActivity.this.c();
            }

            public /* synthetic */ void a() throws Exception {
                MemberActivationActivity.this.c();
            }
        });
        a2.a(-2, I18N.a(R.string.cancel), new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        a2.show();
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        this.pbbitcoin.setVisibility(8);
    }

    public void c(String str) {
        this.loading.setVisibility(0);
        this.activeNow.setVisibility(8);
        this.f5084a.b(this.b.getActivateInfo(str, (String) null, (String) null).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new v(this, str), new c0(this)));
    }

    public void d() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View inflate = getLayoutInflater().inflate(R.layout.dialog_key_manager, (ViewGroup) null);
        builder.b(inflate);
        builder.b((CharSequence) "Input payment informations");
        final EditText editText = (EditText) inflate.findViewById(R.id.edtTransaction);
        final EditText editText2 = (EditText) inflate.findViewById(R.id.edtEmail);
        final EditText editText3 = (EditText) inflate.findViewById(R.id.edtKey);
        editText.requestFocusFromTouch();
        ((InputMethodManager) getSystemService("input_method")).toggleSoftInput(2, 1);
        ((ImageButton) inflate.findViewById(R.id.helpbtn)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Utils.b(MemberActivationActivity.this, "https://cryptomaxx.freshdesk.com/support/solutions/articles/25000001250-where-do-i-get-my-transaction-id-in-coinbase-");
            }
        });
        editText3.setText(Utils.q());
        editText2.setText(FreeMoviesApp.l().getString("pref_payment_bit_mail", ""));
        builder.b((int) R.string.ok, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(MemberActivationActivity.this, KeyManager.class);
                intent.putExtra("TRANSACTION", editText.getText().toString());
                intent.putExtra("EMAIL", editText2.getText().toString());
                intent.putExtra("KEY", editText3.getText().toString());
                MemberActivationActivity.this.startActivity(intent);
            }
        });
        builder.a((int) R.string.cancel, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c();
    }

    public void e() {
        this.loading.setVisibility(0);
        this.activeNow.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296337})
    public void onActivateClick() {
        f();
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296427})
    public void onBtnBitcoinClick() {
        Intent intent = new Intent(this, BitcoinGatewayActivity.class);
        intent.putExtra("isSplitKey", FreeMoviesApp.l().getBoolean("pref_payment_bit_split_keys_mode", false));
        startActivity(intent);
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296418})
    public void onCopyCodeClick() {
        Utils.a((Activity) this, this.c, false);
    }

    /* access modifiers changed from: package-private */
    @OnLongClick({2131296418})
    public void onCopyCodeLongClick() {
        Utils.a((Activity) this, this.c, true);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_member_activation);
        setSupportActionBar(this.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.e(true);
            this.toolbar.setTitle((CharSequence) "Member activation");
            this.toolbar.setNavigationOnClickListener(new a0(this));
        }
        this.c = Utils.q();
        this.f5084a = new CompositeDisposable();
        if (!this.c.isEmpty()) {
            c(this.c);
        } else {
            this.code.setText("You haven't had the code yet.");
            this.loading.setVisibility(8);
        }
        if (!GlobalVariable.c().a().getPayments().contains("bit")) {
            this.btn_bitcoin.setVisibility(8);
        } else {
            this.btn_bitcoin.setVisibility(0);
        }
        if (!GlobalVariable.c().a().getPayments().contains("amz")) {
            this.btn_amz_gift.setVisibility(8);
        } else {
            this.btn_amz_gift.setVisibility(0);
        }
        if (!GlobalVariable.c().a().getPayments().contains("game")) {
            this.btn_game_challenge.setVisibility(8);
        } else {
            this.btn_game_challenge.setVisibility(0);
        }
        String string = FreeMoviesApp.l().getString("pref_payment_bit_mail", "");
        ProductResponse.ResultsBean resultsBean = (ProductResponse.ResultsBean) new Gson().a(FreeMoviesApp.l().getString("pref_payment_bit_product_id", ""), ProductResponse.ResultsBean.class);
        String string2 = FreeMoviesApp.l().getString("pref_payment_bit_address", "");
        if (!string.isEmpty() && !string2.isEmpty()) {
            BitcoinAdressRequest bitcoinAdressRequest = new BitcoinAdressRequest();
            bitcoinAdressRequest.setAddress(string2);
            bitcoinAdressRequest.setDeviceID(Utils.f());
            bitcoinAdressRequest.setEmail(string);
            bitcoinAdressRequest.setProductID(resultsBean.getId());
            bitcoinAdressRequest.setDeviceName(Utils.j());
            this.f5084a.b(this.b.checkPaymentProcess(bitcoinAdressRequest).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new z(this), new b0(this)));
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_keymanager, menu);
        menu.findItem(R.id.action_device_manager);
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f5084a.dispose();
        super.onDestroy();
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296429})
    public void onGameChallengeClick() {
        startActivity(new Intent(this, GameChallenge.class));
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.action_device_manager) {
            d();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296423})
    public void onRemoveClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.a((CharSequence) "Do you want to deactivate?");
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public /* synthetic */ void a(AppConfig appConfig) throws Exception {
                if (appConfig != null) {
                    AdsManager.l().a();
                    GlobalVariable.c().a(new Gson().a((Object) appConfig));
                    MemberActivationActivity memberActivationActivity = MemberActivationActivity.this;
                    memberActivationActivity.a((int) UserResponces.USER_RESPONCE_SUCCSES, memberActivationActivity.getString(R.string.deactive_success));
                    Utils.j("");
                    MemberActivationActivity memberActivationActivity2 = MemberActivationActivity.this;
                    String unused = memberActivationActivity2.c = memberActivationActivity2.c;
                    MemberActivationActivity memberActivationActivity3 = MemberActivationActivity.this;
                    memberActivationActivity3.c(memberActivationActivity3.c);
                    MemberActivationActivity.this.btnRemove.setVisibility(8);
                    MemberActivationActivity.this.btnCopy.setVisibility(8);
                    return;
                }
                MemberActivationActivity memberActivationActivity4 = MemberActivationActivity.this;
                memberActivationActivity4.a((int) UserResponces.USER_RESPONCE_FAIL, memberActivationActivity4.getString(R.string.deactive_faild));
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                MemberActivationActivity.this.e();
                MemberActivationActivity memberActivationActivity = MemberActivationActivity.this;
                memberActivationActivity.f5084a.b(memberActivationActivity.b.deactiveKey(memberActivationActivity.c, Utils.f()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new s(this), new u(this), new t(this)));
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                MemberActivationActivity memberActivationActivity = MemberActivationActivity.this;
                memberActivationActivity.a((int) UserResponces.USER_RESPONCE_FAIL, memberActivationActivity.getString(R.string.deactive_faild));
                MemberActivationActivity.this.c();
            }

            public /* synthetic */ void a() throws Exception {
                MemberActivationActivity.this.c();
            }
        });
        builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ void a(View view) {
        finish();
    }

    public /* synthetic */ void a(BitcoinAddressResponse bitcoinAddressResponse) throws Exception {
        if (bitcoinAddressResponse.getCode().intValue() != 200) {
            bitcoinAddressResponse.getCode().intValue();
        } else if (bitcoinAddressResponse.getRemainingTime().longValue() > 0) {
            Intent intent = new Intent(this, BitcoinGatewayActivity.class);
            intent.putExtra("isSplitKey", bitcoinAddressResponse.getSplitKey());
            FreeMoviesApp.l().edit().putBoolean("isSplitKey", bitcoinAddressResponse.getSplitKey().booleanValue()).apply();
            startActivity(intent);
        }
    }

    public void c() {
        this.loading.setVisibility(8);
        this.activeNow.setVisibility(0);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:13:0x007c, code lost:
        if (r4.compareTo((org.joda.time.ReadableInstant) org.joda.time.DateTime.now()) < 0) goto L_0x0091;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ void a(java.lang.String r8, com.movie.data.model.cinema.KeyResponse r9) throws java.lang.Exception {
        /*
            r7 = this;
            com.movie.data.api.GlobalVariable r0 = com.movie.data.api.GlobalVariable.c()
            com.movie.data.model.AppConfig r0 = r0.a()
            com.movie.data.model.AppConfig$AdsBean r0 = r0.getAds()
            r1 = 1
            r2 = 0
            if (r0 == 0) goto L_0x0012
            r0 = 1
            goto L_0x0013
        L_0x0012:
            r0 = 0
        L_0x0013:
            r3 = 8
            if (r9 == 0) goto L_0x00d8
            java.lang.String r4 = r9.getId()
            if (r4 == 0) goto L_0x00d8
            java.lang.String r4 = r9.getId()
            boolean r4 = r4.isEmpty()
            if (r4 != 0) goto L_0x00d8
            android.widget.Button r4 = r7.activeNow
            r4.setVisibility(r3)
            androidx.constraintlayout.widget.ConstraintLayout r4 = r7.activateResult
            r4.setVisibility(r2)
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            java.lang.String r5 = "Key code : "
            r4.append(r5)
            r4.append(r8)
            java.lang.String r8 = r4.toString()
            java.lang.String r4 = r9.getStartTime()
            if (r4 == 0) goto L_0x007f
            java.lang.String r4 = r9.getStartTime()
            org.joda.time.DateTime r4 = org.joda.time.DateTime.parse(r4)
            long r5 = r9.getTtl()
            org.joda.time.DateTime r4 = r4.plus((long) r5)
            java.lang.StringBuilder r5 = new java.lang.StringBuilder
            r5.<init>()
            r5.append(r8)
            java.lang.String r8 = "\nExpired time : "
            r5.append(r8)
            java.lang.String r8 = r4.toString()
            java.lang.String r8 = com.original.tase.helper.DateTimeHelper.b((java.lang.String) r8)
            r5.append(r8)
            java.lang.String r8 = r5.toString()
            org.joda.time.DateTime r5 = org.joda.time.DateTime.now()
            int r4 = r4.compareTo((org.joda.time.ReadableInstant) r5)
            if (r4 >= 0) goto L_0x0090
            goto L_0x0091
        L_0x007f:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r8)
            java.lang.String r8 = "\nnot activate yet!"
            r1.append(r8)
            java.lang.String r8 = r1.toString()
        L_0x0090:
            r1 = 0
        L_0x0091:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r8)
            java.lang.String r8 = "\nRemaining devices : "
            r4.append(r8)
            int r8 = r9.getLimit()
            int r9 = r9.getCurrentNumberOfDevice()
            int r8 = r8 - r9
            r4.append(r8)
            java.lang.String r8 = r4.toString()
            if (r0 == 0) goto L_0x00c3
            if (r1 != 0) goto L_0x00c3
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r8)
            java.lang.String r8 = "\nSomethings was wrong : Go Setting -> Apps -> Force close and open again to remove Ads."
            r9.append(r8)
            java.lang.String r8 = r9.toString()
        L_0x00c3:
            android.widget.TextView r9 = r7.code
            r9.setText(r8)
            android.widget.ProgressBar r8 = r7.loading
            r8.setVisibility(r3)
            android.widget.Button r8 = r7.btnCopy
            r8.setVisibility(r2)
            android.widget.Button r8 = r7.btnRemove
            r8.setVisibility(r2)
            goto L_0x00ee
        L_0x00d8:
            android.widget.ProgressBar r8 = r7.loading
            r8.setVisibility(r3)
            android.widget.Button r8 = r7.activeNow
            r8.setVisibility(r2)
            android.widget.TextView r8 = r7.code
            java.lang.String r9 = "You haven't had the code yet."
            r8.setText(r9)
            java.lang.String r8 = ""
            com.utils.Utils.j(r8)
        L_0x00ee:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.activity.MemberActivationActivity.a(java.lang.String, com.movie.data.model.cinema.KeyResponse):void");
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        this.code.setText("Make sure your ISP is not blocked our payment service");
        this.loading.setVisibility(8);
        this.activeNow.setVisibility(0);
    }

    public void a(final int i, String str) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.a((CharSequence) str);
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 200) {
                    Utils.c((Activity) MemberActivationActivity.this);
                }
                dialogInterface.dismiss();
            }
        });
        if (i == 200) {
            builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
        }
        builder.c();
    }
}
