package com.movie.ui.activity;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.realdebrid.RealDebridApi;
import dagger.MembersInjector;

public final class SettingsActivity_MembersInjector implements MembersInjector<SettingsActivity> {
    public static void a(SettingsActivity settingsActivity, MvDatabase mvDatabase) {
        settingsActivity.b = mvDatabase;
    }

    public static void a(SettingsActivity settingsActivity, RealDebridApi realDebridApi) {
        settingsActivity.c = realDebridApi;
    }

    public static void a(SettingsActivity settingsActivity, MoviesApi moviesApi) {
        settingsActivity.d = moviesApi;
    }
}
