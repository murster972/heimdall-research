package com.movie.ui.activity.settings.subfragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.preference.Preference;
import com.Setting;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.nononsenseapps.filepicker.FilePickerActivity;
import com.nononsenseapps.filepicker.Utils;
import com.original.tase.Logger;
import com.yoku.marumovie.R;
import java.io.File;

public class DownloadFragment extends BaseSettingFragment {
    public static int PATH_CODE = 1122;

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == PATH_CODE) {
            File a2 = Utils.a(intent.getData());
            Logger.a("Setting", "XML path Uri (" + a2.toString() + ")");
            String file = a2.toString();
            findPreference("pref_dowload_path").setSummary((CharSequence) file);
            getSharedPreference().edit().putString("pref_dowload_path", file).apply();
        }
    }

    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_download, str);
        Preference findPreference = findPreference("pref_dowload_path");
        findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(DownloadFragment.this.getActivity(), FilePickerActivity.class);
                intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", false);
                intent.putExtra("nononsense.intent.ALLOW_CREATE_DIR", true);
                intent.putExtra("nononsense.intent.MODE", 1);
                intent.putExtra("nononsense.intent.START_PATH", DownloadFragment.this.getSharedPreference().getString("pref_dowload_path", Setting.b(com.utils.Utils.i()).toLowerCase()));
                DownloadFragment.this.getActivity().startActivityForResult(intent, DownloadFragment.PATH_CODE);
                return false;
            }
        });
        findPreference.setSummary((CharSequence) getSharedPreference().getString("pref_dowload_path", Setting.b(com.utils.Utils.i()).toLowerCase()));
    }
}
