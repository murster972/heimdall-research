package com.movie.ui.activity.settings;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.google.android.material.tabs.TabItem;
import com.google.android.material.tabs.TabLayout;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;

public class CategoryRetrictionDialog extends DialogFragment {

    /* renamed from: a  reason: collision with root package name */
    private Unbinder f5358a;
    private CompositeDisposable b;
    @BindView(2131296420)
    Button btnDone;
    @BindView(2131297084)
    TabLayout tabLayout;
    @BindView(2131297086)
    TabItem tabMovie;
    @BindView(2131297087)
    TabItem tabShow;
    @BindView(2131296935)
    ViewPager viewPager;

    public static class ShowPageAdapter extends FragmentStatePagerAdapter {
        String h;
        String i;

        public ShowPageAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment a(int i2) {
            if (i2 == 0) {
                return CategoryListFragment.a(this.i, "ref_restrict_mv_category");
            }
            if (i2 != 1) {
                return null;
            }
            return CategoryListFragment.a(this.h, "ref_restrict_show_category");
        }

        public int getCount() {
            return 2;
        }
    }

    public static CategoryRetrictionDialog a(String str, String str2) {
        CategoryRetrictionDialog categoryRetrictionDialog = new CategoryRetrictionDialog();
        Bundle bundle = new Bundle();
        bundle.putString("mvCategorys", str);
        bundle.putString("tvCategorys", str2);
        categoryRetrictionDialog.setArguments(bundle);
        return categoryRetrictionDialog;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                CategoryRetrictionDialog.this.tabLayout.b(i).g();
            }
        });
        ShowPageAdapter showPageAdapter = new ShowPageAdapter(getChildFragmentManager());
        showPageAdapter.h = getArguments().getString("tvCategorys");
        showPageAdapter.i = getArguments().getString("mvCategorys");
        this.viewPager.setAdapter(showPageAdapter);
        this.tabLayout.a((TabLayout.BaseOnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
            public void a(TabLayout.Tab tab) {
                CategoryRetrictionDialog.this.viewPager.setCurrentItem(tab.c());
            }

            public void b(TabLayout.Tab tab) {
            }

            public void c(TabLayout.Tab tab) {
            }
        });
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getDialog().getWindow().getAttributes().windowAnimations = 2131820555;
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(0));
        if (Utils.a((Activity) getActivity()) == 1) {
            getDialog().getWindow().setLayout((int) (((double) getResources().getDisplayMetrics().widthPixels) * 0.9d), (int) (((double) getResources().getDisplayMetrics().heightPixels) * 0.7d));
        } else {
            int i = getResources().getDisplayMetrics().heightPixels;
            getDialog().getWindow().setLayout((int) (((double) getResources().getDisplayMetrics().widthPixels) * 0.7d), i);
        }
        setHasOptionsMenu(true);
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296420})
    public void onBtnDoneClick() {
        dismiss();
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.category_retriction, viewGroup, false);
        this.f5358a = ButterKnife.bind((Object) this, inflate);
        this.b = new CompositeDisposable();
        return inflate;
    }

    public void onDestroy() {
        this.f5358a.unbind();
        this.b.dispose();
        super.onDestroy();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        e();
    }
}
