package com.movie.ui.activity.settings;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import com.movie.AppComponent;
import com.movie.ui.activity.BaseActivity;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;

public class SettingsActivity extends BaseActivity implements PreferenceFragmentCompat.OnPreferenceStartFragmentCallback {
    private static final String TITLE_TAG = "settingsActivityTitle";
    CompositeDisposable compositeDisposable;

    public static class HeaderFragment extends PreferenceFragmentCompat {
        public void onCreatePreferences(Bundle bundle, String str) {
            setPreferencesFromResource(R.xml.header_preferences, str);
        }
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
        for (Fragment onActivityResult : getSupportFragmentManager().p()) {
            onActivityResult.onActivityResult(i, i2, intent);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.settings_activity);
        this.compositeDisposable = new CompositeDisposable();
        if (bundle == null) {
            getSupportFragmentManager().b().b(R.id.settings, new HeaderFragment()).a();
        } else {
            setTitle(bundle.getCharSequence(TITLE_TAG));
        }
        getSupportFragmentManager().a((FragmentManager.OnBackStackChangedListener) new FragmentManager.OnBackStackChangedListener() {
            public void a() {
                if (SettingsActivity.this.getSupportFragmentManager().n() == 0) {
                    SettingsActivity.this.setTitle(R.string.title_activity_settings);
                }
            }
        });
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.compositeDisposable.dispose();
        super.onDestroy();
    }

    public boolean onPreferenceStartFragment(PreferenceFragmentCompat preferenceFragmentCompat, Preference preference) {
        Bundle extras = preference.getExtras();
        Fragment a2 = getSupportFragmentManager().o().a(getClassLoader(), preference.getFragment());
        a2.setArguments(extras);
        a2.setTargetFragment(preferenceFragmentCompat, 0);
        getSupportFragmentManager().b().a((int) R.anim.slide_in_right, (int) R.anim.slide_out_left, (int) R.anim.slide_in_left, (int) R.anim.slide_out_right).b(R.id.settings, a2).a((String) null).a();
        setTitle(preference.getTitle());
        return true;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putCharSequence(TITLE_TAG, getTitle());
    }

    public boolean onSupportNavigateUp() {
        if (getSupportFragmentManager().z()) {
            return true;
        }
        return super.onSupportNavigateUp();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }
}
