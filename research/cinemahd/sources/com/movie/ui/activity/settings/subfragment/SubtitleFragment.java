package com.movie.ui.activity.settings.subfragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.MultiSelectListPreference;
import androidx.preference.Preference;
import com.flask.colorpicker.ColorPickerView;
import com.flask.colorpicker.OnColorSelectedListener;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;
import com.movie.FreeMoviesApp;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.utils.Subtitle.services.LanguageId;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;

public class SubtitleFragment extends BaseSettingFragment implements Preference.OnPreferenceChangeListener {
    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_subtitle, str);
        setupSubtitleList();
        setupColor();
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        if (!preference.getKey().equals("pref_sub_language_international_v3")) {
            return false;
        }
        preference.setSummary((CharSequence) obj.toString());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void setupColor() {
        Preference findPreference = findPreference("pref_cc_subs_font_color");
        findPreference.setSummary((CharSequence) getSharedPreference().getString("pref_cc_subs_font_color", "#FFFFFFFF").toUpperCase());
        findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(final Preference preference) {
                ColorPickerDialogBuilder a2 = ColorPickerDialogBuilder.a((Context) SubtitleFragment.this.getActivity());
                a2.a("Choose subtitle color");
                a2.b(Color.parseColor(FreeMoviesApp.l().getString("pref_cc_subs_font_color", "#FFFFFFFF")));
                a2.a(true);
                a2.a(ColorPickerView.WHEEL_TYPE.FLOWER);
                a2.a(12);
                a2.a((OnColorSelectedListener) new OnColorSelectedListener() {
                    public void a(int i) {
                        FragmentActivity activity = SubtitleFragment.this.getActivity();
                        Utils.a((Activity) activity, "onColorSelected: 0x" + Integer.toHexString(i));
                    }
                });
                a2.a((CharSequence) "ok", (ColorPickerClickListener) new ColorPickerClickListener() {
                    public void a(DialogInterface dialogInterface, int i, Integer[] numArr) {
                        String hexString = Integer.toHexString(i);
                        Utils.a((Activity) SubtitleFragment.this.getActivity(), "onColorSelected: 0x" + hexString);
                        String str = "#" + hexString.toUpperCase();
                        preference.setSummary((CharSequence) str);
                        FreeMoviesApp.l().edit().putString("pref_cc_subs_font_color", str).apply();
                    }
                });
                a2.a((CharSequence) "cancel", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                a2.b().show();
                return true;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setupSubtitleList() {
        MultiSelectListPreference multiSelectListPreference = (MultiSelectListPreference) findPreference("pref_sub_language_international_v3");
        String[] a2 = LanguageId.c().a();
        multiSelectListPreference.setEntries((CharSequence[]) LanguageId.c().b());
        multiSelectListPreference.setEntryValues((CharSequence[]) a2);
        multiSelectListPreference.setOnPreferenceChangeListener(this);
        multiSelectListPreference.setSummary((CharSequence) FreeMoviesApp.l().getStringSet("pref_sub_language_international_v3", new HashSet(Arrays.asList(new String[]{Locale.getDefault().getLanguage()}))).toString());
    }
}
