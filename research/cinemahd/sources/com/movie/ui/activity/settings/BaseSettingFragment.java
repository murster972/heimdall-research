package com.movie.ui.activity.settings;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.TextView;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseSettingFragment extends PreferenceFragmentCompat {
    protected CompositeDisposable compositeDisposable;
    private ProgressDialog waitingDialog = null;

    /* access modifiers changed from: protected */
    public SharedPreferences getSharedPreference() {
        return getPreferenceManager().h();
    }

    public void hideWaitingDialog() {
        ProgressDialog progressDialog = this.waitingDialog;
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void onCreate(Bundle bundle) {
        this.compositeDisposable = new CompositeDisposable();
        super.onCreate(bundle);
    }

    public void onCreatePreferences(Bundle bundle, String str) {
        PreferenceManager preferenceManager = getPreferenceManager();
        preferenceManager.a("hdmovies");
        preferenceManager.a(0);
    }

    public void onDestroy() {
        this.compositeDisposable.dispose();
        super.onDestroy();
    }

    public void showWaitingDialog(int i) {
        showWaitingDialog(getActivity().getString(i));
    }

    public void showWaitingDialog(String str) {
        if (this.waitingDialog == null) {
            this.waitingDialog = new ProgressDialog(getActivity());
            try {
                this.waitingDialog.show();
            } catch (WindowManager.BadTokenException unused) {
            }
        }
        this.waitingDialog.setCancelable(true);
        this.waitingDialog.getWindow().setBackgroundDrawableResource(R.color.transparent);
        this.waitingDialog.setContentView(R.layout.progressbar);
        TextView textView = (TextView) this.waitingDialog.findViewById(R.id.tv_title);
        if (!str.isEmpty()) {
            textView.setText(str);
            textView.setVisibility(0);
        } else {
            textView.setVisibility(8);
        }
        this.waitingDialog.show();
    }
}
