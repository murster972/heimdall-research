package com.movie.ui.activity.settings.subfragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.ViewGroup;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import com.ads.videoreward.AdsManager;
import com.database.MvDatabase;
import com.movie.FreeMoviesApp;
import com.movie.ui.activity.HelpRecaptchar;
import com.movie.ui.activity.SettingsActivity;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.original.tase.Logger;
import com.original.tase.helper.player.BasePlayerHelper;
import com.utils.PrefUtils;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.util.ArrayList;

public class GeneralFragment extends BaseSettingFragment implements Preference.OnPreferenceChangeListener {
    public static int PATH_CODE = 1122;
    public static int PATH_RESTORE_CODE = 1123;
    public static int VERYFY_CAPTCHAR_CODE = 1124;

    static /* synthetic */ void a(Void voidR) throws Exception {
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        FragmentActivity activity = getActivity();
        Utils.a((Activity) activity, "Restore failed : " + th.getMessage());
    }

    /* access modifiers changed from: package-private */
    public void configDefaultAction() {
        ListPreference listPreference = (ListPreference) findPreference("pref_choose_default_action");
        ArrayList arrayList = new ArrayList();
        for (String add : Utils.a(true).keySet()) {
            arrayList.add(add);
        }
        listPreference.setEntries((CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]));
        listPreference.setEntryValues((CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]));
        listPreference.setDefaultValue("Always ask");
        listPreference.setSummary(getSharedPreference().getString("pref_choose_default_action", "Always ask"));
        listPreference.setOnPreferenceChangeListener(this);
    }

    /* access modifiers changed from: package-private */
    public void configDefaultPlayer() {
        ListPreference listPreference = (ListPreference) findPreference("pref_choose_default_player");
        BasePlayerHelper[] f = BasePlayerHelper.f();
        CharSequence[] charSequenceArr = new CharSequence[f.length];
        CharSequence[] charSequenceArr2 = new CharSequence[f.length];
        for (int i = 0; i < f.length; i++) {
            charSequenceArr[i] = f[i].c();
            charSequenceArr2[i] = f[i].b();
        }
        listPreference.setEntries(charSequenceArr);
        listPreference.setEntryValues(charSequenceArr2);
        listPreference.setDefaultValue(BasePlayerHelper.d().b());
        listPreference.setSummary(getSharedPreference().getString("pref_choose_default_player", BasePlayerHelper.d().b()));
        listPreference.setOnPreferenceChangeListener(this);
    }

    /* access modifiers changed from: package-private */
    public void configPosterImageSize() {
        ArrayList arrayList = new ArrayList();
        new ArrayList();
        for (String add : Utils.t().keySet()) {
            arrayList.add(add);
        }
        ListPreference listPreference = (ListPreference) findPreference("pref_column_in_main");
        listPreference.setEntries((CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]));
        listPreference.setEntryValues((CharSequence[]) arrayList.toArray(new CharSequence[arrayList.size()]));
        listPreference.setDefaultValue("Large");
        listPreference.setSummary(getSharedPreference().getString("pref_column_in_main", "Large"));
        listPreference.setOnPreferenceChangeListener(this);
    }

    public /* synthetic */ void e() throws Exception {
        Utils.a((Activity) getActivity(), "Restore successful");
    }

    public void onActivityResult(int i, int i2, final Intent intent) {
        super.onActivityResult(i, i2, intent);
        if (i == PATH_RESTORE_CODE && i2 == -1) {
            this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Void>() {
                public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                    File a2 = com.nononsenseapps.filepicker.Utils.a(intent.getData());
                    Logger.a("Setting", "XML path Uri (" + a2.toString() + ")");
                    String name = a2.getName();
                    MvDatabase.b(GeneralFragment.this.getActivity(), name);
                    PrefUtils.a((Context) GeneralFragment.this.getActivity(), name);
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(i.f5413a, new h(this), new g(this)));
        } else if (i == PATH_CODE && i2 == -1) {
            File a2 = com.nononsenseapps.filepicker.Utils.a(intent.getData());
            Logger.a("Setting", "XML path Uri (" + a2.toString() + ")");
            String file = a2.toString();
            SettingsActivity.SettingsFragment.pref_dowload_path.setSummary((CharSequence) file);
            FreeMoviesApp.l().edit().putString(SettingsActivity.SettingsFragment.pref_dowload_path.getKey(), file).apply();
        }
    }

    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_general, str);
        configDefaultPlayer();
        configPosterImageSize();
        configDefaultAction();
        findPreference("pref_changelog").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                GeneralFragment.this.showChangelog();
                return true;
            }
        });
        findPreference("pref_clear_cache").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                Utils.b((Context) GeneralFragment.this.getActivity());
                Utils.a((Activity) GeneralFragment.this.getActivity(), "Cache cleared.");
                return true;
            }
        });
        findPreference("pref_recoptchar").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                GeneralFragment.this.startActivityForResult(new Intent(Utils.i(), HelpRecaptchar.class), 5);
                return true;
            }
        });
        Preference findPreference = findPreference("pref_show_intertisial_ads");
        if (findPreference != null) {
            findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(this) {
                public boolean onPreferenceClick(Preference preference) {
                    AdsManager.l().j();
                    return true;
                }
            });
        }
        Preference findPreference2 = findPreference("pref_show_video_ads");
        if (findPreference2 != null) {
            findPreference2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener(this) {
                public boolean onPreferenceClick(Preference preference) {
                    AdsManager.l().k();
                    return true;
                }
            });
        }
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        preference.setSummary((CharSequence) obj.toString());
        if (!preference.getKey().equals("pref_column_in_main")) {
            return true;
        }
        Utils.c((Activity) getActivity());
        return true;
    }

    public void showChangelog() {
        new SpannableStringBuilder("Change Logs").setSpan(new ForegroundColorSpan(-256), 0, 11, 33);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.b((CharSequence) "Change Logs");
        builder.b(getActivity().getLayoutInflater().inflate(R.layout.dialog_changelog, (ViewGroup) null));
        builder.b((CharSequence) "OK", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (dialogInterface != null) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.a().show();
    }
}
