package com.movie.ui.activity.settings.subfragment;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.CookieManager;
import android.webkit.ValueCallback;
import android.widget.EditText;
import android.widget.LinearLayout;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import com.database.MvDatabase;
import com.google.gson.Gson;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.premiumize.PremiumizeModule;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.ui.activity.AllDebridAuthWebViewActivity;
import com.movie.ui.activity.TraktAuthWebViewActivity;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.api.TraktUserApi;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.alldebrid.AllDebridUserApi;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeUserApi;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.event.ApiDebridGetTokenFailedEvent;
import com.original.tase.event.ApiDebridGetTokenSuccessEvent;
import com.original.tase.event.ApiDebridWaitingToVerifyEvent;
import com.original.tase.event.trakt.TraktGetTokenFailedEvent;
import com.original.tase.event.trakt.TraktGetTokenSuccessEvent;
import com.original.tase.event.trakt.TraktWaitingToVerifyEvent;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.original.tase.helper.trakt.TraktHelper;
import com.original.tase.model.debrid.alldebrid.ADUserInfor;
import com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo;
import com.original.tase.model.debrid.premiumize.PremiumizeCredentialsInfo;
import com.original.tase.model.debrid.premiumize.PremiumizeUserInfo;
import com.original.tase.model.debrid.realdebrid.RealDebridCheckAuthResult;
import com.original.tase.model.debrid.realdebrid.RealDebridGetDeviceCodeResult;
import com.original.tase.model.debrid.realdebrid.RealDebridGetTokenResult;
import com.original.tase.model.debrid.realdebrid.RealDebridUserInfor;
import com.utils.Utils;
import com.uwetrottmann.trakt5.entities.User;
import com.uwetrottmann.trakt5.entities.UserSlug;
import com.uwetrottmann.trakt5.enums.Extended;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import javax.inject.Inject;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Response;

public class PremiumAccountFragment extends BaseSettingFragment implements Preference.OnPreferenceChangeListener {
    public static final int requestOpenWebviewCode = 100;
    Preference adPref;
    CompositeDisposable compositeDisposable;
    CompositeDisposable loginDisposable;
    @Inject
    MoviesApi moviesApi;
    @Inject
    MvDatabase mvDatabase;
    Preference pmPref;
    Preference rdPref;
    @Inject
    RealDebridApi realDebridApi;
    Preference traktPref;

    static /* synthetic */ void g(Throwable th) throws Exception {
    }

    static /* synthetic */ void h(Throwable th) throws Exception {
    }

    public /* synthetic */ void a(RealDebridUserInfor realDebridUserInfor) throws Exception {
        Logger.a("Real Debrid ", realDebridUserInfor.toString());
        boolean equalsIgnoreCase = realDebridUserInfor.type.equalsIgnoreCase("premium");
        getSharedPreference().edit().putBoolean("pref_realdebrid_type", equalsIgnoreCase).commit();
        getSharedPreference().edit().putString("pref_realdebrid_expiration_str", realDebridUserInfor.expiration).commit();
        String b = DateTimeHelper.b(realDebridUserInfor.expiration);
        if (equalsIgnoreCase) {
            SharedPreferences.Editor edit = getSharedPreference().edit();
            edit.putString("pref_rd_expiration", "Real-Debrid authorized \nUsername : " + realDebridUserInfor.username + "\nType : " + realDebridUserInfor.type + "\nExpiration : " + b).commit();
        } else {
            SharedPreferences.Editor edit2 = getSharedPreference().edit();
            edit2.putString("pref_rd_expiration", "Real-Debrid authorized \nUsername : " + realDebridUserInfor.username + "\nType : " + realDebridUserInfor.type).commit();
        }
        this.rdPref.setSummary((CharSequence) getSharedPreference().getString("pref_rd_expiration", ""));
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        getSharedPreference().edit().putString("pref_pm_info", "").commit();
        this.pmPref.setSummary((CharSequence) "");
        this.pmPref.setTitle((CharSequence) "Login to Premiumize");
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        RealDebridCredentialsHelper.a();
        getSharedPreference().edit().putString("pref_rd_expiration", "").commit();
        this.rdPref.setTitle((CharSequence) "Login to Real-Debird");
        this.rdPref.setSummary((CharSequence) "");
        getSharedPreference().edit().putBoolean("pref_show_debrid_only", false).apply();
        FragmentActivity activity = getActivity();
        Utils.a((Activity) activity, "RealDebrid Error " + th.getMessage());
    }

    public /* synthetic */ void d(Throwable th) throws Exception {
        getSharedPreference().edit().putString("pref_trakt_info", "").commit();
        this.traktPref.setSummary((CharSequence) "");
        this.traktPref.setTitle((CharSequence) "Login to Trakt");
    }

    public /* synthetic */ void e(Throwable th) throws Exception {
        hideWaitingDialog();
    }

    public /* synthetic */ void f(Object obj) throws Exception {
        hideWaitingDialog();
        if (obj instanceof ApiDebridWaitingToVerifyEvent) {
            ApiDebridWaitingToVerifyEvent apiDebridWaitingToVerifyEvent = (ApiDebridWaitingToVerifyEvent) obj;
            Intent intent = new Intent(getActivity(), AllDebridAuthWebViewActivity.class);
            intent.putExtra("verificationUrl", apiDebridWaitingToVerifyEvent.b());
            intent.putExtra("pin", apiDebridWaitingToVerifyEvent.a());
            getActivity().startActivityForResult(intent, 100);
        } else if (obj instanceof TraktWaitingToVerifyEvent) {
            TraktWaitingToVerifyEvent traktWaitingToVerifyEvent = (TraktWaitingToVerifyEvent) obj;
            Intent intent2 = new Intent(getActivity(), TraktAuthWebViewActivity.class);
            intent2.putExtra("verificationUrl", traktWaitingToVerifyEvent.b());
            intent2.putExtra("userCode", traktWaitingToVerifyEvent.a());
            getActivity().startActivityForResult(intent2, 100);
        }
    }

    /* access modifiers changed from: package-private */
    public void getAlDebirdUserInfo() {
        final AllDebridCredentialsInfo b = AllDebridCredentialsHelper.b();
        if (b.isValid()) {
            this.adPref.setTitle((CharSequence) "Logout");
            this.adPref.setSummary((CharSequence) getSharedPreference().getString("pref_ad_expiration", ""));
            this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Void>() {
                public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                    String str;
                    try {
                        str = "https://api.alldebrid.com/v4/user?agent=" + URLEncoder.encode(Utils.k, "UTF-8") + "&apikey=" + URLEncoder.encode(b.getApikey(), "UTF-8");
                    } catch (UnsupportedEncodingException unused) {
                        str = "https://api.alldebrid.com/v4/user?agent=" + Utils.k + "&apikey=" + b.getApikey();
                    }
                    String string = new OkHttpClient().newCall(new Request.Builder().url(str).build()).execute().body().string();
                    if (!string.isEmpty()) {
                        ADUserInfor aDUserInfor = (ADUserInfor) new Gson().a(string, ADUserInfor.class);
                        Logger.a("All Debrid ", aDUserInfor.toString());
                        if (aDUserInfor != null && aDUserInfor.getStatus().contains("success")) {
                            String c = DateTimeHelper.c(aDUserInfor.getData().getUser().getPremiumUntil());
                            PremiumAccountFragment.this.getSharedPreference().edit().putBoolean("pref_alldebrid_type", aDUserInfor.getData().getUser().isIsPremium()).commit();
                            PremiumAccountFragment.this.getSharedPreference().edit().putString("pref_alldebrid_expiration_str", aDUserInfor.getData().getUser().getPremiumUntil()).commit();
                            if (aDUserInfor.getData().getUser().isIsPremium()) {
                                PremiumAccountFragment.this.getSharedPreference().edit().putString("pref_ad_expiration", "All-Debrid authorized \nUsername : " + aDUserInfor.getData().getUser().getUsername() + "\nType : Premium \nExpiration : " + c).commit();
                            } else {
                                PremiumAccountFragment.this.getSharedPreference().edit().putString("pref_ad_expiration", "All-Debrid authorized \nUsername : " + aDUserInfor.getData().getUser().getUsername() + "\nType : Free").commit();
                            }
                            AllDebridUserApi.d().a();
                        }
                    }
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new m(this), new x(this)));
        }
    }

    /* access modifiers changed from: package-private */
    public void getPremiumizeDebirdUserInfo(final String str) {
        String string = getSharedPreference().getString("pref_pm_info", "");
        if (!string.isEmpty()) {
            this.pmPref.setTitle((CharSequence) "Logout");
            this.pmPref.setSummary((CharSequence) string);
        }
        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<PremiumizeUserInfo>(this) {
            public void subscribe(ObservableEmitter<PremiumizeUserInfo> observableEmitter) throws Exception {
                observableEmitter.onNext(PremiumizeModule.b().getPremiumizeUserInfo(str).execute().body());
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new s(this, str), new q(this)));
    }

    /* access modifiers changed from: package-private */
    public void getRealDebirdUserInfo() {
        String string = getSharedPreference().getString("pref_rd_expiration", "");
        if (!string.isEmpty()) {
            this.rdPref.setTitle((CharSequence) "Logout");
            this.rdPref.setSummary((CharSequence) string);
        }
        this.loginDisposable.b(Observable.create(new ObservableOnSubscribe<RealDebridUserInfor>() {
            public void subscribe(ObservableEmitter<RealDebridUserInfor> observableEmitter) throws Exception {
                Response<RealDebridUserInfor> execute = PremiumAccountFragment.this.realDebridApi.getUserInfo().execute();
                if (execute.code() == 200) {
                    observableEmitter.onNext(execute.body());
                }
                if (execute.code() != 401) {
                    observableEmitter.onComplete();
                    return;
                }
                throw new Exception(execute.message());
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new w(this), new v(this)));
    }

    /* access modifiers changed from: package-private */
    public void getTraktUserInfo() {
        String string = getSharedPreference().getString("pref_trakt_info", "");
        if (!string.isEmpty()) {
            this.traktPref.setTitle((CharSequence) "Logout");
            this.traktPref.setSummary((CharSequence) string);
        }
        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<User>(this) {
            public void subscribe(ObservableEmitter<User> observableEmitter) throws Exception {
                observableEmitter.onNext(TraktHelper.a().users().profile(UserSlug.ME, Extended.FULL).execute().body());
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new t(this), new r(this)));
    }

    /* access modifiers changed from: package-private */
    public void loginALlDebird() {
        showWaitingDialog((int) R.string.msg_wait);
        this.loginDisposable.b(Observable.create(new ObservableOnSubscribe<Boolean>(this) {
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v23, resolved type: java.lang.Object} */
            /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v4, resolved type: com.original.tase.model.debrid.alldebrid.ADGetTokenResult} */
            /* JADX WARNING: Multi-variable type inference failed */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void subscribe(io.reactivex.ObservableEmitter<java.lang.Boolean> r16) throws java.lang.Exception {
                /*
                    r15 = this;
                    r1 = r16
                    java.lang.String r2 = "success"
                    com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo r0 = com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper.b()
                    boolean r0 = r0.isValid()
                    r3 = 1
                    java.lang.Boolean r4 = java.lang.Boolean.valueOf(r3)
                    if (r0 == 0) goto L_0x001a
                    r1.onNext(r4)
                    r16.onComplete()
                    return
                L_0x001a:
                    java.util.HashMap r5 = com.original.tase.debrid.alldebrid.AllDebridUserApi.c()
                    r6 = 0
                    com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x0159 }
                    r0.<init>()     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.helper.http.HttpHelper r7 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x0159 }
                    java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0159 }
                    r8.<init>()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r9 = "https://api.alldebrid.com/v4/pin/get?agent="
                    r8.append(r9)     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r9 = com.utils.Utils.k     // Catch:{ Exception -> 0x0159 }
                    r8.append(r9)     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r8 = r8.toString()     // Catch:{ Exception -> 0x0159 }
                    java.lang.Object[] r9 = new java.lang.Object[r6]     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r8 = java.lang.String.format(r8, r9)     // Catch:{ Exception -> 0x0159 }
                    java.util.Map[] r9 = new java.util.Map[r6]     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r7 = r7.a((java.lang.String) r8, (java.util.Map<java.lang.String, java.lang.String>[]) r9)     // Catch:{ Exception -> 0x0159 }
                    java.lang.Class<com.original.tase.model.debrid.alldebrid.ADPin> r8 = com.original.tase.model.debrid.alldebrid.ADPin.class
                    java.lang.Object r0 = r0.a((java.lang.String) r7, r8)     // Catch:{ Exception -> 0x0159 }
                    r7 = r0
                    com.original.tase.model.debrid.alldebrid.ADPin r7 = (com.original.tase.model.debrid.alldebrid.ADPin) r7     // Catch:{ Exception -> 0x0159 }
                    if (r7 == 0) goto L_0x014e
                    java.lang.String r0 = r7.getStatus()     // Catch:{ Exception -> 0x0159 }
                    boolean r0 = r0.contains(r2)     // Catch:{ Exception -> 0x0159 }
                    if (r0 != 0) goto L_0x005e
                    goto L_0x014e
                L_0x005e:
                    com.original.tase.RxBus r0 = com.original.tase.RxBus.b()     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.event.ApiDebridWaitingToVerifyEvent r8 = new com.original.tase.event.ApiDebridWaitingToVerifyEvent     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.model.debrid.alldebrid.ADPin$DataBean r9 = r7.getData()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r9 = r9.getBase_url()     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.model.debrid.alldebrid.ADPin$DataBean r10 = r7.getData()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r10 = r10.getPin()     // Catch:{ Exception -> 0x0159 }
                    r8.<init>(r9, r10)     // Catch:{ Exception -> 0x0159 }
                    r0.a(r8)     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.model.debrid.alldebrid.ADPin$DataBean r0 = r7.getData()     // Catch:{ Exception -> 0x0159 }
                    int r8 = r0.getExpires_in()     // Catch:{ Exception -> 0x0159 }
                    r0 = 0
                    r10 = r0
                    r9 = 0
                L_0x0085:
                    if (r9 >= r8) goto L_0x00fa
                    boolean r0 = r16.isDisposed()     // Catch:{ Exception -> 0x0159 }
                    if (r0 != 0) goto L_0x00fa
                    r11 = 3000(0xbb8, double:1.482E-320)
                    java.lang.Thread.sleep(r11)     // Catch:{ Exception -> 0x00f1 }
                    com.google.gson.Gson r0 = new com.google.gson.Gson     // Catch:{ Exception -> 0x00f1 }
                    r0.<init>()     // Catch:{ Exception -> 0x00f1 }
                    com.original.tase.helper.http.HttpHelper r11 = com.original.tase.helper.http.HttpHelper.e()     // Catch:{ Exception -> 0x00f1 }
                    com.original.tase.model.debrid.alldebrid.ADPin$DataBean r12 = r7.getData()     // Catch:{ Exception -> 0x00f1 }
                    java.lang.String r12 = r12.getCheck_url()     // Catch:{ Exception -> 0x00f1 }
                    java.lang.Object[] r13 = new java.lang.Object[r3]     // Catch:{ Exception -> 0x00f1 }
                    java.lang.String r14 = com.utils.Utils.k     // Catch:{ Exception -> 0x00f1 }
                    r13[r6] = r14     // Catch:{ Exception -> 0x00f1 }
                    java.lang.String r12 = java.lang.String.format(r12, r13)     // Catch:{ Exception -> 0x00f1 }
                    java.util.Map[] r13 = new java.util.Map[r3]     // Catch:{ Exception -> 0x00f1 }
                    r13[r6] = r5     // Catch:{ Exception -> 0x00f1 }
                    java.lang.String r11 = r11.a((java.lang.String) r12, (java.util.Map<java.lang.String, java.lang.String>[]) r13)     // Catch:{ Exception -> 0x00f1 }
                    java.lang.Class<com.original.tase.model.debrid.alldebrid.ADGetTokenResult> r12 = com.original.tase.model.debrid.alldebrid.ADGetTokenResult.class
                    java.lang.Object r0 = r0.a((java.lang.String) r11, r12)     // Catch:{ Exception -> 0x00f1 }
                    r11 = r0
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult r11 = (com.original.tase.model.debrid.alldebrid.ADGetTokenResult) r11     // Catch:{ Exception -> 0x00f1 }
                    java.lang.String r0 = r11.getStatus()     // Catch:{ Exception -> 0x00ee }
                    boolean r0 = r0.contains(r2)     // Catch:{ Exception -> 0x00ee }
                    if (r0 == 0) goto L_0x00ec
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r0 = r11.getData()     // Catch:{ Exception -> 0x00ee }
                    boolean r0 = r0.isActivated()     // Catch:{ Exception -> 0x00ee }
                    if (r0 == 0) goto L_0x00ec
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r0 = r11.getData()     // Catch:{ Exception -> 0x00ee }
                    java.lang.String r0 = r0.getApikey()     // Catch:{ Exception -> 0x00ee }
                    if (r0 == 0) goto L_0x00ec
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r0 = r11.getData()     // Catch:{ Exception -> 0x00ee }
                    java.lang.String r0 = r0.getApikey()     // Catch:{ Exception -> 0x00ee }
                    boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x00ee }
                    if (r0 != 0) goto L_0x00ec
                    r10 = r11
                    goto L_0x00fa
                L_0x00ec:
                    r10 = r11
                    goto L_0x00f7
                L_0x00ee:
                    r0 = move-exception
                    r10 = r11
                    goto L_0x00f2
                L_0x00f1:
                    r0 = move-exception
                L_0x00f2:
                    boolean[] r11 = new boolean[r6]     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r11)     // Catch:{ Exception -> 0x0159 }
                L_0x00f7:
                    int r9 = r9 + 1
                    goto L_0x0085
                L_0x00fa:
                    if (r10 == 0) goto L_0x0143
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r0 = r10.getData()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r0 = r0.getApikey()     // Catch:{ Exception -> 0x0159 }
                    if (r0 == 0) goto L_0x0143
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r0 = r10.getData()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r0 = r0.getApikey()     // Catch:{ Exception -> 0x0159 }
                    boolean r0 = r0.isEmpty()     // Catch:{ Exception -> 0x0159 }
                    if (r0 == 0) goto L_0x0115
                    goto L_0x0143
                L_0x0115:
                    com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo r0 = new com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo     // Catch:{ Exception -> 0x0159 }
                    r0.<init>()     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r2 = r10.getData()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r2 = r2.getApikey()     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.model.debrid.alldebrid.ADGetTokenResult$DataBean r3 = r10.getData()     // Catch:{ Exception -> 0x0159 }
                    int r3 = r3.getExpires_in()     // Catch:{ Exception -> 0x0159 }
                    long r8 = (long) r3     // Catch:{ Exception -> 0x0159 }
                    r0.setApiKey(r2, r8)     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.model.debrid.alldebrid.ADPin$DataBean r2 = r7.getData()     // Catch:{ Exception -> 0x0159 }
                    java.lang.String r2 = r2.getPin()     // Catch:{ Exception -> 0x0159 }
                    r0.setPin(r2)     // Catch:{ Exception -> 0x0159 }
                    com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper.a(r0)     // Catch:{ Exception -> 0x0159 }
                    r1.onNext(r4)     // Catch:{ Exception -> 0x0159 }
                    r16.onComplete()     // Catch:{ Exception -> 0x0159 }
                    goto L_0x0166
                L_0x0143:
                    java.lang.Boolean r0 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x0159 }
                    r1.onNext(r0)     // Catch:{ Exception -> 0x0159 }
                    r16.onComplete()     // Catch:{ Exception -> 0x0159 }
                    goto L_0x0166
                L_0x014e:
                    java.lang.Boolean r0 = java.lang.Boolean.valueOf(r6)     // Catch:{ Exception -> 0x0159 }
                    r1.onNext(r0)     // Catch:{ Exception -> 0x0159 }
                    r16.onComplete()     // Catch:{ Exception -> 0x0159 }
                    return
                L_0x0159:
                    r0 = move-exception
                    boolean[] r2 = new boolean[r6]
                    com.original.tase.Logger.a((java.lang.Throwable) r0, (boolean[]) r2)
                    java.lang.Boolean r0 = java.lang.Boolean.valueOf(r6)
                    r1.onNext(r0)
                L_0x0166:
                    return
                */
                throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.activity.settings.subfragment.PremiumAccountFragment.AnonymousClass10.subscribe(io.reactivex.ObservableEmitter):void");
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new u(this), new n(this)));
    }

    /* access modifiers changed from: package-private */
    public void loginPremiumize() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.b((CharSequence) "Authorize Premiumize");
        AlertDialog a2 = builder.a();
        a2.a((CharSequence) " Enter Apikey. Available at https://www.premiumize.me/account");
        final EditText editText = new EditText(getActivity());
        editText.setLayoutParams(new LinearLayout.LayoutParams(-1, -1));
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, final boolean z) {
                editText.post(new Runnable() {
                    public void run() {
                        InputMethodManager inputMethodManager = (InputMethodManager) PremiumAccountFragment.this.getActivity().getSystemService("input_method");
                        if (z) {
                            inputMethodManager.showSoftInput(editText, 1);
                        } else {
                            inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
                        }
                    }
                });
            }
        });
        a2.a((View) editText);
        a2.a(-1, I18N.a(R.string.ok), new DialogInterface.OnClickListener() {
            public /* synthetic */ void a(String str, Boolean bool) throws Exception {
                if (bool.booleanValue()) {
                    PremiumAccountFragment.this.pmPref.setTitle((CharSequence) "Logout");
                    PremiumAccountFragment.this.getPremiumizeDebirdUserInfo(str);
                    return;
                }
                Utils.a((Activity) PremiumAccountFragment.this.getActivity(), (int) R.string.premiumiz_error_incorrect_apikey);
            }

            public void onClick(DialogInterface dialogInterface, int i) {
                String obj = editText.getText().toString();
                if (obj.isEmpty()) {
                    Utils.a((Activity) PremiumAccountFragment.this.getActivity(), (int) R.string.premiumiz_error_incorrect_apikey);
                    return;
                }
                Utils.a((Activity) PremiumAccountFragment.this.getActivity(), (int) R.string.msg_wait);
                PremiumAccountFragment.this.compositeDisposable.b(PremiumizeUserApi.c().a(obj).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new l(this, obj), new k(this)));
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                Utils.a((Activity) PremiumAccountFragment.this.getActivity(), (int) R.string.premiumiz_error_incorrect_apikey);
            }
        });
        a2.a(-2, I18N.a(R.string.cancel), new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        a2.show();
    }

    /* access modifiers changed from: package-private */
    public void loginRealDebird() {
        showWaitingDialog((int) R.string.msg_wait);
        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<RealDebridGetTokenResult>() {
            public void subscribe(ObservableEmitter<RealDebridGetTokenResult> observableEmitter) throws Exception {
                String rd_client_id = GlobalVariable.c().a().getRd_config().getRd_client_id();
                RealDebridGetDeviceCodeResult body = PremiumAccountFragment.this.realDebridApi.oauthDeviceCode(rd_client_id).execute().body();
                String verification_url = body.getVerification_url();
                String user_code = body.getUser_code();
                String device_code = body.getDevice_code();
                int expires_in = body.getExpires_in();
                int interval = body.getInterval();
                RxBus.b().a(new ApiDebridWaitingToVerifyEvent(verification_url, user_code));
                int i = 0;
                while (true) {
                    if (i >= expires_in || PremiumAccountFragment.this.compositeDisposable.isDisposed()) {
                        break;
                    }
                    try {
                        Thread.sleep(1000);
                        if (((float) i) % ((float) interval) == 0.0f) {
                            RealDebridCheckAuthResult body2 = PremiumAccountFragment.this.realDebridApi.oauthDeviceCredentials(rd_client_id, device_code).execute().body();
                            try {
                                String client_id = body2.getClient_id();
                                String client_secret = body2.getClient_secret();
                                if (client_id != null && client_secret != null && !client_id.isEmpty() && !client_secret.isEmpty()) {
                                    HashMap hashMap = new HashMap();
                                    hashMap.put("client_id", client_id);
                                    hashMap.put("client_secret", client_secret);
                                    hashMap.put("code", device_code);
                                    hashMap.put("grant_type", "http://oauth.net/grant_type/device/1.0");
                                    RealDebridGetTokenResult body3 = PremiumAccountFragment.this.realDebridApi.oauthtoken(hashMap).execute().body();
                                    if (body3 != null) {
                                        body3.setLast_clientID(client_id);
                                        body3.setLast_clientSecret(client_secret);
                                        observableEmitter.onNext(body3);
                                        break;
                                    }
                                }
                            } catch (Exception e) {
                                Logger.a((Throwable) e, new boolean[0]);
                            }
                        }
                    } catch (Exception e2) {
                        Logger.a((Throwable) e2, new boolean[0]);
                    }
                    i++;
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new z(this), new y(this)));
    }

    /* access modifiers changed from: package-private */
    public void loginTrakt() {
        this.compositeDisposable.b(TraktUserApi.f().e().subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new p(this), j.f5414a));
    }

    /* access modifiers changed from: package-private */
    public void logoutAllDebird() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.a((CharSequence) "Do you want logout to All-Debrid?");
        final AlertDialog a2 = builder.a();
        a2.a(-1, "ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                AllDebridCredentialsHelper.a();
                CookieManager.getInstance().removeAllCookie();
                PremiumAccountFragment.this.adPref.setSummary((CharSequence) "All-Debird");
                PremiumAccountFragment.this.getSharedPreference().edit().putString("pref_ad_expiration", "").commit();
                PremiumAccountFragment.this.adPref.setTitle((CharSequence) "Login to All-Debird");
                a2.dismiss();
            }
        });
        a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                a2.dismiss();
            }
        });
        a2.show();
    }

    /* access modifiers changed from: package-private */
    public void logoutPremiumize() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.a((CharSequence) "Do you want logout to Premiumize?");
        final AlertDialog a2 = builder.a();
        a2.a(-1, "ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                PremiumizeCredentialsHelper.a();
                PremiumAccountFragment.this.pmPref.setTitle((CharSequence) "Login to Premiumize");
                PremiumAccountFragment.this.pmPref.setSummary((CharSequence) "Premiumize");
                PremiumAccountFragment.this.getSharedPreference().edit().putString("pref_premiumize_expiration", "").commit();
                a2.dismiss();
            }
        });
        a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                a2.dismiss();
            }
        });
        a2.show();
    }

    /* access modifiers changed from: package-private */
    public void logoutRealDebird() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.a((CharSequence) "Do you want logout to Real-Debrid?");
        final AlertDialog a2 = builder.a();
        a2.a(-1, "ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                RealDebridCredentialsHelper.a();
                CookieManager.getInstance().removeAllCookies(new ValueCallback<Boolean>(this) {
                    /* renamed from: a */
                    public void onReceiveValue(Boolean bool) {
                    }
                });
                HttpHelper.e().c("https://api.real-debrid.com", "__beaconTrackerID=; __gacid=;");
                PremiumAccountFragment.this.getSharedPreference().edit().putString("pref_rd_expiration", "").commit();
                PremiumAccountFragment.this.rdPref.setTitle((CharSequence) "Login to Real-Debird");
                PremiumAccountFragment.this.rdPref.setSummary((CharSequence) "");
                a2.dismiss();
            }
        });
        a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                a2.dismiss();
            }
        });
        a2.show();
    }

    /* access modifiers changed from: package-private */
    public void logoutTrakt() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.a((CharSequence) "Do you want logout to Trakt-TV?");
        final AlertDialog a2 = builder.a();
        a2.a(-1, "ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                TraktCredentialsHelper.a();
                CookieManager.getInstance().removeAllCookie();
                HttpHelper.e().c("https://api.trakt.tv", "__beaconTrackerID=; __gacid=;");
                PremiumAccountFragment.this.traktPref.setSummary((CharSequence) "Trakt-Tv (Unauthorize)");
                PremiumAccountFragment.this.traktPref.setTitle((CharSequence) "Login to Trakt TV");
                a2.dismiss();
            }
        });
        a2.a(-2, "cancel", new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                a2.dismiss();
            }
        });
        a2.show();
    }

    public void onActivityResult(int i, int i2, Intent intent) {
        super.onActivityResult(i, i2, intent);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        DaggerBaseFragmentComponent.a().a(FreeMoviesApp.a((Context) getActivity()).d()).a().a(this);
    }

    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_premium_account, str);
        this.compositeDisposable = new CompositeDisposable();
        this.loginDisposable = new CompositeDisposable();
        this.compositeDisposable.b(this.loginDisposable);
        this.rdPref = findPreference("pref_auth_real_debrid");
        this.adPref = findPreference("pref_auth_All_debrid");
        this.pmPref = findPreference("pref_auth_premiumize_debrid");
        this.traktPref = findPreference("pref_auth_trakt_tv");
        if (RealDebridCredentialsHelper.c().isValid()) {
            getRealDebirdUserInfo();
        }
        if (AllDebridCredentialsHelper.b().isValid()) {
            getAlDebirdUserInfo();
        }
        if (TraktCredentialsHelper.b().isValid()) {
            getTraktUserInfo();
        }
        if (PremiumizeCredentialsHelper.b().isValid()) {
            getPremiumizeDebirdUserInfo(PremiumizeCredentialsHelper.b().getAccessToken());
        }
        this.compositeDisposable.b(RxBus.b().a().subscribe(new a0(this), o.f5419a));
        this.rdPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (RealDebridCredentialsHelper.c().isValid()) {
                    PremiumAccountFragment.this.logoutRealDebird();
                    return true;
                }
                PremiumAccountFragment.this.loginRealDebird();
                return true;
            }
        });
        this.adPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (AllDebridCredentialsHelper.b().isValid()) {
                    PremiumAccountFragment.this.logoutAllDebird();
                    return true;
                }
                PremiumAccountFragment.this.loginALlDebird();
                return true;
            }
        });
        this.pmPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (PremiumizeCredentialsHelper.b().isValid()) {
                    PremiumAccountFragment.this.logoutPremiumize();
                    return true;
                }
                PremiumAccountFragment.this.loginPremiumize();
                return true;
            }
        });
        this.traktPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                if (TraktCredentialsHelper.b().isValid()) {
                    PremiumAccountFragment.this.logoutTrakt();
                    return true;
                }
                PremiumAccountFragment.this.loginTrakt();
                return true;
            }
        });
    }

    public void onDestroy() {
        this.compositeDisposable.dispose();
        super.onDestroy();
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        return false;
    }

    public /* synthetic */ void b(Boolean bool) throws Exception {
        boolean isValid = TraktCredentialsHelper.b().isValid();
        if (isValid) {
            this.traktPref.setSummary((CharSequence) !isValid ? "Trakt-Tv" : "Trakt-Tv authorized");
            getSharedPreference().edit().putBoolean("pre_show_my_calenda_shows_only", true).apply();
            RxBus.b().a(bool.booleanValue() ? new TraktGetTokenSuccessEvent() : new TraktGetTokenFailedEvent());
            TraktUserApi.f().a(FreeMoviesApp.a((Context) getActivity()).e(), (Activity) getActivity(), this.mvDatabase);
            getTraktUserInfo();
        }
    }

    public /* synthetic */ void a(RealDebridGetTokenResult realDebridGetTokenResult) throws Exception {
        hideWaitingDialog();
        RealDebridCredentialsHelper.a(realDebridGetTokenResult.getAccess_token(), realDebridGetTokenResult.getRefresh_token(), realDebridGetTokenResult.getLast_clientID(), realDebridGetTokenResult.getLast_clientSecret());
        getRealDebirdUserInfo();
        RxBus.b().a(realDebridGetTokenResult != null ? new ApiDebridGetTokenSuccessEvent() : new ApiDebridGetTokenFailedEvent());
    }

    public /* synthetic */ void f(Throwable th) throws Exception {
        hideWaitingDialog();
        RxBus.b().a(new ApiDebridGetTokenFailedEvent());
    }

    public /* synthetic */ void a(Void voidR) throws Exception {
        this.adPref.setSummary((CharSequence) getSharedPreference().getString("pref_ad_expiration", ""));
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        getSharedPreference().edit().putString("pref_ad_expiration", "").commit();
    }

    public /* synthetic */ void a(Boolean bool) throws Exception {
        boolean isValid = AllDebridCredentialsHelper.b().isValid();
        if (isValid) {
            this.adPref.setSummary((CharSequence) !isValid ? "All-Debird" : "All-Debrid authorized");
            getAlDebirdUserInfo();
        }
        RxBus.b().a(isValid ? new ApiDebridGetTokenSuccessEvent() : new ApiDebridGetTokenFailedEvent());
    }

    public /* synthetic */ void a(String str, PremiumizeUserInfo premiumizeUserInfo) throws Exception {
        if (premiumizeUserInfo.getStatus().isEmpty() || !premiumizeUserInfo.getStatus().contains("success")) {
            getSharedPreference().edit().putString("pref_pm_info", "").commit();
            this.pmPref.setSummary((CharSequence) "");
            this.pmPref.setTitle((CharSequence) "Login to Premiumize");
            return;
        }
        PremiumizeCredentialsInfo premiumizeCredentialsInfo = new PremiumizeCredentialsInfo();
        premiumizeCredentialsInfo.setAccessToken(str);
        premiumizeCredentialsInfo.setPremium_until(premiumizeUserInfo.getLongPremium_until());
        String a2 = DateTimeHelper.a(premiumizeCredentialsInfo.getPremium_until());
        SharedPreferences.Editor edit = FreeMoviesApp.l().edit();
        edit.putString("pref_premiumize_expiration", "\nApikey : " + str + "\nType : Premium \nExpiration : " + a2).commit();
        PremiumizeCredentialsHelper.a(premiumizeCredentialsInfo);
        FreeMoviesApp.l().edit().putBoolean("pref_premiumize_type", true).commit();
        getSharedPreference().edit().putString("pref_pm_info", premiumizeUserInfo.toString()).commit();
        this.pmPref.setSummary((CharSequence) premiumizeUserInfo.toString());
        PremiumizeUserApi.c().a();
        this.pmPref.setTitle((CharSequence) "Logout");
    }

    public /* synthetic */ void a(User user) throws Exception {
        StringBuilder sb = new StringBuilder();
        sb.append("Username: ");
        sb.append(user.username);
        sb.append("\nName: ");
        sb.append(user.name);
        sb.append("\nJoined at : ");
        sb.append(user.joined_at.toLocalDate().toString());
        sb.append("\nType: ");
        sb.append(user.vip.booleanValue() ? "Vip" : "Free");
        sb.append("\nPrivate:  ");
        sb.append(user.isPrivate.booleanValue() ? "Yes" : "No");
        String sb2 = sb.toString();
        getSharedPreference().edit().putString("pref_trakt_info", sb2).commit();
        this.traktPref.setSummary((CharSequence) sb2);
        this.traktPref.setTitle((CharSequence) "Logout");
    }
}
