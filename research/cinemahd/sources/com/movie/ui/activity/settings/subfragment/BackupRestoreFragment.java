package com.movie.ui.activity.settings.subfragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import androidx.fragment.app.FragmentActivity;
import androidx.preference.Preference;
import com.database.MvDatabase;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.original.tase.Logger;
import com.utils.PrefUtils;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import java.io.File;

public class BackupRestoreFragment extends BaseSettingFragment {
    static /* synthetic */ void a(Void voidR) throws Exception {
    }

    static /* synthetic */ void b(Void voidR) throws Exception {
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        FragmentActivity activity = getActivity();
        Utils.a((Activity) activity, "Backup failed : " + th.getMessage());
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        FragmentActivity activity = getActivity();
        Utils.a((Activity) activity, "Restore failed : " + th.getMessage());
    }

    public void backup() {
        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Void>() {
            public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/cinemahd/" + "backup");
                if (!file.exists() && !file.mkdirs()) {
                    Logger.a("SettingActivity", "Problem creating Image folder");
                }
                MvDatabase.a((Context) BackupRestoreFragment.this.getActivity(), "backup");
                PrefUtils.c(BackupRestoreFragment.this.getActivity(), "backup");
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(d.f5408a, new c(this), new f(this)));
    }

    public /* synthetic */ void e() throws Exception {
        Utils.a((Activity) getActivity(), "Backup successful.");
    }

    public /* synthetic */ void f() throws Exception {
        Utils.a((Activity) getActivity(), "Restore successful");
    }

    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_backup_restore, str);
        Preference findPreference = findPreference("pref_restore");
        Preference findPreference2 = findPreference("pref_backup");
        findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                BackupRestoreFragment.this.restore();
                return true;
            }
        });
        findPreference2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                BackupRestoreFragment.this.backup();
                return true;
            }
        });
    }

    public void restore() {
        this.compositeDisposable.b(Observable.create(new ObservableOnSubscribe<Void>() {
            public void subscribe(ObservableEmitter<Void> observableEmitter) throws Exception {
                MvDatabase.b(BackupRestoreFragment.this.getActivity(), "backup");
                PrefUtils.a((Context) BackupRestoreFragment.this.getActivity(), "backup");
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(e.f5409a, new b(this), new a(this)));
    }
}
