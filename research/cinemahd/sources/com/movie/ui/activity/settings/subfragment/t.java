package com.movie.ui.activity.settings.subfragment;

import com.uwetrottmann.trakt5.entities.User;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class t implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PremiumAccountFragment f5424a;

    public /* synthetic */ t(PremiumAccountFragment premiumAccountFragment) {
        this.f5424a = premiumAccountFragment;
    }

    public final void accept(Object obj) {
        this.f5424a.a((User) obj);
    }
}
