package com.movie.ui.activity.settings.subfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import androidx.appcompat.app.AlertDialog;
import androidx.preference.MultiSelectListPreference;
import androidx.preference.Preference;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.original.tase.I18N;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class PerformanceFragment extends BaseSettingFragment implements Preference.OnPreferenceChangeListener {
    public static String hostStreamPriority = "";

    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_performance, str);
        setupProviders();
        setupPriorityHost();
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        String str;
        if (!preference.getKey().equals("pref_choose_provider_enabled2")) {
            return false;
        }
        getSharedPreference().edit().putBoolean("pref_zerotv_enabled", obj.toString().contains("ZeroTV")).apply();
        Set<String> r = Utils.r();
        HashSet hashSet = new HashSet();
        if (obj != null) {
            for (String next : r) {
                Iterator it2 = ((Set) obj).iterator();
                while (true) {
                    if (it2.hasNext()) {
                        if (((String) it2.next()).equals(next)) {
                            str = next;
                            break;
                        }
                    } else {
                        str = "";
                        break;
                    }
                }
                if (str.length() == 0) {
                    hashSet.add(next.toString());
                }
            }
        }
        getSharedPreference().edit().putStringSet("pref_choose_provider_disabled", hashSet).apply();
        return true;
    }

    /* access modifiers changed from: package-private */
    public void setupPriorityHost() {
        findPreference("pref_choose_host_priority3").setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                PerformanceFragment.this.showHostStreamPriority();
                return true;
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void setupProviders() {
        String str;
        MultiSelectListPreference multiSelectListPreference = (MultiSelectListPreference) findPreference("pref_choose_provider_enabled2");
        Set<String> r = Utils.r();
        Set<String> stringSet = getSharedPreference().getStringSet("pref_choose_provider_disabled", new HashSet());
        HashSet hashSet = new HashSet();
        if (stringSet != null) {
            for (CharSequence next : r) {
                Iterator<String> it2 = stringSet.iterator();
                while (true) {
                    if (!it2.hasNext()) {
                        str = "";
                        break;
                    }
                    str = it2.next();
                    if (next.equals(str)) {
                        break;
                    }
                }
                if (str.length() == 0) {
                    hashSet.add(next.toString());
                }
            }
        }
        multiSelectListPreference.setDefaultValue(r);
        multiSelectListPreference.setEntries((CharSequence[]) r.toArray(new CharSequence[r.size()]));
        multiSelectListPreference.setEntryValues((CharSequence[]) r.toArray(new CharSequence[r.size()]));
        if (hashSet.size() > 0) {
            multiSelectListPreference.setValues(hashSet);
        } else {
            multiSelectListPreference.setValues(r);
        }
        multiSelectListPreference.setOnPreferenceChangeListener(this);
    }

    public void showHostStreamPriority() {
        final Preference findPreference = findPreference("pref_choose_host_priority3");
        String string = getSharedPreference().getString("pref_choose_host_priority3", BaseResolver.c());
        final ArrayList arrayList = new ArrayList();
        if (string.contains(",")) {
            for (String add : string.split(",")) {
                arrayList.add(add);
            }
        }
        final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), 17367046, arrayList);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.b((CharSequence) I18N.a(R.string.host_stream_priority));
        builder.a((ListAdapter) arrayAdapter, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
            }
        });
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                StringBuilder sb = new StringBuilder();
                Iterator it2 = arrayList.iterator();
                while (it2.hasNext()) {
                    String str = (String) it2.next();
                    if (sb.indexOf(str + ",") == -1) {
                        sb.append(str);
                        sb.append(",");
                    }
                }
                String sb2 = sb.toString();
                String substring = sb2.length() + -1 > 0 ? sb2.substring(0, sb2.length() - 1) : "";
                PerformanceFragment.this.getSharedPreference().edit().putString("pref_choose_host_priority3", substring).apply();
                findPreference.setSummary((CharSequence) substring);
                PerformanceFragment.hostStreamPriority = substring;
                dialogInterface.dismiss();
            }
        });
        builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c().b().setOnItemClickListener(new AdapterView.OnItemClickListener(this) {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                if (i > 0) {
                    Collections.swap(arrayList, i, i - 1);
                    arrayAdapter.notifyDataSetChanged();
                }
            }
        });
    }
}
