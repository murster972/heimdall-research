package com.movie.ui.activity.settings.subfragment;

import com.original.tase.model.debrid.premiumize.PremiumizeUserInfo;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class s implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PremiumAccountFragment f5423a;
    private final /* synthetic */ String b;

    public /* synthetic */ s(PremiumAccountFragment premiumAccountFragment, String str) {
        this.f5423a = premiumAccountFragment;
        this.b = str;
    }

    public final void accept(Object obj) {
        this.f5423a.a(this.b, (PremiumizeUserInfo) obj);
    }
}
