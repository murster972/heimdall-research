package com.movie.ui.activity.settings.subfragment;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.realdebrid.RealDebridApi;
import dagger.MembersInjector;

public final class PremiumAccountFragment_MembersInjector implements MembersInjector<PremiumAccountFragment> {
    public static void a(PremiumAccountFragment premiumAccountFragment, MvDatabase mvDatabase) {
        premiumAccountFragment.mvDatabase = mvDatabase;
    }

    public static void a(PremiumAccountFragment premiumAccountFragment, RealDebridApi realDebridApi) {
        premiumAccountFragment.realDebridApi = realDebridApi;
    }

    public static void a(PremiumAccountFragment premiumAccountFragment, MoviesApi moviesApi) {
        premiumAccountFragment.moviesApi = moviesApi;
    }
}
