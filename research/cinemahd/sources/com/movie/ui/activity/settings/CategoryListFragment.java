package com.movie.ui.activity.settings;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import com.facebook.common.util.UriUtil;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.ui.fragment.BaseFragment;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.Map;

public class CategoryListFragment extends BaseFragment {
    ListView c;
    ListViewAdapter d;

    public static CategoryListFragment a(String str, String str2) {
        CategoryListFragment categoryListFragment = new CategoryListFragment();
        Bundle bundle = new Bundle();
        bundle.putString(UriUtil.DATA_SCHEME, str);
        bundle.putString("ref", str2);
        categoryListFragment.setArguments(bundle);
        return categoryListFragment;
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.list_category, viewGroup, false);
        this.c = (ListView) inflate.findViewById(R.id.listCategory);
        return inflate;
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        String string = getArguments().getString(UriUtil.DATA_SCHEME);
        this.d = new ListViewAdapter(getActivity(), R.layout.category_retrict_item);
        this.d.a(string.split(","), getArguments().getString("ref"));
        this.c.setAdapter(this.d);
        this.c.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                CategoryListFragment categoryListFragment = CategoryListFragment.this;
                categoryListFragment.d.a(i, (ViewGroup) categoryListFragment.c);
            }
        });
    }

    public static class ListViewAdapter extends ArrayAdapter<Map.Entry<Integer, String>> {

        /* renamed from: a  reason: collision with root package name */
        String f5355a;
        String[] b;
        Boolean[] c;
        int d;
        Context e;

        static class ViewHolder {

            /* renamed from: a  reason: collision with root package name */
            TextView f5357a;
            CheckBox b;

            ViewHolder() {
            }
        }

        public ListViewAdapter(Context context, int i) {
            super(context, i);
            this.e = context;
            this.d = i;
        }

        public void a(String[] strArr, String str) {
            boolean z;
            this.b = strArr;
            this.f5355a = str;
            this.c = new Boolean[this.b.length];
            String[] split = FreeMoviesApp.l().getString(str, "").split(",");
            for (int i = 0; i < strArr.length; i++) {
                int i2 = 0;
                while (true) {
                    if (i2 >= split.length) {
                        z = false;
                        break;
                    } else if (strArr[i].equalsIgnoreCase(split[i2])) {
                        z = true;
                        break;
                    } else {
                        i2++;
                    }
                }
                this.c[i] = Boolean.valueOf(z);
            }
        }

        public int getCount() {
            return this.b.length;
        }

        public View getView(final int i, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = ((Activity) this.e).getLayoutInflater().inflate(this.d, viewGroup, false);
            }
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.f5357a = (TextView) view.findViewById(R.id.txtCategory);
            viewHolder.b = (CheckBox) view.findViewById(R.id.cbRestrict);
            viewHolder.b.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                    ListViewAdapter.this.c[i] = Boolean.valueOf(z);
                    ListViewAdapter.this.a();
                }
            });
            viewHolder.f5357a.setText(this.b[i]);
            viewHolder.b.setChecked(this.c[i].booleanValue());
            return view;
        }

        public void a(int i, ViewGroup viewGroup) {
            Boolean[] boolArr = this.c;
            boolArr[i] = Boolean.valueOf(!boolArr[i].booleanValue());
            notifyDataSetChanged();
        }

        public void a() {
            ArrayList arrayList = new ArrayList();
            for (int i = 0; i < this.b.length; i++) {
                if (this.c[i].booleanValue()) {
                    arrayList.add(this.b[i]);
                }
            }
            FreeMoviesApp.l().edit().putString(this.f5355a, TextUtils.join(",", arrayList)).apply();
        }
    }
}
