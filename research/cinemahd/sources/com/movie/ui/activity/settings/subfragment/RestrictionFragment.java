package com.movie.ui.activity.settings.subfragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.Preference;
import com.movie.data.model.Categorys;
import com.movie.ui.activity.settings.BaseSettingFragment;
import com.movie.ui.activity.settings.CategoryRetrictionDialog;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;

public class RestrictionFragment extends BaseSettingFragment implements Preference.OnPreferenceChangeListener {
    public void onCreatePreferences(Bundle bundle, String str) {
        super.onCreatePreferences(bundle, str);
        setPreferencesFromResource(R.xml.preferences_restriction, str);
        Preference findPreference = findPreference("pref_restrict_password");
        findPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                RestrictionFragment.this.showSetPassword();
                return true;
            }
        });
        Preference findPreference2 = findPreference("pref_category_restriction");
        findPreference2.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                RestrictionFragment.this.showCategoryRetriction();
                return true;
            }
        });
        findPreference2.setOnPreferenceChangeListener(this);
        final String string = getSharedPreference().getString("pref_restrict_password", "");
        if (!string.isEmpty()) {
            setSumaryPassword(findPreference, string);
            final EditText editText = new EditText(getActivity());
            Utils.a((Activity) getActivity(), "Enter the password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            }, editText, (DialogInterface.OnDismissListener) new DialogInterface.OnDismissListener() {
                public void onDismiss(DialogInterface dialogInterface) {
                    if (string.equals(editText.getText().toString())) {
                        dialogInterface.dismiss();
                        return;
                    }
                    Utils.a((Activity) RestrictionFragment.this.getActivity(), "Password is wrong");
                    RestrictionFragment.this.getParentFragmentManager().y();
                }
            });
        }
    }

    public boolean onPreferenceChange(Preference preference, Object obj) {
        if (!preference.getKey().contains("pref_restrict_password")) {
            return false;
        }
        setSumaryPassword(preference, obj.toString());
        return true;
    }

    /* access modifiers changed from: package-private */
    public void setSumaryPassword(Preference preference, String str) {
        if (str.length() > 2) {
            preference.setSummary((CharSequence) str.substring(0, 2) + "*************");
        } else if (str.isEmpty()) {
            preference.setSummary((CharSequence) "password not set");
        } else {
            preference.setSummary((CharSequence) "********");
        }
    }

    public void showCategoryRetriction() {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        for (int i = 0; i < Categorys.getMVCategory().size(); i++) {
            arrayList.add(Categorys.getMVCategory().valueAt(i));
        }
        for (int i2 = 0; i2 < Categorys.getTVCategory().size(); i2++) {
            arrayList2.add(Categorys.getTVCategory().valueAt(i2));
        }
        FragmentManager supportFragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction b = supportFragmentManager.b();
        Fragment b2 = supportFragmentManager.b("CategoryRetrictionDialog");
        if (b2 != null) {
            b.a(b2);
        }
        b.a((String) null);
        CategoryRetrictionDialog.a(TextUtils.join(",", arrayList), TextUtils.join(",", arrayList2)).show(b, "CategoryRetrictionDialog");
    }

    public void showNewPasswordDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.b((CharSequence) "Enter new password");
        View inflate = getActivity().getLayoutInflater().inflate(R.layout.input_new_password_dialog, (ViewGroup) null);
        final EditText editText = (EditText) inflate.findViewById(R.id.EditText_Pwd1);
        final EditText editText2 = (EditText) inflate.findViewById(R.id.EditText_Pwd2);
        editText.setInputType(129);
        editText2.setInputType(129);
        builder.b(inflate);
        builder.b((CharSequence) "Ok", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (editText.getText().toString().equals(editText2.getText().toString())) {
                    RestrictionFragment.this.getSharedPreference().edit().putString("pref_restrict_password", editText.getText().toString()).commit();
                    Utils.a((Activity) RestrictionFragment.this.getActivity(), "Set password successfully");
                    return;
                }
                Utils.a((Activity) RestrictionFragment.this.getActivity(), "Passwords not match");
            }
        });
        builder.c();
    }

    public void showSetPassword() {
        final String string = getSharedPreference().getString("pref_restrict_password", "");
        if (string.isEmpty()) {
            showNewPasswordDialog();
            return;
        }
        final EditText editText = new EditText(getActivity());
        Utils.a((Activity) getActivity(), "Enter old password", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (string.equals(editText.getText().toString())) {
                    RestrictionFragment.this.showNewPasswordDialog();
                } else {
                    Utils.a((Activity) RestrictionFragment.this.getActivity(), "Password is wrong");
                }
            }
        }, editText, (DialogInterface.OnDismissListener) null);
    }
}
