package com.movie.ui.activity.settings.subfragment;

import com.movie.ui.activity.settings.subfragment.PremiumAccountFragment;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class l implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ PremiumAccountFragment.AnonymousClass15 f5416a;
    private final /* synthetic */ String b;

    public /* synthetic */ l(PremiumAccountFragment.AnonymousClass15 r1, String str) {
        this.f5416a = r1;
        this.b = str;
    }

    public final void accept(Object obj) {
        this.f5416a.a(this.b, (Boolean) obj);
    }
}
