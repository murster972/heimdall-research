package com.movie.ui.activity;

import android.view.View;
import android.widget.ListView;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public class TestCrappers_ViewBinding extends BaseActivity_ViewBinding {
    private TestCrappers b;

    public TestCrappers_ViewBinding(TestCrappers testCrappers, View view) {
        super(testCrappers, view);
        this.b = testCrappers;
        testCrappers.lvSources = (ListView) Utils.findRequiredViewAsType(view, R.id.lvSources, "field 'lvSources'", ListView.class);
    }

    public void unbind() {
        TestCrappers testCrappers = this.b;
        if (testCrappers != null) {
            this.b = null;
            testCrappers.lvSources = null;
            super.unbind();
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
