package com.movie.ui.activity;

import com.movie.data.model.CalendarItem;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarActivity f5212a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ c(CalendarActivity calendarActivity, CalendarItem calendarItem) {
        this.f5212a = calendarActivity;
        this.b = calendarItem;
    }

    public final void accept(Object obj) {
        this.f5212a.a(this.b, obj);
    }
}
