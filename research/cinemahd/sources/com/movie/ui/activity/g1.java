package com.movie.ui.activity;

import android.app.Dialog;
import com.movie.ui.activity.SettingsActivity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class g1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SettingsActivity.SettingsFragment f5230a;
    private final /* synthetic */ Dialog b;

    public /* synthetic */ g1(SettingsActivity.SettingsFragment settingsFragment, Dialog dialog) {
        this.f5230a = settingsFragment;
        this.b = dialog;
    }

    public final void accept(Object obj) {
        this.f5230a.a(this.b, (String) obj);
    }
}
