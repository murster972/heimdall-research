package com.movie.ui.activity;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class m2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5264a;
    private final /* synthetic */ MediaSource b;
    private final /* synthetic */ int c;

    public /* synthetic */ m2(SourceActivity sourceActivity, MediaSource mediaSource, int i) {
        this.f5264a = sourceActivity;
        this.b = mediaSource;
        this.c = i;
    }

    public final void accept(Object obj) {
        this.f5264a.a(this.b, this.c, (MediaSource) obj);
    }
}
