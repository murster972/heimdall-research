package com.movie.ui.activity;

import com.movie.data.model.CalendarItem;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarActivity f5216a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ d(CalendarActivity calendarActivity, CalendarItem calendarItem) {
        this.f5216a = calendarActivity;
        this.b = calendarItem;
    }

    public final void accept(Object obj) {
        this.f5216a.a(this.b, (Throwable) obj);
    }
}
