package com.movie.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;
import com.movie.AppComponent;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.event.ApiDebridGetTokenFailedEvent;
import com.original.tase.event.ApiDebridGetTokenSuccessEvent;
import com.original.tase.event.ApiDebridUserCancelledAuthEvent;
import com.original.tase.utils.NetworkUtils;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class AllDebridAuthWebViewActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    public boolean f5050a;
    private WebView b;
    private Disposable c;

    public class subsCustom implements Consumer<Object> {

        /* renamed from: a  reason: collision with root package name */
        final AllDebridAuthWebViewActivity f5051a;

        public subsCustom(AllDebridAuthWebViewActivity allDebridAuthWebViewActivity, AllDebridAuthWebViewActivity allDebridAuthWebViewActivity2) {
            this.f5051a = allDebridAuthWebViewActivity2;
        }

        public void accept(Object obj) throws Exception {
            boolean z = obj instanceof ApiDebridGetTokenSuccessEvent;
            if (z || (obj instanceof ApiDebridGetTokenFailedEvent)) {
                if (z) {
                    this.f5051a.f5050a = true;
                }
                this.f5051a.finish();
            }
        }
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    @SuppressLint({"WrongConstant"})
    public void a(boolean z) {
        try {
            int i = 8;
            findViewById(R.id.webView).setVisibility(z ? 8 : 0);
            findViewById(R.id.tvPleaseWait).setVisibility(z ? 0 : 8);
            View findViewById = findViewById(R.id.pbPleaseWait);
            if (z) {
                i = 0;
            }
            findViewById.setVisibility(i);
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
    }

    public void finish() {
        if (!this.f5050a) {
            RxBus.b().a(new ApiDebridUserCancelledAuthEvent());
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface", "RestrictedApi"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.needToCancelHttpHelper = false;
        setContentView(R.layout.activity_web_view);
        Bundle extras = getIntent().getExtras();
        if (extras.getString("verificationUrl", (String) null) == null || extras.getString("verificationUrl", (String) null).isEmpty() || extras.getString("pin", (String) null) == null || extras.getString("pin", (String) null).isEmpty() || !NetworkUtils.a()) {
            if (NetworkUtils.a()) {
                Utils.a((Activity) this, "Error");
            } else {
                Utils.a((Activity) this, "No internet");
            }
            setResult(0);
            finish();
            return;
        }
        String string = extras.getString("verificationUrl");
        String string2 = extras.getString("pin");
        setTitle("All-Debrid Auth");
        TextView textView = (TextView) findViewById(R.id.tvPleaseWait);
        textView.setTextSize(2, 24.0f);
        textView.setText(String.format("1) Visit \"%s\" in a browser of any of your devices\n2) Login to All-Debrid\n3) Input the following code: %s\n\nThis window will be closed automatically after you have granted the All-Debrid API access to CINEMA ", new Object[]{string, string2}));
        a(true);
        this.c = RxBus.b().a().subscribe(new subsCustom(this, this), a.f5199a);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WebView webView = this.b;
        if (webView != null) {
            if (webView.getParent() != null && (this.b.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.b.getParent()).removeView(this.b);
            }
            this.b.removeAllViews();
            this.b.destroy();
        }
        Disposable disposable = this.c;
        if (disposable != null && !disposable.isDisposed()) {
            this.c.dispose();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        setResult(0);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        WebView webView = this.b;
        if (webView != null) {
            webView.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        WebView webView = this.b;
        if (webView != null) {
            webView.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }
}
