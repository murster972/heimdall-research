package com.movie.ui.activity.movies.stream;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public class StreamFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private StreamFragment f5275a;

    public StreamFragment_ViewBinding(StreamFragment streamFragment, View view) {
        this.f5275a = streamFragment;
        streamFragment.lvSources = (ListView) Utils.findRequiredViewAsType(view, R.id.lvSources, "field 'lvSources'", ListView.class);
        streamFragment.progressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.progressBar, "field 'progressBar'", ProgressBar.class);
    }

    public void unbind() {
        StreamFragment streamFragment = this.f5275a;
        if (streamFragment != null) {
            this.f5275a = null;
            streamFragment.lvSources = null;
            streamFragment.progressBar = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
