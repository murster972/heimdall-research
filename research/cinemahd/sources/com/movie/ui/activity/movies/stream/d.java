package com.movie.ui.activity.movies.stream;

import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.functions.Predicate;

/* compiled from: lambda */
public final /* synthetic */ class d implements Predicate {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ d f5279a = new d();

    private /* synthetic */ d() {
    }

    public final boolean a(Object obj) {
        return Utils.b(((MediaSource) obj).getStreamLink());
    }
}
