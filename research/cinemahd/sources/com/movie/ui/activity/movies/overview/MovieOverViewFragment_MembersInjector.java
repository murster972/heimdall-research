package com.movie.ui.activity.movies.overview;

import com.movie.data.api.tmdb.TMDBApi;
import dagger.MembersInjector;

public final class MovieOverViewFragment_MembersInjector implements MembersInjector<MovieOverViewFragment> {
    public static void a(MovieOverViewFragment movieOverViewFragment, TMDBApi tMDBApi) {
        movieOverViewFragment.c = tMDBApi;
    }
}
