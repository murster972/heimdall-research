package com.movie.ui.activity.movies.stream;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class k implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ StreamFragment f5286a;

    public /* synthetic */ k(StreamFragment streamFragment) {
        this.f5286a = streamFragment;
    }

    public final void accept(Object obj) {
        this.f5286a.b((MediaSource) obj);
    }
}
