package com.movie.ui.activity.movies;

import android.view.View;
import android.widget.ImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;
import butterknife.internal.Utils;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.movie.ui.activity.BaseActivity_ViewBinding;
import com.yoku.marumovie.R;

public class MovieActivity_ViewBinding extends BaseActivity_ViewBinding {
    private MovieActivity b;

    public MovieActivity_ViewBinding(MovieActivity movieActivity, View view) {
        super(movieActivity, view);
        this.b = movieActivity;
        movieActivity.toolbar = (Toolbar) Utils.findRequiredViewAsType(view, R.id.toolbar, "field 'toolbar'", Toolbar.class);
        movieActivity.tabLayout = (TabLayout) Utils.findRequiredViewAsType(view, R.id.tablayout, "field 'tabLayout'", TabLayout.class);
        movieActivity.collapsingToolbarLayout = (CollapsingToolbarLayout) Utils.findRequiredViewAsType(view, R.id.collapsing_toolbar, "field 'collapsingToolbarLayout'", CollapsingToolbarLayout.class);
        movieActivity.imageView = (ImageView) Utils.findRequiredViewAsType(view, R.id.toolbar_image, "field 'imageView'", ImageView.class);
        movieActivity.viewPager = (ViewPager) Utils.findRequiredViewAsType(view, R.id.viewpage, "field 'viewPager'", ViewPager.class);
        movieActivity.imgBackground = (ImageView) Utils.findRequiredViewAsType(view, R.id.imgBackground, "field 'imgBackground'", ImageView.class);
    }

    public void unbind() {
        MovieActivity movieActivity = this.b;
        if (movieActivity != null) {
            this.b = null;
            movieActivity.toolbar = null;
            movieActivity.tabLayout = null;
            movieActivity.collapsingToolbarLayout = null;
            movieActivity.imageView = null;
            movieActivity.viewPager = null;
            movieActivity.imgBackground = null;
            super.unbind();
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
