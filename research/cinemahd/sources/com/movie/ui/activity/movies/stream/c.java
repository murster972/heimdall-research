package com.movie.ui.activity.movies.stream;

import com.original.tase.model.media.MediaSource;
import com.utils.Getlink.Resolver.BaseResolver;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/* compiled from: lambda */
public final /* synthetic */ class c implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ c f5278a = new c();

    private /* synthetic */ c() {
    }

    public final Object apply(Object obj) {
        return BaseResolver.b((MediaSource) obj).subscribeOn(Schedulers.b());
    }
}
