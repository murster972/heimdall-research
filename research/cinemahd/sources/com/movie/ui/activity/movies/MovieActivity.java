package com.movie.ui.activity.movies;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.palette.graphics.Palette;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.database.entitys.MovieEntity;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.MovieInfo;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.activity.DaggerBaseActivityComponent;
import com.movie.ui.activity.MovieDetailsActivity;
import com.movie.ui.activity.movies.overview.MovieOverViewFragment;
import com.movie.ui.activity.movies.stream.StreamFragment;
import com.movie.ui.activity.shows.ShowActivity;
import com.movie.ui.fragment.BrowseMoviesFragment;
import com.movie.ui.fragment.MoviesFragment;
import com.original.tase.model.media.MediaSource;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;
import java.util.List;
import javax.inject.Inject;

public class MovieActivity extends BaseActivity implements MoviesFragment.Listener, StreamFragment.StreamFragmentListener {
    @Inject

    /* renamed from: a  reason: collision with root package name */
    TMDBApi f5265a;
    MovieEntity b;
    @BindView(2131296507)
    CollapsingToolbarLayout collapsingToolbarLayout;
    @BindView(2131297160)
    ImageView imageView;
    @BindView(2131296696)
    ImageView imgBackground;
    @BindView(2131297096)
    TabLayout tabLayout;
    @BindView(2131297158)
    Toolbar toolbar;
    @BindView(2131297230)
    ViewPager viewPager;

    public class ViewPagerAdapter extends FragmentStatePagerAdapter {
        public ViewPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment a(int i) {
            if (i == 0) {
                return MovieOverViewFragment.a(MovieActivity.this.b);
            }
            if (i != 1) {
                return BrowseMoviesFragment.a(Integer.valueOf((int) MovieActivity.this.b.getTmdbID()), BrowseMoviesFragment.Type.TV_RECOMENDATION);
            }
            String str = MovieActivity.this.b.getRealeaseDate().isEmpty() ? "" : MovieActivity.this.b.getRealeaseDate().split("-")[0];
            MovieEntity movieEntity = MovieActivity.this.b;
            StreamFragment a2 = StreamFragment.a(movieEntity, new MovieInfo(movieEntity.getName(), str, "", "", ""));
            a2.a((StreamFragment.StreamFragmentListener) MovieActivity.this);
            return a2;
        }

        public int getCount() {
            return 3;
        }
    }

    /* access modifiers changed from: private */
    public void a(Palette.Swatch swatch) {
    }

    public void b(List<MediaSource> list) {
        a(this.tabLayout.b(1), list.size());
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_movie);
        setSupportActionBar(this.toolbar);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.e(true);
        }
        this.b = (MovieEntity) getIntent().getParcelableExtra("com.freeapp.freemovies.extras.EXTRA_ENTITY");
        new CompositeDisposable();
        a(this.b);
        this.toolbar.setTitle((CharSequence) this.b.getName());
        this.collapsingToolbarLayout.setTitle(this.b.getName());
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    public void a(TabLayout.Tab tab, int i) {
        TextView textView = (TextView) tab.a().findViewById(R.id.tvTabSubText);
        ((TextView) tab.a().findViewById(R.id.tvTabTitle)).setText("STREAMS");
        if (i > 0) {
            textView.setVisibility(0);
            textView.setText(i + " found");
            return;
        }
        textView.setVisibility(8);
    }

    /* access modifiers changed from: package-private */
    public void a(MovieEntity movieEntity) {
        TabLayout tabLayout2 = this.tabLayout;
        tabLayout2.a(tabLayout2.b().b((CharSequence) "Overview"));
        TabLayout.Tab b2 = this.tabLayout.b().b((CharSequence) "Streams");
        b2.a((int) R.layout.custom_stream_tab);
        a(b2, 0);
        this.tabLayout.a(b2);
        TabLayout tabLayout3 = this.tabLayout;
        tabLayout3.a(tabLayout3.b().b((CharSequence) "Recommendations"));
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                MovieActivity.this.tabLayout.b(i).g();
            }
        });
        this.tabLayout.a((TabLayout.BaseOnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
            public void a(TabLayout.Tab tab) {
                MovieActivity.this.viewPager.setCurrentItem(tab.c());
            }

            public void b(TabLayout.Tab tab) {
            }

            public void c(TabLayout.Tab tab) {
            }
        });
        if (movieEntity.getBackdrop_path() != null) {
            this.imageView.setVisibility(0);
            RequestBuilder<Drawable> a2 = Glide.a((FragmentActivity) this).a(movieEntity.getBackdrop_path()).a((BaseRequestOptions<?>) new RequestOptions().b((int) R.color.movie_cover_placeholder)).a(new DrawableTransitionOptions().b());
            if (!FreeMoviesApp.o()) {
                a2.b((RequestListener<Drawable>) GlidePalette.a(movieEntity.getBackdrop_path()).a((BitmapPalette.CallBack) new BitmapPalette.CallBack() {
                    public void a(Palette palette) {
                        MovieActivity.this.a(palette.b());
                    }
                }));
            }
            a2.a(this.imageView);
        } else {
            this.imageView.setVisibility(8);
        }
        this.viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        if (movieEntity.getPoster_path() != null) {
            Glide.a((FragmentActivity) this).a(movieEntity.getPoster_path()).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b()).a(this.imgBackground);
        }
    }

    public void a(MovieEntity movieEntity, View view) {
        if (movieEntity.getTV().booleanValue()) {
            Intent intent = new Intent(this, ShowActivity.class);
            intent.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent);
        } else {
            Intent intent2 = new Intent(this, MovieDetailsActivity.class);
            intent2.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent2);
        }
        finish();
    }
}
