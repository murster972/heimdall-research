package com.movie.ui.activity.movies.stream;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import butterknife.BindView;
import com.database.entitys.MovieEntity;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.model.MovieInfo;
import com.movie.ui.adapter.MediaSourceArrayAdapter;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.model.media.MediaSource;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class StreamFragment extends BaseFragment implements MediaSourceArrayAdapter.Listener {
    public ArrayList<MediaSource> c;
    public MediaSourceArrayAdapter d;
    CompositeDisposable e;
    StreamFragmentListener f;
    private MovieEntity g = null;
    private MovieInfo h = null;
    @BindView(2131296772)
    ListView lvSources;
    @BindView(2131296953)
    ProgressBar progressBar;

    public interface StreamFragmentListener {
        void b(List<MediaSource> list);
    }

    public static StreamFragment a(MovieEntity movieEntity, MovieInfo movieInfo) {
        StreamFragment streamFragment = new StreamFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("MovieEntity", movieEntity);
        bundle.putParcelable("MovieInfo", movieInfo);
        streamFragment.setArguments(bundle);
        return streamFragment;
    }

    static /* synthetic */ boolean b(BaseProvider baseProvider) throws Exception {
        return baseProvider != null;
    }

    static /* synthetic */ void f() throws Exception {
    }

    static /* synthetic */ boolean h(MediaSource mediaSource) throws Exception {
        boolean z = mediaSource.isTorrent() || mediaSource.getFileSize() > 0 || mediaSource.isHLS();
        if (!Utils.b) {
            return z;
        }
        if (!mediaSource.isHD() || !z) {
            return false;
        }
        return true;
    }

    static /* synthetic */ boolean i(MediaSource mediaSource) throws Exception {
        if (Utils.c) {
            return !mediaSource.getQuality().toLowerCase().contains("cam");
        }
        return true;
    }

    static /* synthetic */ boolean j(MediaSource mediaSource) throws Exception {
        return BasePlayerHelper.e() != null || !mediaSource.getStreamLink().contains("video-downloads");
    }

    static /* synthetic */ boolean k(MediaSource mediaSource) throws Exception {
        if (mediaSource.getQuality().contains("4K") && mediaSource.getFileSize() < 2097152000) {
            return false;
        }
        if (!mediaSource.getQuality().contains("1080") || mediaSource.getFileSize() >= 1097152000) {
            return true;
        }
        return false;
    }

    public void a(MediaSource mediaSource) {
    }

    /* renamed from: c */
    public void b(MediaSource mediaSource) {
        boolean z;
        Iterator<MediaSource> it2 = this.c.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (it2.next().getStreamLink().equals(mediaSource.getStreamLink())) {
                    z = true;
                    break;
                }
            } else {
                z = false;
                break;
            }
        }
        if (!z) {
            this.c.add(mediaSource);
            this.d.a();
            StreamFragmentListener streamFragmentListener = this.f;
            if (streamFragmentListener != null) {
                streamFragmentListener.b(this.c);
            }
        }
    }

    public void e() {
        Utils.a((Activity) getActivity(), "loading streams...");
        this.h.tmdbID = this.g.getTmdbID();
        this.progressBar.setVisibility(0);
        Utils.b = FreeMoviesApp.l().getBoolean("pref_show_hd_only", false);
        Utils.c = FreeMoviesApp.l().getBoolean("pref_filter_cam", false);
        Utils.d = FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false);
        Utils.d();
        this.e.b(Observable.fromIterable(Utils.h()).filter(e.f5280a).flatMap(new i(this)).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new k(this), new j(this), f.f5281a));
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_stream, viewGroup, false);
    }

    public void onDestroy() {
        this.e.a();
        super.onDestroy();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.e = new CompositeDisposable();
        this.g = (MovieEntity) getArguments().getParcelable("MovieEntity");
        this.h = (MovieInfo) getArguments().getParcelable("MovieInfo");
        this.c = new ArrayList<>();
        this.d = new MediaSourceArrayAdapter(getActivity(), R.layout.item_source, this.c);
        this.d.a((MediaSourceArrayAdapter.Listener) this);
        this.lvSources.setAdapter(this.d);
        this.lvSources.setNestedScrollingEnabled(true);
        e();
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ ObservableSource a(BaseProvider baseProvider) throws Exception {
        return baseProvider.a(this.h).subscribeOn(Schedulers.b()).filter(d.f5279a).flatMap(c.f5278a).flatMap(h.f5283a).map(m.f5288a).filter(b.f5277a).filter(l.f5287a).filter(g.f5282a).filter(a.f5276a);
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public void a(StreamFragmentListener streamFragmentListener) {
        this.f = streamFragmentListener;
    }
}
