package com.movie.ui.activity.movies.overview.cast;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.List;

public class CastAdapter extends RecyclerView.Adapter<ViewHolder> {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public List<MovieTMDB.ResultsBean.Credit.CastBean> f5272a = new ArrayList();
    /* access modifiers changed from: private */
    public Context b;

    public class ViewHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        ImageView f5274a;
        TextView b;

        public ViewHolder(CastAdapter castAdapter, View view) {
            super(view);
            this.f5274a = (ImageView) view.findViewById(R.id.image_view);
            this.b = (TextView) view.findViewById(R.id.name);
        }
    }

    public CastAdapter(Context context, List<MovieTMDB.ResultsBean.Credit.CastBean> list) {
        this.f5272a = list;
        this.b = context;
    }

    public int getItemCount() {
        return this.f5272a.size();
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {
        Log.d("CastAdapter", "onBindViewHolder: called.");
        ((RequestBuilder) Glide.d(this.b).a(this.f5272a.get(i).getProfile_path()).a(new DrawableTransitionOptions().b()).a((Transformation<Bitmap>) new RoundedCorners(16))).a(viewHolder.f5274a);
        viewHolder.b.setText(this.f5272a.get(i).getName());
        viewHolder.f5274a.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Log.d("CastAdapter", "onClick: clicked on an image: " + ((MovieTMDB.ResultsBean.Credit.CastBean) CastAdapter.this.f5272a.get(i)).getName());
                Toast.makeText(CastAdapter.this.b, ((MovieTMDB.ResultsBean.Credit.CastBean) CastAdapter.this.f5272a.get(i)).getName(), 0).show();
            }
        });
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cast_item, viewGroup, false));
    }
}
