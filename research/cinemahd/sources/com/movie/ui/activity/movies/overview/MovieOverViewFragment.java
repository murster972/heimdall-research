package com.movie.ui.activity.movies.overview;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.database.entitys.MovieEntity;
import com.movie.AppComponent;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.ui.activity.movies.overview.cast.CastAdapter;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.widget.RatingView;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;

public class MovieOverViewFragment extends BaseFragment {
    @Inject
    TMDBApi c;
    CompositeDisposable d;
    MovieEntity e;
    private CastAdapter f;
    @BindView(2131296526)
    RatingView ratingView;
    @BindView(2131297003)
    RecyclerView rvCast;
    @BindView(2131297188)
    TextView tvOverview;

    public static MovieOverViewFragment a(MovieEntity movieEntity) {
        MovieOverViewFragment movieOverViewFragment = new MovieOverViewFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("movieEntity", movieEntity);
        movieOverViewFragment.setArguments(bundle);
        return movieOverViewFragment;
    }

    public void e() {
        this.d.b(this.c.getMovieDetails(this.e.getTmdbID(), (String) null).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new b(this), new a(this)));
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_movie_overview, viewGroup, false);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.e = (MovieEntity) getArguments().getParcelable("movieEntity");
        this.d = new CompositeDisposable();
        e();
    }

    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ void a(MovieTMDB.ResultsBean resultsBean) throws Exception {
        this.tvOverview.setText(resultsBean.getOverview());
        this.ratingView.a(getActivity().getDrawable(R.drawable.ic_tmdb_icon), String.valueOf(resultsBean.getVote_average()), String.valueOf(resultsBean.getVote_count()), "10");
        a(resultsBean.getCredits().getCast());
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public void a(List<MovieTMDB.ResultsBean.Credit.CastBean> list) {
        this.rvCast.setLayoutManager(new LinearLayoutManager(getActivity(), 0, false));
        this.f = new CastAdapter(getActivity(), list);
        this.rvCast.setAdapter(this.f);
    }
}
