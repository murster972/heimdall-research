package com.movie.ui.activity.movies.overview;

import com.movie.data.model.tmvdb.MovieTMDB;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieOverViewFragment f5271a;

    public /* synthetic */ b(MovieOverViewFragment movieOverViewFragment) {
        this.f5271a = movieOverViewFragment;
    }

    public final void accept(Object obj) {
        this.f5271a.a((MovieTMDB.ResultsBean) obj);
    }
}
