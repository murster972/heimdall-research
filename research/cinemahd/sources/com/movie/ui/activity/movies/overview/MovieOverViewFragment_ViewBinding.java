package com.movie.ui.activity.movies.overview;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.movie.ui.widget.RatingView;
import com.yoku.marumovie.R;

public class MovieOverViewFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private MovieOverViewFragment f5269a;

    public MovieOverViewFragment_ViewBinding(MovieOverViewFragment movieOverViewFragment, View view) {
        this.f5269a = movieOverViewFragment;
        movieOverViewFragment.ratingView = (RatingView) Utils.findRequiredViewAsType(view, R.id.ctRatingView, "field 'ratingView'", RatingView.class);
        movieOverViewFragment.tvOverview = (TextView) Utils.findRequiredViewAsType(view, R.id.tvOverview, "field 'tvOverview'", TextView.class);
        movieOverViewFragment.rvCast = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.rv_cast, "field 'rvCast'", RecyclerView.class);
    }

    public void unbind() {
        MovieOverViewFragment movieOverViewFragment = this.f5269a;
        if (movieOverViewFragment != null) {
            this.f5269a = null;
            movieOverViewFragment.ratingView = null;
            movieOverViewFragment.tvOverview = null;
            movieOverViewFragment.rvCast = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
