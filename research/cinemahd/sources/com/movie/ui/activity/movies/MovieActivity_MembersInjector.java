package com.movie.ui.activity.movies;

import com.movie.data.api.tmdb.TMDBApi;
import dagger.MembersInjector;

public final class MovieActivity_MembersInjector implements MembersInjector<MovieActivity> {
    public static void a(MovieActivity movieActivity, TMDBApi tMDBApi) {
        movieActivity.f5265a = tMDBApi;
    }
}
