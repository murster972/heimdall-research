package com.movie.ui.activity.movies.stream;

import com.original.tase.model.media.MediaSource;
import com.original.tase.utils.SourceObservableUtils;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class m implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ m f5288a = new m();

    private /* synthetic */ m() {
    }

    public final Object apply(Object obj) {
        MediaSource mediaSource = (MediaSource) obj;
        MediaSource unused = SourceObservableUtils.a(mediaSource);
        return mediaSource;
    }
}
