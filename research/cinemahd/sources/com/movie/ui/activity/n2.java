package com.movie.ui.activity;

import com.movie.data.model.MovieInfo;
import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;
import java.util.ArrayList;

/* compiled from: lambda */
public final /* synthetic */ class n2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5292a;
    private final /* synthetic */ MediaSource b;
    private final /* synthetic */ MovieInfo c;

    public /* synthetic */ n2(SourceActivity sourceActivity, MediaSource mediaSource, MovieInfo movieInfo) {
        this.f5292a = sourceActivity;
        this.b = mediaSource;
        this.c = movieInfo;
    }

    public final void accept(Object obj) {
        this.f5292a.a(this.b, this.c, (ArrayList) obj);
    }
}
