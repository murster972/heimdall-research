package com.movie.ui.activity;

import com.movie.data.model.cinema.KeyResponse;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class v implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MemberActivationActivity f5498a;
    private final /* synthetic */ String b;

    public /* synthetic */ v(MemberActivationActivity memberActivationActivity, String str) {
        this.f5498a = memberActivationActivity;
        this.b = str;
    }

    public final void accept(Object obj) {
        this.f5498a.a(this.b, (KeyResponse) obj);
    }
}
