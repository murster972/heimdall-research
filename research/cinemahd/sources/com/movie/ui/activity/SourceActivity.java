package com.movie.ui.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import com.ads.videoreward.AdsManager;
import com.database.entitys.MovieEntity;
import com.database.entitys.TvWatchedEpisode;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.CastStateListener;
import com.google.android.gms.cast.framework.IntroductoryOverlay;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.common.images.WebImage;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.MoviesApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.cinema.Video;
import com.movie.ui.adapter.MediaSourceArrayAdapter;
import com.movie.ui.customdialog.AddMagnetDialog;
import com.movie.ui.helper.MoviesHelper;
import com.movie.ui.widget.AnimatorStateView;
import com.original.Constants;
import com.original.tase.Logger;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.model.socket.ClientObject;
import com.original.tase.socket.Client;
import com.original.tase.utils.DeviceUtils;
import com.original.tase.utils.Regex;
import com.original.tase.utils.SourceUtils;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.Getlink.Provider.ZeroTV;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.ImageUtils;
import com.utils.IntentDataContainer;
import com.utils.PermissionHelper;
import com.utils.Subtitle.ExpandableListSubtitleAdapter;
import com.utils.Subtitle.SubtitleInfo;
import com.utils.Subtitle.SubtitlesConverter;
import com.utils.Subtitle.converter.FormatTTML;
import com.utils.Subtitle.services.SubServiceBase;
import com.utils.Utils;
import com.utils.cast.CastHelper;
import com.utils.cast.CastSubtitlesWebServer;
import com.utils.cast.LocalWebserver;
import com.utils.cast.WebServerManager;
import com.utils.download.DownloadDialog;
import com.yoku.marumovie.R;
import fi.iki.elonen.NanoHTTPD;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.OkHttpClient;
import timber.log.Timber;

public class SourceActivity extends BaseActivity implements MediaSourceArrayAdapter.Listener, BasePlayerHelper.OnChoosePlayListener {
    /* access modifiers changed from: private */
    public AlertDialog A;
    /* access modifiers changed from: private */
    public MediaSource B = null;
    int C = 0;
    @Inject
    @Named("RealDebrid")
    OkHttpClient D;

    /* renamed from: a  reason: collision with root package name */
    public ArrayList<MediaSource> f5177a;
    @BindView(2131296339)
    FrameLayout adViewFrameLayout;
    public MediaSourceArrayAdapter b;
    /* access modifiers changed from: private */
    public MovieEntity c = null;
    /* access modifiers changed from: private */
    public MovieInfo d = null;
    private String e = "";
    /* access modifiers changed from: private */
    public CompositeDisposable f;
    private SessionManagerListener g = null;
    /* access modifiers changed from: private */
    public CastSession h;
    private CastContext i;
    /* access modifiers changed from: private */
    public IntroductoryOverlay j;
    /* access modifiers changed from: private */
    public MenuItem k;
    private CastStateListener l;
    @BindView(2131296772)
    ListView lvSources;
    private CompositeDisposable m;
    @BindView(2131297224)
    AnimatorStateView mViewAnimator;
    private CompositeDisposable n;
    boolean o = false;
    @Inject
    MoviesApi p;
    @BindView(2131296943)
    ProgressBar progressbar;
    @Inject
    MoviesHelper q;
    boolean r = false;
    /* access modifiers changed from: private */
    public boolean s = false;
    /* access modifiers changed from: private */
    public int t = 10;
    private boolean u = false;
    private String v = "eng";
    private Toolbar w;
    private ExpandableListView x;
    /* access modifiers changed from: private */
    public ExpandableListSubtitleAdapter y;
    Map<String, List<SubtitleInfo>> z = null;

    public static final class DrmInfo {
        public void a(Intent intent) {
            throw null;
        }
    }

    private static abstract class Sample {

        /* renamed from: a  reason: collision with root package name */
        public final boolean f5189a;
        public final String b;
        public final DrmInfo c;

        public Sample(String str, boolean z, String str2, DrmInfo drmInfo) {
            this.f5189a = z;
            this.b = str2;
            this.c = drmInfo;
        }

        public Intent a(Context context) {
            Intent intent = new Intent(context, PlayerActivity.class);
            intent.putExtra("prefer_extension_decoders", this.f5189a);
            intent.putExtra("abr_algorithm", this.b);
            DrmInfo drmInfo = this.c;
            if (drmInfo == null) {
                return intent;
            }
            drmInfo.a(intent);
            throw null;
        }
    }

    public static final class UriSample extends Sample {
        public final Uri d;
        public final String e;
        public final String f;

        public UriSample(String str, boolean z, String str2, DrmInfo drmInfo, Uri uri, String str3, String str4) {
            super(str, z, str2, drmInfo);
            this.d = uri;
            this.e = str3;
            this.f = str4;
        }

        public Intent a(Context context) {
            return super.a(context).setData(this.d).putExtra("extension", this.e).putExtra("ad_tag_uri", this.f).setAction("com.google.android.exoplayer.demo.action.VIEW");
        }
    }

    static /* synthetic */ void h(Throwable th) throws Exception {
    }

    static /* synthetic */ boolean k(MediaSource mediaSource) throws Exception {
        if (Utils.c) {
            return !mediaSource.getQuality().toLowerCase().contains("cam");
        }
        return true;
    }

    static /* synthetic */ boolean l(MediaSource mediaSource) throws Exception {
        return BasePlayerHelper.e() != null || !mediaSource.getStreamLink().contains("video-downloads");
    }

    private void setupToolbar() {
        this.w = (Toolbar) findViewById(R.id.toolbar);
        Toolbar toolbar = this.w;
        if (toolbar == null) {
            Timber.b("Didn't find a toolbar", new Object[0]);
            return;
        }
        ViewCompat.b((View) toolbar, getResources().getDimension(R.dimen.toolbar_elevation));
        setSupportActionBar(this.w);
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            supportActionBar.d(true);
            supportActionBar.g(true);
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        CastContext castContext = this.i;
        if (castContext != null) {
            return castContext.a(keyEvent) || super.dispatchKeyEvent(keyEvent);
        }
        return super.dispatchKeyEvent(keyEvent);
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00f9, code lost:
        if (r6 != 44454) goto L_0x00fe;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onActivityResult(int r6, int r7, android.content.Intent r8) {
        /*
            r5 = this;
            super.onActivityResult(r6, r7, r8)
            android.content.SharedPreferences r0 = com.movie.FreeMoviesApp.l()
            r1 = 0
            java.lang.String r2 = "pref_auto_next_eps"
            boolean r0 = r0.getBoolean(r2, r1)
            r2 = 90
            if (r6 != r2) goto L_0x0025
            if (r8 == 0) goto L_0x0025
            java.lang.String r3 = "end_by"
            java.lang.String r3 = r8.getStringExtra(r3)
            if (r3 == 0) goto L_0x0025
            java.lang.String r4 = "user"
            boolean r3 = r3.contains(r4)
            if (r3 == 0) goto L_0x0025
            r7 = 0
        L_0x0025:
            r3 = 1
            if (r0 == 0) goto L_0x00e4
            r0 = -1
            if (r7 != r0) goto L_0x00e4
            com.movie.ui.adapter.MediaSourceArrayAdapter r6 = r5.b
            r6.clear()
            com.movie.data.model.MovieInfo r6 = r5.d
            java.lang.String r6 = r6.eps
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            int r6 = r6.intValue()
            com.movie.data.model.MovieInfo r7 = r5.d
            int r8 = r7.epsCount
            if (r6 >= r8) goto L_0x00dd
            java.lang.String r6 = r7.eps
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            int r6 = r6.intValue()
            int r6 = r6 + r3
            java.lang.String r6 = java.lang.String.valueOf(r6)
            r7.eps = r6
            io.reactivex.disposables.CompositeDisposable r6 = new io.reactivex.disposables.CompositeDisposable
            r6.<init>()
            r5.m = r6
            io.reactivex.disposables.CompositeDisposable r6 = r5.f
            io.reactivex.disposables.CompositeDisposable r7 = r5.m
            r6.b(r7)
            com.movie.data.model.MovieInfo r6 = r5.d
            com.database.entitys.MovieEntity r7 = r5.c
            long r7 = r7.getTmdbID()
            r6.tmdbID = r7
            com.movie.data.model.MovieInfo r6 = r5.d
            r5.b((com.movie.data.model.MovieInfo) r6)
            androidx.appcompat.widget.Toolbar r6 = r5.w
            if (r6 == 0) goto L_0x0154
            com.database.entitys.MovieEntity r6 = r5.c
            java.lang.Boolean r6 = r6.getTV()
            boolean r6 = r6.booleanValue()
            if (r6 == 0) goto L_0x00ae
            androidx.appcompat.widget.Toolbar r6 = r5.w
            com.database.entitys.MovieEntity r7 = r5.c
            java.lang.String r7 = r7.getName()
            r6.setTitle((java.lang.CharSequence) r7)
            androidx.appcompat.widget.Toolbar r6 = r5.w
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            com.movie.data.model.MovieInfo r8 = r5.d
            java.lang.String r8 = r8.session
            r7.append(r8)
            java.lang.String r8 = "x"
            r7.append(r8)
            com.movie.data.model.MovieInfo r8 = r5.d
            java.lang.String r8 = r8.eps
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.setSubtitle((java.lang.CharSequence) r7)
            goto L_0x0154
        L_0x00ae:
            androidx.appcompat.widget.Toolbar r6 = r5.w
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            com.database.entitys.MovieEntity r8 = r5.c
            java.lang.String r8 = r8.getName()
            r7.append(r8)
            java.lang.String r8 = " "
            r7.append(r8)
            com.database.entitys.MovieEntity r8 = r5.c
            java.lang.String r8 = r8.getRealeaseDate()
            java.lang.String r0 = "-"
            java.lang.String[] r8 = r8.split(r0)
            r8 = r8[r1]
            r7.append(r8)
            java.lang.String r7 = r7.toString()
            r6.setTitle((java.lang.CharSequence) r7)
            goto L_0x0154
        L_0x00dd:
            r5.r = r3
            r5.finish()
            goto L_0x0154
        L_0x00e4:
            r7 = 5
            if (r6 == r7) goto L_0x014d
            if (r6 == r2) goto L_0x0127
            r7 = 431(0x1af, float:6.04E-43)
            if (r6 == r7) goto L_0x0101
            r7 = 32123(0x7d7b, float:4.5014E-41)
            if (r6 == r7) goto L_0x014d
            r7 = 37701(0x9345, float:5.283E-41)
            if (r6 == r7) goto L_0x00fc
            r7 = 44454(0xada6, float:6.2293E-41)
            if (r6 == r7) goto L_0x014d
            goto L_0x00fe
        L_0x00fc:
            r5.r = r3
        L_0x00fe:
            r5.r = r3
            goto L_0x0154
        L_0x0101:
            if (r8 == 0) goto L_0x0124
            android.os.Bundle r6 = r8.getExtras()
            if (r6 == 0) goto L_0x0124
            android.os.Bundle r6 = r8.getExtras()
            java.lang.String r7 = "extra_position"
            java.lang.Object r6 = r6.get(r7)
            if (r6 == 0) goto L_0x0124
            java.lang.String r6 = r6.toString()
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            long r6 = r6.longValue()
            r5.a((long) r6, (boolean) r1)
        L_0x0124:
            r5.r = r3
            goto L_0x0154
        L_0x0127:
            if (r8 == 0) goto L_0x014a
            android.os.Bundle r6 = r8.getExtras()
            if (r6 == 0) goto L_0x014a
            android.os.Bundle r6 = r8.getExtras()
            java.lang.String r7 = "position"
            java.lang.Object r6 = r6.get(r7)
            if (r6 == 0) goto L_0x014a
            java.lang.String r6 = r6.toString()
            java.lang.Long r6 = java.lang.Long.valueOf(r6)
            long r6 = r6.longValue()
            r5.a((long) r6, (boolean) r1)
        L_0x014a:
            r5.r = r3
            goto L_0x0154
        L_0x014d:
            r6 = 0
            r5.a((long) r6, (boolean) r3)
            r5.r = r3
        L_0x0154:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.activity.SourceActivity.onActivityResult(int, int, android.content.Intent):void");
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.activity_source);
        this.f = new CompositeDisposable();
        this.m = new CompositeDisposable();
        this.n = new CompositeDisposable();
        this.f.b(this.m);
        this.f.b(this.n);
        ArrayList<MediaSource> arrayList = this.f5177a;
        if (arrayList == null || arrayList.isEmpty()) {
            this.f5177a = new ArrayList<>();
        }
        this.lvSources.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                SourceActivity sourceActivity = SourceActivity.this;
                if (!sourceActivity.o) {
                    CompositeDisposable a2 = sourceActivity.f;
                    SourceActivity sourceActivity2 = SourceActivity.this;
                    FreeMoviesApp.a(a2, sourceActivity2.p, sourceActivity2.d, SourceActivity.this.f5177a);
                    SourceActivity.this.o = true;
                }
                SourceActivity sourceActivity3 = SourceActivity.this;
                BasePlayerHelper.a((Activity) sourceActivity3, sourceActivity3.f5177a.get(i), (BasePlayerHelper.OnChoosePlayListener) SourceActivity.this);
            }
        });
        this.lvSources.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public /* synthetic */ void a(int i, AlertDialog.Builder builder, MediaSource mediaSource) throws Exception {
                SourceActivity.this.f5177a.get(i).setResolved(mediaSource.isResolved());
                SourceActivity.this.f5177a.get(i).setStreamLink(mediaSource.getStreamLink());
                String filename = SourceActivity.this.f5177a.get(i).getFilename();
                if (filename == null) {
                    filename = URLUtil.guessFileName(SourceActivity.this.f5177a.get(i).getStreamLink(), (String) null, (String) null);
                }
                builder.a((CharSequence) filename);
                builder.b((CharSequence) "Ok", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.c();
                SourceActivity.this.hideWaitingDialog();
            }

            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                AlertDialog.Builder builder = new AlertDialog.Builder(SourceActivity.this);
                builder.b((CharSequence) "File Name :");
                MediaSource mediaSource = SourceActivity.this.f5177a.get(i);
                if (!mediaSource.isResolved()) {
                    SourceActivity.this.showWaitingDialog("resolving file name...");
                    SourceActivity.this.f.b(PremiumResolver.c(mediaSource).observeOn(AndroidSchedulers.a()).subscribe(new s1(this, i, builder), new r1(this)));
                    return true;
                }
                String filename = mediaSource.getFilename();
                if (filename == null) {
                    filename = URLUtil.guessFileName(mediaSource.getStreamLink(), (String) null, (String) null);
                }
                builder.a((CharSequence) filename);
                builder.b((CharSequence) "Ok", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.c();
                return true;
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                SourceActivity.this.hideWaitingDialog();
                Utils.c(SourceActivity.this, th.getMessage());
            }
        });
        this.b = new MediaSourceArrayAdapter(this, R.layout.item_source, this.f5177a);
        this.b.a((MediaSourceArrayAdapter.Listener) this);
        this.lvSources.setAdapter(this.b);
        Bundle extras = getIntent().getExtras();
        this.e = extras.getString("LINKID") == null ? "" : extras.getString("LINKID");
        if (extras.getBoolean("isFromAnotherApp")) {
            this.c = (MovieEntity) new Gson().a(extras.getString("Movie"), MovieEntity.class);
            this.d = (MovieInfo) new Gson().a(extras.getString("MovieInfo"), MovieInfo.class);
        } else {
            this.c = (MovieEntity) extras.getParcelable("Movie");
            this.d = (MovieInfo) extras.getParcelable("MovieInfo");
        }
        a(extras);
        b(this.d);
        if (!DeviceUtils.b()) {
            AdsManager.l().a((ViewGroup) this.adViewFrameLayout);
        }
        setupToolbar();
        if (this.w != null) {
            if (this.c.getTV().booleanValue()) {
                this.w.setTitle((CharSequence) this.c.getName());
                Toolbar toolbar = this.w;
                toolbar.setSubtitle((CharSequence) this.d.session + "x" + this.d.eps);
            } else {
                Toolbar toolbar2 = this.w;
                toolbar2.setTitle((CharSequence) this.c.getName() + " " + this.c.getRealeaseDate().split("-")[0]);
            }
            this.w.setNavigationOnClickListener(new r2(this));
        }
        if (Utils.z()) {
            try {
                f();
                this.i = CastContext.a((Context) this);
                this.h = this.i.c().a();
                this.l = new CastStateListener() {
                    public void a(int i) {
                        if (i != 1) {
                            SourceActivity.this.g();
                        }
                    }
                };
            } catch (Exception unused) {
                this.i = null;
            }
        }
        this.s = FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
        this.t = Integer.valueOf(FreeMoviesApp.l().getString("pref_auto_next_eps_number_of_link", "10")).intValue();
        this.u = FreeMoviesApp.l().getBoolean("pref_auto_next_with_fisrt_sub", false);
        this.v = FreeMoviesApp.l().getString("auto_play_next_with_last_sub_language", "eng");
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.activity_source, menu);
        this.k = CastButtonFactory.a(getApplicationContext(), menu, (int) R.id.media_route_menu_item);
        g();
        CheckBox checkBox = (CheckBox) menu.findItem(R.id.auto_play_next).getActionView();
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
                boolean unused = SourceActivity.this.s = z;
                FreeMoviesApp.l().edit().putBoolean("pref_auto_next_eps", z).apply();
                if (z) {
                    int unused2 = SourceActivity.this.t = Integer.valueOf(FreeMoviesApp.l().getString("pref_auto_next_eps_number_of_link", "10")).intValue();
                    Utils.a((Activity) SourceActivity.this, String.format(SourceActivity.this.getString(R.string.auto_play_next), new Object[]{Integer.valueOf(SourceActivity.this.t)}));
                }
            }
        });
        checkBox.setChecked(this.s);
        checkBox.setText("Auto play");
        return true;
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        this.f.a();
        this.f.dispose();
        this.o = false;
        HttpHelper.e().c();
        Utils.C();
        if (this.D != null && RealDebridCredentialsHelper.c().isValid()) {
            Utils.a(this.D);
        }
        OkHttpClient okHttpClient = ZeroTV.e;
        if (okHttpClient != null) {
            Utils.a(okHttpClient);
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId == R.id.auto_play_next) {
            FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false);
            return true;
        }
        if (itemId == R.id.add_rd_magnet) {
            this.m.dispose();
            HttpHelper.e().c();
            AddMagnetDialog a2 = AddMagnetDialog.a(this.c, this.d);
            FragmentTransaction b2 = getSupportFragmentManager().b();
            Fragment b3 = getSupportFragmentManager().b("fragment_add_magnet");
            if (b3 != null) {
                b2.a(b3);
            }
            b2.a((String) null);
            a2.show(b2, "fragment_add_magnet");
        }
        return super.onOptionsItemSelected(menuItem);
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        CastContext castContext;
        if (Utils.z() && (castContext = this.i) != null) {
            castContext.c().b(this.g, CastSession.class);
            this.i.b(this.l);
        }
        e();
        super.onPause();
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        CastContext castContext;
        if (this.r) {
            AdsManager.l().j();
            this.r = false;
        }
        if (Utils.z() && (castContext = this.i) != null) {
            castContext.c().a(this.g, CastSession.class);
            this.i.a(this.l);
            CastSession castSession = this.h;
            if (castSession == null || !castSession.b()) {
                Log.i("MOVIES_TAG", "CAST SESSION RESUME DIS_CONNECTED");
            } else {
                Log.i("MOVIES_TAG", "CAST SESSION RESUME CONNECTED");
            }
        }
        super.onResume();
    }

    /* access modifiers changed from: protected */
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        HttpHelper.e().a(HttpHelper.d);
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
        DaggerBaseActivityComponent.a().a(appComponent).a().a(this);
    }

    static /* synthetic */ boolean j(MediaSource mediaSource) throws Exception {
        if (Utils.b) {
            if (!mediaSource.isHD()) {
                return false;
            }
            if (mediaSource.getFileSize() > 0 || mediaSource.isHLS()) {
                return true;
            }
            return false;
        } else if (mediaSource.getFileSize() > 0 || mediaSource.isHLS()) {
            return true;
        } else {
            return false;
        }
    }

    public void b(MovieInfo movieInfo) {
        movieInfo.tmdbID = this.c.getTmdbID();
        this.progressbar.setVisibility(0);
        this.mViewAnimator.setVisibility(8);
        this.C = 0;
        Utils.b = FreeMoviesApp.l().getBoolean("pref_show_hd_only", false);
        Utils.c = FreeMoviesApp.l().getBoolean("pref_filter_cam", false);
        Utils.d = FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false);
        Utils.d();
        List<BaseProvider> h2 = Utils.h();
        for (BaseProvider next : h2) {
            if (next != null) {
                this.m.b(next.a(movieInfo).subscribeOn(Schedulers.b()).flatMap(v1.f5500a).flatMap(w2.f5505a).map(new Function<MediaSource, MediaSource>(this) {
                    public MediaSource a(MediaSource mediaSource) throws Exception {
                        b(mediaSource);
                        return mediaSource;
                    }

                    public /* bridge */ /* synthetic */ Object apply(Object obj) throws Exception {
                        MediaSource mediaSource = (MediaSource) obj;
                        a(mediaSource);
                        return mediaSource;
                    }

                    public MediaSource b(MediaSource mediaSource) {
                        String str;
                        String str2;
                        try {
                            String lowerCase = mediaSource.getStreamLink().trim().toLowerCase();
                            HashMap<String, String> hashMap = new HashMap<>();
                            String str3 = "";
                            if (!Regex.a(lowerCase, "//[^/]*(ntcdn|micetop)\\.[^/]{2,8}/", 1).isEmpty()) {
                                HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                                if (playHeader != null) {
                                    hashMap = playHeader;
                                }
                                if (hashMap.containsKey("Referer")) {
                                    str2 = hashMap.get("Referer");
                                } else {
                                    str2 = hashMap.containsKey("referer") ? hashMap.get("referer") : str3;
                                }
                                if (str2 != null) {
                                    str3 = str2.toLowerCase();
                                }
                                if (lowerCase.contains("vidcdn_pro/") && !str3.contains("vidnode.net")) {
                                    hashMap.put("Referer", "https://vidnode.net/");
                                } else if (lowerCase.contains("s7_ntcdn_us/") && !str3.contains("m4ufree.info")) {
                                    hashMap.put("Referer", "http://m4ufree.info/");
                                }
                                if (!hashMap.containsKey("User-Agent") && !hashMap.containsKey("user-agent")) {
                                    hashMap.put("User-Agent", Constants.f5838a);
                                }
                                mediaSource.setPlayHeader(hashMap);
                            } else if (Regex.a(lowerCase, "//[^/]*(vidcdn)\\.pro/stream/", 1).isEmpty() || lowerCase.contains(".m3u8")) {
                                if (mediaSource.getPlayHeader() == null) {
                                    mediaSource.setPlayHeader(new HashMap());
                                }
                                if (!mediaSource.getPlayHeader().containsKey("User-Agent")) {
                                    mediaSource.getPlayHeader().put("User-Agent", Constants.f5838a);
                                }
                            } else {
                                HashMap<String, String> playHeader2 = mediaSource.getPlayHeader();
                                if (playHeader2.containsKey("Referer")) {
                                    str = playHeader2.get("Referer");
                                } else {
                                    str = playHeader2.containsKey("referer") ? playHeader2.get("referer") : str3;
                                }
                                if (str != null) {
                                    str3 = str.toLowerCase();
                                }
                                if (!str3.contains("vidnode.net")) {
                                    playHeader2.put("Referer", "https://vidnode.net/");
                                }
                                if (!playHeader2.containsKey("User-Agent") && !playHeader2.containsKey("user-agent")) {
                                    playHeader2.put("User-Agent", Constants.f5838a);
                                }
                                mediaSource.setPlayHeader(playHeader2);
                            }
                        } catch (Throwable th) {
                            Logger.a(th, new boolean[0]);
                        }
                        return mediaSource;
                    }
                }).filter(p2.f5300a).filter(z1.f5516a).filter(w1.f5504a).observeOn(AndroidSchedulers.a()).subscribe(new j2(this), new a2(this, h2), new h2(this, h2)));
            }
        }
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        Utils.c(this, th.getMessage());
    }

    public /* synthetic */ void d(MediaSource mediaSource) throws Exception {
        if (this.s && (this.f5177a.size() >= this.t || this.m.isDisposed())) {
            this.m.dispose();
            this.f.b(PremiumResolver.c(this.f5177a.get(0)).observeOn(AndroidSchedulers.a()).subscribe(new q1(this), new u2(this)));
        } else if (!Utils.d) {
            e(mediaSource);
        } else if (mediaSource.isDebrid()) {
            e(mediaSource);
        }
    }

    /* renamed from: f */
    public void e(MediaSource mediaSource) {
        boolean z2;
        Iterator<MediaSource> it2 = this.f5177a.iterator();
        while (true) {
            if (it2.hasNext()) {
                if (it2.next().getStreamLink().equals(mediaSource.getStreamLink())) {
                    z2 = true;
                    break;
                }
            } else {
                z2 = false;
                break;
            }
        }
        if (!z2) {
            this.f5177a.add(mediaSource);
            this.b.a();
        }
        Toolbar toolbar = this.w;
        toolbar.setSubtitle((CharSequence) this.d.session + "x" + this.d.eps + " (" + this.f5177a.size() + " streams found)");
        if (this.f5177a.size() >= Utils.f6550a) {
            this.m.dispose();
            this.progressbar.setVisibility(4);
        }
    }

    public void g(MediaSource mediaSource) {
        if (mediaSource.isHD()) {
            this.m.b(BaseResolver.b(mediaSource).subscribeOn(Schedulers.b()).flatMap(f2.f5227a).observeOn(AndroidSchedulers.a()).subscribe(new q2(this), p1.f5299a));
        }
    }

    public /* synthetic */ void c(List list) throws Exception {
        Logger.a("SOURCEACTIVITY", "numberCompleteComplete = " + this.C);
        Logger.a("SOURCEACTIVITY", "ALL = " + BaseProvider.b.length);
        this.C = this.C + 1;
        if (this.C == list.size()) {
            e();
            if (this.s && !this.f5177a.isEmpty()) {
                if (this.f5177a.size() >= this.t || this.m.isDisposed()) {
                    this.m.dispose();
                    this.f.b(PremiumResolver.c(this.f5177a.get(0)).observeOn(AndroidSchedulers.a()).subscribe(new x1(this), new y1(this)));
                }
            }
        }
    }

    public /* synthetic */ void e(Throwable th) throws Exception {
        hideWaitingDialog();
    }

    private void e() {
        this.m.dispose();
        ProgressBar progressBar = this.progressbar;
        if (progressBar != null) {
            progressBar.setVisibility(8);
        }
        if (this.f5177a.size() == 0) {
            this.mViewAnimator.setVisibility(0);
        }
    }

    public /* synthetic */ void a(View view) {
        finish();
    }

    public void a(Bundle bundle) {
        ArrayList<Video> parcelableArrayList = bundle.getParcelableArrayList("STREAM");
        if (parcelableArrayList != null) {
            for (Video video : parcelableArrayList) {
                String site = video.getSite();
                if (!site.isEmpty()) {
                    if (GoogleVideoHelper.k(site)) {
                        MediaSource mediaSource = new MediaSource("Cinema", "GoogleVideo Video", false);
                        mediaSource.setStreamLink(video.getSite());
                        mediaSource.setQuality(video.getSize() + "p");
                        g(mediaSource);
                    } else {
                        MediaSource mediaSource2 = new MediaSource("Cinema", "Unknow CDN", true);
                        mediaSource2.setQuality("");
                        mediaSource2.setStreamLink(video.getSite());
                        g(mediaSource2);
                    }
                }
            }
        }
    }

    public /* synthetic */ void g(Throwable th) throws Exception {
        hideWaitingDialog();
        Utils.a((Activity) this, (int) R.string.not_subtitle_found);
    }

    public /* synthetic */ void f(Throwable th) throws Exception {
        hideWaitingDialog();
        Utils.a((Activity) this, th.getMessage());
        Utils.c(this, th.getMessage());
    }

    /* access modifiers changed from: private */
    public void g() {
        IntroductoryOverlay introductoryOverlay = this.j;
        if (introductoryOverlay != null) {
            introductoryOverlay.remove();
        }
        MenuItem menuItem = this.k;
        if (menuItem != null && menuItem.isVisible()) {
            new Handler().post(new Runnable() {
                public void run() {
                    SourceActivity sourceActivity = SourceActivity.this;
                    IntroductoryOverlay.Builder builder = new IntroductoryOverlay.Builder(sourceActivity, sourceActivity.k);
                    builder.a(SourceActivity.this.getString(R.string.introducing_cast));
                    builder.a((int) R.color.theme_primary);
                    builder.c();
                    builder.a((IntroductoryOverlay.OnOverlayDismissedListener) new IntroductoryOverlay.OnOverlayDismissedListener() {
                        public void a() {
                            IntroductoryOverlay unused = SourceActivity.this.j = null;
                        }
                    });
                    IntroductoryOverlay unused = sourceActivity.j = builder.a();
                    SourceActivity.this.j.show();
                }
            });
        }
    }

    public /* synthetic */ void d(Throwable th) throws Exception {
        Utils.c(this, th.getMessage());
    }

    private void f() {
        this.g = new SessionManagerListener<CastSession>() {
            /* renamed from: a */
            public void onSessionEnding(CastSession castSession) {
            }

            /* renamed from: a */
            public void onSessionEnded(CastSession castSession, int i) {
                a();
            }

            /* renamed from: a */
            public void onSessionResuming(CastSession castSession, String str) {
            }

            /* renamed from: b */
            public void onSessionStarting(CastSession castSession) {
            }

            /* renamed from: b */
            public void onSessionResumeFailed(CastSession castSession, int i) {
                a();
            }

            /* renamed from: c */
            public void onSessionStartFailed(CastSession castSession, int i) {
                a();
            }

            /* renamed from: d */
            public void onSessionSuspended(CastSession castSession, int i) {
            }

            private void c(CastSession castSession) {
                CastSession unused = SourceActivity.this.h = castSession;
                SourceActivity.this.hideWaitingDialog();
                Utils.a((Activity) SourceActivity.this, (int) R.string.cast_connected);
            }

            /* renamed from: a */
            public void onSessionResumed(CastSession castSession, boolean z) {
                c(castSession);
            }

            /* renamed from: b */
            public void onSessionStarted(CastSession castSession, String str) {
                c(castSession);
            }

            private void a() {
                CastSession unused = SourceActivity.this.h = null;
                SourceActivity.this.invalidateOptionsMenu();
            }
        };
    }

    public /* synthetic */ void d() throws Exception {
        hideWaitingDialog();
    }

    public /* synthetic */ void c(MediaSource mediaSource) throws Exception {
        this.f5177a.get(0).setResolved(mediaSource.isResolved());
        this.f5177a.get(0).setStreamLink(mediaSource.getStreamLink());
        if (this.u) {
            if (!this.c.getRealeaseDate().isEmpty()) {
                String str = this.c.getRealeaseDate().split("-")[0];
            }
            this.d.tmdbID = this.c.getTmdbID();
            showWaitingDialog("Auto play will get the first subtiles in setting.");
            a(this.d, this.f5177a.get(0), this.v, this.c.getPosition());
            return;
        }
        BasePlayerHelper e2 = BasePlayerHelper.e();
        if (e2 != null) {
            e2.a(this, (Fragment) null, this.f5177a.get(0), this.c.getName() + " Season " + this.d.session + "x" + this.d.eps, this.c.getPosition());
            return;
        }
        a(this.f5177a.get(0), (List<String>) null, (List<String>) null);
    }

    public void a(MovieInfo movieInfo) {
        movieInfo.tmdbID = this.c.getTmdbID();
        movieInfo.imdbIDStr = this.c.getImdbIDStr();
        movieInfo.fileName = this.B.getFilename();
        this.f.b(SubServiceBase.b(movieInfo).observeOn(AndroidSchedulers.a()).subscribe(new l2(this, movieInfo), new g2(this)));
    }

    public /* synthetic */ void b(MediaSource mediaSource) throws Exception {
        this.f5177a.get(0).setResolved(mediaSource.isResolved());
        this.f5177a.get(0).setStreamLink(mediaSource.getStreamLink());
        if (this.u) {
            if (!this.c.getRealeaseDate().isEmpty()) {
                String str = this.c.getRealeaseDate().split("-")[0];
            }
            this.d.tmdbID = this.c.getTmdbID();
            showWaitingDialog("Auto play will get the first subtiles in setting.");
            a(this.d, this.f5177a.get(0), this.v, this.c.getPosition());
            return;
        }
        BasePlayerHelper e2 = BasePlayerHelper.e();
        if (e2 != null) {
            e2.a(this, (Fragment) null, this.f5177a.get(0), this.c.getName() + " Season " + this.d.session + "x" + this.d.eps, this.c.getPosition());
            return;
        }
        a(this.f5177a.get(0), (List<String>) null, (List<String>) null);
    }

    public /* synthetic */ void c(String str) throws Exception {
        Utils.a((Activity) this, str);
    }

    public boolean c() {
        return this.h != null;
    }

    public /* synthetic */ void a(final MovieInfo movieInfo, ArrayList arrayList) throws Exception {
        this.z = new HashMap();
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            SubtitleInfo subtitleInfo = (SubtitleInfo) it2.next();
            List list = this.z.get(subtitleInfo.c);
            if (list == null) {
                list = new ArrayList();
                this.z.put(subtitleInfo.c, list);
            }
            list.add(subtitleInfo);
        }
        if (this.A == null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View inflate = getLayoutInflater().inflate(R.layout.dialog_listsubtitle, (ViewGroup) null);
            builder.b(inflate);
            builder.b((int) R.string.close_msg, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                public void onClick(DialogInterface dialogInterface, int i) {
                }
            });
            this.A = builder.a();
            this.x = (ExpandableListView) inflate.findViewById(R.id.subtitle_expand_view);
            this.x.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                public /* synthetic */ void a(List list) throws Exception {
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it2 = list.iterator();
                    while (it2.hasNext()) {
                        File file = (File) it2.next();
                        arrayList.add(file.getAbsolutePath());
                        arrayList2.add(file.getName());
                    }
                    if (CastHelper.a(SourceActivity.this)) {
                        HashMap hashMap = new HashMap();
                        ArrayList arrayList3 = new ArrayList();
                        for (int i = 0; i < arrayList.size(); i++) {
                            String str = "sub-" + i + "-" + DateTimeHelper.b() + "-" + new Random().nextInt(99999) + ".ttml";
                            String a2 = SubtitlesConverter.a((String) arrayList.get(i), new FormatTTML());
                            if (a2 != null) {
                                hashMap.put(str, a2);
                                arrayList3.add(arrayList.get(i));
                            }
                        }
                        if (WebServerManager.d().b() == null) {
                            WebServerManager.d().a((NanoHTTPD) new CastSubtitlesWebServer(34507));
                        }
                        WebServerManager.d().a((Map<String, String>) hashMap);
                        Intent intent = new Intent(SourceActivity.this, LocalWebserver.class);
                        if (hashMap.size() > 0) {
                            intent.putExtra("isNeededToRefreshTracks", true);
                            intent.putExtra("videoAndSubTrackIdArray", 1);
                        }
                        SourceActivity.this.startService(intent);
                        SourceActivity sourceActivity = SourceActivity.this;
                        sourceActivity.a(sourceActivity.B, (List<String>) arrayList3, (List<String>) new LinkedList(hashMap.keySet()));
                    } else {
                        BasePlayerHelper e = BasePlayerHelper.e();
                        SourceActivity sourceActivity2 = SourceActivity.this;
                        e.a(sourceActivity2, (Fragment) null, sourceActivity2.B, SourceActivity.this.c.getName() + " Season " + SourceActivity.this.d.session + "x" + SourceActivity.this.d.eps, SourceActivity.this.c.getPosition(), arrayList, arrayList2);
                    }
                    SourceActivity.this.hideWaitingDialog();
                }

                public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
                    SubtitleInfo subtitleInfo = (SubtitleInfo) SourceActivity.this.y.getChild(i, i2);
                    SourceActivity.this.A.dismiss();
                    if (!PermissionHelper.b(SourceActivity.this, 777)) {
                        return false;
                    }
                    SourceActivity.this.showWaitingDialog("");
                    SubServiceBase.a(SourceActivity.this, subtitleInfo.b, movieInfo.getNameAndYear()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new t1(this), new u1(this));
                    return true;
                }

                public /* synthetic */ void a(Throwable th) throws Exception {
                    SourceActivity.this.hideWaitingDialog();
                }
            });
        }
        this.y = new ExpandableListSubtitleAdapter(this, this.z);
        this.x.setAdapter(this.y);
        if (this.z.keySet().size() == 1) {
            this.x.expandGroup(0);
        }
        this.A.show();
        hideWaitingDialog();
    }

    private void b(int i2, MediaSource mediaSource) {
        this.f.b(this.q.a(this.c.getTmdbID(), this.c.getImdbIDStr(), this.c.getTraktID(), this.c.getTvdbID(), this.d.getSession().intValue(), this.d.getEps().intValue()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new c2(this), new o2(this), new e2(this, mediaSource, i2)));
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        Utils.a((Activity) this, th.getMessage());
    }

    public void a(MovieInfo movieInfo, MediaSource mediaSource, String str, long j2) {
        movieInfo.tmdbID = this.c.getTmdbID();
        movieInfo.imdbIDStr = this.c.getImdbIDStr();
        movieInfo.fileName = mediaSource.getFilename();
        this.f.b(SubServiceBase.b(movieInfo).observeOn(AndroidSchedulers.a()).subscribe(new n2(this, mediaSource, movieInfo), new t2(this, mediaSource)));
    }

    public /* synthetic */ void a(MediaSource mediaSource, MovieInfo movieInfo, ArrayList arrayList) throws Exception {
        if (BasePlayerHelper.e() == null) {
            ArrayList arrayList2 = new ArrayList();
            arrayList2.add(((SubtitleInfo) arrayList.get(0)).b);
            a(mediaSource, (List<String>) arrayList2, (List<String>) null);
        } else {
            SubServiceBase.a(this, ((SubtitleInfo) arrayList.get(0)).b, movieInfo.getNameAndYear()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new i2(this, mediaSource), new k2(this));
        }
        hideWaitingDialog();
    }

    public /* synthetic */ void a(MediaSource mediaSource, List list) throws Exception {
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            File file = (File) it2.next();
            arrayList.add(file.getAbsolutePath());
            arrayList2.add(file.getName());
        }
        if (CastHelper.a(this)) {
            HashMap hashMap = new HashMap();
            ArrayList arrayList3 = new ArrayList();
            for (int i2 = 0; i2 < arrayList.size(); i2++) {
                String str = "sub-" + i2 + "-" + DateTimeHelper.b() + "-" + new Random().nextInt(99999) + ".ttml";
                String a2 = SubtitlesConverter.a((String) arrayList.get(i2), new FormatTTML());
                if (a2 != null) {
                    hashMap.put(str, a2);
                    arrayList3.add(arrayList.get(i2));
                }
            }
            if (WebServerManager.d().b() == null) {
                WebServerManager.d().a((NanoHTTPD) new CastSubtitlesWebServer(34507));
            }
            WebServerManager.d().a((Map<String, String>) hashMap);
            Intent intent = new Intent(this, LocalWebserver.class);
            if (hashMap.size() > 0) {
                intent.putExtra("isNeededToRefreshTracks", true);
                intent.putExtra("videoAndSubTrackIdArray", 1);
            }
            startService(intent);
            a(mediaSource, (List<String>) arrayList3, (List<String>) new LinkedList(hashMap.keySet()));
        } else {
            BasePlayerHelper.e().a(this, (Fragment) null, mediaSource, this.c.getName() + " Season " + this.d.session + "x" + this.d.eps, this.c.getPosition(), arrayList, arrayList2);
        }
        hideWaitingDialog();
    }

    public /* synthetic */ void a(MediaSource mediaSource, Throwable th) throws Exception {
        hideWaitingDialog();
        BasePlayerHelper e2 = BasePlayerHelper.e();
        if (e2 != null) {
            e2.a(this, (Fragment) null, mediaSource, this.c.getName() + " Season " + this.d.session + "x" + this.d.eps, -1);
        } else {
            a(mediaSource, (List<String>) null, (List<String>) null);
        }
        Utils.a((Activity) this, (int) R.string.not_subtitle_found);
    }

    public /* synthetic */ void a(List list, Throwable th) throws Exception {
        Logger.a(th, new boolean[0]);
        this.C++;
        if (this.C == list.size()) {
            e();
        }
    }

    public void a(MediaSource mediaSource) {
        mediaSource.setMovieName(this.d.name + "(" + this.d.getYear() + ")");
        this.d.tempStreamLink = mediaSource.getStreamLink();
        this.d.extension = mediaSource.getExtension();
        this.d.fileSizeString = mediaSource.getFileSizeString();
        this.d.tmdbID = this.c.getTmdbID();
        this.d.imdbIDStr = this.c.getImdbIDStr();
        try {
            DownloadDialog.a(mediaSource, this.d, this.c.getTmdbID()).show(getSupportFragmentManager(), "downloadDialog");
        } catch (Exception unused) {
            Utils.a((Activity) this, (int) R.string.could_not_setup_download_menu);
        }
    }

    public void a(int i2, MediaSource mediaSource) {
        if (mediaSource.isResolved()) {
            b(i2, mediaSource);
            return;
        }
        showWaitingDialog("");
        this.f.b(PremiumResolver.c(mediaSource).observeOn(AndroidSchedulers.a()).subscribe(new m2(this, mediaSource, i2), new v2(this), new d2(this)));
    }

    public /* synthetic */ void a(MediaSource mediaSource, int i2, MediaSource mediaSource2) throws Exception {
        hideWaitingDialog();
        mediaSource.setStreamLink(mediaSource2.getStreamLink());
        b(i2, mediaSource);
    }

    public /* synthetic */ void a(TvWatchedEpisode tvWatchedEpisode) throws Exception {
        this.c.setPosition(tvWatchedEpisode.d());
        this.c.setSubtitlepath(tvWatchedEpisode.f());
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        this.c.setPosition(0);
        this.c.setSubtitlepath("");
    }

    public /* synthetic */ void a(MediaSource mediaSource, int i2) throws Exception {
        MediaSource mediaSource2 = mediaSource;
        this.B = mediaSource2;
        BasePlayerHelper e2 = BasePlayerHelper.e();
        String str = "";
        switch (i2) {
            case 0:
                if (e2 != null) {
                    e2.a(this, (Fragment) null, mediaSource, this.c.getName() + " Season " + this.d.session + "x" + this.d.eps, this.c.getPosition());
                    return;
                }
                a(mediaSource2, (List<String>) null, (List<String>) null);
                return;
            case 1:
                if (e2 != null) {
                    if (!this.c.getRealeaseDate().isEmpty()) {
                        String str2 = this.c.getRealeaseDate().split("-")[0];
                    }
                    this.d.tmdbID = this.c.getTmdbID();
                    showWaitingDialog(str);
                    a(this.d);
                    return;
                }
                Utils.a((Activity) this, "Please choose external player in setting first.");
                return;
            case 2:
                Intent intent = new Intent("android.intent.action.VIEW");
                Uri parse = Uri.parse(mediaSource.getStreamLink());
                String str3 = "application/x-mpegURL";
                intent.setDataAndType(parse, mediaSource.isHLS() ? str3 : "video/*");
                HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                if (playHeader != null && playHeader.size() > 0) {
                    HashMap<String, String> a2 = SourceUtils.a(playHeader);
                    ArrayList arrayList = new ArrayList();
                    for (Map.Entry next : a2.entrySet()) {
                        arrayList.add(next.getKey());
                        arrayList.add(next.getValue());
                    }
                    intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
                }
                if (!mediaSource.isHLS()) {
                    str3 = Utils.g(mediaSource.getStreamLink());
                }
                if (this.c.getRealeaseDate() == null || this.c.getRealeaseDate().isEmpty()) {
                    str = "1970";
                } else if (!this.c.getRealeaseDate().isEmpty()) {
                    str = this.c.getRealeaseDate().split("-")[0];
                }
                intent.putExtra("title", (this.c.getName() + " " + this.d.session + "x" + this.d.eps + " (" + str + ")") + str3);
                if (Build.VERSION.SDK_INT >= 16) {
                    intent.setDataAndTypeAndNormalize(parse, "video/*");
                } else {
                    intent.setDataAndType(parse, "video/*");
                }
                startActivityForResult(Intent.createChooser(intent, "Open with..."), 44454);
                return;
            case 3:
                a(mediaSource);
                return;
            case 4:
                Utils.a((Activity) this, mediaSource.getStreamLink(), false);
                return;
            case 5:
                a(mediaSource2, (List<String>) null, (List<String>) null);
                return;
            case 6:
                if (!this.c.getRealeaseDate().isEmpty()) {
                    String str4 = this.c.getRealeaseDate().split("-")[0];
                }
                a(this.d);
                showWaitingDialog(str);
                return;
            case 7:
                Client.getIntance().senddata(new ClientObject(e2 == null ? "CINEMA" : e2.b(), mediaSource.getStreamLink(), mediaSource.isHLS(), this.c.getName(), -1.0d, mediaSource.getOriginalLink(), Constants.f5838a, !mediaSource.getPlayHeader().isEmpty()).toString(), this);
                return;
            default:
                return;
        }
    }

    public void a(long j2, boolean z2) {
        TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
        tvWatchedEpisode.a(this.d.getEps().intValue());
        tvWatchedEpisode.c(this.d.getSession().intValue());
        tvWatchedEpisode.c(this.c.getTmdbID());
        tvWatchedEpisode.a(this.c.getImdbIDStr());
        tvWatchedEpisode.e(this.c.getTvdbID());
        tvWatchedEpisode.d(this.c.getTraktID());
        tvWatchedEpisode.b(j2);
        this.f.b(this.q.a(this.c, tvWatchedEpisode, true, z2).observeOn(AndroidSchedulers.a()).subscribe(new s2(this), new b2(this)));
    }

    public void a(final MediaSource mediaSource, final List<String> list, final List<String> list2) {
        if (FreeMoviesApp.l().getBoolean("pref_auto_next_eps", false)) {
            a(mediaSource, true, list, list2);
        } else if (this.c.getPosition() <= 0 || BasePlayerHelper.e() != null) {
            a(mediaSource, false, list, list2);
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.a((CharSequence) "Do you wish to resume the last positison?");
            builder.a(true);
            builder.b((CharSequence) "Resume", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    SourceActivity.this.a(mediaSource, false, (List<String>) list, (List<String>) list2);
                }
            });
            builder.a((CharSequence) "Start over", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialogInterface, int i) {
                    SourceActivity.this.a(mediaSource, true, (List<String>) list, (List<String>) list2);
                }
            });
            builder.c();
        }
    }

    public void a(MediaSource mediaSource, boolean z2, List<String> list, List<String> list2) {
        hideWaitingDialog();
        int i2 = 0;
        while (true) {
            if (i2 >= this.f5177a.size()) {
                i2 = 0;
                break;
            } else if (mediaSource.getStreamLink().equals(this.f5177a.get(i2).getStreamLink())) {
                break;
            } else {
                i2++;
            }
        }
        if (z2) {
            this.c.setPosition(0);
        }
        if (!c()) {
            Intent a2 = new UriSample(this.c.getName(), false, (String) null, (DrmInfo) null, Uri.parse(this.f5177a.get(i2).getStreamLink()), "", "").a(this);
            a2.putExtra("Movie", this.c);
            a2.putExtra("LINKID", this.e);
            a2.putExtra("streamID", i2);
            a2.putExtra("MovieInfo", this.d);
            if (list != null && list.size() > 0) {
                a2.putExtra("SubtitleInfo", new SubtitleInfo("autoSub", list.get(0), "", 0));
            }
            IntentDataContainer.a().a("MediaSouce", this.f5177a);
            this.f5177a.get(i2).setPlayed(true);
            this.b.notifyDataSetChanged();
            startActivityForResult(a2, 37701);
        } else if (list == null) {
            a(a(this.c, i2), (int) this.c.getPosition(), true);
        } else {
            a(CastHelper.a(CastHelper.a(this.c, this.d, mediaSource), mediaSource, list, list2), (int) this.c.getPosition(), true);
        }
    }

    public MediaInfo a(MovieEntity movieEntity, int i2) {
        MediaMetadata mediaMetadata = new MediaMetadata(1);
        mediaMetadata.a("com.google.android.gms.cast.metadata.SUBTITLE", movieEntity.getName());
        mediaMetadata.a("com.google.android.gms.cast.metadata.TITLE", movieEntity.getName());
        mediaMetadata.a(new WebImage(Uri.parse(ImageUtils.a(movieEntity.getPoster_path(), 500))));
        mediaMetadata.a(new WebImage(Uri.parse(ImageUtils.a(movieEntity.getBackdrop_path(), 500))));
        MediaInfo.Builder builder = new MediaInfo.Builder(this.f5177a.get(i2).getStreamLink());
        builder.a(1);
        builder.a("videos/*");
        builder.a(mediaMetadata);
        builder.a(0);
        return builder.a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:3:0x0005, code lost:
        r0 = r0.h();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.google.android.gms.cast.MediaInfo r4, int r5, boolean r6) {
        /*
            r3 = this;
            com.google.android.gms.cast.framework.CastSession r0 = r3.h
            if (r0 != 0) goto L_0x0005
            return
        L_0x0005:
            com.google.android.gms.cast.framework.media.RemoteMediaClient r0 = r0.h()
            if (r0 != 0) goto L_0x000c
            return
        L_0x000c:
            com.movie.ui.activity.SourceActivity$11 r1 = new com.movie.ui.activity.SourceActivity$11
            r1.<init>(r0)
            r0.a((com.google.android.gms.cast.framework.media.RemoteMediaClient.Callback) r1)
            com.google.android.gms.cast.MediaLoadOptions$Builder r1 = new com.google.android.gms.cast.MediaLoadOptions$Builder
            r1.<init>()
            com.google.android.gms.cast.MediaLoadOptions$Builder r6 = r1.a((boolean) r6)
            long r1 = (long) r5
            com.google.android.gms.cast.MediaLoadOptions$Builder r5 = r6.a((long) r1)
            com.google.android.gms.cast.MediaLoadOptions r5 = r5.a()
            r0.a((com.google.android.gms.cast.MediaInfo) r4, (com.google.android.gms.cast.MediaLoadOptions) r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.activity.SourceActivity.a(com.google.android.gms.cast.MediaInfo, int, boolean):void");
    }
}
