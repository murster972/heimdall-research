package com.movie.ui.activity;

import androidx.appcompat.app.AlertDialog;
import com.movie.ui.activity.SourceActivity;
import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class s1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity.AnonymousClass2 f5352a;
    private final /* synthetic */ int b;
    private final /* synthetic */ AlertDialog.Builder c;

    public /* synthetic */ s1(SourceActivity.AnonymousClass2 r1, int i, AlertDialog.Builder builder) {
        this.f5352a = r1;
        this.b = i;
        this.c = builder;
    }

    public final void accept(Object obj) {
        this.f5352a.a(this.b, this.c, (MediaSource) obj);
    }
}
