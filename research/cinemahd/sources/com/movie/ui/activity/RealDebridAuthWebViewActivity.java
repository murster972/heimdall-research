package com.movie.ui.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebViewDatabase;
import android.widget.TextView;
import com.androidadvance.topsnackbar.TSnackbar;
import com.movie.AppComponent;
import com.original.Constants;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.RxBus;
import com.original.tase.event.ApiDebridGetTokenFailedEvent;
import com.original.tase.event.ApiDebridGetTokenSuccessEvent;
import com.original.tase.event.ApiDebridUserCancelledAuthEvent;
import com.original.tase.utils.DeviceUtils;
import com.original.tase.utils.NetworkUtils;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.yoku.marumovie.R;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import java.util.HashMap;

public class RealDebridAuthWebViewActivity extends BaseActivity {

    /* renamed from: a  reason: collision with root package name */
    private WebView f5112a;
    public TSnackbar b;
    private Disposable c;
    public boolean d;

    public class HtmlViewerJavaScriptInterface {

        /* renamed from: a  reason: collision with root package name */
        final RealDebridAuthWebViewActivity f5113a;

        public HtmlViewerJavaScriptInterface(RealDebridAuthWebViewActivity realDebridAuthWebViewActivity, RealDebridAuthWebViewActivity realDebridAuthWebViewActivity2) {
            this.f5113a = realDebridAuthWebViewActivity2;
        }

        @JavascriptInterface
        public void showHTML(String str) {
            if (str != null && !str.isEmpty()) {
                if (str.toLowerCase().contains("application allowed")) {
                    Utils.a((Activity) this.f5113a, I18N.a(R.string.plaese_wait, new Object[0]));
                    this.f5113a.runOnUiThread(new Runnable() {
                        public void run() {
                            TSnackbar tSnackbar = HtmlViewerJavaScriptInterface.this.f5113a.b;
                            if (tSnackbar != null) {
                                tSnackbar.a();
                            }
                            HtmlViewerJavaScriptInterface.this.f5113a.a(true);
                        }
                    });
                } else if (str.toLowerCase().contains("the code")) {
                    this.f5113a.runOnUiThread(new Runnable() {
                        public void run() {
                            TSnackbar tSnackbar = HtmlViewerJavaScriptInterface.this.f5113a.b;
                            if (tSnackbar != null) {
                                tSnackbar.c();
                            }
                            HtmlViewerJavaScriptInterface.this.f5113a.a(false);
                        }
                    });
                } else {
                    this.f5113a.runOnUiThread(new Runnable() {
                        public void run() {
                            TSnackbar tSnackbar = HtmlViewerJavaScriptInterface.this.f5113a.b;
                            if (tSnackbar != null) {
                                tSnackbar.c();
                            }
                            HtmlViewerJavaScriptInterface.this.f5113a.a(false);
                        }
                    });
                }
            }
        }
    }

    public class cusWebViewClient extends WebViewClient {

        /* renamed from: a  reason: collision with root package name */
        private String f5117a;

        public cusWebViewClient(String str) {
            this.f5117a = str;
        }

        public void onPageFinished(WebView webView, String str) {
            super.onPageFinished(webView, str);
            if (Build.VERSION.SDK_INT >= 16) {
                webView.loadUrl("javascript:HtmlViewer.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            } else {
                webView.loadUrl("javascript:window.HtmlViewer.showHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
            String str2 = "javascript:document.getElementById('usercode').value='" + this.f5117a + "';document.getElementsByClassName('btn.btn-primary').click()";
            if (Build.VERSION.SDK_INT >= 19) {
                webView.evaluateJavascript(str2, new ValueCallback<String>(this) {
                    /* renamed from: a */
                    public void onReceiveValue(String str) {
                    }
                });
            } else {
                webView.loadUrl(str2);
            }
        }

        public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
            if (sslErrorHandler != null) {
                sslErrorHandler.proceed();
            } else {
                super.onReceivedSslError(webView, (SslErrorHandler) null, sslError);
            }
        }

        public boolean shouldOverrideUrlLoading(WebView webView, String str) {
            RealDebridAuthWebViewActivity.this.a(true);
            return false;
        }
    }

    public class subsCustom implements Consumer<Object> {

        /* renamed from: a  reason: collision with root package name */
        final RealDebridAuthWebViewActivity f5118a;

        public subsCustom(RealDebridAuthWebViewActivity realDebridAuthWebViewActivity, RealDebridAuthWebViewActivity realDebridAuthWebViewActivity2) {
            this.f5118a = realDebridAuthWebViewActivity2;
        }

        public void accept(Object obj) throws Exception {
            boolean z = obj instanceof ApiDebridGetTokenSuccessEvent;
            if (z || (obj instanceof ApiDebridGetTokenFailedEvent)) {
                if (z) {
                    this.f5118a.d = true;
                }
                this.f5118a.finish();
            }
        }
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    @SuppressLint({"WrongConstant"})
    public void a(boolean z) {
        try {
            int i = 8;
            findViewById(R.id.webView).setVisibility(z ? 8 : 0);
            findViewById(R.id.tvPleaseWait).setVisibility(z ? 0 : 8);
            View findViewById = findViewById(R.id.pbPleaseWait);
            if (z) {
                i = 0;
            }
            findViewById.setVisibility(i);
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
    }

    public void finish() {
        if (!this.d) {
            RxBus.b().a(new ApiDebridUserCancelledAuthEvent());
        }
        super.finish();
    }

    /* access modifiers changed from: protected */
    @SuppressLint({"SetJavaScriptEnabled", "AddJavascriptInterface", "RestrictedApi"})
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.needToCancelHttpHelper = false;
        setContentView(R.layout.activity_web_view);
        Bundle extras = getIntent().getExtras();
        if (extras.getString("verificationUrl", (String) null) == null || extras.getString("verificationUrl", (String) null).isEmpty() || extras.getString("userCode", (String) null) == null || extras.getString("userCode", (String) null).isEmpty() || !NetworkUtils.a()) {
            if (NetworkUtils.a()) {
                Utils.a((Activity) this, "Error");
            }
            Utils.a((Activity) this, "No internet");
            setResult(0);
            finish();
            return;
        }
        String string = extras.getString("verificationUrl");
        String string2 = extras.getString("userCode");
        setTitle("Real-Debrid Auth");
        if (DeviceUtils.a(new boolean[0])) {
            TextView textView = (TextView) findViewById(R.id.tvPleaseWait);
            textView.setTextSize(2, 24.0f);
            textView.setText(String.format("1) Visit \"%s\" in a browser of any of your devices\n2) Login to Real-Debrid\n3) Input the following code: %s\n\nThis window will be closed automatically after you have granted the Real-Debrid API access to CINEMA ", new Object[]{string, string2}));
            a(true);
        } else {
            View findViewById = findViewById(R.id.webViewActivityRoot);
            this.b = TSnackbar.a(findViewById, Html.fromHtml("<font color=\"#ffffff\">Enter the code: " + string2 + "</font>"), -1);
            try {
                WebViewDatabase.getInstance(this).clearFormData();
            } catch (Throwable th) {
                Logger.a(th, new boolean[0]);
            }
            this.f5112a = (WebView) findViewById(R.id.webView);
            this.f5112a.getSettings().setJavaScriptEnabled(true);
            this.f5112a.getSettings().setAllowFileAccess(false);
            this.f5112a.getSettings().setSaveFormData(false);
            this.f5112a.getSettings().setSavePassword(false);
            this.f5112a.getSettings().setJavaScriptCanOpenWindowsAutomatically(false);
            this.f5112a.getSettings().setCacheMode(2);
            this.f5112a.getSettings().setAppCacheEnabled(false);
            this.f5112a.getSettings().setUserAgentString(Constants.f5838a);
            try {
                this.f5112a.clearCache(true);
                this.f5112a.clearFormData();
            } catch (Throwable th2) {
                Logger.a(th2, new boolean[0]);
            }
            CookieManager.getInstance().setAcceptCookie(true);
            this.f5112a.setWebViewClient(new cusWebViewClient(string2));
            this.f5112a.addJavascriptInterface(new HtmlViewerJavaScriptInterface(this, this), "HtmlViewer");
            HashMap hashMap = new HashMap();
            hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US,en;q=0.5");
            this.f5112a.loadUrl(string, hashMap);
        }
        this.c = RxBus.b().a().subscribe(new subsCustom(this, this), s0.f5351a);
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        WebView webView = this.f5112a;
        if (webView != null) {
            if (webView.getParent() != null && (this.f5112a.getParent() instanceof ViewGroup)) {
                ((ViewGroup) this.f5112a.getParent()).removeView(this.f5112a);
            }
            this.f5112a.removeAllViews();
            this.f5112a.destroy();
        }
        Disposable disposable = this.c;
        if (disposable != null && !disposable.isDisposed()) {
            this.c.dispose();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() != 16908332) {
            return super.onOptionsItemSelected(menuItem);
        }
        setResult(0);
        finish();
        return true;
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        WebView webView = this.f5112a;
        if (webView != null) {
            webView.onPause();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        WebView webView = this.f5112a;
        if (webView != null) {
            webView.onResume();
        }
    }

    /* access modifiers changed from: protected */
    public void setupComponent(AppComponent appComponent) {
    }
}
