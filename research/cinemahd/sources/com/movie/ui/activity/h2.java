package com.movie.ui.activity;

import io.reactivex.functions.Action;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class h2 implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ SourceActivity f5244a;
    private final /* synthetic */ List b;

    public /* synthetic */ h2(SourceActivity sourceActivity, List list) {
        this.f5244a = sourceActivity;
        this.b = list;
    }

    public final void run() {
        this.f5244a.c(this.b);
    }
}
