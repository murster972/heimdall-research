package com.movie.ui.adapter;

import com.movie.data.model.tmvdb.TvTMDB;
import com.movie.ui.adapter.CalendarAdapter;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class l implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5551a;
    private final /* synthetic */ Observer b;

    public /* synthetic */ l(CalendarAdapter.MovieHolder movieHolder, Observer observer) {
        this.f5551a = movieHolder;
        this.b = observer;
    }

    public final void accept(Object obj) {
        this.f5551a.a(this.b, (TvTMDB.ResultsBean) obj);
    }
}
