package com.movie.ui.adapter;

import com.movie.ui.adapter.CalendarAdapter;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;

/* compiled from: lambda */
public final /* synthetic */ class b implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5541a;
    private final /* synthetic */ String b;

    public /* synthetic */ b(CalendarAdapter.MovieHolder movieHolder, String str) {
        this.f5541a = movieHolder;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return this.f5541a.a(this.b, (ResponseBody) obj);
    }
}
