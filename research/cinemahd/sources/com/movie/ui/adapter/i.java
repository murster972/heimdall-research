package com.movie.ui.adapter;

import com.movie.ui.adapter.CalendarAdapter;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class i implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5548a;

    public /* synthetic */ i(Observer observer) {
        this.f5548a = observer;
    }

    public final void accept(Object obj) {
        CalendarAdapter.MovieHolder.a(this.f5548a, (CalendarAdapter.MovieHolder.HolderImage) obj);
    }
}
