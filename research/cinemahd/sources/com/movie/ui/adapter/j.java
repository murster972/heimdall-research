package com.movie.ui.adapter;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class j implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5549a;

    public /* synthetic */ j(Observer observer) {
        this.f5549a = observer;
    }

    public final void accept(Object obj) {
        this.f5549a.onComplete();
    }
}
