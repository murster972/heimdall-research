package com.movie.ui.adapter;

import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter;
import com.movie.data.model.TorrentObject;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.List;

public class MagnetInfoAdapter extends RecyclerView.Adapter<MagnetHolder> {

    /* renamed from: a  reason: collision with root package name */
    List<TorrentObject> f5526a;
    MagnetInfoListener b;

    public static class MagnetHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f5529a;
        ImageView b;
        TextView c;
        TextView d;
        TextView e;
        ProgressBar f;

        public MagnetHolder(View view) {
            super(view);
            this.f5529a = (TextView) view.findViewById(R.id.tvID);
            this.b = (ImageView) view.findViewById(R.id.imgdelete);
            this.c = (TextView) view.findViewById(R.id.tvFileSize);
            this.e = (TextView) view.findViewById(R.id.tvStatus);
            this.d = (TextView) view.findViewById(R.id.tvNumberFile);
            this.f = (ProgressBar) view.findViewById(R.id.progressBar);
        }
    }

    public interface MagnetInfoListener {
        void a(TorrentObject torrentObject);

        void a(TorrentObject torrentObject, int i);

        void d(TorrentObject torrentObject);
    }

    public MagnetInfoAdapter(List<TorrentObject> list) {
        this.f5526a = list;
    }

    public void a(MagnetInfoListener magnetInfoListener) {
        this.b = magnetInfoListener;
    }

    public List<TorrentObject> b() {
        return this.f5526a;
    }

    public void c(TorrentObject torrentObject) {
        for (int i = 0; i < this.f5526a.size(); i++) {
            if (this.f5526a.get(i).getId().equals(torrentObject.getId())) {
                this.f5526a.set(i, torrentObject);
                notifyItemChanged(i);
                return;
            }
        }
    }

    public int getItemCount() {
        return this.f5526a.size();
    }

    /* renamed from: a */
    public void onBindViewHolder(MagnetHolder magnetHolder, int i) {
        final TorrentObject torrentObject = this.f5526a.get(i);
        TextView textView = magnetHolder.f5529a;
        textView.setText("[" + TorrentTypeConverter.a(torrentObject.getType()) + "] " + torrentObject.getName());
        magnetHolder.c.setText(Formatter.formatFileSize(Utils.i(), torrentObject.getSize()));
        magnetHolder.e.setText(torrentObject.getStatusBean().getStatus());
        int i2 = 8;
        boolean z = true;
        if (torrentObject.getFiles().size() > 1) {
            TextView textView2 = magnetHolder.d;
            textView2.setText(torrentObject.getFiles().size() + " files");
        } else {
            magnetHolder.d.setVisibility(8);
        }
        ProgressBar progressBar = magnetHolder.f;
        if (torrentObject.getStatusBean().getProgress() != 0 && torrentObject.isGotDetails()) {
            z = false;
        }
        progressBar.setIndeterminate(z);
        magnetHolder.f.setProgress(torrentObject.getStatusBean().getProgress());
        ProgressBar progressBar2 = magnetHolder.f;
        if (torrentObject.getStatusBean().getProgress() < 100 || !torrentObject.isGotDetails()) {
            i2 = 0;
        }
        progressBar2.setVisibility(i2);
        if (this.b != null) {
            magnetHolder.b.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    MagnetInfoListener magnetInfoListener = MagnetInfoAdapter.this.b;
                    if (magnetInfoListener != null) {
                        magnetInfoListener.a(torrentObject);
                    }
                }
            });
            magnetHolder.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    MagnetInfoListener magnetInfoListener = MagnetInfoAdapter.this.b;
                    if (magnetInfoListener != null) {
                        magnetInfoListener.d(torrentObject);
                    }
                }
            });
            if (torrentObject.getStatusBean().getProgress() < 100) {
                this.b.a(torrentObject, 5);
            } else if (!torrentObject.isGotDetails()) {
                this.b.a(torrentObject, 0);
            }
        }
    }

    public void b(TorrentObject torrentObject) {
        int i = 0;
        while (true) {
            if (i >= this.f5526a.size()) {
                break;
            } else if (this.f5526a.get(i).getId().equals(torrentObject.getId())) {
                this.f5526a.remove(i);
                break;
            } else {
                i++;
            }
        }
        notifyDataSetChanged();
    }

    public MagnetHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new MagnetHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.magnet_info_item, viewGroup, false));
    }

    public void a(TorrentObject torrentObject) {
        this.f5526a.add(torrentObject);
        notifyDataSetChanged();
    }
}
