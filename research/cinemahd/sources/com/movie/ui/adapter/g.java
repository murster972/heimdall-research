package com.movie.ui.adapter;

import com.movie.data.model.CalendarItem;
import com.movie.ui.adapter.CalendarAdapter;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class g implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5546a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ g(CalendarAdapter.MovieHolder movieHolder, CalendarItem calendarItem) {
        this.f5546a = movieHolder;
        this.b = calendarItem;
    }

    public final void subscribe(Observer observer) {
        this.f5546a.a(this.b, observer);
    }
}
