package com.movie.ui.adapter;

import com.movie.data.model.CalendarItem;
import com.movie.ui.adapter.CalendarAdapter;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class e implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5544a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ e(CalendarAdapter.MovieHolder movieHolder, CalendarItem calendarItem) {
        this.f5544a = movieHolder;
        this.b = calendarItem;
    }

    public final void subscribe(Observer observer) {
        this.f5544a.b(this.b, observer);
    }
}
