package com.movie.ui.adapter;

import com.movie.data.model.CalendarItem;
import com.movie.ui.adapter.CalendarAdapter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class f implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5545a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ f(CalendarAdapter.MovieHolder movieHolder, CalendarItem calendarItem) {
        this.f5545a = movieHolder;
        this.b = calendarItem;
    }

    public final void accept(Object obj) {
        this.f5545a.a(this.b, (CalendarAdapter.MovieHolder.HolderImage) obj);
    }
}
