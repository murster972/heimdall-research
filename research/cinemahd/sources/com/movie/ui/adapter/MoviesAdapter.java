package com.movie.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.palette.graphics.Palette;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.database.entitys.MovieEntity;
import com.github.florent37.glidepalette.BitmapPalette;
import com.github.florent37.glidepalette.GlidePalette;
import com.movie.FreeMoviesApp;
import com.movie.data.api.imdb.IMDBUtils;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.data.model.tmvdb.TvTMDB;
import com.movie.ui.fragment.MoviesFragment;
import com.utils.PosterCacheHelper;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.entities.SeriesImageQueryResultResponse;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Response;

public final class MoviesAdapter extends EndlessAdapter<MovieEntity, MovieHolder> {
    /* access modifiers changed from: private */
    public final Fragment d;
    /* access modifiers changed from: private */
    public OnMovieClickListener e;
    CompositeDisposable f;

    final class MovieHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        Disposable f5533a = null;
        private final StringBuilder b = new StringBuilder(30);
        private long c = 0;
        @BindColor(2131100135)
        int mColorBackground;
        @BindColor(2131099700)
        int mColorSubtitle;
        @BindColor(2131099705)
        int mColorTitle;
        @BindView(2131296805)
        View mContentContainer;
        @BindView(2131296806)
        View mFooterView;
        @BindView(2131296807)
        TextView mGenresView;
        @BindView(2131296808)
        ImageView mImageView;
        @BindString(2131755578)
        String mTextStart;
        @BindView(2131296809)
        TextView mTitleView;
        @BindView(2131297183)
        TextView mTvView;
        @BindView(2131296810)
        TextView mYearView;
        @BindView(2131297236)
        ProgressBar watchedPercent;

        private class HolderImage {

            /* renamed from: a  reason: collision with root package name */
            public String f5538a;
            public String b;

            public HolderImage(MovieHolder movieHolder, String str, String str2) {
                this.f5538a = str;
                this.b = str2;
            }
        }

        public MovieHolder(View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }

        static /* synthetic */ void a(Throwable th) throws Exception {
        }

        public /* synthetic */ void c(MovieEntity movieEntity, Observer observer) {
            if (movieEntity.getImdbIDStr() == null || movieEntity.getImdbIDStr().isEmpty()) {
                observer.onComplete();
                return;
            }
            String imdbIDStr = movieEntity.getImdbIDStr();
            MoviesAdapter moviesAdapter = MoviesAdapter.this;
            moviesAdapter.f.b(((MoviesFragment) moviesAdapter.d).g.search(imdbIDStr).map(new t(this, imdbIDStr)).subscribeOn(Schedulers.b()).subscribe(new y(observer), new r(observer)));
        }

        public Disposable a(final MovieEntity movieEntity, ImageView imageView) {
            return Observable.create(new ObservableOnSubscribe<HolderImage>() {
                public void subscribe(ObservableEmitter<HolderImage> observableEmitter) throws Exception {
                    if (movieEntity.getPoster_path() == null) {
                        PosterCacheHelper a2 = PosterCacheHelper.a();
                        String c = a2.c(movieEntity.getTmdbID(), movieEntity.getTvdbID(), movieEntity.getImdbIDStr());
                        String b2 = a2.b(movieEntity.getTmdbID(), movieEntity.getTvdbID(), movieEntity.getImdbIDStr());
                        if (c != null && !c.isEmpty()) {
                            observableEmitter.onNext(new HolderImage(MovieHolder.this, c, b2));
                        }
                    } else {
                        observableEmitter.onNext(new HolderImage(MovieHolder.this, movieEntity.getPoster_path(), movieEntity.getBackdrop_path()));
                    }
                    observableEmitter.onComplete();
                }
            }).switchIfEmpty(new o(this, movieEntity)).switchIfEmpty(new x(this, movieEntity)).switchIfEmpty(new p(this, movieEntity)).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new n(this, movieEntity), w.f5562a);
        }

        public /* synthetic */ void b(MovieEntity movieEntity, Observer observer) {
            if (movieEntity.getTV().booleanValue() && movieEntity.getTvdbID() > 0) {
                try {
                    Response<SeriesImageQueryResultResponse> execute = ((MoviesFragment) MoviesAdapter.this.d).h.series().imagesQuery((int) movieEntity.getTvdbID(), "poster", (String) null, (String) null, "en").execute();
                    if (execute.isSuccessful()) {
                        observer.onNext(new HolderImage(this, "http://thetvdb.com/banners/" + execute.body().data.get(0).fileName, ""));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            observer.onComplete();
        }

        private void b(final MovieEntity movieEntity) {
            RequestBuilder<Drawable> a2 = Glide.a(MoviesAdapter.this.d).a(movieEntity.getPoster_path()).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).b(new RequestListener<Drawable>(this) {
                /* renamed from: a */
                public boolean onResourceReady(Drawable drawable, Object obj, Target<Drawable> target, DataSource dataSource, boolean z) {
                    return false;
                }

                public boolean onLoadFailed(GlideException glideException, Object obj, Target<Drawable> target, boolean z) {
                    PosterCacheHelper.a().a(movieEntity.getTmdbID(), movieEntity.getTvdbID(), movieEntity.getImdbIDStr());
                    return false;
                }
            }).a(new DrawableTransitionOptions().b());
            if (!FreeMoviesApp.o()) {
                a2.b((RequestListener<Drawable>) GlidePalette.a(movieEntity.getPoster_path()).a((BitmapPalette.CallBack) new BitmapPalette.CallBack() {
                    public void a(Palette palette) {
                        MovieHolder.this.a(palette.b());
                    }
                }));
            }
            a2.a(this.mImageView);
        }

        private void c(MovieEntity movieEntity) {
            if (this.c != movieEntity.getTmdbID()) {
                this.c = movieEntity.getTmdbID();
                this.mFooterView.setBackgroundColor(this.mColorBackground);
                this.mTitleView.setTextColor(this.mColorTitle);
                this.mGenresView.setTextColor(this.mColorSubtitle);
                this.mImageView.setImageDrawable((Drawable) null);
                this.watchedPercent.setVisibility(8);
            }
        }

        public /* synthetic */ void a(MovieEntity movieEntity, Observer observer) {
            if (movieEntity.getTmdbID() <= 0) {
                observer.onComplete();
            } else if (movieEntity.getTV().booleanValue()) {
                MoviesAdapter moviesAdapter = MoviesAdapter.this;
                moviesAdapter.f.b(((MoviesFragment) moviesAdapter.d).f.getTvDetails(movieEntity.getTmdbID()).subscribeOn(Schedulers.b()).subscribe(new q(this, observer), new s(observer)));
            } else {
                MoviesAdapter moviesAdapter2 = MoviesAdapter.this;
                moviesAdapter2.f.b(((MoviesFragment) moviesAdapter2.d).f.getMovieDetails(this.c, (String) null).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new u(this, observer), new v(observer)));
            }
        }

        public /* synthetic */ void a(Observer observer, TvTMDB.ResultsBean resultsBean) throws Exception {
            if (resultsBean.getPoster_path() != null && !resultsBean.getBackdrop_path().isEmpty()) {
                observer.onNext(new HolderImage(this, resultsBean.getPoster_path(), resultsBean.getBackdrop_path()));
            }
            observer.onComplete();
        }

        public /* synthetic */ void a(Observer observer, MovieTMDB.ResultsBean resultsBean) throws Exception {
            if (resultsBean.getPoster_path() != null && !resultsBean.getBackdrop_path().isEmpty()) {
                observer.onNext(new HolderImage(this, resultsBean.getPoster_path(), resultsBean.getBackdrop_path()));
            }
            observer.onComplete();
        }

        public /* synthetic */ HolderImage a(String str, ResponseBody responseBody) throws Exception {
            return new HolderImage(this, IMDBUtils.a(str, responseBody, true).getPoster_path(), "");
        }

        static /* synthetic */ void a(Observer observer, HolderImage holderImage) throws Exception {
            observer.onNext(holderImage);
            observer.onComplete();
        }

        public /* synthetic */ void a(MovieEntity movieEntity, HolderImage holderImage) throws Exception {
            movieEntity.setPoster_path(holderImage.f5538a);
            movieEntity.setBackdrop_path(holderImage.b);
            PosterCacheHelper a2 = PosterCacheHelper.a();
            long tmdbID = movieEntity.getTmdbID();
            long tvdbID = movieEntity.getTvdbID();
            String imdbIDStr = movieEntity.getImdbIDStr();
            String str = holderImage.f5538a;
            String str2 = holderImage.b;
            if (str2 == null) {
                str2 = "";
            }
            a2.a(tmdbID, tvdbID, imdbIDStr, str, str2);
            b(movieEntity);
        }

        public void a(MovieEntity movieEntity) {
            Disposable disposable = this.f5533a;
            if (disposable != null && !disposable.isDisposed()) {
                MoviesAdapter.this.f.a(this.f5533a);
            }
            this.mContentContainer.setOnClickListener(new m(this, movieEntity));
            this.mTitleView.setText(movieEntity.getName());
            this.mGenresView.setText(Utils.a(movieEntity.getGenres(), ", ", this.b));
            this.mYearView.setText(movieEntity.getRealeaseDate());
            c(movieEntity);
            this.f5533a = a(movieEntity, this.mImageView);
            MoviesAdapter.this.f.b(this.f5533a);
            if (movieEntity.getTV().booleanValue()) {
                int numberSeason = movieEntity.getNumberSeason();
                String format = String.format(" %02d ", new Object[]{Integer.valueOf(numberSeason)});
                if (numberSeason <= 0) {
                    format = "TV";
                }
                this.mTvView.setText(format);
                this.mTvView.setVisibility(0);
            } else {
                this.mTvView.setVisibility(4);
            }
            this.mContentContainer.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View view, boolean z) {
                    if (z) {
                        MovieHolder.this.mImageView.setAlpha(0.5f);
                    } else {
                        MovieHolder.this.mImageView.setAlpha(1.0f);
                    }
                }
            });
            if (movieEntity.getPosition() > 0 && movieEntity.getDuration() > 0) {
                this.watchedPercent.setVisibility(0);
                this.watchedPercent.setProgress((int) ((movieEntity.getPosition() * 100) / movieEntity.getDuration()));
            }
        }

        public /* synthetic */ void a(MovieEntity movieEntity, View view) {
            MoviesAdapter.this.e.a(movieEntity, view, getAdapterPosition());
        }

        /* access modifiers changed from: private */
        public void a(Palette.Swatch swatch) {
            if (swatch != null) {
                this.mFooterView.setBackgroundColor(swatch.d());
                this.mTitleView.setTextColor(swatch.a());
                this.mGenresView.setTextColor(swatch.e());
            }
        }
    }

    public final class MovieHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private MovieHolder f5539a;

        public MovieHolder_ViewBinding(MovieHolder movieHolder, View view) {
            this.f5539a = movieHolder;
            movieHolder.mContentContainer = butterknife.internal.Utils.findRequiredView(view, R.id.movie_item_container, "field 'mContentContainer'");
            movieHolder.mImageView = (ImageView) butterknife.internal.Utils.findRequiredViewAsType(view, R.id.movie_item_image, "field 'mImageView'", ImageView.class);
            movieHolder.mTvView = (TextView) butterknife.internal.Utils.findRequiredViewAsType(view, R.id.tvMV, "field 'mTvView'", TextView.class);
            movieHolder.mTitleView = (TextView) butterknife.internal.Utils.findRequiredViewAsType(view, R.id.movie_item_title, "field 'mTitleView'", TextView.class);
            movieHolder.mGenresView = (TextView) butterknife.internal.Utils.findRequiredViewAsType(view, R.id.movie_item_genres, "field 'mGenresView'", TextView.class);
            movieHolder.mYearView = (TextView) butterknife.internal.Utils.findRequiredViewAsType(view, R.id.movie_item_year, "field 'mYearView'", TextView.class);
            movieHolder.watchedPercent = (ProgressBar) butterknife.internal.Utils.findRequiredViewAsType(view, R.id.watched_percent, "field 'watchedPercent'", ProgressBar.class);
            movieHolder.mFooterView = butterknife.internal.Utils.findRequiredView(view, R.id.movie_item_footer, "field 'mFooterView'");
            Context context = view.getContext();
            Resources resources = context.getResources();
            movieHolder.mColorBackground = ContextCompat.a(context, (int) R.color.theme_primary);
            movieHolder.mColorTitle = ContextCompat.a(context, (int) R.color.body_text_white);
            movieHolder.mColorSubtitle = ContextCompat.a(context, (int) R.color.body_text_1_inverse);
            movieHolder.mTextStart = resources.getString(R.string.text_star);
        }

        public void unbind() {
            MovieHolder movieHolder = this.f5539a;
            if (movieHolder != null) {
                this.f5539a = null;
                movieHolder.mContentContainer = null;
                movieHolder.mImageView = null;
                movieHolder.mTvView = null;
                movieHolder.mTitleView = null;
                movieHolder.mGenresView = null;
                movieHolder.mYearView = null;
                movieHolder.watchedPercent = null;
                movieHolder.mFooterView = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public interface OnMovieClickListener {
        public static final OnMovieClickListener b0 = new OnMovieClickListener() {
            public void a(MovieEntity movieEntity, View view, int i) {
            }
        };

        void a(MovieEntity movieEntity, View view, int i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public MoviesAdapter(Fragment fragment, List<MovieEntity> list) {
        super(fragment.getActivity(), list == null ? new ArrayList<>() : list);
        this.e = OnMovieClickListener.b0;
        this.f = null;
        this.d = fragment;
        setHasStableIds(true);
        this.f = new CompositeDisposable();
    }

    public long getItemId(int i) {
        if (a(i)) {
            return -1;
        }
        return ((MovieEntity) getItem(i)).getTmdbID();
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (getItemViewType(i) == 2) {
            ((MovieHolder) viewHolder).a((MovieEntity) this.b.get(i));
        }
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.f.dispose();
    }

    public void a(OnMovieClickListener onMovieClickListener) {
        this.e = onMovieClickListener;
    }

    public void b(final boolean z) {
        Collections.sort(this.b, new Comparator<MovieEntity>(this) {
            /* renamed from: a */
            public int compare(MovieEntity movieEntity, MovieEntity movieEntity2) {
                if (z) {
                    return movieEntity.getName().compareToIgnoreCase(movieEntity2.getName());
                }
                return movieEntity2.getName().compareToIgnoreCase(movieEntity.getName());
            }
        });
        notifyDataSetChanged();
    }

    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new MovieHolder(this.f5523a.inflate(R.layout.item_movie, viewGroup, false));
    }

    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder b(ViewGroup viewGroup) {
        return new MovieHolder(this.f5523a.inflate(R.layout.item_movie, viewGroup, false));
    }
}
