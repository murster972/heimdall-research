package com.movie.ui.adapter;

import android.view.View;
import com.movie.data.model.CalendarItem;
import com.movie.ui.adapter.CalendarAdapter;

/* compiled from: lambda */
public final /* synthetic */ class k implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5550a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ k(CalendarAdapter.MovieHolder movieHolder, CalendarItem calendarItem) {
        this.f5550a = movieHolder;
        this.b = calendarItem;
    }

    public final void onClick(View view) {
        this.f5550a.a(this.b, view);
    }
}
