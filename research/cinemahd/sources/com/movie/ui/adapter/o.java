package com.movie.ui.adapter;

import com.database.entitys.MovieEntity;
import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class o implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5554a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ o(MoviesAdapter.MovieHolder movieHolder, MovieEntity movieEntity) {
        this.f5554a = movieHolder;
        this.b = movieEntity;
    }

    public final void subscribe(Observer observer) {
        this.f5554a.a(this.b, observer);
    }
}
