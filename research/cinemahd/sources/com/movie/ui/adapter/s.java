package com.movie.ui.adapter;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class s implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5558a;

    public /* synthetic */ s(Observer observer) {
        this.f5558a = observer;
    }

    public final void accept(Object obj) {
        this.f5558a.onComplete();
    }
}
