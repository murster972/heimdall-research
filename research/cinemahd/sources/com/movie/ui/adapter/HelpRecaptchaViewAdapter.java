package com.movie.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.movie.data.model.ItemHelpCaptcha;
import com.yoku.marumovie.R;
import java.util.List;

public class HelpRecaptchaViewAdapter extends RecyclerView.Adapter<ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    private List<ItemHelpCaptcha> f5524a;
    private LayoutInflater b;
    /* access modifiers changed from: private */
    public ItemClickListener c;

    public interface ItemClickListener {
        void a(View view, ItemHelpCaptcha itemHelpCaptcha);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        TextView f5525a;
        Button b;

        ViewHolder(View view) {
            super(view);
            this.f5525a = (TextView) view.findViewById(R.id.help_provider_text);
            this.b = (Button) view.findViewById(R.id.btn_help_verify);
            view.setOnClickListener(this);
            this.b.setOnClickListener(this);
        }

        public void onClick(View view) {
            if (HelpRecaptchaViewAdapter.this.c != null) {
                HelpRecaptchaViewAdapter.this.c.a(view, HelpRecaptchaViewAdapter.this.getItem(getAdapterPosition()));
            }
        }
    }

    public HelpRecaptchaViewAdapter(Context context, List<ItemHelpCaptcha> list, ItemClickListener itemClickListener) {
        this.b = LayoutInflater.from(context);
        this.f5524a = list;
        this.c = itemClickListener;
    }

    /* access modifiers changed from: package-private */
    public ItemHelpCaptcha getItem(int i) {
        return this.f5524a.get(i);
    }

    public int getItemCount() {
        return this.f5524a.size();
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        viewHolder.f5525a.setText(this.f5524a.get(i).getProviderName());
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(this.b.inflate(R.layout.help_recaptcha_recycleview_row, viewGroup, false));
    }
}
