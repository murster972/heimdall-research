package com.movie.ui.adapter;

import android.view.View;
import com.movie.data.model.CalendarItem;
import com.movie.ui.adapter.CalendarAdapter;

/* compiled from: lambda */
public final /* synthetic */ class d implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ CalendarAdapter.MovieHolder f5543a;
    private final /* synthetic */ CalendarItem b;

    public /* synthetic */ d(CalendarAdapter.MovieHolder movieHolder, CalendarItem calendarItem) {
        this.f5543a = movieHolder;
        this.b = calendarItem;
    }

    public final void onClick(View view) {
        this.f5543a.b(this.b, view);
    }
}
