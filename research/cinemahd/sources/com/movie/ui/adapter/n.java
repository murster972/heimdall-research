package com.movie.ui.adapter;

import com.database.entitys.MovieEntity;
import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class n implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5553a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ n(MoviesAdapter.MovieHolder movieHolder, MovieEntity movieEntity) {
        this.f5553a = movieHolder;
        this.b = movieEntity;
    }

    public final void accept(Object obj) {
        this.f5553a.a(this.b, (MoviesAdapter.MovieHolder.HolderImage) obj);
    }
}
