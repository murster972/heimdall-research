package com.movie.ui.adapter;

import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class u implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5560a;
    private final /* synthetic */ Observer b;

    public /* synthetic */ u(MoviesAdapter.MovieHolder movieHolder, Observer observer) {
        this.f5560a = movieHolder;
        this.b = observer;
    }

    public final void accept(Object obj) {
        this.f5560a.a(this.b, (MovieTMDB.ResultsBean) obj);
    }
}
