package com.movie.ui.adapter;

import com.movie.data.model.tmvdb.TvTMDB;
import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class q implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5556a;
    private final /* synthetic */ Observer b;

    public /* synthetic */ q(MoviesAdapter.MovieHolder movieHolder, Observer observer) {
        this.f5556a = movieHolder;
        this.b = observer;
    }

    public final void accept(Object obj) {
        this.f5556a.a(this.b, (TvTMDB.ResultsBean) obj);
    }
}
