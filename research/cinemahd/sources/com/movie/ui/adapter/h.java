package com.movie.ui.adapter;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class h implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5547a;

    public /* synthetic */ h(Observer observer) {
        this.f5547a = observer;
    }

    public final void accept(Object obj) {
        this.f5547a.onComplete();
    }
}
