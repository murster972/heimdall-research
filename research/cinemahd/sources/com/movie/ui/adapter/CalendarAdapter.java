package com.movie.ui.adapter;

import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.movie.data.api.imdb.IMDBUtils;
import com.movie.data.model.CalendarItem;
import com.movie.data.model.tmvdb.TvTMDB;
import com.movie.ui.activity.CalendarActivity;
import com.movie.ui.widget.AspectLockedImageView;
import com.utils.PosterCacheHelper;
import com.uwetrottmann.thetvdb.entities.SeriesImageQueryResultResponse;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import okhttp3.ResponseBody;
import retrofit2.Response;

public final class CalendarAdapter extends EndlessAdapter<CalendarItem, MovieHolder> {
    /* access modifiers changed from: private */
    public final CalendarActivity d;
    CompositeDisposable e;
    /* access modifiers changed from: private */
    public OnCalendarClickListener f;

    final class MovieHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        Disposable f5518a = null;
        private long b;
        @BindView(2131296459)
        CardView calendar_container;
        @BindView(2131296593)
        AspectLockedImageView epi_cover;
        @BindView(2131297178)
        TextView tvEpiName;
        @BindView(2131297188)
        TextView tvOverview;
        @BindView(2131297193)
        TextView tvTitle;
        @BindView(2131297235)
        CheckBox watched;

        private class HolderImage {

            /* renamed from: a  reason: collision with root package name */
            public String f5521a;
            public String b;

            public HolderImage(MovieHolder movieHolder, String str, String str2) {
                this.f5521a = str;
                this.b = str2;
            }
        }

        public MovieHolder(View view) {
            super(view);
            ButterKnife.bind((Object) this, view);
        }

        static /* synthetic */ void a(Throwable th) throws Exception {
        }

        public void a(CalendarItem calendarItem) {
            Disposable disposable = this.f5518a;
            if (disposable != null && !disposable.isDisposed()) {
                CalendarAdapter.this.e.a(this.f5518a);
            }
            this.calendar_container.setOnClickListener(new k(this, calendarItem));
            this.epi_cover.setOnClickListener(new d(this, calendarItem));
            this.tvTitle.setText(calendarItem.showName);
            TextView textView = this.tvEpiName;
            textView.setText(calendarItem.season + "x" + calendarItem.episode + " " + calendarItem.episodeName);
            this.tvOverview.setText(calendarItem.airTime);
            this.epi_cover.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                public void onFocusChange(View view, boolean z) {
                    if (z) {
                        MovieHolder.this.epi_cover.setAlpha(0.5f);
                    } else {
                        MovieHolder.this.epi_cover.setAlpha(1.0f);
                    }
                }
            });
            long j = this.b;
            long j2 = calendarItem.tmdbID;
            if (j != j2) {
                this.b = j2;
                this.epi_cover.setImageDrawable((Drawable) null);
            }
            String str = calendarItem.poster;
            if (str == null || str.isEmpty()) {
                CalendarAdapter.this.e.b(a(calendarItem, (ImageView) this.epi_cover));
            } else {
                b(calendarItem);
            }
        }

        public /* synthetic */ void b(CalendarItem calendarItem, View view) {
            CalendarAdapter.this.f.a(calendarItem, view, getAdapterPosition());
        }

        public /* synthetic */ void c(CalendarItem calendarItem, Observer observer) {
            String str = calendarItem.imdbID;
            if (str == null || str.isEmpty()) {
                observer.onComplete();
                return;
            }
            String str2 = calendarItem.imdbID;
            CalendarAdapter calendarAdapter = CalendarAdapter.this;
            calendarAdapter.e.b(calendarAdapter.d.f.search(str2).map(new b(this, str2)).subscribeOn(Schedulers.b()).subscribe(new i(observer), new j(observer)));
        }

        public /* synthetic */ void b(CalendarItem calendarItem, Observer observer) {
            if (calendarItem.tvdnID > 0) {
                try {
                    Response<SeriesImageQueryResultResponse> execute = CalendarAdapter.this.d.g.series().imagesQuery((int) calendarItem.tvdnID, "poster", (String) null, (String) null, "en").execute();
                    if (execute.isSuccessful()) {
                        observer.onNext(new HolderImage(this, "http://thetvdb.com/banners/" + execute.body().data.get(0).fileName, ""));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            observer.onComplete();
        }

        private void b(CalendarItem calendarItem) {
            if (calendarItem.poster != null) {
                Glide.a((FragmentActivity) CalendarAdapter.this.d).a(calendarItem.poster).a((BaseRequestOptions<?>) ((RequestOptions) new RequestOptions().b((int) R.color.movie_cover_placeholder)).b()).a(new DrawableTransitionOptions().b()).a((ImageView) this.epi_cover);
            }
        }

        public /* synthetic */ void a(CalendarItem calendarItem, View view) {
            CalendarAdapter.this.f.a(calendarItem, view, getAdapterPosition());
        }

        public Disposable a(final CalendarItem calendarItem, ImageView imageView) {
            return Observable.create(new ObservableOnSubscribe<HolderImage>() {
                public void subscribe(ObservableEmitter<HolderImage> observableEmitter) throws Exception {
                    CalendarItem calendarItem = calendarItem;
                    String str = calendarItem.poster;
                    if (str == null) {
                        PosterCacheHelper a2 = PosterCacheHelper.a();
                        CalendarItem calendarItem2 = calendarItem;
                        String c = a2.c(calendarItem2.tmdbID, calendarItem2.tvdnID, calendarItem2.imdbID);
                        CalendarItem calendarItem3 = calendarItem;
                        String b2 = a2.b(calendarItem3.tmdbID, calendarItem3.tvdnID, calendarItem3.imdbID);
                        if (c != null && !c.isEmpty()) {
                            observableEmitter.onNext(new HolderImage(MovieHolder.this, c, b2));
                        }
                    } else {
                        observableEmitter.onNext(new HolderImage(MovieHolder.this, str, calendarItem.backdrop));
                    }
                    observableEmitter.onComplete();
                }
            }).switchIfEmpty(new g(this, calendarItem)).switchIfEmpty(new e(this, calendarItem)).switchIfEmpty(new c(this, calendarItem)).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new f(this, calendarItem), a.f5540a);
        }

        public /* synthetic */ void a(CalendarItem calendarItem, Observer observer) {
            if (calendarItem.tmdbID > 0) {
                CalendarAdapter calendarAdapter = CalendarAdapter.this;
                calendarAdapter.e.b(calendarAdapter.d.e.getTvDetails(calendarItem.tmdbID).subscribeOn(Schedulers.b()).subscribe(new l(this, observer), new h(observer)));
                return;
            }
            observer.onComplete();
        }

        public /* synthetic */ void a(Observer observer, TvTMDB.ResultsBean resultsBean) throws Exception {
            if (resultsBean.getPoster_path() != null && !resultsBean.getBackdrop_path().isEmpty()) {
                observer.onNext(new HolderImage(this, resultsBean.getPoster_path(), resultsBean.getBackdrop_path()));
            }
            observer.onComplete();
        }

        public /* synthetic */ HolderImage a(String str, ResponseBody responseBody) throws Exception {
            return new HolderImage(this, IMDBUtils.a(str, responseBody, true).getPoster_path(), "");
        }

        static /* synthetic */ void a(Observer observer, HolderImage holderImage) throws Exception {
            observer.onNext(holderImage);
            observer.onComplete();
        }

        public /* synthetic */ void a(CalendarItem calendarItem, HolderImage holderImage) throws Exception {
            calendarItem.poster = holderImage.f5521a;
            calendarItem.backdrop = holderImage.b;
            PosterCacheHelper.a().a(calendarItem.tmdbID, calendarItem.tvdnID, calendarItem.imdbID, holderImage.f5521a, holderImage.b);
            b(calendarItem);
        }
    }

    public final class MovieHolder_ViewBinding implements Unbinder {

        /* renamed from: a  reason: collision with root package name */
        private MovieHolder f5522a;

        public MovieHolder_ViewBinding(MovieHolder movieHolder, View view) {
            this.f5522a = movieHolder;
            movieHolder.epi_cover = (AspectLockedImageView) Utils.findRequiredViewAsType(view, R.id.epi_cover, "field 'epi_cover'", AspectLockedImageView.class);
            movieHolder.tvOverview = (TextView) Utils.findRequiredViewAsType(view, R.id.tvOverview, "field 'tvOverview'", TextView.class);
            movieHolder.tvEpiName = (TextView) Utils.findRequiredViewAsType(view, R.id.tvEpiName, "field 'tvEpiName'", TextView.class);
            movieHolder.tvTitle = (TextView) Utils.findRequiredViewAsType(view, R.id.tvTabTitle, "field 'tvTitle'", TextView.class);
            movieHolder.watched = (CheckBox) Utils.findRequiredViewAsType(view, R.id.watched, "field 'watched'", CheckBox.class);
            movieHolder.calendar_container = (CardView) Utils.findRequiredViewAsType(view, R.id.calendar_container, "field 'calendar_container'", CardView.class);
        }

        public void unbind() {
            MovieHolder movieHolder = this.f5522a;
            if (movieHolder != null) {
                this.f5522a = null;
                movieHolder.epi_cover = null;
                movieHolder.tvOverview = null;
                movieHolder.tvEpiName = null;
                movieHolder.tvTitle = null;
                movieHolder.watched = null;
                movieHolder.calendar_container = null;
                return;
            }
            throw new IllegalStateException("Bindings already cleared.");
        }
    }

    public interface OnCalendarClickListener {
        public static final OnCalendarClickListener a0 = new OnCalendarClickListener() {
            public void a(CalendarItem calendarItem, View view, int i) {
            }
        };

        void a(CalendarItem calendarItem, View view, int i);
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public CalendarAdapter(CalendarActivity calendarActivity, List<CalendarItem> list) {
        super(calendarActivity, list == null ? new ArrayList<>() : list);
        this.e = null;
        this.f = OnCalendarClickListener.a0;
        this.d = calendarActivity;
        setHasStableIds(true);
        this.e = new CompositeDisposable();
    }

    public long getItemId(int i) {
        if (a(i)) {
            return -1;
        }
        return (long) ((CalendarItem) getItem(i)).episode;
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (getItemViewType(i) == 2) {
            ((MovieHolder) viewHolder).a((CalendarItem) this.b.get(i));
        }
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.e.dispose();
    }

    public void a(OnCalendarClickListener onCalendarClickListener) {
        this.f = onCalendarClickListener;
    }

    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder b(ViewGroup viewGroup) {
        return new MovieHolder(this.f5523a.inflate(R.layout.calendar_item, viewGroup, false));
    }

    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return new MovieHolder(this.f5523a.inflate(R.layout.calendar_item, viewGroup, false));
    }
}
