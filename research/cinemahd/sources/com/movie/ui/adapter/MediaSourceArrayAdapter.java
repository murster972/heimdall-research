package com.movie.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import com.original.tase.model.media.MediaSource;
import com.yoku.marumovie.R;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MediaSourceArrayAdapter extends ArrayAdapter<MediaSource> {

    /* renamed from: a  reason: collision with root package name */
    private final int f5530a;
    /* access modifiers changed from: private */
    public List<MediaSource> b;
    private final LayoutInflater c;
    /* access modifiers changed from: private */
    public Listener d = null;

    public interface Listener {
        void a(MediaSource mediaSource);
    }

    public MediaSourceArrayAdapter(Context context, int i, List<MediaSource> list) {
        super(context, i, 0, list);
        this.f5530a = i;
        this.c = LayoutInflater.from(context);
        this.b = list;
    }

    public void clear() {
        List<MediaSource> list = this.b;
        if (list != null) {
            list.clear();
        } else {
            this.b = new ArrayList();
        }
    }

    public int getCount() {
        return this.b.size();
    }

    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = this.c.inflate(this.f5530a, viewGroup, false);
        }
        try {
            LinearLayout linearLayout = (LinearLayout) view;
            TextView textView = (TextView) linearLayout.findViewById(R.id.text1);
            ImageButton imageButton = (ImageButton) linearLayout.findViewById(R.id.download_btn);
            MediaSource mediaSource = (MediaSource) getItem(i);
            if (mediaSource != null) {
                textView.setText(mediaSource.toString2());
                if (!mediaSource.isDebrid()) {
                    textView.setTextColor(-1);
                } else if (mediaSource.isRawTorrent()) {
                    textView.setTextColor(-16711936);
                } else {
                    textView.setTextColor(-256);
                }
                if (mediaSource.isPlayed()) {
                    textView.setTextColor(-7829368);
                }
            }
            if (mediaSource.isHLS()) {
                imageButton.setVisibility(8);
            } else {
                imageButton.setVisibility(0);
                imageButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        View view2 = (View) view.getParent();
                        MediaSourceArrayAdapter.this.d.a((MediaSource) MediaSourceArrayAdapter.this.b.get(((ListView) view2.getParent()).getPositionForView(view2)));
                    }
                });
            }
            return view;
        } catch (Throwable th) {
            throw new IllegalStateException("ArrayAdapter requires the resource ID to be a TextView", th);
        }
    }

    public void a(Listener listener) {
        this.d = listener;
    }

    public void a(MediaSource mediaSource) {
        if (mediaSource != null && !this.b.contains(mediaSource)) {
            this.b.add(mediaSource);
        }
    }

    public void a() {
        Collections.sort(this.b);
        notifyDataSetChanged();
    }
}
