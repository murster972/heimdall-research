package com.movie.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerView.ViewHolder;
import com.yoku.marumovie.R;
import java.util.List;
import rx.functions.Action1;

public abstract class EndlessAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Action1<List<T>> {

    /* renamed from: a  reason: collision with root package name */
    protected final LayoutInflater f5523a;
    protected List<T> b;
    protected boolean c = false;

    public EndlessAdapter(Context context, List<T> list) {
        this.f5523a = LayoutInflater.from(context);
        this.b = list;
    }

    /* access modifiers changed from: protected */
    public abstract RecyclerView.ViewHolder a(ViewGroup viewGroup);

    public void a(boolean z) {
        boolean z2 = this.c;
        if (z2 == z) {
            return;
        }
        if (z2) {
            notifyItemRemoved(getItemCount());
            this.c = false;
            return;
        }
        notifyItemInserted(getItemCount());
        this.c = true;
    }

    /* access modifiers changed from: protected */
    public abstract RecyclerView.ViewHolder b(ViewGroup viewGroup);

    public void b() {
        if (!this.b.isEmpty()) {
            this.b.clear();
            notifyDataSetChanged();
        }
    }

    /* access modifiers changed from: protected */
    public int c() {
        return this.c ? 1 : 0;
    }

    public List<T> d() {
        return this.b;
    }

    public boolean e() {
        return this.c;
    }

    public T getItem(int i) {
        if (!a(i)) {
            return this.b.get(i);
        }
        return null;
    }

    public int getItemCount() {
        return this.b.size() + c();
    }

    public int getItemViewType(int i) {
        return a(i) ? 1 : 2;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        if (i == 2) {
            return b(viewGroup);
        }
        if (i != 3) {
            return new RecyclerView.ViewHolder(this, this.f5523a.inflate(R.layout.item_load_more, viewGroup, false)) {
            };
        }
        return a(viewGroup);
    }

    public boolean a(int i) {
        return this.c && i == getItemCount() - 1;
    }

    public void a(List<T> list) {
        this.b = list;
        notifyDataSetChanged();
    }

    public void a(T t) {
        int size = this.b.size();
        this.b.add(t);
        notifyItemRangeInserted(size, 1);
    }
}
