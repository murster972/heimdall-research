package com.movie.ui.adapter;

import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class y implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5564a;

    public /* synthetic */ y(Observer observer) {
        this.f5564a = observer;
    }

    public final void accept(Object obj) {
        MoviesAdapter.MovieHolder.a(this.f5564a, (MoviesAdapter.MovieHolder.HolderImage) obj);
    }
}
