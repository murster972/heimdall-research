package com.movie.ui.adapter;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class r implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5557a;

    public /* synthetic */ r(Observer observer) {
        this.f5557a = observer;
    }

    public final void accept(Object obj) {
        this.f5557a.onComplete();
    }
}
