package com.movie.ui.adapter;

import com.database.entitys.MovieEntity;
import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class x implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5563a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ x(MoviesAdapter.MovieHolder movieHolder, MovieEntity movieEntity) {
        this.f5563a = movieHolder;
        this.b = movieEntity;
    }

    public final void subscribe(Observer observer) {
        this.f5563a.b(this.b, observer);
    }
}
