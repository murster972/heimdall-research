package com.movie.ui.adapter;

import com.movie.ui.adapter.MoviesAdapter;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;

/* compiled from: lambda */
public final /* synthetic */ class t implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5559a;
    private final /* synthetic */ String b;

    public /* synthetic */ t(MoviesAdapter.MovieHolder movieHolder, String str) {
        this.f5559a = movieHolder;
        this.b = str;
    }

    public final Object apply(Object obj) {
        return this.f5559a.a(this.b, (ResponseBody) obj);
    }
}
