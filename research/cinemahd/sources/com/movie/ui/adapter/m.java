package com.movie.ui.adapter;

import android.view.View;
import com.database.entitys.MovieEntity;
import com.movie.ui.adapter.MoviesAdapter;

/* compiled from: lambda */
public final /* synthetic */ class m implements View.OnClickListener {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesAdapter.MovieHolder f5552a;
    private final /* synthetic */ MovieEntity b;

    public /* synthetic */ m(MoviesAdapter.MovieHolder movieHolder, MovieEntity movieEntity) {
        this.f5552a = movieHolder;
        this.b = movieEntity;
    }

    public final void onClick(View view) {
        this.f5552a.a(this.b, view);
    }
}
