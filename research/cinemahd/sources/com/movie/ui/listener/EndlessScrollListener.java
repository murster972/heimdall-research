package com.movie.ui.listener;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public abstract class EndlessScrollListener extends RecyclerView.OnScrollListener {

    /* renamed from: a  reason: collision with root package name */
    private int f5808a = 5;
    private int b = 0;
    private int c = 0;
    private boolean d = true;
    private int e = 0;
    private OnLoadMoreCallback f;

    public interface OnLoadMoreCallback {
        void a(int i, int i2);
    }

    public EndlessScrollListener() {
    }

    public EndlessScrollListener a(OnLoadMoreCallback onLoadMoreCallback) {
        this.f = onLoadMoreCallback;
        return this;
    }

    public void b(int i, int i2, int i3) {
        if (i3 < this.c) {
            this.b = this.e;
            this.c = i3;
            if (i3 == 0) {
                this.d = true;
            }
        }
        if (this.d && i3 > this.c) {
            this.d = false;
            this.c = i3;
            this.b++;
        }
        if ((!this.d && i3 - i2 <= i + this.f5808a) || i2 <= 0) {
            d(this.b + 1, i3);
            this.d = true;
        }
    }

    public void d(int i, int i2) {
        OnLoadMoreCallback onLoadMoreCallback = this.f;
        if (onLoadMoreCallback != null) {
            onLoadMoreCallback.a(i, i2);
        }
    }

    public static EndlessScrollListener a(final LinearLayoutManager linearLayoutManager, int i, int i2) {
        return new EndlessScrollListener(i, i2) {
            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
                if (i2 >= 0) {
                    int findFirstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();
                    b(findFirstVisibleItemPosition, linearLayoutManager.findLastVisibleItemPosition() - findFirstVisibleItemPosition, linearLayoutManager.getItemCount());
                }
            }
        };
    }

    public EndlessScrollListener(int i, int i2) {
        this.f5808a = i;
        this.e = i2;
        this.b = i2;
    }
}
