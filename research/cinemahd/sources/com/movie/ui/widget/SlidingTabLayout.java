package com.movie.ui.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.TextView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

public class SlidingTabLayout extends HorizontalScrollView {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final SlidingTabStrip f5815a;
    private int b;
    private int c;
    private int d;
    /* access modifiers changed from: private */
    public ViewPager e;
    /* access modifiers changed from: private */
    public ViewPager.OnPageChangeListener f;
    /* access modifiers changed from: private */
    public OnTabClickListener g;

    private class InternalViewPagerListener implements ViewPager.OnPageChangeListener {

        /* renamed from: a  reason: collision with root package name */
        private int f5816a;

        private InternalViewPagerListener() {
        }

        public void onPageScrollStateChanged(int i) {
            this.f5816a = i;
            if (SlidingTabLayout.this.f != null) {
                SlidingTabLayout.this.f.onPageScrollStateChanged(i);
            }
        }

        public void onPageScrolled(int i, float f, int i2) {
            int childCount = SlidingTabLayout.this.f5815a.getChildCount();
            if (childCount != 0 && i >= 0 && i < childCount) {
                SlidingTabLayout.this.f5815a.a(i, f);
                View childAt = SlidingTabLayout.this.f5815a.getChildAt(i);
                SlidingTabLayout.this.b(i, childAt != null ? (int) (((float) childAt.getWidth()) * f) : 0);
                if (SlidingTabLayout.this.f != null) {
                    SlidingTabLayout.this.f.onPageScrolled(i, f, i2);
                }
            }
        }

        public void onPageSelected(int i) {
            if (this.f5816a == 0) {
                SlidingTabLayout.this.f5815a.a(i, 0.0f);
                SlidingTabLayout.this.b(i, 0);
            }
            if (SlidingTabLayout.this.f != null) {
                SlidingTabLayout.this.f.onPageSelected(i);
            }
        }
    }

    public interface OnTabClickListener {
        void a(int i);
    }

    private class TabClickListener implements View.OnClickListener {
        private TabClickListener() {
        }

        public void onClick(View view) {
            for (int i = 0; i < SlidingTabLayout.this.f5815a.getChildCount(); i++) {
                if (view == SlidingTabLayout.this.f5815a.getChildAt(i)) {
                    if (SlidingTabLayout.this.g != null) {
                        SlidingTabLayout.this.g.a(i);
                    }
                    SlidingTabLayout.this.e.setCurrentItem(i);
                    return;
                }
            }
        }
    }

    public interface TabColorizer {
        int a(int i);
    }

    public SlidingTabLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewPager viewPager = this.e;
        if (viewPager != null) {
            b(viewPager.getCurrentItem(), 0);
        }
    }

    public void setCustomTabColorizer(TabColorizer tabColorizer) {
        this.f5815a.a(tabColorizer);
    }

    public void setOnPageChangeListener(ViewPager.OnPageChangeListener onPageChangeListener) {
        this.f = onPageChangeListener;
    }

    public void setOnTabClickListener(OnTabClickListener onTabClickListener) {
        this.g = onTabClickListener;
    }

    public void setSelectedIndicatorColors(int... iArr) {
        this.f5815a.a(iArr);
    }

    public void setViewPager(ViewPager viewPager) {
        this.f5815a.removeAllViews();
        this.e = viewPager;
        if (viewPager != null) {
            viewPager.setOnPageChangeListener(new InternalViewPagerListener());
            a();
        }
    }

    public SlidingTabLayout(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    /* access modifiers changed from: private */
    public void b(int i, int i2) {
        View childAt;
        int childCount = this.f5815a.getChildCount();
        if (childCount != 0 && i >= 0 && i < childCount && (childAt = this.f5815a.getChildAt(i)) != null) {
            int left = childAt.getLeft() + i2;
            if (i > 0 || i2 > 0) {
                left -= this.b;
            }
            scrollTo(left, 0);
        }
    }

    public SlidingTabLayout(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        setHorizontalScrollBarEnabled(false);
        setFillViewport(true);
        this.b = (int) (getResources().getDisplayMetrics().density * 24.0f);
        this.f5815a = new SlidingTabStrip(context);
        addView(this.f5815a, -1, -2);
    }

    public void a(int i, int i2) {
        this.c = i;
        this.d = i2;
    }

    /* access modifiers changed from: protected */
    public TextView a(Context context) {
        TextView textView = new TextView(context);
        textView.setGravity(17);
        textView.setTextSize(2, 12.0f);
        textView.setTypeface(Typeface.DEFAULT_BOLD);
        TypedValue typedValue = new TypedValue();
        getContext().getTheme().resolveAttribute(16843534, typedValue, true);
        textView.setBackgroundResource(typedValue.resourceId);
        textView.setAllCaps(true);
        int i = (int) (getResources().getDisplayMetrics().density * 16.0f);
        textView.setPadding(i, i, i, i);
        return textView;
    }

    private void a() {
        TextView textView;
        View view;
        PagerAdapter adapter = this.e.getAdapter();
        TabClickListener tabClickListener = new TabClickListener();
        int i = 0;
        while (i < adapter.getCount()) {
            if (this.c != 0) {
                view = LayoutInflater.from(getContext()).inflate(this.c, this.f5815a, false);
                textView = (TextView) view.findViewById(this.d);
            } else {
                view = null;
                textView = null;
            }
            if (view == null) {
                view = a(getContext());
            }
            if (textView == null && TextView.class.isInstance(view)) {
                textView = (TextView) view;
            }
            if (textView != null) {
                textView.setText(adapter.getPageTitle(i));
                view.setOnClickListener(tabClickListener);
                this.f5815a.addView(view);
                i++;
            } else {
                throw new IllegalArgumentException("tabTitleView == null");
            }
        }
    }
}
