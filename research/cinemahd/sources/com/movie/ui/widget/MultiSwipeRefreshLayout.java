package com.movie.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MultiSwipeRefreshLayout extends SwipeRefreshLayout {

    /* renamed from: a  reason: collision with root package name */
    private CanChildScrollUpCallback f5813a;

    public interface CanChildScrollUpCallback {
        boolean c();
    }

    public MultiSwipeRefreshLayout(Context context) {
        this(context, (AttributeSet) null);
    }

    public boolean canChildScrollUp() {
        CanChildScrollUpCallback canChildScrollUpCallback = this.f5813a;
        if (canChildScrollUpCallback != null) {
            return canChildScrollUpCallback.c();
        }
        return super.canChildScrollUp();
    }

    public void setCanChildScrollUpCallback(CanChildScrollUpCallback canChildScrollUpCallback) {
        this.f5813a = canChildScrollUpCallback;
    }

    public MultiSwipeRefreshLayout(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }
}
