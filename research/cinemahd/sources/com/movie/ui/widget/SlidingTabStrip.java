package com.movie.ui.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import com.movie.ui.widget.SlidingTabLayout;

class SlidingTabStrip extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private final int f5818a;
    private final Paint b;
    private final SimpleTabColorizer c;
    private int d;
    private float e;
    private SlidingTabLayout.TabColorizer f;

    private static class SimpleTabColorizer implements SlidingTabLayout.TabColorizer {

        /* renamed from: a  reason: collision with root package name */
        private int[] f5819a;

        private SimpleTabColorizer() {
        }

        public final int a(int i) {
            int[] iArr = this.f5819a;
            return iArr[i % iArr.length];
        }

        /* access modifiers changed from: package-private */
        public void a(int... iArr) {
            this.f5819a = iArr;
        }
    }

    SlidingTabStrip(Context context) {
        this(context, (AttributeSet) null);
    }

    private static int a(int i, int i2, float f2) {
        float f3 = 1.0f - f2;
        return Color.rgb((int) ((((float) Color.red(i)) * f2) + (((float) Color.red(i2)) * f3)), (int) ((((float) Color.green(i)) * f2) + (((float) Color.green(i2)) * f3)), (int) ((((float) Color.blue(i)) * f2) + (((float) Color.blue(i2)) * f3)));
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        int height = getHeight();
        int childCount = getChildCount();
        SlidingTabLayout.TabColorizer tabColorizer = this.f;
        if (tabColorizer == null) {
            tabColorizer = this.c;
        }
        if (childCount > 0) {
            View childAt = getChildAt(this.d);
            int left = childAt.getLeft();
            int right = childAt.getRight();
            int a2 = tabColorizer.a(this.d);
            if (this.e > 0.0f && this.d < getChildCount() - 1) {
                int a3 = tabColorizer.a(this.d + 1);
                if (a2 != a3) {
                    a2 = a(a3, a2, this.e);
                }
                View childAt2 = getChildAt(this.d + 1);
                float left2 = this.e * ((float) childAt2.getLeft());
                float f2 = this.e;
                left = (int) (left2 + ((1.0f - f2) * ((float) left)));
                right = (int) ((f2 * ((float) childAt2.getRight())) + ((1.0f - this.e) * ((float) right)));
            }
            this.b.setColor(a2);
            canvas.drawRect((float) left, (float) (height - this.f5818a), (float) right, (float) height, this.b);
        }
    }

    SlidingTabStrip(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        setWillNotDraw(false);
        float f2 = getResources().getDisplayMetrics().density;
        this.c = new SimpleTabColorizer();
        this.c.a(-13388315);
        this.f5818a = (int) (f2 * 2.0f);
        this.b = new Paint();
    }

    /* access modifiers changed from: package-private */
    public void a(SlidingTabLayout.TabColorizer tabColorizer) {
        this.f = tabColorizer;
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public void a(int... iArr) {
        this.f = null;
        this.c.a(iArr);
        invalidate();
    }

    /* access modifiers changed from: package-private */
    public void a(int i, float f2) {
        this.d = i;
        this.e = f2;
        invalidate();
    }
}
