package com.movie.ui.widget;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public final class AnimatorStateView_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private AnimatorStateView f5810a;

    public AnimatorStateView_ViewBinding(AnimatorStateView animatorStateView, View view) {
        this.f5810a = animatorStateView;
        animatorStateView.mTextView = (TextView) Utils.findRequiredViewAsType(view, R.id.message_view_text, "field 'mTextView'", TextView.class);
        animatorStateView.mImageView = (ImageView) Utils.findRequiredViewAsType(view, R.id.message_view_image, "field 'mImageView'", ImageView.class);
    }

    public void unbind() {
        AnimatorStateView animatorStateView = this.f5810a;
        if (animatorStateView != null) {
            this.f5810a = null;
            animatorStateView.mTextView = null;
            animatorStateView.mImageView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
