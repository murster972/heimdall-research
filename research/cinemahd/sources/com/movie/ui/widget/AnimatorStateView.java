package com.movie.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.yoku.marumovie.R;
import com.yoku.marumovie.R$styleable;

public final class AnimatorStateView extends LinearLayout {

    /* renamed from: a  reason: collision with root package name */
    private View f5809a;
    @BindView(2131296795)
    ImageView mImageView;
    @BindView(2131296796)
    TextView mTextView;

    private AnimatorStateView(Context context) {
        super(context, (AttributeSet) null, 0);
        a(context, (AttributeSet) null, 0);
    }

    private void a(Context context, AttributeSet attributeSet, int i) {
        this.f5809a = LayoutInflater.from(context).inflate(R.layout.widget_animator_state, this, true);
        ButterKnife.bind((Object) this, this.f5809a);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.AnimatorStateView, i, 0);
        String string = obtainStyledAttributes.getString(1);
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        this.mTextView.setText(string);
        this.mImageView.setImageDrawable(drawable);
        obtainStyledAttributes.recycle();
    }

    public void setMessageImage(Drawable drawable) {
        this.mImageView.setImageDrawable(drawable);
    }

    public void setMessageText(CharSequence charSequence) {
        this.mTextView.setText(charSequence);
    }

    public AnimatorStateView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        a(context, attributeSet, 0);
    }

    private AnimatorStateView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet, i);
    }
}
