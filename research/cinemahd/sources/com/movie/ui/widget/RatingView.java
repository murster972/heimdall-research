package com.movie.ui.widget;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.constraintlayout.widget.ConstraintLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.yoku.marumovie.R;
import com.yoku.marumovie.R$styleable;

public final class RatingView extends ConstraintLayout {
    @BindView(2131296697)
    ImageView imgIcon;
    private View q;
    private Unbinder r;
    @BindView(2131297184)
    TextView tvMaxRating;
    @BindView(2131296525)
    TextView tvRating;
    @BindView(2131297197)
    TextView tvVotes;

    public RatingView(Context context) {
        super(context);
        a(context, (AttributeSet) null, 0);
    }

    private void a(Context context, AttributeSet attributeSet, int i) {
        this.q = LayoutInflater.from(context).inflate(R.layout.rating_view, this, true);
        this.r = ButterKnife.bind((Object) this, this.q);
        context.obtainStyledAttributes(attributeSet, R$styleable.AnimatorStateView, i, 0).recycle();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        this.r.unbind();
        super.onDetachedFromWindow();
    }

    public RatingView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, 0);
        a(context, attributeSet, 0);
    }

    private RatingView(Context context, AttributeSet attributeSet, int i) {
        super(context, attributeSet, i);
        a(context, attributeSet, i);
    }

    public void a(Drawable drawable, String str, String str2, String str3) {
        this.imgIcon.setImageDrawable(drawable);
        this.tvRating.setText(str);
        TextView textView = this.tvVotes;
        textView.setText(str2 + " votes");
        TextView textView2 = this.tvMaxRating;
        textView2.setText("/" + str3);
    }
}
