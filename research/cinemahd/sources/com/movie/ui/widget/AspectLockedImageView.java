package com.movie.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import com.yoku.marumovie.R$styleable;

public final class AspectLockedImageView extends ImageView {

    /* renamed from: a  reason: collision with root package name */
    private float f5811a = 0.0f;
    private AspectRatioSource b = null;

    public interface AspectRatioSource {
        int getHeight();

        int getWidth();
    }

    private static class ViewAspectRatioSource implements AspectRatioSource {

        /* renamed from: a  reason: collision with root package name */
        private View f5812a = null;

        ViewAspectRatioSource(View view) {
            this.f5812a = view;
        }

        public int getHeight() {
            return this.f5812a.getHeight();
        }

        public int getWidth() {
            return this.f5812a.getWidth();
        }
    }

    public AspectLockedImageView(Context context) {
        super(context);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i, int i2) {
        AspectRatioSource aspectRatioSource;
        float f = this.f5811a;
        if (((double) f) == 0.0d && (aspectRatioSource = this.b) != null && aspectRatioSource.getHeight() > 0) {
            f = ((float) this.b.getWidth()) / ((float) this.b.getHeight());
        }
        if (((double) f) == 0.0d) {
            super.onMeasure(i, i2);
            return;
        }
        int size = View.MeasureSpec.getSize(i);
        int size2 = View.MeasureSpec.getSize(i2);
        if (size == 0 && size2 == 0) {
            super.onMeasure(0, 0);
            return;
        }
        int paddingLeft = getPaddingLeft() + getPaddingRight();
        int paddingTop = getPaddingTop() + getPaddingBottom();
        int i3 = size - paddingLeft;
        int i4 = size2 - paddingTop;
        if (i4 > 0) {
            float f2 = ((float) i4) * f;
            if (((float) i3) > f2) {
                i3 = (int) (((double) f2) + 0.5d);
                super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3 + paddingLeft, 1073741824), View.MeasureSpec.makeMeasureSpec(i4 + paddingTop, 1073741824));
            }
        }
        i4 = (int) (((double) (((float) i3) / f)) + 0.5d);
        super.onMeasure(View.MeasureSpec.makeMeasureSpec(i3 + paddingLeft, 1073741824), View.MeasureSpec.makeMeasureSpec(i4 + paddingTop, 1073741824));
    }

    public void setAspectRatio(float f) {
        if (((double) f) <= 0.0d) {
            throw new IllegalArgumentException("aspect ratio must be positive");
        } else if (this.f5811a != f) {
            this.f5811a = f;
            requestLayout();
        }
    }

    public void setAspectRatioSource(View view) {
        this.b = new ViewAspectRatioSource(view);
    }

    public void setAspectRatioSource(AspectRatioSource aspectRatioSource) {
        this.b = aspectRatioSource;
    }

    public AspectLockedImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, R$styleable.AspectLockedImageView);
        this.f5811a = obtainStyledAttributes.getFloat(0, 0.0f);
        obtainStyledAttributes.recycle();
    }
}
