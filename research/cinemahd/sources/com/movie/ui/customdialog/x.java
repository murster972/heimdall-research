package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class x implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5609a;

    public /* synthetic */ x(AddMagnetDialog addMagnetDialog) {
        this.f5609a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5609a.h((TorrentObject) obj);
    }
}
