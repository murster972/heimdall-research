package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5589a;

    public /* synthetic */ d(AddMagnetDialog addMagnetDialog) {
        this.f5589a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5589a.g((TorrentObject) obj);
    }
}
