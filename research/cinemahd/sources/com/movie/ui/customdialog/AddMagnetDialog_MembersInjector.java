package com.movie.ui.customdialog;

import com.database.MvDatabase;
import com.movie.data.api.realdebrid.RealDebridApi;
import dagger.MembersInjector;

public final class AddMagnetDialog_MembersInjector implements MembersInjector<AddMagnetDialog> {
    public static void a(AddMagnetDialog addMagnetDialog, RealDebridApi realDebridApi) {
        addMagnetDialog.f5565a = realDebridApi;
    }

    public static void a(AddMagnetDialog addMagnetDialog, MvDatabase mvDatabase) {
        addMagnetDialog.d = mvDatabase;
    }
}
