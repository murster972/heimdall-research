package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class p implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5601a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ p(AddMagnetDialog addMagnetDialog, TorrentObject torrentObject) {
        this.f5601a = addMagnetDialog;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5601a.a(this.b, (List) obj);
    }
}
