package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class u implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5606a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ u(AddMagnetDialog addMagnetDialog, TorrentObject torrentObject) {
        this.f5606a = addMagnetDialog;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5606a.b(this.b, obj);
    }
}
