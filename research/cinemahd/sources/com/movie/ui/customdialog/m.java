package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class m implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5598a;

    public /* synthetic */ m(AddMagnetDialog addMagnetDialog) {
        this.f5598a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5598a.f((TorrentObject) obj);
    }
}
