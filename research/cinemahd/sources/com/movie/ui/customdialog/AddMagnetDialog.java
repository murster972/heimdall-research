package com.movie.ui.customdialog;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.premiumEntitys.torrents.TorrentEntity;
import com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter;
import com.movie.FreeMoviesApp;
import com.movie.data.api.alldebrid.AllDebridApi;
import com.movie.data.api.alldebrid.AllDebridModule;
import com.movie.data.api.premiumize.PremiumizeApi;
import com.movie.data.api.premiumize.PremiumizeModule;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.TorrentObject;
import com.movie.data.model.premiumize.FolderList;
import com.movie.data.model.premiumize.ItemDetails;
import com.movie.data.model.premiumize.TransferCreate;
import com.movie.data.model.premiumize.TransferList;
import com.movie.data.model.realdebrid.RealDebridTorrentInfoObject;
import com.movie.data.model.realdebrid.UnRestrictCheckObject;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.adapter.MagnetInfoAdapter;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.fragment.premium.FilesBottomSheetFragment;
import com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.model.debrid.alldebrid.ADstatus;
import com.original.tase.model.debrid.alldebrid.Torrent.ADTorrentUpload;
import com.original.tase.utils.Regex;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import retrofit2.Response;

public class AddMagnetDialog extends DialogFragment implements MagnetInfoAdapter.MagnetInfoListener {
    @Inject

    /* renamed from: a  reason: collision with root package name */
    RealDebridApi f5565a;
    AllDebridApi b = AllDebridModule.b();
    @BindView(2131296425)
    ImageButton btnAddMagnet;
    PremiumizeApi c = PremiumizeModule.b();
    @BindView(2131296477)
    CheckBox cbAD;
    @BindView(2131296478)
    CheckBox cbPM;
    @BindView(2131296479)
    CheckBox cbRD;
    @Inject
    MvDatabase d;
    private Unbinder e;
    @BindView(2131296578)
    EditText edtAddMagnet;
    private CompositeDisposable f;
    /* access modifiers changed from: private */
    public MovieEntity g;
    private MovieInfo h;
    List<Integer> i = new ArrayList();
    @BindView(2131296702)
    ImageButton imgbtncopy;
    private MagnetInfoAdapter j;
    @BindView(2131296952)
    ProgressBar progressBar;
    @BindView(2131297002)
    RecyclerView rvMagnet;

    /* renamed from: com.movie.ui.customdialog.AddMagnetDialog$13  reason: invalid class name */
    static /* synthetic */ class AnonymousClass13 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5570a = new int[TorrentObject.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.movie.data.model.TorrentObject$Type[] r0 = com.movie.data.model.TorrentObject.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5570a = r0
                int[] r0 = f5570a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.RD     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5570a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.AD     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5570a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.PM     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.customdialog.AddMagnetDialog.AnonymousClass13.<clinit>():void");
        }
    }

    private Disposable d(final String str) {
        return Observable.create(new ObservableOnSubscribe<TorrentObject>() {
            public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                for (ADTorrentUpload.DataBean.MagnetsBean next : AddMagnetDialog.this.b.uploadMagnet(Arrays.asList(new String[]{str})).execute().body().getData().getMagnets()) {
                    AddMagnetDialog.this.a(String.valueOf(next.getId()), str, (List<String>) new ArrayList(), TorrentObject.Type.AD);
                    observableEmitter.onNext(next.convert());
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new m(this), new w(this), new b0(this));
    }

    private Disposable e(final String str) {
        return Observable.create(new ObservableOnSubscribe<TorrentObject>() {
            public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                PremiumizeApi premiumizeApi = AddMagnetDialog.this.c;
                String accessToken = PremiumizeCredentialsHelper.b().getAccessToken();
                Response<TransferCreate> execute = premiumizeApi.transferCreate(accessToken, "magnet:?xt=urn:btih:" + str).execute();
                if (execute.isSuccessful() && execute.body() != null) {
                    Response<TransferList> execute2 = AddMagnetDialog.this.c.transferlist(PremiumizeCredentialsHelper.b().getAccessToken()).execute();
                    if (execute2.isSuccessful() && execute2.body() != null) {
                        Iterator<TransferList.TransfersBean> it2 = execute2.body().getTransfers().iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            TransferList.TransfersBean next = it2.next();
                            if (next.getId().equals(execute.body().getId())) {
                                AddMagnetDialog.this.a(String.valueOf(next.getId()), str, (List<String>) new ArrayList(), TorrentObject.Type.PM);
                                observableEmitter.onNext(next.convert());
                                break;
                            }
                        }
                    }
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new d(this), new b(this), new f(this));
    }

    private Disposable f(final String str) {
        return Observable.create(new ObservableOnSubscribe<TorrentObject>() {
            public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                if (str.isEmpty()) {
                    observableEmitter.onComplete();
                    return;
                }
                RealDebridApi realDebridApi = AddMagnetDialog.this.f5565a;
                String id = realDebridApi.addMagnet("magnet:?xt=urn:btih:" + str, "").execute().body().getId();
                RealDebridTorrentInfoObject body = AddMagnetDialog.this.f5565a.torrentInfos(id).execute().body();
                AddMagnetDialog addMagnetDialog = AddMagnetDialog.this;
                if (addMagnetDialog.f5565a.selectFiles(id, TextUtils.join(",", addMagnetDialog.a(body.getFiles()))).execute().isSuccessful()) {
                    AddMagnetDialog.this.a(body.getId(), str, body.getFileIDList(), TorrentObject.Type.RD);
                    observableEmitter.onNext(body.convert());
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new x(this), new y(this), new v(this));
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        FragmentActivity activity = getActivity();
        Utils.a((Activity) activity, "add AD magnet error " + th.getMessage());
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void c(TorrentObject torrentObject, Object obj) throws Exception {
        this.j.b(torrentObject);
    }

    public /* synthetic */ void g(TorrentObject torrentObject) throws Exception {
        this.edtAddMagnet.setText("");
        this.j.b().add(torrentObject);
        this.j.notifyDataSetChanged();
    }

    public /* synthetic */ void h(TorrentObject torrentObject) throws Exception {
        this.edtAddMagnet.setText("");
        this.j.b().add(torrentObject);
        this.j.notifyDataSetChanged();
    }

    public void i() {
        this.i.clear();
        this.progressBar.setVisibility(0);
        this.f.b(Observable.create(new ObservableOnSubscribe<List<TorrentEntity>>() {
            public void subscribe(ObservableEmitter<List<TorrentEntity>> observableEmitter) throws Exception {
                MovieEntity a2 = AddMagnetDialog.this.d.m().a(AddMagnetDialog.this.g.getTmdbID(), AddMagnetDialog.this.g.getImdbIDStr(), AddMagnetDialog.this.g.getTraktID(), AddMagnetDialog.this.g.getTvdbID());
                if (a2 != null) {
                    List<TorrentEntity> a3 = AddMagnetDialog.this.d.n().a(a2.getId());
                    for (TorrentEntity b : a3) {
                        AddMagnetDialog.this.i.add(Integer.valueOf(b.b().hashCode()));
                    }
                    observableEmitter.onNext(a3);
                }
                observableEmitter.onComplete();
            }
        }).flatMap(k.f5596a).flatMap(new c(this)).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new c0(this), new l(this), new h(this)));
    }

    /* access modifiers changed from: package-private */
    public void j() {
        this.cbAD.setChecked(AllDebridCredentialsHelper.b().isValid());
        this.cbRD.setChecked(RealDebridCredentialsHelper.c().isValid());
        this.cbPM.setChecked(PremiumizeCredentialsHelper.b().isValid());
    }

    public /* synthetic */ void k(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.j.c(torrentObject);
    }

    @OnClick({2131296425})
    public void onAddMagnetBtnClick() {
        if (this.f.isDisposed()) {
            this.f = new CompositeDisposable();
        }
        String lowerCase = Regex.a(this.edtAddMagnet.getText().toString(), "magnet:\\?xt=urn:btih:([^&.]+)", 1).toLowerCase();
        if (!lowerCase.isEmpty()) {
            a(lowerCase, this.cbRD.isChecked(), this.cbAD.isChecked(), this.cbPM.isChecked());
        }
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        DaggerBaseFragmentComponent.a().a(FreeMoviesApp.a((Context) (Activity) context).d()).a().a(this);
    }

    @OnClick({2131296702})
    public void onCopyTitleToClipBoard() {
        String name = this.g.getName();
        if (this.h.getSession().intValue() > 0 && this.h.getEps().intValue() > 0) {
            name = name + String.format(" s%02de%02d", new Object[]{this.h.getSession(), this.h.getEps()});
        }
        Utils.a((Activity) getActivity(), name, false);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setStyle(1, R.style.DialogStyle90);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        View inflate = layoutInflater.inflate(R.layout.add_magnet_dialog, viewGroup, false);
        this.e = ButterKnife.bind((Object) this, inflate);
        this.f = new CompositeDisposable();
        return inflate;
    }

    public void onDestroyView() {
        this.e.unbind();
        this.f.dispose();
        super.onDestroyView();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.g = (MovieEntity) getArguments().getParcelable("movieEntity");
        this.h = (MovieInfo) getArguments().getParcelable("movieInfo");
        BaseActivity baseActivity = (BaseActivity) getActivity();
        this.j = new MagnetInfoAdapter(new ArrayList());
        this.j.a((MagnetInfoAdapter.MagnetInfoListener) this);
        this.rvMagnet.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.rvMagnet.setAdapter(this.j);
        j();
        i();
    }

    public static AddMagnetDialog a(MovieEntity movieEntity, MovieInfo movieInfo) {
        AddMagnetDialog addMagnetDialog = new AddMagnetDialog();
        Bundle bundle = new Bundle();
        bundle.putParcelable("movieEntity", movieEntity);
        bundle.putParcelable("movieInfo", movieInfo);
        addMagnetDialog.setArguments(bundle);
        return addMagnetDialog;
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void b(TorrentObject torrentObject, Object obj) throws Exception {
        this.j.b(torrentObject);
    }

    public /* synthetic */ ObservableSource c(final String str) throws Exception {
        return Observable.create(new ObservableOnSubscribe<TorrentObject.FileBean>() {
            public void subscribe(ObservableEmitter<TorrentObject.FileBean> observableEmitter) throws Exception {
                Response<UnRestrictCheckObject> execute = AddMagnetDialog.this.f5565a.unrestrictCheck(str, (String) null).execute();
                if (execute.isSuccessful() && execute.body() != null) {
                    UnRestrictCheckObject body = execute.body();
                    observableEmitter.onNext(new TorrentObject.FileBean(body.getFilename(), str, body.getFilesize(), body.getHost()));
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.a());
    }

    public /* synthetic */ void k(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), "update progress error");
    }

    public /* synthetic */ void d(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void e(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void f(TorrentObject torrentObject) throws Exception {
        this.edtAddMagnet.setText("");
        this.j.b().add(torrentObject);
        this.j.notifyDataSetChanged();
    }

    public /* synthetic */ void g() throws Exception {
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void h(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void j(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.j.c(torrentObject);
    }

    public /* synthetic */ ObservableSource b(TorrentEntity torrentEntity) throws Exception {
        return a(torrentEntity).subscribeOn(Schedulers.b());
    }

    public void d(TorrentObject torrentObject) {
        int i2 = AnonymousClass13.f5570a[torrentObject.getType().ordinal()];
        if (i2 == 1) {
            ((BaseActivity) getActivity()).showWaitingDialog("checking available links...");
            this.f.b(Observable.fromIterable(torrentObject.getListLink()).flatMap(new e(this)).toList().a(AndroidSchedulers.a()).a(new p(this, torrentObject), new g(this)));
        } else if (i2 == 2) {
            FilesBottomSheetFragment f2 = FilesBottomSheetFragment.f(torrentObject);
            f2.show(getActivity().getSupportFragmentManager(), f2.getTag());
        } else if (i2 == 3) {
            FilesBottomSheetFragment f3 = FilesBottomSheetFragment.f(torrentObject);
            f3.show(getActivity().getSupportFragmentManager(), f3.getTag());
        }
    }

    public /* synthetic */ void e(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.j.a(torrentObject);
    }

    public /* synthetic */ void g(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
        ((BaseActivity) getActivity()).hideWaitingDialog();
    }

    public /* synthetic */ void h() throws Exception {
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void j(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), "update progress error");
    }

    /* access modifiers changed from: package-private */
    public void a(String str, boolean z, boolean z2, boolean z3) {
        this.progressBar.setVisibility(0);
        if (z) {
            this.f.b(f(str));
        }
        if (z2) {
            this.f.b(d(str));
        }
        if (z3) {
            this.f.b(e(str));
        }
    }

    public /* synthetic */ void e() throws Exception {
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void f() throws Exception {
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void f(Throwable th) throws Exception {
        this.progressBar.setVisibility(8);
    }

    public /* synthetic */ void i(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.j.c(torrentObject);
    }

    /* access modifiers changed from: package-private */
    public List<RealDebridTorrentInfoObject.FilesBean> a(List<RealDebridTorrentInfoObject.FilesBean> list) {
        ArrayList arrayList = new ArrayList();
        for (RealDebridTorrentInfoObject.FilesBean next : list) {
            if (next.getBytes() > 30000000) {
                arrayList.add(next);
            }
        }
        return arrayList;
    }

    public /* synthetic */ void i(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), "update progress error");
    }

    public void a(String str, String str2, List<String> list, TorrentObject.Type type) throws Exception {
        this.g.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        this.d.m().a(this.g);
        MovieEntity a2 = this.d.m().a(this.g.getTmdbID(), this.g.getImdbIDStr(), this.g.getTraktID(), this.g.getTvdbID());
        TorrentEntity torrentEntity = new TorrentEntity();
        torrentEntity.a(list);
        torrentEntity.a(a2.getId());
        torrentEntity.a(type);
        torrentEntity.a(str2);
        String str3 = str;
        torrentEntity.b(str);
        this.d.n().b(torrentEntity);
    }

    public void a(final TorrentObject torrentObject) {
        int i2 = AnonymousClass13.f5570a[torrentObject.getType().ordinal()];
        if (i2 == 1) {
            this.f.b(Observable.create(new ObservableOnSubscribe<Object>() {
                public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                    if (AddMagnetDialog.this.f5565a.delete(torrentObject.getId()).execute().isSuccessful()) {
                        observableEmitter.onNext(new Boolean(true));
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new t(this, torrentObject), new a(this)));
        } else if (i2 == 2) {
            this.f.b(Observable.create(new ObservableOnSubscribe<Object>() {
                public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                    if (AddMagnetDialog.this.b.delete(torrentObject.getId()).execute().isSuccessful()) {
                        observableEmitter.onNext(new Boolean(true));
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new o(this, torrentObject), new r(this)));
        } else if (i2 == 3) {
            this.f.b(Observable.create(new ObservableOnSubscribe<Object>() {
                public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                    if (AddMagnetDialog.this.c.transferdelete(PremiumizeCredentialsHelper.b().getAccessToken(), torrentObject.getId()).execute().isSuccessful()) {
                        observableEmitter.onNext(new Boolean(true));
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new u(this, torrentObject), new j(this)));
        }
    }

    public /* synthetic */ void a(TorrentObject torrentObject, Object obj) throws Exception {
        this.j.b(torrentObject);
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        FragmentActivity activity = getActivity();
        Utils.a((Activity) activity, "Load data error + " + th.getMessage());
    }

    public Observable<TorrentObject> a(final TorrentEntity torrentEntity) {
        return Observable.create(new ObservableOnSubscribe<TorrentObject>() {
            public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                Long l;
                int i = AnonymousClass13.f5570a[torrentEntity.e().ordinal()];
                if (i == 1) {
                    Response<RealDebridTorrentInfoObject> execute = AddMagnetDialog.this.f5565a.torrentInfos(torrentEntity.c()).execute();
                    if (execute.isSuccessful() && execute.body() != null) {
                        TorrentObject convert = execute.body().convert();
                        convert.setTorrentEntity(torrentEntity);
                        observableEmitter.onNext(convert);
                    }
                } else if (i == 2) {
                    Response<ADstatus> execute2 = AddMagnetDialog.this.b.status(torrentEntity.b(), (String) null).execute();
                    if (execute2.isSuccessful() && execute2.body() != null && execute2.body().getData() != null && execute2.body().getData().getMagnets().size() > 0) {
                        observableEmitter.onNext(execute2.body().getData().getMagnets().get(0).convert());
                    }
                } else if (i == 3) {
                    Response<TransferList> execute3 = AddMagnetDialog.this.c.transferlist(PremiumizeCredentialsHelper.b().getAccessToken()).execute();
                    if (execute3.isSuccessful() && execute3.body() != null) {
                        for (TransferList.TransfersBean next : execute3.body().getTransfers()) {
                            if (next.getSrc().toLowerCase().contains(torrentEntity.b())) {
                                ArrayList arrayList = new ArrayList();
                                TorrentObject convert2 = next.convert();
                                Long l2 = new Long(0);
                                if (next.getFolder_id() != null) {
                                    convert2.setFolder_id(next.getFolder_id());
                                    Pair<List<TorrentObject.FileBean>, Long> a2 = AddMagnetDialog.this.a(convert2, convert2.getId(), 0);
                                    arrayList.addAll((Collection) a2.first);
                                    l = Long.valueOf(l2.longValue() + ((Long) a2.second).longValue());
                                } else {
                                    convert2.setId(next.getFile_id());
                                    ItemDetails body = AddMagnetDialog.this.c.itemDetails(PremiumizeCredentialsHelper.b().getAccessToken(), convert2.getId()).execute().body();
                                    TorrentObject.FileBean fileBean = new TorrentObject.FileBean(body.getName(), body.getLink(), body.getSize(), String.valueOf(body.getId()));
                                    fileBean.setQuality(body.getResx() + "p");
                                    arrayList.add(fileBean);
                                    l = Long.valueOf(l2.longValue() + body.getSize());
                                }
                                convert2.setId(next.getId());
                                convert2.setFiles(arrayList);
                                convert2.setSize(l.longValue());
                                convert2.setTorrentEntity(torrentEntity);
                                observableEmitter.onNext(convert2);
                            }
                        }
                    }
                }
                observableEmitter.onComplete();
            }
        });
    }

    public /* synthetic */ void a(TorrentObject torrentObject, List list) throws Exception {
        torrentObject.setFiles(list);
        FilesBottomSheetFragment f2 = FilesBottomSheetFragment.f(torrentObject);
        f2.show(getActivity().getSupportFragmentManager(), f2.getTag());
        ((BaseActivity) getActivity()).hideWaitingDialog();
    }

    public void a(final TorrentObject torrentObject, int i2) {
        int i3 = AnonymousClass13.f5570a[torrentObject.getType().ordinal()];
        if (i3 == 1) {
            this.f.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    Response<RealDebridTorrentInfoObject> execute = AddMagnetDialog.this.f5565a.torrentInfos(torrentObject.getId()).execute();
                    TorrentObject convert = execute.body().convert();
                    convert.setTorrentEntity(AddMagnetDialog.this.d.n().a(execute.body().getHash(), execute.body().getId(), TorrentTypeConverter.a(TorrentObject.Type.RD)));
                    observableEmitter.onNext(convert);
                    observableEmitter.onComplete();
                }
            }).delay((long) i2, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new z(this), new i(this)));
        } else if (i3 == 2) {
            this.f.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    ADstatus.DataBean.MagnetsBean magnetsBean = AddMagnetDialog.this.b.status(torrentObject.getHash(), (String) null).execute().body().getData().getMagnets().get(0);
                    if (magnetsBean.getHash().equals(torrentObject.getHash())) {
                        observableEmitter.onNext(magnetsBean.convert());
                    }
                    observableEmitter.onComplete();
                }
            }).delay((long) i2, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new n(this), new a0(this)));
        } else if (i3 == 3) {
            this.f.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    Long l;
                    ArrayList arrayList = new ArrayList();
                    Long l2 = new Long(0);
                    if (torrentObject.getFolder_id() != null) {
                        AddMagnetDialog addMagnetDialog = AddMagnetDialog.this;
                        TorrentObject torrentObject = torrentObject;
                        Pair<List<TorrentObject.FileBean>, Long> a2 = addMagnetDialog.a(torrentObject, torrentObject.getFolder_id(), 0);
                        arrayList.addAll((Collection) a2.first);
                        l = Long.valueOf(l2.longValue() + ((Long) a2.second).longValue());
                    } else {
                        ItemDetails body = AddMagnetDialog.this.c.itemDetails(PremiumizeCredentialsHelper.b().getAccessToken(), torrentObject.getId()).execute().body();
                        TorrentObject.FileBean fileBean = new TorrentObject.FileBean(body.getName(), body.getLink(), body.getSize(), String.valueOf(body.getId()));
                        fileBean.setQuality(body.getResx() + "p");
                        arrayList.add(fileBean);
                        l = Long.valueOf(l2.longValue() + body.getSize());
                    }
                    torrentObject.setFiles(arrayList);
                    torrentObject.setSize(l.longValue());
                    observableEmitter.onNext(torrentObject);
                    observableEmitter.onComplete();
                }
            }).delay((long) i2, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new q(this), new s(this)));
        }
    }

    /* access modifiers changed from: package-private */
    public Pair<List<TorrentObject.FileBean>, Long> a(TorrentObject torrentObject, String str, int i2) throws IOException {
        ArrayList arrayList = new ArrayList();
        Long l = new Long(0);
        for (FolderList.ContentBean next : this.c.folderList(PremiumizeCredentialsHelper.b().getAccessToken(), str, false).execute().body().getContent()) {
            TorrentObject.FileBean fileBean = new TorrentObject.FileBean(next.getName(), next.getLink(), next.getSize(), String.valueOf(next.getId()));
            fileBean.setQuality(next.getResx() + "p");
            if (next.getType().contains("folder")) {
                int i3 = i2 + 1;
                Pair<List<TorrentObject.FileBean>, Long> a2 = a(torrentObject, next.getId(), i2);
                arrayList.addAll((Collection) a2.first);
                l = Long.valueOf(l.longValue() + ((Long) a2.second).longValue());
                i2 = i3;
            } else {
                l = Long.valueOf(l.longValue() + fileBean.getSize());
                arrayList.add(fileBean);
            }
        }
        return new Pair<>(arrayList, l);
    }
}
