package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class n implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5599a;

    public /* synthetic */ n(AddMagnetDialog addMagnetDialog) {
        this.f5599a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5599a.j((TorrentObject) obj);
    }
}
