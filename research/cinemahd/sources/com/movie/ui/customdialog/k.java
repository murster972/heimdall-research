package com.movie.ui.customdialog;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class k implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ k f5596a = new k();

    private /* synthetic */ k() {
    }

    public final Object apply(Object obj) {
        return Observable.fromIterable((List) obj).subscribeOn(Schedulers.a());
    }
}
