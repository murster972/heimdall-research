package com.movie.ui.customdialog;

import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.yoku.marumovie.R;

public class AddMagnetDialog_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private AddMagnetDialog f5579a;
    private View b;
    private View c;

    public AddMagnetDialog_ViewBinding(final AddMagnetDialog addMagnetDialog, View view) {
        this.f5579a = addMagnetDialog;
        addMagnetDialog.edtAddMagnet = (EditText) Utils.findRequiredViewAsType(view, R.id.edt_add_magnet, "field 'edtAddMagnet'", EditText.class);
        View findRequiredView = Utils.findRequiredView(view, R.id.btn_add_magnet, "field 'btnAddMagnet' and method 'onAddMagnetBtnClick'");
        addMagnetDialog.btnAddMagnet = (ImageButton) Utils.castView(findRequiredView, R.id.btn_add_magnet, "field 'btnAddMagnet'", ImageButton.class);
        this.b = findRequiredView;
        findRequiredView.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                addMagnetDialog.onAddMagnetBtnClick();
            }
        });
        View findRequiredView2 = Utils.findRequiredView(view, R.id.imgbtncopy, "field 'imgbtncopy' and method 'onCopyTitleToClipBoard'");
        addMagnetDialog.imgbtncopy = (ImageButton) Utils.castView(findRequiredView2, R.id.imgbtncopy, "field 'imgbtncopy'", ImageButton.class);
        this.c = findRequiredView2;
        findRequiredView2.setOnClickListener(new DebouncingOnClickListener(this) {
            public void doClick(View view) {
                addMagnetDialog.onCopyTitleToClipBoard();
            }
        });
        addMagnetDialog.rvMagnet = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.rvMagnet, "field 'rvMagnet'", RecyclerView.class);
        addMagnetDialog.progressBar = (ProgressBar) Utils.findRequiredViewAsType(view, R.id.prgbar, "field 'progressBar'", ProgressBar.class);
        addMagnetDialog.cbRD = (CheckBox) Utils.findRequiredViewAsType(view, R.id.cbRD, "field 'cbRD'", CheckBox.class);
        addMagnetDialog.cbAD = (CheckBox) Utils.findRequiredViewAsType(view, R.id.cbAD, "field 'cbAD'", CheckBox.class);
        addMagnetDialog.cbPM = (CheckBox) Utils.findRequiredViewAsType(view, R.id.cbPM, "field 'cbPM'", CheckBox.class);
    }

    public void unbind() {
        AddMagnetDialog addMagnetDialog = this.f5579a;
        if (addMagnetDialog != null) {
            this.f5579a = null;
            addMagnetDialog.edtAddMagnet = null;
            addMagnetDialog.btnAddMagnet = null;
            addMagnetDialog.imgbtncopy = null;
            addMagnetDialog.rvMagnet = null;
            addMagnetDialog.progressBar = null;
            addMagnetDialog.cbRD = null;
            addMagnetDialog.cbAD = null;
            addMagnetDialog.cbPM = null;
            this.b.setOnClickListener((View.OnClickListener) null);
            this.b = null;
            this.c.setOnClickListener((View.OnClickListener) null);
            this.c = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
