package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5588a;

    public /* synthetic */ c0(AddMagnetDialog addMagnetDialog) {
        this.f5588a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5588a.e((TorrentObject) obj);
    }
}
