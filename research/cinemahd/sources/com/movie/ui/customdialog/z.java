package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class z implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5611a;

    public /* synthetic */ z(AddMagnetDialog addMagnetDialog) {
        this.f5611a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5611a.i((TorrentObject) obj);
    }
}
