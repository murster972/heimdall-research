package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class q implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5602a;

    public /* synthetic */ q(AddMagnetDialog addMagnetDialog) {
        this.f5602a = addMagnetDialog;
    }

    public final void accept(Object obj) {
        this.f5602a.k((TorrentObject) obj);
    }
}
