package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class t implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5605a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ t(AddMagnetDialog addMagnetDialog, TorrentObject torrentObject) {
        this.f5605a = addMagnetDialog;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5605a.c(this.b, obj);
    }
}
