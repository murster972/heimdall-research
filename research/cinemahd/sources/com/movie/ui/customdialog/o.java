package com.movie.ui.customdialog;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class o implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ AddMagnetDialog f5600a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ o(AddMagnetDialog addMagnetDialog, TorrentObject torrentObject) {
        this.f5600a = addMagnetDialog;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5600a.a(this.b, obj);
    }
}
