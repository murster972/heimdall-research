package com.movie.ui.customdialog;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.yoku.marumovie.R;

public class CustomDialog {
    public static Dialog a(final Activity activity, String str) {
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(1);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_error_load_video);
        ((TextView) dialog.findViewById(R.id.textView5)).setText(str);
        ((Button) dialog.findViewById(R.id.btn_layout_dialog_error_video)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                activity.finish();
                dialog.dismiss();
            }
        });
        return dialog;
    }
}
