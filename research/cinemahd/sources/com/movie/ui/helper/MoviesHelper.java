package com.movie.ui.helper;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.TvWatchedEpisode;
import com.movie.FreeMoviesApp;
import com.movie.data.model.cinema.Video;
import com.movie.data.repository.MoviesRepository;
import com.original.tase.api.TraktUserApi;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.utils.Utils;
import dagger.Module;
import dagger.Provides;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import timber.log.Timber;

@Module
public class MoviesHelper {

    /* renamed from: a  reason: collision with root package name */
    MvDatabase f5797a;

    /* access modifiers changed from: package-private */
    @Inject
    @Provides
    public MoviesHelper a(MoviesRepository moviesRepository, MvDatabase mvDatabase) {
        MoviesHelper moviesHelper = new MoviesHelper();
        moviesHelper.f5797a = mvDatabase;
        return moviesHelper;
    }

    public Observable<MovieEntity> b(long j, String str, long j2, long j3) {
        final long j4 = j;
        final String str2 = str;
        final long j5 = j2;
        final long j6 = j3;
        return Observable.create(new ObservableOnSubscribe<MovieEntity>() {
            public void subscribe(ObservableEmitter<MovieEntity> observableEmitter) throws Exception {
                MovieEntity a2 = MoviesHelper.this.f5797a.m().a(j4, str2, j5, j6);
                if (a2 != null) {
                    observableEmitter.onNext(a2);
                }
                observableEmitter.onComplete();
            }
        });
    }

    public Observable<MovieEntity> a(long j, String str, long j2, long j3) {
        final long j4 = j;
        final String str2 = str;
        final long j5 = j2;
        final long j6 = j3;
        return Observable.create(new ObservableOnSubscribe<MovieEntity>() {
            public void subscribe(ObservableEmitter<MovieEntity> observableEmitter) throws Exception {
                observableEmitter.onNext(MoviesHelper.this.f5797a.m().a(j4, str2, j5, j6));
                observableEmitter.onComplete();
            }
        });
    }

    public Disposable a(Activity activity, final MovieEntity movieEntity, final boolean z) {
        movieEntity.setCreatedDate(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        movieEntity.setCollected_at(z ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : null);
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            public void subscribe(ObservableEmitter<Boolean> observableEmitter) throws Exception {
                if (z) {
                    MoviesHelper.this.f5797a.m().a(movieEntity);
                } else {
                    MoviesHelper.this.f5797a.m().b(movieEntity);
                }
                if (TraktCredentialsHelper.b().isValid()) {
                    if (movieEntity.getCollected_at() != null) {
                        TraktUserApi.f().a(movieEntity);
                    } else {
                        TraktUserApi.f().b(movieEntity);
                    }
                    observableEmitter.onComplete();
                    return;
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new a(activity), new c(activity), new b(z, activity));
    }

    static /* synthetic */ void a(Activity activity, Boolean bool) throws Exception {
        if (bool.booleanValue()) {
            Utils.a(activity, "Send to trakt collections success");
        } else {
            Utils.a(activity, "Send to trakt collections failed");
        }
    }

    static /* synthetic */ void a(boolean z, Activity activity) throws Exception {
        if (z) {
            Utils.a(activity, "Saved to favorites");
        } else {
            Utils.a(activity, "Removed from favorites");
        }
    }

    public Observable<String> a(final MovieEntity movieEntity) {
        return Observable.create(new ObservableOnSubscribe<String>() {
            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                movieEntity.setWatched_at((OffsetDateTime) null);
                MoviesHelper.this.f5797a.m().b(movieEntity);
                observableEmitter.onNext("Remote from history");
                TraktUserApi.f().a(movieEntity, false);
                observableEmitter.onNext("Sent to trakt successfully");
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b());
    }

    public void a(Activity activity, Video video) {
        if (video.getSite().equals("YouTube")) {
            activity.startActivity(new Intent("android.intent.action.VIEW", Uri.parse("http://www.youtube.com/watch?v=" + video.getKey())));
            return;
        }
        Timber.b("Unsupported video format", new Object[0]);
    }

    public Observable<String> a(final MovieEntity movieEntity, boolean z) {
        int i = FreeMoviesApp.l().getInt("pref_mark_saving_percent", 1) * 60;
        if (!z && movieEntity.getPosition() < ((long) i)) {
            return Observable.just("watched time not enought to save by limit in setting");
        }
        movieEntity.setCreatedDate(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        movieEntity.setWatched_at(movieEntity.getWatched_at() != null ? movieEntity.getWatched_at() : OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        return Observable.create(new ObservableOnSubscribe<String>() {
            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                boolean z = false;
                try {
                    MoviesHelper.this.f5797a.m().a(movieEntity);
                    observableEmitter.onNext("Save to history successfully");
                } catch (Exception unused) {
                    observableEmitter.onNext("Fail to save to history");
                }
                if (TraktCredentialsHelper.b().isValid()) {
                    try {
                        TraktUserApi f = TraktUserApi.f();
                        MovieEntity movieEntity = movieEntity;
                        if (movieEntity.getWatched_at() != null) {
                            z = true;
                        }
                        f.a(movieEntity, z);
                        observableEmitter.onNext("Sent to trakt successfully");
                    } catch (Exception unused2) {
                        observableEmitter.onNext("Fail to send to trakt");
                    }
                }
                observableEmitter.onComplete();
            }
        });
    }

    public Observable<String> a(final MovieEntity movieEntity, final TvWatchedEpisode tvWatchedEpisode, final boolean z, boolean z2) {
        movieEntity.setWatched_at(z ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : null);
        int i = FreeMoviesApp.l().getInt("pref_mark_saving_percent", 1) * 60;
        if (!z2 && movieEntity.getPosition() < ((long) i)) {
            return Observable.just("watched time not enought to save by limit in setting");
        }
        movieEntity.setCreatedDate(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        return Observable.create(new ObservableOnSubscribe<String>() {
            public void subscribe(ObservableEmitter<String> observableEmitter) throws Exception {
                observableEmitter.onNext("Save episode to history");
                try {
                    MoviesHelper.this.f5797a.m().a(movieEntity);
                    MoviesHelper.this.f5797a.o().b(tvWatchedEpisode);
                    if (!z) {
                        MoviesHelper.this.f5797a.o().b(tvWatchedEpisode.g(), tvWatchedEpisode.c(), tvWatchedEpisode.h(), tvWatchedEpisode.i(), tvWatchedEpisode.e(), tvWatchedEpisode.b());
                    }
                    observableEmitter.onNext("Save episode to history successfully");
                } catch (Exception unused) {
                    observableEmitter.onNext("Fail to save show to history");
                }
                if (TraktCredentialsHelper.b().isValid()) {
                    try {
                        TraktUserApi.f().a(movieEntity, tvWatchedEpisode.e(), tvWatchedEpisode.b(), z);
                        observableEmitter.onNext("Sent to trakt successfully");
                    } catch (Exception unused2) {
                        observableEmitter.onNext("Fail to send to trakt");
                    }
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b());
    }

    public Observable<TvWatchedEpisode> a(long j, String str, long j2, long j3, int i, int i2) {
        final long j4 = j;
        final String str2 = str;
        final long j5 = j2;
        final long j6 = j3;
        final int i3 = i;
        final int i4 = i2;
        return Observable.create(new ObservableOnSubscribe<TvWatchedEpisode>() {
            public void subscribe(ObservableEmitter<TvWatchedEpisode> observableEmitter) throws Exception {
                List<TvWatchedEpisode> c2 = MoviesHelper.this.f5797a.o().c(j4, str2, j5, j6, i3, i4);
                if (c2 != null && c2.size() > 0) {
                    observableEmitter.onNext(c2.get(0));
                }
                observableEmitter.onComplete();
            }
        });
    }
}
