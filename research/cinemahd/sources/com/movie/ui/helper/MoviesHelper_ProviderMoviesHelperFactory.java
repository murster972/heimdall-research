package com.movie.ui.helper;

import com.database.MvDatabase;
import com.movie.data.repository.MoviesRepository;
import dagger.internal.Factory;
import dagger.internal.Preconditions;

public final class MoviesHelper_ProviderMoviesHelperFactory implements Factory<MoviesHelper> {
    public static MoviesHelper a(MoviesHelper moviesHelper, MoviesRepository moviesRepository, MvDatabase mvDatabase) {
        MoviesHelper a2 = moviesHelper.a(moviesRepository, mvDatabase);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }
}
