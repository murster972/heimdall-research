package com.movie.ui.helper;

import android.app.Activity;
import io.reactivex.functions.Action;

/* compiled from: lambda */
public final /* synthetic */ class b implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f5806a;
    private final /* synthetic */ Activity b;

    public /* synthetic */ b(boolean z, Activity activity) {
        this.f5806a = z;
        this.b = activity;
    }

    public final void run() {
        MoviesHelper.a(this.f5806a, this.b);
    }
}
