package com.movie.ui.helper;

import android.app.Activity;
import com.utils.Utils;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Activity f5807a;

    public /* synthetic */ c(Activity activity) {
        this.f5807a = activity;
    }

    public final void accept(Object obj) {
        Utils.a(this.f5807a, "Send to trakt collections failed");
    }
}
