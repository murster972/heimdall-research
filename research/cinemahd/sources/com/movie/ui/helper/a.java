package com.movie.ui.helper;

import android.app.Activity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Activity f5805a;

    public /* synthetic */ a(Activity activity) {
        this.f5805a = activity;
    }

    public final void accept(Object obj) {
        MoviesHelper.a(this.f5805a, (Boolean) obj);
    }
}
