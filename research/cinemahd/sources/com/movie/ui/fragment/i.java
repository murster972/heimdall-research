package com.movie.ui.fragment;

import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class i implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5685a;

    public /* synthetic */ i(ObservableEmitter observableEmitter) {
        this.f5685a = observableEmitter;
    }

    public final void accept(Object obj) {
        this.f5685a.onComplete();
    }
}
