package com.movie.ui.fragment;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.helper.MoviesHelper;
import dagger.MembersInjector;
import okhttp3.OkHttpClient;

public final class MovieFragment_MembersInjector implements MembersInjector<MovieFragment> {
    public static void a(MovieFragment movieFragment, MoviesRepository moviesRepository) {
        movieFragment.e = moviesRepository;
    }

    public static void a(MovieFragment movieFragment, MvDatabase mvDatabase) {
        movieFragment.f = mvDatabase;
    }

    public static void a(MovieFragment movieFragment, MoviesApi moviesApi) {
        movieFragment.g = moviesApi;
    }

    public static void a(MovieFragment movieFragment, TMDBApi tMDBApi) {
        movieFragment.h = tMDBApi;
    }

    public static void a(MovieFragment movieFragment, MoviesHelper moviesHelper) {
        movieFragment.i = moviesHelper;
    }

    public static void a(MovieFragment movieFragment, OkHttpClient okHttpClient) {
        movieFragment.k = okHttpClient;
    }
}
