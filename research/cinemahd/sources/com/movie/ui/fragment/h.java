package com.movie.ui.fragment;

import com.utils.ImdbSearchSuggestionModel;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class h implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BrowseMoviesFragment f5681a;
    private final /* synthetic */ ImdbSearchSuggestionModel.DBean b;
    private final /* synthetic */ int c;

    public /* synthetic */ h(BrowseMoviesFragment browseMoviesFragment, ImdbSearchSuggestionModel.DBean dBean, int i) {
        this.f5681a = browseMoviesFragment;
        this.b = dBean;
        this.c = i;
    }

    public final void subscribe(Observer observer) {
        this.f5681a.a(this.b, this.c, observer);
    }
}
