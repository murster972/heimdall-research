package com.movie.ui.fragment;

import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class r0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5771a;
    private final /* synthetic */ List b;

    public /* synthetic */ r0(MovieFragment movieFragment, List list) {
        this.f5771a = movieFragment;
        this.b = list;
    }

    public final void accept(Object obj) {
        this.f5771a.a(this.b, (Throwable) obj);
    }
}
