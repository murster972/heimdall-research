package com.movie.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindColor;
import butterknife.BindView;
import butterknife.OnClick;
import com.ads.videoreward.AdsManager;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.BaseRequestOptions;
import com.bumptech.glide.request.RequestOptions;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.facebook.react.uimanager.ViewProps;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.google.android.material.appbar.AppBarLayout;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.cinema.Video;
import com.movie.data.model.tmvdb.MovieTMDB;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.activity.MovieDetailsActivity;
import com.movie.ui.activity.SourceActivity;
import com.movie.ui.adapter.MediaSourceArrayAdapter;
import com.movie.ui.customdialog.AddMagnetDialog;
import com.movie.ui.helper.MoviesHelper;
import com.original.Constants;
import com.original.tase.I18N;
import com.original.tase.Logger;
import com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper;
import com.original.tase.helper.DateTimeHelper;
import com.original.tase.helper.GoogleVideoHelper;
import com.original.tase.helper.http.HttpHelper;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.model.media.MediaSource;
import com.original.tase.model.socket.ClientObject;
import com.original.tase.socket.Client;
import com.original.tase.utils.Regex;
import com.original.tase.utils.SourceUtils;
import com.utils.Getlink.Provider.BaseProvider;
import com.utils.Getlink.Provider.ZeroTV;
import com.utils.Getlink.Resolver.BaseResolver;
import com.utils.Getlink.Resolver.premium.PremiumResolver;
import com.utils.IntentDataContainer;
import com.utils.PermissionHelper;
import com.utils.PosterCacheHelper;
import com.utils.Subtitle.ExpandableListSubtitleAdapter;
import com.utils.Subtitle.SubtitleInfo;
import com.utils.Subtitle.SubtitlesConverter;
import com.utils.Subtitle.converter.FormatTTML;
import com.utils.Subtitle.services.SubServiceBase;
import com.utils.Utils;
import com.utils.cast.CastHelper;
import com.utils.cast.CastSubtitlesWebServer;
import com.utils.cast.LocalWebserver;
import com.utils.cast.WebServerManager;
import com.utils.download.DownloadDialog;
import com.yoku.marumovie.R;
import fi.iki.elonen.NanoHTTPD;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import javax.inject.Inject;
import javax.inject.Named;
import okhttp3.OkHttpClient;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;

public final class MovieFragment extends BaseFragment implements ObservableScrollViewCallbacks, BasePlayerHelper.OnChoosePlayListener, MediaSourceArrayAdapter.Listener {
    /* access modifiers changed from: private */
    public ExpandableListSubtitleAdapter A;
    /* access modifiers changed from: private */
    public AlertDialog B;
    private ProgressDialog C = null;
    /* access modifiers changed from: private */
    public MediaSource D = null;
    @BindView(2131296366)
    Button addWatchedListbtn;
    public MediaSourceArrayAdapter c;
    /* access modifiers changed from: private */
    public ArrayList<MediaSource> d = new ArrayList<>();
    @Inject
    MoviesRepository e;
    @Inject
    MvDatabase f;
    @Inject
    MoviesApi g;
    @Inject
    public TMDBApi h;
    @Inject
    MoviesHelper i;
    AppBarLayout j;
    @Inject
    @Named("RealDebrid")
    OkHttpClient k;
    /* access modifiers changed from: private */
    public CompositeDisposable l;
    @BindView(2131296772)
    ListView lvSources;
    private CompositeDisposable m;
    @BindColor(2131099705)
    int mColorTextWhite;
    @BindColor(2131100135)
    int mColorThemePrimary;
    @BindView(2131296569)
    Button mDownloadBtn;
    @BindView(2131296645)
    FrameLayout mNativeAdHolder;
    @BindView(2131296811)
    TextView mOverview;
    @BindView(2131296812)
    ImageView mPosterImage;
    @BindView(2131296801)
    TextView mRating;
    @BindView(2131296814)
    TextView mReleaseDate;
    @BindView(2131297168)
    Button mTrailerBtn;
    @BindView(2131296656)
    Button mViewAds;
    @BindView(2131296819)
    TextView movie_videos_header;
    private CompositeDisposable n;
    private List<Runnable> o = new ArrayList();
    /* access modifiers changed from: private */
    public MovieEntity p;
    @BindView(2131296943)
    ProgressBar progressBar;
    private List<Video> q;
    private String r = "";
    private List<Video> s = new ArrayList();
    private MenuItem t;
    boolean u = false;
    boolean v = false;
    Map<String, List<SubtitleInfo>> w = new HashMap();
    int x = 0;
    private boolean y = false;
    private ExpandableListView z;

    private void i() {
        this.m.dispose();
        ProgressBar progressBar2 = this.progressBar;
        if (progressBar2 != null) {
            progressBar2.setVisibility(8);
        }
    }

    static /* synthetic */ void i(Throwable th) throws Exception {
    }

    private void j() {
        this.m.b(this.e.videos(this.p.getTmdbID()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new h1(this), new t0(this)));
        b(new MovieInfo(this.p.getName(), (this.p.getRealeaseDate() == null || this.p.getRealeaseDate().isEmpty()) ? "" : this.p.getRealeaseDate().split("-")[0], "", "", "", this.p.getGenres()));
    }

    static /* synthetic */ boolean k(MediaSource mediaSource) throws Exception {
        if (Utils.c) {
            return !mediaSource.getQuality().toLowerCase().contains("cam");
        }
        return true;
    }

    static /* synthetic */ boolean l(MediaSource mediaSource) throws Exception {
        return BasePlayerHelper.e() != null || !mediaSource.getStreamLink().contains("video-downloads");
    }

    static /* synthetic */ boolean m(MediaSource mediaSource) throws Exception {
        if (mediaSource.getQuality().contains("4K") && mediaSource.getFileSize() < 2097152000) {
            return false;
        }
        if (!mediaSource.getQuality().contains("1080") || mediaSource.getFileSize() >= 1097152000) {
            return true;
        }
        return false;
    }

    public void a(int i2, boolean z2, boolean z3) {
    }

    public void a(ScrollState scrollState) {
    }

    public void d() {
    }

    public /* synthetic */ void g(Throwable th) throws Exception {
        this.addWatchedListbtn.setTag(Boolean.FALSE);
        this.addWatchedListbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.adding, 0, 0, 0);
        this.addWatchedListbtn.setText("WATCHED");
        g();
        Logger.a(th.getMessage());
    }

    /* renamed from: h */
    public void g() {
        this.addWatchedListbtn.setOnClickListener(new View.OnClickListener() {
            public /* synthetic */ void a(String str) throws Exception {
                Utils.a((Activity) MovieFragment.this.getActivity(), str);
            }

            public /* synthetic */ void b(String str) throws Exception {
                Utils.a((Activity) MovieFragment.this.getActivity(), str);
            }

            public void onClick(View view) {
                MovieFragment.this.p.setWatched_at(MovieFragment.this.p.getWatched_at() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : null);
                if (MovieFragment.this.p.getWatched_at() != null) {
                    CompositeDisposable c = MovieFragment.this.l;
                    MovieFragment movieFragment = MovieFragment.this;
                    c.b(movieFragment.i.a(movieFragment.p, true).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new e1(this), new d1(this), new c1(this)));
                    return;
                }
                CompositeDisposable c2 = MovieFragment.this.l;
                MovieFragment movieFragment2 = MovieFragment.this;
                c2.b(movieFragment2.i.a(movieFragment2.p).observeOn(AndroidSchedulers.a()).subscribe(new b1(this), new z0(this), new a1(this)));
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                Utils.a((Activity) MovieFragment.this.getActivity(), th.getMessage());
            }

            public /* synthetic */ void b(Throwable th) throws Exception {
                Utils.a((Activity) MovieFragment.this.getActivity(), th.getMessage());
            }

            public /* synthetic */ void a() throws Exception {
                if (MovieFragment.this.addWatchedListbtn.getTag() == null || !((Boolean) MovieFragment.this.addWatchedListbtn.getTag()).booleanValue()) {
                    MovieFragment.this.addWatchedListbtn.setTag(Boolean.TRUE);
                    MovieFragment.this.addWatchedListbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_minus, 0, 0, 0);
                    MovieFragment.this.addWatchedListbtn.setText("Remove from History");
                    return;
                }
                MovieFragment.this.addWatchedListbtn.setTag(Boolean.FALSE);
                MovieFragment.this.addWatchedListbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.adding, 0, 0, 0);
                MovieFragment.this.addWatchedListbtn.setText("WATCHED");
            }

            public /* synthetic */ void b() throws Exception {
                if (MovieFragment.this.addWatchedListbtn.getTag() == null || !((Boolean) MovieFragment.this.addWatchedListbtn.getTag()).booleanValue()) {
                    MovieFragment.this.addWatchedListbtn.setTag(Boolean.TRUE);
                    MovieFragment.this.addWatchedListbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_minus, 0, 0, 0);
                    MovieFragment.this.addWatchedListbtn.setText("Remove from History");
                    return;
                }
                MovieFragment.this.addWatchedListbtn.setTag(Boolean.FALSE);
                MovieFragment.this.addWatchedListbtn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.adding, 0, 0, 0);
                MovieFragment.this.addWatchedListbtn.setText("WATCHED");
            }
        });
    }

    /* renamed from: hideWaitingDialog */
    public void f() {
        ProgressDialog progressDialog = this.C;
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        this.l = new CompositeDisposable();
        this.m = new CompositeDisposable();
        this.n = new CompositeDisposable();
        this.l.b(this.m);
        d((MovieEntity) getArguments().getParcelable("arg_movie"));
        List<Video> list = this.q;
        if (list != null) {
            a(list, this.r);
        } else {
            j();
        }
        this.mDownloadBtn.setVisibility(0);
        AdsManager.l().a((ViewGroup) this.mNativeAdHolder);
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        Object obj;
        Object obj2;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 5) {
            if (i2 == 90) {
                if (!(intent == null || intent.getExtras() == null || (obj = intent.getExtras().get(ViewProps.POSITION)) == null)) {
                    this.p.setPosition(Long.valueOf(obj.toString()).longValue());
                    this.p.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
                    this.l.b(this.i.a(this.p, false).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new f2(this), new j1(this)));
                }
                this.v = true;
                return;
            } else if (i2 == 431) {
                if (!(intent == null || intent.getExtras() == null || (obj2 = intent.getExtras().get("extra_position")) == null)) {
                    this.p.setPosition(Long.valueOf(obj2.toString()).longValue());
                    this.p.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
                    this.l.b(this.i.a(this.p, false).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new t1(this), new s0(this)));
                }
                this.v = true;
                return;
            } else if (!(i2 == 32123 || i2 == 44454)) {
                this.v = true;
                return;
            }
        }
        this.p.setPosition(0);
        this.p.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        this.l.b(this.i.a(this.p, true).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new e2(this), new c2(this)));
        this.v = true;
    }

    public void onAttach(Activity activity) {
        super.onAttach(activity);
        setHasOptionsMenu(true);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_movie_fragment, menu);
        menu.findItem(R.id.add_rd_magnet).setVisible(RealDebridCredentialsHelper.c().isValid());
        MenuItem findItem = menu.findItem(R.id.menu_favorite);
        if (this.p.getCollected_at() == null) {
            findItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_border));
        } else {
            findItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_full));
        }
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_movie, viewGroup, false);
    }

    public void onDestroyView() {
        this.l.a();
        this.l.dispose();
        this.n.dispose();
        this.u = false;
        Utils.C();
        if (this.k != null && RealDebridCredentialsHelper.c().isValid()) {
            Utils.a(this.k);
        }
        OkHttpClient okHttpClient = ZeroTV.e;
        if (okHttpClient != null) {
            Utils.a(okHttpClient);
        }
        super.onDestroyView();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        boolean z2 = false;
        if (menuItem.getItemId() == R.id.add_rd_magnet) {
            this.m.dispose();
            HttpHelper.e().c();
            String str = (this.p.getRealeaseDate() == null || this.p.getRealeaseDate().isEmpty()) ? "" : this.p.getRealeaseDate().split("-")[0];
            MovieEntity movieEntity = this.p;
            AddMagnetDialog a2 = AddMagnetDialog.a(movieEntity, new MovieInfo(movieEntity.getName(), str, "", "", "", this.p.getGenres()));
            FragmentTransaction b = getActivity().getSupportFragmentManager().b();
            Fragment b2 = getActivity().getSupportFragmentManager().b("fragment_add_magnet");
            if (b2 != null) {
                b.a(b2);
            }
            b.a((String) null);
            a2.show(b, "fragment_add_magnet");
        }
        if (menuItem.getItemId() != R.id.menu_favorite) {
            return super.onOptionsItemSelected(menuItem);
        }
        if (this.p.getCollected_at() == null) {
            this.p.setCollected_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        } else {
            this.p.setCollected_at((OffsetDateTime) null);
        }
        MoviesHelper moviesHelper = this.i;
        FragmentActivity activity = getActivity();
        MovieEntity movieEntity2 = this.p;
        if (movieEntity2.getCollected_at() != null) {
            z2 = true;
        }
        moviesHelper.a(activity, movieEntity2, z2);
        if (this.p.getCollected_at() == null) {
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_border));
        } else {
            menuItem.setIcon(getResources().getDrawable(R.drawable.ic_favorite_full));
        }
        return true;
    }

    /* access modifiers changed from: package-private */
    @OnClick({2131296811})
    public void onOverviewClick() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.a((CharSequence) this.p.getOverview());
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c();
    }

    public void onPause() {
        i();
        super.onPause();
    }

    public void onResume() {
        if (this.v) {
            AdsManager.l().j();
            this.v = false;
        }
        if (GlobalVariable.c().a().getAds() != null) {
            this.mViewAds.setVisibility(0);
            this.mViewAds.setText("Watch Video");
        } else {
            this.mViewAds.setVisibility(8);
        }
        this.l.b(this.i.a(this.p.getTmdbID(), this.p.getImdbIDStr(), this.p.getTraktID(), this.p.getTvdbID()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new b2(this), new g1(this), new m1(this)));
        super.onResume();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
    }

    public void onStart() {
        super.onStart();
    }

    public void onStop() {
        super.onStop();
    }

    public void onViewCreated(final View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(this) {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT > 16) {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        this.lvSources.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long j) {
                MediaSource mediaSource = (MediaSource) MovieFragment.this.d.get(i);
                mediaSource.setPlayed(true);
                Collections.sort(MovieFragment.this.d);
                BasePlayerHelper.a((Activity) MovieFragment.this.getActivity(), mediaSource, (BasePlayerHelper.OnChoosePlayListener) MovieFragment.this);
                if (!MovieFragment.this.u) {
                    MovieInfo movieInfo = new MovieInfo("", "", "-1", "-1", "");
                    movieInfo.tmdbID = MovieFragment.this.p.getTmdbID();
                    CompositeDisposable c = MovieFragment.this.l;
                    MovieFragment movieFragment = MovieFragment.this;
                    FreeMoviesApp.a(c, movieFragment.g, movieInfo, movieFragment.d);
                    MovieFragment.this.u = true;
                }
            }
        });
        this.lvSources.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public /* synthetic */ void a(int i, AlertDialog.Builder builder, MediaSource mediaSource) throws Exception {
                ((MediaSource) MovieFragment.this.d.get(i)).setResolved(mediaSource.isResolved());
                ((MediaSource) MovieFragment.this.d.get(i)).setStreamLink(mediaSource.getStreamLink());
                String filename = ((MediaSource) MovieFragment.this.d.get(i)).getFilename();
                if (filename == null) {
                    filename = URLUtil.guessFileName(((MediaSource) MovieFragment.this.d.get(i)).getStreamLink(), (String) null, (String) null);
                }
                builder.a((CharSequence) filename);
                builder.b((CharSequence) "Ok", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.c();
                MovieFragment.this.f();
            }

            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long j) {
                AlertDialog.Builder builder = new AlertDialog.Builder(MovieFragment.this.getActivity());
                builder.b((CharSequence) "File Name :");
                MediaSource mediaSource = (MediaSource) MovieFragment.this.d.get(i);
                if (!mediaSource.isResolved()) {
                    MovieFragment.this.showWaitingDialog("Resolving file name...");
                    MovieFragment.this.l.b(PremiumResolver.c(mediaSource).observeOn(AndroidSchedulers.a()).subscribe(new v0(this, i, builder), new w0(this)));
                    return true;
                }
                String filename = mediaSource.getFilename();
                if (filename == null) {
                    filename = URLUtil.guessFileName(mediaSource.getStreamLink(), (String) null, (String) null);
                }
                builder.a((CharSequence) filename);
                builder.b((CharSequence) "Ok", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                builder.c();
                return true;
            }

            public /* synthetic */ void a(Throwable th) throws Exception {
                MovieFragment.this.f();
                Utils.c(MovieFragment.this.getActivity(), th.getMessage());
            }
        });
        this.c = new MediaSourceArrayAdapter(getActivity(), R.layout.item_source, this.d);
        this.c.a((MediaSourceArrayAdapter.Listener) this);
        this.lvSources.setAdapter(this.c);
        this.lvSources.setNestedScrollingEnabled(true);
        this.lvSources.setScrollContainer(false);
        this.lvSources.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View view, boolean z) {
                MovieFragment.this.j.a(!z, true);
            }
        });
    }

    public void showWaitingDialog(String str) {
        if (this.C == null) {
            this.C = new ProgressDialog(getActivity());
            try {
                this.C.show();
            } catch (WindowManager.BadTokenException unused) {
            }
        }
        this.C.setCancelable(true);
        this.C.getWindow().setBackgroundDrawableResource(R.color.transparent);
        this.C.setContentView(R.layout.progressbar);
        TextView textView = (TextView) this.C.findViewById(R.id.tv_title);
        if (!str.isEmpty()) {
            textView.setText(str);
            textView.setVisibility(0);
        } else {
            textView.setVisibility(8);
        }
        this.C.show();
    }

    public static MovieFragment c(MovieEntity movieEntity) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("arg_movie", movieEntity);
        MovieFragment movieFragment = new MovieFragment();
        movieFragment.setArguments(bundle);
        return movieFragment;
    }

    @SuppressLint({"StringFormatMatches"})
    private void d(MovieEntity movieEntity) {
        this.p = movieEntity;
        this.mRating.setText(getString(R.string.movie_details_rating, movieEntity.getVote()));
        this.mReleaseDate.setText(Utils.e(movieEntity.getRealeaseDate()));
        if (movieEntity.getOverview().length() <= 197 || !this.mOverview.isFocusable()) {
            this.mOverview.setText(movieEntity.getOverview());
        } else {
            TextView textView = this.mOverview;
            textView.setText(movieEntity.getOverview().substring(0, 197) + "...");
        }
        if (this.mPosterImage == null) {
            return;
        }
        if (movieEntity.getPoster_path() == null || movieEntity.getPoster_path().isEmpty()) {
            String c2 = PosterCacheHelper.a().c(this.p.getTmdbID(), this.p.getTvdbID(), this.p.getImdbIDStr());
            if (c2 == null) {
                this.h.getMovieDetails(movieEntity.getTmdbID(), (String) null).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new z1(this), o0.f5706a);
            } else {
                Glide.a((Fragment) this).a(c2).a((BaseRequestOptions<?>) new RequestOptions().b()).a(new DrawableTransitionOptions().b()).a(this.mPosterImage);
            }
        } else {
            Glide.a((Fragment) this).a(movieEntity.getPoster_path()).a((BaseRequestOptions<?>) new RequestOptions().b()).a(new DrawableTransitionOptions().b()).a(this.mPosterImage);
        }
    }

    public void a(AppBarLayout appBarLayout) {
        this.j = appBarLayout;
    }

    public /* synthetic */ void b(MovieEntity movieEntity) throws Exception {
        this.addWatchedListbtn.setTag(Boolean.valueOf(movieEntity.getWatched_at() != null));
        this.addWatchedListbtn.setCompoundDrawablesWithIntrinsicBounds(movieEntity.getWatched_at() != null ? R.drawable.ic_minus : R.drawable.adding, 0, 0, 0);
        this.addWatchedListbtn.setText(movieEntity.getWatched_at() != null ? "Remove from History" : "WATCHED");
    }

    public void e(MediaSource mediaSource) {
        MovieInfo movieInfo = new MovieInfo(this.p.getName(), this.p.getRealeaseDate().isEmpty() ? "" : this.p.getRealeaseDate().split("-")[0], "", "", "");
        mediaSource.setMovieName(this.p.getName() + "(" + movieInfo.getYear() + ")");
        movieInfo.tempStreamLink = mediaSource.getStreamLink();
        movieInfo.extension = mediaSource.getExtension();
        movieInfo.cinemaID = this.p.getTmdbID();
        movieInfo.fileSizeString = mediaSource.getFileSizeString();
        movieInfo.tmdbID = this.p.getTmdbID();
        movieInfo.imdbIDStr = this.p.getImdbIDStr();
        try {
            DownloadDialog.a(mediaSource, movieInfo, this.p.getTmdbID()).show(getActivity().getSupportFragmentManager(), "downloadDialog");
        } catch (Exception e2) {
            Utils.a((Activity) getActivity(), (int) R.string.could_not_setup_download_menu);
            e2.printStackTrace();
        }
    }

    public void f(MediaSource mediaSource) {
        this.m.b(BaseResolver.b(mediaSource).subscribeOn(Schedulers.b()).flatMap(n1.f5704a).observeOn(AndroidSchedulers.a()).subscribe(new u0(this)));
    }

    private void k() {
        if (this.t != null) {
            for (Runnable run : this.o) {
                run.run();
            }
            this.o.clear();
        }
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ void h(Throwable th) throws Exception {
        c(th.getMessage());
        f();
    }

    public /* synthetic */ void a(MovieTMDB.ResultsBean resultsBean) throws Exception {
        Glide.a((Fragment) this).a(resultsBean.getPoster_path()).a((BaseRequestOptions<?>) new RequestOptions().b()).a(new DrawableTransitionOptions().b()).a(this.mPosterImage);
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        a((List<Video>) null, "");
    }

    public void b(MovieInfo movieInfo) {
        movieInfo.tmdbID = this.p.getTmdbID();
        ProgressBar progressBar2 = this.progressBar;
        if (progressBar2 != null) {
            progressBar2.setVisibility(0);
        }
        this.x = 0;
        Utils.b = FreeMoviesApp.l().getBoolean("pref_show_hd_only", false);
        Utils.c = FreeMoviesApp.l().getBoolean("pref_filter_cam", false);
        Utils.d = FreeMoviesApp.l().getBoolean("pref_show_debrid_only", false);
        Utils.d();
        List<BaseProvider> h2 = Utils.h();
        for (BaseProvider next : h2) {
            if (next != null) {
                this.m.b(next.a(movieInfo).subscribeOn(Schedulers.b()).filter(f1.f5675a).flatMap(w1.f5787a).flatMap(k1.f5695a).map(new Function<MediaSource, MediaSource>(this) {
                    public MediaSource a(MediaSource mediaSource) throws Exception {
                        b(mediaSource);
                        return mediaSource;
                    }

                    public /* bridge */ /* synthetic */ Object apply(Object obj) throws Exception {
                        MediaSource mediaSource = (MediaSource) obj;
                        a(mediaSource);
                        return mediaSource;
                    }

                    public MediaSource b(MediaSource mediaSource) {
                        String str;
                        String str2;
                        try {
                            String lowerCase = mediaSource.getStreamLink().trim().toLowerCase();
                            HashMap<String, String> hashMap = new HashMap<>();
                            String str3 = "";
                            if (!Regex.a(lowerCase, "//[^/]*(ntcdn|micetop)\\.[^/]{2,8}/", 1).isEmpty()) {
                                HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                                if (playHeader != null) {
                                    hashMap = playHeader;
                                }
                                if (hashMap.containsKey("Referer")) {
                                    str2 = hashMap.get("Referer");
                                } else {
                                    str2 = hashMap.containsKey("referer") ? hashMap.get("referer") : str3;
                                }
                                if (str2 != null) {
                                    str3 = str2.toLowerCase();
                                }
                                if (lowerCase.contains("vidcdn_pro/") && !str3.contains("vidnode.net")) {
                                    hashMap.put("Referer", "https://vidnode.net/");
                                } else if (lowerCase.contains("s7_ntcdn_us/") && !str3.contains("m4ufree.info")) {
                                    hashMap.put("Referer", "http://m4ufree.info/");
                                }
                                if (!hashMap.containsKey("User-Agent") && !hashMap.containsKey("user-agent")) {
                                    hashMap.put("User-Agent", Constants.f5838a);
                                }
                                mediaSource.setPlayHeader(hashMap);
                            } else if (Regex.a(lowerCase, "//[^/]*(vidcdn)\\.pro/stream/", 1).isEmpty() || lowerCase.contains(".m3u8")) {
                                if (mediaSource.getPlayHeader() == null) {
                                    mediaSource.setPlayHeader(new HashMap());
                                }
                                if (!mediaSource.getPlayHeader().containsKey("User-Agent")) {
                                    mediaSource.getPlayHeader().put("User-Agent", Constants.f5838a);
                                }
                            } else {
                                HashMap<String, String> playHeader2 = mediaSource.getPlayHeader();
                                if (playHeader2.containsKey("Referer")) {
                                    str = playHeader2.get("Referer");
                                } else {
                                    str = playHeader2.containsKey("referer") ? playHeader2.get("referer") : str3;
                                }
                                if (str != null) {
                                    str3 = str.toLowerCase();
                                }
                                if (!str3.contains("vidnode.net")) {
                                    playHeader2.put("Referer", "https://vidnode.net/");
                                }
                                if (!playHeader2.containsKey("User-Agent") && !playHeader2.containsKey("user-agent")) {
                                    playHeader2.put("User-Agent", Constants.f5838a);
                                }
                                mediaSource.setPlayHeader(playHeader2);
                            }
                        } catch (Throwable th) {
                            Logger.a(th, new boolean[0]);
                        }
                        return mediaSource;
                    }
                }).filter(o1.f5707a).filter(y0.f5792a).filter(p1.f5710a).filter(v1.f5784a).observeOn(AndroidSchedulers.a()).subscribe(new r1(this), new r0(this, h2), new x0(this, h2)));
            }
        }
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void a(Video.Response response) throws Exception {
        a(response.videos, response.linkID);
    }

    public /* synthetic */ void f(Throwable th) throws Exception {
        f();
        Utils.a((Activity) getActivity(), th.getMessage());
        Utils.c(getActivity(), th.getMessage());
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x005b A[EDGE_INSN: B:13:0x005b->B:10:0x005b ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:5:0x0026  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(java.util.List<com.movie.data.model.cinema.Video> r3, java.lang.String r4) {
        /*
            r2 = this;
            r2.q = r3
            r2.r = r4
            android.widget.Button r3 = r2.mViewAds
            com.movie.ui.fragment.d2 r4 = com.movie.ui.fragment.d2.f5668a
            r3.setOnClickListener(r4)
            androidx.fragment.app.FragmentActivity r3 = r2.getActivity()
            android.view.LayoutInflater.from(r3)
            java.util.List<com.movie.data.model.cinema.Video> r3 = r2.q
            boolean r3 = com.utils.Lists.a(r3)
            if (r3 != 0) goto L_0x005b
            java.util.List<com.movie.data.model.cinema.Video> r3 = r2.q
            java.util.Iterator r3 = r3.iterator()
        L_0x0020:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x005b
            java.lang.Object r4 = r3.next()
            com.movie.data.model.cinema.Video r4 = (com.movie.data.model.cinema.Video) r4
            java.lang.String r0 = r4.getType()
            java.lang.String r1 = "Trailer"
            boolean r0 = r0.equals(r1)
            if (r0 != 0) goto L_0x0044
            java.lang.String r0 = r4.getType()
            java.lang.String r1 = "YouTube"
            boolean r0 = r0.equals(r1)
            if (r0 == 0) goto L_0x0020
        L_0x0044:
            r3 = 0
            java.lang.Object[] r3 = new java.lang.Object[r3]
            java.lang.String r0 = "Found trailer!"
            timber.log.Timber.a(r0, r3)
            android.widget.Button r3 = r2.mTrailerBtn
            r3.setTag(r4)
            android.widget.Button r3 = r2.mTrailerBtn
            com.movie.ui.fragment.g2 r4 = new com.movie.ui.fragment.g2
            r4.<init>(r2)
            r3.setOnClickListener(r4)
        L_0x005b:
            java.util.List<com.movie.data.model.cinema.Video> r3 = r2.s
            r2.a((java.util.List<com.movie.data.model.cinema.Video>) r3)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.fragment.MovieFragment.a(java.util.List, java.lang.String):void");
    }

    static /* synthetic */ boolean j(MediaSource mediaSource) throws Exception {
        boolean z2 = mediaSource.isTorrent() || mediaSource.getFileSize() > 0 || mediaSource.isHLS();
        if (!Utils.b) {
            return z2;
        }
        if (!mediaSource.isHD() || !z2) {
            return false;
        }
        return true;
    }

    public /* synthetic */ void f(String str) throws Exception {
        Utils.a((Activity) getActivity(), str);
    }

    public boolean e() {
        return ((MovieDetailsActivity) getActivity()).c();
    }

    public /* synthetic */ void e(String str) throws Exception {
        Utils.a((Activity) getActivity(), str);
    }

    public /* synthetic */ void e(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void a(View view) {
        this.i.a((Activity) getActivity(), (Video) view.getTag());
    }

    public void a(List<Video> list) {
        if (list != null) {
            for (Video next : list) {
                String site = next.getSite();
                if (!site.isEmpty()) {
                    if (GoogleVideoHelper.k(site)) {
                        MediaSource mediaSource = new MediaSource("Cinema", "GoogleVideo Video", false);
                        mediaSource.setStreamLink(next.getSite());
                        mediaSource.setQuality(next.getSize() + "p");
                        f(mediaSource);
                    } else {
                        MediaSource mediaSource2 = new MediaSource("Server Crawler", "OpenLoad", true);
                        mediaSource2.setQuality("");
                        mediaSource2.setStreamLink(next.getSite());
                        f(mediaSource2);
                    }
                }
            }
        }
    }

    /* renamed from: d */
    public void c(MediaSource mediaSource) {
        Logger.a("onAddMediaSouce", mediaSource.toStringAllObjs());
        if (!this.y) {
            this.mDownloadBtn.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    Collections.sort(MovieFragment.this.d);
                    for (int i = 0; i < MovieFragment.this.d.size(); i++) {
                        if (!((MediaSource) MovieFragment.this.d.get(i)).isHLS()) {
                            MovieFragment.this.e((MediaSource) MovieFragment.this.d.get(0));
                            return;
                        }
                    }
                }
            });
            this.y = true;
        }
        try {
            this.d.add(mediaSource);
            b(true);
            this.c.a(mediaSource);
            this.c.a();
            this.c.notifyDataSetChanged();
            TextView textView = this.movie_videos_header;
            textView.setText("Streams: (" + this.d.size() + " found)");
            if (this.d.size() > Utils.f6550a) {
                i();
            }
        } catch (Throwable th) {
            Logger.a(th, new boolean[0]);
        }
    }

    public /* synthetic */ void b(MediaSource mediaSource) throws Exception {
        if (!Utils.d) {
            c(mediaSource);
        } else if (mediaSource.isDebrid()) {
            c(mediaSource);
        }
    }

    public /* synthetic */ void a(List list, Throwable th) throws Exception {
        this.x++;
        if (this.x >= list.size()) {
            i();
        }
    }

    public /* synthetic */ void b(List list) throws Exception {
        this.x++;
        if (this.x >= list.size()) {
            i();
        }
    }

    public /* synthetic */ void a(boolean z2) {
        this.t.setVisible(z2);
    }

    private void b(boolean z2) {
        this.o.add(new q1(this, z2));
        k();
    }

    public void a(int i2, MediaSource mediaSource) {
        if (mediaSource.isResolved() || mediaSource.isRawTorrent()) {
            b(i2, mediaSource);
            return;
        }
        showWaitingDialog("Stream loading...");
        this.l.b(PremiumResolver.c(mediaSource).observeOn(AndroidSchedulers.a()).subscribe(new s1(this, mediaSource, i2), new u1(this), new x1(this)));
    }

    public /* synthetic */ void d(String str) throws Exception {
        Utils.a((Activity) getActivity(), str);
    }

    public /* synthetic */ void d(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    private void b(int i2, MediaSource mediaSource) {
        this.l.b(this.i.b(this.p.getTmdbID(), this.p.getImdbIDStr(), this.p.getTraktID(), this.p.getTvdbID()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new l1(this), new y1(this), new n0(this, mediaSource, i2)));
    }

    public /* synthetic */ void a(MediaSource mediaSource, int i2, MediaSource mediaSource2) throws Exception {
        f();
        mediaSource.setStreamLink(mediaSource2.getStreamLink());
        mediaSource.setResolved(mediaSource2.isResolved());
        b(i2, mediaSource);
    }

    public /* synthetic */ void a(MovieEntity movieEntity) throws Exception {
        this.p.setPosition(movieEntity.getPosition());
        this.p.setSubtitlepath(movieEntity.getSubtitlepath());
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        this.p.setPosition(0);
        this.p.setSubtitlepath("");
    }

    public /* synthetic */ void a(MediaSource mediaSource, int i2) throws Exception {
        MediaSource mediaSource2 = mediaSource;
        this.D = mediaSource2;
        this.D.setMovieName(this.p.getName());
        BasePlayerHelper e2 = BasePlayerHelper.e();
        boolean isRawTorrent = mediaSource.isRawTorrent();
        String str = "";
        switch (i2) {
            case 0:
                if (e2 == null) {
                    a(this.D, (List<String>) null, (List<String>) null);
                    return;
                } else if (!isRawTorrent || e2.a() == 32123) {
                    e2.a(getActivity(), this, this.D, this.p.getName() + " " + this.D.toString(), this.p.getPosition());
                    return;
                } else {
                    Utils.a((Activity) getActivity(), "You need CleafPlayer to play torrent");
                    return;
                }
            case 1:
                if (e2 == null) {
                    c("Please choose external player in setting first.");
                    return;
                } else if (!isRawTorrent || e2.a() == 32123) {
                    if (!this.p.getRealeaseDate().isEmpty()) {
                        str = this.p.getRealeaseDate().split("-")[0];
                    }
                    a(new MovieInfo(this.p.getName(), str, "", "", ""));
                    showWaitingDialog("subtitle loading..");
                    return;
                } else {
                    Utils.a((Activity) getActivity(), "You need CleafPlayer to play torrent");
                    return;
                }
            case 2:
                Intent intent = new Intent("android.intent.action.VIEW");
                Uri parse = Uri.parse(mediaSource.getStreamLink());
                HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                if (playHeader != null && playHeader.size() > 0) {
                    HashMap<String, String> a2 = SourceUtils.a(playHeader);
                    ArrayList arrayList = new ArrayList();
                    for (Map.Entry next : a2.entrySet()) {
                        arrayList.add(next.getKey());
                        arrayList.add(next.getValue());
                    }
                    intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
                }
                String g2 = mediaSource.isHLS() ? "application/x-mpegURL" : Utils.g(mediaSource.getStreamLink());
                if (!this.p.getRealeaseDate().isEmpty()) {
                    str = this.p.getRealeaseDate().split("-")[0];
                }
                intent.putExtra("title", (this.p.getName() + " (" + str + ")") + g2);
                if (mediaSource.isRawTorrent()) {
                    intent.setData(parse);
                } else if (Build.VERSION.SDK_INT >= 16) {
                    intent.setDataAndTypeAndNormalize(parse, "video/*");
                } else {
                    intent.setDataAndType(parse, "video/*");
                }
                startActivityForResult(Intent.createChooser(intent, "Open with..."), 44454);
                return;
            case 3:
                e(mediaSource);
                return;
            case 4:
                ((ClipboardManager) getActivity().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("Source Text", mediaSource.getStreamLink()));
                Utils.a((Activity) getActivity(), "copied");
                return;
            case 5:
                a(mediaSource2, (List<String>) null, (List<String>) null);
                return;
            case 6:
                if (!this.p.getRealeaseDate().isEmpty()) {
                    str = this.p.getRealeaseDate().split("-")[0];
                }
                a(new MovieInfo(this.p.getName(), str, "", "", ""));
                showWaitingDialog("subtitle loading..");
                return;
            case 7:
                Client.getIntance().senddata(new ClientObject(e2 == null ? "CINEMA" : e2.b(), mediaSource.getStreamLink(), mediaSource.isHLS(), this.p.getName(), -1.0d, mediaSource.getOriginalLink(), Constants.f5838a, !mediaSource.getPlayHeader().isEmpty()).toString(), getActivity());
                return;
            default:
                return;
        }
    }

    public void a(MovieInfo movieInfo) {
        movieInfo.tmdbID = this.p.getTmdbID();
        movieInfo.imdbIDStr = this.p.getImdbIDStr();
        movieInfo.cinemaID = this.p.getTmdbID();
        this.n.b(SubServiceBase.b(movieInfo).observeOn(AndroidSchedulers.a()).subscribe(new a2(this, movieInfo), new i1(this)));
    }

    public /* synthetic */ void a(final MovieInfo movieInfo, ArrayList arrayList) throws Exception {
        if (this.n.isDisposed() || arrayList.size() > 0) {
            Iterator it2 = arrayList.iterator();
            while (it2.hasNext()) {
                SubtitleInfo subtitleInfo = (SubtitleInfo) it2.next();
                List list = this.w.get(subtitleInfo.c);
                if (list == null) {
                    list = new ArrayList();
                    this.w.put(subtitleInfo.c, list);
                }
                list.add(subtitleInfo);
            }
            if (this.B == null) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                View inflate = getActivity().getLayoutInflater().inflate(R.layout.dialog_listsubtitle, (ViewGroup) null);
                builder.b(inflate);
                builder.b((int) R.string.close_msg, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                });
                this.B = builder.a();
                this.z = (ExpandableListView) inflate.findViewById(R.id.subtitle_expand_view);
                this.z.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                    public /* synthetic */ void a(MovieInfo movieInfo, List list) throws Exception {
                        ArrayList arrayList = new ArrayList();
                        ArrayList arrayList2 = new ArrayList();
                        Iterator it2 = list.iterator();
                        while (it2.hasNext()) {
                            File file = (File) it2.next();
                            arrayList.add(file.getAbsolutePath());
                            arrayList2.add(file.getName());
                        }
                        if (CastHelper.a(MovieFragment.this.getContext())) {
                            HashMap hashMap = new HashMap();
                            ArrayList arrayList3 = new ArrayList();
                            for (int i = 0; i < arrayList.size(); i++) {
                                String str = "sub-" + i + "-" + DateTimeHelper.b() + "-" + new Random().nextInt(99999) + ".ttml";
                                String a2 = SubtitlesConverter.a((String) arrayList.get(i), new FormatTTML());
                                if (a2 != null) {
                                    hashMap.put(str, a2);
                                    arrayList3.add(arrayList.get(i));
                                }
                            }
                            if (WebServerManager.d().b() == null) {
                                WebServerManager.d().a((NanoHTTPD) new CastSubtitlesWebServer(34507));
                            }
                            WebServerManager.d().a((Map<String, String>) hashMap);
                            Intent intent = new Intent(MovieFragment.this.getActivity(), LocalWebserver.class);
                            if (hashMap.size() > 0) {
                                Bundle bundle = new Bundle();
                                intent.putExtras(bundle);
                                bundle.putBoolean("isNeededToRefreshTracks", true);
                                bundle.putInt("videoAndSubTrackIdArray", 1);
                            }
                            MovieFragment.this.getActivity().startService(intent);
                            MovieFragment movieFragment = MovieFragment.this;
                            movieFragment.a(movieFragment.D, (List<String>) arrayList3, (List<String>) new LinkedList(hashMap.keySet()));
                        } else {
                            BasePlayerHelper e = BasePlayerHelper.e();
                            FragmentActivity activity = MovieFragment.this.getActivity();
                            MovieFragment movieFragment2 = MovieFragment.this;
                            e.a(activity, movieFragment2, movieFragment2.D, movieInfo.name, MovieFragment.this.p.getPosition(), arrayList, arrayList2);
                        }
                        MovieFragment.this.f();
                    }

                    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i2, long j) {
                        SubtitleInfo subtitleInfo = (SubtitleInfo) MovieFragment.this.A.getChild(i, i2);
                        MovieFragment.this.B.dismiss();
                        if (!PermissionHelper.b(MovieFragment.this.getActivity(), 777)) {
                            return false;
                        }
                        MovieFragment.this.showWaitingDialog("");
                        SubServiceBase.a(MovieFragment.this.getActivity(), subtitleInfo.b, movieInfo.getNameAndYear()).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new p0(this, movieInfo), new q0(this));
                        return true;
                    }

                    public /* synthetic */ void a(Throwable th) throws Exception {
                        MovieFragment.this.f();
                    }
                });
            }
            this.A = new ExpandableListSubtitleAdapter(getActivity(), this.w);
            this.z.setAdapter(this.A);
            if (this.w.keySet().size() == 1) {
                this.z.expandGroup(0);
            }
            this.B.show();
            f();
            return;
        }
        throw new Exception("No subtitles found");
    }

    public void a(MediaSource mediaSource) {
        MovieInfo movieInfo = new MovieInfo(this.p.getName(), this.p.getRealeaseDate().isEmpty() ? "" : this.p.getRealeaseDate().split("-")[0], "", "", "");
        mediaSource.setMovieName(this.p.getName() + "(" + movieInfo.getYear() + ")");
        movieInfo.tempStreamLink = mediaSource.getStreamLink();
        movieInfo.extension = mediaSource.getExtension();
        movieInfo.cinemaID = this.p.getTmdbID();
        movieInfo.fileSizeString = mediaSource.getFileSizeString();
        if (mediaSource.getFileSize() > Utils.m() - 100000) {
            Utils.a((Activity) getActivity(), "No space left on device!!");
            return;
        }
        try {
            DownloadDialog.a(mediaSource, movieInfo, this.p.getTmdbID()).show(getActivity().getSupportFragmentManager(), "downloadDialog");
        } catch (Exception unused) {
            Utils.a((Activity) getActivity(), (int) R.string.could_not_setup_download_menu);
        }
    }

    public void a(MediaSource mediaSource, List<String> list, List<String> list2) {
        MovieInfo movieInfo = new MovieInfo(this.p.getName(), this.p.getRealeaseDate().isEmpty() ? "" : this.p.getRealeaseDate().split("-")[0], "", "", "");
        if (this.p.getPosition() <= 0 || BasePlayerHelper.e() != null) {
            a(mediaSource, false, movieInfo, list, list2);
            return;
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.a((CharSequence) "Do you wish to resume the last positison?");
        builder.a(true);
        final MediaSource mediaSource2 = mediaSource;
        final MovieInfo movieInfo2 = movieInfo;
        final List<String> list3 = list;
        final List<String> list4 = list2;
        builder.b((CharSequence) "Resume", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MovieFragment.this.a(mediaSource2, false, movieInfo2, list3, list4);
            }
        });
        builder.a((CharSequence) "Start over", (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                MovieFragment.this.a(mediaSource2, true, movieInfo2, list3, list4);
            }
        });
        builder.c();
    }

    public void a(MediaSource mediaSource, boolean z2, MovieInfo movieInfo, List<String> list, List<String> list2) {
        if (z2) {
            this.p.setPosition(0);
        }
        if (!e()) {
            int i2 = 0;
            int i3 = 0;
            while (true) {
                if (i3 >= this.d.size()) {
                    break;
                } else if (mediaSource.getStreamLink().equals(this.d.get(i3).getStreamLink())) {
                    i2 = i3;
                    break;
                } else {
                    i3++;
                }
            }
            Intent a2 = new SourceActivity.UriSample(this.p.getName(), false, (String) null, (SourceActivity.DrmInfo) null, Uri.parse(mediaSource.getStreamLink()), "", "").a(getContext());
            a2.putExtra("Movie", this.p);
            a2.putExtra("LINKID", this.r);
            a2.putExtra("streamID", i2);
            a2.putExtra("MovieInfo", movieInfo);
            IntentDataContainer.a().a("MediaSouce", this.d);
            i();
            startActivityForResult(a2, 37701);
        } else if (list == null) {
            ((MovieDetailsActivity) getActivity()).a(CastHelper.a(CastHelper.a(this.p, movieInfo, mediaSource), mediaSource), (int) this.p.getPosition(), true);
        } else {
            ((MovieDetailsActivity) getActivity()).a(CastHelper.a(CastHelper.a(this.p, movieInfo, mediaSource), mediaSource, list, list2), (int) this.p.getPosition(), true);
        }
    }
}
