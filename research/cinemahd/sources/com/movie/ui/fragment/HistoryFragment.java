package com.movie.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.ui.widget.AnimatorStateView;
import com.original.tase.RxBus;
import com.original.tase.api.TraktUserApi;
import com.original.tase.event.trakt.TrackSyncFaild;
import com.original.tase.event.trakt.TraktSyncSuccess;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class HistoryFragment extends MoviesFragment {
    private CompositeDisposable s = null;
    @Inject
    MvDatabase t;
    @Inject
    TMDBApi u;
    int v = 0;

    static /* synthetic */ void b(Throwable th) throws Exception {
    }

    static /* synthetic */ void i() throws Exception {
    }

    /* access modifiers changed from: private */
    public void j() {
        if (!this.mSwipeRefreshLayout.isRefreshing()) {
            this.n.b();
        }
        this.mViewAnimator.setDisplayedChildId(R.id.view_loading);
        this.s.b(g().flatMap(h0.f5682a).observeOn(AndroidSchedulers.a()).subscribe(new i0(this), new k0(this), l0.f5697a));
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return this.n.getItemCount() > 0 ? R.id.movies_recycler_view : R.id.view_empty;
    }

    public Observable<List<MovieEntity>> g() {
        final String string = FreeMoviesApp.l().getString("pref_limit_history_size", "Unlimited");
        return new Observable<List<MovieEntity>>() {
            /* access modifiers changed from: protected */
            public void subscribeActual(Observer<? super List<MovieEntity>> observer) {
                if (string.equals("Unlimited")) {
                    HistoryFragment historyFragment = HistoryFragment.this;
                    int i = historyFragment.v;
                    if (i == 0) {
                        observer.onNext(historyFragment.t.m().a((Boolean) true));
                    } else if (i == 1) {
                        observer.onNext(historyFragment.t.m().a((Boolean) false));
                    } else if (i == 2) {
                        observer.onNext(historyFragment.t.m().a());
                    }
                } else {
                    HistoryFragment historyFragment2 = HistoryFragment.this;
                    int i2 = historyFragment2.v;
                    if (i2 == 0) {
                        observer.onNext(historyFragment2.t.m().a(true, Integer.valueOf(string).intValue()));
                    } else if (i2 == 1) {
                        observer.onNext(historyFragment2.t.m().a(false, Integer.valueOf(string).intValue()));
                    } else if (i2 == 2) {
                        observer.onNext(historyFragment2.t.m().b(Integer.valueOf(string).intValue()));
                    }
                }
                observer.onComplete();
            }
        }.subscribeOn(Schedulers.b());
    }

    public void h() {
        j();
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = false;
        setHasOptionsMenu(true);
    }

    public void onRefresh() {
        if (TraktCredentialsHelper.b().isValid()) {
            TraktUserApi.f().c(this.p, (Activity) getActivity(), this.t);
        }
    }

    public void onResume() {
        super.onResume();
        j();
    }

    public void onStart() {
        super.onStart();
        this.v = getArguments().getInt("sortField");
        this.b = false;
        this.s = new CompositeDisposable();
        this.s.b(RxBus.b().a().subscribe(new Consumer<Object>() {
            public void accept(Object obj) throws Exception {
                if (obj instanceof TraktSyncSuccess) {
                    HistoryFragment.this.j();
                } else if (obj instanceof TrackSyncFaild) {
                    AnimatorStateView animatorStateView = (AnimatorStateView) HistoryFragment.this.mViewAnimator.findViewById(R.id.view_empty);
                    if (animatorStateView != null) {
                        animatorStateView.setMessageText("Your history is empty");
                    }
                    HistoryFragment.this.mViewAnimator.setDisplayedChildId(R.id.view_empty);
                }
            }
        }, j0.f5690a));
    }

    public void onStop() {
        this.s.dispose();
        super.onStop();
    }

    public static HistoryFragment a(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("sortField", i);
        HistoryFragment historyFragment = new HistoryFragment();
        historyFragment.setArguments(bundle);
        return historyFragment;
    }

    static /* synthetic */ ObservableSource a(List list) throws Exception {
        if (list.size() > 0) {
            return Observable.fromIterable(list);
        }
        throw new Exception();
    }

    public /* synthetic */ void a(MovieEntity movieEntity) throws Exception {
        boolean z = false;
        this.mSwipeRefreshLayout.setRefreshing(false);
        List d = this.n.d();
        int i = 0;
        while (true) {
            if (i >= d.size()) {
                break;
            } else if (((MovieEntity) d.get(i)).getTmdbID() == movieEntity.getTmdbID()) {
                z = true;
                break;
            } else {
                i++;
            }
        }
        if (!z) {
            this.n.a(movieEntity);
        }
        this.mViewAnimator.setDisplayedChildId(f());
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Timber.a(th, "Favored movies loading failed", new Object[0]);
        AnimatorStateView animatorStateView = (AnimatorStateView) this.mViewAnimator.findViewById(R.id.view_empty);
        if (animatorStateView != null) {
            animatorStateView.setMessageText("Your history is empty");
        }
        this.mViewAnimator.setDisplayedChildId(R.id.view_empty);
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }
}
