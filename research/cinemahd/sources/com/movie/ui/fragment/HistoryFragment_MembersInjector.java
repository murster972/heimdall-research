package com.movie.ui.fragment;

import com.database.MvDatabase;
import com.movie.data.api.tmdb.TMDBApi;
import dagger.MembersInjector;

public final class HistoryFragment_MembersInjector implements MembersInjector<HistoryFragment> {
    public static void a(HistoryFragment historyFragment, MvDatabase mvDatabase) {
        historyFragment.t = mvDatabase;
    }

    public static void a(HistoryFragment historyFragment, TMDBApi tMDBApi) {
        historyFragment.u = tMDBApi;
    }
}
