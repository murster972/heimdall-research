package com.movie.ui.fragment;

import android.view.View;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.tabs.TabLayout;
import com.yoku.marumovie.R;

public class FavoredPageFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private FavoredPageFragment f5630a;

    public FavoredPageFragment_ViewBinding(FavoredPageFragment favoredPageFragment, View view) {
        this.f5630a = favoredPageFragment;
        favoredPageFragment.tabLayout = (TabLayout) Utils.findRequiredViewAsType(view, R.id.tablayout, "field 'tabLayout'", TabLayout.class);
        favoredPageFragment.viewPager = (ViewPager) Utils.findRequiredViewAsType(view, R.id.viewpage, "field 'viewPager'", ViewPager.class);
    }

    public void unbind() {
        FavoredPageFragment favoredPageFragment = this.f5630a;
        if (favoredPageFragment != null) {
            this.f5630a = null;
            favoredPageFragment.tabLayout = null;
            favoredPageFragment.viewPager = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
