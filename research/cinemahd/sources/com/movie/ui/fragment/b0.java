package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5658a;

    public /* synthetic */ b0(Observer observer) {
        this.f5658a = observer;
    }

    public final void accept(Object obj) {
        this.f5658a.onComplete();
    }
}
