package com.movie.ui.fragment;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class u0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5780a;

    public /* synthetic */ u0(MovieFragment movieFragment) {
        this.f5780a = movieFragment;
    }

    public final void accept(Object obj) {
        this.f5780a.c((MediaSource) obj);
    }
}
