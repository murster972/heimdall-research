package com.movie.ui.fragment;

import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class a implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5653a;

    public /* synthetic */ a(ObservableEmitter observableEmitter) {
        this.f5653a = observableEmitter;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.AnonymousClass1.a(this.f5653a, (List) obj);
    }
}
