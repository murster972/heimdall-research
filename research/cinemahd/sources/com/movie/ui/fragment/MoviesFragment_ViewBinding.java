package com.movie.ui.fragment;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.movie.ui.widget.BetterViewAnimator;
import com.movie.ui.widget.MultiSwipeRefreshLayout;
import com.yoku.marumovie.R;

public class MoviesFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private MoviesFragment f5649a;

    public MoviesFragment_ViewBinding(MoviesFragment moviesFragment, View view) {
        this.f5649a = moviesFragment;
        moviesFragment.mSwipeRefreshLayout = (MultiSwipeRefreshLayout) Utils.findRequiredViewAsType(view, R.id.multi_swipe_refresh_layout, "field 'mSwipeRefreshLayout'", MultiSwipeRefreshLayout.class);
        moviesFragment.mViewAnimator = (BetterViewAnimator) Utils.findRequiredViewAsType(view, R.id.movies_animator, "field 'mViewAnimator'", BetterViewAnimator.class);
        moviesFragment.mRecyclerView = (RecyclerView) Utils.findRequiredViewAsType(view, R.id.movies_recycler_view, "field 'mRecyclerView'", RecyclerView.class);
    }

    public void unbind() {
        MoviesFragment moviesFragment = this.f5649a;
        if (moviesFragment != null) {
            this.f5649a = null;
            moviesFragment.mSwipeRefreshLayout = null;
            moviesFragment.mViewAnimator = null;
            moviesFragment.mRecyclerView = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
