package com.movie.ui.fragment;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class r1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5772a;

    public /* synthetic */ r1(MovieFragment movieFragment) {
        this.f5772a = movieFragment;
    }

    public final void accept(Object obj) {
        this.f5772a.b((MediaSource) obj);
    }
}
