package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class e implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ FilesBottomSheetFragment f5745a;

    public /* synthetic */ e(FilesBottomSheetFragment filesBottomSheetFragment) {
        this.f5745a = filesBottomSheetFragment;
    }

    public final void accept(Object obj) {
        this.f5745a.e((TorrentObject) obj);
    }
}
