package com.movie.ui.fragment.premium.viewholder;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public abstract class BaseViewHolder extends RecyclerView.ViewHolder {
    public BaseViewHolder(View view) {
        super(view);
    }

    public void a(int i) {
        b();
    }

    /* access modifiers changed from: protected */
    public abstract void b();
}
