package com.movie.ui.fragment.premium;

import com.database.MvDatabase;
import com.movie.data.api.realdebrid.RealDebridApi;
import dagger.MembersInjector;

public final class TorrentAdapterListFragment_MembersInjector implements MembersInjector<TorrentAdapterListFragment> {
    public static void a(TorrentAdapterListFragment torrentAdapterListFragment, MvDatabase mvDatabase) {
        torrentAdapterListFragment.c = mvDatabase;
    }

    public static void a(TorrentAdapterListFragment torrentAdapterListFragment, RealDebridApi realDebridApi) {
        torrentAdapterListFragment.d = realDebridApi;
    }
}
