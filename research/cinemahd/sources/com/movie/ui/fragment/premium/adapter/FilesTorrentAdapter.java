package com.movie.ui.fragment.premium.adapter;

import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.movie.data.model.TorrentObject;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.ArrayList;

public class FilesTorrentAdapter extends RecyclerView.Adapter<ViewHolder> implements Filterable {

    /* renamed from: a  reason: collision with root package name */
    TorrentObject f5732a;
    TorrentObject b;
    FileTorrentListener c;

    public interface FileTorrentListener {
        void a(TorrentObject.FileBean fileBean);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        /* renamed from: a  reason: collision with root package name */
        TextView f5735a;
        TextView b;

        public ViewHolder(FilesTorrentAdapter filesTorrentAdapter, View view) {
            super(view);
            this.f5735a = (TextView) view.findViewById(R.id.tvID);
            this.b = (TextView) view.findViewById(R.id.tvFileSize);
        }
    }

    public FilesTorrentAdapter(TorrentObject torrentObject) {
        this.f5732a = torrentObject;
        this.b = torrentObject;
    }

    /* renamed from: a */
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        final TorrentObject.FileBean fileBean = this.f5732a.getFiles().get(i);
        viewHolder.f5735a.setText(fileBean.getName());
        viewHolder.b.setText(Formatter.formatFileSize(Utils.i(), fileBean.getSize()));
        viewHolder.f5735a.setTextColor(-256);
        if (this.c != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    FilesTorrentAdapter.this.c.a(fileBean);
                }
            });
        }
    }

    public Filter getFilter() {
        return new Filter() {
            /* access modifiers changed from: protected */
            public Filter.FilterResults performFiltering(CharSequence charSequence) {
                String charSequence2 = charSequence.toString();
                TorrentObject torrentObject = FilesTorrentAdapter.this.f5732a;
                if (charSequence2.isEmpty()) {
                    FilesTorrentAdapter.this.b = torrentObject;
                } else {
                    ArrayList arrayList = new ArrayList();
                    for (TorrentObject.FileBean next : torrentObject.getFiles()) {
                        if (next.getName().toLowerCase().contains(charSequence2.toLowerCase())) {
                            arrayList.add(next);
                        }
                    }
                    FilesTorrentAdapter.this.b.setFiles(arrayList);
                }
                Filter.FilterResults filterResults = new Filter.FilterResults();
                filterResults.values = FilesTorrentAdapter.this.b;
                return filterResults;
            }

            /* access modifiers changed from: protected */
            public void publishResults(CharSequence charSequence, Filter.FilterResults filterResults) {
                FilesTorrentAdapter filesTorrentAdapter = FilesTorrentAdapter.this;
                filesTorrentAdapter.b = (TorrentObject) filterResults.values;
                filesTorrentAdapter.notifyDataSetChanged();
            }
        };
    }

    public int getItemCount() {
        if (this.b.getFiles() != null) {
            return this.b.getFiles().size();
        }
        return 0;
    }

    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
    }

    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return new ViewHolder(this, LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.file_torrent_item, viewGroup, false));
    }

    public void a(FileTorrentListener fileTorrentListener) {
        this.c = fileTorrentListener;
    }
}
