package com.movie.ui.fragment.premium;

import android.app.Activity;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import com.database.MvDatabase;
import com.database.entitys.premiumEntitys.torrents.TorrentEntity;
import com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter;
import com.movie.AppComponent;
import com.movie.data.api.alldebrid.AllDebridApi;
import com.movie.data.api.alldebrid.AllDebridModule;
import com.movie.data.api.premiumize.PremiumizeApi;
import com.movie.data.api.premiumize.PremiumizeModule;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.model.TorrentObject;
import com.movie.data.model.premiumize.FolderList;
import com.movie.data.model.premiumize.ItemDetails;
import com.movie.data.model.realdebrid.RealDebridTorrentInfoObject;
import com.movie.data.model.realdebrid.UnRestrictCheckObject;
import com.movie.ui.activity.BaseActivity;
import com.movie.ui.fragment.BaseFragment;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.fragment.premium.adapter.TorrentAdapter;
import com.movie.ui.listener.EndlessScrollListener;
import com.movie.ui.widget.AnimatorStateView;
import com.original.tase.debrid.premiumize.PremiumizeCredentialsHelper;
import com.original.tase.model.debrid.alldebrid.ADstatus;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import retrofit2.Response;
import timber.log.Timber;

public class TorrentAdapterListFragment extends BaseFragment implements EndlessScrollListener.OnLoadMoreCallback, TorrentAdapter.TorrentAdapterListener {
    @Inject
    MvDatabase c;
    @Inject
    RealDebridApi d;
    AllDebridApi e = AllDebridModule.b();
    PremiumizeApi f = PremiumizeModule.b();
    TorrentAdapter g;
    CompositeDisposable h;
    TorrentObject.Type i;
    EndlessScrollListener j;
    private int k = 0;
    private int l = 0;
    @BindView(2131297004)
    RecyclerView rv_magnetfiles;
    @BindView(2131297224)
    AnimatorStateView viewEmpty;

    /* renamed from: com.movie.ui.fragment.premium.TorrentAdapterListFragment$10  reason: invalid class name */
    static /* synthetic */ class AnonymousClass10 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5720a = new int[TorrentObject.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.movie.data.model.TorrentObject$Type[] r0 = com.movie.data.model.TorrentObject.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5720a = r0
                int[] r0 = f5720a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.RD     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5720a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.AD     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5720a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.PM     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.fragment.premium.TorrentAdapterListFragment.AnonymousClass10.<clinit>():void");
        }
    }

    public static int a(TorrentObject.Type type) {
        int i2 = AnonymousClass10.f5720a[type.ordinal()];
        int i3 = 1;
        if (i2 != 1) {
            i3 = 2;
            if (i2 != 2) {
                i3 = 3;
                if (i2 != 3) {
                    return 0;
                }
            }
        }
        return i3;
    }

    public static TorrentAdapterListFragment b(TorrentObject.Type type) {
        Bundle bundle = new Bundle();
        TorrentAdapterListFragment torrentAdapterListFragment = new TorrentAdapterListFragment();
        bundle.putSerializable("type", type);
        torrentAdapterListFragment.setArguments(bundle);
        return torrentAdapterListFragment;
    }

    public void c(final TorrentObject torrentObject) {
        int i2 = AnonymousClass10.f5720a[torrentObject.getType().ordinal()];
        if (i2 == 1) {
            this.h.b(Observable.create(new ObservableOnSubscribe<Object>() {
                public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                    if (TorrentAdapterListFragment.this.d.delete(torrentObject.getId()).execute().isSuccessful()) {
                        observableEmitter.onNext(new Boolean(true));
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new r(this, torrentObject), new l(this)));
        } else if (i2 == 2) {
            this.h.b(Observable.create(new ObservableOnSubscribe<Object>() {
                public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                    if (TorrentAdapterListFragment.this.e.delete(torrentObject.getId()).execute().isSuccessful()) {
                        observableEmitter.onNext(new Boolean(true));
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new v(this, torrentObject), new a0(this)));
        } else if (i2 == 3) {
            this.h.b(Observable.create(new ObservableOnSubscribe<Object>() {
                public void subscribe(ObservableEmitter<Object> observableEmitter) throws Exception {
                    if (TorrentAdapterListFragment.this.f.transferdelete(PremiumizeCredentialsHelper.b().getAccessToken(), torrentObject.getId()).execute().isSuccessful()) {
                        observableEmitter.onNext(new Boolean(true));
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new x(this, torrentObject), new u(this)));
        }
    }

    public /* synthetic */ void d(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void e(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
        ((BaseActivity) getActivity()).hideWaitingDialog();
    }

    public /* synthetic */ void f(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), "update progress error");
    }

    public /* synthetic */ void g(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), "update progress error");
    }

    public /* synthetic */ void h(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), "update progress error");
    }

    public boolean onContextItemSelected(MenuItem menuItem) {
        if (a(this.i) == menuItem.getGroupId()) {
            TorrentObject torrentObject = (TorrentObject) this.g.getItem(menuItem.getOrder());
            int itemId = menuItem.getItemId();
            if (itemId == 1) {
                c(torrentObject);
            } else if (itemId != 2 && itemId == 3) {
                b(torrentObject);
            }
        }
        return super.onContextItemSelected(menuItem);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.select_magnet_files_fragment, viewGroup, false);
    }

    public void onDestroy() {
        this.h.a();
        super.onDestroy();
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public void onStart() {
        this.k = 0;
        this.viewEmpty.setVisibility(8);
        this.g.b();
        int i2 = this.k + 1;
        this.k = i2;
        b(i2);
        super.onStart();
    }

    public void onStop() {
        unregisterForContextMenu(this.rv_magnetfiles);
        super.onStop();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.b = false;
        this.i = (TorrentObject.Type) getArguments().get("type");
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        this.rv_magnetfiles.setLayoutManager(linearLayoutManager);
        this.g = new TorrentAdapter(getActivity(), new ArrayList());
        this.g.a(true);
        this.g.a((TorrentAdapter.TorrentAdapterListener) this);
        this.rv_magnetfiles.setAdapter(this.g);
        registerForContextMenu(this.rv_magnetfiles);
        a(linearLayoutManager, this.k);
        this.h = new CompositeDisposable();
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public /* synthetic */ ObservableSource d(final String str) throws Exception {
        return Observable.create(new ObservableOnSubscribe<TorrentObject.FileBean>() {
            public void subscribe(ObservableEmitter<TorrentObject.FileBean> observableEmitter) throws Exception {
                Response<UnRestrictCheckObject> execute = TorrentAdapterListFragment.this.d.unrestrictCheck(str, (String) null).execute();
                if (execute.isSuccessful() && execute.body() != null) {
                    UnRestrictCheckObject body = execute.body();
                    observableEmitter.onNext(new TorrentObject.FileBean(body.getFilename(), str, body.getFilesize(), body.getHost()));
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.a());
    }

    public /* synthetic */ void f(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.g.b(torrentObject);
    }

    public /* synthetic */ void g(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.g.b(torrentObject);
    }

    private void a(LinearLayoutManager linearLayoutManager, int i2) {
        EndlessScrollListener endlessScrollListener = this.j;
        if (endlessScrollListener != null) {
            this.rv_magnetfiles.removeOnScrollListener(endlessScrollListener);
        }
        this.j = EndlessScrollListener.a(linearLayoutManager, 10, i2).a(this);
        this.rv_magnetfiles.addOnScrollListener(this.j);
    }

    public /* synthetic */ void e(TorrentObject torrentObject) throws Exception {
        torrentObject.setGotDetails(true);
        this.g.b(torrentObject);
    }

    /* JADX WARNING: Removed duplicated region for block: B:18:0x0095  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(final int r6) {
        /*
            r5 = this;
            int[] r0 = com.movie.ui.fragment.premium.TorrentAdapterListFragment.AnonymousClass10.f5720a
            com.movie.data.model.TorrentObject$Type r1 = r5.i
            int r1 = r1.ordinal()
            r0 = r0[r1]
            java.lang.String r1 = "You are not logged in"
            r2 = 1
            r3 = 0
            if (r0 == r2) goto L_0x0061
            r4 = 2
            if (r0 == r4) goto L_0x002a
            r1 = 3
            if (r0 == r1) goto L_0x0018
            goto L_0x0092
        L_0x0018:
            com.movie.ui.fragment.premium.adapter.TorrentAdapter r0 = r5.g
            r0.a((boolean) r3)
            com.movie.ui.widget.AnimatorStateView r0 = r5.viewEmpty
            java.lang.String r1 = "COMMING SOON!!!!"
            r0.setMessageText(r1)
            com.movie.ui.widget.AnimatorStateView r0 = r5.viewEmpty
            r0.setVisibility(r3)
            goto L_0x0092
        L_0x002a:
            com.original.tase.model.debrid.alldebrid.AllDebridCredentialsInfo r0 = com.original.tase.debrid.alldebrid.AllDebridCredentialsHelper.b()
            boolean r0 = r0.isValid()
            if (r0 == 0) goto L_0x0051
            if (r6 != r2) goto L_0x0092
            com.movie.ui.fragment.premium.TorrentAdapterListFragment$1 r0 = new com.movie.ui.fragment.premium.TorrentAdapterListFragment$1
            r0.<init>()
            io.reactivex.Observable r0 = io.reactivex.Observable.create(r0)
            com.movie.ui.fragment.premium.o r1 = new com.movie.ui.fragment.premium.o
            r1.<init>(r5)
            io.reactivex.Observable r0 = r0.map(r1)
            io.reactivex.Scheduler r1 = io.reactivex.schedulers.Schedulers.b()
            io.reactivex.Observable r0 = r0.subscribeOn(r1)
            goto L_0x0093
        L_0x0051:
            com.movie.ui.fragment.premium.adapter.TorrentAdapter r0 = r5.g
            r0.a((boolean) r3)
            com.movie.ui.widget.AnimatorStateView r0 = r5.viewEmpty
            r0.setMessageText(r1)
            com.movie.ui.widget.AnimatorStateView r0 = r5.viewEmpty
            r0.setVisibility(r3)
            goto L_0x0092
        L_0x0061:
            com.original.tase.model.debrid.realdebrid.RealDebridCredentialsInfo r0 = com.original.tase.debrid.realdebrid.RealDebridCredentialsHelper.c()
            boolean r0 = r0.isValid()
            if (r0 == 0) goto L_0x0083
            com.movie.ui.fragment.premium.TorrentAdapterListFragment$2 r0 = new com.movie.ui.fragment.premium.TorrentAdapterListFragment$2
            r0.<init>(r6)
            io.reactivex.Observable r0 = io.reactivex.Observable.create(r0)
            com.movie.ui.fragment.premium.m r1 = com.movie.ui.fragment.premium.m.f5753a
            io.reactivex.Observable r0 = r0.map(r1)
            io.reactivex.Scheduler r1 = io.reactivex.schedulers.Schedulers.b()
            io.reactivex.Observable r0 = r0.subscribeOn(r1)
            goto L_0x0093
        L_0x0083:
            com.movie.ui.fragment.premium.adapter.TorrentAdapter r0 = r5.g
            r0.a((boolean) r3)
            com.movie.ui.widget.AnimatorStateView r0 = r5.viewEmpty
            r0.setMessageText(r1)
            com.movie.ui.widget.AnimatorStateView r0 = r5.viewEmpty
            r0.setVisibility(r3)
        L_0x0092:
            r0 = 0
        L_0x0093:
            if (r0 == 0) goto L_0x00b5
            io.reactivex.disposables.CompositeDisposable r1 = r5.h
            io.reactivex.Scheduler r2 = io.reactivex.android.schedulers.AndroidSchedulers.a()
            io.reactivex.Observable r0 = r0.observeOn(r2)
            com.movie.ui.fragment.premium.j r2 = new com.movie.ui.fragment.premium.j
            r2.<init>(r5)
            com.movie.ui.fragment.premium.k r3 = new com.movie.ui.fragment.premium.k
            r3.<init>(r5)
            com.movie.ui.fragment.premium.n r4 = new com.movie.ui.fragment.premium.n
            r4.<init>(r5, r6)
            io.reactivex.disposables.Disposable r6 = r0.subscribe(r2, r3, r4)
            r1.b(r6)
        L_0x00b5:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.fragment.premium.TorrentAdapterListFragment.b(int):void");
    }

    public /* synthetic */ List a(ADstatus aDstatus) throws Exception {
        ArrayList arrayList = new ArrayList();
        for (ADstatus.DataBean.MagnetsBean next : aDstatus.getData().getMagnets()) {
            TorrentEntity a2 = this.c.n().a(next.getHash(), String.valueOf(next.getId()), TorrentTypeConverter.a(TorrentObject.Type.AD));
            TorrentObject convert = next.convert();
            convert.setTorrentEntity(a2);
            arrayList.add(convert);
        }
        return arrayList;
    }

    public /* synthetic */ void a(List list) throws Exception {
        this.k++;
        int i2 = this.l;
        int i3 = this.k;
        if (i2 > i3) {
            this.l = i3;
        }
        Timber.a(String.format("Page %d is loaded, %d new items", new Object[]{Integer.valueOf(this.k), Integer.valueOf(list.size())}), new Object[0]);
        if (this.k == 1) {
            this.g.b();
        }
        this.g.b((List<TorrentObject>) list);
        TorrentObject.Type type = this.i;
        if (type == TorrentObject.Type.AD || type == TorrentObject.Type.PM) {
            this.g.a(false);
        } else {
            this.g.a(!list.isEmpty());
        }
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void c(TorrentObject torrentObject, Object obj) throws Exception {
        this.g.a(torrentObject);
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void a(int i2) throws Exception {
        if (this.g.d().size() == 0 && i2 == 1) {
            this.viewEmpty.setMessageText("Empty");
            this.viewEmpty.setVisibility(0);
        }
    }

    static /* synthetic */ List b(List list) throws Exception {
        ArrayList arrayList = new ArrayList();
        Iterator it2 = list.iterator();
        while (it2.hasNext()) {
            arrayList.add(((RealDebridTorrentInfoObject) it2.next()).convert());
        }
        return arrayList;
    }

    public void a(int i2, int i3) {
        if (this.g.e()) {
            b(i2);
        }
    }

    public /* synthetic */ void a(TorrentObject torrentObject, Object obj) throws Exception {
        this.g.a(torrentObject);
    }

    public /* synthetic */ void a(TorrentObject torrentObject, List list) throws Exception {
        torrentObject.setFiles(list);
        FilesBottomSheetFragment f2 = FilesBottomSheetFragment.f(torrentObject);
        f2.show(getActivity().getSupportFragmentManager(), f2.getTag());
        ((BaseActivity) getActivity()).hideWaitingDialog();
    }

    public /* synthetic */ void b(TorrentObject torrentObject, Object obj) throws Exception {
        this.g.a(torrentObject);
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public void b(TorrentObject torrentObject) {
        int i2 = AnonymousClass10.f5720a[torrentObject.getType().ordinal()];
        if (i2 == 1) {
            ((BaseActivity) getActivity()).showWaitingDialog("checking available links...");
            this.h.b(Observable.fromIterable(torrentObject.getListLink()).flatMap(new s(this)).toList().a(AndroidSchedulers.a()).a(new w(this, torrentObject), new q(this)));
        } else if (i2 == 2) {
            FilesBottomSheetFragment f2 = FilesBottomSheetFragment.f(torrentObject);
            f2.show(getActivity().getSupportFragmentManager(), f2.getTag());
        } else if (i2 == 3) {
            FilesBottomSheetFragment f3 = FilesBottomSheetFragment.f(torrentObject);
            f3.show(getActivity().getSupportFragmentManager(), f3.getTag());
        }
    }

    public void a(final TorrentObject torrentObject, int i2) {
        int i3 = AnonymousClass10.f5720a[torrentObject.getType().ordinal()];
        if (i3 == 1) {
            this.h.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    Response<RealDebridTorrentInfoObject> execute = TorrentAdapterListFragment.this.d.torrentInfos(torrentObject.getId()).execute();
                    TorrentObject convert = execute.body().convert();
                    convert.setTorrentEntity(TorrentAdapterListFragment.this.c.n().a(execute.body().getHash(), execute.body().getId(), TorrentTypeConverter.a(TorrentObject.Type.RD)));
                    observableEmitter.onNext(convert);
                    observableEmitter.onComplete();
                }
            }).delay((long) i2, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new p(this), new c0(this)));
        } else if (i3 == 2) {
            this.h.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    ADstatus.DataBean.MagnetsBean magnetsBean = TorrentAdapterListFragment.this.e.status(torrentObject.getHash(), (String) null).execute().body().getData().getMagnets().get(0);
                    if (magnetsBean.getHash().equals(torrentObject.getHash())) {
                        TorrentObject convert = magnetsBean.convert();
                        convert.setTorrentEntity(TorrentAdapterListFragment.this.c.n().a(magnetsBean.getHash(), String.valueOf(magnetsBean.getId()), TorrentTypeConverter.a(TorrentObject.Type.AD)));
                        observableEmitter.onNext(convert);
                    }
                    observableEmitter.onComplete();
                }
            }).delay((long) i2, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new b0(this), new y(this)));
        } else if (i3 == 3) {
            this.h.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    Long l;
                    ArrayList arrayList = new ArrayList();
                    Long l2 = new Long(0);
                    if (torrentObject.getFolder_id() != null) {
                        TorrentAdapterListFragment torrentAdapterListFragment = TorrentAdapterListFragment.this;
                        TorrentObject torrentObject = torrentObject;
                        Pair<List<TorrentObject.FileBean>, Long> a2 = torrentAdapterListFragment.a(torrentObject, torrentObject.getFolder_id(), 0);
                        arrayList.addAll((Collection) a2.first);
                        l = Long.valueOf(l2.longValue() + ((Long) a2.second).longValue());
                    } else {
                        ItemDetails body = TorrentAdapterListFragment.this.f.itemDetails(PremiumizeCredentialsHelper.b().getAccessToken(), torrentObject.getFile_id()).execute().body();
                        TorrentObject.FileBean fileBean = new TorrentObject.FileBean(body.getName(), body.getLink(), body.getSize(), String.valueOf(body.getId()));
                        fileBean.setQuality(body.getResx() + "p");
                        arrayList.add(fileBean);
                        l = Long.valueOf(l2.longValue() + body.getSize());
                    }
                    torrentObject.setFiles(arrayList);
                    torrentObject.setSize(l.longValue());
                    torrentObject.setTorrentEntity(TorrentAdapterListFragment.this.c.n().a(torrentObject.getHash(), torrentObject.getId(), TorrentTypeConverter.a(TorrentObject.Type.PM)));
                    observableEmitter.onNext(torrentObject);
                    observableEmitter.onComplete();
                }
            }).delay((long) i2, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new t(this), new z(this)));
        }
    }

    /* access modifiers changed from: package-private */
    public Pair<List<TorrentObject.FileBean>, Long> a(TorrentObject torrentObject, String str, int i2) throws IOException {
        ArrayList arrayList = new ArrayList();
        Long l2 = new Long(0);
        for (FolderList.ContentBean next : this.f.folderList(PremiumizeCredentialsHelper.b().getAccessToken(), str, false).execute().body().getContent()) {
            TorrentObject.FileBean fileBean = new TorrentObject.FileBean(next.getName(), next.getLink(), next.getSize(), String.valueOf(next.getId()));
            fileBean.setQuality(next.getResx() + "p");
            if (next.getType().contains("folder")) {
                int i3 = i2 + 1;
                Pair<List<TorrentObject.FileBean>, Long> a2 = a(torrentObject, next.getId(), i2);
                arrayList.addAll((Collection) a2.first);
                l2 = Long.valueOf(l2.longValue() + ((Long) a2.second).longValue());
                i2 = i3;
            } else {
                l2 = Long.valueOf(l2.longValue() + fileBean.getSize());
                arrayList.add(fileBean);
            }
        }
        return new Pair<>(arrayList, l2);
    }
}
