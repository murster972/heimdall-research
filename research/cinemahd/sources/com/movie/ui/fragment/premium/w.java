package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class w implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5763a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ w(TorrentAdapterListFragment torrentAdapterListFragment, TorrentObject torrentObject) {
        this.f5763a = torrentAdapterListFragment;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5763a.a(this.b, (List) obj);
    }
}
