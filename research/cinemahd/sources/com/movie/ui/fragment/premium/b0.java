package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5741a;

    public /* synthetic */ b0(TorrentAdapterListFragment torrentAdapterListFragment) {
        this.f5741a = torrentAdapterListFragment;
    }

    public final void accept(Object obj) {
        this.f5741a.f((TorrentObject) obj);
    }
}
