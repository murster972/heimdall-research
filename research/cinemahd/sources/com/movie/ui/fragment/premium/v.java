package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class v implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5762a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ v(TorrentAdapterListFragment torrentAdapterListFragment, TorrentObject torrentObject) {
        this.f5762a = torrentAdapterListFragment;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5762a.b(this.b, obj);
    }
}
