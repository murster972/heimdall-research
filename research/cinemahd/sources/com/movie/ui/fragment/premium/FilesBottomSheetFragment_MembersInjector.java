package com.movie.ui.fragment.premium;

import com.database.MvDatabase;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.ui.helper.MoviesHelper;
import dagger.MembersInjector;

public final class FilesBottomSheetFragment_MembersInjector implements MembersInjector<FilesBottomSheetFragment> {
    public static void a(FilesBottomSheetFragment filesBottomSheetFragment, MoviesHelper moviesHelper) {
        filesBottomSheetFragment.f5711a = moviesHelper;
    }

    public static void a(FilesBottomSheetFragment filesBottomSheetFragment, RealDebridApi realDebridApi) {
        filesBottomSheetFragment.b = realDebridApi;
    }

    public static void a(FilesBottomSheetFragment filesBottomSheetFragment, MvDatabase mvDatabase) {
        filesBottomSheetFragment.c = mvDatabase;
    }
}
