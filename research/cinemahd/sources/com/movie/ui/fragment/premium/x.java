package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class x implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5764a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ x(TorrentAdapterListFragment torrentAdapterListFragment, TorrentObject torrentObject) {
        this.f5764a = torrentAdapterListFragment;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5764a.c(this.b, obj);
    }
}
