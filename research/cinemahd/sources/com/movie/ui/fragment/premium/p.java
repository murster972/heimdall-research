package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class p implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5756a;

    public /* synthetic */ p(TorrentAdapterListFragment torrentAdapterListFragment) {
        this.f5756a = torrentAdapterListFragment;
    }

    public final void accept(Object obj) {
        this.f5756a.e((TorrentObject) obj);
    }
}
