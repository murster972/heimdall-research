package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class t implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5760a;

    public /* synthetic */ t(TorrentAdapterListFragment torrentAdapterListFragment) {
        this.f5760a = torrentAdapterListFragment;
    }

    public final void accept(Object obj) {
        this.f5760a.g((TorrentObject) obj);
    }
}
