package com.movie.ui.fragment.premium;

import com.movie.data.model.TorrentObject;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class r implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ TorrentAdapterListFragment f5758a;
    private final /* synthetic */ TorrentObject b;

    public /* synthetic */ r(TorrentAdapterListFragment torrentAdapterListFragment, TorrentObject torrentObject) {
        this.f5758a = torrentAdapterListFragment;
        this.b = torrentObject;
    }

    public final void accept(Object obj) {
        this.f5758a.a(this.b, obj);
    }
}
