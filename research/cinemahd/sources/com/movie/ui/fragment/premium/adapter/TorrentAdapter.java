package com.movie.ui.fragment.premium.adapter;

import android.content.Context;
import android.text.format.Formatter;
import android.view.ContextMenu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.movie.data.model.TorrentObject;
import com.movie.ui.adapter.EndlessAdapter;
import com.movie.ui.fragment.premium.TorrentAdapterListFragment;
import com.movie.ui.fragment.premium.viewholder.BaseViewHolder;
import com.utils.Utils;
import com.yoku.marumovie.R;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TorrentAdapter extends EndlessAdapter<TorrentObject, BaseViewHolder> {
    TorrentAdapterListener d;

    public class RDTorrentViewHolder extends BaseViewHolder implements View.OnCreateContextMenuListener {

        /* renamed from: a  reason: collision with root package name */
        public ProgressBar f5738a;
        public TextView b;
        public TextView c;
        public TextView d;
        public TextView e;
        public ImageView f;

        public RDTorrentViewHolder(View view) {
            super(view);
            this.f5738a = (ProgressBar) view.findViewById(R.id.pb_percent);
            this.b = (TextView) view.findViewById(R.id.tvStatus);
            this.c = (TextView) view.findViewById(R.id.tvNumPacks);
            this.e = (TextView) view.findViewById(R.id.tvAddedByApp);
            this.d = (TextView) view.findViewById(R.id.tvID);
            ImageButton imageButton = (ImageButton) view.findViewById(R.id.btnPlay);
            this.f = (ImageView) view.findViewById(R.id.imgdelete);
            view.setOnCreateContextMenuListener(this);
        }

        public void a(int i) {
            String str;
            String str2;
            super.a(i);
            final TorrentObject torrentObject = (TorrentObject) TorrentAdapter.this.b.get(i);
            this.f5738a.setIndeterminate(torrentObject.getStatusBean().getProgress() == 0 || !torrentObject.isGotDetails());
            this.f5738a.setProgress(torrentObject.getStatusBean().getProgress());
            this.f5738a.setVisibility((torrentObject.getStatusBean().getProgress() < 100 || !torrentObject.isGotDetails()) ? 0 : 4);
            String str3 = "";
            if (torrentObject.getStatusBean().getSeeders() > 0) {
                str = "/ " + torrentObject.getStatusBean().getSeeders() + " seeders";
            } else {
                str = str3;
            }
            if (torrentObject.getStatusBean().getSpeed() > 0) {
                str2 = " / speed : " + Formatter.formatFileSize(Utils.i(), torrentObject.getStatusBean().getSpeed()) + "/s";
            } else {
                str2 = str3;
            }
            TextView textView = this.b;
            StringBuilder sb = new StringBuilder();
            sb.append(torrentObject.getStatusBean().getStatus());
            if (torrentObject.getStatusBean().getProgress() < 100) {
                str3 = str + str2;
            }
            sb.append(str3);
            textView.setText(sb.toString());
            this.d.setText(torrentObject.getName() + " [" + Formatter.formatFileSize(Utils.i(), torrentObject.getSize()) + "]");
            this.e.setVisibility(torrentObject.getTorrentEntity() != null ? 0 : 8);
            if (torrentObject.getType() == TorrentObject.Type.PM) {
                this.c.setVisibility(0);
                this.c.setText(String.format("%d files", new Object[]{Integer.valueOf(torrentObject.getFiles().size())}));
            } else if (torrentObject.getFiles() == null || torrentObject.getFiles().size() <= 1) {
                this.c.setVisibility(8);
            } else {
                this.c.setVisibility(0);
                this.c.setText(String.format("%d links/%d files", new Object[]{Integer.valueOf(torrentObject.getListLink().size()), Integer.valueOf(torrentObject.getFiles().size())}));
            }
            if (TorrentAdapter.this.d != null) {
                this.f.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        TorrentAdapter.this.d.c(torrentObject);
                    }
                });
                if (torrentObject.getStatusBean().getProgress() < 100) {
                    TorrentAdapter.this.d.a(torrentObject, 5);
                } else if (!torrentObject.isGotDetails()) {
                    TorrentAdapter.this.d.a(torrentObject, 0);
                }
            }
        }

        /* access modifiers changed from: protected */
        public void b() {
        }

        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.add(TorrentAdapterListFragment.a(((TorrentObject) TorrentAdapter.this.b.get(getAdapterPosition())).getType()), 1, getAdapterPosition(), "Delete");
        }
    }

    public interface TorrentAdapterListener {
        void a(TorrentObject torrentObject, int i);

        void b(TorrentObject torrentObject);

        void c(TorrentObject torrentObject);
    }

    public TorrentAdapter(Context context, List<TorrentObject> list) {
        super(context, list);
    }

    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder a(ViewGroup viewGroup) {
        return null;
    }

    public long getItemId(int i) {
        if (a(i)) {
            return -1;
        }
        return (long) ((TorrentObject) getItem(i)).getId().hashCode();
    }

    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int i) {
        if (getItemViewType(i) == 2) {
            ((RDTorrentViewHolder) viewHolder).a(i);
        }
        if (this.d != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    TorrentAdapter torrentAdapter = TorrentAdapter.this;
                    torrentAdapter.d.b((TorrentObject) torrentAdapter.getItem(viewHolder.getAdapterPosition()));
                }
            });
        }
    }

    public void a(TorrentAdapterListener torrentAdapterListener) {
        this.d = torrentAdapterListener;
    }

    /* access modifiers changed from: protected */
    public RecyclerView.ViewHolder b(ViewGroup viewGroup) {
        return new RDTorrentViewHolder(this.f5523a.inflate(R.layout.user_magnet_item, viewGroup, false));
    }

    public void a(TorrentObject torrentObject) {
        int i = 0;
        while (true) {
            if (i >= this.b.size()) {
                break;
            } else if (((TorrentObject) this.b.get(i)).getId().equals(torrentObject.getId())) {
                this.b.remove(i);
                break;
            } else {
                i++;
            }
        }
        notifyDataSetChanged();
    }

    public void b(List<TorrentObject> list) {
        if (!list.isEmpty()) {
            int size = this.b.size();
            int size2 = list.size();
            this.b.addAll(list);
            b(false);
            notifyItemRangeInserted(size, size2);
        }
    }

    public void b(final boolean z) {
        Collections.sort(this.b, new Comparator<TorrentObject>(this) {
            /* renamed from: a */
            public int compare(TorrentObject torrentObject, TorrentObject torrentObject2) {
                if (z) {
                    return torrentObject.getAddedTime().compareToIgnoreCase(torrentObject2.getAddedTime());
                }
                return torrentObject2.getAddedTime().compareToIgnoreCase(torrentObject.getAddedTime());
            }
        });
        notifyDataSetChanged();
    }

    public void b(TorrentObject torrentObject) {
        for (int i = 0; i < this.b.size(); i++) {
            if (((TorrentObject) this.b.get(i)).getId().equals(torrentObject.getId())) {
                this.b.set(i, torrentObject);
                notifyItemChanged(i);
                return;
            }
        }
    }
}
