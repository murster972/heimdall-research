package com.movie.ui.fragment.premium;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.ads.videoreward.AdsManager;
import com.applovin.sdk.AppLovinEventTypes;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.database.entitys.TvWatchedEpisode;
import com.database.entitys.premiumEntitys.torrents.TorrentEntity;
import com.facebook.react.uimanager.ViewProps;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.movie.FreeMoviesApp;
import com.movie.data.api.alldebrid.AllDebridApi;
import com.movie.data.api.alldebrid.AllDebridModule;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.model.MovieInfo;
import com.movie.data.model.TorrentObject;
import com.movie.data.model.realdebrid.RealDebridTorrentInfoObject;
import com.movie.data.model.realdebrid.UnRestrictObject;
import com.movie.ui.activity.SourceActivity;
import com.movie.ui.fragment.DaggerBaseFragmentComponent;
import com.movie.ui.fragment.premium.adapter.FilesTorrentAdapter;
import com.movie.ui.helper.MoviesHelper;
import com.original.Constants;
import com.original.tase.helper.player.BasePlayerHelper;
import com.original.tase.model.debrid.alldebrid.ADResponceLink;
import com.original.tase.model.media.MediaSource;
import com.original.tase.model.socket.ClientObject;
import com.original.tase.socket.Client;
import com.original.tase.utils.Regex;
import com.original.tase.utils.SourceUtils;
import com.utils.IntentDataContainer;
import com.utils.Subtitle.SubtitleInfo;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import okhttp3.internal.cache.DiskLruCache;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;
import retrofit2.Response;

public class FilesBottomSheetFragment extends BottomSheetDialogFragment implements FilesTorrentAdapter.FileTorrentListener, BasePlayerHelper.OnChoosePlayListener {
    @Inject

    /* renamed from: a  reason: collision with root package name */
    MoviesHelper f5711a;
    @Inject
    RealDebridApi b;
    @Inject
    MvDatabase c;
    AllDebridApi d = AllDebridModule.b();
    LinearLayoutManager e = null;
    TorrentObject f;
    private Unbinder g;
    private CompositeDisposable h;
    /* access modifiers changed from: private */
    public FilesTorrentAdapter i;
    private MovieEntity j;
    MovieInfo k = null;
    boolean l = false;
    @BindView(2131296765)
    ProgressBar loading;
    @BindView(2131297001)
    RecyclerView rvList;
    @BindView(2131297014)
    SearchView searchView;

    /* renamed from: com.movie.ui.fragment.premium.FilesBottomSheetFragment$6  reason: invalid class name */
    static /* synthetic */ class AnonymousClass6 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5717a = new int[TorrentObject.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(8:0|1|2|3|4|5|6|8) */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        static {
            /*
                com.movie.data.model.TorrentObject$Type[] r0 = com.movie.data.model.TorrentObject.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5717a = r0
                int[] r0 = f5717a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.RD     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5717a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.AD     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5717a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.PM     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.fragment.premium.FilesBottomSheetFragment.AnonymousClass6.<clinit>():void");
        }
    }

    static /* synthetic */ void d(Throwable th) throws Exception {
    }

    public static FilesBottomSheetFragment f(TorrentObject torrentObject) {
        FilesBottomSheetFragment filesBottomSheetFragment = new FilesBottomSheetFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("infoObject", torrentObject);
        filesBottomSheetFragment.setArguments(bundle);
        return filesBottomSheetFragment;
    }

    public void a(MovieInfo movieInfo) {
    }

    public /* synthetic */ MediaSource b(MediaSource mediaSource) throws Exception {
        TorrentEntity torrentEntity = this.f.getTorrentEntity();
        if (this.f.getTorrentEntity() != null) {
            this.j = this.c.m().a(torrentEntity.d());
        } else {
            this.j = null;
        }
        return mediaSource;
    }

    public /* synthetic */ void c(MediaSource mediaSource) throws Exception {
        BasePlayerHelper.a((Activity) getActivity(), mediaSource, (BasePlayerHelper.OnChoosePlayListener) this);
    }

    public /* synthetic */ void d(String str) throws Exception {
        Utils.a((Activity) getActivity(), str);
    }

    public /* synthetic */ void e(TorrentObject torrentObject) throws Exception {
        this.i = new FilesTorrentAdapter(torrentObject);
        this.i.a(this);
        this.loading.setVisibility(8);
        this.rvList.setAdapter(this.i);
    }

    public void onActivityResult(int i2, int i3, Intent intent) {
        Object obj;
        MovieEntity movieEntity;
        Object obj2;
        MovieEntity movieEntity2;
        super.onActivityResult(i2, i3, intent);
        if (i2 != 5) {
            if (i2 == 90) {
                if (!(intent == null || intent.getExtras() == null || (obj = intent.getExtras().get(ViewProps.POSITION)) == null || (movieEntity = this.j) == null)) {
                    a(movieEntity, this.k, Long.valueOf(obj.toString()).longValue(), false);
                }
                this.l = true;
                return;
            } else if (i2 == 431) {
                if (!(intent == null || intent.getExtras() == null || (obj2 = intent.getExtras().get("extra_position")) == null || (movieEntity2 = this.j) == null)) {
                    a(movieEntity2, this.k, Long.valueOf(obj2.toString()).longValue(), false);
                }
                this.l = true;
                return;
            } else if (!(i2 == 32123 || i2 == 44454)) {
                this.l = true;
                return;
            }
        }
        MovieEntity movieEntity3 = this.j;
        if (movieEntity3 != null) {
            a(movieEntity3, this.k, 0, true);
        }
        this.l = true;
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        DaggerBaseFragmentComponent.a().a(FreeMoviesApp.a(context).d()).a().a(this);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        final View inflate = layoutInflater.inflate(R.layout.files_torrent_bottom_sheet, viewGroup, false);
        this.g = ButterKnife.bind((Object) this, inflate);
        inflate.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < 16) {
                    inflate.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    inflate.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                BottomSheetBehavior from = BottomSheetBehavior.from((FrameLayout) ((BottomSheetDialog) FilesBottomSheetFragment.this.getDialog()).findViewById(R.id.design_bottom_sheet));
                from.c(3);
                from.b(0);
            }
        });
        return inflate;
    }

    public void onDestroyView() {
        this.g.unbind();
        this.h.dispose();
        super.onDestroyView();
    }

    public void onDetach() {
        super.onDetach();
    }

    public void onResume() {
        if (this.l) {
            AdsManager.l().j();
            this.l = false;
        }
        super.onResume();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.h = new CompositeDisposable();
        this.f = (TorrentObject) getArguments().getParcelable("infoObject");
        this.e = new LinearLayoutManager(view.getContext());
        this.e.setStackFromEnd(false);
        this.rvList.setLayoutManager(this.e);
        this.rvList.setNestedScrollingEnabled(true);
        this.loading.setVisibility(0);
        if (this.f.isGotDetails()) {
            this.i = new FilesTorrentAdapter(this.f);
            this.i.a(this);
            this.loading.setVisibility(8);
            this.rvList.setAdapter(this.i);
        } else {
            this.h.b(Observable.create(new ObservableOnSubscribe<TorrentObject>() {
                public void subscribe(ObservableEmitter<TorrentObject> observableEmitter) throws Exception {
                    FilesBottomSheetFragment filesBottomSheetFragment = FilesBottomSheetFragment.this;
                    Response<RealDebridTorrentInfoObject> execute = filesBottomSheetFragment.b.torrentInfos(filesBottomSheetFragment.f.getId()).execute();
                    if (execute.isSuccessful() && execute.body() != null) {
                        observableEmitter.onNext(execute.body().convert());
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new e(this), new h(this)));
        }
        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean a(String str) {
                FilesBottomSheetFragment.this.i.getFilter().filter(str);
                return false;
            }

            public boolean b(String str) {
                return false;
            }
        });
        this.searchView.setSearchableInfo(((SearchManager) getContext().getSystemService(AppLovinEventTypes.USER_EXECUTED_SEARCH)).getSearchableInfo(getActivity().getComponentName()));
        this.searchView.requestFocus();
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public /* synthetic */ void c(String str) throws Exception {
        Utils.a((Activity) getActivity(), str);
    }

    public void a(final TorrentObject.FileBean fileBean) {
        Observable observable;
        int i2 = AnonymousClass6.f5717a[this.f.getType().ordinal()];
        if (i2 == 1) {
            observable = Observable.create(new ObservableOnSubscribe<MediaSource>() {
                public void subscribe(ObservableEmitter<MediaSource> observableEmitter) throws Exception {
                    MediaSource mediaSource = new MediaSource("User Torrent", "RealDebrid", false);
                    mediaSource.setMovieName(fileBean.getName());
                    Response<UnRestrictObject> execute = FilesBottomSheetFragment.this.b.unrestrictLink(fileBean.getLink(), "", "").execute();
                    if (execute.code() == 200) {
                        UnRestrictObject body = execute.body();
                        mediaSource.setStreamLink(body.getDownload());
                        mediaSource.setFileSize(body.getFilesize());
                        mediaSource.setRealdebrid(true);
                        mediaSource.setResolved(true);
                        mediaSource.setFilename(body.getFilename());
                        observableEmitter.onNext(mediaSource);
                        return;
                    }
                    throw new Exception("unRestrictObjectResponse Error : " + execute.code());
                }
            }).subscribeOn(Schedulers.b());
        } else if (i2 != 2) {
            if (i2 == 3) {
                MediaSource mediaSource = new MediaSource("User Torrent", "PM", false);
                mediaSource.setMovieName(fileBean.getName());
                mediaSource.setQuality(fileBean.getQuality());
                mediaSource.setStreamLink(fileBean.getLink());
                mediaSource.setFileSize(fileBean.getSize());
                mediaSource.setPremiumize(true);
                mediaSource.setResolved(true);
                mediaSource.setFilename(fileBean.getName());
                BasePlayerHelper.a((Activity) getActivity(), mediaSource, (BasePlayerHelper.OnChoosePlayListener) this);
            }
            observable = null;
        } else {
            observable = Observable.create(new ObservableOnSubscribe<MediaSource>() {
                public void subscribe(ObservableEmitter<MediaSource> observableEmitter) throws Exception {
                    MediaSource mediaSource = new MediaSource("User Torrent", "AllDebrid", false);
                    mediaSource.setMovieName(fileBean.getName());
                    Response<ADResponceLink> execute = FilesBottomSheetFragment.this.d.getdownloadlink(fileBean.getLink()).execute();
                    if (!execute.isSuccessful() || execute.body() == null || execute.body().getStatus().contains("error")) {
                        throw new Exception("getDownloading Error : " + execute.code());
                    }
                    ADResponceLink body = execute.body();
                    mediaSource.setStreamLink(body.getData().getLink());
                    mediaSource.setFileSize(body.getData().getFilesize());
                    mediaSource.setAlldebrid(true);
                    mediaSource.setResolved(true);
                    HashMap hashMap = new HashMap();
                    hashMap.put(TheTvdb.HEADER_ACCEPT, "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
                    hashMap.put("Accept-Encoding", "gzip, deflate, br");
                    hashMap.put(TheTvdb.HEADER_ACCEPT_LANGUAGE, "en-US,en;q=0.5");
                    hashMap.put("DNT", DiskLruCache.VERSION_1);
                    hashMap.put("Connection", "keep-alive");
                    hashMap.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:81.0) Gecko/20100101 Firefox/81.0");
                    mediaSource.setFilename(body.getData().getFilename());
                    observableEmitter.onNext(mediaSource);
                }
            }).subscribeOn(Schedulers.b());
        }
        if (observable != null) {
            this.h.b(observable.map(new a(this)).observeOn(AndroidSchedulers.a()).subscribe(new f(this), i.f5749a));
        }
    }

    public /* synthetic */ void c(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    private String e(String str) {
        int indexOf = str.toLowerCase().indexOf(String.format("s%se%s", new Object[]{Regex.a(str, "([Ss]?([0-9]{1,2}))[Eex]", 2), Regex.a(str, "([Eex]([0-9]{2})(?:[^0-9]|$))", 2)}));
        return indexOf > -1 ? str.substring(0, indexOf) : "";
    }

    public /* synthetic */ void b(Throwable th) throws Exception {
        Utils.a((Activity) getActivity(), th.getMessage());
    }

    public void a(MediaSource mediaSource, MovieEntity movieEntity, MovieInfo movieInfo, List<String> list, List<String> list2) {
        Intent a2 = new SourceActivity.UriSample(movieEntity.getName(), false, (String) null, (SourceActivity.DrmInfo) null, Uri.parse(mediaSource.getStreamLink()), "", "").a(getActivity());
        a2.putExtra("Movie", movieEntity);
        a2.putExtra("LINKID", "mLinkID");
        a2.putExtra("streamID", 0);
        a2.putExtra("MovieInfo", movieInfo);
        if (list != null && list.size() > 0) {
            a2.putExtra("SubtitleInfo", new SubtitleInfo("autoSub", list.get(0), "", 0));
        }
        ArrayList arrayList = new ArrayList();
        arrayList.add(mediaSource);
        IntentDataContainer.a().a("MediaSouce", arrayList);
        ((MediaSource) arrayList.get(0)).setPlayed(true);
        getActivity().startActivityForResult(a2, 5);
    }

    public void a(int i2, MediaSource mediaSource) {
        String movieName = mediaSource.getMovieName();
        Utils.a((Activity) getActivity(), e(movieName));
        ArrayList<String> a2 = Regex.a(movieName, "(?:PPV\\.)?[HP]DTV|(?:HD)?CAM|B[rR]Rip|TS|(?:PPV )?WEB-?DL(?: DVDRip)?|H[dD]Rip|DVDRip|DVDRiP|DVDRIP|CamRip|W[EB]B[rR]ip|[Bb]lu[Rr]ay|DvDScr|hdtv", false);
        String a3 = Regex.a(movieName, "([Ss]?([0-9]{1,2}))[Eex]", 2);
        String a4 = Regex.a(movieName, "([Eex]([0-9]{2})(?:[^0-9]|$))", 2);
        String str = "-1";
        if (a3 == null || a3.isEmpty()) {
            a3 = str;
        }
        if (a4 != null && !a4.isEmpty()) {
            str = a4;
        }
        ArrayList<String> a5 = Regex.a(movieName, "([\\[\\(]?((?:19[0-9]|20[01])[0-9])[\\]\\)]?)", false);
        Regex.a(movieName, "/xvid|x264|h\\.?264/i", false);
        Regex.a(movieName, "(([0-9]{3,4}p))", false);
        mediaSource.setQuality(Utils.a(a2, "HD", false));
        MovieEntity movieEntity = this.j;
        if (movieEntity == null) {
            movieEntity = new MovieEntity();
            movieEntity.setRealeaseDate(Utils.a(a5, "1997", true));
            movieEntity.setName(mediaSource.getMovieName());
        }
        MovieEntity movieEntity2 = movieEntity;
        this.k = new MovieInfo(movieName, Utils.a(a5, "1997", true), a3, str, Utils.a(a2, "1997", true));
        BasePlayerHelper e2 = BasePlayerHelper.e();
        switch (i2) {
            case 0:
                if (e2 != null) {
                    e2.a(getActivity(), (Fragment) null, mediaSource, movieEntity2.getName() + " Season " + this.k.session + "x" + this.k.eps, movieEntity2.getPosition());
                    return;
                }
                a(mediaSource, movieEntity2, this.k, (List<String>) null, (List<String>) null);
                return;
            case 1:
                if (e2 != null) {
                    if (!movieEntity2.getRealeaseDate().isEmpty()) {
                        String str2 = movieEntity2.getRealeaseDate().split("-")[0];
                    }
                    this.k.tmdbID = movieEntity2.getTmdbID();
                    a(this.k);
                    return;
                }
                Utils.a((Activity) getActivity(), "Please choose external player in setting first.");
                return;
            case 2:
                Intent intent = new Intent("android.intent.action.VIEW");
                Uri parse = Uri.parse(mediaSource.getStreamLink());
                String str3 = "application/x-mpegURL";
                intent.setDataAndType(parse, mediaSource.isHLS() ? str3 : "video/*");
                HashMap<String, String> playHeader = mediaSource.getPlayHeader();
                if (playHeader != null && playHeader.size() > 0) {
                    HashMap<String, String> a6 = SourceUtils.a(playHeader);
                    ArrayList arrayList = new ArrayList();
                    for (Map.Entry next : a6.entrySet()) {
                        arrayList.add(next.getKey());
                        arrayList.add(next.getValue());
                    }
                    intent.putExtra("headers", (String[]) arrayList.toArray(new String[arrayList.size()]));
                }
                if (!mediaSource.isHLS()) {
                    str3 = Utils.g(mediaSource.getStreamLink());
                }
                intent.putExtra("title", (movieEntity2.getName() + " " + this.k.session + "x" + this.k.eps + " (" + ((movieEntity2.getRealeaseDate() == null || movieEntity2.getRealeaseDate().isEmpty()) ? "1970" : movieEntity2.getRealeaseDate().isEmpty() ? "" : movieEntity2.getRealeaseDate().split("-")[0]) + ")") + str3);
                if (Build.VERSION.SDK_INT >= 16) {
                    intent.setDataAndTypeAndNormalize(parse, "video/*");
                } else {
                    intent.setDataAndType(parse, "video/*");
                }
                startActivityForResult(Intent.createChooser(intent, "Open with..."), 44454);
                return;
            case 4:
                Utils.a((Activity) getActivity(), mediaSource.getStreamLink(), false);
                return;
            case 5:
                a(mediaSource, movieEntity2, this.k, (List<String>) null, (List<String>) null);
                return;
            case 6:
                if (!movieEntity2.getRealeaseDate().isEmpty()) {
                    String str4 = movieEntity2.getRealeaseDate().split("-")[0];
                }
                a(this.k);
                return;
            case 7:
                Client.getIntance().senddata(new ClientObject(e2 == null ? "CINEMA" : e2.b(), mediaSource.getStreamLink(), mediaSource.isHLS(), movieEntity2.getName(), -1.0d, mediaSource.getOriginalLink(), Constants.f5838a, !mediaSource.getPlayHeader().isEmpty()).toString(), getActivity());
                return;
            default:
                return;
        }
    }

    public void a(MovieEntity movieEntity, MovieInfo movieInfo, long j2, boolean z) {
        movieEntity.setPosition(j2);
        movieEntity.setWatched_at(OffsetDateTime.now((ZoneId) ZoneOffset.UTC));
        if (movieEntity.getTV().booleanValue()) {
            TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
            tvWatchedEpisode.a(movieInfo.getEps().intValue());
            tvWatchedEpisode.c(movieInfo.getSession().intValue());
            tvWatchedEpisode.c(movieInfo.tmdbID);
            tvWatchedEpisode.a(movieInfo.imdbIDStr);
            tvWatchedEpisode.e(movieInfo.tvdbID);
            tvWatchedEpisode.d(movieInfo.traktID);
            this.h.b(this.f5711a.a(movieEntity, tvWatchedEpisode, true, z).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new c(this), new b(this)));
            return;
        }
        this.h.b(this.f5711a.a(movieEntity, z).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new d(this), new g(this)));
    }
}
