package com.movie.ui.fragment;

import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class k implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5693a;

    public /* synthetic */ k(ObservableEmitter observableEmitter) {
        this.f5693a = observableEmitter;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.AnonymousClass5.a(this.f5693a, (Throwable) obj);
    }
}
