package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class t implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5776a;

    public /* synthetic */ t(Observer observer) {
        this.f5776a = observer;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.a(this.f5776a, (List) obj);
    }
}
