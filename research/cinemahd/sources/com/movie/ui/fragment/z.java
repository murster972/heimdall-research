package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class z implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5794a;

    public /* synthetic */ z(Observer observer) {
        this.f5794a = observer;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.c(this.f5794a, (List) obj);
    }
}
