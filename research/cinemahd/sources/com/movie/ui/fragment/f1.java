package com.movie.ui.fragment;

import com.original.tase.model.media.MediaSource;
import com.utils.Utils;
import io.reactivex.functions.Predicate;

/* compiled from: lambda */
public final /* synthetic */ class f1 implements Predicate {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ f1 f5675a = new f1();

    private /* synthetic */ f1() {
    }

    public final boolean a(Object obj) {
        return Utils.b(((MediaSource) obj).getStreamLink());
    }
}
