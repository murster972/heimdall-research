package com.movie.ui.fragment;

import androidx.appcompat.app.AlertDialog;
import com.movie.ui.fragment.MovieFragment;
import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class v0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment.AnonymousClass3 f5783a;
    private final /* synthetic */ int b;
    private final /* synthetic */ AlertDialog.Builder c;

    public /* synthetic */ v0(MovieFragment.AnonymousClass3 r1, int i, AlertDialog.Builder builder) {
        this.f5783a = r1;
        this.b = i;
        this.c = builder;
    }

    public final void accept(Object obj) {
        this.f5783a.a(this.b, this.c, (MediaSource) obj);
    }
}
