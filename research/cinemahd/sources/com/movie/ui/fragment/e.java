package com.movie.ui.fragment;

import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class e implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5669a;

    public /* synthetic */ e(ObservableEmitter observableEmitter) {
        this.f5669a = observableEmitter;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.AnonymousClass2.a(this.f5669a, (List) obj);
    }
}
