package com.movie.ui.fragment;

import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class f implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5673a;

    public /* synthetic */ f(ObservableEmitter observableEmitter) {
        this.f5673a = observableEmitter;
    }

    public final void accept(Object obj) {
        this.f5673a.onComplete();
    }
}
