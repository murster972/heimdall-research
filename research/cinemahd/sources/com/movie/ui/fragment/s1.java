package com.movie.ui.fragment;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class s1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5775a;
    private final /* synthetic */ MediaSource b;
    private final /* synthetic */ int c;

    public /* synthetic */ s1(MovieFragment movieFragment, MediaSource mediaSource, int i) {
        this.f5775a = movieFragment;
        this.b = mediaSource;
        this.c = i;
    }

    public final void accept(Object obj) {
        this.f5775a.a(this.b, this.c, (MediaSource) obj);
    }
}
