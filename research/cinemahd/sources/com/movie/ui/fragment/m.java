package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class m implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5699a;

    public /* synthetic */ m(Observer observer) {
        this.f5699a = observer;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.b(this.f5699a, (List) obj);
    }
}
