package com.movie.ui.fragment;

import android.view.View;
import com.database.entitys.MovieEntity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class i2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesFragment f5688a;
    private final /* synthetic */ MovieEntity b;
    private final /* synthetic */ View c;

    public /* synthetic */ i2(MoviesFragment moviesFragment, MovieEntity movieEntity, View view) {
        this.f5688a = moviesFragment;
        this.b = movieEntity;
        this.c = view;
    }

    public final void accept(Object obj) {
        this.f5688a.a(this.b, this.c, (Throwable) obj);
    }
}
