package com.movie.ui.fragment;

import com.database.MvDatabase;
import com.movie.data.api.tmdb.TMDBApi;
import dagger.MembersInjector;

public final class FavoredMoviesFragment_MembersInjector implements MembersInjector<FavoredMoviesFragment> {
    public static void a(FavoredMoviesFragment favoredMoviesFragment, MvDatabase mvDatabase) {
        favoredMoviesFragment.t = mvDatabase;
    }

    public static void a(FavoredMoviesFragment favoredMoviesFragment, TMDBApi tMDBApi) {
        favoredMoviesFragment.u = tMDBApi;
    }
}
