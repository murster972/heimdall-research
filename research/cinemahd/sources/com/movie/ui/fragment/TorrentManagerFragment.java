package com.movie.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import com.google.android.material.tabs.TabLayout;
import com.movie.AppComponent;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.model.TorrentObject;
import com.movie.ui.fragment.premium.TorrentAdapterListFragment;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;
import javax.inject.Inject;

public class TorrentManagerFragment extends BaseFragment {
    @Inject
    RealDebridApi c;
    CompositeDisposable d;
    @BindView(2131297096)
    TabLayout tabLayout;
    @BindView(2131297230)
    ViewPager viewPager;

    public static class TorrentPagerAdapter extends FragmentStatePagerAdapter {
        TorrentAdapterListFragment h;
        TorrentAdapterListFragment i;
        TorrentAdapterListFragment j;

        public TorrentPagerAdapter(FragmentManager fragmentManager, CompositeDisposable compositeDisposable) {
            super(fragmentManager);
        }

        public Fragment a(int i2) {
            return b(i2);
        }

        public Fragment b(int i2) {
            if (i2 == 0) {
                if (this.h == null) {
                    this.h = TorrentAdapterListFragment.b(TorrentObject.Type.RD);
                }
                return this.h;
            } else if (i2 != 1) {
                if (this.j == null) {
                    this.j = TorrentAdapterListFragment.b(TorrentObject.Type.PM);
                }
                return this.j;
            } else {
                if (this.i == null) {
                    this.i = TorrentAdapterListFragment.b(TorrentObject.Type.AD);
                }
                return this.i;
            }
        }

        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int i2) {
            return "OBJECT " + (i2 + 1);
        }
    }

    public static TorrentManagerFragment f() {
        Bundle bundle = new Bundle();
        TorrentManagerFragment torrentManagerFragment = new TorrentManagerFragment();
        torrentManagerFragment.setArguments(bundle);
        return torrentManagerFragment;
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        Spinner spinner = (Spinner) toolbar.findViewById(R.id.spinner);
        if (spinner != null) {
            spinner.setVisibility(8);
        }
        toolbar.setTitle((CharSequence) "Torrent Manager");
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.torrent_manager_fragment, viewGroup, false);
    }

    public void onDestroy() {
        this.d.dispose();
        super.onDestroy();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.d = new CompositeDisposable();
        e();
        TabLayout tabLayout2 = this.tabLayout;
        tabLayout2.a(tabLayout2.b().b((CharSequence) "REAL DEBRID"));
        TabLayout tabLayout3 = this.tabLayout;
        tabLayout3.a(tabLayout3.b().b((CharSequence) "All DEBRID"));
        TabLayout tabLayout4 = this.tabLayout;
        tabLayout4.a(tabLayout4.b().b((CharSequence) "PREMIUMZIE"));
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                TorrentManagerFragment.this.tabLayout.b(i).g();
            }
        });
        this.tabLayout.a((TabLayout.BaseOnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
            public void a(TabLayout.Tab tab) {
                TorrentManagerFragment.this.viewPager.setCurrentItem(tab.c());
            }

            public void b(TabLayout.Tab tab) {
            }

            public void c(TabLayout.Tab tab) {
            }
        });
        this.viewPager.setAdapter(new TorrentPagerAdapter(getChildFragmentManager(), this.d));
    }
}
