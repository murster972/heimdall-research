package com.movie.ui.fragment;

import com.movie.data.model.cinema.Movie;
import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class j implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5689a;

    public /* synthetic */ j(ObservableEmitter observableEmitter) {
        this.f5689a = observableEmitter;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.AnonymousClass4.a(this.f5689a, (Movie.Response) obj);
    }
}
