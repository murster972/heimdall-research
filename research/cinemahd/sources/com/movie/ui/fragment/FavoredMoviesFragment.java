package com.movie.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.tmdb.TMDBApi;
import com.original.tase.RxBus;
import com.original.tase.api.TraktUserApi;
import com.original.tase.event.trakt.TrackSyncFaild;
import com.original.tase.event.trakt.TraktSyncSuccess;
import com.original.tase.helper.trakt.TraktCredentialsHelper;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;
import java.util.List;
import javax.inject.Inject;
import timber.log.Timber;

public final class FavoredMoviesFragment extends MoviesFragment {
    private CompositeDisposable s = null;
    @Inject
    MvDatabase t;
    @Inject
    TMDBApi u;
    private int v = 0;
    private int w = 0;

    public static FavoredMoviesFragment b(int i) {
        Bundle bundle = new Bundle();
        bundle.putInt("sortField", i);
        FavoredMoviesFragment favoredMoviesFragment = new FavoredMoviesFragment();
        favoredMoviesFragment.setArguments(bundle);
        return favoredMoviesFragment;
    }

    static /* synthetic */ void b(Throwable th) throws Exception {
    }

    private void c(int i) {
        if (!this.mSwipeRefreshLayout.isRefreshing()) {
            this.mViewAnimator.setDisplayedChildId(R.id.view_loading);
        }
        this.s.b(a(i).observeOn(AndroidSchedulers.a()).subscribe(new d0(this), new e0(this), f0.f5674a));
    }

    static /* synthetic */ void h() throws Exception {
    }

    public Observable<List<MovieEntity>> a(int i) {
        if (i == 1) {
            return new Observable<List<MovieEntity>>() {
                /* access modifiers changed from: protected */
                public void subscribeActual(Observer<? super List<MovieEntity>> observer) {
                    observer.onNext(FavoredMoviesFragment.this.t.m().a(false));
                    observer.onComplete();
                }
            }.subscribeOn(Schedulers.b());
        }
        if (i != 2) {
            return new Observable<List<MovieEntity>>() {
                /* access modifiers changed from: protected */
                public void subscribeActual(Observer<? super List<MovieEntity>> observer) {
                    observer.onNext(FavoredMoviesFragment.this.t.m().d());
                    observer.onComplete();
                }
            }.subscribeOn(Schedulers.b());
        }
        return new Observable<List<MovieEntity>>() {
            /* access modifiers changed from: protected */
            public void subscribeActual(Observer<? super List<MovieEntity>> observer) {
                observer.onNext(FavoredMoviesFragment.this.t.m().a(true));
                observer.onComplete();
            }
        }.subscribeOn(Schedulers.b());
    }

    /* access modifiers changed from: protected */
    public final int f() {
        return this.n.getItemCount() > 0 ? R.id.movies_recycler_view : R.id.view_empty;
    }

    public void g() {
        c(this.v);
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        this.b = false;
        this.v = getArguments().getInt("sortField");
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_favorites, menu);
        menu.findItem(R.id.sync_trakt).setVisible(TraktCredentialsHelper.b().isValid());
        super.onCreateOptionsMenu(menu, menuInflater);
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        int itemId = menuItem.getItemId();
        if (itemId != R.id.sync_trakt) {
            switch (itemId) {
                case R.id.favorite_menu_sort_az:
                    this.n.b(true);
                    this.w = -1;
                    break;
                case R.id.favorite_menu_sort_lastest:
                    this.w = 0;
                    c(this.v);
                    break;
                case R.id.favorite_menu_sort_za:
                    this.n.b(false);
                    this.w = 1;
                    c(this.v);
                    break;
            }
        } else {
            TraktUserApi.f().b(FreeMoviesApp.a((Context) getActivity()).e(), (Activity) getActivity(), this.t);
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onRefresh() {
        if (TraktCredentialsHelper.b().isValid()) {
            TraktUserApi.f().b(FreeMoviesApp.a((Context) getActivity()).e(), (Activity) getActivity(), this.t);
        }
    }

    public void onStart() {
        super.onStart();
        this.s = new CompositeDisposable();
        setHasOptionsMenu(true);
        c(this.v);
        this.s.b(RxBus.b().a().subscribe(new Consumer<Object>() {
            public void accept(Object obj) throws Exception {
                if (obj instanceof TraktSyncSuccess) {
                    FavoredMoviesFragment.this.g();
                } else if (obj instanceof TrackSyncFaild) {
                    FavoredMoviesFragment.this.g();
                }
            }
        }, g0.f5678a));
    }

    public void onStop() {
        this.s.dispose();
        super.onStop();
    }

    public /* synthetic */ void a(List list) throws Exception {
        this.mSwipeRefreshLayout.setRefreshing(false);
        this.n.a(list);
        this.mViewAnimator.setDisplayedChildId(f());
        int i = this.w;
        if (i == -1) {
            this.n.b(true);
        } else if (i == 1) {
            this.n.b(false);
        }
        this.n.notifyDataSetChanged();
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Timber.a(th, "Favored movies loading failed", new Object[0]);
        this.mViewAnimator.setDisplayedChildId(R.id.view_empty);
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }
}
