package com.movie.ui.fragment;

import com.utils.ImdbSearchSuggestionModel;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;

/* compiled from: lambda */
public final /* synthetic */ class q implements Function {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ImdbSearchSuggestionModel.DBean f5767a;

    public /* synthetic */ q(ImdbSearchSuggestionModel.DBean dBean) {
        this.f5767a = dBean;
    }

    public final Object apply(Object obj) {
        return BrowseMoviesFragment.a(this.f5767a, (ResponseBody) obj);
    }
}
