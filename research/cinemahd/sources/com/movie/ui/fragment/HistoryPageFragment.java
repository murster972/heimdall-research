package com.movie.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import com.database.entitys.MovieEntity;
import com.google.android.material.tabs.TabLayout;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.ui.activity.MovieDetailsActivity;
import com.movie.ui.activity.shows.ShowActivity;
import com.movie.ui.fragment.MoviesFragment;
import com.original.tase.RxBus;
import com.original.tase.event.trakt.TraktSyncSuccess;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class HistoryPageFragment extends BaseFragment implements MoviesFragment.Listener {
    CompositeDisposable c;
    @BindView(2131297096)
    TabLayout tabLayout;
    @BindView(2131297230)
    ViewPager viewPager;

    public static class ShowPagerAdapter extends FragmentStatePagerAdapter {
        HistoryFragment h;
        HistoryFragment i;
        HistoryFragment j;
        CompositeDisposable k = this.k;

        public ShowPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment a(int i2) {
            return b(i2);
        }

        public Fragment b(int i2) {
            if (i2 == 0) {
                if (this.h == null) {
                    this.h = HistoryFragment.a(0);
                }
                return this.h;
            } else if (i2 != 1) {
                if (this.j == null) {
                    this.j = HistoryFragment.a(2);
                }
                return this.j;
            } else {
                if (this.i == null) {
                    this.i = HistoryFragment.a(1);
                }
                return this.i;
            }
        }

        /* access modifiers changed from: package-private */
        public void c(int i2) {
            if (i2 == 0) {
                HistoryFragment historyFragment = this.h;
                if (historyFragment != null) {
                    historyFragment.h();
                }
            } else if (i2 != 1) {
                HistoryFragment historyFragment2 = this.j;
                if (historyFragment2 != null) {
                    historyFragment2.h();
                }
            } else {
                HistoryFragment historyFragment3 = this.i;
                if (historyFragment3 != null) {
                    historyFragment3.h();
                }
            }
        }

        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int i2) {
            return "OBJECT " + (i2 + 1);
        }
    }

    static /* synthetic */ void a(Throwable th) throws Exception {
    }

    public void a(MovieEntity movieEntity, View view) {
        if (movieEntity.getTV().booleanValue()) {
            Intent intent = new Intent(getActivity(), ShowActivity.class);
            intent.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(getActivity(), MovieDetailsActivity.class);
        intent2.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
        startActivity(intent2);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        Spinner spinner = (Spinner) toolbar.findViewById(R.id.spinner);
        if (spinner != null) {
            spinner.setVisibility(8);
        }
        String string = FreeMoviesApp.l().getString("pref_limit_history_size", "Unlimited");
        if (string.equals("Unlimited")) {
            toolbar.setTitle((CharSequence) "History");
            return;
        }
        toolbar.setTitle((CharSequence) "History (Showing " + string + " items)");
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_history_page, viewGroup, false);
    }

    public void onDestroy() {
        this.c.dispose();
        super.onDestroy();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        e();
        TabLayout tabLayout2 = this.tabLayout;
        tabLayout2.a(tabLayout2.b().b((CharSequence) "Tv/Shows"));
        TabLayout tabLayout3 = this.tabLayout;
        tabLayout3.a(tabLayout3.b().b((CharSequence) "Movies"));
        TabLayout tabLayout4 = this.tabLayout;
        tabLayout4.a(tabLayout4.b().b((CharSequence) "All"));
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                HistoryPageFragment.this.tabLayout.b(i).g();
            }
        });
        this.tabLayout.a((TabLayout.BaseOnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
            public void a(TabLayout.Tab tab) {
                HistoryPageFragment.this.viewPager.setCurrentItem(tab.c());
                ((ShowPagerAdapter) HistoryPageFragment.this.viewPager.getAdapter()).c(tab.c());
            }

            public void b(TabLayout.Tab tab) {
            }

            public void c(TabLayout.Tab tab) {
            }
        });
        this.viewPager.setAdapter(new ShowPagerAdapter(getChildFragmentManager()));
        this.c = new CompositeDisposable();
        this.c.b(RxBus.b().a().subscribe(new Consumer<Object>() {
            public void accept(Object obj) throws Exception {
                if (obj instanceof TraktSyncSuccess) {
                    ((ShowPagerAdapter) HistoryPageFragment.this.viewPager.getAdapter()).c(HistoryPageFragment.this.viewPager.getCurrentItem());
                }
            }
        }, m0.f5700a));
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }
}
