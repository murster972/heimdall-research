package com.movie.ui.fragment;

import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class d0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ FavoredMoviesFragment f5666a;

    public /* synthetic */ d0(FavoredMoviesFragment favoredMoviesFragment) {
        this.f5666a = favoredMoviesFragment;
    }

    public final void accept(Object obj) {
        this.f5666a.a((List) obj);
    }
}
