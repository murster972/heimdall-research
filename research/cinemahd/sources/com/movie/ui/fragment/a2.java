package com.movie.ui.fragment;

import com.movie.data.model.MovieInfo;
import io.reactivex.functions.Consumer;
import java.util.ArrayList;

/* compiled from: lambda */
public final /* synthetic */ class a2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5656a;
    private final /* synthetic */ MovieInfo b;

    public /* synthetic */ a2(MovieFragment movieFragment, MovieInfo movieInfo) {
        this.f5656a = movieFragment;
        this.b = movieInfo;
    }

    public final void accept(Object obj) {
        this.f5656a.a(this.b, (ArrayList) obj);
    }
}
