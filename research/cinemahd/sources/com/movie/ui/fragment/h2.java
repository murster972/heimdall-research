package com.movie.ui.fragment;

import android.view.View;
import com.database.entitys.MovieEntity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class h2 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MoviesFragment f5684a;
    private final /* synthetic */ View b;

    public /* synthetic */ h2(MoviesFragment moviesFragment, View view) {
        this.f5684a = moviesFragment;
        this.b = view;
    }

    public final void accept(Object obj) {
        this.f5684a.a(this.b, (MovieEntity) obj);
    }
}
