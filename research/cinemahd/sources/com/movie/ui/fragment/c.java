package com.movie.ui.fragment;

import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5661a;

    public /* synthetic */ c(ObservableEmitter observableEmitter) {
        this.f5661a = observableEmitter;
    }

    public final void accept(Object obj) {
        this.f5661a.onComplete();
    }
}
