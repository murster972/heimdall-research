package com.movie.ui.fragment;

import com.original.tase.model.media.MediaSource;
import com.utils.Getlink.Resolver.BaseResolver;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/* compiled from: lambda */
public final /* synthetic */ class w1 implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ w1 f5787a = new w1();

    private /* synthetic */ w1() {
    }

    public final Object apply(Object obj) {
        return BaseResolver.b((MediaSource) obj).subscribeOn(Schedulers.b());
    }
}
