package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class y implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5791a;

    public /* synthetic */ y(Observer observer) {
        this.f5791a = observer;
    }

    public final void accept(Object obj) {
        this.f5791a.onComplete();
    }
}
