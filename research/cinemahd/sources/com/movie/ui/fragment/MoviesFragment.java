package com.movie.ui.fragment;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import com.database.MvDatabase;
import com.database.entitys.MovieEntity;
import com.movie.FreeMoviesApp;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import com.movie.data.repository.trakt.TraktRepositoryImpl;
import com.movie.ui.adapter.MoviesAdapter;
import com.movie.ui.helper.MoviesHelper;
import com.movie.ui.widget.BetterViewAnimator;
import com.movie.ui.widget.MultiSwipeRefreshLayout;
import com.utils.Utils;
import com.uwetrottmann.thetvdb.TheTvdb;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import java.util.ArrayList;
import javax.inject.Inject;

public abstract class MoviesFragment extends BaseFragment implements SwipeRefreshLayout.OnRefreshListener, MultiSwipeRefreshLayout.CanChildScrollUpCallback, MoviesAdapter.OnMovieClickListener {
    @Inject
    MoviesRepository c;
    @Inject
    MoviesApi d;
    @Inject
    MvDatabase e;
    @Inject
    public TMDBApi f;
    @Inject
    public IMDBApi g;
    @Inject
    public TheTvdb h;
    public TraktRepositoryImpl i = null;
    private int j = 5;
    private int k = 3;
    @Inject
    protected MoviesHelper l;
    protected Listener m;
    @BindView(2131296822)
    RecyclerView mRecyclerView;
    @BindView(2131296874)
    MultiSwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(2131296820)
    BetterViewAnimator mViewAnimator;
    protected MoviesAdapter n;
    protected GridLayoutManager o;
    protected CompositeDisposable p;
    protected int q = -1;
    public Toolbar r;

    public interface Listener {
        void a(MovieEntity movieEntity, View view);
    }

    static /* synthetic */ void b(MovieEntity movieEntity, View view) {
    }

    private void f() {
        this.mSwipeRefreshLayout.setColorSchemeColors(getResources().getIntArray(R.array.swipe_progress_colors));
        this.mSwipeRefreshLayout.setOnRefreshListener(this);
        this.mSwipeRefreshLayout.setCanChildScrollUpCallback(this);
    }

    public void a(final MovieEntity movieEntity, View view, int i2) {
        this.q = i2;
        if (movieEntity instanceof MovieEntity) {
            this.p.b(Observable.create(new ObservableOnSubscribe<MovieEntity>() {
                public void subscribe(ObservableEmitter<MovieEntity> observableEmitter) throws Exception {
                    MovieEntity a2 = MoviesFragment.this.e.m().a(movieEntity.getTmdbID(), movieEntity.getImdbIDStr(), movieEntity.getTraktID(), movieEntity.getTvdbID());
                    if (a2 != null) {
                        observableEmitter.onNext(a2);
                    } else {
                        observableEmitter.onNext(movieEntity);
                    }
                    observableEmitter.onComplete();
                }
            }).subscribeOn(Schedulers.b()).observeOn(AndroidSchedulers.a()).subscribe(new h2(this, view), new i2(this, movieEntity, view)));
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x000b, code lost:
        r0 = r2.mViewAnimator;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c() {
        /*
            r2 = this;
            androidx.recyclerview.widget.RecyclerView r0 = r2.mRecyclerView
            if (r0 == 0) goto L_0x000b
            r1 = -1
            boolean r0 = androidx.core.view.ViewCompat.a((android.view.View) r0, (int) r1)
            if (r0 != 0) goto L_0x0018
        L_0x000b:
            com.movie.ui.widget.BetterViewAnimator r0 = r2.mViewAnimator
            if (r0 == 0) goto L_0x001a
            int r0 = r0.getDisplayedChildId()
            r1 = 2131297226(0x7f0903ca, float:1.821239E38)
            if (r0 != r1) goto L_0x001a
        L_0x0018:
            r0 = 1
            goto L_0x001b
        L_0x001a:
            r0 = 0
        L_0x001b:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.fragment.MoviesFragment.c():boolean");
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.j = Integer.parseInt(String.valueOf(Utils.t().get(FreeMoviesApp.l().getString("pref_column_in_main", "Large"))));
        this.o = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.movies_columns));
        int a2 = Utils.a((Activity) getActivity());
        if (a2 == 2 || a2 == 0) {
            this.o.c(this.j);
        } else {
            this.o.c(this.k);
        }
        this.o.setOrientation(1);
        this.o.a(new GridLayoutManager.SpanSizeLookup() {
            public int b(int i) {
                int a2 = MoviesFragment.this.o.a();
                int itemViewType = MoviesFragment.this.n.getItemViewType(i);
                if (itemViewType == 2) {
                    return 1;
                }
                if (itemViewType != 3) {
                    return a2;
                }
                return 3;
            }
        });
        this.mRecyclerView.setLayoutManager(this.o);
        this.mRecyclerView.setAdapter(this.n);
        int i2 = this.q;
        if (i2 != -1) {
            this.mRecyclerView.scrollToPosition(i2);
        }
    }

    public void onAttach(Activity activity) {
        if (activity instanceof Listener) {
            super.onAttach(activity);
            this.m = (Listener) activity;
            return;
        }
        throw new IllegalStateException("Activity must implement MoviesFragment.Listener.");
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.j = Integer.parseInt(String.valueOf(Utils.t().get(FreeMoviesApp.l().getString("pref_column_in_main", "Large"))));
        int i2 = configuration.orientation;
        if (i2 == 2) {
            this.o.c(this.j);
        } else if (i2 == 1) {
            this.o.c(this.k);
        }
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_movies, viewGroup, false);
    }

    public void onDestroyView() {
        this.p.dispose();
        MultiSwipeRefreshLayout multiSwipeRefreshLayout = this.mSwipeRefreshLayout;
        if (multiSwipeRefreshLayout != null) {
            multiSwipeRefreshLayout.setRefreshing(false);
            this.mSwipeRefreshLayout.destroyDrawingCache();
            this.mSwipeRefreshLayout.clearAnimation();
        }
        super.onDestroyView();
    }

    public void onDetach() {
        this.m = j2.f5692a;
        this.n.a(MoviesAdapter.OnMovieClickListener.b0);
        super.onDetach();
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("state_selected_position", this.q);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.p = new CompositeDisposable();
        int i2 = -1;
        if (bundle != null) {
            i2 = bundle.getInt("state_selected_position", -1);
        }
        this.q = i2;
        this.n = new MoviesAdapter(this, new ArrayList());
        this.n.a((MoviesAdapter.OnMovieClickListener) this);
        this.i = new TraktRepositoryImpl(this.e);
        f();
        e();
    }

    public /* synthetic */ void a(View view, MovieEntity movieEntity) throws Exception {
        this.m.a(movieEntity, view);
    }

    public /* synthetic */ void a(MovieEntity movieEntity, View view, Throwable th) throws Exception {
        this.m.a(movieEntity, view);
    }
}
