package com.movie.ui.fragment;

import com.movie.data.model.MovieInfo;
import com.movie.ui.fragment.MovieFragment;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class p0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment.AnonymousClass12 f5709a;
    private final /* synthetic */ MovieInfo b;

    public /* synthetic */ p0(MovieFragment.AnonymousClass12 r1, MovieInfo movieInfo) {
        this.f5709a = r1;
        this.b = movieInfo;
    }

    public final void accept(Object obj) {
        this.f5709a.a(this.b, (List) obj);
    }
}
