package com.movie.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import com.database.entitys.MovieEntity;
import com.google.android.material.tabs.TabLayout;
import com.movie.AppComponent;
import com.movie.ui.activity.MovieDetailsActivity;
import com.movie.ui.activity.shows.ShowActivity;
import com.movie.ui.fragment.MoviesFragment;
import com.yoku.marumovie.R;
import io.reactivex.disposables.CompositeDisposable;

public class FavoredPageFragment extends BaseFragment implements MoviesFragment.Listener {
    CompositeDisposable c;
    @BindView(2131297096)
    TabLayout tabLayout;
    @BindView(2131297230)
    ViewPager viewPager;

    public static class ShowPagerAdapter extends FragmentStatePagerAdapter {
        FavoredMoviesFragment h;
        FavoredMoviesFragment i;
        FavoredMoviesFragment j;
        CompositeDisposable k = this.k;

        public ShowPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        public Fragment a(int i2) {
            return b(i2);
        }

        public Fragment b(int i2) {
            if (i2 == 0) {
                if (this.h == null) {
                    this.h = FavoredMoviesFragment.b(2);
                }
                return this.h;
            } else if (i2 != 1) {
                if (this.j == null) {
                    this.j = FavoredMoviesFragment.b(0);
                }
                return this.j;
            } else {
                if (this.i == null) {
                    this.i = FavoredMoviesFragment.b(1);
                }
                return this.i;
            }
        }

        /* access modifiers changed from: package-private */
        public void c(int i2) {
            if (i2 == 0) {
                FavoredMoviesFragment favoredMoviesFragment = this.h;
                if (favoredMoviesFragment != null) {
                    favoredMoviesFragment.g();
                }
            } else if (i2 != 1) {
                FavoredMoviesFragment favoredMoviesFragment2 = this.j;
                if (favoredMoviesFragment2 != null) {
                    favoredMoviesFragment2.g();
                }
            } else {
                FavoredMoviesFragment favoredMoviesFragment3 = this.i;
                if (favoredMoviesFragment3 != null) {
                    favoredMoviesFragment3.g();
                }
            }
        }

        public int getCount() {
            return 3;
        }

        public CharSequence getPageTitle(int i2) {
            return "OBJECT " + (i2 + 1);
        }
    }

    public void a(MovieEntity movieEntity, View view) {
        if (movieEntity.getTV().booleanValue()) {
            Intent intent = new Intent(getActivity(), ShowActivity.class);
            intent.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
            startActivity(intent);
            return;
        }
        Intent intent2 = new Intent(getActivity(), MovieDetailsActivity.class);
        intent2.putExtra("com.freeapp.freemovies.extras.EXTRA_MOVIE", movieEntity);
        startActivity(intent2);
    }

    /* access modifiers changed from: package-private */
    public void e() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        Spinner spinner = (Spinner) toolbar.findViewById(R.id.spinner);
        if (spinner != null) {
            spinner.setVisibility(8);
        }
        toolbar.setTitle((CharSequence) "Favorites");
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        return layoutInflater.inflate(R.layout.fragment_favored_page, viewGroup, false);
    }

    public void onDestroy() {
        this.c.dispose();
        super.onDestroy();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        e();
        TabLayout tabLayout2 = this.tabLayout;
        tabLayout2.a(tabLayout2.b().b((CharSequence) "Tv/Shows"));
        TabLayout tabLayout3 = this.tabLayout;
        tabLayout3.a(tabLayout3.b().b((CharSequence) "Movies"));
        TabLayout tabLayout4 = this.tabLayout;
        tabLayout4.a(tabLayout4.b().b((CharSequence) "All"));
        this.viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            public void onPageSelected(int i) {
                FavoredPageFragment.this.tabLayout.b(i).g();
            }
        });
        this.tabLayout.a((TabLayout.BaseOnTabSelectedListener) new TabLayout.OnTabSelectedListener() {
            public void a(TabLayout.Tab tab) {
                FavoredPageFragment.this.viewPager.setCurrentItem(tab.c());
                ((ShowPagerAdapter) FavoredPageFragment.this.viewPager.getAdapter()).c(tab.c());
            }

            public void b(TabLayout.Tab tab) {
            }

            public void c(TabLayout.Tab tab) {
            }
        });
        this.viewPager.setAdapter(new ShowPagerAdapter(getChildFragmentManager()));
        this.c = new CompositeDisposable();
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }
}
