package com.movie.ui.fragment;

import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class d implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5665a;

    public /* synthetic */ d(ObservableEmitter observableEmitter) {
        this.f5665a = observableEmitter;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.AnonymousClass1.b(this.f5665a, (List) obj);
    }
}
