package com.movie.ui.fragment;

import com.utils.ImdbSearchSuggestionModel;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class u implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BrowseMoviesFragment f5779a;
    private final /* synthetic */ ImdbSearchSuggestionModel.DBean b;

    public /* synthetic */ u(BrowseMoviesFragment browseMoviesFragment, ImdbSearchSuggestionModel.DBean dBean) {
        this.f5779a = browseMoviesFragment;
        this.b = dBean;
    }

    public final void subscribe(Observer observer) {
        this.f5779a.b(this.b, observer);
    }
}
