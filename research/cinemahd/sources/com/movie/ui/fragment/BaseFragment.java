package com.movie.ui.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import androidx.fragment.app.Fragment;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.original.tase.helper.http.HttpHelper;
import com.utils.Utils;

public abstract class BaseFragment extends Fragment {

    /* renamed from: a  reason: collision with root package name */
    private Unbinder f5612a;
    protected boolean b = true;

    /* access modifiers changed from: protected */
    public abstract void a(AppComponent appComponent);

    /* access modifiers changed from: protected */
    public void c(String str) {
        Utils.a((Activity) getActivity(), str);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof Activity) {
            a(FreeMoviesApp.a((Context) (Activity) context).d());
        }
    }

    public void onDestroy() {
        super.onDestroy();
    }

    public void onDestroyView() {
        Unbinder unbinder = this.f5612a;
        if (unbinder != null) {
            unbinder.unbind();
        }
        if (this.b) {
            HttpHelper.e().c();
        }
        super.onDestroyView();
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        this.f5612a = ButterKnife.bind((Object) this, view);
    }
}
