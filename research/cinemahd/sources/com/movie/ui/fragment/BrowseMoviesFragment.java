package com.movie.ui.fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.Spinner;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import com.database.entitys.MovieEntity;
import com.google.gson.Gson;
import com.movie.AppComponent;
import com.movie.FreeMoviesApp;
import com.movie.data.api.GlobalVariable;
import com.movie.data.api.imdb.IMDBUtils;
import com.movie.data.api.tvdb.TheTvdbRemoteID;
import com.movie.data.model.Categorys;
import com.movie.data.model.cinema.Movie;
import com.movie.data.model.tmvdb.ListResult;
import com.movie.data.model.tmvdb.SearchTMDB;
import com.movie.data.repository.cinema.MoviesRepositoryImpl;
import com.movie.data.repository.tmdb.TMDBRepositoryImpl;
import com.movie.ui.listener.EndlessScrollListener;
import com.original.tase.I18N;
import com.original.tase.helper.http.HttpHelper;
import com.utils.ImdbSearchSuggestionModel;
import com.utils.Utils;
import com.yoku.marumovie.R;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import org.joda.time.DateTime;
import org.json.JSONArray;
import timber.log.Timber;

public final class BrowseMoviesFragment extends MoviesFragment implements EndlessScrollListener.OnLoadMoreCallback {
    private boolean A = false;
    private int B = 0;
    private Type s;
    /* access modifiers changed from: private */
    public int t = -1;
    /* access modifiers changed from: private */
    public String u = "DVD_THISWEEK";
    private EndlessScrollListener v;
    private BehaviorSubject<Observable<List<MovieEntity>>> w = BehaviorSubject.b();
    private ImdbSearchSuggestionModel.DBean x = null;
    private Integer y = 28;
    private int z = 0;

    /* renamed from: com.movie.ui.fragment.BrowseMoviesFragment$12  reason: invalid class name */
    static /* synthetic */ class AnonymousClass12 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f5615a = new int[Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(12:0|1|2|3|4|5|6|7|8|9|10|12) */
        /* JADX WARNING: Code restructure failed: missing block: B:13:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x001f */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x002a */
        /* JADX WARNING: Missing exception handler attribute for start block: B:9:0x0035 */
        static {
            /*
                com.movie.ui.fragment.BrowseMoviesFragment$Type[] r0 = com.movie.ui.fragment.BrowseMoviesFragment.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f5615a = r0
                int[] r0 = f5615a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.movie.ui.fragment.BrowseMoviesFragment$Type r1 = com.movie.ui.fragment.BrowseMoviesFragment.Type.TV     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f5615a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.movie.ui.fragment.BrowseMoviesFragment$Type r1 = com.movie.ui.fragment.BrowseMoviesFragment.Type.MV     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                int[] r0 = f5615a     // Catch:{ NoSuchFieldError -> 0x002a }
                com.movie.ui.fragment.BrowseMoviesFragment$Type r1 = com.movie.ui.fragment.BrowseMoviesFragment.Type.Search     // Catch:{ NoSuchFieldError -> 0x002a }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x002a }
                r2 = 3
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x002a }
            L_0x002a:
                int[] r0 = f5615a     // Catch:{ NoSuchFieldError -> 0x0035 }
                com.movie.ui.fragment.BrowseMoviesFragment$Type r1 = com.movie.ui.fragment.BrowseMoviesFragment.Type.TV_RECOMENDATION     // Catch:{ NoSuchFieldError -> 0x0035 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0035 }
                r2 = 4
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0035 }
            L_0x0035:
                int[] r0 = f5615a     // Catch:{ NoSuchFieldError -> 0x0040 }
                com.movie.ui.fragment.BrowseMoviesFragment$Type r1 = com.movie.ui.fragment.BrowseMoviesFragment.Type.MV_RECOMENDATION     // Catch:{ NoSuchFieldError -> 0x0040 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0040 }
                r2 = 5
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0040 }
            L_0x0040:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.movie.ui.fragment.BrowseMoviesFragment.AnonymousClass12.<clinit>():void");
        }
    }

    public enum Type {
        MV,
        TV,
        Search,
        TV_RECOMENDATION,
        MV_RECOMENDATION
    }

    static /* synthetic */ void c(Observer observer, List list) throws Exception {
        observer.onNext(list);
        observer.onComplete();
    }

    static /* synthetic */ void d(Observer observer, List list) throws Exception {
        if (!list.isEmpty()) {
            observer.onNext(list);
        }
        observer.onComplete();
    }

    static /* synthetic */ void e(Observer observer, List list) throws Exception {
        if (!list.isEmpty()) {
            observer.onNext(list);
        }
        observer.onComplete();
    }

    private void i() {
        Timber.a("Subscribing to items", new Object[0]);
        this.p.b(Observable.concat(this.w).observeOn(AndroidSchedulers.a()).subscribe(new s(this), new w(this)));
    }

    public /* synthetic */ void b(ImdbSearchSuggestionModel.DBean dBean, Observer observer) {
        this.p.b(this.g.search(dBean.getId()).map(new q(dBean)).observeOn(Schedulers.b()).subscribe(new m(observer), new v(observer)));
    }

    /* access modifiers changed from: protected */
    public final void f() {
        if (!this.mSwipeRefreshLayout.isRefreshing()) {
            this.mViewAnimator.setDisplayedChildId(R.id.view_loading);
        }
        this.q = -1;
        GridLayoutManager gridLayoutManager = this.o;
        this.z = 0;
        a(gridLayoutManager, 0);
        a(1);
    }

    public void g() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("DVD_2WEEKAGO");
        arrayList.add("DVD_LASTWEEK");
        arrayList.add("DVD_THISWEEK");
        arrayList.add("DVD_NEXTWEEK");
        final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), 17367046, arrayList);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.b((CharSequence) I18N.a(R.string.filter));
        builder.a((ListAdapter) arrayAdapter, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                String unused = BrowseMoviesFragment.this.u = (String) arrayAdapter.getItem(i);
                BrowseMoviesFragment.this.f();
            }
        });
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c();
    }

    public void h() {
        ArrayList arrayList = new ArrayList();
        arrayList.add("No filter");
        for (int year = DateTime.now().getYear(); year >= 1850; year--) {
            arrayList.add(String.valueOf(year));
        }
        final ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), 17367046, arrayList);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.b((CharSequence) I18N.a(R.string.year_filter));
        builder.a((ListAdapter) arrayAdapter, (DialogInterface.OnClickListener) new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                int i2;
                try {
                    i2 = Integer.parseInt((String) arrayAdapter.getItem(i));
                } catch (Exception unused) {
                    i2 = -1;
                }
                int unused2 = BrowseMoviesFragment.this.t = i2;
                BrowseMoviesFragment.this.f();
            }
        });
        builder.b((CharSequence) I18N.a(R.string.ok), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.a((CharSequence) I18N.a(R.string.cancel), (DialogInterface.OnClickListener) new DialogInterface.OnClickListener(this) {
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.c();
    }

    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        i();
        if (bundle == null) {
            f();
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R.menu.menu_browse_movie, menu);
        MenuItem findItem = menu.findItem(R.id.browse_movie_fillter);
        Type type = this.s;
        boolean z2 = false;
        if (!(type == Type.Search || type == Type.MV_RECOMENDATION || type == Type.TV_RECOMENDATION)) {
            if (Categorys.canFilter(this.y.intValue(), this.s == Type.TV)) {
                z2 = true;
            }
        }
        findItem.setVisible(z2);
        super.onCreateOptionsMenu(menu, menuInflater);
        Spinner spinner = (Spinner) this.r.findViewById(R.id.spinner);
        if (this.s == Type.Search) {
            this.x = (ImdbSearchSuggestionModel.DBean) getArguments().getParcelable("search_query");
            if (spinner != null) {
                spinner.setVisibility(8);
            }
            Toolbar toolbar = this.r;
            toolbar.setTitle((CharSequence) "Results of : " + this.x.getL());
            return;
        }
        this.r.setTitle((CharSequence) "");
        this.y = Integer.valueOf(getArguments().getInt("category"));
    }

    public void onDestroyView() {
        super.onDestroyView();
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == R.id.browse_movie_fillter) {
            if (this.y.intValue() == -2) {
                g();
            } else {
                h();
            }
        }
        return super.onOptionsItemSelected(menuItem);
    }

    public void onRefresh() {
        f();
        if (this.s == Type.Search) {
            this.mSwipeRefreshLayout.setRefreshing(false);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putInt("state_current_page", this.z);
        bundle.putBoolean("state_is_loading", this.A);
        bundle.putInt("category", this.y.intValue());
        bundle.putParcelable("search_query", this.x);
    }

    public void onViewCreated(View view, Bundle bundle) {
        super.onViewCreated(view, bundle);
        if (bundle != null) {
            this.z = bundle.getInt("state_current_page", 0);
            this.A = bundle.getBoolean("state_is_loading", true);
            Timber.a(String.format("Restoring state: pages 1-%d, was loading - %s", new Object[]{Integer.valueOf(this.z), Boolean.valueOf(this.A)}), new Object[0]);
        }
        this.r = (Toolbar) getActivity().findViewById(R.id.toolbar);
        if (this.s == Type.Search) {
            this.x = (ImdbSearchSuggestionModel.DBean) getArguments().getParcelable("search_query");
        } else {
            this.y = Integer.valueOf(getArguments().getInt("category"));
        }
        this.n.a(true);
        this.mViewAnimator.setDisplayedChildId(this.z == 0 ? R.id.view_loading : R.id.movies_recycler_view);
    }

    public static BrowseMoviesFragment a(Integer num, Type type) {
        Bundle bundle = new Bundle();
        bundle.putInt("category", num.intValue());
        BrowseMoviesFragment browseMoviesFragment = new BrowseMoviesFragment();
        browseMoviesFragment.setArguments(bundle);
        browseMoviesFragment.s = type;
        browseMoviesFragment.t = -1;
        return browseMoviesFragment;
    }

    static /* synthetic */ void b(Observer observer, List list) throws Exception {
        if (!list.isEmpty()) {
            observer.onNext(list);
        }
        observer.onComplete();
    }

    /* access modifiers changed from: protected */
    public void e() {
        super.e();
        a(this.o, this.z);
    }

    private void b(final int i) {
        this.w.onNext(Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            static /* synthetic */ void a(ObservableEmitter observableEmitter, Movie.Response response) throws Exception {
                ArrayList arrayList = new ArrayList();
                for (Movie next : response.movies) {
                    MovieEntity convert = next.convert();
                    convert.setTV(convert.getTV());
                    if (convert.getTV().booleanValue()) {
                        MoviesRepositoryImpl.a(convert, next, Categorys.getTVCategory());
                    } else {
                        MoviesRepositoryImpl.a(convert, next, Categorys.getMVCategory());
                    }
                    arrayList.add(convert);
                }
                observableEmitter.onNext(arrayList);
                observableEmitter.onComplete();
            }

            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                BrowseMoviesFragment browseMoviesFragment = BrowseMoviesFragment.this;
                browseMoviesFragment.p.b(browseMoviesFragment.d.getHDList(i).subscribeOn(Schedulers.b()).subscribe(new j(observableEmitter), new i(observableEmitter)));
            }
        }));
    }

    public static BrowseMoviesFragment a(ImdbSearchSuggestionModel.DBean dBean) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("search_query", dBean);
        BrowseMoviesFragment browseMoviesFragment = new BrowseMoviesFragment();
        browseMoviesFragment.setArguments(bundle);
        browseMoviesFragment.s = Type.Search;
        return browseMoviesFragment;
    }

    private void a(int i) {
        Timber.a(String.format("Page %d is loading.", new Object[]{Integer.valueOf(i)}), new Object[0]);
        Type type = this.s;
        if (type != null) {
            int i2 = AnonymousClass12.f5615a[type.ordinal()];
            if (i2 != 1) {
                if (i2 != 2) {
                    if (i2 != 3) {
                        if (i2 == 4) {
                            this.w.onNext(this.c.b((long) this.y.intValue(), i));
                        } else if (i2 == 5) {
                            this.w.onNext(this.c.a((long) this.y.intValue(), i));
                        }
                    } else if (i < 2) {
                        this.w.onNext(a(this.x, i));
                    }
                } else if (this.y.intValue() >= -2 || this.y.intValue() <= -10000) {
                    if (this.y.intValue() == -1) {
                        b(i);
                    } else if (this.y.intValue() == -2) {
                        a(i, false);
                    } else {
                        this.w.onNext(this.c.a(this.y.intValue(), i, this.t));
                    }
                } else if (this.y.intValue() == -9999) {
                    this.w.onNext(this.c.a(GlobalVariable.c().a().getEvent_category(), i));
                } else if (this.y.intValue() == -9) {
                    this.w.onNext(a(false, i, this.t));
                } else {
                    this.w.onNext(this.c.b(Categorys.getMvExtraList().get(this.y.intValue()), i, this.t));
                }
            } else if (this.y.intValue() < 0) {
                if (this.y.intValue() == -11) {
                    this.w.onNext(a(true, i, this.t));
                } else if (this.y.intValue() == -2) {
                    a(i, true);
                } else if (this.y.intValue() == -10) {
                    this.w.onNext(this.i.a(i).delay(50, TimeUnit.MILLISECONDS));
                } else {
                    this.w.onNext(this.c.a(Categorys.getTvExtraList().get(this.y.intValue()), i, this.t));
                }
            } else if (this.y.intValue() < 93401 || this.y.intValue() > 93408) {
                this.w.onNext(this.c.b(this.y.intValue(), i, this.t));
            } else {
                a(i, (long) this.y.intValue());
            }
        }
    }

    /* access modifiers changed from: package-private */
    public Observable<List<MovieEntity>> a(final boolean z2, final int i, final int i2) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            static /* synthetic */ void a(ObservableEmitter observableEmitter, List list) throws Exception {
                observableEmitter.onNext(list);
                observableEmitter.onComplete();
            }

            static /* synthetic */ void b(ObservableEmitter observableEmitter, List list) throws Exception {
                observableEmitter.onNext(list);
                observableEmitter.onComplete();
            }

            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                if (!z2) {
                    BrowseMoviesFragment browseMoviesFragment = BrowseMoviesFragment.this;
                    browseMoviesFragment.p.b(browseMoviesFragment.c.a(-1, i, i2).subscribeOn(Schedulers.b()).subscribe(new a(observableEmitter), new c(observableEmitter)));
                    return;
                }
                BrowseMoviesFragment browseMoviesFragment2 = BrowseMoviesFragment.this;
                browseMoviesFragment2.p.b(browseMoviesFragment2.c.b(-1, i, i2).subscribeOn(Schedulers.b()).subscribe(new d(observableEmitter), new b(observableEmitter)));
            }
        }).switchIfEmpty(new r(this, z2, i, i2)).subscribeOn(Schedulers.b());
    }

    public /* synthetic */ void a(boolean z2, int i, int i2, Observer observer) {
        if (!z2) {
            this.p.b(this.i.a(-1, i, i2).subscribeOn(Schedulers.b()).subscribe(new t(observer), new y(observer)));
        } else {
            this.p.b(this.i.b(-1, i, i2).subscribeOn(Schedulers.b()).subscribe(new z(observer), new b0(observer)));
        }
    }

    static /* synthetic */ void a(Observer observer, List list) throws Exception {
        observer.onNext(list);
        observer.onComplete();
    }

    /* access modifiers changed from: package-private */
    public Observable<List<MovieEntity>> a(final ImdbSearchSuggestionModel.DBean dBean, final int i) {
        return Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            static /* synthetic */ List a(SearchTMDB searchTMDB) throws Exception {
                try {
                    return TMDBRepositoryImpl.b(searchTMDB.getResults());
                } catch (Exception unused) {
                    return new ArrayList();
                }
            }

            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                BrowseMoviesFragment browseMoviesFragment = BrowseMoviesFragment.this;
                browseMoviesFragment.p.b(browseMoviesFragment.f.discoverMoviesByQuery(dBean.getL(), i).timeout(5, TimeUnit.SECONDS).subscribeOn(Schedulers.b()).map(g.f5677a).subscribe(new e(observableEmitter), new f(observableEmitter)));
            }

            static /* synthetic */ void a(ObservableEmitter observableEmitter, List list) throws Exception {
                if (!list.isEmpty()) {
                    observableEmitter.onNext(list);
                }
                observableEmitter.onComplete();
            }
        }).switchIfEmpty(new h(this, dBean, i)).switchIfEmpty(new x(this, dBean)).switchIfEmpty(new u(this, dBean)).subscribeOn(Schedulers.b());
    }

    public /* synthetic */ void a(ImdbSearchSuggestionModel.DBean dBean, int i, Observer observer) {
        this.p.b(this.i.a(dBean.getL(), String.valueOf(dBean.getY()), i).subscribe(new o(observer), new a0(observer)));
    }

    public /* synthetic */ void a(ImdbSearchSuggestionModel.DBean dBean, Observer observer) {
        this.p.b(TheTvdbRemoteID.a(dBean.getId()).subscribe(new n(observer), new c0(observer)));
    }

    static /* synthetic */ List a(ImdbSearchSuggestionModel.DBean dBean, ResponseBody responseBody) throws Exception {
        ArrayList arrayList = new ArrayList();
        MovieEntity a2 = IMDBUtils.a(dBean.getId(), responseBody, false);
        if (a2 != null) {
            arrayList.add(a2);
        }
        return arrayList;
    }

    /* access modifiers changed from: protected */
    public void a(AppComponent appComponent) {
        DaggerBaseFragmentComponent.a().a(appComponent).a().a(this);
    }

    public void a(int i, int i2) {
        if (this.n.e()) {
            a(i);
        }
    }

    private boolean a(long j) {
        for (Integer intValue : GlobalVariable.c().a().getFringing_movie()) {
            if (((long) intValue.intValue()) == j) {
                return true;
            }
        }
        return false;
    }

    public /* synthetic */ void a(List list) throws Exception {
        this.mSwipeRefreshLayout.setRefreshing(false);
        this.z++;
        int i = this.B;
        int i2 = this.z;
        if (i > i2) {
            this.B = i2;
        }
        Timber.a(String.format("Page %d is loaded, %d new items", new Object[]{Integer.valueOf(this.z), Integer.valueOf(list.size())}), new Object[0]);
        if (this.z == 1) {
            this.n.b();
        }
        this.n.a(true ^ list.isEmpty());
        for (int i3 = 0; i3 < list.size(); i3++) {
            if (!a(((MovieEntity) list.get(i3)).getTmdbID())) {
                this.n.a(list.get(i3));
            }
        }
        this.mViewAnimator.setDisplayedChildId(R.id.movies_recycler_view);
    }

    public /* synthetic */ void a(Throwable th) throws Exception {
        Timber.a(th, "Movies loading failed.", new Object[0]);
        this.mSwipeRefreshLayout.setRefreshing(false);
        if (this.mViewAnimator.getDisplayedChildId() == R.id.movies_recycler_view) {
            this.n.a(false);
            Utils.a((Activity) getActivity(), (int) R.string.view_error_message);
            return;
        }
        this.mViewAnimator.setDisplayedChildId(R.id.view_error);
    }

    private void a(final int i, final long j) {
        this.w.onNext(Observable.create(new ObservableOnSubscribe<ListResult>(this) {
            public void subscribe(ObservableEmitter<ListResult> observableEmitter) throws Exception {
                Response execute = HttpHelper.e().b().newCall(new Request.Builder().url(String.format("https://api.themoviedb.org/4/list/%d?&sort_by=popularity.desc&append_to_response=external_id&page=%d&api_key=%s", new Object[]{Long.valueOf(j), Integer.valueOf(i), FreeMoviesApp.l().getString("last_tmdb_api_key", GlobalVariable.c().a().getTmdb_api_keys().get(0))})).build()).execute();
                if (execute.code() == 200 && execute.body() != null) {
                    ListResult listResult = (ListResult) new Gson().a(execute.body().string(), ListResult.class);
                    if (!(listResult == null || listResult.getResults() == null)) {
                        observableEmitter.onNext(listResult);
                    }
                }
                observableEmitter.onComplete();
            }
        }).subscribeOn(Schedulers.b()).map(p.f5708a));
    }

    static /* synthetic */ List a(ListResult listResult) throws Exception {
        ArrayList arrayList = new ArrayList();
        for (ListResult.ResultsBean next : listResult.getResults()) {
            MovieEntity movieEntity = new MovieEntity();
            movieEntity.setTmdbID((long) next.getId());
            movieEntity.setPoster_path(next.getPoster_path());
            movieEntity.setBackdrop_path(next.getBackdrop_path());
            movieEntity.setName(next.getName());
            movieEntity.setRealeaseDate(next.getFirst_air_date());
            movieEntity.setOverview(next.getOverview());
            movieEntity.setVote(Double.valueOf(next.getVote_average()));
            movieEntity.setTV(Boolean.valueOf(next.getMedia_type().contains("tv")));
            arrayList.add(movieEntity);
        }
        return arrayList;
    }

    private void a(final int i, final boolean z2) {
        this.w.onNext(Observable.create(new ObservableOnSubscribe<List<MovieEntity>>() {
            static /* synthetic */ void a(boolean z, ObservableEmitter observableEmitter, ResponseBody responseBody) throws Exception {
                if (responseBody != null) {
                    ArrayList arrayList = new ArrayList();
                    JSONArray jSONArray = new JSONArray(responseBody.string());
                    Gson gson = new Gson();
                    PrintStream printStream = System.out;
                    printStream.println("subcriveToDVDRelease leng " + jSONArray.length());
                    for (int i = 0; i < jSONArray.length(); i++) {
                        MovieEntity movieEntity = (MovieEntity) gson.a(jSONArray.getString(i), MovieEntity.class);
                        if (z == movieEntity.getTV().booleanValue()) {
                            ArrayList arrayList2 = new ArrayList();
                            for (String next : movieEntity.getGenres()) {
                                if (movieEntity.getTV().booleanValue()) {
                                    String str = Categorys.getTVCategory().get(Integer.valueOf(next).intValue());
                                    if (str != null) {
                                        arrayList2.add(str);
                                    }
                                } else {
                                    String str2 = Categorys.getMVCategory().get(Integer.valueOf(next).intValue());
                                    if (str2 != null) {
                                        arrayList2.add(str2);
                                    }
                                }
                            }
                            movieEntity.setGenres(arrayList2);
                            arrayList.add(movieEntity);
                        }
                    }
                    Collections.reverse(arrayList);
                    observableEmitter.onNext(arrayList);
                    observableEmitter.onComplete();
                }
            }

            public void subscribe(ObservableEmitter<List<MovieEntity>> observableEmitter) throws Exception {
                BrowseMoviesFragment browseMoviesFragment = BrowseMoviesFragment.this;
                browseMoviesFragment.d.getDvdList(i, browseMoviesFragment.u).subscribeOn(Schedulers.b()).subscribe(new l(z2, observableEmitter), new k(observableEmitter));
            }

            static /* synthetic */ void a(ObservableEmitter observableEmitter, Throwable th) throws Exception {
                th.printStackTrace();
                observableEmitter.onComplete();
            }
        }));
    }

    private void a(GridLayoutManager gridLayoutManager, int i) {
        EndlessScrollListener endlessScrollListener = this.v;
        if (endlessScrollListener != null) {
            this.mRecyclerView.removeOnScrollListener(endlessScrollListener);
        }
        this.v = EndlessScrollListener.a(gridLayoutManager, 10, i).a(this);
        this.mRecyclerView.addOnScrollListener(this.v);
    }
}
