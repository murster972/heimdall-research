package com.movie.ui.fragment;

import android.view.View;
import androidx.viewpager.widget.ViewPager;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.google.android.material.tabs.TabLayout;
import com.yoku.marumovie.R;

public class TorrentManagerFragment_ViewBinding implements Unbinder {

    /* renamed from: a  reason: collision with root package name */
    private TorrentManagerFragment f5652a;

    public TorrentManagerFragment_ViewBinding(TorrentManagerFragment torrentManagerFragment, View view) {
        this.f5652a = torrentManagerFragment;
        torrentManagerFragment.tabLayout = (TabLayout) Utils.findRequiredViewAsType(view, R.id.tablayout, "field 'tabLayout'", TabLayout.class);
        torrentManagerFragment.viewPager = (ViewPager) Utils.findRequiredViewAsType(view, R.id.viewpage, "field 'viewPager'", ViewPager.class);
    }

    public void unbind() {
        TorrentManagerFragment torrentManagerFragment = this.f5652a;
        if (torrentManagerFragment != null) {
            this.f5652a = null;
            torrentManagerFragment.tabLayout = null;
            torrentManagerFragment.viewPager = null;
            return;
        }
        throw new IllegalStateException("Bindings already cleared.");
    }
}
