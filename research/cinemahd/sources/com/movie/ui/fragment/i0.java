package com.movie.ui.fragment;

import com.database.entitys.MovieEntity;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class i0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ HistoryFragment f5686a;

    public /* synthetic */ i0(HistoryFragment historyFragment) {
        this.f5686a = historyFragment;
    }

    public final void accept(Object obj) {
        this.f5686a.a((MovieEntity) obj);
    }
}
