package com.movie.ui.fragment;

import com.movie.AppComponent;
import com.movie.ui.activity.movies.overview.MovieOverViewFragment;
import com.movie.ui.activity.movies.stream.StreamFragment;
import com.movie.ui.activity.payment.ChooseProductFragment;
import com.movie.ui.activity.payment.PaymentProcessingFragment;
import com.movie.ui.activity.payment.PaymentResultFragment;
import com.movie.ui.activity.settings.subfragment.PremiumAccountFragment;
import com.movie.ui.activity.shows.episodes.bottomSheet.EpisodeBottomSheetFragment;
import com.movie.ui.activity.shows.episodes.pageviewDialog.EpisodeDetailsFragment;
import com.movie.ui.activity.shows.episodes.pageviewDialog.PageViewDialog;
import com.movie.ui.activity.shows.overview.OverviewFragment;
import com.movie.ui.activity.shows.seasons.SeasonFragment;
import com.movie.ui.customdialog.AddMagnetDialog;
import com.movie.ui.fragment.premium.FilesBottomSheetFragment;
import com.movie.ui.fragment.premium.TorrentAdapterListFragment;
import dagger.Component;
import us.shandian.giga.ui.fragment.MissionsFragment;

@Component(dependencies = {AppComponent.class})
public interface BaseFragmentComponent {
    void a(MovieOverViewFragment movieOverViewFragment);

    void a(StreamFragment streamFragment);

    void a(ChooseProductFragment chooseProductFragment);

    void a(PaymentProcessingFragment paymentProcessingFragment);

    void a(PaymentResultFragment paymentResultFragment);

    void a(PremiumAccountFragment premiumAccountFragment);

    void a(EpisodeBottomSheetFragment episodeBottomSheetFragment);

    void a(EpisodeDetailsFragment episodeDetailsFragment);

    void a(PageViewDialog pageViewDialog);

    void a(OverviewFragment overviewFragment);

    void a(SeasonFragment seasonFragment);

    void a(AddMagnetDialog addMagnetDialog);

    void a(BrowseMoviesFragment browseMoviesFragment);

    void a(FavoredMoviesFragment favoredMoviesFragment);

    void a(FavoredPageFragment favoredPageFragment);

    void a(HistoryFragment historyFragment);

    void a(HistoryPageFragment historyPageFragment);

    void a(MovieFragment movieFragment);

    void a(TorrentManagerFragment torrentManagerFragment);

    void a(FilesBottomSheetFragment filesBottomSheetFragment);

    void a(TorrentAdapterListFragment torrentAdapterListFragment);

    void a(MissionsFragment missionsFragment);
}
