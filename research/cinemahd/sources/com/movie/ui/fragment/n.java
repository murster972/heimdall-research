package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class n implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5702a;

    public /* synthetic */ n(Observer observer) {
        this.f5702a = observer;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.e(this.f5702a, (List) obj);
    }
}
