package com.movie.ui.fragment;

import io.reactivex.ObservableSource;
import io.reactivex.Observer;

/* compiled from: lambda */
public final /* synthetic */ class r implements ObservableSource {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ BrowseMoviesFragment f5770a;
    private final /* synthetic */ boolean b;
    private final /* synthetic */ int c;
    private final /* synthetic */ int d;

    public /* synthetic */ r(BrowseMoviesFragment browseMoviesFragment, boolean z, int i, int i2) {
        this.f5770a = browseMoviesFragment;
        this.b = z;
        this.c = i;
        this.d = i2;
    }

    public final void subscribe(Observer observer) {
        this.f5770a.a(this.b, this.c, this.d, observer);
    }
}
