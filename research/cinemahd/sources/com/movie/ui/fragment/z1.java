package com.movie.ui.fragment;

import com.movie.data.model.tmvdb.MovieTMDB;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class z1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5796a;

    public /* synthetic */ z1(MovieFragment movieFragment) {
        this.f5796a = movieFragment;
    }

    public final void accept(Object obj) {
        this.f5796a.a((MovieTMDB.ResultsBean) obj);
    }
}
