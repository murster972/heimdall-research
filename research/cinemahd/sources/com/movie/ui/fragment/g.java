package com.movie.ui.fragment;

import com.movie.data.model.tmvdb.SearchTMDB;
import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.functions.Function;

/* compiled from: lambda */
public final /* synthetic */ class g implements Function {

    /* renamed from: a  reason: collision with root package name */
    public static final /* synthetic */ g f5677a = new g();

    private /* synthetic */ g() {
    }

    public final Object apply(Object obj) {
        return BrowseMoviesFragment.AnonymousClass2.a((SearchTMDB) obj);
    }
}
