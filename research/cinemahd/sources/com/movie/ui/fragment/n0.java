package com.movie.ui.fragment;

import com.original.tase.model.media.MediaSource;
import io.reactivex.functions.Action;

/* compiled from: lambda */
public final /* synthetic */ class n0 implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5703a;
    private final /* synthetic */ MediaSource b;
    private final /* synthetic */ int c;

    public /* synthetic */ n0(MovieFragment movieFragment, MediaSource mediaSource, int i) {
        this.f5703a = movieFragment;
        this.b = mediaSource;
        this.c = i;
    }

    public final void run() {
        this.f5703a.a(this.b, this.c);
    }
}
