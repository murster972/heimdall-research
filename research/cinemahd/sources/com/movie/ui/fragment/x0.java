package com.movie.ui.fragment;

import io.reactivex.functions.Action;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class x0 implements Action {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5789a;
    private final /* synthetic */ List b;

    public /* synthetic */ x0(MovieFragment movieFragment, List list) {
        this.f5789a = movieFragment;
        this.b = list;
    }

    public final void run() {
        this.f5789a.b(this.b);
    }
}
