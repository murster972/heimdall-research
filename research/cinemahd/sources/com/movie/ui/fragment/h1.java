package com.movie.ui.fragment;

import com.movie.data.model.cinema.Video;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class h1 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ MovieFragment f5683a;

    public /* synthetic */ h1(MovieFragment movieFragment) {
        this.f5683a = movieFragment;
    }

    public final void accept(Object obj) {
        this.f5683a.a((Video.Response) obj);
    }
}
