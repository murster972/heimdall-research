package com.movie.ui.fragment;

import com.database.MvDatabase;
import com.movie.AppComponent;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.realdebrid.RealDebridApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.activity.movies.overview.MovieOverViewFragment;
import com.movie.ui.activity.movies.overview.MovieOverViewFragment_MembersInjector;
import com.movie.ui.activity.movies.stream.StreamFragment;
import com.movie.ui.activity.payment.ChooseProductFragment;
import com.movie.ui.activity.payment.ChooseProductFragment_MembersInjector;
import com.movie.ui.activity.payment.PaymentProcessingFragment;
import com.movie.ui.activity.payment.PaymentProcessingFragment_MembersInjector;
import com.movie.ui.activity.payment.PaymentResultFragment;
import com.movie.ui.activity.payment.PaymentResultFragment_MembersInjector;
import com.movie.ui.activity.settings.subfragment.PremiumAccountFragment;
import com.movie.ui.activity.settings.subfragment.PremiumAccountFragment_MembersInjector;
import com.movie.ui.activity.shows.episodes.bottomSheet.EpisodeBottomSheetFragment;
import com.movie.ui.activity.shows.episodes.bottomSheet.EpisodeBottomSheetFragment_MembersInjector;
import com.movie.ui.activity.shows.episodes.pageviewDialog.EpisodeDetailsFragment;
import com.movie.ui.activity.shows.episodes.pageviewDialog.PageViewDialog;
import com.movie.ui.activity.shows.episodes.pageviewDialog.PageViewDialog_MembersInjector;
import com.movie.ui.activity.shows.overview.OverviewFragment;
import com.movie.ui.activity.shows.overview.OverviewFragment_MembersInjector;
import com.movie.ui.activity.shows.seasons.SeasonFragment;
import com.movie.ui.activity.shows.seasons.SeasonFragment_MembersInjector;
import com.movie.ui.customdialog.AddMagnetDialog;
import com.movie.ui.customdialog.AddMagnetDialog_MembersInjector;
import com.movie.ui.fragment.premium.FilesBottomSheetFragment;
import com.movie.ui.fragment.premium.FilesBottomSheetFragment_MembersInjector;
import com.movie.ui.fragment.premium.TorrentAdapterListFragment;
import com.movie.ui.fragment.premium.TorrentAdapterListFragment_MembersInjector;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.internal.Preconditions;
import okhttp3.OkHttpClient;
import us.shandian.giga.ui.fragment.MissionsFragment;
import us.shandian.giga.ui.fragment.MissionsFragment_MembersInjector;

public final class DaggerBaseFragmentComponent implements BaseFragmentComponent {

    /* renamed from: a  reason: collision with root package name */
    private AppComponent f5622a;

    public static final class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public AppComponent f5623a;

        private Builder() {
        }

        public BaseFragmentComponent a() {
            if (this.f5623a != null) {
                return new DaggerBaseFragmentComponent(this);
            }
            throw new IllegalStateException(AppComponent.class.getCanonicalName() + " must be set");
        }

        public Builder a(AppComponent appComponent) {
            Preconditions.a(appComponent);
            this.f5623a = appComponent;
            return this;
        }
    }

    public static Builder a() {
        return new Builder();
    }

    private BrowseMoviesFragment b(BrowseMoviesFragment browseMoviesFragment) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, f);
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, d);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, a2);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, e);
        IMDBApi h = this.f5622a.h();
        Preconditions.a(h, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, h);
        TheTvdb g = this.f5622a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, g);
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) browseMoviesFragment, c);
        return browseMoviesFragment;
    }

    public void a(StreamFragment streamFragment) {
    }

    public void a(EpisodeDetailsFragment episodeDetailsFragment) {
    }

    public void a(FavoredPageFragment favoredPageFragment) {
    }

    public void a(HistoryPageFragment historyPageFragment) {
    }

    private DaggerBaseFragmentComponent(Builder builder) {
        a(builder);
    }

    private void a(Builder builder) {
        this.f5622a = builder.f5623a;
    }

    public void a(BrowseMoviesFragment browseMoviesFragment) {
        b(browseMoviesFragment);
    }

    public void a(MovieFragment movieFragment) {
        b(movieFragment);
    }

    public void a(FavoredMoviesFragment favoredMoviesFragment) {
        b(favoredMoviesFragment);
    }

    public void a(HistoryFragment historyFragment) {
        b(historyFragment);
    }

    public void a(MissionsFragment missionsFragment) {
        b(missionsFragment);
    }

    public void a(SeasonFragment seasonFragment) {
        b(seasonFragment);
    }

    public void a(EpisodeBottomSheetFragment episodeBottomSheetFragment) {
        b(episodeBottomSheetFragment);
    }

    public void a(PageViewDialog pageViewDialog) {
        b(pageViewDialog);
    }

    public void a(AddMagnetDialog addMagnetDialog) {
        b(addMagnetDialog);
    }

    public void a(OverviewFragment overviewFragment) {
        b(overviewFragment);
    }

    public void a(ChooseProductFragment chooseProductFragment) {
        b(chooseProductFragment);
    }

    public void a(PaymentProcessingFragment paymentProcessingFragment) {
        b(paymentProcessingFragment);
    }

    public void a(PaymentResultFragment paymentResultFragment) {
        b(paymentResultFragment);
    }

    public void a(TorrentManagerFragment torrentManagerFragment) {
        b(torrentManagerFragment);
    }

    public void a(TorrentAdapterListFragment torrentAdapterListFragment) {
        b(torrentAdapterListFragment);
    }

    public void a(FilesBottomSheetFragment filesBottomSheetFragment) {
        b(filesBottomSheetFragment);
    }

    public void a(MovieOverViewFragment movieOverViewFragment) {
        b(movieOverViewFragment);
    }

    public void a(PremiumAccountFragment premiumAccountFragment) {
        b(premiumAccountFragment);
    }

    private MovieFragment b(MovieFragment movieFragment) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        MovieFragment_MembersInjector.a(movieFragment, f);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        MovieFragment_MembersInjector.a(movieFragment, a2);
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        MovieFragment_MembersInjector.a(movieFragment, d);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MovieFragment_MembersInjector.a(movieFragment, e);
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        MovieFragment_MembersInjector.a(movieFragment, c);
        OkHttpClient k = this.f5622a.k();
        Preconditions.a(k, "Cannot return null from a non-@Nullable component method");
        MovieFragment_MembersInjector.a(movieFragment, k);
        return movieFragment;
    }

    private FavoredMoviesFragment b(FavoredMoviesFragment favoredMoviesFragment) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, f);
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, d);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, a2);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, e);
        IMDBApi h = this.f5622a.h();
        Preconditions.a(h, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, h);
        TheTvdb g = this.f5622a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, g);
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) favoredMoviesFragment, c);
        MvDatabase a3 = this.f5622a.a();
        Preconditions.a(a3, "Cannot return null from a non-@Nullable component method");
        FavoredMoviesFragment_MembersInjector.a(favoredMoviesFragment, a3);
        TMDBApi e2 = this.f5622a.e();
        Preconditions.a(e2, "Cannot return null from a non-@Nullable component method");
        FavoredMoviesFragment_MembersInjector.a(favoredMoviesFragment, e2);
        return favoredMoviesFragment;
    }

    private HistoryFragment b(HistoryFragment historyFragment) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, f);
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, d);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, a2);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, e);
        IMDBApi h = this.f5622a.h();
        Preconditions.a(h, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, h);
        TheTvdb g = this.f5622a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, g);
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        MoviesFragment_MembersInjector.a((MoviesFragment) historyFragment, c);
        MvDatabase a3 = this.f5622a.a();
        Preconditions.a(a3, "Cannot return null from a non-@Nullable component method");
        HistoryFragment_MembersInjector.a(historyFragment, a3);
        TMDBApi e2 = this.f5622a.e();
        Preconditions.a(e2, "Cannot return null from a non-@Nullable component method");
        HistoryFragment_MembersInjector.a(historyFragment, e2);
        return historyFragment;
    }

    private MissionsFragment b(MissionsFragment missionsFragment) {
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        MissionsFragment_MembersInjector.a(missionsFragment, c);
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        MissionsFragment_MembersInjector.a(missionsFragment, f);
        return missionsFragment;
    }

    private SeasonFragment b(SeasonFragment seasonFragment) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        SeasonFragment_MembersInjector.a(seasonFragment, f);
        TheTvdb g = this.f5622a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        SeasonFragment_MembersInjector.a(seasonFragment, g);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        SeasonFragment_MembersInjector.a(seasonFragment, a2);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        SeasonFragment_MembersInjector.a(seasonFragment, e);
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        SeasonFragment_MembersInjector.a(seasonFragment, d);
        return seasonFragment;
    }

    private EpisodeBottomSheetFragment b(EpisodeBottomSheetFragment episodeBottomSheetFragment) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        EpisodeBottomSheetFragment_MembersInjector.a(episodeBottomSheetFragment, f);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        EpisodeBottomSheetFragment_MembersInjector.a(episodeBottomSheetFragment, a2);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        EpisodeBottomSheetFragment_MembersInjector.a(episodeBottomSheetFragment, e);
        return episodeBottomSheetFragment;
    }

    private PageViewDialog b(PageViewDialog pageViewDialog) {
        MoviesRepository f = this.f5622a.f();
        Preconditions.a(f, "Cannot return null from a non-@Nullable component method");
        PageViewDialog_MembersInjector.a(pageViewDialog, f);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        PageViewDialog_MembersInjector.a(pageViewDialog, a2);
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        PageViewDialog_MembersInjector.a(pageViewDialog, e);
        return pageViewDialog;
    }

    private AddMagnetDialog b(AddMagnetDialog addMagnetDialog) {
        RealDebridApi b = this.f5622a.b();
        Preconditions.a(b, "Cannot return null from a non-@Nullable component method");
        AddMagnetDialog_MembersInjector.a(addMagnetDialog, b);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        AddMagnetDialog_MembersInjector.a(addMagnetDialog, a2);
        return addMagnetDialog;
    }

    private OverviewFragment b(OverviewFragment overviewFragment) {
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        OverviewFragment_MembersInjector.a(overviewFragment, e);
        TheTvdb g = this.f5622a.g();
        Preconditions.a(g, "Cannot return null from a non-@Nullable component method");
        OverviewFragment_MembersInjector.a(overviewFragment, g);
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        OverviewFragment_MembersInjector.a(overviewFragment, c);
        return overviewFragment;
    }

    private ChooseProductFragment b(ChooseProductFragment chooseProductFragment) {
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        ChooseProductFragment_MembersInjector.a(chooseProductFragment, d);
        return chooseProductFragment;
    }

    private PaymentProcessingFragment b(PaymentProcessingFragment paymentProcessingFragment) {
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        PaymentProcessingFragment_MembersInjector.a(paymentProcessingFragment, d);
        return paymentProcessingFragment;
    }

    private PaymentResultFragment b(PaymentResultFragment paymentResultFragment) {
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        PaymentResultFragment_MembersInjector.a(paymentResultFragment, d);
        return paymentResultFragment;
    }

    private TorrentManagerFragment b(TorrentManagerFragment torrentManagerFragment) {
        RealDebridApi b = this.f5622a.b();
        Preconditions.a(b, "Cannot return null from a non-@Nullable component method");
        TorrentManagerFragment_MembersInjector.a(torrentManagerFragment, b);
        return torrentManagerFragment;
    }

    private TorrentAdapterListFragment b(TorrentAdapterListFragment torrentAdapterListFragment) {
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        TorrentAdapterListFragment_MembersInjector.a(torrentAdapterListFragment, a2);
        RealDebridApi b = this.f5622a.b();
        Preconditions.a(b, "Cannot return null from a non-@Nullable component method");
        TorrentAdapterListFragment_MembersInjector.a(torrentAdapterListFragment, b);
        return torrentAdapterListFragment;
    }

    private FilesBottomSheetFragment b(FilesBottomSheetFragment filesBottomSheetFragment) {
        MoviesHelper c = this.f5622a.c();
        Preconditions.a(c, "Cannot return null from a non-@Nullable component method");
        FilesBottomSheetFragment_MembersInjector.a(filesBottomSheetFragment, c);
        RealDebridApi b = this.f5622a.b();
        Preconditions.a(b, "Cannot return null from a non-@Nullable component method");
        FilesBottomSheetFragment_MembersInjector.a(filesBottomSheetFragment, b);
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        FilesBottomSheetFragment_MembersInjector.a(filesBottomSheetFragment, a2);
        return filesBottomSheetFragment;
    }

    private MovieOverViewFragment b(MovieOverViewFragment movieOverViewFragment) {
        TMDBApi e = this.f5622a.e();
        Preconditions.a(e, "Cannot return null from a non-@Nullable component method");
        MovieOverViewFragment_MembersInjector.a(movieOverViewFragment, e);
        return movieOverViewFragment;
    }

    private PremiumAccountFragment b(PremiumAccountFragment premiumAccountFragment) {
        MvDatabase a2 = this.f5622a.a();
        Preconditions.a(a2, "Cannot return null from a non-@Nullable component method");
        PremiumAccountFragment_MembersInjector.a(premiumAccountFragment, a2);
        RealDebridApi b = this.f5622a.b();
        Preconditions.a(b, "Cannot return null from a non-@Nullable component method");
        PremiumAccountFragment_MembersInjector.a(premiumAccountFragment, b);
        MoviesApi d = this.f5622a.d();
        Preconditions.a(d, "Cannot return null from a non-@Nullable component method");
        PremiumAccountFragment_MembersInjector.a(premiumAccountFragment, d);
        return premiumAccountFragment;
    }
}
