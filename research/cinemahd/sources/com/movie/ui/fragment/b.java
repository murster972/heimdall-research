package com.movie.ui.fragment;

import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class b implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ ObservableEmitter f5657a;

    public /* synthetic */ b(ObservableEmitter observableEmitter) {
        this.f5657a = observableEmitter;
    }

    public final void accept(Object obj) {
        this.f5657a.onComplete();
    }
}
