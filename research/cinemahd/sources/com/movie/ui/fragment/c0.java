package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class c0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5662a;

    public /* synthetic */ c0(Observer observer) {
        this.f5662a = observer;
    }

    public final void accept(Object obj) {
        this.f5662a.onComplete();
    }
}
