package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;
import java.util.List;

/* compiled from: lambda */
public final /* synthetic */ class o implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5705a;

    public /* synthetic */ o(Observer observer) {
        this.f5705a = observer;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.d(this.f5705a, (List) obj);
    }
}
