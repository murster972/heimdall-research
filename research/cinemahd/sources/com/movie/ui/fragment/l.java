package com.movie.ui.fragment;

import com.movie.ui.fragment.BrowseMoviesFragment;
import io.reactivex.ObservableEmitter;
import io.reactivex.functions.Consumer;
import okhttp3.ResponseBody;

/* compiled from: lambda */
public final /* synthetic */ class l implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ boolean f5696a;
    private final /* synthetic */ ObservableEmitter b;

    public /* synthetic */ l(boolean z, ObservableEmitter observableEmitter) {
        this.f5696a = z;
        this.b = observableEmitter;
    }

    public final void accept(Object obj) {
        BrowseMoviesFragment.AnonymousClass5.a(this.f5696a, this.b, (ResponseBody) obj);
    }
}
