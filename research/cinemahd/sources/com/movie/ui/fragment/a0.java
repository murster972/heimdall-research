package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class a0 implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5654a;

    public /* synthetic */ a0(Observer observer) {
        this.f5654a = observer;
    }

    public final void accept(Object obj) {
        this.f5654a.onComplete();
    }
}
