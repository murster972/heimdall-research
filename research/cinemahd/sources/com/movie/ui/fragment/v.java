package com.movie.ui.fragment;

import io.reactivex.Observer;
import io.reactivex.functions.Consumer;

/* compiled from: lambda */
public final /* synthetic */ class v implements Consumer {

    /* renamed from: a  reason: collision with root package name */
    private final /* synthetic */ Observer f5782a;

    public /* synthetic */ v(Observer observer) {
        this.f5782a = observer;
    }

    public final void accept(Object obj) {
        this.f5782a.onComplete();
    }
}
