package com.movie.ui.fragment;

import com.database.MvDatabase;
import com.movie.data.api.MoviesApi;
import com.movie.data.api.imdb.IMDBApi;
import com.movie.data.api.tmdb.TMDBApi;
import com.movie.data.repository.MoviesRepository;
import com.movie.ui.helper.MoviesHelper;
import com.uwetrottmann.thetvdb.TheTvdb;
import dagger.MembersInjector;

public final class MoviesFragment_MembersInjector implements MembersInjector<MoviesFragment> {
    public static void a(MoviesFragment moviesFragment, MoviesRepository moviesRepository) {
        moviesFragment.c = moviesRepository;
    }

    public static void a(MoviesFragment moviesFragment, MoviesApi moviesApi) {
        moviesFragment.d = moviesApi;
    }

    public static void a(MoviesFragment moviesFragment, MvDatabase mvDatabase) {
        moviesFragment.e = mvDatabase;
    }

    public static void a(MoviesFragment moviesFragment, TMDBApi tMDBApi) {
        moviesFragment.f = tMDBApi;
    }

    public static void a(MoviesFragment moviesFragment, IMDBApi iMDBApi) {
        moviesFragment.g = iMDBApi;
    }

    public static void a(MoviesFragment moviesFragment, TheTvdb theTvdb) {
        moviesFragment.h = theTvdb;
    }

    public static void a(MoviesFragment moviesFragment, MoviesHelper moviesHelper) {
        moviesFragment.l = moviesHelper;
    }
}
