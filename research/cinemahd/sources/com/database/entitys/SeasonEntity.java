package com.database.entitys;

import android.os.Parcel;
import android.os.Parcelable;

public class SeasonEntity implements Parcelable, Comparable<SeasonEntity> {
    public static final Parcelable.Creator<SeasonEntity> CREATOR = new Parcelable.Creator<SeasonEntity>() {
        public SeasonEntity createFromParcel(Parcel parcel) {
            return new SeasonEntity(parcel);
        }

        public SeasonEntity[] newArray(int i) {
            return new SeasonEntity[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private int f2515a;
    private int b;
    private int c;
    private String d;
    private String e;
    private String f;
    private double g;
    private String h;

    public SeasonEntity() {
    }

    public void a(int i) {
        this.b = i;
    }

    public void b(String str) {
        this.h = str;
    }

    public void c(String str) {
        this.f = str;
    }

    public String d() {
        return this.h;
    }

    public int describeContents() {
        return 0;
    }

    public String e() {
        return this.f;
    }

    public String f() {
        return this.d;
    }

    public int g() {
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f2515a);
        parcel.writeInt(this.b);
        parcel.writeInt(this.c);
        parcel.writeString(this.d);
        parcel.writeString(this.e);
        parcel.writeString(this.f);
        parcel.writeDouble(this.g);
        parcel.writeString(this.h);
    }

    protected SeasonEntity(Parcel parcel) {
        this.f2515a = parcel.readInt();
        this.b = parcel.readInt();
        this.c = parcel.readInt();
        this.d = parcel.readString();
        this.e = parcel.readString();
        this.f = parcel.readString();
        this.g = parcel.readDouble();
        this.h = parcel.readString();
    }

    public String a() {
        return this.e;
    }

    public void b(int i) {
        this.f2515a = i;
    }

    public int c() {
        return this.f2515a;
    }

    public void d(String str) {
        this.d = str;
    }

    public void a(String str) {
        this.e = str;
    }

    public int b() {
        return this.b;
    }

    public void c(int i) {
        this.c = i;
    }

    /* renamed from: a */
    public int compareTo(SeasonEntity seasonEntity) {
        return g() > seasonEntity.g() ? 1 : -1;
    }
}
