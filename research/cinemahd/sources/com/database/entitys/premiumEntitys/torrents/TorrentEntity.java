package com.database.entitys.premiumEntitys.torrents;

import android.os.Parcel;
import android.os.Parcelable;
import com.movie.data.model.TorrentObject;
import java.util.List;

public class TorrentEntity implements Parcelable {
    public static final Parcelable.Creator<TorrentEntity> CREATOR = new Parcelable.Creator<TorrentEntity>() {
        public TorrentEntity createFromParcel(Parcel parcel) {
            return new TorrentEntity(parcel);
        }

        public TorrentEntity[] newArray(int i) {
            return new TorrentEntity[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    String f2518a;
    String b;
    TorrentObject.Type c;
    List<String> d;
    int e;

    public TorrentEntity() {
    }

    public void a(String str) {
        this.f2518a = str;
    }

    public void b(String str) {
        this.b = str;
    }

    public String c() {
        return this.b;
    }

    public int d() {
        return this.e;
    }

    public int describeContents() {
        return 0;
    }

    public TorrentObject.Type e() {
        return this.c;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.f2518a);
        parcel.writeString(this.b);
        parcel.writeStringList(this.d);
        parcel.writeInt(this.e);
    }

    protected TorrentEntity(Parcel parcel) {
        this.f2518a = parcel.readString();
        this.b = parcel.readString();
        this.d = parcel.createStringArrayList();
        this.e = parcel.readInt();
    }

    public List<String> a() {
        return this.d;
    }

    public String b() {
        return this.f2518a;
    }

    public void a(List<String> list) {
        this.d = list;
    }

    public void a(int i) {
        this.e = i;
    }

    public void a(TorrentObject.Type type) {
        this.c = type;
    }
}
