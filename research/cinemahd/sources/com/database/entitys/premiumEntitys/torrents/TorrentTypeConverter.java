package com.database.entitys.premiumEntitys.torrents;

import com.movie.data.model.TorrentObject;

public class TorrentTypeConverter {

    /* renamed from: com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter$1  reason: invalid class name */
    static /* synthetic */ class AnonymousClass1 {

        /* renamed from: a  reason: collision with root package name */
        static final /* synthetic */ int[] f2519a = new int[TorrentObject.Type.values().length];

        /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
        /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x0014 */
        static {
            /*
                com.movie.data.model.TorrentObject$Type[] r0 = com.movie.data.model.TorrentObject.Type.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                f2519a = r0
                int[] r0 = f2519a     // Catch:{ NoSuchFieldError -> 0x0014 }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.AD     // Catch:{ NoSuchFieldError -> 0x0014 }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x0014 }
                r2 = 1
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x0014 }
            L_0x0014:
                int[] r0 = f2519a     // Catch:{ NoSuchFieldError -> 0x001f }
                com.movie.data.model.TorrentObject$Type r1 = com.movie.data.model.TorrentObject.Type.PM     // Catch:{ NoSuchFieldError -> 0x001f }
                int r1 = r1.ordinal()     // Catch:{ NoSuchFieldError -> 0x001f }
                r2 = 2
                r0[r1] = r2     // Catch:{ NoSuchFieldError -> 0x001f }
            L_0x001f:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter.AnonymousClass1.<clinit>():void");
        }
    }

    public static String a(TorrentObject.Type type) {
        int i = AnonymousClass1.f2519a[type.ordinal()];
        if (i != 1) {
            return i != 2 ? "RD" : "PM";
        }
        return "AD";
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0025  */
    /* JADX WARNING: Removed duplicated region for block: B:17:0x002d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static com.movie.data.model.TorrentObject.Type a(java.lang.String r3) {
        /*
            int r0 = r3.hashCode()
            r1 = 2083(0x823, float:2.919E-42)
            r2 = 1
            if (r0 == r1) goto L_0x0018
            r1 = 2557(0x9fd, float:3.583E-42)
            if (r0 == r1) goto L_0x000e
            goto L_0x0022
        L_0x000e:
            java.lang.String r0 = "PM"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0022
            r3 = 0
            goto L_0x0023
        L_0x0018:
            java.lang.String r0 = "AD"
            boolean r3 = r3.equals(r0)
            if (r3 == 0) goto L_0x0022
            r3 = 1
            goto L_0x0023
        L_0x0022:
            r3 = -1
        L_0x0023:
            if (r3 == 0) goto L_0x002d
            if (r3 == r2) goto L_0x002a
            com.movie.data.model.TorrentObject$Type r3 = com.movie.data.model.TorrentObject.Type.RD
            return r3
        L_0x002a:
            com.movie.data.model.TorrentObject$Type r3 = com.movie.data.model.TorrentObject.Type.AD
            return r3
        L_0x002d:
            com.movie.data.model.TorrentObject$Type r3 = com.movie.data.model.TorrentObject.Type.PM
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter.a(java.lang.String):com.movie.data.model.TorrentObject$Type");
    }
}
