package com.database.entitys.premiumEntitys;

import android.os.Parcel;
import android.os.Parcelable;

public class RealDebridEntity implements Parcelable, Cloneable {
    public static final Parcelable.Creator<RealDebridEntity> CREATOR = new Parcelable.Creator<RealDebridEntity>() {
        public RealDebridEntity createFromParcel(Parcel parcel) {
            return new RealDebridEntity(parcel);
        }

        public RealDebridEntity[] newArray(int i) {
            return new RealDebridEntity[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private long f2517a = 0;
    private String b = null;
    private long c = 0;
    private long d = 0;
    private int e = -1;
    private int f = -1;
    private double g;
    private String h;
    private String i;
    private String j;
    private String k;
    private long l;
    private String m;

    public RealDebridEntity() {
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeString(this.i);
        parcel.writeLong(this.f2517a);
        parcel.writeString(this.b);
        parcel.writeLong(this.c);
        parcel.writeLong(this.d);
        parcel.writeInt(this.e);
        parcel.writeInt(this.f);
        parcel.writeDouble(this.g);
        parcel.writeString(this.h);
        parcel.writeString(this.j);
        parcel.writeString(this.k);
        parcel.writeLong(this.l);
        parcel.writeString(this.m);
    }

    public RealDebridEntity clone() {
        Parcel obtain = Parcel.obtain();
        writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        Parcel obtain2 = Parcel.obtain();
        obtain2.unmarshall(marshall, 0, marshall.length);
        obtain2.setDataPosition(0);
        return CREATOR.createFromParcel(obtain2);
    }

    protected RealDebridEntity(Parcel parcel) {
        this.i = parcel.readString();
        this.f2517a = parcel.readLong();
        this.b = parcel.readString();
        this.c = parcel.readLong();
        this.d = parcel.readLong();
        this.e = parcel.readInt();
        this.f = parcel.readInt();
        this.g = parcel.readDouble();
        this.h = parcel.readString();
        this.j = parcel.readString();
        this.k = parcel.readString();
        this.l = parcel.readLong();
        this.m = parcel.readString();
    }
}
