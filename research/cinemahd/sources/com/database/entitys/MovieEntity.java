package com.database.entitys;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;
import java.util.List;
import org.threeten.bp.Instant;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneOffset;

public class MovieEntity implements Parcelable, Cloneable {
    public static final Parcelable.Creator<MovieEntity> CREATOR = new Parcelable.Creator<MovieEntity>() {
        public MovieEntity createFromParcel(Parcel parcel) {
            return new MovieEntity(parcel);
        }

        public MovieEntity[] newArray(int i) {
            return new MovieEntity[i];
        }
    };
    private String backdrop_path;
    private OffsetDateTime collected_at;
    @Deprecated
    private OffsetDateTime createdDate;
    private long duration;
    private List<String> genres;
    private int id;
    private String imdbIDStr;
    @Deprecated
    private Boolean isFavorite;
    private Boolean isTV;
    @Deprecated
    private Boolean isWatched;
    private String name;
    private int numberSeason;
    private String overview;
    private long position;
    private String poster_path;
    private String realeaseDate;
    private String subtitlepath;
    private long tmdbID;
    private long traktID;
    private long tvdbID;
    private Double vote;
    private OffsetDateTime watched_at;

    public static class Converter {
        public static OffsetDateTime a(Long l) {
            if (l == null) {
                return null;
            }
            return OffsetDateTime.ofInstant(Instant.ofEpochMilli(l.longValue()), ZoneOffset.UTC);
        }

        public static String a(List<String> list) {
            if (list == null || list.size() <= 0) {
                return "";
            }
            StringBuilder sb = new StringBuilder();
            for (String append : list) {
                sb.append(append);
                sb.append(",");
            }
            String sb2 = sb.toString();
            return sb2.substring(0, sb2.length() - 1);
        }

        public static List<String> a(String str) {
            return Arrays.asList(str.split(","));
        }
    }

    public MovieEntity() {
        this.subtitlepath = "";
        this.vote = Double.valueOf(0.0d);
        this.isFavorite = false;
        this.isWatched = false;
        this.isTV = false;
    }

    public int describeContents() {
        return 0;
    }

    public String getBackdrop_path() {
        return this.backdrop_path;
    }

    public OffsetDateTime getCollected_at() {
        return this.collected_at;
    }

    @Deprecated
    public OffsetDateTime getCreatedDate() {
        return this.createdDate;
    }

    public long getDuration() {
        return this.duration;
    }

    @Deprecated
    public Boolean getFavorite() {
        return this.isFavorite;
    }

    public List<String> getGenres() {
        return this.genres;
    }

    public int getId() {
        return this.id;
    }

    public String getImdbIDStr() {
        return this.imdbIDStr;
    }

    public String getName() {
        return this.name;
    }

    public int getNumberSeason() {
        return this.numberSeason;
    }

    public String getOverview() {
        return this.overview;
    }

    public long getPosition() {
        return this.position;
    }

    public String getPoster_path() {
        return this.poster_path;
    }

    public String getRealeaseDate() {
        return this.realeaseDate;
    }

    public String getSubtitlepath() {
        return this.subtitlepath;
    }

    public Boolean getTV() {
        return this.isTV;
    }

    public long getTmdbID() {
        return this.tmdbID;
    }

    public long getTraktID() {
        return this.traktID;
    }

    public long getTvdbID() {
        return this.tvdbID;
    }

    public Double getVote() {
        return this.vote;
    }

    @Deprecated
    public Boolean getWatched() {
        return this.isWatched;
    }

    public OffsetDateTime getWatched_at() {
        return this.watched_at;
    }

    public void setBackdrop_path(String str) {
        this.backdrop_path = str;
    }

    public void setCollected_at(OffsetDateTime offsetDateTime) {
        this.collected_at = offsetDateTime;
    }

    @Deprecated
    public void setCreatedDate(OffsetDateTime offsetDateTime) {
        this.createdDate = offsetDateTime;
    }

    public void setDuration(long j) {
        this.duration = j;
    }

    @Deprecated
    public void setFavorite(Boolean bool) {
        this.isFavorite = bool;
    }

    public void setGenres(List<String> list) {
        this.genres = list;
    }

    public void setId(int i) {
        this.id = i;
    }

    public void setImdbIDStr(String str) {
        this.imdbIDStr = str;
    }

    public void setName(String str) {
        this.name = str;
    }

    public void setNumberSeason(int i) {
        this.numberSeason = i;
    }

    public void setOverview(String str) {
        this.overview = str;
    }

    public void setPosition(long j) {
        this.position = j;
    }

    public void setPoster_path(String str) {
        this.poster_path = str;
    }

    public void setRealeaseDate(String str) {
        this.realeaseDate = str;
    }

    public void setSubtitlepath(String str) {
        this.subtitlepath = str;
    }

    public void setTV(Boolean bool) {
        this.isTV = bool;
    }

    public void setTmdbID(long j) {
        this.tmdbID = j;
    }

    public void setTraktID(long j) {
        this.traktID = j;
    }

    public void setTvdbID(long j) {
        this.tvdbID = j;
    }

    public void setVote(Double d) {
        this.vote = d;
    }

    @Deprecated
    public void setWatched(Boolean bool) {
        this.isWatched = bool;
    }

    public void setWatched_at(OffsetDateTime offsetDateTime) {
        this.watched_at = offsetDateTime;
    }

    public String toString() {
        return "MovieEntity{id=" + this.id + ", tmdbID=" + this.tmdbID + ", imdbIDStr='" + this.imdbIDStr + '\'' + ", traktID=" + this.traktID + ", tvdbID=" + this.tvdbID + ", position=" + this.position + ", duration=" + this.duration + ", subtitlepath='" + this.subtitlepath + '\'' + ", poster_path='" + this.poster_path + '\'' + ", backdrop_path='" + this.backdrop_path + '\'' + ", name='" + this.name + '\'' + ", realeaseDate='" + this.realeaseDate + '\'' + ", overview='" + this.overview + '\'' + ", genres=" + this.genres + ", vote=" + this.vote + ", createdDate=" + this.createdDate + ", isFavorite=" + this.isFavorite + ", isTV=" + this.isTV + ", numberSeason=" + this.numberSeason + '}';
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.id);
        parcel.writeLong(this.tmdbID);
        parcel.writeString(this.imdbIDStr);
        parcel.writeLong(this.traktID);
        parcel.writeLong(this.tvdbID);
        parcel.writeLong(this.position);
        parcel.writeLong(this.duration);
        parcel.writeString(this.subtitlepath);
        parcel.writeString(this.poster_path);
        parcel.writeString(this.backdrop_path);
        parcel.writeString(this.name);
        parcel.writeString(this.realeaseDate);
        parcel.writeString(this.overview);
        parcel.writeStringList(this.genres);
        int i2 = 0;
        if (this.vote == null) {
            parcel.writeByte((byte) 0);
        } else {
            parcel.writeByte((byte) 1);
            parcel.writeDouble(this.vote.doubleValue());
        }
        Boolean bool = this.isFavorite;
        parcel.writeByte((byte) (bool == null ? 0 : bool.booleanValue() ? 1 : 2));
        Boolean bool2 = this.isWatched;
        parcel.writeByte((byte) (bool2 == null ? 0 : bool2.booleanValue() ? 1 : 2));
        Boolean bool3 = this.isTV;
        if (bool3 != null) {
            i2 = bool3.booleanValue() ? 1 : 2;
        }
        parcel.writeByte((byte) i2);
        parcel.writeInt(this.numberSeason);
        OffsetDateTime offsetDateTime = this.collected_at;
        long j = 0;
        parcel.writeLong(offsetDateTime == null ? 0 : offsetDateTime.toEpochSecond());
        OffsetDateTime offsetDateTime2 = this.watched_at;
        if (offsetDateTime2 != null) {
            j = offsetDateTime2.toEpochSecond();
        }
        parcel.writeLong(j);
    }

    public MovieEntity clone() {
        Parcel obtain = Parcel.obtain();
        writeToParcel(obtain, 0);
        byte[] marshall = obtain.marshall();
        Parcel obtain2 = Parcel.obtain();
        obtain2.unmarshall(marshall, 0, marshall.length);
        obtain2.setDataPosition(0);
        return CREATOR.createFromParcel(obtain2);
    }

    protected MovieEntity(Parcel parcel) {
        Boolean bool;
        Boolean bool2;
        Boolean bool3;
        OffsetDateTime offsetDateTime;
        this.subtitlepath = "";
        this.vote = Double.valueOf(0.0d);
        boolean z = false;
        this.isFavorite = false;
        this.isWatched = false;
        this.isTV = false;
        this.id = parcel.readInt();
        this.tmdbID = parcel.readLong();
        this.imdbIDStr = parcel.readString();
        this.traktID = parcel.readLong();
        this.tvdbID = parcel.readLong();
        this.position = parcel.readLong();
        this.duration = parcel.readLong();
        this.subtitlepath = parcel.readString();
        this.poster_path = parcel.readString();
        this.backdrop_path = parcel.readString();
        this.name = parcel.readString();
        this.realeaseDate = parcel.readString();
        this.overview = parcel.readString();
        this.genres = parcel.createStringArrayList();
        OffsetDateTime offsetDateTime2 = null;
        if (parcel.readByte() == 0) {
            this.vote = null;
        } else {
            this.vote = Double.valueOf(parcel.readDouble());
        }
        byte readByte = parcel.readByte();
        if (readByte == 0) {
            bool = null;
        } else {
            bool = Boolean.valueOf(readByte == 1);
        }
        this.isFavorite = bool;
        byte readByte2 = parcel.readByte();
        if (readByte2 == 0) {
            bool2 = null;
        } else {
            bool2 = Boolean.valueOf(readByte2 == 1);
        }
        this.isWatched = bool2;
        byte readByte3 = parcel.readByte();
        if (readByte3 == 0) {
            bool3 = null;
        } else {
            bool3 = Boolean.valueOf(readByte3 == 1 ? true : z);
        }
        this.isTV = bool3;
        this.numberSeason = parcel.readInt();
        long readLong = parcel.readLong();
        long readLong2 = parcel.readLong();
        if (readLong == 0) {
            offsetDateTime = null;
        } else {
            offsetDateTime = OffsetDateTime.ofInstant(Instant.ofEpochSecond(readLong, 0), ZoneOffset.UTC);
        }
        this.collected_at = offsetDateTime;
        this.watched_at = readLong2 != 0 ? OffsetDateTime.ofInstant(Instant.ofEpochSecond(readLong2, 0), ZoneOffset.UTC) : offsetDateTime2;
    }
}
