package com.database.entitys;

import android.os.Parcel;
import android.os.Parcelable;

public class GenreEntity implements Parcelable {
    public static final Parcelable.Creator<GenreEntity> CREATOR = new Parcelable.Creator<GenreEntity>() {
        public GenreEntity createFromParcel(Parcel parcel) {
            return new GenreEntity(parcel);
        }

        public GenreEntity[] newArray(int i) {
            return new GenreEntity[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private int f2514a;
    private String b;
    private boolean c;
    private String d;

    public GenreEntity() {
    }

    public void a(int i) {
        this.f2514a = i;
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(this.f2514a);
        parcel.writeString(this.b);
        parcel.writeByte(this.c ? (byte) 1 : 0);
        parcel.writeString(this.d);
    }

    protected GenreEntity(Parcel parcel) {
        this.f2514a = parcel.readInt();
        this.b = parcel.readString();
        this.c = parcel.readByte() != 0;
        this.d = parcel.readString();
    }

    public void a(String str) {
        this.b = str;
    }
}
