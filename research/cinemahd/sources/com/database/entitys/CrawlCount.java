package com.database.entitys;

import com.google.gson.Gson;

public class CrawlCount {

    /* renamed from: a  reason: collision with root package name */
    private int f2513a;
    private String b;
    private String c;
    private int d;

    public void a(String str) {
        this.c = str;
    }

    public void b(String str) {
        this.b = str;
    }

    public int c() {
        return this.f2513a;
    }

    public String d() {
        return this.b;
    }

    public String toString() {
        return new Gson().a((Object) this);
    }

    public int a() {
        return this.d;
    }

    public String b() {
        return this.c;
    }

    public void a(int i) {
        this.d = i;
    }

    public void b(int i) {
        this.f2513a = i;
    }
}
