package com.database.entitys;

import android.os.Parcel;
import android.os.Parcelable;
import org.threeten.bp.OffsetDateTime;

public class TvWatchedEpisode implements Parcelable {
    public static final Parcelable.Creator<TvWatchedEpisode> CREATOR = new Parcelable.Creator<TvWatchedEpisode>() {
        public TvWatchedEpisode createFromParcel(Parcel parcel) {
            return new TvWatchedEpisode(parcel);
        }

        public TvWatchedEpisode[] newArray(int i) {
            return new TvWatchedEpisode[i];
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private int f2516a;
    private long b;
    private String c;
    private long d;
    private long e;
    private int f;
    private int g;
    private long h;
    private long i;
    private String j;

    protected TvWatchedEpisode(Parcel parcel) {
        this.f2516a = parcel.readInt();
        this.b = parcel.readLong();
        this.c = parcel.readString();
        this.d = parcel.readLong();
        this.e = parcel.readLong();
        this.f = parcel.readInt();
        this.g = parcel.readInt();
        this.h = parcel.readLong();
        this.i = parcel.readLong();
        this.j = parcel.readString();
    }

    public void a(String str) {
        this.c = str;
    }

    public void a(OffsetDateTime offsetDateTime) {
    }

    public void b(int i2) {
        this.f2516a = i2;
    }

    public void b(OffsetDateTime offsetDateTime) {
    }

    public void c(long j2) {
        this.b = j2;
    }

    public void d(long j2) {
        this.e = j2;
    }

    public int describeContents() {
        return 0;
    }

    public void e(long j2) {
        this.d = j2;
    }

    public String f() {
        return this.j;
    }

    public long g() {
        return this.b;
    }

    public long h() {
        return this.e;
    }

    public long i() {
        return this.d;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeInt(this.f2516a);
        parcel.writeLong(this.b);
        parcel.writeString(this.c);
        parcel.writeLong(this.d);
        parcel.writeLong(this.e);
        parcel.writeInt(this.f);
        parcel.writeInt(this.g);
        parcel.writeLong(this.h);
        parcel.writeLong(this.i);
        parcel.writeString(this.j);
    }

    public void a(int i2) {
        this.g = i2;
    }

    public int b() {
        return this.g;
    }

    public String c() {
        return this.c;
    }

    public long d() {
        return this.h;
    }

    public int e() {
        return this.f;
    }

    public long a() {
        return this.i;
    }

    public void b(long j2) {
        this.h = j2;
    }

    public void c(int i2) {
        this.f = i2;
    }

    public void a(long j2) {
        this.i = j2;
    }

    public void b(String str) {
        this.j = str;
    }

    public TvWatchedEpisode() {
    }
}
