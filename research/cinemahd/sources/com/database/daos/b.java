package com.database.daos;

import com.database.entitys.TvWatchedEpisode;

/* compiled from: TvWatchedEpisodeDAO */
public final /* synthetic */ class b {
    public static void a(TvWatchedEpisodeDAO _this, TvWatchedEpisode... tvWatchedEpisodeArr) {
        for (TvWatchedEpisode tvWatchedEpisode : tvWatchedEpisodeArr) {
            _this.a(tvWatchedEpisode.g(), tvWatchedEpisode.c(), tvWatchedEpisode.i(), tvWatchedEpisode.h(), tvWatchedEpisode.e(), tvWatchedEpisode.b());
        }
    }

    public static void b(TvWatchedEpisodeDAO _this, TvWatchedEpisode... tvWatchedEpisodeArr) {
        for (TvWatchedEpisode tvWatchedEpisode : tvWatchedEpisodeArr) {
            if (_this.a(tvWatchedEpisode.g(), tvWatchedEpisode.c(), tvWatchedEpisode.i(), tvWatchedEpisode.h(), tvWatchedEpisode.e(), tvWatchedEpisode.b(), tvWatchedEpisode.d(), tvWatchedEpisode.a(), tvWatchedEpisode.f()) <= 0) {
                _this.b(tvWatchedEpisode.g(), tvWatchedEpisode.c(), tvWatchedEpisode.i(), tvWatchedEpisode.h(), tvWatchedEpisode.e(), tvWatchedEpisode.b(), tvWatchedEpisode.d(), tvWatchedEpisode.a(), tvWatchedEpisode.f());
            }
        }
    }
}
