package com.database.daos;

import com.database.entitys.TvWatchedEpisode;
import java.util.List;

public interface TvWatchedEpisodeDAO {
    int a(long j, String str, long j2, long j3, int i, int i2);

    int a(long j, String str, long j2, long j3, int i, int i2, long j4, long j5, String str2);

    List<TvWatchedEpisode> a();

    List<TvWatchedEpisode> a(long j, String str, long j2, long j3);

    List<TvWatchedEpisode> a(long j, String str, long j2, long j3, int i);

    void a(TvWatchedEpisode... tvWatchedEpisodeArr);

    long b(long j, String str, long j2, long j3, int i, int i2, long j4, long j5, String str2);

    void b(long j, String str, long j2, long j3, int i, int i2);

    void b(TvWatchedEpisode... tvWatchedEpisodeArr);

    List<TvWatchedEpisode> c(long j, String str, long j2, long j3, int i, int i2);
}
