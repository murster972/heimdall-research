package com.database.daos;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.database.entitys.MovieEntity;
import com.facebook.react.uimanager.ViewProps;
import com.unity3d.ads.metadata.MediationMetaData;
import java.util.ArrayList;
import java.util.List;

public final class MovieDAO_Impl implements MovieDAO {

    /* renamed from: a  reason: collision with root package name */
    private final RoomDatabase f2510a;
    private final SharedSQLiteStatement b;
    private final SharedSQLiteStatement c;
    private final SharedSQLiteStatement d;
    private final SharedSQLiteStatement e;
    private final SharedSQLiteStatement f;
    private final SharedSQLiteStatement g;
    private final SharedSQLiteStatement h;
    private final SharedSQLiteStatement i;
    private final SharedSQLiteStatement j;

    public MovieDAO_Impl(RoomDatabase roomDatabase) {
        this.f2510a = roomDatabase;
        this.b = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "INSERT INTO MovieEntity(tmdbID,imdbIDStr,traktID,tvdbID,position,duration,subtitlepath,poster_path,backdrop_path,name,realeaseDate,overview,genres,vote,createdDate,isFavorite,isWatched,isTV,numberSeason, collected_at, watched_at) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?, ?, ?)";
            }
        };
        this.c = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "UPDATE MovieEntity SET tmdbID=?, imdbIDStr=?, traktID=?, tvdbID=?, position=?, duration=?,subtitlepath=?,poster_path=?,backdrop_path=?,name=?,realeaseDate=?,overview=?,genres=?,vote=?,createdDate=?,isFavorite=?, isWatched=?, isTV=?, numberSeason=? WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))";
            }
        };
        this.d = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "UPDATE MovieEntity SET tmdbID=?, imdbIDStr=?, traktID=?, tvdbID=?, position=?, duration=?,subtitlepath=?,poster_path=?,backdrop_path=?,name=?,realeaseDate=?,overview=?,genres=?,vote=?,createdDate=?,isFavorite=?, isWatched=?, isTV=?, numberSeason=?, collected_at=? WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))";
            }
        };
        this.e = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "UPDATE MovieEntity SET tmdbID=?, imdbIDStr=?, traktID=?, tvdbID=?, position=?, duration=?,subtitlepath=?,poster_path=?,backdrop_path=?,name=?,realeaseDate=?,overview=?,genres=?,vote=?,createdDate=?,isFavorite=?, isWatched=?, isTV=?, numberSeason=?, watched_at=? WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))";
            }
        };
        this.f = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM MovieEntity WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))";
            }
        };
        this.g = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "UPDATE MovieEntity SET numberSeason=? WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))";
            }
        };
        new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "UPDATE MovieEntity SET position=?,subtitlepath=? WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))";
            }
        };
        this.h = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM MovieEntity";
            }
        };
        this.i = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM MovieEntity WHERE collected_at IS NOT NULL";
            }
        };
        this.j = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM MovieEntity WHERE watched_at IS NOT NULL";
            }
        };
    }

    public long a(long j2, String str, long j3, long j4, long j5, long j6, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d2, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i2, Long l2, Long l3) {
        Integer num;
        Integer num2;
        String str9 = str;
        String str10 = str2;
        String str11 = str3;
        String str12 = str4;
        String str13 = str5;
        String str14 = str6;
        String str15 = str7;
        String str16 = str8;
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.b.a();
        long j7 = j2;
        a2.a(1, j2);
        if (str9 == null) {
            a2.a(2);
        } else {
            a2.a(2, str9);
        }
        a2.a(3, j3);
        a2.a(4, j4);
        a2.a(5, j5);
        a2.a(6, j6);
        if (str10 == null) {
            a2.a(7);
        } else {
            a2.a(7, str10);
        }
        if (str11 == null) {
            a2.a(8);
        } else {
            a2.a(8, str11);
        }
        if (str12 == null) {
            a2.a(9);
        } else {
            a2.a(9, str12);
        }
        if (str13 == null) {
            a2.a(10);
        } else {
            a2.a(10, str13);
        }
        if (str14 == null) {
            a2.a(11);
        } else {
            a2.a(11, str14);
        }
        if (str15 == null) {
            a2.a(12);
        } else {
            a2.a(12, str15);
        }
        if (str16 == null) {
            a2.a(13);
        } else {
            a2.a(13, str16);
        }
        if (d2 == null) {
            a2.a(14);
        } else {
            a2.a(14, d2.doubleValue());
        }
        if (l == null) {
            a2.a(15);
        } else {
            a2.a(15, l.longValue());
        }
        Integer num3 = null;
        if (bool == null) {
            num = null;
        } else {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        }
        if (num == null) {
            a2.a(16);
        } else {
            a2.a(16, (long) num.intValue());
        }
        if (bool2 == null) {
            num2 = null;
        } else {
            num2 = Integer.valueOf(bool2.booleanValue() ? 1 : 0);
        }
        if (num2 == null) {
            a2.a(17);
        } else {
            a2.a(17, (long) num2.intValue());
        }
        if (bool3 != null) {
            num3 = Integer.valueOf(bool3.booleanValue() ? 1 : 0);
        }
        if (num3 == null) {
            a2.a(18);
        } else {
            a2.a(18, (long) num3.intValue());
        }
        a2.a(19, (long) i2);
        if (l2 == null) {
            a2.a(20);
        } else {
            a2.a(20, l2.longValue());
        }
        if (l3 == null) {
            a2.a(21);
        } else {
            a2.a(21, l3.longValue());
        }
        this.f2510a.c();
        try {
            long p = a2.p();
            this.f2510a.k();
            return p;
        } finally {
            this.f2510a.e();
            this.b.a(a2);
        }
    }

    public /* synthetic */ void a(MovieEntity... movieEntityArr) throws Exception {
        a.b(this, movieEntityArr);
    }

    public int b(long j2, String str, long j3, long j4, long j5, long j6, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d2, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i2, Long l2) {
        Integer num;
        Integer num2;
        long j7 = j2;
        String str9 = str;
        long j8 = j3;
        long j9 = j4;
        String str10 = str2;
        String str11 = str3;
        String str12 = str4;
        String str13 = str5;
        String str14 = str6;
        String str15 = str7;
        String str16 = str8;
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.d.a();
        a2.a(1, j7);
        if (str9 == null) {
            a2.a(2);
        } else {
            a2.a(2, str9);
        }
        a2.a(3, j8);
        a2.a(4, j9);
        a2.a(5, j5);
        a2.a(6, j6);
        if (str10 == null) {
            a2.a(7);
        } else {
            a2.a(7, str10);
        }
        if (str11 == null) {
            a2.a(8);
        } else {
            a2.a(8, str11);
        }
        if (str12 == null) {
            a2.a(9);
        } else {
            a2.a(9, str12);
        }
        if (str13 == null) {
            a2.a(10);
        } else {
            a2.a(10, str13);
        }
        if (str14 == null) {
            a2.a(11);
        } else {
            a2.a(11, str14);
        }
        if (str15 == null) {
            a2.a(12);
        } else {
            a2.a(12, str15);
        }
        if (str16 == null) {
            a2.a(13);
        } else {
            a2.a(13, str16);
        }
        if (d2 == null) {
            a2.a(14);
        } else {
            a2.a(14, d2.doubleValue());
        }
        if (l == null) {
            a2.a(15);
        } else {
            a2.a(15, l.longValue());
        }
        Integer num3 = null;
        if (bool == null) {
            num = null;
        } else {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        }
        if (num == null) {
            a2.a(16);
        } else {
            a2.a(16, (long) num.intValue());
        }
        if (bool2 == null) {
            num2 = null;
        } else {
            num2 = Integer.valueOf(bool2.booleanValue() ? 1 : 0);
        }
        if (num2 == null) {
            a2.a(17);
        } else {
            a2.a(17, (long) num2.intValue());
        }
        if (bool3 != null) {
            num3 = Integer.valueOf(bool3.booleanValue() ? 1 : 0);
        }
        if (num3 == null) {
            a2.a(18);
        } else {
            a2.a(18, (long) num3.intValue());
        }
        a2.a(19, (long) i2);
        if (l2 == null) {
            a2.a(20);
        } else {
            a2.a(20, l2.longValue());
        }
        a2.a(21, j7);
        if (str9 == null) {
            a2.a(22);
        } else {
            a2.a(22, str9);
        }
        a2.a(23, j8);
        a2.a(24, j4);
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.d.a(a2);
        }
    }

    public /* synthetic */ int b(MovieEntity... movieEntityArr) {
        return a.a(this, movieEntityArr);
    }

    public int c() {
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.h.a();
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.h.a(a2);
        }
    }

    public List<MovieEntity> d() {
        RoomSQLiteQuery roomSQLiteQuery;
        int i2;
        Double d2;
        Long l;
        Integer num;
        int i3;
        Boolean bool;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        Long l2;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE collected_at IS NOT NULL ORDER BY collected_at DESC", 0);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                int i4 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    MovieEntity movieEntity = new MovieEntity();
                    ArrayList arrayList2 = arrayList;
                    movieEntity.setId(a2.getInt(a3));
                    int i5 = a13;
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(i5));
                    int i6 = a14;
                    movieEntity.setRealeaseDate(a2.getString(i6));
                    movieEntity.setOverview(a2.getString(a15));
                    int i7 = i4;
                    int i8 = a15;
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(i7)));
                    int i9 = a17;
                    Long l3 = null;
                    if (a2.isNull(i9)) {
                        i2 = i9;
                        d2 = null;
                    } else {
                        i2 = i9;
                        d2 = Double.valueOf(a2.getDouble(i9));
                    }
                    movieEntity.setVote(d2);
                    int i10 = a18;
                    if (a2.isNull(i10)) {
                        a18 = i10;
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i10));
                        a18 = i10;
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i11 = a19;
                    if (a2.isNull(i11)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(i11));
                    }
                    boolean z = true;
                    if (num == null) {
                        i3 = i11;
                        bool = null;
                    } else {
                        i3 = i11;
                        bool = Boolean.valueOf(num.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool);
                    int i12 = a20;
                    if (a2.isNull(i12)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i12));
                    }
                    if (num2 == null) {
                        a20 = i12;
                        bool2 = null;
                    } else {
                        a20 = i12;
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setWatched(bool2);
                    int i13 = a21;
                    if (a2.isNull(i13)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i13));
                    }
                    if (num3 == null) {
                        a21 = i13;
                        bool3 = null;
                    } else {
                        if (num3.intValue() == 0) {
                            z = false;
                        }
                        a21 = i13;
                        bool3 = Boolean.valueOf(z);
                    }
                    movieEntity.setTV(bool3);
                    int i14 = i5;
                    int i15 = a22;
                    movieEntity.setNumberSeason(a2.getInt(i15));
                    int i16 = a23;
                    if (a2.isNull(i16)) {
                        a22 = i15;
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i16));
                        a22 = i15;
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i17 = a24;
                    if (!a2.isNull(i17)) {
                        l3 = Long.valueOf(a2.getLong(i17));
                    }
                    a24 = i17;
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(movieEntity);
                    a23 = i16;
                    i4 = i7;
                    a19 = i3;
                    arrayList = arrayList3;
                    a14 = i6;
                    a13 = i14;
                    a15 = i8;
                    a17 = i2;
                }
                ArrayList arrayList4 = arrayList;
                a2.close();
                roomSQLiteQuery.b();
                return arrayList4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public int e() {
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.j.a();
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.j.a(a2);
        }
    }

    public int a(long j2, String str, long j3, long j4, long j5, long j6, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d2, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i2) {
        Integer num;
        Integer num2;
        long j7 = j2;
        String str9 = str;
        long j8 = j3;
        long j9 = j4;
        String str10 = str2;
        String str11 = str3;
        String str12 = str4;
        String str13 = str5;
        String str14 = str6;
        String str15 = str7;
        String str16 = str8;
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.c.a();
        a2.a(1, j7);
        if (str9 == null) {
            a2.a(2);
        } else {
            a2.a(2, str9);
        }
        a2.a(3, j8);
        a2.a(4, j9);
        a2.a(5, j5);
        a2.a(6, j6);
        if (str10 == null) {
            a2.a(7);
        } else {
            a2.a(7, str10);
        }
        if (str11 == null) {
            a2.a(8);
        } else {
            a2.a(8, str11);
        }
        if (str12 == null) {
            a2.a(9);
        } else {
            a2.a(9, str12);
        }
        if (str13 == null) {
            a2.a(10);
        } else {
            a2.a(10, str13);
        }
        if (str14 == null) {
            a2.a(11);
        } else {
            a2.a(11, str14);
        }
        if (str15 == null) {
            a2.a(12);
        } else {
            a2.a(12, str15);
        }
        if (str16 == null) {
            a2.a(13);
        } else {
            a2.a(13, str16);
        }
        if (d2 == null) {
            a2.a(14);
        } else {
            a2.a(14, d2.doubleValue());
        }
        if (l == null) {
            a2.a(15);
        } else {
            a2.a(15, l.longValue());
        }
        Integer num3 = null;
        if (bool == null) {
            num = null;
        } else {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        }
        if (num == null) {
            a2.a(16);
        } else {
            a2.a(16, (long) num.intValue());
        }
        if (bool2 == null) {
            num2 = null;
        } else {
            num2 = Integer.valueOf(bool2.booleanValue() ? 1 : 0);
        }
        if (num2 == null) {
            a2.a(17);
        } else {
            a2.a(17, (long) num2.intValue());
        }
        if (bool3 != null) {
            num3 = Integer.valueOf(bool3.booleanValue() ? 1 : 0);
        }
        if (num3 == null) {
            a2.a(18);
        } else {
            a2.a(18, (long) num3.intValue());
        }
        a2.a(19, (long) i2);
        a2.a(20, j7);
        if (str9 == null) {
            a2.a(21);
        } else {
            a2.a(21, str9);
        }
        a2.a(22, j8);
        a2.a(23, j4);
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.c.a(a2);
        }
    }

    public int b(long j2, String str, long j3, long j4) {
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.f.a();
        a2.a(1, j2);
        if (str == null) {
            a2.a(2);
        } else {
            a2.a(2, str);
        }
        a2.a(3, j3);
        a2.a(4, j4);
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.f.a(a2);
        }
    }

    public int b() {
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.i.a();
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.i.a(a2);
        }
    }

    public List<MovieEntity> b(int i2) {
        RoomSQLiteQuery roomSQLiteQuery;
        int i3;
        Double d2;
        Long l;
        Integer num;
        Boolean bool;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        int i4;
        Long l2;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE watched_at IS NOT NULL ORDER BY watched_at DESC LIMIT ?", 1);
        b2.a(1, (long) i2);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                int i5 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    MovieEntity movieEntity = new MovieEntity();
                    ArrayList arrayList2 = arrayList;
                    movieEntity.setId(a2.getInt(a3));
                    int i6 = a3;
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(a13));
                    movieEntity.setRealeaseDate(a2.getString(a14));
                    a15 = a15;
                    movieEntity.setOverview(a2.getString(a15));
                    int i7 = i5;
                    int i8 = a14;
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(i7)));
                    int i9 = a17;
                    Long l3 = null;
                    if (a2.isNull(i9)) {
                        i3 = i9;
                        d2 = null;
                    } else {
                        i3 = i9;
                        d2 = Double.valueOf(a2.getDouble(i9));
                    }
                    movieEntity.setVote(d2);
                    int i10 = a18;
                    if (a2.isNull(i10)) {
                        a18 = i10;
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i10));
                        a18 = i10;
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i11 = a19;
                    if (a2.isNull(i11)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(i11));
                    }
                    if (num == null) {
                        a19 = i11;
                        bool = null;
                    } else {
                        a19 = i11;
                        bool = Boolean.valueOf(num.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool);
                    int i12 = a20;
                    if (a2.isNull(i12)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i12));
                    }
                    if (num2 == null) {
                        a20 = i12;
                        bool2 = null;
                    } else {
                        a20 = i12;
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setWatched(bool2);
                    int i13 = a21;
                    if (a2.isNull(i13)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i13));
                    }
                    if (num3 == null) {
                        a21 = i13;
                        bool3 = null;
                    } else {
                        a21 = i13;
                        bool3 = Boolean.valueOf(num3.intValue() != 0);
                    }
                    movieEntity.setTV(bool3);
                    int i14 = i6;
                    int i15 = a22;
                    movieEntity.setNumberSeason(a2.getInt(i15));
                    int i16 = a23;
                    if (a2.isNull(i16)) {
                        i4 = i15;
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i16));
                        i4 = i15;
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i17 = a24;
                    if (!a2.isNull(i17)) {
                        l3 = Long.valueOf(a2.getLong(i17));
                    }
                    a24 = i17;
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(movieEntity);
                    i5 = i7;
                    a3 = i14;
                    a22 = i4;
                    a17 = i3;
                    a23 = i16;
                    arrayList = arrayList3;
                    a14 = i8;
                }
                ArrayList arrayList4 = arrayList;
                a2.close();
                roomSQLiteQuery.b();
                return arrayList4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public int a(long j2, String str, long j3, long j4, long j5, long j6, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d2, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i2, Long l2) {
        Integer num;
        Integer num2;
        long j7 = j2;
        String str9 = str;
        long j8 = j3;
        long j9 = j4;
        String str10 = str2;
        String str11 = str3;
        String str12 = str4;
        String str13 = str5;
        String str14 = str6;
        String str15 = str7;
        String str16 = str8;
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.e.a();
        a2.a(1, j7);
        if (str9 == null) {
            a2.a(2);
        } else {
            a2.a(2, str9);
        }
        a2.a(3, j8);
        a2.a(4, j9);
        a2.a(5, j5);
        a2.a(6, j6);
        if (str10 == null) {
            a2.a(7);
        } else {
            a2.a(7, str10);
        }
        if (str11 == null) {
            a2.a(8);
        } else {
            a2.a(8, str11);
        }
        if (str12 == null) {
            a2.a(9);
        } else {
            a2.a(9, str12);
        }
        if (str13 == null) {
            a2.a(10);
        } else {
            a2.a(10, str13);
        }
        if (str14 == null) {
            a2.a(11);
        } else {
            a2.a(11, str14);
        }
        if (str15 == null) {
            a2.a(12);
        } else {
            a2.a(12, str15);
        }
        if (str16 == null) {
            a2.a(13);
        } else {
            a2.a(13, str16);
        }
        if (d2 == null) {
            a2.a(14);
        } else {
            a2.a(14, d2.doubleValue());
        }
        if (l == null) {
            a2.a(15);
        } else {
            a2.a(15, l.longValue());
        }
        Integer num3 = null;
        if (bool == null) {
            num = null;
        } else {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        }
        if (num == null) {
            a2.a(16);
        } else {
            a2.a(16, (long) num.intValue());
        }
        if (bool2 == null) {
            num2 = null;
        } else {
            num2 = Integer.valueOf(bool2.booleanValue() ? 1 : 0);
        }
        if (num2 == null) {
            a2.a(17);
        } else {
            a2.a(17, (long) num2.intValue());
        }
        if (bool3 != null) {
            num3 = Integer.valueOf(bool3.booleanValue() ? 1 : 0);
        }
        if (num3 == null) {
            a2.a(18);
        } else {
            a2.a(18, (long) num3.intValue());
        }
        a2.a(19, (long) i2);
        if (l2 == null) {
            a2.a(20);
        } else {
            a2.a(20, l2.longValue());
        }
        a2.a(21, j7);
        if (str9 == null) {
            a2.a(22);
        } else {
            a2.a(22, str9);
        }
        a2.a(23, j8);
        a2.a(24, j4);
        this.f2510a.c();
        try {
            int o = a2.o();
            this.f2510a.k();
            return o;
        } finally {
            this.f2510a.e();
            this.e.a(a2);
        }
    }

    public void a(int i2, long j2, String str, long j3, long j4) {
        this.f2510a.b();
        SupportSQLiteStatement a2 = this.g.a();
        a2.a(1, (long) i2);
        a2.a(2, j2);
        if (str == null) {
            a2.a(3);
        } else {
            a2.a(3, str);
        }
        a2.a(4, j3);
        a2.a(5, j4);
        this.f2510a.c();
        try {
            a2.o();
            this.f2510a.k();
        } finally {
            this.f2510a.e();
            this.g.a(a2);
        }
    }

    public MovieEntity a(long j2, String str, long j3, long j4) {
        RoomSQLiteQuery roomSQLiteQuery;
        MovieEntity movieEntity;
        Double d2;
        Long l;
        Integer num;
        Boolean bool;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        Long l2;
        String str2 = str;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))", 4);
        b2.a(1, j2);
        if (str2 == null) {
            b2.a(2);
        } else {
            b2.a(2, str2);
        }
        b2.a(3, j3);
        b2.a(4, j4);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                Long l3 = null;
                if (a2.moveToFirst()) {
                    int i2 = a24;
                    movieEntity = new MovieEntity();
                    movieEntity.setId(a2.getInt(a3));
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(a13));
                    movieEntity.setRealeaseDate(a2.getString(a14));
                    movieEntity.setOverview(a2.getString(a15));
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(a16)));
                    int i3 = a17;
                    if (a2.isNull(i3)) {
                        d2 = null;
                    } else {
                        d2 = Double.valueOf(a2.getDouble(i3));
                    }
                    movieEntity.setVote(d2);
                    int i4 = a18;
                    if (a2.isNull(i4)) {
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i4));
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i5 = a19;
                    if (a2.isNull(i5)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(i5));
                    }
                    if (num == null) {
                        bool = null;
                    } else {
                        bool = Boolean.valueOf(num.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool);
                    int i6 = a20;
                    if (a2.isNull(i6)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i6));
                    }
                    if (num2 == null) {
                        bool2 = null;
                    } else {
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setWatched(bool2);
                    int i7 = a21;
                    if (a2.isNull(i7)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i7));
                    }
                    if (num3 == null) {
                        bool3 = null;
                    } else {
                        bool3 = Boolean.valueOf(num3.intValue() != 0);
                    }
                    movieEntity.setTV(bool3);
                    movieEntity.setNumberSeason(a2.getInt(a22));
                    int i8 = a23;
                    if (a2.isNull(i8)) {
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i8));
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i9 = i2;
                    if (!a2.isNull(i9)) {
                        l3 = Long.valueOf(a2.getLong(i9));
                    }
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                } else {
                    movieEntity = null;
                }
                a2.close();
                roomSQLiteQuery.b();
                return movieEntity;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public List<MovieEntity> a(Boolean bool, int i2) {
        Integer num;
        RoomSQLiteQuery roomSQLiteQuery;
        Double d2;
        int i3;
        Long l;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        Integer num4;
        Boolean bool4;
        int i4;
        Long l2;
        Long l3;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE watched_at IS NOT NULL AND isTV=? ORDER BY watched_at DESC LIMIT ?", 2);
        if (bool == null) {
            num = null;
        } else {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        }
        if (num == null) {
            b2.a(1);
        } else {
            b2.a(1, (long) num.intValue());
        }
        b2.a(2, (long) i2);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                int i5 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    MovieEntity movieEntity = new MovieEntity();
                    ArrayList arrayList2 = arrayList;
                    movieEntity.setId(a2.getInt(a3));
                    int i6 = a14;
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(a13));
                    movieEntity.setRealeaseDate(a2.getString(i6));
                    int i7 = a15;
                    movieEntity.setOverview(a2.getString(i7));
                    int i8 = i5;
                    int i9 = a3;
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(i8)));
                    int i10 = a17;
                    if (a2.isNull(i10)) {
                        a17 = i10;
                        d2 = null;
                    } else {
                        a17 = i10;
                        d2 = Double.valueOf(a2.getDouble(i10));
                    }
                    movieEntity.setVote(d2);
                    int i11 = a18;
                    if (a2.isNull(i11)) {
                        i3 = i11;
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i11));
                        i3 = i11;
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i12 = a19;
                    if (a2.isNull(i12)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i12));
                    }
                    if (num2 == null) {
                        a19 = i12;
                        bool2 = null;
                    } else {
                        a19 = i12;
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool2);
                    int i13 = a20;
                    if (a2.isNull(i13)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i13));
                    }
                    if (num3 == null) {
                        a20 = i13;
                        bool3 = null;
                    } else {
                        a20 = i13;
                        bool3 = Boolean.valueOf(num3.intValue() != 0);
                    }
                    movieEntity.setWatched(bool3);
                    int i14 = a21;
                    if (a2.isNull(i14)) {
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a2.getInt(i14));
                    }
                    if (num4 == null) {
                        a21 = i14;
                        bool4 = null;
                    } else {
                        a21 = i14;
                        bool4 = Boolean.valueOf(num4.intValue() != 0);
                    }
                    movieEntity.setTV(bool4);
                    int i15 = i6;
                    int i16 = a22;
                    movieEntity.setNumberSeason(a2.getInt(i16));
                    int i17 = a23;
                    if (a2.isNull(i17)) {
                        i4 = i16;
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i17));
                        i4 = i16;
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i18 = a24;
                    if (a2.isNull(i18)) {
                        a24 = i18;
                        l3 = null;
                    } else {
                        l3 = Long.valueOf(a2.getLong(i18));
                        a24 = i18;
                    }
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(movieEntity);
                    a22 = i4;
                    a23 = i17;
                    arrayList = arrayList3;
                    a3 = i9;
                    int i19 = i8;
                    a15 = i7;
                    a14 = i15;
                    a18 = i3;
                    i5 = i19;
                }
                ArrayList arrayList4 = arrayList;
                a2.close();
                roomSQLiteQuery.b();
                return arrayList4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public List<MovieEntity> a() {
        RoomSQLiteQuery roomSQLiteQuery;
        int i2;
        Double d2;
        Long l;
        Integer num;
        int i3;
        Boolean bool;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        Long l2;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE watched_at IS NOT NULL ORDER BY watched_at DESC", 0);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                int i4 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    MovieEntity movieEntity = new MovieEntity();
                    ArrayList arrayList2 = arrayList;
                    movieEntity.setId(a2.getInt(a3));
                    int i5 = a13;
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(i5));
                    int i6 = a14;
                    movieEntity.setRealeaseDate(a2.getString(i6));
                    movieEntity.setOverview(a2.getString(a15));
                    int i7 = i4;
                    int i8 = a15;
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(i7)));
                    int i9 = a17;
                    Long l3 = null;
                    if (a2.isNull(i9)) {
                        i2 = i9;
                        d2 = null;
                    } else {
                        i2 = i9;
                        d2 = Double.valueOf(a2.getDouble(i9));
                    }
                    movieEntity.setVote(d2);
                    int i10 = a18;
                    if (a2.isNull(i10)) {
                        a18 = i10;
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i10));
                        a18 = i10;
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i11 = a19;
                    if (a2.isNull(i11)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(i11));
                    }
                    boolean z = true;
                    if (num == null) {
                        i3 = i11;
                        bool = null;
                    } else {
                        i3 = i11;
                        bool = Boolean.valueOf(num.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool);
                    int i12 = a20;
                    if (a2.isNull(i12)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i12));
                    }
                    if (num2 == null) {
                        a20 = i12;
                        bool2 = null;
                    } else {
                        a20 = i12;
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setWatched(bool2);
                    int i13 = a21;
                    if (a2.isNull(i13)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i13));
                    }
                    if (num3 == null) {
                        a21 = i13;
                        bool3 = null;
                    } else {
                        if (num3.intValue() == 0) {
                            z = false;
                        }
                        a21 = i13;
                        bool3 = Boolean.valueOf(z);
                    }
                    movieEntity.setTV(bool3);
                    int i14 = i5;
                    int i15 = a22;
                    movieEntity.setNumberSeason(a2.getInt(i15));
                    int i16 = a23;
                    if (a2.isNull(i16)) {
                        a22 = i15;
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i16));
                        a22 = i15;
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i17 = a24;
                    if (!a2.isNull(i17)) {
                        l3 = Long.valueOf(a2.getLong(i17));
                    }
                    a24 = i17;
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(movieEntity);
                    a23 = i16;
                    i4 = i7;
                    a19 = i3;
                    arrayList = arrayList3;
                    a14 = i6;
                    a13 = i14;
                    a15 = i8;
                    a17 = i2;
                }
                ArrayList arrayList4 = arrayList;
                a2.close();
                roomSQLiteQuery.b();
                return arrayList4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public List<MovieEntity> a(Boolean bool) {
        Integer num;
        RoomSQLiteQuery roomSQLiteQuery;
        int i2;
        Double d2;
        Long l;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        Integer num4;
        Boolean bool4;
        int i3;
        Long l2;
        Long l3;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE watched_at IS NOT NULL AND isTV=? ORDER BY watched_at DESC", 1);
        if (bool == null) {
            num = null;
        } else {
            num = Integer.valueOf(bool.booleanValue() ? 1 : 0);
        }
        if (num == null) {
            b2.a(1);
        } else {
            b2.a(1, (long) num.intValue());
        }
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                int i4 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    MovieEntity movieEntity = new MovieEntity();
                    ArrayList arrayList2 = arrayList;
                    movieEntity.setId(a2.getInt(a3));
                    int i5 = a3;
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(a13));
                    movieEntity.setRealeaseDate(a2.getString(a14));
                    a15 = a15;
                    movieEntity.setOverview(a2.getString(a15));
                    int i6 = i4;
                    int i7 = a13;
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(i6)));
                    int i8 = a17;
                    if (a2.isNull(i8)) {
                        i2 = i8;
                        d2 = null;
                    } else {
                        i2 = i8;
                        d2 = Double.valueOf(a2.getDouble(i8));
                    }
                    movieEntity.setVote(d2);
                    int i9 = a18;
                    if (a2.isNull(i9)) {
                        a18 = i9;
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i9));
                        a18 = i9;
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i10 = a19;
                    if (a2.isNull(i10)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i10));
                    }
                    if (num2 == null) {
                        a19 = i10;
                        bool2 = null;
                    } else {
                        a19 = i10;
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool2);
                    int i11 = a20;
                    if (a2.isNull(i11)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i11));
                    }
                    if (num3 == null) {
                        a20 = i11;
                        bool3 = null;
                    } else {
                        a20 = i11;
                        bool3 = Boolean.valueOf(num3.intValue() != 0);
                    }
                    movieEntity.setWatched(bool3);
                    int i12 = a21;
                    if (a2.isNull(i12)) {
                        num4 = null;
                    } else {
                        num4 = Integer.valueOf(a2.getInt(i12));
                    }
                    if (num4 == null) {
                        a21 = i12;
                        bool4 = null;
                    } else {
                        a21 = i12;
                        bool4 = Boolean.valueOf(num4.intValue() != 0);
                    }
                    movieEntity.setTV(bool4);
                    int i13 = i5;
                    int i14 = a22;
                    movieEntity.setNumberSeason(a2.getInt(i14));
                    int i15 = a23;
                    if (a2.isNull(i15)) {
                        i3 = i14;
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i15));
                        i3 = i14;
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i16 = a24;
                    if (a2.isNull(i16)) {
                        a24 = i16;
                        l3 = null;
                    } else {
                        l3 = Long.valueOf(a2.getLong(i16));
                        a24 = i16;
                    }
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(movieEntity);
                    a22 = i3;
                    a23 = i15;
                    arrayList = arrayList3;
                    a13 = i7;
                    int i17 = i2;
                    i4 = i6;
                    a3 = i13;
                    a17 = i17;
                }
                ArrayList arrayList4 = arrayList;
                a2.close();
                roomSQLiteQuery.b();
                return arrayList4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public List<MovieEntity> a(boolean z) {
        RoomSQLiteQuery roomSQLiteQuery;
        int i2;
        Double d2;
        Long l;
        Integer num;
        Boolean bool;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        int i3;
        Long l2;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE isTV=? AND collected_at IS NOT NULL ORDER BY collected_at DESC", 1);
        b2.a(1, z ? 1 : 0);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                int i4 = a16;
                ArrayList arrayList = new ArrayList(a2.getCount());
                while (a2.moveToNext()) {
                    MovieEntity movieEntity = new MovieEntity();
                    ArrayList arrayList2 = arrayList;
                    movieEntity.setId(a2.getInt(a3));
                    int i5 = a3;
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(a13));
                    movieEntity.setRealeaseDate(a2.getString(a14));
                    a15 = a15;
                    movieEntity.setOverview(a2.getString(a15));
                    int i6 = i4;
                    int i7 = a14;
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(i6)));
                    int i8 = a17;
                    Long l3 = null;
                    if (a2.isNull(i8)) {
                        i2 = i8;
                        d2 = null;
                    } else {
                        i2 = i8;
                        d2 = Double.valueOf(a2.getDouble(i8));
                    }
                    movieEntity.setVote(d2);
                    int i9 = a18;
                    if (a2.isNull(i9)) {
                        a18 = i9;
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i9));
                        a18 = i9;
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i10 = a19;
                    if (a2.isNull(i10)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(i10));
                    }
                    if (num == null) {
                        a19 = i10;
                        bool = null;
                    } else {
                        a19 = i10;
                        bool = Boolean.valueOf(num.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool);
                    int i11 = a20;
                    if (a2.isNull(i11)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i11));
                    }
                    if (num2 == null) {
                        a20 = i11;
                        bool2 = null;
                    } else {
                        a20 = i11;
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setWatched(bool2);
                    int i12 = a21;
                    if (a2.isNull(i12)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i12));
                    }
                    if (num3 == null) {
                        a21 = i12;
                        bool3 = null;
                    } else {
                        a21 = i12;
                        bool3 = Boolean.valueOf(num3.intValue() != 0);
                    }
                    movieEntity.setTV(bool3);
                    int i13 = i5;
                    int i14 = a22;
                    movieEntity.setNumberSeason(a2.getInt(i14));
                    int i15 = a23;
                    if (a2.isNull(i15)) {
                        i3 = i14;
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i15));
                        i3 = i14;
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i16 = a24;
                    if (!a2.isNull(i16)) {
                        l3 = Long.valueOf(a2.getLong(i16));
                    }
                    a24 = i16;
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(movieEntity);
                    i4 = i6;
                    a3 = i13;
                    a22 = i3;
                    a17 = i2;
                    a23 = i15;
                    arrayList = arrayList3;
                    a14 = i7;
                }
                ArrayList arrayList4 = arrayList;
                a2.close();
                roomSQLiteQuery.b();
                return arrayList4;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public MovieEntity a(int i2) {
        RoomSQLiteQuery roomSQLiteQuery;
        MovieEntity movieEntity;
        Double d2;
        Long l;
        Integer num;
        Boolean bool;
        Integer num2;
        Boolean bool2;
        Integer num3;
        Boolean bool3;
        Long l2;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM MovieEntity WHERE id = ?", 1);
        b2.a(1, (long) i2);
        this.f2510a.b();
        Cursor a2 = DBUtil.a(this.f2510a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "traktID");
            int a7 = CursorUtil.a(a2, "tvdbID");
            int a8 = CursorUtil.a(a2, ViewProps.POSITION);
            int a9 = CursorUtil.a(a2, "duration");
            int a10 = CursorUtil.a(a2, "subtitlepath");
            int a11 = CursorUtil.a(a2, "poster_path");
            int a12 = CursorUtil.a(a2, "backdrop_path");
            int a13 = CursorUtil.a(a2, MediationMetaData.KEY_NAME);
            int a14 = CursorUtil.a(a2, "realeaseDate");
            int a15 = CursorUtil.a(a2, "overview");
            int a16 = CursorUtil.a(a2, "genres");
            roomSQLiteQuery = b2;
            try {
                int a17 = CursorUtil.a(a2, "vote");
                int a18 = CursorUtil.a(a2, "createdDate");
                int a19 = CursorUtil.a(a2, "isFavorite");
                int a20 = CursorUtil.a(a2, "isWatched");
                int a21 = CursorUtil.a(a2, "isTV");
                int a22 = CursorUtil.a(a2, "numberSeason");
                int a23 = CursorUtil.a(a2, "collected_at");
                int a24 = CursorUtil.a(a2, "watched_at");
                Long l3 = null;
                if (a2.moveToFirst()) {
                    int i3 = a24;
                    movieEntity = new MovieEntity();
                    movieEntity.setId(a2.getInt(a3));
                    movieEntity.setTmdbID(a2.getLong(a4));
                    movieEntity.setImdbIDStr(a2.getString(a5));
                    movieEntity.setTraktID(a2.getLong(a6));
                    movieEntity.setTvdbID(a2.getLong(a7));
                    movieEntity.setPosition(a2.getLong(a8));
                    movieEntity.setDuration(a2.getLong(a9));
                    movieEntity.setSubtitlepath(a2.getString(a10));
                    movieEntity.setPoster_path(a2.getString(a11));
                    movieEntity.setBackdrop_path(a2.getString(a12));
                    movieEntity.setName(a2.getString(a13));
                    movieEntity.setRealeaseDate(a2.getString(a14));
                    movieEntity.setOverview(a2.getString(a15));
                    movieEntity.setGenres(MovieEntity.Converter.a(a2.getString(a16)));
                    int i4 = a17;
                    if (a2.isNull(i4)) {
                        d2 = null;
                    } else {
                        d2 = Double.valueOf(a2.getDouble(i4));
                    }
                    movieEntity.setVote(d2);
                    int i5 = a18;
                    if (a2.isNull(i5)) {
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(i5));
                    }
                    movieEntity.setCreatedDate(MovieEntity.Converter.a(l));
                    int i6 = a19;
                    if (a2.isNull(i6)) {
                        num = null;
                    } else {
                        num = Integer.valueOf(a2.getInt(i6));
                    }
                    if (num == null) {
                        bool = null;
                    } else {
                        bool = Boolean.valueOf(num.intValue() != 0);
                    }
                    movieEntity.setFavorite(bool);
                    int i7 = a20;
                    if (a2.isNull(i7)) {
                        num2 = null;
                    } else {
                        num2 = Integer.valueOf(a2.getInt(i7));
                    }
                    if (num2 == null) {
                        bool2 = null;
                    } else {
                        bool2 = Boolean.valueOf(num2.intValue() != 0);
                    }
                    movieEntity.setWatched(bool2);
                    int i8 = a21;
                    if (a2.isNull(i8)) {
                        num3 = null;
                    } else {
                        num3 = Integer.valueOf(a2.getInt(i8));
                    }
                    if (num3 == null) {
                        bool3 = null;
                    } else {
                        bool3 = Boolean.valueOf(num3.intValue() != 0);
                    }
                    movieEntity.setTV(bool3);
                    movieEntity.setNumberSeason(a2.getInt(a22));
                    int i9 = a23;
                    if (a2.isNull(i9)) {
                        l2 = null;
                    } else {
                        l2 = Long.valueOf(a2.getLong(i9));
                    }
                    movieEntity.setCollected_at(MovieEntity.Converter.a(l2));
                    int i10 = i3;
                    if (!a2.isNull(i10)) {
                        l3 = Long.valueOf(a2.getLong(i10));
                    }
                    movieEntity.setWatched_at(MovieEntity.Converter.a(l3));
                } else {
                    movieEntity = null;
                }
                a2.close();
                roomSQLiteQuery.b();
                return movieEntity;
            } catch (Throwable th) {
                th = th;
                a2.close();
                roomSQLiteQuery.b();
                throw th;
            }
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }
}
