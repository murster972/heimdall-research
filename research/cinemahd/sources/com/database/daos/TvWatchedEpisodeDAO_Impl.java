package com.database.daos;

import android.database.Cursor;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.database.entitys.MovieEntity;
import com.database.entitys.TvWatchedEpisode;
import com.facebook.react.uimanager.ViewProps;
import java.util.ArrayList;
import java.util.List;

public final class TvWatchedEpisodeDAO_Impl implements TvWatchedEpisodeDAO {

    /* renamed from: a  reason: collision with root package name */
    private final RoomDatabase f2511a;
    private final SharedSQLiteStatement b;
    private final SharedSQLiteStatement c;
    private final SharedSQLiteStatement d;
    private final SharedSQLiteStatement e;

    public TvWatchedEpisodeDAO_Impl(RoomDatabase roomDatabase) {
        this.f2511a = roomDatabase;
        this.b = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "INSERT INTO TvWatchedEpisode(tmdbID,imdbIDStr,tvdbID,traktID,season,episode,position,duration,subtitlepath) VALUES(?,?,?,?,?,?,?,?,?)";
            }
        };
        this.c = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "UPDATE TvWatchedEpisode SET tmdbID =?, imdbIDStr=?,tvdbID=?, traktID=?,season=?,episode=?,position=?,duration=?,subtitlepath=? WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?)) AND season=? AND episode=?";
            }
        };
        this.d = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM TvWatchedEpisode WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?)) AND season=? AND episode=?";
            }
        };
        new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM TvWatchedEpisode";
            }
        };
        this.e = new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM TvWatchedEpisode WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?)) AND season=? AND episode=?";
            }
        };
    }

    public int a(long j, String str, long j2, long j3, int i, int i2, long j4, long j5, String str2) {
        long j6 = j;
        String str3 = str;
        long j7 = j2;
        long j8 = j3;
        String str4 = str2;
        this.f2511a.b();
        SupportSQLiteStatement a2 = this.c.a();
        a2.a(1, j6);
        if (str3 == null) {
            a2.a(2);
        } else {
            a2.a(2, str3);
        }
        a2.a(3, j7);
        a2.a(4, j8);
        long j9 = (long) i;
        a2.a(5, j9);
        long j10 = (long) i2;
        a2.a(6, j10);
        long j11 = j10;
        a2.a(7, j4);
        a2.a(8, j5);
        if (str4 == null) {
            a2.a(9);
        } else {
            a2.a(9, str4);
        }
        a2.a(10, j6);
        if (str3 == null) {
            a2.a(11);
        } else {
            a2.a(11, str3);
        }
        a2.a(12, j8);
        a2.a(13, j7);
        a2.a(14, j9);
        a2.a(15, j11);
        this.f2511a.c();
        try {
            int o = a2.o();
            this.f2511a.k();
            return o;
        } finally {
            this.f2511a.e();
            this.c.a(a2);
        }
    }

    public /* synthetic */ void a(TvWatchedEpisode... tvWatchedEpisodeArr) {
        b.a(this, tvWatchedEpisodeArr);
    }

    public long b(long j, String str, long j2, long j3, int i, int i2, long j4, long j5, String str2) {
        String str3 = str;
        String str4 = str2;
        this.f2511a.b();
        SupportSQLiteStatement a2 = this.b.a();
        long j6 = j;
        a2.a(1, j);
        if (str3 == null) {
            a2.a(2);
        } else {
            a2.a(2, str);
        }
        long j7 = j2;
        a2.a(3, j2);
        long j8 = j3;
        a2.a(4, j3);
        a2.a(5, (long) i);
        a2.a(6, (long) i2);
        a2.a(7, j4);
        a2.a(8, j5);
        if (str4 == null) {
            a2.a(9);
        } else {
            a2.a(9, str4);
        }
        this.f2511a.c();
        try {
            long p = a2.p();
            this.f2511a.k();
            return p;
        } finally {
            this.f2511a.e();
            this.b.a(a2);
        }
    }

    public /* synthetic */ void b(TvWatchedEpisode... tvWatchedEpisodeArr) {
        b.b(this, tvWatchedEpisodeArr);
    }

    public List<TvWatchedEpisode> c(long j, String str, long j2, long j3, int i, int i2) {
        RoomSQLiteQuery roomSQLiteQuery;
        Long l;
        String str2 = str;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM TvWatchedEpisode WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?)) AND season=? AND episode=?", 6);
        b2.a(1, j);
        if (str2 == null) {
            b2.a(2);
        } else {
            b2.a(2, str2);
        }
        b2.a(3, j2);
        b2.a(4, j3);
        b2.a(5, (long) i);
        b2.a(6, (long) i2);
        this.f2511a.b();
        Cursor a2 = DBUtil.a(this.f2511a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "tvdbID");
            int a7 = CursorUtil.a(a2, "traktID");
            int a8 = CursorUtil.a(a2, "season");
            int a9 = CursorUtil.a(a2, "episode");
            int a10 = CursorUtil.a(a2, ViewProps.POSITION);
            int a11 = CursorUtil.a(a2, "duration");
            int a12 = CursorUtil.a(a2, "subtitlepath");
            int a13 = CursorUtil.a(a2, "collected_at");
            int a14 = CursorUtil.a(a2, "watched_at");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                roomSQLiteQuery = b2;
                try {
                    tvWatchedEpisode.b(a2.getInt(a3));
                    int i3 = a14;
                    ArrayList arrayList2 = arrayList;
                    tvWatchedEpisode.c(a2.getLong(a4));
                    tvWatchedEpisode.a(a2.getString(a5));
                    tvWatchedEpisode.e(a2.getLong(a6));
                    tvWatchedEpisode.d(a2.getLong(a7));
                    tvWatchedEpisode.c(a2.getInt(a8));
                    tvWatchedEpisode.a(a2.getInt(a9));
                    tvWatchedEpisode.b(a2.getLong(a10));
                    tvWatchedEpisode.a(a2.getLong(a11));
                    tvWatchedEpisode.b(a2.getString(a12));
                    Long l2 = null;
                    if (a2.isNull(a13)) {
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(a13));
                    }
                    tvWatchedEpisode.a(MovieEntity.Converter.a(l));
                    int i4 = i3;
                    if (!a2.isNull(i4)) {
                        l2 = Long.valueOf(a2.getLong(i4));
                    }
                    tvWatchedEpisode.b(MovieEntity.Converter.a(l2));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(tvWatchedEpisode);
                    arrayList = arrayList3;
                    a14 = i4;
                    b2 = roomSQLiteQuery;
                } catch (Throwable th) {
                    th = th;
                    a2.close();
                    roomSQLiteQuery.b();
                    throw th;
                }
            }
            ArrayList arrayList4 = arrayList;
            a2.close();
            b2.b();
            return arrayList4;
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public void b(long j, String str, long j2, long j3, int i, int i2) {
        this.f2511a.b();
        SupportSQLiteStatement a2 = this.e.a();
        a2.a(1, j);
        if (str == null) {
            a2.a(2);
        } else {
            a2.a(2, str);
        }
        a2.a(3, j2);
        a2.a(4, j3);
        a2.a(5, (long) i);
        a2.a(6, (long) i2);
        this.f2511a.c();
        try {
            a2.o();
            this.f2511a.k();
        } finally {
            this.f2511a.e();
            this.e.a(a2);
        }
    }

    public int a(long j, String str, long j2, long j3, int i, int i2) {
        this.f2511a.b();
        SupportSQLiteStatement a2 = this.d.a();
        a2.a(1, j);
        if (str == null) {
            a2.a(2);
        } else {
            a2.a(2, str);
        }
        a2.a(3, j3);
        a2.a(4, j2);
        a2.a(5, (long) i);
        a2.a(6, (long) i2);
        this.f2511a.c();
        try {
            int o = a2.o();
            this.f2511a.k();
            return o;
        } finally {
            this.f2511a.e();
            this.d.a(a2);
        }
    }

    public List<TvWatchedEpisode> a(long j, String str, long j2, long j3, int i) {
        RoomSQLiteQuery roomSQLiteQuery;
        Long l;
        String str2 = str;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM TvWatchedEpisode WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?)) AND season=?", 5);
        b2.a(1, j);
        if (str2 == null) {
            b2.a(2);
        } else {
            b2.a(2, str2);
        }
        b2.a(3, j2);
        b2.a(4, j3);
        b2.a(5, (long) i);
        this.f2511a.b();
        Cursor a2 = DBUtil.a(this.f2511a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "tvdbID");
            int a7 = CursorUtil.a(a2, "traktID");
            int a8 = CursorUtil.a(a2, "season");
            int a9 = CursorUtil.a(a2, "episode");
            int a10 = CursorUtil.a(a2, ViewProps.POSITION);
            int a11 = CursorUtil.a(a2, "duration");
            int a12 = CursorUtil.a(a2, "subtitlepath");
            int a13 = CursorUtil.a(a2, "collected_at");
            int a14 = CursorUtil.a(a2, "watched_at");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                roomSQLiteQuery = b2;
                try {
                    tvWatchedEpisode.b(a2.getInt(a3));
                    int i2 = a14;
                    ArrayList arrayList2 = arrayList;
                    tvWatchedEpisode.c(a2.getLong(a4));
                    tvWatchedEpisode.a(a2.getString(a5));
                    tvWatchedEpisode.e(a2.getLong(a6));
                    tvWatchedEpisode.d(a2.getLong(a7));
                    tvWatchedEpisode.c(a2.getInt(a8));
                    tvWatchedEpisode.a(a2.getInt(a9));
                    tvWatchedEpisode.b(a2.getLong(a10));
                    tvWatchedEpisode.a(a2.getLong(a11));
                    tvWatchedEpisode.b(a2.getString(a12));
                    Long l2 = null;
                    if (a2.isNull(a13)) {
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(a13));
                    }
                    tvWatchedEpisode.a(MovieEntity.Converter.a(l));
                    int i3 = i2;
                    if (!a2.isNull(i3)) {
                        l2 = Long.valueOf(a2.getLong(i3));
                    }
                    tvWatchedEpisode.b(MovieEntity.Converter.a(l2));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(tvWatchedEpisode);
                    arrayList = arrayList3;
                    a14 = i3;
                    b2 = roomSQLiteQuery;
                } catch (Throwable th) {
                    th = th;
                    a2.close();
                    roomSQLiteQuery.b();
                    throw th;
                }
            }
            ArrayList arrayList4 = arrayList;
            a2.close();
            b2.b();
            return arrayList4;
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public List<TvWatchedEpisode> a() {
        RoomSQLiteQuery roomSQLiteQuery;
        Long l;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM TvWatchedEpisode", 0);
        this.f2511a.b();
        Cursor a2 = DBUtil.a(this.f2511a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "tvdbID");
            int a7 = CursorUtil.a(a2, "traktID");
            int a8 = CursorUtil.a(a2, "season");
            int a9 = CursorUtil.a(a2, "episode");
            int a10 = CursorUtil.a(a2, ViewProps.POSITION);
            int a11 = CursorUtil.a(a2, "duration");
            int a12 = CursorUtil.a(a2, "subtitlepath");
            int a13 = CursorUtil.a(a2, "collected_at");
            int a14 = CursorUtil.a(a2, "watched_at");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                roomSQLiteQuery = b2;
                try {
                    tvWatchedEpisode.b(a2.getInt(a3));
                    int i = a14;
                    ArrayList arrayList2 = arrayList;
                    tvWatchedEpisode.c(a2.getLong(a4));
                    tvWatchedEpisode.a(a2.getString(a5));
                    tvWatchedEpisode.e(a2.getLong(a6));
                    tvWatchedEpisode.d(a2.getLong(a7));
                    tvWatchedEpisode.c(a2.getInt(a8));
                    tvWatchedEpisode.a(a2.getInt(a9));
                    tvWatchedEpisode.b(a2.getLong(a10));
                    tvWatchedEpisode.a(a2.getLong(a11));
                    tvWatchedEpisode.b(a2.getString(a12));
                    Long l2 = null;
                    if (a2.isNull(a13)) {
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(a13));
                    }
                    tvWatchedEpisode.a(MovieEntity.Converter.a(l));
                    if (!a2.isNull(i)) {
                        l2 = Long.valueOf(a2.getLong(i));
                    }
                    tvWatchedEpisode.b(MovieEntity.Converter.a(l2));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(tvWatchedEpisode);
                    arrayList = arrayList3;
                    a14 = i;
                    b2 = roomSQLiteQuery;
                } catch (Throwable th) {
                    th = th;
                    a2.close();
                    roomSQLiteQuery.b();
                    throw th;
                }
            }
            ArrayList arrayList4 = arrayList;
            a2.close();
            b2.b();
            return arrayList4;
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }

    public List<TvWatchedEpisode> a(long j, String str, long j2, long j3) {
        RoomSQLiteQuery roomSQLiteQuery;
        Long l;
        String str2 = str;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM TvWatchedEpisode WHERE ((tmdbID > 0 AND tmdbID=?) OR (imdbIDStr IS NOT NULL AND imdbIDStr=? ) OR (traktID >0 AND traktID=?) OR (tvdbID > 0 AND tvdbID=?))", 4);
        b2.a(1, j);
        if (str2 == null) {
            b2.a(2);
        } else {
            b2.a(2, str2);
        }
        b2.a(3, j2);
        b2.a(4, j3);
        this.f2511a.b();
        Cursor a2 = DBUtil.a(this.f2511a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "tmdbID");
            int a5 = CursorUtil.a(a2, "imdbIDStr");
            int a6 = CursorUtil.a(a2, "tvdbID");
            int a7 = CursorUtil.a(a2, "traktID");
            int a8 = CursorUtil.a(a2, "season");
            int a9 = CursorUtil.a(a2, "episode");
            int a10 = CursorUtil.a(a2, ViewProps.POSITION);
            int a11 = CursorUtil.a(a2, "duration");
            int a12 = CursorUtil.a(a2, "subtitlepath");
            int a13 = CursorUtil.a(a2, "collected_at");
            int a14 = CursorUtil.a(a2, "watched_at");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                TvWatchedEpisode tvWatchedEpisode = new TvWatchedEpisode();
                roomSQLiteQuery = b2;
                try {
                    tvWatchedEpisode.b(a2.getInt(a3));
                    int i = a14;
                    ArrayList arrayList2 = arrayList;
                    tvWatchedEpisode.c(a2.getLong(a4));
                    tvWatchedEpisode.a(a2.getString(a5));
                    tvWatchedEpisode.e(a2.getLong(a6));
                    tvWatchedEpisode.d(a2.getLong(a7));
                    tvWatchedEpisode.c(a2.getInt(a8));
                    tvWatchedEpisode.a(a2.getInt(a9));
                    tvWatchedEpisode.b(a2.getLong(a10));
                    tvWatchedEpisode.a(a2.getLong(a11));
                    tvWatchedEpisode.b(a2.getString(a12));
                    Long l2 = null;
                    if (a2.isNull(a13)) {
                        l = null;
                    } else {
                        l = Long.valueOf(a2.getLong(a13));
                    }
                    tvWatchedEpisode.a(MovieEntity.Converter.a(l));
                    int i2 = i;
                    if (!a2.isNull(i2)) {
                        l2 = Long.valueOf(a2.getLong(i2));
                    }
                    tvWatchedEpisode.b(MovieEntity.Converter.a(l2));
                    ArrayList arrayList3 = arrayList2;
                    arrayList3.add(tvWatchedEpisode);
                    arrayList = arrayList3;
                    a14 = i2;
                    b2 = roomSQLiteQuery;
                } catch (Throwable th) {
                    th = th;
                    a2.close();
                    roomSQLiteQuery.b();
                    throw th;
                }
            }
            ArrayList arrayList4 = arrayList;
            a2.close();
            b2.b();
            return arrayList4;
        } catch (Throwable th2) {
            th = th2;
            roomSQLiteQuery = b2;
            a2.close();
            roomSQLiteQuery.b();
            throw th;
        }
    }
}
