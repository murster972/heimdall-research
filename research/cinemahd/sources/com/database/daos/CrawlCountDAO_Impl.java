package com.database.daos;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.database.entitys.CrawlCount;
import java.util.ArrayList;
import java.util.List;

public final class CrawlCountDAO_Impl implements CrawlCountDAO {

    /* renamed from: a  reason: collision with root package name */
    private final RoomDatabase f2509a;

    public CrawlCountDAO_Impl(RoomDatabase roomDatabase) {
        this.f2509a = roomDatabase;
        new EntityInsertionAdapter<CrawlCount>(this, roomDatabase) {
            public String c() {
                return "INSERT OR IGNORE INTO `CrawlCount`(`id`,`provider`,`host`,`count`) VALUES (nullif(?, 0),?,?,?)";
            }

            public void a(SupportSQLiteStatement supportSQLiteStatement, CrawlCount crawlCount) {
                supportSQLiteStatement.a(1, (long) crawlCount.c());
                if (crawlCount.d() == null) {
                    supportSQLiteStatement.a(2);
                } else {
                    supportSQLiteStatement.a(2, crawlCount.d());
                }
                if (crawlCount.b() == null) {
                    supportSQLiteStatement.a(3);
                } else {
                    supportSQLiteStatement.a(3, crawlCount.b());
                }
                supportSQLiteStatement.a(4, (long) crawlCount.a());
            }
        };
        new EntityDeletionOrUpdateAdapter<CrawlCount>(this, roomDatabase) {
            public String c() {
                return "UPDATE OR IGNORE `CrawlCount` SET `id` = ?,`provider` = ?,`host` = ?,`count` = ? WHERE `id` = ?";
            }

            public void a(SupportSQLiteStatement supportSQLiteStatement, CrawlCount crawlCount) {
                supportSQLiteStatement.a(1, (long) crawlCount.c());
                if (crawlCount.d() == null) {
                    supportSQLiteStatement.a(2);
                } else {
                    supportSQLiteStatement.a(2, crawlCount.d());
                }
                if (crawlCount.b() == null) {
                    supportSQLiteStatement.a(3);
                } else {
                    supportSQLiteStatement.a(3, crawlCount.b());
                }
                supportSQLiteStatement.a(4, (long) crawlCount.a());
                supportSQLiteStatement.a(5, (long) crawlCount.c());
            }
        };
        new SharedSQLiteStatement(this, roomDatabase) {
            public String c() {
                return "DELETE FROM CrawlCount";
            }
        };
    }

    public List<CrawlCount> getAll() {
        RoomSQLiteQuery b = RoomSQLiteQuery.b("SELECT * FROM CrawlCount", 0);
        this.f2509a.b();
        Cursor a2 = DBUtil.a(this.f2509a, b, false);
        try {
            int a3 = CursorUtil.a(a2, "id");
            int a4 = CursorUtil.a(a2, "provider");
            int a5 = CursorUtil.a(a2, "host");
            int a6 = CursorUtil.a(a2, "count");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                CrawlCount crawlCount = new CrawlCount();
                crawlCount.b(a2.getInt(a3));
                crawlCount.b(a2.getString(a4));
                crawlCount.a(a2.getString(a5));
                crawlCount.a(a2.getInt(a6));
                arrayList.add(crawlCount);
            }
            return arrayList;
        } finally {
            a2.close();
            b.b();
        }
    }
}
