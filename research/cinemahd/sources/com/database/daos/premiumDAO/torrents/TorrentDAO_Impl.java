package com.database.daos.premiumDAO.torrents;

import android.database.Cursor;
import androidx.room.EntityDeletionOrUpdateAdapter;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.database.entitys.premiumEntitys.torrents.FileIDConverter;
import com.database.entitys.premiumEntitys.torrents.TorrentEntity;
import com.database.entitys.premiumEntitys.torrents.TorrentTypeConverter;
import java.util.ArrayList;
import java.util.List;

public final class TorrentDAO_Impl implements TorrentDAO {

    /* renamed from: a  reason: collision with root package name */
    private final RoomDatabase f2512a;
    private final EntityInsertionAdapter b;
    private final EntityDeletionOrUpdateAdapter c;

    public TorrentDAO_Impl(RoomDatabase roomDatabase) {
        this.f2512a = roomDatabase;
        this.b = new EntityInsertionAdapter<TorrentEntity>(this, roomDatabase) {
            public String c() {
                return "INSERT OR IGNORE INTO `TorrentEntity`(`hash`,`id`,`type`,`fileIDs`,`movieEntityID`) VALUES (?,?,?,?,?)";
            }

            public void a(SupportSQLiteStatement supportSQLiteStatement, TorrentEntity torrentEntity) {
                if (torrentEntity.b() == null) {
                    supportSQLiteStatement.a(1);
                } else {
                    supportSQLiteStatement.a(1, torrentEntity.b());
                }
                if (torrentEntity.c() == null) {
                    supportSQLiteStatement.a(2);
                } else {
                    supportSQLiteStatement.a(2, torrentEntity.c());
                }
                String a2 = TorrentTypeConverter.a(torrentEntity.e());
                if (a2 == null) {
                    supportSQLiteStatement.a(3);
                } else {
                    supportSQLiteStatement.a(3, a2);
                }
                String a3 = FileIDConverter.a(torrentEntity.a());
                if (a3 == null) {
                    supportSQLiteStatement.a(4);
                } else {
                    supportSQLiteStatement.a(4, a3);
                }
                supportSQLiteStatement.a(5, (long) torrentEntity.d());
            }
        };
        this.c = new EntityDeletionOrUpdateAdapter<TorrentEntity>(this, roomDatabase) {
            public String c() {
                return "UPDATE OR IGNORE `TorrentEntity` SET `hash` = ?,`id` = ?,`type` = ?,`fileIDs` = ?,`movieEntityID` = ? WHERE `hash` = ? AND `type` = ?";
            }

            public void a(SupportSQLiteStatement supportSQLiteStatement, TorrentEntity torrentEntity) {
                if (torrentEntity.b() == null) {
                    supportSQLiteStatement.a(1);
                } else {
                    supportSQLiteStatement.a(1, torrentEntity.b());
                }
                if (torrentEntity.c() == null) {
                    supportSQLiteStatement.a(2);
                } else {
                    supportSQLiteStatement.a(2, torrentEntity.c());
                }
                String a2 = TorrentTypeConverter.a(torrentEntity.e());
                if (a2 == null) {
                    supportSQLiteStatement.a(3);
                } else {
                    supportSQLiteStatement.a(3, a2);
                }
                String a3 = FileIDConverter.a(torrentEntity.a());
                if (a3 == null) {
                    supportSQLiteStatement.a(4);
                } else {
                    supportSQLiteStatement.a(4, a3);
                }
                supportSQLiteStatement.a(5, (long) torrentEntity.d());
                if (torrentEntity.b() == null) {
                    supportSQLiteStatement.a(6);
                } else {
                    supportSQLiteStatement.a(6, torrentEntity.b());
                }
                String a4 = TorrentTypeConverter.a(torrentEntity.e());
                if (a4 == null) {
                    supportSQLiteStatement.a(7);
                } else {
                    supportSQLiteStatement.a(7, a4);
                }
            }
        };
    }

    public void a(TorrentEntity... torrentEntityArr) {
        this.f2512a.b();
        this.f2512a.c();
        try {
            this.c.a(torrentEntityArr);
            this.f2512a.k();
        } finally {
            this.f2512a.e();
        }
    }

    public /* synthetic */ void b(TorrentEntity... torrentEntityArr) {
        a.a(this, torrentEntityArr);
    }

    public void c(TorrentEntity... torrentEntityArr) {
        this.f2512a.b();
        this.f2512a.c();
        try {
            this.b.a(torrentEntityArr);
            this.f2512a.k();
        } finally {
            this.f2512a.e();
        }
    }

    public List<TorrentEntity> a(int i) {
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM TorrentEntity WHERE movieEntityID = ?", 1);
        b2.a(1, (long) i);
        this.f2512a.b();
        Cursor a2 = DBUtil.a(this.f2512a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "hash");
            int a4 = CursorUtil.a(a2, "id");
            int a5 = CursorUtil.a(a2, "type");
            int a6 = CursorUtil.a(a2, "fileIDs");
            int a7 = CursorUtil.a(a2, "movieEntityID");
            ArrayList arrayList = new ArrayList(a2.getCount());
            while (a2.moveToNext()) {
                TorrentEntity torrentEntity = new TorrentEntity();
                torrentEntity.a(a2.getString(a3));
                torrentEntity.b(a2.getString(a4));
                torrentEntity.a(TorrentTypeConverter.a(a2.getString(a5)));
                torrentEntity.a(FileIDConverter.a(a2.getString(a6)));
                torrentEntity.a(a2.getInt(a7));
                arrayList.add(torrentEntity);
            }
            return arrayList;
        } finally {
            a2.close();
            b2.b();
        }
    }

    public TorrentEntity a(String str, String str2, String str3) {
        TorrentEntity torrentEntity;
        RoomSQLiteQuery b2 = RoomSQLiteQuery.b("SELECT * FROM TorrentEntity WHERE (hash = ? OR id = ?) AND type = ?", 3);
        if (str == null) {
            b2.a(1);
        } else {
            b2.a(1, str);
        }
        if (str2 == null) {
            b2.a(2);
        } else {
            b2.a(2, str2);
        }
        if (str3 == null) {
            b2.a(3);
        } else {
            b2.a(3, str3);
        }
        this.f2512a.b();
        Cursor a2 = DBUtil.a(this.f2512a, b2, false);
        try {
            int a3 = CursorUtil.a(a2, "hash");
            int a4 = CursorUtil.a(a2, "id");
            int a5 = CursorUtil.a(a2, "type");
            int a6 = CursorUtil.a(a2, "fileIDs");
            int a7 = CursorUtil.a(a2, "movieEntityID");
            if (a2.moveToFirst()) {
                torrentEntity = new TorrentEntity();
                torrentEntity.a(a2.getString(a3));
                torrentEntity.b(a2.getString(a4));
                torrentEntity.a(TorrentTypeConverter.a(a2.getString(a5)));
                torrentEntity.a(FileIDConverter.a(a2.getString(a6)));
                torrentEntity.a(a2.getInt(a7));
            } else {
                torrentEntity = null;
            }
            return torrentEntity;
        } finally {
            a2.close();
            b2.b();
        }
    }
}
