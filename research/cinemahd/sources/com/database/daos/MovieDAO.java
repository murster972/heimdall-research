package com.database.daos;

import com.database.entitys.MovieEntity;
import java.util.List;

public interface MovieDAO {
    int a(long j, String str, long j2, long j3, long j4, long j5, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i);

    int a(long j, String str, long j2, long j3, long j4, long j5, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i, Long l2);

    long a(long j, String str, long j2, long j3, long j4, long j5, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i, Long l2, Long l3);

    MovieEntity a(int i);

    MovieEntity a(long j, String str, long j2, long j3);

    List<MovieEntity> a();

    List<MovieEntity> a(Boolean bool);

    List<MovieEntity> a(Boolean bool, int i);

    List<MovieEntity> a(boolean z);

    void a(int i, long j, String str, long j2, long j3);

    void a(MovieEntity... movieEntityArr) throws Exception;

    int b();

    int b(long j, String str, long j2, long j3);

    int b(long j, String str, long j2, long j3, long j4, long j5, String str2, String str3, String str4, String str5, String str6, String str7, String str8, Double d, Long l, Boolean bool, Boolean bool2, Boolean bool3, int i, Long l2);

    int b(MovieEntity... movieEntityArr);

    List<MovieEntity> b(int i);

    int c();

    List<MovieEntity> d();

    int e();
}
