package com.database.daos;

import com.database.entitys.MovieEntity;
import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.ZoneOffset;

/* compiled from: MovieDAO */
public final /* synthetic */ class a {
    public static int a(MovieDAO _this, MovieEntity... movieEntityArr) {
        int i = 0;
        for (MovieEntity movieEntity : movieEntityArr) {
            if (movieEntity.getWatched_at() == null && movieEntity.getCollected_at() == null) {
                i += _this.b(movieEntity.getTmdbID(), movieEntity.getImdbIDStr(), movieEntity.getTraktID(), movieEntity.getTvdbID());
            } else {
                boolean z = true;
                if (movieEntity.getCollected_at() == null) {
                    long tmdbID = movieEntity.getTmdbID();
                    String imdbIDStr = movieEntity.getImdbIDStr();
                    long traktID = movieEntity.getTraktID();
                    long tvdbID = movieEntity.getTvdbID();
                    long position = movieEntity.getPosition();
                    long duration = movieEntity.getDuration();
                    String subtitlepath = movieEntity.getSubtitlepath();
                    String poster_path = movieEntity.getPoster_path();
                    String backdrop_path = movieEntity.getBackdrop_path();
                    String name = movieEntity.getName();
                    String realeaseDate = movieEntity.getRealeaseDate();
                    String overview = movieEntity.getOverview();
                    String a2 = MovieEntity.Converter.a(movieEntity.getGenres());
                    Double vote = movieEntity.getVote();
                    Long valueOf = Long.valueOf((movieEntity.getCreatedDate() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : movieEntity.getCreatedDate()).toEpochSecond());
                    Boolean valueOf2 = Boolean.valueOf(movieEntity.getCollected_at() != null);
                    if (movieEntity.getWatched_at() == null) {
                        z = false;
                    }
                    _this.b(tmdbID, imdbIDStr, traktID, tvdbID, position, duration, subtitlepath, poster_path, backdrop_path, name, realeaseDate, overview, a2, vote, valueOf, valueOf2, Boolean.valueOf(z), movieEntity.getTV(), movieEntity.getNumberSeason(), (Long) null);
                } else if (movieEntity.getWatched_at() == null) {
                    long tmdbID2 = movieEntity.getTmdbID();
                    String imdbIDStr2 = movieEntity.getImdbIDStr();
                    long traktID2 = movieEntity.getTraktID();
                    long tvdbID2 = movieEntity.getTvdbID();
                    long position2 = movieEntity.getPosition();
                    long duration2 = movieEntity.getDuration();
                    String subtitlepath2 = movieEntity.getSubtitlepath();
                    String poster_path2 = movieEntity.getPoster_path();
                    String backdrop_path2 = movieEntity.getBackdrop_path();
                    String name2 = movieEntity.getName();
                    String realeaseDate2 = movieEntity.getRealeaseDate();
                    String overview2 = movieEntity.getOverview();
                    String a3 = MovieEntity.Converter.a(movieEntity.getGenres());
                    Double vote2 = movieEntity.getVote();
                    Long valueOf3 = Long.valueOf((movieEntity.getCreatedDate() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : movieEntity.getCreatedDate()).toEpochSecond());
                    Boolean valueOf4 = Boolean.valueOf(movieEntity.getCollected_at() != null);
                    if (movieEntity.getWatched_at() == null) {
                        z = false;
                    }
                    _this.a(tmdbID2, imdbIDStr2, traktID2, tvdbID2, position2, duration2, subtitlepath2, poster_path2, backdrop_path2, name2, realeaseDate2, overview2, a3, vote2, valueOf3, valueOf4, Boolean.valueOf(z), movieEntity.getTV(), movieEntity.getNumberSeason(), (Long) null);
                }
            }
        }
        return i;
    }

    public static void b(MovieDAO _this, MovieEntity... movieEntityArr) throws Exception {
        for (MovieEntity movieEntity : movieEntityArr) {
            boolean z = true;
            if (_this.a(movieEntity.getTmdbID(), movieEntity.getImdbIDStr(), movieEntity.getTraktID(), movieEntity.getTvdbID()) == null) {
                long tmdbID = movieEntity.getTmdbID();
                String imdbIDStr = movieEntity.getImdbIDStr();
                long traktID = movieEntity.getTraktID();
                long tvdbID = movieEntity.getTvdbID();
                long position = movieEntity.getPosition();
                long duration = movieEntity.getDuration();
                String subtitlepath = movieEntity.getSubtitlepath();
                String poster_path = movieEntity.getPoster_path();
                String backdrop_path = movieEntity.getBackdrop_path();
                String name = movieEntity.getName();
                String realeaseDate = movieEntity.getRealeaseDate();
                String overview = movieEntity.getOverview();
                String a2 = MovieEntity.Converter.a(movieEntity.getGenres());
                Double vote = movieEntity.getVote();
                Long valueOf = Long.valueOf((movieEntity.getCreatedDate() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : movieEntity.getCreatedDate()).toEpochSecond());
                Boolean valueOf2 = Boolean.valueOf(movieEntity.getCollected_at() != null);
                if (movieEntity.getWatched_at() == null) {
                    z = false;
                }
                _this.a(tmdbID, imdbIDStr, traktID, tvdbID, position, duration, subtitlepath, poster_path, backdrop_path, name, realeaseDate, overview, a2, vote, valueOf, valueOf2, Boolean.valueOf(z), movieEntity.getTV(), movieEntity.getNumberSeason(), movieEntity.getCollected_at() == null ? null : Long.valueOf(movieEntity.getCollected_at().toEpochSecond()), movieEntity.getWatched_at() == null ? null : Long.valueOf(movieEntity.getWatched_at().toEpochSecond()));
            } else if (movieEntity.getCollected_at() != null) {
                long tmdbID2 = movieEntity.getTmdbID();
                String imdbIDStr2 = movieEntity.getImdbIDStr();
                long traktID2 = movieEntity.getTraktID();
                long tvdbID2 = movieEntity.getTvdbID();
                long position2 = movieEntity.getPosition();
                long duration2 = movieEntity.getDuration();
                String subtitlepath2 = movieEntity.getSubtitlepath();
                String poster_path2 = movieEntity.getPoster_path();
                String backdrop_path2 = movieEntity.getBackdrop_path();
                String name2 = movieEntity.getName();
                String realeaseDate2 = movieEntity.getRealeaseDate();
                String overview2 = movieEntity.getOverview();
                String a3 = MovieEntity.Converter.a(movieEntity.getGenres());
                Double vote2 = movieEntity.getVote();
                Long valueOf3 = Long.valueOf((movieEntity.getCreatedDate() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : movieEntity.getCreatedDate()).toEpochSecond());
                Boolean valueOf4 = Boolean.valueOf(movieEntity.getCollected_at() != null);
                if (movieEntity.getWatched_at() == null) {
                    z = false;
                }
                _this.b(tmdbID2, imdbIDStr2, traktID2, tvdbID2, position2, duration2, subtitlepath2, poster_path2, backdrop_path2, name2, realeaseDate2, overview2, a3, vote2, valueOf3, valueOf4, Boolean.valueOf(z), movieEntity.getTV(), movieEntity.getNumberSeason(), Long.valueOf(movieEntity.getCollected_at().toEpochSecond()));
            } else if (movieEntity.getWatched_at() != null) {
                long tmdbID3 = movieEntity.getTmdbID();
                String imdbIDStr3 = movieEntity.getImdbIDStr();
                long traktID3 = movieEntity.getTraktID();
                long tvdbID3 = movieEntity.getTvdbID();
                long position3 = movieEntity.getPosition();
                long duration3 = movieEntity.getDuration();
                String subtitlepath3 = movieEntity.getSubtitlepath();
                String poster_path3 = movieEntity.getPoster_path();
                String backdrop_path3 = movieEntity.getBackdrop_path();
                String name3 = movieEntity.getName();
                String realeaseDate3 = movieEntity.getRealeaseDate();
                String overview3 = movieEntity.getOverview();
                String a4 = MovieEntity.Converter.a(movieEntity.getGenres());
                Double vote3 = movieEntity.getVote();
                Long valueOf5 = Long.valueOf((movieEntity.getCreatedDate() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : movieEntity.getCreatedDate()).toEpochSecond());
                Boolean valueOf6 = Boolean.valueOf(movieEntity.getCollected_at() != null);
                if (movieEntity.getWatched_at() == null) {
                    z = false;
                }
                _this.a(tmdbID3, imdbIDStr3, traktID3, tvdbID3, position3, duration3, subtitlepath3, poster_path3, backdrop_path3, name3, realeaseDate3, overview3, a4, vote3, valueOf5, valueOf6, Boolean.valueOf(z), movieEntity.getTV(), movieEntity.getNumberSeason(), Long.valueOf(movieEntity.getWatched_at().toEpochSecond()));
            } else {
                long tmdbID4 = movieEntity.getTmdbID();
                String imdbIDStr4 = movieEntity.getImdbIDStr();
                long traktID4 = movieEntity.getTraktID();
                long tvdbID4 = movieEntity.getTvdbID();
                long position4 = movieEntity.getPosition();
                long duration4 = movieEntity.getDuration();
                String subtitlepath4 = movieEntity.getSubtitlepath();
                String poster_path4 = movieEntity.getPoster_path();
                String backdrop_path4 = movieEntity.getBackdrop_path();
                String name4 = movieEntity.getName();
                String realeaseDate4 = movieEntity.getRealeaseDate();
                String overview4 = movieEntity.getOverview();
                String a5 = MovieEntity.Converter.a(movieEntity.getGenres());
                Double vote4 = movieEntity.getVote();
                Long valueOf7 = Long.valueOf((movieEntity.getCreatedDate() == null ? OffsetDateTime.now((ZoneId) ZoneOffset.UTC) : movieEntity.getCreatedDate()).toEpochSecond());
                Boolean valueOf8 = Boolean.valueOf(movieEntity.getCollected_at() != null);
                if (movieEntity.getWatched_at() == null) {
                    z = false;
                }
                _this.a(tmdbID4, imdbIDStr4, traktID4, tvdbID4, position4, duration4, subtitlepath4, poster_path4, backdrop_path4, name4, realeaseDate4, overview4, a5, vote4, valueOf7, valueOf8, Boolean.valueOf(z), movieEntity.getTV(), movieEntity.getNumberSeason());
            }
        }
    }
}
