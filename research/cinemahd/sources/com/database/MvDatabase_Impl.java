package com.database;

import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.RoomDatabase;
import androidx.room.RoomOpenHelper;
import androidx.room.util.DBUtil;
import androidx.room.util.TableInfo;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;
import com.database.daos.CrawlCountDAO;
import com.database.daos.CrawlCountDAO_Impl;
import com.database.daos.MovieDAO;
import com.database.daos.MovieDAO_Impl;
import com.database.daos.TvWatchedEpisodeDAO;
import com.database.daos.TvWatchedEpisodeDAO_Impl;
import com.database.daos.premiumDAO.torrents.TorrentDAO;
import com.database.daos.premiumDAO.torrents.TorrentDAO_Impl;
import com.facebook.react.uimanager.ViewProps;
import com.startapp.sdk.adsbase.model.AdPreferences;
import com.unity3d.ads.metadata.MediationMetaData;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public final class MvDatabase_Impl extends MvDatabase {
    private volatile TorrentDAO A;
    private volatile MovieDAO x;
    private volatile TvWatchedEpisodeDAO y;
    private volatile CrawlCountDAO z;

    public CrawlCountDAO l() {
        CrawlCountDAO crawlCountDAO;
        if (this.z != null) {
            return this.z;
        }
        synchronized (this) {
            if (this.z == null) {
                this.z = new CrawlCountDAO_Impl(this);
            }
            crawlCountDAO = this.z;
        }
        return crawlCountDAO;
    }

    public MovieDAO m() {
        MovieDAO movieDAO;
        if (this.x != null) {
            return this.x;
        }
        synchronized (this) {
            if (this.x == null) {
                this.x = new MovieDAO_Impl(this);
            }
            movieDAO = this.x;
        }
        return movieDAO;
    }

    public TorrentDAO n() {
        TorrentDAO torrentDAO;
        if (this.A != null) {
            return this.A;
        }
        synchronized (this) {
            if (this.A == null) {
                this.A = new TorrentDAO_Impl(this);
            }
            torrentDAO = this.A;
        }
        return torrentDAO;
    }

    public TvWatchedEpisodeDAO o() {
        TvWatchedEpisodeDAO tvWatchedEpisodeDAO;
        if (this.y != null) {
            return this.y;
        }
        synchronized (this) {
            if (this.y == null) {
                this.y = new TvWatchedEpisodeDAO_Impl(this);
            }
            tvWatchedEpisodeDAO = this.y;
        }
        return tvWatchedEpisodeDAO;
    }

    /* access modifiers changed from: protected */
    public InvalidationTracker d() {
        return new InvalidationTracker(this, new HashMap(0), new HashMap(0), "MovieEntity", "TvWatchedEpisode", "GenreEntity", "CrawlCount", "RealDebridEntity", "TorrentEntity");
    }

    /* access modifiers changed from: protected */
    public SupportSQLiteOpenHelper a(DatabaseConfiguration databaseConfiguration) {
        RoomOpenHelper roomOpenHelper = new RoomOpenHelper(databaseConfiguration, new RoomOpenHelper.Delegate(12) {
            public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `MovieEntity` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `tmdbID` INTEGER NOT NULL, `imdbIDStr` TEXT, `traktID` INTEGER NOT NULL, `tvdbID` INTEGER NOT NULL, `position` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `subtitlepath` TEXT, `poster_path` TEXT, `backdrop_path` TEXT, `name` TEXT, `realeaseDate` TEXT, `overview` TEXT, `genres` TEXT, `vote` REAL, `createdDate` INTEGER, `isFavorite` INTEGER, `isWatched` INTEGER NOT NULL, `isTV` INTEGER, `numberSeason` INTEGER NOT NULL, `collected_at` INTEGER, `watched_at` INTEGER)");
                supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_tmdbID` ON `MovieEntity` (`tmdbID`)");
                supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_imdbIDStr` ON `MovieEntity` (`imdbIDStr`)");
                supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_traktID` ON `MovieEntity` (`traktID`)");
                supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_tvdbID` ON `MovieEntity` (`tvdbID`)");
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `TvWatchedEpisode` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `tmdbID` INTEGER NOT NULL, `imdbIDStr` TEXT, `tvdbID` INTEGER NOT NULL, `traktID` INTEGER NOT NULL, `season` INTEGER NOT NULL, `episode` INTEGER NOT NULL, `position` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `subtitlepath` TEXT, `collected_at` INTEGER, `watched_at` INTEGER)");
                supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_season` ON `TvWatchedEpisode` (`season`)");
                supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_episode` ON `TvWatchedEpisode` (`episode`)");
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `GenreEntity` (`id` INTEGER NOT NULL, `name` TEXT NOT NULL, `isTV` INTEGER NOT NULL, `sortField` TEXT, PRIMARY KEY(`id`, `name`, `isTV`))");
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `CrawlCount` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `provider` TEXT, `host` TEXT, `count` INTEGER NOT NULL)");
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `RealDebridEntity` (`tmdbID` INTEGER NOT NULL, `imdbIDStr` TEXT, `traktID` INTEGER NOT NULL, `tvdbID` INTEGER NOT NULL, `season` INTEGER NOT NULL, `episode` INTEGER NOT NULL, `percent` REAL NOT NULL, `status` TEXT, `id` TEXT NOT NULL, `name` TEXT, `hash` TEXT, `filesize` INTEGER NOT NULL, `fileLink` TEXT, PRIMARY KEY(`id`))");
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `TorrentEntity` (`hash` TEXT NOT NULL, `id` TEXT NOT NULL, `type` TEXT NOT NULL, `fileIDs` TEXT, `movieEntityID` INTEGER NOT NULL, PRIMARY KEY(`hash`, `type`), FOREIGN KEY(`movieEntityID`) REFERENCES `MovieEntity`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
                supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS room_master_table (id INTEGER PRIMARY KEY,identity_hash TEXT)");
                supportSQLiteDatabase.b("INSERT OR REPLACE INTO room_master_table (id,identity_hash) VALUES(42, '4c256ba0e2bfe1447bdccb13d99fa447')");
            }

            public void b(SupportSQLiteDatabase supportSQLiteDatabase) {
                supportSQLiteDatabase.b("DROP TABLE IF EXISTS `MovieEntity`");
                supportSQLiteDatabase.b("DROP TABLE IF EXISTS `TvWatchedEpisode`");
                supportSQLiteDatabase.b("DROP TABLE IF EXISTS `GenreEntity`");
                supportSQLiteDatabase.b("DROP TABLE IF EXISTS `CrawlCount`");
                supportSQLiteDatabase.b("DROP TABLE IF EXISTS `RealDebridEntity`");
                supportSQLiteDatabase.b("DROP TABLE IF EXISTS `TorrentEntity`");
            }

            /* access modifiers changed from: protected */
            public void c(SupportSQLiteDatabase supportSQLiteDatabase) {
                if (MvDatabase_Impl.this.g != null) {
                    int size = MvDatabase_Impl.this.g.size();
                    for (int i = 0; i < size; i++) {
                        ((RoomDatabase.Callback) MvDatabase_Impl.this.g.get(i)).a(supportSQLiteDatabase);
                    }
                }
            }

            public void d(SupportSQLiteDatabase supportSQLiteDatabase) {
                SupportSQLiteDatabase unused = MvDatabase_Impl.this.f1025a = supportSQLiteDatabase;
                supportSQLiteDatabase.b("PRAGMA foreign_keys = ON");
                MvDatabase_Impl.this.a(supportSQLiteDatabase);
                if (MvDatabase_Impl.this.g != null) {
                    int size = MvDatabase_Impl.this.g.size();
                    for (int i = 0; i < size; i++) {
                        ((RoomDatabase.Callback) MvDatabase_Impl.this.g.get(i)).b(supportSQLiteDatabase);
                    }
                }
            }

            public void e(SupportSQLiteDatabase supportSQLiteDatabase) {
            }

            public void f(SupportSQLiteDatabase supportSQLiteDatabase) {
                DBUtil.a(supportSQLiteDatabase);
            }

            /* access modifiers changed from: protected */
            public void g(SupportSQLiteDatabase supportSQLiteDatabase) {
                SupportSQLiteDatabase supportSQLiteDatabase2 = supportSQLiteDatabase;
                HashMap hashMap = new HashMap(22);
                hashMap.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
                hashMap.put("tmdbID", new TableInfo.Column("tmdbID", "INTEGER", true, 0));
                hashMap.put("imdbIDStr", new TableInfo.Column("imdbIDStr", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("traktID", new TableInfo.Column("traktID", "INTEGER", true, 0));
                hashMap.put("tvdbID", new TableInfo.Column("tvdbID", "INTEGER", true, 0));
                hashMap.put(ViewProps.POSITION, new TableInfo.Column(ViewProps.POSITION, "INTEGER", true, 0));
                hashMap.put("duration", new TableInfo.Column("duration", "INTEGER", true, 0));
                hashMap.put("subtitlepath", new TableInfo.Column("subtitlepath", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("poster_path", new TableInfo.Column("poster_path", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("backdrop_path", new TableInfo.Column("backdrop_path", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put(MediationMetaData.KEY_NAME, new TableInfo.Column(MediationMetaData.KEY_NAME, AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("realeaseDate", new TableInfo.Column("realeaseDate", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("overview", new TableInfo.Column("overview", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("genres", new TableInfo.Column("genres", AdPreferences.TYPE_TEXT, false, 0));
                hashMap.put("vote", new TableInfo.Column("vote", "REAL", false, 0));
                hashMap.put("createdDate", new TableInfo.Column("createdDate", "INTEGER", false, 0));
                hashMap.put("isFavorite", new TableInfo.Column("isFavorite", "INTEGER", false, 0));
                hashMap.put("isWatched", new TableInfo.Column("isWatched", "INTEGER", true, 0));
                hashMap.put("isTV", new TableInfo.Column("isTV", "INTEGER", false, 0));
                hashMap.put("numberSeason", new TableInfo.Column("numberSeason", "INTEGER", true, 0));
                hashMap.put("collected_at", new TableInfo.Column("collected_at", "INTEGER", false, 0));
                hashMap.put("watched_at", new TableInfo.Column("watched_at", "INTEGER", false, 0));
                HashSet hashSet = new HashSet(0);
                HashSet hashSet2 = new HashSet(4);
                String str = MediationMetaData.KEY_NAME;
                String str2 = "duration";
                hashSet2.add(new TableInfo.Index("index_MovieEntity_tmdbID", false, Arrays.asList(new String[]{"tmdbID"})));
                hashSet2.add(new TableInfo.Index("index_MovieEntity_imdbIDStr", false, Arrays.asList(new String[]{"imdbIDStr"})));
                hashSet2.add(new TableInfo.Index("index_MovieEntity_traktID", false, Arrays.asList(new String[]{"traktID"})));
                hashSet2.add(new TableInfo.Index("index_MovieEntity_tvdbID", false, Arrays.asList(new String[]{"tvdbID"})));
                TableInfo tableInfo = new TableInfo("MovieEntity", hashMap, hashSet, hashSet2);
                TableInfo a2 = TableInfo.a(supportSQLiteDatabase2, "MovieEntity");
                if (tableInfo.equals(a2)) {
                    HashMap hashMap2 = new HashMap(12);
                    hashMap2.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
                    hashMap2.put("tmdbID", new TableInfo.Column("tmdbID", "INTEGER", true, 0));
                    hashMap2.put("imdbIDStr", new TableInfo.Column("imdbIDStr", AdPreferences.TYPE_TEXT, false, 0));
                    hashMap2.put("tvdbID", new TableInfo.Column("tvdbID", "INTEGER", true, 0));
                    hashMap2.put("traktID", new TableInfo.Column("traktID", "INTEGER", true, 0));
                    hashMap2.put("season", new TableInfo.Column("season", "INTEGER", true, 0));
                    hashMap2.put("episode", new TableInfo.Column("episode", "INTEGER", true, 0));
                    hashMap2.put(ViewProps.POSITION, new TableInfo.Column(ViewProps.POSITION, "INTEGER", true, 0));
                    String str3 = str2;
                    hashMap2.put(str3, new TableInfo.Column(str3, "INTEGER", true, 0));
                    hashMap2.put("subtitlepath", new TableInfo.Column("subtitlepath", AdPreferences.TYPE_TEXT, false, 0));
                    hashMap2.put("collected_at", new TableInfo.Column("collected_at", "INTEGER", false, 0));
                    hashMap2.put("watched_at", new TableInfo.Column("watched_at", "INTEGER", false, 0));
                    HashSet hashSet3 = new HashSet(0);
                    HashSet hashSet4 = new HashSet(2);
                    String str4 = "\n Found:\n";
                    String str5 = "season";
                    hashSet4.add(new TableInfo.Index("index_TvWatchedEpisode_season", false, Arrays.asList(new String[]{"season"})));
                    hashSet4.add(new TableInfo.Index("index_TvWatchedEpisode_episode", false, Arrays.asList(new String[]{"episode"})));
                    TableInfo tableInfo2 = new TableInfo("TvWatchedEpisode", hashMap2, hashSet3, hashSet4);
                    TableInfo a3 = TableInfo.a(supportSQLiteDatabase2, "TvWatchedEpisode");
                    if (tableInfo2.equals(a3)) {
                        HashMap hashMap3 = new HashMap(4);
                        hashMap3.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
                        String str6 = str;
                        hashMap3.put(str6, new TableInfo.Column(str6, AdPreferences.TYPE_TEXT, true, 2));
                        hashMap3.put("isTV", new TableInfo.Column("isTV", "INTEGER", true, 3));
                        hashMap3.put("sortField", new TableInfo.Column("sortField", AdPreferences.TYPE_TEXT, false, 0));
                        TableInfo tableInfo3 = new TableInfo("GenreEntity", hashMap3, new HashSet(0), new HashSet(0));
                        TableInfo a4 = TableInfo.a(supportSQLiteDatabase2, "GenreEntity");
                        if (tableInfo3.equals(a4)) {
                            HashMap hashMap4 = new HashMap(4);
                            hashMap4.put("id", new TableInfo.Column("id", "INTEGER", true, 1));
                            hashMap4.put("provider", new TableInfo.Column("provider", AdPreferences.TYPE_TEXT, false, 0));
                            hashMap4.put("host", new TableInfo.Column("host", AdPreferences.TYPE_TEXT, false, 0));
                            hashMap4.put("count", new TableInfo.Column("count", "INTEGER", true, 0));
                            TableInfo tableInfo4 = new TableInfo("CrawlCount", hashMap4, new HashSet(0), new HashSet(0));
                            TableInfo a5 = TableInfo.a(supportSQLiteDatabase2, "CrawlCount");
                            if (tableInfo4.equals(a5)) {
                                HashMap hashMap5 = new HashMap(13);
                                hashMap5.put("tmdbID", new TableInfo.Column("tmdbID", "INTEGER", true, 0));
                                hashMap5.put("imdbIDStr", new TableInfo.Column("imdbIDStr", AdPreferences.TYPE_TEXT, false, 0));
                                hashMap5.put("traktID", new TableInfo.Column("traktID", "INTEGER", true, 0));
                                hashMap5.put("tvdbID", new TableInfo.Column("tvdbID", "INTEGER", true, 0));
                                String str7 = str5;
                                hashMap5.put(str7, new TableInfo.Column(str7, "INTEGER", true, 0));
                                hashMap5.put("episode", new TableInfo.Column("episode", "INTEGER", true, 0));
                                hashMap5.put("percent", new TableInfo.Column("percent", "REAL", true, 0));
                                hashMap5.put(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, new TableInfo.Column(ReportDBAdapter.ReportColumns.COLUMN_REPORT_STATUS, AdPreferences.TYPE_TEXT, false, 0));
                                hashMap5.put("id", new TableInfo.Column("id", AdPreferences.TYPE_TEXT, true, 1));
                                hashMap5.put(str6, new TableInfo.Column(str6, AdPreferences.TYPE_TEXT, false, 0));
                                hashMap5.put("hash", new TableInfo.Column("hash", AdPreferences.TYPE_TEXT, false, 0));
                                hashMap5.put("filesize", new TableInfo.Column("filesize", "INTEGER", true, 0));
                                hashMap5.put("fileLink", new TableInfo.Column("fileLink", AdPreferences.TYPE_TEXT, false, 0));
                                TableInfo tableInfo5 = new TableInfo("RealDebridEntity", hashMap5, new HashSet(0), new HashSet(0));
                                TableInfo a6 = TableInfo.a(supportSQLiteDatabase2, "RealDebridEntity");
                                if (tableInfo5.equals(a6)) {
                                    HashMap hashMap6 = new HashMap(5);
                                    hashMap6.put("hash", new TableInfo.Column("hash", AdPreferences.TYPE_TEXT, true, 1));
                                    hashMap6.put("id", new TableInfo.Column("id", AdPreferences.TYPE_TEXT, true, 0));
                                    hashMap6.put("type", new TableInfo.Column("type", AdPreferences.TYPE_TEXT, true, 2));
                                    hashMap6.put("fileIDs", new TableInfo.Column("fileIDs", AdPreferences.TYPE_TEXT, false, 0));
                                    hashMap6.put("movieEntityID", new TableInfo.Column("movieEntityID", "INTEGER", true, 0));
                                    HashSet hashSet5 = new HashSet(1);
                                    hashSet5.add(new TableInfo.ForeignKey("MovieEntity", "NO ACTION", "NO ACTION", Arrays.asList(new String[]{"movieEntityID"}), Arrays.asList(new String[]{"id"})));
                                    TableInfo tableInfo6 = new TableInfo("TorrentEntity", hashMap6, hashSet5, new HashSet(0));
                                    TableInfo a7 = TableInfo.a(supportSQLiteDatabase2, "TorrentEntity");
                                    if (!tableInfo6.equals(a7)) {
                                        throw new IllegalStateException("Migration didn't properly handle TorrentEntity(com.database.entitys.premiumEntitys.torrents.TorrentEntity).\n Expected:\n" + tableInfo6 + str4 + a7);
                                    }
                                    return;
                                }
                                throw new IllegalStateException("Migration didn't properly handle RealDebridEntity(com.database.entitys.premiumEntitys.RealDebridEntity).\n Expected:\n" + tableInfo5 + str4 + a6);
                            }
                            throw new IllegalStateException("Migration didn't properly handle CrawlCount(com.database.entitys.CrawlCount).\n Expected:\n" + tableInfo4 + str4 + a5);
                        }
                        throw new IllegalStateException("Migration didn't properly handle GenreEntity(com.database.entitys.GenreEntity).\n Expected:\n" + tableInfo3 + str4 + a4);
                    }
                    throw new IllegalStateException("Migration didn't properly handle TvWatchedEpisode(com.database.entitys.TvWatchedEpisode).\n Expected:\n" + tableInfo2 + str4 + a3);
                }
                throw new IllegalStateException("Migration didn't properly handle MovieEntity(com.database.entitys.MovieEntity).\n Expected:\n" + tableInfo + "\n Found:\n" + a2);
            }
        }, "4c256ba0e2bfe1447bdccb13d99fa447", "171d96d17ef3476432b3836f9942b68e");
        SupportSQLiteOpenHelper.Configuration.Builder a2 = SupportSQLiteOpenHelper.Configuration.a(databaseConfiguration.b);
        a2.a(databaseConfiguration.c);
        a2.a((SupportSQLiteOpenHelper.Callback) roomOpenHelper);
        return databaseConfiguration.f1007a.a(a2.a());
    }
}
