package com.database;

import android.app.Application;
import dagger.internal.Factory;
import dagger.internal.Preconditions;
import javax.inject.Provider;

public final class DatabaseModule_ProviderDemoDatabaseFactory implements Factory<MvDatabase> {

    /* renamed from: a  reason: collision with root package name */
    private final DatabaseModule f2508a;
    private final Provider<Application> b;

    public DatabaseModule_ProviderDemoDatabaseFactory(DatabaseModule databaseModule, Provider<Application> provider) {
        this.f2508a = databaseModule;
        this.b = provider;
    }

    public static DatabaseModule_ProviderDemoDatabaseFactory a(DatabaseModule databaseModule, Provider<Application> provider) {
        return new DatabaseModule_ProviderDemoDatabaseFactory(databaseModule, provider);
    }

    public static MvDatabase b(DatabaseModule databaseModule, Provider<Application> provider) {
        return a(databaseModule, provider.get());
    }

    public static MvDatabase a(DatabaseModule databaseModule, Application application) {
        MvDatabase a2 = databaseModule.a(application);
        Preconditions.a(a2, "Cannot return null from a non-@Nullable @Provides method");
        return a2;
    }

    public MvDatabase get() {
        return b(this.f2508a, this.b);
    }
}
