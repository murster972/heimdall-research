package com.database;

import android.content.Context;
import android.os.Environment;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import com.database.daos.CrawlCountDAO;
import com.database.daos.MovieDAO;
import com.database.daos.TvWatchedEpisodeDAO;
import com.database.daos.premiumDAO.torrents.TorrentDAO;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public abstract class MvDatabase extends RoomDatabase {
    static MvDatabase j;
    private static final Object k = new Object();
    static final Migration l = new Migration(1, 2) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `CrawlCount` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `provider` TEXT, `host` TEXT, `count` INTEGER NOT NULL)");
        }
    };
    static final Migration m = new Migration(2, 3) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `TvWatchedEpisode_new2` (`tmdbID` INTEGER NOT NULL , `season` INTEGER NOT NULL,`episode` INTEGER NOT NULL, `position` INTEGER NOT NULL, PRIMARY KEY(`tmdbID`,`season`,`episode`), FOREIGN KEY(`tmdbID`) REFERENCES `MovieEntity`(`tmdbID`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
            supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_tmdbID2` ON `TvWatchedEpisode_new2` (`tmdbID`)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_season` ON `TvWatchedEpisode_new2` (`season`)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_episode` ON `TvWatchedEpisode_new2` (`episode`)");
            supportSQLiteDatabase.b("INSERT OR IGNORE INTO TvWatchedEpisode_new2 (`tmdbID`,`season`,`episode`,`position`) SELECT `tmdbID`,`season`,`episode`,`position`FROM TvWatchedEpisode");
            supportSQLiteDatabase.b("DROP TABLE TvWatchedEpisode");
            supportSQLiteDatabase.b("ALTER TABLE TvWatchedEpisode_new2 RENAME TO TvWatchedEpisode");
        }
    };
    static final Migration n = new Migration(3, 4) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity  ADD COLUMN tvdbID INTEGER NOT NULL DEFAULT -1");
        }
    };
    static final Migration o = new Migration(4, 5) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity  ADD COLUMN subtitlepath TEXT");
            supportSQLiteDatabase.b("ALTER TABLE TvWatchedEpisode  ADD COLUMN subtitlepath TEXT");
        }
    };
    static final Migration p = new Migration(5, 6) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity  ADD COLUMN duration INTEGER NOT NULL DEFAULT 0");
            supportSQLiteDatabase.b("ALTER TABLE TvWatchedEpisode  ADD COLUMN duration INTEGER NOT NULL DEFAULT 0");
        }
    };
    static final Migration q = new Migration(6, 7) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity  ADD COLUMN imdbIDStr TEXT");
        }
    };
    static final Migration r = new Migration(7, 8) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `TvWatchedEpisode_2` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `tmdbID` INTEGER NOT NULL, `imdbIDStr` TEXT, `tvdbID` INTEGER NOT NULL, `traktID` INTEGER NOT NULL, `season` INTEGER NOT NULL, `episode` INTEGER NOT NULL, `position` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `subtitlepath` TEXT)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_season_2` ON `TvWatchedEpisode_2` (`season`)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_TvWatchedEpisode_episode_2` ON `TvWatchedEpisode_2` (`episode`)");
            supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `MovieEntity_2` (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `tmdbID` INTEGER NOT NULL, `imdbIDStr` TEXT, `traktID` INTEGER NOT NULL, `tvdbID` INTEGER NOT NULL, `position` INTEGER NOT NULL, `duration` INTEGER NOT NULL, `subtitlepath` TEXT, `poster_path` TEXT, `backdrop_path` TEXT, `name` TEXT, `realeaseDate` TEXT, `overview` TEXT, `genres` TEXT, `vote` REAL, `createdDate` INTEGER, `isFavorite` INTEGER, `isTV` INTEGER, `numberSeason` INTEGER NOT NULL)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_tmdbID_2` ON `MovieEntity_2` (`tmdbID`)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_imdbIDStr_2` ON `MovieEntity_2` (`imdbIDStr`)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_traktID_2` ON `MovieEntity_2` (`traktID`)");
            supportSQLiteDatabase.b("CREATE  INDEX `index_MovieEntity_tvdbID_2` ON `MovieEntity_2` (`tvdbID`)");
            supportSQLiteDatabase.b("INSERT OR IGNORE INTO TvWatchedEpisode_2 (`tmdbID`,`imdbIDStr`,`tvdbID`,`traktID`,`season`,`episode`,`position`,`duration`,`subtitlepath`) SELECT `tmdbID`,NULL,0,0,`season`,`episode`,`position`,`duration`,`subtitlepath`FROM TvWatchedEpisode");
            supportSQLiteDatabase.b("INSERT OR IGNORE INTO MovieEntity_2 (`tmdbID`, `imdbIDStr`, `traktID`, `tvdbID`, `position`, `duration`, `subtitlepath`, `poster_path`, `backdrop_path`, `name`, `realeaseDate`, `overview`, `genres`, `vote`, `createdDate`, `isFavorite`, `isTV`, `numberSeason`)SELECT `tmdbID`,NULL,0,0,`position`,`duration`,`subtitlepath`,`poster_path`, `backdrop_path`, `name`, `realeaseDate`, `overview`, `genres`, `vote`, `createdDate`, `isFavorite`, `isTV`, `numberSeason`FROM MovieEntity");
            supportSQLiteDatabase.b("DROP TABLE TvWatchedEpisode");
            supportSQLiteDatabase.b("DROP TABLE MovieEntity");
            supportSQLiteDatabase.b("ALTER TABLE TvWatchedEpisode_2 RENAME TO TvWatchedEpisode");
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity_2 RENAME TO MovieEntity");
        }
    };
    static final Migration s = new Migration(8, 9) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `RealDebridEntity` (`id` TEXT NOT NULL, `tmdbID` INTEGER NOT NULL, `imdbIDStr` TEXT, `traktID` INTEGER NOT NULL, `tvdbID` INTEGER NOT NULL, `season` INTEGER NOT NULL, `episode` INTEGER NOT NULL, `percent` REAL NOT NULL, `status` TEXT, `name` TEXT, `hash` TEXT, `filesize` INTEGER NOT NULL, `fileLink` TEXT, PRIMARY KEY(`id`))");
        }
    };
    static final Migration t = new Migration(9, 10) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("CREATE TABLE IF NOT EXISTS `TorrentEntity` (`hash` TEXT NOT NULL, `id` TEXT NOT NULL, `type` TEXT NOT NULL, `fileIDs` TEXT, `movieEntityID` INTEGER NOT NULL, PRIMARY KEY(`hash`, `type`), FOREIGN KEY(`movieEntityID`) REFERENCES `MovieEntity`(`id`) ON UPDATE NO ACTION ON DELETE NO ACTION )");
        }
    };
    static final Migration u = new Migration(10, 11) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity ADD COLUMN isWatched INTEGER DEFAULT 1 NOT NULL");
        }
    };
    static final Migration v = new Migration(11, 12) {
        public void a(SupportSQLiteDatabase supportSQLiteDatabase) {
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity ADD COLUMN collected_at INTEGER");
            supportSQLiteDatabase.b("ALTER TABLE MovieEntity ADD COLUMN watched_at INTEGER ");
            supportSQLiteDatabase.b("ALTER TABLE TvWatchedEpisode ADD COLUMN collected_at INTEGER");
            supportSQLiteDatabase.b("ALTER TABLE TvWatchedEpisode ADD COLUMN watched_at INTEGER ");
            supportSQLiteDatabase.b("UPDATE MovieEntity SET collected_at = createdDate WHERE isFavorite = 1");
            supportSQLiteDatabase.b("UPDATE MovieEntity SET watched_at = createdDate WHERE isWatched = 1");
        }
    };
    public static final String w = (Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/cinemahd/%s/history-favorites");

    public static MvDatabase a(Context context) {
        MvDatabase mvDatabase;
        synchronized (k) {
            if (j == null) {
                RoomDatabase.Builder<MvDatabase> a2 = Room.a(context, MvDatabase.class, "MvDatabaseDB");
                a2.a(l, m, n, o, p, q, r, s, t, u, v);
                j = a2.a();
            }
            mvDatabase = j;
        }
        return mvDatabase;
    }

    public static void b(Context context, String str) throws IOException {
        String format = String.format(w, new Object[]{str});
        a(format, context.getDatabasePath("MvDatabaseDB").getAbsolutePath());
        a(format + "-shm", context.getDatabasePath("MvDatabaseDB-shm").getAbsolutePath());
        a(format + "-wal", context.getDatabasePath("MvDatabaseDB-wal").getAbsolutePath());
    }

    public abstract CrawlCountDAO l();

    public abstract MovieDAO m();

    public abstract TorrentDAO n();

    public abstract TvWatchedEpisodeDAO o();

    public static void a(Context context, String str) throws IOException {
        String format = String.format(w, new Object[]{str});
        a(context.getDatabasePath("MvDatabaseDB").getAbsolutePath(), format);
        String absolutePath = context.getDatabasePath("MvDatabaseDB-shm").getAbsolutePath();
        a(absolutePath, format + "-shm");
        String absolutePath2 = context.getDatabasePath("MvDatabaseDB-wal").getAbsolutePath();
        a(absolutePath2, format + "-wal");
    }

    private static void a(String str, String str2) throws IOException {
        FileChannel channel = new FileInputStream(new File(str)).getChannel();
        FileChannel channel2 = new FileOutputStream(new File(str2)).getChannel();
        channel2.transferFrom(channel, 0, channel.size());
        channel.close();
        channel2.close();
    }
}
