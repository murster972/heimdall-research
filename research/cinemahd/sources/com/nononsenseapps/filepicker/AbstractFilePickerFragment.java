package com.nononsenseapps.filepicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;
import com.nononsenseapps.filepicker.NewItemFragment;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractFilePickerFragment<T> extends Fragment implements LoaderManager.LoaderCallbacks<SortedList<T>>, NewItemFragment.OnNewFolderListener, LogicHandler<T> {

    /* renamed from: a  reason: collision with root package name */
    protected final HashSet<T> f5821a = new HashSet<>();
    protected final HashSet<AbstractFilePickerFragment<T>.CheckableViewHolder> b = new HashSet<>();
    protected int c = 0;
    protected T d = null;
    protected boolean e = false;
    protected boolean f = false;
    protected boolean g = true;
    protected boolean h = false;
    protected OnFilePickedListener i;
    protected FileItemAdapter<T> j = null;
    protected TextView k;
    protected EditText l;
    protected RecyclerView m;
    protected LinearLayoutManager n;
    protected Toast o = null;
    protected boolean p = false;
    protected View q = null;
    protected View r = null;

    public class CheckableViewHolder extends AbstractFilePickerFragment<T>.DirViewHolder {
        public CheckBox e;

        public CheckableViewHolder(View view) {
            super(view);
            int i = 0;
            boolean z = AbstractFilePickerFragment.this.c == 3;
            this.e = (CheckBox) view.findViewById(R$id.checkbox);
            this.e.setVisibility((z || AbstractFilePickerFragment.this.h) ? 8 : i);
            this.e.setOnClickListener(new View.OnClickListener(AbstractFilePickerFragment.this) {
                public void onClick(View view) {
                    CheckableViewHolder checkableViewHolder = CheckableViewHolder.this;
                    AbstractFilePickerFragment.this.a((AbstractFilePickerFragment<T>.CheckableViewHolder) checkableViewHolder);
                }
            });
        }

        public void onClick(View view) {
            AbstractFilePickerFragment.this.a(view, (AbstractFilePickerFragment<T>.CheckableViewHolder) this);
        }

        public boolean onLongClick(View view) {
            return AbstractFilePickerFragment.this.b(view, (AbstractFilePickerFragment<T>.CheckableViewHolder) this);
        }
    }

    public class DirViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        /* renamed from: a  reason: collision with root package name */
        public View f5827a;
        public TextView b;
        public T c;

        public DirViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
            this.f5827a = view.findViewById(R$id.item_icon);
            this.b = (TextView) view.findViewById(16908308);
        }

        public void onClick(View view) {
            AbstractFilePickerFragment.this.a(view, (AbstractFilePickerFragment<T>.DirViewHolder) this);
        }

        public boolean onLongClick(View view) {
            return AbstractFilePickerFragment.this.b(view, (AbstractFilePickerFragment<T>.DirViewHolder) this);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        /* renamed from: a  reason: collision with root package name */
        final TextView f5828a;

        public HeaderViewHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            this.f5828a = (TextView) view.findViewById(16908308);
        }

        public void onClick(View view) {
            AbstractFilePickerFragment.this.a(view, (AbstractFilePickerFragment<T>.HeaderViewHolder) this);
        }
    }

    public interface OnFilePickedListener {
        void a(Uri uri);

        void a(List<Uri> list);

        void b();
    }

    public AbstractFilePickerFragment() {
        setRetainInstance(true);
    }

    public void b(View view) {
        Uri uri;
        if (this.i != null) {
            if ((this.f || this.c == 0) && (this.f5821a.isEmpty() || g() == null)) {
                if (this.o == null) {
                    this.o = Toast.makeText(getActivity(), R$string.nnf_select_something_first, 0);
                }
                this.o.show();
                return;
            }
            int i2 = this.c;
            if (i2 == 3) {
                String h2 = h();
                if (h2.startsWith("/")) {
                    uri = a(a(h2));
                } else {
                    uri = a(a(Utils.a(c(this.d), h2)));
                }
                this.i.a(uri);
            } else if (this.f) {
                this.i.a(a(this.f5821a));
            } else if (i2 == 0) {
                this.i.a(a(g()));
            } else if (i2 == 1) {
                this.i.a(a(this.d));
            } else if (this.f5821a.isEmpty()) {
                this.i.a(a(this.d));
            } else {
                this.i.a(a(g()));
            }
        }
    }

    public boolean b(View view, AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder) {
        return false;
    }

    public void e() {
        Iterator<AbstractFilePickerFragment<T>.CheckableViewHolder> it2 = this.b.iterator();
        while (it2.hasNext()) {
            it2.next().e.setChecked(false);
        }
        this.b.clear();
        this.f5821a.clear();
    }

    /* access modifiers changed from: protected */
    public FileItemAdapter<T> f() {
        return new FileItemAdapter<>(this);
    }

    public T g() {
        Iterator<T> it2 = this.f5821a.iterator();
        if (it2.hasNext()) {
            return it2.next();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public void g(T t) {
    }

    /* access modifiers changed from: protected */
    public String h() {
        return this.l.getText().toString();
    }

    /* access modifiers changed from: protected */
    public boolean h(T t) {
        return true;
    }

    public boolean i(T t) {
        if (!d(t)) {
            int i2 = this.c;
            if (i2 == 0 || i2 == 2 || this.g) {
                return true;
            }
            return false;
        } else if ((this.c != 1 || !this.f) && (this.c != 2 || !this.f)) {
            return false;
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void j() {
        int i2 = 0;
        boolean z = this.c == 3;
        this.q.setVisibility(z ? 0 : 8);
        View view = this.r;
        if (z) {
            i2 = 8;
        }
        view.setVisibility(i2);
        if (!z && this.h) {
            getActivity().findViewById(R$id.nnf_button_ok).setVisibility(8);
        }
    }

    /* access modifiers changed from: protected */
    public void k(T t) {
        if (h(t)) {
            this.d = t;
            this.p = true;
            getLoaderManager().a(0, (Bundle) null, this);
            return;
        }
        g(t);
    }

    public void onActivityCreated(Bundle bundle) {
        String string;
        super.onActivityCreated(bundle);
        if (this.d == null) {
            if (bundle != null) {
                this.c = bundle.getInt("KEY_MODE", this.c);
                this.e = bundle.getBoolean("KEY_ALLOW_DIR_CREATE", this.e);
                this.f = bundle.getBoolean("KEY_ALLOW_MULTIPLE", this.f);
                this.g = bundle.getBoolean("KEY_ALLOW_EXISTING_FILE", this.g);
                this.h = bundle.getBoolean("KEY_SINGLE_CLICK", this.h);
                String string2 = bundle.getString("KEY_CURRENT_PATH");
                if (string2 != null) {
                    this.d = a(string2.trim());
                }
            } else if (getArguments() != null) {
                this.c = getArguments().getInt("KEY_MODE", this.c);
                this.e = getArguments().getBoolean("KEY_ALLOW_DIR_CREATE", this.e);
                this.f = getArguments().getBoolean("KEY_ALLOW_MULTIPLE", this.f);
                this.g = getArguments().getBoolean("KEY_ALLOW_EXISTING_FILE", this.g);
                this.h = getArguments().getBoolean("KEY_SINGLE_CLICK", this.h);
                if (getArguments().containsKey("KEY_START_PATH") && (string = getArguments().getString("KEY_START_PATH")) != null) {
                    T a2 = a(string.trim());
                    if (d(a2)) {
                        this.d = a2;
                    } else {
                        this.d = e(a2);
                        this.l.setText(b(a2));
                    }
                }
            }
        }
        j();
        if (this.d == null) {
            this.d = getRoot();
        }
        k(this.d);
    }

    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            this.i = (OnFilePickedListener) context;
        } catch (ClassCastException unused) {
            throw new ClassCastException(context.toString() + " must implement OnFilePickedListener");
        }
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) {
        menuInflater.inflate(R$menu.picker_actions, menu);
        menu.findItem(R$id.nnf_action_createdir).setVisible(this.e);
    }

    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        TextView textView;
        View a2 = a(layoutInflater, viewGroup);
        Toolbar toolbar = (Toolbar) a2.findViewById(R$id.nnf_picker_toolbar);
        if (toolbar != null) {
            a(toolbar);
        }
        this.m = (RecyclerView) a2.findViewById(16908298);
        this.m.setHasFixedSize(true);
        this.n = new LinearLayoutManager(getActivity());
        this.m.setLayoutManager(this.n);
        a(layoutInflater, this.m);
        this.j = new FileItemAdapter<>(this);
        this.m.setAdapter(this.j);
        a2.findViewById(R$id.nnf_button_cancel).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AbstractFilePickerFragment.this.a(view);
            }
        });
        a2.findViewById(R$id.nnf_button_ok).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AbstractFilePickerFragment.this.b(view);
            }
        });
        a2.findViewById(R$id.nnf_button_ok_newfile).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                AbstractFilePickerFragment.this.b(view);
            }
        });
        this.q = a2.findViewById(R$id.nnf_newfile_button_container);
        this.r = a2.findViewById(R$id.nnf_button_container);
        this.l = (EditText) a2.findViewById(R$id.nnf_text_filename);
        this.l.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable editable) {
                AbstractFilePickerFragment.this.e();
            }

            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }

            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            }
        });
        this.k = (TextView) a2.findViewById(R$id.nnf_current_dir);
        T t = this.d;
        if (!(t == null || (textView = this.k) == null)) {
            textView.setText(c(t));
        }
        return a2;
    }

    public void onDetach() {
        super.onDetach();
        this.i = null;
    }

    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (R$id.nnf_action_createdir != menuItem.getItemId()) {
            return false;
        }
        FragmentActivity activity = getActivity();
        if (!(activity instanceof AppCompatActivity)) {
            return true;
        }
        NewFolderFragment.a(((AppCompatActivity) activity).getSupportFragmentManager(), this);
        return true;
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("KEY_CURRENT_PATH", this.d.toString());
        bundle.putBoolean("KEY_ALLOW_MULTIPLE", this.f);
        bundle.putBoolean("KEY_ALLOW_EXISTING_FILE", this.g);
        bundle.putBoolean("KEY_ALLOW_DIR_CREATE", this.e);
        bundle.putBoolean("KEY_SINGLE_CLICK", this.h);
        bundle.putInt("KEY_MODE", this.c);
        super.onSaveInstanceState(bundle);
    }

    public void a(String str, int i2, boolean z, boolean z2, boolean z3, boolean z4) {
        if (i2 == 3 && z) {
            throw new IllegalArgumentException("MODE_NEW_FILE does not support 'allowMultiple'");
        } else if (!z4 || !z) {
            Bundle arguments = getArguments();
            if (arguments == null) {
                arguments = new Bundle();
            }
            if (str != null) {
                arguments.putString("KEY_START_PATH", str);
            }
            arguments.putBoolean("KEY_ALLOW_DIR_CREATE", z2);
            arguments.putBoolean("KEY_ALLOW_MULTIPLE", z);
            arguments.putBoolean("KEY_ALLOW_EXISTING_FILE", z3);
            arguments.putBoolean("KEY_SINGLE_CLICK", z4);
            arguments.putInt("KEY_MODE", i2);
            setArguments(arguments);
        } else {
            throw new IllegalArgumentException("'singleClick' can not be used with 'allowMultiple'");
        }
    }

    public void f(T t) {
        if (!this.p) {
            this.f5821a.clear();
            this.b.clear();
            k(t);
        }
    }

    public void i() {
        f(e(this.d));
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0006, code lost:
        r2 = r1.c;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean j(T r2) {
        /*
            r1 = this;
            boolean r2 = r1.d(r2)
            if (r2 != 0) goto L_0x0017
            int r2 = r1.c
            if (r2 == 0) goto L_0x0017
            r0 = 2
            if (r2 == r0) goto L_0x0017
            r0 = 3
            if (r2 != r0) goto L_0x0015
            boolean r2 = r1.g
            if (r2 == 0) goto L_0x0015
            goto L_0x0017
        L_0x0015:
            r2 = 0
            goto L_0x0018
        L_0x0017:
            r2 = 1
        L_0x0018:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.nononsenseapps.filepicker.AbstractFilePickerFragment.j(java.lang.Object):boolean");
    }

    /* access modifiers changed from: protected */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup) {
        return layoutInflater.inflate(R$layout.nnf_fragment_filepicker, viewGroup, false);
    }

    /* access modifiers changed from: protected */
    public void a(LayoutInflater layoutInflater, RecyclerView recyclerView) {
        TypedArray obtainStyledAttributes = getActivity().obtainStyledAttributes(new int[]{R$attr.nnf_list_item_divider});
        Drawable drawable = obtainStyledAttributes.getDrawable(0);
        obtainStyledAttributes.recycle();
        if (drawable != null) {
            recyclerView.addItemDecoration(new DividerItemDecoration(drawable));
        }
    }

    public void a(View view) {
        OnFilePickedListener onFilePickedListener = this.i;
        if (onFilePickedListener != null) {
            onFilePickedListener.b();
        }
    }

    /* access modifiers changed from: protected */
    public void a(Toolbar toolbar) {
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
    }

    public boolean b(View view, AbstractFilePickerFragment<T>.CheckableViewHolder checkableViewHolder) {
        if (3 == this.c) {
            this.l.setText(b(checkableViewHolder.c));
        }
        a(checkableViewHolder);
        return true;
    }

    /* access modifiers changed from: protected */
    public List<Uri> a(Iterable<T> iterable) {
        ArrayList arrayList = new ArrayList();
        for (T a2 : iterable) {
            arrayList.add(a(a2));
        }
        return arrayList;
    }

    public Loader<SortedList<T>> a(int i2, Bundle bundle) {
        return b();
    }

    public void a(Loader<SortedList<T>> loader, SortedList<T> sortedList) {
        this.p = false;
        this.f5821a.clear();
        this.b.clear();
        this.j.a(sortedList);
        TextView textView = this.k;
        if (textView != null) {
            textView.setText(c(this.d));
        }
        getLoaderManager().a(0);
    }

    public void a(Loader<SortedList<T>> loader) {
        this.p = false;
    }

    public int a(int i2, T t) {
        return i(t) ? 2 : 1;
    }

    public void a(AbstractFilePickerFragment<T>.HeaderViewHolder headerViewHolder) {
        headerViewHolder.f5828a.setText("..");
    }

    public RecyclerView.ViewHolder a(ViewGroup viewGroup, int i2) {
        if (i2 == 0) {
            return new HeaderViewHolder(LayoutInflater.from(getActivity()).inflate(R$layout.nnf_filepicker_listitem_dir, viewGroup, false));
        }
        if (i2 != 2) {
            return new DirViewHolder(LayoutInflater.from(getActivity()).inflate(R$layout.nnf_filepicker_listitem_dir, viewGroup, false));
        }
        return new CheckableViewHolder(LayoutInflater.from(getActivity()).inflate(R$layout.nnf_filepicker_listitem_checkable, viewGroup, false));
    }

    public void a(AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder, int i2, T t) {
        dirViewHolder.c = t;
        dirViewHolder.f5827a.setVisibility(d(t) ? 0 : 8);
        dirViewHolder.b.setText(b(t));
        if (!i(t)) {
            return;
        }
        if (this.f5821a.contains(t)) {
            CheckableViewHolder checkableViewHolder = (CheckableViewHolder) dirViewHolder;
            this.b.add(checkableViewHolder);
            checkableViewHolder.e.setChecked(true);
            return;
        }
        this.b.remove(dirViewHolder);
        ((CheckableViewHolder) dirViewHolder).e.setChecked(false);
    }

    public void a(View view, AbstractFilePickerFragment<T>.HeaderViewHolder headerViewHolder) {
        i();
    }

    public void a(View view, AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder) {
        if (d(dirViewHolder.c)) {
            f(dirViewHolder.c);
        }
    }

    public void a(View view, AbstractFilePickerFragment<T>.CheckableViewHolder checkableViewHolder) {
        if (d(checkableViewHolder.c)) {
            f(checkableViewHolder.c);
            return;
        }
        b(view, checkableViewHolder);
        if (this.h) {
            b(view);
        }
    }

    public void a(AbstractFilePickerFragment<T>.CheckableViewHolder checkableViewHolder) {
        if (this.f5821a.contains(checkableViewHolder.c)) {
            checkableViewHolder.e.setChecked(false);
            this.f5821a.remove(checkableViewHolder.c);
            this.b.remove(checkableViewHolder);
            return;
        }
        if (!this.f) {
            e();
        }
        checkableViewHolder.e.setChecked(true);
        this.f5821a.add(checkableViewHolder.c);
        this.b.add(checkableViewHolder);
    }
}
