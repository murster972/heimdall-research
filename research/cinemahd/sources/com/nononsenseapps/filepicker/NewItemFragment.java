package com.nononsenseapps.filepicker;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

public abstract class NewItemFragment extends DialogFragment {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public OnNewFolderListener f5832a = null;

    public interface OnNewFolderListener {
        void b(String str);
    }

    /* access modifiers changed from: protected */
    public abstract boolean c(String str);

    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public Dialog onCreateDialog(Bundle bundle) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.d(R$layout.nnf_dialog_folder_name);
        builder.c(R$string.nnf_new_folder);
        builder.a(R$string.nnf_new_folder_cancel, (DialogInterface.OnClickListener) null);
        builder.b(R$string.nnf_new_folder_ok, (DialogInterface.OnClickListener) null);
        AlertDialog a2 = builder.a();
        a2.setOnShowListener(new DialogInterface.OnShowListener() {
            public void onShow(DialogInterface dialogInterface) {
                final AlertDialog alertDialog = (AlertDialog) dialogInterface;
                final EditText editText = (EditText) alertDialog.findViewById(R$id.edit_text);
                if (editText != null) {
                    alertDialog.b(-2).setOnClickListener(new View.OnClickListener(this) {
                        public void onClick(View view) {
                            alertDialog.cancel();
                        }
                    });
                    final Button b = alertDialog.b(-1);
                    b.setEnabled(false);
                    b.setOnClickListener(new View.OnClickListener() {
                        public void onClick(View view) {
                            String obj = editText.getText().toString();
                            if (NewItemFragment.this.c(obj)) {
                                if (NewItemFragment.this.f5832a != null) {
                                    NewItemFragment.this.f5832a.b(obj);
                                }
                                alertDialog.dismiss();
                            }
                        }
                    });
                    editText.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable editable) {
                            b.setEnabled(NewItemFragment.this.c(editable.toString()));
                        }

                        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                        }

                        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                        }
                    });
                    return;
                }
                throw new NullPointerException("Could not find an edit text in the dialog");
            }
        });
        return a2;
    }

    public void a(OnNewFolderListener onNewFolderListener) {
        this.f5832a = onNewFolderListener;
    }
}
