package com.nononsenseapps.filepicker;

import android.net.Uri;
import android.view.ViewGroup;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;

public interface LogicHandler<T> {
    int a(int i, T t);

    Uri a(T t);

    RecyclerView.ViewHolder a(ViewGroup viewGroup, int i);

    T a(String str);

    void a(AbstractFilePickerFragment<T>.DirViewHolder dirViewHolder, int i, T t);

    void a(AbstractFilePickerFragment<T>.HeaderViewHolder headerViewHolder);

    Loader<SortedList<T>> b();

    String b(T t);

    String c(T t);

    boolean d(T t);

    T e(T t);

    T getRoot();
}
