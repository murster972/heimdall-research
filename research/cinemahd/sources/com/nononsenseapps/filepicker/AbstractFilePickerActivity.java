package com.nononsenseapps.filepicker;

import android.annotation.TargetApi;
import android.content.ClipData;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFilePickerActivity<T> extends AppCompatActivity implements AbstractFilePickerFragment.OnFilePickedListener {

    /* renamed from: a  reason: collision with root package name */
    protected String f5820a = null;
    protected int b = 0;
    protected boolean c = false;
    protected boolean d = false;
    private boolean e = true;
    protected boolean f = false;

    /* access modifiers changed from: protected */
    public abstract AbstractFilePickerFragment<T> a(String str, int i, boolean z, boolean z2, boolean z3, boolean z4);

    public void a(Uri uri) {
        Intent intent = new Intent();
        intent.setData(uri);
        setResult(-1, intent);
        finish();
    }

    public void b() {
        setResult(0);
        finish();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R$layout.nnf_activity_filepicker);
        Intent intent = getIntent();
        if (intent != null) {
            this.f5820a = intent.getStringExtra("nononsense.intent.START_PATH");
            this.b = intent.getIntExtra("nononsense.intent.MODE", this.b);
            this.c = intent.getBooleanExtra("nononsense.intent.ALLOW_CREATE_DIR", this.c);
            this.d = intent.getBooleanExtra("android.intent.extra.ALLOW_MULTIPLE", this.d);
            this.e = intent.getBooleanExtra("android.intent.extra.ALLOW_EXISTING_FILE", this.e);
            this.f = intent.getBooleanExtra("nononsense.intent.SINGLE_CLICK", this.f);
        }
        setResult(0);
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        FragmentManager supportFragmentManager = getSupportFragmentManager();
        Fragment b2 = supportFragmentManager.b("filepicker_fragment");
        if (b2 == null) {
            b2 = a(this.f5820a, this.b, this.d, this.c, this.e, this.f);
        }
        if (b2 != null) {
            supportFragmentManager.b().b(R$id.fragment, b2, "filepicker_fragment").a();
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        bundle.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(bundle);
    }

    @TargetApi(16)
    public void a(List<Uri> list) {
        Intent intent = new Intent();
        intent.putExtra("android.intent.extra.ALLOW_MULTIPLE", true);
        ArrayList arrayList = new ArrayList();
        for (Uri uri : list) {
            arrayList.add(uri.toString());
        }
        intent.putStringArrayListExtra("nononsense.intent.PATHS", arrayList);
        if (Build.VERSION.SDK_INT >= 16) {
            ClipData clipData = null;
            for (Uri next : list) {
                if (clipData == null) {
                    clipData = new ClipData("Paths", new String[0], new ClipData.Item(next));
                } else {
                    clipData.addItem(new ClipData.Item(next));
                }
            }
            intent.setClipData(clipData);
        }
        setResult(-1, intent);
        finish();
    }
}
