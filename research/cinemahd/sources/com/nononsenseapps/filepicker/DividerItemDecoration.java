package com.nononsenseapps.filepicker;

import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    /* renamed from: a  reason: collision with root package name */
    private Drawable f5829a;

    public DividerItemDecoration(Drawable drawable) {
        this.f5829a = drawable;
    }

    public void a(Rect rect, View view, RecyclerView recyclerView, RecyclerView.State state) {
        super.a(rect, view, recyclerView, state);
        if (recyclerView.getChildAdapterPosition(view) != 0) {
            rect.top = this.f5829a.getIntrinsicHeight();
        }
    }

    public void a(Canvas canvas, RecyclerView recyclerView, RecyclerView.State state) {
        int paddingLeft = recyclerView.getPaddingLeft();
        int width = recyclerView.getWidth() - recyclerView.getPaddingRight();
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View childAt = recyclerView.getChildAt(i);
            int bottom = childAt.getBottom() + ((RecyclerView.LayoutParams) childAt.getLayoutParams()).bottomMargin;
            this.f5829a.setBounds(paddingLeft, bottom, width, this.f5829a.getIntrinsicHeight() + bottom);
            this.f5829a.draw(canvas);
        }
    }
}
