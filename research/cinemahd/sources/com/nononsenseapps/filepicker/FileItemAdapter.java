package com.nononsenseapps.filepicker;

import android.view.ViewGroup;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SortedList;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;

public class FileItemAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /* renamed from: a  reason: collision with root package name */
    protected final LogicHandler<T> f5830a;
    protected SortedList<T> b = null;

    public FileItemAdapter(LogicHandler<T> logicHandler) {
        this.f5830a = logicHandler;
    }

    public void a(SortedList<T> sortedList) {
        this.b = sortedList;
        notifyDataSetChanged();
    }

    public int getItemCount() {
        SortedList<T> sortedList = this.b;
        if (sortedList == null) {
            return 0;
        }
        return sortedList.c() + 1;
    }

    public int getItemViewType(int i) {
        if (i == 0) {
            return 0;
        }
        int i2 = i - 1;
        return this.f5830a.a(i2, this.b.a(i2));
    }

    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int i) {
        if (i == 0) {
            this.f5830a.a((AbstractFilePickerFragment<T>.HeaderViewHolder) (AbstractFilePickerFragment.HeaderViewHolder) viewHolder);
            return;
        }
        int i2 = i - 1;
        this.f5830a.a((AbstractFilePickerFragment.DirViewHolder) viewHolder, i2, this.b.a(i2));
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        return this.f5830a.a(viewGroup, i);
    }
}
