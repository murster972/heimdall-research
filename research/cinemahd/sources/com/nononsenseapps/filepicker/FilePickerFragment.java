package com.nononsenseapps.filepicker;

import android.content.Context;
import android.net.Uri;
import android.os.FileObserver;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.loader.content.AsyncTaskLoader;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.SortedList;
import androidx.recyclerview.widget.SortedListAdapterCallback;
import com.nononsenseapps.filepicker.AbstractFilePickerFragment;
import java.io.File;

public class FilePickerFragment extends AbstractFilePickerFragment<File> {
    protected boolean s = false;
    private File t = null;

    /* renamed from: f */
    public boolean d(File file) {
        return file.isDirectory();
    }

    public void onRequestPermissionsResult(int i, String[] strArr, int[] iArr) {
        if (strArr.length == 0) {
            AbstractFilePickerFragment.OnFilePickedListener onFilePickedListener = this.i;
            if (onFilePickedListener != null) {
                onFilePickedListener.b();
            }
        } else if (iArr[0] == 0) {
            File file = this.t;
            if (file != null) {
                k(file);
            }
        } else {
            Toast.makeText(getContext(), R$string.nnf_permission_external_write_denied, 0).show();
            AbstractFilePickerFragment.OnFilePickedListener onFilePickedListener2 = this.i;
            if (onFilePickedListener2 != null) {
                onFilePickedListener2.b();
            }
        }
    }

    public String b(File file) {
        return file.getName();
    }

    /* renamed from: c */
    public File e(File file) {
        return (!file.getPath().equals(getRoot().getPath()) && file.getParentFile() != null) ? file.getParentFile() : file;
    }

    /* access modifiers changed from: protected */
    /* renamed from: d */
    public void g(File file) {
        this.t = file;
        requestPermissions(new String[]{"android.permission.WRITE_EXTERNAL_STORAGE"}, 1);
    }

    /* access modifiers changed from: protected */
    /* renamed from: e */
    public boolean h(File file) {
        return ContextCompat.a(getContext(), "android.permission.WRITE_EXTERNAL_STORAGE") == 0;
    }

    /* access modifiers changed from: protected */
    public boolean g(File file) {
        if (this.s || !file.isHidden()) {
            return super.j(file);
        }
        return false;
    }

    public File getRoot() {
        return new File("/");
    }

    /* renamed from: h */
    public Uri a(File file) {
        Context context = getContext();
        return FileProvider.a(context, getContext().getApplicationContext().getPackageName() + ".provider", file);
    }

    public File a(String str) {
        return new File(str);
    }

    public Loader<SortedList<File>> b() {
        return new AsyncTaskLoader<SortedList<File>>(getActivity()) {
            FileObserver o;

            /* access modifiers changed from: protected */
            public void k() {
                super.k();
                FileObserver fileObserver = this.o;
                if (fileObserver != null) {
                    fileObserver.stopWatching();
                    this.o = null;
                }
            }

            /* access modifiers changed from: protected */
            public void l() {
                super.l();
                T t = FilePickerFragment.this.d;
                if (t == null || !((File) t).isDirectory()) {
                    FilePickerFragment filePickerFragment = FilePickerFragment.this;
                    filePickerFragment.d = filePickerFragment.getRoot();
                }
                this.o = new FileObserver(((File) FilePickerFragment.this.d).getPath(), 960) {
                    public void onEvent(int i, String str) {
                        AnonymousClass1.this.i();
                    }
                };
                this.o.startWatching();
                e();
            }

            public SortedList<File> t() {
                int i;
                File[] listFiles = ((File) FilePickerFragment.this.d).listFiles();
                if (listFiles == null) {
                    i = 0;
                } else {
                    i = listFiles.length;
                }
                SortedList<File> sortedList = new SortedList<>(File.class, new SortedListAdapterCallback<File>(FilePickerFragment.this.f()) {
                    /* renamed from: c */
                    public int compare(File file, File file2) {
                        return FilePickerFragment.this.a(file, file2);
                    }

                    public boolean a(File file, File file2) {
                        return file.getAbsolutePath().equals(file2.getAbsolutePath()) && file.isFile() == file2.isFile();
                    }

                    public boolean b(File file, File file2) {
                        return a(file, file2);
                    }
                }, i);
                sortedList.a();
                if (listFiles != null) {
                    for (File file : listFiles) {
                        if (FilePickerFragment.this.g(file)) {
                            sortedList.a(file);
                        }
                    }
                }
                sortedList.b();
                return sortedList;
            }
        };
    }

    /* renamed from: a */
    public String c(File file) {
        return file.getPath();
    }

    public void b(String str) {
        File file = new File((File) this.d, str);
        if (file.mkdir()) {
            k(file);
        } else {
            Toast.makeText(getActivity(), R$string.nnf_create_folder_error, 0).show();
        }
    }

    /* access modifiers changed from: protected */
    public int a(File file, File file2) {
        if (file.isDirectory() && !file2.isDirectory()) {
            return -1;
        }
        if (!file2.isDirectory() || file.isDirectory()) {
            return file.getName().compareToIgnoreCase(file2.getName());
        }
        return 1;
    }
}
