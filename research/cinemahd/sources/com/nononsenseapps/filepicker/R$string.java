package com.nononsenseapps.filepicker;

public final class R$string {
    public static final int abc_action_bar_home_description = 2131755014;
    public static final int abc_action_bar_up_description = 2131755015;
    public static final int abc_action_menu_overflow_description = 2131755016;
    public static final int abc_action_mode_done = 2131755017;
    public static final int abc_activity_chooser_view_see_all = 2131755018;
    public static final int abc_activitychooserview_choose_application = 2131755019;
    public static final int abc_capital_off = 2131755020;
    public static final int abc_capital_on = 2131755021;
    public static final int abc_search_hint = 2131755032;
    public static final int abc_searchview_description_clear = 2131755033;
    public static final int abc_searchview_description_query = 2131755034;
    public static final int abc_searchview_description_search = 2131755035;
    public static final int abc_searchview_description_submit = 2131755036;
    public static final int abc_searchview_description_voice = 2131755037;
    public static final int abc_shareactionprovider_share_with = 2131755038;
    public static final int abc_shareactionprovider_share_with_application = 2131755039;
    public static final int abc_toolbar_collapse_description = 2131755040;
    public static final int nnf_create_folder_error = 2131755447;
    public static final int nnf_filename = 2131755448;
    public static final int nnf_list_cancel = 2131755449;
    public static final int nnf_list_ok = 2131755450;
    public static final int nnf_name = 2131755451;
    public static final int nnf_need_valid_filename = 2131755452;
    public static final int nnf_new_folder = 2131755453;
    public static final int nnf_new_folder_cancel = 2131755454;
    public static final int nnf_new_folder_ok = 2131755455;
    public static final int nnf_permission_external_write_denied = 2131755456;
    public static final int nnf_select_something_first = 2131755457;
    public static final int search_menu_title = 2131755523;
    public static final int status_bar_notification_info_overflow = 2131755551;

    private R$string() {
    }
}
