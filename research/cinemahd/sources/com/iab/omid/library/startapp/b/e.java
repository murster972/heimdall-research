package com.iab.omid.library.startapp.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.startapp.a.a;
import com.iab.omid.library.startapp.b.b;
import com.startapp.common.b.c;

public final class e implements a, b.a {
    private static e d;

    /* renamed from: a  reason: collision with root package name */
    private float f4560a = 0.0f;
    private com.iab.omid.library.startapp.a.b b;
    private a c;

    private e(c cVar, com.iab.omid.library.startapp.d.c cVar2) {
    }

    public static e d() {
        if (d == null) {
            d = new e(new c(), new com.iab.omid.library.startapp.d.c());
        }
        return d;
    }

    public final void a() {
        b.d().a((b.a) this);
        b.d().a();
        if (b.d().c()) {
            com.iab.omid.library.startapp.walking.a.b();
            com.iab.omid.library.startapp.walking.a.c();
        }
        this.b.a();
    }

    public final void a(Context context) {
        this.b = new com.iab.omid.library.startapp.a.b(new Handler(), context, new com.iab.omid.library.startapp.d.a(), this);
    }

    public final void a(boolean z) {
        com.iab.omid.library.startapp.walking.a.b();
        if (z) {
            com.iab.omid.library.startapp.walking.a.c();
        } else {
            com.iab.omid.library.startapp.walking.a.d();
        }
    }

    public final void b() {
        com.iab.omid.library.startapp.walking.a.b().a();
        b.d().b();
        this.b.b();
    }

    public final float c() {
        return this.f4560a;
    }

    public final void a(float f) {
        this.f4560a = f;
        if (this.c == null) {
            this.c = a.c();
        }
        for (com.iab.omid.library.startapp.adsession.b e : this.c.b()) {
            e.e().a(f);
        }
    }
}
