package com.iab.omid.library.startapp.adsession;

import android.os.Build;
import android.view.View;
import com.iab.omid.library.startapp.b.c;
import com.iab.omid.library.startapp.b.d;
import com.iab.omid.library.startapp.publisher.AdSessionStatePublisher;
import com.startapp.common.b.e;
import com.startapp.common.c.a;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import org.json.JSONArray;
import org.json.JSONObject;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private final a f4552a;
    private final e b;
    private final List<com.iab.omid.library.startapp.e.a> c;
    private com.iab.omid.library.startapp.e.a d;
    private AdSessionStatePublisher e;
    private boolean f;
    private boolean g;
    private String h;
    private boolean i;

    public b() {
    }

    private b(e eVar, a aVar) {
        this();
        this.c = new ArrayList();
        this.f = false;
        this.g = false;
        this.b = eVar;
        this.f4552a = aVar;
        this.h = UUID.randomUUID().toString();
        d((View) null);
        this.e = aVar.g() == AdSessionContextType.HTML ? new com.iab.omid.library.startapp.publisher.a(aVar.d()) : new com.iab.omid.library.startapp.publisher.b(aVar.c(), aVar.f());
        this.e.a();
        com.iab.omid.library.startapp.b.a.c().a(this);
        d.a().a(this.e.c(), eVar.c());
    }

    private com.iab.omid.library.startapp.e.a c(View view) {
        for (com.iab.omid.library.startapp.e.a next : this.c) {
            if (next.get() == view) {
                return next;
            }
        }
        return null;
    }

    public final void a() {
        if (!this.f) {
            this.f = true;
            com.iab.omid.library.startapp.b.a.c().b(this);
            this.e.a(com.iab.omid.library.startapp.b.e.d().c());
            AdSessionStatePublisher adSessionStatePublisher = this.e;
            a aVar = this.f4552a;
            String str = this.h;
            JSONObject jSONObject = new JSONObject();
            com.iab.omid.library.startapp.d.b.a(jSONObject, "environment", "app");
            com.iab.omid.library.startapp.d.b.a(jSONObject, "adSessionType", aVar.g());
            JSONObject jSONObject2 = new JSONObject();
            com.iab.omid.library.startapp.d.b.a(jSONObject2, "deviceType", Build.MANUFACTURER + "; " + Build.MODEL);
            com.iab.omid.library.startapp.d.b.a(jSONObject2, "osVersion", Integer.toString(Build.VERSION.SDK_INT));
            com.iab.omid.library.startapp.d.b.a(jSONObject2, "os", "Android");
            com.iab.omid.library.startapp.d.b.a(jSONObject, "deviceInfo", jSONObject2);
            JSONArray jSONArray = new JSONArray();
            jSONArray.put("clid");
            jSONArray.put("vlid");
            com.iab.omid.library.startapp.d.b.a(jSONObject, "supports", jSONArray);
            JSONObject jSONObject3 = new JSONObject();
            com.iab.omid.library.startapp.d.b.a(jSONObject3, "partnerName", aVar.b().a());
            com.iab.omid.library.startapp.d.b.a(jSONObject3, "partnerVersion", aVar.b().b());
            com.iab.omid.library.startapp.d.b.a(jSONObject, "omidNativeInfo", jSONObject3);
            JSONObject jSONObject4 = new JSONObject();
            com.iab.omid.library.startapp.d.b.a(jSONObject4, "libraryVersion", "1.2.0-Startapp");
            com.iab.omid.library.startapp.d.b.a(jSONObject4, "appId", c.b().a().getApplicationContext().getPackageName());
            com.iab.omid.library.startapp.d.b.a(jSONObject, "app", jSONObject4);
            if (aVar.e() != null) {
                com.iab.omid.library.startapp.d.b.a(jSONObject, "customReferenceData", aVar.e());
            }
            JSONObject jSONObject5 = new JSONObject();
            for (com.startapp.networkTest.utils.e next : aVar.c()) {
                com.iab.omid.library.startapp.d.b.a(jSONObject5, next.a(), next.c());
            }
            d.a().a(adSessionStatePublisher.c(), str, jSONObject, jSONObject5);
        }
    }

    public final void b() {
        if (!this.g) {
            this.d.clear();
            if (!this.g) {
                this.c.clear();
            }
            this.g = true;
            d.a().a(this.e.c());
            com.iab.omid.library.startapp.b.a.c().c(this);
            this.e.b();
            this.e = null;
        }
    }

    /* access modifiers changed from: package-private */
    public final void d() {
        if (!this.i) {
            d.a().b(this.e.c());
            this.i = true;
            return;
        }
        throw new IllegalStateException("Impression event can only be sent once");
    }

    public final AdSessionStatePublisher e() {
        return this.e;
    }

    public final String f() {
        return this.h;
    }

    public final View g() {
        return (View) this.d.get();
    }

    public final boolean h() {
        return this.f && !this.g;
    }

    public final boolean i() {
        return this.f;
    }

    public final boolean j() {
        return this.g;
    }

    public final boolean k() {
        return this.b.a();
    }

    public final boolean l() {
        return this.b.b();
    }

    public final List<com.iab.omid.library.startapp.e.a> c() {
        return this.c;
    }

    private void d(View view) {
        this.d = new com.iab.omid.library.startapp.e.a(view);
    }

    public final void b(View view) {
        if (!this.g) {
            if (view == null) {
                throw new IllegalArgumentException("FriendlyObstruction is null");
            } else if (c(view) == null) {
                this.c.add(new com.iab.omid.library.startapp.e.a(view));
            }
        }
    }

    public final void a(View view) {
        if (!this.g) {
            com.iab.omid.library.startapp.b.a((Object) view, "AdView is null");
            if (g() != view) {
                d(view);
                this.e.f();
                Collection<b> a2 = com.iab.omid.library.startapp.b.a.c().a();
                if (a2 != null && a2.size() > 0) {
                    for (b next : a2) {
                        if (next != this && next.g() == view) {
                            next.d.clear();
                        }
                    }
                }
            }
        }
    }

    public static b a(e eVar, a aVar) {
        if (com.iab.omid.library.startapp.a.b()) {
            com.iab.omid.library.startapp.b.a((Object) eVar, "AdSessionConfiguration is null");
            com.iab.omid.library.startapp.b.a((Object) aVar, "AdSessionContext is null");
            return new b(eVar, aVar);
        }
        throw new IllegalStateException("Method called before OMID activation");
    }
}
