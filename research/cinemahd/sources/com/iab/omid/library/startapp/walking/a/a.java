package com.iab.omid.library.startapp.walking.a;

import com.iab.omid.library.startapp.walking.a.b;
import java.util.HashSet;
import org.json.JSONObject;

public abstract class a extends b {
    protected final HashSet<String> c;
    protected final JSONObject d;
    protected final double e;

    public a(b.C0060b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d2) {
        super(bVar);
        this.c = new HashSet<>(hashSet);
        this.d = jSONObject;
        this.e = d2;
    }
}
