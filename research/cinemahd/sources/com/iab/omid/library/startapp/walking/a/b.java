package com.iab.omid.library.startapp.walking.a;

import android.os.AsyncTask;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b extends AsyncTask<Object, Void, String> {

    /* renamed from: a  reason: collision with root package name */
    private a f4572a;
    protected final C0060b b;

    public interface a {
        void a();
    }

    /* renamed from: com.iab.omid.library.startapp.walking.a.b$b  reason: collision with other inner class name */
    public interface C0060b {
        JSONObject a();

        void a(JSONObject jSONObject);
    }

    public b(C0060b bVar) {
        this.b = bVar;
    }

    public final void a(a aVar) {
        this.f4572a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        a aVar = this.f4572a;
        if (aVar != null) {
            aVar.a();
        }
    }

    public final void a(ThreadPoolExecutor threadPoolExecutor) {
        executeOnExecutor(threadPoolExecutor, new Object[0]);
    }
}
