package com.iab.omid.library.startapp.walking.a;

import android.text.TextUtils;
import com.iab.omid.library.startapp.b.a;
import com.iab.omid.library.startapp.walking.a.b;
import java.util.HashSet;
import org.json.JSONObject;

public final class f extends a {
    public f(b.C0060b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        a c;
        if (!TextUtils.isEmpty(str) && (c = a.c()) != null) {
            for (com.iab.omid.library.startapp.adsession.b next : c.a()) {
                if (this.c.contains(next.f())) {
                    next.e().a(str, this.e);
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        if (com.iab.omid.library.startapp.d.b.b(this.d, this.b.a())) {
            return null;
        }
        this.b.a(this.d);
        return this.d.toString();
    }
}
