package com.iab.omid.library.startapp.publisher;

import android.os.Handler;
import android.webkit.WebView;
import com.iab.omid.library.startapp.b.c;
import com.iab.omid.library.startapp.b.d;
import com.startapp.networkTest.utils.e;
import com.vungle.warren.AdLoader;
import java.util.List;

public final class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */
    public WebView f;
    private List<e> g;
    private final String h;

    public b(List<e> list, String str) {
        this.g = list;
        this.h = str;
    }

    public final void a() {
        super.a();
        this.f = new WebView(c.b().a());
        this.f.getSettings().setJavaScriptEnabled(true);
        a(this.f);
        d.a();
        d.c(this.f, this.h);
        for (e b : this.g) {
            String externalForm = b.b().toExternalForm();
            d.a();
            WebView webView = this.f;
            if (externalForm != null) {
                d.c(webView, "var script=document.createElement('script');script.setAttribute(\"type\",\"text/javascript\");script.setAttribute(\"src\",\"%SCRIPT_SRC%\");document.body.appendChild(script);".replace("%SCRIPT_SRC%", externalForm));
            }
        }
    }

    public final void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {

            /* renamed from: a  reason: collision with root package name */
            private WebView f4569a = b.this.f;

            public final void run() {
                this.f4569a.destroy();
            }
        }, AdLoader.RETRY_DELAY);
        this.f = null;
    }
}
