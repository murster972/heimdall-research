package com.iab.omid.library.startapp.b;

import android.annotation.SuppressLint;
import android.content.Context;

public final class c {
    @SuppressLint({"StaticFieldLeak"})
    private static c b = new c();

    /* renamed from: a  reason: collision with root package name */
    private Context f4557a;

    private c() {
    }

    public static c b() {
        return b;
    }

    public final Context a() {
        return this.f4557a;
    }

    public final void a(Context context) {
        this.f4557a = context != null ? context.getApplicationContext() : null;
    }
}
