package com.iab.omid.library.startapp.publisher;

import android.webkit.WebView;
import com.iab.omid.library.startapp.b;
import com.iab.omid.library.startapp.b.d;
import com.startapp.sdk.ads.banner.banner3d.Banner3DSize;
import org.json.JSONObject;

public abstract class AdSessionStatePublisher {

    /* renamed from: a  reason: collision with root package name */
    private com.iab.omid.library.startapp.e.a f4567a = new com.iab.omid.library.startapp.e.a((WebView) null);
    private com.iab.omid.library.startapp.adsession.a b;
    private Banner3DSize c;
    private a d;
    private double e;

    enum a {
        AD_STATE_IDLE,
        AD_STATE_VISIBLE,
        AD_STATE_HIDDEN
    }

    public AdSessionStatePublisher() {
        f();
    }

    public void a() {
    }

    public final void a(float f) {
        d.a().a(c(), f);
    }

    /* access modifiers changed from: package-private */
    public final void a(WebView webView) {
        this.f4567a = new com.iab.omid.library.startapp.e.a(webView);
    }

    public final void a(com.iab.omid.library.startapp.adsession.a aVar) {
        this.b = aVar;
    }

    public final void a(Banner3DSize banner3DSize) {
        this.c = banner3DSize;
    }

    public final void a(String str) {
        d.a().a(c(), str, (JSONObject) null);
    }

    public final void a(String str, double d2) {
        if (d2 > this.e) {
            this.d = a.AD_STATE_VISIBLE;
            d.a().a(c(), str);
        }
    }

    public final void a(String str, JSONObject jSONObject) {
        d.a().a(c(), str, jSONObject);
    }

    public final void a(boolean z) {
        if (this.f4567a.get() != null) {
            d.a().b(c(), z ? "foregrounded" : "backgrounded");
        }
    }

    public void b() {
        this.f4567a.clear();
    }

    public final void b(String str, double d2) {
        a aVar;
        if (d2 > this.e && this.d != (aVar = a.AD_STATE_HIDDEN)) {
            this.d = aVar;
            d.a().a(c(), str);
        }
    }

    public final WebView c() {
        return (WebView) this.f4567a.get();
    }

    public final com.iab.omid.library.startapp.adsession.a d() {
        return this.b;
    }

    public final Banner3DSize e() {
        return this.c;
    }

    public final void f() {
        this.e = b.b();
        this.d = a.AD_STATE_IDLE;
    }
}
