package com.iab.omid.library.startapp.d;

import android.content.Context;
import java.io.File;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static com.startapp.a.b.a f4564a;

    public static float a(int i, int i2) {
        if (i2 <= 0 || i <= 0) {
            return 0.0f;
        }
        float f = ((float) i) / ((float) i2);
        if (f > 1.0f) {
            return 1.0f;
        }
        return f;
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0060  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r11) {
        /*
            com.startapp.a.b.a r0 = f4564a
            if (r0 != 0) goto L_0x000f
            com.startapp.a.b.a r0 = new com.startapp.a.b.a
            android.content.Context r1 = r11.getApplicationContext()
            r0.<init>(r1)
            f4564a = r0
        L_0x000f:
            com.startapp.a.b.a r0 = f4564a
            boolean r1 = r0.a()
            java.lang.String r2 = "test-keys"
            r3 = 0
            r4 = 1
            if (r1 != 0) goto L_0x005d
            boolean r0 = r0.b()
            if (r0 != 0) goto L_0x005d
            java.lang.String r0 = "su"
            boolean r0 = com.startapp.a.b.a.a((java.lang.String) r0)
            if (r0 != 0) goto L_0x005d
            java.lang.String r0 = "busybox"
            boolean r0 = com.startapp.a.b.a.a((java.lang.String) r0)
            if (r0 != 0) goto L_0x005d
            boolean r0 = com.startapp.a.b.a.c()
            if (r0 != 0) goto L_0x005d
            boolean r0 = com.startapp.a.b.a.d()
            if (r0 != 0) goto L_0x005d
            java.lang.String r0 = android.os.Build.TAGS
            if (r0 == 0) goto L_0x0049
            boolean r0 = r0.contains(r2)
            if (r0 == 0) goto L_0x0049
            r0 = 1
            goto L_0x004a
        L_0x0049:
            r0 = 0
        L_0x004a:
            if (r0 != 0) goto L_0x005d
            boolean r0 = com.startapp.a.b.a.e()
            if (r0 != 0) goto L_0x005d
            java.lang.String r0 = "magisk"
            boolean r0 = com.startapp.a.b.a.a((java.lang.String) r0)
            if (r0 == 0) goto L_0x005b
            goto L_0x005d
        L_0x005b:
            r0 = 0
            goto L_0x005e
        L_0x005d:
            r0 = 1
        L_0x005e:
            if (r0 != 0) goto L_0x00ad
            java.lang.String r0 = android.os.Build.TAGS
            if (r0 == 0) goto L_0x006c
            boolean r0 = r0.contains(r2)
            if (r0 == 0) goto L_0x006c
            r0 = 1
            goto L_0x006d
        L_0x006c:
            r0 = 0
        L_0x006d:
            if (r0 != 0) goto L_0x00ad
            boolean r0 = a()
            if (r0 != 0) goto L_0x00ad
            boolean r0 = a()
            if (r0 != 0) goto L_0x00ad
            boolean r0 = b()
            if (r0 != 0) goto L_0x00ad
            boolean r0 = c()
            if (r0 != 0) goto L_0x00ad
            java.lang.String r5 = "com.noshufou.android.su"
            java.lang.String r6 = "com.thirdparty.superuser"
            java.lang.String r7 = "eu.chainfire.supersu"
            java.lang.String r8 = "com.koushikdutta.superuser"
            java.lang.String r9 = "com.zachspong.temprootremovejb"
            java.lang.String r10 = "com.ramdroid.appquarantine"
            java.lang.String[] r0 = new java.lang.String[]{r5, r6, r7, r8, r9, r10}
            r1 = 0
        L_0x0098:
            r2 = 6
            if (r1 >= r2) goto L_0x00a8
            r2 = r0[r1]
            boolean r2 = a((android.content.Context) r11, (java.lang.String) r2)
            if (r2 == 0) goto L_0x00a5
            r11 = 1
            goto L_0x00a9
        L_0x00a5:
            int r1 = r1 + 1
            goto L_0x0098
        L_0x00a8:
            r11 = 0
        L_0x00a9:
            if (r11 == 0) goto L_0x00ac
            goto L_0x00ad
        L_0x00ac:
            return r3
        L_0x00ad:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iab.omid.library.startapp.d.a.a(android.content.Context):boolean");
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x0031  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean b() {
        /*
            r0 = 0
            java.lang.Runtime r1 = java.lang.Runtime.getRuntime()     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "/system/xbin/which"
            java.lang.String r3 = "su"
            java.lang.String[] r2 = new java.lang.String[]{r2, r3}     // Catch:{ all -> 0x002e }
            java.lang.Process r1 = r1.exec(r2)     // Catch:{ all -> 0x002e }
            java.io.BufferedReader r2 = new java.io.BufferedReader     // Catch:{ all -> 0x002c }
            java.io.InputStreamReader r3 = new java.io.InputStreamReader     // Catch:{ all -> 0x002c }
            java.io.InputStream r4 = r1.getInputStream()     // Catch:{ all -> 0x002c }
            r3.<init>(r4)     // Catch:{ all -> 0x002c }
            r2.<init>(r3)     // Catch:{ all -> 0x002c }
            java.lang.String r2 = r2.readLine()     // Catch:{ all -> 0x002c }
            if (r2 == 0) goto L_0x0026
            r0 = 1
        L_0x0026:
            if (r1 == 0) goto L_0x002b
            r1.destroy()
        L_0x002b:
            return r0
        L_0x002c:
            goto L_0x002f
        L_0x002e:
            r1 = 0
        L_0x002f:
            if (r1 == 0) goto L_0x0034
            r1.destroy()
        L_0x0034:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.iab.omid.library.startapp.d.a.b():boolean");
    }

    private static boolean c() {
        try {
            return new File("/system/app/Superuser.apk").exists();
        } catch (Throwable unused) {
            return false;
        }
    }

    private static boolean a() {
        String[] strArr = {"/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su", "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
        for (int i = 0; i < 10; i++) {
            if (new File(strArr[i]).exists()) {
                return true;
            }
        }
        return false;
    }

    private static boolean a(Context context, String str) {
        try {
            context.getPackageManager().getPackageInfo(str, 1);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }
}
