package com.iab.omid.library.startapp.b;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public final class b {
    @SuppressLint({"StaticFieldLeak"})
    private static b f = new b();

    /* renamed from: a  reason: collision with root package name */
    private Context f4555a;
    private BroadcastReceiver b;
    private boolean c;
    private boolean d;
    private a e;

    public interface a {
        void a(boolean z);
    }

    private b() {
    }

    public static b d() {
        return f;
    }

    private void e() {
        boolean z = !this.d;
        for (com.iab.omid.library.startapp.adsession.b e2 : a.c().a()) {
            e2.e().a(z);
        }
    }

    public final void a() {
        this.b = new BroadcastReceiver() {
            public final void onReceive(Context context, Intent intent) {
                KeyguardManager keyguardManager;
                if (intent != null) {
                    if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                        b.a(b.this, true);
                    } else if ("android.intent.action.USER_PRESENT".equals(intent.getAction())) {
                        b.a(b.this, false);
                    } else if ("android.intent.action.SCREEN_ON".equals(intent.getAction()) && (keyguardManager = (KeyguardManager) context.getSystemService("keyguard")) != null && !keyguardManager.inKeyguardRestrictedInputMode()) {
                        b.a(b.this, false);
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.f4555a.registerReceiver(this.b, intentFilter);
        this.c = true;
        e();
    }

    public final void a(Context context) {
        this.f4555a = context.getApplicationContext();
    }

    public final void a(a aVar) {
        this.e = aVar;
    }

    public final void b() {
        BroadcastReceiver broadcastReceiver;
        Context context = this.f4555a;
        if (!(context == null || (broadcastReceiver = this.b) == null)) {
            context.unregisterReceiver(broadcastReceiver);
            this.b = null;
        }
        this.c = false;
        this.d = false;
        this.e = null;
    }

    public final boolean c() {
        return !this.d;
    }

    static /* synthetic */ void a(b bVar, boolean z) {
        if (bVar.d != z) {
            bVar.d = z;
            if (bVar.c) {
                bVar.e();
                a aVar = bVar.e;
                if (aVar != null) {
                    aVar.a(bVar.c());
                }
            }
        }
    }
}
