package com.iab.omid.library.startapp.walking;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.iab.omid.library.startapp.c.a;
import com.iab.omid.library.startapp.walking.a.c;
import com.startapp.sdk.ads.banner.bannerstandard.b;
import com.startapp.sdk.ads.video.VideoUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.json.JSONObject;

public final class a implements a.C0059a {
    private static a g = new a();
    private static Handler h = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static Handler i = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public final void run() {
            a.b(a.b());
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public final void run() {
            if (a.i != null) {
                a.i.post(a.j);
                a.i.postDelayed(a.k, 200);
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private List<Object> f4570a = new ArrayList();
    private int b;
    private b c = new b();
    private VideoUtil d = new VideoUtil();
    /* access modifiers changed from: private */
    public b e = new b(new c());
    private double f;

    a() {
    }

    private void a(View view, com.iab.omid.library.startapp.c.a aVar, JSONObject jSONObject, c cVar) {
        aVar.a(view, jSONObject, this, cVar == c.PARENT_VIEW);
    }

    public static a b() {
        return g;
    }

    static /* synthetic */ void b(a aVar) {
        aVar.b = 0;
        aVar.f = com.iab.omid.library.startapp.b.b();
        aVar.d.c();
        double b2 = com.iab.omid.library.startapp.b.b();
        com.iab.omid.library.startapp.c.a a2 = aVar.c.a();
        if (aVar.d.b().size() > 0) {
            aVar.e.b(a2.a((View) null), aVar.d.b(), b2);
        }
        if (aVar.d.a().size() > 0) {
            JSONObject a3 = a2.a((View) null);
            aVar.a((View) null, a2, a3, c.PARENT_VIEW);
            com.iab.omid.library.startapp.d.b.a(a3);
            aVar.e.a(a3, aVar.d.a(), b2);
        } else {
            aVar.e.b();
        }
        aVar.d.d();
        com.iab.omid.library.startapp.b.b();
        if (aVar.f4570a.size() > 0) {
            Iterator<Object> it2 = aVar.f4570a.iterator();
            while (it2.hasNext()) {
                it2.next();
            }
        }
    }

    public static void c() {
        if (i == null) {
            Handler handler = new Handler(Looper.getMainLooper());
            i = handler;
            handler.post(j);
            i.postDelayed(k, 200);
        }
    }

    public static void d() {
        h();
    }

    private static void h() {
        Handler handler = i;
        if (handler != null) {
            handler.removeCallbacks(k);
            i = null;
        }
    }

    public final void a() {
        h();
        this.f4570a.clear();
        h.post(new Runnable() {
            public final void run() {
                a.this.e.b();
            }
        });
    }

    public final void a(View view, com.iab.omid.library.startapp.c.a aVar, JSONObject jSONObject) {
        c c2;
        boolean z;
        if (com.iab.omid.library.startapp.d.c.c(view) && (c2 = this.d.c(view)) != c.UNDERLYING_VIEW) {
            JSONObject a2 = aVar.a(view);
            com.iab.omid.library.startapp.d.b.a(jSONObject, a2);
            String a3 = this.d.a(view);
            if (a3 != null) {
                com.iab.omid.library.startapp.d.b.a(a2, a3);
                this.d.e();
                z = true;
            } else {
                z = false;
            }
            if (!z) {
                ArrayList<String> b2 = this.d.b(view);
                if (b2 != null) {
                    com.iab.omid.library.startapp.d.b.a(a2, (List<String>) b2);
                }
                a(view, aVar, a2, c2);
            }
            this.b++;
        }
    }
}
