package com.iab.omid.library.startapp.b;

import com.iab.omid.library.startapp.adsession.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public final class a {
    private static a c = new a();

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<b> f4554a = new ArrayList<>();
    private final ArrayList<b> b = new ArrayList<>();

    private a() {
    }

    public static a c() {
        return c;
    }

    private boolean d() {
        return this.b.size() > 0;
    }

    public final Collection<b> a() {
        return Collections.unmodifiableCollection(this.f4554a);
    }

    public final void a(b bVar) {
        this.f4554a.add(bVar);
    }

    public final Collection<b> b() {
        return Collections.unmodifiableCollection(this.b);
    }

    public final void b(b bVar) {
        boolean d = d();
        this.b.add(bVar);
        if (!d) {
            e.d().a();
        }
    }

    public final void c(b bVar) {
        boolean d = d();
        this.f4554a.remove(bVar);
        this.b.remove(bVar);
        if (d && !d()) {
            e.d().b();
        }
    }
}
