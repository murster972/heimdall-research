package com.iab.omid.library.startapp.adsession;

import com.facebook.react.uimanager.ViewProps;

public enum Owner {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE(ViewProps.NONE);
    
    private final String owner;

    private Owner(String str) {
        this.owner = str;
    }

    public final String toString() {
        return this.owner;
    }
}
