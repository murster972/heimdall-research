package com.iab.omid.library.startapp.c;

import android.view.View;
import com.iab.omid.library.startapp.c.a;
import com.iab.omid.library.startapp.d.c;
import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.json.JSONObject;

public final class b implements a {

    /* renamed from: a  reason: collision with root package name */
    private final a f4562a;

    public b(a aVar) {
        this.f4562a = aVar;
    }

    public final JSONObject a(View view) {
        return com.iab.omid.library.startapp.d.b.a(0, 0, 0, 0);
    }

    public final void a(View view, JSONObject jSONObject, a.C0059a aVar, boolean z) {
        View rootView;
        ArrayList arrayList = new ArrayList();
        com.iab.omid.library.startapp.b.a c = com.iab.omid.library.startapp.b.a.c();
        if (c != null) {
            Collection<com.iab.omid.library.startapp.adsession.b> b = c.b();
            IdentityHashMap identityHashMap = new IdentityHashMap((b.size() << 1) + 3);
            for (com.iab.omid.library.startapp.adsession.b g : b) {
                View g2 = g.g();
                if (g2 != null && c.b(g2) && (rootView = g2.getRootView()) != null && !identityHashMap.containsKey(rootView)) {
                    identityHashMap.put(rootView, rootView);
                    float a2 = c.a(rootView);
                    int size = arrayList.size();
                    while (size > 0 && c.a((View) arrayList.get(size - 1)) > a2) {
                        size--;
                    }
                    arrayList.add(size, rootView);
                }
            }
        }
        Iterator it2 = arrayList.iterator();
        while (it2.hasNext()) {
            aVar.a((View) it2.next(), this.f4562a, jSONObject);
        }
    }
}
