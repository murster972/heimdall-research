package com.iab.omid.library.startapp;

import android.content.Context;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private static b f4547a = new b();

    public static String a() {
        return "1.2.0-Startapp";
    }

    public static boolean a(String str, Context context) {
        return f4547a.a(str, context.getApplicationContext());
    }

    public static boolean b() {
        return f4547a.a();
    }
}
