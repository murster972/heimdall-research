package com.iab.omid.library.startapp.adsession;

import com.iab.omid.library.startapp.b;

public final class a {

    /* renamed from: a  reason: collision with root package name */
    private final b f4551a;

    private a(b bVar) {
        this.f4551a = bVar;
    }

    public static a a(b bVar) {
        b.a((Object) bVar, "AdSession is null");
        if (bVar.e().d() == null) {
            b.a(bVar);
            a aVar = new a(bVar);
            bVar.e().a(aVar);
            return aVar;
        }
        throw new IllegalStateException("AdEvents already exists for AdSession");
    }

    public final void a() {
        b.a(this.f4551a);
        if (this.f4551a.k()) {
            if (!this.f4551a.h()) {
                try {
                    this.f4551a.a();
                } catch (Exception unused) {
                }
            }
            if (this.f4551a.h()) {
                this.f4551a.d();
                return;
            }
            return;
        }
        throw new IllegalStateException("Impression event is not expected from the Native AdSession");
    }
}
