package com.iab.omid.library.startapp.walking.a;

import com.iab.omid.library.startapp.b.a;
import com.iab.omid.library.startapp.walking.a.b;
import java.util.HashSet;
import org.json.JSONObject;

public final class e extends a {
    public e(b.C0060b bVar, HashSet<String> hashSet, JSONObject jSONObject, double d) {
        super(bVar, hashSet, jSONObject, d);
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public final void onPostExecute(String str) {
        a c = a.c();
        if (c != null) {
            for (com.iab.omid.library.startapp.adsession.b next : c.a()) {
                if (this.c.contains(next.f())) {
                    next.e().b(str, this.e);
                }
            }
        }
        super.onPostExecute(str);
    }

    /* access modifiers changed from: protected */
    public final /* synthetic */ Object doInBackground(Object[] objArr) {
        return this.d.toString();
    }
}
