package com.iab.omid.library.startapp.a;

import android.content.Context;
import android.database.ContentObserver;
import android.media.AudioManager;
import android.os.Handler;
import android.provider.Settings;
import com.iab.omid.library.startapp.d.a;

public final class b extends ContentObserver {

    /* renamed from: a  reason: collision with root package name */
    private final Context f4548a;
    private final AudioManager b;
    private final a c;
    private float d;

    public b(Handler handler, Context context, a aVar, a aVar2) {
        super(handler);
        this.f4548a = context;
        this.b = (AudioManager) context.getSystemService("audio");
        this.c = aVar2;
    }

    private float c() {
        return a.a(this.b.getStreamVolume(3), this.b.getStreamMaxVolume(3));
    }

    private void d() {
        this.c.a(this.d);
    }

    public final void a() {
        this.d = c();
        d();
        this.f4548a.getContentResolver().registerContentObserver(Settings.System.CONTENT_URI, true, this);
    }

    public final void b() {
        this.f4548a.getContentResolver().unregisterContentObserver(this);
    }

    public final void onChange(boolean z) {
        super.onChange(z);
        float c2 = c();
        if (c2 != this.d) {
            this.d = c2;
            d();
        }
    }
}
