package com.iab.omid.library.startapp;

import android.content.Context;
import android.net.LinkProperties;
import android.net.NetworkCapabilities;
import android.text.TextUtils;
import android.util.Log;
import android.webkit.WebView;
import com.facebook.common.statfs.StatFsHelper;
import com.iab.omid.library.startapp.b.c;
import com.iab.omid.library.startapp.b.e;
import com.startapp.b.a.b.a;
import com.startapp.common.parser.d;
import com.startapp.networkTest.data.TimeInfo;
import com.startapp.networkTest.data.radio.NetworkRegistrationInfo;
import com.startapp.networkTest.enums.ThreeStateShort;
import com.startapp.sdk.adsbase.AdsCommonMetaData;
import com.startapp.sdk.adsbase.SimpleTokenUtils;
import com.startapp.sdk.adsbase.apppresence.AppPresenceDetails;
import com.startapp.sdk.adsbase.j.t;
import com.startapp.sdk.adsbase.j.u;
import com.startapp.sdk.adsbase.model.AdDetails;
import com.startapp.sdk.adsbase.mraid.bridge.MraidState;
import java.io.UnsupportedEncodingException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public final class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4553a;

    public static String a(String str, String str2) {
        return c.a(str, str2);
    }

    private static int b(String str) {
        c(str);
        return Integer.parseInt(str.split("\\.", 2)[0]);
    }

    private static void c(String str) {
        a((Object) str, "Version cannot be null");
        if (!str.matches("[0-9]+\\.[0-9]+\\.[0-9]+.*")) {
            throw new IllegalArgumentException("Invalid version format : ".concat(String.valueOf(str)));
        }
    }

    public static int d(List<Integer> list) {
        if (list.size() == 0) {
            return 0;
        }
        if (list.size() == 1) {
            return list.get(0).intValue();
        }
        long j = 0;
        for (int i = 0; i < list.size(); i++) {
            j += (long) list.get(i).intValue();
        }
        return Math.round((float) (j / ((long) list.size())));
    }

    public static int e(List<Integer> list) {
        if (list.size() == 0) {
            return 0;
        }
        if (list.size() == 1) {
            return list.get(0).intValue();
        }
        int i = Integer.MAX_VALUE;
        for (int i2 = 0; i2 < list.size(); i2++) {
            if (list.get(i2).intValue() < i) {
                i = list.get(i2).intValue();
            }
        }
        return i;
    }

    public static int f(List<Integer> list) {
        if (list.size() == 0) {
            return 0;
        }
        if (list.size() == 1) {
            return list.get(0).intValue();
        }
        int i = 0;
        for (int i2 = 0; i2 < list.size(); i2++) {
            if (list.get(i2).intValue() > i) {
                i = list.get(i2).intValue();
            }
        }
        return i;
    }

    /* access modifiers changed from: package-private */
    public final boolean a() {
        return this.f4553a;
    }

    public static double b() {
        return (double) TimeUnit.NANOSECONDS.toMillis(System.nanoTime());
    }

    public static void c(String str, String str2) {
        if (str.length() > 256) {
            throw new IllegalArgumentException(str2);
        }
    }

    /* access modifiers changed from: package-private */
    public final boolean a(String str, Context context) {
        c(str);
        a((Object) context, "Application Context cannot be null");
        if (!(b("1.2.0-Startapp") == b(str))) {
            return false;
        }
        if (!this.f4553a) {
            this.f4553a = true;
            e.d().a(context);
            com.iab.omid.library.startapp.b.b.d().a(context);
            com.iab.omid.library.startapp.d.b.a(context);
            c.b().a(context);
        }
        return true;
    }

    public static void b(String str, String str2) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException(str2);
        }
    }

    public static int c(List<Integer> list) {
        if (list.size() == 0) {
            return 0;
        }
        if (list.size() == 1) {
            return list.get(0).intValue();
        }
        ArrayList arrayList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            arrayList.add(list.get(i));
        }
        Collections.sort(arrayList);
        if (arrayList.size() % 2 == 0) {
            return (int) Math.round((((double) ((Integer) arrayList.get(arrayList.size() / 2)).intValue()) + ((double) ((Integer) arrayList.get((arrayList.size() / 2) - 1)).intValue())) / 2.0d);
        }
        return ((Integer) arrayList.get(arrayList.size() / 2)).intValue();
    }

    public static void b(com.iab.omid.library.startapp.adsession.b bVar) {
        if (bVar.i()) {
            a(bVar);
            return;
        }
        throw new IllegalStateException("AdSession is not started");
    }

    public static void a(String str, Exception exc) {
        Log.e("OMIDLIB", str, exc);
    }

    public static double b(List<Integer> list) {
        long j = 0;
        for (int i = 0; i < list.size(); i++) {
            j += (long) list.get(i).intValue();
        }
        double size = ((double) j) / ((double) list.size());
        double d = 0.0d;
        for (int i2 = 0; i2 < list.size(); i2++) {
            d += Math.pow(((double) list.get(i2).intValue()) - size, 2.0d);
        }
        double sqrt = Math.sqrt(d / ((double) list.size()));
        if (Double.isNaN(sqrt)) {
            return 0.0d;
        }
        return sqrt;
    }

    private static String d(String str, String str2) throws Exception {
        String substring = str.substring(str.indexOf(str2));
        String replaceAll = substring.replaceAll(str2 + " ", "");
        return replaceAll.substring(0, replaceAll.contains(" ") ? replaceAll.indexOf(" ") : replaceAll.length() - 1);
    }

    private static String e(String str) {
        try {
            int parseInt = Integer.parseInt(str);
            if (parseInt != 1) {
                return parseInt != 2 ? Integer.toString(parseInt) : "WLAN";
            }
            return "WWAN";
        } catch (Exception unused) {
            return str;
        }
    }

    public static void a(Object obj, String str) {
        if (obj == null) {
            throw new IllegalArgumentException(str);
        }
    }

    public static void a(com.iab.omid.library.startapp.adsession.b bVar) {
        if (bVar.j()) {
            throw new IllegalStateException("AdSession is finished");
        }
    }

    private static String d(String str) {
        String[] split = str.split("=");
        return split.length > 1 ? split[1] : "";
    }

    public static String a(List<String> list) {
        return new a().a(list);
    }

    public static String a(Field field) {
        Annotation[] declaredAnnotations = field.getDeclaredAnnotations();
        if (declaredAnnotations != null && declaredAnnotations.length > 0) {
            Annotation annotation = field.getDeclaredAnnotations()[0];
            if (annotation.annotationType().equals(d.class)) {
                d dVar = (d) annotation;
                if (!"".equals(dVar.f())) {
                    return dVar.f();
                }
            }
        }
        return field.getName();
    }

    public static com.startapp.networkTest.utils.a.a c(long j) {
        return a(j, TimeZone.getDefault().getOffset(j));
    }

    public static int b(NetworkCapabilities networkCapabilities) {
        if (networkCapabilities != null) {
            try {
                return Integer.parseInt(d(networkCapabilities.toString(), "Specifier:").replaceAll("<", "").replaceAll(">", ""));
            } catch (Exception unused) {
            }
        }
        return -1;
    }

    public static String b(long j) {
        return a(j, false);
    }

    public static String b(Object obj) {
        obj.getClass();
        return com.startapp.common.parser.b.a(obj);
    }

    public static boolean a(Object obj) {
        Class<?> cls = obj.getClass();
        return cls.equals(Boolean.class) || cls.equals(Integer.class) || cls.equals(Character.class) || cls.equals(Byte.class) || cls.equals(Short.class) || cls.equals(Double.class) || cls.equals(Long.class) || cls.equals(Float.class) || cls.equals(String.class);
    }

    public static void b(Context context, int i, int i2, WebView webView) {
        u.a(webView, "mraid.setMaxSize", Integer.valueOf(t.b(context, i)), Integer.valueOf(t.b(context, i2)));
    }

    public static void b(Context context, int i, int i2, int i3, int i4, WebView webView) {
        u.a(webView, "mraid.setDefaultPosition", Integer.valueOf(t.b(context, i)), Integer.valueOf(t.b(context, i2)), Integer.valueOf(t.b(context, i3)), Integer.valueOf(t.b(context, i4)));
    }

    public static String a(NetworkCapabilities networkCapabilities) {
        if (networkCapabilities != null) {
            try {
                return d(networkCapabilities.toString(), "Capabilities:").replaceAll("&", ",").toLowerCase();
            } catch (Exception unused) {
            }
        }
        return "";
    }

    public static String a(LinkProperties linkProperties) {
        if (linkProperties == null) {
            return "";
        }
        try {
            String d = d(linkProperties.toString().replaceAll("\\[ ", "\\[").replaceAll(" \\]", "\\]"), "PcscfAddresses:");
            if (d.isEmpty()) {
                return "";
            }
            String replace = d.replace("[", "").replace("]", "");
            if (replace.lastIndexOf(",") == replace.length() - 1) {
                replace = replace.substring(0, replace.length() - 1);
            }
            return replace;
        } catch (Exception unused) {
            return "";
        }
    }

    public static String a(long j) {
        return a(j, true);
    }

    private static String a(long j, boolean z) {
        com.startapp.networkTest.utils.a.a c = c(j);
        return a(c.f6088a, c.b, c.c, c.d, c.e, c.f, c.g, z, c.h);
    }

    private static com.startapp.networkTest.utils.a.a a(long j, int i) {
        int i2;
        long j2 = ((long) i) + j;
        long j3 = j2 / 1000;
        int i3 = (int) (j2 % 1000);
        long j4 = j3 / 60;
        int i4 = (int) (j3 % 60);
        long j5 = j4 / 60;
        int i5 = (int) (j4 % 60);
        int i6 = (int) (j5 / 24);
        int i7 = (int) (j5 % 24);
        int i8 = 365;
        int i9 = 1;
        int i10 = 0;
        int i11 = 1970;
        boolean z = false;
        while (true) {
            i2 = i6 + 1;
            if (i8 >= i2) {
                break;
            }
            i11++;
            int i12 = i8 + 365;
            if ((i11 % 4 != 0 || i11 % 100 == 0) && i11 % StatFsHelper.DEFAULT_DISK_YELLOW_LEVEL_IN_MB != 0) {
                z = false;
            } else {
                i12++;
                z = true;
            }
            int i13 = i12;
            i10 = i8;
            i8 = i13;
        }
        int i14 = i2 - i10;
        int i15 = 0;
        int i16 = 31;
        while (i16 < i14) {
            i9++;
            int i17 = i16;
            i16 = (!z || i9 != 2) ? i9 == 2 ? i16 + 28 : (i9 == 4 || i9 == 6 || i9 == 9 || i9 == 11) ? i16 + 30 : i16 + 31 : i16 + 29;
            i15 = i17;
        }
        return new com.startapp.networkTest.utils.a.a(i11, i9, i14 - i15, i7, i5, i4, i3, i);
    }

    private static String a(int i, int i2, int i3, int i4, int i5, int i6, int i7, boolean z, int i8) {
        int i9 = i7;
        int i10 = i8;
        String valueOf = String.valueOf(i3);
        String valueOf2 = String.valueOf(i2);
        String valueOf3 = String.valueOf(i4);
        String valueOf4 = String.valueOf(i5);
        String valueOf5 = String.valueOf(i6);
        String valueOf6 = String.valueOf(i7);
        if (i3 < 10) {
            valueOf = "0".concat(String.valueOf(i3));
        }
        String str = valueOf;
        if (i2 < 10) {
            valueOf2 = "0".concat(String.valueOf(i2));
        }
        if (i4 < 10) {
            valueOf3 = "0".concat(String.valueOf(i4));
        }
        if (i5 < 10) {
            valueOf4 = "0".concat(String.valueOf(i5));
        }
        if (i6 < 10) {
            valueOf5 = "0".concat(String.valueOf(i6));
        }
        if (i9 < 10) {
            valueOf6 = "00".concat(String.valueOf(i7));
        } else if (i9 < 100) {
            valueOf6 = "0".concat(String.valueOf(i7));
        }
        StringBuilder sb = new StringBuilder();
        int i11 = i;
        sb.append(i);
        String str2 = "-";
        sb.append(str2);
        sb.append(valueOf2);
        sb.append(str2);
        sb.append(str);
        sb.append(" ");
        sb.append(valueOf3);
        sb.append(":");
        sb.append(valueOf4);
        sb.append(":");
        sb.append(valueOf5);
        sb.append(".");
        sb.append(valueOf6);
        String sb2 = sb.toString();
        if (!z) {
            return sb2;
        }
        int i12 = (i10 / 1000) / 60;
        if (i10 < 0) {
            i12 = -i12;
        } else {
            str2 = "+";
        }
        int i13 = i12 / 60;
        int i14 = i12 % 60;
        String valueOf7 = String.valueOf(i13);
        String valueOf8 = String.valueOf(i14);
        if (i13 < 10) {
            valueOf7 = "0".concat(String.valueOf(i13));
        }
        if (i14 < 10) {
            valueOf8 = "0".concat(String.valueOf(i14));
        }
        return sb2 + " " + str2 + valueOf7 + valueOf8;
    }

    public static String a(TimeInfo timeInfo, String str) {
        byte[] bArr;
        if (timeInfo == null || str == null || str.length() == 0) {
            return null;
        }
        try {
            bArr = com.startapp.networkTest.a.a.a.a((str + timeInfo.TimestampMillis).getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            bArr = null;
        }
        if (bArr == null) {
            return null;
        }
        return com.startapp.networkTest.utils.c.a(bArr);
    }

    public static NetworkRegistrationInfo[] a(String str) {
        String str2 = "mNetworkRegistrationStates=";
        try {
            int indexOf = str.indexOf(str2);
            if (indexOf == -1) {
                str2 = "mNetworkRegistrationInfos=";
                indexOf = str.indexOf(str2);
            }
            if (indexOf == -1) {
                return new NetworkRegistrationInfo[0];
            }
            String replaceAll = str.substring(indexOf).substring(str2.length() + 1).replaceAll("\\[\\w@", "@");
            int indexOf2 = replaceAll.indexOf("]");
            int indexOf3 = replaceAll.indexOf("[");
            while (indexOf3 != -1 && indexOf2 > indexOf3) {
                replaceAll = replaceAll.replaceFirst("\\[", "").replaceFirst("]", "");
                indexOf3 = replaceAll.indexOf("[");
                indexOf2 = replaceAll.indexOf("]");
            }
            String[] split = replaceAll.substring(0, indexOf2).split(", ");
            NetworkRegistrationInfo[] networkRegistrationInfoArr = new NetworkRegistrationInfo[split.length];
            for (int i = 0; i < split.length; i++) {
                split[i] = split[i].replace("isDcNrRestricted = false", "isDcNrRestricted=false").replace("isDcNrRestricted = true", "isDcNrRestricted=true").replace("isNrAvailable = false", "isNrAvailable=false").replace("isNrAvailable = true", "isNrAvailable=true").replace("isEnDcAvailable = false", "isEnDcAvailable=false").replace("isEnDcAvailable = true", "isEnDcAvailable=true").replace("mIsUsingCarrierAggregation = false", "mIsUsingCarrierAggregation=false").replace("mIsUsingCarrierAggregation = true", "mIsUsingCarrierAggregation=true");
                split[i] = split[i].trim();
                networkRegistrationInfoArr[i] = a(split[i].replace("NetworkRegistrationState", " ").replace("NetworkRegistrationInfo", " ").replace("}", " ").replace("{", " ").replace(":", "").replaceAll(" +", " ").trim().split(" "));
            }
            return networkRegistrationInfoArr;
        } catch (Exception e) {
            e.printStackTrace();
            return new NetworkRegistrationInfo[0];
        }
    }

    private static NetworkRegistrationInfo a(String[] strArr) {
        NetworkRegistrationInfo networkRegistrationInfo = new NetworkRegistrationInfo();
        for (String str : strArr) {
            if (str.startsWith("transportType")) {
                networkRegistrationInfo.TransportType = e(d(str));
            } else if (str.startsWith("domain")) {
                networkRegistrationInfo.Domain = d(str);
            } else if (str.startsWith("regState")) {
                networkRegistrationInfo.RegState = d(str);
            } else if (str.startsWith("accessNetworkTechnology")) {
                networkRegistrationInfo.NetworkTechnology = d(str);
            } else if (str.startsWith("reasonForDenial")) {
                networkRegistrationInfo.ReasonForDenial = d(str);
            } else if (str.startsWith("emergencyEnabled")) {
                networkRegistrationInfo.EmergencyEnabled = d(str).equals("true");
            } else if (str.startsWith("mIsUsingCarrierAggregation")) {
                networkRegistrationInfo.CarrierAggregation = d(str).equals("true") ? ThreeStateShort.Yes : ThreeStateShort.No;
            } else {
                String str2 = "";
                if (str.startsWith("cellIdentity")) {
                    networkRegistrationInfo.CellTechnology = d(str);
                    networkRegistrationInfo.CellTechnology = networkRegistrationInfo.CellTechnology.replace("CellIdentity", str2);
                } else if (str.startsWith("mCid") || str.startsWith("mCi") || str.startsWith("mNetworkId") || str.startsWith("mNci")) {
                    networkRegistrationInfo.CellId = d(str);
                } else if (str.startsWith("mLac") || str.startsWith("mTac") || str.startsWith("mSystemId")) {
                    networkRegistrationInfo.Tac = d(str);
                } else if (str.startsWith("mBsic") || str.startsWith("mPsc") || str.startsWith("mPci") || str.startsWith("mBasestationId")) {
                    String d = d(str);
                    if (!d.startsWith("0x") || d.length() <= 2) {
                        str2 = d;
                    } else {
                        try {
                            str2 = String.valueOf((int) Long.parseLong(d.substring(2), 16));
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                    networkRegistrationInfo.Pci = str2;
                } else if (str.startsWith("mArfcn") || str.startsWith("mUarfcn") || str.startsWith("mEarfcn") || str.startsWith("mNrArfcn")) {
                    try {
                        networkRegistrationInfo.Arfcn = Integer.parseInt(d(str));
                    } catch (NumberFormatException e2) {
                        e2.printStackTrace();
                    }
                } else if (str.startsWith("mBandwidth")) {
                    try {
                        networkRegistrationInfo.Bandwidth = Integer.parseInt(d(str));
                    } catch (NumberFormatException e3) {
                        e3.printStackTrace();
                    }
                } else if (str.startsWith("mMcc")) {
                    networkRegistrationInfo.Mcc = d(str);
                } else if (str.startsWith("mMnc")) {
                    networkRegistrationInfo.Mnc = d(str);
                } else if (str.startsWith("mAlphaLong")) {
                    networkRegistrationInfo.OperatorLong = d(str);
                } else if (str.startsWith("mAlphaShort")) {
                    networkRegistrationInfo.OperatorShort = d(str);
                } else if (str.startsWith("mMaxDataCalls")) {
                    try {
                        networkRegistrationInfo.MaxDataCalls = Integer.parseInt(d(str));
                    } catch (NumberFormatException e4) {
                        e4.printStackTrace();
                    }
                } else if (str.startsWith("availableServices")) {
                    networkRegistrationInfo.AvailableServices = d(str);
                } else if (str.startsWith("nrState") || str.startsWith("nrStatus")) {
                    networkRegistrationInfo.NrState = d(str);
                } else if (str.startsWith("isDcNrRestricted")) {
                    networkRegistrationInfo.DcNrRestricted = d(str).equals("true") ? ThreeStateShort.Yes : ThreeStateShort.No;
                } else if (str.startsWith("isNrAvailable")) {
                    networkRegistrationInfo.NrAvailable = d(str).equals("true") ? ThreeStateShort.Yes : ThreeStateShort.No;
                } else if (str.startsWith("isEnDcAvailable")) {
                    networkRegistrationInfo.EnDcAvailable = d(str).equals("true") ? ThreeStateShort.Yes : ThreeStateShort.No;
                }
            }
        }
        return networkRegistrationInfo;
    }

    public static List<AdDetails> a(Context context, List<AdDetails> list, int i, Set<String> set) {
        return a(context, list, i, set, true);
    }

    public static List<AdDetails> a(Context context, List<AdDetails> list, int i, Set<String> set, boolean z) {
        Context context2 = context;
        int i2 = i;
        ArrayList arrayList = new ArrayList();
        ArrayList arrayList2 = new ArrayList();
        ArrayList arrayList3 = new ArrayList();
        ArrayList arrayList4 = new ArrayList();
        boolean z2 = false;
        for (AdDetails next : list) {
            AppPresenceDetails appPresenceDetails = new AppPresenceDetails(next.d(), next.o(), i2, next.v());
            boolean z3 = next.o() != null && next.o().startsWith("!");
            String o = next.o();
            if (z3) {
                o = o.substring(1);
            }
            boolean a2 = com.startapp.common.b.b.a(context, o, next.v());
            boolean z4 = AdsCommonMetaData.a().F() && ((a2 && !z3) || (!a2 && z3));
            arrayList3.add(appPresenceDetails);
            if (z4) {
                appPresenceDetails.b(a2);
                appPresenceDetails.a(false);
                if (!z3) {
                    arrayList2.add(next);
                    arrayList4.add(appPresenceDetails);
                }
                set.add(next.n());
                z2 = true;
            } else {
                Set<String> set2 = set;
                arrayList.add(next);
            }
        }
        if (arrayList.size() < 5 && (list.size() != 1 || i2 > 0)) {
            int min = Math.min(5 - arrayList.size(), arrayList2.size());
            arrayList.addAll(arrayList2.subList(0, min));
            for (AppPresenceDetails a3 : arrayList4.subList(0, min)) {
                a3.a(true);
            }
        }
        if (z2) {
            SimpleTokenUtils.c(context);
            if (z) {
                new com.startapp.sdk.adsbase.apppresence.a(context, arrayList3).a();
            }
        }
        return arrayList;
    }

    public static List<AppPresenceDetails> a(String str, int i) {
        ArrayList arrayList = new ArrayList();
        String[] strArr = new String[0];
        String a2 = u.a(str, "@tracking@", "@tracking@");
        if (a2 != null) {
            strArr = a2.split(",");
        }
        String[] strArr2 = new String[0];
        String a3 = u.a(str, "@appPresencePackage@", "@appPresencePackage@");
        if (a3 != null) {
            strArr2 = a3.split(",");
        }
        String[] strArr3 = new String[0];
        String a4 = u.a(str, "@minAppVersion@", "@minAppVersion@");
        if (a4 != null) {
            strArr3 = a4.split(",");
        }
        int i2 = 0;
        while (i2 < strArr2.length) {
            arrayList.add(new AppPresenceDetails(strArr.length > i2 ? strArr[i2] : null, strArr2[i2], i, strArr3.length > i2 ? Integer.valueOf(strArr3[i2]).intValue() : 0));
            i2++;
        }
        while (i2 < strArr.length) {
            arrayList.add(new AppPresenceDetails(strArr[i2], "", i, strArr3.length > i2 ? Integer.valueOf(strArr3[i2]).intValue() : 0));
            i2++;
        }
        return arrayList;
    }

    public static Boolean a(Context context, List<AppPresenceDetails> list, int i, Set<String> set, List<AppPresenceDetails> list2) {
        boolean z = false;
        for (AppPresenceDetails next : list) {
            boolean startsWith = next.b().startsWith("!");
            boolean z2 = true;
            String b = next.b();
            if (startsWith) {
                b = b.substring(1);
            }
            boolean a2 = com.startapp.common.b.b.a(context, b, next.e());
            if ((!startsWith && a2) || (startsWith && !a2)) {
                next.b(a2);
                if (i != 0) {
                    z2 = false;
                }
                if (z2 && !startsWith) {
                    set.add(next.b());
                } else if (!z2 && next.a() != null) {
                    next.a(next.a() + "&isShown=" + next.c() + "&appPresence=" + next.d());
                }
                z = z2;
            }
            list2.add(next);
        }
        if (z) {
            for (int i2 = 0; i2 < list2.size(); i2++) {
                list2.get(i2).a(false);
            }
        }
        return Boolean.valueOf(z);
    }

    public static void a(String str, WebView webView) {
        u.a(webView, "mraid.setPlacementType", str);
    }

    public static void a(MraidState mraidState, WebView webView) {
        u.a(webView, "mraid.fireStateChangeEvent", mraidState.toString());
    }

    public static void a(Context context, int i, int i2, WebView webView) {
        u.a(webView, "mraid.setScreenSize", Integer.valueOf(t.b(context, i)), Integer.valueOf(t.b(context, i2)));
    }

    public static void a(Context context, int i, int i2, int i3, int i4, WebView webView) {
        u.a(webView, "mraid.setCurrentPosition", Integer.valueOf(t.b(context, i)), Integer.valueOf(t.b(context, i2)), Integer.valueOf(t.b(context, i3)), Integer.valueOf(t.b(context, i4)));
    }

    public static void a(WebView webView) {
        u.a(webView, "mraid.fireReadyEvent", new Object[0]);
    }

    public static void a(WebView webView, boolean z) {
        u.a(webView, "mraid.fireViewableChangeEvent", Boolean.valueOf(z));
    }

    public static void a(WebView webView, String str, String str2) {
        u.a(webView, "mraid.fireErrorEvent", str, str2);
    }

    private static void a(WebView webView, String str, boolean z) {
        u.a(webView, false, "mraid.setSupports", str, Boolean.valueOf(z));
    }

    public static void a(Context context, WebView webView, com.startapp.sdk.adsbase.mraid.a.a aVar) {
        if (aVar == null) {
            aVar = new com.startapp.sdk.adsbase.mraid.a.a(context);
        }
        a(webView, "mraid.SUPPORTED_FEATURES.CALENDAR", aVar.a());
        a(webView, "mraid.SUPPORTED_FEATURES.INLINEVIDEO", aVar.b());
        a(webView, "mraid.SUPPORTED_FEATURES.SMS", aVar.c());
        a(webView, "mraid.SUPPORTED_FEATURES.STOREPICTURE", aVar.d());
        a(webView, "mraid.SUPPORTED_FEATURES.TEL", aVar.e());
    }
}
