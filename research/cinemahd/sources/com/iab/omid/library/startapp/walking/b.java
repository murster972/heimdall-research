package com.iab.omid.library.startapp.walking;

import com.iab.omid.library.startapp.walking.a.b;
import com.iab.omid.library.startapp.walking.a.c;
import com.iab.omid.library.startapp.walking.a.d;
import com.iab.omid.library.startapp.walking.a.e;
import com.iab.omid.library.startapp.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public final class b implements b.C0060b {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f4574a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public final JSONObject a() {
        return this.f4574a;
    }

    public final void a(JSONObject jSONObject) {
        this.f4574a = jSONObject;
    }

    public final void a(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        this.b.a(new f(this, hashSet, jSONObject, d));
    }

    public final void b() {
        this.b.a(new d(this));
    }

    public final void b(JSONObject jSONObject, HashSet<String> hashSet, double d) {
        this.b.a(new e(this, hashSet, jSONObject, d));
    }
}
