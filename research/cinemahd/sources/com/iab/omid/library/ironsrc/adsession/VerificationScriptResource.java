package com.iab.omid.library.ironsrc.adsession;

import java.net.URL;

public final class VerificationScriptResource {

    /* renamed from: a  reason: collision with root package name */
    private final String f4523a;
    private final URL b;
    private final String c;

    public URL a() {
        return this.b;
    }

    public String b() {
        return this.f4523a;
    }

    public String c() {
        return this.c;
    }
}
