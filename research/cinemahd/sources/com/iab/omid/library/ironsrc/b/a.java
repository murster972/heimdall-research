package com.iab.omid.library.ironsrc.b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class a {
    private static a c = new a();

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<com.iab.omid.library.ironsrc.adsession.a> f4525a = new ArrayList<>();
    private final ArrayList<com.iab.omid.library.ironsrc.adsession.a> b = new ArrayList<>();

    private a() {
    }

    public static a d() {
        return c;
    }

    public Collection<com.iab.omid.library.ironsrc.adsession.a> a() {
        return Collections.unmodifiableCollection(this.f4525a);
    }

    public void a(com.iab.omid.library.ironsrc.adsession.a aVar) {
        this.f4525a.add(aVar);
    }

    public Collection<com.iab.omid.library.ironsrc.adsession.a> b() {
        return Collections.unmodifiableCollection(this.b);
    }

    public void b(com.iab.omid.library.ironsrc.adsession.a aVar) {
        boolean c2 = c();
        this.b.add(aVar);
        if (!c2) {
            e.d().a();
        }
    }

    public void c(com.iab.omid.library.ironsrc.adsession.a aVar) {
        boolean c2 = c();
        this.f4525a.remove(aVar);
        this.b.remove(aVar);
        if (c2 && !c()) {
            e.d().b();
        }
    }

    public boolean c() {
        return this.b.size() > 0;
    }
}
