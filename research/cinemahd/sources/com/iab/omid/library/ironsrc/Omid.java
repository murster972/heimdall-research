package com.iab.omid.library.ironsrc;

import android.content.Context;

public final class Omid {

    /* renamed from: a  reason: collision with root package name */
    private static a f4514a = new a();

    private Omid() {
    }

    public static String a() {
        return f4514a.a();
    }

    public static boolean a(String str, Context context) {
        f4514a.a(context.getApplicationContext());
        return true;
    }

    public static boolean b() {
        return f4514a.b();
    }
}
