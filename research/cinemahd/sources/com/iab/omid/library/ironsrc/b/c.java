package com.iab.omid.library.ironsrc.b;

import android.annotation.SuppressLint;
import android.content.Context;

public class c {
    @SuppressLint({"StaticFieldLeak"})
    private static c b = new c();

    /* renamed from: a  reason: collision with root package name */
    private Context f4528a;

    private c() {
    }

    public static c b() {
        return b;
    }

    public Context a() {
        return this.f4528a;
    }

    public void a(Context context) {
        this.f4528a = context != null ? context.getApplicationContext() : null;
    }
}
