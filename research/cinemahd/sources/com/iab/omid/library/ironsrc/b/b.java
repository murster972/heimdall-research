package com.iab.omid.library.ironsrc.b;

import android.annotation.SuppressLint;
import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

public class b {
    @SuppressLint({"StaticFieldLeak"})
    private static b f = new b();

    /* renamed from: a  reason: collision with root package name */
    private Context f4526a;
    private BroadcastReceiver b;
    private boolean c;
    private boolean d;
    private a e;

    public interface a {
        void a(boolean z);
    }

    private b() {
    }

    /* access modifiers changed from: private */
    public void a(boolean z) {
        if (this.d != z) {
            this.d = z;
            if (this.c) {
                g();
                a aVar = this.e;
                if (aVar != null) {
                    aVar.a(c());
                }
            }
        }
    }

    public static b d() {
        return f;
    }

    private void e() {
        this.b = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
                KeyguardManager keyguardManager;
                if (intent != null) {
                    if ("android.intent.action.SCREEN_OFF".equals(intent.getAction())) {
                        b.this.a(true);
                    } else if ("android.intent.action.USER_PRESENT".equals(intent.getAction()) || ("android.intent.action.SCREEN_ON".equals(intent.getAction()) && (keyguardManager = (KeyguardManager) context.getSystemService("keyguard")) != null && !keyguardManager.inKeyguardRestrictedInputMode())) {
                        b.this.a(false);
                    }
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.SCREEN_OFF");
        intentFilter.addAction("android.intent.action.SCREEN_ON");
        intentFilter.addAction("android.intent.action.USER_PRESENT");
        this.f4526a.registerReceiver(this.b, intentFilter);
    }

    private void f() {
        BroadcastReceiver broadcastReceiver;
        Context context = this.f4526a;
        if (context != null && (broadcastReceiver = this.b) != null) {
            context.unregisterReceiver(broadcastReceiver);
            this.b = null;
        }
    }

    private void g() {
        boolean z = !this.d;
        for (com.iab.omid.library.ironsrc.adsession.a j : a.d().a()) {
            j.j().a(z);
        }
    }

    public void a() {
        e();
        this.c = true;
        g();
    }

    public void a(Context context) {
        this.f4526a = context.getApplicationContext();
    }

    public void a(a aVar) {
        this.e = aVar;
    }

    public void b() {
        f();
        this.c = false;
        this.d = false;
        this.e = null;
    }

    public boolean c() {
        return !this.d;
    }
}
