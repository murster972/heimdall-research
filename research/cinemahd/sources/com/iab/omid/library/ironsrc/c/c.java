package com.iab.omid.library.ironsrc.c;

import android.view.View;
import com.iab.omid.library.ironsrc.b.a;
import com.iab.omid.library.ironsrc.c.a;
import com.iab.omid.library.ironsrc.d.b;
import com.iab.omid.library.ironsrc.d.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.json.JSONObject;

public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private final a f4533a;

    public c(a aVar) {
        this.f4533a = aVar;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<View> a() {
        View rootView;
        ArrayList<View> arrayList = new ArrayList<>();
        a d = a.d();
        if (d != null) {
            Collection<com.iab.omid.library.ironsrc.adsession.a> b = d.b();
            IdentityHashMap identityHashMap = new IdentityHashMap((b.size() * 2) + 3);
            for (com.iab.omid.library.ironsrc.adsession.a e : b) {
                View e2 = e.e();
                if (e2 != null && f.c(e2) && (rootView = e2.getRootView()) != null && !identityHashMap.containsKey(rootView)) {
                    identityHashMap.put(rootView, rootView);
                    float a2 = f.a(rootView);
                    int size = arrayList.size();
                    while (size > 0 && f.a(arrayList.get(size - 1)) > a2) {
                        size--;
                    }
                    arrayList.add(size, rootView);
                }
            }
        }
        return arrayList;
    }

    public JSONObject a(View view) {
        return b.a(0, 0, 0, 0);
    }

    public void a(View view, JSONObject jSONObject, a.C0057a aVar, boolean z) {
        Iterator<View> it2 = a().iterator();
        while (it2.hasNext()) {
            aVar.a(it2.next(), this.f4533a, jSONObject);
        }
    }
}
