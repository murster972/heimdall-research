package com.iab.omid.library.ironsrc.walking;

import android.view.View;
import android.view.ViewParent;
import com.iab.omid.library.ironsrc.d.f;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<View, String> f4542a = new HashMap<>();
    private final HashMap<String, View> b = new HashMap<>();
    private final HashMap<View, ArrayList<String>> c = new HashMap<>();
    private final HashSet<View> d = new HashSet<>();
    private final HashSet<String> e = new HashSet<>();
    private final HashSet<String> f = new HashSet<>();
    private final HashMap<String, String> g = new HashMap<>();
    private boolean h;

    private void a(View view, com.iab.omid.library.ironsrc.adsession.a aVar) {
        ArrayList arrayList = this.c.get(view);
        if (arrayList == null) {
            arrayList = new ArrayList();
            this.c.put(view, arrayList);
        }
        arrayList.add(aVar.i());
    }

    private void a(com.iab.omid.library.ironsrc.adsession.a aVar) {
        for (com.iab.omid.library.ironsrc.e.a aVar2 : aVar.c()) {
            View view = (View) aVar2.get();
            if (view != null) {
                a(view, aVar);
            }
        }
    }

    private String d(View view) {
        if (!view.hasWindowFocus()) {
            return "noWindowFocus";
        }
        HashSet hashSet = new HashSet();
        while (view != null) {
            String e2 = f.e(view);
            if (e2 != null) {
                return e2;
            }
            hashSet.add(view);
            ViewParent parent = view.getParent();
            view = parent instanceof View ? (View) parent : null;
        }
        this.d.addAll(hashSet);
        return null;
    }

    public String a(View view) {
        if (this.f4542a.size() == 0) {
            return null;
        }
        String str = this.f4542a.get(view);
        if (str != null) {
            this.f4542a.remove(view);
        }
        return str;
    }

    public String a(String str) {
        return this.g.get(str);
    }

    public HashSet<String> a() {
        return this.e;
    }

    public View b(String str) {
        return this.b.get(str);
    }

    public ArrayList<String> b(View view) {
        if (this.c.size() == 0) {
            return null;
        }
        ArrayList<String> arrayList = this.c.get(view);
        if (arrayList != null) {
            this.c.remove(view);
            Collections.sort(arrayList);
        }
        return arrayList;
    }

    public HashSet<String> b() {
        return this.f;
    }

    public c c(View view) {
        return this.d.contains(view) ? c.PARENT_VIEW : this.h ? c.OBSTRUCTION_VIEW : c.UNDERLYING_VIEW;
    }

    public void c() {
        com.iab.omid.library.ironsrc.b.a d2 = com.iab.omid.library.ironsrc.b.a.d();
        if (d2 != null) {
            for (com.iab.omid.library.ironsrc.adsession.a next : d2.b()) {
                View e2 = next.e();
                if (next.f()) {
                    String i = next.i();
                    if (e2 != null) {
                        String d3 = d(e2);
                        if (d3 == null) {
                            this.e.add(i);
                            this.f4542a.put(e2, i);
                            a(next);
                        } else {
                            this.f.add(i);
                            this.b.put(i, e2);
                            this.g.put(i, d3);
                        }
                    } else {
                        this.f.add(i);
                        this.g.put(i, "noAdView");
                    }
                }
            }
        }
    }

    public void d() {
        this.f4542a.clear();
        this.b.clear();
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h = false;
    }

    public void e() {
        this.h = true;
    }
}
