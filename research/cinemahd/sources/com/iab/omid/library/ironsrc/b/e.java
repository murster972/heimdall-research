package com.iab.omid.library.ironsrc.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.ironsrc.a.c;
import com.iab.omid.library.ironsrc.a.d;
import com.iab.omid.library.ironsrc.adsession.a;
import com.iab.omid.library.ironsrc.b.b;
import com.iab.omid.library.ironsrc.walking.TreeWalker;

public class e implements c, b.a {
    private static e f;

    /* renamed from: a  reason: collision with root package name */
    private float f4531a = 0.0f;
    private final com.iab.omid.library.ironsrc.a.e b;
    private final com.iab.omid.library.ironsrc.a.b c;
    private d d;
    private a e;

    public e(com.iab.omid.library.ironsrc.a.e eVar, com.iab.omid.library.ironsrc.a.b bVar) {
        this.b = eVar;
        this.c = bVar;
    }

    public static e d() {
        if (f == null) {
            f = new e(new com.iab.omid.library.ironsrc.a.e(), new com.iab.omid.library.ironsrc.a.b());
        }
        return f;
    }

    private a e() {
        if (this.e == null) {
            this.e = a.d();
        }
        return this.e;
    }

    public void a() {
        b.d().a((b.a) this);
        b.d().a();
        if (b.d().c()) {
            TreeWalker.h().a();
        }
        this.d.a();
    }

    public void a(float f2) {
        this.f4531a = f2;
        for (a j : e().b()) {
            j.j().a(f2);
        }
    }

    public void a(Context context) {
        this.d = this.b.a(new Handler(), context, this.c.a(), this);
    }

    public void a(boolean z) {
        if (z) {
            TreeWalker.h().a();
        } else {
            TreeWalker.h().c();
        }
    }

    public void b() {
        TreeWalker.h().b();
        b.d().b();
        this.d.b();
    }

    public float c() {
        return this.f4531a;
    }
}
