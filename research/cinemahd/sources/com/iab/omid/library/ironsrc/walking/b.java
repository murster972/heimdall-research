package com.iab.omid.library.ironsrc.walking;

import com.iab.omid.library.ironsrc.walking.a.b;
import com.iab.omid.library.ironsrc.walking.a.c;
import com.iab.omid.library.ironsrc.walking.a.d;
import com.iab.omid.library.ironsrc.walking.a.e;
import com.iab.omid.library.ironsrc.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public class b implements b.C0058b {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f4545a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public void a() {
        this.b.b(new d(this));
    }

    public void a(JSONObject jSONObject) {
        this.f4545a = jSONObject;
    }

    public void a(JSONObject jSONObject, HashSet<String> hashSet, long j) {
        this.b.b(new f(this, hashSet, jSONObject, j));
    }

    public JSONObject b() {
        return this.f4545a;
    }

    public void b(JSONObject jSONObject, HashSet<String> hashSet, long j) {
        this.b.b(new e(this, hashSet, jSONObject, j));
    }
}
