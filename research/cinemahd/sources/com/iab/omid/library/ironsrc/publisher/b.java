package com.iab.omid.library.ironsrc.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.iab.omid.library.ironsrc.adsession.AdSessionContext;
import com.iab.omid.library.ironsrc.adsession.VerificationScriptResource;
import com.iab.omid.library.ironsrc.adsession.a;
import com.iab.omid.library.ironsrc.b.c;
import com.iab.omid.library.ironsrc.d.d;
import com.vungle.warren.AdLoader;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */
    public WebView e;
    private Long f = null;
    private Map<String, VerificationScriptResource> g;
    private final String h;

    public b(Map<String, VerificationScriptResource> map, String str) {
        this.g = map;
        this.h = str;
    }

    public void a() {
        super.a();
        i();
    }

    public void a(a aVar, AdSessionContext adSessionContext) {
        JSONObject jSONObject = new JSONObject();
        Map<String, VerificationScriptResource> c = adSessionContext.c();
        for (String next : c.keySet()) {
            com.iab.omid.library.ironsrc.d.b.a(jSONObject, next, c.get(next));
        }
        a(aVar, adSessionContext, jSONObject);
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new Runnable() {

            /* renamed from: a  reason: collision with root package name */
            private WebView f4539a = b.this.e;

            public void run() {
                this.f4539a.destroy();
            }
        }, Math.max(4000 - (this.f == null ? 4000 : TimeUnit.MILLISECONDS.convert(d.a() - this.f.longValue(), TimeUnit.NANOSECONDS)), AdLoader.RETRY_DELAY));
        this.e = null;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void i() {
        this.e = new WebView(c.b().a());
        this.e.getSettings().setJavaScriptEnabled(true);
        a(this.e);
        com.iab.omid.library.ironsrc.b.d.a().a(this.e, this.h);
        for (String next : this.g.keySet()) {
            com.iab.omid.library.ironsrc.b.d.a().a(this.e, this.g.get(next).a().toExternalForm(), next);
        }
        this.f = Long.valueOf(d.a());
    }
}
