package com.iab.omid.library.ironsrc.adsession;

import android.webkit.WebView;
import com.iab.omid.library.ironsrc.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class AdSessionContext {

    /* renamed from: a  reason: collision with root package name */
    private final Partner f4519a;
    private final WebView b;
    private final List<VerificationScriptResource> c = new ArrayList();
    private final Map<String, VerificationScriptResource> d = new HashMap();
    private final String e;
    private final String f;
    private final AdSessionContextType g;

    private AdSessionContext(Partner partner, WebView webView, String str, List<VerificationScriptResource> list, String str2) {
        AdSessionContextType adSessionContextType;
        this.f4519a = partner;
        this.b = webView;
        this.e = str;
        if (list != null) {
            this.c.addAll(list);
            for (VerificationScriptResource put : list) {
                String uuid = UUID.randomUUID().toString();
                this.d.put(uuid, put);
            }
            adSessionContextType = AdSessionContextType.NATIVE;
        } else {
            adSessionContextType = AdSessionContextType.HTML;
        }
        this.g = adSessionContextType;
        this.f = str2;
    }

    public static AdSessionContext a(Partner partner, WebView webView, String str) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) webView, "WebView is null");
        if (str != null) {
            e.a(str, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner, webView, (String) null, (List<VerificationScriptResource>) null, str);
    }

    public AdSessionContextType a() {
        return this.g;
    }

    public String b() {
        return this.f;
    }

    public Map<String, VerificationScriptResource> c() {
        return Collections.unmodifiableMap(this.d);
    }

    public String d() {
        return this.e;
    }

    public Partner e() {
        return this.f4519a;
    }

    public List<VerificationScriptResource> f() {
        return Collections.unmodifiableList(this.c);
    }

    public WebView g() {
        return this.b;
    }
}
