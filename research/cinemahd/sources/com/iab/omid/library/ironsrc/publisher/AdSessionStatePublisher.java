package com.iab.omid.library.ironsrc.publisher;

import android.webkit.WebView;
import com.iab.omid.library.ironsrc.adsession.AdEvents;
import com.iab.omid.library.ironsrc.adsession.AdSessionConfiguration;
import com.iab.omid.library.ironsrc.adsession.AdSessionContext;
import com.iab.omid.library.ironsrc.adsession.VerificationScriptResource;
import com.iab.omid.library.ironsrc.b.c;
import com.iab.omid.library.ironsrc.b.d;
import com.iab.omid.library.ironsrc.e.b;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class AdSessionStatePublisher {

    /* renamed from: a  reason: collision with root package name */
    private b f4537a = new b((WebView) null);
    private AdEvents b;
    private a c;
    private long d;

    enum a {
        AD_STATE_IDLE,
        AD_STATE_VISIBLE,
        AD_STATE_NOTVISIBLE
    }

    public AdSessionStatePublisher() {
        h();
    }

    public void a() {
    }

    public void a(float f) {
        d.a().a(g(), f);
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView) {
        this.f4537a = new b(webView);
    }

    public void a(AdEvents adEvents) {
        this.b = adEvents;
    }

    public void a(AdSessionConfiguration adSessionConfiguration) {
        d.a().a(g(), adSessionConfiguration.b());
    }

    public void a(com.iab.omid.library.ironsrc.adsession.a aVar, AdSessionContext adSessionContext) {
        a(aVar, adSessionContext, (JSONObject) null);
    }

    /* access modifiers changed from: protected */
    public void a(com.iab.omid.library.ironsrc.adsession.a aVar, AdSessionContext adSessionContext, JSONObject jSONObject) {
        String i = aVar.i();
        JSONObject jSONObject2 = new JSONObject();
        com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "environment", "app");
        com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "adSessionType", adSessionContext.a());
        com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "deviceInfo", com.iab.omid.library.ironsrc.d.a.d());
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("clid");
        jSONArray.put("vlid");
        com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "supports", jSONArray);
        JSONObject jSONObject3 = new JSONObject();
        com.iab.omid.library.ironsrc.d.b.a(jSONObject3, "partnerName", adSessionContext.e().a());
        com.iab.omid.library.ironsrc.d.b.a(jSONObject3, "partnerVersion", adSessionContext.e().b());
        com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "omidNativeInfo", jSONObject3);
        JSONObject jSONObject4 = new JSONObject();
        com.iab.omid.library.ironsrc.d.b.a(jSONObject4, "libraryVersion", "1.2.22-Ironsrc");
        com.iab.omid.library.ironsrc.d.b.a(jSONObject4, "appId", c.b().a().getApplicationContext().getPackageName());
        com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "app", jSONObject4);
        if (adSessionContext.b() != null) {
            com.iab.omid.library.ironsrc.d.b.a(jSONObject2, "customReferenceData", adSessionContext.b());
        }
        JSONObject jSONObject5 = new JSONObject();
        for (VerificationScriptResource next : adSessionContext.f()) {
            com.iab.omid.library.ironsrc.d.b.a(jSONObject5, next.b(), next.c());
        }
        d.a().a(g(), i, jSONObject2, jSONObject5, jSONObject);
    }

    public void a(String str, long j) {
        if (j >= this.d) {
            this.c = a.AD_STATE_VISIBLE;
            d.a().b(g(), str);
        }
    }

    public void a(boolean z) {
        if (d()) {
            d.a().c(g(), z ? "foregrounded" : "backgrounded");
        }
    }

    public void b() {
        this.f4537a.clear();
    }

    public void b(String str, long j) {
        a aVar;
        if (j >= this.d && this.c != (aVar = a.AD_STATE_NOTVISIBLE)) {
            this.c = aVar;
            d.a().b(g(), str);
        }
    }

    public AdEvents c() {
        return this.b;
    }

    public boolean d() {
        return this.f4537a.get() != null;
    }

    public void e() {
        d.a().a(g());
    }

    public void f() {
        d.a().b(g());
    }

    public WebView g() {
        return (WebView) this.f4537a.get();
    }

    public void h() {
        this.d = com.iab.omid.library.ironsrc.d.d.a();
        this.c = a.AD_STATE_IDLE;
    }
}
