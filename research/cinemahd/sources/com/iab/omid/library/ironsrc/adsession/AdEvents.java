package com.iab.omid.library.ironsrc.adsession;

import com.iab.omid.library.ironsrc.d.e;

public final class AdEvents {

    /* renamed from: a  reason: collision with root package name */
    private final a f4517a;

    private AdEvents(a aVar) {
        this.f4517a = aVar;
    }

    public static AdEvents a(AdSession adSession) {
        a aVar = (a) adSession;
        e.a((Object) adSession, "AdSession is null");
        e.b(aVar);
        e.a(aVar);
        AdEvents adEvents = new AdEvents(aVar);
        aVar.j().a(adEvents);
        return adEvents;
    }

    public void a() {
        e.a(this.f4517a);
        e.c(this.f4517a);
        if (!this.f4517a.f()) {
            try {
                this.f4517a.b();
            } catch (Exception unused) {
            }
        }
        if (this.f4517a.f()) {
            this.f4517a.d();
        }
    }
}
