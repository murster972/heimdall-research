package com.iab.omid.library.ironsrc.adsession;

import android.view.View;
import com.iab.omid.library.ironsrc.d.e;

public abstract class AdSession {
    public static AdSession a(AdSessionConfiguration adSessionConfiguration, AdSessionContext adSessionContext) {
        e.a();
        e.a((Object) adSessionConfiguration, "AdSessionConfiguration is null");
        e.a((Object) adSessionContext, "AdSessionContext is null");
        return new a(adSessionConfiguration, adSessionContext);
    }

    public abstract void a();

    public abstract void a(View view);

    public abstract void b();
}
