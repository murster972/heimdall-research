package com.iab.omid.library.ironsrc.walking;

import android.os.Handler;
import android.os.Looper;
import android.view.View;
import com.iab.omid.library.ironsrc.c.a;
import com.iab.omid.library.ironsrc.c.b;
import com.iab.omid.library.ironsrc.d.d;
import com.iab.omid.library.ironsrc.d.f;
import com.iab.omid.library.ironsrc.walking.a.c;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class TreeWalker implements a.C0057a {
    private static TreeWalker g = new TreeWalker();
    private static Handler h = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public static Handler i = null;
    /* access modifiers changed from: private */
    public static final Runnable j = new Runnable() {
        public void run() {
            TreeWalker.h().i();
        }
    };
    /* access modifiers changed from: private */
    public static final Runnable k = new Runnable() {
        public void run() {
            if (TreeWalker.i != null) {
                TreeWalker.i.post(TreeWalker.j);
                TreeWalker.i.postDelayed(TreeWalker.k, 200);
            }
        }
    };

    /* renamed from: a  reason: collision with root package name */
    private List<TreeWalkerTimeLogger> f4540a = new ArrayList();
    private int b;
    private b c = new b();
    private a d = new a();
    /* access modifiers changed from: private */
    public b e = new b(new c());
    private long f;

    public interface TreeWalkerNanoTimeLogger extends TreeWalkerTimeLogger {
        void a(int i, long j);
    }

    public interface TreeWalkerTimeLogger {
        void b(int i, long j);
    }

    TreeWalker() {
    }

    private void a(long j2) {
        if (this.f4540a.size() > 0) {
            for (TreeWalkerTimeLogger next : this.f4540a) {
                next.b(this.b, TimeUnit.NANOSECONDS.toMillis(j2));
                if (next instanceof TreeWalkerNanoTimeLogger) {
                    ((TreeWalkerNanoTimeLogger) next).a(this.b, j2);
                }
            }
        }
    }

    private void a(View view, a aVar, JSONObject jSONObject, c cVar) {
        aVar.a(view, jSONObject, this, cVar == c.PARENT_VIEW);
    }

    private void a(String str, View view, JSONObject jSONObject) {
        a b2 = this.c.b();
        String a2 = this.d.a(str);
        if (a2 != null) {
            JSONObject a3 = b2.a(view);
            com.iab.omid.library.ironsrc.d.b.a(a3, str);
            com.iab.omid.library.ironsrc.d.b.b(a3, a2);
            com.iab.omid.library.ironsrc.d.b.a(jSONObject, a3);
        }
    }

    private boolean a(View view, JSONObject jSONObject) {
        String a2 = this.d.a(view);
        if (a2 == null) {
            return false;
        }
        com.iab.omid.library.ironsrc.d.b.a(jSONObject, a2);
        this.d.e();
        return true;
    }

    private void b(View view, JSONObject jSONObject) {
        ArrayList<String> b2 = this.d.b(view);
        if (b2 != null) {
            com.iab.omid.library.ironsrc.d.b.a(jSONObject, (List<String>) b2);
        }
    }

    public static TreeWalker h() {
        return g;
    }

    /* access modifiers changed from: private */
    public void i() {
        j();
        d();
        k();
    }

    private void j() {
        this.b = 0;
        this.f = d.a();
    }

    private void k() {
        a(d.a() - this.f);
    }

    private void l() {
        if (i == null) {
            i = new Handler(Looper.getMainLooper());
            i.post(j);
            i.postDelayed(k, 200);
        }
    }

    private void m() {
        Handler handler = i;
        if (handler != null) {
            handler.removeCallbacks(k);
            i = null;
        }
    }

    public void a() {
        l();
    }

    public void a(View view, a aVar, JSONObject jSONObject) {
        c c2;
        if (f.d(view) && (c2 = this.d.c(view)) != c.UNDERLYING_VIEW) {
            JSONObject a2 = aVar.a(view);
            com.iab.omid.library.ironsrc.d.b.a(jSONObject, a2);
            if (!a(view, a2)) {
                b(view, a2);
                a(view, aVar, a2, c2);
            }
            this.b++;
        }
    }

    public void b() {
        c();
        this.f4540a.clear();
        h.post(new Runnable() {
            public void run() {
                TreeWalker.this.e.a();
            }
        });
    }

    public void c() {
        m();
    }

    /* access modifiers changed from: package-private */
    public void d() {
        this.d.c();
        long a2 = d.a();
        a a3 = this.c.a();
        if (this.d.b().size() > 0) {
            Iterator<String> it2 = this.d.b().iterator();
            while (it2.hasNext()) {
                String next = it2.next();
                JSONObject a4 = a3.a((View) null);
                a(next, this.d.b(next), a4);
                com.iab.omid.library.ironsrc.d.b.a(a4);
                HashSet hashSet = new HashSet();
                hashSet.add(next);
                this.e.b(a4, hashSet, a2);
            }
        }
        if (this.d.a().size() > 0) {
            JSONObject a5 = a3.a((View) null);
            a((View) null, a3, a5, c.PARENT_VIEW);
            com.iab.omid.library.ironsrc.d.b.a(a5);
            this.e.a(a5, this.d.a(), a2);
        } else {
            this.e.a();
        }
        this.d.d();
    }
}
