package com.iab.omid.library.ironsrc.adsession;

import com.iab.omid.library.ironsrc.d.b;
import com.iab.omid.library.ironsrc.d.e;
import org.json.JSONObject;

public class AdSessionConfiguration {

    /* renamed from: a  reason: collision with root package name */
    private final Owner f4518a;
    private final Owner b;
    private final boolean c;

    private AdSessionConfiguration(Owner owner, Owner owner2, boolean z) {
        this.f4518a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = z;
    }

    public static AdSessionConfiguration a(Owner owner, Owner owner2, boolean z) {
        e.a((Object) owner, "Impression owner is null");
        e.a(owner);
        return new AdSessionConfiguration(owner, owner2, z);
    }

    public boolean a() {
        return Owner.NATIVE == this.f4518a;
    }

    public JSONObject b() {
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.f4518a);
        b.a(jSONObject, "videoEventsOwner", this.b);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(this.c));
        return jSONObject;
    }
}
