package com.iab.omid.library.ironsrc.adsession;

import com.facebook.react.uimanager.ViewProps;

public enum Owner {
    NATIVE("native"),
    JAVASCRIPT("javascript"),
    NONE(ViewProps.NONE);
    
    private final String owner;

    private Owner(String str) {
        this.owner = str;
    }

    public String toString() {
        return this.owner;
    }
}
