package com.iab.omid.library.ironsrc.adsession;

import android.view.View;
import com.iab.omid.library.ironsrc.d.e;
import com.iab.omid.library.ironsrc.publisher.AdSessionStatePublisher;
import com.iab.omid.library.ironsrc.publisher.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class a extends AdSession {

    /* renamed from: a  reason: collision with root package name */
    private final AdSessionContext f4524a;
    private final AdSessionConfiguration b;
    private final List<com.iab.omid.library.ironsrc.e.a> c = new ArrayList();
    private com.iab.omid.library.ironsrc.e.a d;
    private AdSessionStatePublisher e;
    private boolean f = false;
    private boolean g = false;
    private String h;
    private boolean i;

    a(AdSessionConfiguration adSessionConfiguration, AdSessionContext adSessionContext) {
        this.b = adSessionConfiguration;
        this.f4524a = adSessionContext;
        this.h = UUID.randomUUID().toString();
        b((View) null);
        this.e = adSessionContext.a() == AdSessionContextType.HTML ? new com.iab.omid.library.ironsrc.publisher.a(adSessionContext.g()) : new b(adSessionContext.c(), adSessionContext.d());
        this.e.a();
        com.iab.omid.library.ironsrc.b.a.d().a(this);
        this.e.a(adSessionConfiguration);
    }

    private void b(View view) {
        this.d = new com.iab.omid.library.ironsrc.e.a(view);
    }

    private void c(View view) {
        Collection<a> a2 = com.iab.omid.library.ironsrc.b.a.d().a();
        if (a2 != null && a2.size() > 0) {
            for (a next : a2) {
                if (next != this && next.e() == view) {
                    next.d.clear();
                }
            }
        }
    }

    private void l() {
        if (this.i) {
            throw new IllegalStateException("Impression event can only be sent once");
        }
    }

    public void a() {
        if (!this.g) {
            this.d.clear();
            k();
            this.g = true;
            j().e();
            com.iab.omid.library.ironsrc.b.a.d().c(this);
            j().b();
            this.e = null;
        }
    }

    public void a(View view) {
        if (!this.g) {
            e.a((Object) view, "AdView is null");
            if (e() != view) {
                b(view);
                j().h();
                c(view);
            }
        }
    }

    public void b() {
        if (!this.f) {
            this.f = true;
            com.iab.omid.library.ironsrc.b.a.d().b(this);
            this.e.a(com.iab.omid.library.ironsrc.b.e.d().c());
            this.e.a(this, this.f4524a);
        }
    }

    public List<com.iab.omid.library.ironsrc.e.a> c() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void d() {
        l();
        j().f();
        this.i = true;
    }

    public View e() {
        return (View) this.d.get();
    }

    public boolean f() {
        return this.f && !this.g;
    }

    public boolean g() {
        return this.g;
    }

    public boolean h() {
        return this.b.a();
    }

    public String i() {
        return this.h;
    }

    public AdSessionStatePublisher j() {
        return this.e;
    }

    public void k() {
        if (!this.g) {
            this.c.clear();
        }
    }
}
