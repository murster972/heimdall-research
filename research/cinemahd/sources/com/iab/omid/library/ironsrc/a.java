package com.iab.omid.library.ironsrc;

import android.content.Context;
import com.iab.omid.library.ironsrc.b.b;
import com.iab.omid.library.ironsrc.b.c;
import com.iab.omid.library.ironsrc.d.e;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4515a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return "1.2.22-Ironsrc";
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        b(context);
        if (!b()) {
            a(true);
            com.iab.omid.library.ironsrc.b.e.d().a(context);
            b.d().a(context);
            com.iab.omid.library.ironsrc.d.b.a(context);
            c.b().a(context);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f4515a = z;
    }

    /* access modifiers changed from: package-private */
    public boolean b() {
        return this.f4515a;
    }
}
