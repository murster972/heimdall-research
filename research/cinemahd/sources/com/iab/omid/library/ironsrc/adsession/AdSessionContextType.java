package com.iab.omid.library.ironsrc.adsession;

public enum AdSessionContextType {
    HTML("html"),
    NATIVE("native");
    
    private final String typeString;

    private AdSessionContextType(String str) {
        this.typeString = str;
    }

    public String toString() {
        return this.typeString;
    }
}
