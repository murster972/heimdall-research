package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.adsession.media.VastProperties;
import com.iab.omid.library.adcolony.d.e;

public final class AdEvents {

    /* renamed from: a  reason: collision with root package name */
    private final a f4474a;

    private AdEvents(a aVar) {
        this.f4474a = aVar;
    }

    public static AdEvents a(AdSession adSession) {
        a aVar = (a) adSession;
        e.a((Object) adSession, "AdSession is null");
        e.d(aVar);
        e.b(aVar);
        AdEvents adEvents = new AdEvents(aVar);
        aVar.k().a(adEvents);
        return adEvents;
    }

    public void a() {
        e.b(this.f4474a);
        e.f(this.f4474a);
        if (!this.f4474a.h()) {
            try {
                this.f4474a.c();
            } catch (Exception unused) {
            }
        }
        if (this.f4474a.h()) {
            this.f4474a.e();
        }
    }

    public void a(VastProperties vastProperties) {
        e.a((Object) vastProperties, "VastProperties is null");
        e.c(this.f4474a);
        e.f(this.f4474a);
        this.f4474a.a(vastProperties.a());
    }

    public void b() {
        e.c(this.f4474a);
        e.f(this.f4474a);
        this.f4474a.f();
    }
}
