package com.iab.omid.library.adcolony;

import android.content.Context;
import com.iab.omid.library.adcolony.b.d;
import com.iab.omid.library.adcolony.b.f;
import com.iab.omid.library.adcolony.d.e;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private boolean f4489a;

    private void b(Context context) {
        e.a((Object) context, "Application Context cannot be null");
    }

    /* access modifiers changed from: package-private */
    public void a(Context context) {
        b(context);
        if (!a()) {
            a(true);
            f.d().a(context);
            com.iab.omid.library.adcolony.b.b.d().a(context);
            com.iab.omid.library.adcolony.d.b.a(context);
            d.b().a(context);
        }
    }

    /* access modifiers changed from: package-private */
    public void a(boolean z) {
        this.f4489a = z;
    }

    /* access modifiers changed from: package-private */
    public boolean a() {
        return this.f4489a;
    }
}
