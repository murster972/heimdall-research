package com.iab.omid.library.adcolony.b;

import android.annotation.SuppressLint;
import android.content.Context;

public class b {
    @SuppressLint({"StaticFieldLeak"})
    private static b b = new b();

    /* renamed from: a  reason: collision with root package name */
    private boolean f4491a;

    public interface a {
    }

    private b() {
    }

    public static b d() {
        return b;
    }

    private void e() {
        boolean z = !this.f4491a;
        for (com.iab.omid.library.adcolony.adsession.a k : a.d().a()) {
            k.k().a(z);
        }
    }

    public void a() {
        e();
    }

    public void a(Context context) {
        context.getApplicationContext();
    }

    public void a(a aVar) {
    }

    public void b() {
        this.f4491a = false;
    }

    public boolean c() {
        return !this.f4491a;
    }
}
