package com.iab.omid.library.adcolony.b;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class a {
    private static a c = new a();

    /* renamed from: a  reason: collision with root package name */
    private final ArrayList<com.iab.omid.library.adcolony.adsession.a> f4490a = new ArrayList<>();
    private final ArrayList<com.iab.omid.library.adcolony.adsession.a> b = new ArrayList<>();

    private a() {
    }

    public static a d() {
        return c;
    }

    public Collection<com.iab.omid.library.adcolony.adsession.a> a() {
        return Collections.unmodifiableCollection(this.f4490a);
    }

    public void a(com.iab.omid.library.adcolony.adsession.a aVar) {
        this.f4490a.add(aVar);
    }

    public Collection<com.iab.omid.library.adcolony.adsession.a> b() {
        return Collections.unmodifiableCollection(this.b);
    }

    public void b(com.iab.omid.library.adcolony.adsession.a aVar) {
        boolean c2 = c();
        this.b.add(aVar);
        if (!c2) {
            f.d().a();
        }
    }

    public void c(com.iab.omid.library.adcolony.adsession.a aVar) {
        boolean c2 = c();
        this.f4490a.remove(aVar);
        this.b.remove(aVar);
        if (c2 && !c()) {
            f.d().b();
        }
    }

    public boolean c() {
        return this.b.size() > 0;
    }
}
