package com.iab.omid.library.adcolony.walking.a;

import com.iab.omid.library.adcolony.walking.a.b;
import java.util.ArrayDeque;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class c implements b.a {

    /* renamed from: a  reason: collision with root package name */
    private final ThreadPoolExecutor f4511a = new ThreadPoolExecutor(1, 1, 1, TimeUnit.SECONDS, new LinkedBlockingQueue());
    private final ArrayDeque<b> b = new ArrayDeque<>();
    private b c = null;

    private void a() {
        b poll = this.b.poll();
        this.c = poll;
        if (poll != null) {
            poll.a(this.f4511a);
        }
    }

    public void a(b bVar) {
        this.c = null;
        a();
    }

    public void b(b bVar) {
        bVar.a((b.a) this);
        this.b.add(bVar);
        if (this.c == null) {
            a();
        }
    }
}
