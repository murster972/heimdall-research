package com.iab.omid.library.adcolony.adsession.media;

public enum InteractionType {
    CLICK("click");
    

    /* renamed from: a  reason: collision with root package name */
    String f4485a;

    private InteractionType(String str) {
        this.f4485a = str;
    }

    public String toString() {
        return this.f4485a;
    }
}
