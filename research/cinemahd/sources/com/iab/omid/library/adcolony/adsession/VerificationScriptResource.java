package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.e;
import java.net.URL;

public final class VerificationScriptResource {

    /* renamed from: a  reason: collision with root package name */
    private final String f4483a;
    private final URL b;
    private final String c;

    private VerificationScriptResource(String str, URL url, String str2) {
        this.f4483a = str;
        this.b = url;
        this.c = str2;
    }

    public static VerificationScriptResource a(String str, URL url, String str2) {
        e.a(str, "VendorKey is null or empty");
        e.a((Object) url, "ResourceURL is null");
        e.a(str2, "VerificationParameters is null or empty");
        return new VerificationScriptResource(str, url, str2);
    }

    public static VerificationScriptResource a(URL url) {
        e.a((Object) url, "ResourceURL is null");
        return new VerificationScriptResource((String) null, url, (String) null);
    }

    public URL a() {
        return this.b;
    }

    public String b() {
        return this.f4483a;
    }

    public String c() {
        return this.c;
    }
}
