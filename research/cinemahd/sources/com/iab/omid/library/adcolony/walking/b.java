package com.iab.omid.library.adcolony.walking;

import com.iab.omid.library.adcolony.walking.a.b;
import com.iab.omid.library.adcolony.walking.a.c;
import com.iab.omid.library.adcolony.walking.a.d;
import com.iab.omid.library.adcolony.walking.a.e;
import com.iab.omid.library.adcolony.walking.a.f;
import java.util.HashSet;
import org.json.JSONObject;

public class b implements b.C0056b {

    /* renamed from: a  reason: collision with root package name */
    private JSONObject f4512a;
    private final c b;

    public b(c cVar) {
        this.b = cVar;
    }

    public void a() {
        this.b.b(new d(this));
    }

    public void a(JSONObject jSONObject) {
        this.f4512a = jSONObject;
    }

    public void a(JSONObject jSONObject, HashSet<String> hashSet, long j) {
        this.b.b(new f(this, hashSet, jSONObject, j));
    }

    public JSONObject b() {
        return this.f4512a;
    }

    public void b(JSONObject jSONObject, HashSet<String> hashSet, long j) {
        this.b.b(new e(this, hashSet, jSONObject, j));
    }
}
