package com.iab.omid.library.adcolony.adsession;

import com.vungle.warren.model.Advertisement;

public enum CreativeType {
    DEFINED_BY_JAVASCRIPT("definedByJavaScript"),
    HTML_DISPLAY("htmlDisplay"),
    NATIVE_DISPLAY("nativeDisplay"),
    VIDEO(Advertisement.KEY_VIDEO);
    

    /* renamed from: a  reason: collision with root package name */
    private final String f4478a;

    private CreativeType(String str) {
        this.f4478a = str;
    }

    public String toString() {
        return this.f4478a;
    }
}
