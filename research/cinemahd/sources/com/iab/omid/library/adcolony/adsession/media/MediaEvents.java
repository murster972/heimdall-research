package com.iab.omid.library.adcolony.adsession.media;

import com.facebook.react.uimanager.ViewProps;
import com.iab.omid.library.adcolony.adsession.AdSession;
import com.iab.omid.library.adcolony.adsession.a;
import com.iab.omid.library.adcolony.b.f;
import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import org.json.JSONObject;

public final class MediaEvents {

    /* renamed from: a  reason: collision with root package name */
    private final a f4486a;

    private MediaEvents(a aVar) {
        this.f4486a = aVar;
    }

    public static MediaEvents a(AdSession adSession) {
        a aVar = (a) adSession;
        e.a((Object) adSession, "AdSession is null");
        e.g(aVar);
        e.a(aVar);
        e.b(aVar);
        e.e(aVar);
        MediaEvents mediaEvents = new MediaEvents(aVar);
        aVar.k().a(mediaEvents);
        return mediaEvents;
    }

    private void b(float f) {
        if (f <= 0.0f) {
            throw new IllegalArgumentException("Invalid Media duration");
        }
    }

    private void c(float f) {
        if (f < 0.0f || f > 1.0f) {
            throw new IllegalArgumentException("Invalid Media volume");
        }
    }

    public void a() {
        e.c(this.f4486a);
        this.f4486a.k().a("bufferFinish");
    }

    public void a(float f) {
        c(f);
        e.c(this.f4486a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "mediaPlayerVolume", Float.valueOf(f));
        b.a(jSONObject, "deviceVolume", Float.valueOf(f.d().c()));
        this.f4486a.k().a("volumeChange", jSONObject);
    }

    public void a(float f, float f2) {
        b(f);
        c(f2);
        e.c(this.f4486a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "duration", Float.valueOf(f));
        b.a(jSONObject, "mediaPlayerVolume", Float.valueOf(f2));
        b.a(jSONObject, "deviceVolume", Float.valueOf(f.d().c()));
        this.f4486a.k().a(ViewProps.START, jSONObject);
    }

    public void a(InteractionType interactionType) {
        e.a((Object) interactionType, "InteractionType is null");
        e.c(this.f4486a);
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "interactionType", interactionType);
        this.f4486a.k().a("adUserInteraction", jSONObject);
    }

    public void b() {
        e.c(this.f4486a);
        this.f4486a.k().a("bufferStart");
    }

    public void c() {
        e.c(this.f4486a);
        this.f4486a.k().a("complete");
    }

    public void d() {
        e.c(this.f4486a);
        this.f4486a.k().a("firstQuartile");
    }

    public void e() {
        e.c(this.f4486a);
        this.f4486a.k().a("midpoint");
    }

    public void f() {
        e.c(this.f4486a);
        this.f4486a.k().a("pause");
    }

    public void g() {
        e.c(this.f4486a);
        this.f4486a.k().a("resume");
    }

    public void h() {
        e.c(this.f4486a);
        this.f4486a.k().a("skipped");
    }

    public void i() {
        e.c(this.f4486a);
        this.f4486a.k().a("thirdQuartile");
    }
}
