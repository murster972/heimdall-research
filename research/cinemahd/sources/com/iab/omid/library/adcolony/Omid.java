package com.iab.omid.library.adcolony;

import android.content.Context;

public final class Omid {

    /* renamed from: a  reason: collision with root package name */
    private static b f4471a = new b();

    private Omid() {
    }

    public static void a(Context context) {
        f4471a.a(context.getApplicationContext());
    }

    public static boolean a() {
        return f4471a.a();
    }
}
