package com.iab.omid.library.adcolony.publisher;

import android.annotation.SuppressLint;
import android.os.Handler;
import android.webkit.WebView;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.b.e;
import com.iab.omid.library.adcolony.d.d;
import com.vungle.warren.AdLoader;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class b extends AdSessionStatePublisher {
    /* access modifiers changed from: private */
    public WebView f;
    private Long g = null;
    private Map<String, VerificationScriptResource> h;
    private final String i;

    class a implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private WebView f4505a;

        a(b bVar) {
            this.f4505a = bVar.f;
        }

        public void run() {
            this.f4505a.destroy();
        }
    }

    public b(Map<String, VerificationScriptResource> map, String str) {
        this.h = map;
        this.i = str;
    }

    public void a() {
        super.a();
        k();
    }

    public void a(com.iab.omid.library.adcolony.adsession.a aVar, AdSessionContext adSessionContext) {
        JSONObject jSONObject = new JSONObject();
        Map<String, VerificationScriptResource> d = adSessionContext.d();
        for (String next : d.keySet()) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject, next, d.get(next));
        }
        a(aVar, adSessionContext, jSONObject);
    }

    public void b() {
        super.b();
        new Handler().postDelayed(new a(this), Math.max(4000 - (this.g == null ? 4000 : TimeUnit.MILLISECONDS.convert(d.a() - this.g.longValue(), TimeUnit.NANOSECONDS)), AdLoader.RETRY_DELAY));
        this.f = null;
    }

    /* access modifiers changed from: package-private */
    @SuppressLint({"SetJavaScriptEnabled"})
    public void k() {
        WebView webView = new WebView(com.iab.omid.library.adcolony.b.d.b().a());
        this.f = webView;
        webView.getSettings().setJavaScriptEnabled(true);
        a(this.f);
        e.a().a(this.f, this.i);
        for (String next : this.h.keySet()) {
            e.a().a(this.f, this.h.get(next).a().toExternalForm(), next);
        }
        this.g = Long.valueOf(d.a());
    }
}
