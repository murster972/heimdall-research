package com.iab.omid.library.adcolony.adsession;

import android.view.View;
import com.iab.omid.library.adcolony.b.c;
import com.iab.omid.library.adcolony.b.f;
import com.iab.omid.library.adcolony.d.e;
import com.iab.omid.library.adcolony.publisher.AdSessionStatePublisher;
import com.iab.omid.library.adcolony.publisher.b;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.regex.Pattern;
import org.json.JSONObject;

public class a extends AdSession {
    private static final Pattern k = Pattern.compile("^[a-zA-Z0-9 ]+$");

    /* renamed from: a  reason: collision with root package name */
    private final AdSessionContext f4484a;
    private final AdSessionConfiguration b;
    private final List<c> c = new ArrayList();
    private com.iab.omid.library.adcolony.e.a d;
    private AdSessionStatePublisher e;
    private boolean f = false;
    private boolean g = false;
    private String h;
    private boolean i;
    private boolean j;

    a(AdSessionConfiguration adSessionConfiguration, AdSessionContext adSessionContext) {
        this.b = adSessionConfiguration;
        this.f4484a = adSessionContext;
        this.h = UUID.randomUUID().toString();
        e((View) null);
        this.e = (adSessionContext.a() == AdSessionContextType.HTML || adSessionContext.a() == AdSessionContextType.JAVASCRIPT) ? new com.iab.omid.library.adcolony.publisher.a(adSessionContext.h()) : new b(adSessionContext.d(), adSessionContext.e());
        this.e.a();
        com.iab.omid.library.adcolony.b.a.d().a(this);
        this.e.a(adSessionConfiguration);
    }

    private void a(String str) {
        if (str == null) {
            return;
        }
        if (str.length() > 50 || !k.matcher(str).matches()) {
            throw new IllegalArgumentException("FriendlyObstruction has improperly formatted detailed reason");
        }
    }

    private c c(View view) {
        for (c next : this.c) {
            if (next.a().get() == view) {
                return next;
            }
        }
        return null;
    }

    private void d(View view) {
        if (view == null) {
            throw new IllegalArgumentException("FriendlyObstruction is null");
        }
    }

    private void e(View view) {
        this.d = new com.iab.omid.library.adcolony.e.a(view);
    }

    private void f(View view) {
        Collection<a> a2 = com.iab.omid.library.adcolony.b.a.d().a();
        if (a2 != null && a2.size() > 0) {
            for (a next : a2) {
                if (next != this && next.g() == view) {
                    next.d.clear();
                }
            }
        }
    }

    private void o() {
        if (this.i) {
            throw new IllegalStateException("Impression event can only be sent once");
        }
    }

    private void p() {
        if (this.j) {
            throw new IllegalStateException("Loaded event can only be sent once");
        }
    }

    public void a() {
        if (!this.g) {
            this.d.clear();
            n();
            this.g = true;
            k().f();
            com.iab.omid.library.adcolony.b.a.d().c(this);
            k().b();
            this.e = null;
        }
    }

    public void a(View view) {
        if (!this.g) {
            e.a((Object) view, "AdView is null");
            if (g() != view) {
                e(view);
                k().j();
                f(view);
            }
        }
    }

    public void a(View view, FriendlyObstructionPurpose friendlyObstructionPurpose, String str) {
        if (!this.g) {
            d(view);
            a(str);
            if (c(view) == null) {
                this.c.add(new c(view, friendlyObstructionPurpose, str));
            }
        }
    }

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject) {
        p();
        k().a(jSONObject);
        this.j = true;
    }

    public String b() {
        return this.h;
    }

    public void b(View view) {
        if (!this.g) {
            d(view);
            c c2 = c(view);
            if (c2 != null) {
                this.c.remove(c2);
            }
        }
    }

    public void c() {
        if (!this.f) {
            this.f = true;
            com.iab.omid.library.adcolony.b.a.d().b(this);
            this.e.a(f.d().c());
            this.e.a(this, this.f4484a);
        }
    }

    public List<c> d() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public void e() {
        o();
        k().g();
        this.i = true;
    }

    /* access modifiers changed from: package-private */
    public void f() {
        p();
        k().i();
        this.j = true;
    }

    public View g() {
        return (View) this.d.get();
    }

    public boolean h() {
        return this.f && !this.g;
    }

    public boolean i() {
        return this.f;
    }

    public boolean j() {
        return this.g;
    }

    public AdSessionStatePublisher k() {
        return this.e;
    }

    public boolean l() {
        return this.b.a();
    }

    public boolean m() {
        return this.b.b();
    }

    public void n() {
        if (!this.g) {
            this.c.clear();
        }
    }
}
