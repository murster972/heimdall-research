package com.iab.omid.library.adcolony.adsession;

public enum AdSessionContextType {
    HTML("html"),
    NATIVE("native"),
    JAVASCRIPT("javascript");
    

    /* renamed from: a  reason: collision with root package name */
    private final String f4477a;

    private AdSessionContextType(String str) {
        this.f4477a = str;
    }

    public String toString() {
        return this.f4477a;
    }
}
