package com.iab.omid.library.adcolony.walking.a;

import android.os.AsyncTask;
import java.util.concurrent.ThreadPoolExecutor;
import org.json.JSONObject;

public abstract class b extends AsyncTask<Object, Void, String> {

    /* renamed from: a  reason: collision with root package name */
    private a f4510a;
    protected final C0056b b;

    public interface a {
        void a(b bVar);
    }

    /* renamed from: com.iab.omid.library.adcolony.walking.a.b$b  reason: collision with other inner class name */
    public interface C0056b {
        void a(JSONObject jSONObject);

        JSONObject b();
    }

    public b(C0056b bVar) {
        this.b = bVar;
    }

    public void a(a aVar) {
        this.f4510a = aVar;
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        a aVar = this.f4510a;
        if (aVar != null) {
            aVar.a(this);
        }
    }

    public void a(ThreadPoolExecutor threadPoolExecutor) {
        executeOnExecutor(threadPoolExecutor, new Object[0]);
    }
}
