package com.iab.omid.library.adcolony.adsession;

public enum FriendlyObstructionPurpose {
    CLOSE_AD,
    OTHER
}
