package com.iab.omid.library.adcolony.adsession.media;

import com.facebook.react.uimanager.ViewProps;
import com.iab.omid.library.adcolony.d.c;
import com.iab.omid.library.adcolony.d.e;
import org.json.JSONException;
import org.json.JSONObject;

public final class VastProperties {

    /* renamed from: a  reason: collision with root package name */
    private final boolean f4488a;
    private final Float b;
    private final boolean c;
    private final Position d;

    private VastProperties(boolean z, Float f, boolean z2, Position position) {
        this.f4488a = z;
        this.b = f;
        this.c = z2;
        this.d = position;
    }

    public static VastProperties a(float f, boolean z, Position position) {
        e.a((Object) position, "Position is null");
        return new VastProperties(true, Float.valueOf(f), z, position);
    }

    public static VastProperties a(boolean z, Position position) {
        e.a((Object) position, "Position is null");
        return new VastProperties(false, (Float) null, z, position);
    }

    public JSONObject a() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("skippable", this.f4488a);
            if (this.f4488a) {
                jSONObject.put("skipOffset", this.b);
            }
            jSONObject.put("autoPlay", this.c);
            jSONObject.put(ViewProps.POSITION, this.d);
        } catch (JSONException e) {
            c.a("VastProperties: JSON error", e);
        }
        return jSONObject;
    }
}
