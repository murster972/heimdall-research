package com.iab.omid.library.adcolony.adsession;

import android.webkit.WebView;
import com.iab.omid.library.adcolony.d.e;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public final class AdSessionContext {

    /* renamed from: a  reason: collision with root package name */
    private final Partner f4476a;
    private final WebView b;
    private final List<VerificationScriptResource> c;
    private final Map<String, VerificationScriptResource> d = new HashMap();
    private final String e;
    private final String f;
    private final String g;
    private final AdSessionContextType h;

    private AdSessionContext(Partner partner, WebView webView, String str, List<VerificationScriptResource> list, String str2, String str3, AdSessionContextType adSessionContextType) {
        ArrayList arrayList = new ArrayList();
        this.c = arrayList;
        this.f4476a = partner;
        this.b = webView;
        this.e = str;
        this.h = adSessionContextType;
        if (list != null) {
            arrayList.addAll(list);
            for (VerificationScriptResource put : list) {
                String uuid = UUID.randomUUID().toString();
                this.d.put(uuid, put);
            }
        }
        this.g = str2;
        this.f = str3;
    }

    public static AdSessionContext a(Partner partner, WebView webView, String str, String str2) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) webView, "WebView is null");
        if (str2 != null) {
            e.a(str2, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner, webView, (String) null, (List<VerificationScriptResource>) null, str, str2, AdSessionContextType.HTML);
    }

    public static AdSessionContext a(Partner partner, String str, List<VerificationScriptResource> list, String str2, String str3) {
        e.a((Object) partner, "Partner is null");
        e.a((Object) str, "OM SDK JS script content is null");
        e.a((Object) list, "VerificationScriptResources is null");
        if (str3 != null) {
            e.a(str3, 256, "CustomReferenceData is greater than 256 characters");
        }
        return new AdSessionContext(partner, (WebView) null, str, list, str2, str3, AdSessionContextType.NATIVE);
    }

    public AdSessionContextType a() {
        return this.h;
    }

    public String b() {
        return this.g;
    }

    public String c() {
        return this.f;
    }

    public Map<String, VerificationScriptResource> d() {
        return Collections.unmodifiableMap(this.d);
    }

    public String e() {
        return this.e;
    }

    public Partner f() {
        return this.f4476a;
    }

    public List<VerificationScriptResource> g() {
        return Collections.unmodifiableList(this.c);
    }

    public WebView h() {
        return this.b;
    }
}
