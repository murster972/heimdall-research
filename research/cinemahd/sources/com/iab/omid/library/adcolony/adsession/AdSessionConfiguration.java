package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.e;
import org.json.JSONObject;

public class AdSessionConfiguration {

    /* renamed from: a  reason: collision with root package name */
    private final Owner f4475a;
    private final Owner b;
    private final boolean c;
    private final CreativeType d;
    private final ImpressionType e;

    private AdSessionConfiguration(CreativeType creativeType, ImpressionType impressionType, Owner owner, Owner owner2, boolean z) {
        this.d = creativeType;
        this.e = impressionType;
        this.f4475a = owner;
        if (owner2 == null) {
            this.b = Owner.NONE;
        } else {
            this.b = owner2;
        }
        this.c = z;
    }

    public static AdSessionConfiguration a(CreativeType creativeType, ImpressionType impressionType, Owner owner, Owner owner2, boolean z) {
        e.a((Object) creativeType, "CreativeType is null");
        e.a((Object) impressionType, "ImpressionType is null");
        e.a((Object) owner, "Impression owner is null");
        e.a(owner, creativeType, impressionType);
        return new AdSessionConfiguration(creativeType, impressionType, owner, owner2, z);
    }

    public boolean a() {
        return Owner.NATIVE == this.f4475a;
    }

    public boolean b() {
        return Owner.NATIVE == this.b;
    }

    public JSONObject c() {
        JSONObject jSONObject = new JSONObject();
        b.a(jSONObject, "impressionOwner", this.f4475a);
        b.a(jSONObject, "mediaEventsOwner", this.b);
        b.a(jSONObject, "creativeType", this.d);
        b.a(jSONObject, "impressionType", this.e);
        b.a(jSONObject, "isolateVerificationScripts", Boolean.valueOf(this.c));
        return jSONObject;
    }
}
