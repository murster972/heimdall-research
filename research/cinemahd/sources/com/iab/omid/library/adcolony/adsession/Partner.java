package com.iab.omid.library.adcolony.adsession;

import com.iab.omid.library.adcolony.d.e;

public class Partner {

    /* renamed from: a  reason: collision with root package name */
    private final String f4482a;
    private final String b;

    private Partner(String str, String str2) {
        this.f4482a = str;
        this.b = str2;
    }

    public static Partner a(String str, String str2) {
        e.a(str, "Name is null or empty");
        e.a(str2, "Version is null or empty");
        return new Partner(str, str2);
    }

    public String a() {
        return this.f4482a;
    }

    public String b() {
        return this.b;
    }
}
