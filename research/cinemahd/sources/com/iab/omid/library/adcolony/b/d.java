package com.iab.omid.library.adcolony.b;

import android.annotation.SuppressLint;
import android.content.Context;

public class d {
    @SuppressLint({"StaticFieldLeak"})
    private static d b = new d();

    /* renamed from: a  reason: collision with root package name */
    private Context f4493a;

    private d() {
    }

    public static d b() {
        return b;
    }

    public Context a() {
        return this.f4493a;
    }

    public void a(Context context) {
        this.f4493a = context != null ? context.getApplicationContext() : null;
    }
}
