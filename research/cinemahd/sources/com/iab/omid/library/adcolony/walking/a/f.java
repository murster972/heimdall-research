package com.iab.omid.library.adcolony.walking.a;

import android.text.TextUtils;
import com.iab.omid.library.adcolony.b.a;
import com.iab.omid.library.adcolony.walking.a.b;
import java.util.HashSet;
import org.json.JSONObject;

public class f extends a {
    public f(b.C0056b bVar, HashSet<String> hashSet, JSONObject jSONObject, long j) {
        super(bVar, hashSet, jSONObject, j);
    }

    private void b(String str) {
        a d = a.d();
        if (d != null) {
            for (com.iab.omid.library.adcolony.adsession.a next : d.a()) {
                if (this.c.contains(next.b())) {
                    next.k().a(str, this.e);
                }
            }
        }
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public String doInBackground(Object... objArr) {
        if (com.iab.omid.library.adcolony.d.b.b(this.d, this.b.b())) {
            return null;
        }
        this.b.a(this.d);
        return this.d.toString();
    }

    /* access modifiers changed from: protected */
    /* renamed from: a */
    public void onPostExecute(String str) {
        if (!TextUtils.isEmpty(str)) {
            b(str);
        }
        super.onPostExecute(str);
    }
}
