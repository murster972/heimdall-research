package com.iab.omid.library.adcolony.adsession;

public enum ImpressionType {
    DEFINED_BY_JAVASCRIPT("definedByJavaScript"),
    BEGIN_TO_RENDER("beginToRender");
    

    /* renamed from: a  reason: collision with root package name */
    private final String f4480a;

    private ImpressionType(String str) {
        this.f4480a = str;
    }

    public String toString() {
        return this.f4480a;
    }
}
