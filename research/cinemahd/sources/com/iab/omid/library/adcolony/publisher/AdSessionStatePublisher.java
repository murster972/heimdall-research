package com.iab.omid.library.adcolony.publisher;

import android.webkit.WebView;
import com.iab.omid.library.adcolony.adsession.AdEvents;
import com.iab.omid.library.adcolony.adsession.AdSessionConfiguration;
import com.iab.omid.library.adcolony.adsession.AdSessionContext;
import com.iab.omid.library.adcolony.adsession.VerificationScriptResource;
import com.iab.omid.library.adcolony.adsession.media.MediaEvents;
import com.iab.omid.library.adcolony.b.d;
import com.iab.omid.library.adcolony.b.e;
import com.iab.omid.library.adcolony.e.b;
import org.json.JSONArray;
import org.json.JSONObject;

public abstract class AdSessionStatePublisher {

    /* renamed from: a  reason: collision with root package name */
    private b f4503a = new b((WebView) null);
    private AdEvents b;
    private MediaEvents c;
    private a d;
    private long e;

    enum a {
        AD_STATE_IDLE,
        AD_STATE_VISIBLE,
        AD_STATE_NOTVISIBLE
    }

    public AdSessionStatePublisher() {
        j();
    }

    public void a() {
    }

    public void a(float f) {
        e.a().a(h(), f);
    }

    /* access modifiers changed from: package-private */
    public void a(WebView webView) {
        this.f4503a = new b(webView);
    }

    public void a(AdEvents adEvents) {
        this.b = adEvents;
    }

    public void a(AdSessionConfiguration adSessionConfiguration) {
        e.a().a(h(), adSessionConfiguration.c());
    }

    public void a(com.iab.omid.library.adcolony.adsession.a aVar, AdSessionContext adSessionContext) {
        a(aVar, adSessionContext, (JSONObject) null);
    }

    /* access modifiers changed from: protected */
    public void a(com.iab.omid.library.adcolony.adsession.a aVar, AdSessionContext adSessionContext, JSONObject jSONObject) {
        String b2 = aVar.b();
        JSONObject jSONObject2 = new JSONObject();
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "environment", "app");
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "adSessionType", adSessionContext.a());
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "deviceInfo", com.iab.omid.library.adcolony.d.a.d());
        JSONArray jSONArray = new JSONArray();
        jSONArray.put("clid");
        jSONArray.put("vlid");
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "supports", jSONArray);
        JSONObject jSONObject3 = new JSONObject();
        com.iab.omid.library.adcolony.d.b.a(jSONObject3, "partnerName", adSessionContext.f().a());
        com.iab.omid.library.adcolony.d.b.a(jSONObject3, "partnerVersion", adSessionContext.f().b());
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "omidNativeInfo", jSONObject3);
        JSONObject jSONObject4 = new JSONObject();
        com.iab.omid.library.adcolony.d.b.a(jSONObject4, "libraryVersion", "1.3.11-Adcolony");
        com.iab.omid.library.adcolony.d.b.a(jSONObject4, "appId", d.b().a().getApplicationContext().getPackageName());
        com.iab.omid.library.adcolony.d.b.a(jSONObject2, "app", jSONObject4);
        if (adSessionContext.b() != null) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject2, "contentUrl", adSessionContext.b());
        }
        if (adSessionContext.c() != null) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject2, "customReferenceData", adSessionContext.c());
        }
        JSONObject jSONObject5 = new JSONObject();
        for (VerificationScriptResource next : adSessionContext.g()) {
            com.iab.omid.library.adcolony.d.b.a(jSONObject5, next.b(), next.c());
        }
        e.a().a(h(), b2, jSONObject2, jSONObject5, jSONObject);
    }

    public void a(MediaEvents mediaEvents) {
        this.c = mediaEvents;
    }

    public void a(String str) {
        e.a().a(h(), str, (JSONObject) null);
    }

    public void a(String str, long j) {
        if (j >= this.e) {
            this.d = a.AD_STATE_VISIBLE;
            e.a().b(h(), str);
        }
    }

    public void a(String str, JSONObject jSONObject) {
        e.a().a(h(), str, jSONObject);
    }

    public void a(JSONObject jSONObject) {
        e.a().b(h(), jSONObject);
    }

    public void a(boolean z) {
        if (e()) {
            e.a().c(h(), z ? "foregrounded" : "backgrounded");
        }
    }

    public void b() {
        this.f4503a.clear();
    }

    public void b(String str, long j) {
        a aVar;
        if (j >= this.e && this.d != (aVar = a.AD_STATE_NOTVISIBLE)) {
            this.d = aVar;
            e.a().b(h(), str);
        }
    }

    public AdEvents c() {
        return this.b;
    }

    public MediaEvents d() {
        return this.c;
    }

    public boolean e() {
        return this.f4503a.get() != null;
    }

    public void f() {
        e.a().a(h());
    }

    public void g() {
        e.a().b(h());
    }

    public WebView h() {
        return (WebView) this.f4503a.get();
    }

    public void i() {
        e.a().c(h());
    }

    public void j() {
        this.e = com.iab.omid.library.adcolony.d.d.a();
        this.d = a.AD_STATE_IDLE;
    }
}
