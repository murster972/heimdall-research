package com.iab.omid.library.adcolony.adsession.media;

public enum Position {
    PREROLL("preroll");
    

    /* renamed from: a  reason: collision with root package name */
    private final String f4487a;

    private Position(String str) {
        this.f4487a = str;
    }

    public String toString() {
        return this.f4487a;
    }
}
