package com.iab.omid.library.adcolony.adsession;

import com.facebook.react.uimanager.ViewProps;

public enum Owner {
    NATIVE("native"),
    NONE(ViewProps.NONE);
    

    /* renamed from: a  reason: collision with root package name */
    private final String f4481a;

    private Owner(String str) {
        this.f4481a = str;
    }

    public String toString() {
        return this.f4481a;
    }
}
