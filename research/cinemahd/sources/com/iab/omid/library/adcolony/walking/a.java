package com.iab.omid.library.adcolony.walking;

import android.view.View;
import android.view.ViewParent;
import com.iab.omid.library.adcolony.b.c;
import com.iab.omid.library.adcolony.d.f;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class a {

    /* renamed from: a  reason: collision with root package name */
    private final HashMap<View, String> f4508a = new HashMap<>();
    private final HashMap<View, C0055a> b = new HashMap<>();
    private final HashMap<String, View> c = new HashMap<>();
    private final HashSet<View> d = new HashSet<>();
    private final HashSet<String> e = new HashSet<>();
    private final HashSet<String> f = new HashSet<>();
    private final HashMap<String, String> g = new HashMap<>();
    private boolean h;

    /* renamed from: com.iab.omid.library.adcolony.walking.a$a  reason: collision with other inner class name */
    public static class C0055a {

        /* renamed from: a  reason: collision with root package name */
        private final c f4509a;
        private final ArrayList<String> b = new ArrayList<>();

        public C0055a(c cVar, String str) {
            this.f4509a = cVar;
            a(str);
        }

        public c a() {
            return this.f4509a;
        }

        public void a(String str) {
            this.b.add(str);
        }

        public ArrayList<String> b() {
            return this.b;
        }
    }

    private void a(com.iab.omid.library.adcolony.adsession.a aVar) {
        for (c a2 : aVar.d()) {
            a(a2, aVar);
        }
    }

    private void a(c cVar, com.iab.omid.library.adcolony.adsession.a aVar) {
        View view = (View) cVar.a().get();
        if (view != null) {
            C0055a aVar2 = this.b.get(view);
            if (aVar2 != null) {
                aVar2.a(aVar.b());
            } else {
                this.b.put(view, new C0055a(cVar, aVar.b()));
            }
        }
    }

    private String d(View view) {
        if (!view.hasWindowFocus()) {
            return "noWindowFocus";
        }
        HashSet hashSet = new HashSet();
        while (view != null) {
            String e2 = f.e(view);
            if (e2 != null) {
                return e2;
            }
            hashSet.add(view);
            ViewParent parent = view.getParent();
            view = parent instanceof View ? (View) parent : null;
        }
        this.d.addAll(hashSet);
        return null;
    }

    public String a(View view) {
        if (this.f4508a.size() == 0) {
            return null;
        }
        String str = this.f4508a.get(view);
        if (str != null) {
            this.f4508a.remove(view);
        }
        return str;
    }

    public String a(String str) {
        return this.g.get(str);
    }

    public HashSet<String> a() {
        return this.e;
    }

    public View b(String str) {
        return this.c.get(str);
    }

    public C0055a b(View view) {
        C0055a aVar = this.b.get(view);
        if (aVar != null) {
            this.b.remove(view);
        }
        return aVar;
    }

    public HashSet<String> b() {
        return this.f;
    }

    public c c(View view) {
        return this.d.contains(view) ? c.PARENT_VIEW : this.h ? c.OBSTRUCTION_VIEW : c.UNDERLYING_VIEW;
    }

    public void c() {
        com.iab.omid.library.adcolony.b.a d2 = com.iab.omid.library.adcolony.b.a.d();
        if (d2 != null) {
            for (com.iab.omid.library.adcolony.adsession.a next : d2.b()) {
                View g2 = next.g();
                if (next.h()) {
                    String b2 = next.b();
                    if (g2 != null) {
                        String d3 = d(g2);
                        if (d3 == null) {
                            this.e.add(b2);
                            this.f4508a.put(g2, b2);
                            a(next);
                        } else {
                            this.f.add(b2);
                            this.c.put(b2, g2);
                            this.g.put(b2, d3);
                        }
                    } else {
                        this.f.add(b2);
                        this.g.put(b2, "noAdView");
                    }
                }
            }
        }
    }

    public void d() {
        this.f4508a.clear();
        this.b.clear();
        this.c.clear();
        this.d.clear();
        this.e.clear();
        this.f.clear();
        this.g.clear();
        this.h = false;
    }

    public void e() {
        this.h = true;
    }
}
