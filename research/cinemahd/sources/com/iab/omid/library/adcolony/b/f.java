package com.iab.omid.library.adcolony.b;

import android.content.Context;
import android.os.Handler;
import com.iab.omid.library.adcolony.a.c;
import com.iab.omid.library.adcolony.a.d;
import com.iab.omid.library.adcolony.a.e;
import com.iab.omid.library.adcolony.adsession.a;
import com.iab.omid.library.adcolony.b.b;
import com.iab.omid.library.adcolony.walking.TreeWalker;

public class f implements c, b.a {
    private static f f;

    /* renamed from: a  reason: collision with root package name */
    private float f4496a = 0.0f;
    private final e b;
    private final com.iab.omid.library.adcolony.a.b c;
    private d d;
    private a e;

    public f(e eVar, com.iab.omid.library.adcolony.a.b bVar) {
        this.b = eVar;
        this.c = bVar;
    }

    public static f d() {
        if (f == null) {
            f = new f(new e(), new com.iab.omid.library.adcolony.a.b());
        }
        return f;
    }

    private a e() {
        if (this.e == null) {
            this.e = a.d();
        }
        return this.e;
    }

    public void a() {
        b.d().a((b.a) this);
        b.d().a();
        if (b.d().c()) {
            TreeWalker.h().a();
        }
        this.d.a();
    }

    public void a(float f2) {
        this.f4496a = f2;
        for (a k : e().b()) {
            k.k().a(f2);
        }
    }

    public void a(Context context) {
        this.d = this.b.a(new Handler(), context, this.c.a(), this);
    }

    public void b() {
        TreeWalker.h().b();
        b.d().b();
        this.d.b();
    }

    public float c() {
        return this.f4496a;
    }
}
