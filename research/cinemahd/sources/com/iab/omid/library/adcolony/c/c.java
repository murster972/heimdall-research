package com.iab.omid.library.adcolony.c;

import android.view.View;
import com.iab.omid.library.adcolony.b.a;
import com.iab.omid.library.adcolony.c.a;
import com.iab.omid.library.adcolony.d.b;
import com.iab.omid.library.adcolony.d.f;
import java.util.ArrayList;
import java.util.Collection;
import java.util.IdentityHashMap;
import java.util.Iterator;
import org.json.JSONObject;

public class c implements a {

    /* renamed from: a  reason: collision with root package name */
    private final a f4499a;

    public c(a aVar) {
        this.f4499a = aVar;
    }

    /* access modifiers changed from: package-private */
    public ArrayList<View> a() {
        View rootView;
        ArrayList<View> arrayList = new ArrayList<>();
        a d = a.d();
        if (d != null) {
            Collection<com.iab.omid.library.adcolony.adsession.a> b = d.b();
            IdentityHashMap identityHashMap = new IdentityHashMap((b.size() * 2) + 3);
            for (com.iab.omid.library.adcolony.adsession.a g : b) {
                View g2 = g.g();
                if (g2 != null && f.c(g2) && (rootView = g2.getRootView()) != null && !identityHashMap.containsKey(rootView)) {
                    identityHashMap.put(rootView, rootView);
                    float a2 = f.a(rootView);
                    int size = arrayList.size();
                    while (size > 0 && f.a(arrayList.get(size - 1)) > a2) {
                        size--;
                    }
                    arrayList.add(size, rootView);
                }
            }
        }
        return arrayList;
    }

    public JSONObject a(View view) {
        return b.a(0, 0, 0, 0);
    }

    public void a(View view, JSONObject jSONObject, a.C0054a aVar, boolean z) {
        Iterator<View> it2 = a().iterator();
        while (it2.hasNext()) {
            aVar.a(it2.next(), this.f4499a, jSONObject);
        }
    }
}
