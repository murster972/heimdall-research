package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class c {
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public static boolean f36 = false;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Application f37 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static boolean f38 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    static WeakReference<Activity> f39;
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public static int f40;

    static class a implements Application.ActivityLifecycleCallbacks {
        a() {
        }

        /* renamed from: ॱ  reason: contains not printable characters */
        private static void m207(boolean z) {
            if (z) {
                a.m182(3, "ActivityState", (Object) null, "App became visible");
                if (t.m350().f182 == t.a.f193 && !((f) MoatAnalytics.getInstance()).f58) {
                    n.m292().m302();
                    return;
                }
                return;
            }
            a.m182(3, "ActivityState", (Object) null, "App became invisible");
            if (t.m350().f182 == t.a.f193 && !((f) MoatAnalytics.getInstance()).f58) {
                n.m292().m303();
            }
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            int unused = c.f40 = 1;
        }

        public final void onActivityDestroyed(Activity activity) {
            try {
                if (!(c.f40 == 3 || c.f40 == 5)) {
                    if (c.f36) {
                        m207(false);
                    }
                    boolean unused = c.f36 = false;
                }
                int unused2 = c.f40 = 6;
                a.m182(3, "ActivityState", this, "Activity destroyed: " + activity.getClass() + "@" + activity.hashCode());
                if (c.m201(activity)) {
                    c.f39 = new WeakReference<>((Object) null);
                }
            } catch (Exception e) {
                o.m306(e);
            }
        }

        public final void onActivityPaused(Activity activity) {
            try {
                int unused = c.f40 = 4;
                if (c.m201(activity)) {
                    c.f39 = new WeakReference<>((Object) null);
                }
                a.m182(3, "ActivityState", this, "Activity paused: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m306(e);
            }
        }

        public final void onActivityResumed(Activity activity) {
            try {
                c.f39 = new WeakReference<>(activity);
                int unused = c.f40 = 3;
                t.m350().m356();
                a.m182(3, "ActivityState", this, "Activity resumed: " + activity.getClass() + "@" + activity.hashCode());
                if (((f) MoatAnalytics.getInstance()).f60) {
                    e.m219(activity);
                }
            } catch (Exception e) {
                o.m306(e);
            }
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public final void onActivityStarted(Activity activity) {
            try {
                c.f39 = new WeakReference<>(activity);
                int unused = c.f40 = 2;
                if (!c.f36) {
                    m207(true);
                }
                boolean unused2 = c.f36 = true;
                a.m182(3, "ActivityState", this, "Activity started: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m306(e);
            }
        }

        public final void onActivityStopped(Activity activity) {
            try {
                if (c.f40 != 3) {
                    boolean unused = c.f36 = false;
                    m207(false);
                }
                int unused2 = c.f40 = 5;
                if (c.m201(activity)) {
                    c.f39 = new WeakReference<>((Object) null);
                }
                a.m182(3, "ActivityState", this, "Activity stopped: " + activity.getClass() + "@" + activity.hashCode());
            } catch (Exception e) {
                o.m306(e);
            }
        }
    }

    c() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Application m203() {
        return f37;
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static /* synthetic */ boolean m201(Activity activity) {
        WeakReference<Activity> weakReference = f39;
        return weakReference != null && weakReference.get() == activity;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m205(Application application) {
        f37 = application;
        if (!f38) {
            f38 = true;
            f37.registerActivityLifecycleCallbacks(new a());
        }
    }
}
