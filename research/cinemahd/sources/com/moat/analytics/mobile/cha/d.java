package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.j;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

abstract class d {

    /* renamed from: ʻ  reason: contains not printable characters */
    private WeakReference<View> f41;

    /* renamed from: ʼ  reason: contains not printable characters */
    private final boolean f42;

    /* renamed from: ʽ  reason: contains not printable characters */
    final boolean f43;

    /* renamed from: ˊ  reason: contains not printable characters */
    TrackerListener f44;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f45;

    /* renamed from: ˋ  reason: contains not printable characters */
    final String f46;

    /* renamed from: ˎ  reason: contains not printable characters */
    j f47;

    /* renamed from: ˏ  reason: contains not printable characters */
    WeakReference<WebView> f48;

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean f49;

    /* renamed from: ॱ  reason: contains not printable characters */
    o f50 = null;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final u f51;

    d(View view, boolean z, boolean z2) {
        String str;
        a.m182(3, "BaseTracker", this, "Initializing.");
        if (z) {
            str = "m" + hashCode();
        } else {
            str = "";
        }
        this.f46 = str;
        this.f41 = new WeakReference<>(view);
        this.f42 = z;
        this.f43 = z2;
        this.f45 = false;
        this.f49 = false;
        this.f51 = new u();
    }

    public void changeTargetView(View view) {
        a.m182(3, "BaseTracker", this, "changing view to " + a.m181(view));
        this.f41 = new WeakReference<>(view);
    }

    public void removeListener() {
        this.f44 = null;
    }

    @Deprecated
    public void setActivity(Activity activity) {
    }

    public void setListener(TrackerListener trackerListener) {
        this.f44 = trackerListener;
    }

    public void startTracking() {
        try {
            a.m182(3, "BaseTracker", this, "In startTracking method.");
            m215();
            if (this.f44 != null) {
                this.f44.onTrackingStarted("Tracking started on " + a.m181((View) this.f41.get()));
            }
            String str = "startTracking succeeded for " + a.m181((View) this.f41.get());
            a.m182(3, "BaseTracker", this, str);
            a.m179("[SUCCESS] ", m212() + " " + str);
        } catch (Exception e) {
            m218("startTracking", e);
        }
    }

    public void stopTracking() {
        boolean z = false;
        try {
            a.m182(3, "BaseTracker", this, "In stopTracking method.");
            this.f49 = true;
            if (this.f47 != null) {
                this.f47.m272(this);
                z = true;
            }
        } catch (Exception e) {
            o.m306(e);
        }
        StringBuilder sb = new StringBuilder("Attempt to stop tracking ad impression was ");
        sb.append(z ? "" : "un");
        sb.append("successful.");
        a.m182(3, "BaseTracker", this, sb.toString());
        String str = z ? "[SUCCESS] " : "[ERROR] ";
        StringBuilder sb2 = new StringBuilder();
        sb2.append(m212());
        sb2.append(" stopTracking ");
        sb2.append(z ? "succeeded" : "failed");
        sb2.append(" for ");
        sb2.append(a.m181((View) this.f41.get()));
        a.m179(str, sb2.toString());
        TrackerListener trackerListener = this.f44;
        if (trackerListener != null) {
            trackerListener.onTrackingStopped("");
            this.f44 = null;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻ  reason: contains not printable characters */
    public final String m208() {
        return a.m181((View) this.f41.get());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʼ  reason: contains not printable characters */
    public final View m209() {
        return (View) this.f41.get();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʽ  reason: contains not printable characters */
    public final String m210() {
        this.f51.m372(this.f46, (View) this.f41.get());
        return this.f51.f213;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m211() {
        return this.f45 && !this.f49;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public abstract String m212();

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m213(List<String> list) throws o {
        if (((View) this.f41.get()) == null && !this.f43) {
            list.add("Tracker's target view is null");
        }
        if (!list.isEmpty()) {
            throw new o(TextUtils.join(" and ", list));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m214() throws o {
        if (this.f50 != null) {
            throw new o("Tracker initialization failed: " + this.f50.getMessage());
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m215() throws o {
        a.m182(3, "BaseTracker", this, "Attempting to start impression.");
        m214();
        if (this.f45) {
            throw new o("Tracker already started");
        } else if (!this.f49) {
            m213(new ArrayList());
            j jVar = this.f47;
            if (jVar != null) {
                jVar.m273(this);
                this.f45 = true;
                a.m182(3, "BaseTracker", this, "Impression started.");
                return;
            }
            a.m182(3, "BaseTracker", this, "Bridge is null, won't start tracking");
            throw new o("Bridge is null");
        } else {
            throw new o("Tracker already stopped");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m217(WebView webView) throws o {
        if (webView != null) {
            this.f48 = new WeakReference<>(webView);
            if (this.f47 == null) {
                if (!(this.f42 || this.f43)) {
                    a.m182(3, "BaseTracker", this, "Attempting bridge installation.");
                    if (this.f48.get() != null) {
                        this.f47 = new j((WebView) this.f48.get(), j.e.f120);
                        a.m182(3, "BaseTracker", this, "Bridge installed.");
                    } else {
                        this.f47 = null;
                        a.m182(3, "BaseTracker", this, "Bridge not installed, WebView is null.");
                    }
                }
            }
            j jVar = this.f47;
            if (jVar != null) {
                jVar.m271(this);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m218(String str, Exception exc) {
        try {
            o.m306(exc);
            String r3 = o.m305(str, exc);
            if (this.f44 != null) {
                this.f44.onTrackingFailedToStart(r3);
            }
            a.m182(3, "BaseTracker", this, r3);
            a.m179("[ERROR] ", m212() + " " + r3);
        } catch (Exception unused) {
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m216() throws o {
        if (this.f45) {
            throw new o("Tracker already started");
        } else if (this.f49) {
            throw new o("Tracker already stopped");
        }
    }
}
