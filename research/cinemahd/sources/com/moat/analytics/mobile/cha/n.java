package com.moat.analytics.mobile.cha;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import androidx.core.content.ContextCompat;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class n implements LocationListener {

    /* renamed from: ˎ  reason: contains not printable characters */
    private static n f131;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Location f132;

    /* renamed from: ˊ  reason: contains not printable characters */
    private ScheduledExecutorService f133;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private boolean f134;

    /* renamed from: ˋ  reason: contains not printable characters */
    private ScheduledFuture<?> f135;

    /* renamed from: ˏ  reason: contains not printable characters */
    private ScheduledFuture<?> f136;

    /* renamed from: ॱ  reason: contains not printable characters */
    private LocationManager f137;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private boolean f138;

    private n() {
        try {
            this.f134 = ((f) MoatAnalytics.getInstance()).f58;
            if (this.f134) {
                a.m182(3, "LocationManager", this, "Moat location services disabled");
                return;
            }
            this.f133 = Executors.newScheduledThreadPool(1);
            this.f137 = (LocationManager) c.m203().getSystemService("location");
            if (this.f137.getAllProviders().size() == 0) {
                a.m182(3, "LocationManager", this, "Device has no location providers");
            } else {
                m290();
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    private void m285() {
        try {
            if (!this.f138) {
                a.m182(3, "LocationManager", this, "Attempting to start update");
                if (m294()) {
                    a.m182(3, "LocationManager", this, "start updating gps location");
                    this.f137.requestLocationUpdates("gps", 0, 0.0f, this, Looper.getMainLooper());
                    this.f138 = true;
                }
                if (m299()) {
                    a.m182(3, "LocationManager", this, "start updating network location");
                    this.f137.requestLocationUpdates("network", 0, 0.0f, this, Looper.getMainLooper());
                    this.f138 = true;
                }
                if (this.f138) {
                    m289();
                    this.f136 = this.f133.schedule(new Runnable() {
                        public final void run() {
                            try {
                                a.m182(3, "LocationManager", this, "fetchTimedOut");
                                n.this.m297(true);
                            } catch (Exception e) {
                                o.m306(e);
                            }
                        }
                    }, 60, TimeUnit.SECONDS);
                }
            }
        } catch (SecurityException e) {
            o.m306(e);
        }
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private void m286() {
        try {
            a.m182(3, "LocationManager", this, "Stopping to update location");
            boolean z = true;
            if (!(ContextCompat.a(c.m203().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0)) {
                if (!(ContextCompat.a(c.m203().getApplicationContext(), "android.permission.ACCESS_COARSE_LOCATION") == 0)) {
                    z = false;
                }
            }
            if (z && this.f137 != null) {
                this.f137.removeUpdates(this);
                this.f138 = false;
            }
        } catch (SecurityException e) {
            o.m306(e);
        }
    }

    /* renamed from: ʽ  reason: contains not printable characters */
    private Location m287() {
        try {
            boolean r1 = m294();
            boolean r2 = m299();
            if (r1 && r2) {
                return m295(this.f137.getLastKnownLocation("gps"), this.f137.getLastKnownLocation("network"));
            }
            if (r1) {
                return this.f137.getLastKnownLocation("gps");
            }
            if (r2) {
                return this.f137.getLastKnownLocation("network");
            }
            return null;
        } catch (SecurityException e) {
            o.m306(e);
            return null;
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private void m289() {
        ScheduledFuture<?> scheduledFuture = this.f136;
        if (scheduledFuture != null && !scheduledFuture.isCancelled()) {
            this.f136.cancel(true);
            this.f136 = null;
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m290() {
        try {
            if (this.f134) {
                return;
            }
            if (this.f137 != null) {
                if (this.f138) {
                    a.m182(3, "LocationManager", this, "already updating location");
                }
                a.m182(3, "LocationManager", this, "starting location fetch");
                this.f132 = m295(this.f132, m287());
                if (this.f132 != null) {
                    a.m182(3, "LocationManager", this, "Have a valid location, won't fetch = " + this.f132.toString());
                    m293();
                    return;
                }
                m285();
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static n m292() {
        if (f131 == null) {
            f131 = new n();
        }
        return f131;
    }

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private void m293() {
        a.m182(3, "LocationManager", this, "Resetting fetch timer");
        m300();
        Location location = this.f132;
        float f = 600.0f;
        if (location != null) {
            f = Math.max(600.0f - ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)), 0.0f);
        }
        this.f135 = this.f133.schedule(new Runnable() {
            public final void run() {
                try {
                    a.m182(3, "LocationManager", this, "fetchTimerCompleted");
                    n.this.m290();
                } catch (Exception e) {
                    o.m306(e);
                }
            }
        }, (long) f, TimeUnit.SECONDS);
    }

    /* renamed from: ͺ  reason: contains not printable characters */
    private boolean m294() {
        return (ContextCompat.a(c.m203().getApplicationContext(), "android.permission.ACCESS_FINE_LOCATION") == 0) && this.f137.getProvider("gps") != null && this.f137.isProviderEnabled("gps");
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0044 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x0045 A[RETURN] */
    /* renamed from: ॱˊ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean m299() {
        /*
            r4 = this;
            android.app.Application r0 = com.moat.analytics.mobile.cha.c.m203()
            android.content.Context r0 = r0.getApplicationContext()
            java.lang.String r1 = "android.permission.ACCESS_FINE_LOCATION"
            int r0 = androidx.core.content.ContextCompat.a((android.content.Context) r0, (java.lang.String) r1)
            r1 = 1
            r2 = 0
            if (r0 != 0) goto L_0x0014
            r0 = 1
            goto L_0x0015
        L_0x0014:
            r0 = 0
        L_0x0015:
            if (r0 != 0) goto L_0x002f
            android.app.Application r0 = com.moat.analytics.mobile.cha.c.m203()
            android.content.Context r0 = r0.getApplicationContext()
            java.lang.String r3 = "android.permission.ACCESS_COARSE_LOCATION"
            int r0 = androidx.core.content.ContextCompat.a((android.content.Context) r0, (java.lang.String) r3)
            if (r0 != 0) goto L_0x0029
            r0 = 1
            goto L_0x002a
        L_0x0029:
            r0 = 0
        L_0x002a:
            if (r0 == 0) goto L_0x002d
            goto L_0x002f
        L_0x002d:
            r0 = 0
            goto L_0x0030
        L_0x002f:
            r0 = 1
        L_0x0030:
            if (r0 == 0) goto L_0x0045
            android.location.LocationManager r0 = r4.f137
            java.lang.String r3 = "network"
            android.location.LocationProvider r0 = r0.getProvider(r3)
            if (r0 == 0) goto L_0x0045
            android.location.LocationManager r0 = r4.f137
            boolean r0 = r0.isProviderEnabled(r3)
            if (r0 == 0) goto L_0x0045
            return r1
        L_0x0045:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.n.m299():boolean");
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private void m300() {
        ScheduledFuture<?> scheduledFuture = this.f135;
        if (scheduledFuture != null && !scheduledFuture.isCancelled()) {
            this.f135.cancel(true);
            this.f135 = null;
        }
    }

    public final void onLocationChanged(Location location) {
        try {
            a.m182(3, "LocationManager", this, "Received an updated location = " + location.toString());
            float currentTimeMillis = (float) ((System.currentTimeMillis() - location.getTime()) / 1000);
            if (location.hasAccuracy() && location.getAccuracy() <= 100.0f && currentTimeMillis < 600.0f) {
                this.f132 = m295(this.f132, location);
                a.m182(3, "LocationManager", this, "fetchCompleted");
                m297(true);
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }

    public final void onProviderDisabled(String str) {
    }

    public final void onProviderEnabled(String str) {
    }

    public final void onStatusChanged(String str, int i, Bundle bundle) {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m302() {
        m290();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final Location m301() {
        if (this.f134 || this.f137 == null) {
            return null;
        }
        return this.f132;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m303() {
        m297(false);
    }

    /* access modifiers changed from: private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public void m297(boolean z) {
        try {
            a.m182(3, "LocationManager", this, "stopping location fetch");
            m286();
            m289();
            if (z) {
                m293();
            } else {
                m300();
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Location m295(Location location, Location location2) {
        boolean r0 = m298(location);
        boolean r1 = m298(location2);
        if (r0) {
            return (r1 && location.getAccuracy() >= location.getAccuracy()) ? location2 : location;
        }
        if (!r1) {
            return null;
        }
        return location2;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static boolean m291(Location location, Location location2) {
        if (location == location2) {
            return true;
        }
        return (location == null || location2 == null || location.getTime() != location2.getTime()) ? false : true;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    private static boolean m298(Location location) {
        if (location == null) {
            return false;
        }
        if ((location.getLatitude() != 0.0d || location.getLongitude() != 0.0d) && location.getAccuracy() >= 0.0f && ((float) ((System.currentTimeMillis() - location.getTime()) / 1000)) < 600.0f) {
            return true;
        }
        return false;
    }
}
