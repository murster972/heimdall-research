package com.moat.analytics.mobile.cha;

import android.app.Application;
import com.moat.analytics.mobile.cha.NoOp;

public abstract class MoatAnalytics {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static MoatAnalytics f9;

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f9 == null) {
                try {
                    f9 = new f();
                } catch (Exception e) {
                    o.m306(e);
                    f9 = new NoOp.MoatAnalytics();
                }
            }
            moatAnalytics = f9;
        }
        return moatAnalytics;
    }

    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);
}
