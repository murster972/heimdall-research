package com.moat.analytics.mobile.cha;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.NoOp;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.p;
import java.lang.ref.WeakReference;
import java.util.Map;

final class k extends MoatFactory {
    k() throws o {
        if (!((f) MoatAnalytics.getInstance()).m221()) {
            String str = "Failed to initialize MoatFactory" + ", SDK was not started";
            a.m182(3, "Factory", this, str);
            a.m179("[ERROR] ", str);
            throw new o("Failed to initialize MoatFactory");
        }
    }

    public final <T> T createCustomTracker(l<T> lVar) {
        try {
            return lVar.create();
        } catch (Exception e) {
            o.m306(e);
            return lVar.createNoOp();
        }
    }

    public final NativeDisplayTracker createNativeDisplayTracker(View view, final Map<String, String> map) {
        try {
            final WeakReference weakReference = new WeakReference(view);
            return (NativeDisplayTracker) p.m309(new p.c<NativeDisplayTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<NativeDisplayTracker> m279() {
                    View view = (View) weakReference.get();
                    String str = "Attempting to create NativeDisplayTracker for " + a.m181(view);
                    a.m182(3, "Factory", this, str);
                    a.m179("[INFO] ", str);
                    return Optional.of(new q(view, map));
                }
            }, NativeDisplayTracker.class);
        } catch (Exception e) {
            o.m306(e);
            return new NoOp.c();
        }
    }

    public final NativeVideoTracker createNativeVideoTracker(final String str) {
        try {
            return (NativeVideoTracker) p.m309(new p.c<NativeVideoTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<NativeVideoTracker> m280() {
                    a.m182(3, "Factory", this, "Attempting to create NativeVideoTracker");
                    a.m179("[INFO] ", "Attempting to create NativeVideoTracker");
                    return Optional.of(new s(str));
                }
            }, NativeVideoTracker.class);
        } catch (Exception e) {
            o.m306(e);
            return new NoOp.b();
        }
    }

    public final WebAdTracker createWebAdTracker(WebView webView) {
        try {
            final WeakReference weakReference = new WeakReference(webView);
            return (WebAdTracker) p.m309(new p.c<WebAdTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<WebAdTracker> m282() {
                    WebView webView = (WebView) weakReference.get();
                    String str = "Attempting to create WebAdTracker for " + a.m181(webView);
                    a.m182(3, "Factory", this, str);
                    a.m179("[INFO] ", str);
                    return Optional.of(new v(webView));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m306(e);
            return new NoOp.e();
        }
    }

    public final WebAdTracker createWebAdTracker(ViewGroup viewGroup) {
        try {
            final WeakReference weakReference = new WeakReference(viewGroup);
            return (WebAdTracker) p.m309(new p.c<WebAdTracker>() {
                /* renamed from: ˋ  reason: contains not printable characters */
                public final Optional<WebAdTracker> m281() throws o {
                    ViewGroup viewGroup = (ViewGroup) weakReference.get();
                    String str = "Attempting to create WebAdTracker for adContainer " + a.m181(viewGroup);
                    a.m182(3, "Factory", this, str);
                    a.m179("[INFO] ", str);
                    return Optional.of(new v(viewGroup));
                }
            }, WebAdTracker.class);
        } catch (Exception e) {
            o.m306(e);
            return new NoOp.e();
        }
    }
}
