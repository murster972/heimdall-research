package com.moat.analytics.mobile.cha;

import android.view.View;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class w extends b implements ReactiveVideoTracker {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private Integer f223;

    public w(String str) {
        super(str);
        a.m182(3, "ReactiveVideoTracker", this, "Initializing.");
        a.m179("[SUCCESS] ", "ReactiveVideoTracker created");
    }

    public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
        try {
            m214();
            m216();
            this.f223 = num;
            return super.m197(map, view);
        } catch (Exception e) {
            m218("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m374() {
        return "ReactiveVideoTracker";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m375(List<String> list) throws o {
        if (this.f223.intValue() >= 1000) {
            super.m192(list);
            return;
        }
        throw new o(String.format(Locale.ROOT, "Invalid duration = %d. Please make sure duration is in milliseconds.", new Object[]{this.f223}));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m376(MoatAdEvent moatAdEvent) {
        if (moatAdEvent.f5 == MoatAdEventType.AD_EVT_COMPLETE && !moatAdEvent.f4.equals(MoatAdEvent.f0) && !b.m189(moatAdEvent.f4, this.f223)) {
            moatAdEvent.f5 = MoatAdEventType.AD_EVT_STOPPED;
        }
        return super.m194(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m377() throws o {
        Integer num;
        HashMap hashMap = new HashMap();
        View view = (View) this.f25.get();
        int i = 0;
        if (view != null) {
            i = Integer.valueOf(view.getWidth());
            num = Integer.valueOf(view.getHeight());
        } else {
            num = 0;
        }
        hashMap.put("duration", this.f223);
        hashMap.put("width", i);
        hashMap.put("height", num);
        return hashMap;
    }
}
