package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.text.TextUtils;
import android.view.View;
import com.moat.analytics.mobile.cha.a;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

abstract class b extends d {

    /* renamed from: ʻ  reason: contains not printable characters */
    static final MoatAdEventType[] f22 = {MoatAdEventType.AD_EVT_FIRST_QUARTILE, MoatAdEventType.AD_EVT_MID_POINT, MoatAdEventType.AD_EVT_THIRD_QUARTILE};

    /* renamed from: ʼ  reason: contains not printable characters */
    final Map<MoatAdEventType, Integer> f23;

    /* renamed from: ʼॱ  reason: contains not printable characters */
    private final String f24;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    WeakReference<View> f25;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private boolean f26;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private Map<String, String> f27;

    /* renamed from: ͺ  reason: contains not printable characters */
    private Double f28;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public VideoTrackerListener f29;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private final Set<MoatAdEventType> f30;
    /* access modifiers changed from: private */

    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final a f31 = new a(c.m203(), a.d.f20);

    /* renamed from: ᐝ  reason: contains not printable characters */
    final Handler f32;

    b(String str) {
        super((View) null, false, true);
        a.m182(3, "BaseVideoTracker", this, "Initializing.");
        this.f24 = str;
        a aVar = this.f31;
        this.f47 = aVar.f16;
        try {
            super.m217(aVar.f14);
        } catch (o e) {
            this.f50 = e;
        }
        this.f23 = new HashMap();
        this.f30 = new HashSet();
        this.f32 = new Handler();
        this.f26 = false;
        this.f28 = Double.valueOf(1.0d);
    }

    public void changeTargetView(View view) {
        a.m182(3, "BaseVideoTracker", this, "changing view to " + a.m181(view));
        this.f25 = new WeakReference<>(view);
        try {
            super.changeTargetView(view);
        } catch (Exception e) {
            o.m306(e);
        }
    }

    public void dispatchEvent(MoatAdEvent moatAdEvent) {
        try {
            JSONObject r0 = m194(moatAdEvent);
            boolean z = false;
            a.m182(3, "BaseVideoTracker", this, String.format("Received event: %s", new Object[]{r0.toString()}));
            a.m179("[SUCCESS] ", m212() + String.format(" Received event: %s", new Object[]{r0.toString()}));
            if (m211() && this.f47 != null) {
                this.f47.m278(this.f31.f13, r0);
                if (!this.f30.contains(moatAdEvent.f5)) {
                    this.f30.add(moatAdEvent.f5);
                    if (this.f29 != null) {
                        this.f29.onVideoEventReported(moatAdEvent.f5);
                    }
                }
            }
            MoatAdEventType moatAdEventType = moatAdEvent.f5;
            if (moatAdEventType == MoatAdEventType.AD_EVT_COMPLETE || moatAdEventType == MoatAdEventType.AD_EVT_STOPPED || moatAdEventType == MoatAdEventType.AD_EVT_SKIPPED) {
                z = true;
            }
            if (z) {
                this.f23.put(moatAdEventType, 1);
                if (this.f47 != null) {
                    this.f47.m272((d) this);
                }
                m196();
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }

    public void removeVideoListener() {
        this.f29 = null;
    }

    public void setPlayerVolume(Double d) {
        Double valueOf = Double.valueOf(this.f28.doubleValue() * r.m330());
        if (!d.equals(this.f28)) {
            a.m182(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "player volume changed to %f ", new Object[]{d}));
            this.f28 = d;
            if (!valueOf.equals(Double.valueOf(this.f28.doubleValue() * r.m330()))) {
                dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_VOLUME_CHANGE, MoatAdEvent.f0, this.f28));
            }
        }
    }

    public void setVideoListener(VideoTrackerListener videoTrackerListener) {
        this.f29 = videoTrackerListener;
    }

    public void stopTracking() {
        try {
            super.stopTracking();
            m196();
            if (this.f29 != null) {
                this.f29 = null;
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊॱ  reason: contains not printable characters */
    public final Double m191() {
        return Double.valueOf(this.f28.doubleValue() * r.m330());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public void m192(List<String> list) throws o {
        if (this.f27 == null) {
            list.add("Null adIds object");
        }
        if (list.isEmpty()) {
            super.m213(list);
            return;
        }
        throw new o(TextUtils.join(" and ", list));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋॱ  reason: contains not printable characters */
    public final boolean m193() {
        return this.f23.containsKey(MoatAdEventType.AD_EVT_COMPLETE) || this.f23.containsKey(MoatAdEventType.AD_EVT_STOPPED) || this.f23.containsKey(MoatAdEventType.AD_EVT_SKIPPED);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public JSONObject m194(MoatAdEvent moatAdEvent) {
        if (Double.isNaN(moatAdEvent.f3.doubleValue())) {
            moatAdEvent.f3 = this.f28;
        }
        return new JSONObject(moatAdEvent.m176());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏॱ  reason: contains not printable characters */
    public final void m196() {
        if (!this.f26) {
            this.f26 = true;
            this.f32.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        a.m182(3, "BaseVideoTracker", this, "Shutting down.");
                        a r0 = b.this.f31;
                        a.m182(3, "GlobalWebView", r0, "Cleaning up");
                        r0.f16.m270();
                        r0.f16 = null;
                        r0.f14.destroy();
                        r0.f14 = null;
                        VideoTrackerListener unused = b.this.f29 = null;
                    } catch (Exception e) {
                        o.m306(e);
                    }
                }
            }, 500);
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    public boolean m197(Map<String, String> map, View view) {
        try {
            m214();
            m216();
            if (view == null) {
                a.m182(3, "BaseVideoTracker", this, "trackVideoAd received null video view instance");
            }
            this.f27 = map;
            this.f25 = new WeakReference<>(view);
            m195();
            String format = String.format("trackVideoAd tracking ids: %s | view: %s", new Object[]{new JSONObject(map).toString(), a.m181(view)});
            a.m182(3, "BaseVideoTracker", this, format);
            a.m179("[SUCCESS] ", m212() + " " + format);
            if (this.f44 != null) {
                this.f44.onTrackingStarted(m208());
            }
            return true;
        } catch (Exception e) {
            m218("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˊ  reason: contains not printable characters */
    public final Double m198() {
        return this.f28;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public abstract Map<String, Object> m199() throws o;

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final void m195() throws o {
        super.changeTargetView((View) this.f25.get());
        super.m215();
        Map<String, Object> r0 = m199();
        Integer num = (Integer) r0.get("width");
        Integer num2 = (Integer) r0.get("height");
        Integer num3 = (Integer) r0.get("duration");
        a.m182(3, "BaseVideoTracker", this, String.format(Locale.ROOT, "Player metadata: height = %d, width = %d, duration = %d", new Object[]{num2, num, num3}));
        this.f31.m187(this.f24, this.f27, num, num2, num3);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m189(Integer num, Integer num2) {
        return ((double) Math.abs(num2.intValue() - num.intValue())) <= Math.min(750.0d, ((double) num2.intValue()) * 0.05d);
    }
}
