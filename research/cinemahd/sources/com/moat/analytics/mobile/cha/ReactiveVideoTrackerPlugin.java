package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.view.View;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.p;
import java.util.Map;

public class ReactiveVideoTrackerPlugin implements l<ReactiveVideoTracker> {
    /* access modifiers changed from: private */

    /* renamed from: ˊ  reason: contains not printable characters */
    public final String f11;

    static class d implements ReactiveVideoTracker {
        d() {
        }

        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, Integer num, View view) {
            return false;
        }
    }

    public ReactiveVideoTrackerPlugin(String str) {
        this.f11 = str;
    }

    public ReactiveVideoTracker create() throws o {
        return (ReactiveVideoTracker) p.m309(new p.c<ReactiveVideoTracker>() {
            /* renamed from: ˋ  reason: contains not printable characters */
            public final Optional<ReactiveVideoTracker> m178() {
                a.m179("[INFO] ", "Attempting to create ReactiveVideoTracker");
                return Optional.of(new w(ReactiveVideoTrackerPlugin.this.f11));
            }
        }, ReactiveVideoTracker.class);
    }

    public ReactiveVideoTracker createNoOp() {
        return new d();
    }
}
