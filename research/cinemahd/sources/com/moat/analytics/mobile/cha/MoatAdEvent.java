package com.moat.analytics.mobile.cha;

import java.util.HashMap;
import java.util.Map;

public class MoatAdEvent {
    public static final Double VOLUME_MUTED = Double.valueOf(0.0d);
    public static final Double VOLUME_UNMUTED = Double.valueOf(1.0d);

    /* renamed from: ˋ  reason: contains not printable characters */
    static final Integer f0 = Integer.MIN_VALUE;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static final Double f1 = Double.valueOf(Double.NaN);

    /* renamed from: ʽ  reason: contains not printable characters */
    private final Long f2;

    /* renamed from: ˊ  reason: contains not printable characters */
    Double f3;

    /* renamed from: ˏ  reason: contains not printable characters */
    Integer f4;

    /* renamed from: ॱ  reason: contains not printable characters */
    MoatAdEventType f5;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Double f6;

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num, Double d) {
        this.f2 = Long.valueOf(System.currentTimeMillis());
        this.f5 = moatAdEventType;
        this.f3 = d;
        this.f4 = num;
        this.f6 = Double.valueOf(r.m330());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public final Map<String, Object> m176() {
        HashMap hashMap = new HashMap();
        hashMap.put("adVolume", this.f3);
        hashMap.put("playhead", this.f4);
        hashMap.put("aTimeStamp", this.f2);
        hashMap.put("type", this.f5.toString());
        hashMap.put("deviceVolume", this.f6);
        return hashMap;
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType, Integer num) {
        this(moatAdEventType, num, f1);
    }

    public MoatAdEvent(MoatAdEventType moatAdEventType) {
        this(moatAdEventType, f0, f1);
    }
}
