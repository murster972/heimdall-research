package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import com.chartboost.sdk.impl.b;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import java.lang.ref.WeakReference;

final class r {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static int f157 = 1;

    /* renamed from: ˊ  reason: contains not printable characters */
    private static e f158 = null;

    /* renamed from: ˋ  reason: contains not printable characters */
    private static d f159 = null;

    /* renamed from: ˎ  reason: contains not printable characters */
    private static int f160 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ˏ  reason: contains not printable characters */
    public static String f161;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static int[] f162 = {-39340411, 1646369784, -593413711, -1069164445, -50787683, -1327220997, 423245644, -742130253, 54775946, -495304555, 1880137505, 1742082653, 65717847, 1497802820, 828947133, -614454858, 941569790, -1897799303};

    static class d {

        /* renamed from: ʽ  reason: contains not printable characters */
        boolean f164;

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f165;

        /* renamed from: ˋ  reason: contains not printable characters */
        boolean f166;

        /* renamed from: ˎ  reason: contains not printable characters */
        String f167;

        /* renamed from: ˏ  reason: contains not printable characters */
        String f168;

        /* renamed from: ॱ  reason: contains not printable characters */
        Integer f169;

        /* synthetic */ d(byte b) {
            this();
        }

        private d() {
            this.f167 = "_unknown_";
            this.f168 = "_unknown_";
            this.f169 = -1;
            this.f166 = false;
            this.f165 = false;
            this.f164 = false;
            try {
                Context r0 = r.m329();
                if (r0 != null) {
                    this.f164 = true;
                    TelephonyManager telephonyManager = (TelephonyManager) r0.getSystemService("phone");
                    this.f167 = telephonyManager.getSimOperatorName();
                    this.f168 = telephonyManager.getNetworkOperatorName();
                    this.f169 = Integer.valueOf(telephonyManager.getPhoneType());
                    this.f166 = r.m320();
                    this.f165 = r.m325(r0);
                }
            } catch (Exception e) {
                o.m306(e);
            }
        }
    }

    static class e {

        /* renamed from: ˊ  reason: contains not printable characters */
        private String f170;

        /* renamed from: ˋ  reason: contains not printable characters */
        private String f171;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public boolean f172;

        /* renamed from: ॱ  reason: contains not printable characters */
        private String f173;

        /* synthetic */ e(byte b) {
            this();
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˎ  reason: contains not printable characters */
        public final String m334() {
            return this.f170;
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ॱ  reason: contains not printable characters */
        public final String m335() {
            String str = this.f173;
            return str != null ? str : "_unknown_";
        }

        private e() {
            this.f172 = false;
            this.f170 = "_unknown_";
            this.f171 = "_unknown_";
            this.f173 = "_unknown_";
            try {
                Context r0 = r.m329();
                if (r0 != null) {
                    this.f172 = true;
                    PackageManager packageManager = r0.getPackageManager();
                    this.f171 = r0.getPackageName();
                    this.f170 = packageManager.getApplicationLabel(r0.getApplicationInfo()).toString();
                    this.f173 = packageManager.getInstallerPackageName(this.f171);
                    return;
                }
                a.m182(3, "Util", this, "Can't get app name, appContext is null.");
            } catch (Exception e) {
                o.m306(e);
            }
        }

        /* access modifiers changed from: package-private */
        /* renamed from: ˋ  reason: contains not printable characters */
        public final String m333() {
            return this.f171;
        }
    }

    r() {
    }

    /* renamed from: ʻ  reason: contains not printable characters */
    static /* synthetic */ boolean m320() {
        int i;
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f57;
        Context context = weakReference != null ? (Context) weakReference.get() : null;
        if (context != null) {
            int i2 = f157 + 27;
            f160 = i2 % 128;
            int i3 = i2 % 2;
            if ((Build.VERSION.SDK_INT < 17 ? (char) 22 : 19) != 22) {
                int i4 = f157 + 87;
                f160 = i4 % 128;
                int i5 = i4 % 2;
                i = Settings.Global.getInt(context.getContentResolver(), m327(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            } else {
                i = Settings.Secure.getInt(context.getContentResolver(), m327(new int[]{-474338915, -1244865125, 562481890, 44523707, -1306238932, 74746991}, 11).intern(), 0);
            }
        } else {
            i = 0;
        }
        if (!(i == 1)) {
            return false;
        }
        int i6 = f160 + 33;
        f157 = i6 % 128;
        int i7 = i6 % 2;
        return true;
    }

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int m321() {
        try {
            return ((AudioManager) c.m203().getSystemService(m327(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamVolume(3);
        } catch (Exception e2) {
            o.m306(e2);
            return 0;
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static e m323() {
        e eVar = f158;
        if (eVar == null || !eVar.f172) {
            f158 = new e((byte) 0);
        }
        return f158;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static d m324() {
        d dVar = f159;
        if (dVar == null || !dVar.f164) {
            f159 = new d((byte) 0);
        }
        return f159;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m328(final Application application) {
        try {
            AsyncTask.execute(new Runnable() {
                public final void run() {
                    try {
                        AdvertisingIdClient.Info advertisingIdInfo = AdvertisingIdClient.getAdvertisingIdInfo(application);
                        if (!advertisingIdInfo.isLimitAdTrackingEnabled()) {
                            String unused = r.f161 = advertisingIdInfo.getId();
                            a.m182(3, "Util", this, "Retrieved Advertising ID = " + r.f161);
                            return;
                        }
                        a.m182(3, "Util", this, "User has limited ad tracking");
                    } catch (Exception e) {
                        o.m306(e);
                    }
                }
            });
        } catch (Exception e2) {
            o.m306(e2);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static Context m329() {
        WeakReference<Context> weakReference = ((f) MoatAnalytics.getInstance()).f57;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static double m330() {
        try {
            return ((double) m321()) / ((double) ((AudioManager) c.m203().getSystemService(m327(new int[]{-1741845568, 995393484, -1443163044, -1832527325}, 5).intern())).getStreamMaxVolume(3));
        } catch (Exception e2) {
            o.m306(e2);
            return 0.0d;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m326() {
        return f161;
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static boolean m325(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static String m327(int[] iArr, int i) {
        char[] cArr = new char[4];
        char[] cArr2 = new char[(iArr.length << 1)];
        int[] iArr2 = (int[]) f162.clone();
        int i2 = 0;
        while (true) {
            if (i2 >= iArr.length) {
                return new String(cArr2, 0, i);
            }
            cArr[0] = iArr[i2] >>> 16;
            cArr[1] = (char) iArr[i2];
            int i3 = i2 + 1;
            cArr[2] = iArr[i3] >>> 16;
            cArr[3] = (char) iArr[i3];
            b.a(cArr, iArr2, false);
            int i4 = i2 << 1;
            cArr2[i4] = cArr[0];
            cArr2[i4 + 1] = cArr[1];
            cArr2[i4 + 2] = cArr[2];
            cArr2[i4 + 3] = cArr[3];
            i2 += 2;
        }
    }
}
