package com.moat.analytics.mobile.cha;

import android.graphics.Rect;
import android.view.View;
import com.moat.analytics.mobile.cha.NativeDisplayTracker;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.json.JSONException;
import org.json.JSONObject;

final class q extends d implements NativeDisplayTracker {

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private final Map<String, String> f155;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private final Set<NativeDisplayTracker.MoatUserInteractionType> f156 = new HashSet();

    q(View view, Map<String, String> map) {
        super(view, true, false);
        a.m182(3, "NativeDisplayTracker", this, "Initializing.");
        this.f155 = map;
        if (view == null) {
            String str = "NativeDisplayTracker initialization not successful, " + "Target view is null";
            a.m182(3, "NativeDisplayTracker", this, str);
            a.m179("[ERROR] ", str);
            this.f50 = new o("Target view is null");
        } else if (map == null || map.isEmpty()) {
            String str2 = "NativeDisplayTracker initialization not successful, " + "AdIds is null or empty";
            a.m182(3, "NativeDisplayTracker", this, str2);
            a.m179("[ERROR] ", str2);
            this.f50 = new o("AdIds is null or empty");
        } else {
            a aVar = ((f) MoatAnalytics.getInstance()).f61;
            if (aVar == null) {
                String str3 = "NativeDisplayTracker initialization not successful, " + "prepareNativeDisplayTracking was not called successfully";
                a.m182(3, "NativeDisplayTracker", this, str3);
                a.m179("[ERROR] ", str3);
                this.f50 = new o("prepareNativeDisplayTracking was not called successfully");
                return;
            }
            this.f47 = aVar.f16;
            try {
                super.m217(aVar.f14);
                if (this.f47 != null) {
                    this.f47.m276(m317());
                }
                a.m179("[SUCCESS] ", "NativeDisplayTracker created for " + m208() + ", with adIds:" + map.toString());
            } catch (o e) {
                this.f50 = e;
            }
        }
    }

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private String m317() {
        try {
            Map<String, String> map = this.f155;
            LinkedHashMap linkedHashMap = new LinkedHashMap();
            for (int i = 0; i < 8; i++) {
                String str = "moatClientLevel" + i;
                if (map.containsKey(str)) {
                    linkedHashMap.put(str, map.get(str));
                }
            }
            for (int i2 = 0; i2 < 8; i2++) {
                String str2 = "moatClientSlicer" + i2;
                if (map.containsKey(str2)) {
                    linkedHashMap.put(str2, map.get(str2));
                }
            }
            for (String next : map.keySet()) {
                if (!linkedHashMap.containsKey(next)) {
                    linkedHashMap.put(next, map.get(next));
                }
            }
            String jSONObject = new JSONObject(linkedHashMap).toString();
            a.m182(3, "NativeDisplayTracker", this, "Parsed ad ids = " + jSONObject);
            return "{\"adIds\":" + jSONObject + ", \"adKey\":\"" + this.f46 + "\", \"adSize\":" + m318() + "}";
        } catch (Exception e) {
            o.m306(e);
            return "";
        }
    }

    /* renamed from: ᐝ  reason: contains not printable characters */
    private String m318() {
        try {
            Rect r0 = u.m365(super.m209());
            int width = r0.width();
            int height = r0.height();
            HashMap hashMap = new HashMap();
            hashMap.put("width", Integer.toString(width));
            hashMap.put("height", Integer.toString(height));
            return new JSONObject(hashMap).toString();
        } catch (Exception e) {
            o.m306(e);
            return null;
        }
    }

    public final void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType moatUserInteractionType) {
        try {
            a.m182(3, "NativeDisplayTracker", this, "reportUserInteractionEvent:" + moatUserInteractionType.name());
            if (!this.f156.contains(moatUserInteractionType)) {
                this.f156.add(moatUserInteractionType);
                JSONObject jSONObject = new JSONObject();
                jSONObject.accumulate("adKey", this.f46);
                jSONObject.accumulate("event", moatUserInteractionType.name().toLowerCase());
                if (this.f47 != null) {
                    this.f47.m277(jSONObject.toString());
                }
            }
        } catch (JSONException e) {
            a.m183("NativeDisplayTracker", this, "Got JSON exception");
            o.m306(e);
        } catch (Exception e2) {
            o.m306(e2);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m319() {
        return "NativeDisplayTracker";
    }
}
