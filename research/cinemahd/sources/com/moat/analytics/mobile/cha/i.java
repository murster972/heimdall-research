package com.moat.analytics.mobile.cha;

import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class i extends b {

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private int f83 = c.f93;

    /* renamed from: ˏॱ  reason: contains not printable characters */
    private int f84 = Integer.MIN_VALUE;

    /* renamed from: ͺ  reason: contains not printable characters */
    private int f85 = Integer.MIN_VALUE;

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private double f86 = Double.NaN;

    /* renamed from: ॱˋ  reason: contains not printable characters */
    private int f87 = Integer.MIN_VALUE;

    /* renamed from: ॱˎ  reason: contains not printable characters */
    private int f88 = 0;

    enum c {
        ;
        

        /* renamed from: ˊ  reason: contains not printable characters */
        public static final int f90 = 2;

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f91 = 4;

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f92 = 3;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f93 = 1;

        /* renamed from: ॱ  reason: contains not printable characters */
        public static final int f94 = 5;
    }

    i(String str) {
        super(str);
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.f86 = m191().doubleValue();
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            o.m306(e);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ʻॱ  reason: contains not printable characters */
    public final boolean m243() throws o {
        if (m245() && !m193()) {
            try {
                int intValue = m247().intValue();
                if (this.f84 >= 0 && intValue < 0) {
                    return false;
                }
                this.f84 = intValue;
                if (intValue == 0) {
                    return true;
                }
                int intValue2 = m249().intValue();
                boolean r4 = m248();
                double d = ((double) intValue2) / 4.0d;
                double doubleValue = m191().doubleValue();
                MoatAdEventType moatAdEventType = null;
                if (intValue > this.f87) {
                    this.f87 = intValue;
                }
                if (this.f85 == Integer.MIN_VALUE) {
                    this.f85 = intValue2;
                }
                if (r4) {
                    if (this.f83 == c.f93) {
                        moatAdEventType = MoatAdEventType.AD_EVT_START;
                        this.f83 = c.f92;
                    } else if (this.f83 == c.f90) {
                        moatAdEventType = MoatAdEventType.AD_EVT_PLAYING;
                        this.f83 = c.f92;
                    } else {
                        int floor = ((int) Math.floor(((double) intValue) / d)) - 1;
                        if (floor >= 0 && floor < 3) {
                            MoatAdEventType moatAdEventType2 = b.f22[floor];
                            if (!this.f23.containsKey(moatAdEventType2)) {
                                this.f23.put(moatAdEventType2, 1);
                                moatAdEventType = moatAdEventType2;
                            }
                        }
                    }
                } else if (this.f83 != c.f90) {
                    moatAdEventType = MoatAdEventType.AD_EVT_PAUSED;
                    this.f83 = c.f90;
                }
                boolean z = moatAdEventType != null;
                if (!z && !Double.isNaN(this.f86) && Math.abs(this.f86 - doubleValue) > 0.05d) {
                    moatAdEventType = MoatAdEventType.AD_EVT_VOLUME_CHANGE;
                    z = true;
                }
                if (z) {
                    dispatchEvent(new MoatAdEvent(moatAdEventType, Integer.valueOf(intValue), m198()));
                }
                this.f86 = doubleValue;
                this.f88 = 0;
                return true;
            } catch (Exception unused) {
                int i = this.f88;
                this.f88 = i + 1;
                if (i < 5) {
                    return true;
                }
            }
        }
        return false;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final JSONObject m244(MoatAdEvent moatAdEvent) {
        Integer num;
        int i;
        if (!moatAdEvent.f4.equals(MoatAdEvent.f0)) {
            num = moatAdEvent.f4;
        } else {
            try {
                num = m247();
            } catch (Exception unused) {
                num = Integer.valueOf(this.f84);
            }
            moatAdEvent.f4 = num;
        }
        if (moatAdEvent.f4.intValue() < 0 || (moatAdEvent.f4.intValue() == 0 && moatAdEvent.f5 == MoatAdEventType.AD_EVT_COMPLETE && this.f84 > 0)) {
            num = Integer.valueOf(this.f84);
            moatAdEvent.f4 = num;
        }
        if (moatAdEvent.f5 == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || (i = this.f85) == Integer.MIN_VALUE || !b.m189(num, Integer.valueOf(i))) {
                this.f83 = c.f91;
                moatAdEvent.f5 = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.f83 = c.f94;
            }
        }
        return super.m194(moatAdEvent);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public abstract boolean m245();

    /* renamed from: ॱ  reason: contains not printable characters */
    public final boolean m246(Map<String, String> map, View view) {
        try {
            boolean r4 = super.m197(map, view);
            if (!r4) {
                return r4;
            }
            this.f32.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (!i.this.m245() || i.this.m193()) {
                            i.this.m196();
                        } else if (Boolean.valueOf(i.this.m243()).booleanValue()) {
                            i.this.f32.postDelayed(this, 200);
                        } else {
                            i.this.m196();
                        }
                    } catch (Exception e) {
                        i.this.m196();
                        o.m306(e);
                    }
                }
            }, 200);
            return r4;
        } catch (Exception e) {
            a.m182(3, "IntervalVideoTracker", this, "Problem with video loop");
            m218("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public abstract Integer m247();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public abstract boolean m248();

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public abstract Integer m249();
}
