package com.moat.analytics.mobile.cha;

import android.util.Log;

final class o extends Exception {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Exception f141 = null;

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Long f142 = 60000L;

    /* renamed from: ॱ  reason: contains not printable characters */
    private static Long f143;

    o(String str) {
        super(str);
    }

    /* JADX WARNING: Can't wrap try/catch for region: R(14:12|(1:14)(1:15)|16|(5:17|18|19|(1:21)(2:22|23)|24)|25|27|28|29|30|31|32|38|39|(2:43|46)(1:47)) */
    /* JADX WARNING: Missing exception handler attribute for start block: B:38:0x00d9 */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0183 A[Catch:{ Exception -> 0x0194 }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:? A[Catch:{ Exception -> 0x0194 }, RETURN, SYNTHETIC] */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m304(java.lang.Exception r13) {
        /*
            java.lang.String r0 = ""
            com.moat.analytics.mobile.cha.t r1 = com.moat.analytics.mobile.cha.t.m350()     // Catch:{ Exception -> 0x0194 }
            int r1 = r1.f182     // Catch:{ Exception -> 0x0194 }
            int r2 = com.moat.analytics.mobile.cha.t.a.f193     // Catch:{ Exception -> 0x0194 }
            if (r1 != r2) goto L_0x0192
            com.moat.analytics.mobile.cha.t r1 = com.moat.analytics.mobile.cha.t.m350()     // Catch:{ Exception -> 0x0194 }
            int r1 = r1.f185     // Catch:{ Exception -> 0x0194 }
            if (r1 != 0) goto L_0x0015
            return
        L_0x0015:
            r2 = 100
            if (r1 >= r2) goto L_0x0026
            double r2 = (double) r1     // Catch:{ Exception -> 0x0194 }
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r2 = r2 / r4
            double r4 = java.lang.Math.random()     // Catch:{ Exception -> 0x0194 }
            int r6 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r6 >= 0) goto L_0x0026
            return
        L_0x0026:
            java.lang.String r2 = "https://px.moatads.com/pixel.gif?e=0&i=MOATSDK1&ac=1"
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            r3.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r4 = "&zt="
            r2.<init>(r4)     // Catch:{ Exception -> 0x0194 }
            boolean r4 = r13 instanceof com.moat.analytics.mobile.cha.o     // Catch:{ Exception -> 0x0194 }
            r5 = 1
            r6 = 0
            if (r4 == 0) goto L_0x003c
            r4 = 1
            goto L_0x003d
        L_0x003c:
            r4 = 0
        L_0x003d:
            r2.append(r4)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = r2.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r4 = "&zr="
            r2.<init>(r4)     // Catch:{ Exception -> 0x0194 }
            r2.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = r2.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r2 = "&zm="
            r1.<init>(r2)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r2 = r13.getMessage()     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r4 = "UTF-8"
            if (r2 != 0) goto L_0x006a
            java.lang.String r2 = "null"
            goto L_0x007a
        L_0x006a:
            java.lang.String r2 = r13.getMessage()     // Catch:{ Exception -> 0x00a5 }
            byte[] r2 = r2.getBytes(r4)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r2 = android.util.Base64.encodeToString(r2, r6)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r2 = java.net.URLEncoder.encode(r2, r4)     // Catch:{ Exception -> 0x00a5 }
        L_0x007a:
            r1.append(r2)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r1 = r1.toString()     // Catch:{ Exception -> 0x00a5 }
            r3.append(r1)     // Catch:{ Exception -> 0x00a5 }
            java.lang.StringBuilder r1 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r2 = "&k="
            r1.<init>(r2)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r13 = android.util.Log.getStackTraceString(r13)     // Catch:{ Exception -> 0x00a5 }
            byte[] r13 = r13.getBytes(r4)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r13 = android.util.Base64.encodeToString(r13, r6)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r13 = java.net.URLEncoder.encode(r13, r4)     // Catch:{ Exception -> 0x00a5 }
            r1.append(r13)     // Catch:{ Exception -> 0x00a5 }
            java.lang.String r13 = r1.toString()     // Catch:{ Exception -> 0x00a5 }
            r3.append(r13)     // Catch:{ Exception -> 0x00a5 }
        L_0x00a5:
            java.lang.String r13 = "CHA"
            java.lang.String r1 = "&zMoatMMAKv=35d482907bc2811c2e46b96f16eb5f9fe00185f3"
            r3.append(r1)     // Catch:{ Exception -> 0x00d4 }
            java.lang.String r1 = "2.4.1"
            com.moat.analytics.mobile.cha.r$e r2 = com.moat.analytics.mobile.cha.r.m323()     // Catch:{ Exception -> 0x00d2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r7 = "&zMoatMMAKan="
            r4.<init>(r7)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r7 = r2.m334()     // Catch:{ Exception -> 0x00d2 }
            r4.append(r7)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r4 = r4.toString()     // Catch:{ Exception -> 0x00d2 }
            r3.append(r4)     // Catch:{ Exception -> 0x00d2 }
            java.lang.String r2 = r2.m333()     // Catch:{ Exception -> 0x00d2 }
            int r4 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x00d9 }
            java.lang.String r0 = java.lang.Integer.toString(r4)     // Catch:{ Exception -> 0x00d9 }
            goto L_0x00d9
        L_0x00d2:
            r2 = r0
            goto L_0x00d9
        L_0x00d4:
            r1 = r0
            goto L_0x00d8
        L_0x00d6:
            r13 = r0
            r1 = r13
        L_0x00d8:
            r2 = r1
        L_0x00d9:
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r7 = "&d=Android:"
            r4.<init>(r7)     // Catch:{ Exception -> 0x0194 }
            r4.append(r13)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r13 = ":"
            r4.append(r13)     // Catch:{ Exception -> 0x0194 }
            r4.append(r2)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r13 = ":-"
            r4.append(r13)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r13 = r4.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r13)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "&bo="
            r13.<init>(r2)     // Catch:{ Exception -> 0x0194 }
            r13.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r13)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r13 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "&bd="
            r13.<init>(r1)     // Catch:{ Exception -> 0x0194 }
            r13.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r13 = r13.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r13)     // Catch:{ Exception -> 0x0194 }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ Exception -> 0x0194 }
            java.lang.Long r13 = java.lang.Long.valueOf(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "&t="
            r0.<init>(r1)     // Catch:{ Exception -> 0x0194 }
            r0.append(r13)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.StringBuilder r0 = new java.lang.StringBuilder     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = "&de="
            r0.<init>(r1)     // Catch:{ Exception -> 0x0194 }
            java.util.Locale r1 = java.util.Locale.ROOT     // Catch:{ Exception -> 0x0194 }
            java.lang.String r2 = "%.0f"
            java.lang.Object[] r4 = new java.lang.Object[r5]     // Catch:{ Exception -> 0x0194 }
            double r7 = java.lang.Math.random()     // Catch:{ Exception -> 0x0194 }
            r9 = 4621819117588971520(0x4024000000000000, double:10.0)
            r11 = 4622945017495814144(0x4028000000000000, double:12.0)
            double r9 = java.lang.Math.pow(r9, r11)     // Catch:{ Exception -> 0x0194 }
            double r7 = r7 * r9
            double r7 = java.lang.Math.floor(r7)     // Catch:{ Exception -> 0x0194 }
            java.lang.Double r5 = java.lang.Double.valueOf(r7)     // Catch:{ Exception -> 0x0194 }
            r4[r6] = r5     // Catch:{ Exception -> 0x0194 }
            java.lang.String r1 = java.lang.String.format(r1, r2, r4)     // Catch:{ Exception -> 0x0194 }
            r0.append(r1)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = r0.toString()     // Catch:{ Exception -> 0x0194 }
            r3.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.String r0 = "&cs=0"
            r3.append(r0)     // Catch:{ Exception -> 0x0194 }
            java.lang.Long r0 = f143     // Catch:{ Exception -> 0x0194 }
            if (r0 == 0) goto L_0x0183
            long r0 = r13.longValue()     // Catch:{ Exception -> 0x0194 }
            java.lang.Long r2 = f143     // Catch:{ Exception -> 0x0194 }
            long r4 = r2.longValue()     // Catch:{ Exception -> 0x0194 }
            long r0 = r0 - r4
            java.lang.Long r2 = f142     // Catch:{ Exception -> 0x0194 }
            long r4 = r2.longValue()     // Catch:{ Exception -> 0x0194 }
            int r2 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r2 <= 0) goto L_0x0191
        L_0x0183:
            java.lang.String r0 = r3.toString()     // Catch:{ Exception -> 0x0194 }
            com.moat.analytics.mobile.cha.m$2 r1 = new com.moat.analytics.mobile.cha.m$2     // Catch:{ Exception -> 0x0194 }
            r1.<init>(r0)     // Catch:{ Exception -> 0x0194 }
            r1.start()     // Catch:{ Exception -> 0x0194 }
            f143 = r13     // Catch:{ Exception -> 0x0194 }
        L_0x0191:
            return
        L_0x0192:
            f141 = r13     // Catch:{ Exception -> 0x0194 }
        L_0x0194:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.o.m304(java.lang.Exception):void");
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static String m305(String str, Exception exc) {
        if (exc instanceof o) {
            return str + " failed: " + exc.getMessage();
        }
        return str + " failed unexpectedly";
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m307() {
        Exception exc = f141;
        if (exc != null) {
            m304(exc);
            f141 = null;
        }
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m306(Exception exc) {
        if (t.m350().f183) {
            Log.e("MoatException", Log.getStackTraceString(exc));
        } else {
            m304(exc);
        }
    }
}
