package com.moat.analytics.mobile.cha;

import android.content.Context;
import android.content.Intent;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

final class h {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static final h f73 = new h();
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public ScheduledFuture<?> f74;
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public final Map<j, String> f75 = new WeakHashMap();
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public ScheduledFuture<?> f76;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final ScheduledExecutorService f77 = Executors.newScheduledThreadPool(1);
    /* access modifiers changed from: private */

    /* renamed from: ॱ  reason: contains not printable characters */
    public final Map<d, String> f78 = new WeakHashMap();

    private h() {
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m241(final Context context, d dVar) {
        if (dVar != null) {
            a.m182(3, "JSUpdateLooper", this, "addActiveTracker" + dVar.hashCode());
            if (!this.f78.containsKey(dVar)) {
                this.f78.put(dVar, "");
                ScheduledFuture<?> scheduledFuture = this.f76;
                if (scheduledFuture == null || scheduledFuture.isDone()) {
                    a.m182(3, "JSUpdateLooper", this, "Starting view update loop");
                    this.f76 = this.f77.scheduleWithFixedDelay(new Runnable() {
                        public final void run() {
                            try {
                                LocalBroadcastManager.a(context.getApplicationContext()).a(new Intent("UPDATE_VIEW_INFO"));
                                if (h.this.f78.isEmpty()) {
                                    a.m182(3, "JSUpdateLooper", h.this, "No more active trackers");
                                    h.this.f76.cancel(true);
                                }
                            } catch (Exception e) {
                                o.m306(e);
                            }
                        }
                    }, 0, (long) t.m350().f178, TimeUnit.MILLISECONDS);
                }
            }
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static h m234() {
        return f73;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m240(final Context context, j jVar) {
        if (jVar != null) {
            this.f75.put(jVar, "");
            ScheduledFuture<?> scheduledFuture = this.f74;
            if (scheduledFuture == null || scheduledFuture.isDone()) {
                a.m182(3, "JSUpdateLooper", this, "Starting metadata reporting loop");
                this.f74 = this.f77.scheduleWithFixedDelay(new Runnable() {
                    public final void run() {
                        try {
                            LocalBroadcastManager.a(context.getApplicationContext()).a(new Intent("UPDATE_METADATA"));
                            if (h.this.f75.isEmpty()) {
                                h.this.f74.cancel(true);
                            }
                        } catch (Exception e) {
                            o.m306(e);
                        }
                    }
                }, 0, 50, TimeUnit.MILLISECONDS);
            }
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m239(j jVar) {
        if (jVar != null) {
            a.m182(3, "JSUpdateLooper", this, "removeSetupNeededBridge" + jVar.hashCode());
            this.f75.remove(jVar);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱ  reason: contains not printable characters */
    public final void m242(d dVar) {
        if (dVar != null) {
            a.m182(3, "JSUpdateLooper", this, "removeActiveTracker" + dVar.hashCode());
            this.f78.remove(dVar);
        }
    }
}
