package com.moat.analytics.mobile.cha;

import android.os.Handler;
import android.os.Looper;
import com.original.tase.model.socket.UserResponces;
import java.util.Iterator;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

final class t {

    /* renamed from: ʻ  reason: contains not printable characters */
    private static t f175;
    /* access modifiers changed from: private */

    /* renamed from: ʽ  reason: contains not printable characters */
    public static final Queue<e> f176 = new ConcurrentLinkedQueue();
    /* access modifiers changed from: private */

    /* renamed from: ʼ  reason: contains not printable characters */
    public long f177 = 60000;

    /* renamed from: ˊ  reason: contains not printable characters */
    volatile int f178 = UserResponces.USER_RESPONCE_SUCCSES;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private long f179 = 1800000;

    /* renamed from: ˋ  reason: contains not printable characters */
    volatile boolean f180 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˋॱ  reason: contains not printable characters */
    public final AtomicBoolean f181 = new AtomicBoolean(false);

    /* renamed from: ˎ  reason: contains not printable characters */
    volatile int f182 = a.f194;

    /* renamed from: ˏ  reason: contains not printable characters */
    volatile boolean f183 = false;
    /* access modifiers changed from: private */

    /* renamed from: ˏॱ  reason: contains not printable characters */
    public final AtomicBoolean f184 = new AtomicBoolean(false);

    /* renamed from: ॱ  reason: contains not printable characters */
    volatile int f185 = 10;
    /* access modifiers changed from: private */

    /* renamed from: ॱˊ  reason: contains not printable characters */
    public final AtomicInteger f186 = new AtomicInteger(0);
    /* access modifiers changed from: private */

    /* renamed from: ॱˋ  reason: contains not printable characters */
    public volatile long f187 = 0;
    /* access modifiers changed from: private */

    /* renamed from: ᐝ  reason: contains not printable characters */
    public Handler f188;

    enum a {
        ;
        

        /* renamed from: ˎ  reason: contains not printable characters */
        public static final int f193 = 2;

        /* renamed from: ॱ  reason: contains not printable characters */
        public static final int f194 = 1;
    }

    interface b {
        /* renamed from: ˎ  reason: contains not printable characters */
        void m358() throws o;
    }

    interface c {
        /* renamed from: ˏ  reason: contains not printable characters */
        void m359(g gVar) throws o;
    }

    class d implements Runnable {

        /* renamed from: ˎ  reason: contains not printable characters */
        private final String f196;
        /* access modifiers changed from: private */

        /* renamed from: ˏ  reason: contains not printable characters */
        public final AnonymousClass2.AnonymousClass2 f197;

        /* renamed from: ॱ  reason: contains not printable characters */
        private final Handler f198;

        /* renamed from: ˎ  reason: contains not printable characters */
        private String m360() {
            try {
                return m.m284(this.f196 + "?ts=" + System.currentTimeMillis() + "&v=2.4.1").get();
            } catch (Exception unused) {
                return null;
            }
        }

        public final void run() {
            try {
                String r0 = m360();
                final g gVar = new g(r0);
                t.this.f183 = gVar.m229();
                t.this.f180 = gVar.m233();
                t.this.f178 = gVar.m232();
                t.this.f185 = gVar.m231();
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public final void run() {
                        try {
                            d.this.f197.m359(gVar);
                        } catch (Exception e) {
                            o.m306(e);
                        }
                    }
                });
                long unused = t.this.f187 = System.currentTimeMillis();
                t.this.f184.compareAndSet(true, false);
                if (r0 != null) {
                    t.this.f186.set(0);
                } else if (t.this.f186.incrementAndGet() < 10) {
                    t.this.m351(t.this.f177);
                }
            } catch (Exception e) {
                o.m306(e);
            }
            this.f198.removeCallbacks(this);
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        }

        private d(String str, Handler handler, AnonymousClass2.AnonymousClass2 r4) {
            this.f197 = r4;
            this.f198 = handler;
            this.f196 = "https://z.moatads.com/" + str + "/android/" + "35d4829" + "/status.json";
        }
    }

    class e {

        /* renamed from: ˎ  reason: contains not printable characters */
        final Long f201;

        /* renamed from: ॱ  reason: contains not printable characters */
        final b f203;

        e(Long l, b bVar) {
            this.f201 = l;
            this.f203 = bVar;
        }
    }

    private t() {
        try {
            this.f188 = new Handler(Looper.getMainLooper());
        } catch (Exception e2) {
            o.m306(e2);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static synchronized t m350() {
        t tVar;
        synchronized (t.class) {
            if (f175 == null) {
                f175 = new t();
            }
            tVar = f175;
        }
        return tVar;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m355(b bVar) throws o {
        if (this.f182 == a.f193) {
            bVar.m358();
            return;
        }
        m344();
        f176.add(new e(Long.valueOf(System.currentTimeMillis()), bVar));
        if (this.f181.compareAndSet(false, true)) {
            this.f188.postDelayed(new Runnable() {
                public final void run() {
                    try {
                        if (t.f176.size() > 0) {
                            t.m344();
                            t.this.f188.postDelayed(this, 60000);
                            return;
                        }
                        t.this.f181.compareAndSet(true, false);
                        t.this.f188.removeCallbacks(this);
                    } catch (Exception e) {
                        o.m306(e);
                    }
                }
            }, 60000);
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m356() {
        if (System.currentTimeMillis() - this.f187 > this.f179) {
            m351(0);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public void m351(final long j) {
        if (this.f184.compareAndSet(false, true)) {
            a.m182(3, "OnOff", this, "Performing status check.");
            new Thread() {
                public final void run() {
                    Looper.prepare();
                    Handler handler = new Handler();
                    handler.postDelayed(new d(BuildConfig.NAMESPACE, handler, new c() {
                        /* renamed from: ˏ  reason: contains not printable characters */
                        public final void m357(g gVar) throws o {
                            synchronized (t.f176) {
                                boolean z = ((f) MoatAnalytics.getInstance()).f59;
                                if (t.this.f182 != gVar.m230() || (t.this.f182 == a.f194 && z)) {
                                    t.this.f182 = gVar.m230();
                                    if (t.this.f182 == a.f194 && z) {
                                        t.this.f182 = a.f193;
                                    }
                                    if (t.this.f182 == a.f193) {
                                        a.m182(3, "OnOff", this, "Moat enabled - Version 2.4.1");
                                    }
                                    for (e eVar : t.f176) {
                                        if (t.this.f182 == a.f193) {
                                            eVar.f203.m358();
                                        }
                                    }
                                }
                                while (!t.f176.isEmpty()) {
                                    t.f176.remove();
                                }
                            }
                        }
                    }), j);
                    Looper.loop();
                }
            }.start();
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public static void m344() {
        synchronized (f176) {
            long currentTimeMillis = System.currentTimeMillis();
            Iterator it2 = f176.iterator();
            while (it2.hasNext()) {
                if (currentTimeMillis - ((e) it2.next()).f201.longValue() >= 60000) {
                    it2.remove();
                }
            }
            if (f176.size() >= 15) {
                for (int i = 0; i < 5; i++) {
                    f176.remove();
                }
            }
        }
    }
}
