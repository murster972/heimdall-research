package com.moat.analytics.mobile.cha;

import android.graphics.Rect;
import android.location.Location;
import android.util.DisplayMetrics;
import android.view.View;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.json.JSONObject;

final class u {

    /* renamed from: ʼ  reason: contains not printable characters */
    private static int f204 = 0;

    /* renamed from: ˋॱ  reason: contains not printable characters */
    private static int f205 = 1;

    /* renamed from: ʻ  reason: contains not printable characters */
    private Location f206;

    /* renamed from: ʽ  reason: contains not printable characters */
    private JSONObject f207;

    /* renamed from: ˊ  reason: contains not printable characters */
    private Rect f208;

    /* renamed from: ˊॱ  reason: contains not printable characters */
    private JSONObject f209;

    /* renamed from: ˋ  reason: contains not printable characters */
    private JSONObject f210;

    /* renamed from: ˎ  reason: contains not printable characters */
    private Rect f211;

    /* renamed from: ˏ  reason: contains not printable characters */
    private c f212 = new c();

    /* renamed from: ॱ  reason: contains not printable characters */
    String f213 = "{}";

    /* renamed from: ᐝ  reason: contains not printable characters */
    private Map<String, Object> f214 = new HashMap();

    static class a {

        /* renamed from: ˎ  reason: contains not printable characters */
        final Rect f215;

        /* renamed from: ˏ  reason: contains not printable characters */
        final View f216;

        a(View view, a aVar) {
            this.f216 = view;
            if (aVar != null) {
                Rect rect = aVar.f215;
                int i = rect.left;
                int i2 = rect.top;
                int left = i + view.getLeft();
                int top = i2 + view.getTop();
                this.f215 = new Rect(left, top, view.getWidth() + left, view.getHeight() + top);
                return;
            }
            this.f215 = u.m370(view);
        }
    }

    static class b {

        /* renamed from: ˊ  reason: contains not printable characters */
        boolean f217 = false;

        /* renamed from: ˋ  reason: contains not printable characters */
        final Set<Rect> f218 = new HashSet();

        /* renamed from: ˎ  reason: contains not printable characters */
        int f219 = 0;

        b() {
        }
    }

    static class c {

        /* renamed from: ˊ  reason: contains not printable characters */
        double f220 = 0.0d;

        /* renamed from: ˋ  reason: contains not printable characters */
        Rect f221 = new Rect(0, 0, 0, 0);

        /* renamed from: ॱ  reason: contains not printable characters */
        double f222 = 0.0d;

        c() {
        }
    }

    u() {
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static float m362(View view) {
        float alpha = view.getAlpha();
        while (true) {
            if ((view != null ? 25 : 'E') == 25) {
                boolean z = false;
                if (!(view.getParent() != null)) {
                    break;
                }
                if (((double) alpha) != 0.0d) {
                    z = true;
                }
                if (!z) {
                    break;
                }
                if ((view.getParent() instanceof View ? 27 : ']') != 27) {
                    break;
                }
                alpha *= ((View) view.getParent()).getAlpha();
                view = (View) view.getParent();
            } else {
                break;
            }
        }
        return alpha;
    }

    /* JADX WARNING: Code restructure failed: missing block: B:37:0x008e, code lost:
        if ((r9.getBackground() != null ? 'Y' : '0') != 'Y') goto L_0x006f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009c, code lost:
        if ((r9.getBackground() != null ? '=' : 6) != 6) goto L_0x009e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00d3, code lost:
        if ((r2) != false) goto L_0x00e1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00df, code lost:
        if ((r2 ? 'S' : '9') != '9') goto L_0x00e1;
     */
    /* JADX WARNING: Removed duplicated region for block: B:122:0x0116 A[EDGE_INSN: B:122:0x0116->B:85:0x0116 ?: BREAK  , SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:14:0x0035 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:53:0x00b6  */
    /* JADX WARNING: Removed duplicated region for block: B:54:0x00b9  */
    /* JADX WARNING: Removed duplicated region for block: B:56:0x00be  */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x00ef  */
    /* JADX WARNING: Removed duplicated region for block: B:75:0x00f2  */
    /* JADX WARNING: Removed duplicated region for block: B:78:0x00f8  */
    /* renamed from: ˏ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static void m371(com.moat.analytics.mobile.cha.u.a r13, android.graphics.Rect r14, com.moat.analytics.mobile.cha.u.b r15) {
        /*
            android.view.View r0 = r13.f216
            boolean r1 = r0.isShown()
            r2 = 19
            r3 = 74
            if (r1 == 0) goto L_0x000f
            r1 = 19
            goto L_0x0011
        L_0x000f:
            r1 = 74
        L_0x0011:
            r4 = 3
            r5 = 2
            r6 = 0
            r7 = 1
            if (r1 == r3) goto L_0x0032
            float r0 = r0.getAlpha()
            double r0 = (double) r0
            r8 = 0
            int r3 = (r0 > r8 ? 1 : (r0 == r8 ? 0 : -1))
            if (r3 <= 0) goto L_0x0024
            r0 = 0
            goto L_0x0025
        L_0x0024:
            r0 = 1
        L_0x0025:
            if (r0 == 0) goto L_0x0028
            goto L_0x0032
        L_0x0028:
            int r0 = f204
            int r0 = r0 + r4
            int r1 = r0 % 128
            f205 = r1
            int r0 = r0 % r5
            r0 = 1
            goto L_0x0033
        L_0x0032:
            r0 = 0
        L_0x0033:
            if (r0 != 0) goto L_0x0036
            return
        L_0x0036:
            android.view.View r0 = r13.f216
            boolean r0 = r0 instanceof android.view.ViewGroup
            if (r0 == 0) goto L_0x003e
            r0 = 0
            goto L_0x003f
        L_0x003e:
            r0 = 1
        L_0x003f:
            r1 = 91
            r3 = 90
            r8 = 48
            if (r0 == 0) goto L_0x004a
            r0 = 1
            goto L_0x0116
        L_0x004a:
            int r0 = f205
            int r0 = r0 + 39
            int r9 = r0 % 128
            f204 = r9
            int r0 = r0 % r5
            java.lang.Class<android.view.ViewGroup> r0 = android.view.ViewGroup.class
            android.view.View r9 = r13.f216
            java.lang.Class r9 = r9.getClass()
            java.lang.Class r9 = r9.getSuperclass()
            boolean r0 = r0.equals(r9)
            android.view.View r9 = r13.f216
            int r10 = android.os.Build.VERSION.SDK_INT
            r11 = 4
            if (r10 < r2) goto L_0x006c
            r2 = 4
            goto L_0x006d
        L_0x006c:
            r2 = 1
        L_0x006d:
            if (r2 == r11) goto L_0x0071
        L_0x006f:
            r2 = 1
            goto L_0x00b2
        L_0x0071:
            int r2 = f205
            int r2 = r2 + 113
            int r10 = r2 % 128
            f204 = r10
            int r2 = r2 % r5
            if (r2 == 0) goto L_0x007e
            r2 = 1
            goto L_0x007f
        L_0x007e:
            r2 = 0
        L_0x007f:
            if (r2 == r7) goto L_0x0091
            android.graphics.drawable.Drawable r2 = r9.getBackground()
            r10 = 89
            if (r2 == 0) goto L_0x008c
            r2 = 89
            goto L_0x008e
        L_0x008c:
            r2 = 48
        L_0x008e:
            if (r2 == r10) goto L_0x009e
            goto L_0x006f
        L_0x0091:
            android.graphics.drawable.Drawable r2 = r9.getBackground()
            if (r2 == 0) goto L_0x009a
            r2 = 61
            goto L_0x009b
        L_0x009a:
            r2 = 6
        L_0x009b:
            r10 = 6
            if (r2 == r10) goto L_0x006f
        L_0x009e:
            android.graphics.drawable.Drawable r2 = r9.getBackground()
            int r2 = r2.getAlpha()
            if (r2 != 0) goto L_0x00ab
            r2 = 31
            goto L_0x00ad
        L_0x00ab:
            r2 = 13
        L_0x00ad:
            r9 = 31
            if (r2 == r9) goto L_0x006f
            r2 = 0
        L_0x00b2:
            r9 = 70
            if (r0 == 0) goto L_0x00b9
            r0 = 70
            goto L_0x00bb
        L_0x00b9:
            r0 = 91
        L_0x00bb:
            if (r0 == r9) goto L_0x00be
            goto L_0x00e3
        L_0x00be:
            int r0 = f205
            int r0 = r0 + 75
            int r9 = r0 % 128
            f204 = r9
            int r0 = r0 % r5
            if (r0 == 0) goto L_0x00cb
            r0 = 0
            goto L_0x00cc
        L_0x00cb:
            r0 = 1
        L_0x00cc:
            if (r0 == r7) goto L_0x00d6
            if (r2 == 0) goto L_0x00d2
            r0 = 1
            goto L_0x00d3
        L_0x00d2:
            r0 = 0
        L_0x00d3:
            if (r0 == 0) goto L_0x00e3
            goto L_0x00e1
        L_0x00d6:
            if (r2 == 0) goto L_0x00db
            r0 = 83
            goto L_0x00dd
        L_0x00db:
            r0 = 57
        L_0x00dd:
            r2 = 57
            if (r0 == r2) goto L_0x00e3
        L_0x00e1:
            r0 = 0
            goto L_0x00e4
        L_0x00e3:
            r0 = 1
        L_0x00e4:
            android.view.View r2 = r13.f216
            android.view.ViewGroup r2 = (android.view.ViewGroup) r2
            int r9 = r2.getChildCount()
            r10 = 0
        L_0x00ed:
            if (r10 >= r9) goto L_0x00f2
            r11 = 90
            goto L_0x00f4
        L_0x00f2:
            r11 = 99
        L_0x00f4:
            r12 = 99
            if (r11 == r12) goto L_0x0116
            int r11 = r15.f219
            int r11 = r11 + r7
            r15.f219 = r11
            r12 = 500(0x1f4, float:7.0E-43)
            if (r11 <= r12) goto L_0x0102
            return
        L_0x0102:
            com.moat.analytics.mobile.cha.u$a r11 = new com.moat.analytics.mobile.cha.u$a
            android.view.View r12 = r2.getChildAt(r10)
            r11.<init>(r12, r13)
            m371(r11, r14, r15)
            boolean r11 = r15.f217
            if (r11 == 0) goto L_0x0113
            return
        L_0x0113:
            int r10 = r10 + 1
            goto L_0x00ed
        L_0x0116:
            r2 = 52
            if (r0 == 0) goto L_0x011d
            r0 = 52
            goto L_0x011f
        L_0x011d:
            r0 = 16
        L_0x011f:
            if (r0 == r2) goto L_0x0123
            goto L_0x01b0
        L_0x0123:
            android.graphics.Rect r0 = r13.f215
            boolean r2 = r0.setIntersect(r14, r0)
            if (r2 == 0) goto L_0x012c
            goto L_0x012e
        L_0x012c:
            r3 = 48
        L_0x012e:
            if (r3 == r8) goto L_0x01b0
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 22
            if (r2 < r3) goto L_0x0138
            r2 = 0
            goto L_0x0139
        L_0x0138:
            r2 = 1
        L_0x0139:
            if (r2 == 0) goto L_0x013c
            goto L_0x0159
        L_0x013c:
            android.graphics.Rect r0 = new android.graphics.Rect
            r0.<init>(r6, r6, r6, r6)
            android.view.View r2 = r13.f216
            boolean r2 = m367((android.view.View) r2, (android.graphics.Rect) r0)
            if (r2 == 0) goto L_0x014b
            r2 = 0
            goto L_0x014c
        L_0x014b:
            r2 = 1
        L_0x014c:
            if (r2 == 0) goto L_0x014f
            return
        L_0x014f:
            android.graphics.Rect r2 = r13.f215
            boolean r0 = r2.setIntersect(r0, r2)
            if (r0 != 0) goto L_0x0158
            return
        L_0x0158:
            r0 = r2
        L_0x0159:
            com.moat.analytics.mobile.cha.t r2 = com.moat.analytics.mobile.cha.t.m350()
            boolean r2 = r2.f180
            if (r2 == 0) goto L_0x0163
            r2 = 1
            goto L_0x0164
        L_0x0163:
            r2 = 0
        L_0x0164:
            if (r2 == 0) goto L_0x0193
            android.view.View r2 = r13.f216
            java.util.Locale r3 = java.util.Locale.ROOT
            java.lang.Object[] r4 = new java.lang.Object[r4]
            java.lang.Class r8 = r2.getClass()
            java.lang.String r8 = r8.getName()
            r4[r6] = r8
            java.lang.String r6 = r0.toString()
            r4[r7] = r6
            android.view.View r13 = r13.f216
            float r13 = r13.getAlpha()
            java.lang.Float r13 = java.lang.Float.valueOf(r13)
            r4[r5] = r13
            java.lang.String r13 = "Covered by %s-%s alpha=%f"
            java.lang.String r13 = java.lang.String.format(r3, r13, r4)
            java.lang.String r3 = "VisibilityInfo"
            com.moat.analytics.mobile.cha.a.m183(r3, r2, r13)
        L_0x0193:
            java.util.Set<android.graphics.Rect> r13 = r15.f218
            r13.add(r0)
            boolean r13 = r0.contains(r14)
            if (r13 == 0) goto L_0x019f
            goto L_0x01a1
        L_0x019f:
            r1 = 25
        L_0x01a1:
            r13 = 25
            if (r1 == r13) goto L_0x01b0
            int r13 = f205
            int r13 = r13 + 39
            int r14 = r13 % 128
            f204 = r14
            int r13 = r13 % r5
            r15.f217 = r7
        L_0x01b0:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.u.m371(com.moat.analytics.mobile.cha.u$a, android.graphics.Rect, com.moat.analytics.mobile.cha.u$b):void");
    }

    /* JADX WARNING: type inference failed for: r0v62, types: [java.util.HashSet, java.util.Set<android.graphics.Rect>] */
    /* access modifiers changed from: package-private */
    /* JADX WARNING: Code restructure failed: missing block: B:260:0x0343, code lost:
        if ((r9.f220 == r1.f212.f220 ? '&' : 'G') != 'G') goto L_0x0345;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:279:0x039e, code lost:
        if (r10.equals(r1.f211) == false) goto L_0x03a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:300:0x03f9, code lost:
        if (r12 != r0) goto L_0x03fb;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0073, code lost:
        if ((r6 != null ? '<' : '0') != '0') goto L_0x0075;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:68:0x00d8, code lost:
        if ((r20.getWindowToken() != null ? 'Z' : 20) != 'Z') goto L_0x00da;
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0145 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0148 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x014b A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:123:0x015e A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:124:0x0160 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:126:0x0163 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:127:0x0168 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:130:0x018c  */
    /* JADX WARNING: Removed duplicated region for block: B:131:0x018f  */
    /* JADX WARNING: Removed duplicated region for block: B:134:0x0195  */
    /* JADX WARNING: Removed duplicated region for block: B:135:0x0197  */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x01b6  */
    /* JADX WARNING: Removed duplicated region for block: B:144:0x01b9  */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x01be  */
    /* JADX WARNING: Removed duplicated region for block: B:148:0x01c0  */
    /* JADX WARNING: Removed duplicated region for block: B:152:0x01d6  */
    /* JADX WARNING: Removed duplicated region for block: B:153:0x01d9  */
    /* JADX WARNING: Removed duplicated region for block: B:155:0x01dd  */
    /* JADX WARNING: Removed duplicated region for block: B:156:0x01df  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x020a A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:165:0x020d A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x0213 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x0219 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:173:0x022e  */
    /* JADX WARNING: Removed duplicated region for block: B:174:0x0230  */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x0236  */
    /* JADX WARNING: Removed duplicated region for block: B:246:0x0316  */
    /* JADX WARNING: Removed duplicated region for block: B:247:0x0318  */
    /* JADX WARNING: Removed duplicated region for block: B:250:0x031d  */
    /* JADX WARNING: Removed duplicated region for block: B:273:0x0388  */
    /* JADX WARNING: Removed duplicated region for block: B:274:0x038a  */
    /* JADX WARNING: Removed duplicated region for block: B:276:0x038d  */
    /* JADX WARNING: Removed duplicated region for block: B:283:0x03b6 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:284:0x03b9 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:291:0x03db  */
    /* JADX WARNING: Removed duplicated region for block: B:292:0x03de  */
    /* JADX WARNING: Removed duplicated region for block: B:294:0x03e2  */
    /* JADX WARNING: Removed duplicated region for block: B:304:0x040e  */
    /* JADX WARNING: Removed duplicated region for block: B:305:0x0411  */
    /* JADX WARNING: Removed duplicated region for block: B:308:0x0417  */
    /* JADX WARNING: Removed duplicated region for block: B:312:0x0426 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:313:0x0428 A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:315:0x042b A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:316:0x042d A[Catch:{ Exception -> 0x04c9 }] */
    /* JADX WARNING: Removed duplicated region for block: B:44:0x0098  */
    /* JADX WARNING: Removed duplicated region for block: B:45:0x009a  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x00a3  */
    /* JADX WARNING: Removed duplicated region for block: B:71:0x00de  */
    /* JADX WARNING: Removed duplicated region for block: B:85:0x0109  */
    /* JADX WARNING: Removed duplicated region for block: B:86:0x010c  */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x0110  */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void m372(java.lang.String r19, android.view.View r20) {
        /*
            r18 = this;
            r1 = r18
            r0 = r20
            java.util.HashMap r2 = new java.util.HashMap
            r2.<init>()
            java.lang.String r3 = "{}"
            r4 = 0
            r5 = 1
            if (r0 == 0) goto L_0x0011
            r6 = 1
            goto L_0x0012
        L_0x0011:
            r6 = 0
        L_0x0012:
            if (r6 == 0) goto L_0x04cf
            int r6 = f204
            r7 = 5
            int r6 = r6 + r7
            int r8 = r6 % 128
            f205 = r8
            int r6 = r6 % 2
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x04c9 }
            r8 = 17
            if (r6 < r8) goto L_0x0026
            r6 = 0
            goto L_0x0027
        L_0x0026:
            r6 = 1
        L_0x0027:
            r9 = 48
            r10 = 57
            if (r6 == r5) goto L_0x0086
            int r6 = f205
            int r6 = r6 + 113
            int r11 = r6 % 128
            f204 = r11
            int r6 = r6 % 2
            java.lang.ref.WeakReference<android.app.Activity> r6 = com.moat.analytics.mobile.cha.c.f39     // Catch:{ Exception -> 0x04c9 }
            if (r6 == 0) goto L_0x003e
            r6 = 57
            goto L_0x0040
        L_0x003e:
            r6 = 25
        L_0x0040:
            r11 = 25
            if (r6 == r11) goto L_0x0086
            int r6 = f204
            int r6 = r6 + r10
            int r11 = r6 % 128
            f205 = r11
            int r6 = r6 % 2
            if (r6 != 0) goto L_0x0051
            r6 = 1
            goto L_0x0052
        L_0x0051:
            r6 = 0
        L_0x0052:
            if (r6 == 0) goto L_0x0064
            java.lang.ref.WeakReference<android.app.Activity> r6 = com.moat.analytics.mobile.cha.c.f39     // Catch:{ Exception -> 0x04c9 }
            java.lang.Object r6 = r6.get()     // Catch:{ Exception -> 0x04c9 }
            android.app.Activity r6 = (android.app.Activity) r6     // Catch:{ Exception -> 0x04c9 }
            if (r6 == 0) goto L_0x0060
            r11 = 1
            goto L_0x0061
        L_0x0060:
            r11 = 0
        L_0x0061:
            if (r11 == r5) goto L_0x0075
            goto L_0x0086
        L_0x0064:
            java.lang.ref.WeakReference<android.app.Activity> r6 = com.moat.analytics.mobile.cha.c.f39     // Catch:{ Exception -> 0x04c9 }
            java.lang.Object r6 = r6.get()     // Catch:{ Exception -> 0x04c9 }
            android.app.Activity r6 = (android.app.Activity) r6     // Catch:{ Exception -> 0x04c9 }
            if (r6 == 0) goto L_0x0071
            r11 = 60
            goto L_0x0073
        L_0x0071:
            r11 = 48
        L_0x0073:
            if (r11 == r9) goto L_0x0086
        L_0x0075:
            android.util.DisplayMetrics r11 = new android.util.DisplayMetrics     // Catch:{ Exception -> 0x04c9 }
            r11.<init>()     // Catch:{ Exception -> 0x04c9 }
            android.view.WindowManager r6 = r6.getWindowManager()     // Catch:{ Exception -> 0x04c9 }
            android.view.Display r6 = r6.getDefaultDisplay()     // Catch:{ Exception -> 0x04c9 }
            r6.getRealMetrics(r11)     // Catch:{ Exception -> 0x04c9 }
            goto L_0x0092
        L_0x0086:
            android.content.Context r6 = r20.getContext()     // Catch:{ Exception -> 0x04c9 }
            android.content.res.Resources r6 = r6.getResources()     // Catch:{ Exception -> 0x04c9 }
            android.util.DisplayMetrics r11 = r6.getDisplayMetrics()     // Catch:{ Exception -> 0x04c9 }
        L_0x0092:
            int r6 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x04c9 }
            r12 = 19
            if (r6 < r12) goto L_0x009a
            r6 = 0
            goto L_0x009b
        L_0x009a:
            r6 = 1
        L_0x009b:
            r12 = 90
            r13 = 69
            r14 = 91
            if (r6 == 0) goto L_0x00de
            if (r0 == 0) goto L_0x00a8
            r6 = 57
            goto L_0x00aa
        L_0x00a8:
            r6 = 9
        L_0x00aa:
            if (r6 == r10) goto L_0x00ad
            goto L_0x00da
        L_0x00ad:
            int r6 = f205
            int r6 = r6 + 45
            int r10 = r6 % 128
            f204 = r10
            int r6 = r6 % 2
            if (r6 == 0) goto L_0x00bb
            r6 = 1
            goto L_0x00bd
        L_0x00bb:
            r6 = 65
        L_0x00bd:
            if (r6 == r5) goto L_0x00cd
            android.os.IBinder r6 = r20.getWindowToken()     // Catch:{ Exception -> 0x04c9 }
            if (r6 == 0) goto L_0x00c8
            r6 = 91
            goto L_0x00ca
        L_0x00c8:
            r6 = 69
        L_0x00ca:
            if (r6 == r13) goto L_0x00da
            goto L_0x00dc
        L_0x00cd:
            android.os.IBinder r6 = r20.getWindowToken()     // Catch:{ Exception -> 0x04c9 }
            if (r6 == 0) goto L_0x00d6
            r6 = 90
            goto L_0x00d8
        L_0x00d6:
            r6 = 20
        L_0x00d8:
            if (r6 == r12) goto L_0x00dc
        L_0x00da:
            r6 = 0
            goto L_0x0105
        L_0x00dc:
            r6 = 1
            goto L_0x0105
        L_0x00de:
            int r6 = f204
            int r6 = r6 + 51
            int r10 = r6 % 128
            f205 = r10
            int r6 = r6 % 2
            if (r0 == 0) goto L_0x00ec
            r6 = 1
            goto L_0x00ed
        L_0x00ec:
            r6 = 0
        L_0x00ed:
            if (r6 == 0) goto L_0x00da
            boolean r6 = r20.isAttachedToWindow()     // Catch:{ Exception -> 0x04c9 }
            if (r6 == 0) goto L_0x00f7
            r6 = 1
            goto L_0x00f8
        L_0x00f7:
            r6 = 0
        L_0x00f8:
            if (r6 == r5) goto L_0x00fb
            goto L_0x00da
        L_0x00fb:
            int r6 = f205
            int r6 = r6 + r13
            int r10 = r6 % 128
            f204 = r10
            int r6 = r6 % 2
            goto L_0x00dc
        L_0x0105:
            r10 = 37
            if (r0 == 0) goto L_0x010c
            r13 = 54
            goto L_0x010e
        L_0x010c:
            r13 = 37
        L_0x010e:
            if (r13 == r10) goto L_0x0141
            int r13 = f205
            int r13 = r13 + 101
            int r12 = r13 % 128
            f204 = r12
            int r13 = r13 % 2
            if (r13 == 0) goto L_0x011f
            r12 = 91
            goto L_0x0121
        L_0x011f:
            r12 = 31
        L_0x0121:
            if (r12 == r14) goto L_0x0133
            boolean r12 = r20.hasWindowFocus()     // Catch:{ Exception -> 0x04c9 }
            if (r12 == 0) goto L_0x012c
            r12 = 14
            goto L_0x012e
        L_0x012c:
            r12 = 16
        L_0x012e:
            r13 = 16
            if (r12 == r13) goto L_0x0141
            goto L_0x013f
        L_0x0133:
            boolean r12 = r20.hasWindowFocus()     // Catch:{ Exception -> 0x04c9 }
            if (r12 == 0) goto L_0x013b
            r12 = 1
            goto L_0x013c
        L_0x013b:
            r12 = 0
        L_0x013c:
            if (r12 == r5) goto L_0x013f
            goto L_0x0141
        L_0x013f:
            r12 = 1
            goto L_0x0142
        L_0x0141:
            r12 = 0
        L_0x0142:
            r13 = 4
            if (r0 == 0) goto L_0x0148
            r14 = 82
            goto L_0x0149
        L_0x0148:
            r14 = 4
        L_0x0149:
            if (r14 == r13) goto L_0x015b
            boolean r14 = r20.isShown()     // Catch:{ Exception -> 0x04c9 }
            if (r14 != 0) goto L_0x0154
            r14 = 64
            goto L_0x0156
        L_0x0154:
            r14 = 37
        L_0x0156:
            if (r14 == r10) goto L_0x0159
            goto L_0x015b
        L_0x0159:
            r14 = 0
            goto L_0x015c
        L_0x015b:
            r14 = 1
        L_0x015c:
            if (r0 != 0) goto L_0x0160
            r8 = 1
            goto L_0x0161
        L_0x0160:
            r8 = 0
        L_0x0161:
            if (r8 == r5) goto L_0x0168
            float r8 = m362(r20)     // Catch:{ Exception -> 0x04c9 }
            goto L_0x0169
        L_0x0168:
            r8 = 0
        L_0x0169:
            java.lang.String r10 = "dr"
            float r13 = r11.density     // Catch:{ Exception -> 0x04c9 }
            java.lang.Float r13 = java.lang.Float.valueOf(r13)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r10, r13)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r10 = "dv"
            double r16 = com.moat.analytics.mobile.cha.r.m330()     // Catch:{ Exception -> 0x04c9 }
            java.lang.Double r13 = java.lang.Double.valueOf(r16)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r10, r13)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r10 = "adKey"
            r13 = r19
            r2.put(r10, r13)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r10 = "isAttached"
            if (r6 == 0) goto L_0x018f
            r13 = 23
            goto L_0x0191
        L_0x018f:
            r13 = 30
        L_0x0191:
            r15 = 23
            if (r13 == r15) goto L_0x0197
            r13 = 0
            goto L_0x01ab
        L_0x0197:
            int r13 = f205
            int r13 = r13 + 89
            int r15 = r13 % 128
            f204 = r15
            int r13 = r13 % 2
            if (r13 == 0) goto L_0x01a6
            r13 = 46
            goto L_0x01a8
        L_0x01a6:
            r13 = 79
        L_0x01a8:
            r15 = 79
            r13 = 1
        L_0x01ab:
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r10, r13)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r10 = "inFocus"
            if (r12 == 0) goto L_0x01b9
            r13 = 85
            goto L_0x01ba
        L_0x01b9:
            r13 = 1
        L_0x01ba:
            r15 = 85
            if (r13 == r15) goto L_0x01c0
            r13 = 0
            goto L_0x01cb
        L_0x01c0:
            int r13 = f204
            int r13 = r13 + 19
            int r15 = r13 % 128
            f205 = r15
            int r13 = r13 % 2
            r13 = 1
        L_0x01cb:
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r10, r13)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r10 = "isHidden"
            if (r14 == 0) goto L_0x01d9
            r13 = 48
            goto L_0x01db
        L_0x01d9:
            r13 = 42
        L_0x01db:
            if (r13 == r9) goto L_0x01df
            r9 = 0
            goto L_0x01ef
        L_0x01df:
            int r9 = f205
            int r9 = r9 + 27
            int r13 = r9 % 128
            f204 = r13
            int r9 = r9 % 2
            if (r9 == 0) goto L_0x01ed
            r9 = 1
            goto L_0x01ee
        L_0x01ed:
            r9 = 0
        L_0x01ee:
            r9 = 1
        L_0x01ef:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r9)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r10, r9)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r9 = "opacity"
            java.lang.Float r8 = java.lang.Float.valueOf(r8)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r9, r8)     // Catch:{ Exception -> 0x04c9 }
            int r8 = r11.widthPixels     // Catch:{ Exception -> 0x04c9 }
            int r9 = r11.heightPixels     // Catch:{ Exception -> 0x04c9 }
            android.graphics.Rect r10 = new android.graphics.Rect     // Catch:{ Exception -> 0x04c9 }
            r10.<init>(r4, r4, r8, r9)     // Catch:{ Exception -> 0x04c9 }
            if (r0 == 0) goto L_0x020d
            r8 = 88
            goto L_0x020f
        L_0x020d:
            r8 = 57
        L_0x020f:
            r9 = 88
            if (r8 == r9) goto L_0x0219
            android.graphics.Rect r8 = new android.graphics.Rect     // Catch:{ Exception -> 0x04c9 }
            r8.<init>(r4, r4, r4, r4)     // Catch:{ Exception -> 0x04c9 }
            goto L_0x021d
        L_0x0219:
            android.graphics.Rect r8 = m370(r20)     // Catch:{ Exception -> 0x04c9 }
        L_0x021d:
            com.moat.analytics.mobile.cha.u$c r9 = new com.moat.analytics.mobile.cha.u$c     // Catch:{ Exception -> 0x04c9 }
            r9.<init>()     // Catch:{ Exception -> 0x04c9 }
            int r13 = r8.width()     // Catch:{ Exception -> 0x04c9 }
            int r15 = r8.height()     // Catch:{ Exception -> 0x04c9 }
            int r13 = r13 * r15
            if (r0 == 0) goto L_0x0230
            r15 = 5
            goto L_0x0232
        L_0x0230:
            r15 = 74
        L_0x0232:
            if (r15 == r7) goto L_0x0236
            goto L_0x0312
        L_0x0236:
            int r7 = f205
            int r7 = r7 + 21
            int r15 = r7 % 128
            f204 = r15
            int r7 = r7 % 2
            if (r6 == 0) goto L_0x0245
            r6 = 67
            goto L_0x0247
        L_0x0245:
            r6 = 86
        L_0x0247:
            r7 = 86
            if (r6 == r7) goto L_0x0312
            if (r12 == 0) goto L_0x0250
            r6 = 10
            goto L_0x0252
        L_0x0250:
            r6 = 40
        L_0x0252:
            r7 = 40
            if (r6 == r7) goto L_0x0312
            int r6 = f204
            int r6 = r6 + 87
            int r7 = r6 % 128
            f205 = r7
            int r6 = r6 % 2
            if (r14 != 0) goto L_0x0265
            r6 = 41
            goto L_0x0267
        L_0x0265:
            r6 = 78
        L_0x0267:
            r7 = 78
            if (r6 == r7) goto L_0x0312
            if (r13 <= 0) goto L_0x026f
            r6 = 1
            goto L_0x0271
        L_0x026f:
            r6 = 13
        L_0x0271:
            if (r6 == r5) goto L_0x0275
            goto L_0x0312
        L_0x0275:
            int r6 = f205
            int r6 = r6 + 101
            int r7 = r6 % 128
            f204 = r7
            int r6 = r6 % 2
            if (r6 == 0) goto L_0x0284
            r6 = 38
            goto L_0x0286
        L_0x0284:
            r6 = 57
        L_0x0286:
            r7 = 38
            if (r6 == r7) goto L_0x02a2
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ Exception -> 0x04c9 }
            r6.<init>(r4, r4, r4, r4)     // Catch:{ Exception -> 0x04c9 }
            boolean r7 = m367((android.view.View) r0, (android.graphics.Rect) r6)     // Catch:{ Exception -> 0x04c9 }
            if (r7 == 0) goto L_0x029a
            r7 = 9
            r15 = 9
            goto L_0x029e
        L_0x029a:
            r15 = 49
            r7 = 9
        L_0x029e:
            if (r15 == r7) goto L_0x02b3
            goto L_0x0312
        L_0x02a2:
            android.graphics.Rect r6 = new android.graphics.Rect     // Catch:{ Exception -> 0x04c9 }
            r6.<init>(r4, r4, r4, r4)     // Catch:{ Exception -> 0x04c9 }
            boolean r7 = m367((android.view.View) r0, (android.graphics.Rect) r6)     // Catch:{ Exception -> 0x04c9 }
            if (r7 == 0) goto L_0x02af
            r7 = 1
            goto L_0x02b0
        L_0x02af:
            r7 = 0
        L_0x02b0:
            if (r7 == r5) goto L_0x02b3
            goto L_0x0312
        L_0x02b3:
            int r7 = r6.width()     // Catch:{ Exception -> 0x04c9 }
            int r12 = r6.height()     // Catch:{ Exception -> 0x04c9 }
            int r7 = r7 * r12
            if (r7 >= r13) goto L_0x02c1
            r12 = 1
            goto L_0x02c2
        L_0x02c1:
            r12 = 0
        L_0x02c2:
            if (r12 == 0) goto L_0x02cc
            java.lang.String r12 = "VisibilityInfo"
            r14 = 0
            java.lang.String r15 = "Ad is clipped"
            com.moat.analytics.mobile.cha.a.m183(r12, r14, r15)     // Catch:{ Exception -> 0x04c9 }
        L_0x02cc:
            android.view.View r12 = r20.getRootView()     // Catch:{ Exception -> 0x04c9 }
            boolean r12 = r12 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x04c9 }
            if (r12 == 0) goto L_0x02d6
            r12 = 1
            goto L_0x02d7
        L_0x02d6:
            r12 = 0
        L_0x02d7:
            if (r12 == r5) goto L_0x02da
            goto L_0x0312
        L_0x02da:
            r9.f221 = r6     // Catch:{ Exception -> 0x04c9 }
            com.moat.analytics.mobile.cha.u$b r0 = m366((android.graphics.Rect) r6, (android.view.View) r0)     // Catch:{ Exception -> 0x04c9 }
            boolean r12 = r0.f217     // Catch:{ Exception -> 0x04c9 }
            if (r12 == 0) goto L_0x02e6
            r12 = 1
            goto L_0x02e7
        L_0x02e6:
            r12 = 0
        L_0x02e7:
            if (r12 == r5) goto L_0x030e
            java.util.Set<android.graphics.Rect> r0 = r0.f218     // Catch:{ Exception -> 0x04c9 }
            int r0 = m363((android.graphics.Rect) r6, (java.util.Set<android.graphics.Rect>) r0)     // Catch:{ Exception -> 0x04c9 }
            if (r0 <= 0) goto L_0x02f4
            r6 = 90
            goto L_0x02f5
        L_0x02f4:
            r6 = 4
        L_0x02f5:
            r12 = 4
            if (r6 == r12) goto L_0x0307
            int r6 = f205
            int r6 = r6 + 35
            int r12 = r6 % 128
            f204 = r12
            int r6 = r6 % 2
            double r14 = (double) r0
            double r4 = (double) r7
            double r14 = r14 / r4
            r9.f222 = r14     // Catch:{ Exception -> 0x04c9 }
        L_0x0307:
            int r7 = r7 - r0
            double r4 = (double) r7     // Catch:{ Exception -> 0x04c9 }
            double r13 = (double) r13     // Catch:{ Exception -> 0x04c9 }
            double r4 = r4 / r13
            r9.f220 = r4     // Catch:{ Exception -> 0x04c9 }
            goto L_0x0312
        L_0x030e:
            r4 = 4607182418800017408(0x3ff0000000000000, double:1.0)
            r9.f222 = r4     // Catch:{ Exception -> 0x04c9 }
        L_0x0312:
            org.json.JSONObject r0 = r1.f210     // Catch:{ Exception -> 0x04c9 }
            if (r0 == 0) goto L_0x0318
            r0 = 1
            goto L_0x0319
        L_0x0318:
            r0 = 0
        L_0x0319:
            r4 = 71
            if (r0 == 0) goto L_0x0363
            int r0 = f205
            int r0 = r0 + 39
            int r5 = r0 % 128
            f204 = r5
            int r0 = r0 % 2
            if (r0 == 0) goto L_0x0334
            double r13 = r9.f220     // Catch:{ Exception -> 0x04c9 }
            com.moat.analytics.mobile.cha.u$c r0 = r1.f212     // Catch:{ Exception -> 0x04c9 }
            double r6 = r0.f220     // Catch:{ Exception -> 0x04c9 }
            int r0 = (r13 > r6 ? 1 : (r13 == r6 ? 0 : -1))
            if (r0 != 0) goto L_0x0363
            goto L_0x0345
        L_0x0334:
            double r5 = r9.f220     // Catch:{ Exception -> 0x04c9 }
            com.moat.analytics.mobile.cha.u$c r0 = r1.f212     // Catch:{ Exception -> 0x04c9 }
            double r13 = r0.f220     // Catch:{ Exception -> 0x04c9 }
            int r0 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r0 != 0) goto L_0x0341
            r0 = 38
            goto L_0x0343
        L_0x0341:
            r0 = 71
        L_0x0343:
            if (r0 == r4) goto L_0x0363
        L_0x0345:
            android.graphics.Rect r0 = r9.f221     // Catch:{ Exception -> 0x04c9 }
            com.moat.analytics.mobile.cha.u$c r5 = r1.f212     // Catch:{ Exception -> 0x04c9 }
            android.graphics.Rect r5 = r5.f221     // Catch:{ Exception -> 0x04c9 }
            boolean r0 = r0.equals(r5)     // Catch:{ Exception -> 0x04c9 }
            if (r0 == 0) goto L_0x0353
            r0 = 1
            goto L_0x0354
        L_0x0353:
            r0 = 0
        L_0x0354:
            if (r0 == 0) goto L_0x0363
            double r5 = r9.f222     // Catch:{ Exception -> 0x04c9 }
            com.moat.analytics.mobile.cha.u$c r0 = r1.f212     // Catch:{ Exception -> 0x04c9 }
            double r13 = r0.f222     // Catch:{ Exception -> 0x04c9 }
            int r0 = (r5 > r13 ? 1 : (r5 == r13 ? 0 : -1))
            if (r0 == 0) goto L_0x0361
            goto L_0x0363
        L_0x0361:
            r5 = 0
            goto L_0x0379
        L_0x0363:
            r1.f212 = r9     // Catch:{ Exception -> 0x04c9 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x04c9 }
            com.moat.analytics.mobile.cha.u$c r5 = r1.f212     // Catch:{ Exception -> 0x04c9 }
            android.graphics.Rect r5 = r5.f221     // Catch:{ Exception -> 0x04c9 }
            android.graphics.Rect r5 = m364((android.graphics.Rect) r5, (android.util.DisplayMetrics) r11)     // Catch:{ Exception -> 0x04c9 }
            java.util.HashMap r5 = m369((android.graphics.Rect) r5)     // Catch:{ Exception -> 0x04c9 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x04c9 }
            r1.f210 = r0     // Catch:{ Exception -> 0x04c9 }
            r5 = 1
        L_0x0379:
            java.lang.String r0 = "coveredPercent"
            double r6 = r9.f222     // Catch:{ Exception -> 0x04c9 }
            java.lang.Double r6 = java.lang.Double.valueOf(r6)     // Catch:{ Exception -> 0x04c9 }
            r2.put(r0, r6)     // Catch:{ Exception -> 0x04c9 }
            org.json.JSONObject r0 = r1.f209     // Catch:{ Exception -> 0x04c9 }
            if (r0 == 0) goto L_0x038a
            r0 = 1
            goto L_0x038b
        L_0x038a:
            r0 = 0
        L_0x038b:
            if (r0 == 0) goto L_0x03a0
            int r0 = f205
            r6 = 37
            int r0 = r0 + r6
            int r6 = r0 % 128
            f204 = r6
            int r0 = r0 % 2
            android.graphics.Rect r0 = r1.f211     // Catch:{ Exception -> 0x04c9 }
            boolean r0 = r10.equals(r0)     // Catch:{ Exception -> 0x04c9 }
            if (r0 != 0) goto L_0x03b2
        L_0x03a0:
            r1.f211 = r10     // Catch:{ Exception -> 0x04c9 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x04c9 }
            android.graphics.Rect r5 = m364((android.graphics.Rect) r10, (android.util.DisplayMetrics) r11)     // Catch:{ Exception -> 0x04c9 }
            java.util.HashMap r5 = m369((android.graphics.Rect) r5)     // Catch:{ Exception -> 0x04c9 }
            r0.<init>(r5)     // Catch:{ Exception -> 0x04c9 }
            r1.f209 = r0     // Catch:{ Exception -> 0x04c9 }
            r5 = 1
        L_0x03b2:
            org.json.JSONObject r0 = r1.f207     // Catch:{ Exception -> 0x04c9 }
            if (r0 == 0) goto L_0x03b9
            r0 = 18
            goto L_0x03bb
        L_0x03b9:
            r0 = 71
        L_0x03bb:
            if (r0 == r4) goto L_0x03c5
            android.graphics.Rect r0 = r1.f208     // Catch:{ Exception -> 0x04c9 }
            boolean r0 = r8.equals(r0)     // Catch:{ Exception -> 0x04c9 }
            if (r0 != 0) goto L_0x03d7
        L_0x03c5:
            r1.f208 = r8     // Catch:{ Exception -> 0x04c9 }
            org.json.JSONObject r0 = new org.json.JSONObject     // Catch:{ Exception -> 0x04c9 }
            android.graphics.Rect r4 = m364((android.graphics.Rect) r8, (android.util.DisplayMetrics) r11)     // Catch:{ Exception -> 0x04c9 }
            java.util.HashMap r4 = m369((android.graphics.Rect) r4)     // Catch:{ Exception -> 0x04c9 }
            r0.<init>(r4)     // Catch:{ Exception -> 0x04c9 }
            r1.f207 = r0     // Catch:{ Exception -> 0x04c9 }
            r5 = 1
        L_0x03d7:
            java.util.Map<java.lang.String, java.lang.Object> r0 = r1.f214     // Catch:{ Exception -> 0x04c9 }
            if (r0 == 0) goto L_0x03de
            r0 = 1
            r12 = 0
            goto L_0x03e0
        L_0x03de:
            r0 = 1
            r12 = 1
        L_0x03e0:
            if (r12 == r0) goto L_0x03fb
            int r0 = f204
            int r0 = r0 + 113
            int r4 = r0 % 128
            f205 = r4
            int r0 = r0 % 2
            java.util.Map<java.lang.String, java.lang.Object> r0 = r1.f214     // Catch:{ Exception -> 0x04c9 }
            boolean r0 = r2.equals(r0)     // Catch:{ Exception -> 0x04c9 }
            if (r0 != 0) goto L_0x03f7
            r0 = 1
            r12 = 0
            goto L_0x03f9
        L_0x03f7:
            r0 = 1
            r12 = 1
        L_0x03f9:
            if (r12 == r0) goto L_0x03fe
        L_0x03fb:
            r1.f214 = r2     // Catch:{ Exception -> 0x04c9 }
            r5 = 1
        L_0x03fe:
            com.moat.analytics.mobile.cha.n r2 = com.moat.analytics.mobile.cha.n.m292()     // Catch:{ Exception -> 0x04c9 }
            android.location.Location r2 = r2.m301()     // Catch:{ Exception -> 0x04c9 }
            android.location.Location r4 = r1.f206     // Catch:{ Exception -> 0x04c9 }
            boolean r4 = com.moat.analytics.mobile.cha.n.m291(r2, r4)     // Catch:{ Exception -> 0x04c9 }
            if (r4 != 0) goto L_0x0411
            r4 = 21
            goto L_0x0413
        L_0x0411:
            r4 = 93
        L_0x0413:
            r6 = 93
            if (r4 == r6) goto L_0x0424
            int r4 = f205
            int r4 = r4 + 81
            int r5 = r4 % 128
            f204 = r5
            int r4 = r4 % 2
            r1.f206 = r2     // Catch:{ Exception -> 0x04c9 }
            r5 = 1
        L_0x0424:
            if (r5 == 0) goto L_0x0428
            r4 = 0
            goto L_0x0429
        L_0x0428:
            r4 = 1
        L_0x0429:
            if (r4 == 0) goto L_0x042d
            goto L_0x04cf
        L_0x042d:
            org.json.JSONObject r4 = new org.json.JSONObject     // Catch:{ Exception -> 0x04c9 }
            java.util.Map<java.lang.String, java.lang.Object> r5 = r1.f214     // Catch:{ Exception -> 0x04c9 }
            r4.<init>(r5)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r5 = "screen"
            org.json.JSONObject r6 = r1.f209     // Catch:{ Exception -> 0x04c9 }
            r4.accumulate(r5, r6)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r5 = "view"
            org.json.JSONObject r6 = r1.f207     // Catch:{ Exception -> 0x04c9 }
            r4.accumulate(r5, r6)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r5 = "visible"
            org.json.JSONObject r6 = r1.f210     // Catch:{ Exception -> 0x04c9 }
            r4.accumulate(r5, r6)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r5 = "maybe"
            org.json.JSONObject r6 = r1.f210     // Catch:{ Exception -> 0x04c9 }
            r4.accumulate(r5, r6)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r5 = "visiblePercent"
            com.moat.analytics.mobile.cha.u$c r6 = r1.f212     // Catch:{ Exception -> 0x04c9 }
            double r6 = r6.f220     // Catch:{ Exception -> 0x04c9 }
            java.lang.Double r6 = java.lang.Double.valueOf(r6)     // Catch:{ Exception -> 0x04c9 }
            r4.accumulate(r5, r6)     // Catch:{ Exception -> 0x04c9 }
            if (r2 == 0) goto L_0x0464
            r8 = 62
            r5 = 60
            goto L_0x0468
        L_0x0464:
            r5 = 60
            r8 = 60
        L_0x0468:
            if (r8 == r5) goto L_0x04c2
            java.lang.String r5 = "location"
            if (r2 != 0) goto L_0x046f
            goto L_0x0470
        L_0x046f:
            r0 = 0
        L_0x0470:
            if (r0 == 0) goto L_0x0474
            r0 = 0
            goto L_0x04ad
        L_0x0474:
            java.util.HashMap r0 = new java.util.HashMap     // Catch:{ Exception -> 0x04c9 }
            r0.<init>()     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r6 = "latitude"
            double r7 = r2.getLatitude()     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r7 = java.lang.Double.toString(r7)     // Catch:{ Exception -> 0x04c9 }
            r0.put(r6, r7)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r6 = "longitude"
            double r7 = r2.getLongitude()     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r7 = java.lang.Double.toString(r7)     // Catch:{ Exception -> 0x04c9 }
            r0.put(r6, r7)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r6 = "timestamp"
            long r7 = r2.getTime()     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r7 = java.lang.Long.toString(r7)     // Catch:{ Exception -> 0x04c9 }
            r0.put(r6, r7)     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r6 = "horizontalAccuracy"
            float r2 = r2.getAccuracy()     // Catch:{ Exception -> 0x04c9 }
            java.lang.String r2 = java.lang.Float.toString(r2)     // Catch:{ Exception -> 0x04c9 }
            r0.put(r6, r2)     // Catch:{ Exception -> 0x04c9 }
        L_0x04ad:
            if (r0 != 0) goto L_0x04b2
            r2 = 14
            goto L_0x04b4
        L_0x04b2:
            r2 = 85
        L_0x04b4:
            r6 = 14
            if (r2 == r6) goto L_0x04be
            org.json.JSONObject r2 = new org.json.JSONObject     // Catch:{ Exception -> 0x04c9 }
            r2.<init>(r0)     // Catch:{ Exception -> 0x04c9 }
            goto L_0x04bf
        L_0x04be:
            r2 = 0
        L_0x04bf:
            r4.accumulate(r5, r2)     // Catch:{ Exception -> 0x04c9 }
        L_0x04c2:
            java.lang.String r3 = r4.toString()     // Catch:{ Exception -> 0x04c9 }
            r1.f213 = r3     // Catch:{ Exception -> 0x04c9 }
            goto L_0x04cf
        L_0x04c9:
            r0 = move-exception
            com.moat.analytics.mobile.cha.o.m306(r0)
            r1.f213 = r3
        L_0x04cf:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.u.m372(java.lang.String, android.view.View):void");
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    private static Map<String, String> m369(Rect rect) {
        HashMap hashMap = new HashMap();
        hashMap.put("x", String.valueOf(rect.left));
        hashMap.put("y", String.valueOf(rect.top));
        hashMap.put("w", String.valueOf(rect.right - rect.left));
        hashMap.put("h", String.valueOf(rect.bottom - rect.top));
        return hashMap;
    }

    /* access modifiers changed from: private */
    /* renamed from: ˏ  reason: contains not printable characters */
    public static Rect m370(View view) {
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationOnScreen(iArr);
        int i = iArr[0];
        int i2 = iArr[1];
        return new Rect(i, i2, view.getWidth() + i, view.getHeight() + i2);
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static Rect m365(View view) {
        if ((view != null ? '+' : '>') != '+') {
            return new Rect(0, 0, 0, 0);
        }
        int i = f205 + 39;
        f204 = i % 128;
        if (i % 2 == 0) {
        }
        return m370(view);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:108:0x0145, code lost:
        com.moat.analytics.mobile.cha.a.m182(r8, "VisibilityInfo", (java.lang.Object) null, "Short-circuiting cover retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:142:0x01a7, code lost:
        if ((r9.getZ() <= r4.getZ()) != false) goto L_0x01a9;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x005e, code lost:
        r3 = f204 + 123;
        f205 = r3 % 128;
        r3 = r3 % 2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:?, code lost:
        com.moat.analytics.mobile.cha.a.m182(3, "VisibilityInfo", (java.lang.Object) null, "Short-circuiting chain retrieval, reached max");
     */
    /* JADX WARNING: Code restructure failed: missing block: B:84:0x0106, code lost:
        if ((r4.getParent() instanceof android.view.ViewGroup ? 18 : 'S') != 'S') goto L_0x0108;
     */
    /* JADX WARNING: Removed duplicated region for block: B:111:0x0152 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:112:0x0154 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:114:0x0157 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:157:0x01cd A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:158:0x01d2 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:160:0x01d8 A[Catch:{ Exception -> 0x0203 }] */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x01ed  */
    /* JADX WARNING: Removed duplicated region for block: B:167:0x01f0  */
    /* renamed from: ˋ  reason: contains not printable characters */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static com.moat.analytics.mobile.cha.u.b m366(android.graphics.Rect r17, android.view.View r18) {
        /*
            com.moat.analytics.mobile.cha.u$b r1 = new com.moat.analytics.mobile.cha.u$b
            r1.<init>()
            java.util.ArrayDeque r0 = new java.util.ArrayDeque     // Catch:{ Exception -> 0x0203 }
            r0.<init>()     // Catch:{ Exception -> 0x0203 }
            r3 = r18
            r4 = 0
        L_0x000d:
            android.view.ViewParent r5 = r3.getParent()     // Catch:{ Exception -> 0x0203 }
            r6 = 95
            r7 = 73
            if (r5 != 0) goto L_0x001a
            r5 = 95
            goto L_0x001c
        L_0x001a:
            r5 = 73
        L_0x001c:
            r8 = 3
            r9 = 49
            r10 = 0
            java.lang.String r11 = "VisibilityInfo"
            r12 = 53
            r13 = 1
            if (r5 == r6) goto L_0x0028
            goto L_0x0050
        L_0x0028:
            int r5 = f205
            int r5 = r5 + 81
            int r6 = r5 % 128
            f204 = r6
            int r5 = r5 % 2
            if (r5 == 0) goto L_0x0036
            r5 = 0
            goto L_0x0037
        L_0x0036:
            r5 = 1
        L_0x0037:
            if (r5 == r13) goto L_0x0045
            android.view.View r5 = r18.getRootView()     // Catch:{ Exception -> 0x0203 }
            if (r3 != r5) goto L_0x0041
            r5 = 1
            goto L_0x0042
        L_0x0041:
            r5 = 0
        L_0x0042:
            if (r5 == r13) goto L_0x0050
            goto L_0x0087
        L_0x0045:
            android.view.View r5 = r18.getRootView()     // Catch:{ Exception -> 0x0203 }
            if (r3 != r5) goto L_0x004d
            r5 = 1
            goto L_0x004e
        L_0x004d:
            r5 = 0
        L_0x004e:
            if (r5 == 0) goto L_0x0087
        L_0x0050:
            int r4 = r4 + r13
            r5 = 50
            r6 = 92
            if (r4 <= r5) goto L_0x005a
            r5 = 53
            goto L_0x005c
        L_0x005a:
            r5 = 92
        L_0x005c:
            if (r5 == r6) goto L_0x006e
            int r3 = f204
            int r3 = r3 + 123
            int r4 = r3 % 128
            f205 = r4
            int r3 = r3 % 2
            java.lang.String r3 = "Short-circuiting chain retrieval, reached max"
            com.moat.analytics.mobile.cha.a.m182(r8, r11, r10, r3)     // Catch:{ Exception -> 0x0203 }
            goto L_0x0087
        L_0x006e:
            r0.add(r3)     // Catch:{ Exception -> 0x0203 }
            android.view.ViewParent r5 = r3.getParent()     // Catch:{ Exception -> 0x0203 }
            boolean r5 = r5 instanceof android.view.View     // Catch:{ Exception -> 0x0203 }
            if (r5 == 0) goto L_0x007c
            r5 = 29
            goto L_0x007e
        L_0x007c:
            r5 = 49
        L_0x007e:
            if (r5 == r9) goto L_0x0087
            android.view.ViewParent r3 = r3.getParent()     // Catch:{ Exception -> 0x0203 }
            android.view.View r3 = (android.view.View) r3     // Catch:{ Exception -> 0x0203 }
            goto L_0x000d
        L_0x0087:
            boolean r3 = r0.isEmpty()     // Catch:{ Exception -> 0x0203 }
            r4 = 64
            if (r3 == 0) goto L_0x0090
            goto L_0x0092
        L_0x0090:
            r7 = 64
        L_0x0092:
            if (r7 == r4) goto L_0x00a6
            int r0 = f205
            int r0 = r0 + 125
            int r3 = r0 % 128
            f204 = r3
            int r0 = r0 % 2
            if (r0 == 0) goto L_0x00a2
            r2 = 0
            goto L_0x00a3
        L_0x00a2:
            r2 = 1
        L_0x00a3:
            if (r2 == 0) goto L_0x00a5
        L_0x00a5:
            return r1
        L_0x00a6:
            java.lang.String r3 = "starting covering rect search"
            r4 = r18
            com.moat.analytics.mobile.cha.a.m183(r11, r4, r3)     // Catch:{ Exception -> 0x0203 }
            r3 = r10
        L_0x00ae:
            boolean r4 = r0.isEmpty()     // Catch:{ Exception -> 0x0203 }
            if (r4 != 0) goto L_0x00b6
            r4 = 0
            goto L_0x00b7
        L_0x00b6:
            r4 = 1
        L_0x00b7:
            if (r4 == r13) goto L_0x0207
            java.lang.Object r4 = r0.pollLast()     // Catch:{ Exception -> 0x0203 }
            android.view.View r4 = (android.view.View) r4     // Catch:{ Exception -> 0x0203 }
            com.moat.analytics.mobile.cha.u$a r5 = new com.moat.analytics.mobile.cha.u$a     // Catch:{ Exception -> 0x0203 }
            r5.<init>(r4, r3)     // Catch:{ Exception -> 0x0203 }
            android.view.ViewParent r6 = r4.getParent()     // Catch:{ Exception -> 0x0203 }
            if (r6 == 0) goto L_0x00cc
            r6 = 0
            goto L_0x00cd
        L_0x00cc:
            r6 = 1
        L_0x00cd:
            if (r6 == 0) goto L_0x00d3
        L_0x00cf:
            r9 = r17
            goto L_0x01fd
        L_0x00d3:
            int r6 = f204
            r7 = 19
            int r6 = r6 + r7
            int r14 = r6 % 128
            f205 = r14
            int r6 = r6 % 2
            if (r6 != 0) goto L_0x00e2
            r6 = 0
            goto L_0x00e3
        L_0x00e2:
            r6 = 1
        L_0x00e3:
            if (r6 == r13) goto L_0x00f7
            android.view.ViewParent r6 = r4.getParent()     // Catch:{ Exception -> 0x0203 }
            boolean r6 = r6 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x0203 }
            if (r6 == 0) goto L_0x00f0
            r6 = 90
            goto L_0x00f2
        L_0x00f0:
            r6 = 71
        L_0x00f2:
            r15 = 71
            if (r6 == r15) goto L_0x00cf
            goto L_0x0108
        L_0x00f7:
            android.view.ViewParent r6 = r4.getParent()     // Catch:{ Exception -> 0x0203 }
            boolean r6 = r6 instanceof android.view.ViewGroup     // Catch:{ Exception -> 0x0203 }
            if (r6 == 0) goto L_0x0102
            r6 = 18
            goto L_0x0104
        L_0x0102:
            r6 = 83
        L_0x0104:
            r15 = 83
            if (r6 == r15) goto L_0x00cf
        L_0x0108:
            android.view.ViewParent r6 = r4.getParent()     // Catch:{ Exception -> 0x0203 }
            android.view.ViewGroup r6 = (android.view.ViewGroup) r6     // Catch:{ Exception -> 0x0203 }
            int r15 = r6.getChildCount()     // Catch:{ Exception -> 0x0203 }
            r2 = 0
            r16 = 0
        L_0x0115:
            if (r2 >= r15) goto L_0x0119
            r9 = 1
            goto L_0x011a
        L_0x0119:
            r9 = 0
        L_0x011a:
            if (r9 == r13) goto L_0x011d
            goto L_0x00cf
        L_0x011d:
            int r9 = f204
            int r9 = r9 + 25
            int r14 = r9 % 128
            f205 = r14
            int r9 = r9 % 2
            if (r9 != 0) goto L_0x012b
            r9 = 0
            goto L_0x012c
        L_0x012b:
            r9 = 1
        L_0x012c:
            if (r9 == r13) goto L_0x013a
            int r9 = r1.f219     // Catch:{ Exception -> 0x0203 }
            r14 = 500(0x1f4, float:7.0E-43)
            if (r9 < r14) goto L_0x0136
            r9 = 1
            goto L_0x0137
        L_0x0136:
            r9 = 0
        L_0x0137:
            if (r9 == r13) goto L_0x0145
            goto L_0x014c
        L_0x013a:
            int r9 = r1.f219     // Catch:{ Exception -> 0x0203 }
            r14 = 500(0x1f4, float:7.0E-43)
            if (r9 < r14) goto L_0x0142
            r9 = 0
            goto L_0x0143
        L_0x0142:
            r9 = 1
        L_0x0143:
            if (r9 == r13) goto L_0x014c
        L_0x0145:
            java.lang.String r0 = "Short-circuiting cover retrieval, reached max"
            com.moat.analytics.mobile.cha.a.m182(r8, r11, r10, r0)     // Catch:{ Exception -> 0x0203 }
            goto L_0x0207
        L_0x014c:
            android.view.View r9 = r6.getChildAt(r2)     // Catch:{ Exception -> 0x0203 }
            if (r9 != r4) goto L_0x0154
            r14 = 1
            goto L_0x0155
        L_0x0154:
            r14 = 0
        L_0x0155:
            if (r14 == r13) goto L_0x01f0
            int r14 = r1.f219     // Catch:{ Exception -> 0x0203 }
            int r14 = r14 + r13
            r1.f219 = r14     // Catch:{ Exception -> 0x0203 }
            if (r16 == 0) goto L_0x0160
            r14 = 0
            goto L_0x0162
        L_0x0160:
            r14 = 49
        L_0x0162:
            if (r14 == 0) goto L_0x01ab
            int r14 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0203 }
            r8 = 21
            if (r14 < r8) goto L_0x016d
            r8 = 77
            goto L_0x016f
        L_0x016d:
            r8 = 19
        L_0x016f:
            if (r8 == r7) goto L_0x01a9
            int r8 = f204
            int r8 = r8 + 115
            int r14 = r8 % 128
            f205 = r14
            int r8 = r8 % 2
            if (r8 != 0) goto L_0x0180
            r8 = 36
            goto L_0x0182
        L_0x0180:
            r8 = 72
        L_0x0182:
            r14 = 72
            if (r8 == r14) goto L_0x0198
            float r8 = r9.getZ()     // Catch:{ Exception -> 0x0203 }
            float r14 = r4.getZ()     // Catch:{ Exception -> 0x0203 }
            int r8 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r8 <= 0) goto L_0x0194
            r8 = 0
            goto L_0x0195
        L_0x0194:
            r8 = 1
        L_0x0195:
            if (r8 == 0) goto L_0x01ca
            goto L_0x01a9
        L_0x0198:
            float r8 = r9.getZ()     // Catch:{ Exception -> 0x0203 }
            float r14 = r4.getZ()     // Catch:{ Exception -> 0x0203 }
            int r8 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r8 <= 0) goto L_0x01a6
            r8 = 0
            goto L_0x01a7
        L_0x01a6:
            r8 = 1
        L_0x01a7:
            if (r8 == 0) goto L_0x01ca
        L_0x01a9:
            r8 = 0
            goto L_0x01cb
        L_0x01ab:
            int r8 = android.os.Build.VERSION.SDK_INT     // Catch:{ Exception -> 0x0203 }
            r14 = 21
            if (r8 < r14) goto L_0x01b3
            r8 = 0
            goto L_0x01b4
        L_0x01b3:
            r8 = 1
        L_0x01b4:
            if (r8 == r13) goto L_0x01ca
            float r8 = r9.getZ()     // Catch:{ Exception -> 0x0203 }
            float r14 = r4.getZ()     // Catch:{ Exception -> 0x0203 }
            int r8 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r8 < 0) goto L_0x01c5
            r8 = 53
            goto L_0x01c7
        L_0x01c5:
            r8 = 94
        L_0x01c7:
            if (r8 == r12) goto L_0x01ca
            goto L_0x01a9
        L_0x01ca:
            r8 = 1
        L_0x01cb:
            if (r8 == 0) goto L_0x01d2
            r14 = 16
            r8 = 18
            goto L_0x01d6
        L_0x01d2:
            r8 = 18
            r14 = 18
        L_0x01d6:
            if (r14 == r8) goto L_0x01ed
            com.moat.analytics.mobile.cha.u$a r14 = new com.moat.analytics.mobile.cha.u$a     // Catch:{ Exception -> 0x0203 }
            r14.<init>(r9, r3)     // Catch:{ Exception -> 0x0203 }
            r9 = r17
            m371(r14, r9, r1)     // Catch:{ Exception -> 0x0203 }
            boolean r14 = r1.f217     // Catch:{ Exception -> 0x0203 }
            if (r14 == 0) goto L_0x01e8
            r14 = 0
            goto L_0x01e9
        L_0x01e8:
            r14 = 1
        L_0x01e9:
            if (r14 == 0) goto L_0x01ec
            goto L_0x01f6
        L_0x01ec:
            return r1
        L_0x01ed:
            r9 = r17
            goto L_0x01f6
        L_0x01f0:
            r8 = 18
            r9 = r17
            r16 = 1
        L_0x01f6:
            int r2 = r2 + 1
            r8 = 3
            r9 = 49
            goto L_0x0115
        L_0x01fd:
            r3 = r5
            r8 = 3
            r9 = 49
            goto L_0x00ae
        L_0x0203:
            r0 = move-exception
            com.moat.analytics.mobile.cha.o.m306(r0)
        L_0x0207:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.cha.u.m366(android.graphics.Rect, android.view.View):com.moat.analytics.mobile.cha.u$b");
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static int m363(Rect rect, Set<Rect> set) {
        Object next;
        if (set.isEmpty()) {
            return 0;
        }
        ArrayList arrayList = new ArrayList();
        arrayList.addAll(set);
        Collections.sort(arrayList, new Comparator<Rect>() {
            public final /* synthetic */ int compare(Object obj, Object obj2) {
                return Integer.valueOf(((Rect) obj).top).compareTo(Integer.valueOf(((Rect) obj2).top));
            }
        });
        ArrayList arrayList2 = new ArrayList();
        Iterator it2 = arrayList.iterator();
        while (true) {
            if (!(it2.hasNext())) {
                break;
            }
            int i = f204 + 59;
            f205 = i % 128;
            if (!(i % 2 == 0)) {
                next = it2.next();
            } else {
                next = it2.next();
            }
            Rect rect2 = (Rect) next;
            arrayList2.add(Integer.valueOf(rect2.left));
            arrayList2.add(Integer.valueOf(rect2.right));
        }
        Collections.sort(arrayList2);
        int i2 = 0;
        int i3 = 0;
        while (true) {
            if ((i2 < arrayList2.size() - 1 ? '&' : 'U') == 'U') {
                return i3;
            }
            int i4 = i2 + 1;
            if (!(((Integer) arrayList2.get(i2)).equals(arrayList2.get(i4)))) {
                Rect rect3 = new Rect(((Integer) arrayList2.get(i2)).intValue(), rect.top, ((Integer) arrayList2.get(i4)).intValue(), rect.bottom);
                int i5 = rect.top;
                Iterator it3 = arrayList.iterator();
                while (true) {
                    if ((it3.hasNext() ? (char) 25 : 9) == 9) {
                        break;
                    }
                    int i6 = f205 + 23;
                    f204 = i6 % 128;
                    int i7 = i6 % 2;
                    Rect rect4 = (Rect) it3.next();
                    if (Rect.intersects(rect4, rect3)) {
                        if ((rect4.bottom > i5 ? 16 : 'A') == 16) {
                            i3 += rect3.width() * (rect4.bottom - Math.max(i5, rect4.top));
                            i5 = rect4.bottom;
                        }
                        if (rect4.bottom == rect3.bottom) {
                            break;
                        }
                    }
                }
            }
            i2 = i4;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static Rect m364(Rect rect, DisplayMetrics displayMetrics) {
        float f = displayMetrics.density;
        if (!(f != 0.0f)) {
            return rect;
        }
        return new Rect(Math.round(((float) rect.left) / f), Math.round(((float) rect.top) / f), Math.round(((float) rect.right) / f), Math.round(((float) rect.bottom) / f));
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    private static boolean m367(View view, Rect rect) {
        if ((view.getGlobalVisibleRect(rect) ? 'Z' : 1) != 'Z') {
            return false;
        }
        int[] iArr = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationInWindow(iArr);
        int[] iArr2 = {Integer.MIN_VALUE, Integer.MIN_VALUE};
        view.getLocationOnScreen(iArr2);
        rect.offset(iArr2[0] - iArr[0], iArr2[1] - iArr[1]);
        return true;
    }
}
