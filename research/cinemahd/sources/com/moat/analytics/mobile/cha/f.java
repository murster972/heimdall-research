package com.moat.analytics.mobile.cha;

import android.app.Application;
import android.content.Context;
import com.moat.analytics.mobile.cha.a;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class f extends MoatAnalytics implements t.b {

    /* renamed from: ʻ  reason: contains not printable characters */
    private boolean f54 = false;

    /* renamed from: ʼ  reason: contains not printable characters */
    private String f55;

    /* renamed from: ʽ  reason: contains not printable characters */
    private MoatOptions f56;

    /* renamed from: ˊ  reason: contains not printable characters */
    WeakReference<Context> f57;

    /* renamed from: ˋ  reason: contains not printable characters */
    boolean f58 = false;

    /* renamed from: ˎ  reason: contains not printable characters */
    boolean f59 = false;

    /* renamed from: ˏ  reason: contains not printable characters */
    boolean f60 = false;

    /* renamed from: ॱ  reason: contains not printable characters */
    a f61;

    f() {
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    private void m220() {
        if (this.f61 == null) {
            this.f61 = new a(c.m203(), a.d.f21);
            this.f61.m186(this.f55);
            a.m182(3, "Analytics", this, "Preparing native display tracking with partner code " + this.f55);
            a.m179("[SUCCESS] ", "Prepared for native display tracking with partner code " + this.f55);
        }
    }

    public final void prepareNativeDisplayTracking(String str) {
        this.f55 = str;
        if (t.m350().f182 != t.a.f194) {
            try {
                m220();
            } catch (Exception e) {
                o.m306(e);
            }
        }
    }

    public final void start(Application application) {
        start(new MoatOptions(), application);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final boolean m221() {
        return this.f54;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final boolean m222() {
        MoatOptions moatOptions = this.f56;
        return moatOptions != null && moatOptions.disableLocationServices;
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    public final void m223() throws o {
        o.m307();
        n.m292();
        if (this.f55 != null) {
            try {
                m220();
            } catch (Exception e) {
                o.m306(e);
            }
        }
    }

    public final void start(MoatOptions moatOptions, Application application) {
        try {
            if (this.f54) {
                a.m182(3, "Analytics", this, "Moat SDK has already been started.");
                return;
            }
            this.f56 = moatOptions;
            t.m350().m356();
            this.f58 = moatOptions.disableLocationServices;
            if (application != null) {
                if (moatOptions.loggingEnabled && r.m325(application.getApplicationContext())) {
                    this.f59 = true;
                }
                this.f57 = new WeakReference<>(application.getApplicationContext());
                this.f54 = true;
                this.f60 = moatOptions.autoTrackGMAInterstitials;
                c.m205(application);
                t.m350().m355((t.b) this);
                if (!moatOptions.disableAdIdCollection) {
                    r.m328(application);
                }
                a.m179("[SUCCESS] ", "Moat Analytics SDK Version 2.4.1 started");
                return;
            }
            throw new o("Moat Analytics SDK didn't start, application was null");
        } catch (Exception e) {
            o.m306(e);
        }
    }
}
