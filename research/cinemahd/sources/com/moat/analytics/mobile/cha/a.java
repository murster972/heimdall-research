package com.moat.analytics.mobile.cha;

import android.annotation.SuppressLint;
import android.app.Application;
import android.os.Build;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.facebook.ads.AudienceNetworkActivity;
import com.moat.analytics.mobile.cha.j;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

final class a {

    /* renamed from: ˊ  reason: contains not printable characters */
    final String f13;

    /* renamed from: ˋ  reason: contains not printable characters */
    WebView f14;
    /* access modifiers changed from: private */

    /* renamed from: ˎ  reason: contains not printable characters */
    public boolean f15;

    /* renamed from: ˏ  reason: contains not printable characters */
    j f16;

    /* renamed from: ॱ  reason: contains not printable characters */
    private final int f17;

    enum d {
        ;
        

        /* renamed from: ˋ  reason: contains not printable characters */
        public static final int f20 = 2;

        /* renamed from: ˏ  reason: contains not printable characters */
        public static final int f21 = 1;
    }

    @SuppressLint({"SetJavaScriptEnabled"})
    a(Application application, int i) {
        this.f17 = i;
        this.f15 = false;
        this.f13 = String.format(Locale.ROOT, "_moatTracker%d", new Object[]{Integer.valueOf((int) (Math.random() * 1.0E8d))});
        this.f14 = new WebView(application);
        WebSettings settings = this.f14.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setAllowContentAccess(false);
        settings.setAllowFileAccess(false);
        settings.setDatabaseEnabled(false);
        settings.setDomStorageEnabled(false);
        settings.setGeolocationEnabled(false);
        settings.setJavaScriptCanOpenWindowsAutomatically(false);
        settings.setSaveFormData(false);
        if (Build.VERSION.SDK_INT >= 16) {
            settings.setAllowFileAccessFromFileURLs(false);
            settings.setAllowUniversalAccessFromFileURLs(false);
        }
        if (Build.VERSION.SDK_INT >= 21) {
            settings.setMixedContentMode(1);
        }
        try {
            this.f16 = new j(this.f14, i == d.f20 ? j.e.f118 : j.e.f119);
        } catch (o e) {
            o.m306(e);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m182(int i, String str, Object obj, String str2) {
        if (!t.m350().f183) {
            return;
        }
        if (obj == null) {
            Log.println(i, "Moat" + str, String.format("message = %s", new Object[]{str2}));
            return;
        }
        Log.println(i, "Moat" + str, String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}));
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m186(String str) {
        if (this.f17 == d.f21) {
            this.f14.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f15) {
                        try {
                            boolean unused = a.this.f15 = true;
                            a.this.f16.m275();
                        } catch (Exception e) {
                            o.m306(e);
                        }
                    }
                }
            });
            WebView webView = this.f14;
            webView.loadData("<!DOCTYPE html>\n<html>\n<head lang=\"en\">\n   <meta charset=\"UTF-8\">\n   <title></title>\n</head>\n<body style=\"margin:0;padding:0;\">\n    <script src=\"https://z.moatads.com/" + str + "/moatad.js\" type=\"text/javascript\"></script>\n</body>\n</html>", AudienceNetworkActivity.WEBVIEW_MIME_TYPE, AudienceNetworkActivity.WEBVIEW_ENCODING);
        }
    }

    /* renamed from: ॱ  reason: contains not printable characters */
    static void m184(String str, Object obj, String str2, Exception exc) {
        if (t.m350().f183) {
            Log.e("Moat" + str, String.format("id = %s, message = %s", new Object[]{Integer.valueOf(obj.hashCode()), str2}), exc);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static void m183(String str, Object obj, String str2) {
        Object obj2;
        if (t.m350().f180) {
            String str3 = "Moat" + str;
            Object[] objArr = new Object[2];
            if (obj == null) {
                obj2 = "null";
            } else {
                obj2 = Integer.valueOf(obj.hashCode());
            }
            objArr[0] = obj2;
            objArr[1] = str2;
            Log.println(2, str3, String.format("id = %s, message = %s", objArr));
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˊ  reason: contains not printable characters */
    public final void m187(String str, Map<String, String> map, Integer num, Integer num2, Integer num3) {
        if (this.f17 == d.f20) {
            this.f14.setWebViewClient(new WebViewClient() {
                public final void onPageFinished(WebView webView, String str) {
                    if (!a.this.f15) {
                        try {
                            boolean unused = a.this.f15 = true;
                            a.this.f16.m275();
                            a.this.f16.m274(a.this.f13);
                        } catch (Exception e) {
                            o.m306(e);
                        }
                    }
                }
            });
            JSONObject jSONObject = new JSONObject(map);
            WebView webView = this.f14;
            String str2 = this.f13;
            webView.loadData(String.format(Locale.ROOT, "<html><head></head><body><div id=\"%s\" style=\"width: %dpx; height: %dpx;\"></div><script>(function initMoatTracking(apiname, pcode, ids, duration) {var events = [];window[pcode + '_moatElToTrack'] = document.getElementById('%s');var moatapi = {'dropTime':%d,'adData': {'ids': ids, 'duration': duration, 'url': 'n/a'},'dispatchEvent': function(ev) {if (this.sendEvent) {if (events) { events.push(ev); ev = events; events = false; }this.sendEvent(ev);} else {events.push(ev);}},'dispatchMany': function(evs){for (var i=0, l=evs.length; i<l; i++) {this.dispatchEvent(evs[i]);}}};Object.defineProperty(window, apiname, {'value': moatapi});var s = document.createElement('script');s.src = 'https://z.moatads.com/' + pcode + '/moatvideo.js?' + apiname + '#' + apiname;document.body.appendChild(s);})('%s', '%s', %s, %s);</script></body></html>", new Object[]{"mianahwvc", num, num2, "mianahwvc", Long.valueOf(System.currentTimeMillis()), str2, str, jSONObject.toString(), num3}), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null);
        }
    }

    /* renamed from: ˏ  reason: contains not printable characters */
    static String m181(View view) {
        if (view == null) {
            return "null";
        }
        return view.getClass().getSimpleName() + "@" + view.hashCode();
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static void m179(String str, String str2) {
        if (!t.m350().f183 && ((f) MoatAnalytics.getInstance()).f59) {
            int i = 2;
            if (str.equals("[ERROR] ")) {
                i = 6;
            }
            Log.println(i, "MoatAnalytics", str + str2);
        }
    }

    a() {
    }
}
