package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.app.Application;
import android.media.MediaPlayer;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.NativeDisplayTracker;
import java.util.Map;

abstract class NoOp {

    public static class MoatAnalytics extends MoatAnalytics {
        public void prepareNativeDisplayTracking(String str) {
        }

        public void start(Application application) {
        }

        public void start(MoatOptions moatOptions, Application application) {
        }
    }

    public static class MoatFactory extends MoatFactory {
        public <T> T createCustomTracker(l<T> lVar) {
            return lVar.createNoOp();
        }

        public NativeDisplayTracker createNativeDisplayTracker(View view, Map<String, String> map) {
            return new c();
        }

        public NativeVideoTracker createNativeVideoTracker(String str) {
            return new b();
        }

        public WebAdTracker createWebAdTracker(WebView webView) {
            return new e();
        }

        public WebAdTracker createWebAdTracker(ViewGroup viewGroup) {
            return new e();
        }
    }

    static class b implements NativeVideoTracker {
        b() {
        }

        public final void changeTargetView(View view) {
        }

        public final void dispatchEvent(MoatAdEvent moatAdEvent) {
        }

        public final void removeListener() {
        }

        public final void removeVideoListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void setPlayerVolume(Double d) {
        }

        public final void setVideoListener(VideoTrackerListener videoTrackerListener) {
        }

        public final void stopTracking() {
        }

        public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
            return false;
        }
    }

    static class c implements NativeDisplayTracker {
        c() {
        }

        public final void removeListener() {
        }

        public final void reportUserInteractionEvent(NativeDisplayTracker.MoatUserInteractionType moatUserInteractionType) {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void startTracking() {
        }

        public final void stopTracking() {
        }
    }

    static class e implements WebAdTracker {
        e() {
        }

        public final void removeListener() {
        }

        public final void setActivity(Activity activity) {
        }

        public final void setListener(TrackerListener trackerListener) {
        }

        public final void startTracking() {
        }

        public final void stopTracking() {
        }
    }

    NoOp() {
    }
}
