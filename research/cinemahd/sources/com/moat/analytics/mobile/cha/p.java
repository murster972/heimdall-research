package com.moat.analytics.mobile.cha;

import com.moat.analytics.mobile.cha.base.asserts.Asserts;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

class p<T> implements InvocationHandler {
    /* access modifiers changed from: private */

    /* renamed from: ˋ  reason: contains not printable characters */
    public static final Object[] f144 = new Object[0];

    /* renamed from: ˊ  reason: contains not printable characters */
    private final c<T> f145;

    /* renamed from: ˎ  reason: contains not printable characters */
    private boolean f146;

    /* renamed from: ˏ  reason: contains not printable characters */
    private final LinkedList<p<T>.d> f147 = new LinkedList<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final Class<T> f148;

    /* renamed from: ᐝ  reason: contains not printable characters */
    private T f149;

    interface c<T> {
        /* renamed from: ˋ  reason: contains not printable characters */
        Optional<T> m314() throws o;
    }

    class d {
        /* access modifiers changed from: private */

        /* renamed from: ˊ  reason: contains not printable characters */
        public final WeakReference[] f151;
        /* access modifiers changed from: private */

        /* renamed from: ˋ  reason: contains not printable characters */
        public final Method f152;

        /* renamed from: ˎ  reason: contains not printable characters */
        private final LinkedList<Object> f153;

        /* synthetic */ d(p pVar, Method method, Object[] objArr, byte b) {
            this(method, objArr);
        }

        private d(Method method, Object... objArr) {
            this.f153 = new LinkedList<>();
            objArr = objArr == null ? p.f144 : objArr;
            WeakReference[] weakReferenceArr = new WeakReference[objArr.length];
            int length = objArr.length;
            int i = 0;
            int i2 = 0;
            while (i < length) {
                Object obj = objArr[i];
                if ((obj instanceof Map) || (obj instanceof Integer) || (obj instanceof Double)) {
                    this.f153.add(obj);
                }
                weakReferenceArr[i2] = new WeakReference(obj);
                i++;
                i2++;
            }
            this.f151 = weakReferenceArr;
            this.f152 = method;
        }
    }

    private p(c<T> cVar, Class<T> cls) throws o {
        Asserts.checkNotNull(cVar);
        Asserts.checkNotNull(cls);
        this.f145 = cVar;
        this.f148 = cls;
        t.m350().m355((t.b) new t.b() {
            /* renamed from: ˎ  reason: contains not printable characters */
            public final void m313() throws o {
                p.this.m310();
            }
        });
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static Boolean m308(Method method) {
        try {
            if (Boolean.TYPE.equals(method.getReturnType())) {
                return true;
            }
            return null;
        } catch (Exception e) {
            o.m306(e);
            return null;
        }
    }

    /* renamed from: ˋ  reason: contains not printable characters */
    static <T> T m309(c<T> cVar, Class<T> cls) throws o {
        ClassLoader classLoader = cls.getClassLoader();
        p pVar = new p(cVar, cls);
        return Proxy.newProxyInstance(classLoader, new Class[]{cls}, pVar);
    }

    public Object invoke(Object obj, Method method, Object[] objArr) throws Throwable {
        try {
            Class<?> declaringClass = method.getDeclaringClass();
            t r0 = t.m350();
            if (Object.class.equals(declaringClass)) {
                String name = method.getName();
                if ("getClass".equals(name)) {
                    return this.f148;
                }
                if (!"toString".equals(name)) {
                    return method.invoke(this, objArr);
                }
                Object invoke = method.invoke(this, objArr);
                return String.valueOf(invoke).replace(p.class.getName(), this.f148.getName());
            } else if (!this.f146 || this.f149 != null) {
                if (r0.f182 == t.a.f193) {
                    m310();
                    if (this.f149 != null) {
                        return method.invoke(this.f149, objArr);
                    }
                }
                if (r0.f182 == t.a.f194 && (!this.f146 || this.f149 != null)) {
                    if (this.f147.size() >= 15) {
                        this.f147.remove(5);
                    }
                    this.f147.add(new d(this, method, objArr, (byte) 0));
                }
                return m308(method);
            } else {
                this.f147.clear();
                return m308(method);
            }
        } catch (Exception e) {
            o.m306(e);
            return m308(method);
        }
    }

    /* access modifiers changed from: private */
    /* renamed from: ˎ  reason: contains not printable characters */
    public void m310() throws o {
        if (!this.f146) {
            try {
                this.f149 = this.f145.m314().orElse(null);
            } catch (Exception e) {
                a.m184("OnOffTrackerProxy", this, "Could not create instance", e);
                o.m306(e);
            }
            this.f146 = true;
        }
        if (this.f149 != null) {
            Iterator it2 = this.f147.iterator();
            while (it2.hasNext()) {
                d dVar = (d) it2.next();
                try {
                    Object[] objArr = new Object[dVar.f151.length];
                    WeakReference[] r3 = dVar.f151;
                    int length = r3.length;
                    int i = 0;
                    int i2 = 0;
                    while (i < length) {
                        objArr[i2] = r3[i].get();
                        i++;
                        i2++;
                    }
                    dVar.f152.invoke(this.f149, objArr);
                } catch (Exception e2) {
                    o.m306(e2);
                }
            }
            this.f147.clear();
        }
    }
}
