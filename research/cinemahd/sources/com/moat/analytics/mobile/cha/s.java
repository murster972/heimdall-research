package com.moat.analytics.mobile.cha;

import android.media.MediaPlayer;
import android.view.View;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

final class s extends i implements NativeVideoTracker {

    /* renamed from: ॱˊ  reason: contains not printable characters */
    private WeakReference<MediaPlayer> f174;

    s(String str) {
        super(str);
        a.m182(3, "NativeVideoTracker", this, "In initialization method.");
        if (str == null || str.isEmpty()) {
            StringBuilder sb = new StringBuilder("PartnerCode is ");
            sb.append(str == null ? "null" : "empty");
            String sb2 = sb.toString();
            String str2 = "NativeDisplayTracker creation problem, " + sb2;
            a.m182(3, "NativeVideoTracker", this, str2);
            a.m179("[ERROR] ", str2);
            this.f50 = new o(sb2);
        }
        a.m179("[SUCCESS] ", "NativeVideoTracker created");
    }

    public final boolean trackVideoAd(Map<String, String> map, MediaPlayer mediaPlayer, View view) {
        try {
            m214();
            m216();
            if (mediaPlayer != null) {
                mediaPlayer.getCurrentPosition();
                this.f174 = new WeakReference<>(mediaPlayer);
                return super.m246(map, view);
            }
            throw new o("Null player instance");
        } catch (Exception unused) {
            throw new o("Playback has already completed");
        } catch (Exception e) {
            o.m306(e);
            String r2 = o.m305("trackVideoAd", e);
            TrackerListener trackerListener = this.f44;
            if (trackerListener != null) {
                trackerListener.onTrackingFailedToStart(r2);
            }
            a.m182(3, "NativeVideoTracker", this, r2);
            a.m179("[ERROR] ", "NativeVideoTracker " + r2);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m336() {
        return "NativeVideoTracker";
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final void m337(List<String> list) throws o {
        WeakReference<MediaPlayer> weakReference = this.f174;
        if (!((weakReference == null || weakReference.get() == null) ? false : true)) {
            list.add("Player is null");
        }
        super.m192(list);
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ͺ  reason: contains not printable characters */
    public final boolean m338() {
        WeakReference<MediaPlayer> weakReference = this.f174;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˋ  reason: contains not printable characters */
    public final Integer m339() {
        return Integer.valueOf(((MediaPlayer) this.f174.get()).getCurrentPosition());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱˎ  reason: contains not printable characters */
    public final boolean m340() {
        return ((MediaPlayer) this.f174.get()).isPlaying();
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ॱᐝ  reason: contains not printable characters */
    public final Integer m341() {
        return Integer.valueOf(((MediaPlayer) this.f174.get()).getDuration());
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ᐝ  reason: contains not printable characters */
    public final Map<String, Object> m342() throws o {
        MediaPlayer mediaPlayer = (MediaPlayer) this.f174.get();
        HashMap hashMap = new HashMap();
        hashMap.put("width", Integer.valueOf(mediaPlayer.getVideoWidth()));
        hashMap.put("height", Integer.valueOf(mediaPlayer.getVideoHeight()));
        hashMap.put("duration", Integer.valueOf(mediaPlayer.getDuration()));
        return hashMap;
    }
}
