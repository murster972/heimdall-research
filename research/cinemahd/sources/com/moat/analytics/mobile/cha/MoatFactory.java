package com.moat.analytics.mobile.cha;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.NoOp;
import java.util.Map;

public abstract class MoatFactory {
    public static MoatFactory create() {
        try {
            return new k();
        } catch (Exception e) {
            o.m306(e);
            return new NoOp.MoatFactory();
        }
    }

    public abstract <T> T createCustomTracker(l<T> lVar);

    public abstract NativeDisplayTracker createNativeDisplayTracker(View view, Map<String, String> map);

    public abstract NativeVideoTracker createNativeVideoTracker(String str);

    public abstract WebAdTracker createWebAdTracker(ViewGroup viewGroup);

    public abstract WebAdTracker createWebAdTracker(WebView webView);
}
