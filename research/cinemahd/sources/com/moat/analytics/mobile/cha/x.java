package com.moat.analytics.mobile.cha;

import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.LinkedList;

final class x {

    /* renamed from: ˋ  reason: contains not printable characters */
    private static final LinkedHashSet<String> f224 = new LinkedHashSet<>();

    x() {
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    static Optional<WebView> m378(ViewGroup viewGroup, boolean z) {
        if (viewGroup == null) {
            try {
                return Optional.empty();
            } catch (Exception unused) {
                return Optional.empty();
            }
        } else if (viewGroup instanceof WebView) {
            return Optional.of((WebView) viewGroup);
        } else {
            LinkedList linkedList = new LinkedList();
            linkedList.add(viewGroup);
            WebView webView = null;
            int i = 0;
            while (!linkedList.isEmpty() && i < 100) {
                i++;
                ViewGroup viewGroup2 = (ViewGroup) linkedList.poll();
                int childCount = viewGroup2.getChildCount();
                WebView webView2 = webView;
                int i2 = 0;
                while (true) {
                    if (i2 >= childCount) {
                        webView = webView2;
                        break;
                    }
                    View childAt = viewGroup2.getChildAt(i2);
                    if (childAt instanceof WebView) {
                        a.m182(3, "WebViewHound", childAt, "Found WebView");
                        if (z || m379(String.valueOf(childAt.hashCode()))) {
                            if (webView2 != null) {
                                a.m182(3, "WebViewHound", childAt, "Ambiguous ad container: multiple WebViews reside within it.");
                                a.m179("[ERROR] ", "WebAdTracker not created, ambiguous ad container: multiple WebViews reside within it");
                                webView = null;
                                break;
                            }
                            webView2 = (WebView) childAt;
                        }
                    }
                    if (childAt instanceof ViewGroup) {
                        linkedList.add((ViewGroup) childAt);
                    }
                    i2++;
                }
            }
            return Optional.ofNullable(webView);
        }
    }

    /* renamed from: ˊ  reason: contains not printable characters */
    private static boolean m379(String str) {
        try {
            boolean add = f224.add(str);
            if (f224.size() > 50) {
                Iterator it2 = f224.iterator();
                for (int i = 0; i < 25 && it2.hasNext(); i++) {
                    it2.next();
                    it2.remove();
                }
            }
            a.m182(3, "WebViewHound", (Object) null, add ? "Newly Found WebView" : "Already Found WebView");
            return add;
        } catch (Exception e) {
            o.m306(e);
            return false;
        }
    }
}
