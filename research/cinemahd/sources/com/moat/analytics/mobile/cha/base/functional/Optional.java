package com.moat.analytics.mobile.cha.base.functional;

import java.util.NoSuchElementException;

public final class Optional<T> {

    /* renamed from: ˏ  reason: contains not printable characters */
    private static final Optional<?> f34 = new Optional<>();

    /* renamed from: ॱ  reason: contains not printable characters */
    private final T f35;

    private Optional() {
        this.f35 = null;
    }

    public static <T> Optional<T> empty() {
        return f34;
    }

    public static <T> Optional<T> of(T t) {
        return new Optional<>(t);
    }

    public static <T> Optional<T> ofNullable(T t) {
        return t == null ? empty() : of(t);
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Optional)) {
            return false;
        }
        T t = this.f35;
        T t2 = ((Optional) obj).f35;
        return t == t2 || !(t == null || t2 == null || !t.equals(t2));
    }

    public final T get() {
        T t = this.f35;
        if (t != null) {
            return t;
        }
        throw new NoSuchElementException("No value present");
    }

    public final int hashCode() {
        T t = this.f35;
        if (t == null) {
            return 0;
        }
        return t.hashCode();
    }

    public final boolean isPresent() {
        return this.f35 != null;
    }

    public final T orElse(T t) {
        T t2 = this.f35;
        return t2 != null ? t2 : t;
    }

    public final String toString() {
        T t = this.f35;
        if (t == null) {
            return "Optional.empty";
        }
        return String.format("Optional[%s]", new Object[]{t});
    }

    private Optional(T t) {
        if (t != null) {
            this.f35 = t;
            return;
        }
        throw new NullPointerException("Optional of null value.");
    }
}
