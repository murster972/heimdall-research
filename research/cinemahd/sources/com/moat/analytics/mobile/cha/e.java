package com.moat.analytics.mobile.cha;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import com.google.android.gms.ads.AdActivity;
import com.moat.analytics.mobile.cha.base.functional.Optional;
import com.moat.analytics.mobile.cha.t;
import java.lang.ref.WeakReference;

final class e {

    /* renamed from: ˊ  reason: contains not printable characters */
    private static WeakReference<Activity> f52 = new WeakReference<>((Object) null);

    /* renamed from: ˋ  reason: contains not printable characters */
    private static WebAdTracker f53;

    e() {
    }

    /* renamed from: ˎ  reason: contains not printable characters */
    static void m219(Activity activity) {
        try {
            if (t.m350().f182 != t.a.f194) {
                String name = activity.getClass().getName();
                a.m182(3, "GMAInterstitialHelper", activity, "Activity name: " + name);
                if (!name.contains(AdActivity.CLASS_NAME)) {
                    if (f53 != null) {
                        a.m182(3, "GMAInterstitialHelper", f52.get(), "Stopping to track GMA interstitial");
                        f53.stopTracking();
                        f53 = null;
                    }
                    f52 = new WeakReference<>((Object) null);
                } else if (f52.get() == null || f52.get() != activity) {
                    View decorView = activity.getWindow().getDecorView();
                    if (decorView instanceof ViewGroup) {
                        Optional<WebView> r1 = x.m378((ViewGroup) decorView, true);
                        if (r1.isPresent()) {
                            f52 = new WeakReference<>(activity);
                            a.m182(3, "GMAInterstitialHelper", f52.get(), "Starting to track GMA interstitial");
                            WebAdTracker createWebAdTracker = MoatFactory.create().createWebAdTracker(r1.get());
                            f53 = createWebAdTracker;
                            createWebAdTracker.startTracking();
                            return;
                        }
                        a.m182(3, "GMAInterstitialHelper", activity, "Sorry, no WebView in this activity");
                    }
                }
            }
        } catch (Exception e) {
            o.m306(e);
        }
    }
}
