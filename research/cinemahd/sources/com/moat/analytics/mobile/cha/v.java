package com.moat.analytics.mobile.cha;

import android.view.ViewGroup;
import android.webkit.WebView;

final class v extends d implements WebAdTracker {
    v(ViewGroup viewGroup) {
        this(x.m378(viewGroup, false).orElse(null));
        if (viewGroup == null) {
            String str = "WebAdTracker initialization not successful, " + "Target ViewGroup is null";
            a.m182(3, "WebAdTracker", this, str);
            a.m179("[ERROR] ", str);
            this.f50 = new o("Target ViewGroup is null");
        }
        if (this.f48 == null) {
            String str2 = "WebAdTracker initialization not successful, " + "No WebView to track inside of ad container";
            a.m182(3, "WebAdTracker", this, str2);
            a.m179("[ERROR] ", str2);
            this.f50 = new o("No WebView to track inside of ad container");
        }
    }

    /* access modifiers changed from: package-private */
    /* renamed from: ˋ  reason: contains not printable characters */
    public final String m373() {
        return "WebAdTracker";
    }

    v(WebView webView) {
        super(webView, false, false);
        a.m182(3, "WebAdTracker", this, "Initializing.");
        if (webView == null) {
            String str = "WebAdTracker initialization not successful, " + "WebView is null";
            a.m182(3, "WebAdTracker", this, str);
            a.m179("[ERROR] ", str);
            this.f50 = new o("WebView is null");
            return;
        }
        try {
            super.m217(webView);
            a.m179("[SUCCESS] ", "WebAdTracker created for " + m208());
        } catch (o e) {
            this.f50 = e;
        }
    }
}
