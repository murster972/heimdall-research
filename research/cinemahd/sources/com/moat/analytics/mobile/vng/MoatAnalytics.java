package com.moat.analytics.mobile.vng;

import android.app.Application;
import com.moat.analytics.mobile.vng.v;

public abstract class MoatAnalytics {

    /* renamed from: a  reason: collision with root package name */
    private static MoatAnalytics f4949a;

    public static synchronized MoatAnalytics getInstance() {
        MoatAnalytics moatAnalytics;
        synchronized (MoatAnalytics.class) {
            if (f4949a == null) {
                try {
                    f4949a = new k();
                } catch (Exception e) {
                    n.a(e);
                    f4949a = new v.a();
                }
            }
            moatAnalytics = f4949a;
        }
        return moatAnalytics;
    }

    public abstract void prepareNativeDisplayTracking(String str);

    public abstract void start(Application application);

    public abstract void start(MoatOptions moatOptions, Application application);
}
