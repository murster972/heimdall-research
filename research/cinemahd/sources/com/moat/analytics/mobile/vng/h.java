package com.moat.analytics.mobile.vng;

import android.view.View;
import java.util.Map;
import org.json.JSONObject;

abstract class h extends c {
    int l = Integer.MIN_VALUE;
    private a m = a.UNINITIALIZED;
    private int n = Integer.MIN_VALUE;
    private double o = Double.NaN;
    private int p = Integer.MIN_VALUE;
    private int q = 0;

    enum a {
        UNINITIALIZED,
        PAUSED,
        PLAYING,
        STOPPED,
        COMPLETED
    }

    h(String str) {
        super(str);
    }

    private void t() {
        this.i.postDelayed(new Runnable() {
            public void run() {
                h hVar;
                try {
                    if (!h.this.n() || h.this.m()) {
                        hVar = h.this;
                    } else if (Boolean.valueOf(h.this.s()).booleanValue()) {
                        h.this.i.postDelayed(this, 200);
                        return;
                    } else {
                        hVar = h.this;
                    }
                    hVar.l();
                } catch (Exception e) {
                    h.this.l();
                    n.a(e);
                }
            }
        }, 200);
    }

    /* access modifiers changed from: package-private */
    public JSONObject a(MoatAdEvent moatAdEvent) {
        Integer num;
        int i;
        if (!moatAdEvent.b.equals(MoatAdEvent.f4947a)) {
            num = moatAdEvent.b;
        } else {
            try {
                num = o();
            } catch (Exception unused) {
                num = Integer.valueOf(this.n);
            }
            moatAdEvent.b = num;
        }
        if (moatAdEvent.b.intValue() < 0 || (moatAdEvent.b.intValue() == 0 && moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE && this.n > 0)) {
            num = Integer.valueOf(this.n);
            moatAdEvent.b = num;
        }
        if (moatAdEvent.d == MoatAdEventType.AD_EVT_COMPLETE) {
            if (num.intValue() == Integer.MIN_VALUE || (i = this.l) == Integer.MIN_VALUE || !a(num, Integer.valueOf(i))) {
                this.m = a.STOPPED;
                moatAdEvent.d = MoatAdEventType.AD_EVT_STOPPED;
            } else {
                this.m = a.COMPLETED;
            }
        }
        return super.a(moatAdEvent);
    }

    public boolean a(Map<String, String> map, View view) {
        try {
            boolean a2 = super.a(map, view);
            if (!a2 || !p()) {
                return a2;
            }
            t();
            return a2;
        } catch (Exception e) {
            p.a(3, "IntervalVideoTracker", (Object) this, "Problem with video loop");
            a("trackVideoAd", e);
            return false;
        }
    }

    /* access modifiers changed from: package-private */
    public abstract boolean n();

    /* access modifiers changed from: package-private */
    public abstract Integer o();

    /* access modifiers changed from: protected */
    public boolean p() {
        return true;
    }

    /* access modifiers changed from: package-private */
    public abstract boolean q();

    /* access modifiers changed from: package-private */
    public abstract Integer r();

    /* access modifiers changed from: package-private */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0097 A[Catch:{ Exception -> 0x00ce }] */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0099 A[Catch:{ Exception -> 0x00ce }] */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00b9 A[Catch:{ Exception -> 0x00ce }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean s() {
        /*
            r12 = this;
            boolean r0 = r12.n()
            r1 = 0
            if (r0 == 0) goto L_0x00db
            boolean r0 = r12.m()
            if (r0 == 0) goto L_0x000f
            goto L_0x00db
        L_0x000f:
            r0 = 1
            java.lang.Integer r2 = r12.o()     // Catch:{ Exception -> 0x00ce }
            int r2 = r2.intValue()     // Catch:{ Exception -> 0x00ce }
            int r3 = r12.n     // Catch:{ Exception -> 0x00ce }
            if (r3 < 0) goto L_0x001f
            if (r2 >= 0) goto L_0x001f
            return r1
        L_0x001f:
            r12.n = r2     // Catch:{ Exception -> 0x00ce }
            if (r2 != 0) goto L_0x0024
            return r0
        L_0x0024:
            java.lang.Integer r3 = r12.r()     // Catch:{ Exception -> 0x00ce }
            int r3 = r3.intValue()     // Catch:{ Exception -> 0x00ce }
            boolean r4 = r12.q()     // Catch:{ Exception -> 0x00ce }
            double r5 = (double) r3     // Catch:{ Exception -> 0x00ce }
            r7 = 4616189618054758400(0x4010000000000000, double:4.0)
            double r5 = r5 / r7
            java.lang.Double r7 = r12.j()     // Catch:{ Exception -> 0x00ce }
            double r7 = r7.doubleValue()     // Catch:{ Exception -> 0x00ce }
            r9 = 0
            int r10 = r12.p     // Catch:{ Exception -> 0x00ce }
            if (r2 <= r10) goto L_0x0043
            r12.p = r2     // Catch:{ Exception -> 0x00ce }
        L_0x0043:
            int r10 = r12.l     // Catch:{ Exception -> 0x00ce }
            r11 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r10 != r11) goto L_0x004b
            r12.l = r3     // Catch:{ Exception -> 0x00ce }
        L_0x004b:
            if (r4 == 0) goto L_0x008a
            com.moat.analytics.mobile.vng.h$a r3 = r12.m     // Catch:{ Exception -> 0x00ce }
            com.moat.analytics.mobile.vng.h$a r4 = com.moat.analytics.mobile.vng.h.a.UNINITIALIZED     // Catch:{ Exception -> 0x00ce }
            if (r3 != r4) goto L_0x005a
            com.moat.analytics.mobile.vng.MoatAdEventType r9 = com.moat.analytics.mobile.vng.MoatAdEventType.AD_EVT_START     // Catch:{ Exception -> 0x00ce }
            com.moat.analytics.mobile.vng.h$a r3 = com.moat.analytics.mobile.vng.h.a.PLAYING     // Catch:{ Exception -> 0x00ce }
        L_0x0057:
            r12.m = r3     // Catch:{ Exception -> 0x00ce }
            goto L_0x0095
        L_0x005a:
            com.moat.analytics.mobile.vng.h$a r3 = r12.m     // Catch:{ Exception -> 0x00ce }
            com.moat.analytics.mobile.vng.h$a r4 = com.moat.analytics.mobile.vng.h.a.PAUSED     // Catch:{ Exception -> 0x00ce }
            if (r3 != r4) goto L_0x0065
            com.moat.analytics.mobile.vng.MoatAdEventType r9 = com.moat.analytics.mobile.vng.MoatAdEventType.AD_EVT_PLAYING     // Catch:{ Exception -> 0x00ce }
            com.moat.analytics.mobile.vng.h$a r3 = com.moat.analytics.mobile.vng.h.a.PLAYING     // Catch:{ Exception -> 0x00ce }
            goto L_0x0057
        L_0x0065:
            double r3 = (double) r2     // Catch:{ Exception -> 0x00ce }
            double r3 = r3 / r5
            double r3 = java.lang.Math.floor(r3)     // Catch:{ Exception -> 0x00ce }
            int r3 = (int) r3     // Catch:{ Exception -> 0x00ce }
            int r3 = r3 - r0
            r4 = -1
            if (r3 <= r4) goto L_0x0095
            r4 = 3
            if (r3 >= r4) goto L_0x0095
            com.moat.analytics.mobile.vng.MoatAdEventType[] r4 = com.moat.analytics.mobile.vng.c.g     // Catch:{ Exception -> 0x00ce }
            r3 = r4[r3]     // Catch:{ Exception -> 0x00ce }
            java.util.Map<com.moat.analytics.mobile.vng.MoatAdEventType, java.lang.Integer> r4 = r12.h     // Catch:{ Exception -> 0x00ce }
            boolean r4 = r4.containsKey(r3)     // Catch:{ Exception -> 0x00ce }
            if (r4 != 0) goto L_0x0095
            java.util.Map<com.moat.analytics.mobile.vng.MoatAdEventType, java.lang.Integer> r4 = r12.h     // Catch:{ Exception -> 0x00ce }
            java.lang.Integer r5 = java.lang.Integer.valueOf(r0)     // Catch:{ Exception -> 0x00ce }
            r4.put(r3, r5)     // Catch:{ Exception -> 0x00ce }
            r9 = r3
            goto L_0x0095
        L_0x008a:
            com.moat.analytics.mobile.vng.h$a r3 = r12.m     // Catch:{ Exception -> 0x00ce }
            com.moat.analytics.mobile.vng.h$a r4 = com.moat.analytics.mobile.vng.h.a.PAUSED     // Catch:{ Exception -> 0x00ce }
            if (r3 == r4) goto L_0x0095
            com.moat.analytics.mobile.vng.MoatAdEventType r9 = com.moat.analytics.mobile.vng.MoatAdEventType.AD_EVT_PAUSED     // Catch:{ Exception -> 0x00ce }
            com.moat.analytics.mobile.vng.h$a r3 = com.moat.analytics.mobile.vng.h.a.PAUSED     // Catch:{ Exception -> 0x00ce }
            goto L_0x0057
        L_0x0095:
            if (r9 == 0) goto L_0x0099
            r3 = 1
            goto L_0x009a
        L_0x0099:
            r3 = 0
        L_0x009a:
            if (r3 != 0) goto L_0x00b7
            double r4 = r12.o     // Catch:{ Exception -> 0x00ce }
            boolean r4 = java.lang.Double.isNaN(r4)     // Catch:{ Exception -> 0x00ce }
            if (r4 != 0) goto L_0x00b7
            double r4 = r12.o     // Catch:{ Exception -> 0x00ce }
            double r4 = r4 - r7
            double r4 = java.lang.Math.abs(r4)     // Catch:{ Exception -> 0x00ce }
            r10 = 4587366580439587226(0x3fa999999999999a, double:0.05)
            int r6 = (r4 > r10 ? 1 : (r4 == r10 ? 0 : -1))
            if (r6 <= 0) goto L_0x00b7
            com.moat.analytics.mobile.vng.MoatAdEventType r9 = com.moat.analytics.mobile.vng.MoatAdEventType.AD_EVT_VOLUME_CHANGE     // Catch:{ Exception -> 0x00ce }
            r3 = 1
        L_0x00b7:
            if (r3 == 0) goto L_0x00c9
            com.moat.analytics.mobile.vng.MoatAdEvent r3 = new com.moat.analytics.mobile.vng.MoatAdEvent     // Catch:{ Exception -> 0x00ce }
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)     // Catch:{ Exception -> 0x00ce }
            java.lang.Double r4 = r12.k()     // Catch:{ Exception -> 0x00ce }
            r3.<init>(r9, r2, r4)     // Catch:{ Exception -> 0x00ce }
            r12.dispatchEvent(r3)     // Catch:{ Exception -> 0x00ce }
        L_0x00c9:
            r12.o = r7     // Catch:{ Exception -> 0x00ce }
            r12.q = r1     // Catch:{ Exception -> 0x00ce }
            return r0
        L_0x00ce:
            int r2 = r12.q
            int r3 = r2 + 1
            r12.q = r3
            r3 = 5
            if (r2 >= r3) goto L_0x00d9
            goto L_0x00da
        L_0x00d9:
            r0 = 0
        L_0x00da:
            return r0
        L_0x00db:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.moat.analytics.mobile.vng.h.s():boolean");
    }

    public void setPlayerVolume(Double d) {
        super.setPlayerVolume(d);
        this.o = j().doubleValue();
    }

    public void stopTracking() {
        try {
            dispatchEvent(new MoatAdEvent(MoatAdEventType.AD_EVT_COMPLETE));
            super.stopTracking();
        } catch (Exception e) {
            n.a(e);
        }
    }
}
