package com.applovin.sdk;

public final class R$style {
    public static final int ExoMediaButton = 2131820781;
    public static final int ExoMediaButton_FastForward = 2131820782;
    public static final int ExoMediaButton_Next = 2131820783;
    public static final int ExoMediaButton_Pause = 2131820784;
    public static final int ExoMediaButton_Play = 2131820785;
    public static final int ExoMediaButton_Previous = 2131820786;
    public static final int ExoMediaButton_Rewind = 2131820787;
    public static final int LargeIconView = 2131820794;
    public static final int SmallIconView = 2131820874;
    public static final int TextAppearance_Compat_Notification = 2131820935;
    public static final int TextAppearance_Compat_Notification_Info = 2131820936;
    public static final int TextAppearance_Compat_Notification_Info_Media = 2131820937;
    public static final int TextAppearance_Compat_Notification_Line2 = 2131820938;
    public static final int TextAppearance_Compat_Notification_Line2_Media = 2131820939;
    public static final int TextAppearance_Compat_Notification_Media = 2131820940;
    public static final int TextAppearance_Compat_Notification_Time = 2131820941;
    public static final int TextAppearance_Compat_Notification_Time_Media = 2131820942;
    public static final int TextAppearance_Compat_Notification_Title = 2131820943;
    public static final int TextAppearance_Compat_Notification_Title_Media = 2131820944;
    public static final int Widget_Compat_NotificationActionContainer = 2131821157;
    public static final int Widget_Compat_NotificationActionText = 2131821158;
    public static final int Widget_Support_CoordinatorLayout = 2131821211;
    public static final int com_applovin_mediation_MaxDebuggerActivity_ActionBar = 2131821212;
    public static final int com_applovin_mediation_MaxDebuggerActivity_ActionBar_Live = 2131821213;
    public static final int com_applovin_mediation_MaxDebuggerActivity_ActionBar_TitleTextStyle = 2131821214;
    public static final int com_applovin_mediation_MaxDebuggerActivity_Theme = 2131821215;
    public static final int com_applovin_mediation_MaxDebuggerActivity_Theme_Live = 2131821216;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_AdBadgeTextView = 2131821217;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_AutoScrollingTextView = 2131821218;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_CTAButton = 2131821219;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_LargeAdBadgeTextView = 2131821220;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_LargeScrollingBodyTextView = 2131821221;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_LargeScrollingTitleTextView = 2131821222;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_LargeVerticalBodyTextSize = 2131821223;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_LargeVerticalTitleTextSize = 2131821224;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_ScrollingTitleTextView = 2131821225;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_SmallAdBadgeTextView = 2131821226;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_SmallScrollingBodyTextView = 2131821227;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_SmallScrollingTitleTextView = 2131821228;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_SmallVerticalBodyTextSize = 2131821229;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_SmallVerticalTitleTextSize = 2131821230;
    public static final int com_applovin_mediation_nativeAds_MaxNativeAdView_TitleTextStyle = 2131821231;

    private R$style() {
    }
}
