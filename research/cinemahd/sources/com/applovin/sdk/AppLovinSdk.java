package com.applovin.sdk;

import android.content.Context;
import android.text.TextUtils;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.c;
import com.applovin.impl.sdk.utils.e;
import com.applovin.nativeAds.AppLovinNativeAdService;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class AppLovinSdk {
    private static final String TAG = "AppLovinSdk";
    public static final String VERSION = getVersion();
    public static final int VERSION_CODE = getVersionCode();
    private static final Map<String, AppLovinSdk> sdkInstances = new HashMap();
    private static final Object sdkInstancesLock = new Object();
    public final l coreSdk;

    public interface SdkInitializationListener {
        void onSdkInitialized(AppLovinSdkConfiguration appLovinSdkConfiguration);
    }

    private static class a extends AppLovinSdkSettings {
        a(Context context) {
            super(context);
        }
    }

    private AppLovinSdk(l lVar) {
        this.coreSdk = lVar;
    }

    public static AppLovinSdk a(boolean z, String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (appLovinSdkSettings == null) {
            throw new IllegalArgumentException("No userSettings specified");
        } else if (context != null) {
            synchronized (sdkInstancesLock) {
                if (sdkInstances.containsKey(str)) {
                    AppLovinSdk appLovinSdk = sdkInstances.get(str);
                    return appLovinSdk;
                }
                if (!TextUtils.isEmpty(str) && str.contains(File.separator)) {
                    r.i(TAG, "\n**************************************************\nINVALID SDK KEY: " + str + "\n**************************************************\n");
                    if (!sdkInstances.isEmpty()) {
                        AppLovinSdk next = sdkInstances.values().iterator().next();
                        return next;
                    }
                    str = str.replace(File.separator, "");
                }
                l lVar = new l();
                lVar.a(z, str, appLovinSdkSettings, context);
                AppLovinSdk appLovinSdk2 = new AppLovinSdk(lVar);
                lVar.a(appLovinSdk2);
                sdkInstances.put(str, appLovinSdk2);
                return appLovinSdk2;
            }
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    public static List<AppLovinSdk> a() {
        return new ArrayList(sdkInstances.values());
    }

    public static AppLovinSdk getInstance(Context context) {
        return getInstance(new a(context), context);
    }

    public static AppLovinSdk getInstance(AppLovinSdkSettings appLovinSdkSettings, Context context) {
        if (context != null) {
            return getInstance(c.a(context).a("applovin.sdk.key", ""), appLovinSdkSettings, context);
        }
        throw new IllegalArgumentException("No context specified");
    }

    public static AppLovinSdk getInstance(String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        return a(false, str, appLovinSdkSettings, context);
    }

    private static String getVersion() {
        return "9.15.1";
    }

    private static int getVersionCode() {
        return 9150199;
    }

    public static void initializeSdk(Context context) {
        initializeSdk(context, (SdkInitializationListener) null);
    }

    public static void initializeSdk(Context context, SdkInitializationListener sdkInitializationListener) {
        if (context != null) {
            AppLovinSdk instance = getInstance(context);
            if (instance != null) {
                instance.initializeSdk(sdkInitializationListener);
            } else {
                r.i(TAG, "Unable to initialize AppLovin SDK: SDK object not created");
            }
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    static void reinitializeAll(Boolean bool, Boolean bool2, Boolean bool3) {
        synchronized (sdkInstancesLock) {
            for (AppLovinSdk next : sdkInstances.values()) {
                next.coreSdk.M();
                if (bool != null) {
                    r j0 = next.coreSdk.j0();
                    j0.c(TAG, "Toggled 'huc' to " + bool);
                    next.getEventService().trackEvent("huc", e.a("value", bool.toString()));
                }
                if (bool2 != null) {
                    r j02 = next.coreSdk.j0();
                    j02.c(TAG, "Toggled 'aru' to " + bool2);
                    next.getEventService().trackEvent("aru", e.a("value", bool2.toString()));
                }
                if (bool3 != null) {
                    r j03 = next.coreSdk.j0();
                    j03.c(TAG, "Toggled 'dns' to " + bool3);
                    next.getEventService().trackEvent("dns", e.a("value", bool3.toString()));
                }
            }
        }
    }

    public AppLovinAdService getAdService() {
        return this.coreSdk.c0();
    }

    /* access modifiers changed from: package-private */
    @Deprecated
    public Context getApplicationContext() {
        return this.coreSdk.i();
    }

    public AppLovinSdkConfiguration getConfiguration() {
        return this.coreSdk.a0();
    }

    public AppLovinEventService getEventService() {
        return this.coreSdk.e0();
    }

    @Deprecated
    public r getLogger() {
        return this.coreSdk.j0();
    }

    public String getMediationProvider() {
        return this.coreSdk.b0();
    }

    public AppLovinNativeAdService getNativeAdService() {
        return this.coreSdk.d0();
    }

    public AppLovinPostbackService getPostbackService() {
        return this.coreSdk.u();
    }

    public String getSdkKey() {
        return this.coreSdk.h0();
    }

    public AppLovinSdkSettings getSettings() {
        return this.coreSdk.Y();
    }

    public String getUserIdentifier() {
        return this.coreSdk.V();
    }

    public AppLovinUserSegment getUserSegment() {
        return this.coreSdk.Z();
    }

    public AppLovinUserService getUserService() {
        return this.coreSdk.f0();
    }

    public AppLovinVariableService getVariableService() {
        return this.coreSdk.g0();
    }

    public boolean hasCriticalErrors() {
        return this.coreSdk.i0();
    }

    public void initializeSdk() {
    }

    public void initializeSdk(SdkInitializationListener sdkInitializationListener) {
        this.coreSdk.a(sdkInitializationListener);
    }

    public boolean isEnabled() {
        return this.coreSdk.O();
    }

    public void setMediationProvider(String str) {
        this.coreSdk.c(str);
    }

    public void setPluginVersion(String str) {
        this.coreSdk.a(str);
    }

    public void setUserIdentifier(String str) {
        this.coreSdk.b(str);
    }

    public void showMediationDebugger() {
        this.coreSdk.U();
    }

    public String toString() {
        return "AppLovinSdk{sdkKey='" + getSdkKey() + "', isEnabled=" + isEnabled() + ", isFirstSession=" + this.coreSdk.l() + '}';
    }
}
