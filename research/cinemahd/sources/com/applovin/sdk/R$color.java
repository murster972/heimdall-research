package com.applovin.sdk;

public final class R$color {
    public static final int applovin_sdk_adBadgeTextColor = 2131099674;
    public static final int applovin_sdk_adControlbutton_brightBlueColor = 2131099675;
    public static final int applovin_sdk_brand_color = 2131099676;
    public static final int applovin_sdk_brand_color_dark = 2131099677;
    public static final int applovin_sdk_checkmarkColor = 2131099678;
    public static final int applovin_sdk_colorEdgeEffect = 2131099679;
    public static final int applovin_sdk_ctaButtonColor = 2131099680;
    public static final int applovin_sdk_ctaButtonPressedColor = 2131099681;
    public static final int applovin_sdk_disclosureButtonColor = 2131099682;
    public static final int applovin_sdk_greenColor = 2131099683;
    public static final int applovin_sdk_listViewBackground = 2131099684;
    public static final int applovin_sdk_listViewSectionTextColor = 2131099685;
    public static final int applovin_sdk_textColorPrimary = 2131099686;
    public static final int applovin_sdk_xmarkColor = 2131099687;
    public static final int exo_edit_mode_background_color = 2131099789;
    public static final int exo_error_message_background_color = 2131099790;
    public static final int notification_action_color_filter = 2131100106;
    public static final int notification_icon_bg_color = 2131100107;
    public static final int notification_material_background_media_default_color = 2131100108;
    public static final int primary_text_default_material_dark = 2131100116;
    public static final int ripple_material_light = 2131100121;
    public static final int secondary_text_default_material_dark = 2131100123;
    public static final int secondary_text_default_material_light = 2131100124;

    private R$color() {
    }
}
