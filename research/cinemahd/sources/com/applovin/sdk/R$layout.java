package com.applovin.sdk;

public final class R$layout {
    public static final int custom_dialog = 2131492957;
    public static final int exo_list_divider = 2131493008;
    public static final int exo_player_control_view = 2131493010;
    public static final int exo_player_view = 2131493011;
    public static final int exo_track_selection_dialog = 2131493013;
    public static final int list_item_detail = 2131493071;
    public static final int list_item_right_detail = 2131493072;
    public static final int list_section = 2131493073;
    public static final int list_view = 2131493074;
    public static final int max_native_ad_banner_icon_and_text_layout = 2131493076;
    public static final int max_native_ad_banner_view = 2131493077;
    public static final int max_native_ad_leader_view = 2131493078;
    public static final int max_native_ad_media_banner_view = 2131493079;
    public static final int max_native_ad_mrec_view = 2131493080;
    public static final int max_native_ad_vertical_banner_view = 2131493081;
    public static final int max_native_ad_vertical_leader_view = 2131493082;
    public static final int max_native_ad_vertical_media_banner_view = 2131493083;
    public static final int mediation_debugger_ad_unit_detail_activity = 2131493084;
    public static final int mediation_debugger_multi_ad_activity = 2131493085;
    public static final int notification_action = 2131493112;
    public static final int notification_action_tombstone = 2131493113;
    public static final int notification_media_action = 2131493115;
    public static final int notification_media_cancel_action = 2131493116;
    public static final int notification_template_big_media = 2131493117;
    public static final int notification_template_big_media_custom = 2131493118;
    public static final int notification_template_big_media_narrow = 2131493119;
    public static final int notification_template_big_media_narrow_custom = 2131493120;
    public static final int notification_template_custom_big = 2131493121;
    public static final int notification_template_icon_group = 2131493122;
    public static final int notification_template_lines_media = 2131493123;
    public static final int notification_template_media = 2131493124;
    public static final int notification_template_media_custom = 2131493125;
    public static final int notification_template_part_chronometer = 2131493126;
    public static final int notification_template_part_time = 2131493127;

    private R$layout() {
    }
}
