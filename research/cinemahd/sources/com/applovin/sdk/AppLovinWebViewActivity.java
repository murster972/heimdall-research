package com.applovin.sdk;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.o;
import java.util.Set;

public class AppLovinWebViewActivity extends Activity {
    public static final String EVENT_DISMISSED_VIA_BACK_BUTTON = "dismissed_via_back_button";
    public static final String INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON = "immersive_mode_on";
    public static final String INTENT_EXTRA_KEY_SDK_KEY = "sdk_key";

    /* renamed from: a  reason: collision with root package name */
    private String f1961a;
    private WebView b;
    /* access modifiers changed from: private */
    public EventListener c;

    public interface EventListener {
        void onReceivedEvent(String str);
    }

    public void loadUrl(String str, EventListener eventListener) {
        this.c = eventListener;
        WebView webView = this.b;
        if (webView == null) {
            this.f1961a = str;
        } else {
            webView.loadUrl(str);
        }
    }

    public void onBackPressed() {
        EventListener eventListener = this.c;
        if (eventListener != null) {
            eventListener.onReceivedEvent(EVENT_DISMISSED_VIA_BACK_BUTTON);
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        String stringExtra = getIntent().getStringExtra(INTENT_EXTRA_KEY_SDK_KEY);
        if (TextUtils.isEmpty(stringExtra)) {
            r.i("AppLovinWebViewActivity", "No SDK key specified");
        } else {
            final AppLovinSdk instance = AppLovinSdk.getInstance(stringExtra, new AppLovinSdkSettings(), getApplicationContext());
            try {
                this.b = new WebView(this);
                setContentView(this.b);
                WebSettings settings = this.b.getSettings();
                settings.setSupportMultipleWindows(false);
                settings.setJavaScriptEnabled(true);
                this.b.setVerticalScrollBarEnabled(true);
                this.b.setHorizontalScrollBarEnabled(true);
                this.b.setScrollBarStyle(33554432);
                this.b.setWebViewClient(new WebViewClient() {
                    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
                        Uri parse = Uri.parse(str);
                        String scheme = parse.getScheme();
                        String host = parse.getHost();
                        String path = parse.getPath();
                        r logger = instance.getLogger();
                        logger.b("AppLovinWebViewActivity", "Handling url load: " + str);
                        if (!"applovin".equalsIgnoreCase(scheme) || !"com.applovin.sdk".equalsIgnoreCase(host) || AppLovinWebViewActivity.this.c == null) {
                            return super.shouldOverrideUrlLoading(webView, str);
                        }
                        if (!path.endsWith("webview_event")) {
                            return true;
                        }
                        Set<String> queryParameterNames = parse.getQueryParameterNames();
                        String str2 = queryParameterNames.isEmpty() ? "" : (String) queryParameterNames.toArray()[0];
                        if (o.b(str2)) {
                            String queryParameter = parse.getQueryParameter(str2);
                            r logger2 = instance.getLogger();
                            logger2.b("AppLovinWebViewActivity", "Parsed WebView event parameter name: " + str2 + " and value: " + queryParameter);
                            AppLovinWebViewActivity.this.c.onReceivedEvent(queryParameter);
                            return true;
                        }
                        instance.getLogger().e("AppLovinWebViewActivity", "Failed to parse WebView event parameter");
                        return true;
                    }
                });
                if (getIntent().getBooleanExtra(INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, false)) {
                    getWindow().getDecorView().setSystemUiVisibility(5894);
                }
                if (o.b(this.f1961a)) {
                    this.b.loadUrl(this.f1961a);
                    return;
                }
                return;
            } catch (Throwable th) {
                instance.getLogger().b("AppLovinWebViewActivity", "Failed to initialize WebView.", th);
            }
        }
        finish();
    }
}
