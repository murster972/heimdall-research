package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.i;

public class AppLovinPrivacySettings {
    public static boolean hasUserConsent(Context context) {
        Boolean a2 = i.b().a(context);
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    public static boolean isAgeRestrictedUser(Context context) {
        Boolean a2 = i.a().a(context);
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    public static boolean isDoNotSell(Context context) {
        Boolean a2 = i.c().a(context);
        if (a2 != null) {
            return a2.booleanValue();
        }
        return false;
    }

    public static void setDoNotSell(boolean z, Context context) {
        if (i.c(z, context)) {
            AppLovinSdk.reinitializeAll((Boolean) null, (Boolean) null, Boolean.valueOf(z));
        }
    }

    public static void setHasUserConsent(boolean z, Context context) {
        if (i.b(z, context)) {
            AppLovinSdk.reinitializeAll(Boolean.valueOf(z), (Boolean) null, (Boolean) null);
        }
    }

    public static void setIsAgeRestrictedUser(boolean z, Context context) {
        if (i.a(z, context)) {
            AppLovinSdk.reinitializeAll((Boolean) null, Boolean.valueOf(z), (Boolean) null);
        }
    }
}
