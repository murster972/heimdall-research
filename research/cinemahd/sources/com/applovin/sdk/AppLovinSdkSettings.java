package com.applovin.sdk;

import android.content.Context;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AppLovinSdkSettings {

    /* renamed from: a  reason: collision with root package name */
    private boolean f1958a;
    private long b;
    private boolean c;
    private boolean d;
    private boolean e;
    private List<String> f;
    private List<String> g;
    private final Map<String, Object> localSettings;

    public AppLovinSdkSettings() {
        this((Context) null);
    }

    public AppLovinSdkSettings(Context context) {
        this.localSettings = new HashMap();
        new HashMap();
        this.f = Collections.emptyList();
        this.g = Collections.emptyList();
        this.f1958a = r.a(context);
        this.b = -1;
        this.e = true;
    }

    @Deprecated
    public long getBannerAdRefreshSeconds() {
        return this.b;
    }

    public List<String> getInitializationAdUnitIds() {
        return this.g;
    }

    public List<String> getTestDeviceAdvertisingIds() {
        return this.f;
    }

    public boolean isAdInfoButtonEnabled() {
        return this.d;
    }

    public boolean isExceptionHandlerEnabled() {
        return this.e;
    }

    public boolean isMuted() {
        return this.c;
    }

    public boolean isVerboseLoggingEnabled() {
        return this.f1958a;
    }

    public void setAdInfoButtonEnabled(boolean z) {
        this.d = z;
    }

    @Deprecated
    public void setBannerAdRefreshSeconds(long j) {
        this.b = j;
    }

    public void setExceptionHandlerEnabled(boolean z) {
        this.e = true;
    }

    public void setInitializationAdUnitIds(List<String> list) {
        if (list != null) {
            ArrayList arrayList = new ArrayList(list.size());
            for (String next : list) {
                if (o.b(next) && next.length() > 0) {
                    if (next.length() == 16) {
                        arrayList.add(next);
                    } else {
                        com.applovin.impl.sdk.r.i("AppLovinSdkSettings", "Unable to set initialization ad unit id (" + next + ") - please make sure it is in the format of XXXXXXXXXXXXXXXX");
                    }
                }
            }
            this.g = arrayList;
            return;
        }
        this.g = Collections.emptyList();
    }

    public void setMuted(boolean z) {
        this.c = z;
    }

    public void setTestDeviceAdvertisingIds(List<String> list) {
        if (list != null) {
            ArrayList arrayList = new ArrayList(list.size());
            for (String next : list) {
                if (next == null || next.length() != 36) {
                    com.applovin.impl.sdk.r.i("AppLovinSdkSettings", "Unable to set test device advertising id (" + next + ") - please make sure it is in the format of xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx");
                } else {
                    arrayList.add(next);
                }
            }
            this.f = arrayList;
            return;
        }
        this.f = Collections.emptyList();
    }

    public void setVerboseLogging(boolean z) {
        if (r.a()) {
            com.applovin.impl.sdk.r.i("AppLovinSdkSettings", "Ignoring setting of verbose logging - it is configured from Android manifest already or AppLovinSdkSettings was initialized without a context.");
            if (r.a((Context) null) != z) {
                com.applovin.impl.sdk.r.i("AppLovinSdkSettings", "Attempted to programmatically set verbose logging flag to value different from value configured in Android Manifest.");
                return;
            }
            return;
        }
        this.f1958a = z;
    }

    public String toString() {
        return "AppLovinSdkSettings{isVerboseLoggingEnabled=" + this.f1958a + ", muted=" + this.c + ", testDeviceAdvertisingIds=" + this.f.toString() + ", initializationAdUnitIds=" + this.g.toString() + ", adInfoButtonEnabled=" + this.d + ", exceptionHandlerEnabled=" + this.e + '}';
    }
}
