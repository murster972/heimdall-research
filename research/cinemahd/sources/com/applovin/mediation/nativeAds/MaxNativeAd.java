package com.applovin.mediation.nativeAds;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;
import com.applovin.mediation.MaxAdFormat;

public class MaxNativeAd {

    /* renamed from: a  reason: collision with root package name */
    private final MaxAdFormat f1949a;
    private final String b;
    private final String c;
    private final String d;
    private final MaxNativeAdImage e;
    private final View f;
    private final View g;
    private final View h;

    public static class Builder {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public MaxAdFormat f1950a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public MaxNativeAdImage e;
        /* access modifiers changed from: private */
        public View f;
        /* access modifiers changed from: private */
        public View g;
        /* access modifiers changed from: private */
        public View h;

        public MaxNativeAd build() {
            return new MaxNativeAd(this);
        }

        public Builder setAdFormat(MaxAdFormat maxAdFormat) {
            this.f1950a = maxAdFormat;
            return this;
        }

        public Builder setBody(String str) {
            this.c = str;
            return this;
        }

        public Builder setCallToAction(String str) {
            this.d = str;
            return this;
        }

        public Builder setIcon(MaxNativeAdImage maxNativeAdImage) {
            this.e = maxNativeAdImage;
            return this;
        }

        public Builder setIconView(View view) {
            this.f = view;
            return this;
        }

        public Builder setMediaView(View view) {
            this.h = view;
            return this;
        }

        public Builder setOptionsView(View view) {
            this.g = view;
            return this;
        }

        public Builder setTitle(String str) {
            this.b = str;
            return this;
        }
    }

    public static class MaxNativeAdImage {

        /* renamed from: a  reason: collision with root package name */
        private Drawable f1951a;
        private Uri b;

        public MaxNativeAdImage(Drawable drawable) {
            this.f1951a = drawable;
        }

        public MaxNativeAdImage(Uri uri) {
            this.b = uri;
        }

        public Drawable getDrawable() {
            return this.f1951a;
        }

        public Uri getUri() {
            return this.b;
        }
    }

    private MaxNativeAd(Builder builder) {
        this.f1949a = builder.f1950a;
        this.b = builder.b;
        this.c = builder.c;
        this.d = builder.d;
        this.e = builder.e;
        this.f = builder.f;
        this.g = builder.g;
        this.h = builder.h;
    }

    public String getBody() {
        return this.c;
    }

    public String getCallToAction() {
        return this.d;
    }

    public MaxAdFormat getFormat() {
        return this.f1949a;
    }

    public MaxNativeAdImage getIcon() {
        return this.e;
    }

    public View getIconView() {
        return this.f;
    }

    public View getMediaView() {
        return this.h;
    }

    public View getOptionsView() {
        return this.g;
    }

    public String getTitle() {
        return this.b;
    }
}
