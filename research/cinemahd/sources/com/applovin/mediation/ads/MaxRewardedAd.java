package com.applovin.mediation.ads;

import android.app.Activity;
import android.text.TextUtils;
import com.applovin.impl.mediation.ads.MaxFullscreenAdImpl;
import com.applovin.impl.mediation.ads.a;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

public class MaxRewardedAd implements MaxFullscreenAdImpl.a {
    private static final Map<String, MaxRewardedAd> b = new HashMap();
    private static final Object c = new Object();
    private static WeakReference<Activity> d = new WeakReference<>((Object) null);

    /* renamed from: a  reason: collision with root package name */
    private final MaxFullscreenAdImpl f1947a;

    private MaxRewardedAd(String str, AppLovinSdk appLovinSdk) {
        this.f1947a = new MaxFullscreenAdImpl(str.trim(), MaxAdFormat.REWARDED, this, "MaxRewardedAd", appLovinSdk.coreSdk);
    }

    public static MaxRewardedAd getInstance(String str, Activity activity) {
        return getInstance(str, AppLovinSdk.getInstance(activity), activity);
    }

    public static MaxRewardedAd getInstance(String str, AppLovinSdk appLovinSdk, Activity activity) {
        a.logApiCall("MaxRewardedAd", "getInstance(adUnitId=" + str + ", sdk=" + appLovinSdk + ", activity=" + activity + ")");
        if (str == null) {
            throw new IllegalArgumentException("No ad unit ID specified");
        } else if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Empty ad unit ID specified");
        } else if (activity == null) {
            throw new IllegalArgumentException("No activity specified");
        } else if (appLovinSdk != null) {
            updateActivity(activity);
            synchronized (c) {
                MaxRewardedAd maxRewardedAd = b.get(str);
                if (maxRewardedAd != null) {
                    return maxRewardedAd;
                }
                MaxRewardedAd maxRewardedAd2 = new MaxRewardedAd(str, appLovinSdk);
                b.put(str, maxRewardedAd2);
                return maxRewardedAd2;
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static void updateActivity(Activity activity) {
        a.logApiCall("MaxRewardedAd", "updateActivity(activity=" + activity + ")");
        if (activity != null) {
            d = new WeakReference<>(activity);
        }
    }

    public void destroy() {
        this.f1947a.logApiCall("destroy()");
        synchronized (c) {
            b.remove(this.f1947a.getAdUnitId());
        }
        this.f1947a.destroy();
    }

    public Activity getActivity() {
        this.f1947a.logApiCall("getActivity()");
        return (Activity) d.get();
    }

    public boolean isReady() {
        boolean isReady = this.f1947a.isReady();
        MaxFullscreenAdImpl maxFullscreenAdImpl = this.f1947a;
        maxFullscreenAdImpl.logApiCall("isReady() " + isReady + " for ad unit id " + this.f1947a.getAdUnitId());
        return isReady;
    }

    public void loadAd() {
        this.f1947a.logApiCall("loadAd()");
        this.f1947a.loadAd(getActivity());
    }

    public void setExtraParameter(String str, String str2) {
        MaxFullscreenAdImpl maxFullscreenAdImpl = this.f1947a;
        maxFullscreenAdImpl.logApiCall("setExtraParameter(key=" + str + ", value=" + str2 + ")");
        this.f1947a.setExtraParameter(str, str2);
    }

    public void setListener(MaxRewardedAdListener maxRewardedAdListener) {
        MaxFullscreenAdImpl maxFullscreenAdImpl = this.f1947a;
        maxFullscreenAdImpl.logApiCall("setListener(listener=" + maxRewardedAdListener + ")");
        this.f1947a.setListener(maxRewardedAdListener);
    }

    public void showAd() {
        showAd((String) null);
    }

    public void showAd(String str) {
        MaxFullscreenAdImpl maxFullscreenAdImpl = this.f1947a;
        maxFullscreenAdImpl.logApiCall("showAd(placement=" + str + ")");
        this.f1947a.showAd(str, getActivity());
    }

    public String toString() {
        return "" + this.f1947a;
    }
}
