package com.applovin.mediation;

public interface MaxAd {
    String getAdUnitId();

    String getCreativeId();

    MaxAdFormat getFormat();

    String getNetworkName();

    String getPlacement();
}
