package com.applovin.mediation;

import android.app.Activity;
import com.applovin.impl.mediation.c.c;
import com.applovin.sdk.AppLovinSdkUtils;
import com.startapp.sdk.adsbase.model.AdPreferences;

public class MaxAdFormat {
    public static final MaxAdFormat BANNER = new MaxAdFormat(AdPreferences.TYPE_BANNER, "Banner");
    public static final MaxAdFormat INTERSTITIAL = new MaxAdFormat("INTER", "Interstitial");
    public static final MaxAdFormat LEADER = new MaxAdFormat("LEADER", "Leader");
    public static final MaxAdFormat MREC = new MaxAdFormat("MREC", "MREC");
    public static final MaxAdFormat NATIVE = new MaxAdFormat("NATIVE", "Native");
    public static final MaxAdFormat REWARDED = new MaxAdFormat("REWARDED", "Rewarded");
    public static final MaxAdFormat REWARDED_INTERSTITIAL = new MaxAdFormat("REWARDED_INTER", "Rewarded Interstitial");

    /* renamed from: a  reason: collision with root package name */
    private final String f1943a;
    private final String b;

    private MaxAdFormat(String str, String str2) {
        this.f1943a = str;
        this.b = str2;
    }

    public AppLovinSdkUtils.Size getAdaptiveSize(int i, Activity activity) {
        return (this == BANNER || this == LEADER) ? c.a(i, this, activity) : getSize();
    }

    public AppLovinSdkUtils.Size getAdaptiveSize(Activity activity) {
        return getAdaptiveSize(-1, activity);
    }

    public String getDisplayName() {
        return this.b;
    }

    public String getLabel() {
        return this.f1943a;
    }

    public AppLovinSdkUtils.Size getSize() {
        return this == BANNER ? new AppLovinSdkUtils.Size(320, 50) : this == LEADER ? new AppLovinSdkUtils.Size(728, 90) : this == MREC ? new AppLovinSdkUtils.Size(300, 250) : new AppLovinSdkUtils.Size(0, 0);
    }

    public boolean isAdViewAd() {
        return this == BANNER || this == MREC || this == LEADER;
    }

    public boolean isFullscreenAd() {
        return this == INTERSTITIAL || this == REWARDED || this == REWARDED_INTERSTITIAL;
    }

    public String toString() {
        return "MaxAdFormat{label='" + this.f1943a + '\'' + '}';
    }
}
