package com.applovin.communicator;

import android.content.Context;
import com.applovin.impl.communicator.MessagingServiceImpl;
import com.applovin.impl.communicator.a;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.util.Collections;
import java.util.List;

public final class AppLovinCommunicator {
    private static AppLovinCommunicator e;
    private static final Object f = new Object();

    /* renamed from: a  reason: collision with root package name */
    private l f1434a;
    private r b;
    private final a c;
    private final MessagingServiceImpl d;

    private AppLovinCommunicator(Context context) {
        this.c = new a(context);
        this.d = new MessagingServiceImpl(context);
    }

    private void a(String str) {
        r rVar = this.b;
        if (rVar != null) {
            rVar.b("AppLovinCommunicator", str);
        }
    }

    public static AppLovinCommunicator getInstance(Context context) {
        synchronized (f) {
            if (e == null) {
                e = new AppLovinCommunicator(context.getApplicationContext());
            }
        }
        return e;
    }

    public void a(l lVar) {
        this.f1434a = lVar;
        this.b = lVar.j0();
        a("Attached SDK instance: " + lVar + "...");
    }

    public AppLovinCommunicatorMessagingService getMessagingService() {
        return this.d;
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        subscribe(appLovinCommunicatorSubscriber, (List<String>) Collections.singletonList(str));
    }

    public void subscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            if (this.c.a(appLovinCommunicatorSubscriber, next)) {
                this.d.maybeFlushStickyMessages(next);
            } else {
                a("Unable to subscribe " + appLovinCommunicatorSubscriber + " to topic: " + next);
            }
        }
    }

    public String toString() {
        return "AppLovinCommunicator{sdk=" + this.f1434a + '}';
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, String str) {
        unsubscribe(appLovinCommunicatorSubscriber, (List<String>) Collections.singletonList(str));
    }

    public void unsubscribe(AppLovinCommunicatorSubscriber appLovinCommunicatorSubscriber, List<String> list) {
        for (String next : list) {
            a("Unsubscribing " + appLovinCommunicatorSubscriber + " from topic: " + next);
            this.c.b(appLovinCommunicatorSubscriber, next);
        }
    }
}
