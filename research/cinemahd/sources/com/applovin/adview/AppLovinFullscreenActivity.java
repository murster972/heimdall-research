package com.applovin.adview;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Process;
import android.view.KeyEvent;
import android.webkit.WebView;
import com.applovin.impl.adview.activity.FullscreenAdService;
import com.applovin.impl.adview.activity.b.a;
import com.applovin.impl.adview.activity.b.e;
import com.applovin.impl.adview.j;
import com.applovin.impl.adview.o;
import com.applovin.impl.sdk.a.i;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.k;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkSettings;
import com.facebook.common.util.ByteConstants;
import java.util.concurrent.atomic.AtomicBoolean;
import okhttp3.internal.http2.Http2Connection;

public class AppLovinFullscreenActivity extends Activity implements j {
    public static o parentInterstitialWrapper;

    /* renamed from: a  reason: collision with root package name */
    private l f1431a;
    private a b;
    private final AtomicBoolean c = new AtomicBoolean(true);
    private com.applovin.impl.adview.activity.a d;

    private void a(String str, Throwable th) {
        r.c("InterActivityV2", str, th);
        AppLovinAdDisplayListener d2 = parentInterstitialWrapper.d();
        if (d2 instanceof i) {
            k.a(d2, str);
        } else {
            k.b(d2, (AppLovinAd) parentInterstitialWrapper.b());
        }
        dismiss();
    }

    public void dismiss() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.f();
        } else {
            finish();
        }
    }

    public void onBackPressed() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.j();
        }
        if (com.applovin.impl.sdk.utils.r.f(getApplicationContext())) {
            super.onBackPressed();
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        a aVar = this.b;
        if (aVar != null) {
            aVar.a(configuration);
        }
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        ActivityManager activityManager;
        super.onCreate(bundle);
        requestWindowFeature(1);
        getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
        getWindow().addFlags(Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE);
        getWindow().addFlags(128);
        findViewById(16908290).setBackgroundColor(-16777216);
        this.f1431a = AppLovinSdk.getInstance(getIntent().getStringExtra("com.applovin.interstitial.sdk_key"), new AppLovinSdkSettings(this), this).coreSdk;
        if (parentInterstitialWrapper != null) {
            int intValue = ((Integer) this.f1431a.a(b.T3)).intValue();
            if (!(intValue == -1 || (activityManager = (ActivityManager) getSystemService("activity")) == null)) {
                ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
                activityManager.getMemoryInfo(memoryInfo);
                if (memoryInfo.availMem < ((long) intValue)) {
                    a("Not enough available memory", (Throwable) null);
                    return;
                }
            }
            present(parentInterstitialWrapper.b(), parentInterstitialWrapper.e(), parentInterstitialWrapper.d(), parentInterstitialWrapper.c());
            return;
        }
        Intent intent = new Intent(this, FullscreenAdService.class);
        this.d = new com.applovin.impl.adview.activity.a(this, this.f1431a);
        bindService(intent, this.d, 1);
        if (g.g()) {
            try {
                WebView.setDataDirectorySuffix(String.valueOf(Process.myPid()));
            } catch (Throwable unused) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDestroy() {
        parentInterstitialWrapper = null;
        com.applovin.impl.adview.activity.a aVar = this.d;
        if (aVar != null) {
            try {
                unbindService(aVar);
            } catch (Throwable unused) {
            }
        }
        a aVar2 = this.b;
        if (aVar2 != null) {
            aVar2.h();
        }
        super.onDestroy();
    }

    public boolean onKeyDown(int i, KeyEvent keyEvent) {
        a aVar = this.b;
        if (aVar != null) {
            aVar.a(i, keyEvent);
        }
        return super.onKeyDown(i, keyEvent);
    }

    public void onLowMemory() {
        a aVar = this.b;
        if (aVar != null) {
            aVar.i();
        }
        super.onLowMemory();
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        a aVar = this.b;
        if (aVar != null) {
            aVar.e();
        }
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        a aVar;
        try {
            super.onResume();
            if (!this.c.get() && (aVar = this.b) != null) {
                aVar.d();
            }
        } catch (IllegalArgumentException e) {
            this.f1431a.j0().b("InterActivityV2", "Error was encountered in onResume().", e);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public void onStop() {
        super.onStop();
        a aVar = this.b;
        if (aVar != null) {
            aVar.g();
        }
    }

    public void onWindowFocusChanged(boolean z) {
        if (this.b != null) {
            if (!this.c.getAndSet(false) || (this.b instanceof e)) {
                this.b.c(z);
            }
            if (z) {
                getWindow().getDecorView().setSystemUiVisibility(5894);
            }
        }
        super.onWindowFocusChanged(z);
    }

    /* JADX WARNING: type inference failed for: r0v3, types: [com.applovin.impl.adview.activity.b.a] */
    /* JADX WARNING: type inference failed for: r1v21, types: [com.applovin.impl.adview.activity.b.b] */
    /* JADX WARNING: type inference failed for: r1v22, types: [com.applovin.impl.adview.activity.b.d] */
    /* JADX WARNING: type inference failed for: r1v23, types: [com.applovin.impl.adview.activity.b.d] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void present(com.applovin.impl.sdk.a.g r14, com.applovin.sdk.AppLovinAdClickListener r15, com.applovin.sdk.AppLovinAdDisplayListener r16, com.applovin.sdk.AppLovinAdVideoPlaybackListener r17) {
        /*
            r13 = this;
            r8 = r13
            int r0 = com.applovin.impl.sdk.utils.r.g()
            com.applovin.impl.sdk.l r1 = r8.f1431a
            com.applovin.impl.sdk.c.b<java.lang.Integer> r2 = com.applovin.impl.sdk.c.b.S1
            java.lang.Object r1 = r1.a(r2)
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r1 = r1.intValue()
            boolean r2 = r14.I()
            if (r2 == 0) goto L_0x0037
            java.lang.String r2 = "com.google.android.exoplayer2.ui.PlayerView"
            boolean r2 = com.applovin.impl.sdk.utils.r.e((java.lang.String) r2)
            if (r2 == 0) goto L_0x0037
            com.applovin.impl.sdk.l r2 = r8.f1431a
            com.applovin.impl.sdk.c.b<java.lang.Integer> r3 = com.applovin.impl.sdk.c.b.R1
            java.lang.Object r2 = r2.a(r3)
            java.lang.Integer r2 = (java.lang.Integer) r2
            int r2 = r2.intValue()
            if (r0 < r2) goto L_0x0037
            if (r1 < 0) goto L_0x0035
            if (r0 > r1) goto L_0x0037
        L_0x0035:
            r0 = 1
            goto L_0x0038
        L_0x0037:
            r0 = 0
        L_0x0038:
            r9 = r14
            boolean r1 = r9 instanceof com.applovin.impl.a.a
            java.lang.String r10 = "Failed to create ExoPlayer presenter to show the ad. Falling back to using native media player presenter."
            java.lang.String r11 = "InterActivityV2"
            if (r1 == 0) goto L_0x0082
            if (r0 == 0) goto L_0x0071
            com.applovin.impl.adview.activity.b.c r0 = new com.applovin.impl.adview.activity.b.c     // Catch:{ all -> 0x0056 }
            com.applovin.impl.sdk.l r4 = r8.f1431a     // Catch:{ all -> 0x0056 }
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x0056 }
            r8.b = r0     // Catch:{ all -> 0x0056 }
            goto L_0x0123
        L_0x0056:
            r0 = move-exception
            com.applovin.impl.sdk.l r1 = r8.f1431a
            com.applovin.impl.sdk.r r1 = r1.j0()
            r1.a((java.lang.String) r11, (java.lang.String) r10, (java.lang.Throwable) r0)
            com.applovin.impl.adview.activity.b.d r0 = new com.applovin.impl.adview.activity.b.d
            com.applovin.impl.sdk.l r4 = r8.f1431a
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)
            goto L_0x0121
        L_0x0071:
            com.applovin.impl.adview.activity.b.d r0 = new com.applovin.impl.adview.activity.b.d
            com.applovin.impl.sdk.l r4 = r8.f1431a
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)
            goto L_0x0121
        L_0x0082:
            boolean r1 = r14.hasVideoUrl()
            if (r1 == 0) goto L_0x0112
            java.lang.String r12 = " and throwable: "
            if (r0 == 0) goto L_0x00de
            com.applovin.impl.adview.activity.b.e r0 = new com.applovin.impl.adview.activity.b.e     // Catch:{ all -> 0x009f }
            com.applovin.impl.sdk.l r4 = r8.f1431a     // Catch:{ all -> 0x009f }
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x009f }
            r8.b = r0     // Catch:{ all -> 0x009f }
            goto L_0x0123
        L_0x009f:
            r0 = move-exception
            com.applovin.impl.sdk.l r1 = r8.f1431a
            com.applovin.impl.sdk.r r1 = r1.j0()
            r1.a((java.lang.String) r11, (java.lang.String) r10, (java.lang.Throwable) r0)
            com.applovin.impl.adview.activity.b.f r0 = new com.applovin.impl.adview.activity.b.f     // Catch:{ all -> 0x00bc }
            com.applovin.impl.sdk.l r4 = r8.f1431a     // Catch:{ all -> 0x00bc }
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00bc }
            r8.b = r0     // Catch:{ all -> 0x00bc }
            goto L_0x0123
        L_0x00bc:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to create FullscreenVideoAdExoPlayerPresenter with sdk: "
            r1.append(r2)
            com.applovin.impl.sdk.l r2 = r8.f1431a
            r1.append(r2)
            r1.append(r12)
            java.lang.String r2 = r0.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r13.a(r1, r0)
            return
        L_0x00de:
            com.applovin.impl.adview.activity.b.f r0 = new com.applovin.impl.adview.activity.b.f     // Catch:{ all -> 0x00f0 }
            com.applovin.impl.sdk.l r4 = r8.f1431a     // Catch:{ all -> 0x00f0 }
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)     // Catch:{ all -> 0x00f0 }
            r8.b = r0     // Catch:{ all -> 0x00f0 }
            goto L_0x0123
        L_0x00f0:
            r0 = move-exception
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Failed to create FullscreenVideoAdPresenter with sdk: "
            r1.append(r2)
            com.applovin.impl.sdk.l r2 = r8.f1431a
            r1.append(r2)
            r1.append(r12)
            java.lang.String r2 = r0.getMessage()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            r13.a(r1, r0)
            return
        L_0x0112:
            com.applovin.impl.adview.activity.b.b r0 = new com.applovin.impl.adview.activity.b.b
            com.applovin.impl.sdk.l r4 = r8.f1431a
            r1 = r0
            r2 = r14
            r3 = r13
            r5 = r15
            r6 = r16
            r7 = r17
            r1.<init>(r2, r3, r4, r5, r6, r7)
        L_0x0121:
            r8.b = r0
        L_0x0123:
            com.applovin.impl.adview.activity.b.a r0 = r8.b
            r0.c()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.adview.AppLovinFullscreenActivity.present(com.applovin.impl.sdk.a.g, com.applovin.sdk.AppLovinAdClickListener, com.applovin.sdk.AppLovinAdDisplayListener, com.applovin.sdk.AppLovinAdVideoPlaybackListener):void");
    }
}
