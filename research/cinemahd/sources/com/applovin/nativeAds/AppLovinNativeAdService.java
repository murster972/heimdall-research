package com.applovin.nativeAds;

@Deprecated
public interface AppLovinNativeAdService {
    @Deprecated
    void loadNativeAds(int i, AppLovinNativeAdLoadListener appLovinNativeAdLoadListener);

    @Deprecated
    void loadNextAd(AppLovinNativeAdLoadListener appLovinNativeAdLoadListener);

    @Deprecated
    void precacheResources(AppLovinNativeAd appLovinNativeAd, AppLovinNativeAdPrecacheListener appLovinNativeAdPrecacheListener);
}
