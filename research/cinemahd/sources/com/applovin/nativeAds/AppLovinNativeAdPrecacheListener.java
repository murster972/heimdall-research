package com.applovin.nativeAds;

@Deprecated
public interface AppLovinNativeAdPrecacheListener {
    @Deprecated
    void onNativeAdImagePrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i);

    @Deprecated
    void onNativeAdImagesPrecached(AppLovinNativeAd appLovinNativeAd);

    @Deprecated
    void onNativeAdVideoPrecachingFailed(AppLovinNativeAd appLovinNativeAd, int i);

    @Deprecated
    void onNativeAdVideoPreceached(AppLovinNativeAd appLovinNativeAd);
}
