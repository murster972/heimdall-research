package com.applovin.nativeAds;

import android.content.Context;
import com.applovin.sdk.AppLovinPostbackListener;

@Deprecated
public interface AppLovinNativeAd {
    @Deprecated
    long getAdId();

    @Deprecated
    String getCaptionText();

    @Deprecated
    String getCtaText();

    @Deprecated
    String getDescriptionText();

    @Deprecated
    String getIconUrl();

    @Deprecated
    String getImageUrl();

    @Deprecated
    float getStarRating();

    @Deprecated
    String getTitle();

    @Deprecated
    String getVideoEndTrackingUrl(int i, boolean z);

    @Deprecated
    String getVideoStartTrackingUrl();

    @Deprecated
    String getVideoUrl();

    @Deprecated
    String getZoneId();

    @Deprecated
    boolean isImagePrecached();

    @Deprecated
    boolean isVideoPrecached();

    @Deprecated
    void launchClickTarget(Context context);

    @Deprecated
    void trackImpression();

    @Deprecated
    void trackImpression(AppLovinPostbackListener appLovinPostbackListener);
}
