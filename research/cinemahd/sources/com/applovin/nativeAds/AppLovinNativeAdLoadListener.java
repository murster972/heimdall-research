package com.applovin.nativeAds;

import java.util.List;

@Deprecated
public interface AppLovinNativeAdLoadListener {
    @Deprecated
    void onNativeAdsFailedToLoad(int i);

    @Deprecated
    void onNativeAdsLoaded(List<AppLovinNativeAd> list);
}
