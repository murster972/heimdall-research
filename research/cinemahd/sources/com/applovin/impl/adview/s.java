package com.applovin.impl.adview;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.facebook.react.uimanager.ViewProps;
import org.json.JSONObject;

public class s {

    /* renamed from: a  reason: collision with root package name */
    private final int f1589a;
    private final int b;
    private final int c;
    private final int d;
    private final boolean e;
    private final int f;
    private final int g;
    private final int h;
    private final float i;
    private final float j;

    public s(JSONObject jSONObject, l lVar) {
        r j0 = lVar.j0();
        j0.c("VideoButtonProperties", "Updating video button properties with JSON = " + j.e(jSONObject));
        this.f1589a = j.b(jSONObject, "width", 64, lVar);
        this.b = j.b(jSONObject, "height", 7, lVar);
        this.c = j.b(jSONObject, ViewProps.MARGIN, 20, lVar);
        this.d = j.b(jSONObject, "gravity", 85, lVar);
        this.e = j.a(jSONObject, "tap_to_fade", (Boolean) false, lVar).booleanValue();
        this.f = j.b(jSONObject, "tap_to_fade_duration_milliseconds", 500, lVar);
        this.g = j.b(jSONObject, "fade_in_duration_milliseconds", 500, lVar);
        this.h = j.b(jSONObject, "fade_out_duration_milliseconds", 500, lVar);
        this.i = j.a(jSONObject, "fade_in_delay_seconds", 1.0f, lVar);
        this.j = j.a(jSONObject, "fade_out_delay_seconds", 6.0f, lVar);
    }

    public int a() {
        return this.f1589a;
    }

    public int b() {
        return this.b;
    }

    public int c() {
        return this.c;
    }

    public int d() {
        return this.d;
    }

    public boolean e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || s.class != obj.getClass()) {
            return false;
        }
        s sVar = (s) obj;
        return this.f1589a == sVar.f1589a && this.b == sVar.b && this.c == sVar.c && this.d == sVar.d && this.e == sVar.e && this.f == sVar.f && this.g == sVar.g && this.h == sVar.h && Float.compare(sVar.i, this.i) == 0 && Float.compare(sVar.j, this.j) == 0;
    }

    public long f() {
        return (long) this.f;
    }

    public long g() {
        return (long) this.g;
    }

    public long h() {
        return (long) this.h;
    }

    public int hashCode() {
        int i2 = ((((((((((((((this.f1589a * 31) + this.b) * 31) + this.c) * 31) + this.d) * 31) + (this.e ? 1 : 0)) * 31) + this.f) * 31) + this.g) * 31) + this.h) * 31;
        float f2 = this.i;
        int i3 = 0;
        int floatToIntBits = (i2 + (f2 != 0.0f ? Float.floatToIntBits(f2) : 0)) * 31;
        float f3 = this.j;
        if (f3 != 0.0f) {
            i3 = Float.floatToIntBits(f3);
        }
        return floatToIntBits + i3;
    }

    public float i() {
        return this.i;
    }

    public float j() {
        return this.j;
    }

    public String toString() {
        return "VideoButtonProperties{widthPercentOfScreen=" + this.f1589a + ", heightPercentOfScreen=" + this.b + ", margin=" + this.c + ", gravity=" + this.d + ", tapToFade=" + this.e + ", tapToFadeDurationMillis=" + this.f + ", fadeInDurationMillis=" + this.g + ", fadeOutDurationMillis=" + this.h + ", fadeInDelay=" + this.i + ", fadeOutDelay=" + this.j + '}';
    }
}
