package com.applovin.impl.adview;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.StrictMode;
import android.text.TextUtils;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.adview.AppLovinInterstitialActivity;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.h;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class o implements AppLovinInterstitialAdDialog {
    private static final Map<String, o> k = Collections.synchronizedMap(new HashMap());
    public static volatile boolean l = false;
    public static volatile boolean m = false;

    /* renamed from: a  reason: collision with root package name */
    private final String f1580a;
    protected final l b;
    private final WeakReference<Context> c;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener d;
    private volatile AppLovinAdDisplayListener e;
    private volatile AppLovinAdVideoPlaybackListener f;
    private volatile AppLovinAdClickListener g;
    private volatile g h;
    private volatile g.b i;
    /* access modifiers changed from: private */
    public volatile j j;

    o(AppLovinSdk appLovinSdk, Context context) {
        if (appLovinSdk == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (context != null) {
            this.b = appLovinSdk.coreSdk;
            this.f1580a = UUID.randomUUID().toString();
            this.c = new WeakReference<>(context);
            l = true;
            m = false;
        } else {
            throw new IllegalArgumentException("No context specified");
        }
    }

    public static o a(String str) {
        return k.get(str);
    }

    /* access modifiers changed from: private */
    public void a(final int i2) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (o.this.d != null) {
                    o.this.d.failedToReceiveAd(i2);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        Intent intent = new Intent(context, this.h.o0() ? AppLovinFullscreenActivity.class : AppLovinInterstitialActivity.class);
        intent.putExtra("com.applovin.interstitial.wrapper_id", this.f1580a);
        intent.putExtra("com.applovin.interstitial.sdk_key", this.b.h0());
        n.lastKnownWrapper = this;
        AppLovinFullscreenActivity.parentInterstitialWrapper = this;
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        if (context instanceof Activity) {
            context.startActivity(intent);
            ((Activity) context).overridePendingTransition(0, 0);
        } else {
            intent.setFlags(268435456);
            context.startActivity(intent);
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    private void a(g gVar, final Context context) {
        if (this.b.D().b() == null) {
            gVar.b(true);
            this.b.p().a(com.applovin.impl.sdk.d.g.p);
        }
        k.put(this.f1580a, this);
        if (((Boolean) this.b.a(b.N3)).booleanValue()) {
            this.b.o().b().execute(new Runnable(this) {
                public void run() {
                    System.gc();
                }
            });
        }
        this.h = gVar;
        this.i = this.h.p0();
        final long max = Math.max(0, ((Long) this.b.a(b.u1)).longValue());
        r j0 = this.b.j0();
        j0.b("InterstitialAdDialogWrapper", "Presenting ad with delay of " + max);
        a(gVar, context, new Runnable() {
            public void run() {
                new Handler(context.getMainLooper()).postDelayed(new Runnable() {
                    public void run() {
                        AnonymousClass3 r0 = AnonymousClass3.this;
                        o.this.a(context);
                    }
                }, max);
            }
        });
    }

    private void a(g gVar, Context context, final Runnable runnable) {
        if (!TextUtils.isEmpty(gVar.k()) || !gVar.O() || h.a(context) || !(context instanceof Activity)) {
            runnable.run();
            return;
        }
        AlertDialog create = new AlertDialog.Builder(context).setTitle(gVar.P()).setMessage(gVar.Q()).setPositiveButton(gVar.R(), (DialogInterface.OnClickListener) null).create();
        create.setOnDismissListener(new DialogInterface.OnDismissListener(this) {
            public void onDismiss(DialogInterface dialogInterface) {
                runnable.run();
            }
        });
        create.show();
    }

    private void a(AppLovinAd appLovinAd) {
        if (this.e != null) {
            this.e.adHidden(appLovinAd);
        }
    }

    /* access modifiers changed from: private */
    public void b(final AppLovinAd appLovinAd) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (o.this.d != null) {
                    o.this.d.adReceived(appLovinAd);
                }
            }
        });
    }

    private Context h() {
        WeakReference<Context> weakReference = this.c;
        if (weakReference != null) {
            return (Context) weakReference.get();
        }
        return null;
    }

    public l a() {
        return this.b;
    }

    public void a(j jVar) {
        this.j = jVar;
    }

    /* access modifiers changed from: protected */
    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.b.c0().loadNextAd(AppLovinAdSize.INTERSTITIAL, appLovinAdLoadListener);
    }

    public g b() {
        return this.h;
    }

    public AppLovinAdVideoPlaybackListener c() {
        return this.f;
    }

    public AppLovinAdDisplayListener d() {
        return this.e;
    }

    public void dismiss() {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (o.this.j != null) {
                    o.this.j.dismiss();
                }
            }
        });
    }

    public AppLovinAdClickListener e() {
        return this.g;
    }

    public g.b f() {
        return this.i;
    }

    public void g() {
        l = false;
        m = true;
        k.remove(this.f1580a);
        if (this.h != null) {
            this.j = null;
        }
    }

    public boolean isAdReadyToDisplay() {
        return this.b.c0().hasPreloadedAd(AppLovinAdSize.INTERSTITIAL);
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.g = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.e = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.d = appLovinAdLoadListener;
    }

    public void setAdVideoPlaybackListener(AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.f = appLovinAdVideoPlaybackListener;
    }

    public void show() {
        a((AppLovinAdLoadListener) new AppLovinAdLoadListener() {
            public void adReceived(AppLovinAd appLovinAd) {
                o.this.b(appLovinAd);
                o.this.showAndRender(appLovinAd);
            }

            public void failedToReceiveAd(int i) {
                o.this.a(i);
            }
        });
    }

    public void showAndRender(AppLovinAd appLovinAd) {
        String str;
        r rVar;
        Context h2 = h();
        if (h2 != null) {
            AppLovinAd a2 = com.applovin.impl.sdk.utils.r.a(appLovinAd, this.b);
            if (a2 == null) {
                rVar = this.b.j0();
                str = "Failed to show ad: " + appLovinAd;
            } else if (((AppLovinAdBase) a2).hasShown() && ((Boolean) this.b.a(b.b1)).booleanValue()) {
                throw new IllegalStateException("Failed to display ad - ad can only be displayed once. Load the next ad.");
            } else if (a2 instanceof g) {
                a((g) a2, h2);
                return;
            } else {
                this.b.j0().e("InterstitialAdDialogWrapper", "Failed to show interstitial: unknown ad type provided: '" + a2 + "'");
                a(a2);
                return;
            }
        } else {
            rVar = this.b.j0();
            str = "Failed to show interstitial: stale activity reference provided";
        }
        rVar.e("InterstitialAdDialogWrapper", str);
        a(appLovinAd);
    }

    public String toString() {
        return "AppLovinInterstitialAdDialog{}";
    }
}
