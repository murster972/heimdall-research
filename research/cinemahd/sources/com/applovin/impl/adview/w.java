package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.applovin.impl.adview.h;

@SuppressLint({"ViewConstructor"})
public final class w extends h {
    private static final Paint b = new Paint(1);
    private static final Paint c = new Paint(1);
    private static final Paint d = new Paint(1);

    public w(Context context) {
        super(context);
        b.setColor(-1);
        c.setColor(-16777216);
        d.setColor(-1);
        d.setStyle(Paint.Style.STROKE);
    }

    /* access modifiers changed from: protected */
    public float getCenter() {
        return getSize() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getCrossOffset() {
        return this.f1531a * 10.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleOffset() {
        return this.f1531a * 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getInnerCircleRadius() {
        return getCenter() - getInnerCircleOffset();
    }

    /* access modifiers changed from: protected */
    public float getStrokeWidth() {
        return this.f1531a * 3.0f;
    }

    public h.a getStyle() {
        return h.a.WHITE_ON_BLACK;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float center = getCenter();
        canvas.drawCircle(center, center, center, b);
        canvas.drawCircle(center, center, getInnerCircleRadius(), c);
        float crossOffset = getCrossOffset();
        float size = getSize() - crossOffset;
        d.setStrokeWidth(getStrokeWidth());
        Canvas canvas2 = canvas;
        float f = crossOffset;
        float f2 = size;
        canvas2.drawLine(f, crossOffset, f2, size, d);
        canvas2.drawLine(f, size, f2, crossOffset, d);
    }
}
