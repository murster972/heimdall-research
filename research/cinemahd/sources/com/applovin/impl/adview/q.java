package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import com.applovin.impl.adview.h;

@SuppressLint({"ViewConstructor"})
public final class q extends h {
    private static final Paint b = new Paint(1);
    private static final Paint c = new Paint(1);

    public q(Context context) {
        super(context);
        b.setARGB(80, 0, 0, 0);
        c.setColor(-1);
        c.setStyle(Paint.Style.STROKE);
    }

    public void a(int i) {
        setViewScale(((float) i) / 30.0f);
    }

    /* access modifiers changed from: protected */
    public float getCenter() {
        return getSize() / 2.0f;
    }

    /* access modifiers changed from: protected */
    public float getCrossOffset() {
        return this.f1531a * 8.0f;
    }

    /* access modifiers changed from: protected */
    public float getStrokeWidth() {
        return this.f1531a * 2.0f;
    }

    public h.a getStyle() {
        return h.a.WHITE_ON_TRANSPARENT;
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float center = getCenter();
        canvas.drawCircle(center, center, center, b);
        float crossOffset = getCrossOffset();
        float size = getSize() - crossOffset;
        c.setStrokeWidth(getStrokeWidth());
        Canvas canvas2 = canvas;
        float f = crossOffset;
        float f2 = size;
        canvas2.drawLine(f, crossOffset, f2, size, c);
        canvas2.drawLine(f, size, f2, crossOffset, c);
    }
}
