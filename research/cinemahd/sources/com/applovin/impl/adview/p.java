package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import com.applovin.impl.adview.h;

@SuppressLint({"ViewConstructor"})
public final class p extends h {
    public p(Context context) {
        super(context);
    }

    public void a(int i) {
        setViewScale(((float) i) / 30.0f);
    }

    public h.a getStyle() {
        return h.a.INVISIBLE;
    }
}
