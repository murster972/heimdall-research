package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.webkit.WebSettings;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.facebook.react.uimanager.ViewProps;
import org.json.JSONObject;

public final class v {

    /* renamed from: a  reason: collision with root package name */
    private l f1591a;
    private JSONObject b;

    public v(JSONObject jSONObject, l lVar) {
        this.f1591a = lVar;
        this.b = jSONObject;
    }

    /* access modifiers changed from: package-private */
    @TargetApi(21)
    public Integer a() {
        int i;
        String b2 = j.b(this.b, "mixed_content_mode", (String) null, this.f1591a);
        if (o.b(b2)) {
            if ("always_allow".equalsIgnoreCase(b2)) {
                i = 0;
            } else if ("never_allow".equalsIgnoreCase(b2)) {
                i = 1;
            } else if ("compatibility_mode".equalsIgnoreCase(b2)) {
                i = 2;
            }
            return Integer.valueOf(i);
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public WebSettings.PluginState b() {
        String b2 = j.b(this.b, "plugin_state", (String) null, this.f1591a);
        if (o.b(b2)) {
            if (ViewProps.ON.equalsIgnoreCase(b2)) {
                return WebSettings.PluginState.ON;
            }
            if ("on_demand".equalsIgnoreCase(b2)) {
                return WebSettings.PluginState.ON_DEMAND;
            }
            if ("off".equalsIgnoreCase(b2)) {
                return WebSettings.PluginState.OFF;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public Boolean c() {
        return j.a(this.b, "allow_file_access", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean d() {
        return j.a(this.b, "load_with_overview_mode", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean e() {
        return j.a(this.b, "use_wide_view_port", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean f() {
        return j.a(this.b, "allow_content_access", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean g() {
        return j.a(this.b, "use_built_in_zoom_controls", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean h() {
        return j.a(this.b, "display_zoom_controls", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean i() {
        return j.a(this.b, "save_form_data", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean j() {
        return j.a(this.b, "geolocation_enabled", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean k() {
        return j.a(this.b, "need_initial_focus", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean l() {
        return j.a(this.b, "allow_file_access_from_file_urls", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean m() {
        return j.a(this.b, "allow_universal_access_from_file_urls", (Boolean) null, this.f1591a);
    }

    /* access modifiers changed from: package-private */
    public Boolean n() {
        return j.a(this.b, "offscreen_pre_raster", (Boolean) null, this.f1591a);
    }
}
