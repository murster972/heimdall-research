package com.applovin.impl.adview;

import android.webkit.ConsoleMessage;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;

class b extends WebChromeClient {

    /* renamed from: a  reason: collision with root package name */
    private final r f1523a;

    public b(l lVar) {
        this.f1523a = lVar.j0();
    }

    public void onConsoleMessage(String str, int i, String str2) {
        r rVar = this.f1523a;
        rVar.d("AdWebView", "console.log[" + i + "] :" + str);
    }

    public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
        this.f1523a.b("AdWebView", consoleMessage.sourceId() + ": " + consoleMessage.lineNumber() + ": " + consoleMessage.message());
        return true;
    }

    public boolean onJsAlert(WebView webView, String str, String str2, JsResult jsResult) {
        r rVar = this.f1523a;
        rVar.d("AdWebView", "Alert attempted: " + str2);
        return true;
    }

    public boolean onJsBeforeUnload(WebView webView, String str, String str2, JsResult jsResult) {
        r rVar = this.f1523a;
        rVar.d("AdWebView", "JS onBeforeUnload attempted: " + str2);
        return true;
    }

    public boolean onJsConfirm(WebView webView, String str, String str2, JsResult jsResult) {
        r rVar = this.f1523a;
        rVar.d("AdWebView", "JS confirm attempted: " + str2);
        return true;
    }
}
