package com.applovin.impl.adview;

import android.os.Handler;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

public final class i {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final r f1533a;
    private final Handler b;
    private final Set<b> c = new HashSet();
    /* access modifiers changed from: private */
    public final AtomicInteger d = new AtomicInteger();

    public interface a {
        void a();

        boolean b();
    }

    private static class b {

        /* renamed from: a  reason: collision with root package name */
        private final String f1535a;
        private final a b;
        private final long c;

        private b(String str, long j, a aVar) {
            this.f1535a = str;
            this.c = j;
            this.b = aVar;
        }

        /* access modifiers changed from: private */
        public String a() {
            return this.f1535a;
        }

        /* access modifiers changed from: private */
        public long b() {
            return this.c;
        }

        /* access modifiers changed from: private */
        public a c() {
            return this.b;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            String str = this.f1535a;
            String str2 = ((b) obj).f1535a;
            return str != null ? str.equalsIgnoreCase(str2) : str2 == null;
        }

        public int hashCode() {
            String str = this.f1535a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return "CountdownProxy{identifier='" + this.f1535a + '\'' + ", countdownStepMillis=" + this.c + '}';
        }
    }

    public i(Handler handler, l lVar) {
        if (handler == null) {
            throw new IllegalArgumentException("No handler specified.");
        } else if (lVar != null) {
            this.b = handler;
            this.f1533a = lVar.j0();
        } else {
            throw new IllegalArgumentException("No sdk specified.");
        }
    }

    /* access modifiers changed from: private */
    public void a(final b bVar, final int i) {
        this.b.postDelayed(new Runnable() {
            public void run() {
                a b2 = bVar.c();
                if (!b2.b()) {
                    r b3 = i.this.f1533a;
                    b3.b("CountdownManager", "Ending countdown for " + bVar.a());
                } else if (i.this.d.get() == i) {
                    try {
                        b2.a();
                    } catch (Throwable th) {
                        r b4 = i.this.f1533a;
                        b4.b("CountdownManager", "Encountered error on countdown step for: " + bVar.a(), th);
                    }
                    i.this.a(bVar, i);
                } else {
                    r b5 = i.this.f1533a;
                    b5.d("CountdownManager", "Killing duplicate countdown from previous generation: " + bVar.a());
                }
            }
        }, bVar.b());
    }

    public void a() {
        HashSet<b> hashSet = new HashSet<>(this.c);
        r rVar = this.f1533a;
        rVar.b("CountdownManager", "Starting " + hashSet.size() + " countdowns...");
        int incrementAndGet = this.d.incrementAndGet();
        for (b bVar : hashSet) {
            r rVar2 = this.f1533a;
            rVar2.b("CountdownManager", "Starting countdown: " + bVar.a() + " for generation " + incrementAndGet + "...");
            a(bVar, incrementAndGet);
        }
    }

    public void a(String str, long j, a aVar) {
        if (j <= 0) {
            throw new IllegalArgumentException("Invalid step specified.");
        } else if (this.b != null) {
            r rVar = this.f1533a;
            rVar.b("CountdownManager", "Adding countdown: " + str);
            this.c.add(new b(str, j, aVar));
        } else {
            throw new IllegalArgumentException("No handler specified.");
        }
    }

    public void b() {
        this.f1533a.b("CountdownManager", "Removing all countdowns...");
        c();
        this.c.clear();
    }

    public void c() {
        this.f1533a.b("CountdownManager", "Stopping countdowns...");
        this.d.incrementAndGet();
        this.b.removeCallbacksAndMessages((Object) null);
    }
}
