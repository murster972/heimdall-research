package com.applovin.impl.adview;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PointF;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.a.b;
import com.applovin.impl.a.i;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.d.c;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinWebViewActivity;

class d extends WebViewClient {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1526a;
    private final r b;
    /* access modifiers changed from: private */
    public final AdViewControllerImpl c;

    public d(AdViewControllerImpl adViewControllerImpl, l lVar) {
        this.f1526a = lVar;
        this.b = lVar.j0();
        this.c = adViewControllerImpl;
    }

    private void a() {
        this.c.a();
    }

    private void a(PointF pointF) {
        this.c.expandAd(pointF);
    }

    private void a(Uri uri, c cVar) {
        String str;
        r rVar;
        final String queryParameter = uri.getQueryParameter("n");
        if (o.b(queryParameter)) {
            String queryParameter2 = uri.getQueryParameter("load_type");
            if ("external".equalsIgnoreCase(queryParameter2)) {
                r rVar2 = this.b;
                rVar2.b("AdWebView", "Loading new page externally: " + queryParameter);
                com.applovin.impl.sdk.utils.r.a(cVar.getContext(), Uri.parse(queryParameter), this.f1526a);
                k.c(this.c.getAdViewEventListener(), (AppLovinAd) this.c.getCurrentAd(), this.c.getParentView());
                return;
            } else if ("internal".equalsIgnoreCase(queryParameter2)) {
                r rVar3 = this.b;
                rVar3.b("AdWebView", "Loading new page in WebView: " + queryParameter);
                cVar.loadUrl(queryParameter);
                String queryParameter3 = uri.getQueryParameter("bg_color");
                if (o.b(queryParameter3)) {
                    cVar.setBackgroundColor(Color.parseColor(queryParameter3));
                    return;
                }
                return;
            } else if ("in_app".equalsIgnoreCase(queryParameter2)) {
                r rVar4 = this.b;
                rVar4.b("AdWebView", "Loading new page in slide-up webview: " + queryParameter);
                this.f1526a.D().a(new a() {
                    public void onActivityCreated(Activity activity, Bundle bundle) {
                        if (activity instanceof AppLovinWebViewActivity) {
                            ((AppLovinWebViewActivity) activity).loadUrl(queryParameter, (AppLovinWebViewActivity.EventListener) null);
                            k.a(d.this.c.getAdViewEventListener(), (AppLovinAd) d.this.c.getCurrentAd(), d.this.c.getParentView());
                        }
                    }

                    public void onActivityDestroyed(Activity activity) {
                        if (activity instanceof AppLovinWebViewActivity) {
                            k.b(d.this.c.getAdViewEventListener(), (AppLovinAd) d.this.c.getCurrentAd(), d.this.c.getParentView());
                            d.this.f1526a.D().b(this);
                        }
                    }
                });
                Intent intent = new Intent(this.f1526a.i(), AppLovinWebViewActivity.class);
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1526a.h0());
                intent.setFlags(268435456);
                this.f1526a.i().startActivity(intent);
                return;
            } else {
                rVar = this.b;
                str = "Could not find load type in original uri";
            }
        } else {
            rVar = this.b;
            str = "Could not find url to load from query in original uri";
        }
        rVar.e("AdWebView", str);
    }

    private void a(com.applovin.impl.a.a aVar, c cVar) {
        b N0 = aVar.N0();
        if (N0 != null) {
            i.a(N0.c(), this.c.getSdk());
            a(cVar, N0.a());
        }
    }

    private void a(c cVar, Uri uri) {
        g currentAd = cVar.getCurrentAd();
        AppLovinAdView parentView = this.c.getParentView();
        if (parentView == null || currentAd == null) {
            r rVar = this.b;
            rVar.e("AdWebView", "Attempting to track click that is null or not an ApplovinAdView instance for clickedUri = " + uri);
            return;
        }
        com.applovin.impl.sdk.d.d statsManagerHelper = cVar.getStatsManagerHelper();
        if (statsManagerHelper != null) {
            statsManagerHelper.b();
        }
        this.c.a(currentAd, parentView, uri, cVar.getAndClearLastClickLocation());
    }

    /* JADX WARNING: Code restructure failed: missing block: B:48:0x0114, code lost:
        if (r6.B0() != false) goto L_0x00a5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean a(android.webkit.WebView r9, java.lang.String r10, boolean r11) {
        /*
            r8 = this;
            com.applovin.impl.sdk.r r0 = r8.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Processing click on ad URL \""
            r1.append(r2)
            r1.append(r10)
            java.lang.String r2 = "\""
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "AdWebView"
            r0.c(r2, r1)
            r0 = 1
            if (r10 == 0) goto L_0x011d
            boolean r1 = r9 instanceof com.applovin.impl.adview.c
            if (r1 == 0) goto L_0x011d
            android.net.Uri r1 = android.net.Uri.parse(r10)
            com.applovin.impl.adview.c r9 = (com.applovin.impl.adview.c) r9
            java.lang.String r3 = r1.getScheme()
            java.lang.String r4 = r1.getHost()
            java.lang.String r5 = r1.getPath()
            com.applovin.impl.adview.AdViewControllerImpl r6 = r8.c
            com.applovin.impl.sdk.a.g r6 = r6.getCurrentAd()
            if (r6 != 0) goto L_0x0046
            com.applovin.impl.sdk.r r9 = r8.b
            java.lang.String r10 = "Unable to process click, ad not found!"
            r9.e(r2, r10)
            return r0
        L_0x0046:
            java.lang.String r7 = "applovin"
            boolean r7 = r7.equals(r3)
            if (r7 == 0) goto L_0x00df
            java.lang.String r7 = "com.applovin.sdk"
            boolean r7 = r7.equals(r4)
            if (r7 == 0) goto L_0x00df
            java.lang.String r11 = "/adservice/close_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0063
            r8.a()
            goto L_0x011d
        L_0x0063:
            java.lang.String r11 = "/adservice/expand_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0074
            android.graphics.PointF r9 = r9.getAndClearLastClickLocation()
            r8.a((android.graphics.PointF) r9)
            goto L_0x011d
        L_0x0074:
            java.lang.String r11 = "/adservice/contract_ad"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0081
            r8.b()
            goto L_0x011d
        L_0x0081:
            java.lang.String r11 = "/adservice/no_op"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x008a
            return r0
        L_0x008a:
            java.lang.String r11 = "/adservice/load_url"
            boolean r11 = r11.equals(r5)
            if (r11 == 0) goto L_0x0097
            r8.a((android.net.Uri) r1, (com.applovin.impl.adview.c) r9)
            goto L_0x011d
        L_0x0097:
            java.lang.String r11 = "/adservice/track_click_now"
            boolean r1 = r11.equals(r5)
            if (r1 == 0) goto L_0x00b2
            boolean r10 = r6 instanceof com.applovin.impl.a.a
            if (r10 == 0) goto L_0x00aa
            com.applovin.impl.a.a r6 = (com.applovin.impl.a.a) r6
        L_0x00a5:
            r8.a((com.applovin.impl.a.a) r6, (com.applovin.impl.adview.c) r9)
            goto L_0x011d
        L_0x00aa:
            android.net.Uri r10 = android.net.Uri.parse(r11)
            r8.a((com.applovin.impl.adview.c) r9, (android.net.Uri) r10)
            goto L_0x011d
        L_0x00b2:
            com.applovin.impl.sdk.r r9 = r8.b
            java.lang.StringBuilder r11 = new java.lang.StringBuilder
            r11.<init>()
            java.lang.String r1 = "Unknown URL: "
            r11.append(r1)
            r11.append(r10)
            java.lang.String r10 = r11.toString()
            r9.d(r2, r10)
            com.applovin.impl.sdk.r r9 = r8.b
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "Path: "
            r10.append(r11)
            r10.append(r5)
            java.lang.String r10 = r10.toString()
            r9.d(r2, r10)
            goto L_0x011d
        L_0x00df:
            if (r11 == 0) goto L_0x011b
            java.util.List r10 = r6.i0()
            java.util.List r11 = r6.j0()
            boolean r5 = r10.isEmpty()
            if (r5 != 0) goto L_0x00f5
            boolean r10 = r10.contains(r3)
            if (r10 == 0) goto L_0x0102
        L_0x00f5:
            boolean r10 = r11.isEmpty()
            if (r10 != 0) goto L_0x010a
            boolean r10 = r11.contains(r4)
            if (r10 == 0) goto L_0x0102
            goto L_0x010a
        L_0x0102:
            com.applovin.impl.sdk.r r9 = r8.b
            java.lang.String r10 = "URL is not whitelisted - bypassing click"
            r9.e(r2, r10)
            goto L_0x011d
        L_0x010a:
            boolean r10 = r6 instanceof com.applovin.impl.a.a
            if (r10 == 0) goto L_0x0117
            com.applovin.impl.a.a r6 = (com.applovin.impl.a.a) r6
            boolean r10 = r6.B0()
            if (r10 == 0) goto L_0x0117
            goto L_0x00a5
        L_0x0117:
            r8.a((com.applovin.impl.adview.c) r9, (android.net.Uri) r1)
            goto L_0x011d
        L_0x011b:
            r9 = 0
            return r9
        L_0x011d:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.d.a(android.webkit.WebView, java.lang.String, boolean):boolean");
    }

    private void b() {
        this.c.contractAd();
    }

    public void onLoadResource(WebView webView, String str) {
        super.onLoadResource(webView, str);
        r rVar = this.b;
        rVar.c("AdWebView", "Loaded resource: " + str);
    }

    public void onPageFinished(WebView webView, String str) {
        super.onPageFinished(webView, str);
        this.c.onAdHtmlLoaded(webView);
    }

    public void onReceivedError(WebView webView, int i, String str, String str2) {
        super.onReceivedError(webView, i, str, str2);
        g currentAd = this.c.getCurrentAd();
        String str3 = "Received error with error code: " + i + " with description \\'" + str + "\\' for URL: " + str2;
        if (currentAd != null) {
            c.b a2 = this.f1526a.A().a((AppLovinAdBase) currentAd);
            a2.a(com.applovin.impl.sdk.d.b.C, str3);
            a2.a();
        }
        this.b.e("AdWebView", str3 + " for ad: " + currentAd);
    }

    @TargetApi(23)
    public void onReceivedError(WebView webView, WebResourceRequest webResourceRequest, WebResourceError webResourceError) {
        super.onReceivedError(webView, webResourceRequest, webResourceError);
        onReceivedError(webView, webResourceError.getErrorCode(), webResourceError.getDescription().toString(), webResourceRequest.getUrl().toString());
    }

    @TargetApi(21)
    public void onReceivedHttpError(WebView webView, WebResourceRequest webResourceRequest, WebResourceResponse webResourceResponse) {
        super.onReceivedHttpError(webView, webResourceRequest, webResourceResponse);
        g currentAd = this.c.getCurrentAd();
        c.b a2 = this.f1526a.A().a((AppLovinAdBase) currentAd);
        a2.a(com.applovin.impl.sdk.d.b.D);
        a2.a();
        r rVar = this.b;
        rVar.e("AdWebView", "Received HTTP error: " + webResourceResponse + "for url: " + webResourceRequest.getUrl() + " and ad: " + currentAd);
    }

    public void onReceivedSslError(WebView webView, SslErrorHandler sslErrorHandler, SslError sslError) {
        super.onReceivedSslError(webView, sslErrorHandler, sslError);
        g currentAd = this.c.getCurrentAd();
        String str = "Received SSL error: " + sslError;
        c.b a2 = this.f1526a.A().a((AppLovinAdBase) currentAd);
        a2.a(com.applovin.impl.sdk.d.b.F, str);
        a2.a();
        this.b.e("AdWebView", str + " for ad: " + currentAd);
    }

    @TargetApi(26)
    public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
        r.i("AdWebView", "Render process gone for ad: " + this.c.getCurrentAd() + ". Process did crash: " + renderProcessGoneDetail.didCrash());
        g currentAd = this.c.getCurrentAd();
        if (currentAd != null) {
            c.b a2 = this.f1526a.A().a((AppLovinAdBase) currentAd);
            a2.a(com.applovin.impl.sdk.d.b.E);
            a2.a();
        }
        if (!((Boolean) this.f1526a.a(com.applovin.impl.sdk.c.b.L3)).booleanValue()) {
            return super.onRenderProcessGone(webView, renderProcessGoneDetail);
        }
        if (renderProcessGoneDetail.didCrash() && ((Boolean) this.f1526a.a(com.applovin.impl.sdk.c.b.O3)).booleanValue()) {
            String valueOf = currentAd != null ? String.valueOf(currentAd.getAdIdNumber()) : "null";
            throw new RuntimeException("Render process crashed. This is likely caused by a crash in an AppLovin ad with ID: " + valueOf);
        } else if (webView == null || !webView.equals(this.c.getAdWebView())) {
            return true;
        } else {
            this.c.destroy();
            AppLovinAdSize size = this.c.getSize();
            if (!com.applovin.impl.sdk.utils.r.a(size)) {
                return true;
            }
            this.c.attachNewAdView(size);
            this.c.resume();
            return true;
        }
    }

    @TargetApi(24)
    public boolean shouldOverrideUrlLoading(WebView webView, WebResourceRequest webResourceRequest) {
        boolean hasGesture = ((Boolean) this.f1526a.a(com.applovin.impl.sdk.c.b.I0)).booleanValue() ? webResourceRequest.hasGesture() : true;
        Uri url = webResourceRequest.getUrl();
        if (url != null) {
            return a(webView, url.toString(), hasGesture);
        }
        this.b.e("AdWebView", "No url found for request");
        return false;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        return a(webView, str, true);
    }
}
