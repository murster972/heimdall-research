package com.applovin.impl.adview.activity.a;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.adview.a;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.sdk.AppLovinSdkUtils;

public class c extends a {
    public c(g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, l lVar) {
        super(gVar, appLovinFullscreenActivity, lVar);
    }

    public void a(ImageView imageView, com.applovin.impl.adview.l lVar, a aVar, ProgressBar progressBar, View view, AppLovinAdView appLovinAdView) {
        view.setLayoutParams(this.e);
        this.d.addView(view);
        appLovinAdView.setLayoutParams(this.e);
        this.d.addView(appLovinAdView);
        appLovinAdView.setVisibility(4);
        if (lVar != null) {
            a(this.c.u(), (this.c.z() ? 3 : 5) | 48, lVar);
        }
        if (imageView != null) {
            int dpToPx = AppLovinSdkUtils.dpToPx(this.b, ((Integer) this.f1483a.a(b.F1)).intValue());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(dpToPx, dpToPx, ((Integer) this.f1483a.a(b.H1)).intValue());
            int dpToPx2 = AppLovinSdkUtils.dpToPx(this.b, ((Integer) this.f1483a.a(b.G1)).intValue());
            layoutParams.setMargins(dpToPx2, dpToPx2, dpToPx2, dpToPx2);
            this.d.addView(imageView, layoutParams);
        }
        if (aVar != null) {
            this.d.addView(aVar, this.e);
        }
        if (progressBar != null) {
            FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(-1, 20, 80);
            layoutParams2.setMargins(0, 0, 0, ((Integer) this.f1483a.a(b.L1)).intValue());
            this.d.addView(progressBar, layoutParams2);
        }
        this.b.setContentView(this.d);
    }

    public void a(com.applovin.impl.adview.l lVar, View view) {
        view.setVisibility(0);
        com.applovin.impl.sdk.utils.b.a(this.d, view);
        if (lVar != null) {
            a(this.c.u(), (this.c.y() ? 3 : 5) | 48, lVar);
        }
    }
}
