package com.applovin.impl.adview.activity;

import android.content.ComponentName;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.adview.activity.FullscreenAdService;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.e.p;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class a implements ServiceConnection {

    /* renamed from: a  reason: collision with root package name */
    private final l f1481a;
    /* access modifiers changed from: private */
    public final r b;
    /* access modifiers changed from: private */
    public final WeakReference<AppLovinFullscreenActivity> c;
    private final AtomicBoolean d = new AtomicBoolean();
    /* access modifiers changed from: private */
    public Messenger e;

    /* renamed from: com.applovin.impl.adview.activity.a$a  reason: collision with other inner class name */
    private class C0008a implements AppLovinAdClickListener, AppLovinAdDisplayListener, AppLovinAdVideoPlaybackListener {
        private C0008a() {
        }

        private void a(Bundle bundle, FullscreenAdService.b bVar) {
            Message obtain = Message.obtain((Handler) null, bVar.a());
            if (bundle != null) {
                obtain.setData(bundle);
            }
            try {
                a.this.e.send(obtain);
            } catch (RemoteException e) {
                r b = a.this.b;
                b.b("InterActivityV2", "Failed to forward callback (" + bVar.a() + ")", e);
            }
        }

        private void a(FullscreenAdService.b bVar) {
            a((Bundle) null, bVar);
        }

        public void adClicked(AppLovinAd appLovinAd) {
            a(FullscreenAdService.b.AD_CLICKED);
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            a(FullscreenAdService.b.AD_DISPLAYED);
        }

        public void adHidden(AppLovinAd appLovinAd) {
            a(FullscreenAdService.b.AD_HIDDEN);
        }

        public void videoPlaybackBegan(AppLovinAd appLovinAd) {
            a(FullscreenAdService.b.AD_VIDEO_STARTED);
        }

        public void videoPlaybackEnded(AppLovinAd appLovinAd, double d, boolean z) {
            Bundle bundle = new Bundle();
            bundle.putDouble("percent_viewed", d);
            bundle.putBoolean("fully_watched", z);
            a(bundle, FullscreenAdService.b.AD_VIDEO_ENDED);
        }
    }

    private static class b extends Handler {

        /* renamed from: a  reason: collision with root package name */
        private final WeakReference<a> f1485a;

        private b(a aVar) {
            this.f1485a = new WeakReference<>(aVar);
        }

        public void handleMessage(Message message) {
            a aVar;
            if (message.what != FullscreenAdService.b.AD.a() || (aVar = (a) this.f1485a.get()) == null) {
                super.handleMessage(message);
                return;
            }
            aVar.a(com.applovin.impl.sdk.a.b.a(message.getData().getInt(FullscreenAdService.DATA_KEY_AD_SOURCE)), message.getData().getString(FullscreenAdService.DATA_KEY_RAW_FULL_AD_RESPONSE));
        }
    }

    public a(AppLovinFullscreenActivity appLovinFullscreenActivity, l lVar) {
        this.f1481a = lVar;
        this.b = lVar.j0();
        this.c = new WeakReference<>(appLovinFullscreenActivity);
    }

    /* access modifiers changed from: private */
    public void a() {
        AppLovinFullscreenActivity appLovinFullscreenActivity = (AppLovinFullscreenActivity) this.c.get();
        if (appLovinFullscreenActivity != null) {
            this.b.b("InterActivityV2", "Dismissing...");
            appLovinFullscreenActivity.dismiss();
            return;
        }
        this.b.e("InterActivityV2", "Unable to dismiss parent Activity");
    }

    /* access modifiers changed from: private */
    public void a(com.applovin.impl.sdk.a.b bVar, String str) {
        try {
            JSONObject jSONObject = new JSONObject(str);
            com.applovin.impl.sdk.a.b bVar2 = bVar;
            this.f1481a.o().a((com.applovin.impl.sdk.e.a) new p(jSONObject, d.a(j.b(jSONObject, "zone_id", "", this.f1481a), this.f1481a), bVar2, new AppLovinAdLoadListener() {
                public void adReceived(AppLovinAd appLovinAd) {
                    AppLovinFullscreenActivity appLovinFullscreenActivity = (AppLovinFullscreenActivity) a.this.c.get();
                    if (appLovinFullscreenActivity != null) {
                        a.this.b.b("InterActivityV2", "Presenting ad...");
                        C0008a aVar = new C0008a();
                        appLovinFullscreenActivity.present((g) appLovinAd, aVar, aVar, aVar);
                        return;
                    }
                    r b = a.this.b;
                    b.e("InterActivityV2", "Unable to present ad, parent activity has been GC'd - " + appLovinAd);
                }

                public void failedToReceiveAd(int i) {
                    a.this.a();
                }
            }, this.f1481a));
        } catch (JSONException e2) {
            r rVar = this.b;
            rVar.b("InterActivityV2", "Unable to process ad: " + str, e2);
            a();
        }
    }

    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (this.d.compareAndSet(false, true)) {
            r rVar = this.b;
            rVar.b("InterActivityV2", "Fullscreen ad service connected to " + componentName);
            this.e = new Messenger(iBinder);
            Message obtain = Message.obtain((Handler) null, FullscreenAdService.b.AD.a());
            obtain.replyTo = new Messenger(new b());
            try {
                this.b.b("InterActivityV2", "Requesting ad from FullscreenAdService...");
                this.e.send(obtain);
            } catch (RemoteException e2) {
                this.b.b("InterActivityV2", "Failed to send ad request message to FullscreenAdService", e2);
                a();
            }
        }
    }

    public void onServiceDisconnected(ComponentName componentName) {
        if (this.d.compareAndSet(true, false)) {
            r rVar = this.b;
            rVar.b("InterActivityV2", "FullscreenAdService disconnected from " + componentName);
        }
    }
}
