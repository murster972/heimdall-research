package com.applovin.impl.adview.activity.a;

import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.l;

abstract class a {

    /* renamed from: a  reason: collision with root package name */
    final l f1483a;
    final AppLovinFullscreenActivity b;
    final g c;
    final ViewGroup d;
    final FrameLayout.LayoutParams e = new FrameLayout.LayoutParams(-1, -1, 17);

    a(g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, l lVar) {
        this.c = gVar;
        this.f1483a = lVar;
        this.b = appLovinFullscreenActivity;
        this.d = new FrameLayout(appLovinFullscreenActivity);
        this.d.setBackgroundColor(-16777216);
        this.d.setLayoutParams(this.e);
    }

    /* access modifiers changed from: package-private */
    public void a(g.c cVar, int i, com.applovin.impl.adview.l lVar) {
        lVar.a(cVar.f1769a, cVar.e, cVar.d, i);
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(lVar.getLayoutParams());
        int i2 = cVar.c;
        layoutParams.setMargins(i2, cVar.b, i2, 0);
        layoutParams.gravity = i;
        this.d.addView(lVar, layoutParams);
    }
}
