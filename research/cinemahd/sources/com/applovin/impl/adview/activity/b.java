package com.applovin.impl.adview.activity;

import android.content.Context;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinSdkUtils;

public class b {

    /* renamed from: a  reason: collision with root package name */
    private final AppLovinFullscreenActivity f1486a;
    private final int b = a(this.c, this.d);
    private final int c;
    private final boolean d;

    public b(AppLovinFullscreenActivity appLovinFullscreenActivity) {
        this.f1486a = appLovinFullscreenActivity;
        this.c = r.d((Context) appLovinFullscreenActivity);
        this.d = AppLovinSdkUtils.isTablet(appLovinFullscreenActivity);
    }

    private int a(int i, boolean z) {
        if (z) {
            if (i == 0) {
                return 0;
            }
            if (i == 1) {
                return 9;
            }
            if (i == 2) {
                return 8;
            }
            return i == 3 ? 1 : -1;
        } else if (i == 0) {
            return 1;
        } else {
            if (i == 1) {
                return 0;
            }
            if (i == 2) {
                return 9;
            }
            return i == 3 ? 8 : -1;
        }
    }

    private void a(int i) {
        try {
            this.f1486a.setRequestedOrientation(i);
        } catch (Throwable unused) {
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0030, code lost:
        if (r6 == 2) goto L_0x0012;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x003d, code lost:
        if (r6 == 1) goto L_0x0033;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:5:0x000d, code lost:
        if (r6 != 3) goto L_0x001a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:6:0x0010, code lost:
        if (r6 != 1) goto L_0x001a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void a(com.applovin.impl.sdk.a.g.b r5, int r6, boolean r7) {
        /*
            r4 = this;
            com.applovin.impl.sdk.a.g$b r0 = com.applovin.impl.sdk.a.g.b.ACTIVITY_PORTRAIT
            r1 = 3
            r2 = 2
            r3 = 1
            if (r5 != r0) goto L_0x0022
            r5 = 9
            if (r7 == 0) goto L_0x0016
            if (r6 == r3) goto L_0x0010
            if (r6 == r1) goto L_0x0010
            goto L_0x001a
        L_0x0010:
            if (r6 != r3) goto L_0x001a
        L_0x0012:
            r4.a((int) r5)
            goto L_0x0040
        L_0x0016:
            if (r6 == 0) goto L_0x001e
            if (r6 == r2) goto L_0x001e
        L_0x001a:
            r4.a((int) r3)
            goto L_0x0040
        L_0x001e:
            if (r6 != 0) goto L_0x0012
            r5 = 1
            goto L_0x0012
        L_0x0022:
            com.applovin.impl.sdk.a.g$b r0 = com.applovin.impl.sdk.a.g.b.ACTIVITY_LANDSCAPE
            if (r5 != r0) goto L_0x0040
            r5 = 8
            r0 = 0
            if (r7 == 0) goto L_0x0035
            if (r6 == 0) goto L_0x0030
            if (r6 == r2) goto L_0x0030
            goto L_0x0039
        L_0x0030:
            if (r6 != r2) goto L_0x0033
            goto L_0x0012
        L_0x0033:
            r5 = 0
            goto L_0x0012
        L_0x0035:
            if (r6 == r3) goto L_0x003d
            if (r6 == r1) goto L_0x003d
        L_0x0039:
            r4.a((int) r0)
            goto L_0x0040
        L_0x003d:
            if (r6 != r3) goto L_0x0012
            goto L_0x0033
        L_0x0040:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.activity.b.a(com.applovin.impl.sdk.a.g$b, int, boolean):void");
    }

    public void a(g gVar) {
        int i;
        if (!gVar.c() || (i = this.b) == -1) {
            a(gVar.p0(), this.c, this.d);
        } else {
            a(i);
        }
    }
}
