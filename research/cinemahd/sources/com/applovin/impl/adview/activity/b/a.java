package com.applovin.impl.adview.activity.b;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.adview.AdViewControllerImpl;
import com.applovin.impl.adview.c;
import com.applovin.impl.adview.m;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.b.b;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.u;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.g;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.AppKilledService;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.s;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class a implements b.a {

    /* renamed from: a  reason: collision with root package name */
    protected final g f1487a;
    protected final l b;
    protected final r c;
    protected final AppLovinFullscreenActivity d;
    protected final d e;
    private final Handler f = new Handler(Looper.getMainLooper());
    private final com.applovin.impl.sdk.utils.a g;
    /* access modifiers changed from: private */
    public final AppLovinBroadcastManager.Receiver h;
    private final g.a i;
    protected final AppLovinAdView j;
    protected final com.applovin.impl.adview.l k;
    protected final long l = SystemClock.elapsedRealtime();
    private final AtomicBoolean m = new AtomicBoolean();
    /* access modifiers changed from: private */
    public final AtomicBoolean n = new AtomicBoolean();
    private long o;
    protected long p = -1;
    protected int q = com.applovin.impl.sdk.g.h;
    protected boolean r;
    protected final AppLovinAdClickListener s;
    protected final AppLovinAdDisplayListener t;
    protected final AppLovinAdVideoPlaybackListener u;
    protected final b v;
    protected p w;

    /* renamed from: com.applovin.impl.adview.activity.b.a$a  reason: collision with other inner class name */
    private class C0009a implements View.OnClickListener, AppLovinAdClickListener {
        private C0009a() {
        }

        public void adClicked(AppLovinAd appLovinAd) {
            a.this.c.b("InterActivityV2", "Clicking through graphic");
            k.a(a.this.s, appLovinAd);
            a.this.e.b();
        }

        public void onClick(View view) {
            a aVar = a.this;
            if (view == aVar.k) {
                if (aVar.f1487a.s()) {
                    a.this.b("javascript:al_onCloseButtonTapped();");
                }
                a.this.f();
                return;
            }
            r rVar = aVar.c;
            rVar.e("InterActivityV2", "Unhandled click on widget: " + view);
        }
    }

    a(final com.applovin.impl.sdk.a.g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, final l lVar, AppLovinAdClickListener appLovinAdClickListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        this.f1487a = gVar;
        this.b = lVar;
        this.c = lVar.j0();
        this.d = appLovinFullscreenActivity;
        this.s = appLovinAdClickListener;
        this.t = appLovinAdDisplayListener;
        this.u = appLovinAdVideoPlaybackListener;
        this.v = new b(appLovinFullscreenActivity, lVar);
        this.v.a((b.a) this);
        this.e = new d(gVar, lVar);
        C0009a aVar = new C0009a();
        this.j = new m(lVar.v(), AppLovinAdSize.INTERSTITIAL, appLovinFullscreenActivity);
        this.j.setAdClickListener(aVar);
        this.j.setAdDisplayListener(new AppLovinAdDisplayListener() {
            public void adDisplayed(AppLovinAd appLovinAd) {
                a.this.c.b("InterActivityV2", "Web content rendered");
            }

            public void adHidden(AppLovinAd appLovinAd) {
                a.this.c.b("InterActivityV2", "Closing from WebView");
                a.this.f();
            }
        });
        AdViewControllerImpl adViewControllerImpl = (AdViewControllerImpl) this.j.getAdViewController();
        adViewControllerImpl.setStatsManagerHelper(this.e);
        adViewControllerImpl.getAdWebView().setIsShownOutOfContext(gVar.T());
        lVar.c0().trackImpression(gVar);
        if (gVar.t0() >= 0) {
            this.k = new com.applovin.impl.adview.l(gVar.u0(), appLovinFullscreenActivity);
            this.k.setVisibility(8);
            this.k.setOnClickListener(aVar);
        } else {
            this.k = null;
        }
        if (((Boolean) lVar.a(com.applovin.impl.sdk.c.b.z1)).booleanValue()) {
            this.h = new AppLovinBroadcastManager.Receiver(this) {
                public void onReceive(Context context, Intent intent, Map<String, Object> map) {
                    lVar.c0().trackAppKilled(gVar);
                    lVar.I().unregisterReceiver(this);
                }
            };
            lVar.I().registerReceiver(this.h, new IntentFilter(AppKilledService.ACTION_APP_KILLED));
        } else {
            this.h = null;
        }
        if (gVar.S()) {
            this.i = new g.a() {
                public void onRingerModeChanged(int i) {
                    String str;
                    a aVar = a.this;
                    if (aVar.q != com.applovin.impl.sdk.g.h) {
                        aVar.r = true;
                    }
                    c adWebView = ((AdViewControllerImpl) a.this.j.getAdViewController()).getAdWebView();
                    if (!com.applovin.impl.sdk.g.a(i) || com.applovin.impl.sdk.g.a(a.this.q)) {
                        if (i == 2) {
                            str = "javascript:al_muteSwitchOff();";
                        }
                        a.this.q = i;
                    }
                    str = "javascript:al_muteSwitchOn();";
                    adWebView.a(str);
                    a.this.q = i;
                }
            };
            lVar.H().a(this.i);
        } else {
            this.i = null;
        }
        if (((Boolean) lVar.a(com.applovin.impl.sdk.c.b.I3)).booleanValue()) {
            this.g = new com.applovin.impl.sdk.utils.a() {
                public void onActivityCreated(Activity activity, Bundle bundle) {
                    if (!a.this.n.get()) {
                        if (activity.getClass().getName().equals(com.applovin.impl.sdk.utils.r.c(activity.getApplicationContext()))) {
                            lVar.o().a((com.applovin.impl.sdk.e.a) new y(lVar, new Runnable() {
                                public void run() {
                                    r.i("InterActivityV2", "Dismissing on-screen ad due to app relaunched via launcher.");
                                    a.this.f();
                                }
                            }), o.a.MAIN);
                        }
                    }
                }
            };
            lVar.D().a(this.g);
            return;
        }
        this.g = null;
    }

    public void a(int i2, KeyEvent keyEvent) {
        r rVar = this.c;
        if (rVar != null) {
            rVar.c("InterActivityV2", "onKeyDown(int, KeyEvent) -  " + i2 + ", " + keyEvent);
        }
    }

    /* access modifiers changed from: protected */
    public void a(int i2, boolean z, boolean z2, long j2) {
        int i3 = i2;
        if (this.m.compareAndSet(false, true)) {
            if (this.f1487a.hasVideoUrl() || o()) {
                k.a(this.u, (AppLovinAd) this.f1487a, (double) i3, z2);
            }
            if (this.f1487a.hasVideoUrl()) {
                this.e.c((long) i3);
            }
            long elapsedRealtime = SystemClock.elapsedRealtime() - this.l;
            this.b.c0().trackVideoEnd(this.f1487a, TimeUnit.MILLISECONDS.toSeconds(elapsedRealtime), i2, z);
            long j3 = -1;
            if (this.p != -1) {
                j3 = SystemClock.elapsedRealtime() - this.p;
            }
            this.b.c0().trackFullScreenAdClosed(this.f1487a, j3, j2, this.r, this.q);
            r rVar = this.c;
            rVar.b("InterActivityV2", "Video ad ended at percent: " + i3 + "%, elapsedTime: " + elapsedRealtime + "ms, skipTimeMillis: " + j2 + "ms, closeTimeMillis: " + j3 + "ms");
        }
    }

    /* access modifiers changed from: protected */
    public void a(long j2) {
        r rVar = this.c;
        rVar.b("InterActivityV2", "Scheduling report reward in " + TimeUnit.MILLISECONDS.toSeconds(j2) + " seconds...");
        this.w = p.a(j2, this.b, new Runnable() {
            public void run() {
                if (!a.this.f1487a.N().getAndSet(true)) {
                    a aVar = a.this;
                    a.this.b.o().a((com.applovin.impl.sdk.e.a) new u(aVar.f1487a, aVar.b), o.a.REWARD);
                }
            }
        });
    }

    public void a(Configuration configuration) {
        r rVar = this.c;
        rVar.c("InterActivityV2", "onConfigurationChanged(Configuration) -  " + configuration);
    }

    /* access modifiers changed from: protected */
    public void a(final com.applovin.impl.adview.l lVar, long j2, final Runnable runnable) {
        this.b.o().a((com.applovin.impl.sdk.e.a) new y(this.b, new Runnable(this) {
            public void run() {
                AppLovinSdkUtils.runOnUiThread(new Runnable() {
                    public void run() {
                        s.a((View) lVar, 400, (Runnable) new Runnable() {
                            public void run() {
                                runnable.run();
                            }
                        });
                    }
                });
            }
        }), o.a.MAIN, TimeUnit.SECONDS.toMillis(j2), true);
    }

    /* access modifiers changed from: protected */
    public void a(Runnable runnable, long j2) {
        AppLovinSdkUtils.runOnUiThreadDelayed(runnable, j2, this.f);
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        if (this.f1487a.t()) {
            a(str, 0);
        }
    }

    /* access modifiers changed from: protected */
    public void a(final String str, long j2) {
        if (j2 >= 0) {
            a((Runnable) new Runnable() {
                public void run() {
                    c adWebView;
                    if (com.applovin.impl.sdk.utils.o.b(str) && (adWebView = ((AdViewControllerImpl) a.this.j.getAdViewController()).getAdWebView()) != null) {
                        adWebView.a(str);
                    }
                }
            }, j2);
        }
    }

    /* access modifiers changed from: protected */
    public void a(boolean z) {
        List<Uri> a2 = com.applovin.impl.sdk.utils.r.a(z, this.f1487a, this.b, (Context) this.d);
        if (a2.isEmpty()) {
            return;
        }
        if (!((Boolean) this.b.a(com.applovin.impl.sdk.c.b.M3)).booleanValue()) {
            this.f1487a.A();
            return;
        }
        throw new IllegalStateException("Missing cached resource(s): " + a2);
    }

    /* access modifiers changed from: protected */
    public void a(boolean z, long j2) {
        if (this.f1487a.r()) {
            a(z ? "javascript:al_mute();" : "javascript:al_unmute();", j2);
        }
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        a(str, 0);
    }

    /* access modifiers changed from: protected */
    public void b(boolean z) {
        a(z, ((Long) this.b.a(com.applovin.impl.sdk.c.b.Q1)).longValue());
        k.a(this.t, (AppLovinAd) this.f1487a);
        this.b.C().a((Object) this.f1487a);
        this.b.K().a((Object) this.f1487a);
        if (this.f1487a.hasVideoUrl() || o()) {
            k.a(this.u, (AppLovinAd) this.f1487a);
        }
        new com.applovin.impl.adview.activity.b(this.d).a(this.f1487a);
        this.e.a();
        this.f1487a.setHasShown(true);
    }

    public abstract void c();

    public void c(boolean z) {
        r rVar = this.c;
        rVar.c("InterActivityV2", "onWindowFocusChanged(boolean) - " + z);
        a("javascript:al_onWindowFocusChanged( " + z + " );");
    }

    public void d() {
        this.c.c("InterActivityV2", "onResume()");
        this.e.d(SystemClock.elapsedRealtime() - this.o);
        a("javascript:al_onAppResumed();");
        n();
        if (this.v.d()) {
            this.v.a();
        }
    }

    public void e() {
        this.c.c("InterActivityV2", "onPause()");
        this.o = SystemClock.elapsedRealtime();
        a("javascript:al_onAppPaused();");
        this.v.a();
        m();
    }

    public void f() {
        this.c.c("InterActivityV2", "dismiss()");
        this.f.removeCallbacksAndMessages((Object) null);
        a("javascript:al_onPoststitialDismiss();", (long) this.f1487a.q());
        l();
        this.e.c();
        if (this.h != null) {
            p.a(TimeUnit.SECONDS.toMillis(2), this.b, new Runnable() {
                public void run() {
                    a.this.d.stopService(new Intent(a.this.d.getApplicationContext(), AppKilledService.class));
                    a.this.b.I().unregisterReceiver(a.this.h);
                }
            });
        }
        if (this.i != null) {
            this.b.H().b(this.i);
        }
        if (this.g != null) {
            this.b.D().b(this.g);
        }
        this.d.finish();
    }

    public void g() {
        this.c.c("InterActivityV2", "onStop()");
    }

    public void h() {
        AppLovinAdView appLovinAdView = this.j;
        if (appLovinAdView != null) {
            ViewParent parent = appLovinAdView.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeAllViews();
            }
            this.j.destroy();
        }
        k();
        l();
    }

    public void i() {
        r.i("InterActivityV2", "---low memory detected - running garbage collection---");
        System.gc();
    }

    public void j() {
        this.c.c("InterActivityV2", "onBackPressed()");
        if (this.f1487a.s()) {
            b("javascript:onBackPressed();");
        }
    }

    /* access modifiers changed from: protected */
    public abstract void k();

    /* access modifiers changed from: protected */
    public void l() {
        if (this.n.compareAndSet(false, true)) {
            k.b(this.t, (AppLovinAd) this.f1487a);
            this.b.C().b((Object) this.f1487a);
            this.b.K().a();
        }
    }

    /* access modifiers changed from: protected */
    public void m() {
        p pVar = this.w;
        if (pVar != null) {
            pVar.b();
        }
    }

    /* access modifiers changed from: protected */
    public void n() {
        p pVar = this.w;
        if (pVar != null) {
            pVar.c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean o() {
        return AppLovinAdType.INCENTIVIZED == this.f1487a.getType() || AppLovinAdType.AUTO_INCENTIVIZED == this.f1487a.getType();
    }

    /* access modifiers changed from: protected */
    public boolean p() {
        return ((Boolean) this.b.a(com.applovin.impl.sdk.c.b.E1)).booleanValue() ? this.b.Y().isMuted() : ((Boolean) this.b.a(com.applovin.impl.sdk.c.b.C1)).booleanValue();
    }
}
