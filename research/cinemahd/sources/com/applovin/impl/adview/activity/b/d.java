package com.applovin.impl.adview.activity.b;

import android.graphics.PointF;
import android.net.Uri;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.a.a;
import com.applovin.impl.a.g;
import com.applovin.impl.a.h;
import com.applovin.impl.a.i;
import com.applovin.impl.a.j;
import com.applovin.impl.a.k;
import com.applovin.impl.adview.i;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.ui.JavascriptBridge;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class d extends f {
    private final a S;
    /* access modifiers changed from: private */
    public final Set<g> T = new HashSet();

    public d(com.applovin.impl.sdk.a.g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, l lVar, AppLovinAdClickListener appLovinAdClickListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        super(gVar, appLovinFullscreenActivity, lVar, appLovinAdClickListener, appLovinAdDisplayListener, appLovinAdVideoPlaybackListener);
        this.S = (a) gVar;
        this.T.addAll(this.S.a(a.c.VIDEO, h.f1445a));
        a(a.c.IMPRESSION);
        a(a.c.VIDEO, "creativeView");
    }

    private void a(a.c cVar) {
        a(cVar, com.applovin.impl.a.d.UNSPECIFIED);
    }

    private void a(a.c cVar, com.applovin.impl.a.d dVar) {
        a(cVar, "", dVar);
    }

    private void a(a.c cVar, String str) {
        a(cVar, str, com.applovin.impl.a.d.UNSPECIFIED);
    }

    private void a(a.c cVar, String str, com.applovin.impl.a.d dVar) {
        a(this.S.a(cVar, str), dVar);
    }

    /* access modifiers changed from: private */
    public void a(Set<g> set) {
        a(set, com.applovin.impl.a.d.UNSPECIFIED);
    }

    private void a(Set<g> set, com.applovin.impl.a.d dVar) {
        if (set != null && !set.isEmpty()) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds((long) this.z.getCurrentPosition());
            k M0 = this.S.M0();
            Uri a2 = M0 != null ? M0.a() : null;
            r rVar = this.c;
            rVar.b("InterActivityV2", "Firing " + set.size() + " tracker(s): " + set);
            i.a(set, seconds, a2, dVar, this.b);
        }
    }

    private void z() {
        if (q() && !this.T.isEmpty()) {
            r rVar = this.c;
            rVar.d("InterActivityV2", "Firing " + this.T.size() + " un-fired video progress trackers when video was completed.");
            a(this.T);
        }
    }

    public void a(PointF pointF) {
        a(a.c.VIDEO_CLICK);
        super.a(pointF);
    }

    public void c() {
        super.c();
        this.G.a("PROGRESS_TRACKING", ((Long) this.b.a(b.n3)).longValue(), (i.a) new i.a() {
            public void a() {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(d.this.J - ((long) (d.this.z.getDuration() - d.this.z.getCurrentPosition())));
                HashSet hashSet = new HashSet();
                for (g gVar : new HashSet(d.this.T)) {
                    if (gVar.a(seconds, d.this.y())) {
                        hashSet.add(gVar);
                        d.this.T.remove(gVar);
                    }
                }
                d.this.a((Set<g>) hashSet);
            }

            public boolean b() {
                return !d.this.M;
            }
        });
    }

    public void c(String str) {
        a(a.c.ERROR, com.applovin.impl.a.d.MEDIA_FILE_ERROR);
        super.c(str);
    }

    public void d() {
        super.d();
        a(this.M ? a.c.COMPANION : a.c.VIDEO, "resume");
    }

    public void e() {
        super.e();
        a(this.M ? a.c.COMPANION : a.c.VIDEO, "pause");
    }

    public void f() {
        a(a.c.VIDEO, JavascriptBridge.MraidHandler.CLOSE_ACTION);
        a(a.c.COMPANION, JavascriptBridge.MraidHandler.CLOSE_ACTION);
        super.f();
    }

    /* access modifiers changed from: protected */
    public void s() {
        long j;
        int t0;
        long j2 = 0;
        if (this.S.K() >= 0 || this.S.L() >= 0) {
            int i = (this.S.K() > 0 ? 1 : (this.S.K() == 0 ? 0 : -1));
            a aVar = this.S;
            if (i >= 0) {
                j = aVar.K();
            } else {
                j L0 = aVar.L0();
                if (L0 == null || L0.b() <= 0) {
                    long j3 = this.J;
                    if (j3 > 0) {
                        j2 = 0 + j3;
                    }
                } else {
                    j2 = 0 + TimeUnit.SECONDS.toMillis((long) L0.b());
                }
                if (aVar.M() && (t0 = (int) aVar.t0()) > 0) {
                    j2 += TimeUnit.SECONDS.toMillis((long) t0);
                }
                j = (long) (((double) j2) * (((double) this.S.L()) / 100.0d));
            }
            a(j);
        }
    }

    /* access modifiers changed from: protected */
    public void t() {
        this.G.c();
        super.t();
    }

    public void u() {
        a(a.c.VIDEO, "skip");
        super.u();
    }

    public void v() {
        super.v();
        a(a.c.VIDEO, this.I ? AnalyticsEvent.Ad.mute : AnalyticsEvent.Ad.unmute);
    }

    public void w() {
        z();
        if (!com.applovin.impl.a.i.c(this.S)) {
            this.c.b("InterActivityV2", "VAST ad does not have valid companion ad - dismissing...");
            f();
        } else if (!this.M) {
            a(a.c.COMPANION, "creativeView");
            super.w();
        }
    }
}
