package com.applovin.impl.adview.activity.b;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.impl.adview.AppLovinVideoView;
import com.applovin.impl.adview.activity.a.c;
import com.applovin.impl.adview.i;
import com.applovin.impl.adview.l;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.k;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.R$drawable;
import com.vungle.warren.AdLoader;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class f extends a {
    /* access modifiers changed from: private */
    public final com.applovin.impl.adview.a A;
    /* access modifiers changed from: private */
    public final l B;
    /* access modifiers changed from: private */
    public final ImageView C;
    /* access modifiers changed from: private */
    public final ProgressBar D;
    /* access modifiers changed from: private */
    public final a E = new a();
    private final Handler F = new Handler(Looper.getMainLooper());
    protected final i G = new i(this.F, this.b);
    private final boolean H = this.f1487a.l0();
    protected boolean I = p();
    protected long J;
    private int K;
    private int L = -1;
    protected boolean M;
    /* access modifiers changed from: private */
    public boolean N;
    private AtomicBoolean O = new AtomicBoolean();
    private AtomicBoolean P = new AtomicBoolean();
    /* access modifiers changed from: private */
    public long Q = -2;
    /* access modifiers changed from: private */
    public long R = 0;
    private final c x = new c(this.f1487a, this.d, this.b);
    /* access modifiers changed from: private */
    public MediaPlayer y;
    protected final AppLovinVideoView z;

    private class a implements MediaPlayer.OnCompletionListener, MediaPlayer.OnErrorListener, MediaPlayer.OnInfoListener, MediaPlayer.OnPreparedListener, AppLovinTouchToClickListener.OnClickListener {
        private a() {
        }

        public void onClick(View view, PointF pointF) {
            f.this.a(pointF);
        }

        public void onCompletion(MediaPlayer mediaPlayer) {
            f.this.c.b("InterActivityV2", "Video completed");
            boolean unused = f.this.N = true;
            f.this.w();
        }

        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            f fVar = f.this;
            fVar.c("Video view error (" + i + "," + i2 + ")");
            f.this.z.start();
            return true;
        }

        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
            r rVar = f.this.c;
            rVar.b("InterActivityV2", "MediaPlayer Info: (" + i + ", " + i2 + ")");
            if (i == 701) {
                if (f.this.A != null) {
                    f.this.A.a();
                }
                f.this.e.g();
                return false;
            } else if (i == 3) {
                f.this.G.a();
                if (f.this.B != null) {
                    f.this.A();
                }
                if (f.this.A != null) {
                    f.this.A.b();
                }
                if (!f.this.v.d()) {
                    return false;
                }
                f.this.t();
                return false;
            } else if (i != 702 || f.this.A == null) {
                return false;
            } else {
                f.this.A.b();
                return false;
            }
        }

        public void onPrepared(MediaPlayer mediaPlayer) {
            MediaPlayer unused = f.this.y = mediaPlayer;
            mediaPlayer.setOnInfoListener(f.this.E);
            mediaPlayer.setOnErrorListener(f.this.E);
            float f = f.this.I ^ true ? 1.0f : 0.0f;
            mediaPlayer.setVolume(f, f);
            f.this.J = (long) mediaPlayer.getDuration();
            f.this.s();
            r rVar = f.this.c;
            rVar.b("InterActivityV2", "MediaPlayer prepared: " + f.this.y);
        }
    }

    private class b implements View.OnClickListener {
        private b() {
        }

        public void onClick(View view) {
            if (view == f.this.B) {
                if (f.this.r()) {
                    f.this.t();
                    f.this.m();
                    f.this.v.b();
                    return;
                }
                f.this.u();
            } else if (view == f.this.C) {
                f.this.v();
            } else {
                r rVar = f.this.c;
                rVar.e("InterActivityV2", "Unhandled click on widget: " + view);
            }
        }
    }

    public f(g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, com.applovin.impl.sdk.l lVar, AppLovinAdClickListener appLovinAdClickListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        super(gVar, appLovinFullscreenActivity, lVar, appLovinAdClickListener, appLovinAdDisplayListener, appLovinAdVideoPlaybackListener);
        if (gVar.hasVideoUrl()) {
            this.z = new AppLovinVideoView(appLovinFullscreenActivity, lVar);
            this.z.setOnPreparedListener(this.E);
            this.z.setOnCompletionListener(this.E);
            this.z.setOnErrorListener(this.E);
            this.z.setOnTouchListener(new AppLovinTouchToClickListener(lVar, com.applovin.impl.sdk.c.b.S, appLovinFullscreenActivity, this.E));
            b bVar = new b();
            if (gVar.r0() >= 0) {
                this.B = new l(gVar.v0(), appLovinFullscreenActivity);
                this.B.setVisibility(8);
                this.B.setOnClickListener(bVar);
            } else {
                this.B = null;
            }
            if (a(this.I, lVar)) {
                this.C = new ImageView(appLovinFullscreenActivity);
                this.C.setScaleType(ImageView.ScaleType.FIT_CENTER);
                this.C.setClickable(true);
                this.C.setOnClickListener(bVar);
                e(this.I);
            } else {
                this.C = null;
            }
            if (this.H) {
                this.A = new com.applovin.impl.adview.a(appLovinFullscreenActivity, ((Integer) lVar.a(com.applovin.impl.sdk.c.b.O1)).intValue(), 16842874);
                this.A.setColor(Color.parseColor("#75FFFFFF"));
                this.A.setBackgroundColor(Color.parseColor("#00000000"));
                this.A.setVisibility(8);
            } else {
                this.A = null;
            }
            if (gVar.l()) {
                this.D = new ProgressBar(appLovinFullscreenActivity, (AttributeSet) null, 16842872);
                this.D.setMax(10000);
                this.D.setPadding(0, 0, 0, 0);
                if (com.applovin.impl.sdk.utils.g.d()) {
                    this.D.setProgressTintList(ColorStateList.valueOf(gVar.m()));
                }
                this.G.a("PROGRESS_BAR", ((Long) lVar.a(com.applovin.impl.sdk.c.b.J1)).longValue(), (i.a) new i.a() {
                    public void a() {
                        f fVar = f.this;
                        if (fVar.M) {
                            fVar.D.setVisibility(8);
                            return;
                        }
                        f fVar2 = f.this;
                        fVar2.D.setProgress((int) ((((float) fVar.z.getCurrentPosition()) / ((float) fVar2.J)) * 10000.0f));
                    }

                    public boolean b() {
                        return !f.this.M;
                    }
                });
                return;
            }
            this.D = null;
            return;
        }
        throw new IllegalStateException("Attempting to use fullscreen video ad presenter for non-video ad");
    }

    /* access modifiers changed from: private */
    public void A() {
        if (this.P.compareAndSet(false, true)) {
            a(this.B, this.f1487a.r0(), new Runnable() {
                public void run() {
                    long unused = f.this.Q = -1;
                    long unused2 = f.this.R = SystemClock.elapsedRealtime();
                }
            });
        }
    }

    private static boolean a(boolean z2, com.applovin.impl.sdk.l lVar) {
        if (!((Boolean) lVar.a(com.applovin.impl.sdk.c.b.A1)).booleanValue()) {
            return false;
        }
        if (!((Boolean) lVar.a(com.applovin.impl.sdk.c.b.B1)).booleanValue() || z2) {
            return true;
        }
        return ((Boolean) lVar.a(com.applovin.impl.sdk.c.b.D1)).booleanValue();
    }

    private void d(boolean z2) {
        this.K = y();
        if (z2) {
            this.z.pause();
        } else {
            this.z.stopPlayback();
        }
    }

    private void e(boolean z2) {
        if (com.applovin.impl.sdk.utils.g.d()) {
            AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) this.d.getDrawable(z2 ? R$drawable.unmute_to_mute : R$drawable.mute_to_unmute);
            if (animatedVectorDrawable != null) {
                this.C.setScaleType(ImageView.ScaleType.FIT_XY);
                this.C.setImageDrawable(animatedVectorDrawable);
                animatedVectorDrawable.start();
                return;
            }
        }
        Uri D2 = z2 ? this.f1487a.D() : this.f1487a.E();
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        this.C.setImageURI(D2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    /* access modifiers changed from: private */
    public void z() {
        r rVar;
        String str;
        if (this.M) {
            rVar = this.c;
            str = "Skip video resume - postitial shown";
        } else if (this.b.B().a()) {
            rVar = this.c;
            str = "Skip video resume - app paused";
        } else if (this.L >= 0) {
            r rVar2 = this.c;
            rVar2.b("InterActivityV2", "Resuming video at position " + this.L + "ms for MediaPlayer: " + this.y);
            this.z.seekTo(this.L);
            this.z.start();
            this.G.a();
            this.L = -1;
            a((Runnable) new Runnable() {
                public void run() {
                    if (f.this.A != null) {
                        f.this.A.a();
                        f.this.a((Runnable) new Runnable() {
                            public void run() {
                                f.this.A.b();
                            }
                        }, (long) AdLoader.RETRY_DELAY);
                    }
                }
            }, 250);
            return;
        } else {
            this.c.b("InterActivityV2", "Invalid last video position");
            return;
        }
        rVar.d("InterActivityV2", str);
    }

    public void a() {
        this.c.b("InterActivityV2", "Continue video from prompt - will resume in onWindowFocusChanged(true) when alert dismisses");
    }

    /* access modifiers changed from: protected */
    public void a(PointF pointF) {
        if (this.f1487a.b()) {
            this.c.b("InterActivityV2", "Clicking through video");
            Uri n0 = this.f1487a.n0();
            if (n0 != null) {
                k.a(this.s, (AppLovinAd) this.f1487a);
                this.b.c0().trackAndLaunchVideoClick(this.f1487a, this.j, n0, pointF);
                this.e.b();
            }
        }
    }

    public void b() {
        this.c.b("InterActivityV2", "Skipping video from prompt");
        u();
    }

    public void c() {
        this.x.a(this.C, this.B, this.A, this.D, this.z, this.j);
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        a(!this.H);
        this.z.setVideoURI(this.f1487a.m0());
        StrictMode.setThreadPolicy(allowThreadDiskReads);
        if (this.f1487a.U()) {
            this.v.a(this.f1487a, (Runnable) new Runnable() {
                public void run() {
                    f.this.x();
                }
            });
        }
        this.z.start();
        if (this.H) {
            this.A.a();
        }
        this.j.renderAd(this.f1487a);
        this.e.b(this.H ? 1 : 0);
        if (this.B != null) {
            this.b.o().a((com.applovin.impl.sdk.e.a) new y(this.b, new Runnable() {
                public void run() {
                    f.this.A();
                }
            }), o.a.MAIN, this.f1487a.s0(), true);
        }
        super.b(this.I);
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        r rVar = this.c;
        rVar.e("InterActivityV2", "Encountered media error: " + str + " for ad: " + this.f1487a);
        if (this.O.compareAndSet(false, true)) {
            AppLovinAdDisplayListener appLovinAdDisplayListener = this.t;
            if (appLovinAdDisplayListener instanceof com.applovin.impl.sdk.a.i) {
                ((com.applovin.impl.sdk.a.i) appLovinAdDisplayListener).onAdDisplayFailed(str);
            }
            f();
        }
    }

    public void c(boolean z2) {
        super.c(z2);
        if (z2) {
            x();
        } else if (!this.M) {
            t();
        }
    }

    public void f() {
        this.G.b();
        this.F.removeCallbacksAndMessages((Object) null);
        k();
        super.f();
    }

    public void h() {
        this.c.c("InterActivityV2", "Destroying video components");
        try {
            if (this.z != null) {
                this.z.pause();
                this.z.stopPlayback();
            }
            if (this.y != null) {
                this.y.release();
            }
        } catch (Throwable th) {
            Log.e("InterActivityV2", "Unable to destroy presenter", th);
        }
        super.h();
    }

    /* access modifiers changed from: protected */
    public void k() {
        super.a(y(), this.H, q(), this.Q);
    }

    /* access modifiers changed from: protected */
    public boolean q() {
        return y() >= this.f1487a.n();
    }

    /* access modifiers changed from: protected */
    public boolean r() {
        return o() && !q();
    }

    /* access modifiers changed from: protected */
    public void s() {
        long j;
        int F0;
        long j2 = 0;
        if (this.f1487a.K() >= 0 || this.f1487a.L() >= 0) {
            int i = (this.f1487a.K() > 0 ? 1 : (this.f1487a.K() == 0 ? 0 : -1));
            g gVar = this.f1487a;
            if (i >= 0) {
                j = gVar.K();
            } else {
                com.applovin.impl.sdk.a.a aVar = (com.applovin.impl.sdk.a.a) gVar;
                long j3 = this.J;
                if (j3 > 0) {
                    j2 = 0 + j3;
                }
                if (aVar.M() && ((F0 = (int) ((com.applovin.impl.sdk.a.a) this.f1487a).F0()) > 0 || (F0 = (int) aVar.t0()) > 0)) {
                    j2 += TimeUnit.SECONDS.toMillis((long) F0);
                }
                j = (long) (((double) j2) * (((double) this.f1487a.L()) / 100.0d));
            }
            a(j);
        }
    }

    /* access modifiers changed from: protected */
    public void t() {
        String str;
        r rVar;
        this.c.b("InterActivityV2", "Pausing video");
        if (this.z.isPlaying()) {
            this.L = this.z.getCurrentPosition();
            this.z.pause();
            this.G.c();
            rVar = this.c;
            str = "Paused video at position " + this.L + "ms";
        } else {
            rVar = this.c;
            str = "Nothing to pause";
        }
        rVar.b("InterActivityV2", str);
    }

    public void u() {
        this.Q = SystemClock.elapsedRealtime() - this.R;
        r rVar = this.c;
        rVar.b("InterActivityV2", "Skipping video with skip time: " + this.Q + "ms");
        this.e.f();
        if (this.f1487a.w0()) {
            f();
        } else {
            w();
        }
    }

    /* access modifiers changed from: protected */
    public void v() {
        MediaPlayer mediaPlayer = this.y;
        if (mediaPlayer != null) {
            this.I = !this.I;
            float f = this.I ^ true ? 1.0f : 0.0f;
            mediaPlayer.setVolume(f, f);
            e(this.I);
            a(this.I, 0);
        }
    }

    public void w() {
        this.c.b("InterActivityV2", "Showing postitial...");
        d(this.f1487a.J());
        this.x.a(this.k, this.j);
        a("javascript:al_onPoststitialShow();", (long) this.f1487a.p());
        if (this.k != null) {
            int i = (this.f1487a.t0() > 0 ? 1 : (this.f1487a.t0() == 0 ? 0 : -1));
            l lVar = this.k;
            if (i >= 0) {
                a(lVar, this.f1487a.t0(), new Runnable() {
                    public void run() {
                        f.this.p = SystemClock.elapsedRealtime();
                    }
                });
            } else {
                lVar.setVisibility(0);
            }
        }
        this.M = true;
    }

    public void x() {
        a((Runnable) new Runnable() {
            public void run() {
                f.this.z();
            }
        }, 250);
    }

    /* access modifiers changed from: protected */
    public int y() {
        long currentPosition = (long) this.z.getCurrentPosition();
        if (this.N) {
            return 100;
        }
        return currentPosition > 0 ? (int) ((((float) currentPosition) / ((float) this.J)) * 100.0f) : this.K;
    }
}
