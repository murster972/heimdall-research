package com.applovin.impl.adview.activity.b;

import android.os.SystemClock;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.sdk.a.a;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public class b extends a {
    /* access modifiers changed from: private */
    public AtomicBoolean A = new AtomicBoolean();
    private final com.applovin.impl.adview.activity.a.b x = new com.applovin.impl.adview.activity.a.b(this.f1487a, this.d, this.b);
    private d y;
    private long z;

    public b(g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, l lVar, AppLovinAdClickListener appLovinAdClickListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        super(gVar, appLovinFullscreenActivity, lVar, appLovinAdClickListener, appLovinAdDisplayListener, appLovinAdVideoPlaybackListener);
    }

    private long s() {
        g gVar = this.f1487a;
        if (!(gVar instanceof a)) {
            return 0;
        }
        float F0 = ((a) gVar).F0();
        if (F0 <= 0.0f) {
            F0 = (float) this.f1487a.t0();
        }
        return (long) (((double) r.b(F0)) * (((double) this.f1487a.o()) / 100.0d));
    }

    public void a() {
    }

    public void b() {
    }

    public void c() {
        this.x.a(this.k, this.j);
        a(false);
        this.j.renderAd(this.f1487a);
        a("javascript:al_onPoststitialShow();", (long) this.f1487a.p());
        if (o()) {
            this.z = s();
            if (this.z > 0) {
                com.applovin.impl.sdk.r rVar = this.c;
                rVar.b("InterActivityV2", "Scheduling timer for ad fully watched in " + this.z + "ms...");
                this.y = d.a(this.z, this.b, new Runnable() {
                    public void run() {
                        b.this.c.b("InterActivityV2", "Marking ad as fully watched");
                        b.this.A.set(true);
                    }
                });
            }
        }
        if (this.k != null) {
            if (this.f1487a.t0() >= 0) {
                a(this.k, this.f1487a.t0(), new Runnable() {
                    public void run() {
                        b.this.p = SystemClock.elapsedRealtime();
                    }
                });
            } else {
                this.k.setVisibility(0);
            }
        }
        r();
        super.b(p());
    }

    public void f() {
        k();
        d dVar = this.y;
        if (dVar != null) {
            dVar.a();
            this.y = null;
        }
        super.f();
    }

    /* access modifiers changed from: protected */
    public void k() {
        d dVar;
        boolean q = q();
        int i = 100;
        if (o()) {
            if (!q && (dVar = this.y) != null) {
                i = (int) Math.min(100.0d, (((double) (this.z - dVar.b())) / ((double) this.z)) * 100.0d);
            }
            com.applovin.impl.sdk.r rVar = this.c;
            rVar.b("InterActivityV2", "Ad engaged at " + i + "%");
        }
        super.a(i, false, q, -2);
    }

    /* access modifiers changed from: protected */
    public boolean q() {
        if (o()) {
            return this.A.get();
        }
        return true;
    }

    /* access modifiers changed from: protected */
    public void r() {
        long j;
        long millis;
        long j2 = 0;
        if (this.f1487a.K() >= 0 || this.f1487a.L() >= 0) {
            int i = (this.f1487a.K() > 0 ? 1 : (this.f1487a.K() == 0 ? 0 : -1));
            g gVar = this.f1487a;
            if (i >= 0) {
                j = gVar.K();
            } else {
                if (gVar.M()) {
                    int F0 = (int) ((a) this.f1487a).F0();
                    if (F0 > 0) {
                        millis = TimeUnit.SECONDS.toMillis((long) F0);
                    } else {
                        int t0 = (int) this.f1487a.t0();
                        if (t0 > 0) {
                            millis = TimeUnit.SECONDS.toMillis((long) t0);
                        }
                    }
                    j2 = 0 + millis;
                }
                j = (long) (((double) j2) * (((double) this.f1487a.L()) / 100.0d));
            }
            a(j);
        }
    }
}
