package com.applovin.impl.adview.activity.b;

import android.graphics.PointF;
import android.net.Uri;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.a.a;
import com.applovin.impl.a.d;
import com.applovin.impl.a.g;
import com.applovin.impl.a.h;
import com.applovin.impl.a.i;
import com.applovin.impl.a.k;
import com.applovin.impl.adview.i;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.ui.JavascriptBridge;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class c extends e {
    private final a R;
    /* access modifiers changed from: private */
    public final Set<g> S = new HashSet();

    public c(com.applovin.impl.sdk.a.g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, l lVar, AppLovinAdClickListener appLovinAdClickListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        super(gVar, appLovinFullscreenActivity, lVar, appLovinAdClickListener, appLovinAdDisplayListener, appLovinAdVideoPlaybackListener);
        this.R = (a) gVar;
        this.S.addAll(this.R.a(a.c.VIDEO, h.f1445a));
        a(a.c.IMPRESSION);
        a(a.c.VIDEO, "creativeView");
    }

    private void C() {
        if (t() && !this.S.isEmpty()) {
            r rVar = this.c;
            rVar.d("InterActivityV2", "Firing " + this.S.size() + " un-fired video progress trackers when video was completed.");
            a(this.S);
        }
    }

    private void a(a.c cVar) {
        a(cVar, d.UNSPECIFIED);
    }

    private void a(a.c cVar, d dVar) {
        a(cVar, "", dVar);
    }

    private void a(a.c cVar, String str) {
        a(cVar, str, d.UNSPECIFIED);
    }

    private void a(a.c cVar, String str, d dVar) {
        a(this.R.a(cVar, str), dVar);
    }

    /* access modifiers changed from: private */
    public void a(Set<g> set) {
        a(set, d.UNSPECIFIED);
    }

    private void a(Set<g> set, d dVar) {
        if (set != null && !set.isEmpty()) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds(this.z.getCurrentPosition());
            k M0 = this.R.M0();
            Uri a2 = M0 != null ? M0.a() : null;
            r rVar = this.c;
            rVar.b("InterActivityV2", "Firing " + set.size() + " tracker(s): " + set);
            i.a(set, seconds, a2, dVar, this.b);
        }
    }

    public void a(PointF pointF) {
        a(a.c.VIDEO_CLICK);
        super.a(pointF);
    }

    public void c() {
        super.c();
        this.F.a("PROGRESS_TRACKING", ((Long) this.b.a(b.n3)).longValue(), (i.a) new i.a() {
            public void a() {
                long seconds = TimeUnit.MILLISECONDS.toSeconds(c.this.I - (c.this.z.getDuration() - c.this.z.getCurrentPosition()));
                HashSet hashSet = new HashSet();
                for (g gVar : new HashSet(c.this.S)) {
                    if (gVar.a(seconds, c.this.s())) {
                        hashSet.add(gVar);
                        c.this.S.remove(gVar);
                    }
                }
                c.this.a((Set<g>) hashSet);
            }

            public boolean b() {
                return !c.this.K;
            }
        });
    }

    public void c(String str) {
        a(a.c.ERROR, d.MEDIA_FILE_ERROR);
        super.c(str);
    }

    public void d() {
        super.d();
        a(this.K ? a.c.COMPANION : a.c.VIDEO, "resume");
    }

    public void e() {
        super.e();
        a(this.K ? a.c.COMPANION : a.c.VIDEO, "pause");
    }

    public void f() {
        a(a.c.VIDEO, JavascriptBridge.MraidHandler.CLOSE_ACTION);
        a(a.c.COMPANION, JavascriptBridge.MraidHandler.CLOSE_ACTION);
        super.f();
    }

    /* access modifiers changed from: protected */
    public void v() {
        this.F.c();
        super.v();
    }

    public void w() {
        a(a.c.VIDEO, "skip");
        super.w();
    }

    public void x() {
        super.x();
        a(a.c.VIDEO, this.H ? AnalyticsEvent.Ad.mute : AnalyticsEvent.Ad.unmute);
    }

    public void y() {
        C();
        if (!com.applovin.impl.a.i.c(this.R)) {
            this.c.b("InterActivityV2", "VAST ad does not have valid companion ad - dismissing...");
            f();
        } else if (!this.K) {
            a(a.c.COMPANION, "creativeView");
            super.y();
        }
    }
}
