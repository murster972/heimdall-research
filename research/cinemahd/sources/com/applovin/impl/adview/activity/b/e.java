package com.applovin.impl.adview.activity.b;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import com.applovin.adview.AppLovinFullscreenActivity;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.impl.adview.activity.a.c;
import com.applovin.impl.adview.i;
import com.applovin.impl.adview.l;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.k;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.R$drawable;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.MediaItem;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.ProgressiveMediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.PlayerControlView;
import com.google.android.exoplayer2.ui.PlayerView;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import java.util.concurrent.atomic.AtomicBoolean;

public class e extends a {
    /* access modifiers changed from: private */
    public final com.applovin.impl.adview.a A;
    /* access modifiers changed from: private */
    public final l B;
    /* access modifiers changed from: private */
    public final ImageView C;
    /* access modifiers changed from: private */
    public final ProgressBar D;
    private final Handler E = new Handler(Looper.getMainLooper());
    protected final i F = new i(this.E, this.b);
    private final boolean G = this.f1487a.l0();
    protected boolean H = p();
    protected long I;
    protected int J;
    protected boolean K;
    protected boolean L;
    private long M = -1;
    private AtomicBoolean N = new AtomicBoolean();
    private AtomicBoolean O = new AtomicBoolean();
    /* access modifiers changed from: private */
    public long P = -2;
    /* access modifiers changed from: private */
    public long Q = 0;
    private final c x = new c(this.f1487a, this.d, this.b);
    protected final PlayerView y;
    protected final SimpleExoPlayer z;

    private class a implements AppLovinTouchToClickListener.OnClickListener, Player.EventListener, PlayerControlView.VisibilityListener {
        private a() {
        }

        public void a(Timeline timeline, Object obj, int i) {
        }

        public void b(int i) {
        }

        public void b(boolean z) {
        }

        public void c(int i) {
            if (i == 0) {
                e.this.y.c();
            }
        }

        public void h() {
        }

        public void onClick(View view, PointF pointF) {
            e.this.a(pointF);
        }

        public void onLoadingChanged(boolean z) {
        }

        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }

        public void onPlayerError(ExoPlaybackException exoPlaybackException) {
            e eVar = e.this;
            eVar.c("Video view error (" + exoPlaybackException + ")");
            e.this.f();
        }

        public void onPlayerStateChanged(boolean z, int i) {
        }

        public void onRepeatModeChanged(int i) {
        }

        public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {
        }
    }

    private class b implements View.OnClickListener {
        private b() {
        }

        public void onClick(View view) {
            if (view == e.this.B) {
                if (e.this.u()) {
                    e.this.v();
                    e.this.m();
                    e.this.v.b();
                    return;
                }
                e.this.w();
            } else if (view == e.this.C) {
                e.this.x();
            } else {
                r rVar = e.this.c;
                rVar.e("InterActivityV2", "Unhandled click on widget: " + view);
            }
        }
    }

    public e(g gVar, AppLovinFullscreenActivity appLovinFullscreenActivity, com.applovin.impl.sdk.l lVar, AppLovinAdClickListener appLovinAdClickListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener) {
        super(gVar, appLovinFullscreenActivity, lVar, appLovinAdClickListener, appLovinAdDisplayListener, appLovinAdVideoPlaybackListener);
        if (gVar.hasVideoUrl()) {
            b bVar = new b();
            if (gVar.r0() >= 0) {
                this.B = new l(gVar.v0(), appLovinFullscreenActivity);
                this.B.setVisibility(8);
                this.B.setOnClickListener(bVar);
            } else {
                this.B = null;
            }
            if (a(this.H, lVar)) {
                this.C = new ImageView(appLovinFullscreenActivity);
                this.C.setScaleType(ImageView.ScaleType.FIT_CENTER);
                this.C.setClickable(true);
                this.C.setOnClickListener(bVar);
                d(this.H);
            } else {
                this.C = null;
            }
            if (this.G) {
                this.A = new com.applovin.impl.adview.a(appLovinFullscreenActivity, ((Integer) lVar.a(com.applovin.impl.sdk.c.b.O1)).intValue(), 16842874);
                this.A.setColor(Color.parseColor("#75FFFFFF"));
                this.A.setBackgroundColor(Color.parseColor("#00000000"));
                this.A.setVisibility(8);
            } else {
                this.A = null;
            }
            if (gVar.l()) {
                this.D = new ProgressBar(appLovinFullscreenActivity, (AttributeSet) null, 16842872);
                this.D.setMax(10000);
                this.D.setPadding(0, 0, 0, 0);
                if (com.applovin.impl.sdk.utils.g.d()) {
                    this.D.setProgressTintList(ColorStateList.valueOf(gVar.m()));
                }
                this.F.a("PROGRESS_BAR", ((Long) lVar.a(com.applovin.impl.sdk.c.b.J1)).longValue(), (i.a) new i.a() {
                    public void a() {
                        e eVar = e.this;
                        if (eVar.K) {
                            eVar.D.setVisibility(8);
                            return;
                        }
                        e eVar2 = e.this;
                        eVar2.D.setProgress((int) ((((float) eVar.z.getCurrentPosition()) / ((float) eVar2.I)) * 10000.0f));
                    }

                    public boolean b() {
                        return !e.this.K;
                    }
                });
            } else {
                this.D = null;
            }
            this.z = new SimpleExoPlayer.Builder(appLovinFullscreenActivity).build();
            a aVar = new a();
            this.z.b((Player.EventListener) aVar);
            this.z.setRepeatMode(0);
            this.y = new PlayerView(appLovinFullscreenActivity);
            this.y.c();
            this.y.setControllerVisibilityListener(aVar);
            this.y.setPlayer(this.z);
            this.y.setOnTouchListener(new AppLovinTouchToClickListener(lVar, com.applovin.impl.sdk.c.b.S, appLovinFullscreenActivity, aVar));
            return;
        }
        throw new IllegalStateException("Attempting to use fullscreen video ad presenter for non-video ad");
    }

    private static boolean a(boolean z2, com.applovin.impl.sdk.l lVar) {
        if (!((Boolean) lVar.a(com.applovin.impl.sdk.c.b.A1)).booleanValue()) {
            return false;
        }
        if (!((Boolean) lVar.a(com.applovin.impl.sdk.c.b.B1)).booleanValue() || z2) {
            return true;
        }
        return ((Boolean) lVar.a(com.applovin.impl.sdk.c.b.D1)).booleanValue();
    }

    /* access modifiers changed from: protected */
    public void A() {
        if (this.O.compareAndSet(false, true)) {
            a(this.B, this.f1487a.r0(), new Runnable() {
                public void run() {
                    long unused = e.this.P = -1;
                    long unused2 = e.this.Q = SystemClock.elapsedRealtime();
                }
            });
        }
    }

    /* access modifiers changed from: protected */
    public void B() {
        this.J = s();
        this.z.a(false);
    }

    public void a() {
        this.c.b("InterActivityV2", "Continue video from prompt - will resume in onWindowFocusChanged(true) when alert dismisses");
    }

    /* access modifiers changed from: protected */
    public void a(PointF pointF) {
        if (this.f1487a.b()) {
            this.c.b("InterActivityV2", "Clicking through video");
            Uri n0 = this.f1487a.n0();
            if (n0 != null) {
                k.a(this.s, (AppLovinAd) this.f1487a);
                this.b.c0().trackAndLaunchVideoClick(this.f1487a, this.j, n0, pointF);
                this.e.b();
            }
        }
    }

    public void b() {
        this.c.b("InterActivityV2", "Skipping video from prompt");
        w();
    }

    public void c() {
        this.x.a(this.C, this.B, this.A, this.D, this.y, this.j);
        a(!this.G);
        z();
        if (this.G) {
            this.A.a();
        }
        this.j.renderAd(this.f1487a);
        this.e.b(this.G ? 1 : 0);
        if (this.B != null) {
            this.b.o().a((com.applovin.impl.sdk.e.a) new y(this.b, new Runnable() {
                public void run() {
                    e.this.A();
                }
            }), o.a.MAIN, this.f1487a.s0(), true);
        }
        super.b(this.H);
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        r rVar = this.c;
        rVar.e("InterActivityV2", "Encountered media error: " + str + " for ad: " + this.f1487a);
        if (this.N.compareAndSet(false, true)) {
            AppLovinAdDisplayListener appLovinAdDisplayListener = this.t;
            if (appLovinAdDisplayListener instanceof com.applovin.impl.sdk.a.i) {
                ((com.applovin.impl.sdk.a.i) appLovinAdDisplayListener).onAdDisplayFailed(str);
            }
            f();
        }
    }

    public void c(boolean z2) {
        super.c(z2);
        if (z2) {
            q();
        } else if (!this.K) {
            v();
        }
    }

    /* access modifiers changed from: protected */
    public void d(boolean z2) {
        if (com.applovin.impl.sdk.utils.g.d()) {
            AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) this.d.getDrawable(z2 ? R$drawable.unmute_to_mute : R$drawable.mute_to_unmute);
            if (animatedVectorDrawable != null) {
                this.C.setScaleType(ImageView.ScaleType.FIT_XY);
                this.C.setImageDrawable(animatedVectorDrawable);
                animatedVectorDrawable.start();
                return;
            }
        }
        Uri D2 = z2 ? this.f1487a.D() : this.f1487a.E();
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        this.C.setImageURI(D2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    public void f() {
        this.F.b();
        this.E.removeCallbacksAndMessages((Object) null);
        k();
        super.f();
    }

    /* access modifiers changed from: protected */
    public void k() {
        super.a(s(), this.G, t(), this.P);
    }

    public void q() {
        a((Runnable) new Runnable() {
            public void run() {
                e.this.r();
            }
        }, 250);
    }

    /* access modifiers changed from: protected */
    public void r() {
        r rVar;
        String str;
        if (this.K) {
            rVar = this.c;
            str = "Skip video resume - postitial shown";
        } else if (this.b.B().a()) {
            rVar = this.c;
            str = "Skip video resume - app paused";
        } else if (this.M >= 0) {
            r rVar2 = this.c;
            rVar2.b("InterActivityV2", "Resuming video at position " + this.M + "ms for MediaPlayer: " + this.z);
            AppLovinSdkUtils.runOnUiThread(new Runnable() {
                public void run() {
                    if (e.this.A != null) {
                        e.this.A.a();
                    }
                }
            });
            this.z.a(true);
            this.F.a();
            this.M = -1;
            return;
        } else {
            r rVar3 = this.c;
            rVar3.b("InterActivityV2", "Invalid last video position, isVideoPlaying=" + this.z.isPlaying());
            return;
        }
        rVar.d("InterActivityV2", str);
    }

    /* access modifiers changed from: protected */
    public int s() {
        long currentPosition = this.z.getCurrentPosition();
        if (this.L) {
            return 100;
        }
        return currentPosition > 0 ? (int) ((((float) currentPosition) / ((float) this.I)) * 100.0f) : this.J;
    }

    /* access modifiers changed from: protected */
    public boolean t() {
        return s() >= this.f1487a.n();
    }

    /* access modifiers changed from: protected */
    public boolean u() {
        return o() && !t();
    }

    /* access modifiers changed from: protected */
    public void v() {
        String str;
        r rVar;
        this.c.b("InterActivityV2", "Pausing video");
        if (this.z.isPlaying()) {
            this.M = this.z.getCurrentPosition();
            this.z.a(false);
            this.F.c();
            rVar = this.c;
            str = "Paused video at position " + this.M + "ms";
        } else {
            rVar = this.c;
            str = "Nothing to pause";
        }
        rVar.b("InterActivityV2", str);
    }

    public void w() {
        this.P = SystemClock.elapsedRealtime() - this.Q;
        r rVar = this.c;
        rVar.b("InterActivityV2", "Skipping video with skip time: " + this.P + "ms");
        this.e.f();
        if (this.f1487a.w0()) {
            f();
        } else {
            y();
        }
    }

    /* access modifiers changed from: protected */
    public void x() {
        this.H = !this.H;
        this.z.a(this.H ^ true ? 1.0f : 0.0f);
        d(this.H);
        a(this.H, 0);
    }

    public void y() {
        B();
        this.x.a(this.k, this.j);
        a("javascript:al_onPoststitialShow();", (long) this.f1487a.p());
        if (this.k != null) {
            int i = (this.f1487a.t0() > 0 ? 1 : (this.f1487a.t0() == 0 ? 0 : -1));
            l lVar = this.k;
            if (i >= 0) {
                a(lVar, this.f1487a.t0(), new Runnable() {
                    public void run() {
                        e.this.p = SystemClock.elapsedRealtime();
                    }
                });
            } else {
                lVar.setVisibility(0);
            }
        }
        this.K = true;
    }

    /* access modifiers changed from: protected */
    public void z() {
        AppLovinFullscreenActivity appLovinFullscreenActivity = this.d;
        this.z.setMediaSource(new ProgressiveMediaSource.Factory(new DefaultDataSourceFactory((Context) appLovinFullscreenActivity, Util.a((Context) appLovinFullscreenActivity, "com.applovin.sdk"))).createMediaSource(MediaItem.fromUri(this.f1487a.m0())));
        this.z.prepare();
        this.z.a(true);
        if (this.f1487a.U()) {
            this.v.a(this.f1487a, (Runnable) new Runnable() {
                public void run() {
                    e.this.q();
                }
            });
        }
    }
}
