package com.applovin.impl.adview;

import android.graphics.PointF;
import android.net.Uri;
import android.os.Bundle;
import com.applovin.impl.a.a;
import com.applovin.impl.a.d;
import com.applovin.impl.a.g;
import com.applovin.impl.a.h;
import com.applovin.impl.a.i;
import com.applovin.impl.a.k;
import com.applovin.impl.adview.i;
import com.applovin.impl.sdk.c.b;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.ui.JavascriptBridge;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class r extends n {
    private final Set<g> S = new HashSet();

    private void a() {
        if (isFullyWatched() && !this.S.isEmpty()) {
            com.applovin.impl.sdk.r rVar = this.logger;
            rVar.d("InterstitialActivity", "Firing " + this.S.size() + " un-fired video progress trackers when video was completed.");
            a(this.S);
        }
    }

    private void a(a.c cVar) {
        a(cVar, d.UNSPECIFIED);
    }

    private void a(a.c cVar, d dVar) {
        a(cVar, "", dVar);
    }

    private void a(a.c cVar, String str) {
        a(cVar, str, d.UNSPECIFIED);
    }

    private void a(a.c cVar, String str, d dVar) {
        if (isVastAd()) {
            a(((a) this.currentAd).a(cVar, str), dVar);
        }
    }

    private void a(Set<g> set) {
        a(set, d.UNSPECIFIED);
    }

    private void a(Set<g> set, d dVar) {
        if (isVastAd() && set != null && !set.isEmpty()) {
            long seconds = TimeUnit.MILLISECONDS.toSeconds((long) this.videoView.getCurrentPosition());
            k M0 = b().M0();
            Uri a2 = M0 != null ? M0.a() : null;
            com.applovin.impl.sdk.r rVar = this.logger;
            rVar.b("InterstitialActivity", "Firing " + set.size() + " tracker(s): " + set);
            i.a(set, seconds, a2, dVar, this.sdk);
        }
    }

    private a b() {
        if (this.currentAd instanceof a) {
            return (a) this.currentAd;
        }
        return null;
    }

    public void clickThroughFromVideo(PointF pointF) {
        super.clickThroughFromVideo(pointF);
        a(a.c.VIDEO_CLICK);
    }

    public void dismiss() {
        if (isVastAd()) {
            a(a.c.VIDEO, JavascriptBridge.MraidHandler.CLOSE_ACTION);
            a(a.c.COMPANION, JavascriptBridge.MraidHandler.CLOSE_ACTION);
        }
        super.dismiss();
    }

    public void handleCountdownStep() {
        if (isVastAd()) {
            long seconds = ((long) this.computedLengthSeconds) - TimeUnit.MILLISECONDS.toSeconds((long) (this.videoView.getDuration() - this.videoView.getCurrentPosition()));
            HashSet hashSet = new HashSet();
            for (g gVar : new HashSet(this.S)) {
                if (gVar.a(seconds, getVideoPercentViewed())) {
                    hashSet.add(gVar);
                    this.S.remove(gVar);
                }
            }
            a((Set<g>) hashSet);
        }
    }

    public void handleMediaError(String str) {
        a(a.c.ERROR, d.MEDIA_FILE_ERROR);
        super.handleMediaError(str);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        if (isVastAd()) {
            this.S.addAll(b().a(a.c.VIDEO, h.f1445a));
            a(a.c.IMPRESSION);
            a(a.c.VIDEO, "creativeView");
        }
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        a(this.postitialWasDisplayed ? a.c.COMPANION : a.c.VIDEO, "pause");
    }

    /* access modifiers changed from: protected */
    public void onResume() {
        super.onResume();
        a(this.postitialWasDisplayed ? a.c.COMPANION : a.c.VIDEO, "resume");
    }

    public void playVideo() {
        this.countdownManager.a("PROGRESS_TRACKING", ((Long) this.sdk.a(b.n3)).longValue(), (i.a) new i.a() {
            public void a() {
                r.this.handleCountdownStep();
            }

            public boolean b() {
                return r.this.shouldContinueFullLengthVideoCountdown();
            }
        });
        super.playVideo();
    }

    public void showPostitial() {
        if (isVastAd()) {
            a();
            if (!com.applovin.impl.a.i.c(b())) {
                dismiss();
                return;
            } else if (!this.postitialWasDisplayed) {
                a(a.c.COMPANION, "creativeView");
            } else {
                return;
            }
        }
        super.showPostitial();
    }

    public void skipVideo() {
        a(a.c.VIDEO, "skip");
        super.skipVideo();
    }

    public void toggleMute() {
        super.toggleMute();
        a(a.c.VIDEO, this.videoMuted ? AnalyticsEvent.Ad.mute : AnalyticsEvent.Ad.unmute);
    }
}
