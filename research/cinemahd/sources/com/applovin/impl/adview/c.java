package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Paint;
import android.net.Uri;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.a.e;
import com.applovin.impl.sdk.a.a;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinSdkUtils;
import com.facebook.ads.AudienceNetworkActivity;

public class c extends g {
    /* access modifiers changed from: private */
    public static WebView h;
    /* access modifiers changed from: private */
    public final r b;
    private final l c;
    private d d;
    private g e;
    private boolean f;
    private boolean g;

    c(d dVar, l lVar, Context context) {
        this(dVar, lVar, context, false);
    }

    c(d dVar, l lVar, Context context, boolean z) {
        super(context);
        if (lVar != null) {
            this.c = lVar;
            this.b = lVar.j0();
            setBackgroundColor(0);
            WebSettings settings = getSettings();
            settings.setSupportMultipleWindows(false);
            settings.setJavaScriptEnabled(true);
            setWebViewClient(dVar);
            setWebChromeClient(new b(lVar));
            setVerticalScrollBarEnabled(false);
            setHorizontalScrollBarEnabled(false);
            setScrollBarStyle(33554432);
            if (com.applovin.impl.sdk.utils.g.h() && ((Boolean) lVar.a(b.R3)).booleanValue()) {
                setWebViewRenderProcessClient(new e(lVar).a());
            }
            setOnTouchListener(new View.OnTouchListener(this) {
                @SuppressLint({"ClickableViewAccessibility"})
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (view.hasFocus()) {
                        return false;
                    }
                    view.requestFocus();
                    return false;
                }
            });
            setOnLongClickListener(new View.OnLongClickListener() {
                public boolean onLongClick(View view) {
                    c.this.b.b("AdWebView", "Received a LongClick event.");
                    return true;
                }
            });
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    private String a(String str, String str2) {
        if (o.b(str)) {
            return com.applovin.impl.sdk.utils.r.a(this.g, str).replace("{SOURCE}", str2);
        }
        return null;
    }

    public static void a(final com.applovin.impl.sdk.network.g gVar, final AppLovinPostbackListener appLovinPostbackListener) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                String a2 = gVar.a();
                c.c();
                if (c.h == null) {
                    appLovinPostbackListener.onPostbackFailure(a2, -1);
                    return;
                }
                if (gVar.c() != null) {
                    a2 = o.b(a2, gVar.c());
                }
                String str = "al_firePostback('" + a2 + "');";
                if (com.applovin.impl.sdk.utils.g.c()) {
                    c.h.evaluateJavascript(str, (ValueCallback) null);
                } else {
                    c.h.loadUrl("javascript:" + str);
                }
                appLovinPostbackListener.onPostbackSuccess(a2);
            }
        });
    }

    private void a(String str, String str2, String str3, l lVar) {
        String a2 = a(str3, str);
        if (o.b(a2)) {
            r rVar = this.b;
            rVar.b("AdWebView", "Rendering webview for VAST ad with resourceContents : " + a2);
            loadDataWithBaseURL(str2, a2, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, "");
            return;
        }
        String a3 = a((String) lVar.a(b.k3), str);
        if (o.b(a3)) {
            r rVar2 = this.b;
            rVar2.b("AdWebView", "Rendering webview for VAST ad with resourceContents : " + a3);
            loadDataWithBaseURL(str2, a3, AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, "");
            return;
        }
        r rVar3 = this.b;
        rVar3.b("AdWebView", "Rendering webview for VAST ad with resourceURL : " + str);
        loadUrl(str);
    }

    private void b(g gVar) {
        Boolean n;
        Integer a2;
        loadUrl("about:blank");
        int h0 = this.e.h0();
        if (h0 >= 0) {
            setLayerType(h0, (Paint) null);
        }
        if (com.applovin.impl.sdk.utils.g.b()) {
            getSettings().setMediaPlaybackRequiresUserGesture(gVar.d0());
        }
        if (com.applovin.impl.sdk.utils.g.c() && gVar.f0()) {
            WebView.setWebContentsDebuggingEnabled(true);
        }
        v g0 = gVar.g0();
        if (g0 != null) {
            WebSettings settings = getSettings();
            WebSettings.PluginState b2 = g0.b();
            if (b2 != null) {
                settings.setPluginState(b2);
            }
            Boolean c2 = g0.c();
            if (c2 != null) {
                settings.setAllowFileAccess(c2.booleanValue());
            }
            Boolean d2 = g0.d();
            if (d2 != null) {
                settings.setLoadWithOverviewMode(d2.booleanValue());
            }
            Boolean e2 = g0.e();
            if (e2 != null) {
                settings.setUseWideViewPort(e2.booleanValue());
            }
            Boolean f2 = g0.f();
            if (f2 != null) {
                settings.setAllowContentAccess(f2.booleanValue());
            }
            Boolean g2 = g0.g();
            if (g2 != null) {
                settings.setBuiltInZoomControls(g2.booleanValue());
            }
            Boolean h2 = g0.h();
            if (h2 != null) {
                settings.setDisplayZoomControls(h2.booleanValue());
            }
            Boolean i = g0.i();
            if (i != null) {
                settings.setSaveFormData(i.booleanValue());
            }
            Boolean j = g0.j();
            if (j != null) {
                settings.setGeolocationEnabled(j.booleanValue());
            }
            Boolean k = g0.k();
            if (k != null) {
                settings.setNeedInitialFocus(k.booleanValue());
            }
            Boolean l = g0.l();
            if (l != null) {
                settings.setAllowFileAccessFromFileURLs(l.booleanValue());
            }
            Boolean m = g0.m();
            if (m != null) {
                settings.setAllowUniversalAccessFromFileURLs(m.booleanValue());
            }
            if (com.applovin.impl.sdk.utils.g.d() && (a2 = g0.a()) != null) {
                settings.setMixedContentMode(a2.intValue());
            }
            if (com.applovin.impl.sdk.utils.g.e() && (n = g0.n()) != null) {
                settings.setOffscreenPreRaster(n.booleanValue());
            }
        }
    }

    /* access modifiers changed from: private */
    public static void c() {
        if (h == null) {
            try {
                h = new WebView(l.l0());
                h.getSettings().setJavaScriptEnabled(true);
                h.loadData("<html><head>\n<script type=\"text/javascript\">\n    window.al_firePostback = function(postback) {\n    setTimeout(function() {\n        var img = new Image();\n        img.src = postback;\n    }, 100);\n};\n</script></head>\n<body></body></html>", AudienceNetworkActivity.WEBVIEW_MIME_TYPE, "UTF-8");
                h.setWebViewClient(new WebViewClient() {
                    public boolean onRenderProcessGone(WebView webView, RenderProcessGoneDetail renderProcessGoneDetail) {
                        if (webView != c.h) {
                            return true;
                        }
                        c.h.destroy();
                        WebView unused = c.h = null;
                        AppLovinSdkUtils.runOnUiThread(new Runnable(this) {
                            public void run() {
                                c.c();
                            }
                        });
                        return true;
                    }
                });
            } catch (Throwable th) {
                r.c("AdWebView", "Failed to initialize WebView for postbacks.", th);
            }
        }
    }

    public void a(g gVar) {
        r rVar;
        String str;
        r rVar2;
        String str2;
        String str3;
        String e0;
        String str4;
        String str5;
        String str6;
        String e02;
        l lVar;
        if (!this.f) {
            this.e = gVar;
            try {
                b(gVar);
                if (com.applovin.impl.sdk.utils.r.a(gVar.getSize())) {
                    setVisibility(0);
                }
                if (gVar instanceof a) {
                    loadDataWithBaseURL(gVar.e0(), com.applovin.impl.sdk.utils.r.a(this.g, ((a) gVar).B0()), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, "");
                    rVar = this.b;
                    str = "AppLovinAd rendered";
                } else if (gVar instanceof com.applovin.impl.a.a) {
                    com.applovin.impl.a.a aVar = (com.applovin.impl.a.a) gVar;
                    com.applovin.impl.a.b N0 = aVar.N0();
                    if (N0 != null) {
                        e b2 = N0.b();
                        Uri b3 = b2.b();
                        String uri = b3 != null ? b3.toString() : "";
                        String c2 = b2.c();
                        String C0 = aVar.C0();
                        if (!o.b(uri)) {
                            if (!o.b(c2)) {
                                rVar2 = this.b;
                                str2 = "Unable to load companion ad. No resources provided.";
                                rVar2.e("AdWebView", str2);
                                return;
                            }
                        }
                        if (b2.a() == e.a.STATIC) {
                            this.b.b("AdWebView", "Rendering WebView for static VAST ad");
                            loadDataWithBaseURL(gVar.e0(), a((String) this.c.a(b.j3), uri), AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, "");
                            return;
                        }
                        if (b2.a() == e.a.HTML) {
                            if (o.b(c2)) {
                                String a2 = a(C0, c2);
                                str3 = o.b(a2) ? a2 : c2;
                                r rVar3 = this.b;
                                rVar3.b("AdWebView", "Rendering WebView for HTML VAST ad with resourceContents: " + str3);
                                e0 = gVar.e0();
                                str4 = AudienceNetworkActivity.WEBVIEW_MIME_TYPE;
                                str5 = null;
                                str6 = "";
                            } else if (o.b(uri)) {
                                this.b.b("AdWebView", "Preparing to load HTML VAST ad resourceUri");
                                e02 = gVar.e0();
                                lVar = this.c;
                                a(uri, e02, C0, lVar);
                                return;
                            } else {
                                return;
                            }
                        } else if (b2.a() != e.a.IFRAME) {
                            rVar2 = this.b;
                            str2 = "Failed to render VAST companion ad of invalid type";
                            rVar2.e("AdWebView", str2);
                            return;
                        } else if (o.b(uri)) {
                            this.b.b("AdWebView", "Preparing to load iFrame VAST ad resourceUri");
                            e02 = gVar.e0();
                            lVar = this.c;
                            a(uri, e02, C0, lVar);
                            return;
                        } else if (o.b(c2)) {
                            String a3 = a(C0, c2);
                            str3 = o.b(a3) ? a3 : c2;
                            r rVar4 = this.b;
                            rVar4.b("AdWebView", "Rendering WebView for iFrame VAST ad with resourceContents: " + str3);
                            e0 = gVar.e0();
                            str4 = AudienceNetworkActivity.WEBVIEW_MIME_TYPE;
                            str5 = null;
                            str6 = "";
                        } else {
                            return;
                        }
                        loadDataWithBaseURL(e0, str3, str4, str5, str6);
                        return;
                    }
                    rVar = this.b;
                    str = "No companion ad provided.";
                } else {
                    return;
                }
                rVar.b("AdWebView", str);
            } catch (Throwable th) {
                String valueOf = gVar != null ? String.valueOf(gVar.getAdIdNumber()) : "null";
                throw new RuntimeException("Unable to render AppLovin ad (" + valueOf + ") - " + th);
            }
        } else {
            r.i("AdWebView", "Ad can not be loaded in a destroyed webview");
        }
    }

    public void a(String str) {
        a(str, (Runnable) null);
    }

    public void a(String str, Runnable runnable) {
        try {
            r rVar = this.b;
            rVar.b("AdWebView", "Forwarding \"" + str + "\" to ad template");
            loadUrl(str);
        } catch (Throwable th) {
            this.b.b("AdWebView", "Unable to forward to template", th);
            if (runnable != null) {
                runnable.run();
            }
        }
    }

    public void computeScroll() {
    }

    public void destroy() {
        this.f = true;
        super.destroy();
    }

    /* access modifiers changed from: package-private */
    public g getCurrentAd() {
        return this.e;
    }

    public d getStatsManagerHelper() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void onScrollChanged(int i, int i2, int i3, int i4) {
    }

    public void scrollTo(int i, int i2) {
    }

    public void setIsShownOutOfContext(boolean z) {
        this.g = z;
    }

    public void setStatsManagerHelper(d dVar) {
        this.d = dVar;
    }
}
