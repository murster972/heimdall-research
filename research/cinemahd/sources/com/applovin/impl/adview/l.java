package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import com.applovin.impl.adview.h;

@SuppressLint({"ViewConstructor"})
public class l extends FrameLayout {

    /* renamed from: a  reason: collision with root package name */
    private final h f1544a;

    public l(h.a aVar, Activity activity) {
        super(activity);
        setBackgroundColor(0);
        this.f1544a = h.a(aVar, activity);
        addView(this.f1544a);
    }

    public void a(int i, int i2, int i3, int i4) {
        int i5 = i2 + i + i3;
        ViewGroup.LayoutParams layoutParams = getLayoutParams();
        if (layoutParams != null) {
            layoutParams.height = i5;
            layoutParams.width = i5;
        } else {
            setLayoutParams(new FrameLayout.LayoutParams(i5, i5));
        }
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(i, i, i4);
        layoutParams2.setMargins(i3, i3, i3, 0);
        this.f1544a.setLayoutParams(layoutParams2);
        this.f1544a.a(i);
    }
}
