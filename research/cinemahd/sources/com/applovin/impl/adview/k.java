package com.applovin.impl.adview;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.RelativeLayout;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.a.a;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinSdkUtils;
import okhttp3.internal.http2.Http2Connection;

class k extends Dialog implements j {

    /* renamed from: a  reason: collision with root package name */
    private final Activity f1536a;
    private final l b;
    /* access modifiers changed from: private */
    public final r c;
    /* access modifiers changed from: private */
    public final c d;
    private final a e;
    /* access modifiers changed from: private */
    public RelativeLayout f;
    /* access modifiers changed from: private */
    public h g;

    k(a aVar, c cVar, Activity activity, l lVar) {
        super(activity, 16973840);
        if (aVar == null) {
            throw new IllegalArgumentException("No ad specified");
        } else if (cVar == null) {
            throw new IllegalArgumentException("No main view specified");
        } else if (lVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (activity != null) {
            this.b = lVar;
            this.c = lVar.j0();
            this.f1536a = activity;
            this.d = cVar;
            this.e = aVar;
            requestWindowFeature(1);
            setCancelable(false);
        } else {
            throw new IllegalArgumentException("No activity specified");
        }
    }

    private int a(int i) {
        return AppLovinSdkUtils.dpToPx(this.f1536a, i);
    }

    private void a(h.a aVar) {
        if (this.g != null) {
            this.c.d("ExpandedAdDialog", "Attempting to create duplicate close button");
            return;
        }
        this.g = h.a(aVar, this.f1536a);
        this.g.setVisibility(8);
        this.g.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                k.this.c();
            }
        });
        this.g.setClickable(false);
        int a2 = a(((Integer) this.b.a(b.W0)).intValue());
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(a2, a2);
        layoutParams.addRule(10);
        int i = 9;
        layoutParams.addRule(((Boolean) this.b.a(b.Z0)).booleanValue() ? 9 : 11);
        this.g.a(a2);
        int a3 = a(((Integer) this.b.a(b.Y0)).intValue());
        int a4 = a(((Integer) this.b.a(b.X0)).intValue());
        layoutParams.setMargins(a4, a3, a4, 0);
        this.f.addView(this.g, layoutParams);
        this.g.bringToFront();
        int a5 = a(((Integer) this.b.a(b.a1)).intValue());
        View view = new View(this.f1536a);
        view.setBackgroundColor(0);
        int i2 = a2 + a5;
        RelativeLayout.LayoutParams layoutParams2 = new RelativeLayout.LayoutParams(i2, i2);
        layoutParams2.addRule(10);
        if (!((Boolean) this.b.a(b.Z0)).booleanValue()) {
            i = 11;
        }
        layoutParams2.addRule(i);
        layoutParams2.setMargins(a4 - a(5), a3 - a(5), a4 - a(5), 0);
        view.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (k.this.g.isClickable()) {
                    k.this.g.performClick();
                }
            }
        });
        this.f.addView(view, layoutParams2);
        view.bringToFront();
    }

    private void b() {
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
        layoutParams.addRule(13);
        this.d.setLayoutParams(layoutParams);
        this.f = new RelativeLayout(this.f1536a);
        this.f.setLayoutParams(new RelativeLayout.LayoutParams(-1, -1));
        this.f.setBackgroundColor(-1157627904);
        this.f.addView(this.d);
        if (!this.e.H0()) {
            a(this.e.I0());
            d();
        }
        setContentView(this.f);
    }

    /* access modifiers changed from: private */
    public void c() {
        this.d.a("javascript:al_onCloseTapped();", (Runnable) new Runnable() {
            public void run() {
                k.this.dismiss();
            }
        });
    }

    private void d() {
        this.f1536a.runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (k.this.g == null) {
                        k.this.c();
                    }
                    k.this.g.setVisibility(0);
                    k.this.g.bringToFront();
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration(300);
                    alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
                        public void onAnimationEnd(Animation animation) {
                            k.this.g.setClickable(true);
                        }

                        public void onAnimationRepeat(Animation animation) {
                        }

                        public void onAnimationStart(Animation animation) {
                        }
                    });
                    k.this.g.startAnimation(alphaAnimation);
                } catch (Throwable th) {
                    k.this.c.b("ExpandedAdDialog", "Unable to fade in close button", th);
                    k.this.c();
                }
            }
        });
    }

    public a a() {
        return this.e;
    }

    public void dismiss() {
        d statsManagerHelper = this.d.getStatsManagerHelper();
        if (statsManagerHelper != null) {
            statsManagerHelper.e();
        }
        this.f1536a.runOnUiThread(new Runnable() {
            public void run() {
                k.this.f.removeView(k.this.d);
                k.super.dismiss();
            }
        });
    }

    public void onBackPressed() {
        this.d.a("javascript:al_onBackPressed();", (Runnable) new Runnable() {
            public void run() {
                k.this.dismiss();
            }
        });
    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        b();
    }

    /* access modifiers changed from: protected */
    public void onStart() {
        super.onStart();
        try {
            Window window = getWindow();
            if (window != null) {
                window.setFlags(this.f1536a.getWindow().getAttributes().flags, this.f1536a.getWindow().getAttributes().flags);
                window.addFlags(Http2Connection.OKHTTP_CLIENT_WINDOW_SIZE);
                return;
            }
            this.c.e("ExpandedAdDialog", "Unable to turn on hardware acceleration - window is null");
        } catch (Throwable th) {
            this.c.b("ExpandedAdDialog", "Setting window flags failed.", th);
        }
    }
}
