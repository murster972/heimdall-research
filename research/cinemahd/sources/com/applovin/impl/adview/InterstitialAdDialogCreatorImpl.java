package com.applovin.impl.adview;

import android.content.Context;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.adview.InterstitialAdDialogCreator;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinSdk;
import java.lang.ref.WeakReference;

public class InterstitialAdDialogCreatorImpl implements InterstitialAdDialogCreator {

    /* renamed from: a  reason: collision with root package name */
    private static final Object f1478a = new Object();
    private static WeakReference<o> b = new WeakReference<>((Object) null);
    private static WeakReference<Context> c = new WeakReference<>((Object) null);

    public AppLovinInterstitialAdDialog createInterstitialAdDialog(AppLovinSdk appLovinSdk, Context context) {
        o oVar;
        if (appLovinSdk == null) {
            appLovinSdk = AppLovinSdk.getInstance(context);
        }
        synchronized (f1478a) {
            oVar = (o) b.get();
            if (oVar != null) {
                if (c.get() == context) {
                    r.i("InterstitialAdDialogCreator", "An interstitial dialog is already showing, returning it");
                }
            }
            oVar = new o(appLovinSdk, context);
            b = new WeakReference<>(oVar);
            c = new WeakReference<>(context);
        }
        return oVar;
    }
}
