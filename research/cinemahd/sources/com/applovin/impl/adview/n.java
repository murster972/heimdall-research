package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.impl.adview.i;
import com.applovin.impl.adview.u;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.a.h;
import com.applovin.impl.sdk.b.b;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.g;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.AppKilledService;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.p;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.R$drawable;
import com.facebook.common.util.ByteConstants;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class n extends Activity implements j, g.a {
    public static volatile o lastKnownWrapper;
    private final Handler A = new Handler(Looper.getMainLooper());
    /* access modifiers changed from: private */
    public final Handler B = new Handler(Looper.getMainLooper());
    private FrameLayout C;
    /* access modifiers changed from: private */
    public h D;
    /* access modifiers changed from: private */
    public View E;
    /* access modifiers changed from: private */
    public h F;
    /* access modifiers changed from: private */
    public View G;
    /* access modifiers changed from: private */
    public f H;
    private ImageView I;
    /* access modifiers changed from: private */
    public WeakReference<MediaPlayer> J = new WeakReference<>((Object) null);
    private b K;
    /* access modifiers changed from: private */
    public t L;
    /* access modifiers changed from: private */
    public ProgressBar M;
    private u.a N;
    private a O;
    private p P;
    private a Q;
    private AppLovinBroadcastManager.Receiver R;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public m f1545a;
    /* access modifiers changed from: private */
    public o b;
    /* access modifiers changed from: private */
    public d c;
    protected int computedLengthSeconds = 0;
    protected i countdownManager;
    public volatile com.applovin.impl.sdk.a.g currentAd;
    private volatile boolean d = false;
    /* access modifiers changed from: private */
    public volatile boolean e = false;
    /* access modifiers changed from: private */
    public volatile boolean f = false;
    private volatile boolean g = false;
    private volatile boolean h = false;
    /* access modifiers changed from: private */
    public volatile boolean i = false;
    /* access modifiers changed from: private */
    public volatile boolean j = false;
    private boolean k = false;
    private volatile boolean l = false;
    public r logger;
    private boolean m = true;
    /* access modifiers changed from: private */
    public boolean n = false;
    private long o = 0;
    /* access modifiers changed from: private */
    public long p = 0;
    protected volatile boolean postitialWasDisplayed = false;
    /* access modifiers changed from: private */
    public long q = 0;
    /* access modifiers changed from: private */
    public long r = 0;
    /* access modifiers changed from: private */
    public long s = -2;
    public l sdk;
    private int t = 0;
    private int u = Integer.MIN_VALUE;
    private AtomicBoolean v = new AtomicBoolean(false);
    protected volatile boolean videoMuted = false;
    public AppLovinVideoView videoView;
    private AtomicBoolean w = new AtomicBoolean(false);
    private AtomicBoolean x = new AtomicBoolean(false);
    private int y = g.h;
    private boolean z;

    /* access modifiers changed from: private */
    public void A() {
        if (this.I == null) {
            try {
                this.videoMuted = y();
                this.I = new ImageView(this);
                if (!B()) {
                    int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.F1)).intValue());
                    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.H1)).intValue());
                    this.I.setScaleType(ImageView.ScaleType.FIT_CENTER);
                    int a3 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.G1)).intValue());
                    layoutParams.setMargins(a3, a3, a3, a3);
                    if ((this.videoMuted ? this.currentAd.D() : this.currentAd.E()) != null) {
                        r j0 = this.sdk.j0();
                        j0.b("InterActivity", "Added mute button with params: " + layoutParams);
                        a(this.videoMuted);
                        this.I.setClickable(true);
                        this.I.setOnClickListener(new View.OnClickListener() {
                            public void onClick(View view) {
                                n.this.toggleMute();
                            }
                        });
                        this.C.addView(this.I, layoutParams);
                        this.I.bringToFront();
                        return;
                    }
                    this.sdk.j0().e("InterActivity", "Attempting to add mute button but could not find uri");
                    return;
                }
                this.sdk.j0().b("InterActivity", "Mute button should be hidden");
            } catch (Exception e2) {
                this.sdk.j0().a("InterActivity", "Failed to attach mute button", (Throwable) e2);
            }
        }
    }

    private boolean B() {
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.A1)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.B1)).booleanValue() || y()) {
            return false;
        }
        return !((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.D1)).booleanValue();
    }

    /* access modifiers changed from: private */
    public void C() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (n.this.i) {
                        n.this.D.setVisibility(0);
                        return;
                    }
                    long unused = n.this.q = SystemClock.elapsedRealtime();
                    boolean unused2 = n.this.i = true;
                    if (n.this.D() && n.this.E != null) {
                        n.this.E.setVisibility(0);
                        n.this.E.bringToFront();
                    }
                    n.this.D.setVisibility(0);
                    n.this.D.bringToFront();
                    AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration(400);
                    alphaAnimation.setRepeatCount(0);
                    n.this.D.startAnimation(alphaAnimation);
                } catch (Throwable unused3) {
                    n.this.dismiss();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public boolean D() {
        return ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.J0)).intValue() > 0;
    }

    /* access modifiers changed from: private */
    public void E() {
        runOnUiThread(new Runnable() {
            public void run() {
                try {
                    if (!n.this.j && n.this.F != null) {
                        long unused = n.this.s = -1;
                        long unused2 = n.this.r = SystemClock.elapsedRealtime();
                        boolean unused3 = n.this.j = true;
                        n.this.F.setVisibility(0);
                        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                        alphaAnimation.setDuration(400);
                        alphaAnimation.setRepeatCount(0);
                        n.this.F.startAnimation(alphaAnimation);
                        if (n.this.D() && n.this.G != null) {
                            n.this.G.setVisibility(0);
                            n.this.G.bringToFront();
                        }
                    }
                } catch (Throwable th) {
                    r rVar = n.this.logger;
                    rVar.d("InterActivity", "Unable to show skip button: " + th);
                }
            }
        });
    }

    private void F() {
        h hVar;
        if (this.currentAd.r0() >= 0) {
            if (!this.k || (hVar = this.F) == null) {
                hVar = this.D;
            }
            a(com.applovin.impl.sdk.utils.r.b((float) this.currentAd.r0()), hVar);
        }
    }

    /* access modifiers changed from: private */
    public void G() {
        boolean z2 = ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.q1)).booleanValue() && J() > 0;
        if (this.H == null && z2) {
            this.H = new f(this);
            int e2 = this.currentAd.e();
            this.H.setTextColor(e2);
            this.H.setTextSize((float) ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.p1)).intValue());
            this.H.setFinishedStrokeColor(e2);
            this.H.setFinishedStrokeWidth((float) ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.o1)).intValue());
            this.H.setMax(J());
            this.H.setProgress(J());
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.n1)).intValue()), a(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.n1)).intValue()), ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.m1)).intValue());
            int a2 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.l1)).intValue());
            layoutParams.setMargins(a2, a2, a2, a2);
            this.C.addView(this.H, layoutParams);
            this.H.bringToFront();
            this.H.setVisibility(0);
            final long I2 = I();
            this.countdownManager.a("COUNTDOWN_CLOCK", 1000, (i.a) new i.a() {
                public void a() {
                    if (n.this.H != null) {
                        long seconds = TimeUnit.MILLISECONDS.toSeconds(I2 - ((long) n.this.videoView.getCurrentPosition()));
                        if (seconds <= 0) {
                            n.this.H.setVisibility(8);
                            boolean unused = n.this.n = true;
                        } else if (n.this.H()) {
                            n.this.H.setProgress((int) seconds);
                        }
                    }
                }

                public boolean b() {
                    return n.this.H();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public boolean H() {
        return !this.n && !this.postitialWasDisplayed && this.videoView.isPlaying();
    }

    private long I() {
        return TimeUnit.SECONDS.toMillis((long) J());
    }

    private int J() {
        int d2 = this.currentAd.d();
        return (d2 <= 0 && ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.P1)).booleanValue()) ? this.computedLengthSeconds + 1 : d2;
    }

    /* access modifiers changed from: private */
    @SuppressLint({"NewApi"})
    public void K() {
        if (this.M == null && this.currentAd.l()) {
            this.logger.c("InterActivity", "Attaching video progress bar...");
            this.M = new ProgressBar(this, (AttributeSet) null, 16842872);
            this.M.setMax(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.K1)).intValue());
            this.M.setPadding(0, 0, 0, 0);
            if (com.applovin.impl.sdk.utils.g.d()) {
                try {
                    this.M.setProgressTintList(ColorStateList.valueOf(this.currentAd.m()));
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Unable to update progress bar color.", th);
                }
            }
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(this.videoView.getWidth(), 20, 80);
            layoutParams.setMargins(0, 0, 0, ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.L1)).intValue());
            this.C.addView(this.M, layoutParams);
            this.M.bringToFront();
            this.countdownManager.a("PROGRESS_BAR", ((Long) this.sdk.a(com.applovin.impl.sdk.c.b.J1)).longValue(), (i.a) new i.a() {
                public void a() {
                    if (n.this.M == null) {
                        return;
                    }
                    if (n.this.shouldContinueFullLengthVideoCountdown()) {
                        n.this.M.setProgress((int) ((((float) n.this.videoView.getCurrentPosition()) / ((float) n.this.videoView.getDuration())) * ((float) ((Integer) n.this.sdk.a(com.applovin.impl.sdk.c.b.K1)).intValue())));
                        return;
                    }
                    n.this.M.setVisibility(8);
                }

                public boolean b() {
                    return n.this.shouldContinueFullLengthVideoCountdown();
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public void L() {
        final s a2 = this.currentAd.a();
        if (o.b(this.currentAd.A0()) && a2 != null && this.L == null) {
            this.logger.c("InterActivity", "Attaching video button...");
            this.L = M();
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams((int) ((((double) a2.a()) / 100.0d) * ((double) this.videoView.getWidth())), (int) ((((double) a2.b()) / 100.0d) * ((double) this.videoView.getHeight())), a2.d());
            int a3 = a(a2.c());
            layoutParams.setMargins(a3, a3, a3, a3);
            this.C.addView(this.L, layoutParams);
            this.L.bringToFront();
            if (a2.i() > 0.0f) {
                this.L.setVisibility(4);
                this.B.postDelayed(new Runnable() {
                    public void run() {
                        long g = a2.g();
                        n nVar = n.this;
                        nVar.a((View) nVar.L, true, g);
                        n.this.L.bringToFront();
                    }
                }, com.applovin.impl.sdk.utils.r.b(a2.i()));
            }
            if (a2.j() > 0.0f) {
                this.B.postDelayed(new Runnable() {
                    public void run() {
                        long h = a2.h();
                        n nVar = n.this;
                        nVar.a((View) nVar.L, false, h);
                    }
                }, com.applovin.impl.sdk.utils.r.b(a2.j()));
            }
        }
    }

    private t M() {
        r rVar = this.logger;
        rVar.b("InterActivity", "Create video button with HTML = " + this.currentAd.A0());
        u uVar = new u(this.sdk);
        this.N = new u.a() {
            public void a(t tVar) {
                n.this.logger.b("InterActivity", "Clicking through from video button...");
                n.this.clickThroughFromVideo(tVar.getAndClearLastClickLocation());
            }

            public void b(t tVar) {
                n.this.logger.b("InterActivity", "Skipping video from video button...");
                n.this.skipVideo();
            }

            public void c(t tVar) {
                n.this.logger.b("InterActivity", "Closing ad from video button...");
                n.this.dismiss();
            }
        };
        uVar.a(new WeakReference(this.N));
        t tVar = new t(uVar, getApplicationContext());
        tVar.a(this.currentAd.A0());
        return tVar;
    }

    private void N() {
        if (this.l) {
            this.O = new a(this, ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.O1)).intValue(), 16842874);
            this.O.setColor(Color.parseColor("#75FFFFFF"));
            this.O.setBackgroundColor(Color.parseColor("#00000000"));
            this.O.setVisibility(8);
            this.C.addView(this.O, new FrameLayout.LayoutParams(-1, -1, 17));
            this.C.bringChildToFront(this.O);
        }
    }

    /* access modifiers changed from: private */
    public void O() {
        a aVar = this.O;
        if (aVar != null) {
            aVar.a();
        }
    }

    /* access modifiers changed from: private */
    public void P() {
        a aVar = this.O;
        if (aVar != null) {
            aVar.b();
        }
    }

    private int a(int i2) {
        return AppLovinSdkUtils.dpToPx(this, i2);
    }

    private int a(int i2, boolean z2) {
        if (z2) {
            if (i2 == 0) {
                return 0;
            }
            if (i2 == 1) {
                return 9;
            }
            if (i2 == 2) {
                return 8;
            }
            return i2 == 3 ? 1 : -1;
        } else if (i2 == 0) {
            return 1;
        } else {
            if (i2 == 1) {
                return 0;
            }
            if (i2 == 2) {
                return 9;
            }
            return i2 == 3 ? 8 : -1;
        }
    }

    /* access modifiers changed from: private */
    public void a() {
        if (c()) {
            m();
            pauseReportRewardTask();
            this.logger.b("InterActivity", "Prompting incentivized ad close warning");
            this.K.b();
            return;
        }
        skipVideo();
    }

    private void a(long j2, final h hVar) {
        this.B.postDelayed(new Runnable() {
            public void run() {
                if (hVar.equals(n.this.D)) {
                    n.this.C();
                } else if (hVar.equals(n.this.F)) {
                    n.this.E();
                }
            }
        }, j2);
    }

    /* access modifiers changed from: private */
    public void a(PointF pointF) {
        if (!this.currentAd.b() || this.currentAd.n0() == null) {
            u();
            v();
            return;
        }
        this.sdk.j0().b("InterActivity", "Clicking through video...");
        clickThroughFromVideo(pointF);
    }

    private void a(Uri uri) {
        this.videoView = new AppLovinVideoView(this, this.sdk);
        if (uri != null) {
            this.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mediaPlayer) {
                    WeakReference unused = n.this.J = new WeakReference(mediaPlayer);
                    float f = n.this.y() ^ true ? 1.0f : 0.0f;
                    mediaPlayer.setVolume(f, f);
                    int videoWidth = mediaPlayer.getVideoWidth();
                    int videoHeight = mediaPlayer.getVideoHeight();
                    n.this.computedLengthSeconds = (int) TimeUnit.MILLISECONDS.toSeconds((long) mediaPlayer.getDuration());
                    n.this.videoView.setVideoSize(videoWidth, videoHeight);
                    SurfaceHolder holder = n.this.videoView.getHolder();
                    if (holder.getSurface() != null) {
                        mediaPlayer.setDisplay(holder);
                    }
                    mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                        public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                            n.this.B.post(new Runnable() {
                                public void run() {
                                    n nVar = n.this;
                                    nVar.handleMediaError("Media player error (" + i + "," + i2 + ")");
                                }
                            });
                            return true;
                        }
                    });
                    mediaPlayer.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
                            if (i != 3) {
                                if (i == 701) {
                                    n.this.O();
                                    if (n.this.c == null) {
                                        return false;
                                    }
                                    n.this.c.g();
                                    return false;
                                } else if (i != 702) {
                                    return false;
                                }
                            }
                            n.this.P();
                            return false;
                        }
                    });
                    if (n.this.p == 0) {
                        n.this.G();
                        n.this.A();
                        n.this.L();
                        n.this.K();
                        n.this.playVideo();
                        n.this.i();
                    }
                }
            });
            this.videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                public void onCompletion(MediaPlayer mediaPlayer) {
                    n.this.x();
                }
            });
            this.videoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                public boolean onError(MediaPlayer mediaPlayer, final int i, final int i2) {
                    n.this.B.post(new Runnable() {
                        public void run() {
                            n nVar = n.this;
                            nVar.handleMediaError("Video view error (" + i + "," + i2 + ")");
                        }
                    });
                    return true;
                }
            });
            StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
            this.videoView.setVideoURI(uri);
            StrictMode.setThreadPolicy(allowThreadDiskReads);
        }
        this.videoView.setLayoutParams(new FrameLayout.LayoutParams(-1, -1, 17));
        this.videoView.setOnTouchListener(new AppLovinTouchToClickListener(this.sdk, this, new AppLovinTouchToClickListener.OnClickListener() {
            public void onClick(View view, PointF pointF) {
                n.this.a(pointF);
            }
        }));
        this.C.addView(this.videoView);
        setContentView(this.C);
        F();
        N();
    }

    /* access modifiers changed from: private */
    public void a(final View view, final boolean z2, long j2) {
        float f2 = 0.0f;
        float f3 = z2 ? 0.0f : 1.0f;
        if (z2) {
            f2 = 1.0f;
        }
        AlphaAnimation alphaAnimation = new AlphaAnimation(f3, f2);
        alphaAnimation.setDuration(j2);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener(this) {
            public void onAnimationEnd(Animation animation) {
                if (!z2) {
                    view.setVisibility(4);
                }
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationStart(Animation animation) {
                view.setVisibility(0);
            }
        });
        view.startAnimation(alphaAnimation);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        k.a(this.b.d(), appLovinAd);
        this.e = true;
        this.sdk.C().a((Object) appLovinAd);
        this.sdk.K().a((Object) appLovinAd);
        AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
            public void run() {
                n nVar = n.this;
                nVar.b(nVar.videoMuted);
            }
        }, ((Long) this.sdk.a(com.applovin.impl.sdk.c.b.Q1)).longValue());
    }

    private void a(AppLovinAd appLovinAd, double d2, boolean z2) {
        k.a(this.b.c(), appLovinAd, d2, z2);
    }

    private void a(final String str) {
        o oVar = this.b;
        if (oVar != null) {
            final AppLovinAdDisplayListener d2 = oVar.d();
            if ((d2 instanceof com.applovin.impl.sdk.a.i) && this.x.compareAndSet(false, true)) {
                runOnUiThread(new Runnable(this) {
                    public void run() {
                        ((com.applovin.impl.sdk.a.i) d2).onAdDisplayFailed(str);
                    }
                });
            }
        }
    }

    private void a(final String str, long j2) {
        if (j2 >= 0) {
            this.B.postDelayed(new Runnable() {
                public void run() {
                    c adWebView = ((AdViewControllerImpl) n.this.f1545a.getAdViewController()).getAdWebView();
                    if (adWebView != null && o.b(str)) {
                        adWebView.a(str);
                    }
                }
            }, j2);
        }
    }

    private void a(boolean z2) {
        if (com.applovin.impl.sdk.utils.g.d()) {
            AnimatedVectorDrawable animatedVectorDrawable = (AnimatedVectorDrawable) getDrawable(z2 ? R$drawable.unmute_to_mute : R$drawable.mute_to_unmute);
            if (animatedVectorDrawable != null) {
                this.I.setScaleType(ImageView.ScaleType.FIT_XY);
                this.I.setImageDrawable(animatedVectorDrawable);
                animatedVectorDrawable.start();
                return;
            }
            return;
        }
        Uri D2 = z2 ? this.currentAd.D() : this.currentAd.E();
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        this.I.setImageURI(D2);
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    /* access modifiers changed from: private */
    public void b() {
        c adWebView;
        if (this.currentAd.s() && (adWebView = ((AdViewControllerImpl) this.f1545a.getAdViewController()).getAdWebView()) != null) {
            adWebView.a("javascript:al_onCloseButtonTapped();");
        }
        if (d()) {
            this.logger.b("InterActivity", "Prompting incentivized non-video ad close warning");
            this.K.c();
            return;
        }
        dismiss();
    }

    private void b(int i2) {
        try {
            setRequestedOrientation(i2);
        } catch (Throwable th) {
            this.sdk.j0().b("InterActivity", "Failed to set requested orientation", th);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        if (r7 == 2) goto L_0x0028;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:28:0x005f, code lost:
        if (r7 == 1) goto L_0x0053;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:7:0x0026, code lost:
        if (r7 == 1) goto L_0x0028;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private void b(int r7, boolean r8) {
        /*
            r6 = this;
            com.applovin.impl.sdk.l r0 = r6.sdk
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.v1
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            com.applovin.impl.adview.o r1 = r6.b
            com.applovin.impl.sdk.a.g$b r1 = r1.f()
            com.applovin.impl.sdk.a.g$b r2 = com.applovin.impl.sdk.a.g.b.ACTIVITY_PORTRAIT
            r3 = 3
            r4 = 2
            r5 = 1
            if (r1 != r2) goto L_0x003a
            r1 = 9
            if (r8 == 0) goto L_0x002c
            if (r7 == r5) goto L_0x0024
            if (r7 == r3) goto L_0x0024
            goto L_0x0030
        L_0x0024:
            if (r0 == 0) goto L_0x0062
            if (r7 != r5) goto L_0x0030
        L_0x0028:
            r6.b((int) r1)
            goto L_0x0062
        L_0x002c:
            if (r7 == 0) goto L_0x0034
            if (r7 == r4) goto L_0x0034
        L_0x0030:
            r6.b((int) r5)
            goto L_0x0062
        L_0x0034:
            if (r0 == 0) goto L_0x0062
            if (r7 != 0) goto L_0x0028
            r1 = 1
            goto L_0x0028
        L_0x003a:
            com.applovin.impl.adview.o r1 = r6.b
            com.applovin.impl.sdk.a.g$b r1 = r1.f()
            com.applovin.impl.sdk.a.g$b r2 = com.applovin.impl.sdk.a.g.b.ACTIVITY_LANDSCAPE
            if (r1 != r2) goto L_0x0062
            r1 = 8
            r2 = 0
            if (r8 == 0) goto L_0x0055
            if (r7 == 0) goto L_0x004e
            if (r7 == r4) goto L_0x004e
            goto L_0x0059
        L_0x004e:
            if (r0 == 0) goto L_0x0062
            if (r7 != r4) goto L_0x0053
            goto L_0x0028
        L_0x0053:
            r1 = 0
            goto L_0x0028
        L_0x0055:
            if (r7 == r5) goto L_0x005d
            if (r7 == r3) goto L_0x005d
        L_0x0059:
            r6.b((int) r2)
            goto L_0x0062
        L_0x005d:
            if (r0 == 0) goto L_0x0062
            if (r7 != r5) goto L_0x0028
            goto L_0x0053
        L_0x0062:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.n.b(int, boolean):void");
    }

    /* access modifiers changed from: private */
    public void b(AppLovinAd appLovinAd) {
        dismiss();
        c(appLovinAd);
    }

    private void b(String str) {
        com.applovin.impl.sdk.a.g gVar = this.currentAd;
        if (gVar != null && gVar.t()) {
            a(str, 0);
        }
    }

    /* access modifiers changed from: private */
    public void b(boolean z2) {
        c adWebView;
        if (this.currentAd.r() && (adWebView = ((AdViewControllerImpl) this.f1545a.getAdViewController()).getAdWebView()) != null) {
            try {
                adWebView.a(z2 ? "javascript:al_mute();" : "javascript:al_unmute();");
            } catch (Throwable th) {
                this.logger.b("InterActivity", "Unable to forward mute setting to template.", th);
            }
        }
    }

    private void c(AppLovinAd appLovinAd) {
        if (!this.f) {
            this.f = true;
            o oVar = this.b;
            if (oVar != null) {
                k.b(oVar.d(), appLovinAd);
            }
            this.sdk.C().b((Object) appLovinAd);
            this.sdk.K().a();
        }
    }

    private void c(boolean z2) {
        this.videoMuted = z2;
        MediaPlayer mediaPlayer = (MediaPlayer) this.J.get();
        if (mediaPlayer != null) {
            float f2 = (float) (z2 ? 0 : 1);
            try {
                mediaPlayer.setVolume(f2, f2);
            } catch (IllegalStateException e2) {
                r rVar = this.logger;
                rVar.b("InterActivity", "Failed to set MediaPlayer muted: " + z2, e2);
            }
        }
    }

    private boolean c() {
        return g() && !isFullyWatched() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.y0)).booleanValue() && this.K != null;
    }

    private void d(AppLovinAd appLovinAd) {
        if (!this.g) {
            this.g = true;
            k.a(this.b.c(), appLovinAd);
        }
    }

    private boolean d() {
        return h() && !f() && ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.D0)).booleanValue() && this.K != null;
    }

    private int e() {
        if (!(this.currentAd instanceof com.applovin.impl.sdk.a.a)) {
            return 0;
        }
        float F0 = ((com.applovin.impl.sdk.a.a) this.currentAd).F0();
        if (F0 <= 0.0f) {
            F0 = (float) this.currentAd.t0();
        }
        return (int) Math.min((com.applovin.impl.sdk.utils.r.a(System.currentTimeMillis() - this.o) / ((double) F0)) * 100.0d, 100.0d);
    }

    private boolean f() {
        return e() >= this.currentAd.n();
    }

    private boolean g() {
        return AppLovinAdType.INCENTIVIZED.equals(this.currentAd.getType());
    }

    private boolean h() {
        return !this.currentAd.hasVideoUrl() && g();
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0066, code lost:
        if (r1 > 0) goto L_0x008a;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0098, code lost:
        if (r1 > 0) goto L_0x008a;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void i() {
        /*
            r7 = this;
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            if (r0 == 0) goto L_0x00d9
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            long r0 = r0.K()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x0018
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            int r0 = r0.L()
            if (r0 < 0) goto L_0x00d9
        L_0x0018:
            com.applovin.impl.sdk.utils.p r0 = r7.P
            if (r0 != 0) goto L_0x00d9
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            long r0 = r0.K()
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 < 0) goto L_0x002e
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            long r0 = r0.K()
            goto L_0x00a9
        L_0x002e:
            boolean r0 = r7.isVastAd()
            if (r0 == 0) goto L_0x0069
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            com.applovin.impl.a.a r0 = (com.applovin.impl.a.a) r0
            com.applovin.impl.a.j r1 = r0.L0()
            if (r1 == 0) goto L_0x0051
            int r4 = r1.b()
            if (r4 <= 0) goto L_0x0051
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.SECONDS
            int r1 = r1.b()
            long r5 = (long) r1
            long r4 = r4.toMillis(r5)
            long r2 = r2 + r4
            goto L_0x005b
        L_0x0051:
            com.applovin.impl.adview.AppLovinVideoView r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x005b
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x005b:
            boolean r1 = r0.M()
            if (r1 == 0) goto L_0x009b
            long r0 = r0.t0()
            int r1 = (int) r0
            if (r1 <= 0) goto L_0x009b
            goto L_0x008a
        L_0x0069:
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.a.a
            if (r0 == 0) goto L_0x009b
            com.applovin.impl.sdk.a.g r0 = r7.currentAd
            com.applovin.impl.sdk.a.a r0 = (com.applovin.impl.sdk.a.a) r0
            com.applovin.impl.adview.AppLovinVideoView r1 = r7.videoView
            int r1 = r1.getDuration()
            if (r1 <= 0) goto L_0x007d
            long r4 = (long) r1
            long r2 = r2 + r4
        L_0x007d:
            boolean r1 = r0.M()
            if (r1 == 0) goto L_0x009b
            float r1 = r0.F0()
            int r1 = (int) r1
            if (r1 <= 0) goto L_0x0093
        L_0x008a:
            java.util.concurrent.TimeUnit r0 = java.util.concurrent.TimeUnit.SECONDS
            long r4 = (long) r1
            long r0 = r0.toMillis(r4)
            long r2 = r2 + r0
            goto L_0x009b
        L_0x0093:
            long r0 = r0.t0()
            int r1 = (int) r0
            if (r1 <= 0) goto L_0x009b
            goto L_0x008a
        L_0x009b:
            double r0 = (double) r2
            com.applovin.impl.sdk.a.g r2 = r7.currentAd
            int r2 = r2.L()
            double r2 = (double) r2
            r4 = 4636737291354636288(0x4059000000000000, double:100.0)
            double r2 = r2 / r4
            double r0 = r0 * r2
            long r0 = (long) r0
        L_0x00a9:
            com.applovin.impl.sdk.r r2 = r7.logger
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Scheduling report reward in "
            r3.append(r4)
            java.util.concurrent.TimeUnit r4 = java.util.concurrent.TimeUnit.MILLISECONDS
            long r4 = r4.toSeconds(r0)
            r3.append(r4)
            java.lang.String r4 = " seconds..."
            r3.append(r4)
            java.lang.String r3 = r3.toString()
            java.lang.String r4 = "InterActivity"
            r2.b(r4, r3)
            com.applovin.impl.sdk.l r2 = r7.sdk
            com.applovin.impl.adview.n$17 r3 = new com.applovin.impl.adview.n$17
            r3.<init>()
            com.applovin.impl.sdk.utils.p r0 = com.applovin.impl.sdk.utils.p.a(r0, r2, r3)
            r7.P = r0
        L_0x00d9:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.n.i():void");
    }

    private void j() {
        View view;
        String str;
        StringBuilder sb;
        r rVar;
        m mVar = this.f1545a;
        if (mVar != null) {
            mVar.setAdDisplayListener(new AppLovinAdDisplayListener() {
                public void adDisplayed(AppLovinAd appLovinAd) {
                    if (!n.this.e) {
                        n.this.a(appLovinAd);
                    }
                }

                public void adHidden(AppLovinAd appLovinAd) {
                    n.this.b(appLovinAd);
                }
            });
            this.f1545a.setAdClickListener(new AppLovinAdClickListener() {
                public void adClicked(AppLovinAd appLovinAd) {
                    k.a(n.this.b.e(), appLovinAd);
                }
            });
            this.currentAd = this.b.b();
            if (this.w.compareAndSet(false, true)) {
                this.sdk.c0().trackImpression(this.currentAd);
                this.currentAd.setHasShown(true);
            }
            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(-1, -1);
            this.C = new FrameLayout(this);
            this.C.setLayoutParams(layoutParams);
            this.C.setBackgroundColor(-16777216);
            this.countdownManager = new i(this.A, this.sdk);
            z();
            if (this.currentAd.isVideoAd()) {
                this.l = this.currentAd.l0();
                if (this.l) {
                    rVar = this.logger;
                    sb = new StringBuilder();
                    str = "Preparing stream for ";
                } else {
                    rVar = this.logger;
                    sb = new StringBuilder();
                    str = "Preparing cached video playback for ";
                }
                sb.append(str);
                sb.append(this.currentAd.m0());
                rVar.b("InterActivity", sb.toString());
                d dVar = this.c;
                if (dVar != null) {
                    dVar.b(this.l ? 1 : 0);
                }
            }
            this.videoMuted = y();
            Uri m0 = this.currentAd.m0();
            a(m0);
            if (m0 == null) {
                i();
            }
            this.D.bringToFront();
            if (D() && (view = this.E) != null) {
                view.bringToFront();
            }
            h hVar = this.F;
            if (hVar != null) {
                hVar.bringToFront();
            }
            if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.K3)).booleanValue()) {
                this.C.addView(this.f1545a);
                this.f1545a.setVisibility(4);
            }
            this.f1545a.renderAd(this.currentAd);
            if (!this.currentAd.hasVideoUrl()) {
                if (h()) {
                    d((AppLovinAd) this.currentAd);
                }
                showPostitial();
                return;
            }
            return;
        }
        exitWithError("AdView was null");
    }

    private void k() {
        if (this.videoView != null) {
            boolean z2 = false;
            try {
                z2 = this.currentAd.J();
            } catch (Throwable unused) {
            }
            this.t = getVideoPercentViewed();
            if (z2) {
                this.videoView.pause();
            } else {
                this.videoView.stopPlayback();
            }
        }
    }

    private boolean l() {
        return this.videoMuted;
    }

    private void m() {
        AppLovinVideoView appLovinVideoView = this.videoView;
        this.sdk.a(com.applovin.impl.sdk.c.d.v, Integer.valueOf(appLovinVideoView != null ? appLovinVideoView.getCurrentPosition() : 0));
        this.sdk.a(com.applovin.impl.sdk.c.d.w, true);
        try {
            this.countdownManager.c();
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to pause countdown timers", th);
        }
        AppLovinVideoView appLovinVideoView2 = this.videoView;
        if (appLovinVideoView2 != null) {
            appLovinVideoView2.pause();
        }
    }

    private void n() {
        long max = Math.max(0, ((Long) this.sdk.a(com.applovin.impl.sdk.c.b.M1)).longValue());
        if (max > 0) {
            r j0 = this.sdk.j0();
            j0.b("InterActivity", "Resuming video with delay of " + max);
            this.B.postDelayed(new Runnable() {
                public void run() {
                    n.this.o();
                }
            }, max);
            return;
        }
        this.sdk.j0().b("InterActivity", "Resuming video immediately");
        o();
    }

    /* access modifiers changed from: private */
    public void o() {
        AppLovinVideoView appLovinVideoView;
        if (!this.postitialWasDisplayed && (appLovinVideoView = this.videoView) != null && !appLovinVideoView.isPlaying()) {
            this.videoView.seekTo(((Integer) this.sdk.b(com.applovin.impl.sdk.c.d.v, Integer.valueOf(this.videoView.getDuration()))).intValue());
            this.videoView.start();
            this.countdownManager.a();
        }
    }

    private void p() {
        if (!this.h) {
            boolean z2 = true;
            this.h = true;
            try {
                int videoPercentViewed = getVideoPercentViewed();
                if (this.currentAd.hasVideoUrl()) {
                    a((AppLovinAd) this.currentAd, (double) videoPercentViewed, isFullyWatched());
                    if (this.c != null) {
                        this.c.c((long) videoPercentViewed);
                    }
                } else if ((this.currentAd instanceof com.applovin.impl.sdk.a.a) && h()) {
                    int e2 = e();
                    r rVar = this.logger;
                    rVar.b("InterActivity", "Rewarded playable engaged at " + e2 + " percent");
                    com.applovin.impl.sdk.a.g gVar = this.currentAd;
                    double d2 = (double) e2;
                    if (e2 < this.currentAd.n()) {
                        z2 = false;
                    }
                    a((AppLovinAd) gVar, d2, z2);
                }
                long currentTimeMillis = System.currentTimeMillis() - this.o;
                this.sdk.c0().trackVideoEnd(this.currentAd, TimeUnit.MILLISECONDS.toSeconds(currentTimeMillis), videoPercentViewed, this.l);
                this.sdk.c0().trackFullScreenAdClosed(this.currentAd, SystemClock.elapsedRealtime() - this.q, this.s, this.z, this.y);
            } catch (Throwable th) {
                r rVar2 = this.logger;
                if (rVar2 != null) {
                    rVar2.b("InterActivity", "Failed to notify end listener.", th);
                }
            }
        }
    }

    private boolean q() {
        int identifier = getResources().getIdentifier((String) this.sdk.a(com.applovin.impl.sdk.c.b.x1), "bool", "android");
        return identifier > 0 && getResources().getBoolean(identifier);
    }

    /* access modifiers changed from: private */
    @TargetApi(19)
    public void r() {
        getWindow().getDecorView().setSystemUiVisibility(5894);
    }

    private boolean s() {
        l lVar;
        if (this.b == null || (lVar = this.sdk) == null || ((Boolean) lVar.a(com.applovin.impl.sdk.c.b.r1)).booleanValue()) {
            return true;
        }
        if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.s1)).booleanValue() || !this.i) {
            return ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.t1)).booleanValue() && this.postitialWasDisplayed;
        }
        return true;
    }

    @SuppressLint({"WrongConstant"})
    private void t() {
        int i2;
        if (this.sdk == null || !isFinishing()) {
            if (!(this.currentAd == null || (i2 = this.u) == Integer.MIN_VALUE)) {
                b(i2);
            }
            finish();
        }
    }

    private void u() {
        f fVar;
        if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.y1)).booleanValue() && (fVar = this.H) != null && fVar.getVisibility() != 8) {
            a((View) this.H, this.H.getVisibility() == 4, 750);
        }
    }

    private void v() {
        t tVar;
        s a2 = this.currentAd.a();
        if (a2 != null && a2.e() && !this.postitialWasDisplayed && (tVar = this.L) != null) {
            a((View) this.L, tVar.getVisibility() == 4, a2.f());
        }
    }

    private void w() {
        l lVar = this.sdk;
        if (lVar != null) {
            lVar.a(com.applovin.impl.sdk.c.d.w, false);
            this.sdk.a(com.applovin.impl.sdk.c.d.v, 0);
        }
    }

    /* access modifiers changed from: private */
    public void x() {
        this.d = true;
        showPostitial();
    }

    /* access modifiers changed from: private */
    public boolean y() {
        return ((Integer) this.sdk.b(com.applovin.impl.sdk.c.d.v, 0)).intValue() > 0 ? this.videoMuted : ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.E1)).booleanValue() ? this.sdk.Y().isMuted() : ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.C1)).booleanValue();
    }

    private void z() {
        this.D = h.a(this.currentAd.u0(), this);
        this.D.setVisibility(8);
        this.D.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                n.this.b();
            }
        });
        int a2 = a(this.currentAd.v());
        int i2 = 3;
        int i3 = (this.currentAd.y() ? 3 : 5) | 48;
        if (!this.currentAd.z()) {
            i2 = 5;
        }
        int i4 = i2 | 48;
        FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(a2, a2, i3 | 48);
        this.D.a(a2);
        int a3 = a(this.currentAd.w());
        int a4 = a(this.currentAd.x());
        layoutParams.setMargins(a4, a3, a4, a3);
        this.C.addView(this.D, layoutParams);
        this.F = h.a(this.currentAd.v0(), this);
        this.F.setVisibility(8);
        this.F.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                n.this.a();
            }
        });
        FrameLayout.LayoutParams layoutParams2 = new FrameLayout.LayoutParams(a2, a2, i4);
        layoutParams2.setMargins(a4, a3, a4, a3);
        this.F.a(a2);
        this.C.addView(this.F, layoutParams2);
        this.F.bringToFront();
        if (D()) {
            int a5 = a(((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.J0)).intValue());
            this.E = new View(this);
            this.E.setBackgroundColor(0);
            this.E.setVisibility(8);
            this.G = new View(this);
            this.G.setBackgroundColor(0);
            this.G.setVisibility(8);
            int i5 = a2 + a5;
            int a6 = a3 - a(5);
            FrameLayout.LayoutParams layoutParams3 = new FrameLayout.LayoutParams(i5, i5, i3);
            layoutParams3.setMargins(a6, a6, a6, a6);
            FrameLayout.LayoutParams layoutParams4 = new FrameLayout.LayoutParams(i5, i5, i4);
            layoutParams4.setMargins(a6, a6, a6, a6);
            this.E.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    n.this.D.performClick();
                }
            });
            this.G.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    n.this.F.performClick();
                }
            });
            this.C.addView(this.E, layoutParams3);
            this.E.bringToFront();
            this.C.addView(this.G, layoutParams4);
            this.G.bringToFront();
        }
    }

    public void clickThroughFromVideo(PointF pointF) {
        try {
            this.sdk.c0().trackAndLaunchVideoClick(this.currentAd, this.f1545a, this.currentAd.n0(), pointF);
            k.a(this.b.e(), (AppLovinAd) this.currentAd);
            if (this.c != null) {
                this.c.b();
            }
        } catch (Throwable th) {
            this.sdk.j0().b("InterActivity", "Encountered error while clicking through video.", th);
        }
    }

    public void continueVideo() {
        o();
    }

    public void dismiss() {
        long currentTimeMillis = System.currentTimeMillis() - this.o;
        r.f("InterActivity", "Dismissing ad after " + currentTimeMillis + " milliseconds elapsed");
        l lVar = this.sdk;
        if (lVar != null) {
            if (((Boolean) lVar.a(com.applovin.impl.sdk.c.b.z1)).booleanValue()) {
                stopService(new Intent(getBaseContext(), AppKilledService.class));
                this.sdk.I().unregisterReceiver(this.R);
            }
            this.sdk.H().b((g.a) this);
        }
        w();
        p();
        if (this.b != null) {
            if (this.currentAd != null) {
                c((AppLovinAd) this.currentAd);
                d dVar = this.c;
                if (dVar != null) {
                    dVar.c();
                    this.c = null;
                }
                a("javascript:al_onPoststitialDismiss();", (long) this.currentAd.q());
            }
            this.b.g();
        }
        lastKnownWrapper = null;
        t();
    }

    public void exitWithError(String str) {
        a(str);
        try {
            r.c("InterActivity", "Failed to properly render an Interstitial Activity, due to error: " + str, new Throwable("Initialized = " + o.l + "; CleanedUp = " + o.m));
            c((AppLovinAd) new h(this.currentAd != null ? this.currentAd.getAdZone() : com.applovin.impl.sdk.a.d.a(str, this.sdk), this.sdk));
        } catch (Exception e2) {
            r.c("InterActivity", "Failed to show a video ad due to error:", e2);
        }
        dismiss();
    }

    public boolean getPostitialWasDisplayed() {
        return this.postitialWasDisplayed;
    }

    public int getVideoPercentViewed() {
        if (this.d) {
            return 100;
        }
        AppLovinVideoView appLovinVideoView = this.videoView;
        if (appLovinVideoView != null) {
            int duration = appLovinVideoView.getDuration();
            return duration > 0 ? (int) ((((double) this.videoView.getCurrentPosition()) / ((double) duration)) * 100.0d) : this.t;
        }
        this.logger.e("InterActivity", "No video view detected on video end");
        return 0;
    }

    public void handleMediaError(String str) {
        this.logger.e("InterActivity", str);
        if (this.v.compareAndSet(false, true)) {
            a(str);
            dismiss();
        }
    }

    /* access modifiers changed from: protected */
    public boolean isFullyWatched() {
        return getVideoPercentViewed() >= this.currentAd.n();
    }

    /* access modifiers changed from: protected */
    public boolean isVastAd() {
        return this.currentAd instanceof com.applovin.impl.a.a;
    }

    public void onBackPressed() {
        h hVar;
        if (this.currentAd != null) {
            if (this.currentAd.B() && !this.postitialWasDisplayed) {
                return;
            }
            if (this.currentAd.C() && this.postitialWasDisplayed) {
                return;
            }
        }
        if (s()) {
            this.logger.b("InterActivity", "Back button was pressed; forwarding to Android for handling...");
        } else {
            try {
                if (!this.postitialWasDisplayed && this.k && this.F != null && this.F.getVisibility() == 0 && this.F.getAlpha() > 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to skip button.");
                    hVar = this.F;
                } else if (this.D == null || this.D.getVisibility() != 0 || this.D.getAlpha() <= 0.0f) {
                    this.logger.b("InterActivity", "Back button was pressed, but was not eligible for dismissal.");
                    b("javascript:al_onBackPressed();");
                    return;
                } else {
                    this.logger.b("InterActivity", "Back button was pressed; forwarding as a click to close button.");
                    hVar = this.D;
                }
                hVar.performClick();
                b("javascript:al_onBackPressed();");
                return;
            } catch (Exception unused) {
            }
        }
        super.onBackPressed();
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Removed duplicated region for block: B:63:0x01da  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onCreate(android.os.Bundle r10) {
        /*
            r9 = this;
            java.lang.String r0 = "InterActivity"
            super.onCreate(r10)
            if (r10 == 0) goto L_0x0012
            java.lang.String r1 = "instance_impression_tracked"
            boolean r1 = r10.getBoolean(r1)
            java.util.concurrent.atomic.AtomicBoolean r2 = r9.w
            r2.set(r1)
        L_0x0012:
            r1 = 1
            r9.requestWindowFeature(r1)
            android.os.StrictMode$ThreadPolicy r1 = android.os.StrictMode.allowThreadDiskReads()
            android.content.Intent r2 = r9.getIntent()     // Catch:{ all -> 0x01bf }
            java.lang.String r3 = "com.applovin.interstitial.wrapper_id"
            java.lang.String r2 = r2.getStringExtra(r3)     // Catch:{ all -> 0x01bf }
            if (r2 == 0) goto L_0x01bc
            boolean r3 = r2.isEmpty()     // Catch:{ all -> 0x01bf }
            if (r3 != 0) goto L_0x01bc
            com.applovin.impl.adview.o r2 = com.applovin.impl.adview.o.a((java.lang.String) r2)     // Catch:{ all -> 0x01bf }
            r9.b = r2     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.o r2 = r9.b     // Catch:{ all -> 0x01bf }
            if (r2 != 0) goto L_0x003e
            com.applovin.impl.adview.o r2 = lastKnownWrapper     // Catch:{ all -> 0x01bf }
            if (r2 == 0) goto L_0x003e
            com.applovin.impl.adview.o r2 = lastKnownWrapper     // Catch:{ all -> 0x01bf }
            r9.b = r2     // Catch:{ all -> 0x01bf }
        L_0x003e:
            com.applovin.impl.adview.o r2 = r9.b     // Catch:{ all -> 0x01bf }
            if (r2 == 0) goto L_0x01a8
            com.applovin.impl.adview.o r2 = r9.b     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.a.g r2 = r2.b()     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.o r3 = r9.b     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r3 = r3.a()     // Catch:{ all -> 0x01bf }
            r9.sdk = r3     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.o r3 = r9.b     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r3 = r3.a()     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.r r3 = r3.j0()     // Catch:{ all -> 0x01bf }
            r9.logger = r3     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r3 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r4 = com.applovin.impl.sdk.c.b.z1     // Catch:{ all -> 0x01bf }
            java.lang.Object r3 = r3.a(r4)     // Catch:{ all -> 0x01bf }
            java.lang.Boolean r3 = (java.lang.Boolean) r3     // Catch:{ all -> 0x01bf }
            boolean r3 = r3.booleanValue()     // Catch:{ all -> 0x01bf }
            if (r3 == 0) goto L_0x0085
            com.applovin.impl.adview.n$1 r3 = new com.applovin.impl.adview.n$1     // Catch:{ all -> 0x01bf }
            r3.<init>(r2)     // Catch:{ all -> 0x01bf }
            r9.R = r3     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r3 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.AppLovinBroadcastManager r3 = r3.I()     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.AppLovinBroadcastManager$Receiver r4 = r9.R     // Catch:{ all -> 0x01bf }
            android.content.IntentFilter r5 = new android.content.IntentFilter     // Catch:{ all -> 0x01bf }
            java.lang.String r6 = "com.applovin.app_killed"
            r5.<init>(r6)     // Catch:{ all -> 0x01bf }
            r3.registerReceiver(r4, r5)     // Catch:{ all -> 0x01bf }
        L_0x0085:
            if (r2 == 0) goto L_0x01a2
            com.applovin.impl.sdk.l r3 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.c.b<java.lang.Integer> r4 = com.applovin.impl.sdk.c.b.T3     // Catch:{ all -> 0x01bf }
            java.lang.Object r3 = r3.a(r4)     // Catch:{ all -> 0x01bf }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ all -> 0x01bf }
            int r3 = r3.intValue()     // Catch:{ all -> 0x01bf }
            r4 = -1
            if (r3 == r4) goto L_0x00ba
            java.lang.String r5 = "activity"
            java.lang.Object r5 = r9.getSystemService(r5)     // Catch:{ all -> 0x01bf }
            android.app.ActivityManager r5 = (android.app.ActivityManager) r5     // Catch:{ all -> 0x01bf }
            if (r5 == 0) goto L_0x00ba
            android.app.ActivityManager$MemoryInfo r6 = new android.app.ActivityManager$MemoryInfo     // Catch:{ all -> 0x01bf }
            r6.<init>()     // Catch:{ all -> 0x01bf }
            r5.getMemoryInfo(r6)     // Catch:{ all -> 0x01bf }
            long r5 = r6.availMem     // Catch:{ all -> 0x01bf }
            long r7 = (long) r3     // Catch:{ all -> 0x01bf }
            int r3 = (r5 > r7 ? 1 : (r5 == r7 ? 0 : -1))
            if (r3 >= 0) goto L_0x00ba
            java.lang.String r10 = "Not enough available memory"
            r9.exitWithError(r10)     // Catch:{ all -> 0x01bf }
            android.os.StrictMode.setThreadPolicy(r1)
            return
        L_0x00ba:
            com.applovin.impl.sdk.d.d r3 = new com.applovin.impl.sdk.d.d     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r5 = r9.sdk     // Catch:{ all -> 0x01bf }
            r3.<init>(r2, r5)     // Catch:{ all -> 0x01bf }
            r9.c = r3     // Catch:{ all -> 0x01bf }
            boolean r3 = r2.S()     // Catch:{ all -> 0x01bf }
            if (r3 == 0) goto L_0x00d2
            com.applovin.impl.sdk.l r3 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.g r3 = r3.H()     // Catch:{ all -> 0x01bf }
            r3.a((com.applovin.impl.sdk.g.a) r9)     // Catch:{ all -> 0x01bf }
        L_0x00d2:
            r3 = 16908290(0x1020002, float:2.3877235E-38)
            android.view.View r3 = r9.findViewById(r3)     // Catch:{ all -> 0x01bf }
            if (r3 == 0) goto L_0x00ee
            boolean r5 = r2.hasVideoUrl()     // Catch:{ all -> 0x01bf }
            if (r5 == 0) goto L_0x00e7
            r5 = -16777216(0xffffffffff000000, float:-1.7014118E38)
            r3.setBackgroundColor(r5)     // Catch:{ all -> 0x01bf }
            goto L_0x00ee
        L_0x00e7:
            int r5 = r2.f()     // Catch:{ all -> 0x01bf }
            r3.setBackgroundColor(r5)     // Catch:{ all -> 0x01bf }
        L_0x00ee:
            long r5 = java.lang.System.currentTimeMillis()     // Catch:{ all -> 0x01bf }
            r9.o = r5     // Catch:{ all -> 0x01bf }
            android.view.Window r3 = r9.getWindow()     // Catch:{ all -> 0x01bf }
            r5 = 16777216(0x1000000, float:2.3509887E-38)
            r3.setFlags(r5, r5)     // Catch:{ all -> 0x01bf }
            int r3 = com.applovin.impl.sdk.utils.r.d((android.content.Context) r9)     // Catch:{ all -> 0x01bf }
            boolean r5 = com.applovin.sdk.AppLovinSdkUtils.isTablet(r9)     // Catch:{ all -> 0x01bf }
            int r6 = r9.a((int) r3, (boolean) r5)     // Catch:{ all -> 0x01bf }
            if (r10 != 0) goto L_0x010e
            r9.u = r6     // Catch:{ all -> 0x01bf }
            goto L_0x0116
        L_0x010e:
            java.lang.String r7 = "original_orientation"
            int r10 = r10.getInt(r7, r6)     // Catch:{ all -> 0x01bf }
            r9.u = r10     // Catch:{ all -> 0x01bf }
        L_0x0116:
            boolean r10 = r2.c()     // Catch:{ all -> 0x01bf }
            if (r10 == 0) goto L_0x0143
            if (r6 == r4) goto L_0x0138
            com.applovin.impl.sdk.r r10 = r9.logger     // Catch:{ all -> 0x01bf }
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bf }
            r2.<init>()     // Catch:{ all -> 0x01bf }
            java.lang.String r3 = "Locking activity orientation to current orientation: "
            r2.append(r3)     // Catch:{ all -> 0x01bf }
            r2.append(r6)     // Catch:{ all -> 0x01bf }
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x01bf }
            r10.b(r0, r2)     // Catch:{ all -> 0x01bf }
            r9.b((int) r6)     // Catch:{ all -> 0x01bf }
            goto L_0x014b
        L_0x0138:
            com.applovin.impl.sdk.r r10 = r9.logger     // Catch:{ all -> 0x01bf }
            java.lang.String r2 = "Unable to detect current orientation. Locking to targeted orientation..."
            r10.e(r0, r2)     // Catch:{ all -> 0x01bf }
        L_0x013f:
            r9.b((int) r3, (boolean) r5)     // Catch:{ all -> 0x01bf }
            goto L_0x014b
        L_0x0143:
            com.applovin.impl.sdk.r r10 = r9.logger     // Catch:{ all -> 0x01bf }
            java.lang.String r2 = "Locking activity orientation to targeted orientation..."
            r10.b(r0, r2)     // Catch:{ all -> 0x01bf }
            goto L_0x013f
        L_0x014b:
            com.applovin.impl.adview.m r10 = new com.applovin.impl.adview.m     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r2 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.sdk.AppLovinSdk r2 = r2.v()     // Catch:{ all -> 0x01bf }
            com.applovin.sdk.AppLovinAdSize r3 = com.applovin.sdk.AppLovinAdSize.INTERSTITIAL     // Catch:{ all -> 0x01bf }
            r10.<init>(r2, r3, r9)     // Catch:{ all -> 0x01bf }
            r9.f1545a = r10     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.m r10 = r9.f1545a     // Catch:{ all -> 0x01bf }
            com.applovin.adview.AdViewController r10 = r10.getAdViewController()     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.AdViewControllerImpl r10 = (com.applovin.impl.adview.AdViewControllerImpl) r10     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.d.d r2 = r9.c     // Catch:{ all -> 0x01bf }
            r10.setStatsManagerHelper(r2)     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.o r10 = r9.b     // Catch:{ all -> 0x01bf }
            r10.a((com.applovin.impl.adview.j) r9)     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r10 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.b.N1     // Catch:{ all -> 0x01bf }
            java.lang.Object r10 = r10.a(r2)     // Catch:{ all -> 0x01bf }
            java.lang.Boolean r10 = (java.lang.Boolean) r10     // Catch:{ all -> 0x01bf }
            boolean r10 = r10.booleanValue()     // Catch:{ all -> 0x01bf }
            r9.k = r10     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.b.b r10 = new com.applovin.impl.sdk.b.b     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r2 = r9.sdk     // Catch:{ all -> 0x01bf }
            r10.<init>(r9, r2)     // Catch:{ all -> 0x01bf }
            r9.K = r10     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.b.b r10 = r9.K     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.n$12 r2 = new com.applovin.impl.adview.n$12     // Catch:{ all -> 0x01bf }
            r2.<init>()     // Catch:{ all -> 0x01bf }
            r10.a((com.applovin.impl.sdk.b.b.a) r2)     // Catch:{ all -> 0x01bf }
            com.applovin.impl.adview.n$23 r10 = new com.applovin.impl.adview.n$23     // Catch:{ all -> 0x01bf }
            r10.<init>()     // Catch:{ all -> 0x01bf }
            r9.Q = r10     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.l r10 = r9.sdk     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.a r10 = r10.D()     // Catch:{ all -> 0x01bf }
            com.applovin.impl.sdk.utils.a r2 = r9.Q     // Catch:{ all -> 0x01bf }
            r10.a(r2)     // Catch:{ all -> 0x01bf }
            goto L_0x01d0
        L_0x01a2:
            java.lang.String r10 = "No current ad found."
        L_0x01a4:
            r9.exitWithError(r10)     // Catch:{ all -> 0x01bf }
            goto L_0x01d0
        L_0x01a8:
            java.lang.StringBuilder r10 = new java.lang.StringBuilder     // Catch:{ all -> 0x01bf }
            r10.<init>()     // Catch:{ all -> 0x01bf }
            java.lang.String r2 = "Wrapper is null; initialized state: "
            r10.append(r2)     // Catch:{ all -> 0x01bf }
            boolean r2 = com.applovin.impl.adview.o.l     // Catch:{ all -> 0x01bf }
            r10.append(r2)     // Catch:{ all -> 0x01bf }
            java.lang.String r10 = r10.toString()     // Catch:{ all -> 0x01bf }
            goto L_0x01a4
        L_0x01bc:
            java.lang.String r10 = "Wrapper ID is null"
            goto L_0x01a4
        L_0x01bf:
            r10 = move-exception
            com.applovin.impl.sdk.r r2 = r9.logger     // Catch:{ all -> 0x01e1 }
            if (r2 == 0) goto L_0x01cb
            com.applovin.impl.sdk.r r2 = r9.logger     // Catch:{ all -> 0x01e1 }
            java.lang.String r3 = "Encountered error during onCreate."
            r2.b(r0, r3, r10)     // Catch:{ all -> 0x01e1 }
        L_0x01cb:
            java.lang.String r10 = "An error was encountered during interstitial ad creation."
            r9.exitWithError(r10)     // Catch:{ all -> 0x01e1 }
        L_0x01d0:
            android.os.StrictMode.setThreadPolicy(r1)
            r9.w()
            com.applovin.impl.sdk.d.d r10 = r9.c
            if (r10 == 0) goto L_0x01dd
            r10.a()
        L_0x01dd:
            r9.j()
            return
        L_0x01e1:
            r10 = move-exception
            android.os.StrictMode.setThreadPolicy(r1)
            throw r10
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.n.onCreate(android.os.Bundle):void");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0064, code lost:
        if (r4.currentAd != null) goto L_0x0079;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x0077, code lost:
        if (r4.currentAd == null) goto L_0x0081;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0079, code lost:
        p();
        c((com.applovin.sdk.AppLovinAd) r4.currentAd);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0081, code lost:
        super.onDestroy();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0084, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onDestroy() {
        /*
            r4 = this;
            com.applovin.impl.adview.m r0 = r4.f1545a     // Catch:{ all -> 0x0067 }
            r1 = 0
            if (r0 == 0) goto L_0x001d
            com.applovin.impl.adview.m r0 = r4.f1545a     // Catch:{ all -> 0x0067 }
            android.view.ViewParent r0 = r0.getParent()     // Catch:{ all -> 0x0067 }
            boolean r2 = r0 instanceof android.view.ViewGroup     // Catch:{ all -> 0x0067 }
            if (r2 == 0) goto L_0x0016
            android.view.ViewGroup r0 = (android.view.ViewGroup) r0     // Catch:{ all -> 0x0067 }
            com.applovin.impl.adview.m r2 = r4.f1545a     // Catch:{ all -> 0x0067 }
            r0.removeView(r2)     // Catch:{ all -> 0x0067 }
        L_0x0016:
            com.applovin.impl.adview.m r0 = r4.f1545a     // Catch:{ all -> 0x0067 }
            r0.destroy()     // Catch:{ all -> 0x0067 }
            r4.f1545a = r1     // Catch:{ all -> 0x0067 }
        L_0x001d:
            com.applovin.impl.adview.AppLovinVideoView r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x002b
            com.applovin.impl.adview.AppLovinVideoView r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            r0.pause()     // Catch:{ all -> 0x0067 }
            com.applovin.impl.adview.AppLovinVideoView r0 = r4.videoView     // Catch:{ all -> 0x0067 }
            r0.stopPlayback()     // Catch:{ all -> 0x0067 }
        L_0x002b:
            com.applovin.impl.sdk.l r0 = r4.sdk     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0047
            java.lang.ref.WeakReference<android.media.MediaPlayer> r0 = r4.J     // Catch:{ all -> 0x0067 }
            java.lang.Object r0 = r0.get()     // Catch:{ all -> 0x0067 }
            android.media.MediaPlayer r0 = (android.media.MediaPlayer) r0     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x003c
            r0.release()     // Catch:{ all -> 0x0067 }
        L_0x003c:
            com.applovin.impl.sdk.l r0 = r4.sdk     // Catch:{ all -> 0x0067 }
            com.applovin.impl.sdk.a r0 = r0.D()     // Catch:{ all -> 0x0067 }
            com.applovin.impl.sdk.utils.a r2 = r4.Q     // Catch:{ all -> 0x0067 }
            r0.b(r2)     // Catch:{ all -> 0x0067 }
        L_0x0047:
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0050
            com.applovin.impl.adview.i r0 = r4.countdownManager     // Catch:{ all -> 0x0067 }
            r0.b()     // Catch:{ all -> 0x0067 }
        L_0x0050:
            android.os.Handler r0 = r4.B     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0059
            android.os.Handler r0 = r4.B     // Catch:{ all -> 0x0067 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ all -> 0x0067 }
        L_0x0059:
            android.os.Handler r0 = r4.A     // Catch:{ all -> 0x0067 }
            if (r0 == 0) goto L_0x0062
            android.os.Handler r0 = r4.A     // Catch:{ all -> 0x0067 }
            r0.removeCallbacksAndMessages(r1)     // Catch:{ all -> 0x0067 }
        L_0x0062:
            com.applovin.impl.sdk.a.g r0 = r4.currentAd
            if (r0 == 0) goto L_0x0081
            goto L_0x0079
        L_0x0067:
            r0 = move-exception
            com.applovin.impl.sdk.r r1 = r4.logger     // Catch:{ all -> 0x0085 }
            if (r1 == 0) goto L_0x0075
            com.applovin.impl.sdk.r r1 = r4.logger     // Catch:{ all -> 0x0085 }
            java.lang.String r2 = "InterActivity"
            java.lang.String r3 = "Unable to destroy video view"
            r1.a((java.lang.String) r2, (java.lang.String) r3, (java.lang.Throwable) r0)     // Catch:{ all -> 0x0085 }
        L_0x0075:
            com.applovin.impl.sdk.a.g r0 = r4.currentAd
            if (r0 == 0) goto L_0x0081
        L_0x0079:
            r4.p()
            com.applovin.impl.sdk.a.g r0 = r4.currentAd
            r4.c((com.applovin.sdk.AppLovinAd) r0)
        L_0x0081:
            super.onDestroy()
            return
        L_0x0085:
            r0 = move-exception
            com.applovin.impl.sdk.a.g r1 = r4.currentAd
            if (r1 == 0) goto L_0x0092
            r4.p()
            com.applovin.impl.sdk.a.g r1 = r4.currentAd
            r4.c((com.applovin.sdk.AppLovinAd) r1)
        L_0x0092:
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.n.onDestroy():void");
    }

    /* access modifiers changed from: protected */
    public void onPause() {
        super.onPause();
        this.logger.b("InterActivity", "App paused...");
        this.p = System.currentTimeMillis();
        if (this.postitialWasDisplayed) {
            m();
        }
        this.K.a();
        pauseReportRewardTask();
        b("javascript:al_onAppPaused();");
    }

    /* access modifiers changed from: protected */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0065, code lost:
        if (r0 != null) goto L_0x0097;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0095, code lost:
        if (r2 == false) goto L_0x0097;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onResume() {
        /*
            r5 = this;
            super.onResume()     // Catch:{ IllegalArgumentException -> 0x00b7 }
            com.applovin.impl.sdk.r r0 = r5.logger
            java.lang.String r1 = "InterActivity"
            java.lang.String r2 = "App resumed..."
            r0.b(r1, r2)
            boolean r0 = r5.m
            if (r0 != 0) goto L_0x009e
            com.applovin.impl.sdk.d.d r0 = r5.c
            if (r0 == 0) goto L_0x001e
            long r1 = java.lang.System.currentTimeMillis()
            long r3 = r5.p
            long r1 = r1 - r3
            r0.d(r1)
        L_0x001e:
            com.applovin.impl.sdk.l r0 = r5.sdk
            com.applovin.impl.sdk.c.d<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.d.w
            r2 = 0
            java.lang.Boolean r3 = java.lang.Boolean.valueOf(r2)
            java.lang.Object r0 = r0.b(r1, r3)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            r3 = 0
            if (r0 == 0) goto L_0x0068
            com.applovin.impl.sdk.b.b r0 = r5.K
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x0068
            boolean r0 = r5.postitialWasDisplayed
            if (r0 != 0) goto L_0x0068
            r5.n()
            r5.O()
            com.applovin.impl.sdk.a.g r0 = r5.currentAd
            if (r0 == 0) goto L_0x009a
            com.applovin.impl.sdk.l r0 = r5.sdk
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.k1
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x009a
            boolean r0 = r5.postitialWasDisplayed
            if (r0 != 0) goto L_0x009a
            boolean r0 = r5.k
            if (r0 == 0) goto L_0x009a
            com.applovin.impl.adview.h r0 = r5.F
            if (r0 == 0) goto L_0x009a
            goto L_0x0097
        L_0x0068:
            com.applovin.impl.sdk.a.g r0 = r5.currentAd
            boolean r0 = r0 instanceof com.applovin.impl.sdk.a.a
            if (r0 == 0) goto L_0x0079
            com.applovin.impl.sdk.a.g r0 = r5.currentAd
            com.applovin.impl.sdk.a.a r0 = (com.applovin.impl.sdk.a.a) r0
            boolean r0 = r0.G0()
            if (r0 == 0) goto L_0x0079
            r2 = 1
        L_0x0079:
            com.applovin.impl.sdk.a.g r0 = r5.currentAd
            if (r0 == 0) goto L_0x009a
            com.applovin.impl.sdk.l r0 = r5.sdk
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.k1
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x009a
            boolean r0 = r5.postitialWasDisplayed
            if (r0 == 0) goto L_0x009a
            com.applovin.impl.adview.h r0 = r5.D
            if (r0 == 0) goto L_0x009a
            if (r2 != 0) goto L_0x009a
        L_0x0097:
            r5.a((long) r3, (com.applovin.impl.adview.h) r0)
        L_0x009a:
            r5.resumeReportRewardTask()
            goto L_0x00b1
        L_0x009e:
            com.applovin.impl.sdk.b.b r0 = r5.K
            boolean r0 = r0.d()
            if (r0 != 0) goto L_0x00b1
            boolean r0 = r5.postitialWasDisplayed
            if (r0 != 0) goto L_0x00b1
            com.applovin.impl.sdk.a.g r0 = r5.currentAd
            if (r0 == 0) goto L_0x00b1
            r5.O()
        L_0x00b1:
            java.lang.String r0 = "javascript:al_onAppResumed();"
            r5.b((java.lang.String) r0)
            return
        L_0x00b7:
            java.lang.String r0 = "Error was encountered in onResume()."
            r5.exitWithError(r0)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.n.onResume():void");
    }

    public void onRingerModeChanged(int i2) {
        String str;
        if (this.y != g.h) {
            this.z = true;
        }
        c adWebView = ((AdViewControllerImpl) this.f1545a.getAdViewController()).getAdWebView();
        if (adWebView != null) {
            if (g.a(i2) && !g.a(this.y)) {
                str = "javascript:al_muteSwitchOn();";
            } else if (i2 == 2) {
                str = "javascript:al_muteSwitchOff();";
            }
            adWebView.a(str);
        }
        this.y = i2;
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putBoolean("instance_impression_tracked", this.w.get());
        bundle.putInt("original_orientation", this.u);
    }

    public void onWindowFocusChanged(boolean z2) {
        String str;
        super.onWindowFocusChanged(z2);
        l lVar = this.sdk;
        if (z2) {
            if (lVar != null) {
                this.logger.b("InterActivity", "Window gained focus");
                try {
                    if (!com.applovin.impl.sdk.utils.g.c() || !((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.I1)).booleanValue() || !q()) {
                        getWindow().setFlags(ByteConstants.KB, ByteConstants.KB);
                    } else {
                        r();
                        this.B.postDelayed(new Runnable() {
                            public void run() {
                                n.this.r();
                            }
                        }, 2500);
                    }
                    if (!this.postitialWasDisplayed) {
                        n();
                        resumeReportRewardTask();
                    }
                } catch (Throwable th) {
                    this.logger.b("InterActivity", "Setting window flags failed.", th);
                }
                this.m = false;
                b("javascript:al_onWindowFocusChanged( " + z2 + " );");
            }
            str = "Window gained focus. SDK is null.";
        } else if (lVar != null) {
            this.logger.b("InterActivity", "Window lost focus");
            if (!this.postitialWasDisplayed) {
                m();
                pauseReportRewardTask();
            }
            this.m = false;
            b("javascript:al_onWindowFocusChanged( " + z2 + " );");
        } else {
            str = "Window lost focus. SDK is null.";
        }
        r.f("InterActivity", str);
        this.m = false;
        b("javascript:al_onWindowFocusChanged( " + z2 + " );");
    }

    public void pauseReportRewardTask() {
        p pVar = this.P;
        if (pVar != null) {
            pVar.b();
        }
    }

    /* access modifiers changed from: protected */
    public void playVideo() {
        d((AppLovinAd) this.currentAd);
        this.videoView.start();
        this.countdownManager.a();
    }

    public void resumeReportRewardTask() {
        p pVar = this.P;
        if (pVar != null) {
            pVar.c();
        }
    }

    /* access modifiers changed from: protected */
    public boolean shouldContinueFullLengthVideoCountdown() {
        return !this.d && !this.postitialWasDisplayed;
    }

    public void showPostitial() {
        try {
            k();
            if (this.f1545a != null) {
                ViewParent parent = this.f1545a.getParent();
                if ((parent instanceof ViewGroup) && (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.K3)).booleanValue() || parent != this.C)) {
                    ((ViewGroup) parent).removeView(this.f1545a);
                }
                FrameLayout frameLayout = ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.K3)).booleanValue() ? this.C : new FrameLayout(this);
                frameLayout.setLayoutParams(new FrameLayout.LayoutParams(-1, -1));
                frameLayout.setBackgroundColor(this.currentAd.f());
                if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.K3)).booleanValue()) {
                    this.f1545a.setVisibility(0);
                } else {
                    frameLayout.addView(this.f1545a);
                }
                if (this.C != null) {
                    if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.K3)).booleanValue()) {
                        com.applovin.impl.sdk.utils.b.a(this.C, this.f1545a);
                    } else {
                        this.C.removeAllViewsInLayout();
                    }
                }
                if (D() && this.E != null) {
                    if (this.E.getParent() instanceof ViewGroup) {
                        ((ViewGroup) this.E.getParent()).removeView(this.E);
                    }
                    frameLayout.addView(this.E);
                    this.E.bringToFront();
                }
                if (this.D != null) {
                    ViewParent parent2 = this.D.getParent();
                    if (parent2 instanceof ViewGroup) {
                        ((ViewGroup) parent2).removeView(this.D);
                    }
                    frameLayout.addView(this.D);
                    this.D.bringToFront();
                }
                if (!((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.K3)).booleanValue()) {
                    setContentView(frameLayout);
                }
                if (((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.H3)).booleanValue()) {
                    this.f1545a.setVisibility(4);
                    this.f1545a.setVisibility(0);
                }
                a("javascript:al_onPoststitialShow();", (long) this.currentAd.p());
            }
            if ((this.currentAd instanceof com.applovin.impl.sdk.a.a) && ((com.applovin.impl.sdk.a.a) this.currentAd).G0()) {
                this.logger.b("InterActivity", "Skip showing of close button");
            } else if (this.currentAd.t0() >= 0) {
                a(com.applovin.impl.sdk.utils.r.b((float) this.currentAd.t0()), this.D);
            } else if (this.currentAd.t0() == -2) {
                this.D.setVisibility(0);
            } else {
                a(0, this.D);
            }
            this.postitialWasDisplayed = true;
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Encountered error while showing postitial. Dismissing...", th);
            dismiss();
        }
    }

    public void skipVideo() {
        this.s = SystemClock.elapsedRealtime() - this.r;
        d dVar = this.c;
        if (dVar != null) {
            dVar.f();
        }
        if (this.currentAd.w0()) {
            dismiss();
        } else {
            showPostitial();
        }
    }

    public void toggleMute() {
        boolean z2 = !l();
        try {
            c(z2);
            a(z2);
            b(z2);
        } catch (Throwable th) {
            this.logger.b("InterActivity", "Unable to set volume to " + z2, th);
        }
    }
}
