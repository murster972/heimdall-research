package com.applovin.impl.adview;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.net.Uri;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import com.applovin.adview.AdViewController;
import com.applovin.adview.AppLovinAdView;
import com.applovin.adview.AppLovinAdViewDisplayErrorCode;
import com.applovin.adview.AppLovinAdViewEventListener;
import com.applovin.communicator.AppLovinCommunicator;
import com.applovin.communicator.AppLovinCommunicatorMessage;
import com.applovin.communicator.AppLovinCommunicatorSubscriber;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.k;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import com.facebook.ads.AudienceNetworkActivity;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class AdViewControllerImpl implements AdViewController, AppLovinCommunicatorSubscriber {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public Context f1451a;
    /* access modifiers changed from: private */
    public ViewGroup b;
    /* access modifiers changed from: private */
    public l c;
    /* access modifiers changed from: private */
    public AppLovinAdServiceImpl d;
    /* access modifiers changed from: private */
    public r e;
    /* access modifiers changed from: private */
    public AppLovinAdSize f;
    private String g;
    /* access modifiers changed from: private */
    public d h;
    private d i;
    private c j;
    /* access modifiers changed from: private */
    public c k;
    private Runnable l;
    private Runnable m;
    /* access modifiers changed from: private */
    public volatile g n = null;
    private volatile AppLovinAd o = null;
    /* access modifiers changed from: private */
    public k p = null;
    /* access modifiers changed from: private */
    public k q = null;
    private final AtomicReference<AppLovinAd> r = new AtomicReference<>();
    /* access modifiers changed from: private */
    public final AtomicBoolean s = new AtomicBoolean();
    private volatile boolean t = false;
    /* access modifiers changed from: private */
    public volatile boolean u = false;
    /* access modifiers changed from: private */
    public volatile AppLovinAdLoadListener v;
    private volatile AppLovinAdDisplayListener w;
    /* access modifiers changed from: private */
    public volatile AppLovinAdViewEventListener x;
    private volatile AppLovinAdClickListener y;

    private class a implements Runnable {
        private a() {
        }

        public void run() {
            if (AdViewControllerImpl.this.k != null) {
                AdViewControllerImpl.this.k.setVisibility(8);
            }
        }
    }

    private class b implements Runnable {
        private b() {
        }

        public void run() {
            if (AdViewControllerImpl.this.n == null) {
                return;
            }
            if (AdViewControllerImpl.this.k != null) {
                AdViewControllerImpl.this.f();
                r c = AdViewControllerImpl.this.e;
                c.b("AppLovinAdView", "Rendering advertisement ad for #" + AdViewControllerImpl.this.n.getAdIdNumber() + "...");
                AdViewControllerImpl.b((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.n.getSize());
                AdViewControllerImpl.this.k.a(AdViewControllerImpl.this.n);
                if (AdViewControllerImpl.this.n.getSize() != AppLovinAdSize.INTERSTITIAL && !AdViewControllerImpl.this.u) {
                    AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                    d unused = adViewControllerImpl.h = new d(adViewControllerImpl.n, AdViewControllerImpl.this.c);
                    AdViewControllerImpl.this.h.a();
                    AdViewControllerImpl.this.k.setStatsManagerHelper(AdViewControllerImpl.this.h);
                    AdViewControllerImpl.this.n.setHasShown(true);
                }
                if (AdViewControllerImpl.this.k.getStatsManagerHelper() != null) {
                    AdViewControllerImpl.this.k.getStatsManagerHelper().a(AdViewControllerImpl.this.n.y0() ? 0 : 1);
                    return;
                }
                return;
            }
            r.i("AppLovinAdView", "Unable to render advertisement for ad #" + AdViewControllerImpl.this.n.getAdIdNumber() + ". Please make sure you are not calling AppLovinAdView.destroy() prematurely.");
            k.a(AdViewControllerImpl.this.x, (AppLovinAd) AdViewControllerImpl.this.n, (AppLovinAdView) null, AppLovinAdViewDisplayErrorCode.WEBVIEW_NOT_FOUND);
        }
    }

    static class c implements AppLovinAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        private final AdViewControllerImpl f1464a;

        c(AdViewControllerImpl adViewControllerImpl, l lVar) {
            if (adViewControllerImpl == null) {
                throw new IllegalArgumentException("No view specified");
            } else if (lVar != null) {
                this.f1464a = adViewControllerImpl;
            } else {
                throw new IllegalArgumentException("No sdk specified");
            }
        }

        private AdViewControllerImpl a() {
            return this.f1464a;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(appLovinAd);
            } else {
                r.i("AppLovinAdView", "Ad view has been garbage collected by the time an ad was received");
            }
        }

        public void failedToReceiveAd(int i) {
            AdViewControllerImpl a2 = a();
            if (a2 != null) {
                a2.a(i);
            }
        }
    }

    private void a(AppLovinAdView appLovinAdView, l lVar, AppLovinAdSize appLovinAdSize, String str, Context context) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (lVar == null) {
            throw new IllegalArgumentException("No sdk specified");
        } else if (appLovinAdSize != null) {
            this.c = lVar;
            this.d = lVar.c0();
            this.e = lVar.j0();
            AppLovinCommunicator.getInstance(context);
            this.f = appLovinAdSize;
            this.g = str;
            this.f1451a = context;
            this.b = appLovinAdView;
            this.i = new d(this, lVar);
            this.m = new a();
            this.l = new b();
            this.j = new c(this, lVar);
            attachNewAdView(appLovinAdSize);
        } else {
            throw new IllegalArgumentException("No ad size specified");
        }
    }

    private void a(Runnable runnable) {
        AppLovinSdkUtils.runOnUiThread(runnable);
    }

    private void b() {
        r rVar = this.e;
        if (rVar != null) {
            rVar.b("AppLovinAdView", "Destroying...");
        }
        c cVar = this.k;
        if (cVar != null) {
            ViewParent parent = cVar.getParent();
            if (parent instanceof ViewGroup) {
                ((ViewGroup) parent).removeView(this.k);
            }
            this.k.removeAllViews();
            this.k.loadUrl("about:blank");
            this.k.onPause();
            this.k.destroyDrawingCache();
            this.k.destroy();
            this.k = null;
            this.c.J().b(this.n);
        }
        this.u = true;
    }

    /* access modifiers changed from: private */
    public static void b(View view, AppLovinAdSize appLovinAdSize) {
        if (view != null) {
            DisplayMetrics displayMetrics = view.getResources().getDisplayMetrics();
            int i2 = -1;
            int applyDimension = appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel()) ? -1 : appLovinAdSize.getWidth() == -1 ? displayMetrics.widthPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getWidth(), displayMetrics);
            if (!appLovinAdSize.getLabel().equals(AppLovinAdSize.INTERSTITIAL.getLabel())) {
                i2 = appLovinAdSize.getHeight() == -1 ? displayMetrics.heightPixels : (int) TypedValue.applyDimension(1, (float) appLovinAdSize.getHeight(), displayMetrics);
            }
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            if (layoutParams == null) {
                layoutParams = new RelativeLayout.LayoutParams(-2, -2);
            }
            layoutParams.width = applyDimension;
            layoutParams.height = i2;
            if (layoutParams instanceof RelativeLayout.LayoutParams) {
                RelativeLayout.LayoutParams layoutParams2 = (RelativeLayout.LayoutParams) layoutParams;
                layoutParams2.addRule(10);
                layoutParams2.addRule(9);
            }
            view.setLayoutParams(layoutParams);
        }
    }

    private void c() {
        a((Runnable) new Runnable() {
            public void run() {
                if (AdViewControllerImpl.this.p != null) {
                    r c = AdViewControllerImpl.this.e;
                    c.b("AppLovinAdView", "Detaching expanded ad: " + AdViewControllerImpl.this.p.a());
                    AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                    k unused = adViewControllerImpl.q = adViewControllerImpl.p;
                    k unused2 = AdViewControllerImpl.this.p = null;
                    AdViewControllerImpl adViewControllerImpl2 = AdViewControllerImpl.this;
                    adViewControllerImpl2.attachNewAdView(adViewControllerImpl2.f);
                }
            }
        });
    }

    /* access modifiers changed from: private */
    public void d() {
        a((Runnable) new Runnable() {
            public void run() {
                com.applovin.impl.sdk.a.a aVar;
                if (AdViewControllerImpl.this.q != null || AdViewControllerImpl.this.p != null) {
                    if (AdViewControllerImpl.this.q != null) {
                        aVar = AdViewControllerImpl.this.q.a();
                        AdViewControllerImpl.this.q.dismiss();
                        k unused = AdViewControllerImpl.this.q = null;
                    } else {
                        aVar = AdViewControllerImpl.this.p.a();
                        AdViewControllerImpl.this.p.dismiss();
                        k unused2 = AdViewControllerImpl.this.p = null;
                    }
                    k.b(AdViewControllerImpl.this.x, (AppLovinAd) aVar, (AppLovinAdView) AdViewControllerImpl.this.b);
                }
            }
        });
    }

    private void e() {
        d dVar = this.h;
        if (dVar != null) {
            dVar.c();
            this.h = null;
        }
    }

    /* access modifiers changed from: private */
    public void f() {
        g gVar = this.n;
        com.applovin.impl.sdk.utils.l lVar = new com.applovin.impl.sdk.utils.l();
        lVar.a().a(gVar).a(getParentView());
        if (!com.applovin.impl.sdk.utils.r.a(gVar.getSize())) {
            lVar.a().a("Fullscreen Ad Properties").b(gVar);
        }
        lVar.a(this.c);
        lVar.a();
        r.f("AppLovinAdView", lVar.toString());
    }

    /* access modifiers changed from: package-private */
    public void a() {
        if (this.p == null && this.q == null) {
            r rVar = this.e;
            rVar.b("AppLovinAdView", "Ad: " + this.n + " closed.");
            a(this.m);
            k.b(this.w, (AppLovinAd) this.n);
            this.c.J().b(this.n);
            this.n = null;
            return;
        }
        contractAd();
    }

    /* access modifiers changed from: package-private */
    public void a(final int i2) {
        if (!this.u) {
            a(this.m);
        }
        a((Runnable) new Runnable() {
            public void run() {
                try {
                    if (AdViewControllerImpl.this.v != null) {
                        AdViewControllerImpl.this.v.failedToReceiveAd(i2);
                    }
                } catch (Throwable th) {
                    r.c("AppLovinAdView", "Exception while running app load  callback", th);
                }
            }
        });
    }

    /* access modifiers changed from: package-private */
    public void a(g gVar, AppLovinAdView appLovinAdView, Uri uri, PointF pointF) {
        k.a(this.y, (AppLovinAd) gVar);
        if (appLovinAdView != null) {
            this.d.trackAndLaunchClick(gVar, appLovinAdView, this, uri, pointF);
        } else {
            this.e.e("AppLovinAdView", "Unable to process ad click - AppLovinAdView destroyed prematurely");
        }
    }

    /* access modifiers changed from: package-private */
    public void a(final AppLovinAd appLovinAd) {
        if (appLovinAd != null) {
            if (!this.u) {
                renderAd(appLovinAd);
            } else {
                this.r.set(appLovinAd);
                this.e.b("AppLovinAdView", "Ad view has paused when an ad was received, ad saved for later");
            }
            a((Runnable) new Runnable() {
                public void run() {
                    if (AdViewControllerImpl.this.s.compareAndSet(true, false)) {
                        AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                        adViewControllerImpl.attachNewAdView(adViewControllerImpl.f);
                    }
                    try {
                        if (AdViewControllerImpl.this.v != null) {
                            AdViewControllerImpl.this.v.adReceived(appLovinAd);
                        }
                    } catch (Throwable th) {
                        r.i("AppLovinAdView", "Exception while running ad load callback: " + th.getMessage());
                    }
                }
            });
            return;
        }
        this.e.e("AppLovinAdView", "No provided when to the view controller");
        a(-1);
    }

    /* access modifiers changed from: protected */
    public void attachNewAdView(AppLovinAdSize appLovinAdSize) {
        try {
            this.k = new c(this.i, this.c, this.f1451a);
            this.k.setBackgroundColor(0);
            this.k.setWillNotCacheDrawing(false);
            this.b.setBackgroundColor(0);
            this.b.addView(this.k);
            b((View) this.k, appLovinAdSize);
            if (!this.t) {
                a(this.m);
            }
            a((Runnable) new Runnable() {
                public void run() {
                    AdViewControllerImpl.this.k.loadDataWithBaseURL("/", "<html></html>", AudienceNetworkActivity.WEBVIEW_MIME_TYPE, (String) null, "");
                }
            });
            this.t = true;
        } catch (Throwable th) {
            r.c("AppLovinAdView", "Failed to initialize AdWebView", th);
            this.s.set(true);
        }
    }

    public void contractAd() {
        a((Runnable) new Runnable() {
            public void run() {
                AdViewControllerImpl.this.d();
                if (AdViewControllerImpl.this.b != null && AdViewControllerImpl.this.k != null && AdViewControllerImpl.this.k.getParent() == null) {
                    AdViewControllerImpl.this.b.addView(AdViewControllerImpl.this.k);
                    AdViewControllerImpl.b((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.n.getSize());
                }
            }
        });
    }

    public void destroy() {
        if (!(this.k == null || this.p == null)) {
            contractAd();
        }
        b();
    }

    public void dismissInterstitialIfRequired() {
        if ((this.f1451a instanceof j) && this.n != null) {
            if (this.n.g() == g.a.DISMISS) {
                ((j) this.f1451a).dismiss();
            }
        }
    }

    public void expandAd(final PointF pointF) {
        a((Runnable) new Runnable() {
            public void run() {
                if (AdViewControllerImpl.this.p == null && (AdViewControllerImpl.this.n instanceof com.applovin.impl.sdk.a.a) && AdViewControllerImpl.this.k != null) {
                    com.applovin.impl.sdk.a.a aVar = (com.applovin.impl.sdk.a.a) AdViewControllerImpl.this.n;
                    Activity a2 = AdViewControllerImpl.this.f1451a instanceof Activity ? (Activity) AdViewControllerImpl.this.f1451a : com.applovin.impl.sdk.utils.r.a((View) AdViewControllerImpl.this.k, AdViewControllerImpl.this.c);
                    if (a2 != null) {
                        if (AdViewControllerImpl.this.b != null) {
                            AdViewControllerImpl.this.b.removeView(AdViewControllerImpl.this.k);
                        }
                        AdViewControllerImpl adViewControllerImpl = AdViewControllerImpl.this;
                        k unused = adViewControllerImpl.p = new k(aVar, adViewControllerImpl.k, a2, AdViewControllerImpl.this.c);
                        AdViewControllerImpl.this.p.setOnDismissListener(new DialogInterface.OnDismissListener() {
                            public void onDismiss(DialogInterface dialogInterface) {
                                AdViewControllerImpl.this.contractAd();
                            }
                        });
                        AdViewControllerImpl.this.p.show();
                        k.a(AdViewControllerImpl.this.x, (AppLovinAd) AdViewControllerImpl.this.n, (AppLovinAdView) AdViewControllerImpl.this.b);
                        if (AdViewControllerImpl.this.h != null) {
                            AdViewControllerImpl.this.h.d();
                            return;
                        }
                        return;
                    }
                    r.i("AppLovinAdView", "Unable to expand ad. No Activity found.");
                    Uri E0 = aVar.E0();
                    if (E0 != null) {
                        AdViewControllerImpl.this.d.trackAndLaunchClick(aVar, AdViewControllerImpl.this.getParentView(), AdViewControllerImpl.this, E0, pointF);
                        if (AdViewControllerImpl.this.h != null) {
                            AdViewControllerImpl.this.h.b();
                        }
                    }
                    AdViewControllerImpl.this.k.a("javascript:al_onFailedExpand();");
                }
            }
        });
    }

    public AppLovinAdViewEventListener getAdViewEventListener() {
        return this.x;
    }

    public c getAdWebView() {
        return this.k;
    }

    public String getCommunicatorId() {
        return AdViewControllerImpl.class.getSimpleName();
    }

    public g getCurrentAd() {
        return this.n;
    }

    public AppLovinAdView getParentView() {
        return (AppLovinAdView) this.b;
    }

    public l getSdk() {
        return this.c;
    }

    public AppLovinAdSize getSize() {
        return this.f;
    }

    public String getZoneId() {
        return this.g;
    }

    public void initializeAdView(AppLovinAdView appLovinAdView, Context context, AppLovinAdSize appLovinAdSize, String str, AppLovinSdk appLovinSdk, AttributeSet attributeSet) {
        if (appLovinAdView == null) {
            throw new IllegalArgumentException("No parent view specified");
        } else if (context == null) {
            r.i("AppLovinAdView", "Unable to build AppLovinAdView: no context provided. Please use a different constructor for this view.");
        } else {
            if (appLovinAdSize == null && (appLovinAdSize = com.applovin.impl.sdk.utils.b.a(attributeSet)) == null) {
                appLovinAdSize = AppLovinAdSize.BANNER;
            }
            AppLovinAdSize appLovinAdSize2 = appLovinAdSize;
            if (appLovinSdk == null) {
                appLovinSdk = AppLovinSdk.getInstance(context);
            }
            if (appLovinSdk != null && !appLovinSdk.hasCriticalErrors()) {
                a(appLovinAdView, appLovinSdk.coreSdk, appLovinAdSize2, str, context);
                if (com.applovin.impl.sdk.utils.b.b(attributeSet)) {
                    loadNextAd();
                }
            }
        }
    }

    public boolean isAdReadyToDisplay() {
        return !TextUtils.isEmpty(this.g) ? this.d.hasPreloadedAdForZoneId(this.g) : this.d.hasPreloadedAd(this.f);
    }

    public void loadNextAd() {
        if (this.c == null || this.j == null || this.f1451a == null || !this.t) {
            r.g("AppLovinAdView", "Unable to load next ad: AppLovinAdView is not initialized.");
        } else {
            this.d.loadNextAd(this.g, this.f, this.j);
        }
    }

    public void onAdHtmlLoaded(final WebView webView) {
        a((Runnable) new Runnable(this) {
            public void run() {
                webView.setVisibility(0);
            }
        });
        try {
            if (this.n != this.o && this.w != null) {
                this.o = this.n;
                k.a(this.w, (AppLovinAd) this.n);
                this.c.J().a((Object) this.n);
                this.k.a("javascript:al_onAdViewRendered();");
            }
        } catch (Throwable th) {
            r.c("AppLovinAdView", "Exception while notifying ad display listener", th);
        }
    }

    public void onAttachedToWindow() {
        if (com.applovin.impl.sdk.utils.b.a((View) this.k)) {
            this.c.p().a(com.applovin.impl.sdk.d.g.p);
        }
    }

    public void onDetachedFromWindow() {
        if (this.t) {
            k.b(this.w, (AppLovinAd) this.n);
            this.c.J().b(this.n);
            if (this.k == null || this.p == null) {
                this.e.b("AppLovinAdView", "onDetachedFromWindowCalled without an expanded ad present");
                return;
            }
            this.e.b("AppLovinAdView", "onDetachedFromWindowCalled with expanded ad present");
            c();
        }
    }

    public void onMessageReceived(AppLovinCommunicatorMessage appLovinCommunicatorMessage) {
        if ("crash_applovin_ad_webview".equals(appLovinCommunicatorMessage.getTopic())) {
            a((Runnable) new Runnable() {
                public void run() {
                    AdViewControllerImpl.this.getAdWebView().loadUrl("chrome://crash");
                }
            });
        }
    }

    public void pause() {
        if (this.t && !this.u) {
            this.u = true;
        }
    }

    public void renderAd(AppLovinAd appLovinAd) {
        renderAd(appLovinAd, (String) null);
    }

    public void renderAd(AppLovinAd appLovinAd, String str) {
        if (appLovinAd != null) {
            com.applovin.impl.sdk.utils.r.b(appLovinAd, this.c);
            if (this.t) {
                g gVar = (g) com.applovin.impl.sdk.utils.r.a(appLovinAd, this.c);
                if (gVar != null && gVar != this.n) {
                    r rVar = this.e;
                    rVar.b("AppLovinAdView", "Rendering ad #" + gVar.getAdIdNumber() + " (" + gVar.getSize() + ")");
                    k.b(this.w, (AppLovinAd) this.n);
                    this.c.J().b(this.n);
                    if (gVar.getSize() != AppLovinAdSize.INTERSTITIAL) {
                        e();
                    }
                    this.r.set((Object) null);
                    this.o = null;
                    this.n = gVar;
                    if (!this.u && com.applovin.impl.sdk.utils.r.a(this.f)) {
                        this.c.c0().trackImpression(gVar);
                    }
                    if (this.p != null) {
                        c();
                    }
                    a(this.l);
                } else if (gVar == null) {
                    this.e.d("AppLovinAdView", "Unable to render ad. Ad is null. Internal inconsistency error.");
                } else {
                    r rVar2 = this.e;
                    rVar2.d("AppLovinAdView", "Ad #" + gVar.getAdIdNumber() + " is already showing, ignoring");
                    if (((Boolean) this.c.a(com.applovin.impl.sdk.c.b.b1)).booleanValue()) {
                        throw new IllegalStateException("Failed to display ad - ad can only be displayed once. Load the next ad.");
                    }
                }
            } else {
                r.g("AppLovinAdView", "Unable to render ad: AppLovinAdView is not initialized.");
            }
        } else {
            throw new IllegalArgumentException("No ad specified");
        }
    }

    public void resume() {
        if (this.t) {
            AppLovinAd andSet = this.r.getAndSet((Object) null);
            if (andSet != null) {
                renderAd(andSet);
            }
            this.u = false;
        }
    }

    public void setAdClickListener(AppLovinAdClickListener appLovinAdClickListener) {
        this.y = appLovinAdClickListener;
    }

    public void setAdDisplayListener(AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.w = appLovinAdDisplayListener;
    }

    public void setAdLoadListener(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.v = appLovinAdLoadListener;
    }

    public void setAdViewEventListener(AppLovinAdViewEventListener appLovinAdViewEventListener) {
        this.x = appLovinAdViewEventListener;
    }

    public void setStatsManagerHelper(d dVar) {
        c cVar = this.k;
        if (cVar != null) {
            cVar.setStatsManagerHelper(dVar);
        }
    }
}
