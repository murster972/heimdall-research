package com.applovin.impl.adview;

import android.annotation.SuppressLint;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.MediaController;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.util.Map;

@SuppressLint({"ViewConstructor"})
public class AppLovinVideoViewV2 extends SurfaceView implements MediaController.MediaPlayerControl {
    private final MediaPlayer.OnCompletionListener A = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mediaPlayer) {
            int unused = AppLovinVideoViewV2.this.e = 5;
            int unused2 = AppLovinVideoViewV2.this.f = 5;
            if (AppLovinVideoViewV2.this.n != null) {
                AppLovinVideoViewV2.this.n.onCompletion(AppLovinVideoViewV2.this.h);
            }
            if (AppLovinVideoViewV2.this.x != 0) {
                AppLovinVideoViewV2.this.w.abandonAudioFocus((AudioManager.OnAudioFocusChangeListener) null);
            }
        }
    };
    private final MediaPlayer.OnInfoListener B = new MediaPlayer.OnInfoListener() {
        public boolean onInfo(MediaPlayer mediaPlayer, int i, int i2) {
            if (AppLovinVideoViewV2.this.r == null) {
                return true;
            }
            AppLovinVideoViewV2.this.r.onInfo(mediaPlayer, i, i2);
            return true;
        }
    };
    private final MediaPlayer.OnErrorListener C = new MediaPlayer.OnErrorListener() {
        public boolean onError(MediaPlayer mediaPlayer, int i, int i2) {
            r a2 = AppLovinVideoViewV2.this.f1468a;
            a2.b("AppLovinVideoView", "Media player error: " + i + ", " + i2);
            int unused = AppLovinVideoViewV2.this.e = -1;
            int unused2 = AppLovinVideoViewV2.this.f = -1;
            if (AppLovinVideoViewV2.this.q == null || AppLovinVideoViewV2.this.q.onError(AppLovinVideoViewV2.this.h, i, i2)) {
            }
            return true;
        }
    };
    private final MediaPlayer.OnBufferingUpdateListener D = new MediaPlayer.OnBufferingUpdateListener() {
        public void onBufferingUpdate(MediaPlayer mediaPlayer, int i) {
            r a2 = AppLovinVideoViewV2.this.f1468a;
            a2.b("AppLovinVideoView", "Buffered: " + i + "%");
            int unused = AppLovinVideoViewV2.this.p = i;
        }
    };
    private final MediaPlayer.OnSeekCompleteListener E = new MediaPlayer.OnSeekCompleteListener() {
        public void onSeekComplete(MediaPlayer mediaPlayer) {
            AppLovinVideoViewV2.this.f1468a.b("AppLovinVideoView", "Seek finished");
        }
    };
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final r f1468a;
    private final g.d b;
    private final l c;
    private Uri d;
    /* access modifiers changed from: private */
    public int e = 0;
    /* access modifiers changed from: private */
    public int f = 0;
    /* access modifiers changed from: private */
    public SurfaceHolder g = null;
    /* access modifiers changed from: private */
    public MediaPlayer h = null;
    private int i;
    /* access modifiers changed from: private */
    public int j;
    /* access modifiers changed from: private */
    public int k;
    /* access modifiers changed from: private */
    public int l;
    /* access modifiers changed from: private */
    public int m;
    /* access modifiers changed from: private */
    public MediaPlayer.OnCompletionListener n;
    /* access modifiers changed from: private */
    public MediaPlayer.OnPreparedListener o;
    /* access modifiers changed from: private */
    public int p;
    /* access modifiers changed from: private */
    public MediaPlayer.OnErrorListener q;
    /* access modifiers changed from: private */
    public MediaPlayer.OnInfoListener r;
    /* access modifiers changed from: private */
    public int s;
    /* access modifiers changed from: private */
    public boolean t;
    /* access modifiers changed from: private */
    public boolean u;
    /* access modifiers changed from: private */
    public boolean v;
    /* access modifiers changed from: private */
    public AudioManager w;
    /* access modifiers changed from: private */
    public int x = 1;
    private final MediaPlayer.OnVideoSizeChangedListener y = new MediaPlayer.OnVideoSizeChangedListener() {
        public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i2) {
            int unused = AppLovinVideoViewV2.this.j = mediaPlayer.getVideoWidth();
            int unused2 = AppLovinVideoViewV2.this.k = mediaPlayer.getVideoHeight();
            if (AppLovinVideoViewV2.this.j != 0 && AppLovinVideoViewV2.this.k != 0) {
                AppLovinVideoViewV2.this.getHolder().setFixedSize(AppLovinVideoViewV2.this.j, AppLovinVideoViewV2.this.k);
                AppLovinVideoViewV2.this.requestLayout();
            }
        }
    };
    private final MediaPlayer.OnPreparedListener z = new MediaPlayer.OnPreparedListener() {
        public void onPrepared(MediaPlayer mediaPlayer) {
            int unused = AppLovinVideoViewV2.this.e = 2;
            AppLovinVideoViewV2 appLovinVideoViewV2 = AppLovinVideoViewV2.this;
            boolean unused2 = appLovinVideoViewV2.v = true;
            boolean unused3 = appLovinVideoViewV2.u = true;
            boolean unused4 = appLovinVideoViewV2.t = true;
            if (AppLovinVideoViewV2.this.o != null) {
                AppLovinVideoViewV2.this.o.onPrepared(AppLovinVideoViewV2.this.h);
            }
            int unused5 = AppLovinVideoViewV2.this.j = mediaPlayer.getVideoWidth();
            int unused6 = AppLovinVideoViewV2.this.k = mediaPlayer.getVideoHeight();
            int g = AppLovinVideoViewV2.this.s;
            if (g != 0) {
                AppLovinVideoViewV2.this.seekTo(g);
            }
            if (AppLovinVideoViewV2.this.j != 0 && AppLovinVideoViewV2.this.k != 0) {
                AppLovinVideoViewV2.this.getHolder().setFixedSize(AppLovinVideoViewV2.this.j, AppLovinVideoViewV2.this.k);
                if (!(AppLovinVideoViewV2.this.l == AppLovinVideoViewV2.this.j && AppLovinVideoViewV2.this.m == AppLovinVideoViewV2.this.k && AppLovinVideoViewV2.this.f == 3)) {
                    return;
                }
            } else if (AppLovinVideoViewV2.this.f != 3) {
                return;
            }
            AppLovinVideoViewV2.this.start();
        }
    };

    public AppLovinVideoViewV2(g.d dVar, Context context, l lVar) {
        super(context);
        this.b = dVar;
        this.f1468a = lVar.j0();
        this.c = lVar;
        this.w = (AudioManager) context.getSystemService("audio");
        getHolder().addCallback(new SurfaceHolder.Callback() {
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i2, int i3) {
                r a2 = AppLovinVideoViewV2.this.f1468a;
                a2.b("AppLovinVideoView", "Surface changed with format: " + i + ", width: " + i2 + ", height: " + i3);
                int unused = AppLovinVideoViewV2.this.l = i2;
                int unused2 = AppLovinVideoViewV2.this.m = i3;
                boolean z = false;
                boolean z2 = AppLovinVideoViewV2.this.f == 3 || AppLovinVideoViewV2.this.f == 4;
                if (AppLovinVideoViewV2.this.j == i2 && AppLovinVideoViewV2.this.k == i3) {
                    z = true;
                }
                if (AppLovinVideoViewV2.this.h != null && z2 && z) {
                    if (AppLovinVideoViewV2.this.s != 0) {
                        AppLovinVideoViewV2 appLovinVideoViewV2 = AppLovinVideoViewV2.this;
                        appLovinVideoViewV2.seekTo(appLovinVideoViewV2.s);
                    }
                    AppLovinVideoViewV2.this.start();
                }
            }

            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                AppLovinVideoViewV2.this.f1468a.b("AppLovinVideoView", "Surface created");
                SurfaceHolder unused = AppLovinVideoViewV2.this.g = surfaceHolder;
                if (AppLovinVideoViewV2.this.h != null) {
                    AppLovinVideoViewV2.this.h.setSurface(surfaceHolder.getSurface());
                } else {
                    AppLovinVideoViewV2.this.a();
                }
            }

            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
                AppLovinVideoViewV2.this.f1468a.b("AppLovinVideoView", "Surface destroyed");
                SurfaceHolder unused = AppLovinVideoViewV2.this.g = null;
            }
        });
        setFocusable(true);
        setFocusableInTouchMode(true);
        requestFocus();
        this.e = 0;
        this.f = 0;
    }

    /* access modifiers changed from: private */
    public void a() {
        this.f1468a.b("AppLovinVideoView", "Opening video");
        if (this.d != null && this.g != null) {
            if (this.h != null) {
                this.f1468a.b("AppLovinVideoView", "Using existing MediaPlayer");
                this.h.start();
                return;
            }
            try {
                this.h = new MediaPlayer();
                if (this.i != 0) {
                    this.h.setAudioSessionId(this.i);
                } else {
                    this.i = this.h.getAudioSessionId();
                }
                this.h.setOnPreparedListener(this.z);
                this.h.setOnVideoSizeChangedListener(this.y);
                this.h.setOnCompletionListener(this.A);
                this.h.setOnErrorListener(this.C);
                this.h.setOnInfoListener(this.B);
                this.h.setOnBufferingUpdateListener(this.D);
                this.h.setOnSeekCompleteListener(this.E);
                this.p = 0;
                this.h.setDataSource(getContext(), this.d, (Map) null);
                this.h.setDisplay(this.g);
                this.h.setScreenOnWhilePlaying(true);
                this.h.prepareAsync();
                this.e = 1;
            } catch (Throwable th) {
                r.c("AppLovinVideoView", "Unable to open video: " + this.d, th);
                this.e = -1;
                this.f = -1;
                this.C.onError(this.h, 1, 0);
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:2:0x0005, code lost:
        r0 = r3.e;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b() {
        /*
            r3 = this;
            android.media.MediaPlayer r0 = r3.h
            r1 = 1
            if (r0 == 0) goto L_0x000f
            int r0 = r3.e
            r2 = -1
            if (r0 == r2) goto L_0x000f
            if (r0 == 0) goto L_0x000f
            if (r0 == r1) goto L_0x000f
            goto L_0x0010
        L_0x000f:
            r1 = 0
        L_0x0010:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.adview.AppLovinVideoViewV2.b():boolean");
    }

    public boolean canPause() {
        return this.t;
    }

    public boolean canSeekBackward() {
        return this.u;
    }

    public boolean canSeekForward() {
        return this.v;
    }

    public int getAudioSessionId() {
        if (this.i == 0) {
            MediaPlayer mediaPlayer = new MediaPlayer();
            this.i = mediaPlayer.getAudioSessionId();
            mediaPlayer.release();
        }
        return this.i;
    }

    public int getBufferPercentage() {
        if (this.h != null) {
            return this.p;
        }
        return 0;
    }

    public int getCurrentPosition() {
        if (b()) {
            return this.h.getCurrentPosition();
        }
        return 0;
    }

    public int getDuration() {
        if (b()) {
            return this.h.getDuration();
        }
        return -1;
    }

    public boolean isPlaying() {
        return b() && this.h.isPlaying();
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        int defaultSize = SurfaceView.getDefaultSize(this.j, i2);
        int defaultSize2 = SurfaceView.getDefaultSize(this.k, i3);
        if (this.j > 0 && this.k > 0) {
            i5 = View.MeasureSpec.getSize(i2);
            i4 = View.MeasureSpec.getSize(i3);
            boolean z2 = true;
            boolean z3 = this.j * defaultSize2 < this.k * defaultSize;
            if (this.j * defaultSize2 <= this.k * defaultSize) {
                z2 = false;
            }
            g.d dVar = this.b;
            if (dVar == g.d.RESIZE_ASPECT) {
                if (z3) {
                    i6 = (this.j * i4) / this.k;
                } else {
                    if (z2) {
                        defaultSize2 = (this.k * i5) / this.j;
                        i4 = defaultSize2;
                    }
                    setMeasuredDimension(i5, i4);
                }
            } else if (dVar == g.d.RESIZE_ASPECT_FILL) {
                if (z3) {
                    defaultSize2 = (int) (((float) this.k) * (((float) i5) / ((float) this.j)));
                    i4 = defaultSize2;
                    setMeasuredDimension(i5, i4);
                }
                if (z2) {
                    i6 = (int) (((float) this.j) * (((float) i4) / ((float) this.k)));
                }
                setMeasuredDimension(i5, i4);
            }
            i5 = i6;
            setMeasuredDimension(i5, i4);
        }
        i5 = defaultSize;
        i4 = defaultSize2;
        setMeasuredDimension(i5, i4);
    }

    public void pause() {
        this.f1468a.b("AppLovinVideoView", "Pausing video");
        if (b() && this.h.isPlaying()) {
            this.h.pause();
        }
        this.f = 4;
    }

    public void resume() {
        this.f1468a.b("AppLovinVideoView", "Resuming video");
        a();
    }

    public void seekAndStart(long j2) {
        r rVar = this.f1468a;
        rVar.b("AppLovinVideoView", "Seeking and starting to " + j2 + "ms...");
        MediaPlayer mediaPlayer = this.h;
        if (mediaPlayer != null) {
            mediaPlayer.seekTo((int) j2);
        } else {
            this.f1468a.e("AppLovinVideoView", "Media player unavailable");
        }
    }

    public void seekTo(int i2) {
        r rVar = this.f1468a;
        rVar.b("AppLovinVideoView", "Seeking to " + i2 + "ms");
        if (b()) {
            this.h.seekTo(i2);
            i2 = 0;
        } else {
            this.f1468a.b("AppLovinVideoView", "Seek delayed");
        }
        this.s = i2;
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener onCompletionListener) {
        this.n = onCompletionListener;
    }

    public void setOnErrorListener(MediaPlayer.OnErrorListener onErrorListener) {
        this.q = onErrorListener;
    }

    public void setOnInfoListener(MediaPlayer.OnInfoListener onInfoListener) {
        this.r = onInfoListener;
    }

    public void setOnPreparedListener(MediaPlayer.OnPreparedListener onPreparedListener) {
        this.o = onPreparedListener;
    }

    public void setVideoURI(Uri uri) {
        r rVar = this.f1468a;
        rVar.b("AppLovinVideoView", "Setting video uri: " + uri);
        this.d = uri;
        this.s = 0;
        a();
        requestLayout();
        invalidate();
    }

    public void start() {
        this.f1468a.b("AppLovinVideoView", "Starting video");
        if (b()) {
            this.h.start();
        }
        this.f = 3;
    }

    public void stopPlayback() {
        this.f1468a.b("AppLovinVideoView", "Stopping playback");
        MediaPlayer mediaPlayer = this.h;
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            final MediaPlayer mediaPlayer2 = this.h;
            this.h = null;
            this.e = 0;
            this.f = 0;
            this.w.abandonAudioFocus((AudioManager.OnAudioFocusChangeListener) null);
            if (((Boolean) this.c.a(b.P3)).booleanValue()) {
                this.c.o().a((a) new y(this.c, new Runnable(this) {
                    public void run() {
                        mediaPlayer2.release();
                    }
                }), o.a.BACKGROUND);
            } else {
                mediaPlayer2.release();
            }
        }
    }
}
