package com.applovin.impl.adview;

import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.lang.ref.WeakReference;

public class u extends WebViewClient {

    /* renamed from: a  reason: collision with root package name */
    private final r f1590a;
    private WeakReference<a> b;

    public interface a {
        void a(t tVar);

        void b(t tVar);

        void c(t tVar);
    }

    public u(l lVar) {
        this.f1590a = lVar.j0();
    }

    private void a(WebView webView, String str) {
        r rVar = this.f1590a;
        rVar.c("WebViewButtonClient", "Processing click on ad URL \"" + str + "\"");
        if (str != null && (webView instanceof t)) {
            t tVar = (t) webView;
            Uri parse = Uri.parse(str);
            String scheme = parse.getScheme();
            String host = parse.getHost();
            String path = parse.getPath();
            a aVar = (a) this.b.get();
            if ("applovin".equalsIgnoreCase(scheme) && "com.applovin.sdk".equalsIgnoreCase(host) && aVar != null) {
                if ("/track_click".equals(path)) {
                    aVar.a(tVar);
                } else if ("/close_ad".equals(path)) {
                    aVar.c(tVar);
                } else if ("/skip_ad".equals(path)) {
                    aVar.b(tVar);
                } else {
                    r rVar2 = this.f1590a;
                    rVar2.d("WebViewButtonClient", "Unknown URL: " + str);
                    r rVar3 = this.f1590a;
                    rVar3.d("WebViewButtonClient", "Path: " + path);
                }
            }
        }
    }

    public void a(WeakReference<a> weakReference) {
        this.b = weakReference;
    }

    public boolean shouldOverrideUrlLoading(WebView webView, String str) {
        a(webView, str);
        return true;
    }
}
