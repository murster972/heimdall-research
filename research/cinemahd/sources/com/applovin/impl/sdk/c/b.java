package com.applovin.impl.sdk.c;

import android.net.Uri;
import com.applovin.impl.a.j;
import com.applovin.impl.adview.AppLovinTouchToClickListener;
import com.applovin.sdk.AppLovinAdSize;
import com.facebook.common.util.ByteConstants;
import com.google.android.gms.ads.AdRequest;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class b<T> implements Comparable {
    public static final b<Long> A = a("consent_dialog_show_from_alert_delay_ms", 450L);
    public static final b<String> A0 = a("text_incent_warning_body", "You won’t get your reward if the video hasn’t finished.");
    public static final b<Boolean> A1 = a("mute_controls_enabled", false);
    public static final b<Boolean> A2 = a("qq7", true);
    public static final b<Boolean> A3 = a("fetch_settings_gzip", false);
    public static final b<Boolean> B = a("alert_consent_for_dialog_rejected", false);
    public static final b<String> B0 = a("text_incent_warning_close_option", "Close");
    public static final b<Boolean> B1 = a("allow_user_muting", true);
    public static final b<Boolean> B2 = a("qq8", true);
    public static final b<Boolean> B3 = a("device_init_gzip", false);
    public static final b<Boolean> C = a("alert_consent_for_dialog_closed", false);
    public static final b<String> C0 = a("text_incent_warning_continue_option", "Keep Watching");
    public static final b<Boolean> C1 = a("mute_videos", false);
    public static final b<Boolean> C2 = a("pui", true);
    public static final b<Boolean> C3 = a("fetch_ad_gzip", false);
    public static final b<Boolean> D = a("alert_consent_for_dialog_closed_with_back_button", false);
    public static final b<Boolean> D0 = a("incent_nonvideo_warning_enabled", false);
    public static final b<Boolean> D1 = a("show_mute_by_default", false);
    public static final b<String> D2 = a("plugin_version", "");
    public static final b<Boolean> D3 = a("event_tracking_gzip", false);
    public static final b<Boolean> E = a("alert_consent_after_init", false);
    public static final b<String> E0 = a("text_incent_nonvideo_warning_title", "Attention!");
    public static final b<Boolean> E1 = a("mute_with_user_settings", true);
    public static final b<Boolean> E2 = a("hgn", false);
    public static final b<Boolean> E3 = a("submit_ad_stats_gzip", false);
    public static final b<Long> F = a("alert_consent_after_init_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final b<String> F0 = a("text_incent_nonvideo_warning_body", "You won’t get your reward if the game hasn’t finished.");
    public static final b<Integer> F1 = a("mute_button_size", 32);
    public static final b<Boolean> F2 = a("cso", false);
    public static final b<Boolean> F3 = a("reward_postback_gzip", false);
    public static final b<Long> G = a("alert_consent_after_dialog_rejection_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(30)));
    public static final b<String> G0 = a("text_incent_nonvideo_warning_close_option", "Close");
    public static final b<Integer> G1 = a("mute_button_margin", 10);
    public static final b<Boolean> G2 = a("cfs", false);
    public static final b<Boolean> G3 = a("zt_flush_gzip", false);
    public static final b<Long> H = a("alert_consent_after_dialog_close_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final b<String> H0 = a("text_incent_nonvideo_warning_continue_option", "Keep Playing");
    public static final b<Integer> H1 = a("mute_button_gravity", 85);
    public static final b<Boolean> H2 = a("cmi", false);
    public static final b<Boolean> H3 = a("force_rerender", false);
    public static final b<Long> I = a("alert_consent_after_dialog_close_with_back_button_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final b<Boolean> I0 = a("check_webview_has_gesture", false);
    public static final b<Boolean> I1 = a("video_immersive_mode_enabled", false);
    public static final b<Boolean> I2 = a("crat", false);
    public static final b<Boolean> I3 = a("daostr", false);
    public static final b<Long> J = a("alert_consent_after_cancel_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(10)));
    public static final b<Integer> J0 = a("close_button_touch_area", 0);
    public static final b<Long> J1 = a("progress_bar_step", 25L);
    public static final b<Boolean> J2 = a("cvs", false);
    public static final b<Boolean> J3 = a("urrr", false);
    public static final b<Long> K = a("alert_consent_reschedule_interval_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(5)));
    public static final b<Integer> K0 = a("close_button_outside_touch_area", 0);
    public static final b<Integer> K1 = a("progress_bar_scale", 10000);
    public static final b<Boolean> K2 = a("caf", false);
    public static final b<Boolean> K3 = a("rwvdv", false);
    public static final b<String> L = a("text_alert_consent_title", "Make this App Better and Stay Free!");
    public static final b<Boolean> L0 = a("ad_info_button_enabled", false);
    public static final b<Integer> L1 = a("progress_bar_vertical_padding", -8);
    public static final b<Boolean> L2 = a("cf", false);
    public static final b<Boolean> L3 = a("handle_render_process_gone", true);
    public static final b<String> M = a("text_alert_consent_body", "If you don't give us consent to use your data, you will be making our ability to support this app harder, which may negatively affect the user experience.");
    public static final b<Long> M0 = a("viewability_adview_imp_delay_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final b<Long> M1 = a("video_resume_delay", 250L);
    public static final b<Boolean> M2 = a("cnr", false);
    public static final b<Boolean> M3 = a("comcr", true);
    public static final b<String> N = a("text_alert_consent_yes_option", "I Agree");
    public static final b<Integer> N0 = a("viewability_adview_banner_min_width", 320);
    public static final b<Boolean> N1 = a("is_video_skippable", false);
    public static final b<Boolean> N2 = a("adr", false);
    public static final b<Boolean> N3 = a("gcoas", false);
    public static final b<String> O = a("text_alert_consent_no_option", "Cancel");
    public static final b<Integer> O0 = a("viewability_adview_banner_min_height", Integer.valueOf(AppLovinAdSize.BANNER.getHeight()));
    public static final b<Integer> O1 = a("vs_buffer_indicator_size", 50);
    public static final b<Float> O2 = a("volume_normalization_factor", Float.valueOf(6.6666665f));
    public static final b<Boolean> O3 = a("teorpc", false);
    public static final b<Long> P = a("ttc_max_click_duration_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final b<Integer> P0 = a("viewability_adview_mrec_min_width", Integer.valueOf(AppLovinAdSize.MREC.getWidth()));
    public static final b<Boolean> P1 = a("video_zero_length_as_computed", false);
    public static final b<Boolean> P2 = a("system_user_agent_collection_enabled", false);
    public static final b<Boolean> P3 = a("rmpibt", false);
    public static final b<Integer> Q = a("ttc_max_click_distance_dp", 10);
    public static final b<Integer> Q0 = a("viewability_adview_mrec_min_height", Integer.valueOf(AppLovinAdSize.MREC.getHeight()));
    public static final b<Long> Q1 = a("set_poststitial_muted_initial_delay_ms", 500L);
    public static final b<Boolean> Q2 = a("user_agent_collection_enabled", false);
    public static final b<Boolean> Q3 = a("spbcioa", false);
    public static final b<Integer> R = a("ttc_acrs", Integer.valueOf(AppLovinTouchToClickListener.ClickRecognitionState.DISABLED.ordinal()));
    public static final b<Integer> R0 = a("viewability_adview_leader_min_width", 728);
    public static final b<Integer> R1 = a("minepsv", 2012000);
    public static final b<Boolean> R2 = a("http_headers_collection_enabled", false);
    public static final b<Boolean> R3 = a("set_webview_render_process_client", false);
    public static final b<Integer> S = a("ttc_acrsv2a", Integer.valueOf(AppLovinTouchToClickListener.ClickRecognitionState.ACTION_DOWN.ordinal()));
    public static final b<Integer> S0 = a("viewability_adview_leader_min_height", Integer.valueOf(AppLovinAdSize.LEADER.getHeight()));
    public static final b<Integer> S1 = a("maxepsv", -1);
    public static final b<Long> S2 = a("http_headers_collection_timeout_ms", 600L);
    public static final b<Boolean> S3 = a("disable_webview_hardware_acceleration", false);
    public static final b<String> T = a("whitelisted_postback_endpoints", "https://prod-a.applovin.com,https://rt.applovin.com/4.0/pix, https://rt.applvn.com/4.0/pix,https://ms.applovin.com/,https://ms.applvn.com/");
    public static final b<Float> T0 = a("viewability_adview_min_alpha", Float.valueOf(10.0f));
    public static final b<Integer> T1 = a("submit_postback_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final b<String> T2 = a("webview_package_name", "com.google.android.webview");
    public static final b<Integer> T3 = a("mmbfas", -1);
    public static final b<String> U = a("fetch_settings_endpoint", "https://ms.applovin.com/");
    public static final b<Long> U0 = a("viewability_timer_min_visible_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final b<Integer> U1 = a("submit_postback_retries", 4);
    public static final b<Boolean> U2 = a("collect_device_angle", false);
    public static final b<String> U3 = a("config_consent_dialog_state", "unknown");
    public static final b<String> V = a("fetch_settings_backup_endpoint", "https://ms.applvn.com/");
    public static final b<Long> V0 = a("viewability_timer_interval_ms", 100L);
    public static final b<Integer> V1 = a("max_postback_attempts", 3);
    public static final b<Boolean> V2 = a("collect_device_movement", false);
    public static final b<String> V3 = a("c_sticky_topics", "safedk_init,max_ad_events,test_mode_enabled,test_mode_networks,send_http_request,adapter_initialization_status");
    public static final b<String> W = a("adserver_endpoint", "https://a.applovin.com/");
    public static final b<Integer> W0 = a("expandable_close_button_size", 27);
    public static final b<Boolean> W1 = a("fppopq", false);
    public static final b<Float> W2 = a("movement_degradation", Float.valueOf(0.75f));
    public static final b<Boolean> W3 = a("zt_enabled", true);
    public static final b<String> X = a("adserver_backup_endpoint", "https://a.applvn.com/");
    public static final b<Integer> X0 = a("expandable_h_close_button_margin", 10);
    public static final b<Boolean> X1 = a("retry_on_all_errors", false);
    public static final b<Integer> X2 = a("device_sensor_period_ms", 250);
    public static final b<String> X3 = a("zt_endpoint", "https://a.applovin.com/");
    public static final b<String> Y = a("api_endpoint", "https://d.applovin.com/");
    public static final b<Integer> Y0 = a("expandable_t_close_button_margin", 10);
    public static final b<Integer> Y1 = a("get_retry_delay_v1", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final b<Boolean> Y2 = a("dte", true);
    public static final b<String> Y3 = a("zt_backup_endpoint", "https://a.applvn.com/");
    public static final b<Boolean> Z0 = a("expandable_lhs_close_button", false);
    public static final b<Integer> Z1 = a("http_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final b<Boolean> Z2 = a("idcw", false);
    public static final b<Integer> Z3 = a("zt_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final b<Integer> a1 = a("expandable_close_button_touch_area", 0);
    public static final b<Integer> a2 = a("http_socket_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(20)));
    public static final b<Long> a3 = a("anr_debug_thread_refresh_time_ms", -1L);
    public static final b<Long> a4 = a("zt_flush_interval_s", -1L);
    public static final b<Boolean> b1 = a("iaad", false);
    public static final b<Boolean> b2 = a("force_ssl", false);
    public static final b<Boolean> b3 = a("is_track_ad_info", true);
    public static final b<Boolean> b4 = a("zt_bg_aware_timer", true);
    private static final List<?> c = Arrays.asList(new Class[]{Boolean.class, Float.class, Integer.class, Long.class, String.class});
    public static final b<String> c0 = a("api_backup_endpoint", "https://d.applvn.com/");
    public static final b<Integer> c1 = a("auxiliary_operations_threads", 3);
    public static final b<Integer> c2 = a("fetch_ad_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final b<Boolean> c3 = a("submit_ad_stats_enabled", false);
    public static final b<Boolean> c4 = a("zt_flush_on_impression", true);
    private static final Map<String, b<?>> d = new HashMap(AdRequest.MAX_CONTENT_URL_LENGTH);
    public static final b<String> d0 = a("event_tracking_endpoint_v2", "https://rt.applovin.com/");
    public static final b<Integer> d1 = a("caching_operations_threads", 8);
    public static final b<Integer> d2 = a("fetch_ad_retry_count_v1", 1);
    public static final b<Integer> d3 = a("submit_ad_stats_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final b<Boolean> d4 = a("zt_flush_on_app_bg", false);
    public static final b<Boolean> e = a("is_disabled", false);
    public static final b<String> e0 = a("event_tracking_backup_endpoint_v2", "https://rt.applvn.com/");
    public static final b<Long> e1 = a("fullscreen_ad_pending_display_state_timeout_ms", Long.valueOf(TimeUnit.SECONDS.toMillis(10)));
    public static final b<Boolean> e2 = a("faer", false);
    public static final b<Integer> e3 = a("submit_ad_stats_retry_count", 1);
    public static final b<Boolean> e4 = a("zt_continue_through_error", true);
    public static final b<String> f = a("device_id", "");
    public static final b<String> f0 = a("fetch_variables_endpoint", "https://ms.applovin.com/");
    public static final b<Long> f1 = a("fullscreen_ad_showing_state_timeout_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(2)));
    public static final b<Boolean> f2 = a("faroae", false);
    public static final b<Integer> f3 = a("submit_ad_stats_max_count", 500);
    public static final b<Boolean> g = a("rss", true);
    public static final b<String> g0 = a("fetch_variables_backup_endpoint", "https://ms.applvn.com/");
    public static final b<Boolean> g1 = a("lhs_close_button_video", false);
    public static final b<Integer> g2 = a("submit_data_retry_count_v1", 1);
    public static final b<Boolean> g3 = a("asdm", false);
    public static final b<String> h = a("device_token", "");
    public static final b<String> h0 = a("token_type_prefixes_r", "4!");
    public static final b<Integer> h1 = a("close_button_right_margin_video", 4);
    public static final b<Integer> h2 = a("response_buffer_size", 16000);
    public static final b<Boolean> h3 = a("error_reporting_enabled", false);
    public static final b<Long> i = a("publisher_id", 0L);
    public static final b<String> i0 = a("token_type_prefixes_arj", "json_v3!");
    public static final b<Integer> i1 = a("close_button_size_video", 30);
    public static final b<Integer> i2 = a("fetch_basic_settings_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(10)));
    public static final b<Integer> i3 = a("error_reporting_log_limit", 100);
    public static final b<Boolean> j = a("is_verbose_logging", false);
    public static final b<String> j0 = a("top_level_events", "landing,paused,resumed,checkout,iap");
    public static final b<Integer> j1 = a("close_button_top_margin_video", 8);
    public static final b<Integer> j2 = a("fetch_basic_settings_retry_count", 3);
    public static final b<String> j3 = a("vast_image_html", "<html><head><style>html,body{height:100%;width:100%}body{background-image:url({SOURCE});background-repeat:no-repeat;background-size:contain;background-position:center;}a{position:absolute;top:0;bottom:0;left:0;right:0}</style></head><body><a href=\"applovin://com.applovin.sdk/adservice/track_click_now\"></a></body></html>");
    public static final b<String> k = a("sc", "");
    public static final b<String> k0 = a("valid_super_property_types", String.class.getName() + "," + Integer.class.getName() + "," + Long.class.getName() + "," + Double.class.getName() + "," + Float.class.getName() + "," + Date.class.getName() + "," + Uri.class.getName() + "," + List.class.getName() + "," + Map.class.getName());
    public static final b<Boolean> k1 = a("show_close_on_exit", true);
    public static final b<Boolean> k2 = a("fetch_basic_settings_on_reconnect", false);
    public static final b<String> k3 = a("vast_link_html", "<html><head><style>html,body,iframe{height:100%;width:100%;}body{margin:0}iframe{border:0;overflow:hidden;position:absolute}</style></head><body><iframe src={SOURCE} frameborder=0></iframe></body></html>");
    public static final b<String> l = a("sc2", "");
    public static final b<Boolean> l0 = a("persist_super_properties", true);
    public static final b<Integer> l1 = a("video_countdown_clock_margin", 10);
    public static final b<Boolean> l2 = a("skip_fetch_basic_settings_if_not_connected", false);
    public static final b<Integer> l3 = a("vast_max_response_length", 640000);
    public static final b<String> m = a("sc3", "");
    public static final b<Integer> m0 = a("super_property_string_max_length", Integer.valueOf(ByteConstants.KB));
    public static final b<Integer> m1 = a("video_countdown_clock_gravity", 83);
    public static final b<Integer> m2 = a("fetch_basic_settings_retry_delay_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(2)));
    public static final b<Integer> m3 = a("vast_max_wrapper_depth", 5);
    public static final b<String> n = a("server_installed_at", "");
    public static final b<Integer> n0 = a("super_property_url_max_length", Integer.valueOf(ByteConstants.KB));
    public static final b<Integer> n1 = a("countdown_clock_size", 32);
    public static final b<Integer> n2 = a("fetch_variables_connection_timeout_ms", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(5)));
    public static final b<Long> n3 = a("vast_progress_tracking_countdown_step", Long.valueOf(TimeUnit.SECONDS.toMillis(1)));
    public static final b<Boolean> o = a("track_network_response_codes", false);
    public static final b<Boolean> o0 = a("use_per_format_cache_queues", true);
    public static final b<Integer> o1 = a("countdown_clock_stroke_size", 4);
    public static final b<Boolean> o2 = a("idflrwbe", false);
    public static final b<String> o3 = a("vast_unsupported_video_extensions", "ogv,flv");
    public static final b<Boolean> p = a("submit_network_response_codes", false);
    public static final b<Boolean> p0 = a("cache_cleanup_enabled", false);
    public static final b<Integer> p1 = a("countdown_clock_text_size", 28);
    public static final b<Boolean> p2 = a("falawpr", false);
    public static final b<String> p3 = a("vast_unsupported_video_types", "video/ogg,video/x-flv");
    public static final b<Boolean> q = a("clear_network_response_codes_on_request", true);
    public static final b<Long> q0 = a("cache_file_ttl_seconds", Long.valueOf(TimeUnit.DAYS.toSeconds(1)));
    public static final b<Boolean> q1 = a("draw_countdown_clock", true);
    public static final b<Integer> q2 = a("ad_session_minutes", 60);
    public static final b<Boolean> q3 = a("vast_validate_with_extension_if_no_video_type", true);
    public static final b<Boolean> r = a("clear_completion_callback_on_failure", false);
    public static final b<Integer> r0 = a("cache_max_size_mb", -1);
    public static final b<Boolean> r1 = a("force_back_button_enabled_always", false);
    public static final b<Boolean> r2 = a("session_tracking_cooldown_on_event_fire", true);
    public static final b<Integer> r3 = a("vast_video_selection_policy", Integer.valueOf(j.a.MEDIUM.ordinal()));
    public static final b<Long> s = a("sicd_ms", 0L);
    public static final b<String> s0 = a("precache_delimiters", ")]',");
    public static final b<Boolean> s1 = a("force_back_button_enabled_close_button", false);
    public static final b<Long> s2 = a("session_tracking_resumed_cooldown_minutes", 90L);
    public static final b<Integer> s3 = a("vast_wrapper_resolution_retry_count_v1", 1);
    public static final b<Integer> t = a("logcat_max_line_size", 1000);
    public static final b<Boolean> t0 = a("ad_resource_caching_enabled", true);
    public static final b<Boolean> t1 = a("force_back_button_enabled_poststitial", false);
    public static final b<Long> t2 = a("session_tracking_paused_cooldown_minutes", 90L);
    public static final b<Integer> t3 = a("vast_wrapper_resolution_connection_timeout", Integer.valueOf((int) TimeUnit.SECONDS.toMillis(30)));
    public static final b<Integer> u = a("stps", 32);
    public static final b<String> u0 = a("resource_cache_prefix", "https://vid.applovin.com/,https://pdn.applovin.com/,https://img.applovin.com/,https://d.applovin.com/,https://assets.applovin.com/,https://cdnjs.cloudflare.com/,http://vid.applovin.com/,http://pdn.applovin.com/,http://img.applovin.com/,http://d.applovin.com/,http://assets.applovin.com/,http://cdnjs.cloudflare.com/");
    public static final b<Long> u1 = a("inter_display_delay", 200L);
    public static final b<Boolean> u2 = a("qq", false);
    public static final b<Boolean> u3 = a("ree", true);
    public static final b<Boolean> v = a("ustp", false);
    public static final b<String> v0 = a("preserved_cached_assets", "sound_off.png,sound_on.png,closeOptOut.png,1381250003_28x28.png,zepto-1.1.3.min.js,jquery-2.1.1.min.js,jquery-1.9.1.min.js,jquery.knob.js");
    public static final b<Boolean> v1 = a("lock_specific_orientation", false);
    public static final b<Boolean> v2 = a("qq1", true);
    public static final b<Boolean> v3 = a("btee", true);
    public static final b<Boolean> w = a("exception_handler_enabled", true);
    public static final b<Integer> w0 = a("vr_retry_count_v1", 1);
    public static final b<Boolean> w1 = a("lhs_skip_button", true);
    public static final b<Boolean> w2 = a("qq3", true);
    public static final b<Long> w3 = a("server_timestamp_ms", 0L);
    public static final b<Boolean> x = a("publisher_can_show_consent_dialog", true);
    public static final b<Integer> x0 = a("cr_retry_count_v1", 1);
    public static final b<String> x1 = a("soft_buttons_resource_id", "config_showNavigationBar");
    public static final b<Boolean> x2 = a("qq4", true);
    public static final b<Long> x3 = a("device_timestamp_ms", 0L);
    public static final b<String> y = a("consent_dialog_url", "https://assets.applovin.com/gdpr/flow_v1/gdpr-flow-1.html");
    public static final b<Boolean> y0 = a("incent_warning_enabled", false);
    public static final b<Boolean> y1 = a("countdown_toggleable", false);
    public static final b<Boolean> y2 = a("qq5", true);
    public static final b<Integer> y3 = a("gzip_min_length", 0);
    public static final b<Boolean> z = a("consent_dialog_immersive_mode_on", false);
    public static final b<String> z0 = a("text_incent_warning_title", "Attention!");
    public static final b<Boolean> z1 = a("track_app_killed", false);
    public static final b<Boolean> z2 = a("qq6", true);
    public static final b<Boolean> z3 = a("gzip_encoding_default", false);

    /* renamed from: a  reason: collision with root package name */
    private final String f1796a;
    private final T b;

    static {
        a("cached_advertising_info_ttl_ms", Long.valueOf(TimeUnit.MINUTES.toMillis(10)));
        a("fail_ad_load_on_failed_video_cache", true);
        a("tctlaa", false);
    }

    public b(String str, T t4) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified");
        } else if (t4 != null) {
            this.f1796a = str;
            this.b = t4;
        } else {
            throw new IllegalArgumentException("No default value specified");
        }
    }

    protected static <T> b<T> a(String str, T t4) {
        if (t4 == null) {
            throw new IllegalArgumentException("No default value specified");
        } else if (c.contains(t4.getClass())) {
            b<T> bVar = new b<>(str, t4);
            if (!d.containsKey(str)) {
                d.put(str, bVar);
                return bVar;
            }
            throw new IllegalArgumentException("Setting has already been used: " + str);
        } else {
            throw new IllegalArgumentException("Unsupported value type: " + t4.getClass());
        }
    }

    public static Collection<b<?>> c() {
        return Collections.synchronizedCollection(d.values());
    }

    public T a(Object obj) {
        return this.b.getClass().cast(obj);
    }

    public String a() {
        return this.f1796a;
    }

    public T b() {
        return this.b;
    }

    public int compareTo(Object obj) {
        if (!(obj instanceof b)) {
            return 0;
        }
        return this.f1796a.compareTo(((b) obj).a());
    }
}
