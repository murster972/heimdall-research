package com.applovin.impl.sdk.c;

import java.util.HashSet;

public class d<T> {
    public static final d<String> A = new d<>("com.applovin.sdk.mediation_provider", String.class);
    public static final d<String> B = new d<>("com.applovin.sdk.mediation.test_mode_network", String.class);
    public static final d<Boolean> C = new d<>("com.applovin.sdk.mediation.test_mode_enabled", Boolean.class);
    public static final d<String> c = new d<>("com.applovin.sdk.impl.isFirstRun", String.class);
    public static final d<Boolean> d = new d<>("com.applovin.sdk.launched_before", Boolean.class);
    public static final d<String> e = new d<>("com.applovin.sdk.user_agent", String.class);
    public static final d<String> f = new d<>("com.applovin.sdk.user_id", String.class);
    public static final d<String> g = new d<>("com.applovin.sdk.compass_id", String.class);
    public static final d<String> h = new d<>("com.applovin.sdk.compass_random_token", String.class);
    public static final d<String> i = new d<>("com.applovin.sdk.applovin_random_token", String.class);
    public static final d<String> j = new d<>("com.applovin.sdk.device_test_group", String.class);
    public static final d<String> k = new d<>("com.applovin.sdk.variables", String.class);
    public static final d<Boolean> l = new d<>("com.applovin.sdk.compliance.has_user_consent", Boolean.class);
    public static final d<Boolean> m = new d<>("com.applovin.sdk.compliance.is_age_restricted_user", Boolean.class);
    public static final d<Boolean> n = new d<>("com.applovin.sdk.compliance.is_do_not_sell", Boolean.class);
    public static final d<HashSet> o = new d<>("com.applovin.sdk.impl.postbackQueue.key", HashSet.class);
    public static final d<String> p = new d<>("com.applovin.sdk.stats", String.class);
    public static final d<String> q = new d<>("com.applovin.sdk.errors", String.class);
    public static final d<String> r = new d<>("com.applovin.sdk.network_response_code_mapping", String.class);
    public static final d<String> s = new d<>("com.applovin.sdk.event_tracking.super_properties", String.class);
    public static final d<String> t = new d<>("com.applovin.sdk.request_tracker.counter", String.class);
    public static final d<HashSet> u = new d<>("com.applovin.sdk.ad.stats", HashSet.class);
    public static final d<Integer> v = new d<>("com.applovin.sdk.last_video_position", Integer.class);
    public static final d<Boolean> w = new d<>("com.applovin.sdk.should_resume_video", Boolean.class);
    public static final d<String> x = new d<>("com.applovin.sdk.mediation.signal_providers", String.class);
    public static final d<String> y = new d<>("com.applovin.sdk.mediation.auto_init_adapters", String.class);
    public static final d<String> z = new d<>("com.applovin.sdk.persisted_data", String.class);

    /* renamed from: a  reason: collision with root package name */
    private final String f1798a;
    private final Class<T> b;

    static {
        new d("com.applovin.sdk.task.stats", HashSet.class);
    }

    public d(String str, Class<T> cls) {
        this.f1798a = str;
        this.b = cls;
    }

    public String a() {
        return this.f1798a;
    }

    public Class<T> b() {
        return this.b;
    }

    public String toString() {
        return "Key{name='" + this.f1798a + '\'' + ", type=" + this.b + '}';
    }
}
