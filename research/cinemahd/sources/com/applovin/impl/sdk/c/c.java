package com.applovin.impl.sdk.c;

import android.content.Context;
import android.content.SharedPreferences;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.mediation.MaxAdFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    protected final l f1797a;
    protected final r b;
    protected final Context c;
    protected final SharedPreferences d;
    private final Map<String, Object> e = new HashMap();
    private final Object f = new Object();

    /* JADX WARNING: Can't wrap try/catch for region: R(6:0|1|2|3|4|6) */
    /* JADX WARNING: Code restructure failed: missing block: B:7:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:3:0x003c */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public c(com.applovin.impl.sdk.l r4) {
        /*
            r3 = this;
            r3.<init>()
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            r3.e = r0
            java.lang.Object r0 = new java.lang.Object
            r0.<init>()
            r3.f = r0
            r3.f1797a = r4
            com.applovin.impl.sdk.r r0 = r4.j0()
            r3.b = r0
            android.content.Context r0 = r4.i()
            r3.c = r0
            android.content.Context r0 = r3.c
            java.lang.String r1 = "com.applovin.sdk.1"
            r2 = 0
            android.content.SharedPreferences r0 = r0.getSharedPreferences(r1, r2)
            r3.d = r0
            java.lang.Class<com.applovin.impl.sdk.c.b> r0 = com.applovin.impl.sdk.c.b.class
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x003c }
            java.lang.Class.forName(r0)     // Catch:{ all -> 0x003c }
            java.lang.Class<com.applovin.impl.sdk.c.a> r0 = com.applovin.impl.sdk.c.a.class
            java.lang.String r0 = r0.getName()     // Catch:{ all -> 0x003c }
            java.lang.Class.forName(r0)     // Catch:{ all -> 0x003c }
        L_0x003c:
            com.applovin.sdk.AppLovinSdkSettings r0 = r4.Y()     // Catch:{ all -> 0x0058 }
            java.lang.Class r0 = r0.getClass()     // Catch:{ all -> 0x0058 }
            java.lang.String r1 = "localSettings"
            java.lang.reflect.Field r0 = com.applovin.impl.sdk.utils.r.a((java.lang.Class) r0, (java.lang.String) r1)     // Catch:{ all -> 0x0058 }
            r1 = 1
            r0.setAccessible(r1)     // Catch:{ all -> 0x0058 }
            com.applovin.sdk.AppLovinSdkSettings r4 = r4.Y()     // Catch:{ all -> 0x0058 }
            java.lang.Object r4 = r0.get(r4)     // Catch:{ all -> 0x0058 }
            java.util.HashMap r4 = (java.util.HashMap) r4     // Catch:{ all -> 0x0058 }
        L_0x0058:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.c.c.<init>(com.applovin.impl.sdk.l):void");
    }

    private static Object a(String str, JSONObject jSONObject, Object obj) throws JSONException {
        if (obj instanceof Boolean) {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        }
        if (obj instanceof Float) {
            return Float.valueOf((float) jSONObject.getDouble(str));
        }
        if (obj instanceof Integer) {
            return Integer.valueOf(jSONObject.getInt(str));
        }
        if (obj instanceof Long) {
            return Long.valueOf(jSONObject.getLong(str));
        }
        if (obj instanceof String) {
            return jSONObject.getString(str);
        }
        throw new RuntimeException("SDK Error: unknown value type: " + obj.getClass());
    }

    private String e() {
        return "com.applovin.sdk." + com.applovin.impl.sdk.utils.r.a(this.f1797a.h0()) + ".";
    }

    public <T> b<T> a(String str, b<T> bVar) {
        synchronized (this.f) {
            for (b<T> next : b.c()) {
                if (next.a().equals(str)) {
                    return next;
                }
            }
            return bVar;
        }
    }

    public <T> T a(b<T> bVar) {
        if (bVar != null) {
            synchronized (this.f) {
                Object obj = this.e.get(bVar.a());
                if (obj == null) {
                    T b2 = bVar.b();
                    return b2;
                }
                T a2 = bVar.a(obj);
                return a2;
            }
        }
        throw new IllegalArgumentException("No setting type specified");
    }

    public void a() {
        if (this.c != null) {
            String e2 = e();
            synchronized (this.f) {
                SharedPreferences.Editor edit = this.d.edit();
                for (b next : b.c()) {
                    Object obj = this.e.get(next.a());
                    if (obj != null) {
                        this.f1797a.a(e2 + next.a(), obj, edit);
                    }
                }
                edit.apply();
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public <T> void a(b<?> bVar, Object obj) {
        if (bVar == null) {
            throw new IllegalArgumentException("No setting type specified");
        } else if (obj != null) {
            synchronized (this.f) {
                this.e.put(bVar.a(), obj);
            }
        } else {
            throw new IllegalArgumentException("No new value specified");
        }
    }

    public void a(JSONObject jSONObject) {
        r rVar;
        String str;
        String str2;
        synchronized (this.f) {
            Iterator<String> keys = jSONObject.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                if (next != null && next.length() > 0) {
                    try {
                        b<Long> a2 = a(next, (b) null);
                        if (a2 != null) {
                            this.e.put(a2.a(), a(next, jSONObject, a2.b()));
                            if (a2 == b.w3) {
                                this.e.put(b.x3.a(), Long.valueOf(System.currentTimeMillis()));
                            }
                        }
                    } catch (JSONException e2) {
                        th = e2;
                        rVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to parse JSON settingsValues array";
                        rVar.b(str, str2, th);
                    } catch (Throwable th) {
                        th = th;
                        rVar = this.b;
                        str = "SettingsManager";
                        str2 = "Unable to convert setting object ";
                        rVar.b(str, str2, th);
                    }
                }
            }
        }
    }

    /* JADX WARNING: type inference failed for: r1v0, types: [com.applovin.impl.sdk.c.b<java.lang.String>, com.applovin.impl.sdk.c.b] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.util.List<java.lang.String> b(com.applovin.impl.sdk.c.b<java.lang.String> r1) {
        /*
            r0 = this;
            java.lang.Object r1 = r0.a(r1)
            java.lang.String r1 = (java.lang.String) r1
            java.util.List r1 = com.applovin.impl.sdk.utils.e.a((java.lang.String) r1)
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.c.c.b(com.applovin.impl.sdk.c.b):java.util.List");
    }

    public void b() {
        if (this.c != null) {
            String e2 = e();
            synchronized (this.f) {
                for (b next : b.c()) {
                    try {
                        Object a2 = this.f1797a.a(e2 + next.a(), null, (Class) next.b().getClass(), this.d);
                        if (a2 != null) {
                            this.e.put(next.a(), a2);
                        }
                    } catch (Exception e3) {
                        r rVar = this.b;
                        rVar.b("SettingsManager", "Unable to load \"" + next.a() + "\"", e3);
                    }
                }
            }
            return;
        }
        throw new IllegalArgumentException("No context specified");
    }

    public List<MaxAdFormat> c(b<String> bVar) {
        ArrayList arrayList = new ArrayList(6);
        for (String c2 : b(bVar)) {
            arrayList.add(com.applovin.impl.sdk.utils.r.c(c2));
        }
        return arrayList;
    }

    public void c() {
        synchronized (this.f) {
            this.e.clear();
        }
        this.f1797a.a(this.d);
    }

    public boolean d() {
        return this.f1797a.Y().isVerboseLoggingEnabled() || ((Boolean) a(b.j)).booleanValue();
    }
}
