package com.applovin.impl.sdk;

import android.content.Context;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.c.e;

public class i {

    /* renamed from: a  reason: collision with root package name */
    private static final a f1836a = new a("Age Restricted User", d.m);
    private static final a b = new a("Has User Consent", d.l);
    private static final a c = new a("\"Do Not Sell\"", d.n);

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final String f1837a;
        private final d<Boolean> b;

        a(String str, d<Boolean> dVar) {
            this.f1837a = str;
            this.b = dVar;
        }

        public Boolean a(Context context) {
            return (Boolean) e.b(this.b, null, context);
        }

        public String a() {
            return this.f1837a;
        }

        public String b(Context context) {
            Boolean a2 = a(context);
            return a2 != null ? a2.toString() : "No value set";
        }
    }

    public static a a() {
        return f1836a;
    }

    public static String a(Context context) {
        return a(f1836a, context) + a(b, context) + a(c, context);
    }

    private static String a(a aVar, Context context) {
        return ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE + aVar.f1837a + " - " + aVar.b(context);
    }

    /* JADX WARNING: type inference failed for: r1v0, types: [com.applovin.impl.sdk.c.d, com.applovin.impl.sdk.c.d<java.lang.Boolean>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static boolean a(com.applovin.impl.sdk.c.d<java.lang.Boolean> r1, java.lang.Boolean r2, android.content.Context r3) {
        /*
            r0 = 0
            java.lang.Object r0 = com.applovin.impl.sdk.c.e.b(r1, r0, (android.content.Context) r3)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            com.applovin.impl.sdk.c.e.a(r1, r2, (android.content.Context) r3)
            r1 = 1
            if (r0 == 0) goto L_0x0011
            if (r0 == r2) goto L_0x0010
            goto L_0x0011
        L_0x0010:
            r1 = 0
        L_0x0011:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.i.a(com.applovin.impl.sdk.c.d, java.lang.Boolean, android.content.Context):boolean");
    }

    public static boolean a(boolean z, Context context) {
        return a(d.m, Boolean.valueOf(z), context);
    }

    public static a b() {
        return b;
    }

    public static boolean b(boolean z, Context context) {
        return a(d.l, Boolean.valueOf(z), context);
    }

    public static a c() {
        return c;
    }

    public static boolean c(boolean z, Context context) {
        return a(d.n, Boolean.valueOf(z), context);
    }
}
