package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.l;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.json.JSONObject;

public class b<T> {

    /* renamed from: a  reason: collision with root package name */
    private String f1867a;
    private String b;
    private Map<String, String> c;
    private Map<String, String> d;
    private final JSONObject e;
    private String f;
    private final T g;
    private final boolean h;
    private final int i;
    private int j;
    private final int k;
    private final int l;
    private final boolean m;
    private final boolean n;
    private final boolean o;
    private final boolean p;
    private final boolean q;

    public static class a<T> {

        /* renamed from: a  reason: collision with root package name */
        String f1868a;
        String b;
        String c;
        Map<String, String> d;
        Map<String, String> e;
        JSONObject f;
        T g;
        boolean h = true;
        int i = 1;
        int j;
        int k;
        boolean l;
        boolean m;
        boolean n;
        boolean o;
        boolean p;

        public a(l lVar) {
            this.j = ((Integer) lVar.a(com.applovin.impl.sdk.c.b.Z1)).intValue();
            this.k = ((Integer) lVar.a(com.applovin.impl.sdk.c.b.Y1)).intValue();
            this.m = ((Boolean) lVar.a(com.applovin.impl.sdk.c.b.X1)).booleanValue();
            this.n = ((Boolean) lVar.a(com.applovin.impl.sdk.c.b.u3)).booleanValue();
            this.o = ((Boolean) lVar.a(com.applovin.impl.sdk.c.b.z3)).booleanValue();
            this.d = new HashMap();
        }

        public a<T> a(int i2) {
            this.i = i2;
            return this;
        }

        public a<T> a(T t) {
            this.g = t;
            return this;
        }

        public a<T> a(String str) {
            this.b = str;
            return this;
        }

        public a<T> a(Map<String, String> map) {
            this.d = map;
            return this;
        }

        public a<T> a(JSONObject jSONObject) {
            this.f = jSONObject;
            return this;
        }

        public a<T> a(boolean z) {
            this.l = z;
            return this;
        }

        public b<T> a() {
            return new b<>(this);
        }

        public a<T> b(int i2) {
            this.j = i2;
            return this;
        }

        public a<T> b(String str) {
            this.f1868a = str;
            return this;
        }

        public a<T> b(Map<String, String> map) {
            this.e = map;
            return this;
        }

        public a<T> b(boolean z) {
            this.m = z;
            return this;
        }

        public a<T> c(int i2) {
            this.k = i2;
            return this;
        }

        public a<T> c(String str) {
            this.c = str;
            return this;
        }

        public a<T> c(boolean z) {
            this.n = z;
            return this;
        }

        public a<T> d(boolean z) {
            this.o = z;
            return this;
        }

        public a<T> e(boolean z) {
            this.p = z;
            return this;
        }
    }

    protected b(a<T> aVar) {
        this.f1867a = aVar.b;
        this.b = aVar.f1868a;
        this.c = aVar.d;
        this.d = aVar.e;
        this.e = aVar.f;
        this.f = aVar.c;
        this.g = aVar.g;
        this.h = aVar.h;
        int i2 = aVar.i;
        this.i = i2;
        this.j = i2;
        this.k = aVar.j;
        this.l = aVar.k;
        this.m = aVar.l;
        this.n = aVar.m;
        this.o = aVar.n;
        this.p = aVar.o;
        this.q = aVar.p;
    }

    public static <T> a<T> a(l lVar) {
        return new a<>(lVar);
    }

    public String a() {
        return this.f1867a;
    }

    public void a(int i2) {
        this.j = i2;
    }

    public void a(String str) {
        this.f1867a = str;
    }

    public String b() {
        return this.b;
    }

    public void b(String str) {
        this.b = str;
    }

    public Map<String, String> c() {
        return this.c;
    }

    public Map<String, String> d() {
        return this.d;
    }

    public JSONObject e() {
        return this.e;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof b)) {
            return false;
        }
        b bVar = (b) obj;
        String str = this.f1867a;
        if (str == null ? bVar.f1867a != null : !str.equals(bVar.f1867a)) {
            return false;
        }
        Map<String, String> map = this.c;
        if (map == null ? bVar.c != null : !map.equals(bVar.c)) {
            return false;
        }
        Map<String, String> map2 = this.d;
        if (map2 == null ? bVar.d != null : !map2.equals(bVar.d)) {
            return false;
        }
        String str2 = this.f;
        if (str2 == null ? bVar.f != null : !str2.equals(bVar.f)) {
            return false;
        }
        String str3 = this.b;
        if (str3 == null ? bVar.b != null : !str3.equals(bVar.b)) {
            return false;
        }
        JSONObject jSONObject = this.e;
        if (jSONObject == null ? bVar.e != null : !jSONObject.equals(bVar.e)) {
            return false;
        }
        T t = this.g;
        if (t == null ? bVar.g == null : t.equals(bVar.g)) {
            return this.h == bVar.h && this.i == bVar.i && this.j == bVar.j && this.k == bVar.k && this.l == bVar.l && this.m == bVar.m && this.n == bVar.n && this.o == bVar.o && this.p == bVar.p && this.q == bVar.q;
        }
        return false;
    }

    public String f() {
        return this.f;
    }

    public T g() {
        return this.g;
    }

    public boolean h() {
        return this.h;
    }

    public int hashCode() {
        int hashCode = super.hashCode() * 31;
        String str = this.f1867a;
        int i2 = 0;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.f;
        int hashCode3 = (hashCode2 + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.b;
        int hashCode4 = (hashCode3 + (str3 != null ? str3.hashCode() : 0)) * 31;
        T t = this.g;
        if (t != null) {
            i2 = t.hashCode();
        }
        int i3 = ((((((((((((((((((((hashCode4 + i2) * 31) + (this.h ? 1 : 0)) * 31) + this.i) * 31) + this.j) * 31) + this.k) * 31) + this.l) * 31) + (this.m ? 1 : 0)) * 31) + (this.n ? 1 : 0)) * 31) + (this.o ? 1 : 0)) * 31) + (this.p ? 1 : 0)) * 31) + (this.q ? 1 : 0);
        Map<String, String> map = this.c;
        if (map != null) {
            i3 = (i3 * 31) + map.hashCode();
        }
        Map<String, String> map2 = this.d;
        if (map2 != null) {
            i3 = (i3 * 31) + map2.hashCode();
        }
        JSONObject jSONObject = this.e;
        if (jSONObject == null) {
            return i3;
        }
        char[] charArray = jSONObject.toString().toCharArray();
        Arrays.sort(charArray);
        return (i3 * 31) + new String(charArray).hashCode();
    }

    public int i() {
        return this.j;
    }

    public int j() {
        return this.i - this.j;
    }

    public int k() {
        return this.k;
    }

    public int l() {
        return this.l;
    }

    public boolean m() {
        return this.m;
    }

    public boolean n() {
        return this.n;
    }

    public boolean o() {
        return this.o;
    }

    public boolean p() {
        return this.p;
    }

    public boolean q() {
        return this.q;
    }

    public String toString() {
        return "HttpRequest {endpoint=" + this.f1867a + ", backupEndpoint=" + this.f + ", httpMethod=" + this.b + ", httpHeaders=" + this.d + ", body=" + this.e + ", emptyResponse=" + this.g + ", requiresResponse=" + this.h + ", initialRetryAttempts=" + this.i + ", retryAttemptsLeft=" + this.j + ", timeoutMillis=" + this.k + ", retryDelayMillis=" + this.l + ", exponentialRetries=" + this.m + ", retryOnAllErrors=" + this.n + ", encodingEnabled=" + this.o + ", gzipBodyEncoding=" + this.p + ", trackConnectionSpeed=" + this.q + '}';
    }
}
