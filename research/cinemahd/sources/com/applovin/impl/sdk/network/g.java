package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.b;
import java.util.Map;
import org.json.JSONObject;

public class g<T> extends b {
    private String r;
    private boolean s;

    public static class a<T> extends b.a<T> {
        /* access modifiers changed from: private */
        public String q;
        /* access modifiers changed from: private */
        public boolean r;

        public a(l lVar) {
            super(lVar);
            this.h = false;
            this.i = ((Integer) lVar.a(com.applovin.impl.sdk.c.b.U1)).intValue();
            this.j = ((Integer) lVar.a(com.applovin.impl.sdk.c.b.T1)).intValue();
            this.k = ((Integer) lVar.a(com.applovin.impl.sdk.c.b.Y1)).intValue();
        }

        public /* synthetic */ b.a a(int i) {
            d(i);
            return this;
        }

        public /* synthetic */ b.a a(Object obj) {
            b(obj);
            return this;
        }

        public /* synthetic */ b.a a(String str) {
            d(str);
            return this;
        }

        public /* synthetic */ b.a a(Map map) {
            c((Map<String, String>) map);
            return this;
        }

        public /* synthetic */ b.a a(JSONObject jSONObject) {
            b(jSONObject);
            return this;
        }

        public /* synthetic */ b.a b(int i) {
            e(i);
            return this;
        }

        public /* synthetic */ b.a b(String str) {
            f(str);
            return this;
        }

        public /* synthetic */ b.a b(Map map) {
            d((Map<String, String>) map);
            return this;
        }

        public a b(T t) {
            this.g = t;
            return this;
        }

        public a b(JSONObject jSONObject) {
            this.f = jSONObject;
            return this;
        }

        /* renamed from: b */
        public g<T> a() {
            return new g<>(this);
        }

        public /* synthetic */ b.a c(int i) {
            f(i);
            return this;
        }

        public /* synthetic */ b.a c(String str) {
            e(str);
            return this;
        }

        public /* synthetic */ b.a c(boolean z) {
            f(z);
            return this;
        }

        public a c(Map<String, String> map) {
            this.d = map;
            return this;
        }

        public /* synthetic */ b.a d(boolean z) {
            g(z);
            return this;
        }

        public a d(int i) {
            this.i = i;
            return this;
        }

        public a d(String str) {
            this.b = str;
            return this;
        }

        public a d(Map<String, String> map) {
            this.e = map;
            return this;
        }

        public a e(int i) {
            this.j = i;
            return this;
        }

        public a e(String str) {
            this.c = str;
            return this;
        }

        public a f(int i) {
            this.k = i;
            return this;
        }

        public a f(String str) {
            this.f1868a = str;
            return this;
        }

        public a f(boolean z) {
            this.n = z;
            return this;
        }

        public a g(String str) {
            this.q = str;
            return this;
        }

        public a g(boolean z) {
            this.o = z;
            return this;
        }

        public a h(boolean z) {
            this.r = z;
            return this;
        }
    }

    protected g(a aVar) {
        super(aVar);
        this.r = aVar.q;
        this.s = aVar.r;
    }

    public static a b(l lVar) {
        return new a(lVar);
    }

    public boolean r() {
        return this.r != null;
    }

    public String s() {
        return this.r;
    }

    public boolean t() {
        return this.s;
    }
}
