package com.applovin.impl.sdk.network;

import android.content.SharedPreferences;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final l f1871a;
    /* access modifiers changed from: private */
    public final r b;
    private final SharedPreferences c;
    /* access modifiers changed from: private */
    public final Object d = new Object();
    /* access modifiers changed from: private */
    public final ArrayList<f> e;
    private final ArrayList<f> f = new ArrayList<>();
    private final Set<f> g = new HashSet();

    public e(l lVar) {
        if (lVar != null) {
            this.f1871a = lVar;
            this.b = lVar.j0();
            this.c = lVar.i().getSharedPreferences("com.applovin.sdk.impl.postbackQueue.domain", 0);
            this.e = b();
            return;
        }
        throw new IllegalArgumentException("No sdk specified");
    }

    /* access modifiers changed from: private */
    /* JADX WARNING: Code restructure failed: missing block: B:14:0x005a, code lost:
        r0 = ((java.lang.Integer) r4.f1871a.a(com.applovin.impl.sdk.c.b.V1)).intValue();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x006c, code lost:
        if (r5.k() <= r0) goto L_0x0092;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x006e, code lost:
        r6 = r4.b;
        r6.d("PersistentPostbackManager", "Exceeded maximum persisted attempt count of " + r0 + ". Dequeuing postback: " + r5);
        d(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:17:0x0092, code lost:
        r1 = r4.d;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x0094, code lost:
        monitor-enter(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:20:?, code lost:
        r4.g.add(r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:21:0x009a, code lost:
        monitor-exit(r1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x009f, code lost:
        if (r5.f() == null) goto L_0x00ab;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x00a1, code lost:
        r0 = new org.json.JSONObject(r5.f());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x00ab, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00ac, code lost:
        r1 = com.applovin.impl.sdk.network.g.b(r4.f1871a);
        r1.d(r5.b());
        r1.e(r5.c());
        r1.c(r5.d());
        r1.f(r5.a());
        r1.d(r5.e());
        r1.b(r0);
        r1.g(r5.h());
        r1.f(r5.g());
        r1.h(r5.i());
        r1.g(r5.j());
        r4.f1871a.u().dispatchPostbackRequest(r1.b(), new com.applovin.impl.sdk.network.e.AnonymousClass2(r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:?, code lost:
        return;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:?, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(final com.applovin.impl.sdk.network.f r5, final com.applovin.sdk.AppLovinPostbackListener r6) {
        /*
            r4 = this;
            com.applovin.impl.sdk.r r0 = r4.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Preparing to submit postback..."
            r1.append(r2)
            r1.append(r5)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "PersistentPostbackManager"
            r0.b(r2, r1)
            com.applovin.impl.sdk.l r0 = r4.f1871a
            boolean r0 = r0.N()
            if (r0 == 0) goto L_0x002a
            com.applovin.impl.sdk.r r5 = r4.b
            java.lang.String r6 = "PersistentPostbackManager"
            java.lang.String r0 = "Skipping postback dispatch because SDK is still initializing - postback will be dispatched afterwards"
            r5.b(r6, r0)
            return
        L_0x002a:
            java.lang.Object r0 = r4.d
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.network.f> r1 = r4.g     // Catch:{ all -> 0x010a }
            boolean r1 = r1.contains(r5)     // Catch:{ all -> 0x010a }
            if (r1 == 0) goto L_0x0053
            com.applovin.impl.sdk.r r6 = r4.b     // Catch:{ all -> 0x010a }
            java.lang.String r1 = "PersistentPostbackManager"
            java.lang.StringBuilder r2 = new java.lang.StringBuilder     // Catch:{ all -> 0x010a }
            r2.<init>()     // Catch:{ all -> 0x010a }
            java.lang.String r3 = "Skip pending postback: "
            r2.append(r3)     // Catch:{ all -> 0x010a }
            java.lang.String r5 = r5.b()     // Catch:{ all -> 0x010a }
            r2.append(r5)     // Catch:{ all -> 0x010a }
            java.lang.String r5 = r2.toString()     // Catch:{ all -> 0x010a }
            r6.b(r1, r5)     // Catch:{ all -> 0x010a }
            monitor-exit(r0)     // Catch:{ all -> 0x010a }
            return
        L_0x0053:
            r5.l()     // Catch:{ all -> 0x010a }
            r4.c()     // Catch:{ all -> 0x010a }
            monitor-exit(r0)     // Catch:{ all -> 0x010a }
            com.applovin.impl.sdk.l r0 = r4.f1871a
            com.applovin.impl.sdk.c.b<java.lang.Integer> r1 = com.applovin.impl.sdk.c.b.V1
            java.lang.Object r0 = r0.a(r1)
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            int r1 = r5.k()
            if (r1 <= r0) goto L_0x0092
            com.applovin.impl.sdk.r r6 = r4.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Exceeded maximum persisted attempt count of "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = ". Dequeuing postback: "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
            java.lang.String r1 = "PersistentPostbackManager"
            r6.d(r1, r0)
            r4.d((com.applovin.impl.sdk.network.f) r5)
            goto L_0x0106
        L_0x0092:
            java.lang.Object r1 = r4.d
            monitor-enter(r1)
            java.util.Set<com.applovin.impl.sdk.network.f> r0 = r4.g     // Catch:{ all -> 0x0107 }
            r0.add(r5)     // Catch:{ all -> 0x0107 }
            monitor-exit(r1)     // Catch:{ all -> 0x0107 }
            java.util.Map r0 = r5.f()
            if (r0 == 0) goto L_0x00ab
            org.json.JSONObject r0 = new org.json.JSONObject
            java.util.Map r1 = r5.f()
            r0.<init>(r1)
            goto L_0x00ac
        L_0x00ab:
            r0 = 0
        L_0x00ac:
            com.applovin.impl.sdk.l r1 = r4.f1871a
            com.applovin.impl.sdk.network.g$a r1 = com.applovin.impl.sdk.network.g.b(r1)
            java.lang.String r2 = r5.b()
            r1.d((java.lang.String) r2)
            java.lang.String r2 = r5.c()
            r1.e((java.lang.String) r2)
            java.util.Map r2 = r5.d()
            r1.c((java.util.Map<java.lang.String, java.lang.String>) r2)
            java.lang.String r2 = r5.a()
            r1.f((java.lang.String) r2)
            java.util.Map r2 = r5.e()
            r1.d((java.util.Map<java.lang.String, java.lang.String>) r2)
            r1.b((org.json.JSONObject) r0)
            boolean r0 = r5.h()
            r1.g((boolean) r0)
            boolean r0 = r5.g()
            r1.f((boolean) r0)
            boolean r0 = r5.i()
            r1.h(r0)
            java.lang.String r0 = r5.j()
            r1.g((java.lang.String) r0)
            com.applovin.impl.sdk.network.g r0 = r1.a()
            com.applovin.impl.sdk.l r1 = r4.f1871a
            com.applovin.impl.sdk.network.PostbackServiceImpl r1 = r1.u()
            com.applovin.impl.sdk.network.e$2 r2 = new com.applovin.impl.sdk.network.e$2
            r2.<init>(r5, r6)
            r1.dispatchPostbackRequest(r0, r2)
        L_0x0106:
            return
        L_0x0107:
            r5 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0107 }
            throw r5
        L_0x010a:
            r5 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x010a }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.e.a(com.applovin.impl.sdk.network.f, com.applovin.sdk.AppLovinPostbackListener):void");
    }

    private ArrayList<f> b() {
        Set<String> set = (Set) this.f1871a.b(d.o, new LinkedHashSet(0), this.c);
        ArrayList<f> arrayList = new ArrayList<>(Math.max(1, set.size()));
        int intValue = ((Integer) this.f1871a.a(b.V1)).intValue();
        r rVar = this.b;
        rVar.b("PersistentPostbackManager", "Deserializing " + set.size() + " postback(s).");
        for (String str : set) {
            try {
                f fVar = new f(new JSONObject(str), this.f1871a);
                if (fVar.k() < intValue) {
                    arrayList.add(fVar);
                } else {
                    r rVar2 = this.b;
                    rVar2.b("PersistentPostbackManager", "Skipping deserialization because maximum attempt count exceeded for postback: " + fVar);
                }
            } catch (Throwable th) {
                r rVar3 = this.b;
                rVar3.b("PersistentPostbackManager", "Unable to deserialize postback request from json: " + str, th);
            }
        }
        r rVar4 = this.b;
        rVar4.b("PersistentPostbackManager", "Successfully loaded postback queue with " + arrayList.size() + " postback(s).");
        return arrayList;
    }

    /* access modifiers changed from: private */
    public void b(f fVar) {
        synchronized (this.d) {
            this.e.add(fVar);
            c();
            r rVar = this.b;
            rVar.b("PersistentPostbackManager", "Enqueued postback: " + fVar);
        }
    }

    private void c() {
        LinkedHashSet linkedHashSet = new LinkedHashSet(this.e.size());
        Iterator<f> it2 = this.e.iterator();
        while (it2.hasNext()) {
            try {
                linkedHashSet.add(it2.next().n().toString());
            } catch (Throwable th) {
                this.b.b("PersistentPostbackManager", "Unable to serialize postback request to JSON.", th);
            }
        }
        this.f1871a.a(d.o, linkedHashSet, this.c);
        this.b.b("PersistentPostbackManager", "Wrote updated postback queue to disk.");
    }

    /* access modifiers changed from: private */
    public void c(f fVar) {
        a(fVar, (AppLovinPostbackListener) null);
    }

    /* access modifiers changed from: private */
    public void d() {
        synchronized (this.d) {
            Iterator<f> it2 = this.f.iterator();
            while (it2.hasNext()) {
                c(it2.next());
            }
            this.f.clear();
        }
    }

    /* access modifiers changed from: private */
    public void d(f fVar) {
        synchronized (this.d) {
            this.g.remove(fVar);
            this.e.remove(fVar);
            c();
        }
        r rVar = this.b;
        rVar.b("PersistentPostbackManager", "Dequeued successfully transmitted postback: " + fVar);
    }

    /* access modifiers changed from: private */
    public void e(f fVar) {
        synchronized (this.d) {
            this.g.remove(fVar);
            this.f.add(fVar);
        }
    }

    public void a() {
        AnonymousClass3 r0 = new Runnable() {
            public void run() {
                synchronized (e.this.d) {
                    if (e.this.e != null) {
                        for (f d : new ArrayList(e.this.e)) {
                            e.this.c(d);
                        }
                    }
                }
            }
        };
        if (((Boolean) this.f1871a.a(b.W1)).booleanValue()) {
            this.f1871a.o().a((a) new y(this.f1871a, r0), o.a.POSTBACKS);
            return;
        }
        r0.run();
    }

    public void a(f fVar) {
        a(fVar, true);
    }

    public void a(f fVar, boolean z) {
        a(fVar, z, (AppLovinPostbackListener) null);
    }

    public void a(final f fVar, boolean z, final AppLovinPostbackListener appLovinPostbackListener) {
        if (com.applovin.impl.sdk.utils.o.b(fVar.b())) {
            if (z) {
                fVar.m();
            }
            AnonymousClass1 r3 = new Runnable() {
                public void run() {
                    synchronized (e.this.d) {
                        e.this.b(fVar);
                        e.this.a(fVar, appLovinPostbackListener);
                    }
                }
            };
            if (com.applovin.impl.sdk.utils.r.b()) {
                this.f1871a.o().a((a) new y(this.f1871a, r3), o.a.POSTBACKS);
                return;
            }
            r3.run();
        }
    }
}
