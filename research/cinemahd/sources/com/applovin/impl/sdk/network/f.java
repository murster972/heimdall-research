package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.facebook.ads.AudienceNetworkActivity;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import org.json.JSONException;
import org.json.JSONObject;

public class f {

    /* renamed from: a  reason: collision with root package name */
    private String f1875a;
    private String b;
    private String c;
    private String d;
    private Map<String, String> e;
    private Map<String, String> f;
    private Map<String, Object> g;
    private boolean h;
    private boolean i;
    private boolean j;
    private String k;
    private int l;

    public static class a {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public String f1876a;
        /* access modifiers changed from: private */
        public String b;
        /* access modifiers changed from: private */
        public String c;
        /* access modifiers changed from: private */
        public String d;
        /* access modifiers changed from: private */
        public Map<String, String> e;
        /* access modifiers changed from: private */
        public Map<String, String> f;
        /* access modifiers changed from: private */
        public Map<String, Object> g;
        /* access modifiers changed from: private */
        public boolean h;
        /* access modifiers changed from: private */
        public boolean i;
        /* access modifiers changed from: private */
        public boolean j;

        public a a(String str) {
            this.f1876a = str;
            return this;
        }

        public a a(Map<String, String> map) {
            this.e = map;
            return this;
        }

        public a a(boolean z) {
            this.h = z;
            return this;
        }

        public f a() {
            return new f(this);
        }

        public a b(String str) {
            this.b = str;
            return this;
        }

        public a b(Map<String, String> map) {
            this.f = map;
            return this;
        }

        public a b(boolean z) {
            this.i = z;
            return this;
        }

        public a c(String str) {
            this.c = str;
            return this;
        }

        public a c(Map<String, Object> map) {
            this.g = map;
            return this;
        }

        public a c(boolean z) {
            this.j = z;
            return this;
        }

        public a d(String str) {
            this.d = str;
            return this;
        }
    }

    private f(a aVar) {
        this.f1875a = UUID.randomUUID().toString();
        this.b = aVar.b;
        this.c = aVar.c;
        this.d = aVar.d;
        this.e = aVar.e;
        this.f = aVar.f;
        this.g = aVar.g;
        this.h = aVar.h;
        this.i = aVar.i;
        this.j = aVar.j;
        this.k = aVar.f1876a;
        this.l = 0;
    }

    f(JSONObject jSONObject, l lVar) throws Exception {
        String b2 = j.b(jSONObject, AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, UUID.randomUUID().toString(), lVar);
        String b3 = j.b(jSONObject, "communicatorRequestId", "", lVar);
        j.b(jSONObject, "httpMethod", "", lVar);
        String string = jSONObject.getString("targetUrl");
        String b4 = j.b(jSONObject, "backupUrl", "", lVar);
        int i2 = jSONObject.getInt("attemptNumber");
        Map<String, String> synchronizedMap = j.a(jSONObject, "parameters") ? Collections.synchronizedMap(j.a(jSONObject.getJSONObject("parameters"))) : Collections.emptyMap();
        Map<String, String> synchronizedMap2 = j.a(jSONObject, "httpHeaders") ? Collections.synchronizedMap(j.a(jSONObject.getJSONObject("httpHeaders"))) : Collections.emptyMap();
        Map<String, Object> synchronizedMap3 = j.a(jSONObject, "requestBody") ? Collections.synchronizedMap(j.b(jSONObject.getJSONObject("requestBody"))) : Collections.emptyMap();
        this.f1875a = b2;
        this.k = b3;
        this.c = string;
        this.d = b4;
        this.e = synchronizedMap;
        this.f = synchronizedMap2;
        this.g = synchronizedMap3;
        this.h = jSONObject.optBoolean("isEncodingEnabled", false);
        this.i = jSONObject.optBoolean("gzipBodyEncoding", false);
        this.j = jSONObject.optBoolean("shouldFireInWebView", false);
        this.l = i2;
    }

    public static a o() {
        return new a();
    }

    /* access modifiers changed from: package-private */
    public String a() {
        return this.b;
    }

    /* access modifiers changed from: package-private */
    public String b() {
        return this.c;
    }

    /* access modifiers changed from: package-private */
    public String c() {
        return this.d;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> d() {
        return this.e;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> e() {
        return this.f;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || f.class != obj.getClass()) {
            return false;
        }
        return this.f1875a.equals(((f) obj).f1875a);
    }

    /* access modifiers changed from: package-private */
    public Map<String, Object> f() {
        return this.g;
    }

    /* access modifiers changed from: package-private */
    public boolean g() {
        return this.h;
    }

    /* access modifiers changed from: package-private */
    public boolean h() {
        return this.i;
    }

    public int hashCode() {
        return this.f1875a.hashCode();
    }

    /* access modifiers changed from: package-private */
    public boolean i() {
        return this.j;
    }

    /* access modifiers changed from: package-private */
    public String j() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public int k() {
        return this.l;
    }

    /* access modifiers changed from: package-private */
    public void l() {
        this.l++;
    }

    /* access modifiers changed from: package-private */
    public void m() {
        HashMap hashMap = new HashMap();
        Map<String, String> map = this.e;
        if (map != null) {
            hashMap.putAll(map);
        }
        hashMap.put("postback_ts", String.valueOf(System.currentTimeMillis()));
        this.e = hashMap;
    }

    /* access modifiers changed from: package-private */
    public JSONObject n() throws JSONException {
        JSONObject jSONObject = new JSONObject();
        jSONObject.put(AudienceNetworkActivity.AUDIENCE_NETWORK_UNIQUE_ID_EXTRA, this.f1875a);
        jSONObject.put("communicatorRequestId", this.k);
        jSONObject.put("httpMethod", this.b);
        jSONObject.put("targetUrl", this.c);
        jSONObject.put("backupUrl", this.d);
        jSONObject.put("isEncodingEnabled", this.h);
        jSONObject.put("gzipBodyEncoding", this.i);
        jSONObject.put("attemptNumber", this.l);
        Map<String, String> map = this.e;
        if (map != null) {
            jSONObject.put("parameters", new JSONObject(map));
        }
        Map<String, String> map2 = this.f;
        if (map2 != null) {
            jSONObject.put("httpHeaders", new JSONObject(map2));
        }
        Map<String, Object> map3 = this.g;
        if (map3 != null) {
            jSONObject.put("requestBody", new JSONObject(map3));
        }
        return jSONObject;
    }

    public String toString() {
        return "PostbackRequest{uniqueId='" + this.f1875a + '\'' + ", communicatorRequestId='" + this.k + '\'' + ", httpMethod='" + this.b + '\'' + ", targetUrl='" + this.c + '\'' + ", backupUrl='" + this.d + '\'' + ", attemptNumber=" + this.l + ", isEncodingEnabled=" + this.h + ", isGzipBodyEncoding=" + this.i + '}';
    }
}
