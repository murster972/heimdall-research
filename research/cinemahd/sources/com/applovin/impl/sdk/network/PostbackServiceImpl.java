package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.e.a;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.g;
import com.applovin.sdk.AppLovinPostbackListener;
import com.applovin.sdk.AppLovinPostbackService;

public class PostbackServiceImpl implements AppLovinPostbackService {

    /* renamed from: a  reason: collision with root package name */
    private final l f1863a;

    public PostbackServiceImpl(l lVar) {
        this.f1863a = lVar;
    }

    public void dispatchPostbackAsync(String str, AppLovinPostbackListener appLovinPostbackListener) {
        g.a b = g.b(this.f1863a);
        b.d(str);
        b.f(false);
        dispatchPostbackRequest(b.a(), appLovinPostbackListener);
    }

    public void dispatchPostbackRequest(g gVar, o.a aVar, AppLovinPostbackListener appLovinPostbackListener) {
        this.f1863a.o().a((a) new com.applovin.impl.sdk.e.g(gVar, aVar, this.f1863a, appLovinPostbackListener), aVar);
    }

    public void dispatchPostbackRequest(g gVar, AppLovinPostbackListener appLovinPostbackListener) {
        dispatchPostbackRequest(gVar, o.a.POSTBACKS, appLovinPostbackListener);
    }

    public String toString() {
        return "PostbackService{}";
    }
}
