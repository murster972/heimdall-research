package com.applovin.impl.sdk.network;

import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.h;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import com.applovin.impl.sdk.utils.u;
import com.applovin.sdk.AppLovinErrorCodes;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.json.JSONException;
import org.json.JSONObject;
import org.xml.sax.SAXException;

public class a {
    private static final List<String> d = Arrays.asList(new String[]{"5.0/i", "4.0/ad", "1.0/mediate"});

    /* renamed from: a  reason: collision with root package name */
    private final l f1864a;
    private final r b;
    private b c;

    /* renamed from: com.applovin.impl.sdk.network.a$a  reason: collision with other inner class name */
    public static class C0016a {

        /* renamed from: a  reason: collision with root package name */
        private long f1865a;
        private long b;

        /* access modifiers changed from: private */
        public void a(long j) {
            this.f1865a = j;
        }

        /* access modifiers changed from: private */
        public void b(long j) {
            this.b = j;
        }

        public long a() {
            return this.f1865a;
        }

        public long b() {
            return this.b;
        }
    }

    public static class b {

        /* renamed from: a  reason: collision with root package name */
        private final long f1866a = System.currentTimeMillis();
        private final String b;
        private final long c;
        private final long d;

        b(String str, long j, long j2) {
            this.b = str;
            this.c = j;
            this.d = j2;
        }

        public long a() {
            return this.f1866a;
        }

        public String b() {
            return this.b;
        }

        public long c() {
            return this.c;
        }

        public long d() {
            return this.d;
        }

        public String toString() {
            return "RequestMeasurement{timestampMillis=" + this.f1866a + ", urlHostAndPathString='" + this.b + '\'' + ", responseSize=" + this.c + ", connectionTimeMillis=" + this.d + '}';
        }
    }

    public interface c<T> {
        void a(int i);

        void a(T t, int i);
    }

    public a(l lVar) {
        this.f1864a = lVar;
        this.b = lVar.j0();
    }

    private int a(Throwable th) {
        if (th instanceof UnknownHostException) {
            return AppLovinErrorCodes.NO_NETWORK;
        }
        if (th instanceof SocketTimeoutException) {
            return AppLovinErrorCodes.FETCH_AD_TIMEOUT;
        }
        if (th instanceof IOException) {
            return -100;
        }
        return th instanceof JSONException ? -104 : -1;
    }

    private <T> T a(String str, T t) throws JSONException, SAXException, ClassCastException {
        if (t == null) {
            return str;
        }
        if (str != null && str.length() >= 3) {
            if (t instanceof JSONObject) {
                return new JSONObject(str);
            }
            if (t instanceof t) {
                return u.a(str, this.f1864a);
            }
            if (t instanceof String) {
                return str;
            }
            r rVar = this.b;
            rVar.e("ConnectionManager", "Failed to process response of type '" + t.getClass().getName() + "'");
        }
        return t;
    }

    private HttpURLConnection a(String str, String str2, Map<String, String> map, int i) throws IOException {
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL(str).openConnection();
        httpURLConnection.setRequestMethod(str2);
        httpURLConnection.setConnectTimeout(i < 0 ? ((Integer) this.f1864a.a(com.applovin.impl.sdk.c.b.Z1)).intValue() : i);
        if (i < 0) {
            i = ((Integer) this.f1864a.a(com.applovin.impl.sdk.c.b.a2)).intValue();
        }
        httpURLConnection.setReadTimeout(i);
        httpURLConnection.setDefaultUseCaches(false);
        httpURLConnection.setAllowUserInteraction(false);
        httpURLConnection.setUseCaches(false);
        httpURLConnection.setInstanceFollowRedirects(true);
        httpURLConnection.setDoInput(true);
        if (map != null && map.size() > 0) {
            for (Map.Entry next : map.entrySet()) {
                httpURLConnection.setRequestProperty((String) next.getKey(), (String) next.getValue());
            }
        }
        return httpURLConnection;
    }

    private void a(int i, String str) {
        if (((Boolean) this.f1864a.a(com.applovin.impl.sdk.c.b.o)).booleanValue()) {
            try {
                d.a(i, str, this.f1864a.i());
            } catch (Throwable th) {
                r j0 = this.f1864a.j0();
                j0.b("ConnectionManager", "Failed to track response code for " + b(str), th);
            }
        }
    }

    private void a(String str) {
        h hVar;
        g gVar;
        if (o.a(str, com.applovin.impl.sdk.utils.h.c(this.f1864a)) || o.a(str, com.applovin.impl.sdk.utils.h.d(this.f1864a))) {
            hVar = this.f1864a.p();
            gVar = g.k;
        } else if (o.a(str, com.applovin.impl.mediation.c.b.g(this.f1864a)) || o.a(str, com.applovin.impl.mediation.c.b.h(this.f1864a))) {
            hVar = this.f1864a.p();
            gVar = g.r;
        } else {
            hVar = this.f1864a.p();
            gVar = g.l;
        }
        hVar.a(gVar);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v3, resolved type: org.json.JSONObject} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v8, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r7v4, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v11, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v12, resolved type: java.lang.String} */
    /* JADX WARNING: Incorrect type for immutable var: ssa=T, code=org.json.JSONObject, for r7v0, types: [T, java.lang.Object] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private <T> void a(java.lang.String r4, int r5, java.lang.String r6, org.json.JSONObject r7, boolean r8, com.applovin.impl.sdk.network.a.c<T> r9) {
        /*
            r3 = this;
            com.applovin.impl.sdk.r r0 = r3.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r5)
            java.lang.String r2 = " received from "
            r1.append(r2)
            java.lang.String r2 = r3.b(r6)
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "ConnectionManager"
            r0.b(r2, r1)
            com.applovin.impl.sdk.r r0 = r3.b
            r0.a((java.lang.String) r2, (java.lang.String) r4)
            r0 = 200(0xc8, float:2.8E-43)
            if (r5 < r0) goto L_0x00ba
            r0 = 300(0x12c, float:4.2E-43)
            if (r5 >= r0) goto L_0x00ba
            if (r8 == 0) goto L_0x0038
            com.applovin.impl.sdk.l r8 = r3.f1864a
            java.lang.String r8 = r8.h0()
            java.lang.String r4 = com.applovin.impl.sdk.utils.m.a((java.lang.String) r4, (java.lang.String) r8)
        L_0x0038:
            if (r4 == 0) goto L_0x0043
            int r8 = r4.length()
            r0 = 2
            if (r8 <= r0) goto L_0x0043
            r8 = 1
            goto L_0x0044
        L_0x0043:
            r8 = 0
        L_0x0044:
            r0 = 204(0xcc, float:2.86E-43)
            if (r5 == r0) goto L_0x00b6
            if (r8 == 0) goto L_0x00b6
            boolean r8 = r7 instanceof java.lang.String     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            if (r8 == 0) goto L_0x0050
        L_0x004e:
            r7 = r4
            goto L_0x00b6
        L_0x0050:
            boolean r8 = r7 instanceof com.applovin.impl.sdk.utils.t     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            if (r8 == 0) goto L_0x005b
            com.applovin.impl.sdk.l r8 = r3.f1864a     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            com.applovin.impl.sdk.utils.t r4 = com.applovin.impl.sdk.utils.u.a((java.lang.String) r4, (com.applovin.impl.sdk.l) r8)     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            goto L_0x004e
        L_0x005b:
            boolean r8 = r7 instanceof org.json.JSONObject     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            if (r8 == 0) goto L_0x0066
            org.json.JSONObject r8 = new org.json.JSONObject     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            r8.<init>(r4)     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            r7 = r8
            goto L_0x00b6
        L_0x0066:
            com.applovin.impl.sdk.r r4 = r3.b     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            java.lang.StringBuilder r8 = new java.lang.StringBuilder     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            r8.<init>()     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            java.lang.String r0 = "Unable to handle '"
            r8.append(r0)     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            java.lang.Class r0 = r7.getClass()     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            java.lang.String r0 = r0.getName()     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            r8.append(r0)     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            java.lang.String r0 = "'"
            r8.append(r0)     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            java.lang.String r8 = r8.toString()     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            r4.e(r2, r8)     // Catch:{ JSONException -> 0x0098, SAXException -> 0x008a }
            goto L_0x00b6
        L_0x008a:
            r4 = move-exception
            r3.a((java.lang.String) r6)
            com.applovin.impl.sdk.r r8 = r3.b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Invalid XML returned from "
            goto L_0x00a5
        L_0x0098:
            r4 = move-exception
            r3.a((java.lang.String) r6)
            com.applovin.impl.sdk.r r8 = r3.b
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = "Invalid JSON returned from "
        L_0x00a5:
            r0.append(r1)
            java.lang.String r6 = r3.b(r6)
            r0.append(r6)
            java.lang.String r6 = r0.toString()
            r8.b(r2, r6, r4)
        L_0x00b6:
            r9.a(r7, r5)
            goto L_0x00da
        L_0x00ba:
            com.applovin.impl.sdk.r r4 = r3.b
            java.lang.StringBuilder r7 = new java.lang.StringBuilder
            r7.<init>()
            r7.append(r5)
            java.lang.String r8 = " error received from "
            r7.append(r8)
            java.lang.String r6 = r3.b(r6)
            r7.append(r6)
            java.lang.String r6 = r7.toString()
            r4.e(r2, r6)
            r9.a(r5)
        L_0x00da:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.a.a(java.lang.String, int, java.lang.String, java.lang.Object, boolean, com.applovin.impl.sdk.network.a$c):void");
    }

    private void a(String str, String str2, int i, long j) {
        r rVar = this.b;
        rVar.c("ConnectionManager", "Successful " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s over " + com.applovin.impl.sdk.utils.h.b(this.f1864a) + " to " + b(str2));
    }

    private void a(String str, String str2, int i, long j, Throwable th) {
        r rVar = this.b;
        rVar.b("ConnectionManager", "Failed " + str + " returned " + i + " in " + (((float) (System.currentTimeMillis() - j)) / 1000.0f) + " s over " + com.applovin.impl.sdk.utils.h.b(this.f1864a) + " to " + b(str2), th);
    }

    private String b(String str) {
        return "#" + str.hashCode() + " \"" + o.g(str) + "\"";
    }

    public b a() {
        return this.c;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v5, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v6, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v11, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v12, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v13, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v14, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v15, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v16, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v17, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v18, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v19, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v21, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r14v23, resolved type: java.io.InputStream} */
    /* JADX WARNING: type inference failed for: r14v25 */
    /* JADX WARNING: type inference failed for: r14v26 */
    /* JADX WARNING: Code restructure failed: missing block: B:178:0x03ea, code lost:
        if (r29.h() == false) goto L_0x03ec;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:176:0x03e6 A[SYNTHETIC, Splitter:B:176:0x03e6] */
    /* JADX WARNING: Removed duplicated region for block: B:185:0x0400  */
    /* JADX WARNING: Removed duplicated region for block: B:199:0x043e A[SYNTHETIC, Splitter:B:199:0x043e] */
    /* JADX WARNING: Removed duplicated region for block: B:201:0x0443 A[Catch:{ all -> 0x0460 }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public <T> void a(com.applovin.impl.sdk.network.b<T> r29, com.applovin.impl.sdk.network.a.C0016a r30, com.applovin.impl.sdk.network.a.c<T> r31) {
        /*
            r28 = this;
            r8 = r28
            r7 = r30
            r9 = r31
            if (r29 == 0) goto L_0x0484
            java.lang.String r0 = r29.a()
            java.lang.String r10 = r29.b()
            if (r0 == 0) goto L_0x047c
            if (r10 == 0) goto L_0x0474
            if (r9 == 0) goto L_0x046c
            java.lang.String r1 = r0.toLowerCase()
            java.lang.String r2 = "http"
            boolean r1 = r1.startsWith(r2)
            java.lang.String r11 = "ConnectionManager"
            if (r1 != 0) goto L_0x0043
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Requested postback submission to non HTTP endpoint "
            r1.append(r2)
            r1.append(r0)
            java.lang.String r0 = "; skipping..."
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            com.applovin.impl.sdk.r.i(r11, r0)
            r0 = -900(0xfffffffffffffc7c, float:NaN)
            r9.a(r0)
            return
        L_0x0043:
            com.applovin.impl.sdk.l r1 = r8.f1864a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.b.b2
            java.lang.Object r1 = r1.a(r2)
            java.lang.Boolean r1 = (java.lang.Boolean) r1
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x006c
            java.lang.String r1 = "https://"
            boolean r2 = r0.contains(r1)
            if (r2 != 0) goto L_0x006c
            com.applovin.impl.sdk.l r2 = r8.f1864a
            com.applovin.impl.sdk.r r2 = r2.j0()
            java.lang.String r3 = "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting..."
            r2.d(r11, r3)
            java.lang.String r2 = "http://"
            java.lang.String r0 = r0.replace(r2, r1)
        L_0x006c:
            boolean r12 = r29.o()
            com.applovin.impl.sdk.l r1 = r8.f1864a
            long r1 = com.applovin.impl.sdk.utils.r.a((com.applovin.impl.sdk.l) r1)
            java.util.Map r3 = r29.c()
            if (r3 == 0) goto L_0x0086
            java.util.Map r3 = r29.c()
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x008c
        L_0x0086:
            int r3 = r29.j()
            if (r3 < 0) goto L_0x00c0
        L_0x008c:
            java.util.Map r3 = r29.c()
            if (r3 == 0) goto L_0x00a5
            int r4 = r29.j()
            if (r4 < 0) goto L_0x00a5
            int r4 = r29.j()
            java.lang.String r4 = java.lang.String.valueOf(r4)
            java.lang.String r5 = "current_retry_attempt"
            r3.put(r5, r4)
        L_0x00a5:
            if (r12 == 0) goto L_0x00bc
            java.lang.String r3 = com.applovin.impl.sdk.utils.r.a((java.util.Map<java.lang.String, java.lang.String>) r3)
            com.applovin.impl.sdk.l r4 = r8.f1864a
            java.lang.String r4 = r4.h0()
            java.lang.String r3 = com.applovin.impl.sdk.utils.m.a((java.lang.String) r3, (java.lang.String) r4, (long) r1)
            java.lang.String r4 = "p"
            java.lang.String r0 = com.applovin.impl.sdk.utils.o.a((java.lang.String) r0, (java.lang.String) r4, (java.lang.String) r3)
            goto L_0x00c0
        L_0x00bc:
            java.lang.String r0 = com.applovin.impl.sdk.utils.o.b((java.lang.String) r0, (java.util.Map<java.lang.String, java.lang.String>) r3)
        L_0x00c0:
            r13 = r0
            long r14 = java.lang.System.currentTimeMillis()
            r16 = 0
            java.lang.String r0 = com.applovin.impl.sdk.utils.o.g(r13)     // Catch:{ all -> 0x0433 }
            java.util.List<java.lang.String> r4 = d     // Catch:{ all -> 0x0433 }
            java.lang.Boolean r0 = com.applovin.impl.sdk.utils.o.a((java.lang.String) r0, (java.util.List<java.lang.String>) r4)     // Catch:{ all -> 0x0433 }
            com.applovin.impl.sdk.r r4 = r8.b     // Catch:{ all -> 0x0433 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0433 }
            r5.<init>()     // Catch:{ all -> 0x0433 }
            java.lang.String r6 = "Sending "
            r5.append(r6)     // Catch:{ all -> 0x0433 }
            r5.append(r10)     // Catch:{ all -> 0x0433 }
            java.lang.String r6 = " request to id=#"
            r5.append(r6)     // Catch:{ all -> 0x0433 }
            int r6 = r13.hashCode()     // Catch:{ all -> 0x0433 }
            r5.append(r6)     // Catch:{ all -> 0x0433 }
            java.lang.String r6 = " \""
            r5.append(r6)     // Catch:{ all -> 0x0433 }
            boolean r0 = r0.booleanValue()     // Catch:{ all -> 0x0433 }
            if (r0 == 0) goto L_0x00f9
            r0 = r13
            goto L_0x00fd
        L_0x00f9:
            java.lang.String r0 = com.applovin.impl.sdk.utils.o.g(r13)     // Catch:{ all -> 0x0433 }
        L_0x00fd:
            r5.append(r0)     // Catch:{ all -> 0x0433 }
            java.lang.String r0 = "\"..."
            r5.append(r0)     // Catch:{ all -> 0x0433 }
            java.lang.String r0 = r5.toString()     // Catch:{ all -> 0x0433 }
            r4.c(r11, r0)     // Catch:{ all -> 0x0433 }
            java.util.Map r0 = r29.d()     // Catch:{ all -> 0x0433 }
            int r4 = r29.k()     // Catch:{ all -> 0x0433 }
            java.net.HttpURLConnection r5 = r8.a((java.lang.String) r13, (java.lang.String) r10, (java.util.Map<java.lang.String, java.lang.String>) r0, (int) r4)     // Catch:{ all -> 0x0433 }
            org.json.JSONObject r0 = r29.e()     // Catch:{ all -> 0x042a }
            if (r0 == 0) goto L_0x01f9
            if (r12 == 0) goto L_0x0133
            org.json.JSONObject r0 = r29.e()     // Catch:{ all -> 0x01f2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01f2 }
            com.applovin.impl.sdk.l r4 = r8.f1864a     // Catch:{ all -> 0x01f2 }
            java.lang.String r4 = r4.h0()     // Catch:{ all -> 0x01f2 }
            java.lang.String r0 = com.applovin.impl.sdk.utils.m.a((java.lang.String) r0, (java.lang.String) r4, (long) r1)     // Catch:{ all -> 0x01f2 }
            goto L_0x013b
        L_0x0133:
            org.json.JSONObject r0 = r29.e()     // Catch:{ all -> 0x01f2 }
            java.lang.String r0 = r0.toString()     // Catch:{ all -> 0x01f2 }
        L_0x013b:
            r1 = r0
            boolean r0 = r29.p()     // Catch:{ all -> 0x01f2 }
            java.lang.String r2 = "UTF-8"
            if (r0 == 0) goto L_0x0182
            if (r1 == 0) goto L_0x0182
            int r0 = r1.length()     // Catch:{ all -> 0x01f2 }
            com.applovin.impl.sdk.l r4 = r8.f1864a     // Catch:{ all -> 0x01f2 }
            com.applovin.impl.sdk.c.b<java.lang.Integer> r6 = com.applovin.impl.sdk.c.b.y3     // Catch:{ all -> 0x01f2 }
            java.lang.Object r4 = r4.a(r6)     // Catch:{ all -> 0x01f2 }
            java.lang.Integer r4 = (java.lang.Integer) r4     // Catch:{ all -> 0x01f2 }
            int r4 = r4.intValue()     // Catch:{ all -> 0x01f2 }
            if (r0 <= r4) goto L_0x0182
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r2)     // Catch:{ all -> 0x0167 }
            byte[] r0 = r1.getBytes(r0)     // Catch:{ all -> 0x0167 }
            byte[] r0 = com.applovin.impl.sdk.utils.r.a((byte[]) r0)     // Catch:{ all -> 0x0167 }
            goto L_0x0184
        L_0x0167:
            r0 = move-exception
            com.applovin.impl.sdk.r r4 = r8.b     // Catch:{ all -> 0x01f2 }
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f2 }
            r6.<init>()     // Catch:{ all -> 0x01f2 }
            java.lang.String r3 = "Failed to gzip POST body for request "
            r6.append(r3)     // Catch:{ all -> 0x01f2 }
            java.lang.String r3 = r8.b(r13)     // Catch:{ all -> 0x01f2 }
            r6.append(r3)     // Catch:{ all -> 0x01f2 }
            java.lang.String r3 = r6.toString()     // Catch:{ all -> 0x01f2 }
            r4.b(r11, r3, r0)     // Catch:{ all -> 0x01f2 }
        L_0x0182:
            r0 = r16
        L_0x0184:
            com.applovin.impl.sdk.r r3 = r8.b     // Catch:{ all -> 0x01f2 }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x01f2 }
            r4.<init>()     // Catch:{ all -> 0x01f2 }
            java.lang.String r6 = "Request to "
            r4.append(r6)     // Catch:{ all -> 0x01f2 }
            java.lang.String r6 = r8.b(r13)     // Catch:{ all -> 0x01f2 }
            r4.append(r6)     // Catch:{ all -> 0x01f2 }
            java.lang.String r6 = " is "
            r4.append(r6)     // Catch:{ all -> 0x01f2 }
            r4.append(r1)     // Catch:{ all -> 0x01f2 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x01f2 }
            r3.b(r11, r4)     // Catch:{ all -> 0x01f2 }
            java.lang.String r3 = "Content-Type"
            java.lang.String r4 = "application/json; charset=utf-8"
            r5.setRequestProperty(r3, r4)     // Catch:{ all -> 0x01f2 }
            r3 = 1
            r5.setDoOutput(r3)     // Catch:{ all -> 0x01f2 }
            boolean r3 = r29.p()     // Catch:{ all -> 0x01f2 }
            if (r3 == 0) goto L_0x01cf
            if (r0 == 0) goto L_0x01cf
            java.lang.String r1 = "Content-Encoding"
            java.lang.String r2 = "gzip"
            r5.setRequestProperty(r1, r2)     // Catch:{ all -> 0x01f2 }
            int r1 = r0.length     // Catch:{ all -> 0x01f2 }
            r5.setFixedLengthStreamingMode(r1)     // Catch:{ all -> 0x01f2 }
            java.io.OutputStream r1 = r5.getOutputStream()     // Catch:{ all -> 0x01f2 }
            r1.write(r0)     // Catch:{ all -> 0x01f2 }
            r1.close()     // Catch:{ all -> 0x01f2 }
            goto L_0x01f9
        L_0x01cf:
            java.nio.charset.Charset r0 = java.nio.charset.Charset.forName(r2)     // Catch:{ all -> 0x01f2 }
            byte[] r0 = r1.getBytes(r0)     // Catch:{ all -> 0x01f2 }
            int r0 = r0.length     // Catch:{ all -> 0x01f2 }
            r5.setFixedLengthStreamingMode(r0)     // Catch:{ all -> 0x01f2 }
            java.io.PrintWriter r0 = new java.io.PrintWriter     // Catch:{ all -> 0x01f2 }
            java.io.OutputStreamWriter r2 = new java.io.OutputStreamWriter     // Catch:{ all -> 0x01f2 }
            java.io.OutputStream r3 = r5.getOutputStream()     // Catch:{ all -> 0x01f2 }
            java.lang.String r4 = "UTF8"
            r2.<init>(r3, r4)     // Catch:{ all -> 0x01f2 }
            r0.<init>(r2)     // Catch:{ all -> 0x01f2 }
            r0.print(r1)     // Catch:{ all -> 0x01f2 }
            r0.close()     // Catch:{ all -> 0x01f2 }
            goto L_0x01f9
        L_0x01f2:
            r0 = move-exception
            r7 = r0
            r11 = r5
            r26 = r14
            goto L_0x0430
        L_0x01f9:
            int r6 = r5.getResponseCode()     // Catch:{ MalformedURLException -> 0x03ca, all -> 0x03be }
            if (r6 <= 0) goto L_0x039c
            com.applovin.impl.sdk.l r0 = r8.f1864a     // Catch:{ MalformedURLException -> 0x0395, all -> 0x038e }
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.J3     // Catch:{ MalformedURLException -> 0x0395, all -> 0x038e }
            java.lang.Object r0 = r0.a(r1)     // Catch:{ MalformedURLException -> 0x0395, all -> 0x038e }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ MalformedURLException -> 0x0395, all -> 0x038e }
            boolean r0 = r0.booleanValue()     // Catch:{ MalformedURLException -> 0x0395, all -> 0x038e }
            if (r0 == 0) goto L_0x022c
            r1 = r28
            r2 = r10
            r3 = r13
            r4 = r6
            r18 = r5
            r19 = r11
            r11 = r6
            r5 = r14
            r1.a((java.lang.String) r2, (java.lang.String) r3, (int) r4, (long) r5)     // Catch:{ MalformedURLException -> 0x0226, all -> 0x021e }
            goto L_0x0231
        L_0x021e:
            r0 = move-exception
            r7 = r0
            r17 = r11
            r26 = r14
            goto L_0x03b5
        L_0x0226:
            r0 = move-exception
            r7 = r0
            r26 = r14
            goto L_0x03d1
        L_0x022c:
            r18 = r5
            r19 = r11
            r11 = r6
        L_0x0231:
            java.io.InputStream r6 = r18.getInputStream()     // Catch:{ MalformedURLException -> 0x038c, all -> 0x038a }
            r8.a((int) r11, (java.lang.String) r13)     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            long r0 = java.lang.System.currentTimeMillis()     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            long r4 = r0 - r14
            com.applovin.impl.sdk.l r0 = r8.f1864a     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.J3     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            java.lang.Object r0 = r0.a(r1)     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            java.lang.Boolean r0 = (java.lang.Boolean) r0     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            boolean r0 = r0.booleanValue()     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            if (r0 == 0) goto L_0x02c7
            com.applovin.impl.sdk.l r0 = r8.f1864a     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            java.lang.String r2 = com.applovin.impl.sdk.utils.h.a((java.io.InputStream) r6, (com.applovin.impl.sdk.l) r0)     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            boolean r0 = r29.h()     // Catch:{ MalformedURLException -> 0x0382, all -> 0x037a }
            if (r0 == 0) goto L_0x02ba
            if (r7 == 0) goto L_0x029e
            if (r2 == 0) goto L_0x028d
            int r0 = r2.length()     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            long r0 = (long) r0     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            r7.b(r0)     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            boolean r0 = r29.q()     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            if (r0 == 0) goto L_0x028d
            com.applovin.impl.sdk.network.a$b r0 = new com.applovin.impl.sdk.network.a$b     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            java.lang.String r21 = r29.a()     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            int r1 = r2.length()     // Catch:{ MalformedURLException -> 0x0289, all -> 0x0285 }
            r26 = r14
            long r14 = (long) r1
            r20 = r0
            r22 = r14
            r24 = r4
            r20.<init>(r21, r22, r24)     // Catch:{ MalformedURLException -> 0x0298, all -> 0x0293 }
            r8.c = r0     // Catch:{ MalformedURLException -> 0x0298, all -> 0x0293 }
            goto L_0x028f
        L_0x0285:
            r0 = move-exception
            r26 = r14
            goto L_0x0294
        L_0x0289:
            r0 = move-exception
            r26 = r14
            goto L_0x0299
        L_0x028d:
            r26 = r14
        L_0x028f:
            r7.a(r4)     // Catch:{ MalformedURLException -> 0x0298, all -> 0x0293 }
            goto L_0x02a0
        L_0x0293:
            r0 = move-exception
        L_0x0294:
            r7 = r0
            r14 = r6
            goto L_0x037f
        L_0x0298:
            r0 = move-exception
        L_0x0299:
            r7 = r0
            r16 = r6
            goto L_0x03d1
        L_0x029e:
            r26 = r14
        L_0x02a0:
            int r3 = r18.getResponseCode()     // Catch:{ MalformedURLException -> 0x02b7, all -> 0x02b4 }
            java.lang.Object r5 = r29.g()     // Catch:{ MalformedURLException -> 0x02b7, all -> 0x02b4 }
            r1 = r28
            r4 = r13
            r14 = r6
            r6 = r12
            r7 = r31
            r1.a(r2, r3, r4, r5, r6, r7)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            goto L_0x0413
        L_0x02b4:
            r0 = move-exception
            goto L_0x037d
        L_0x02b7:
            r0 = move-exception
            goto L_0x0385
        L_0x02ba:
            r26 = r14
            r14 = r6
            if (r7 == 0) goto L_0x02c2
            r7.a(r4)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
        L_0x02c2:
            r9.a(r2, r11)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            goto L_0x0413
        L_0x02c7:
            r26 = r14
            r14 = r6
            r0 = 200(0xc8, float:2.8E-43)
            if (r11 < r0) goto L_0x0366
            r0 = 400(0x190, float:5.6E-43)
            if (r11 >= r0) goto L_0x0366
            if (r7 == 0) goto L_0x02d7
            r7.a(r4)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
        L_0x02d7:
            r1 = r28
            r2 = r10
            r3 = r13
            r15 = r4
            r4 = r11
            r5 = r26
            r1.a((java.lang.String) r2, (java.lang.String) r3, (int) r4, (long) r5)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            com.applovin.impl.sdk.l r0 = r8.f1864a     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r0 = com.applovin.impl.sdk.utils.h.a((java.io.InputStream) r14, (com.applovin.impl.sdk.l) r0)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            if (r0 == 0) goto L_0x035d
            com.applovin.impl.sdk.r r1 = r8.b     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r2 = r19
            r1.a((java.lang.String) r2, (java.lang.String) r0)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            if (r7 == 0) goto L_0x02fb
            int r1 = r0.length()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            long r3 = (long) r1     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r7.b(r3)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
        L_0x02fb:
            boolean r1 = r29.q()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            if (r1 == 0) goto L_0x0317
            com.applovin.impl.sdk.network.a$b r1 = new com.applovin.impl.sdk.network.a$b     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r21 = r29.a()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            int r3 = r0.length()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            long r3 = (long) r3     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r20 = r1
            r22 = r3
            r24 = r15
            r20.<init>(r21, r22, r24)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r8.c = r1     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
        L_0x0317:
            if (r12 == 0) goto L_0x0323
            com.applovin.impl.sdk.l r1 = r8.f1864a     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r1 = r1.h0()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r0 = com.applovin.impl.sdk.utils.m.a((java.lang.String) r0, (java.lang.String) r1)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
        L_0x0323:
            java.lang.Object r1 = r29.g()     // Catch:{ all -> 0x0330 }
            java.lang.Object r0 = r8.a((java.lang.String) r0, r1)     // Catch:{ all -> 0x0330 }
            r9.a(r0, r11)     // Catch:{ all -> 0x0330 }
            goto L_0x0413
        L_0x0330:
            r0 = move-exception
            com.applovin.impl.sdk.r r1 = r8.b     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.StringBuilder r3 = new java.lang.StringBuilder     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r3.<init>()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r4 = "Unable to parse response from "
            r3.append(r4)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r4 = r8.b(r13)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r3.append(r4)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            java.lang.String r3 = r3.toString()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r1.b(r2, r3, r0)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            com.applovin.impl.sdk.l r0 = r8.f1864a     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            com.applovin.impl.sdk.d.h r0 = r0.p()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            com.applovin.impl.sdk.d.g r1 = com.applovin.impl.sdk.d.g.l     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r0.a(r1)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r0 = -800(0xfffffffffffffce0, float:NaN)
            r9.a(r0)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            goto L_0x0413
        L_0x035d:
            java.lang.Object r0 = r29.g()     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r9.a(r0, r11)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            goto L_0x0413
        L_0x0366:
            r7 = 0
            r1 = r28
            r2 = r10
            r3 = r13
            r4 = r11
            r5 = r26
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            r9.a(r11)     // Catch:{ MalformedURLException -> 0x0378, all -> 0x0376 }
            goto L_0x0413
        L_0x0376:
            r0 = move-exception
            goto L_0x037e
        L_0x0378:
            r0 = move-exception
            goto L_0x0386
        L_0x037a:
            r0 = move-exception
            r26 = r14
        L_0x037d:
            r14 = r6
        L_0x037e:
            r7 = r0
        L_0x037f:
            r17 = r11
            goto L_0x03b7
        L_0x0382:
            r0 = move-exception
            r26 = r14
        L_0x0385:
            r14 = r6
        L_0x0386:
            r7 = r0
            r16 = r14
            goto L_0x03d1
        L_0x038a:
            r0 = move-exception
            goto L_0x0392
        L_0x038c:
            r0 = move-exception
            goto L_0x0399
        L_0x038e:
            r0 = move-exception
            r18 = r5
            r11 = r6
        L_0x0392:
            r26 = r14
            goto L_0x03b2
        L_0x0395:
            r0 = move-exception
            r18 = r5
            r11 = r6
        L_0x0399:
            r26 = r14
            goto L_0x03bc
        L_0x039c:
            r18 = r5
            r11 = r6
            r26 = r14
            r7 = 0
            r1 = r28
            r2 = r10
            r3 = r13
            r4 = r11
            r5 = r26
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ MalformedURLException -> 0x03bb }
            r9.a(r11)     // Catch:{ MalformedURLException -> 0x03bb }
            goto L_0x0411
        L_0x03b1:
            r0 = move-exception
        L_0x03b2:
            r7 = r0
            r17 = r11
        L_0x03b5:
            r14 = r16
        L_0x03b7:
            r11 = r18
            goto L_0x043c
        L_0x03bb:
            r0 = move-exception
        L_0x03bc:
            r7 = r0
            goto L_0x03d1
        L_0x03be:
            r0 = move-exception
            r18 = r5
            r26 = r14
            r7 = r0
            r14 = r16
            r11 = r18
            goto L_0x043a
        L_0x03ca:
            r0 = move-exception
            r18 = r5
            r26 = r14
            r7 = r0
            r11 = 0
        L_0x03d1:
            r0 = -901(0xfffffffffffffc7b, float:NaN)
            r8.a((int) r0, (java.lang.String) r13)     // Catch:{ all -> 0x0420 }
            com.applovin.impl.sdk.l r1 = r8.f1864a     // Catch:{ all -> 0x0420 }
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r2 = com.applovin.impl.sdk.c.b.J3     // Catch:{ all -> 0x0420 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x0420 }
            java.lang.Boolean r1 = (java.lang.Boolean) r1     // Catch:{ all -> 0x0420 }
            boolean r1 = r1.booleanValue()     // Catch:{ all -> 0x0420 }
            if (r1 == 0) goto L_0x03ec
            boolean r1 = r29.h()     // Catch:{ all -> 0x03b1 }
            if (r1 != 0) goto L_0x03f2
        L_0x03ec:
            java.lang.Object r1 = r29.g()     // Catch:{ all -> 0x0420 }
            if (r1 == 0) goto L_0x0400
        L_0x03f2:
            r1 = r28
            r2 = r10
            r3 = r13
            r4 = r11
            r5 = r26
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ all -> 0x03b1 }
            r9.a(r0)     // Catch:{ all -> 0x03b1 }
            goto L_0x0411
        L_0x0400:
            r1 = r28
            r2 = r10
            r3 = r13
            r4 = r11
            r5 = r26
            r1.a((java.lang.String) r2, (java.lang.String) r3, (int) r4, (long) r5)     // Catch:{ all -> 0x0420 }
            java.lang.Object r1 = r29.g()     // Catch:{ all -> 0x0420 }
            r9.a(r1, r0)     // Catch:{ all -> 0x0420 }
        L_0x0411:
            r14 = r16
        L_0x0413:
            com.applovin.impl.sdk.l r0 = r8.f1864a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r14, (com.applovin.impl.sdk.l) r0)
            com.applovin.impl.sdk.l r0 = r8.f1864a
            r1 = r18
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r1, (com.applovin.impl.sdk.l) r0)
            goto L_0x045f
        L_0x0420:
            r0 = move-exception
            r1 = r18
            r7 = r0
            r17 = r11
            r14 = r16
            r11 = r1
            goto L_0x043c
        L_0x042a:
            r0 = move-exception
            r1 = r5
            r26 = r14
            r7 = r0
            r11 = r1
        L_0x0430:
            r14 = r16
            goto L_0x043a
        L_0x0433:
            r0 = move-exception
            r26 = r14
            r7 = r0
            r11 = r16
            r14 = r11
        L_0x043a:
            r17 = 0
        L_0x043c:
            if (r17 != 0) goto L_0x0443
            int r0 = r8.a((java.lang.Throwable) r7)     // Catch:{ all -> 0x0460 }
            goto L_0x0445
        L_0x0443:
            r0 = r17
        L_0x0445:
            r8.a((int) r0, (java.lang.String) r13)     // Catch:{ all -> 0x0460 }
            r1 = r28
            r2 = r10
            r3 = r13
            r4 = r0
            r5 = r26
            r1.a(r2, r3, r4, r5, r7)     // Catch:{ all -> 0x0460 }
            r9.a(r0)     // Catch:{ all -> 0x0460 }
            com.applovin.impl.sdk.l r0 = r8.f1864a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r14, (com.applovin.impl.sdk.l) r0)
            com.applovin.impl.sdk.l r0 = r8.f1864a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r11, (com.applovin.impl.sdk.l) r0)
        L_0x045f:
            return
        L_0x0460:
            r0 = move-exception
            com.applovin.impl.sdk.l r1 = r8.f1864a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r14, (com.applovin.impl.sdk.l) r1)
            com.applovin.impl.sdk.l r1 = r8.f1864a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r11, (com.applovin.impl.sdk.l) r1)
            throw r0
        L_0x046c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No callback specified"
            r0.<init>(r1)
            throw r0
        L_0x0474:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No method specified"
            r0.<init>(r1)
            throw r0
        L_0x047c:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No endpoint specified"
            r0.<init>(r1)
            throw r0
        L_0x0484:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No request specified"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.network.a.a(com.applovin.impl.sdk.network.b, com.applovin.impl.sdk.network.a$a, com.applovin.impl.sdk.network.a$c):void");
    }
}
