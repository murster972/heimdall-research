package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import com.applovin.impl.mediation.MediationServiceImpl;
import com.applovin.impl.mediation.debugger.a;
import com.applovin.impl.mediation.debugger.ui.testmode.b;
import com.applovin.impl.mediation.g;
import com.applovin.impl.mediation.h;
import com.applovin.impl.mediation.i;
import com.applovin.impl.mediation.k;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.d.f;
import com.applovin.impl.sdk.e.h;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.y;
import com.applovin.impl.sdk.network.PostbackServiceImpl;
import com.applovin.impl.sdk.network.c;
import com.applovin.impl.sdk.network.e;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.n;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinEventService;
import com.applovin.sdk.AppLovinMediationProvider;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkConfiguration;
import com.applovin.sdk.AppLovinSdkSettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserSegment;
import com.applovin.sdk.AppLovinUserService;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONObject;

public class l {
    protected static Context f0;
    private a A;
    private q B;
    private u C;
    /* access modifiers changed from: private */
    public c D;
    private h E;
    private n F;
    private g G;
    private n H;
    private c I;
    private PostbackServiceImpl J;
    private e K;
    private i L;
    private h M;
    private MediationServiceImpl N;
    private k O;
    private a P;
    private s Q;
    private g R;
    private b S;
    /* access modifiers changed from: private */
    public final Object T = new Object();
    private final AtomicBoolean U = new AtomicBoolean(true);
    private boolean V = false;
    /* access modifiers changed from: private */
    public boolean W = false;
    private boolean X = false;
    private boolean Y = false;
    private boolean Z = false;

    /* renamed from: a  reason: collision with root package name */
    private String f1849a;
    private boolean a0 = false;
    private WeakReference<Activity> b;
    private int b0 = 0;
    private long c;
    private AppLovinSdk.SdkInitializationListener c0;
    private AppLovinSdkSettings d;
    private AppLovinSdk.SdkInitializationListener d0;
    private AppLovinUserSegment e;
    /* access modifiers changed from: private */
    public AppLovinSdkConfiguration e0;
    private AppLovinAdServiceImpl f;
    private NativeAdServiceImpl g;
    private EventServiceImpl h;
    private UserServiceImpl i;
    private VariableServiceImpl j;
    private AppLovinSdk k;
    /* access modifiers changed from: private */
    public r l;
    /* access modifiers changed from: private */
    public o m;
    protected com.applovin.impl.sdk.c.c n;
    private com.applovin.impl.sdk.network.a o;
    private com.applovin.impl.sdk.d.h p;
    private m q;
    private com.applovin.impl.sdk.c.e r;
    private f s;
    private k t;
    private q u;
    private d v;
    private p w;
    private com.applovin.impl.sdk.a.e x;
    private com.applovin.impl.sdk.d.c y;
    private v z;

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        for (String i2 : j.a(jSONObject, "error_messages", Collections.emptyList(), this)) {
            r.i("AppLovinSdk", i2);
        }
    }

    public static Context l0() {
        return f0;
    }

    private void m0() {
        this.D.a((c.a) new c.a() {
            public void a() {
                l.this.l.c("AppLovinSdk", "Connected to internet - re-initializing SDK");
                synchronized (l.this.T) {
                    if (!l.this.W) {
                        l.this.M();
                    }
                }
                l.this.D.b(this);
            }

            public void b() {
            }
        });
    }

    public com.applovin.impl.sdk.d.c A() {
        return this.y;
    }

    public v B() {
        return this.z;
    }

    public q C() {
        return this.B;
    }

    public a D() {
        return this.A;
    }

    public u E() {
        return this.C;
    }

    public h F() {
        return this.E;
    }

    public n G() {
        return this.F;
    }

    public g H() {
        return this.G;
    }

    public AppLovinBroadcastManager I() {
        return AppLovinBroadcastManager.getInstance(f0);
    }

    public n J() {
        return this.H;
    }

    public c K() {
        return this.I;
    }

    public Activity L() {
        Activity j2 = j();
        if (j2 != null) {
            return j2;
        }
        Activity a2 = D().a();
        if (a2 != null) {
            return a2;
        }
        return null;
    }

    public void M() {
        synchronized (this.T) {
            if (this.V) {
                a(true);
            } else {
                this.W = true;
                o().c();
                int i2 = this.b0 + 1;
                this.b0 = i2;
                o().a((com.applovin.impl.sdk.e.a) new com.applovin.impl.sdk.e.h(i2, this, new h.a() {
                    public void a(JSONObject jSONObject) {
                        boolean z = jSONObject.length() > 0;
                        com.applovin.impl.sdk.utils.h.b(jSONObject, l.this);
                        com.applovin.impl.sdk.utils.h.a(jSONObject, l.this);
                        com.applovin.impl.sdk.utils.h.c(jSONObject, l.this);
                        com.applovin.impl.sdk.utils.h.a(jSONObject, z, l.this);
                        com.applovin.impl.mediation.c.b.g(jSONObject, l.this);
                        com.applovin.impl.mediation.c.b.h(jSONObject, l.this);
                        l.this.d().a(j.a(jSONObject, "smd", (Boolean) false, l.this).booleanValue());
                        com.applovin.impl.sdk.utils.h.f(jSONObject, l.this);
                        com.applovin.impl.sdk.utils.h.d(jSONObject, l.this);
                        l.this.g().a(jSONObject);
                        l.this.a(jSONObject);
                        l.this.o().a((com.applovin.impl.sdk.e.a) new com.applovin.impl.sdk.e.n(l.this));
                        com.applovin.impl.sdk.utils.h.e(jSONObject, l.this);
                    }
                }), o.a.MAIN);
            }
        }
    }

    public boolean N() {
        boolean z2;
        synchronized (this.T) {
            z2 = this.W;
        }
        return z2;
    }

    public boolean O() {
        boolean z2;
        synchronized (this.T) {
            z2 = this.X;
        }
        return z2;
    }

    public boolean P() {
        return "HSrCHRtOan6wp2kwOIGJC1RDtuSrF2mWVbio2aBcMHX9KF3iTJ1lLSzCKP1ZSo5yNolPNw1kCTtWpxELFF4ah1".equalsIgnoreCase(h0());
    }

    public boolean Q() {
        return com.applovin.impl.sdk.utils.o.a(b0(), AppLovinMediationProvider.MAX);
    }

    public boolean R() {
        return r.e("com.unity3d.player.UnityPlayerActivity");
    }

    public void S() {
        final AppLovinSdk.SdkInitializationListener sdkInitializationListener = this.c0;
        if (sdkInitializationListener != null) {
            if (O()) {
                this.c0 = null;
                this.d0 = null;
            } else if (this.d0 != sdkInitializationListener) {
                if (((Boolean) a(com.applovin.impl.sdk.c.b.r)).booleanValue()) {
                    this.c0 = null;
                } else {
                    this.d0 = sdkInitializationListener;
                }
            } else {
                return;
            }
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    l.this.l.b("AppLovinSdk", "Calling back publisher's initialization completion handler...");
                    sdkInitializationListener.onSdkInitialized(l.this.e0);
                }
            }, Math.max(0, ((Long) a(com.applovin.impl.sdk.c.b.s)).longValue()));
        }
    }

    public void T() {
        r.i("AppLovinSdk", "Resetting SDK state...");
        long b2 = this.p.b(com.applovin.impl.sdk.d.g.j);
        this.n.c();
        this.n.a();
        this.p.a();
        this.y.b();
        this.p.b(com.applovin.impl.sdk.d.g.j, b2 + 1);
        if (this.U.compareAndSet(true, false)) {
            M();
        } else {
            this.U.set(true);
        }
    }

    public void U() {
        this.P.c();
    }

    public String V() {
        return this.u.a();
    }

    public String W() {
        return this.u.b();
    }

    public String X() {
        return this.u.c();
    }

    public AppLovinSdkSettings Y() {
        return this.d;
    }

    public AppLovinUserSegment Z() {
        return this.e;
    }

    public com.applovin.impl.mediation.h a() {
        return this.M;
    }

    public <T> T a(com.applovin.impl.sdk.c.b<T> bVar) {
        return this.n.a(bVar);
    }

    public <T> T a(d<T> dVar) {
        return b(dVar, (Object) null);
    }

    public <T> T a(String str, T t2, Class cls, SharedPreferences sharedPreferences) {
        return com.applovin.impl.sdk.c.e.a(str, t2, cls, sharedPreferences);
    }

    public void a(long j2) {
        this.t.a(j2);
    }

    public void a(SharedPreferences sharedPreferences) {
        this.r.a(sharedPreferences);
    }

    public void a(com.applovin.impl.mediation.a.e eVar) {
        if (!this.m.a()) {
            List<String> b2 = b(com.applovin.impl.sdk.c.a.f4);
            if (b2.size() > 0 && this.M.c().containsAll(b2)) {
                this.l.b("AppLovinSdk", "All required adapters initialized");
                this.m.d();
                S();
            }
        }
    }

    public <T> void a(d<T> dVar, T t2) {
        this.r.a(dVar, t2);
    }

    public <T> void a(d<T> dVar, T t2, SharedPreferences sharedPreferences) {
        this.r.a(dVar, t2, sharedPreferences);
    }

    public void a(AppLovinSdk.SdkInitializationListener sdkInitializationListener) {
        if (!O()) {
            this.c0 = sdkInitializationListener;
        } else if (sdkInitializationListener != null) {
            sdkInitializationListener.onSdkInitialized(this.e0);
        }
    }

    public void a(AppLovinSdk appLovinSdk) {
        this.k = appLovinSdk;
    }

    public void a(String str) {
        r.f("AppLovinSdk", "Setting plugin version: " + str);
        this.n.a((com.applovin.impl.sdk.c.b<?>) com.applovin.impl.sdk.c.b.D2, (Object) str);
        this.n.a();
    }

    public <T> void a(String str, T t2, SharedPreferences.Editor editor) {
        this.r.a(str, t2, editor);
    }

    public void a(boolean z2) {
        synchronized (this.T) {
            this.W = false;
            this.X = z2;
        }
        if (this.n != null && this.m != null) {
            List<String> b2 = b(com.applovin.impl.sdk.c.a.f4);
            if (b2.isEmpty()) {
                this.m.d();
                S();
                return;
            }
            long longValue = ((Long) a(com.applovin.impl.sdk.c.a.g4)).longValue();
            y yVar = new y(this, true, new Runnable() {
                public void run() {
                    if (!l.this.m.a()) {
                        l.this.l.b("AppLovinSdk", "Timing out adapters init...");
                        l.this.m.d();
                        l.this.S();
                    }
                }
            });
            r rVar = this.l;
            rVar.b("AppLovinSdk", "Waiting for required adapters to init: " + b2 + " - timing out in " + longValue + "ms...");
            this.m.a((com.applovin.impl.sdk.e.a) yVar, o.a.MEDIATION_TIMEOUT, longValue, true);
        }
    }

    public void a(boolean z2, String str, AppLovinSdkSettings appLovinSdkSettings, Context context) {
        d<String> dVar;
        com.applovin.impl.sdk.c.e eVar;
        String str2;
        this.f1849a = str;
        this.c = System.currentTimeMillis();
        this.d = appLovinSdkSettings;
        this.e = new f();
        this.e0 = new SdkConfigurationImpl(this);
        f0 = context.getApplicationContext();
        if (context instanceof Activity) {
            this.b = new WeakReference<>((Activity) context);
        }
        StrictMode.ThreadPolicy allowThreadDiskReads = StrictMode.allowThreadDiskReads();
        this.r = new com.applovin.impl.sdk.c.e(this);
        this.n = new com.applovin.impl.sdk.c.c(this);
        this.n.b();
        this.l = new r(this);
        this.s = new f(this);
        this.s.b();
        this.w = new p(this);
        this.v = new d(this);
        this.x = new com.applovin.impl.sdk.a.e(this);
        this.h = new EventServiceImpl(this);
        this.i = new UserServiceImpl(this);
        this.j = new VariableServiceImpl(this);
        this.y = new com.applovin.impl.sdk.d.c(this);
        this.m = new o(this);
        this.o = new com.applovin.impl.sdk.network.a(this);
        this.p = new com.applovin.impl.sdk.d.h(this);
        this.q = new m(this);
        this.A = new a(context);
        this.f = new AppLovinAdServiceImpl(this);
        this.g = new NativeAdServiceImpl(this);
        this.z = new v(this);
        this.B = new q(this);
        this.J = new PostbackServiceImpl(this);
        this.K = new e(this);
        this.L = new i(this);
        this.M = new com.applovin.impl.mediation.h(this);
        this.N = new MediationServiceImpl(this);
        this.Q = new s(this);
        this.P = new a(this);
        this.O = new k();
        this.R = new g(this);
        this.t = new k(this);
        this.C = new u(this);
        this.F = new n(this);
        this.G = new g(this);
        this.H = new n(this);
        this.S = new b(this);
        this.I = new c(this);
        this.E = new h(this);
        this.u = new q(this);
        if (appLovinSdkSettings.isExceptionHandlerEnabled() && ((Boolean) a(com.applovin.impl.sdk.c.b.w)).booleanValue()) {
            AppLovinExceptionHandler.shared().addSdk(this);
            AppLovinExceptionHandler.shared().enable();
        }
        if (((Boolean) a(com.applovin.impl.sdk.c.b.k2)).booleanValue()) {
            this.D = new c(context);
        }
        if (TextUtils.isEmpty(str)) {
            this.Y = true;
            r.i("AppLovinSdk", "Unable to find AppLovin SDK key. Please add  meta-data android:name=\"applovin.sdk.key\" android:value=\"YOUR_SDK_KEY_HERE\" into AndroidManifest.xml.");
            StringWriter stringWriter = new StringWriter();
            new Throwable("").printStackTrace(new PrintWriter(stringWriter));
            r.i("AppLovinSdk", "Called with an invalid SDK key from: " + stringWriter.toString());
        }
        if (str.length() != 86 && r.e(context)) {
            final String str3 = "Please double-check that you entered your SDK key correctly: " + str;
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    r.a("Invalid AppLovin SDK Key", str3, (Context) l.this.L());
                }
            }, TimeUnit.SECONDS.toMillis(3));
            r.i("AppLovinSdk", str3);
        }
        if (r.h()) {
            r.i("AppLovinSdk", "Failed to find class for name: com.applovin.sdk.AppLovinSdk. Please ensure proguard rules have not been omitted from the build.");
        }
        if (!i0()) {
            if (r.a(context)) {
                appLovinSdkSettings.setVerboseLogging(true);
            }
            h().a((com.applovin.impl.sdk.c.b<?>) com.applovin.impl.sdk.c.b.j, (Object) Boolean.valueOf(appLovinSdkSettings.isVerboseLoggingEnabled()));
            h().a();
            Class<com.applovin.impl.adview.c> cls = com.applovin.impl.adview.c.class;
            SharedPreferences defaultSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            if (TextUtils.isEmpty((String) this.r.b(d.c, null, defaultSharedPreferences))) {
                this.Z = true;
                eVar = this.r;
                dVar = d.c;
                str2 = Boolean.toString(true);
            } else {
                eVar = this.r;
                dVar = d.c;
                str2 = Boolean.toString(false);
            }
            eVar.a(dVar, str2, defaultSharedPreferences);
            if (((Boolean) this.r.b(d.d, false)).booleanValue()) {
                this.l.b("AppLovinSdk", "Initializing SDK for non-maiden launch");
                this.a0 = true;
            } else {
                this.l.b("AppLovinSdk", "Initializing SDK for maiden launch");
                this.r.a(d.d, true);
            }
            boolean a2 = com.applovin.impl.sdk.utils.h.a(i());
            if (!((Boolean) a(com.applovin.impl.sdk.c.b.l2)).booleanValue() || a2) {
                M();
            }
            if (((Boolean) a(com.applovin.impl.sdk.c.b.k2)).booleanValue() && !a2) {
                this.l.c("AppLovinSdk", "SDK initialized with no internet connection - listening for connection");
                m0();
            }
        } else {
            a(false);
        }
        StrictMode.setThreadPolicy(allowThreadDiskReads);
    }

    public boolean a(com.applovin.impl.sdk.c.b<String> bVar, MaxAdFormat maxAdFormat) {
        return c(bVar).contains(maxAdFormat);
    }

    public AppLovinSdkConfiguration a0() {
        return this.e0;
    }

    public MediationServiceImpl b() {
        return this.N;
    }

    public <T> T b(d<T> dVar, T t2) {
        return this.r.b(dVar, t2);
    }

    public <T> T b(d<T> dVar, T t2, SharedPreferences sharedPreferences) {
        return this.r.b(dVar, t2, sharedPreferences);
    }

    public List<String> b(com.applovin.impl.sdk.c.b<String> bVar) {
        return this.n.b(bVar);
    }

    public <T> void b(d<T> dVar) {
        this.r.a(dVar);
    }

    public void b(String str) {
        r rVar = this.l;
        rVar.b("AppLovinSdk", "Setting user id: " + str);
        this.u.a(str);
    }

    public String b0() {
        return (String) a(d.A);
    }

    public s c() {
        return this.Q;
    }

    public List<MaxAdFormat> c(com.applovin.impl.sdk.c.b<String> bVar) {
        return this.n.c(bVar);
    }

    public void c(String str) {
        a(d.A, str);
    }

    public AppLovinAdServiceImpl c0() {
        return this.f;
    }

    public a d() {
        return this.P;
    }

    public NativeAdServiceImpl d0() {
        return this.g;
    }

    public k e() {
        return this.O;
    }

    public AppLovinEventService e0() {
        return this.h;
    }

    public g f() {
        return this.R;
    }

    public AppLovinUserService f0() {
        return this.i;
    }

    public b g() {
        return this.S;
    }

    public VariableServiceImpl g0() {
        return this.j;
    }

    public com.applovin.impl.sdk.c.c h() {
        return this.n;
    }

    public String h0() {
        return this.f1849a;
    }

    public Context i() {
        return f0;
    }

    public boolean i0() {
        return this.Y;
    }

    public Activity j() {
        WeakReference<Activity> weakReference = this.b;
        if (weakReference != null) {
            return (Activity) weakReference.get();
        }
        return null;
    }

    public r j0() {
        return this.l;
    }

    public long k() {
        return this.c;
    }

    public i k0() {
        return this.L;
    }

    public boolean l() {
        return this.Z;
    }

    public boolean m() {
        return this.a0;
    }

    public com.applovin.impl.sdk.network.a n() {
        return this.o;
    }

    public o o() {
        return this.m;
    }

    public com.applovin.impl.sdk.d.h p() {
        return this.p;
    }

    public e q() {
        return this.K;
    }

    public m r() {
        return this.q;
    }

    public f s() {
        return this.s;
    }

    public k t() {
        return this.t;
    }

    public String toString() {
        return "CoreSdk{sdkKey='" + this.f1849a + '\'' + ", enabled=" + this.X + ", isFirstSession=" + this.Z + '}';
    }

    public PostbackServiceImpl u() {
        return this.J;
    }

    public AppLovinSdk v() {
        return this.k;
    }

    public d w() {
        return this.v;
    }

    public p x() {
        return this.w;
    }

    public com.applovin.impl.sdk.a.e y() {
        return this.x;
    }

    public void z() {
        synchronized (this.T) {
            if (!this.W && !this.X) {
                M();
            }
        }
    }
}
