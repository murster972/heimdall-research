package com.applovin.impl.sdk.utils;

import com.applovin.impl.sdk.l;
import java.util.Timer;
import java.util.TimerTask;

public class p {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1922a;
    /* access modifiers changed from: private */
    public Timer b;
    private long c;
    private long d;
    /* access modifiers changed from: private */
    public final Runnable e;
    private long f;
    /* access modifiers changed from: private */
    public final Object g = new Object();

    private p(l lVar, Runnable runnable) {
        this.f1922a = lVar;
        this.e = runnable;
    }

    public static p a(long j, l lVar, Runnable runnable) {
        if (j < 0) {
            throw new IllegalArgumentException("Cannot create a scheduled timer. Invalid fire time passed in: " + j + ".");
        } else if (runnable != null) {
            p pVar = new p(lVar, runnable);
            pVar.c = System.currentTimeMillis();
            pVar.d = j;
            try {
                pVar.b = new Timer();
                pVar.b.schedule(pVar.e(), j);
            } catch (OutOfMemoryError e2) {
                lVar.j0().b("Timer", "Failed to create timer due to OOM error", e2);
            }
            return pVar;
        } else {
            throw new IllegalArgumentException("Cannot create a scheduled timer. Runnable is null.");
        }
    }

    private TimerTask e() {
        return new TimerTask() {
            /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
            /* Code decompiled incorrectly, please refer to instructions dump. */
            public void run() {
                /*
                    r5 = this;
                    r0 = 0
                    com.applovin.impl.sdk.utils.p r1 = com.applovin.impl.sdk.utils.p.this     // Catch:{ all -> 0x001b }
                    java.lang.Runnable r1 = r1.e     // Catch:{ all -> 0x001b }
                    r1.run()     // Catch:{ all -> 0x001b }
                    com.applovin.impl.sdk.utils.p r1 = com.applovin.impl.sdk.utils.p.this
                    java.lang.Object r1 = r1.g
                    monitor-enter(r1)
                    com.applovin.impl.sdk.utils.p r2 = com.applovin.impl.sdk.utils.p.this     // Catch:{ all -> 0x0018 }
                    java.util.Timer unused = r2.b = r0     // Catch:{ all -> 0x0018 }
                    monitor-exit(r1)     // Catch:{ all -> 0x0018 }
                    goto L_0x0042
                L_0x0018:
                    r0 = move-exception
                    monitor-exit(r1)     // Catch:{ all -> 0x0018 }
                    throw r0
                L_0x001b:
                    r1 = move-exception
                    com.applovin.impl.sdk.utils.p r2 = com.applovin.impl.sdk.utils.p.this     // Catch:{ all -> 0x0046 }
                    com.applovin.impl.sdk.l r2 = r2.f1922a     // Catch:{ all -> 0x0046 }
                    if (r2 == 0) goto L_0x0035
                    com.applovin.impl.sdk.utils.p r2 = com.applovin.impl.sdk.utils.p.this     // Catch:{ all -> 0x0046 }
                    com.applovin.impl.sdk.l r2 = r2.f1922a     // Catch:{ all -> 0x0046 }
                    com.applovin.impl.sdk.r r2 = r2.j0()     // Catch:{ all -> 0x0046 }
                    java.lang.String r3 = "Timer"
                    java.lang.String r4 = "Encountered error while executing timed task"
                    r2.b(r3, r4, r1)     // Catch:{ all -> 0x0046 }
                L_0x0035:
                    com.applovin.impl.sdk.utils.p r1 = com.applovin.impl.sdk.utils.p.this
                    java.lang.Object r1 = r1.g
                    monitor-enter(r1)
                    com.applovin.impl.sdk.utils.p r2 = com.applovin.impl.sdk.utils.p.this     // Catch:{ all -> 0x0043 }
                    java.util.Timer unused = r2.b = r0     // Catch:{ all -> 0x0043 }
                    monitor-exit(r1)     // Catch:{ all -> 0x0043 }
                L_0x0042:
                    return
                L_0x0043:
                    r0 = move-exception
                    monitor-exit(r1)     // Catch:{ all -> 0x0043 }
                    throw r0
                L_0x0046:
                    r1 = move-exception
                    com.applovin.impl.sdk.utils.p r2 = com.applovin.impl.sdk.utils.p.this
                    java.lang.Object r2 = r2.g
                    monitor-enter(r2)
                    com.applovin.impl.sdk.utils.p r3 = com.applovin.impl.sdk.utils.p.this     // Catch:{ all -> 0x0055 }
                    java.util.Timer unused = r3.b = r0     // Catch:{ all -> 0x0055 }
                    monitor-exit(r2)     // Catch:{ all -> 0x0055 }
                    throw r1
                L_0x0055:
                    r0 = move-exception
                    monitor-exit(r2)     // Catch:{ all -> 0x0055 }
                    throw r0
                */
                throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.p.AnonymousClass1.run():void");
            }
        };
    }

    public long a() {
        if (this.b == null) {
            return this.d - this.f;
        }
        return this.d - (System.currentTimeMillis() - this.c);
    }

    public void b() {
        synchronized (this.g) {
            if (this.b != null) {
                try {
                    this.b.cancel();
                    this.f = System.currentTimeMillis() - this.c;
                } catch (Throwable th) {
                    this.b = null;
                    throw th;
                }
                this.b = null;
            }
        }
    }

    public void c() {
        synchronized (this.g) {
            if (this.f > 0) {
                try {
                    this.d -= this.f;
                    if (this.d < 0) {
                        this.d = 0;
                    }
                    this.b = new Timer();
                    this.b.schedule(e(), this.d);
                    this.c = System.currentTimeMillis();
                } catch (Throwable th) {
                    this.f = 0;
                    throw th;
                }
                this.f = 0;
            }
        }
    }

    public void d() {
        synchronized (this.g) {
            if (this.b != null) {
                try {
                    this.b.cancel();
                    this.b = null;
                } catch (Throwable th) {
                    this.b = null;
                    this.f = 0;
                    throw th;
                }
                this.f = 0;
            }
        }
    }
}
