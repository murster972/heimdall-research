package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.l;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class d implements AppLovinBroadcastManager.Receiver {
    private static final Set<d> b = new HashSet();

    /* renamed from: a  reason: collision with root package name */
    private final p f1888a;

    private d(long j, final l lVar, final Runnable runnable) {
        this.f1888a = p.a(j, lVar, new Runnable() {
            public void run() {
                lVar.I().unregisterReceiver(d.this);
                d.this.a();
                Runnable runnable = runnable;
                if (runnable != null) {
                    runnable.run();
                }
            }
        });
        b.add(this);
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public static d a(long j, l lVar, Runnable runnable) {
        return new d(j, lVar, runnable);
    }

    public void a() {
        this.f1888a.d();
        b.remove(this);
    }

    public long b() {
        return this.f1888a.a();
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.f1888a.b();
        } else if ("com.applovin.application_resumed".equals(action)) {
            this.f1888a.c();
        }
    }
}
