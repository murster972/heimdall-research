package com.applovin.impl.sdk.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Looper;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.Window;
import android.view.WindowManager;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.a.h;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.a;
import com.applovin.impl.sdk.l;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkSettings;
import com.facebook.common.time.Clock;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import org.json.JSONObject;

public abstract class r {
    public static double a(long j) {
        return ((double) j) / 1000.0d;
    }

    public static float a(float f) {
        return f * 1000.0f;
    }

    public static int a(JSONObject jSONObject) {
        int b = j.b(jSONObject, "video_completion_percent", -1, (l) null);
        if (b < 0 || b > 100) {
            return 95;
        }
        return b;
    }

    public static long a(l lVar) {
        long longValue = ((Long) lVar.a(b.w3)).longValue();
        long longValue2 = ((Long) lVar.a(b.x3)).longValue();
        long currentTimeMillis = System.currentTimeMillis();
        return (longValue <= 0 || longValue2 <= 0) ? currentTimeMillis : currentTimeMillis + (longValue - longValue2);
    }

    public static Activity a(View view, l lVar) {
        if (view == null) {
            return null;
        }
        int i = 0;
        while (i < 1000) {
            i++;
            try {
                Context context = view.getContext();
                if (context instanceof Activity) {
                    return (Activity) context;
                }
                ViewParent parent = view.getParent();
                if (!(parent instanceof View)) {
                    return null;
                }
                view = (View) parent;
            } catch (Throwable th) {
                lVar.j0().b("Utils", "Encountered error while retrieving activity from view", th);
            }
        }
        return null;
    }

    public static View a(View view) {
        View rootView;
        if (view == null || (rootView = view.getRootView()) == null) {
            return null;
        }
        View findViewById = rootView.findViewById(16908290);
        return findViewById != null ? findViewById : rootView;
    }

    public static d a(JSONObject jSONObject, l lVar) {
        return d.a(AppLovinAdSize.fromString(j.b(jSONObject, "ad_size", (String) null, lVar)), AppLovinAdType.fromString(j.b(jSONObject, "ad_type", (String) null, lVar)), j.b(jSONObject, "zone_id", (String) null, lVar), lVar);
    }

    public static AppLovinAd a(AppLovinAd appLovinAd, l lVar) {
        if (!(appLovinAd instanceof h)) {
            return appLovinAd;
        }
        h hVar = (h) appLovinAd;
        AppLovinAd dequeueAd = lVar.c0().dequeueAd(hVar.getAdZone());
        com.applovin.impl.sdk.r j0 = lVar.j0();
        j0.b("Utils", "Dequeued ad for dummy ad: " + dequeueAd);
        if (dequeueAd == null) {
            return hVar.a();
        }
        hVar.a(dequeueAd);
        ((AppLovinAdBase) dequeueAd).setDummyAd(hVar);
        return dequeueAd;
    }

    public static Object a(Object obj, l lVar) {
        int i;
        if (obj instanceof Map) {
            Map map = (Map) obj;
            HashMap hashMap = new HashMap(map.size());
            for (Map.Entry entry : map.entrySet()) {
                Object key = entry.getKey();
                hashMap.put(key instanceof String ? (String) key : String.valueOf(key), a(entry.getValue(), lVar));
            }
            return hashMap;
        } else if (obj instanceof List) {
            List<Object> list = (List) obj;
            ArrayList arrayList = new ArrayList(list.size());
            for (Object a2 : list) {
                arrayList.add(a(a2, lVar));
            }
            return arrayList;
        } else if (obj instanceof Date) {
            return String.valueOf(((Date) obj).getTime());
        } else {
            String valueOf = String.valueOf(obj);
            if (obj instanceof String) {
                i = ((Integer) lVar.a(b.m0)).intValue();
                if (i <= 0 || valueOf.length() <= i) {
                    return valueOf;
                }
            } else if (!(obj instanceof Uri) || (i = ((Integer) lVar.a(b.n0)).intValue()) <= 0 || valueOf.length() <= i) {
                return valueOf;
            }
            return valueOf.substring(0, i);
        }
    }

    public static String a(String str) {
        return (str == null || str.length() <= 4) ? "NOKEY" : str.substring(str.length() - 4);
    }

    public static String a(Map<String, String> map) {
        if (map == null || map.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (Map.Entry next : map.entrySet()) {
            if (sb.length() > 0) {
                sb.append("&");
            }
            sb.append(next.getKey());
            sb.append('=');
            sb.append(next.getValue());
        }
        return sb.toString();
    }

    public static String a(boolean z, String str) {
        return str.replace("{PLACEMENT}", "").replace("{SOC}", String.valueOf(z));
    }

    public static Field a(Class cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Class superclass = cls.getSuperclass();
            if (superclass == null) {
                return null;
            }
            return a(superclass, str);
        }
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, String str3, l lVar) {
        return a(str, jSONObject, str2, (Map<String, String>) null, str3, lVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, Map<String, String> map, String str3, l lVar) {
        return a(str, jSONObject, str2, map, str3, (Map<String, String>) null, false, lVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, String str2, Map<String, String> map, String str3, Map<String, String> map2, boolean z, l lVar) {
        if (map == null) {
            map = new HashMap<>(1);
        }
        Map<String, String> map3 = map;
        map3.put("{CLCODE}", str2);
        return a(str, jSONObject, map3, str3, map2, z, lVar);
    }

    public static List<a> a(String str, JSONObject jSONObject, Map<String, String> map, String str2, Map<String, String> map2, boolean z, l lVar) {
        JSONObject b = j.b(jSONObject, str, new JSONObject(), lVar);
        ArrayList arrayList = new ArrayList(b.length() + 1);
        if (o.b(str2)) {
            arrayList.add(new a(str2, (String) null, map2, z));
        }
        if (b.length() > 0) {
            Iterator<String> keys = b.keys();
            while (keys.hasNext()) {
                try {
                    String next = keys.next();
                    if (!TextUtils.isEmpty(next)) {
                        String optString = b.optString(next);
                        String a2 = o.a(next, map);
                        if (!TextUtils.isEmpty(optString)) {
                            optString = o.a(optString, map);
                        }
                        arrayList.add(new a(a2, optString, map2, z));
                    }
                } catch (Throwable th) {
                    lVar.j0().b("Utils", "Failed to create and add postback url.", th);
                }
            }
        }
        return arrayList;
    }

    private static List<Class> a(List<String> list, l lVar) {
        if (list == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(list.size());
        for (String next : list) {
            try {
                arrayList.add(Class.forName(next));
            } catch (ClassNotFoundException unused) {
                com.applovin.impl.sdk.r j0 = lVar.j0();
                j0.e("Utils", "Failed to create class for name: " + next);
            }
        }
        return arrayList;
    }

    public static List<Uri> a(boolean z, g gVar, l lVar, Context context) {
        if (gVar instanceof com.applovin.impl.a.a) {
            return Collections.emptyList();
        }
        ArrayList arrayList = new ArrayList();
        for (Uri uri : new ArrayList(gVar.z0())) {
            if (!lVar.x().b(uri.getLastPathSegment(), context)) {
                com.applovin.impl.sdk.r j0 = lVar.j0();
                j0.e("Utils", "Cached HTML asset missing: " + uri);
                arrayList.add(uri);
            }
        }
        if (z) {
            Uri m0 = gVar.m0();
            if (!lVar.x().b(m0.getLastPathSegment(), context)) {
                com.applovin.impl.sdk.r j02 = lVar.j0();
                j02.e("Utils", "Cached video missing: " + m0);
                arrayList.add(m0);
            }
        }
        return arrayList;
    }

    public static Map<String, String> a(AppLovinSdkSettings appLovinSdkSettings) {
        try {
            Field a2 = a((Class) appLovinSdkSettings.getClass(), "metaData");
            if (a2 != null) {
                a2.setAccessible(true);
            }
            return (Map) a2.get(appLovinSdkSettings);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static void a(Closeable closeable, l lVar) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (Throwable th) {
                if (lVar != null) {
                    com.applovin.impl.sdk.r j0 = lVar.j0();
                    j0.b("Utils", "Unable to close stream: " + closeable, th);
                }
            }
        }
    }

    public static void a(String str, MaxAdFormat maxAdFormat, JSONObject jSONObject, l lVar) {
        if (jSONObject.has("no_fill_reason")) {
            Object b = j.b(jSONObject, "no_fill_reason", new Object(), lVar);
            com.applovin.impl.sdk.r.i("AppLovinSdk", "\n**************************************************\nNO FILL received:\n..ID: \"" + str + "\"\n..FORMAT: \"" + maxAdFormat.getLabel() + "\"\n..SDK KEY: \"" + lVar.h0() + "\"\n..PACKAGE NAME: \"" + lVar.i().getPackageName() + "\"\n..Reason: " + b + "\n**************************************************\n");
        }
    }

    public static void a(String str, String str2, Context context) {
        new AlertDialog.Builder(context).setTitle(str).setMessage(str2).setNegativeButton(17039370, (DialogInterface.OnClickListener) null).create().show();
    }

    public static void a(String str, String str2, Map map) {
        if (!TextUtils.isEmpty(str2)) {
            map.put(str, str2);
        }
    }

    public static void a(HttpURLConnection httpURLConnection, l lVar) {
        if (httpURLConnection != null) {
            try {
                httpURLConnection.disconnect();
            } catch (Throwable th) {
                if (lVar != null) {
                    com.applovin.impl.sdk.r j0 = lVar.j0();
                    j0.b("Utils", "Unable to disconnect connection: " + httpURLConnection, th);
                }
            }
        }
    }

    public static boolean a() {
        Context l0 = l.l0();
        if (l0 != null) {
            return c.a(l0).a("applovin.sdk.verbose_logging");
        }
        return false;
    }

    public static boolean a(int i) {
        return i > 0 && i <= 100 && ((int) (Math.random() * 100.0d)) + 1 <= i;
    }

    public static boolean a(long j, long j2) {
        return (j & j2) != 0;
    }

    public static boolean a(Context context) {
        if (context == null) {
            context = l.l0();
        }
        if (context != null) {
            return c.a(context).a("applovin.sdk.verbose_logging", false);
        }
        return false;
    }

    public static boolean a(Context context, Uri uri, l lVar) {
        boolean z;
        try {
            Intent intent = new Intent("android.intent.action.VIEW", uri);
            if (!(context instanceof Activity)) {
                intent.setFlags(268435456);
            }
            if ("market".equals(intent.getScheme())) {
                intent.setPackage("com.android.vending");
            }
            lVar.B().b();
            context.startActivity(intent);
            z = true;
        } catch (Throwable th) {
            com.applovin.impl.sdk.r j0 = lVar.j0();
            j0.b("Utils", "Unable to open \"" + uri + "\".", th);
            z = false;
        }
        if (!z) {
            lVar.B().c();
        }
        return z;
    }

    public static boolean a(View view, Activity activity) {
        View rootView;
        if (!(activity == null || view == null)) {
            Window window = activity.getWindow();
            if (window != null) {
                rootView = window.getDecorView();
            } else {
                View findViewById = activity.findViewById(16908290);
                if (findViewById != null) {
                    rootView = findViewById.getRootView();
                }
            }
            return a(view, rootView);
        }
        return false;
    }

    public static boolean a(View view, View view2) {
        if (view == view2) {
            return true;
        }
        if (view2 instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view2;
            for (int i = 0; i < viewGroup.getChildCount(); i++) {
                if (a(view, viewGroup.getChildAt(i))) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean a(AppLovinAdSize appLovinAdSize) {
        return appLovinAdSize == AppLovinAdSize.BANNER || appLovinAdSize == AppLovinAdSize.MREC || appLovinAdSize == AppLovinAdSize.LEADER;
    }

    public static boolean a(Object obj, List<String> list, l lVar) {
        if (list == null) {
            return false;
        }
        for (Class isInstance : a(list, lVar)) {
            if (isInstance.isInstance(obj)) {
                if (obj instanceof Map) {
                    for (Map.Entry entry : ((Map) obj).entrySet()) {
                        if (!(entry.getKey() instanceof String)) {
                            lVar.j0().b("Utils", "Invalid key type used. Map keys should be of type String.");
                            return false;
                        } else if (!a(entry.getValue(), list, lVar)) {
                            return false;
                        }
                    }
                    return true;
                } else if (!(obj instanceof List)) {
                    return true;
                } else {
                    for (Object a2 : (List) obj) {
                        if (!a(a2, list, lVar)) {
                            return false;
                        }
                    }
                    return true;
                }
            }
        }
        com.applovin.impl.sdk.r j0 = lVar.j0();
        j0.b("Utils", "Object '" + obj + "' does not match any of the required types '" + list + "'.");
        return false;
    }

    public static boolean a(String str, List<String> list) {
        for (String startsWith : list) {
            if (str.startsWith(startsWith)) {
                return true;
            }
        }
        return false;
    }

    public static byte[] a(byte[] bArr) throws IOException {
        if (bArr == null || bArr.length == 0) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bArr.length);
        GZIPOutputStream gZIPOutputStream = new GZIPOutputStream(byteArrayOutputStream);
        gZIPOutputStream.write(bArr);
        gZIPOutputStream.close();
        return byteArrayOutputStream.toByteArray();
    }

    public static int b(Context context) {
        Resources resources;
        Configuration configuration;
        if (context == null || (resources = context.getResources()) == null || (configuration = resources.getConfiguration()) == null) {
            return 0;
        }
        return configuration.orientation;
    }

    public static long b(float f) {
        return c(a(f));
    }

    public static String b(Class cls, String str) {
        try {
            Field a2 = a(cls, str);
            a2.setAccessible(true);
            return (String) a2.get((Object) null);
        } catch (Throwable unused) {
            return null;
        }
    }

    public static String b(String str) {
        return a(false, str);
    }

    public static Map<String, String> b(Map<String, Object> map) {
        HashMap hashMap = new HashMap();
        if (map != null && !map.isEmpty()) {
            for (Map.Entry next : map.entrySet()) {
                hashMap.put(next.getKey(), String.valueOf(next.getValue()));
            }
        }
        return hashMap;
    }

    public static void b(AppLovinAd appLovinAd, l lVar) {
        if (appLovinAd instanceof AppLovinAdBase) {
            String h0 = lVar.h0();
            String h02 = ((AppLovinAdBase) appLovinAd).getSdk().h0();
            if (!h0.equals(h02)) {
                com.applovin.impl.sdk.r.i("AppLovinAd", "Ad was loaded from sdk with key: " + h02 + ", but is being rendered from sdk with key: " + h0);
                lVar.p().a(com.applovin.impl.sdk.d.g.o);
            }
        }
    }

    public static void b(String str, String str2, Map<String, Object> map) {
        if (map.containsKey(str)) {
            map.put(str2, map.get(str));
            map.remove(str);
        }
    }

    public static boolean b() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    private static long c(float f) {
        return (long) Math.round(f);
    }

    public static MaxAdFormat c(String str) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        if (str.equalsIgnoreCase("banner")) {
            return MaxAdFormat.BANNER;
        }
        if (str.equalsIgnoreCase("mrec")) {
            return MaxAdFormat.MREC;
        }
        if (str.equalsIgnoreCase("leaderboard") || str.equalsIgnoreCase("leader")) {
            return MaxAdFormat.LEADER;
        }
        if (str.equalsIgnoreCase("interstitial") || str.equalsIgnoreCase("inter")) {
            return MaxAdFormat.INTERSTITIAL;
        }
        if (str.equalsIgnoreCase("rewarded") || str.equalsIgnoreCase("reward")) {
            return MaxAdFormat.REWARDED;
        }
        if (str.equalsIgnoreCase("rewarded_inter") || str.equalsIgnoreCase("rewarded_interstitial")) {
            return MaxAdFormat.REWARDED_INTERSTITIAL;
        }
        com.applovin.impl.sdk.r.i("AppLovinSdk", "Unknown ad format: " + str);
        return null;
    }

    public static String c(Context context) {
        Intent intent = new Intent("android.intent.action.MAIN");
        intent.setPackage(context.getPackageName());
        List<ResolveInfo> queryIntentActivities = context.getPackageManager().queryIntentActivities(intent, 0);
        if (!queryIntentActivities.isEmpty()) {
            return queryIntentActivities.get(0).activityInfo.name;
        }
        return null;
    }

    public static Map<String, String> c(Map<String, String> map) {
        HashMap hashMap = new HashMap(map);
        for (String str : hashMap.keySet()) {
            String str2 = (String) hashMap.get(str);
            if (str2 != null) {
                hashMap.put(str, o.f(str2));
            }
        }
        return hashMap;
    }

    public static boolean c() {
        ActivityManager.RunningAppProcessInfo runningAppProcessInfo = new ActivityManager.RunningAppProcessInfo();
        try {
            ActivityManager.getMyMemoryState(runningAppProcessInfo);
        } catch (Throwable th) {
            com.applovin.impl.sdk.r.c("Utils", "Exception thrown while getting memory state.", th);
        }
        int i = runningAppProcessInfo.importance;
        return i == 100 || i == 200;
    }

    public static int d(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService("window");
        if (windowManager == null) {
            return 0;
        }
        return windowManager.getDefaultDisplay().getRotation();
    }

    public static String d(String str) {
        Uri parse = Uri.parse(str);
        return new Uri.Builder().scheme(parse.getScheme()).authority(parse.getAuthority()).path(parse.getPath()).build().toString();
    }

    /* JADX WARNING: Removed duplicated region for block: B:4:0x000a A[Catch:{ all -> 0x002e }] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean d() {
        /*
            java.util.Enumeration r0 = java.net.NetworkInterface.getNetworkInterfaces()     // Catch:{ all -> 0x002e }
        L_0x0004:
            boolean r1 = r0.hasMoreElements()     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0036
            java.lang.Object r1 = r0.nextElement()     // Catch:{ all -> 0x002e }
            java.net.NetworkInterface r1 = (java.net.NetworkInterface) r1     // Catch:{ all -> 0x002e }
            java.lang.String r1 = r1.getDisplayName()     // Catch:{ all -> 0x002e }
            java.lang.String r2 = "tun"
            boolean r2 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ppp"
            boolean r2 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r2 != 0) goto L_0x002c
            java.lang.String r2 = "ipsec"
            boolean r1 = r1.contains(r2)     // Catch:{ all -> 0x002e }
            if (r1 == 0) goto L_0x0004
        L_0x002c:
            r0 = 1
            return r0
        L_0x002e:
            r0 = move-exception
            java.lang.String r1 = "Utils"
            java.lang.String r2 = "Unable to check Network Interfaces"
            com.applovin.impl.sdk.r.c(r1, r2, r0)
        L_0x0036:
            r0 = 0
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.r.d():boolean");
    }

    public static String e() {
        try {
            for (Field field : Build.VERSION_CODES.class.getFields()) {
                if (field.getInt((Object) null) == Build.VERSION.SDK_INT) {
                    return field.getName();
                }
            }
            return "";
        } catch (Throwable th) {
            com.applovin.impl.sdk.r.c("Utils", "Unable to get Android SDK codename", th);
            return "";
        }
    }

    public static boolean e(Context context) {
        return (context.getApplicationInfo().flags & 2) != 0;
    }

    public static boolean e(String str) {
        if (TextUtils.isEmpty(str)) {
            return false;
        }
        try {
            Class.forName(str);
            return true;
        } catch (Throwable unused) {
            return false;
        }
    }

    public static long f(String str) {
        if (!o.b(str)) {
            return Clock.MAX_TIME;
        }
        try {
            return (long) Color.parseColor(str);
        } catch (Throwable unused) {
            return Clock.MAX_TIME;
        }
    }

    public static String f() {
        Class<?> cls;
        try {
            cls = Class.forName("com.applovin.quality.AppLovinQualityService");
        } catch (Throwable unused) {
            return "";
        }
        return (String) cls.getMethod("getVersion", new Class[0]).invoke((Object) null, new Object[0]);
    }

    public static boolean f(Context context) {
        return c.a(context).a("applovin.sdk.is_test_environment");
    }

    public static int g() {
        try {
            Field a2 = a((Class) Class.forName("com.google.android.exoplayer2.ExoPlayerLibraryInfo"), "VERSION_INT");
            a2.setAccessible(true);
            return ((Integer) a2.get((Object) null)).intValue();
        } catch (Exception unused) {
            return -1;
        }
    }

    public static int g(String str) {
        int i = 0;
        for (String str2 : str.replaceAll("-beta", ".").split("\\.")) {
            if (str2.length() > 2) {
                com.applovin.impl.sdk.r.i("Utils", "Version number components cannot be longer than two digits -> " + str);
                return i;
            }
            i = (i * 100) + Integer.parseInt(str2);
        }
        return !str.contains("-beta") ? (i * 100) + 99 : i;
    }

    public static boolean h() {
        try {
            Class.forName("com.applovin.sdk.AppLovinSdk");
            return false;
        } catch (ClassNotFoundException unused) {
            return true;
        }
    }
}
