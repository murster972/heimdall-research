package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.security.NetworkSecurityPolicy;
import android.text.TextUtils;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.c.c;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class h {

    /* renamed from: a  reason: collision with root package name */
    private static final int[] f1890a = {7, 4, 2, 1, 11};
    private static final int[] b = {5, 6, 10, 3, 9, 8, 14};
    private static final int[] c = {15, 12, 13};
    private static final int[] d = {20};

    public static String a(InputStream inputStream, l lVar) throws IOException {
        if (inputStream == null) {
            return null;
        }
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] bArr = new byte[((Integer) lVar.a(b.h2)).intValue()];
        while (true) {
            int read = inputStream.read(bArr);
            if (read <= 0) {
                return byteArrayOutputStream.toString("UTF-8");
            }
            byteArrayOutputStream.write(bArr, 0, read);
        }
    }

    public static String a(String str, l lVar) {
        return a((String) lVar.a(b.Y), str, lVar);
    }

    public static String a(String str, String str2, l lVar) {
        if (str == null || str.length() < 4) {
            throw new IllegalArgumentException("Invalid domain specified");
        } else if (str2 == null) {
            throw new IllegalArgumentException("No endpoint specified");
        } else if (lVar != null) {
            return str + str2;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static Map<String, String> a(l lVar) {
        String str;
        HashMap hashMap = new HashMap();
        String str2 = (String) lVar.a(b.h);
        if (o.b(str2)) {
            str = "device_token";
        } else {
            if (!((Boolean) lVar.a(b.u3)).booleanValue()) {
                str2 = lVar.h0();
                str = "api_key";
            }
            hashMap.putAll(r.b(lVar.r().h()));
            return hashMap;
        }
        hashMap.put(str, str2);
        hashMap.putAll(r.b(lVar.r().h()));
        return hashMap;
    }

    public static JSONObject a(JSONObject jSONObject) throws JSONException {
        return (JSONObject) jSONObject.getJSONArray("results").get(0);
    }

    public static void a(int i, l lVar) {
        StringBuilder sb;
        String str;
        c h = lVar.h();
        if (i == 401) {
            h.a((b<?>) b.f, (Object) "");
            h.a((b<?>) b.h, (Object) "");
            h.a();
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(lVar.h0());
            str = "\" is rejected by AppLovin. Please make sure the SDK key is correct.";
        } else if (i == 418) {
            h.a((b<?>) b.e, (Object) true);
            h.a();
            sb = new StringBuilder();
            sb.append("SDK key \"");
            sb.append(lVar.h0());
            str = "\" has been blocked. Please contact AppLovin support at support@applovin.com.";
        } else {
            if (i < 400 || i >= 500) {
                if (i != -1 || !((Boolean) lVar.a(b.g)).booleanValue()) {
                    return;
                }
            } else if (!((Boolean) lVar.a(b.g)).booleanValue()) {
                return;
            }
            lVar.T();
            return;
        }
        sb.append(str);
        r.i("AppLovinSdk", sb.toString());
    }

    public static void a(JSONObject jSONObject, l lVar) {
        String b2 = j.b(jSONObject, "persisted_data", (String) null, lVar);
        if (o.b(b2)) {
            lVar.a(d.z, b2);
            lVar.j0().c("ConnectionUtils", "Updated persisted data");
        }
    }

    public static void a(JSONObject jSONObject, boolean z, l lVar) {
        lVar.F().a(jSONObject, z);
    }

    public static boolean a() {
        return a((String) null);
    }

    private static boolean a(int i, int[] iArr) {
        for (int i2 : iArr) {
            if (i2 == i) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        if (context.getSystemService("connectivity") == null) {
            return true;
        }
        NetworkInfo b2 = b(context);
        if (b2 != null) {
            return b2.isConnected();
        }
        return false;
    }

    public static boolean a(String str) {
        if (g.e()) {
            return (!g.f() || TextUtils.isEmpty(str)) ? NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted() : NetworkSecurityPolicy.getInstance().isCleartextTrafficPermitted(str);
        }
        return true;
    }

    private static NetworkInfo b(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService("connectivity");
        if (connectivityManager != null) {
            return connectivityManager.getActiveNetworkInfo();
        }
        return null;
    }

    public static String b(l lVar) {
        NetworkInfo b2 = b(lVar.i());
        if (b2 == null) {
            return "unknown";
        }
        int type = b2.getType();
        int subtype = b2.getSubtype();
        return type == 1 ? "wifi" : type == 0 ? a(subtype, f1890a) ? "2g" : a(subtype, b) ? "3g" : a(subtype, c) ? "4g" : a(subtype, d) ? "5g" : "mobile" : "unknown";
    }

    public static String b(String str, l lVar) {
        return a((String) lVar.a(b.c0), str, lVar);
    }

    public static void b(JSONObject jSONObject, l lVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (lVar != null) {
            try {
                if (jSONObject.has("settings")) {
                    c h = lVar.h();
                    if (!jSONObject.isNull("settings")) {
                        h.a(jSONObject.getJSONObject("settings"));
                        h.a();
                    }
                }
            } catch (JSONException e) {
                lVar.j0().b("ConnectionUtils", "Unable to parse settings out of API response", e);
            }
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    public static String c(l lVar) {
        return a((String) lVar.a(b.W), ((Boolean) lVar.a(b.p2)).booleanValue() ? "5.0/ad" : "4.0/ad", lVar);
    }

    public static void c(JSONObject jSONObject, l lVar) {
        JSONObject b2 = j.b(jSONObject, "filesystem_values", (JSONObject) null, lVar);
        if (b2 != null) {
            SharedPreferences.Editor edit = PreferenceManager.getDefaultSharedPreferences(lVar.i()).edit();
            Iterator<String> keys = b2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                Object b3 = j.b(b2, next, (Object) null, lVar);
                if (b3 != null) {
                    e.a(next, b3, (SharedPreferences) null, edit);
                }
            }
            edit.apply();
        }
    }

    public static String d(l lVar) {
        return a((String) lVar.a(b.X), ((Boolean) lVar.a(b.p2)).booleanValue() ? "5.0/ad" : "4.0/ad", lVar);
    }

    public static void d(JSONObject jSONObject, l lVar) {
        JSONArray b2 = j.b(jSONObject, "zones", (JSONArray) null, lVar);
        if (b2 != null && b2.length() > 0) {
            lVar.y().a(b2);
        }
    }

    public static String e(l lVar) {
        return a((String) lVar.a(b.f0), "1.0/variable_config", lVar);
    }

    public static void e(JSONObject jSONObject, l lVar) {
        JSONArray b2 = j.b(jSONObject, "zones", (JSONArray) null, lVar);
        if (b2 != null) {
            lVar.y().b(b2);
        }
    }

    public static String f(l lVar) {
        return a((String) lVar.a(b.g0), "1.0/variable_config", lVar);
    }

    public static void f(JSONObject jSONObject, l lVar) {
        JSONObject b2 = j.b(jSONObject, "variables", (JSONObject) null, lVar);
        if (b2 != null) {
            lVar.g0().updateVariables(b2);
        }
    }
}
