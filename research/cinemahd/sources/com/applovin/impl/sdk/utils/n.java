package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class n implements SensorEventListener, AppLovinBroadcastManager.Receiver {

    /* renamed from: a  reason: collision with root package name */
    private final int f1920a;
    private final float b;
    private final SensorManager c;
    private final Sensor d = this.c.getDefaultSensor(9);
    private final Sensor e = this.c.getDefaultSensor(4);
    private final l f;
    private float[] g;
    private float h;

    public n(l lVar) {
        this.f = lVar;
        this.c = (SensorManager) lVar.i().getSystemService("sensor");
        this.f1920a = ((Integer) lVar.a(b.X2)).intValue();
        this.b = ((Float) lVar.a(b.W2)).floatValue();
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        lVar.I().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    public void a() {
        this.c.unregisterListener(this);
        if (((Boolean) this.f.h().a(b.U2)).booleanValue()) {
            this.c.registerListener(this, this.d, (int) TimeUnit.MILLISECONDS.toMicros((long) this.f1920a));
        }
        if (((Boolean) this.f.h().a(b.V2)).booleanValue()) {
            this.c.registerListener(this, this.e, (int) TimeUnit.MILLISECONDS.toMicros((long) this.f1920a));
        }
    }

    public float b() {
        return this.h;
    }

    public float c() {
        float[] fArr = this.g;
        if (fArr == null) {
            return 0.0f;
        }
        return (float) Math.toDegrees(Math.acos((double) (fArr[2] / 9.81f)));
    }

    public void onAccuracyChanged(Sensor sensor, int i) {
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.c.unregisterListener(this);
        } else if ("com.applovin.application_resumed".equals(action)) {
            a();
        }
    }

    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == 9) {
            this.g = sensorEvent.values;
        } else if (sensorEvent.sensor.getType() == 4) {
            this.h *= this.b;
            this.h += Math.abs(sensorEvent.values[0]) + Math.abs(sensorEvent.values[1]) + Math.abs(sensorEvent.values[2]);
        }
    }
}
