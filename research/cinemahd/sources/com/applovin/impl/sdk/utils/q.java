package com.applovin.impl.sdk.utils;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.c.d;
import com.applovin.impl.sdk.c.e;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.vungle.warren.model.ReportDBAdapter;

public final class q {

    /* renamed from: a  reason: collision with root package name */
    private final l f1924a;
    private String b;
    private final String c;
    private final String d;

    public q(l lVar) {
        this.f1924a = lVar;
        this.c = a(d.h, (String) e.b(d.g, null, lVar.i()));
        this.d = a(d.i, (String) lVar.a(b.f));
        a(d());
    }

    /* JADX WARNING: type inference failed for: r3v0, types: [com.applovin.impl.sdk.c.d, com.applovin.impl.sdk.c.d<java.lang.String>] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private java.lang.String a(com.applovin.impl.sdk.c.d<java.lang.String> r3, java.lang.String r4) {
        /*
            r2 = this;
            com.applovin.impl.sdk.l r0 = r2.f1924a
            android.content.Context r0 = r0.i()
            r1 = 0
            java.lang.Object r0 = com.applovin.impl.sdk.c.e.b(r3, r1, (android.content.Context) r0)
            java.lang.String r0 = (java.lang.String) r0
            boolean r1 = com.applovin.impl.sdk.utils.o.b(r0)
            if (r1 == 0) goto L_0x0014
            return r0
        L_0x0014:
            boolean r0 = com.applovin.impl.sdk.utils.o.b(r4)
            if (r0 == 0) goto L_0x001b
            goto L_0x0029
        L_0x001b:
            java.util.UUID r4 = java.util.UUID.randomUUID()
            java.lang.String r4 = r4.toString()
            java.util.Locale r0 = java.util.Locale.US
            java.lang.String r4 = r4.toLowerCase(r0)
        L_0x0029:
            com.applovin.impl.sdk.l r0 = r2.f1924a
            android.content.Context r0 = r0.i()
            com.applovin.impl.sdk.c.e.a(r3, r4, (android.content.Context) r0)
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.utils.q.a(com.applovin.impl.sdk.c.d, java.lang.String):java.lang.String");
    }

    public static String a(l lVar) {
        String str = (String) lVar.a(d.j);
        if (!TextUtils.isEmpty(str)) {
            return str;
        }
        String valueOf = String.valueOf(((int) (Math.random() * 100.0d)) + 1);
        lVar.a(d.j, valueOf);
        return valueOf;
    }

    private String d() {
        if (!((Boolean) this.f1924a.a(b.C2)).booleanValue()) {
            this.f1924a.b(d.f);
        }
        String str = (String) this.f1924a.a(d.f);
        if (!o.b(str)) {
            return null;
        }
        r j0 = this.f1924a.j0();
        j0.b("AppLovinSdk", "Using identifier (" + str + ") from previous session");
        return str;
    }

    public String a() {
        return this.b;
    }

    public void a(String str) {
        if (((Boolean) this.f1924a.a(b.C2)).booleanValue()) {
            this.f1924a.a(d.f, str);
        }
        this.b = str;
        Bundle bundle = new Bundle();
        bundle.putString(ReportDBAdapter.ReportColumns.COLUMN_USER_ID, o.c(str));
        bundle.putString("applovin_random_token", c());
        this.f1924a.F().a(str, "user_info");
    }

    public String b() {
        return this.c;
    }

    public String c() {
        return this.d;
    }
}
