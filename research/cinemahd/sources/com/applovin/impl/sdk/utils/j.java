package com.applovin.impl.sdk.utils;

import android.os.Bundle;
import android.text.TextUtils;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class j {
    public static float a(JSONObject jSONObject, String str, float f, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return f;
        }
        try {
            double d = jSONObject.getDouble(str);
            return (-3.4028234663852886E38d >= d || d >= 3.4028234663852886E38d) ? f : (float) d;
        } catch (JSONException e) {
            if (lVar == null) {
                return f;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve float property for key = " + str, e);
            return f;
        }
    }

    public static long a(JSONObject jSONObject, String str, long j, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return j;
        }
        try {
            return jSONObject.getLong(str);
        } catch (JSONException e) {
            if (lVar == null) {
                return j;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve int property for key = " + str, e);
            return j;
        }
    }

    public static Bundle a(Object obj) {
        JSONObject jSONObject;
        if (obj instanceof JSONObject) {
            jSONObject = (JSONObject) obj;
        } else {
            if (obj instanceof String) {
                try {
                    jSONObject = new JSONObject((String) obj);
                } catch (JSONException unused) {
                }
            }
            jSONObject = null;
        }
        return c(jSONObject);
    }

    public static Boolean a(JSONObject jSONObject, String str, Boolean bool, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return bool;
        }
        try {
            return Boolean.valueOf(jSONObject.getBoolean(str));
        } catch (JSONException unused) {
            boolean z = true;
            if (b(jSONObject, str, (bool == null || !bool.booleanValue()) ? 0 : 1, lVar) <= 0) {
                z = false;
            }
            return Boolean.valueOf(z);
        }
    }

    public static Float a(JSONObject jSONObject, String str, Float f, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return f;
        }
        try {
            double d = jSONObject.getDouble(str);
            return (-3.4028234663852886E38d >= d || d >= 3.4028234663852886E38d) ? f : Float.valueOf((float) d);
        } catch (JSONException e) {
            if (lVar == null) {
                return f;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve float property for key = " + str, e);
            return f;
        }
    }

    public static Object a(JSONArray jSONArray, int i, Object obj, l lVar) {
        if (jSONArray == null || jSONArray.length() <= i) {
            return obj;
        }
        try {
            return jSONArray.get(i);
        } catch (JSONException e) {
            if (lVar == null) {
                return obj;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve object at index " + i + " for JSON array", e);
            return obj;
        }
    }

    public static String a(Map<String, Object> map, String str, l lVar) {
        try {
            return a((Map<String, ?>) map).toString();
        } catch (JSONException e) {
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to convert map '" + map + "' to JSON string.", e);
            return str;
        }
    }

    public static ArrayList<Bundle> a(JSONArray jSONArray) {
        if (jSONArray == null || jSONArray.length() == 0) {
            return new ArrayList<>();
        }
        ArrayList<Bundle> arrayList = new ArrayList<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(c(jSONArray.optJSONObject(i)));
        }
        return arrayList;
    }

    public static <T> List<T> a(JSONArray jSONArray, List<T> list) {
        try {
            return b(jSONArray, list);
        } catch (JSONException unused) {
            return list;
        }
    }

    public static List a(JSONObject jSONObject, String str, List list, l lVar) {
        try {
            JSONArray b = b(jSONObject, str, (JSONArray) null, lVar);
            return b != null ? b(b) : list;
        } catch (JSONException unused) {
            return list;
        }
    }

    public static Map<String, String> a(Bundle bundle) {
        if (bundle == null) {
            return Collections.emptyMap();
        }
        HashMap hashMap = new HashMap(bundle.size());
        for (String str : bundle.keySet()) {
            hashMap.put(str, String.valueOf(bundle.get(str)));
        }
        return hashMap;
    }

    public static Map<String, Object> a(String str, Map<String, Object> map, l lVar) {
        try {
            return b(new JSONObject(str));
        } catch (JSONException e) {
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to convert json string '" + str + "' to map", e);
            return map;
        }
    }

    public static Map<String, String> a(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, c(jSONObject.get(next)).toString());
        }
        return hashMap;
    }

    public static JSONObject a(String str, l lVar) {
        if (TextUtils.isEmpty(str)) {
            return null;
        }
        try {
            return new JSONObject(str);
        } catch (Throwable unused) {
            r j0 = lVar.j0();
            j0.e("JsonUtils", "Failed to deserialize into JSON: " + str);
            return null;
        }
    }

    public static JSONObject a(String str, JSONObject jSONObject, l lVar) {
        try {
            return new JSONObject(str);
        } catch (JSONException e) {
            if (lVar != null) {
                r j0 = lVar.j0();
                j0.b("JsonUtils", "Failed to convert JSON string '" + str + "' to JSONObject", e);
            }
            return jSONObject;
        }
    }

    public static JSONObject a(Map<String, ?> map) throws JSONException {
        if (map == null) {
            return new JSONObject();
        }
        JSONObject jSONObject = new JSONObject();
        for (Map.Entry next : map.entrySet()) {
            jSONObject.put((String) next.getKey(), next.getValue());
        }
        return jSONObject;
    }

    public static JSONObject a(JSONArray jSONArray, int i, JSONObject jSONObject, l lVar) {
        if (jSONArray == null || i >= jSONArray.length()) {
            return jSONObject;
        }
        try {
            return jSONArray.getJSONObject(i);
        } catch (JSONException e) {
            if (lVar == null) {
                return jSONObject;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve JSON object from array for index = " + i, e);
            return jSONObject;
        }
    }

    public static void a(JSONObject jSONObject, String str, int i, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, i);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put int property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, Object obj, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, obj);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put Object property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, String str2, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, str2);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put String property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, JSONArray jSONArray, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, jSONArray);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put JSONArray property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, JSONObject jSONObject2, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, jSONObject2);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put JSON property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String str, boolean z, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, z);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put boolean property for key = " + str, e);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, JSONObject jSONObject2, l lVar) {
        if (jSONObject != null && jSONObject2 != null) {
            Iterator<String> keys = jSONObject2.keys();
            while (keys.hasNext()) {
                String next = keys.next();
                Object b = b(jSONObject2, next, (Object) null, lVar);
                if (b != null) {
                    a(jSONObject, next, b, lVar);
                }
            }
        }
    }

    public static void a(JSONObject jSONObject, String[] strArr) {
        for (String remove : strArr) {
            jSONObject.remove(remove);
        }
    }

    public static boolean a(String str, JSONArray jSONArray) {
        int i = 0;
        while (i < jSONArray.length()) {
            try {
                Object obj = jSONArray.get(i);
                if ((obj instanceof String) && ((String) obj).equalsIgnoreCase(str)) {
                    return true;
                }
                i++;
            } catch (JSONException unused) {
            }
        }
        return false;
    }

    public static boolean a(JSONObject jSONObject, String str) {
        return jSONObject != null && jSONObject.has(str);
    }

    public static int b(JSONObject jSONObject, String str, int i, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return i;
        }
        try {
            return jSONObject.getInt(str);
        } catch (JSONException e) {
            if (lVar == null) {
                return i;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve int property for key = " + str, e);
            return i;
        }
    }

    public static Object b(JSONObject jSONObject, String str, Object obj, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return obj;
        }
        try {
            Object obj2 = jSONObject.get(str);
            return obj2 != null ? obj2 : obj;
        } catch (JSONException e) {
            if (lVar == null) {
                return obj;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve Object for key = " + str, e);
            return obj;
        }
    }

    public static String b(JSONObject jSONObject, String str, String str2, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return str2;
        }
        try {
            return jSONObject.getString(str);
        } catch (JSONException e) {
            if (lVar == null) {
                return str2;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve string property for key = " + str, e);
            return str2;
        }
    }

    public static <T> List<T> b(JSONArray jSONArray) throws JSONException {
        return b(jSONArray, new ArrayList());
    }

    private static <T> List<T> b(JSONArray jSONArray, List<T> list) throws JSONException {
        if (jSONArray == null) {
            return list;
        }
        ArrayList arrayList = new ArrayList(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            arrayList.add(c(jSONArray.get(i)));
        }
        return arrayList;
    }

    public static Map<String, Object> b(JSONObject jSONObject) throws JSONException {
        HashMap hashMap = new HashMap();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            hashMap.put(next, c(jSONObject.get(next)));
        }
        return hashMap;
    }

    public static JSONArray b(Object obj) {
        if (obj == null) {
            return new JSONArray();
        }
        JSONArray jSONArray = new JSONArray();
        jSONArray.put(obj);
        return jSONArray;
    }

    public static JSONArray b(JSONObject jSONObject, String str, JSONArray jSONArray, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return jSONArray;
        }
        try {
            return jSONObject.getJSONArray(str);
        } catch (JSONException e) {
            if (lVar == null) {
                return jSONArray;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve JSON array for key = " + str, e);
            return jSONArray;
        }
    }

    public static JSONObject b(JSONObject jSONObject, String str, JSONObject jSONObject2, l lVar) {
        if (jSONObject == null || !jSONObject.has(str)) {
            return jSONObject2;
        }
        try {
            return jSONObject.getJSONObject(str);
        } catch (JSONException e) {
            if (lVar == null) {
                return jSONObject2;
            }
            r j0 = lVar.j0();
            j0.b("JsonUtils", "Failed to retrieve JSON property for key = " + str, e);
            return jSONObject2;
        }
    }

    public static void b(JSONObject jSONObject, String str, long j, l lVar) {
        if (jSONObject != null) {
            try {
                jSONObject.put(str, j);
            } catch (JSONException e) {
                if (lVar != null) {
                    r j0 = lVar.j0();
                    j0.b("JsonUtils", "Failed to put long property for key = " + str, e);
                }
            }
        }
    }

    public static Bundle c(JSONObject jSONObject) {
        if (jSONObject == null || jSONObject.length() == 0) {
            return new Bundle();
        }
        Bundle bundle = new Bundle();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            if (jSONObject.isNull(next)) {
                bundle.putString(next, (String) null);
            } else {
                Object opt = jSONObject.opt(next);
                if (opt instanceof JSONObject) {
                    bundle.putBundle(next, c((JSONObject) opt));
                } else if (opt instanceof JSONArray) {
                    JSONArray jSONArray = (JSONArray) opt;
                    if (jSONArray.length() == 0) {
                        bundle.putStringArrayList(next, new ArrayList(0));
                    } else if (a(jSONArray, 0, (Object) null, (l) null) instanceof String) {
                        ArrayList arrayList = new ArrayList(jSONArray.length());
                        for (int i = 0; i < jSONArray.length(); i++) {
                            arrayList.add((String) a(jSONArray, i, (Object) null, (l) null));
                        }
                        bundle.putStringArrayList(next, arrayList);
                    } else {
                        bundle.putParcelableArrayList(next, a(jSONArray));
                    }
                } else if (opt instanceof Boolean) {
                    bundle.putBoolean(next, ((Boolean) opt).booleanValue());
                } else if (opt instanceof String) {
                    bundle.putString(next, (String) opt);
                } else if (opt instanceof Integer) {
                    bundle.putInt(next, ((Integer) opt).intValue());
                } else if (opt instanceof Long) {
                    bundle.putLong(next, ((Long) opt).longValue());
                } else if (opt instanceof Double) {
                    bundle.putDouble(next, ((Double) opt).doubleValue());
                }
            }
        }
        return bundle;
    }

    private static Object c(Object obj) throws JSONException {
        if (obj == JSONObject.NULL) {
            return null;
        }
        return obj instanceof JSONObject ? b((JSONObject) obj) : obj instanceof JSONArray ? b((JSONArray) obj) : obj;
    }

    public static JSONObject d(JSONObject jSONObject) {
        JSONObject jSONObject2 = new JSONObject();
        Iterator<String> keys = jSONObject.keys();
        while (keys.hasNext()) {
            String next = keys.next();
            try {
                jSONObject2.put(next, jSONObject.get(next));
            } catch (JSONException unused) {
                r.h("JsonUtils", "Failed to copy over item for key '" + next + "' to JSONObject copy");
            }
        }
        return jSONObject2;
    }

    public static String e(JSONObject jSONObject) {
        if (jSONObject == null) {
            return null;
        }
        try {
            return jSONObject.toString(4);
        } catch (JSONException unused) {
            return jSONObject.toString();
        }
    }
}
