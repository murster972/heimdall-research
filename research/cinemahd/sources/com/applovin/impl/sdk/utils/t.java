package com.applovin.impl.sdk.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class t {
    public static final t e = new t();

    /* renamed from: a  reason: collision with root package name */
    private final String f1926a;
    private final Map<String, String> b;
    protected String c;
    protected final List<t> d;

    private t() {
        this.f1926a = "";
        this.b = Collections.emptyMap();
        this.c = "";
        this.d = Collections.emptyList();
    }

    public t(String str, Map<String, String> map, t tVar) {
        this.f1926a = str;
        this.b = Collections.unmodifiableMap(map);
        this.d = new ArrayList();
    }

    public String a() {
        return this.f1926a;
    }

    public List<t> a(String str) {
        if (str != null) {
            ArrayList arrayList = new ArrayList(this.d.size());
            for (t next : this.d) {
                if (str.equalsIgnoreCase(next.a())) {
                    arrayList.add(next);
                }
            }
            return arrayList;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public t b(String str) {
        if (str != null) {
            for (t next : this.d) {
                if (str.equalsIgnoreCase(next.a())) {
                    return next;
                }
            }
            return null;
        }
        throw new IllegalArgumentException("No name specified.");
    }

    public Map<String, String> b() {
        return this.b;
    }

    public t c(String str) {
        if (str == null) {
            throw new IllegalArgumentException("No name specified.");
        } else if (this.d.size() <= 0) {
            return null;
        } else {
            ArrayList arrayList = new ArrayList();
            arrayList.add(this);
            while (!arrayList.isEmpty()) {
                t tVar = (t) arrayList.get(0);
                arrayList.remove(0);
                if (str.equalsIgnoreCase(tVar.a())) {
                    return tVar;
                }
                arrayList.addAll(tVar.d());
            }
            return null;
        }
    }

    public String c() {
        return this.c;
    }

    public List<t> d() {
        return Collections.unmodifiableList(this.d);
    }

    public String toString() {
        return "XmlNode{elementName='" + this.f1926a + '\'' + ", text='" + this.c + '\'' + ", attributes=" + this.b + '}';
    }
}
