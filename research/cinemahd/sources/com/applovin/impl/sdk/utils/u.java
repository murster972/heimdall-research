package com.applovin.impl.sdk.utils;

import android.util.Xml;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;

public class u {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final r f1927a;
    /* access modifiers changed from: private */
    public Stack<a> b;
    /* access modifiers changed from: private */
    public StringBuilder c;
    /* access modifiers changed from: private */
    public long d;
    /* access modifiers changed from: private */
    public a e;

    private static class a extends t {
        a(String str, Map<String, String> map, t tVar) {
            super(str, map, tVar);
        }

        /* access modifiers changed from: package-private */
        public void a(t tVar) {
            if (tVar != null) {
                this.d.add(tVar);
                return;
            }
            throw new IllegalArgumentException("None specified.");
        }

        /* access modifiers changed from: package-private */
        public void d(String str) {
            this.c = str;
        }
    }

    u(l lVar) {
        if (lVar != null) {
            this.f1927a = lVar.j0();
            return;
        }
        throw new IllegalArgumentException("No sdk specified.");
    }

    public static t a(String str, l lVar) throws SAXException {
        return new u(lVar).a(str);
    }

    /* access modifiers changed from: private */
    public Map<String, String> a(Attributes attributes) {
        if (attributes == null) {
            return Collections.emptyMap();
        }
        int length = attributes.getLength();
        HashMap hashMap = new HashMap(length);
        for (int i = 0; i < length; i++) {
            hashMap.put(attributes.getQName(i), attributes.getValue(i));
        }
        return hashMap;
    }

    public t a(String str) throws SAXException {
        if (str != null) {
            this.c = new StringBuilder();
            this.b = new Stack<>();
            this.e = null;
            Xml.parse(str, new ContentHandler() {
                public void characters(char[] cArr, int i, int i2) {
                    String trim = new String(Arrays.copyOfRange(cArr, i, i2)).trim();
                    if (o.b(trim)) {
                        u.this.c.append(trim);
                    }
                }

                public void endDocument() {
                    long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()) - u.this.d;
                    r a2 = u.this.f1927a;
                    a2.b("XmlParser", "Finished parsing in " + seconds + " seconds");
                }

                public void endElement(String str, String str2, String str3) {
                    u uVar = u.this;
                    a unused = uVar.e = (a) uVar.b.pop();
                    u.this.e.d(u.this.c.toString().trim());
                    u.this.c.setLength(0);
                }

                public void endPrefixMapping(String str) {
                }

                public void ignorableWhitespace(char[] cArr, int i, int i2) {
                }

                public void processingInstruction(String str, String str2) {
                }

                public void setDocumentLocator(Locator locator) {
                }

                public void skippedEntity(String str) {
                }

                public void startDocument() {
                    u.this.f1927a.b("XmlParser", "Begin parsing...");
                    long unused = u.this.d = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
                }

                public void startElement(String str, String str2, String str3, Attributes attributes) throws SAXException {
                    a aVar = null;
                    try {
                        if (!u.this.b.isEmpty()) {
                            aVar = (a) u.this.b.peek();
                        }
                        a aVar2 = new a(str2, u.this.a(attributes), aVar);
                        if (aVar != null) {
                            aVar.a(aVar2);
                        }
                        u.this.b.push(aVar2);
                    } catch (Exception e) {
                        r a2 = u.this.f1927a;
                        a2.b("XmlParser", "Unable to process element <" + str2 + ">", e);
                        throw new SAXException("Failed to start element", e);
                    }
                }

                public void startPrefixMapping(String str, String str2) {
                }
            });
            a aVar = this.e;
            if (aVar != null) {
                return aVar;
            }
            throw new SAXException("Unable to parse XML into node");
        }
        throw new IllegalArgumentException("Unable to parse. No XML specified.");
    }
}
