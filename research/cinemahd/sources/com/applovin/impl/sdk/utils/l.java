package com.applovin.impl.sdk.utils;

import com.applovin.adview.AppLovinAdView;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.sdk.a.g;

public class l {

    /* renamed from: a  reason: collision with root package name */
    private final StringBuilder f1919a = new StringBuilder();

    public l a() {
        this.f1919a.append("\n========================================");
        return this;
    }

    public l a(AppLovinAdView appLovinAdView) {
        return a("Size", appLovinAdView.getSize().getWidth() + "x" + appLovinAdView.getSize().getHeight()).a("Alpha", Float.valueOf(appLovinAdView.getAlpha())).a("Visibility", s.b(appLovinAdView.getVisibility()));
    }

    public l a(a aVar) {
        return a("Network", aVar.e()).a("Format", aVar.getFormat().getLabel()).a("Ad Unit ID", aVar.getAdUnitId()).a("Placement", aVar.getPlacement()).a("Network Placement", aVar.s()).a("Serve ID", aVar.o()).a("Creative ID", o.b(aVar.getCreativeId()) ? aVar.getCreativeId() : "None").a("Server Parameters", aVar.k());
    }

    public l a(g gVar) {
        boolean z = gVar instanceof com.applovin.impl.a.a;
        a("Format", gVar.getAdZone().b() != null ? gVar.getAdZone().b().getLabel() : null).a("Ad ID", Long.valueOf(gVar.getAdIdNumber())).a("Zone ID", gVar.getAdZone().a()).a("Source", gVar.getSource()).a("Ad Class", z ? "VastAd" : "AdServerAd");
        String q0 = gVar.q0();
        if (o.b(q0)) {
            a("DSP Name", q0);
        }
        if (z) {
            a("VAST DSP", ((com.applovin.impl.a.a) gVar).K0());
        }
        return this;
    }

    public l a(com.applovin.impl.sdk.l lVar) {
        return a("Muted", Boolean.valueOf(lVar.Y().isMuted()));
    }

    public l a(String str) {
        StringBuilder sb = this.f1919a;
        sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        sb.append(str);
        return this;
    }

    public l a(String str, Object obj) {
        return a(str, obj, "");
    }

    public l a(String str, Object obj, String str2) {
        StringBuilder sb = this.f1919a;
        sb.append(ReactEditTextInputConnectionWrapper.NEWLINE_RAW_VALUE);
        sb.append(str);
        sb.append(": ");
        sb.append(obj);
        sb.append(str2);
        return this;
    }

    public l b(g gVar) {
        a("Target", gVar.p0()).a("close_style", gVar.u0()).a("close_delay_graphic", Long.valueOf(gVar.t0()), "s");
        if (gVar.hasVideoUrl()) {
            a("close_delay", Long.valueOf(gVar.r0()), "s").a("skip_style", gVar.v0()).a("Streaming", Boolean.valueOf(gVar.l0())).a("Video Location", gVar.k0()).a("video_button_properties", gVar.a());
        }
        return this;
    }

    public l b(String str) {
        this.f1919a.append(str);
        return this;
    }

    public String toString() {
        return this.f1919a.toString();
    }
}
