package com.applovin.impl.sdk.utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.XmlResourceParser;
import android.os.Bundle;
import com.applovin.impl.sdk.r;

public class c {
    private static c c;
    private static final Object d = new Object();

    /* renamed from: a  reason: collision with root package name */
    private final Bundle f1887a;
    private final int b;

    private c(Context context) {
        int i;
        Bundle bundle = null;
        try {
            bundle = context.getPackageManager().getApplicationInfo(context.getPackageName(), 128).metaData;
        } catch (PackageManager.NameNotFoundException e) {
            r.c("AndroidManifest", "Failed to get meta data.", e);
        } catch (Throwable th) {
            this.f1887a = null;
            throw th;
        }
        this.f1887a = bundle;
        try {
            XmlResourceParser openXmlResourceParser = context.getAssets().openXmlResourceParser("AndroidManifest.xml");
            int eventType = openXmlResourceParser.getEventType();
            i = 0;
            do {
                if (2 == eventType) {
                    try {
                        if (openXmlResourceParser.getName().equals("application")) {
                            int i2 = 0;
                            while (true) {
                                if (i2 >= openXmlResourceParser.getAttributeCount()) {
                                    break;
                                }
                                String attributeName = openXmlResourceParser.getAttributeName(i2);
                                String attributeValue = openXmlResourceParser.getAttributeValue(i2);
                                if (attributeName.equals("networkSecurityConfig")) {
                                    i = Integer.valueOf(attributeValue.substring(1)).intValue();
                                    break;
                                }
                                i2++;
                            }
                        }
                    } catch (Throwable th2) {
                        th = th2;
                        try {
                            r.c("AndroidManifest", "Failed to parse AndroidManifest.xml.", th);
                            this.b = i;
                        } catch (Throwable th3) {
                            this.b = i;
                            throw th3;
                        }
                    }
                }
                eventType = openXmlResourceParser.next();
            } while (eventType != 1);
        } catch (Throwable th4) {
            th = th4;
            i = 0;
            r.c("AndroidManifest", "Failed to parse AndroidManifest.xml.", th);
            this.b = i;
        }
        this.b = i;
    }

    public static c a(Context context) {
        c cVar;
        synchronized (d) {
            if (c == null) {
                c = new c(context);
            }
            cVar = c;
        }
        return cVar;
    }

    public String a(String str, String str2) {
        return this.f1887a.getString(str, str2);
    }

    public boolean a() {
        return this.b != 0;
    }

    public boolean a(String str) {
        return this.f1887a.containsKey(str);
    }

    public boolean a(String str, boolean z) {
        return this.f1887a.getBoolean(str, z);
    }
}
