package com.applovin.impl.sdk;

import android.text.TextUtils;
import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.a.h;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Arrays;
import org.json.JSONObject;

public abstract class AppLovinAdBase implements AppLovinAd {

    /* renamed from: a  reason: collision with root package name */
    private final int f1739a;
    protected final JSONObject adObject;
    protected final Object adObjectLock;
    private d b;
    private final long c;
    private h d;
    protected final JSONObject fullResponse;
    protected final Object fullResponseLock;
    /* access modifiers changed from: protected */
    public final l sdk;
    protected final b source;

    protected AppLovinAdBase(JSONObject jSONObject, JSONObject jSONObject2, b bVar, l lVar) {
        if (jSONObject == null) {
            throw new IllegalArgumentException("No ad object specified");
        } else if (jSONObject2 == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (lVar != null) {
            this.adObject = jSONObject;
            this.fullResponse = jSONObject2;
            this.source = bVar;
            this.sdk = lVar;
            this.adObjectLock = new Object();
            this.fullResponseLock = new Object();
            this.c = System.currentTimeMillis();
            char[] charArray = jSONObject.toString().toCharArray();
            Arrays.sort(charArray);
            this.f1739a = new String(charArray).hashCode();
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    /* access modifiers changed from: protected */
    public boolean containsKeyForAdObject(String str) {
        boolean has;
        synchronized (this.adObjectLock) {
            has = this.adObject.has(str);
        }
        return has;
    }

    public boolean equals(Object obj) {
        AppLovinAd b2;
        if ((obj instanceof h) && (b2 = ((h) obj).b()) != null) {
            obj = b2;
        }
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) obj;
        d dVar = this.b;
        if (dVar == null ? appLovinAdBase.b == null : dVar.equals(appLovinAdBase.b)) {
            return this.source == appLovinAdBase.source && this.f1739a == appLovinAdBase.f1739a;
        }
        return false;
    }

    public long getAdIdNumber() {
        return getLongFromAdObject("ad_id", -1);
    }

    public String getAdValue(String str) {
        JSONObject jsonObjectFromAdObject;
        if (!TextUtils.isEmpty(str) && (jsonObjectFromAdObject = getJsonObjectFromAdObject("ad_values", (JSONObject) null)) != null && jsonObjectFromAdObject.length() > 0) {
            return j.b(jsonObjectFromAdObject, str, (String) null, this.sdk);
        }
        return null;
    }

    public d getAdZone() {
        d dVar = this.b;
        if (dVar != null) {
            if (dVar.c() != null && this.b.d() != null) {
                return this.b;
            }
            if (getSize() == null && getType() == null) {
                return this.b;
            }
        }
        this.b = d.a(getSize(), getType(), getStringFromFullResponse("zone_id", (String) null), this.sdk);
        return this.b;
    }

    /* access modifiers changed from: protected */
    public boolean getBooleanFromAdObject(String str, Boolean bool) {
        boolean booleanValue;
        synchronized (this.adObjectLock) {
            booleanValue = j.a(this.adObject, str, bool, this.sdk).booleanValue();
        }
        return booleanValue;
    }

    /* access modifiers changed from: protected */
    public boolean getBooleanFromFullResponse(String str, boolean z) {
        boolean booleanValue;
        synchronized (this.fullResponseLock) {
            booleanValue = j.a(this.fullResponse, str, Boolean.valueOf(z), this.sdk).booleanValue();
        }
        return booleanValue;
    }

    public String getClCode() {
        String stringFromAdObject = getStringFromAdObject("clcode", "");
        return o.b(stringFromAdObject) ? stringFromAdObject : getStringFromFullResponse("clcode", "");
    }

    public long getCreatedAtMillis() {
        return this.c;
    }

    public h getDummyAd() {
        return this.d;
    }

    public long getFetchLatencyMillis() {
        return getLongFromFullResponse("ad_fetch_latency_millis", -1);
    }

    public long getFetchResponseSize() {
        return getLongFromFullResponse("ad_fetch_response_size", -1);
    }

    /* access modifiers changed from: protected */
    public float getFloatFromAdObject(String str, float f) {
        float a2;
        synchronized (this.adObjectLock) {
            a2 = j.a(this.adObject, str, f, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public float getFloatFromFullResponse(String str, float f) {
        float a2;
        synchronized (this.fullResponseLock) {
            a2 = j.a(this.fullResponse, str, f, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public int getIntFromAdObject(String str, int i) {
        int b2;
        synchronized (this.adObjectLock) {
            b2 = j.b(this.adObject, str, i, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public int getIntFromFullResponse(String str, int i) {
        int b2;
        synchronized (this.fullResponseLock) {
            b2 = j.b(this.fullResponse, str, i, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject getJsonObjectFromAdObject(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.adObjectLock) {
            b2 = j.b(this.adObject, str, jSONObject, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public JSONObject getJsonObjectFromFullResponse(String str, JSONObject jSONObject) {
        JSONObject b2;
        synchronized (this.fullResponseLock) {
            b2 = j.b(this.fullResponse, str, jSONObject, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public long getLongFromAdObject(String str, long j) {
        long a2;
        synchronized (this.adObjectLock) {
            a2 = j.a(this.adObject, str, j, this.sdk);
        }
        return a2;
    }

    /* access modifiers changed from: protected */
    public long getLongFromFullResponse(String str, long j) {
        long a2;
        synchronized (this.fullResponseLock) {
            a2 = j.a(this.fullResponse, str, j, this.sdk);
        }
        return a2;
    }

    public String getPrimaryKey() {
        return getStringFromAdObject("pk", "NA");
    }

    public String getRawFullResponse() {
        String jSONObject;
        synchronized (this.fullResponseLock) {
            jSONObject = this.fullResponse.toString();
        }
        return jSONObject;
    }

    public l getSdk() {
        return this.sdk;
    }

    public String getSecondaryKey1() {
        return getStringFromAdObject("sk1", (String) null);
    }

    public String getSecondaryKey2() {
        return getStringFromAdObject("sk2", (String) null);
    }

    public AppLovinAdSize getSize() {
        return AppLovinAdSize.fromString(getStringFromFullResponse("ad_size", (String) null));
    }

    public b getSource() {
        return this.source;
    }

    /* access modifiers changed from: protected */
    public String getStringFromAdObject(String str, String str2) {
        String b2;
        synchronized (this.adObjectLock) {
            b2 = j.b(this.adObject, str, str2, this.sdk);
        }
        return b2;
    }

    /* access modifiers changed from: protected */
    public String getStringFromFullResponse(String str, String str2) {
        String b2;
        synchronized (this.fullResponseLock) {
            b2 = j.b(this.fullResponse, str, str2, this.sdk);
        }
        return b2;
    }

    public AppLovinAdType getType() {
        return AppLovinAdType.fromString(getStringFromFullResponse("ad_type", (String) null));
    }

    public String getZoneId() {
        if (getAdZone().e()) {
            return null;
        }
        return getStringFromFullResponse("zone_id", (String) null);
    }

    public boolean hasShown() {
        return getBooleanFromAdObject("shown", false);
    }

    public boolean hasVideoUrl() {
        this.sdk.j0().e("AppLovinAdBase", "Attempting to invoke hasVideoUrl() from base ad class");
        return false;
    }

    public int hashCode() {
        return this.f1739a;
    }

    public boolean isVideoAd() {
        return this.adObject.has("is_video_ad") ? getBooleanFromAdObject("is_video_ad", false) : hasVideoUrl();
    }

    public void setDummyAd(h hVar) {
        this.d = hVar;
    }

    public void setHasShown(boolean z) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("shown", z);
            }
        } catch (Throwable unused) {
        }
    }

    public boolean shouldCancelHtmlCachingIfShown() {
        return getBooleanFromAdObject("chcis", false);
    }

    public String toString() {
        return "AppLovinAd{adIdNumber=" + getAdIdNumber() + ", source=" + getSource() + ", zoneId='" + getZoneId() + "'" + '}';
    }
}
