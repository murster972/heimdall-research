package com.applovin.impl.sdk;

import com.applovin.impl.sdk.a.h;
import java.util.HashMap;
import java.util.Map;

public class d {

    /* renamed from: a  reason: collision with root package name */
    private final l f1801a;
    private final r b;
    private final Object c = new Object();
    private final Map<com.applovin.impl.sdk.a.d, t> d = new HashMap();
    private final Map<com.applovin.impl.sdk.a.d, t> e = new HashMap();

    d(l lVar) {
        this.f1801a = lVar;
        this.b = lVar.j0();
        for (com.applovin.impl.sdk.a.d next : com.applovin.impl.sdk.a.d.a(lVar)) {
            this.d.put(next, new t());
            this.e.put(next, new t());
        }
    }

    private t e(com.applovin.impl.sdk.a.d dVar) {
        t tVar;
        synchronized (this.c) {
            tVar = this.d.get(dVar);
            if (tVar == null) {
                tVar = new t();
                this.d.put(dVar, tVar);
            }
        }
        return tVar;
    }

    private t f(com.applovin.impl.sdk.a.d dVar) {
        t tVar;
        synchronized (this.c) {
            tVar = this.e.get(dVar);
            if (tVar == null) {
                tVar = new t();
                this.e.put(dVar, tVar);
            }
        }
        return tVar;
    }

    private t g(com.applovin.impl.sdk.a.d dVar) {
        synchronized (this.c) {
            t f = f(dVar);
            if (f.a() > 0) {
                return f;
            }
            t e2 = e(dVar);
            return e2;
        }
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAdBase appLovinAdBase) {
        synchronized (this.c) {
            e(appLovinAdBase.getAdZone()).a(appLovinAdBase);
            r rVar = this.b;
            rVar.b("AdPreloadManager", "Ad enqueued: " + appLovinAdBase);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:12:0x001d, code lost:
        return r2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(com.applovin.impl.sdk.a.d r4) {
        /*
            r3 = this;
            java.lang.Object r0 = r3.c
            monitor-enter(r0)
            com.applovin.impl.sdk.t r1 = r3.f(r4)     // Catch:{ all -> 0x001e }
            int r1 = r1.a()     // Catch:{ all -> 0x001e }
            r2 = 1
            if (r1 <= 0) goto L_0x0010
            monitor-exit(r0)     // Catch:{ all -> 0x001e }
            return r2
        L_0x0010:
            com.applovin.impl.sdk.t r4 = r3.e(r4)     // Catch:{ all -> 0x001e }
            int r4 = r4.a()     // Catch:{ all -> 0x001e }
            if (r4 <= 0) goto L_0x001b
            goto L_0x001c
        L_0x001b:
            r2 = 0
        L_0x001c:
            monitor-exit(r0)     // Catch:{ all -> 0x001e }
            return r2
        L_0x001e:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001e }
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.d.a(com.applovin.impl.sdk.a.d):boolean");
    }

    public AppLovinAdBase b(com.applovin.impl.sdk.a.d dVar) {
        h hVar;
        StringBuilder sb;
        String str;
        synchronized (this.c) {
            t e2 = e(dVar);
            if (e2.a() > 0) {
                f(dVar).a(e2.c());
                hVar = new h(dVar, this.f1801a);
            } else {
                hVar = null;
            }
        }
        r rVar = this.b;
        if (hVar != null) {
            str = "Retrieved ad of zone ";
        } else {
            sb = new StringBuilder();
            str = "Unable to retrieve ad of zone ";
        }
        sb.append(str);
        sb.append(dVar);
        sb.append("...");
        rVar.b("AdPreloadManager", sb.toString());
        return hVar;
    }

    public AppLovinAdBase c(com.applovin.impl.sdk.a.d dVar) {
        AppLovinAdBase c2;
        synchronized (this.c) {
            c2 = g(dVar).c();
        }
        return c2;
    }

    public AppLovinAdBase d(com.applovin.impl.sdk.a.d dVar) {
        AppLovinAdBase d2;
        synchronized (this.c) {
            d2 = g(dVar).d();
        }
        return d2;
    }
}
