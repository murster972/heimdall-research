package com.applovin.impl.sdk;

import android.os.Process;
import java.lang.Thread;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class AppLovinExceptionHandler implements Thread.UncaughtExceptionHandler {
    private static final AppLovinExceptionHandler d = new AppLovinExceptionHandler();

    /* renamed from: a  reason: collision with root package name */
    private final Set<l> f1749a = new HashSet(2);
    private final AtomicBoolean b = new AtomicBoolean();
    private Thread.UncaughtExceptionHandler c;

    public static AppLovinExceptionHandler shared() {
        return d;
    }

    public void addSdk(l lVar) {
        this.f1749a.add(lVar);
    }

    public void enable() {
        if (this.b.compareAndSet(false, true)) {
            this.c = Thread.getDefaultUncaughtExceptionHandler();
            Thread.setDefaultUncaughtExceptionHandler(this);
        }
    }

    public void uncaughtException(Thread thread, Throwable th) {
        for (l next : this.f1749a) {
            next.j0().b("AppLovinExceptionHandler", "Detected unhandled exception");
            ((EventServiceImpl) next.e0()).trackEventSynchronously("paused");
            ((EventServiceImpl) next.e0()).trackEventSynchronously("crashed");
        }
        try {
            Thread.sleep(100);
        } catch (InterruptedException unused) {
        }
        Thread.UncaughtExceptionHandler uncaughtExceptionHandler = this.c;
        if (uncaughtExceptionHandler != null) {
            uncaughtExceptionHandler.uncaughtException(thread, th);
            return;
        }
        Process.killProcess(Process.myPid());
        System.exit(1);
    }
}
