package com.applovin.impl.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.utils.p;
import java.util.Map;

public class b implements AppLovinBroadcastManager.Receiver {

    /* renamed from: a  reason: collision with root package name */
    private final l f1771a;
    /* access modifiers changed from: private */
    public final a b;
    private p c;
    private final Object d = new Object();
    private long e;

    public interface a {
        void onAdExpired();
    }

    public b(l lVar, a aVar) {
        this.f1771a = lVar;
        this.b = aVar;
    }

    private void b() {
        p pVar = this.c;
        if (pVar != null) {
            pVar.d();
            this.c = null;
        }
    }

    private void c() {
        synchronized (this.d) {
            b();
        }
    }

    private void d() {
        boolean z;
        synchronized (this.d) {
            long currentTimeMillis = this.e - System.currentTimeMillis();
            if (currentTimeMillis <= 0) {
                a();
                z = true;
            } else {
                a(currentTimeMillis);
                z = false;
            }
        }
        if (z) {
            this.b.onAdExpired();
        }
    }

    public void a() {
        synchronized (this.d) {
            b();
            this.f1771a.I().unregisterReceiver(this);
        }
    }

    public void a(long j) {
        synchronized (this.d) {
            a();
            this.e = System.currentTimeMillis() + j;
            this.f1771a.I().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
            this.f1771a.I().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
            if (((Boolean) this.f1771a.a(com.applovin.impl.sdk.c.a.H4)).booleanValue() || !this.f1771a.B().a()) {
                this.c = p.a(j, this.f1771a, new Runnable() {
                    public void run() {
                        b.this.a();
                        b.this.b.onAdExpired();
                    }
                });
            }
        }
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            c();
        } else if ("com.applovin.application_resumed".equals(action)) {
            d();
        }
    }
}
