package com.applovin.impl.sdk;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.utils.AppKilledService;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.o;
import java.util.Map;

public class r {

    /* renamed from: a  reason: collision with root package name */
    private final l f1882a;

    r(final l lVar) {
        this.f1882a = lVar;
        if (!lVar.P()) {
            a("SDK Session Begin");
            lVar.I().registerReceiver(new AppLovinBroadcastManager.Receiver() {
                public void onReceive(Context context, Intent intent, Map<String, Object> map) {
                    r.this.a("SDK Session End");
                    lVar.I().unregisterReceiver(this);
                }
            }, new IntentFilter(AppKilledService.ACTION_APP_KILLED));
        }
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        l lVar = new l();
        lVar.a().a(str).a();
        f("AppLovinSdk", lVar.toString());
    }

    private void a(String str, String str2, boolean z) {
        int intValue;
        if (o.b(str2) && (intValue = ((Integer) this.f1882a.a(b.t)).intValue()) > 0) {
            int length = str2.length();
            int i = ((length + intValue) - 1) / intValue;
            for (int i2 = 0; i2 < i; i2++) {
                int i3 = i2 * intValue;
                String substring = str2.substring(i3, Math.min(length, i3 + intValue));
                if (z) {
                    Log.d(str, substring);
                } else {
                    b(str, substring);
                }
            }
        }
    }

    private boolean a() {
        return this.f1882a.h().d();
    }

    public static void c(String str, String str2, Throwable th) {
        Log.e("AppLovinSdk", "[" + str + "] " + str2, th);
    }

    public static void f(String str, String str2) {
        Log.d("AppLovinSdk", "[" + str + "] " + str2);
    }

    public static void g(String str, String str2) {
        Log.i("AppLovinSdk", "[" + str + "] " + str2);
    }

    public static void h(String str, String str2) {
        Log.w("AppLovinSdk", "[" + str + "] " + str2);
    }

    public static void i(String str, String str2) {
        c(str, str2, (Throwable) null);
    }

    private void j(String str, String str2) {
    }

    public void a(String str, Boolean bool, String str2) {
        a(str, bool, str2, (Throwable) null);
    }

    public void a(String str, Boolean bool, String str2, Throwable th) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.e("AppLovinSdk", str3, th);
            j("ERROR", str3 + " : " + th);
        }
        if (bool.booleanValue() && ((Boolean) this.f1882a.a(b.h3)).booleanValue() && this.f1882a.s() != null) {
            this.f1882a.s().a(str2, th);
        }
    }

    public void a(String str, String str2) {
        if (a()) {
            a(str, str2, false);
        }
    }

    public void a(String str, String str2, Throwable th) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.w("AppLovinSdk", str3, th);
            j("WARN", str3);
        }
    }

    public void b(String str, String str2) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.d("AppLovinSdk", str3);
            j("DEBUG", str3);
        }
    }

    public void b(String str, String str2, Throwable th) {
        a(str, true, str2, th);
    }

    public void c(String str, String str2) {
        if (a()) {
            String str3 = "[" + str + "] " + str2;
            Log.i("AppLovinSdk", str3);
            j("INFO", str3);
        }
    }

    public void d(String str, String str2) {
        a(str, str2, (Throwable) null);
    }

    public void e(String str, String str2) {
        b(str, str2, (Throwable) null);
    }
}
