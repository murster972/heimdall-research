package com.applovin.impl.sdk;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class g extends BroadcastReceiver implements AppLovinBroadcastManager.Receiver {
    public static int h = -1;

    /* renamed from: a  reason: collision with root package name */
    private final AudioManager f1833a;
    private final Context b;
    private final l c;
    private final Set<a> d = new HashSet();
    private final Object e = new Object();
    private boolean f;
    private int g;

    public interface a {
        void onRingerModeChanged(int i);
    }

    g(l lVar) {
        this.c = lVar;
        this.b = lVar.i();
        this.f1833a = (AudioManager) this.b.getSystemService("audio");
    }

    public static boolean a(int i) {
        return i == 0 || i == 1;
    }

    private void b() {
        this.c.j0().b("AudioSessionManager", "Observing ringer mode...");
        this.g = h;
        this.b.registerReceiver(this, new IntentFilter("android.media.RINGER_MODE_CHANGED"));
        this.c.I().registerReceiver(this, new IntentFilter("com.applovin.application_paused"));
        this.c.I().registerReceiver(this, new IntentFilter("com.applovin.application_resumed"));
    }

    private void b(final int i) {
        if (!this.f) {
            r j0 = this.c.j0();
            j0.b("AudioSessionManager", "Ringer mode is " + i);
            synchronized (this.e) {
                for (final a next : this.d) {
                    AppLovinSdkUtils.runOnUiThread(new Runnable(this) {
                        public void run() {
                            next.onRingerModeChanged(i);
                        }
                    });
                }
            }
        }
    }

    private void c() {
        this.c.j0().b("AudioSessionManager", "Stopping observation of mute switch state...");
        this.b.unregisterReceiver(this);
        this.c.I().unregisterReceiver(this);
    }

    public int a() {
        return this.f1833a.getRingerMode();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001f, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.sdk.g.a r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.e
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.g$a> r1 = r2.d     // Catch:{ all -> 0x0020 }
            boolean r1 = r1.contains(r3)     // Catch:{ all -> 0x0020 }
            if (r1 == 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x0020 }
            return
        L_0x000d:
            java.util.Set<com.applovin.impl.sdk.g$a> r1 = r2.d     // Catch:{ all -> 0x0020 }
            r1.add(r3)     // Catch:{ all -> 0x0020 }
            java.util.Set<com.applovin.impl.sdk.g$a> r3 = r2.d     // Catch:{ all -> 0x0020 }
            int r3 = r3.size()     // Catch:{ all -> 0x0020 }
            r1 = 1
            if (r3 != r1) goto L_0x001e
            r2.b()     // Catch:{ all -> 0x0020 }
        L_0x001e:
            monitor-exit(r0)     // Catch:{ all -> 0x0020 }
            return
        L_0x0020:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0020 }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.g.a(com.applovin.impl.sdk.g$a):void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(com.applovin.impl.sdk.g.a r3) {
        /*
            r2 = this;
            java.lang.Object r0 = r2.e
            monitor-enter(r0)
            java.util.Set<com.applovin.impl.sdk.g$a> r1 = r2.d     // Catch:{ all -> 0x001f }
            boolean r1 = r1.contains(r3)     // Catch:{ all -> 0x001f }
            if (r1 != 0) goto L_0x000d
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            return
        L_0x000d:
            java.util.Set<com.applovin.impl.sdk.g$a> r1 = r2.d     // Catch:{ all -> 0x001f }
            r1.remove(r3)     // Catch:{ all -> 0x001f }
            java.util.Set<com.applovin.impl.sdk.g$a> r3 = r2.d     // Catch:{ all -> 0x001f }
            boolean r3 = r3.isEmpty()     // Catch:{ all -> 0x001f }
            if (r3 == 0) goto L_0x001d
            r2.c()     // Catch:{ all -> 0x001f }
        L_0x001d:
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            return
        L_0x001f:
            r3 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x001f }
            throw r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.g.b(com.applovin.impl.sdk.g$a):void");
    }

    public void onReceive(Context context, Intent intent) {
        if ("android.media.RINGER_MODE_CHANGED".equals(intent.getAction())) {
            b(this.f1833a.getRingerMode());
        }
    }

    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
        String action = intent.getAction();
        if ("com.applovin.application_paused".equals(action)) {
            this.f = true;
            this.g = this.f1833a.getRingerMode();
        } else if ("com.applovin.application_resumed".equals(action)) {
            this.f = false;
            if (this.g != this.f1833a.getRingerMode()) {
                this.g = h;
                b(this.f1833a.getRingerMode());
            }
        }
    }
}
