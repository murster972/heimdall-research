package com.applovin.impl.sdk;

import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinUserSegment;

class f implements AppLovinUserSegment {

    /* renamed from: a  reason: collision with root package name */
    private String f1832a;

    f() {
    }

    public String getName() {
        return this.f1832a;
    }

    public void setName(String str) {
        if (str != null) {
            if (str.length() > 32) {
                r.i("AppLovinUserSegment", "Setting name greater than 32 characters: " + str);
            }
            if (!o.e(str)) {
                r.i("AppLovinUserSegment", "Setting name that is not alphanumeric: " + str);
            }
        }
        this.f1832a = str;
    }

    public String toString() {
        return "AppLovinUserSegment{name=" + getName() + '}';
    }
}
