package com.applovin.impl.sdk;

import android.app.Activity;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.View;
import android.view.animation.Animation;
import com.applovin.impl.mediation.a.b;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.sdk.AppLovinSdkUtils;

public class w {

    /* renamed from: a  reason: collision with root package name */
    private final l f1934a;
    private final r b;
    private final MaxAdView c;

    public w(MaxAdView maxAdView, l lVar) {
        this.f1934a = lVar;
        this.b = lVar.j0();
        this.c = maxAdView;
    }

    public long a(b bVar) {
        long j;
        this.b.b("ViewabilityTracker", "Checking visibility...");
        if (!this.c.isShown()) {
            this.b.e("ViewabilityTracker", "View is hidden");
            j = 2;
        } else {
            j = 0;
        }
        if (this.c.getAlpha() < bVar.F()) {
            this.b.e("ViewabilityTracker", "View is transparent");
            j |= 4;
        }
        Animation animation = this.c.getAnimation();
        if (animation != null && animation.hasStarted() && !animation.hasEnded()) {
            this.b.e("ViewabilityTracker", "View is animating");
            j |= 8;
        }
        if (this.c.getParent() == null) {
            this.b.e("ViewabilityTracker", "No parent view found");
            j |= 16;
        }
        int pxToDp = AppLovinSdkUtils.pxToDp(this.c.getContext(), this.c.getWidth());
        if (pxToDp < bVar.D()) {
            r rVar = this.b;
            rVar.e("ViewabilityTracker", "View has width (" + pxToDp + ") below threshold");
            j |= 32;
        }
        int pxToDp2 = AppLovinSdkUtils.pxToDp(this.c.getContext(), this.c.getHeight());
        if (pxToDp2 < bVar.E()) {
            r rVar2 = this.b;
            rVar2.e("ViewabilityTracker", "View has height (" + pxToDp2 + ") below threshold");
            j |= 64;
        }
        Point a2 = g.a(this.c.getContext());
        Rect rect = new Rect(0, 0, a2.x, a2.y);
        int[] iArr = {-1, -1};
        this.c.getLocationOnScreen(iArr);
        Rect rect2 = new Rect(iArr[0], iArr[1], iArr[0] + this.c.getWidth(), iArr[1] + this.c.getHeight());
        if (!Rect.intersects(rect, rect2)) {
            r rVar3 = this.b;
            rVar3.e("ViewabilityTracker", "Rect (" + rect2 + ") outside of screen's bounds (" + rect + ")");
            j |= 128;
        }
        Activity a3 = this.f1934a.D().a();
        if (a3 != null && !r.a((View) this.c, a3)) {
            this.b.e("ViewabilityTracker", "View is not in top activity's view hierarchy");
            j |= 256;
        }
        r rVar4 = this.b;
        rVar4.b("ViewabilityTracker", "Returning flags: " + Long.toBinaryString(j));
        return j;
    }
}
