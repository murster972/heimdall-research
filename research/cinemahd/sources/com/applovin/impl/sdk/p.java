package com.applovin.impl.sdk;

import android.content.Context;
import android.net.Uri;
import android.support.v4.media.session.PlaybackStateCompat;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.e;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.utils.o;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class p {

    /* renamed from: a  reason: collision with root package name */
    private final l f1878a;
    private final r b;
    private final Object c = new Object();

    p(l lVar) {
        this.f1878a = lVar;
        this.b = lVar.j0();
    }

    private long a() {
        long longValue = ((Long) this.f1878a.a(b.q0)).longValue();
        if (longValue < 0 || !b()) {
            return -1;
        }
        return longValue;
    }

    private long a(long j) {
        return j / PlaybackStateCompat.ACTION_SET_CAPTIONING_ENABLED;
    }

    private void a(long j, Context context) {
        r rVar;
        String str;
        if (b()) {
            long intValue = (long) ((Integer) this.f1878a.a(b.r0)).intValue();
            if (intValue == -1) {
                rVar = this.b;
                str = "Cache has no maximum size set; skipping drop...";
            } else {
                int i = (a(j) > intValue ? 1 : (a(j) == intValue ? 0 : -1));
                rVar = this.b;
                if (i > 0) {
                    rVar.b("FileManager", "Cache has exceeded maximum size; dropping...");
                    for (File b2 : d(context)) {
                        b(b2);
                    }
                    this.f1878a.p().a(g.i);
                    return;
                }
                str = "Cache is present but under size limit; not dropping...";
            }
            rVar.b("FileManager", str);
        }
    }

    private boolean a(File file, String str, List<String> list, boolean z, e eVar) {
        if (file == null || !file.exists() || file.isDirectory()) {
            ByteArrayOutputStream a2 = a(str, list, z);
            if (!(eVar == null || a2 == null)) {
                eVar.a((long) a2.size());
            }
            return a(a2, file);
        }
        r rVar = this.b;
        rVar.b("FileManager", "File exists for " + str);
        if (eVar == null) {
            return true;
        }
        eVar.b(file.length());
        return true;
    }

    private boolean b() {
        return ((Boolean) this.f1878a.a(b.p0)).booleanValue();
    }

    /* JADX WARNING: Unknown top exception splitter block from list: {B:16:0x0038=Splitter:B:16:0x0038, B:22:0x0048=Splitter:B:22:0x0048} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private boolean b(java.io.ByteArrayOutputStream r5, java.io.File r6) {
        /*
            r4 = this;
            com.applovin.impl.sdk.r r0 = r4.b
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r2 = "Writing resource to filesystem: "
            r1.append(r2)
            java.lang.String r2 = r6.getName()
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.String r2 = "FileManager"
            r0.b(r2, r1)
            java.lang.Object r0 = r4.c
            monitor-enter(r0)
            r1 = 0
            java.io.FileOutputStream r2 = new java.io.FileOutputStream     // Catch:{ IOException -> 0x0047, all -> 0x0037 }
            r2.<init>(r6)     // Catch:{ IOException -> 0x0047, all -> 0x0037 }
            r5.writeTo(r2)     // Catch:{ IOException -> 0x0034, all -> 0x0031 }
            r5 = 1
            com.applovin.impl.sdk.l r6 = r4.f1878a     // Catch:{ all -> 0x002f }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r6)     // Catch:{ all -> 0x002f }
            goto L_0x0055
        L_0x002f:
            r5 = move-exception
            goto L_0x005e
        L_0x0031:
            r5 = move-exception
            r1 = r2
            goto L_0x0038
        L_0x0034:
            r5 = move-exception
            r1 = r2
            goto L_0x0048
        L_0x0037:
            r5 = move-exception
        L_0x0038:
            com.applovin.impl.sdk.r r6 = r4.b     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "FileManager"
            java.lang.String r3 = "Unknown failure to write file."
            r6.b(r2, r3, r5)     // Catch:{ all -> 0x0057 }
            com.applovin.impl.sdk.l r5 = r4.f1878a     // Catch:{ all -> 0x002f }
        L_0x0043:
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r1, (com.applovin.impl.sdk.l) r5)     // Catch:{ all -> 0x002f }
            goto L_0x0054
        L_0x0047:
            r5 = move-exception
        L_0x0048:
            com.applovin.impl.sdk.r r6 = r4.b     // Catch:{ all -> 0x0057 }
            java.lang.String r2 = "FileManager"
            java.lang.String r3 = "Unable to write data to file."
            r6.b(r2, r3, r5)     // Catch:{ all -> 0x0057 }
            com.applovin.impl.sdk.l r5 = r4.f1878a     // Catch:{ all -> 0x002f }
            goto L_0x0043
        L_0x0054:
            r5 = 0
        L_0x0055:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            return r5
        L_0x0057:
            r5 = move-exception
            com.applovin.impl.sdk.l r6 = r4.f1878a     // Catch:{ all -> 0x002f }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r1, (com.applovin.impl.sdk.l) r6)     // Catch:{ all -> 0x002f }
            throw r5     // Catch:{ all -> 0x002f }
        L_0x005e:
            monitor-exit(r0)     // Catch:{ all -> 0x002f }
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.p.b(java.io.ByteArrayOutputStream, java.io.File):boolean");
    }

    private boolean b(File file) {
        boolean delete;
        r rVar = this.b;
        rVar.b("FileManager", "Removing file " + file.getName() + " from filesystem...");
        synchronized (this.c) {
            try {
                delete = file.delete();
            } catch (Exception e) {
                r rVar2 = this.b;
                rVar2.b("FileManager", "Failed to remove file " + file.getName() + " from filesystem!", e);
                return false;
            } catch (Throwable th) {
                throw th;
            }
        }
        return delete;
    }

    private long c(Context context) {
        long j;
        boolean z;
        long a2 = a();
        boolean z2 = a2 != -1;
        long seconds = TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
        synchronized (this.c) {
            List<String> b2 = this.f1878a.b(b.v0);
            j = 0;
            for (File next : d(context)) {
                if (!z2 || b2.contains(next.getName()) || seconds - TimeUnit.MILLISECONDS.toSeconds(next.lastModified()) <= a2) {
                    z = false;
                } else {
                    r rVar = this.b;
                    rVar.b("FileManager", "File " + next.getName() + " has expired, removing...");
                    z = b(next);
                }
                if (z) {
                    this.f1878a.p().a(g.h);
                } else {
                    j += next.length();
                }
            }
        }
        return j;
    }

    private List<File> d(Context context) {
        List<File> asList;
        File e = e(context);
        if (!e.isDirectory()) {
            return Collections.emptyList();
        }
        synchronized (this.c) {
            asList = Arrays.asList(e.listFiles());
        }
        return asList;
    }

    private File e(Context context) {
        return new File(context.getFilesDir(), "al");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0050, code lost:
        r9 = th;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0052, code lost:
        r3 = e;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0054, code lost:
        r9 = e;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x003d */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x0050 A[ExcHandler: all (th java.lang.Throwable), Splitter:B:7:0x0028] */
    /* JADX WARNING: Unknown top exception splitter block from list: {B:43:0x0092=Splitter:B:43:0x0092, B:27:0x0058=Splitter:B:27:0x0058, B:35:0x006a=Splitter:B:35:0x006a} */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.ByteArrayOutputStream a(java.io.File r9) {
        /*
            r8 = this;
            r0 = 0
            if (r9 != 0) goto L_0x0004
            return r0
        L_0x0004:
            com.applovin.impl.sdk.r r1 = r8.b
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            java.lang.String r3 = "Reading resource from filesystem: "
            r2.append(r3)
            java.lang.String r3 = r9.getName()
            r2.append(r3)
            java.lang.String r2 = r2.toString()
            java.lang.String r3 = "FileManager"
            r1.b(r3, r2)
            java.lang.Object r1 = r8.c
            monitor-enter(r1)
            java.io.FileInputStream r2 = new java.io.FileInputStream     // Catch:{ FileNotFoundException -> 0x0090, IOException -> 0x0068, all -> 0x0056 }
            r2.<init>(r9)     // Catch:{ FileNotFoundException -> 0x0090, IOException -> 0x0068, all -> 0x0056 }
            java.io.ByteArrayOutputStream r3 = new java.io.ByteArrayOutputStream     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            r3.<init>()     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
        L_0x0031:
            int r5 = r4.length     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            r6 = 0
            int r5 = r2.read(r4, r6, r5)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            if (r5 < 0) goto L_0x0049
            r3.write(r4, r6, r5)     // Catch:{ Exception -> 0x003d, all -> 0x0050 }
            goto L_0x0031
        L_0x003d:
            com.applovin.impl.sdk.l r4 = r8.f1878a     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r3, (com.applovin.impl.sdk.l) r4)     // Catch:{ FileNotFoundException -> 0x0054, IOException -> 0x0052, all -> 0x0050 }
            com.applovin.impl.sdk.l r9 = r8.f1878a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x0049:
            com.applovin.impl.sdk.l r9 = r8.f1878a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r3
        L_0x0050:
            r9 = move-exception
            goto L_0x0058
        L_0x0052:
            r3 = move-exception
            goto L_0x006a
        L_0x0054:
            r9 = move-exception
            goto L_0x0092
        L_0x0056:
            r9 = move-exception
            r2 = r0
        L_0x0058:
            com.applovin.impl.sdk.r r3 = r8.b     // Catch:{ all -> 0x00b1 }
            java.lang.String r4 = "FileManager"
            java.lang.String r5 = "Unknown failure to read file."
            r3.b(r4, r5, r9)     // Catch:{ all -> 0x00b1 }
            com.applovin.impl.sdk.l r9 = r8.f1878a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x0068:
            r3 = move-exception
            r2 = r0
        L_0x006a:
            com.applovin.impl.sdk.r r4 = r8.b     // Catch:{ all -> 0x00b1 }
            java.lang.String r5 = "FileManager"
            java.lang.StringBuilder r6 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b1 }
            r6.<init>()     // Catch:{ all -> 0x00b1 }
            java.lang.String r7 = "Failed to read file: "
            r6.append(r7)     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r9.getName()     // Catch:{ all -> 0x00b1 }
            r6.append(r9)     // Catch:{ all -> 0x00b1 }
            r6.append(r3)     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r6.toString()     // Catch:{ all -> 0x00b1 }
            r4.b(r5, r9)     // Catch:{ all -> 0x00b1 }
            com.applovin.impl.sdk.l r9 = r8.f1878a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x0090:
            r9 = move-exception
            r2 = r0
        L_0x0092:
            com.applovin.impl.sdk.r r3 = r8.b     // Catch:{ all -> 0x00b1 }
            java.lang.String r4 = "FileManager"
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00b1 }
            r5.<init>()     // Catch:{ all -> 0x00b1 }
            java.lang.String r6 = "File not found. "
            r5.append(r6)     // Catch:{ all -> 0x00b1 }
            r5.append(r9)     // Catch:{ all -> 0x00b1 }
            java.lang.String r9 = r5.toString()     // Catch:{ all -> 0x00b1 }
            r3.c(r4, r9)     // Catch:{ all -> 0x00b1 }
            com.applovin.impl.sdk.l r9 = r8.f1878a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r9)     // Catch:{ all -> 0x00b8 }
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            return r0
        L_0x00b1:
            r9 = move-exception
            com.applovin.impl.sdk.l r0 = r8.f1878a     // Catch:{ all -> 0x00b8 }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r0)     // Catch:{ all -> 0x00b8 }
            throw r9     // Catch:{ all -> 0x00b8 }
        L_0x00b8:
            r9 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00b8 }
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.p.a(java.io.File):java.io.ByteArrayOutputStream");
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v4, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v5, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v2, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v6, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v3, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v7, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v8, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v9, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v10, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r10v13, resolved type: java.net.HttpURLConnection} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v4, resolved type: java.io.InputStream} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r2v18, resolved type: java.io.InputStream} */
    /* JADX WARNING: Can't wrap try/catch for region: R(3:28|29|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, r7.f1878a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:32:0x00c7, code lost:
        com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, r7.f1878a);
        com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, r7.f1878a);
        com.applovin.impl.sdk.utils.r.a(r10, r7.f1878a);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:33:0x00d6, code lost:
        return null;
     */
    /* JADX WARNING: Missing exception handler attribute for start block: B:30:0x00c2 */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.io.ByteArrayOutputStream a(java.lang.String r8, java.util.List<java.lang.String> r9, boolean r10) {
        /*
            r7 = this;
            java.lang.String r0 = "FileManager"
            r1 = 0
            if (r10 == 0) goto L_0x0022
            boolean r9 = com.applovin.impl.sdk.utils.r.a((java.lang.String) r8, (java.util.List<java.lang.String>) r9)
            if (r9 != 0) goto L_0x0022
            com.applovin.impl.sdk.r r9 = r7.b
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r2 = "Domain is not whitelisted, skipping precache for url: "
            r10.append(r2)
            r10.append(r8)
            java.lang.String r8 = r10.toString()
            r9.b(r0, r8)
            return r1
        L_0x0022:
            com.applovin.impl.sdk.l r9 = r7.f1878a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r10 = com.applovin.impl.sdk.c.b.b2
            java.lang.Object r9 = r9.a(r10)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x0047
            java.lang.String r9 = "https://"
            boolean r10 = r8.contains(r9)
            if (r10 != 0) goto L_0x0047
            com.applovin.impl.sdk.r r10 = r7.b
            java.lang.String r2 = "Plaintext HTTP operation requested; upgrading to HTTPS due to universal SSL setting..."
            r10.d(r0, r2)
            java.lang.String r10 = "http://"
            java.lang.String r8 = r8.replace(r10, r9)
        L_0x0047:
            com.applovin.impl.sdk.r r9 = r7.b
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r2 = "Loading "
            r10.append(r2)
            r10.append(r8)
            java.lang.String r2 = "..."
            r10.append(r2)
            java.lang.String r10 = r10.toString()
            r9.b(r0, r10)
            java.io.ByteArrayOutputStream r9 = new java.io.ByteArrayOutputStream     // Catch:{ IOException -> 0x011e, all -> 0x011a }
            r9.<init>()     // Catch:{ IOException -> 0x011e, all -> 0x011a }
            java.net.URL r10 = new java.net.URL     // Catch:{ IOException -> 0x0117, all -> 0x0114 }
            r10.<init>(r8)     // Catch:{ IOException -> 0x0117, all -> 0x0114 }
            java.net.URLConnection r10 = r10.openConnection()     // Catch:{ IOException -> 0x0117, all -> 0x0114 }
            java.net.HttpURLConnection r10 = (java.net.HttpURLConnection) r10     // Catch:{ IOException -> 0x0117, all -> 0x0114 }
            com.applovin.impl.sdk.l r2 = r7.f1878a     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            com.applovin.impl.sdk.c.b<java.lang.Integer> r3 = com.applovin.impl.sdk.c.b.Z1     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            int r2 = r2.intValue()     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r10.setConnectTimeout(r2)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            com.applovin.impl.sdk.l r2 = r7.f1878a     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            com.applovin.impl.sdk.c.b<java.lang.Integer> r3 = com.applovin.impl.sdk.c.b.a2     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            java.lang.Object r2 = r2.a(r3)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            java.lang.Integer r2 = (java.lang.Integer) r2     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            int r2 = r2.intValue()     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r10.setReadTimeout(r2)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r2 = 1
            r10.setDefaultUseCaches(r2)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r10.setUseCaches(r2)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r3 = 0
            r10.setAllowUserInteraction(r3)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r10.setInstanceFollowRedirects(r2)     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            int r2 = r10.getResponseCode()     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r4 = 200(0xc8, float:2.8E-43)
            if (r2 < r4) goto L_0x00ff
            r4 = 300(0x12c, float:4.2E-43)
            if (r2 < r4) goto L_0x00af
            goto L_0x00ff
        L_0x00af:
            java.io.InputStream r2 = r10.getInputStream()     // Catch:{ IOException -> 0x0111, all -> 0x010f }
            r4 = 1024(0x400, float:1.435E-42)
            byte[] r4 = new byte[r4]     // Catch:{ IOException -> 0x00fd }
        L_0x00b7:
            int r5 = r4.length     // Catch:{ IOException -> 0x00fd }
            int r5 = r2.read(r4, r3, r5)     // Catch:{ IOException -> 0x00fd }
            if (r5 < 0) goto L_0x00d7
            r9.write(r4, r3, r5)     // Catch:{ Exception -> 0x00c2 }
            goto L_0x00b7
        L_0x00c2:
            com.applovin.impl.sdk.l r3 = r7.f1878a     // Catch:{ IOException -> 0x00fd }
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, (com.applovin.impl.sdk.l) r3)     // Catch:{ IOException -> 0x00fd }
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r10, (com.applovin.impl.sdk.l) r8)
            return r1
        L_0x00d7:
            com.applovin.impl.sdk.r r3 = r7.b     // Catch:{ IOException -> 0x00fd }
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ IOException -> 0x00fd }
            r4.<init>()     // Catch:{ IOException -> 0x00fd }
            java.lang.String r5 = "Loaded resource at "
            r4.append(r5)     // Catch:{ IOException -> 0x00fd }
            r4.append(r8)     // Catch:{ IOException -> 0x00fd }
            java.lang.String r4 = r4.toString()     // Catch:{ IOException -> 0x00fd }
            r3.b(r0, r4)     // Catch:{ IOException -> 0x00fd }
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r10, (com.applovin.impl.sdk.l) r8)
            return r9
        L_0x00fd:
            r3 = move-exception
            goto L_0x0122
        L_0x00ff:
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r1, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r10, (com.applovin.impl.sdk.l) r8)
            return r1
        L_0x010f:
            r8 = move-exception
            goto L_0x014a
        L_0x0111:
            r3 = move-exception
            r2 = r1
            goto L_0x0122
        L_0x0114:
            r8 = move-exception
            r10 = r1
            goto L_0x014a
        L_0x0117:
            r3 = move-exception
            r10 = r1
            goto L_0x0121
        L_0x011a:
            r8 = move-exception
            r9 = r1
            r10 = r9
            goto L_0x014a
        L_0x011e:
            r3 = move-exception
            r9 = r1
            r10 = r9
        L_0x0121:
            r2 = r10
        L_0x0122:
            com.applovin.impl.sdk.r r4 = r7.b     // Catch:{ all -> 0x0148 }
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x0148 }
            r5.<init>()     // Catch:{ all -> 0x0148 }
            java.lang.String r6 = "Error loading "
            r5.append(r6)     // Catch:{ all -> 0x0148 }
            r5.append(r8)     // Catch:{ all -> 0x0148 }
            java.lang.String r8 = r5.toString()     // Catch:{ all -> 0x0148 }
            r4.b(r0, r8, r3)     // Catch:{ all -> 0x0148 }
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r2, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, (com.applovin.impl.sdk.l) r8)
            com.applovin.impl.sdk.l r8 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r10, (com.applovin.impl.sdk.l) r8)
            return r1
        L_0x0148:
            r8 = move-exception
            r1 = r2
        L_0x014a:
            com.applovin.impl.sdk.l r0 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r1, (com.applovin.impl.sdk.l) r0)
            com.applovin.impl.sdk.l r0 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.io.Closeable) r9, (com.applovin.impl.sdk.l) r0)
            com.applovin.impl.sdk.l r9 = r7.f1878a
            com.applovin.impl.sdk.utils.r.a((java.net.HttpURLConnection) r10, (com.applovin.impl.sdk.l) r9)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.p.a(java.lang.String, java.util.List, boolean):java.io.ByteArrayOutputStream");
    }

    public File a(String str, Context context) {
        File file;
        if (!o.b(str)) {
            this.b.b("FileManager", "Nothing to look up, skipping...");
            return null;
        }
        r rVar = this.b;
        rVar.b("FileManager", "Looking up cached resource: " + str);
        if (str.contains("icon")) {
            str = str.replace("/", "_").replace(".", "_");
        }
        synchronized (this.c) {
            File e = e(context);
            file = new File(e, str);
            try {
                e.mkdirs();
            } catch (Throwable th) {
                r rVar2 = this.b;
                rVar2.b("FileManager", "Unable to make cache directory at " + e, th);
                return null;
            }
        }
        return file;
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, e eVar) {
        return a(context, str, str2, list, z, false, eVar);
    }

    public String a(Context context, String str, String str2, List<String> list, boolean z, boolean z2, e eVar) {
        if (!o.b(str)) {
            this.b.b("FileManager", "Nothing to cache, skipping...");
            return null;
        }
        String lastPathSegment = Uri.parse(str).getLastPathSegment();
        if (o.b(lastPathSegment) && o.b(str2)) {
            StringBuilder sb = new StringBuilder();
            String str3 = str2;
            sb.append(str2);
            sb.append(lastPathSegment);
            lastPathSegment = sb.toString();
        }
        String str4 = lastPathSegment;
        Context context2 = context;
        File a2 = a(str4, context);
        if (!a(a2, str, list, z, eVar)) {
            return null;
        }
        r rVar = this.b;
        rVar.b("FileManager", "Caching succeeded for file " + str4);
        return z2 ? Uri.fromFile(a2).toString() : str4;
    }

    public void a(Context context) {
        if (b() && this.f1878a.N()) {
            this.b.b("FileManager", "Compacting cache...");
            synchronized (this.c) {
                a(c(context), context);
            }
        }
    }

    public boolean a(ByteArrayOutputStream byteArrayOutputStream, File file) {
        if (file == null) {
            return false;
        }
        r rVar = this.b;
        rVar.b("FileManager", "Caching " + file.getAbsolutePath() + "...");
        if (byteArrayOutputStream == null || byteArrayOutputStream.size() <= 0) {
            r rVar2 = this.b;
            rVar2.d("FileManager", "No data for " + file.getAbsolutePath());
            return false;
        } else if (!b(byteArrayOutputStream, file)) {
            r rVar3 = this.b;
            rVar3.e("FileManager", "Unable to cache " + file.getAbsolutePath());
            return false;
        } else {
            r rVar4 = this.b;
            rVar4.b("FileManager", "Caching completed for " + file);
            return true;
        }
    }

    public boolean a(File file, String str, List<String> list, e eVar) {
        return a(file, str, list, true, eVar);
    }

    public void b(Context context) {
        try {
            a(".nomedia", context);
            File file = new File(e(context), ".nomedia");
            if (!file.exists()) {
                r rVar = this.b;
                rVar.b("FileManager", "Creating .nomedia file at " + file.getAbsolutePath());
                if (!file.createNewFile()) {
                    this.b.e("FileManager", "Failed to create .nomedia file");
                }
            }
        } catch (IOException e) {
            this.b.b("FileManager", "Failed to create .nomedia file", e);
        }
    }

    public boolean b(String str, Context context) {
        boolean z;
        synchronized (this.c) {
            File a2 = a(str, context);
            z = a2 != null && a2.exists() && !a2.isDirectory();
        }
        return z;
    }
}
