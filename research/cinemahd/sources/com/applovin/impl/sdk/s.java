package com.applovin.impl.sdk;

import com.applovin.impl.mediation.a.a;
import com.applovin.impl.mediation.a.e;
import java.util.HashMap;
import java.util.Map;

public class s {

    /* renamed from: a  reason: collision with root package name */
    private final r f1884a;
    private final Map<String, a> b = new HashMap(4);
    private final Object c = new Object();

    s(l lVar) {
        this.f1884a = lVar.j0();
    }

    public String a(String str) {
        String d;
        synchronized (this.c) {
            a aVar = this.b.get(str);
            d = aVar != null ? aVar.d() : null;
        }
        return d;
    }

    public void a(a aVar) {
        synchronized (this.c) {
            r rVar = this.f1884a;
            rVar.b("MediationWaterfallWinnerTracker", "Tracking winning ad: " + aVar);
            this.b.put(aVar.getAdUnitId(), aVar);
        }
    }

    public void b(a aVar) {
        synchronized (this.c) {
            String adUnitId = aVar.getAdUnitId();
            e eVar = this.b.get(adUnitId);
            if (aVar == eVar) {
                r rVar = this.f1884a;
                rVar.b("MediationWaterfallWinnerTracker", "Clearing previous winning ad: " + eVar);
                this.b.remove(adUnitId);
            } else {
                r rVar2 = this.f1884a;
                rVar2.b("MediationWaterfallWinnerTracker", "Previous winner not cleared for ad: " + aVar + " , since it could have already been updated with a new ad: " + eVar);
            }
        }
    }
}
