package com.applovin.impl.sdk;

import com.applovin.impl.mediation.c.c;
import com.applovin.impl.sdk.c.b;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.concurrent.atomic.AtomicBoolean;

public class q {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1879a;
    /* access modifiers changed from: private */
    public final AtomicBoolean b = new AtomicBoolean();
    /* access modifiers changed from: private */
    public long c;
    private final Object d = new Object();
    /* access modifiers changed from: private */
    public final AtomicBoolean e = new AtomicBoolean();
    /* access modifiers changed from: private */
    public long f;
    private Object g;

    q(l lVar) {
        this.f1879a = lVar;
    }

    public void a(final Object obj) {
        this.f1879a.J().a(obj);
        if (!c.a(obj) && this.b.compareAndSet(false, true)) {
            this.g = obj;
            this.c = System.currentTimeMillis();
            r j0 = this.f1879a.j0();
            j0.b("FullScreenAdTracker", "Setting fullscreen ad displayed: " + this.c);
            this.f1879a.I().sendBroadcastWithAdObject("com.applovin.fullscreen_ad_displayed", obj);
            final long longValue = ((Long) this.f1879a.a(b.f1)).longValue();
            if (longValue >= 0) {
                AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                    public void run() {
                        if (q.this.b.get() && System.currentTimeMillis() - q.this.c >= longValue) {
                            q.this.f1879a.j0().b("FullScreenAdTracker", "Resetting \"display\" state...");
                            q.this.b(obj);
                        }
                    }
                }, longValue);
            }
        }
    }

    public void a(boolean z) {
        synchronized (this.d) {
            this.e.set(z);
            if (z) {
                this.f = System.currentTimeMillis();
                r j0 = this.f1879a.j0();
                j0.b("FullScreenAdTracker", "Setting fullscreen ad pending display: " + this.f);
                final long longValue = ((Long) this.f1879a.a(b.e1)).longValue();
                if (longValue >= 0) {
                    AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                        public void run() {
                            if (q.this.a() && System.currentTimeMillis() - q.this.f >= longValue) {
                                q.this.f1879a.j0().b("FullScreenAdTracker", "Resetting \"pending display\" state...");
                                q.this.e.set(false);
                            }
                        }
                    }, longValue);
                }
            } else {
                this.f = 0;
                r j02 = this.f1879a.j0();
                j02.b("FullScreenAdTracker", "Setting fullscreen ad not pending display: " + System.currentTimeMillis());
            }
        }
    }

    public boolean a() {
        return this.e.get();
    }

    public void b(Object obj) {
        this.f1879a.J().b(obj);
        if (!c.a(obj) && this.b.compareAndSet(true, false)) {
            this.g = null;
            r j0 = this.f1879a.j0();
            j0.b("FullScreenAdTracker", "Setting fullscreen ad hidden: " + System.currentTimeMillis());
            this.f1879a.I().sendBroadcastWithAdObject("com.applovin.fullscreen_ad_hidden", obj);
        }
    }

    public boolean b() {
        return this.b.get();
    }

    public Object c() {
        return this.g;
    }
}
