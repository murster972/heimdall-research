package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import org.json.JSONObject;

public class u extends v {
    private final g f;

    public u(g gVar, l lVar) {
        super("TaskReportAppLovinReward", lVar);
        this.f = gVar;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        super.a(i);
        d("Failed to report reward for ad: " + this.f + " - error code: " + i);
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        j.a(jSONObject, "zone_id", this.f.getAdZone().a(), this.f1817a);
        j.a(jSONObject, "fire_percent", this.f.L(), this.f1817a);
        String clCode = this.f.getClCode();
        if (!o.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        j.a(jSONObject, "clcode", clCode, this.f1817a);
    }

    /* access modifiers changed from: protected */
    public void b(JSONObject jSONObject) {
        a("Reported reward successfully for ad: " + this.f);
    }

    /* access modifiers changed from: protected */
    public String e() {
        return "2.0/cr";
    }

    /* access modifiers changed from: protected */
    public c h() {
        return this.f.H();
    }

    /* access modifiers changed from: protected */
    public void i() {
        d("No reward result was found for ad: " + this.f);
    }
}
