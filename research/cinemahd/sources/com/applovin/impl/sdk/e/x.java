package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import org.json.JSONObject;

public abstract class x extends a {
    protected x(String str, l lVar) {
        super(str, lVar);
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        h.a(i, this.f1817a);
    }

    /* access modifiers changed from: protected */
    public abstract void a(JSONObject jSONObject);

    /* access modifiers changed from: package-private */
    public void a(JSONObject jSONObject, final a.c<JSONObject> cVar) {
        AnonymousClass1 r0 = new t<JSONObject>(this, b.a(this.f1817a).a(h.a(e(), this.f1817a)).c(h.b(e(), this.f1817a)).a(h.a(this.f1817a)).b("POST").a(jSONObject).d(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.F3)).booleanValue()).a(new JSONObject()).a(f()).a(), this.f1817a) {
            public void a(int i) {
                cVar.a(i);
            }

            public void a(JSONObject jSONObject, int i) {
                cVar.a(jSONObject, i);
            }
        };
        r0.a(com.applovin.impl.sdk.c.b.Y);
        r0.b(com.applovin.impl.sdk.c.b.c0);
        this.f1817a.o().a((a) r0);
    }

    /* access modifiers changed from: protected */
    public abstract String e();

    /* access modifiers changed from: protected */
    public abstract int f();

    /* access modifiers changed from: protected */
    public JSONObject g() {
        JSONObject jSONObject = new JSONObject();
        String V = this.f1817a.V();
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.x2)).booleanValue() && o.b(V)) {
            j.a(jSONObject, "cuid", V, this.f1817a);
        }
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.z2)).booleanValue()) {
            j.a(jSONObject, "compass_random_token", this.f1817a.W(), this.f1817a);
        }
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.B2)).booleanValue()) {
            j.a(jSONObject, "applovin_random_token", this.f1817a.X(), this.f1817a);
        }
        a(jSONObject);
        return jSONObject;
    }
}
