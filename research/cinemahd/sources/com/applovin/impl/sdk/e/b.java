package com.applovin.impl.sdk.e;

import android.text.TextUtils;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.network.d;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.j;
import com.applovin.sdk.AppLovinSdk;
import com.vungle.warren.model.ReportDBAdapter;
import java.util.Map;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

class b extends a {
    b(l lVar) {
        super("TaskApiSubmitData", lVar);
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        try {
            this.f1817a.s().c();
            JSONObject a2 = h.a(jSONObject);
            this.f1817a.h().a((com.applovin.impl.sdk.c.b<?>) com.applovin.impl.sdk.c.b.f, (Object) a2.getString("device_id"));
            this.f1817a.h().a((com.applovin.impl.sdk.c.b<?>) com.applovin.impl.sdk.c.b.h, (Object) a2.getString("device_token"));
            this.f1817a.h().a((com.applovin.impl.sdk.c.b<?>) com.applovin.impl.sdk.c.b.i, (Object) Long.valueOf(a2.getLong("publisher_id")));
            this.f1817a.h().a();
            h.b(a2, this.f1817a);
            h.c(a2, this.f1817a);
            h.e(a2, this.f1817a);
            String b = j.b(a2, "latest_version", "", this.f1817a);
            if (!TextUtils.isEmpty(b) && !AppLovinSdk.VERSION.equals(b)) {
                String str = "Current SDK version (" + AppLovinSdk.VERSION + ") is outdated. Please integrate the latest version of the AppLovin SDK (" + b + "). Doing so will improve your CPMs and ensure you have access to the latest revenue earning features.";
                if (j.a(a2, "sdk_update_message")) {
                    str = j.b(a2, "sdk_update_message", str, this.f1817a);
                }
                r.h("AppLovinSdk", str);
            }
            this.f1817a.p().b();
        } catch (Throwable th) {
            a("Unable to parse API response", th);
        }
    }

    private void b(JSONObject jSONObject) throws JSONException {
        m r = this.f1817a.r();
        Map<String, Object> d = r.d();
        com.applovin.impl.sdk.utils.r.b("platform", "type", d);
        com.applovin.impl.sdk.utils.r.b("api_level", "sdk_version", d);
        jSONObject.put("device_info", new JSONObject(d));
        Map<String, Object> g = r.g();
        com.applovin.impl.sdk.utils.r.b("sdk_version", "applovin_sdk_version", g);
        com.applovin.impl.sdk.utils.r.b("ia", "installed_at", g);
        jSONObject.put("app_info", new JSONObject(g));
    }

    private void c(JSONObject jSONObject) throws JSONException {
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.b3)).booleanValue()) {
            jSONObject.put("stats", this.f1817a.p().c());
        }
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.p)).booleanValue()) {
            JSONObject b = d.b(c());
            if (b.length() > 0) {
                jSONObject.put("network_response_codes", b);
            }
            if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.q)).booleanValue()) {
                d.a(c());
            }
        }
    }

    private void d(JSONObject jSONObject) throws JSONException {
        JSONArray a2;
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.h3)).booleanValue() && (a2 = this.f1817a.s().a()) != null && a2.length() > 0) {
            jSONObject.put(ReportDBAdapter.ReportColumns.COLUMN_ERRORS, a2);
        }
    }

    private void e(JSONObject jSONObject) {
        AnonymousClass1 r0 = new t<JSONObject>(com.applovin.impl.sdk.network.b.a(this.f1817a).a(h.a("2.0/device", this.f1817a)).c(h.b("2.0/device", this.f1817a)).a(h.a(this.f1817a)).b("POST").a(jSONObject).d(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.B3)).booleanValue()).a(new JSONObject()).a(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.g2)).intValue()).a(), this.f1817a) {
            public void a(int i) {
                h.a(i, this.f1817a);
            }

            public void a(JSONObject jSONObject, int i) {
                b.this.a(jSONObject);
            }
        };
        r0.a(com.applovin.impl.sdk.c.b.Y);
        r0.b(com.applovin.impl.sdk.c.b.c0);
        this.f1817a.o().a((a) r0);
    }

    public void run() {
        try {
            b("Submitting user data...");
            JSONObject jSONObject = new JSONObject();
            b(jSONObject);
            c(jSONObject);
            d(jSONObject);
            e(jSONObject);
        } catch (JSONException e) {
            a("Unable to build JSON message with collected data", e);
        }
    }
}
