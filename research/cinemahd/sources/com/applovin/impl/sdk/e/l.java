package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.r;
import java.util.Map;
import org.json.JSONObject;

public class l extends a {
    /* access modifiers changed from: private */
    public final a f;

    public interface a {
        void a();
    }

    public l(com.applovin.impl.sdk.l lVar, a aVar) {
        super("TaskFetchVariables", lVar);
        this.f = aVar;
    }

    private Map<String, String> e() {
        return r.b(this.f1817a.r().a((Map<String, String>) null, false, false));
    }

    public void run() {
        AnonymousClass1 r1 = new t<JSONObject>(b.a(this.f1817a).a(h.e(this.f1817a)).c(h.f(this.f1817a)).a(e()).b("GET").a(new JSONObject()).b(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.n2)).intValue()).a(), this.f1817a) {
            public void a(int i) {
                d("Unable to fetch variables: server returned " + i);
                com.applovin.impl.sdk.r.i("AppLovinVariableService", "Failed to load variables.");
                l.this.f.a();
            }

            public void a(JSONObject jSONObject, int i) {
                h.b(jSONObject, this.f1817a);
                h.a(jSONObject, this.f1817a);
                h.f(jSONObject, this.f1817a);
                h.c(jSONObject, this.f1817a);
                l.this.f.a();
            }
        };
        r1.a(com.applovin.impl.sdk.c.b.f0);
        r1.b(com.applovin.impl.sdk.c.b.g0);
        this.f1817a.o().a((a) r1);
    }
}
