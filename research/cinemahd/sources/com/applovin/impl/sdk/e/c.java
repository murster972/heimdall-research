package com.applovin.impl.sdk.e;

import android.net.Uri;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.mediation.k;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.d.e;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.p;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdkUtils;
import com.vungle.warren.analytics.AnalyticsEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

abstract class c extends a implements k.a {
    protected final g f;
    /* access modifiers changed from: private */
    public AppLovinAdLoadListener g;
    private final p h;
    private final Collection<Character> i;
    private final e j;
    private boolean k;

    c(String str, g gVar, l lVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super(str, lVar);
        if (gVar != null) {
            this.f = gVar;
            this.g = appLovinAdLoadListener;
            this.h = lVar.x();
            this.i = j();
            this.j = new e();
            return;
        }
        throw new IllegalArgumentException("No ad specified.");
    }

    private Uri a(Uri uri, String str) {
        String str2;
        StringBuilder sb;
        if (uri != null) {
            String uri2 = uri.toString();
            if (o.b(uri2)) {
                a("Caching " + str + " image...");
                return g(uri2);
            }
            sb = new StringBuilder();
            sb.append("Failed to cache ");
            sb.append(str);
            str2 = " image";
        } else {
            sb = new StringBuilder();
            sb.append("No ");
            sb.append(str);
            str2 = " image to cache";
        }
        sb.append(str2);
        a(sb.toString());
        return null;
    }

    private Uri a(String str, String str2) {
        StringBuilder sb;
        String replace = str2.replace("/", "_");
        String i2 = this.f.i();
        if (o.b(i2)) {
            replace = i2 + replace;
        }
        File a2 = this.h.a(replace, this.f1817a.i());
        if (a2 == null) {
            return null;
        }
        if (a2.exists()) {
            this.j.b(a2.length());
            sb = new StringBuilder();
        } else {
            if (!this.h.a(a2, str + str2, Arrays.asList(new String[]{str}), this.j)) {
                return null;
            }
            sb = new StringBuilder();
        }
        sb.append("file://");
        sb.append(a2.getAbsolutePath());
        return Uri.parse(sb.toString());
    }

    private Uri g(String str) {
        return b(str, this.f.h(), true);
    }

    private Collection<Character> j() {
        HashSet hashSet = new HashSet();
        for (char valueOf : ((String) this.f1817a.a(b.s0)).toCharArray()) {
            hashSet.add(Character.valueOf(valueOf));
        }
        hashSet.add('\"');
        return hashSet;
    }

    /* access modifiers changed from: package-private */
    public Uri a(String str, List<String> list, boolean z) {
        String str2;
        if (!o.b(str)) {
            return null;
        }
        a("Caching video " + str + "...");
        String a2 = this.h.a(c(), str, this.f.i(), list, z, this.j);
        if (o.b(a2)) {
            File a3 = this.h.a(a2, c());
            if (a3 != null) {
                Uri fromFile = Uri.fromFile(a3);
                if (fromFile != null) {
                    a("Finish caching video for ad #" + this.f.getAdIdNumber() + ". Updating ad with cachedVideoFilename = " + a2);
                    return fromFile;
                }
                str2 = "Unable to create URI from cached video file = " + a3;
            } else {
                str2 = "Unable to cache video = " + str + "Video file was missing or null";
            }
            d(str2);
            return null;
        }
        d("Failed to cache video");
        h();
        return null;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, List<String> list) {
        if (o.b(str)) {
            Uri parse = Uri.parse(str);
            if (parse == null) {
                a("Nothing to cache, skipping...");
                return null;
            }
            String lastPathSegment = parse.getLastPathSegment();
            if (o.b(this.f.i())) {
                lastPathSegment = this.f.i() + lastPathSegment;
            }
            File a2 = this.h.a(lastPathSegment, c());
            ByteArrayOutputStream a3 = (a2 == null || !a2.exists()) ? null : this.h.a(a2);
            if (a3 == null) {
                a3 = this.h.a(str, list, true);
                if (a3 != null) {
                    this.h.a(a3, a2);
                    this.j.a((long) a3.size());
                }
            } else {
                this.j.b((long) a3.size());
            }
            try {
                return a3.toString("UTF-8");
            } catch (UnsupportedEncodingException e) {
                a("UTF-8 encoding not supported.", e);
            } catch (Throwable th) {
                a("String resource at " + str + " failed to load.", th);
                return null;
            }
        }
        return null;
    }

    /* access modifiers changed from: package-private */
    public String a(String str, List<String> list, g gVar) {
        if (!o.b(str)) {
            return str;
        }
        if (!((Boolean) this.f1817a.a(b.t0)).booleanValue()) {
            a("Resource caching is disabled, skipping cache...");
            return str;
        }
        StringBuilder sb = new StringBuilder(str);
        boolean shouldCancelHtmlCachingIfShown = gVar.shouldCancelHtmlCachingIfShown();
        for (String next : list) {
            int i2 = 0;
            int i3 = 0;
            while (i2 < sb.length()) {
                if (f()) {
                    return str;
                }
                i2 = sb.indexOf(next, i3);
                if (i2 == -1) {
                    continue;
                    break;
                }
                int length = sb.length();
                int i4 = i2;
                while (!this.i.contains(Character.valueOf(sb.charAt(i4))) && i4 < length) {
                    i4++;
                }
                if (i4 <= i2 || i4 == length) {
                    d("Unable to cache resource; ad HTML is invalid.");
                    return str;
                }
                String substring = sb.substring(next.length() + i2, i4);
                if (!o.b(substring)) {
                    a("Skip caching of non-resource " + substring);
                } else if (!shouldCancelHtmlCachingIfShown || !gVar.hasShown()) {
                    Uri a2 = a(next, substring);
                    if (a2 != null) {
                        sb.replace(i2, i4, a2.toString());
                        gVar.a(a2);
                        this.j.e();
                    } else {
                        this.j.f();
                    }
                } else {
                    a("Cancelling HTML caching due to ad being shown already");
                    this.j.a();
                    return str;
                }
                i3 = i4;
            }
        }
        return sb.toString();
    }

    public void a(a aVar) {
        if (aVar.o().equalsIgnoreCase(this.f.k())) {
            d("Updating flag for timeout...");
            this.k = true;
        }
        this.f1817a.e().b(this);
    }

    /* access modifiers changed from: package-private */
    public void a(AppLovinAdBase appLovinAdBase) {
        d.a(this.j, appLovinAdBase, this.f1817a);
    }

    /* access modifiers changed from: package-private */
    public Uri b(String str, List<String> list, boolean z) {
        String str2;
        try {
            String a2 = this.h.a(c(), str, this.f.i(), list, z, this.j);
            if (!o.b(a2)) {
                return null;
            }
            File a3 = this.h.a(a2, c());
            if (a3 != null) {
                Uri fromFile = Uri.fromFile(a3);
                if (fromFile != null) {
                    return fromFile;
                }
                str2 = "Unable to extract Uri from image file";
            } else {
                str2 = "Unable to retrieve File from cached image filename = " + a2;
            }
            d(str2);
            return null;
        } catch (Throwable th) {
            a("Failed to cache image at url = " + str, th);
            return null;
        }
    }

    /* access modifiers changed from: package-private */
    public Uri e(String str) {
        return a(str, this.f.h(), true);
    }

    /* access modifiers changed from: protected */
    public void e() {
        this.f1817a.e().b(this);
    }

    /* access modifiers changed from: package-private */
    public String f(final String str) {
        if (!o.b(str)) {
            return null;
        }
        com.applovin.impl.sdk.network.b a2 = com.applovin.impl.sdk.network.b.a(this.f1817a).a(str).b("GET").a("").a(0).a();
        final AtomicReference atomicReference = new AtomicReference((Object) null);
        this.f1817a.n().a(a2, new a.C0016a(), new a.c<String>() {
            public void a(int i) {
                c cVar = c.this;
                cVar.d("Failed to load resource from '" + str + "'");
            }

            public void a(String str, int i) {
                atomicReference.set(str);
            }
        });
        String str2 = (String) atomicReference.get();
        if (str2 != null) {
            this.j.a((long) str2.length());
        }
        return str2;
    }

    /* access modifiers changed from: protected */
    public boolean f() {
        return this.k;
    }

    /* access modifiers changed from: package-private */
    public void g() {
        a("Caching mute images...");
        Uri a2 = a(this.f.D(), AnalyticsEvent.Ad.mute);
        if (a2 != null) {
            this.f.b(a2);
        }
        Uri a3 = a(this.f.E(), AnalyticsEvent.Ad.unmute);
        if (a3 != null) {
            this.f.c(a3);
        }
        a("Ad updated with muteImageFilename = " + this.f.D() + ", unmuteImageFilename = " + this.f.E());
    }

    /* access modifiers changed from: package-private */
    public void h() {
        AppLovinAdLoadListener appLovinAdLoadListener = this.g;
        if (appLovinAdLoadListener != null) {
            appLovinAdLoadListener.failedToReceiveAd(AppLovinErrorCodes.UNABLE_TO_PRECACHE_VIDEO_RESOURCES);
            this.g = null;
        }
    }

    /* access modifiers changed from: package-private */
    public void i() {
        a("Rendered new ad:" + this.f);
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                if (c.this.g != null) {
                    c.this.g.adReceived(c.this.f);
                    AppLovinAdLoadListener unused = c.this.g = null;
                }
            }
        });
    }

    public void run() {
        if (this.f.j()) {
            a("Subscribing to timeout events...");
            this.f1817a.e().a((k.a) this);
        }
    }
}
