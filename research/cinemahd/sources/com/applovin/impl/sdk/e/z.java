package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.Collections;
import java.util.Map;
import org.json.JSONObject;

public class z extends aa {
    private final g f;
    private final AppLovinAdRewardListener g;

    public z(g gVar, AppLovinAdRewardListener appLovinAdRewardListener, l lVar) {
        super("TaskValidateAppLovinReward", lVar);
        this.f = gVar;
        this.g = appLovinAdRewardListener;
    }

    /* access modifiers changed from: protected */
    public void a(int i) {
        String str;
        super.a(i);
        if (i < 400 || i >= 500) {
            this.g.validationRequestFailed(this.f, i);
            str = "network_timeout";
        } else {
            this.g.userRewardRejected(this.f, Collections.emptyMap());
            str = "rejected";
        }
        this.f.a(c.a(str));
    }

    /* access modifiers changed from: protected */
    public void a(c cVar) {
        this.f.a(cVar);
        String b = cVar.b();
        Map<String, String> a2 = cVar.a();
        if (b.equals("accepted")) {
            this.g.userRewardVerified(this.f, a2);
        } else if (b.equals("quota_exceeded")) {
            this.g.userOverQuota(this.f, a2);
        } else if (b.equals("rejected")) {
            this.g.userRewardRejected(this.f, a2);
        } else {
            this.g.validationRequestFailed(this.f, AppLovinErrorCodes.INCENTIVIZED_UNKNOWN_SERVER_ERROR);
        }
    }

    /* access modifiers changed from: protected */
    public void a(JSONObject jSONObject) {
        j.a(jSONObject, "zone_id", this.f.getAdZone().a(), this.f1817a);
        String clCode = this.f.getClCode();
        if (!o.b(clCode)) {
            clCode = "NO_CLCODE";
        }
        j.a(jSONObject, "clcode", clCode, this.f1817a);
    }

    public String e() {
        return "2.0/vr";
    }

    /* access modifiers changed from: protected */
    public boolean h() {
        return this.f.F();
    }
}
