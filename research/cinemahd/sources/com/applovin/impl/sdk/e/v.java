package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.utils.j;
import java.util.Map;
import org.json.JSONObject;

public abstract class v extends x {
    protected v(String str, l lVar) {
        super(str, lVar);
    }

    private JSONObject a(c cVar) {
        JSONObject g = g();
        j.a(g, "result", cVar.b(), this.f1817a);
        Map<String, String> a2 = cVar.a();
        if (a2 != null) {
            j.a(g, "params", new JSONObject(a2), this.f1817a);
        }
        return g;
    }

    /* access modifiers changed from: protected */
    public abstract void b(JSONObject jSONObject);

    /* access modifiers changed from: protected */
    public int f() {
        return ((Integer) this.f1817a.a(b.x0)).intValue();
    }

    /* access modifiers changed from: protected */
    public abstract c h();

    /* access modifiers changed from: protected */
    public abstract void i();

    public void run() {
        c h = h();
        if (h != null) {
            a(a(h), new a.c<JSONObject>() {
                public void a(int i) {
                    v.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    v.this.b(jSONObject);
                }
            });
        } else {
            i();
        }
    }
}
