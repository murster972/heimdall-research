package com.applovin.impl.sdk.e;

import android.text.TextUtils;
import com.applovin.impl.sdk.c.c;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinErrorCodes;
import java.util.concurrent.TimeUnit;

public abstract class t<T> extends a implements a.c<T> {
    /* access modifiers changed from: private */
    public final b<T> f;
    private final a.c<T> g;
    /* access modifiers changed from: private */
    public o.a h;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.c.b<String> i;
    /* access modifiers changed from: private */
    public com.applovin.impl.sdk.c.b<String> j;
    protected a.C0016a k;

    public t(b<T> bVar, l lVar) {
        this(bVar, lVar, false);
    }

    public t(b<T> bVar, final l lVar, boolean z) {
        super("TaskRepeatRequest", lVar, z);
        this.h = o.a.BACKGROUND;
        this.i = null;
        this.j = null;
        if (bVar != null) {
            this.f = bVar;
            this.k = new a.C0016a();
            this.g = new a.c<T>() {
                public void a(int i) {
                    com.applovin.impl.sdk.c.b bVar;
                    t tVar;
                    boolean z = false;
                    boolean z2 = i < 200 || i >= 500;
                    boolean z3 = i == 429;
                    if ((i != -103) && (z2 || z3 || t.this.f.n())) {
                        String f = t.this.f.f();
                        if (t.this.f.i() > 0) {
                            t tVar2 = t.this;
                            tVar2.c("Unable to send request due to server failure (code " + i + "). " + t.this.f.i() + " attempts left, retrying in " + TimeUnit.MILLISECONDS.toSeconds((long) t.this.f.l()) + " seconds...");
                            int i2 = t.this.f.i() - 1;
                            t.this.f.a(i2);
                            if (i2 == 0) {
                                t tVar3 = t.this;
                                tVar3.c(tVar3.i);
                                if (com.applovin.impl.sdk.utils.o.b(f) && f.length() >= 4) {
                                    t tVar4 = t.this;
                                    tVar4.b("Switching to backup endpoint " + f);
                                    t.this.f.a(f);
                                    z = true;
                                }
                            }
                            long millis = (!((Boolean) lVar.a(com.applovin.impl.sdk.c.b.o2)).booleanValue() || !z) ? t.this.f.m() ? TimeUnit.SECONDS.toMillis((long) Math.pow(2.0d, (double) t.this.f.j())) : (long) t.this.f.l() : 0;
                            o o = lVar.o();
                            t tVar5 = t.this;
                            o.a(tVar5, tVar5.h, millis);
                            return;
                        }
                        if (f == null || !f.equals(t.this.f.a())) {
                            tVar = t.this;
                            bVar = tVar.i;
                        } else {
                            tVar = t.this;
                            bVar = tVar.j;
                        }
                        tVar.c(bVar);
                    }
                    t.this.a(i);
                }

                public void a(T t, int i) {
                    t.this.f.a(0);
                    t.this.a(t, i);
                }
            };
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    /* access modifiers changed from: private */
    public <ST> void c(com.applovin.impl.sdk.c.b<ST> bVar) {
        if (bVar != null) {
            c h2 = a().h();
            h2.a((com.applovin.impl.sdk.c.b<?>) bVar, (Object) bVar.b());
            h2.a();
        }
    }

    public abstract void a(int i2);

    public void a(com.applovin.impl.sdk.c.b<String> bVar) {
        this.i = bVar;
    }

    public void a(o.a aVar) {
        this.h = aVar;
    }

    public abstract void a(T t, int i2);

    public void b(com.applovin.impl.sdk.c.b<String> bVar) {
        this.j = bVar;
    }

    public void run() {
        int i2;
        a n = a().n();
        if (!a().N() && !a().O()) {
            d("AppLovin SDK is disabled: please check your connection");
            r.i("AppLovinSdk", "AppLovin SDK is disabled: please check your connection");
            i2 = -22;
        } else if (!com.applovin.impl.sdk.utils.o.b(this.f.a()) || this.f.a().length() < 4) {
            d("Task has an invalid or null request endpoint.");
            i2 = AppLovinErrorCodes.INVALID_URL;
        } else {
            if (TextUtils.isEmpty(this.f.b())) {
                this.f.b(this.f.e() != null ? "POST" : "GET");
            }
            n.a(this.f, this.k, this.g);
            return;
        }
        a(i2);
    }
}
