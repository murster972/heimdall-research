package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.a;
import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

class r extends a {
    private final JSONObject f;
    private final JSONObject g;
    private final AppLovinAdLoadListener h;
    private final b i;

    r(JSONObject jSONObject, JSONObject jSONObject2, b bVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super("TaskRenderAppLovinAd", lVar);
        this.f = jSONObject;
        this.g = jSONObject2;
        this.i = bVar;
        this.h = appLovinAdLoadListener;
    }

    public void run() {
        a("Rendering ad...");
        a aVar = new a(this.f, this.g, this.i, this.f1817a);
        boolean booleanValue = j.a(this.f, "gs_load_immediately", (Boolean) false, this.f1817a).booleanValue();
        boolean booleanValue2 = j.a(this.f, "vs_load_immediately", (Boolean) true, this.f1817a).booleanValue();
        d dVar = new d(aVar, this.f1817a, this.h);
        dVar.a(booleanValue2);
        dVar.b(booleanValue);
        o.a aVar2 = o.a.CACHING_OTHER;
        if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.o0)).booleanValue()) {
            if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.REGULAR) {
                aVar2 = o.a.CACHING_INTERSTITIAL;
            } else if (aVar.getSize() == AppLovinAdSize.INTERSTITIAL && aVar.getType() == AppLovinAdType.INCENTIVIZED) {
                aVar2 = o.a.CACHING_INCENTIVIZED;
            }
        }
        this.f1817a.o().a((a) dVar, aVar2);
    }
}
