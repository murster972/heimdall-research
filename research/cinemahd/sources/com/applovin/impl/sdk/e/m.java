package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.f;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.r;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class m extends a {
    private final f.b f;
    private final f.b g;
    private final JSONArray h;
    private final MaxAdFormat i;

    public m(f.b bVar, f.b bVar2, JSONArray jSONArray, MaxAdFormat maxAdFormat, l lVar) {
        super("TaskFlushZones", lVar);
        this.f = bVar;
        this.g = bVar2;
        this.h = jSONArray;
        this.i = maxAdFormat;
    }

    private JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        j.b(jSONObject, "ts_s", TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), this.f1817a);
        if (this.f != f.b.UNKNOWN_ZONE) {
            j.a(jSONObject, "format", this.i.getLabel(), this.f1817a);
            j.a(jSONObject, "previous_trigger_code", this.g.a(), this.f1817a);
            j.a(jSONObject, "previous_trigger_reason", this.g.b(), this.f1817a);
        }
        j.a(jSONObject, "trigger_code", this.f.a(), this.f1817a);
        j.a(jSONObject, "trigger_reason", this.f.b(), this.f1817a);
        j.a(jSONObject, "zones", this.h, this.f1817a);
        return jSONObject;
    }

    public Map<String, String> e() {
        com.applovin.impl.sdk.m r = this.f1817a.r();
        Map<String, Object> d = r.d();
        d.putAll(r.g());
        d.putAll(r.h());
        if (!((Boolean) this.f1817a.a(b.u3)).booleanValue()) {
            d.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1817a.h0());
        }
        return r.b(d);
    }

    public void run() {
        Map<String, String> e = e();
        JSONObject f2 = f();
        String a2 = h.a((String) this.f1817a.a(b.X3), "1.0/flush_zones", this.f1817a);
        AnonymousClass1 r1 = new t<JSONObject>(this, com.applovin.impl.sdk.network.b.a(this.f1817a).a(a2).c(h.a((String) this.f1817a.a(b.Y3), "1.0/flush_zones", this.f1817a)).a(e).a(f2).d(((Boolean) this.f1817a.a(b.G3)).booleanValue()).b("POST").a(new JSONObject()).b(((Integer) this.f1817a.a(b.Z3)).intValue()).a(), this.f1817a) {
            public void a(int i) {
            }

            public void a(JSONObject jSONObject, int i) {
                h.b(jSONObject, this.f1817a);
            }
        };
        r1.a(b.f0);
        r1.b(b.g0);
        this.f1817a.o().a((a) r1);
    }
}
