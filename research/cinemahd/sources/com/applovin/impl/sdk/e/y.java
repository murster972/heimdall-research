package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.l;

public class y extends a {
    private final Runnable f;

    public y(l lVar, Runnable runnable) {
        this(lVar, false, runnable);
    }

    public y(l lVar, boolean z, Runnable runnable) {
        super("TaskRunnable", lVar, z);
        this.f = runnable;
    }

    public void run() {
        this.f.run();
    }
}
