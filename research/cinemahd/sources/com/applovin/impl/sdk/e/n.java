package com.applovin.impl.sdk.e;

import android.app.Activity;
import android.os.Build;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.q;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinSdk;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class n extends a {
    /* access modifiers changed from: private */
    public final l f;

    public n(l lVar) {
        super("TaskInitializeSdk", lVar);
        this.f = lVar;
    }

    private void e() {
        if (!this.f.a().a()) {
            Activity L = this.f.L();
            if (L != null) {
                this.f.a().a(L);
            } else {
                this.f.o().a(new y(this.f, true, new Runnable() {
                    public void run() {
                        n.this.f.a().a(n.this.f.D().a());
                    }
                }), o.a.MAIN, TimeUnit.SECONDS.toMillis(1));
            }
        }
    }

    private void f() {
        String str;
        if (!this.f.P()) {
            boolean d = this.f.h().d();
            if (d) {
                str = this.f.r().j().b + " (use this for test devices)";
            } else {
                str = "<Enable verbose logging to see the GAID to use for test devices - https://monetization-support.applovin.com/hc/en-us/articles/236114328-How-can-I-expose-verbose-logging-for-the-SDK>";
            }
            Map<String, Object> b = this.f.r().b();
            Map<String, Object> c = this.f.r().c();
            com.applovin.impl.sdk.utils.l lVar = new com.applovin.impl.sdk.utils.l();
            lVar.a().a("AppLovin SDK");
            lVar.a("Version", AppLovinSdk.VERSION).a("Plugin Version", this.f.a(b.D2)).a("Ad Review Version", r.f());
            lVar.a("OS", r.e() + " " + Build.VERSION.SDK_INT).a("Target SDK", c.get("target_sdk")).a("GAID", str).a("SDK Key", this.f.h0());
            lVar.a("Model", b.get("model")).a("Locale", b.get("locale")).a("Emulator", b.get("sim"));
            lVar.a("Application ID", c.get("package_name")).a("Test Mode On", Boolean.valueOf(this.f.g().a())).a("Verbose Logging On", Boolean.valueOf(d));
            lVar.a("Mediation Provider", this.f.b0()).a("TG", q.a(this.f));
            lVar.a("===Privacy States===\nPlease review AppLovin MAX documentation to be compliant with regional privacy policies.").a(i.a(c()));
            lVar.a();
            com.applovin.impl.sdk.r.f("AppLovinSdk", lVar.toString());
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x012a, code lost:
        if (r12.f.O() != false) goto L_0x0178;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:23:0x0174, code lost:
        if (r12.f.O() == false) goto L_0x0177;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:24:0x0177, code lost:
        r2 = "failed";
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x0178, code lost:
        r8.append(r2);
        r8.append(" in ");
        r8.append(java.lang.System.currentTimeMillis() - r6);
        r8.append("ms");
        a(r8.toString());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x0190, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void run() {
        /*
            r12 = this;
            java.lang.String r0 = "ms"
            java.lang.String r1 = " in "
            java.lang.String r2 = "succeeded"
            java.lang.String r3 = "failed"
            java.lang.String r4 = " initialization "
            java.lang.String r5 = "AppLovin SDK "
            long r6 = java.lang.System.currentTimeMillis()
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            java.lang.String r9 = "Initializing AppLovin SDK v"
            r8.append(r9)
            java.lang.String r9 = com.applovin.sdk.AppLovinSdk.VERSION
            r8.append(r9)
            java.lang.String r9 = "..."
            r8.append(r9)
            java.lang.String r8 = r8.toString()
            r12.a(r8)
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.d.h r8 = r8.p()     // Catch:{ all -> 0x012d }
            r8.d()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.d.h r8 = r8.p()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.d.g r9 = com.applovin.impl.sdk.d.g.e     // Catch:{ all -> 0x012d }
            r8.c(r9)     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.p r8 = r8.x()     // Catch:{ all -> 0x012d }
            android.content.Context r9 = r12.c()     // Catch:{ all -> 0x012d }
            r8.a((android.content.Context) r9)     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.p r8 = r8.x()     // Catch:{ all -> 0x012d }
            android.content.Context r9 = r12.c()     // Catch:{ all -> 0x012d }
            r8.b((android.content.Context) r9)     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.d.c r8 = r8.A()     // Catch:{ all -> 0x012d }
            r8.a()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.e.o r8 = r8.o()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.e.b r9 = new com.applovin.impl.sdk.e.b     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r10 = r12.f     // Catch:{ all -> 0x012d }
            r9.<init>(r10)     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.e.o$a r10 = com.applovin.impl.sdk.e.o.a.MAIN     // Catch:{ all -> 0x012d }
            r8.a((com.applovin.impl.sdk.e.a) r9, (com.applovin.impl.sdk.e.o.a) r10)     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.m r8 = r8.r()     // Catch:{ all -> 0x012d }
            r8.l()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.m r8 = r8.r()     // Catch:{ all -> 0x012d }
            r8.e()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.utils.n r8 = r8.G()     // Catch:{ all -> 0x012d }
            r8.a()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.n r8 = r8.J()     // Catch:{ all -> 0x012d }
            r8.a()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            r9 = 1
            r8.a((boolean) r9)     // Catch:{ all -> 0x012d }
            r12.f()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.network.e r8 = r8.q()     // Catch:{ all -> 0x012d }
            r8.a()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.sdk.AppLovinEventService r8 = r8.e0()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.EventServiceImpl r8 = (com.applovin.impl.sdk.EventServiceImpl) r8     // Catch:{ all -> 0x012d }
            r8.maybeTrackAppOpenEvent()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r9 = com.applovin.impl.sdk.c.b.Z2     // Catch:{ all -> 0x012d }
            java.lang.Object r8 = r8.a(r9)     // Catch:{ all -> 0x012d }
            java.lang.Boolean r8 = (java.lang.Boolean) r8     // Catch:{ all -> 0x012d }
            boolean r8 = r8.booleanValue()     // Catch:{ all -> 0x012d }
            if (r8 == 0) goto L_0x00cd
            com.applovin.impl.sdk.e.n$1 r8 = new com.applovin.impl.sdk.e.n$1     // Catch:{ all -> 0x012d }
            r8.<init>()     // Catch:{ all -> 0x012d }
            com.applovin.sdk.AppLovinSdkUtils.runOnUiThread(r8)     // Catch:{ all -> 0x012d }
        L_0x00cd:
            r12.e()     // Catch:{ all -> 0x012d }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            android.content.Context r8 = r8.i()     // Catch:{ all -> 0x012d }
            boolean r8 = com.applovin.impl.sdk.utils.r.e((android.content.Context) r8)     // Catch:{ all -> 0x012d }
            if (r8 != 0) goto L_0x00e8
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.mediation.debugger.a r8 = r8.d()     // Catch:{ all -> 0x012d }
            boolean r8 = r8.b()     // Catch:{ all -> 0x012d }
            if (r8 == 0) goto L_0x00f1
        L_0x00e8:
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x012d }
            com.applovin.impl.mediation.debugger.a r8 = r8.d()     // Catch:{ all -> 0x012d }
            r8.a()     // Catch:{ all -> 0x012d }
        L_0x00f1:
            com.applovin.impl.sdk.l r8 = r12.f
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r9 = com.applovin.impl.sdk.c.b.E
            java.lang.Object r8 = r8.a(r9)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            boolean r8 = r8.booleanValue()
            if (r8 == 0) goto L_0x0114
            com.applovin.impl.sdk.l r8 = r12.f
            com.applovin.impl.sdk.c.b<java.lang.Long> r9 = com.applovin.impl.sdk.c.b.F
            java.lang.Object r8 = r8.a(r9)
            java.lang.Long r8 = (java.lang.Long) r8
            long r8 = r8.longValue()
            com.applovin.impl.sdk.l r10 = r12.f
            r10.a((long) r8)
        L_0x0114:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r5)
            java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
            r8.append(r5)
            r8.append(r4)
            com.applovin.impl.sdk.l r4 = r12.f
            boolean r4 = r4.O()
            if (r4 == 0) goto L_0x0177
            goto L_0x0178
        L_0x012d:
            r8 = move-exception
            java.lang.String r9 = "AppLovinSdk"
            java.lang.String r10 = "Failed to initialize SDK!"
            com.applovin.impl.sdk.r.c(r9, r10, r8)     // Catch:{ all -> 0x0191 }
            com.applovin.impl.sdk.l r8 = r12.f     // Catch:{ all -> 0x0191 }
            r9 = 0
            r8.a((boolean) r9)     // Catch:{ all -> 0x0191 }
            com.applovin.impl.sdk.l r8 = r12.f
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r9 = com.applovin.impl.sdk.c.b.E
            java.lang.Object r8 = r8.a(r9)
            java.lang.Boolean r8 = (java.lang.Boolean) r8
            boolean r8 = r8.booleanValue()
            if (r8 == 0) goto L_0x015e
            com.applovin.impl.sdk.l r8 = r12.f
            com.applovin.impl.sdk.c.b<java.lang.Long> r9 = com.applovin.impl.sdk.c.b.F
            java.lang.Object r8 = r8.a(r9)
            java.lang.Long r8 = (java.lang.Long) r8
            long r8 = r8.longValue()
            com.applovin.impl.sdk.l r10 = r12.f
            r10.a((long) r8)
        L_0x015e:
            java.lang.StringBuilder r8 = new java.lang.StringBuilder
            r8.<init>()
            r8.append(r5)
            java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
            r8.append(r5)
            r8.append(r4)
            com.applovin.impl.sdk.l r4 = r12.f
            boolean r4 = r4.O()
            if (r4 == 0) goto L_0x0177
            goto L_0x0178
        L_0x0177:
            r2 = r3
        L_0x0178:
            r8.append(r2)
            r8.append(r1)
            long r1 = java.lang.System.currentTimeMillis()
            long r1 = r1 - r6
            r8.append(r1)
            r8.append(r0)
            java.lang.String r0 = r8.toString()
            r12.a(r0)
            return
        L_0x0191:
            r8 = move-exception
            com.applovin.impl.sdk.l r9 = r12.f
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r10 = com.applovin.impl.sdk.c.b.E
            java.lang.Object r9 = r9.a(r10)
            java.lang.Boolean r9 = (java.lang.Boolean) r9
            boolean r9 = r9.booleanValue()
            if (r9 == 0) goto L_0x01b5
            com.applovin.impl.sdk.l r9 = r12.f
            com.applovin.impl.sdk.c.b<java.lang.Long> r10 = com.applovin.impl.sdk.c.b.F
            java.lang.Object r9 = r9.a(r10)
            java.lang.Long r9 = (java.lang.Long) r9
            long r9 = r9.longValue()
            com.applovin.impl.sdk.l r11 = r12.f
            r11.a((long) r9)
        L_0x01b5:
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r5)
            java.lang.String r5 = com.applovin.sdk.AppLovinSdk.VERSION
            r9.append(r5)
            r9.append(r4)
            com.applovin.impl.sdk.l r4 = r12.f
            boolean r4 = r4.O()
            if (r4 == 0) goto L_0x01ce
            goto L_0x01cf
        L_0x01ce:
            r2 = r3
        L_0x01cf:
            r9.append(r2)
            r9.append(r1)
            long r1 = java.lang.System.currentTimeMillis()
            long r1 = r1 - r6
            r9.append(r1)
            r9.append(r0)
            java.lang.String r0 = r9.toString()
            r12.a(r0)
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.n.run():void");
    }
}
