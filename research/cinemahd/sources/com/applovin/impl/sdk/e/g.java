package com.applovin.impl.sdk.e;

import android.text.TextUtils;
import com.applovin.impl.adview.c;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.h;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinPostbackListener;
import java.util.Iterator;
import org.json.JSONException;
import org.json.JSONObject;

public class g extends a {
    /* access modifiers changed from: private */
    public final com.applovin.impl.sdk.network.g f;
    /* access modifiers changed from: private */
    public final AppLovinPostbackListener g;
    private final o.a h;

    public g(com.applovin.impl.sdk.network.g gVar, o.a aVar, l lVar, AppLovinPostbackListener appLovinPostbackListener) {
        super("TaskDispatchPostback", lVar);
        if (gVar != null) {
            this.f = gVar;
            this.g = appLovinPostbackListener;
            this.h = aVar;
            return;
        }
        throw new IllegalArgumentException("No request specified");
    }

    /* access modifiers changed from: private */
    public void e() {
        AnonymousClass2 r0 = new t<Object>(this.f, a()) {
            final String l = g.this.f.a();

            public void a(int i) {
                d("Failed to dispatch postback. Error code: " + i + " URL: " + this.l);
                if (g.this.g != null) {
                    g.this.g.onPostbackFailure(this.l, i);
                }
                if (g.this.f.r()) {
                    this.f1817a.F().a(g.this.f.s(), this.l, i, (Object) null);
                }
            }

            public void a(Object obj, int i) {
                if (((Boolean) this.f1817a.a(b.J3)).booleanValue()) {
                    if (obj instanceof JSONObject) {
                        JSONObject jSONObject = (JSONObject) obj;
                        Iterator<String> it2 = this.f1817a.b(b.T).iterator();
                        while (true) {
                            if (!it2.hasNext()) {
                                break;
                            }
                            String next = it2.next();
                            if (next.startsWith(next)) {
                                h.b(jSONObject, this.f1817a);
                                h.a(jSONObject, this.f1817a);
                                h.c(jSONObject, this.f1817a);
                                break;
                            }
                        }
                    }
                } else if (obj instanceof String) {
                    for (String next2 : this.f1817a.b(b.T)) {
                        if (next2.startsWith(next2)) {
                            String str = (String) obj;
                            if (!TextUtils.isEmpty(str)) {
                                try {
                                    JSONObject jSONObject2 = new JSONObject(str);
                                    h.b(jSONObject2, this.f1817a);
                                    h.a(jSONObject2, this.f1817a);
                                    h.c(jSONObject2, this.f1817a);
                                    break;
                                } catch (JSONException unused) {
                                    continue;
                                }
                            } else {
                                continue;
                            }
                        }
                    }
                }
                if (g.this.g != null) {
                    g.this.g.onPostbackSuccess(this.l);
                }
                if (g.this.f.r()) {
                    this.f1817a.F().a(g.this.f.s(), this.l, i, obj);
                }
            }
        };
        r0.a(this.h);
        a().o().a((a) r0);
    }

    public void run() {
        if (!com.applovin.impl.sdk.utils.o.b(this.f.a())) {
            b("Requested URL is not valid; nothing to do...");
            AppLovinPostbackListener appLovinPostbackListener = this.g;
            if (appLovinPostbackListener != null) {
                appLovinPostbackListener.onPostbackFailure(this.f.a(), AppLovinErrorCodes.INVALID_URL);
            }
        } else if (this.f.t()) {
            c.a(this.f, (AppLovinPostbackListener) new AppLovinPostbackListener() {
                public void onPostbackFailure(String str, int i) {
                    g.this.e();
                }

                public void onPostbackSuccess(String str) {
                    if (g.this.g != null) {
                        g.this.g.onPostbackSuccess(g.this.f.a());
                    }
                }
            });
        } else {
            e();
        }
    }
}
