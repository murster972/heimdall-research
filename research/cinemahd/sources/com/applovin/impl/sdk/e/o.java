package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.g;
import com.vungle.warren.AdLoader;
import java.lang.Thread;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class o {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1824a;
    /* access modifiers changed from: private */
    public final r b;
    private final ScheduledThreadPoolExecutor c;
    private final ScheduledThreadPoolExecutor d;
    private final ScheduledThreadPoolExecutor e;
    private final ScheduledThreadPoolExecutor f;
    private final ScheduledThreadPoolExecutor g;
    private final ScheduledThreadPoolExecutor h;
    private final ScheduledThreadPoolExecutor i;
    private final ScheduledThreadPoolExecutor j;
    private final ScheduledThreadPoolExecutor k;
    private final ScheduledThreadPoolExecutor l;
    private final ScheduledThreadPoolExecutor m;
    private final ScheduledThreadPoolExecutor n;
    private final ScheduledThreadPoolExecutor o;
    private final ScheduledThreadPoolExecutor p;
    private final ScheduledThreadPoolExecutor q;
    private final ScheduledThreadPoolExecutor r;
    private final ScheduledThreadPoolExecutor s;
    private final ScheduledThreadPoolExecutor t;
    private final ScheduledThreadPoolExecutor u;
    private final ScheduledThreadPoolExecutor v;
    private final List<c> w = new ArrayList(5);
    private final Object x = new Object();
    private boolean y;

    public enum a {
        MAIN,
        TIMEOUT,
        BACKGROUND,
        ADVERTISING_INFO_COLLECTION,
        POSTBACKS,
        CACHING_INTERSTITIAL,
        CACHING_INCENTIVIZED,
        CACHING_OTHER,
        REWARD,
        MEDIATION_MAIN,
        MEDIATION_TIMEOUT,
        MEDIATION_BACKGROUND,
        MEDIATION_POSTBACKS,
        MEDIATION_BANNER,
        MEDIATION_INTERSTITIAL,
        MEDIATION_INCENTIVIZED,
        MEDIATION_REWARDED_INTERSTITIAL,
        MEDIATION_REWARD
    }

    private class b implements ThreadFactory {

        /* renamed from: a  reason: collision with root package name */
        private final String f1827a;

        b(String str) {
            this.f1827a = str;
        }

        public Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, "AppLovinSdk:" + this.f1827a + ":" + com.applovin.impl.sdk.utils.r.a(o.this.f1824a.h0()));
            thread.setDaemon(true);
            thread.setPriority(10);
            thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
                public void uncaughtException(Thread thread, Throwable th) {
                    o.this.b.b("TaskManager", "Caught unhandled exception", th);
                }
            });
            return thread;
        }
    }

    private class c implements Runnable {

        /* renamed from: a  reason: collision with root package name */
        private final String f1829a;
        /* access modifiers changed from: private */
        public final a b;
        /* access modifiers changed from: private */
        public final a c;

        c(a aVar, a aVar2) {
            this.f1829a = aVar.b();
            this.b = aVar;
            this.c = aVar2;
        }

        public void run() {
            long j;
            StringBuilder sb;
            r rVar;
            try {
                g.a();
                if (o.this.f1824a.N()) {
                    if (!this.b.d()) {
                        o.this.b.c(this.f1829a, "Task re-scheduled...");
                        o.this.a(this.b, this.c, AdLoader.RETRY_DELAY);
                        j = o.this.a(this.c) - 1;
                        rVar = o.this.b;
                        sb = new StringBuilder();
                        sb.append(this.c);
                        sb.append(" queue finished task ");
                        sb.append(this.b.b());
                        sb.append(" with queue size ");
                        sb.append(j);
                        rVar.c("TaskManager", sb.toString());
                    }
                }
                this.b.run();
                j = o.this.a(this.c) - 1;
                rVar = o.this.b;
                sb = new StringBuilder();
            } catch (Throwable th) {
                r b2 = o.this.b;
                b2.c("TaskManager", this.c + " queue finished task " + this.b.b() + " with queue size " + (o.this.a(this.c) - 1));
                throw th;
            }
            sb.append(this.c);
            sb.append(" queue finished task ");
            sb.append(this.b.b());
            sb.append(" with queue size ");
            sb.append(j);
            rVar.c("TaskManager", sb.toString());
        }
    }

    public o(l lVar) {
        this.f1824a = lVar;
        this.b = lVar.j0();
        this.c = a("main");
        this.d = a("timeout");
        this.e = a("back");
        this.f = a("advertising_info_collection");
        this.g = a("postbacks");
        this.h = a("caching_interstitial");
        this.i = a("caching_incentivized");
        this.j = a("caching_other");
        this.k = a("reward");
        this.l = a("mediation_main");
        this.m = a("mediation_timeout");
        this.n = a("mediation_background");
        this.o = a("mediation_postbacks");
        this.p = a("mediation_banner");
        this.q = a("mediation_interstitial");
        this.r = a("mediation_incentivized");
        this.s = a("mediation_rewarded_interstitial");
        this.t = a("mediation_reward");
        this.u = a("auxiliary_operations", ((Integer) lVar.a(com.applovin.impl.sdk.c.b.c1)).intValue());
        a("caching_operations", ((Integer) lVar.a(com.applovin.impl.sdk.c.b.d1)).intValue());
        this.v = a("shared_thread_pool", ((Integer) lVar.a(com.applovin.impl.sdk.c.b.u)).intValue());
    }

    /* access modifiers changed from: private */
    public long a(a aVar) {
        long taskCount;
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;
        if (aVar == a.MAIN) {
            taskCount = this.c.getTaskCount();
            scheduledThreadPoolExecutor = this.c;
        } else if (aVar == a.TIMEOUT) {
            taskCount = this.d.getTaskCount();
            scheduledThreadPoolExecutor = this.d;
        } else if (aVar == a.BACKGROUND) {
            taskCount = this.e.getTaskCount();
            scheduledThreadPoolExecutor = this.e;
        } else if (aVar == a.ADVERTISING_INFO_COLLECTION) {
            taskCount = this.f.getTaskCount();
            scheduledThreadPoolExecutor = this.f;
        } else if (aVar == a.POSTBACKS) {
            taskCount = this.g.getTaskCount();
            scheduledThreadPoolExecutor = this.g;
        } else if (aVar == a.CACHING_INTERSTITIAL) {
            taskCount = this.h.getTaskCount();
            scheduledThreadPoolExecutor = this.h;
        } else if (aVar == a.CACHING_INCENTIVIZED) {
            taskCount = this.i.getTaskCount();
            scheduledThreadPoolExecutor = this.i;
        } else if (aVar == a.CACHING_OTHER) {
            taskCount = this.j.getTaskCount();
            scheduledThreadPoolExecutor = this.j;
        } else if (aVar == a.REWARD) {
            taskCount = this.k.getTaskCount();
            scheduledThreadPoolExecutor = this.k;
        } else if (aVar == a.MEDIATION_MAIN) {
            taskCount = this.l.getTaskCount();
            scheduledThreadPoolExecutor = this.l;
        } else if (aVar == a.MEDIATION_TIMEOUT) {
            taskCount = this.m.getTaskCount();
            scheduledThreadPoolExecutor = this.m;
        } else if (aVar == a.MEDIATION_BACKGROUND) {
            taskCount = this.n.getTaskCount();
            scheduledThreadPoolExecutor = this.n;
        } else if (aVar == a.MEDIATION_POSTBACKS) {
            taskCount = this.o.getTaskCount();
            scheduledThreadPoolExecutor = this.o;
        } else if (aVar == a.MEDIATION_BANNER) {
            taskCount = this.p.getTaskCount();
            scheduledThreadPoolExecutor = this.p;
        } else if (aVar == a.MEDIATION_INTERSTITIAL) {
            taskCount = this.q.getTaskCount();
            scheduledThreadPoolExecutor = this.q;
        } else if (aVar == a.MEDIATION_INCENTIVIZED) {
            taskCount = this.r.getTaskCount();
            scheduledThreadPoolExecutor = this.r;
        } else if (aVar == a.MEDIATION_REWARDED_INTERSTITIAL) {
            taskCount = this.s.getTaskCount();
            scheduledThreadPoolExecutor = this.s;
        } else if (aVar != a.MEDIATION_REWARD) {
            return 0;
        } else {
            taskCount = this.t.getTaskCount();
            scheduledThreadPoolExecutor = this.t;
        }
        return taskCount - scheduledThreadPoolExecutor.getCompletedTaskCount();
    }

    private ScheduledThreadPoolExecutor a(String str) {
        return a(str, 1);
    }

    private ScheduledThreadPoolExecutor a(String str, int i2) {
        return new ScheduledThreadPoolExecutor(i2, new b(str));
    }

    private void a(final Runnable runnable, long j2, final ScheduledExecutorService scheduledExecutorService, boolean z) {
        if (j2 <= 0) {
            scheduledExecutorService.submit(runnable);
        } else if (z) {
            d.a(j2, this.f1824a, new Runnable(this) {
                public void run() {
                    scheduledExecutorService.execute(runnable);
                }
            });
        } else {
            scheduledExecutorService.schedule(runnable, j2, TimeUnit.MILLISECONDS);
        }
    }

    private boolean a(c cVar) {
        if (cVar.b.d()) {
            return false;
        }
        synchronized (this.x) {
            if (this.y) {
                return false;
            }
            this.w.add(cVar);
            return true;
        }
    }

    public void a(a aVar) {
        if (aVar != null) {
            try {
                aVar.run();
            } catch (Throwable th) {
                this.b.b(aVar.b(), "Task failed execution", th);
            }
        } else {
            this.b.e("TaskManager", "Attempted to execute null task immediately");
        }
    }

    public void a(a aVar, a aVar2) {
        a(aVar, aVar2, 0);
    }

    public void a(a aVar, a aVar2, long j2) {
        a(aVar, aVar2, j2, false);
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v2, resolved type: com.applovin.impl.sdk.e.o$c} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v30, resolved type: com.applovin.impl.sdk.e.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v31, resolved type: com.applovin.impl.sdk.e.a} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r1v32, resolved type: com.applovin.impl.sdk.e.a} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(com.applovin.impl.sdk.e.a r12, com.applovin.impl.sdk.e.o.a r13, long r14, boolean r16) {
        /*
            r11 = this;
            r6 = r11
            r1 = r12
            r0 = r13
            r2 = r14
            if (r1 == 0) goto L_0x012b
            r4 = 0
            int r7 = (r2 > r4 ? 1 : (r2 == r4 ? 0 : -1))
            if (r7 < 0) goto L_0x0114
            com.applovin.impl.sdk.e.o$c r4 = new com.applovin.impl.sdk.e.o$c
            r4.<init>(r12, r13)
            boolean r5 = r11.a((com.applovin.impl.sdk.e.o.c) r4)
            if (r5 != 0) goto L_0x00f0
            com.applovin.impl.sdk.l r5 = r6.f1824a
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r7 = com.applovin.impl.sdk.c.b.v
            java.lang.Object r5 = r5.a(r7)
            java.lang.Boolean r5 = (java.lang.Boolean) r5
            boolean r5 = r5.booleanValue()
            if (r5 == 0) goto L_0x0033
            java.util.concurrent.ScheduledThreadPoolExecutor r4 = r6.v
            r0 = r11
            r1 = r12
            r2 = r14
        L_0x002c:
            r5 = r16
            r0.a((java.lang.Runnable) r1, (long) r2, (java.util.concurrent.ScheduledExecutorService) r4, (boolean) r5)
            goto L_0x0113
        L_0x0033:
            long r7 = r11.a((com.applovin.impl.sdk.e.o.a) r13)
            r9 = 1
            long r7 = r7 + r9
            com.applovin.impl.sdk.r r5 = r6.b
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            java.lang.String r10 = "Scheduling "
            r9.append(r10)
            java.lang.String r1 = r12.b()
            r9.append(r1)
            java.lang.String r1 = " on "
            r9.append(r1)
            r9.append(r13)
            java.lang.String r1 = " queue in "
            r9.append(r1)
            r9.append(r14)
            java.lang.String r1 = "ms with new queue size "
            r9.append(r1)
            r9.append(r7)
            java.lang.String r1 = r9.toString()
            java.lang.String r7 = "TaskManager"
            r5.b(r7, r1)
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MAIN
            if (r0 != r1) goto L_0x0079
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.c
        L_0x0074:
            r0 = r11
            r1 = r4
            r2 = r14
            r4 = r5
            goto L_0x002c
        L_0x0079:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.TIMEOUT
            if (r0 != r1) goto L_0x0080
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.d
            goto L_0x0074
        L_0x0080:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.BACKGROUND
            if (r0 != r1) goto L_0x0087
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.e
            goto L_0x0074
        L_0x0087:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.ADVERTISING_INFO_COLLECTION
            if (r0 != r1) goto L_0x008e
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.f
            goto L_0x0074
        L_0x008e:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.POSTBACKS
            if (r0 != r1) goto L_0x0095
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.g
            goto L_0x0074
        L_0x0095:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.CACHING_INTERSTITIAL
            if (r0 != r1) goto L_0x009c
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.h
            goto L_0x0074
        L_0x009c:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.CACHING_INCENTIVIZED
            if (r0 != r1) goto L_0x00a3
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.i
            goto L_0x0074
        L_0x00a3:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.CACHING_OTHER
            if (r0 != r1) goto L_0x00aa
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.j
            goto L_0x0074
        L_0x00aa:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.REWARD
            if (r0 != r1) goto L_0x00b1
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.k
            goto L_0x0074
        L_0x00b1:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_MAIN
            if (r0 != r1) goto L_0x00b8
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.l
            goto L_0x0074
        L_0x00b8:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_TIMEOUT
            if (r0 != r1) goto L_0x00bf
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.m
            goto L_0x0074
        L_0x00bf:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_BACKGROUND
            if (r0 != r1) goto L_0x00c6
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.n
            goto L_0x0074
        L_0x00c6:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_POSTBACKS
            if (r0 != r1) goto L_0x00cd
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.o
            goto L_0x0074
        L_0x00cd:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_BANNER
            if (r0 != r1) goto L_0x00d4
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.p
            goto L_0x0074
        L_0x00d4:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_INTERSTITIAL
            if (r0 != r1) goto L_0x00db
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.q
            goto L_0x0074
        L_0x00db:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_INCENTIVIZED
            if (r0 != r1) goto L_0x00e2
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.r
            goto L_0x0074
        L_0x00e2:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_REWARDED_INTERSTITIAL
            if (r0 != r1) goto L_0x00e9
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.s
            goto L_0x0074
        L_0x00e9:
            com.applovin.impl.sdk.e.o$a r1 = com.applovin.impl.sdk.e.o.a.MEDIATION_REWARD
            if (r0 != r1) goto L_0x0113
            java.util.concurrent.ScheduledThreadPoolExecutor r5 = r6.t
            goto L_0x0074
        L_0x00f0:
            com.applovin.impl.sdk.r r0 = r6.b
            java.lang.String r2 = r12.b()
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>()
            java.lang.String r4 = "Task "
            r3.append(r4)
            java.lang.String r1 = r12.b()
            r3.append(r1)
            java.lang.String r1 = " execution delayed until after init"
            r3.append(r1)
            java.lang.String r1 = r3.toString()
            r0.c(r2, r1)
        L_0x0113:
            return
        L_0x0114:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            java.lang.String r4 = "Invalid delay specified: "
            r1.append(r4)
            r1.append(r14)
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x012b:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "No task specified"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.o.a(com.applovin.impl.sdk.e.a, com.applovin.impl.sdk.e.o$a, long, boolean):void");
    }

    public boolean a() {
        return this.y;
    }

    public ScheduledExecutorService b() {
        return this.u;
    }

    public void c() {
        synchronized (this.x) {
            this.y = false;
        }
    }

    public void d() {
        synchronized (this.x) {
            this.y = true;
            for (c next : this.w) {
                a(next.b, next.c);
            }
            this.w.clear();
        }
    }
}
