package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.a.c;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.l;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.HashMap;
import java.util.Map;

public class k extends j {
    private final c h;

    public k(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super(d.a("adtoken_zone", lVar), appLovinAdLoadListener, "TaskFetchTokenAd", lVar);
        this.h = cVar;
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> e() {
        HashMap hashMap = new HashMap(2);
        hashMap.put("adtoken", this.h.a());
        hashMap.put("adtoken_prefix", this.h.c());
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b f() {
        return b.REGULAR_AD_TOKEN;
    }
}
