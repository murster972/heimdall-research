package com.applovin.impl.sdk.e;

import android.net.Uri;
import android.webkit.URLUtil;
import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.e;
import com.applovin.impl.a.k;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.d.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.Collections;
import java.util.List;

class e extends c {
    private final a l;

    public e(a aVar, l lVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super("TaskCacheVastAd", aVar, lVar, appLovinAdLoadListener);
        this.l = aVar;
    }

    private void j() {
        String str;
        String str2;
        String str3;
        if (!f()) {
            if (this.l.E0()) {
                b N0 = this.l.N0();
                if (N0 != null) {
                    com.applovin.impl.a.e b = N0.b();
                    if (b != null) {
                        Uri b2 = b.b();
                        String uri = b2 != null ? b2.toString() : "";
                        String c = b.c();
                        if (URLUtil.isValidUrl(uri) || o.b(c)) {
                            if (b.a() == e.a.STATIC) {
                                a("Caching static companion ad at " + uri + "...");
                                Uri b3 = b(uri, Collections.emptyList(), false);
                                if (b3 != null) {
                                    b.a(b3);
                                } else {
                                    str2 = "Failed to cache static companion ad";
                                }
                            } else if (b.a() == e.a.HTML) {
                                if (o.b(uri)) {
                                    a("Begin caching HTML companion ad. Fetching from " + uri + "...");
                                    c = f(uri);
                                    if (o.b(c)) {
                                        str3 = "HTML fetched. Caching HTML now...";
                                    } else {
                                        str2 = "Unable to load companion ad resources from " + uri;
                                    }
                                } else {
                                    str3 = "Caching provided HTML for companion ad. No fetch required. HTML: " + c;
                                }
                                a(str3);
                                b.a(a(c, (List<String>) Collections.emptyList(), (g) this.l));
                            } else if (b.a() == e.a.IFRAME) {
                                str = "Skip caching of iFrame resource...";
                            } else {
                                return;
                            }
                            this.l.a(true);
                            return;
                        }
                        c("Companion ad does not have any resources attached. Skipping...");
                        return;
                    }
                    str2 = "Failed to retrieve non-video resources from companion ad. Skipping...";
                    d(str2);
                    return;
                }
                str = "No companion ad provided. Skipping...";
            } else {
                str = "Companion ad caching disabled. Skipping...";
            }
            a(str);
        }
    }

    private void k() {
        k M0;
        Uri b;
        if (!f()) {
            if (!this.l.F0()) {
                a("Video caching disabled. Skipping...");
            } else if (this.l.L0() != null && (M0 = this.l.M0()) != null && (b = M0.b()) != null) {
                Uri a2 = a(b.toString(), (List<String>) Collections.emptyList(), false);
                if (a2 != null) {
                    a("Video file successfully cached into: " + a2);
                    M0.a(a2);
                    return;
                }
                d("Failed to cache video file: " + M0);
            }
        }
    }

    private void l() {
        String str;
        String str2;
        if (!f()) {
            if (this.l.D0() != null) {
                a("Begin caching HTML template. Fetching from " + this.l.D0() + "...");
                str = a(this.l.D0().toString(), this.l.h());
            } else {
                str = this.l.C0();
            }
            if (o.b(str)) {
                a aVar = this.l;
                aVar.a(a(str, aVar.h(), (g) this.l));
                str2 = "Finish caching HTML template " + this.l.C0() + " for ad #" + this.l.getAdIdNumber();
            } else {
                str2 = "Unable to load HTML template";
            }
            a(str2);
        }
    }

    public void run() {
        super.run();
        if (this.l.l0()) {
            a("Begin caching for VAST streaming ad #" + this.f.getAdIdNumber() + "...");
            g();
            if (this.l.I0()) {
                i();
            }
            if (this.l.H0() == a.b.COMPANION_AD) {
                j();
                l();
            } else {
                k();
            }
            if (!this.l.I0()) {
                i();
            }
            if (this.l.H0() == a.b.COMPANION_AD) {
                k();
            } else {
                j();
                l();
            }
        } else {
            a("Begin caching for VAST ad #" + this.f.getAdIdNumber() + "...");
            g();
            j();
            k();
            l();
            i();
        }
        a("Finished caching VAST ad #" + this.l.getAdIdNumber());
        long currentTimeMillis = System.currentTimeMillis() - this.l.getCreatedAtMillis();
        d.a(this.l, this.f1817a);
        d.a(currentTimeMillis, (AppLovinAdBase) this.l, this.f1817a);
        a((AppLovinAdBase) this.l);
        this.l.G0();
        e();
    }
}
