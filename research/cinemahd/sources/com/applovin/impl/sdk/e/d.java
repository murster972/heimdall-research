package com.applovin.impl.sdk.e;

import android.net.Uri;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.a.a;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.sdk.AppLovinAdLoadListener;

public class d extends c {
    private final a l;
    private boolean m;
    private boolean n;

    public d(a aVar, l lVar, AppLovinAdLoadListener appLovinAdLoadListener) {
        super("TaskCacheAppLovinAd", aVar, lVar, appLovinAdLoadListener);
        this.l = aVar;
    }

    private void j() {
        a("Caching HTML resources...");
        this.l.a(a(this.l.B0(), this.l.h(), (g) this.l));
        this.l.a(true);
        a("Finish caching non-video resources for ad #" + this.l.getAdIdNumber());
        r j0 = this.f1817a.j0();
        String b = b();
        j0.a(b, "Ad updated with cachedHTML = " + this.l.B0());
    }

    private void k() {
        Uri e;
        if (!f() && (e = e(this.l.D0())) != null) {
            this.l.C0();
            this.l.d(e);
        }
    }

    public void a(boolean z) {
        this.m = z;
    }

    public void b(boolean z) {
        this.n = z;
    }

    public void run() {
        super.run();
        boolean l0 = this.l.l0();
        boolean z = this.n;
        if (l0 || z) {
            a("Begin caching for streaming ad #" + this.l.getAdIdNumber() + "...");
            g();
            if (l0) {
                if (this.m) {
                    i();
                }
                j();
                if (!this.m) {
                    i();
                }
                k();
            } else {
                i();
                j();
            }
        } else {
            a("Begin processing for non-streaming ad #" + this.l.getAdIdNumber() + "...");
            g();
            j();
            k();
            i();
        }
        long currentTimeMillis = System.currentTimeMillis() - this.l.getCreatedAtMillis();
        com.applovin.impl.sdk.d.d.a(this.l, this.f1817a);
        com.applovin.impl.sdk.d.d.a(currentTimeMillis, (AppLovinAdBase) this.l, this.f1817a);
        a((AppLovinAdBase) this.l);
        e();
    }
}
