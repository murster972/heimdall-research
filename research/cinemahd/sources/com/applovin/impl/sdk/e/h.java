package com.applovin.impl.sdk.e;

import android.os.Build;
import com.applovin.impl.mediation.c.c;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.m;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.q;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinWebViewActivity;
import com.google.android.gms.security.ProviderInstaller;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONException;
import org.json.JSONObject;

public class h extends a {
    private static final AtomicBoolean h = new AtomicBoolean();
    private final int f;
    /* access modifiers changed from: private */
    public a g;

    public interface a {
        void a(JSONObject jSONObject);
    }

    private class b extends a {
        public b(l lVar) {
            super("TaskTimeoutFetchBasicSettings", lVar, true);
        }

        public void run() {
            if (h.this.g != null) {
                d("Timing out fetch basic settings...");
                h.this.a(new JSONObject());
            }
        }
    }

    public h(int i, l lVar, a aVar) {
        super("TaskFetchBasicSettings", lVar, true);
        this.f = i;
        this.g = aVar;
    }

    /* access modifiers changed from: private */
    public void a(JSONObject jSONObject) {
        a aVar = this.g;
        if (aVar != null) {
            aVar.a(jSONObject);
            this.g = null;
        }
    }

    private String g() {
        return com.applovin.impl.sdk.utils.h.a((String) this.f1817a.a(com.applovin.impl.sdk.c.b.U), "5.0/i", a());
    }

    private String h() {
        return com.applovin.impl.sdk.utils.h.a((String) this.f1817a.a(com.applovin.impl.sdk.c.b.V), "5.0/i", a());
    }

    /* access modifiers changed from: protected */
    public Map<String, String> e() {
        HashMap hashMap = new HashMap();
        hashMap.put("rid", UUID.randomUUID().toString());
        if (!((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.u3)).booleanValue()) {
            hashMap.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1817a.h0());
        }
        Boolean a2 = i.b().a(c());
        if (a2 != null) {
            hashMap.put("huc", a2.toString());
        }
        Boolean a3 = i.a().a(c());
        if (a3 != null) {
            hashMap.put("aru", a3.toString());
        }
        Boolean a4 = i.c().a(c());
        if (a4 != null) {
            hashMap.put("dns", a4.toString());
        }
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public JSONObject f() {
        JSONObject jSONObject = new JSONObject();
        try {
            jSONObject.put("sdk_version", AppLovinSdk.VERSION);
            jSONObject.put("build", 131);
            jSONObject.put("is_cross_promo", this.f1817a.P());
            jSONObject.put("init_count", this.f);
            jSONObject.put("server_installed_at", this.f1817a.a(com.applovin.impl.sdk.c.b.n));
            if (this.f1817a.l()) {
                jSONObject.put("first_install", true);
            }
            if (!this.f1817a.m()) {
                jSONObject.put("first_install_v2", true);
            }
            String str = (String) this.f1817a.a(com.applovin.impl.sdk.c.b.D2);
            if (o.b(str)) {
                jSONObject.put("plugin_version", str);
            }
            String b0 = this.f1817a.b0();
            if (o.b(b0)) {
                jSONObject.put("mediation_provider", b0);
            }
            jSONObject.put("installed_mediation_adapters", c.a(this.f1817a));
            Map<String, Object> g2 = this.f1817a.r().g();
            jSONObject.put("package_name", g2.get("package_name"));
            jSONObject.put("app_version", g2.get("app_version"));
            jSONObject.put("test_ads", g2.get("test_ads"));
            jSONObject.put("debug", g2.get("debug"));
            jSONObject.put("target_sdk", g2.get("target_sdk"));
            if (this.f1817a.Y().getInitializationAdUnitIds().size() > 0) {
                List<String> a2 = e.a(this.f1817a.Y().getInitializationAdUnitIds());
                jSONObject.put("ad_unit_ids", e.a((Collection<String>) a2, a2.size()));
            }
            jSONObject.put("platform", "android");
            jSONObject.put("os", Build.VERSION.RELEASE);
            jSONObject.put("tg", q.a(this.f1817a));
            jSONObject.put("locale", Locale.getDefault().toString());
            m.a j = this.f1817a.r().j();
            jSONObject.put("dnt", j.f1858a);
            if (o.b(j.b)) {
                jSONObject.put("idfa", j.b);
            }
            String name = this.f1817a.Z().getName();
            if (o.b(name)) {
                jSONObject.put("user_segment_name", o.f(name));
            }
            if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.y2)).booleanValue()) {
                jSONObject.put("compass_random_token", this.f1817a.W());
            }
            if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.A2)).booleanValue()) {
                jSONObject.put("applovin_random_token", this.f1817a.X());
            }
        } catch (JSONException e) {
            a("Failed to construct JSON body", e);
        }
        return jSONObject;
    }

    public void run() {
        if (h.compareAndSet(false, true)) {
            try {
                ProviderInstaller.a(this.f1817a.i());
            } catch (Throwable th) {
                a("Cannot update security provider", th);
            }
        }
        Map<String, String> e = e();
        b.a<T> b2 = com.applovin.impl.sdk.network.b.a(this.f1817a).a(g()).c(h()).a(e).a(f()).d(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.A3)).booleanValue()).b("POST").a(new JSONObject()).a(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.j2)).intValue()).c(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.m2)).intValue()).b(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.i2)).intValue());
        b2.e(true);
        com.applovin.impl.sdk.network.b<T> a2 = b2.a();
        this.f1817a.o().a(new b(this.f1817a), o.a.TIMEOUT, ((long) ((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.i2)).intValue()) + 250);
        AnonymousClass1 r1 = new t<JSONObject>(a2, this.f1817a, d()) {
            public void a(int i) {
                d("Unable to fetch basic SDK settings: server returned " + i);
                h.this.a(new JSONObject());
            }

            public void a(JSONObject jSONObject, int i) {
                h.this.a(jSONObject);
            }
        };
        r1.a(com.applovin.impl.sdk.c.b.U);
        r1.b(com.applovin.impl.sdk.c.b.V);
        this.f1817a.o().a((a) r1);
    }
}
