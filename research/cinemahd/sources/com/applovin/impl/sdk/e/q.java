package com.applovin.impl.sdk.e;

import com.applovin.impl.a.d;
import com.applovin.impl.a.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import com.applovin.impl.sdk.utils.u;
import com.applovin.sdk.AppLovinAdLoadListener;
import org.json.JSONObject;

abstract class q extends a {
    private final AppLovinAdLoadListener f;
    private final a g;

    private static final class a extends com.applovin.impl.a.c {
        a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.a.b bVar, l lVar) {
            super(jSONObject, jSONObject2, bVar, lVar);
        }

        /* access modifiers changed from: package-private */
        public void a(t tVar) {
            if (tVar != null) {
                this.f1439a.add(tVar);
                return;
            }
            throw new IllegalArgumentException("No aggregated vast response specified");
        }
    }

    private static final class b extends q {
        private final JSONObject h;

        b(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
            super(cVar, appLovinAdLoadListener, lVar);
            if (appLovinAdLoadListener != null) {
                this.h = cVar.c();
                return;
            }
            throw new IllegalArgumentException("No callback specified.");
        }

        public void run() {
            d dVar;
            a("Processing SDK JSON response...");
            String b = j.b(this.h, "xml", (String) null, this.f1817a);
            if (!o.b(b)) {
                d("No VAST response received.");
                dVar = d.NO_WRAPPER_RESPONSE;
            } else if (b.length() < ((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.l3)).intValue()) {
                try {
                    a(u.a(b, this.f1817a));
                    return;
                } catch (Throwable th) {
                    a("Unable to parse VAST response", th);
                }
            } else {
                d("VAST response is over max length");
                dVar = d.XML_PARSING;
            }
            a(dVar);
        }
    }

    private static final class c extends q {
        private final t h;

        c(t tVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
            super(cVar, appLovinAdLoadListener, lVar);
            if (tVar == null) {
                throw new IllegalArgumentException("No response specified.");
            } else if (cVar == null) {
                throw new IllegalArgumentException("No context specified.");
            } else if (appLovinAdLoadListener != null) {
                this.h = tVar;
            } else {
                throw new IllegalArgumentException("No callback specified.");
            }
        }

        public void run() {
            a("Processing VAST Wrapper response...");
            a(this.h);
        }
    }

    q(com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super("TaskProcessVastResponse", lVar);
        if (cVar != null) {
            this.f = appLovinAdLoadListener;
            this.g = (a) cVar;
            return;
        }
        throw new IllegalArgumentException("No context specified.");
    }

    public static q a(t tVar, com.applovin.impl.a.c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        return new c(tVar, cVar, appLovinAdLoadListener, lVar);
    }

    public static q a(JSONObject jSONObject, JSONObject jSONObject2, com.applovin.impl.sdk.a.b bVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        return new b(new a(jSONObject, jSONObject2, bVar, lVar), appLovinAdLoadListener, lVar);
    }

    /* access modifiers changed from: package-private */
    public void a(d dVar) {
        d("Failed to process VAST response due to VAST error code " + dVar);
        i.a((com.applovin.impl.a.c) this.g, this.f, dVar, -6, this.f1817a);
    }

    /* access modifiers changed from: package-private */
    public void a(t tVar) {
        d dVar;
        a aVar;
        int a2 = this.g.a();
        a("Finished parsing XML at depth " + a2);
        this.g.a(tVar);
        if (i.a(tVar)) {
            int intValue = ((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.m3)).intValue();
            if (a2 < intValue) {
                a("VAST response is wrapper. Resolving...");
                aVar = new w(this.g, this.f, this.f1817a);
            } else {
                d("Reached beyond max wrapper depth of " + intValue);
                dVar = d.WRAPPER_LIMIT_REACHED;
                a(dVar);
                return;
            }
        } else if (i.b(tVar)) {
            a("VAST response is inline. Rendering ad...");
            aVar = new s(this.g, this.f, this.f1817a);
        } else {
            d("VAST response is an error");
            dVar = d.NO_WRAPPER_RESPONSE;
            a(dVar);
            return;
        }
        this.f1817a.o().a(aVar);
    }
}
