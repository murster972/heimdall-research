package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.a.f;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.d.g;
import com.applovin.impl.sdk.d.h;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.y;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class j extends a {
    private final d f;
    private final AppLovinAdLoadListener g;

    public j(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        this(dVar, appLovinAdLoadListener, "TaskFetchNextAd", lVar);
    }

    j(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, String str, l lVar) {
        super(str, lVar);
        this.f = dVar;
        this.g = appLovinAdLoadListener;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        boolean z = i != 204;
        r j0 = a().j0();
        String b = b();
        Boolean valueOf = Boolean.valueOf(z);
        j0.a(b, valueOf, "Unable to fetch " + this.f + " ad: server returned " + i);
        if (i == -800) {
            this.f1817a.p().a(g.k);
        }
        this.f1817a.y().a(this.f, j(), i);
        this.g.failedToReceiveAd(i);
    }

    private void a(h hVar) {
        long b = hVar.b(g.f);
        long currentTimeMillis = System.currentTimeMillis();
        if (currentTimeMillis - b > TimeUnit.MINUTES.toMillis((long) ((Integer) this.f1817a.a(b.q2)).intValue())) {
            hVar.b(g.f, currentTimeMillis);
            hVar.c(g.g);
        }
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        com.applovin.impl.sdk.utils.h.b(jSONObject, this.f1817a);
        com.applovin.impl.sdk.utils.h.a(jSONObject, this.f1817a);
        com.applovin.impl.sdk.utils.h.e(jSONObject, this.f1817a);
        com.applovin.impl.sdk.utils.h.c(jSONObject, this.f1817a);
        d.a(jSONObject, this.f1817a);
        this.f1817a.o().a(a(jSONObject));
    }

    private Map<String, String> i() {
        HashMap hashMap = new HashMap(3);
        hashMap.put("AppLovin-Zone-Id", this.f.a());
        if (this.f.c() != null) {
            hashMap.put("AppLovin-Ad-Size", this.f.c().getLabel());
        }
        if (this.f.d() != null) {
            hashMap.put("AppLovin-Ad-Type", this.f.d().getLabel());
        }
        return hashMap;
    }

    private boolean j() {
        return (this instanceof k) || (this instanceof i);
    }

    /* access modifiers changed from: protected */
    public a a(JSONObject jSONObject) {
        f.a aVar = new f.a(this.f, this.g, this.f1817a);
        aVar.a(j());
        return new p(jSONObject, this.f, f(), aVar, this.f1817a);
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> e() {
        HashMap hashMap = new HashMap(4);
        hashMap.put("zone_id", this.f.a());
        if (this.f.c() != null) {
            hashMap.put("size", this.f.c().getLabel());
        }
        if (this.f.d() != null) {
            hashMap.put("require", this.f.d().getLabel());
        }
        hashMap.put("n", String.valueOf(this.f1817a.E().a(this.f.a())));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public com.applovin.impl.sdk.a.b f() {
        return this.f.e() ? com.applovin.impl.sdk.a.b.APPLOVIN_PRIMARY_ZONE : com.applovin.impl.sdk.a.b.APPLOVIN_CUSTOM_ZONE;
    }

    /* access modifiers changed from: protected */
    public String g() {
        return com.applovin.impl.sdk.utils.h.c(this.f1817a);
    }

    /* access modifiers changed from: protected */
    public String h() {
        return com.applovin.impl.sdk.utils.h.d(this.f1817a);
    }

    public void run() {
        JSONObject jSONObject;
        Map<String, String> map;
        String str;
        a("Fetching next ad of zone: " + this.f);
        if (((Boolean) this.f1817a.a(b.J2)).booleanValue() && com.applovin.impl.sdk.utils.r.d()) {
            a("User is connected to a VPN");
        }
        h p = this.f1817a.p();
        p.a(g.d);
        if (p.b(g.f) == 0) {
            p.b(g.f, System.currentTimeMillis());
        }
        try {
            if (((Boolean) this.f1817a.a(b.p2)).booleanValue()) {
                str = "POST";
                jSONObject = new JSONObject(this.f1817a.r().a(e(), false, true));
                map = new HashMap<>();
                map.put("rid", UUID.randomUUID().toString());
                if (!((Boolean) this.f1817a.a(b.u3)).booleanValue()) {
                    map.put(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, this.f1817a.h0());
                }
            } else {
                jSONObject = null;
                str = "GET";
                map = com.applovin.impl.sdk.utils.r.b(this.f1817a.r().a(e(), false, false));
            }
            HashMap hashMap = new HashMap();
            if (((Boolean) this.f1817a.a(b.R2)).booleanValue()) {
                hashMap.putAll(y.a(((Long) this.f1817a.a(b.S2)).longValue(), this.f1817a));
            }
            hashMap.putAll(i());
            a(p);
            b.a<T> a2 = com.applovin.impl.sdk.network.b.a(this.f1817a).a(g()).a(map).c(h()).b(str).b((Map<String, String>) hashMap).a(new JSONObject()).a(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.d2)).intValue());
            a2.a(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.e2)).booleanValue());
            a2.b(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.f2)).booleanValue());
            b.a<T> b = a2.b(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.c2)).intValue());
            b.e(true);
            if (jSONObject != null) {
                b.a(jSONObject);
                b.d(((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.C3)).booleanValue());
            }
            AnonymousClass1 r1 = new t<JSONObject>(b.a(), this.f1817a) {
                public void a(int i) {
                    j.this.a(i);
                }

                public void a(JSONObject jSONObject, int i) {
                    if (i == 200) {
                        com.applovin.impl.sdk.utils.j.b(jSONObject, "ad_fetch_latency_millis", this.k.a(), this.f1817a);
                        com.applovin.impl.sdk.utils.j.b(jSONObject, "ad_fetch_response_size", this.k.b(), this.f1817a);
                        j.this.b(jSONObject);
                        return;
                    }
                    j.this.a(i);
                }
            };
            r1.a(com.applovin.impl.sdk.c.b.W);
            r1.b(com.applovin.impl.sdk.c.b.X);
            this.f1817a.o().a((a) r1);
        } catch (Throwable th) {
            a("Unable to fetch ad " + this.f, th);
            a(0);
        }
    }
}
