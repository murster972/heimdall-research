package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.e;
import com.applovin.sdk.AppLovinAdLoadListener;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class i extends j {
    private final List<String> h;

    public i(List<String> list, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super(d.a(a(list), lVar), appLovinAdLoadListener, "TaskFetchMultizoneAd", lVar);
        this.h = Collections.unmodifiableList(list);
    }

    private static String a(List<String> list) {
        if (list != null && !list.isEmpty()) {
            return list.get(0);
        }
        throw new IllegalArgumentException("No zone identifiers specified");
    }

    /* access modifiers changed from: package-private */
    public Map<String, String> e() {
        HashMap hashMap = new HashMap(1);
        List<String> list = this.h;
        hashMap.put("zone_ids", e.a((Collection<String>) list, list.size()));
        return hashMap;
    }

    /* access modifiers changed from: protected */
    public b f() {
        return b.APPLOVIN_MULTIZONE;
    }
}
