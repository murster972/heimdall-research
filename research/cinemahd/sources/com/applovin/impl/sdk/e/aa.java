package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.b.c;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.a;
import org.json.JSONObject;

public abstract class aa extends x {
    protected aa(String str, l lVar) {
        super(str, lVar);
    }

    /* access modifiers changed from: private */
    public void b(JSONObject jSONObject) {
        c c = c(jSONObject);
        if (c != null) {
            a(c);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:?, code lost:
        r3 = java.util.Collections.emptyMap();
     */
    /* JADX WARNING: Exception block dominator not found, dom blocks: [] */
    /* JADX WARNING: Missing exception handler attribute for start block: B:5:0x0020 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private com.applovin.impl.sdk.b.c c(org.json.JSONObject r3) {
        /*
            r2 = this;
            org.json.JSONObject r0 = com.applovin.impl.sdk.utils.h.a((org.json.JSONObject) r3)     // Catch:{ JSONException -> 0x0032 }
            com.applovin.impl.sdk.l r1 = r2.f1817a     // Catch:{ JSONException -> 0x0032 }
            com.applovin.impl.sdk.utils.h.b((org.json.JSONObject) r0, (com.applovin.impl.sdk.l) r1)     // Catch:{ JSONException -> 0x0032 }
            com.applovin.impl.sdk.l r1 = r2.f1817a     // Catch:{ JSONException -> 0x0032 }
            com.applovin.impl.sdk.utils.h.a((org.json.JSONObject) r3, (com.applovin.impl.sdk.l) r1)     // Catch:{ JSONException -> 0x0032 }
            com.applovin.impl.sdk.l r1 = r2.f1817a     // Catch:{ JSONException -> 0x0032 }
            com.applovin.impl.sdk.utils.h.c(r3, r1)     // Catch:{ JSONException -> 0x0032 }
            java.lang.String r3 = "params"
            java.lang.Object r3 = r0.get(r3)     // Catch:{ all -> 0x0020 }
            org.json.JSONObject r3 = (org.json.JSONObject) r3     // Catch:{ all -> 0x0020 }
            java.util.Map r3 = com.applovin.impl.sdk.utils.j.a((org.json.JSONObject) r3)     // Catch:{ all -> 0x0020 }
            goto L_0x0024
        L_0x0020:
            java.util.Map r3 = java.util.Collections.emptyMap()     // Catch:{ JSONException -> 0x0032 }
        L_0x0024:
            java.lang.String r1 = "result"
            java.lang.String r0 = r0.getString(r1)     // Catch:{ all -> 0x002b }
            goto L_0x002d
        L_0x002b:
            java.lang.String r0 = "network_timeout"
        L_0x002d:
            com.applovin.impl.sdk.b.c r3 = com.applovin.impl.sdk.b.c.a(r0, r3)     // Catch:{ JSONException -> 0x0032 }
            return r3
        L_0x0032:
            r3 = move-exception
            java.lang.String r0 = "Unable to parse API response"
            r2.a(r0, r3)
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.e.aa.c(org.json.JSONObject):com.applovin.impl.sdk.b.c");
    }

    /* access modifiers changed from: protected */
    public abstract void a(c cVar);

    /* access modifiers changed from: protected */
    public int f() {
        return ((Integer) this.f1817a.a(b.w0)).intValue();
    }

    /* access modifiers changed from: protected */
    public abstract boolean h();

    public void run() {
        a(g(), new a.c<JSONObject>() {
            public void a(int i) {
                if (!aa.this.h()) {
                    aa.this.a(i);
                }
            }

            public void a(JSONObject jSONObject, int i) {
                if (!aa.this.h()) {
                    aa.this.b(jSONObject);
                }
            }
        });
    }
}
