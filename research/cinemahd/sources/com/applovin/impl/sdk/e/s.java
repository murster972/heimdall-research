package com.applovin.impl.sdk.e;

import com.applovin.impl.a.a;
import com.applovin.impl.a.b;
import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.f;
import com.applovin.impl.a.g;
import com.applovin.impl.a.i;
import com.applovin.impl.a.j;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.t;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdType;
import java.util.HashSet;
import java.util.Set;

class s extends a {
    private c f;
    private final AppLovinAdLoadListener g;

    s(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super("TaskRenderVastAd", lVar);
        this.g = appLovinAdLoadListener;
        this.f = cVar;
    }

    public void run() {
        a("Rendering VAST ad...");
        int size = this.f.b().size();
        HashSet hashSet = new HashSet(size);
        HashSet hashSet2 = new HashSet(size);
        String str = "";
        f fVar = null;
        j jVar = null;
        b bVar = null;
        String str2 = str;
        for (t next : this.f.b()) {
            t c = next.c(i.a(next) ? "Wrapper" : "InLine");
            if (c != null) {
                t c2 = c.c("AdSystem");
                if (c2 != null) {
                    fVar = f.a(c2, fVar, this.f1817a);
                }
                str = i.a(c, "AdTitle", str);
                str2 = i.a(c, "Description", str2);
                i.a(c.a("Impression"), (Set<g>) hashSet, this.f, this.f1817a);
                t b = c.b("ViewableImpression");
                if (b != null) {
                    i.a(b.a("Viewable"), (Set<g>) hashSet, this.f, this.f1817a);
                }
                i.a(c.a("Error"), (Set<g>) hashSet2, this.f, this.f1817a);
                t b2 = c.b("Creatives");
                if (b2 != null) {
                    for (t next2 : b2.d()) {
                        t b3 = next2.b("Linear");
                        if (b3 != null) {
                            jVar = j.a(b3, jVar, this.f, this.f1817a);
                        } else {
                            t c3 = next2.c("CompanionAds");
                            if (c3 != null) {
                                t c4 = c3.c("Companion");
                                if (c4 != null) {
                                    bVar = b.a(c4, bVar, this.f, this.f1817a);
                                }
                            } else {
                                d("Received and will skip rendering for an unidentified creative: " + next2);
                            }
                        }
                    }
                }
            } else {
                d("Did not find wrapper or inline response for node: " + next);
            }
        }
        a a2 = a.O0().a(this.f1817a).a(this.f.c()).b(this.f.d()).a(this.f.e()).a(this.f.f()).a(str).b(str2).a(fVar).a(jVar).a(bVar).a((Set<g>) hashSet).b((Set<g>) hashSet2).a();
        d a3 = i.a(a2);
        if (a3 == null) {
            e eVar = new e(a2, this.f1817a, this.g);
            o.a aVar = o.a.CACHING_OTHER;
            if (((Boolean) this.f1817a.a(com.applovin.impl.sdk.c.b.o0)).booleanValue()) {
                if (a2.getType() == AppLovinAdType.REGULAR) {
                    aVar = o.a.CACHING_INTERSTITIAL;
                } else if (a2.getType() == AppLovinAdType.INCENTIVIZED) {
                    aVar = o.a.CACHING_INCENTIVIZED;
                }
            }
            this.f1817a.o().a((a) eVar, aVar);
            return;
        }
        i.a(this.f, this.g, a3, -6, this.f1817a);
    }
}
