package com.applovin.impl.sdk.e;

import com.applovin.impl.sdk.a.b;
import com.applovin.impl.sdk.a.d;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.original.tase.helper.js.JJDecoder;
import org.json.JSONArray;
import org.json.JSONObject;

public class p extends a implements AppLovinAdLoadListener {
    private final JSONObject f;
    private final d g;
    private final b h;
    private final AppLovinAdLoadListener i;

    public p(JSONObject jSONObject, d dVar, b bVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super("TaskProcessAdResponse", lVar);
        if (jSONObject == null) {
            throw new IllegalArgumentException("No response specified");
        } else if (dVar != null) {
            this.f = jSONObject;
            this.g = dVar;
            this.h = bVar;
            this.i = appLovinAdLoadListener;
        } else {
            throw new IllegalArgumentException("No zone specified");
        }
    }

    private void a(int i2) {
        AppLovinAdLoadListener appLovinAdLoadListener = this.i;
        if (appLovinAdLoadListener != null) {
            appLovinAdLoadListener.failedToReceiveAd(i2);
        }
    }

    private void a(JSONObject jSONObject) {
        String b = j.b(jSONObject, "type", JJDecoder.UNDEFINED, this.f1817a);
        if ("applovin".equalsIgnoreCase(b)) {
            a("Starting task for AppLovin ad...");
            this.f1817a.o().a((a) new r(jSONObject, this.f, this.h, this, this.f1817a));
        } else if ("vast".equalsIgnoreCase(b)) {
            a("Starting task for VAST ad...");
            this.f1817a.o().a((a) q.a(jSONObject, this.f, this.h, this, this.f1817a));
        } else {
            c("Unable to process ad of unknown type: " + b);
            failedToReceiveAd(AppLovinErrorCodes.INVALID_RESPONSE);
        }
    }

    public void adReceived(AppLovinAd appLovinAd) {
        AppLovinAdLoadListener appLovinAdLoadListener = this.i;
        if (appLovinAdLoadListener != null) {
            appLovinAdLoadListener.adReceived(appLovinAd);
        }
    }

    public void failedToReceiveAd(int i2) {
        a(i2);
    }

    public void run() {
        JSONArray b = j.b(this.f, "ads", new JSONArray(), this.f1817a);
        if (b.length() > 0) {
            a("Processing ad...");
            a(j.a(b, 0, new JSONObject(), this.f1817a));
            return;
        }
        c("No ads were returned from the server");
        r.a(this.g.a(), this.g.b(), this.f, this.f1817a);
        a(204);
    }
}
