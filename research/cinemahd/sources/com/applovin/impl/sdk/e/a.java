package com.applovin.impl.sdk.e;

import android.content.Context;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;

public abstract class a implements Runnable {
    /* access modifiers changed from: protected */

    /* renamed from: a  reason: collision with root package name */
    public final l f1817a;
    private final String b;
    private final r c;
    private final Context d;
    private final boolean e;

    public a(String str, l lVar) {
        this(str, lVar, false);
    }

    public a(String str, l lVar, boolean z) {
        this.b = str;
        this.f1817a = lVar;
        this.c = lVar.j0();
        this.d = lVar.i();
        this.e = z;
    }

    /* access modifiers changed from: protected */
    public l a() {
        return this.f1817a;
    }

    /* access modifiers changed from: protected */
    public void a(String str) {
        this.c.b(this.b, str);
    }

    /* access modifiers changed from: protected */
    public void a(String str, Throwable th) {
        this.c.b(this.b, str, th);
    }

    public String b() {
        return this.b;
    }

    /* access modifiers changed from: protected */
    public void b(String str) {
        this.c.c(this.b, str);
    }

    /* access modifiers changed from: protected */
    public Context c() {
        return this.d;
    }

    /* access modifiers changed from: protected */
    public void c(String str) {
        this.c.d(this.b, str);
    }

    /* access modifiers changed from: protected */
    public void d(String str) {
        this.c.e(this.b, str);
    }

    public boolean d() {
        return this.e;
    }
}
