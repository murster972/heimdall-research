package com.applovin.impl.sdk.e;

import com.applovin.impl.a.c;
import com.applovin.impl.a.d;
import com.applovin.impl.a.i;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.network.b;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.t;
import com.applovin.sdk.AppLovinAdLoadListener;

class w extends a {
    /* access modifiers changed from: private */
    public final c f;
    /* access modifiers changed from: private */
    public final AppLovinAdLoadListener g;

    w(c cVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
        super("TaskResolveVastWrapper", lVar);
        this.g = appLovinAdLoadListener;
        this.f = cVar;
    }

    /* access modifiers changed from: private */
    public void a(int i) {
        d("Failed to resolve VAST wrapper due to error code " + i);
        if (i == -103) {
            AppLovinAdLoadListener appLovinAdLoadListener = this.g;
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.failedToReceiveAd(i);
                return;
            }
            return;
        }
        i.a(this.f, this.g, i == -102 ? d.TIMED_OUT : d.GENERAL_WRAPPER_ERROR, i, this.f1817a);
    }

    public void run() {
        String a2 = i.a(this.f);
        if (o.b(a2)) {
            a("Resolving VAST ad with depth " + this.f.a() + " at " + a2);
            try {
                this.f1817a.o().a((a) new t<t>(b.a(this.f1817a).a(a2).b("GET").a(t.e).a(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.s3)).intValue()).b(((Integer) this.f1817a.a(com.applovin.impl.sdk.c.b.t3)).intValue()).c(false).a(), this.f1817a) {
                    public void a(int i) {
                        d("Unable to resolve VAST wrapper. Server returned " + i);
                        w.this.a(i);
                    }

                    public void a(t tVar, int i) {
                        this.f1817a.o().a((a) q.a(tVar, w.this.f, w.this.g, w.this.f1817a));
                    }
                });
            } catch (Throwable th) {
                a("Unable to resolve VAST wrapper", th);
            }
        } else {
            d("Resolving VAST failed. Could not find resolution URL");
            a(-1);
        }
    }
}
