package com.applovin.impl.sdk;

import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.PowerManager;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.g;
import com.applovin.impl.sdk.utils.o;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class n {

    /* renamed from: a  reason: collision with root package name */
    private final Map<Integer, Thread> f1860a = new HashMap();
    private final Object b = new Object();
    private final l c;
    /* access modifiers changed from: private */
    public final Lock d = new ReentrantLock();
    private final Runnable e;
    private final Runnable f;
    private Thread g = null;
    /* access modifiers changed from: private */
    public d h = null;

    public n(l lVar) {
        this.c = lVar;
        this.d.lock();
        this.e = new Runnable() {
            public void run() {
                try {
                    n.this.d.lockInterruptibly();
                } catch (InterruptedException unused) {
                    Thread.currentThread().interrupt();
                }
            }
        };
        this.f = new Runnable() {
            public void run() {
                d unused = n.this.h = null;
                n.this.a();
            }
        };
    }

    private Thread a(String str) {
        Thread thread = new Thread(this.e, str);
        thread.setDaemon(true);
        return thread;
    }

    private void b() {
        Context i = this.c.i();
        StringBuilder sb = new StringBuilder("ALDEBUG");
        try {
            PackageInfo packageInfo = i.getPackageManager().getPackageInfo("com.google.android.webview", 0);
            sb.append("-");
            sb.append(packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException unused) {
        }
        ActivityManager activityManager = (ActivityManager) i.getSystemService("activity");
        ActivityManager.MemoryInfo memoryInfo = new ActivityManager.MemoryInfo();
        if (activityManager != null) {
            activityManager.getMemoryInfo(memoryInfo);
            sb.append("-");
            sb.append(memoryInfo.availMem / 1000000);
            sb.append("MB");
            sb.append("/");
            sb.append(memoryInfo.totalMem / 1000000);
            sb.append("MB");
            sb.append("(");
            sb.append(memoryInfo.lowMemory);
            sb.append(")");
        }
        if (g.d()) {
            BatteryManager batteryManager = (BatteryManager) i.getSystemService("batterymanager");
            if (batteryManager != null) {
                sb.append("-");
                sb.append(batteryManager.getIntProperty(4));
                sb.append("%");
            }
            PowerManager powerManager = (PowerManager) i.getSystemService("power");
            if (powerManager != null) {
                sb.append("(");
                sb.append(powerManager.isPowerSaveMode());
                sb.append(")");
            }
        }
        sb.append("-");
        sb.append(this.c.p().b(com.applovin.impl.sdk.d.g.g));
        String sb2 = sb.toString();
        if (this.g == null) {
            r j0 = this.c.j0();
            j0.b("AppLovinSdk", "Creating ANR debug thread with name: " + sb2);
            this.g = a(sb2);
            this.g.start();
            return;
        }
        r j02 = this.c.j0();
        j02.b("AppLovinSdk", "Updating ANR debug thread with name: " + sb2);
        this.g.setName(sb2);
    }

    private String c(Object obj) {
        if (obj instanceof a) {
            a aVar = (a) obj;
            StringBuilder sb = new StringBuilder("MAX-");
            sb.append(aVar.getFormat().getLabel());
            sb.append("-");
            sb.append(aVar.e());
            if (o.b(aVar.getCreativeId())) {
                sb.append("-");
                sb.append(aVar.getCreativeId());
            }
            return sb.toString();
        } else if (!(obj instanceof com.applovin.impl.sdk.a.g)) {
            return null;
        } else {
            com.applovin.impl.sdk.a.g gVar = (com.applovin.impl.sdk.a.g) obj;
            String label = gVar.getAdZone().b() != null ? gVar.getAdZone().b().getLabel() : "NULL";
            StringBuilder sb2 = new StringBuilder("AL-");
            sb2.append(label);
            sb2.append("-");
            sb2.append(gVar.getAdIdNumber());
            if (gVar instanceof com.applovin.impl.a.a) {
                sb2.append("-VAST-");
                sb2.append(((com.applovin.impl.a.a) gVar).K0().a());
            }
            if (o.b(gVar.q0())) {
                sb2.append("-DSP-");
                sb2.append(gVar.q0());
            }
            return sb2.toString();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:4:0x0019, code lost:
        r0 = ((java.lang.Long) r5.c.a(com.applovin.impl.sdk.c.b.a3)).longValue();
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r5 = this;
            com.applovin.impl.sdk.l r0 = r5.c
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.Y2
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0047
            com.applovin.impl.sdk.l r0 = r5.c
            boolean r0 = r0.P()
            if (r0 == 0) goto L_0x0019
            goto L_0x0047
        L_0x0019:
            com.applovin.impl.sdk.l r0 = r5.c
            com.applovin.impl.sdk.c.b<java.lang.Long> r1 = com.applovin.impl.sdk.c.b.a3
            java.lang.Object r0 = r0.a(r1)
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            r2 = 0
            int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
            if (r4 >= 0) goto L_0x002e
            return
        L_0x002e:
            if (r4 <= 0) goto L_0x0042
            com.applovin.impl.sdk.utils.d r2 = r5.h
            if (r2 != 0) goto L_0x0042
            r5.b()
            com.applovin.impl.sdk.l r2 = r5.c
            java.lang.Runnable r3 = r5.f
            com.applovin.impl.sdk.utils.d r0 = com.applovin.impl.sdk.utils.d.a(r0, r2, r3)
            r5.h = r0
            goto L_0x0047
        L_0x0042:
            if (r4 != 0) goto L_0x0047
            r5.b()
        L_0x0047:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.n.a():void");
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x0057, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Object r7) {
        /*
            r6 = this;
            com.applovin.impl.sdk.l r0 = r6.c
            com.applovin.impl.sdk.c.b<java.lang.Boolean> r1 = com.applovin.impl.sdk.c.b.Y2
            java.lang.Object r0 = r0.a(r1)
            java.lang.Boolean r0 = (java.lang.Boolean) r0
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x005b
            if (r7 != 0) goto L_0x0013
            goto L_0x005b
        L_0x0013:
            int r0 = java.lang.System.identityHashCode(r7)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.Object r1 = r6.b
            monitor-enter(r1)
            java.util.Map<java.lang.Integer, java.lang.Thread> r2 = r6.f1860a     // Catch:{ all -> 0x0058 }
            boolean r2 = r2.containsKey(r0)     // Catch:{ all -> 0x0058 }
            if (r2 != 0) goto L_0x0056
            java.lang.String r7 = r6.c(r7)     // Catch:{ all -> 0x0058 }
            if (r7 != 0) goto L_0x002e
            monitor-exit(r1)     // Catch:{ all -> 0x0058 }
            return
        L_0x002e:
            com.applovin.impl.sdk.l r2 = r6.c     // Catch:{ all -> 0x0058 }
            com.applovin.impl.sdk.r r2 = r2.j0()     // Catch:{ all -> 0x0058 }
            java.lang.String r3 = "AppLovinSdk"
            java.lang.StringBuilder r4 = new java.lang.StringBuilder     // Catch:{ all -> 0x0058 }
            r4.<init>()     // Catch:{ all -> 0x0058 }
            java.lang.String r5 = "Creating ad debug thread with name: "
            r4.append(r5)     // Catch:{ all -> 0x0058 }
            r4.append(r7)     // Catch:{ all -> 0x0058 }
            java.lang.String r4 = r4.toString()     // Catch:{ all -> 0x0058 }
            r2.b(r3, r4)     // Catch:{ all -> 0x0058 }
            java.lang.Thread r7 = r6.a((java.lang.String) r7)     // Catch:{ all -> 0x0058 }
            r7.start()     // Catch:{ all -> 0x0058 }
            java.util.Map<java.lang.Integer, java.lang.Thread> r2 = r6.f1860a     // Catch:{ all -> 0x0058 }
            r2.put(r0, r7)     // Catch:{ all -> 0x0058 }
        L_0x0056:
            monitor-exit(r1)     // Catch:{ all -> 0x0058 }
            return
        L_0x0058:
            r7 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0058 }
            throw r7
        L_0x005b:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.n.a(java.lang.Object):void");
    }

    public void b(Object obj) {
        if (((Boolean) this.c.a(b.Y2)).booleanValue() && obj != null) {
            Integer valueOf = Integer.valueOf(System.identityHashCode(obj));
            synchronized (this.b) {
                Thread thread = this.f1860a.get(valueOf);
                if (thread != null) {
                    r j0 = this.c.j0();
                    j0.b("AppLovinSdk", "Destroying ad debug thread with name: " + thread.getName());
                    thread.interrupt();
                    this.f1860a.remove(valueOf);
                }
            }
        }
    }
}
