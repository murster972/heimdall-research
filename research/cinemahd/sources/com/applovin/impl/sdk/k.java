package com.applovin.impl.sdk;

import android.app.Activity;
import android.content.Intent;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.j;
import com.applovin.impl.sdk.utils.a;
import com.applovin.impl.sdk.utils.h;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinPrivacySettings;
import com.applovin.sdk.AppLovinSdkUtils;
import com.applovin.sdk.AppLovinUserService;
import com.applovin.sdk.AppLovinWebViewActivity;
import java.io.Serializable;
import java.lang.ref.WeakReference;
import java.util.concurrent.atomic.AtomicBoolean;

public class k implements j.a, AppLovinWebViewActivity.EventListener {
    /* access modifiers changed from: private */
    public static final AtomicBoolean g = new AtomicBoolean();
    /* access modifiers changed from: private */
    public static WeakReference<AppLovinWebViewActivity> h;
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1843a;
    /* access modifiers changed from: private */
    public final r b;
    /* access modifiers changed from: private */
    public AppLovinUserService.OnConsentDialogDismissListener c;
    /* access modifiers changed from: private */
    public j d;
    /* access modifiers changed from: private */
    public WeakReference<Activity> e = new WeakReference<>((Object) null);
    /* access modifiers changed from: private */
    public a f;

    k(l lVar) {
        this.f1843a = lVar;
        this.b = lVar.j0();
        if (lVar.j() != null) {
            this.e = new WeakReference<>(lVar.j());
        }
        lVar.D().a(new a() {
            public void onActivityStarted(Activity activity) {
                WeakReference unused = k.this.e = new WeakReference(activity);
            }
        });
        this.d = new j(this, lVar);
    }

    private void a(boolean z, long j) {
        f();
        if (z) {
            a(j);
        }
    }

    /* access modifiers changed from: private */
    public boolean a(l lVar) {
        if (c()) {
            r.i("AppLovinSdk", "Consent dialog already showing");
            return false;
        } else if (!h.a(lVar.i())) {
            r.i("AppLovinSdk", "No internet available, skip showing of consent dialog");
            return false;
        } else if (!((Boolean) lVar.a(b.x)).booleanValue()) {
            this.b.e("ConsentDialogManager", "Blocked publisher from showing consent dialog");
            return false;
        } else if (o.b((String) lVar.a(b.y))) {
            return true;
        } else {
            this.b.e("ConsentDialogManager", "AdServer returned empty consent dialog URL");
            return false;
        }
    }

    private void f() {
        this.f1843a.D().b(this.f);
        if (c()) {
            AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) h.get();
            h = null;
            if (appLovinWebViewActivity != null) {
                appLovinWebViewActivity.finish();
                AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener = this.c;
                if (onConsentDialogDismissListener != null) {
                    onConsentDialogDismissListener.onDismiss();
                    this.c = null;
                }
            }
        }
    }

    public void a() {
        if (this.e.get() != null) {
            final Activity activity = (Activity) this.e.get();
            AppLovinSdkUtils.runOnUiThreadDelayed(new Runnable() {
                public void run() {
                    k.this.a(activity, (AppLovinUserService.OnConsentDialogDismissListener) null);
                }
            }, ((Long) this.f1843a.a(b.A)).longValue());
        }
    }

    public void a(final long j) {
        AppLovinSdkUtils.runOnUiThread(new Runnable() {
            public void run() {
                k.this.b.b("ConsentDialogManager", "Scheduling repeating consent alert");
                k.this.d.a(j, k.this.f1843a, k.this);
            }
        });
    }

    public void a(final Activity activity, final AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener) {
        activity.runOnUiThread(new Runnable() {
            public void run() {
                k kVar = k.this;
                if (!kVar.a(kVar.f1843a) || k.g.getAndSet(true)) {
                    AppLovinUserService.OnConsentDialogDismissListener onConsentDialogDismissListener = onConsentDialogDismissListener;
                    if (onConsentDialogDismissListener != null) {
                        onConsentDialogDismissListener.onDismiss();
                        return;
                    }
                    return;
                }
                WeakReference unused = k.this.e = new WeakReference(activity);
                AppLovinUserService.OnConsentDialogDismissListener unused2 = k.this.c = onConsentDialogDismissListener;
                a unused3 = k.this.f = new a() {
                    public void onActivityStarted(Activity activity) {
                        if (activity instanceof AppLovinWebViewActivity) {
                            if (!k.this.c() || k.h.get() != activity) {
                                AppLovinWebViewActivity appLovinWebViewActivity = (AppLovinWebViewActivity) activity;
                                WeakReference unused = k.h = new WeakReference(appLovinWebViewActivity);
                                appLovinWebViewActivity.loadUrl((String) k.this.f1843a.a(b.y), k.this);
                            }
                            k.g.set(false);
                        }
                    }
                };
                k.this.f1843a.D().a(k.this.f);
                Intent intent = new Intent(activity, AppLovinWebViewActivity.class);
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_SDK_KEY, k.this.f1843a.h0());
                intent.putExtra(AppLovinWebViewActivity.INTENT_EXTRA_KEY_IMMERSIVE_MODE_ON, (Serializable) k.this.f1843a.a(b.z));
                activity.startActivity(intent);
            }
        });
    }

    public void b() {
    }

    /* access modifiers changed from: package-private */
    public boolean c() {
        WeakReference<AppLovinWebViewActivity> weakReference = h;
        return (weakReference == null || weakReference.get() == null) ? false : true;
    }

    public void onReceivedEvent(String str) {
        boolean booleanValue;
        l lVar;
        b bVar;
        if ("accepted".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(true, this.f1843a.i());
            f();
            return;
        }
        if ("rejected".equalsIgnoreCase(str)) {
            AppLovinPrivacySettings.setHasUserConsent(false, this.f1843a.i());
            booleanValue = ((Boolean) this.f1843a.a(b.B)).booleanValue();
            lVar = this.f1843a;
            bVar = b.G;
        } else if ("closed".equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.f1843a.a(b.C)).booleanValue();
            lVar = this.f1843a;
            bVar = b.H;
        } else if (AppLovinWebViewActivity.EVENT_DISMISSED_VIA_BACK_BUTTON.equalsIgnoreCase(str)) {
            booleanValue = ((Boolean) this.f1843a.a(b.D)).booleanValue();
            lVar = this.f1843a;
            bVar = b.I;
        } else {
            return;
        }
        a(booleanValue, ((Long) lVar.a(bVar)).longValue());
    }
}
