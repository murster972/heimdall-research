package com.applovin.impl.sdk.b;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.l;
import com.applovin.sdk.AppLovinSdkUtils;

public class b {
    /* access modifiers changed from: private */

    /* renamed from: a  reason: collision with root package name */
    public final l f1779a;
    /* access modifiers changed from: private */
    public final Activity b;
    /* access modifiers changed from: private */
    public AlertDialog c;
    /* access modifiers changed from: private */
    public a d;

    public interface a {
        void a();

        void b();
    }

    public b(Activity activity, l lVar) {
        this.f1779a = lVar;
        this.b = activity;
    }

    public void a() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                if (b.this.c != null) {
                    b.this.c.dismiss();
                }
            }
        });
    }

    public void a(final g gVar, final Runnable runnable) {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(b.this.b);
                builder.setTitle(gVar.V());
                String W = gVar.W();
                if (AppLovinSdkUtils.isValidString(W)) {
                    builder.setMessage(W);
                }
                builder.setPositiveButton(gVar.X(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        runnable.run();
                    }
                });
                builder.setCancelable(false);
                AlertDialog unused = b.this.c = builder.show();
            }
        });
    }

    public void a(a aVar) {
        this.d = aVar;
    }

    public void b() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                b bVar = b.this;
                AlertDialog unused = bVar.c = new AlertDialog.Builder(bVar.b).setTitle((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.z0)).setMessage((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.A0)).setCancelable(false).setPositiveButton((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.C0), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.d.a();
                    }
                }).setNegativeButton((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.B0), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.d.b();
                    }
                }).show();
            }
        });
    }

    public void c() {
        this.b.runOnUiThread(new Runnable() {
            public void run() {
                AlertDialog.Builder builder = new AlertDialog.Builder(b.this.b);
                builder.setTitle((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.E0));
                builder.setMessage((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.F0));
                builder.setCancelable(false);
                builder.setPositiveButton((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.H0), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.d.a();
                    }
                });
                builder.setNegativeButton((CharSequence) b.this.f1779a.a(com.applovin.impl.sdk.c.b.G0), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        b.this.d.b();
                    }
                });
                AlertDialog unused = b.this.c = builder.show();
            }
        });
    }

    public boolean d() {
        AlertDialog alertDialog = this.c;
        if (alertDialog != null) {
            return alertDialog.isShowing();
        }
        return false;
    }
}
