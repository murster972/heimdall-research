package com.applovin.impl.sdk.b;

import android.content.Context;
import com.applovin.adview.AppLovinInterstitialAd;
import com.applovin.adview.AppLovinInterstitialAdDialog;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.AppLovinAdServiceImpl;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.a.h;
import com.applovin.impl.sdk.a.i;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.e.u;
import com.applovin.impl.sdk.e.z;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.k;
import com.applovin.impl.sdk.utils.o;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdClickListener;
import com.applovin.sdk.AppLovinAdDisplayListener;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.applovin.sdk.AppLovinAdRewardListener;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinAdVideoPlaybackListener;
import com.applovin.sdk.AppLovinErrorCodes;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.SoftReference;
import java.util.Map;

public class a {

    /* renamed from: a  reason: collision with root package name */
    protected final l f1773a;
    protected final AppLovinAdServiceImpl b;
    /* access modifiers changed from: private */
    public AppLovinAd c;
    private String d;
    private SoftReference<AppLovinAdLoadListener> e;
    private final Object f = new Object();
    private volatile String g;
    /* access modifiers changed from: private */
    public volatile boolean h = false;

    /* renamed from: com.applovin.impl.sdk.b.a$a  reason: collision with other inner class name */
    private class C0014a implements AppLovinAdLoadListener {
        /* access modifiers changed from: private */

        /* renamed from: a  reason: collision with root package name */
        public final AppLovinAdLoadListener f1775a;

        C0014a(AppLovinAdLoadListener appLovinAdLoadListener) {
            this.f1775a = appLovinAdLoadListener;
        }

        public void adReceived(final AppLovinAd appLovinAd) {
            AppLovinAd unused = a.this.c = appLovinAd;
            if (this.f1775a != null) {
                AppLovinSdkUtils.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            C0014a.this.f1775a.adReceived(appLovinAd);
                        } catch (Throwable th) {
                            r.c("AppLovinIncentivizedInterstitial", "Unable to notify ad listener about a newly loaded ad", th);
                        }
                    }
                });
            }
        }

        public void failedToReceiveAd(final int i) {
            if (this.f1775a != null) {
                AppLovinSdkUtils.runOnUiThread(new Runnable() {
                    public void run() {
                        try {
                            C0014a.this.f1775a.failedToReceiveAd(i);
                        } catch (Throwable th) {
                            r.c("AppLovinIncentivizedInterstitial", "Unable to notify listener about ad load failure", th);
                        }
                    }
                });
            }
        }
    }

    private class b implements i, AppLovinAdClickListener, AppLovinAdRewardListener, AppLovinAdVideoPlaybackListener {

        /* renamed from: a  reason: collision with root package name */
        private final AppLovinAdDisplayListener f1778a;
        private final AppLovinAdClickListener b;
        private final AppLovinAdVideoPlaybackListener c;
        private final AppLovinAdRewardListener d;

        private b(AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
            this.f1778a = appLovinAdDisplayListener;
            this.b = appLovinAdClickListener;
            this.c = appLovinAdVideoPlaybackListener;
            this.d = appLovinAdRewardListener;
        }

        private void a(g gVar) {
            String str;
            int i;
            if (!o.b(a.this.e()) || !a.this.h) {
                gVar.G();
                if (a.this.h) {
                    i = AppLovinErrorCodes.INCENTIVIZED_SERVER_TIMEOUT;
                    str = "network_timeout";
                } else {
                    i = AppLovinErrorCodes.INCENTIVIZED_USER_CLOSED_VIDEO;
                    str = "user_closed_video";
                }
                gVar.a(c.a(str));
                k.a(this.d, (AppLovinAd) gVar, i);
            }
            a.this.a((AppLovinAd) gVar);
            k.b(this.f1778a, (AppLovinAd) gVar);
            if (!gVar.N().getAndSet(true)) {
                a.this.f1773a.o().a((com.applovin.impl.sdk.e.a) new u(gVar, a.this.f1773a), o.a.REWARD);
            }
        }

        public void adClicked(AppLovinAd appLovinAd) {
            k.a(this.b, appLovinAd);
        }

        public void adDisplayed(AppLovinAd appLovinAd) {
            k.a(this.f1778a, appLovinAd);
        }

        public void adHidden(AppLovinAd appLovinAd) {
            if (appLovinAd instanceof h) {
                appLovinAd = ((h) appLovinAd).a();
            }
            if (appLovinAd instanceof g) {
                a((g) appLovinAd);
                return;
            }
            r j0 = a.this.f1773a.j0();
            j0.e("IncentivizedAdController", "Something is terribly wrong. Received `adHidden` callback for invalid ad of type: " + appLovinAd);
        }

        public void onAdDisplayFailed(String str) {
            k.a(this.f1778a, str);
        }

        public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
        }

        public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("quota_exceeded");
            k.b(this.d, appLovinAd, map);
        }

        public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("rejected");
            k.c(this.d, appLovinAd, map);
        }

        public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
            a.this.a("accepted");
            k.a(this.d, appLovinAd, map);
        }

        public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
            a.this.a("network_timeout");
            k.a(this.d, appLovinAd, i);
        }

        public void videoPlaybackBegan(AppLovinAd appLovinAd) {
            k.a(this.c, appLovinAd);
        }

        public void videoPlaybackEnded(AppLovinAd appLovinAd, double d2, boolean z) {
            k.a(this.c, appLovinAd, d2, z);
            boolean unused = a.this.h = z;
        }
    }

    public a(String str, AppLovinSdk appLovinSdk) {
        this.f1773a = appLovinSdk.coreSdk;
        this.b = (AppLovinAdServiceImpl) appLovinSdk.getAdService();
        this.d = str;
    }

    private void a(AppLovinAdBase appLovinAdBase, Context context, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAdBase.getType() == AppLovinAdType.INCENTIVIZED || appLovinAdBase.getType() == AppLovinAdType.AUTO_INCENTIVIZED) {
            AppLovinAd a2 = com.applovin.impl.sdk.utils.r.a((AppLovinAd) appLovinAdBase, this.f1773a);
            if (a2 != null) {
                AppLovinInterstitialAdDialog create = AppLovinInterstitialAd.create(this.f1773a.v(), context);
                b bVar = new b(appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
                create.setAdDisplayListener(bVar);
                create.setAdVideoPlaybackListener(bVar);
                create.setAdClickListener(bVar);
                create.showAndRender(a2);
                if (a2 instanceof g) {
                    a((g) a2, (AppLovinAdRewardListener) bVar);
                    return;
                }
                return;
            }
            a(appLovinAdBase, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener);
            return;
        }
        r j0 = this.f1773a.j0();
        j0.e("IncentivizedAdController", "Failed to render an ad of type " + appLovinAdBase.getType() + " in an Incentivized Ad interstitial.");
        a(appLovinAdBase, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener);
    }

    private void a(g gVar, AppLovinAdRewardListener appLovinAdRewardListener) {
        this.f1773a.o().a((com.applovin.impl.sdk.e.a) new z(gVar, appLovinAdRewardListener, this.f1773a), o.a.REWARD);
    }

    /* access modifiers changed from: private */
    public void a(AppLovinAd appLovinAd) {
        AppLovinAd appLovinAd2 = this.c;
        if (appLovinAd2 != null) {
            if (appLovinAd2 instanceof h) {
                if (appLovinAd != ((h) appLovinAd2).a()) {
                    return;
                }
            } else if (appLovinAd != appLovinAd2) {
                return;
            }
            this.c = null;
        }
    }

    private void a(AppLovinAd appLovinAd, Context context, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAd == null) {
            appLovinAd = this.c;
        }
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) appLovinAd;
        if (appLovinAdBase != null) {
            a(appLovinAdBase, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
            return;
        }
        r.i("IncentivizedAdController", "Skipping incentivized video playback: user attempted to play an incentivized video before one was preloaded.");
        d();
    }

    private void a(AppLovinAd appLovinAd, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener) {
        this.f1773a.p().a(com.applovin.impl.sdk.d.g.m);
        k.a(appLovinAdVideoPlaybackListener, appLovinAd, 0.0d, false);
        k.b(appLovinAdDisplayListener, appLovinAd);
    }

    /* access modifiers changed from: private */
    public void a(String str) {
        synchronized (this.f) {
            this.g = str;
        }
    }

    private void b(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.b.loadNextIncentivizedAd(this.d, appLovinAdLoadListener);
    }

    private void d() {
        AppLovinAdLoadListener appLovinAdLoadListener;
        SoftReference<AppLovinAdLoadListener> softReference = this.e;
        if (softReference != null && (appLovinAdLoadListener = softReference.get()) != null) {
            appLovinAdLoadListener.failedToReceiveAd(AppLovinErrorCodes.INCENTIVIZED_NO_AD_PRELOADED);
        }
    }

    /* access modifiers changed from: private */
    public String e() {
        String str;
        synchronized (this.f) {
            str = this.g;
        }
        return str;
    }

    private AppLovinAdRewardListener f() {
        return new AppLovinAdRewardListener() {
            public void userDeclinedToViewAd(AppLovinAd appLovinAd) {
                a.this.f1773a.j0().b("IncentivizedAdController", "User declined to view");
            }

            public void userOverQuota(AppLovinAd appLovinAd, Map<String, String> map) {
                r j0 = a.this.f1773a.j0();
                j0.b("IncentivizedAdController", "User over quota: " + map);
            }

            public void userRewardRejected(AppLovinAd appLovinAd, Map<String, String> map) {
                r j0 = a.this.f1773a.j0();
                j0.b("IncentivizedAdController", "Reward rejected: " + map);
            }

            public void userRewardVerified(AppLovinAd appLovinAd, Map<String, String> map) {
                r j0 = a.this.f1773a.j0();
                j0.b("IncentivizedAdController", "Reward validated: " + map);
            }

            public void validationRequestFailed(AppLovinAd appLovinAd, int i) {
                r j0 = a.this.f1773a.j0();
                j0.b("IncentivizedAdController", "Reward validation failed: " + i);
            }
        };
    }

    public void a(AppLovinAd appLovinAd, Context context, String str, AppLovinAdRewardListener appLovinAdRewardListener, AppLovinAdVideoPlaybackListener appLovinAdVideoPlaybackListener, AppLovinAdDisplayListener appLovinAdDisplayListener, AppLovinAdClickListener appLovinAdClickListener) {
        if (appLovinAdRewardListener == null) {
            appLovinAdRewardListener = f();
        }
        a(appLovinAd, context, appLovinAdRewardListener, appLovinAdVideoPlaybackListener, appLovinAdDisplayListener, appLovinAdClickListener);
    }

    public void a(AppLovinAdLoadListener appLovinAdLoadListener) {
        this.f1773a.j0().b("IncentivizedAdController", "User requested preload of incentivized ad...");
        this.e = new SoftReference<>(appLovinAdLoadListener);
        if (a()) {
            r.i("IncentivizedAdController", "Attempted to call preloadAndNotify: while an ad was already loaded or currently being played. Do not call preloadAndNotify: again until the last ad has been closed (adHidden).");
            if (appLovinAdLoadListener != null) {
                appLovinAdLoadListener.adReceived(this.c);
                return;
            }
            return;
        }
        b((AppLovinAdLoadListener) new C0014a(appLovinAdLoadListener));
    }

    public boolean a() {
        return this.c != null;
    }

    public String b() {
        return this.d;
    }

    public void c() {
    }
}
