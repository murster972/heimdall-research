package com.applovin.impl.sdk;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.AlphaAnimation;
import android.widget.FrameLayout;
import com.applovin.impl.mediation.a.a;
import com.applovin.impl.sdk.a.g;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.utils.i;
import com.applovin.impl.sdk.utils.l;
import com.applovin.impl.sdk.utils.p;
import com.applovin.impl.sdk.utils.r;
import com.applovin.sdk.AppLovinSdk;
import com.applovin.sdk.AppLovinSdkUtils;
import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;
import org.json.JSONObject;

public class c implements i.a {

    /* renamed from: a  reason: collision with root package name */
    private final l f1790a;
    private Object b;
    private WeakReference<View> c = new WeakReference<>((Object) null);
    private i d;
    private p e;
    /* access modifiers changed from: private */
    public int f;

    public c(l lVar) {
        this.f1790a = lVar;
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v2, resolved type: android.widget.Button} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v4, resolved type: android.widget.ImageButton} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v5, resolved type: android.widget.Button} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v6, resolved type: android.widget.Button} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private android.view.View a(final android.app.Activity r6) {
        /*
            r5 = this;
            r0 = 40
            int r0 = com.applovin.sdk.AppLovinSdkUtils.dpToPx(r6, r0)
            int r1 = r0 / 10
            android.widget.FrameLayout$LayoutParams r2 = new android.widget.FrameLayout$LayoutParams
            r3 = 8388629(0x800015, float:1.1754973E-38)
            r2.<init>(r0, r0, r3)
            r2.setMargins(r1, r1, r1, r1)
            android.widget.ImageButton r0 = new android.widget.ImageButton     // Catch:{ NotFoundException -> 0x0030 }
            r0.<init>(r6)     // Catch:{ NotFoundException -> 0x0030 }
            android.content.res.Resources r3 = r6.getResources()     // Catch:{ NotFoundException -> 0x0030 }
            int r4 = com.applovin.sdk.R$drawable.applovin_ic_white_small     // Catch:{ NotFoundException -> 0x0030 }
            android.graphics.drawable.Drawable r3 = r3.getDrawable(r4)     // Catch:{ NotFoundException -> 0x0030 }
            r0.setImageDrawable(r3)     // Catch:{ NotFoundException -> 0x0030 }
            android.widget.ImageView$ScaleType r3 = android.widget.ImageView.ScaleType.FIT_CENTER     // Catch:{ NotFoundException -> 0x0030 }
            r0.setScaleType(r3)     // Catch:{ NotFoundException -> 0x0030 }
            int r3 = r1 * 2
            r0.setPadding(r1, r1, r1, r3)     // Catch:{ NotFoundException -> 0x0030 }
            goto L_0x004b
        L_0x0030:
            android.widget.Button r0 = new android.widget.Button
            r0.<init>(r6)
            java.lang.String r1 = "ⓘ"
            r0.setText(r1)
            r1 = -1
            r0.setTextColor(r1)
            r1 = 0
            r0.setAllCaps(r1)
            r3 = 1101004800(0x41a00000, float:20.0)
            r4 = 2
            r0.setTextSize(r4, r3)
            r0.setPadding(r1, r1, r1, r1)
        L_0x004b:
            r0.setLayoutParams(r2)
            android.graphics.drawable.Drawable r1 = r5.e()
            r0.setBackground(r1)
            com.applovin.impl.sdk.c$4 r1 = new com.applovin.impl.sdk.c$4
            r1.<init>(r6)
            r0.setOnClickListener(r1)
            boolean r1 = com.applovin.impl.sdk.utils.g.d()
            if (r1 == 0) goto L_0x006c
            r1 = 5
            int r6 = com.applovin.sdk.AppLovinSdkUtils.dpToPx(r6, r1)
            float r6 = (float) r6
            r0.setElevation(r6)
        L_0x006c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.applovin.impl.sdk.c.a(android.app.Activity):android.view.View");
    }

    /* access modifiers changed from: private */
    public void a(Context context) {
        final String f2 = f();
        final WeakReference weakReference = new WeakReference(context);
        new AlertDialog.Builder(context).setTitle("Ad Info").setMessage(f2).setNegativeButton("Close", (DialogInterface.OnClickListener) null).setPositiveButton("Report", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                if (weakReference.get() != null) {
                    c.this.a((Context) weakReference.get(), f2);
                }
            }
        }).show();
    }

    /* access modifiers changed from: private */
    public void a(Context context, String str) {
        l lVar = new l();
        lVar.b("Describe your issue below:\n\n\n").a("Ad Info:").a(str).a("\nDebug Info:\n").a("Platform", "Android").a("AppLovin SDK Version", AppLovinSdk.VERSION).a("Plugin Version", this.f1790a.a(b.D2)).a("Ad Review Version", r.f()).a("App Package Name", context.getPackageName()).a("Device", Build.DEVICE).a("OS Version", Build.VERSION.RELEASE).a("AppLovin Random Token", this.f1790a.X());
        Intent intent = new Intent("android.intent.action.SEND").setType("text/plain").putExtra("android.intent.extra.SUBJECT", "MAX Ad Report").setPackage("com.google.android.gm");
        Object obj = this.b;
        if (obj instanceof g) {
            JSONObject jSONObject = ((g) obj).fullResponse;
            lVar.a("\nAd Response:\n");
            lVar.a(jSONObject.toString());
            intent.putExtra("android.intent.extra.SUBJECT", "AppLovin Ad Report");
        }
        intent.putExtra("android.intent.extra.TEXT", lVar.toString());
        if (intent.resolveActivity(context.getPackageManager()) == null) {
            intent.setPackage((String) null);
        }
        context.startActivity(intent);
    }

    /* access modifiers changed from: private */
    public void d() {
        if (this.f1790a.C().b() && this.c.get() == null) {
            Activity a2 = this.f1790a.D().a();
            View findViewById = a2.findViewById(16908290);
            if (findViewById instanceof FrameLayout) {
                r j0 = this.f1790a.j0();
                j0.b("AppLovinSdk", "Displaying ad info button for ad: " + this.b);
                final FrameLayout frameLayout = (FrameLayout) findViewById;
                final View a3 = a(a2);
                frameLayout.addView(a3);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(150);
                a3.startAnimation(alphaAnimation);
                findViewById.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener(this) {
                    public void onGlobalLayout() {
                        if (a3.getParent() == null) {
                            frameLayout.addView(a3);
                        }
                    }
                });
                this.c = new WeakReference<>(a3);
            }
        }
    }

    private Drawable e() {
        GradientDrawable gradientDrawable = new GradientDrawable();
        gradientDrawable.setShape(1);
        gradientDrawable.setColor(Color.rgb(5, 131, 170));
        GradientDrawable gradientDrawable2 = new GradientDrawable();
        gradientDrawable2.setShape(1);
        gradientDrawable2.setColor(Color.rgb(2, 98, 127));
        StateListDrawable stateListDrawable = new StateListDrawable();
        stateListDrawable.addState(new int[]{16842919}, gradientDrawable2);
        stateListDrawable.addState(new int[0], gradientDrawable);
        return stateListDrawable;
    }

    private String f() {
        l lVar = new l();
        Object obj = this.b;
        if (obj instanceof g) {
            g gVar = (g) obj;
            lVar.a("Network", "APPLOVIN").a(gVar).b(gVar);
        } else if (obj instanceof a) {
            lVar.a((a) obj);
        }
        lVar.a(this.f1790a);
        return lVar.toString();
    }

    public void a() {
        i iVar = this.d;
        if (iVar != null) {
            iVar.b();
        }
        this.b = null;
        this.c = new WeakReference<>((Object) null);
    }

    public void a(Object obj) {
        if (!com.applovin.impl.mediation.c.c.b(obj)) {
            this.b = obj;
            if (((Boolean) this.f1790a.a(b.L0)).booleanValue() && this.f1790a.Y().isAdInfoButtonEnabled()) {
                if (this.d == null) {
                    this.d = new i(this.f1790a, this);
                }
                this.d.a();
            }
        }
    }

    public void b() {
        if (this.f == 0) {
            this.e = p.a(TimeUnit.SECONDS.toMillis(3), this.f1790a, new Runnable() {
                public void run() {
                    int unused = c.this.f = 0;
                }
            });
        }
        int i = this.f;
        if (i % 2 == 0) {
            this.f = i + 1;
        }
    }

    public void c() {
        int i = this.f;
        if (i % 2 == 1) {
            this.f = i + 1;
        }
        if (this.f / 2 == 2) {
            AppLovinSdkUtils.runOnUiThread(new Runnable() {
                public void run() {
                    c.this.d();
                }
            });
            this.f = 0;
            this.e.d();
            this.d.b();
        }
    }
}
