package com.applovin.impl.sdk.a;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAdFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import org.json.JSONArray;
import org.json.JSONObject;

public class e {

    /* renamed from: a  reason: collision with root package name */
    private final l f1762a;
    private final r b;
    private final AtomicBoolean c;
    private final Map<MaxAdFormat, f> d;
    private final Object e = new Object();
    private volatile boolean f;

    public e(l lVar) {
        new LinkedHashSet();
        this.f1762a = lVar;
        this.b = lVar.j0();
        this.c = new AtomicBoolean();
        this.d = new HashMap(5);
        Map<MaxAdFormat, f> map = this.d;
        MaxAdFormat maxAdFormat = MaxAdFormat.BANNER;
        map.put(maxAdFormat, new f(maxAdFormat, lVar));
        Map<MaxAdFormat, f> map2 = this.d;
        MaxAdFormat maxAdFormat2 = MaxAdFormat.LEADER;
        map2.put(maxAdFormat2, new f(maxAdFormat2, lVar));
        Map<MaxAdFormat, f> map3 = this.d;
        MaxAdFormat maxAdFormat3 = MaxAdFormat.MREC;
        map3.put(maxAdFormat3, new f(maxAdFormat3, lVar));
        Map<MaxAdFormat, f> map4 = this.d;
        MaxAdFormat maxAdFormat4 = MaxAdFormat.INTERSTITIAL;
        map4.put(maxAdFormat4, new f(maxAdFormat4, lVar));
        Map<MaxAdFormat, f> map5 = this.d;
        MaxAdFormat maxAdFormat5 = MaxAdFormat.REWARDED;
        map5.put(maxAdFormat5, new f(maxAdFormat5, lVar));
        Map<MaxAdFormat, f> map6 = this.d;
        MaxAdFormat maxAdFormat6 = MaxAdFormat.REWARDED_INTERSTITIAL;
        map6.put(maxAdFormat6, new f(maxAdFormat6, lVar));
    }

    private boolean a() {
        return ((Boolean) this.f1762a.a(b.W3)).booleanValue() && this.c.get();
    }

    private LinkedHashSet<d> c(JSONArray jSONArray) {
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>(jSONArray.length());
        for (int i = 0; i < jSONArray.length(); i++) {
            JSONObject a2 = j.a(jSONArray, i, (JSONObject) null, this.f1762a);
            linkedHashSet.add(d.a(j.b(a2, "id", (String) null, this.f1762a), a2, this.f1762a));
        }
        return linkedHashSet;
    }

    public void a(AppLovinAdBase appLovinAdBase, boolean z, boolean z2) {
        if (a()) {
            this.d.get(appLovinAdBase.getAdZone().b()).a(appLovinAdBase, z, z2);
        }
    }

    public void a(d dVar, boolean z, int i) {
        if (a()) {
            MaxAdFormat b2 = dVar.b();
            if (b2 != null) {
                this.d.get(b2).a(dVar, z, i);
            } else {
                f.a(dVar, i, this.f1762a);
            }
        }
    }

    public void a(g gVar) {
        if (a()) {
            this.d.get(gVar.getAdZone().b()).a();
        }
    }

    public void a(JSONArray jSONArray) {
        if (((Boolean) this.f1762a.a(b.W3)).booleanValue()) {
            if (this.c.compareAndSet(false, true)) {
                ArrayList arrayList = new ArrayList();
                ArrayList arrayList2 = new ArrayList();
                ArrayList arrayList3 = new ArrayList();
                ArrayList arrayList4 = new ArrayList();
                ArrayList arrayList5 = new ArrayList();
                ArrayList arrayList6 = new ArrayList();
                for (int i = 0; i < jSONArray.length(); i++) {
                    JSONObject a2 = j.a(jSONArray, i, (JSONObject) null, this.f1762a);
                    d a3 = d.a(j.b(a2, "id", (String) null, this.f1762a), a2, this.f1762a);
                    MaxAdFormat b2 = a3.b();
                    if (b2 == MaxAdFormat.BANNER) {
                        arrayList.add(a3);
                    } else if (b2 == MaxAdFormat.LEADER) {
                        arrayList2.add(a3);
                    } else if (b2 == MaxAdFormat.MREC) {
                        arrayList3.add(a3);
                    } else if (b2 == MaxAdFormat.INTERSTITIAL) {
                        arrayList4.add(a3);
                    } else if (b2 == MaxAdFormat.REWARDED_INTERSTITIAL) {
                        arrayList6.add(a3);
                    } else if (b2 == MaxAdFormat.REWARDED) {
                        arrayList5.add(a3);
                    }
                }
                this.d.get(MaxAdFormat.BANNER).a((List<d>) arrayList);
                this.d.get(MaxAdFormat.LEADER).a((List<d>) arrayList2);
                this.d.get(MaxAdFormat.MREC).a((List<d>) arrayList3);
                this.d.get(MaxAdFormat.INTERSTITIAL).a((List<d>) arrayList4);
                this.d.get(MaxAdFormat.REWARDED).a((List<d>) arrayList5);
                this.d.get(MaxAdFormat.REWARDED_INTERSTITIAL).a((List<d>) arrayList6);
            }
        }
    }

    public LinkedHashSet<d> b(JSONArray jSONArray) {
        if (jSONArray == null) {
            return new LinkedHashSet<>();
        }
        LinkedHashSet<d> linkedHashSet = new LinkedHashSet<>(jSONArray.length());
        synchronized (this.e) {
            if (!this.f) {
                r rVar = this.b;
                rVar.b("AdZoneManager", "Found " + jSONArray.length() + " zone(s)...");
                linkedHashSet = c(jSONArray);
                this.f = true;
            }
        }
        return linkedHashSet;
    }
}
