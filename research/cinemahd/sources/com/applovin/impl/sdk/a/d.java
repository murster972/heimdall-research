package com.applovin.impl.sdk.a;

import android.text.TextUtils;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Map;
import org.json.JSONObject;

public final class d {
    private static final Map<String, d> f = new HashMap();
    private static final Object g = new Object();

    /* renamed from: a  reason: collision with root package name */
    private l f1761a;
    private JSONObject b;
    private final String c;
    private AppLovinAdSize d;
    private AppLovinAdType e;

    private d(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, l lVar) {
        String str2;
        if (!TextUtils.isEmpty(str) || !(appLovinAdType == null || appLovinAdSize == null)) {
            this.f1761a = lVar;
            this.d = appLovinAdSize;
            this.e = appLovinAdType;
            if (!TextUtils.isEmpty(str)) {
                str2 = str.toLowerCase(Locale.ENGLISH);
            } else {
                str2 = (appLovinAdSize.getLabel() + "_" + appLovinAdType.getLabel()).toLowerCase(Locale.ENGLISH);
            }
            this.c = str2;
            return;
        }
        throw new IllegalArgumentException("No zone identifier or type or size specified");
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, l lVar) {
        return a(appLovinAdSize, appLovinAdType, (String) null, lVar);
    }

    public static d a(AppLovinAdSize appLovinAdSize, AppLovinAdType appLovinAdType, String str, l lVar) {
        d dVar = new d(appLovinAdSize, appLovinAdType, str, lVar);
        synchronized (g) {
            String str2 = dVar.c;
            if (f.containsKey(str2)) {
                dVar = f.get(str2);
            } else {
                f.put(str2, dVar);
            }
        }
        return dVar;
    }

    public static d a(String str, l lVar) {
        return a((AppLovinAdSize) null, (AppLovinAdType) null, str, lVar);
    }

    public static d a(String str, JSONObject jSONObject, l lVar) {
        d a2 = a(str, lVar);
        a2.b = jSONObject;
        return a2;
    }

    public static Collection<d> a(l lVar) {
        LinkedHashSet linkedHashSet = new LinkedHashSet(5);
        Collections.addAll(linkedHashSet, new d[]{b(lVar), c(lVar), d(lVar), e(lVar), f(lVar)});
        return Collections.unmodifiableSet(linkedHashSet);
    }

    public static void a(JSONObject jSONObject, l lVar) {
        if (jSONObject != null && jSONObject.has("ad_size") && jSONObject.has("ad_type")) {
            synchronized (g) {
                d dVar = f.get(j.b(jSONObject, "zone_id", "", lVar));
                if (dVar != null) {
                    dVar.d = AppLovinAdSize.fromString(j.b(jSONObject, "ad_size", "", lVar));
                    dVar.e = AppLovinAdType.fromString(j.b(jSONObject, "ad_type", "", lVar));
                }
            }
        }
    }

    public static d b(l lVar) {
        return a(AppLovinAdSize.BANNER, AppLovinAdType.REGULAR, lVar);
    }

    public static d b(String str, l lVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, str, lVar);
    }

    public static d c(l lVar) {
        return a(AppLovinAdSize.MREC, AppLovinAdType.REGULAR, lVar);
    }

    public static d d(l lVar) {
        return a(AppLovinAdSize.LEADER, AppLovinAdType.REGULAR, lVar);
    }

    public static d e(l lVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.REGULAR, lVar);
    }

    public static d f(l lVar) {
        return a(AppLovinAdSize.INTERSTITIAL, AppLovinAdType.INCENTIVIZED, lVar);
    }

    public String a() {
        return this.c;
    }

    public MaxAdFormat b() {
        AppLovinAdSize c2 = c();
        if (c2 == AppLovinAdSize.BANNER) {
            return MaxAdFormat.BANNER;
        }
        if (c2 == AppLovinAdSize.LEADER) {
            return MaxAdFormat.LEADER;
        }
        if (c2 == AppLovinAdSize.MREC) {
            return MaxAdFormat.MREC;
        }
        if (c2 != AppLovinAdSize.INTERSTITIAL) {
            return null;
        }
        if (d() == AppLovinAdType.REGULAR) {
            return MaxAdFormat.INTERSTITIAL;
        }
        if (d() == AppLovinAdType.INCENTIVIZED) {
            return MaxAdFormat.REWARDED;
        }
        if (d() == AppLovinAdType.AUTO_INCENTIVIZED) {
            return MaxAdFormat.REWARDED_INTERSTITIAL;
        }
        return null;
    }

    public AppLovinAdSize c() {
        if (this.d == null && j.a(this.b, "ad_size")) {
            this.d = AppLovinAdSize.fromString(j.b(this.b, "ad_size", (String) null, this.f1761a));
        }
        return this.d;
    }

    public AppLovinAdType d() {
        if (this.e == null && j.a(this.b, "ad_type")) {
            this.e = AppLovinAdType.fromString(j.b(this.b, "ad_type", (String) null, this.f1761a));
        }
        return this.e;
    }

    public boolean e() {
        return a(this.f1761a).contains(this);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || d.class != obj.getClass()) {
            return false;
        }
        return this.c.equalsIgnoreCase(((d) obj).c);
    }

    public int hashCode() {
        return this.c.hashCode();
    }

    public String toString() {
        return "AdZone{id=" + this.c + ", zoneObject=" + this.b + '}';
    }
}
