package com.applovin.impl.sdk.a;

import android.net.Uri;
import com.applovin.impl.adview.h;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.vungle.warren.analytics.AnalyticsEvent;
import com.vungle.warren.model.Advertisement;
import org.json.JSONObject;

public final class a extends g {
    private final String o = B0();
    private final String p = D0();
    private final String q = J0();

    public a(JSONObject jSONObject, JSONObject jSONObject2, b bVar, l lVar) {
        super(jSONObject, jSONObject2, bVar, lVar);
    }

    private String J0() {
        return getStringFromAdObject("stream_url", "");
    }

    public void A() {
        synchronized (this.adObjectLock) {
            j.a(this.adObject, "html", this.o, this.sdk);
            j.a(this.adObject, "stream_url", this.q, this.sdk);
        }
    }

    public String B0() {
        String b;
        synchronized (this.adObjectLock) {
            b = j.b(this.adObject, "html", (String) null, this.sdk);
        }
        return b;
    }

    public void C0() {
        synchronized (this.adObjectLock) {
            this.adObject.remove("stream_url");
        }
    }

    public String D0() {
        return getStringFromAdObject(Advertisement.KEY_VIDEO, "");
    }

    public Uri E0() {
        String stringFromAdObject = getStringFromAdObject(AnalyticsEvent.Ad.clickUrl, "");
        if (o.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public float F0() {
        return getFloatFromAdObject("mraid_close_delay_graphic", 0.0f);
    }

    public boolean G0() {
        return getBooleanFromAdObject("close_button_graphic_hidden", false);
    }

    public boolean H0() {
        if (this.adObject.has("close_button_expandable_hidden")) {
            return getBooleanFromAdObject("close_button_expandable_hidden", false);
        }
        return true;
    }

    public h.a I0() {
        return a(getIntFromAdObject("expandable_style", h.a.INVISIBLE.a()));
    }

    public void a(String str) {
        synchronized (this.adObjectLock) {
            j.a(this.adObject, "html", str, this.sdk);
        }
    }

    public void d(Uri uri) {
        synchronized (this.adObjectLock) {
            j.a(this.adObject, Advertisement.KEY_VIDEO, uri.toString(), this.sdk);
        }
    }

    public boolean hasVideoUrl() {
        return m0() != null;
    }

    public String k0() {
        return this.p;
    }

    public boolean l0() {
        return this.adObject.has("stream_url");
    }

    public Uri m0() {
        String J0 = J0();
        if (o.b(J0)) {
            return Uri.parse(J0);
        }
        String D0 = D0();
        if (o.b(D0)) {
            return Uri.parse(D0);
        }
        return null;
    }

    public Uri n0() {
        String stringFromAdObject = getStringFromAdObject("video_click_url", "");
        return o.b(stringFromAdObject) ? Uri.parse(stringFromAdObject) : E0();
    }
}
