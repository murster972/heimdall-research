package com.applovin.impl.sdk.a;

import android.text.TextUtils;
import android.util.Base64;
import com.applovin.impl.sdk.c.b;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.o;
import java.io.UnsupportedEncodingException;
import org.json.JSONException;
import org.json.JSONObject;

public class c {

    /* renamed from: a  reason: collision with root package name */
    private final l f1759a;
    private final String b;

    public enum a {
        UNSPECIFIED("UNSPECIFIED"),
        REGULAR("REGULAR"),
        AD_RESPONSE_JSON("AD_RESPONSE_JSON");
        
        private final String d;

        private a(String str) {
            this.d = str;
        }

        public String toString() {
            return this.d;
        }
    }

    public c(String str, l lVar) {
        if (TextUtils.isEmpty(str)) {
            throw new IllegalArgumentException("Identifier is empty");
        } else if (lVar != null) {
            this.b = str;
            this.f1759a = lVar;
        } else {
            throw new IllegalArgumentException("No sdk specified");
        }
    }

    private String a(b<String> bVar) {
        for (String next : this.f1759a.b(bVar)) {
            if (this.b.startsWith(next)) {
                return next;
            }
        }
        return null;
    }

    public String a() {
        return this.b;
    }

    public a b() {
        return a(b.h0) != null ? a.REGULAR : a(b.i0) != null ? a.AD_RESPONSE_JSON : a.UNSPECIFIED;
    }

    public String c() {
        String a2 = a(b.h0);
        if (!TextUtils.isEmpty(a2)) {
            return a2;
        }
        String a3 = a(b.i0);
        if (!TextUtils.isEmpty(a3)) {
            return a3;
        }
        return null;
    }

    public JSONObject d() {
        if (b() != a.AD_RESPONSE_JSON) {
            return null;
        }
        try {
            try {
                JSONObject jSONObject = new JSONObject(new String(Base64.decode(this.b.substring(c().length()), 0), "UTF-8"));
                r j0 = this.f1759a.j0();
                j0.b("AdToken", "Decoded token into ad response: " + jSONObject);
                return jSONObject;
            } catch (JSONException e) {
                r j02 = this.f1759a.j0();
                j02.b("AdToken", "Unable to decode token '" + this.b + "' into JSON", e);
                return null;
            }
        } catch (UnsupportedEncodingException e2) {
            r j03 = this.f1759a.j0();
            j03.b("AdToken", "Unable to process ad response from token '" + this.b + "'", e2);
            return null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof c)) {
            return false;
        }
        String str = this.b;
        String str2 = ((c) obj).b;
        return str != null ? str.equals(str2) : str2 == null;
    }

    public int hashCode() {
        String str = this.b;
        if (str != null) {
            return str.hashCode();
        }
        return 0;
    }

    public String toString() {
        String a2 = o.a(32, this.b);
        return "AdToken{id=" + a2 + ", type=" + b() + '}';
    }
}
