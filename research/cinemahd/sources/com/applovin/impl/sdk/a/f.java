package com.applovin.impl.sdk.a;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.AppLovinBroadcastManager;
import com.applovin.impl.sdk.e.m;
import com.applovin.impl.sdk.e.o;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.r;
import com.applovin.impl.sdk.utils.d;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.p;
import com.applovin.mediation.MaxAdFormat;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdLoadListener;
import com.facebook.react.uimanager.ViewProps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.json.JSONArray;
import org.json.JSONObject;

public class f implements Runnable {
    private static boolean i;

    /* renamed from: a  reason: collision with root package name */
    private final l f1763a;
    private final MaxAdFormat b;
    private List<d> c;
    /* access modifiers changed from: private */
    public final List<JSONObject> d;
    /* access modifiers changed from: private */
    public final Object e;
    private d f;
    private b g = b.NONE;
    private boolean h;

    public static class a implements AppLovinAdLoadListener {

        /* renamed from: a  reason: collision with root package name */
        private final l f1765a;
        private final d b;
        private final AppLovinAdLoadListener c;
        private boolean d;

        public a(d dVar, AppLovinAdLoadListener appLovinAdLoadListener, l lVar) {
            this.f1765a = lVar;
            this.b = dVar;
            this.c = appLovinAdLoadListener;
        }

        public void a(boolean z) {
            this.d = z;
        }

        public void adReceived(AppLovinAd appLovinAd) {
            this.f1765a.y().a((AppLovinAdBase) (g) appLovinAd, false, this.d);
            this.c.adReceived(appLovinAd);
        }

        public void failedToReceiveAd(int i) {
            this.f1765a.y().a(this.b, this.d, i);
            this.c.failedToReceiveAd(i);
        }
    }

    public enum b {
        NONE(0, ViewProps.NONE),
        TIMER(1, "timer"),
        APP_PAUSED(2, "backgrounded"),
        IMPRESSION(3, "impression"),
        WATERFALL_RESTARTED(3, "waterfall_restarted"),
        UNKNOWN_ZONE(4, "unknown_zone"),
        SKIPPED_ZONE(5, "skipped_zone"),
        REPEATED_ZONE(6, "repeated_zone");
        
        private final int i;
        private final String j;

        private b(int i2, String str) {
            this.i = i2;
            this.j = str;
        }

        public int a() {
            return this.i;
        }

        public String b() {
            return this.j;
        }
    }

    public f(MaxAdFormat maxAdFormat, l lVar) {
        this.f1763a = lVar;
        this.b = maxAdFormat;
        this.d = new ArrayList();
        this.e = new Object();
    }

    private static JSONObject a(d dVar, l lVar) {
        JSONObject jSONObject = new JSONObject();
        j.a(jSONObject, "id", dVar.a(), lVar);
        j.b(jSONObject, "response_ts_s", TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis()), lVar);
        return jSONObject;
    }

    public static void a(d dVar, int i2, l lVar) {
        if (!((Boolean) lVar.a(com.applovin.impl.sdk.c.b.e4)).booleanValue()) {
            if (!i) {
                r.i("AppLovinSdk", "Unknown zone in waterfall: " + dVar.a());
                i = true;
            } else {
                return;
            }
        }
        JSONObject a2 = a(dVar, lVar);
        j.a(a2, "error_code", i2, lVar);
        a(b.UNKNOWN_ZONE, b.NONE, j.b((Object) a2), (MaxAdFormat) null, lVar);
    }

    private void a(d dVar, JSONObject jSONObject) {
        b bVar;
        j.a(jSONObject, a(dVar, this.f1763a), this.f1763a);
        synchronized (this.e) {
            if (a(dVar)) {
                a(b.WATERFALL_RESTARTED);
            } else {
                if (b(dVar)) {
                    a(jSONObject, dVar);
                    bVar = b.REPEATED_ZONE;
                } else if (c(dVar)) {
                    a(jSONObject, dVar);
                    bVar = b.SKIPPED_ZONE;
                }
                a(bVar, dVar);
            }
            a(jSONObject, dVar);
        }
    }

    /* access modifiers changed from: private */
    public void a(b bVar) {
        a(bVar, (d) null);
    }

    private void a(b bVar, d dVar) {
        if (!((Boolean) this.f1763a.a(com.applovin.impl.sdk.c.b.e4)).booleanValue()) {
            if (!this.h) {
                if (bVar == b.SKIPPED_ZONE || bVar == b.REPEATED_ZONE) {
                    r.i("AppLovinSdk", "Invalid zone in waterfall: " + dVar);
                    this.h = true;
                }
            } else {
                return;
            }
        }
        synchronized (this.e) {
            if (!this.d.isEmpty()) {
                JSONArray jSONArray = new JSONArray(this.d);
                this.d.clear();
                b bVar2 = this.g;
                this.g = bVar;
                a(bVar, bVar2, jSONArray, this.b, this.f1763a);
            }
        }
    }

    private static void a(b bVar, b bVar2, JSONArray jSONArray, MaxAdFormat maxAdFormat, l lVar) {
        lVar.o().a((com.applovin.impl.sdk.e.a) new m(bVar, bVar2, jSONArray, maxAdFormat, lVar), o.a.BACKGROUND);
    }

    private void a(JSONObject jSONObject, d dVar) {
        synchronized (this.e) {
            this.d.add(jSONObject);
            this.f = dVar;
        }
    }

    private boolean a(d dVar) {
        if (this.f != null) {
            int indexOf = this.c.indexOf(dVar);
            return indexOf == 0 || indexOf < this.c.indexOf(this.f);
        }
    }

    private void b() {
        long c2 = c();
        if (c2 <= 0) {
            return;
        }
        if (((Boolean) this.f1763a.a(com.applovin.impl.sdk.c.b.b4)).booleanValue()) {
            d.a(c2, this.f1763a, this);
        } else {
            p.a(c2, this.f1763a, this);
        }
    }

    private boolean b(d dVar) {
        return this.f == dVar;
    }

    private long c() {
        return TimeUnit.SECONDS.toMillis(((Long) this.f1763a.a(com.applovin.impl.sdk.c.b.a4)).longValue());
    }

    private boolean c(d dVar) {
        int indexOf = this.c.indexOf(dVar);
        d dVar2 = this.f;
        return indexOf != (dVar2 != null ? this.c.indexOf(dVar2) + 1 : 0);
    }

    public void a() {
        if (((Boolean) this.f1763a.a(com.applovin.impl.sdk.c.b.c4)).booleanValue()) {
            a(b.IMPRESSION);
        }
    }

    public void a(AppLovinAdBase appLovinAdBase, boolean z, boolean z2) {
        JSONObject jSONObject = new JSONObject();
        j.b(jSONObject, "ad_id", appLovinAdBase.getAdIdNumber(), this.f1763a);
        j.b(jSONObject, "ad_created_ts_s", TimeUnit.MILLISECONDS.toSeconds(appLovinAdBase.getCreatedAtMillis()), this.f1763a);
        j.a(jSONObject, "is_preloaded", z, this.f1763a);
        j.a(jSONObject, "for_bidding", z2, this.f1763a);
        a(appLovinAdBase.getAdZone(), jSONObject);
    }

    public void a(d dVar, boolean z, int i2) {
        JSONObject jSONObject = new JSONObject();
        j.a(jSONObject, "error_code", i2, this.f1763a);
        j.a(jSONObject, "for_bidding", z, this.f1763a);
        a(dVar, jSONObject);
    }

    public void a(List<d> list) {
        if (this.c == null) {
            this.c = list;
            b();
            if (((Boolean) this.f1763a.a(com.applovin.impl.sdk.c.b.d4)).booleanValue()) {
                this.f1763a.I().registerReceiver(new AppLovinBroadcastManager.Receiver() {
                    public void onReceive(Context context, Intent intent, Map<String, Object> map) {
                        f.this.a(b.APP_PAUSED);
                        synchronized (f.this.e) {
                            f.this.d.clear();
                        }
                    }
                }, new IntentFilter("com.applovin.application_paused"));
            }
        }
    }

    public void run() {
        a(b.TIMER);
        b();
    }
}
