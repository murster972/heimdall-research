package com.applovin.impl.sdk.a;

import android.graphics.Color;
import android.graphics.Point;
import android.graphics.PointF;
import android.net.Uri;
import com.applovin.impl.adview.h;
import com.applovin.impl.adview.s;
import com.applovin.impl.adview.v;
import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.l;
import com.applovin.impl.sdk.utils.e;
import com.applovin.impl.sdk.utils.j;
import com.applovin.impl.sdk.utils.o;
import com.applovin.impl.sdk.utils.r;
import com.applovin.impl.sdk.y;
import com.applovin.sdk.AppLovinAdType;
import com.applovin.sdk.AppLovinSdkUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import org.json.JSONObject;

public abstract class g extends AppLovinAdBase {
    private final List<Uri> e = e.a();
    private final AtomicBoolean f = new AtomicBoolean();
    private final AtomicBoolean g = new AtomicBoolean();
    private final AtomicReference<com.applovin.impl.sdk.b.c> h = new AtomicReference<>();
    private boolean i;
    private List<com.applovin.impl.sdk.d.a> j;
    private List<com.applovin.impl.sdk.d.a> k;
    private List<com.applovin.impl.sdk.d.a> l;
    private List<com.applovin.impl.sdk.d.a> m;
    private c n;

    public enum a {
        UNSPECIFIED,
        DISMISS,
        DO_NOT_DISMISS
    }

    public enum b {
        DEFAULT,
        ACTIVITY_PORTRAIT,
        ACTIVITY_LANDSCAPE
    }

    public class c {

        /* renamed from: a  reason: collision with root package name */
        public final int f1769a;
        public final int b;
        public final int c;
        public final int d;
        public final int e;

        private c(g gVar) {
            this.f1769a = AppLovinSdkUtils.dpToPx(gVar.sdk.i(), gVar.v());
            this.b = AppLovinSdkUtils.dpToPx(gVar.sdk.i(), gVar.w());
            this.c = AppLovinSdkUtils.dpToPx(gVar.sdk.i(), gVar.x());
            this.d = AppLovinSdkUtils.dpToPx(gVar.sdk.i(), ((Integer) gVar.sdk.a(com.applovin.impl.sdk.c.b.K0)).intValue());
            this.e = AppLovinSdkUtils.dpToPx(gVar.sdk.i(), ((Integer) gVar.sdk.a(com.applovin.impl.sdk.c.b.J0)).intValue());
        }
    }

    public enum d {
        RESIZE_ASPECT,
        RESIZE_ASPECT_FILL,
        RESIZE
    }

    public g(JSONObject jSONObject, JSONObject jSONObject2, b bVar, l lVar) {
        super(jSONObject, jSONObject2, bVar, lVar);
    }

    private String B0() {
        String stringFromAdObject = getStringFromAdObject("video_end_url", (String) null);
        if (stringFromAdObject != null) {
            return stringFromAdObject.replace("{CLCODE}", getClCode());
        }
        return null;
    }

    private List<com.applovin.impl.sdk.d.a> a(PointF pointF, boolean z) {
        List<com.applovin.impl.sdk.d.a> a2;
        synchronized (this.adObjectLock) {
            a2 = r.a("click_tracking_urls", this.adObject, c(pointF, z), b(pointF, z), c0(), x0(), this.sdk);
        }
        return a2;
    }

    private String b(PointF pointF, boolean z) {
        String stringFromAdObject = getStringFromAdObject("click_tracking_url", (String) null);
        Map<String, String> c2 = c(pointF, z);
        if (stringFromAdObject != null) {
            return o.a(stringFromAdObject, c2);
        }
        return null;
    }

    private h.a c(boolean z) {
        return z ? h.a.WHITE_ON_TRANSPARENT : h.a.WHITE_ON_BLACK;
    }

    private Map<String, String> c(PointF pointF, boolean z) {
        Point a2 = com.applovin.impl.sdk.utils.g.a(this.sdk.i());
        HashMap hashMap = new HashMap(5);
        hashMap.put("{CLCODE}", getClCode());
        hashMap.put("{CLICK_X}", String.valueOf(pointF.x));
        hashMap.put("{CLICK_Y}", String.valueOf(pointF.y));
        hashMap.put("{SCREEN_WIDTH}", String.valueOf(a2.x));
        hashMap.put("{SCREEN_HEIGHT}", String.valueOf(a2.y));
        hashMap.put("{IS_VIDEO_CLICK}", String.valueOf(z));
        return hashMap;
    }

    public abstract void A();

    public String A0() {
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("video_button_properties", (JSONObject) null);
        return jsonObjectFromAdObject != null ? j.b(jsonObjectFromAdObject, "video_button_html", "", this.sdk) : "";
    }

    public boolean B() {
        return getBooleanFromAdObject("ibbdfs", false);
    }

    public boolean C() {
        return getBooleanFromAdObject("ibbdfc", false);
    }

    public Uri D() {
        String stringFromAdObject = getStringFromAdObject("mute_image", (String) null);
        if (o.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public Uri E() {
        String stringFromAdObject = getStringFromAdObject("unmute_image", "");
        if (o.b(stringFromAdObject)) {
            return Uri.parse(stringFromAdObject);
        }
        return null;
    }

    public boolean F() {
        return this.g.get();
    }

    public void G() {
        this.g.set(true);
    }

    public com.applovin.impl.sdk.b.c H() {
        return this.h.getAndSet((Object) null);
    }

    public boolean I() {
        return getBooleanFromAdObject("suep", false);
    }

    public boolean J() {
        return getBooleanFromAdObject("upiosp", false);
    }

    public long K() {
        long longFromAdObject = getLongFromAdObject("report_reward_duration", -1);
        if (longFromAdObject >= 0) {
            return TimeUnit.SECONDS.toMillis(longFromAdObject);
        }
        return -1;
    }

    public int L() {
        return getIntFromAdObject("report_reward_percent", -1);
    }

    public boolean M() {
        return getBooleanFromAdObject("report_reward_percent_include_close_delay", true);
    }

    public AtomicBoolean N() {
        return this.f;
    }

    public boolean O() {
        return getBooleanFromAdObject("show_nia", false);
    }

    public String P() {
        return getStringFromAdObject("nia_title", "");
    }

    public String Q() {
        return getStringFromAdObject("nia_message", "");
    }

    public String R() {
        return getStringFromAdObject("nia_button_title", "");
    }

    public boolean S() {
        return getBooleanFromAdObject("avoms", false);
    }

    public boolean T() {
        return this.i;
    }

    public boolean U() {
        return getBooleanFromAdObject("show_rewarded_interstitial_overlay_alert", Boolean.valueOf(AppLovinAdType.AUTO_INCENTIVIZED == getType()));
    }

    public String V() {
        return getStringFromAdObject("text_rewarded_inter_alert_title", "Watch a video to earn a reward!");
    }

    public String W() {
        return getStringFromAdObject("text_rewarded_inter_alert_body", "");
    }

    public String X() {
        return getStringFromAdObject("text_rewarded_inter_alert_ok_action", "OK!");
    }

    public List<com.applovin.impl.sdk.d.a> Y() {
        List<com.applovin.impl.sdk.d.a> list = this.j;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.j = r.a("video_end_urls", this.adObject, getClCode(), B0(), this.sdk);
        }
        return this.j;
    }

    public List<com.applovin.impl.sdk.d.a> Z() {
        List<com.applovin.impl.sdk.d.a> list = this.k;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.k = r.a("ad_closed_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.k;
    }

    /* access modifiers changed from: protected */
    public h.a a(int i2) {
        return i2 == 1 ? h.a.WHITE_ON_TRANSPARENT : i2 == 2 ? h.a.INVISIBLE : h.a.WHITE_ON_BLACK;
    }

    public s a() {
        return new s(getJsonObjectFromAdObject("video_button_properties", (JSONObject) null), this.sdk);
    }

    public List<com.applovin.impl.sdk.d.a> a(PointF pointF) {
        return a(pointF, false);
    }

    public void a(Uri uri) {
        this.e.add(uri);
    }

    public void a(com.applovin.impl.sdk.b.c cVar) {
        this.h.set(cVar);
    }

    public void a(boolean z) {
        try {
            synchronized (this.adObjectLock) {
                this.adObject.put("html_resources_cached", z);
            }
        } catch (Throwable unused) {
        }
    }

    public List<com.applovin.impl.sdk.d.a> a0() {
        List<com.applovin.impl.sdk.d.a> list = this.l;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.l = r.a("app_killed_urls", this.adObject, getClCode(), (String) null, this.sdk);
        }
        return this.l;
    }

    public List<com.applovin.impl.sdk.d.a> b(PointF pointF) {
        List<com.applovin.impl.sdk.d.a> a2;
        synchronized (this.adObjectLock) {
            a2 = r.a("video_click_tracking_urls", this.adObject, c(pointF, true), (String) null, c0(), x0(), this.sdk);
        }
        return a2.isEmpty() ? a(pointF, true) : a2;
    }

    public void b(Uri uri) {
        synchronized (this.adObjectLock) {
            j.a(this.adObject, "mute_image", (Object) uri, this.sdk);
        }
    }

    public void b(boolean z) {
        this.i = z;
    }

    public boolean b() {
        return getBooleanFromAdObject("video_clickable", false);
    }

    public List<com.applovin.impl.sdk.d.a> b0() {
        List<com.applovin.impl.sdk.d.a> list = this.m;
        if (list != null) {
            return list;
        }
        synchronized (this.adObjectLock) {
            this.m = r.a("imp_urls", this.adObject, getClCode(), e.a("{SOC}", String.valueOf(T())), (String) null, c0(), x0(), this.sdk);
        }
        return this.m;
    }

    public void c(Uri uri) {
        synchronized (this.adObjectLock) {
            j.a(this.adObject, "unmute_image", (Object) uri, this.sdk);
        }
    }

    public boolean c() {
        return getBooleanFromAdObject("lock_current_orientation", false);
    }

    public Map<String, String> c0() {
        HashMap hashMap = new HashMap();
        if (getBooleanFromAdObject("send_webview_http_headers", false)) {
            hashMap.putAll(y.b());
        }
        if (getBooleanFromAdObject("use_webview_ua_for_postbacks", false)) {
            hashMap.put("User-Agent", y.a());
        }
        return hashMap;
    }

    public int d() {
        return getIntFromAdObject("countdown_length", 0);
    }

    public boolean d0() {
        return getBooleanFromAdObject("playback_requires_user_action", true);
    }

    public int e() {
        int parseColor = Color.parseColor("#C8FFFFFF");
        String stringFromAdObject = getStringFromAdObject("countdown_color", (String) null);
        return o.b(stringFromAdObject) ? Color.parseColor(stringFromAdObject) : parseColor;
    }

    public String e0() {
        String stringFromAdObject = getStringFromAdObject("base_url", "/");
        if ("null".equalsIgnoreCase(stringFromAdObject)) {
            return null;
        }
        return stringFromAdObject;
    }

    public int f() {
        int i2 = hasVideoUrl() ? -16777216 : -1157627904;
        String stringFromAdObject = getStringFromAdObject("graphic_background_color", (String) null);
        return o.b(stringFromAdObject) ? Color.parseColor(stringFromAdObject) : i2;
    }

    public boolean f0() {
        return getBooleanFromAdObject("web_contents_debugging_enabled", false);
    }

    public a g() {
        String stringFromAdObject = getStringFromAdObject("poststitial_dismiss_type", (String) null);
        if (o.b(stringFromAdObject)) {
            if ("dismiss".equalsIgnoreCase(stringFromAdObject)) {
                return a.DISMISS;
            }
            if ("no_dismiss".equalsIgnoreCase(stringFromAdObject)) {
                return a.DO_NOT_DISMISS;
            }
        }
        return a.UNSPECIFIED;
    }

    public v g0() {
        JSONObject jsonObjectFromAdObject = getJsonObjectFromAdObject("web_view_settings", (JSONObject) null);
        if (jsonObjectFromAdObject != null) {
            return new v(jsonObjectFromAdObject, this.sdk);
        }
        return null;
    }

    public List<String> h() {
        String stringFromAdObject = getStringFromAdObject("resource_cache_prefix", (String) null);
        return stringFromAdObject != null ? e.a(stringFromAdObject) : this.sdk.b(com.applovin.impl.sdk.c.b.u0);
    }

    public int h0() {
        return getIntFromAdObject("whalt", r.a(getSize()) ? 1 : ((Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.S3)).booleanValue() ? 0 : -1);
    }

    public String i() {
        return getStringFromAdObject("cache_prefix", (String) null);
    }

    public List<String> i0() {
        return e.a(getStringFromAdObject("wls", ""));
    }

    public boolean j() {
        return getBooleanFromAdObject("sscomt", false);
    }

    public List<String> j0() {
        return e.a(getStringFromAdObject("wlh", (String) null));
    }

    public String k() {
        return getStringFromFullResponse("event_id", (String) null);
    }

    public abstract String k0();

    public boolean l() {
        return getBooleanFromAdObject("progress_bar_enabled", false);
    }

    public boolean l0() {
        this.sdk.j0().e("DirectAd", "Attempting to invoke isVideoStream() from base ad class");
        return false;
    }

    public int m() {
        String stringFromAdObject = getStringFromAdObject("progress_bar_color", "#C8FFFFFF");
        if (o.b(stringFromAdObject)) {
            return Color.parseColor(stringFromAdObject);
        }
        return 0;
    }

    public Uri m0() {
        this.sdk.j0().e("DirectAd", "Attempting to invoke getVideoUri() from base ad class");
        return null;
    }

    public int n() {
        int a2;
        synchronized (this.adObjectLock) {
            a2 = r.a(this.adObject);
        }
        return a2;
    }

    public Uri n0() {
        this.sdk.j0().e("DirectAd", "Attempting to invoke getVideoClickDestinationUri() from base ad class");
        return null;
    }

    public int o() {
        synchronized (this.adObjectLock) {
            int b2 = j.b(this.adObject, "graphic_completion_percent", -1, (l) null);
            if (b2 < 0 || b2 > 100) {
                return 90;
            }
            return b2;
        }
    }

    public boolean o0() {
        return getBooleanFromAdObject("fs_2", false);
    }

    public int p() {
        return getIntFromAdObject("poststitial_shown_forward_delay_millis", -1);
    }

    public b p0() {
        String upperCase = getStringFromAdObject("ad_target", b.DEFAULT.toString()).toUpperCase(Locale.ENGLISH);
        return "ACTIVITY_PORTRAIT".equalsIgnoreCase(upperCase) ? b.ACTIVITY_PORTRAIT : "ACTIVITY_LANDSCAPE".equalsIgnoreCase(upperCase) ? b.ACTIVITY_LANDSCAPE : b.DEFAULT;
    }

    public int q() {
        return getIntFromAdObject("poststitial_dismiss_forward_delay_millis", -1);
    }

    public String q0() {
        return getStringFromFullResponse("dsp_name", "");
    }

    public boolean r() {
        return getBooleanFromAdObject("should_apply_mute_setting_to_poststitial", false);
    }

    public long r0() {
        return getLongFromAdObject("close_delay", 0);
    }

    public boolean s() {
        return getBooleanFromAdObject("should_forward_close_button_tapped_to_poststitial", false);
    }

    public long s0() {
        return TimeUnit.SECONDS.toMillis(getLongFromAdObject("close_delay_max_buffering_time_seconds", 5));
    }

    public boolean t() {
        return getBooleanFromAdObject("forward_lifecycle_events_to_webview", false);
    }

    public long t0() {
        long longFromAdObject = getLongFromAdObject("close_delay_graphic", 0);
        if (!o0()) {
            return longFromAdObject;
        }
        if (longFromAdObject == -1 || longFromAdObject == -2) {
            return 0;
        }
        return longFromAdObject;
    }

    public c u() {
        if (this.n == null) {
            this.n = new c();
        }
        return this.n;
    }

    public h.a u0() {
        int intFromAdObject = getIntFromAdObject("close_style", -1);
        return intFromAdObject == -1 ? c(hasVideoUrl()) : a(intFromAdObject);
    }

    public int v() {
        return getIntFromAdObject("close_button_size", ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.i1)).intValue());
    }

    public h.a v0() {
        int intFromAdObject = getIntFromAdObject("skip_style", -1);
        return intFromAdObject == -1 ? u0() : a(intFromAdObject);
    }

    public int w() {
        return getIntFromAdObject("close_button_top_margin", ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.j1)).intValue());
    }

    public boolean w0() {
        return getBooleanFromAdObject("dismiss_on_skip", false);
    }

    public int x() {
        return getIntFromAdObject("close_button_horizontal_margin", ((Integer) this.sdk.a(com.applovin.impl.sdk.c.b.h1)).intValue());
    }

    public boolean x0() {
        return getBooleanFromAdObject("fire_postbacks_from_webview", false);
    }

    public boolean y() {
        return getBooleanFromAdObject("lhs_close_button", (Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.g1));
    }

    public boolean y0() {
        return getBooleanFromAdObject("html_resources_cached", false);
    }

    public boolean z() {
        return getBooleanFromAdObject("lhs_skip_button", (Boolean) this.sdk.a(com.applovin.impl.sdk.c.b.w1));
    }

    public List<Uri> z0() {
        return this.e;
    }
}
