package com.applovin.impl.sdk.a;

import com.applovin.impl.sdk.AppLovinAdBase;
import com.applovin.impl.sdk.l;
import com.applovin.sdk.AppLovinAd;
import com.applovin.sdk.AppLovinAdSize;
import com.applovin.sdk.AppLovinAdType;
import org.json.JSONObject;

public final class h extends AppLovinAdBase {
    private AppLovinAd e;
    private final d f;

    public h(d dVar, l lVar) {
        super(new JSONObject(), new JSONObject(), b.UNKNOWN, lVar);
        this.f = dVar;
    }

    private AppLovinAd c() {
        return this.sdk.w().d(this.f);
    }

    private String d() {
        d adZone = getAdZone();
        if (adZone == null || adZone.e()) {
            return null;
        }
        return adZone.a();
    }

    public AppLovinAd a() {
        return this.e;
    }

    public void a(AppLovinAd appLovinAd) {
        this.e = appLovinAd;
    }

    public AppLovinAd b() {
        AppLovinAd appLovinAd = this.e;
        return appLovinAd != null ? appLovinAd : c();
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || h.class != obj.getClass()) {
            return false;
        }
        AppLovinAd b = b();
        return b != null ? b.equals(obj) : super.equals(obj);
    }

    public long getAdIdNumber() {
        AppLovinAd b = b();
        if (b != null) {
            return b.getAdIdNumber();
        }
        return 0;
    }

    public d getAdZone() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getAdZone() : this.f;
    }

    public long getCreatedAtMillis() {
        AppLovinAd b = b();
        if (b instanceof AppLovinAdBase) {
            return ((AppLovinAdBase) b).getCreatedAtMillis();
        }
        return 0;
    }

    public AppLovinAdSize getSize() {
        return getAdZone().c();
    }

    public b getSource() {
        AppLovinAdBase appLovinAdBase = (AppLovinAdBase) b();
        return appLovinAdBase != null ? appLovinAdBase.getSource() : b.UNKNOWN;
    }

    public AppLovinAdType getType() {
        return getAdZone().d();
    }

    public String getZoneId() {
        if (this.f.e()) {
            return null;
        }
        return this.f.a();
    }

    public int hashCode() {
        AppLovinAd b = b();
        return b != null ? b.hashCode() : super.hashCode();
    }

    public boolean isVideoAd() {
        AppLovinAd b = b();
        return b != null && b.isVideoAd();
    }

    public String toString() {
        return "AppLovinAd{ #" + getAdIdNumber() + ", adType=" + getType() + ", adSize=" + getSize() + ", zoneId='" + d() + '\'' + '}';
    }
}
